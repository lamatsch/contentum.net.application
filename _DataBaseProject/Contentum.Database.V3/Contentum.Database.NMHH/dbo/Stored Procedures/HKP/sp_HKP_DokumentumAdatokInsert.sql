﻿create procedure [dbo].[sp_HKP_DokumentumAdatokInsert]    
                @Id      uniqueidentifier = null,    
                               @Irany     int  = null,
                @Allapot     int  = null,
                @FeladoTipusa     int  = null,
                @KapcsolatiKod     Nvarchar(400)  = null,
                @Nev     Nvarchar(400)  = null,
                @Email     Nvarchar(400)  = null,
                @RovidNev     Nvarchar(100)  = null,
                @MAKKod     Nvarchar(400)  = null,
                @KRID     int  = null,
                @ErkeztetesiSzam     Nvarchar(400)  = null,
                @HivatkozasiSzam     Nvarchar(400)  = null,
                @DokTipusHivatal     Nvarchar(100)  = null,
                @DokTipusAzonosito     Nvarchar(100)  = null,
                @DokTipusLeiras     Nvarchar(400)  = null,
                @Megjegyzes     Nvarchar(4000)  = null,
                @FileNev     Nvarchar(400)  = null,
                @ErvenyessegiDatum     datetime  = null,
                @ErkeztetesiDatum     datetime  = null,
                @Kezbesitettseg     int  = null,
                @Idopecset     Nvarchar(4000)  = null,
                @ValaszTitkositas     char(1)  = null,
                @ValaszUtvonal     int  = null,
                @Rendszeruzenet     char(1)  = null,
                @Tarterulet     int  = null,
                @ETertiveveny     int  = null,
                @Lenyomat     Nvarchar(4000)  = null,
                @Dokumentum_Id     uniqueidentifier  = null,
                @KuldKuldemeny_Id     uniqueidentifier  = null,
                @IraIrat_Id     uniqueidentifier  = null,
                @IratPeldany_Id     uniqueidentifier  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Irany is not null
         begin
            SET @insertColumns = @insertColumns + ',Irany'
            SET @insertValues = @insertValues + ',@Irany'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @FeladoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladoTipusa'
            SET @insertValues = @insertValues + ',@FeladoTipusa'
         end 
       
         if @KapcsolatiKod is not null
         begin
            SET @insertColumns = @insertColumns + ',KapcsolatiKod'
            SET @insertValues = @insertValues + ',@KapcsolatiKod'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Email is not null
         begin
            SET @insertColumns = @insertColumns + ',Email'
            SET @insertValues = @insertValues + ',@Email'
         end 
       
         if @RovidNev is not null
         begin
            SET @insertColumns = @insertColumns + ',RovidNev'
            SET @insertValues = @insertValues + ',@RovidNev'
         end 
       
         if @MAKKod is not null
         begin
            SET @insertColumns = @insertColumns + ',MAKKod'
            SET @insertValues = @insertValues + ',@MAKKod'
         end 
       
         if @KRID is not null
         begin
            SET @insertColumns = @insertColumns + ',KRID'
            SET @insertValues = @insertValues + ',@KRID'
         end 
       
         if @ErkeztetesiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',ErkeztetesiSzam'
            SET @insertValues = @insertValues + ',@ErkeztetesiSzam'
         end 
       
         if @HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@HivatkozasiSzam'
         end 
       
         if @DokTipusHivatal is not null
         begin
            SET @insertColumns = @insertColumns + ',DokTipusHivatal'
            SET @insertValues = @insertValues + ',@DokTipusHivatal'
         end 
       
         if @DokTipusAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',DokTipusAzonosito'
            SET @insertValues = @insertValues + ',@DokTipusAzonosito'
         end 
       
         if @DokTipusLeiras is not null
         begin
            SET @insertColumns = @insertColumns + ',DokTipusLeiras'
            SET @insertValues = @insertValues + ',@DokTipusLeiras'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @FileNev is not null
         begin
            SET @insertColumns = @insertColumns + ',FileNev'
            SET @insertValues = @insertValues + ',@FileNev'
         end 
       
         if @ErvenyessegiDatum is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvenyessegiDatum'
            SET @insertValues = @insertValues + ',@ErvenyessegiDatum'
         end 
       
         if @ErkeztetesiDatum is not null
         begin
            SET @insertColumns = @insertColumns + ',ErkeztetesiDatum'
            SET @insertValues = @insertValues + ',@ErkeztetesiDatum'
         end 
       
         if @Kezbesitettseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Kezbesitettseg'
            SET @insertValues = @insertValues + ',@Kezbesitettseg'
         end 
       
         if @Idopecset is not null
         begin
            SET @insertColumns = @insertColumns + ',Idopecset'
            SET @insertValues = @insertValues + ',@Idopecset'
         end 
       
         if @ValaszTitkositas is not null
         begin
            SET @insertColumns = @insertColumns + ',ValaszTitkositas'
            SET @insertValues = @insertValues + ',@ValaszTitkositas'
         end 
       
         if @ValaszUtvonal is not null
         begin
            SET @insertColumns = @insertColumns + ',ValaszUtvonal'
            SET @insertValues = @insertValues + ',@ValaszUtvonal'
         end 
       
         if @Rendszeruzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',Rendszeruzenet'
            SET @insertValues = @insertValues + ',@Rendszeruzenet'
         end 
       
         if @Tarterulet is not null
         begin
            SET @insertColumns = @insertColumns + ',Tarterulet'
            SET @insertValues = @insertValues + ',@Tarterulet'
         end 
       
         if @ETertiveveny is not null
         begin
            SET @insertColumns = @insertColumns + ',ETertiveveny'
            SET @insertValues = @insertValues + ',@ETertiveveny'
         end 
       
         if @Lenyomat is not null
         begin
            SET @insertColumns = @insertColumns + ',Lenyomat'
            SET @insertValues = @insertValues + ',@Lenyomat'
         end 
       
         if @Dokumentum_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id'
            SET @insertValues = @insertValues + ',@Dokumentum_Id'
         end 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @IraIrat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrat_Id'
            SET @insertValues = @insertValues + ',@IraIrat_Id'
         end 
       
         if @IratPeldany_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IratPeldany_Id'
            SET @insertValues = @insertValues + ',@IratPeldany_Id'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into HKP_DokumentumAdatok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Irany int,@Allapot int,@FeladoTipusa int,@KapcsolatiKod Nvarchar(400),@Nev Nvarchar(400),@Email Nvarchar(400),@RovidNev Nvarchar(100),@MAKKod Nvarchar(400),@KRID int,@ErkeztetesiSzam Nvarchar(400),@HivatkozasiSzam Nvarchar(400),@DokTipusHivatal Nvarchar(100),@DokTipusAzonosito Nvarchar(100),@DokTipusLeiras Nvarchar(400),@Megjegyzes Nvarchar(4000),@FileNev Nvarchar(400),@ErvenyessegiDatum datetime,@ErkeztetesiDatum datetime,@Kezbesitettseg int,@Idopecset Nvarchar(4000),@ValaszTitkositas char(1),@ValaszUtvonal int,@Rendszeruzenet char(1),@Tarterulet int,@ETertiveveny int,@Lenyomat Nvarchar(4000),@Dokumentum_Id uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@IraIrat_Id uniqueidentifier,@IratPeldany_Id uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Irany = @Irany,@Allapot = @Allapot,@FeladoTipusa = @FeladoTipusa,@KapcsolatiKod = @KapcsolatiKod,@Nev = @Nev,@Email = @Email,@RovidNev = @RovidNev,@MAKKod = @MAKKod,@KRID = @KRID,@ErkeztetesiSzam = @ErkeztetesiSzam,@HivatkozasiSzam = @HivatkozasiSzam,@DokTipusHivatal = @DokTipusHivatal,@DokTipusAzonosito = @DokTipusAzonosito,@DokTipusLeiras = @DokTipusLeiras,@Megjegyzes = @Megjegyzes,@FileNev = @FileNev,@ErvenyessegiDatum = @ErvenyessegiDatum,@ErkeztetesiDatum = @ErkeztetesiDatum,@Kezbesitettseg = @Kezbesitettseg,@Idopecset = @Idopecset,@ValaszTitkositas = @ValaszTitkositas,@ValaszUtvonal = @ValaszUtvonal,@Rendszeruzenet = @Rendszeruzenet,@Tarterulet = @Tarterulet,@ETertiveveny = @ETertiveveny,@Lenyomat = @Lenyomat,@Dokumentum_Id = @Dokumentum_Id,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@IraIrat_Id = @IraIrat_Id,@IratPeldany_Id = @IratPeldany_Id,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'HKP_DokumentumAdatok',@ResultUid
					,'HKP_DokumentumAdatokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH