﻿
create procedure sp_INT_AutoEUzenetSzabalyGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_AutoEUzenetSzabaly.Id,
	   INT_AutoEUzenetSzabaly.Nev,
	   INT_AutoEUzenetSzabaly.Leiras,
	   INT_AutoEUzenetSzabaly.Szabaly,
	   INT_AutoEUzenetSzabaly.Sorrend,
	   INT_AutoEUzenetSzabaly.Tipus,
	   INT_AutoEUzenetSzabaly.Leallito,
	   INT_AutoEUzenetSzabaly.Ver,
	   INT_AutoEUzenetSzabaly.Note,
	   INT_AutoEUzenetSzabaly.Stat_id,
	   INT_AutoEUzenetSzabaly.ErvKezd,
	   INT_AutoEUzenetSzabaly.ErvVege,
	   INT_AutoEUzenetSzabaly.Letrehozo_id,
	   INT_AutoEUzenetSzabaly.LetrehozasIdo,
	   INT_AutoEUzenetSzabaly.Modosito_id,
	   INT_AutoEUzenetSzabaly.ModositasIdo,
	   INT_AutoEUzenetSzabaly.Zarolo_id,
	   INT_AutoEUzenetSzabaly.ZarolasIdo,
	   INT_AutoEUzenetSzabaly.Tranz_id,
	   INT_AutoEUzenetSzabaly.UIAccessLog_id
	   from 
		 INT_AutoEUzenetSzabaly as INT_AutoEUzenetSzabaly 
	   where
		 INT_AutoEUzenetSzabaly.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end