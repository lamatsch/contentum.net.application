﻿/*SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1
            FROM  SYSOBJECTS
           WHERE  ID = OBJECT_ID('sp_INT_AutoEUzenetSzabalyInsert')
            AND   TYPE = 'P')
   DROP PROCEDURE sp_INT_AutoEUzenetSzabalyInsert
GO
*/
create procedure sp_INT_AutoEUzenetSzabalyInsert    
                @Id      uniqueidentifier = null,    
               	@Nev     Nvarchar(400),
                @Leiras     Nvarchar(400)  = null,
                @Szabaly     Nvarchar(max)  = null,
	            @Sorrend     int			= null,
                @Tipus     Nvarchar(100)  = null,
                @Leallito     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

				@UpdatedColumns              xml = null,
				@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
/*SORREND BEALLITASA*/
 DECLARE @NEWSORREND INT
 DECLARE @MAXSORREND INT

 SELECT @MAXSORREND = MAX(ISNULL(SORREND,0))
 FROM INT_AUTOEUZENETSZABALY
 WHERE getdate() between ErvKezd and ErvVege

  IF(@sorrend IS NULL OR @sorrend <= 0)
	 BEGIN
		SET @Sorrend = @MAXSORREND+1
		begin try
			begin
				SET @Szabaly = 	REPLACE(@Szabaly,'"OrderProperty":0','"OrderProperty":'+(CAST(@Sorrend AS VARCHAR(MAX))))
			end
		end try
		begin catch
		end catch
	 END
  ELSE IF (@Sorrend <= @MAXSORREND)
	BEGIN
		BEGIN TRANSACTION
			BEGIN TRY
				UPDATE INT_AUTOEUZENETSZABALY
				SET SORREND = SORREND +1,
					Szabaly = REPLACE(Szabaly,'"OrderProperty":'+CAST(SORREND AS VARCHAR(MAX)),'"OrderProperty":'+(CAST(SORREND+1 AS VARCHAR(MAX))))
				WHERE SORREND >= @SORREND
				    AND GETDATE() BETWEEN ERVKEZD AND ERVVEGE
			END TRY
			BEGIN CATCH

 			    ROLLBACK TRANSACTION

				DECLARE @SerrorSeverity INT, @SerrorState INT
				DECLARE @SerrorCode NVARCHAR(1000)    
				SET @SerrorSeverity = ERROR_SEVERITY()
				SET @SerrorState = ERROR_STATE()
	
				if ERROR_NUMBER()<50000	
					SET @SerrorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
				else
					SET @SerrorCode = ERROR_MESSAGE()
      
			   if @SerrorState = 0 SET @SerrorState = 1

				RAISERROR(@SerrorCode,@SerrorSeverity,@SerrorState)
			END CATCH
		COMMIT TRANSACTION
	END
  ELSE IF (@sorrend > (@MAXSORREND + 1))
	BEGIN
		SET @sorrend = @MAXSORREND + 1
		begin try	
			begin
				DECLARE @JSON_SORREND_FIELD VARCHAR(MAX)
				SELECT @JSON_SORREND_FIELD =  SUBSTRING(@Szabaly, CHARINDEX('"OrderProperty":', @Szabaly), CHARINDEX(',"ParametersProperty',@Szabaly) - CHARINDEX('"OrderProperty":', @Szabaly) )
				SET @Szabaly = 	REPLACE(@Szabaly,@JSON_SORREND_FIELD,'"OrderProperty":'+(CAST(@Sorrend AS VARCHAR(MAX))))
			end
		end try
		begin catch
		end catch
		
	END
  ELSE 
	BEGIN
		SET @sorrend = @sorrend
	END
/*SORREND BEALLITASA*/


DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @Szabaly is not null
         begin
            SET @insertColumns = @insertColumns + ',Szabaly'
            SET @insertValues = @insertValues + ',@Szabaly'
         end 
       
         if @Sorrend is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorrend'
            SET @insertValues = @insertValues + ',@Sorrend'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Leallito is not null
         begin
            SET @insertColumns = @insertColumns + ',Leallito'
            SET @insertValues = @insertValues + ',@Leallito'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into INT_AutoEUzenetSzabaly ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Nev Nvarchar(400),@Leiras Nvarchar(400),@Szabaly Nvarchar(max),@Sorrend int,@Tipus Nvarchar(100),@Leallito char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Nev = @Nev,@Leiras = @Leiras,@Szabaly = @Szabaly,@Sorrend = @Sorrend,@Tipus = @Tipus,@Leallito = @Leallito,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'INT_AutoEUzenetSzabaly',@ResultUid
					,'INT_AutoEUzenetSzabalyHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH