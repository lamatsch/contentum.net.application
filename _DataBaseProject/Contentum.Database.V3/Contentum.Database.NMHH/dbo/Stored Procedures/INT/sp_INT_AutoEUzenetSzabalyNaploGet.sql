﻿
create procedure sp_INT_AutoEUzenetSzabalyNaploGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_AutoEUzenetSzabalyNaplo.Id,
	   INT_AutoEUzenetSzabalyNaplo.ESzabalyId,
	   INT_AutoEUzenetSzabalyNaplo.EUzenetId,
	   INT_AutoEUzenetSzabalyNaplo.EUzenetTipusId,
	   INT_AutoEUzenetSzabalyNaplo.Megjegyzes,
	   INT_AutoEUzenetSzabalyNaplo.Ver,
	   INT_AutoEUzenetSzabalyNaplo.Note,
	   INT_AutoEUzenetSzabalyNaplo.Stat_id,
	   INT_AutoEUzenetSzabalyNaplo.ErvKezd,
	   INT_AutoEUzenetSzabalyNaplo.ErvVege,
	   INT_AutoEUzenetSzabalyNaplo.Letrehozo_id,
	   INT_AutoEUzenetSzabalyNaplo.LetrehozasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Modosito_id,
	   INT_AutoEUzenetSzabalyNaplo.ModositasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Zarolo_id,
	   INT_AutoEUzenetSzabalyNaplo.ZarolasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Tranz_id,
	   INT_AutoEUzenetSzabalyNaplo.UIAccessLog_id
	   from 
		 INT_AutoEUzenetSzabalyNaplo as INT_AutoEUzenetSzabalyNaplo 
	   where
		 INT_AutoEUzenetSzabalyNaplo.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end