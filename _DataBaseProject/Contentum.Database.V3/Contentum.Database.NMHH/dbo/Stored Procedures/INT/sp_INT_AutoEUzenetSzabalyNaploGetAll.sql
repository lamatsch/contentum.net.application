﻿
create procedure sp_INT_AutoEUzenetSzabalyNaploGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_AutoEUzenetSzabalyNaplo.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_AutoEUzenetSzabalyNaplo.Id,
	   INT_AutoEUzenetSzabalyNaplo.ESzabalyId,
	   INT_AutoEUzenetSzabalyNaplo.EUzenetId,
	   INT_AutoEUzenetSzabalyNaplo.EUzenetTipusId,
	   INT_AutoEUzenetSzabalyNaplo.Megjegyzes,
	   INT_AutoEUzenetSzabalyNaplo.Ver,
	   INT_AutoEUzenetSzabalyNaplo.Note,
	   INT_AutoEUzenetSzabalyNaplo.Stat_id,
	   INT_AutoEUzenetSzabalyNaplo.ErvKezd,
	   INT_AutoEUzenetSzabalyNaplo.ErvVege,
	   INT_AutoEUzenetSzabalyNaplo.Letrehozo_id,
	   INT_AutoEUzenetSzabalyNaplo.LetrehozasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Modosito_id,
	   INT_AutoEUzenetSzabalyNaplo.ModositasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Zarolo_id,
	   INT_AutoEUzenetSzabalyNaplo.ZarolasIdo,
	   INT_AutoEUzenetSzabalyNaplo.Tranz_id,
	   INT_AutoEUzenetSzabalyNaplo.UIAccessLog_id  
   from 
     INT_AutoEUzenetSzabalyNaplo as INT_AutoEUzenetSzabalyNaplo      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end