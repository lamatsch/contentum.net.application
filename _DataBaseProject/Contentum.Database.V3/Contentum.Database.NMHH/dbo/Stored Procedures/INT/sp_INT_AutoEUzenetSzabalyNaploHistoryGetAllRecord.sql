﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_AutoEUzenetSzabalyNaploHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_INT_AutoEUzenetSzabalyNaploHistoryGetAllRecord
go
*/
create procedure sp_INT_AutoEUzenetSzabalyNaploHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from INT_AutoEUzenetSzabalyNaploHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_AutoEUzenetSzabalyNaploHistory Old
         inner join INT_AutoEUzenetSzabalyNaploHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_AutoEUzenetSzabalyNaploHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ESzabalyId' as ColumnName,               cast(Old.ESzabalyId as nvarchar(99)) as OldValue,
               cast(New.ESzabalyId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_AutoEUzenetSzabalyNaploHistory Old
         inner join INT_AutoEUzenetSzabalyNaploHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_AutoEUzenetSzabalyNaploHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ESzabalyId as nvarchar(max)),'') != ISNULL(CAST(New.ESzabalyId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EUzenetId' as ColumnName,               cast(Old.EUzenetId as nvarchar(99)) as OldValue,
               cast(New.EUzenetId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_AutoEUzenetSzabalyNaploHistory Old
         inner join INT_AutoEUzenetSzabalyNaploHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_AutoEUzenetSzabalyNaploHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EUzenetId as nvarchar(max)),'') != ISNULL(CAST(New.EUzenetId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EUzenetTipusId' as ColumnName,               cast(Old.EUzenetTipusId as nvarchar(99)) as OldValue,
               cast(New.EUzenetTipusId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_AutoEUzenetSzabalyNaploHistory Old
         inner join INT_AutoEUzenetSzabalyNaploHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_AutoEUzenetSzabalyNaploHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EUzenetTipusId as nvarchar(max)),'') != ISNULL(CAST(New.EUzenetTipusId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_AutoEUzenetSzabalyNaploHistory Old
         inner join INT_AutoEUzenetSzabalyNaploHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_AutoEUzenetSzabalyNaploHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Megjegyzes as nvarchar(max)),'') != ISNULL(CAST(New.Megjegyzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end