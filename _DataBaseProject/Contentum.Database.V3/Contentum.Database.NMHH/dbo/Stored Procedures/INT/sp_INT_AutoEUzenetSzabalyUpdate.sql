﻿/*SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1
            FROM  SYSOBJECTS
           WHERE  ID = OBJECT_ID('SP_INT_AUTOEUZENETSZABALYUPDATE')
            AND   TYPE = 'P')
   DROP PROCEDURE SP_INT_AUTOEUZENETSZABALYUPDATE
GO
*/

create procedure sp_INT_AutoEUzenetSzabalyUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Nev     Nvarchar(400)  = null,         
             @Leiras     Nvarchar(400)  = null,         
             @Szabaly     Nvarchar(max)  = null,         
             @Sorrend     int  = null,         
             @Tipus     Nvarchar(100)  = null,         
             @Leallito     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Nev     Nvarchar(400)         
     DECLARE @Act_Leiras     Nvarchar(400)         
     DECLARE @Act_Szabaly     Nvarchar(max)         
     DECLARE @Act_Sorrend     int         
     DECLARE @Act_Tipus     Nvarchar(100)         
     DECLARE @Act_Leallito     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Nev = Nev,
     @Act_Leiras = Leiras,
     @Act_Szabaly = Szabaly,
     @Act_Sorrend = Sorrend,
     @Act_Tipus = Tipus,
     @Act_Leallito = Leallito,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from INT_AutoEUzenetSzabaly
where Id = @Id

--------------------------
/*SORREND BEALLITASA*/
 DECLARE @NEWSORREND INT
 DECLARE @MAXSORREND INT

 SELECT @MAXSORREND = MAX(ISNULL(SORREND,0))
 FROM INT_AUTOEUZENETSZABALY
 WHERE GETDATE() BETWEEN ErvKezd and ErvVege
 
 IF(@sorrend IS NULL OR @sorrend <= 0)
	 BEGIN
		SET @Sorrend = @MAXSORREND+1
		begin try
			begin
				SET @Szabaly = 	REPLACE(@Szabaly,'"OrderProperty":0','"OrderProperty":'+(CAST(@Sorrend AS VARCHAR(MAX))))
			end
		end try
		begin catch
		end catch
	 END
--------------------------

  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/Szabaly')=1
         SET @Act_Szabaly = @Szabaly
   IF @UpdatedColumns.exist('/root/Sorrend')=1
         SET @Act_Sorrend = @Sorrend
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Leallito')=1
         SET @Act_Leallito = @Leallito
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   


   

UPDATE INT_AutoEUzenetSzabaly
SET
     Nev = @Act_Nev,
     Leiras = @Act_Leiras,
     Szabaly = @Act_Szabaly,
     Sorrend = @Act_Sorrend,
     Tipus = @Act_Tipus,
     Leallito = @Act_Leallito,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'INT_AutoEUzenetSzabaly',@Id
					,'INT_AutoEUzenetSzabalyHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH