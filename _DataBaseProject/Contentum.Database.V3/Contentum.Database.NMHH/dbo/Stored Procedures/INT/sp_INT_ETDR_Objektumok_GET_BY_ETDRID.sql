﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ETDR_Objektumok_Get_By_EtdrId')
            and   type = 'P')
   drop procedure sp_INT_ETDR_Objektumok_Get_By_EtdrId
go
*/
CREATE PROCEDURE sp_INT_ETDR_Objektumok_Get_By_EtdrId
		 @ETDR_Id			INT,
		 @Obj_Tip_Kod		NVARCHAR(100),
         @ExecutorUserId	UNIQUEIDENTIFIER       
as

begin
BEGIN TRY

	set nocount on

    DECLARE @Obj_Tip_Id UNIQUEIDENTIFIER
	SET @Obj_Tip_Id = (SELECT Id FROM KRT_ObjTipusok WHERE Kod = @Obj_Tip_Kod)

	IF @Obj_Tip_Id is NULL
		RAISERROR('A objektum típus nem található!',16,1)

	SELECT * 
	FROM INT_ETDR_Objektumok
	WHERE ETDR_Id = @ETDR_Id
	AND Obj_Tip_Id = @Obj_Tip_Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end