﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ETDR_Objektumok_Get_IntMod')
            and   type = 'P')
   drop procedure sp_INT_ETDR_Objektumok_Get_IntMod
go
*/
CREATE PROCEDURE sp_INT_ETDR_Objektumok_Get_IntMod
      @ExecutorUserId	UNIQUEIDENTIFIER = NULL      
as

begin
BEGIN TRY

	SET NOCOUNT ON

	DECLARE @KCS_IRATTIPUS_ID UNIQUEIDENTIFIER
	SELECT @KCS_IRATTIPUS_ID = Id FROM KRT_KodCsoportok WHERE Kod = 'IRATTIPUS'

	SELECT EO.ETDR_Id AS VALASZ_TIP_ID, 
		   KT.Nev AS VALASZ_TIP_NEV 
	FROM KRT_KodTarak AS KT
	INNER JOIN INT_ETDR_Objektumok AS EO ON EO.Obj_Id = KT.Id
	WHERE KT.KodCsoport_id = @KCS_IRATTIPUS_ID
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end