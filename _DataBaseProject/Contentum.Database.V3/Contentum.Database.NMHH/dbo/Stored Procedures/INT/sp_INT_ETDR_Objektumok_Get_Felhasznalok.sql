﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ETDR_Objektumok_Get_Felhasznalok')
            and   type = 'P')
   drop procedure sp_INT_ETDR_Objektumok_Get_Felhasznalok
go
*/

CREATE PROCEDURE sp_INT_ETDR_Objektumok_Get_Felhasznalok
         @ExecutorUserId	UNIQUEIDENTIFIER       
as

begin
BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	declare @Obj_Tip_Id uniqueidentifier
	set @Obj_Tip_Id = (select Id from KRT_ObjTipusok where Kod = 'KRT_Felhasznalok')

	select Id, Nev into #result from KRT_Felhasznalok
	where Org = @Org
	and GETDATE() between ErvKezd and ErvVege

	-- felhasználók beszúrása az INT_ETDR_Objektumok táblába, ha még nincs benne
	INSERT INTO INT_ETDR_Objektumok
	(Obj_Tip_Id, Obj_Id)
	SELECT @Obj_Tip_Id, Id
	FROM #result result
	WHERE result.Id not in
	(select Obj_Id from INT_ETDR_Objektumok where Obj_Tip_Id = @Obj_Tip_Id)

	select etdr_obj.ETDR_Id,
		   result.Nev
	from  #result result
	join INT_ETDR_Objektumok etdr_obj on result.Id = etdr_obj.Obj_Id and etdr_obj.Obj_Tip_Id = @Obj_Tip_Id
	order by etdr_obj.ETDR_Id

	IF OBJECT_ID('tempdb..#result') IS NOT NULL 
		DROP TABLE #result


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end