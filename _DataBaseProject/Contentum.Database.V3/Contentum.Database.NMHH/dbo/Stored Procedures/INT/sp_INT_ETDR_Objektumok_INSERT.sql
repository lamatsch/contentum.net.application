﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ETDR_Objektumok_INSERT')
            and   type = 'P')
   drop procedure sp_INT_ETDR_Objektumok_INSERT
go
*/
CREATE PROCEDURE sp_INT_ETDR_Objektumok_Insert    
	@Obj_Tip_Id      	UNIQUEIDENTIFIER	= NULL, 
    @Obj_Tip_Nev      	NVARCHAR(MAX)		= NULL,  
	@Obj_Id				UNIQUEIDENTIFIER,
	@ETDR_Data  	   	NVARCHAR(MAX)		= NULL,
	@ETDR_Note  	   	NVARCHAR(MAX)		= NULL,
	@Letrehozo_id		UNIQUEIDENTIFIER	= NULL,
	@Org     			UNIQUEIDENTIFIER 	= NULL,  
	@ResultUid          INT				 	OUTPUT
		  
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
 SET NOCOUNT ON;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
 END
 
 IF @Obj_Tip_Id IS NULL AND @Obj_Tip_Nev IS NULL
 BEGIN 
   	RAISERROR('Incorrect Obj_Tip parameter',16,1)
 END
 
 IF @Obj_Tip_Id IS NULL
 BEGIN 
   	SELECT @Obj_Tip_Id = Id FROM KRT_ObjTipusok 
	WHERE Kod = @Obj_Tip_Nev
	
	IF @Obj_Tip_Id IS NULL
		BEGIN
			RAISERROR('Incorrect Obj_Tip parameter',16,1)
		END
 END
 
	IF NOT EXISTS (SELECT 1 
				   FROM INT_ETDR_Objektumok 
				   WHERE Obj_Tip_Id = @Obj_Tip_Id AND Obj_Id = @Obj_Id )
		BEGIN
			INSERT INTO INT_ETDR_Objektumok(Obj_Tip_Id,Obj_Id,ETDR_Data, ETDR_Note)
			VALUES(@Obj_Tip_Id,@Obj_Id, @ETDR_Data, @ETDR_Note)
			
			SELECT @ResultUid = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			SELECT @ResultUid = ETDR_Id
		    FROM INT_ETDR_Objektumok 
		    WHERE Obj_Tip_Id = @Obj_Tip_Id AND Obj_Id = @Obj_Id 
		END
	
	
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
GO