
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ExternalIDsGet')
            and   type = 'P')
   drop procedure sp_INT_ExternalIDsGet
go
*/
create procedure sp_INT_ExternalIDsGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_ExternalIDs.Id,
	   INT_ExternalIDs.Modul_Id,
	   INT_ExternalIDs.Edok_Type,
	   INT_ExternalIDs.Edok_Id,
	   INT_ExternalIDs.Edok_ExpiredDate,
	   INT_ExternalIDs.External_Group,
	   INT_ExternalIDs.External_Id,
	   INT_ExternalIDs.Deleted,
	   INT_ExternalIDs.LastSync,
	   INT_ExternalIDs.State,
	   INT_ExternalIDs.Ver,
	   INT_ExternalIDs.Note,
	   INT_ExternalIDs.Stat_id,
	   INT_ExternalIDs.ErvKezd,
	   INT_ExternalIDs.ErvVege,
	   INT_ExternalIDs.Letrehozo_id,
	   INT_ExternalIDs.LetrehozasIdo,
	   INT_ExternalIDs.Modosito_id,
	   INT_ExternalIDs.ModositasIdo,
	   INT_ExternalIDs.Zarolo_id,
	   INT_ExternalIDs.ZarolasIdo,
	   INT_ExternalIDs.Tranz_id,
	   INT_ExternalIDs.UIAccessLog_id
	   from 
		 INT_ExternalIDs as INT_ExternalIDs 
	   where
		 INT_ExternalIDs.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
