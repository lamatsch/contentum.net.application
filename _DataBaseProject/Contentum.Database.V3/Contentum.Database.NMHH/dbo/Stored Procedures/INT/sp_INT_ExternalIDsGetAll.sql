/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ExternalIDsGetAll')
            and   type = 'P')
   drop procedure sp_INT_ExternalIDsGetAll
go
*/
create procedure sp_INT_ExternalIDsGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_ExternalIDs.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_ExternalIDs.Id,
	   INT_ExternalIDs.Modul_Id,
	   INT_ExternalIDs.Edok_Type,
	   INT_ExternalIDs.Edok_Id,
	   INT_ExternalIDs.Edok_ExpiredDate,
	   INT_ExternalIDs.External_Group,
	   INT_ExternalIDs.External_Id,
	   INT_ExternalIDs.Deleted,
	   INT_ExternalIDs.LastSync,
	   INT_ExternalIDs.State,
	   INT_ExternalIDs.Ver,
	   INT_ExternalIDs.Note,
	   INT_ExternalIDs.Stat_id,
	   INT_ExternalIDs.ErvKezd,
	   INT_ExternalIDs.ErvVege,
	   INT_ExternalIDs.Letrehozo_id,
	   INT_ExternalIDs.LetrehozasIdo,
	   INT_ExternalIDs.Modosito_id,
	   INT_ExternalIDs.ModositasIdo,
	   INT_ExternalIDs.Zarolo_id,
	   INT_ExternalIDs.ZarolasIdo,
	   INT_ExternalIDs.Tranz_id,
	   INT_ExternalIDs.UIAccessLog_id  
   from 
     INT_ExternalIDs as INT_ExternalIDs      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go