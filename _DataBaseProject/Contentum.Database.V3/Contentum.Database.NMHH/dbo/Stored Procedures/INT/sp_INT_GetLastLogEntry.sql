﻿CREATE procedure [dbo].[sp_INT_GetLastLogEntry]
				@Modul_id   uniqueidentifier  -- INT_Modul azonosító
AS
/*
-- FELADATA:
	Adott Modul utolsó futtatásának lekérdezése
*/
---------
---------
BEGIN TRY

set nocount on;


Select TOP 1 * from INT_Log
WHERE Modul_Id = @Modul_id
ORDER BY Sync_StartDate

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------