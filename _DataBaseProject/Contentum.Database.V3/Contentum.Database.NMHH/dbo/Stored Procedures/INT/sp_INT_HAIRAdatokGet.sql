
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_HAIRAdatokGet')
            and   type = 'P')
   drop procedure sp_INT_HAIRAdatokGet
go
*/
create procedure sp_INT_HAIRAdatokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_HAIRAdatok.Id,
	   INT_HAIRAdatok.Obj_Id,
	   INT_HAIRAdatok.ObjTip_Id,
	   INT_HAIRAdatok.Tipus,
	   INT_HAIRAdatok.Ertek,
	   INT_HAIRAdatok.Ver,
	   INT_HAIRAdatok.Note,
	   INT_HAIRAdatok.Stat_id,
	   INT_HAIRAdatok.ErvKezd,
	   INT_HAIRAdatok.ErvVege,
	   INT_HAIRAdatok.Letrehozo_id,
	   INT_HAIRAdatok.LetrehozasIdo,
	   INT_HAIRAdatok.Modosito_id,
	   INT_HAIRAdatok.ModositasIdo,
	   INT_HAIRAdatok.Zarolo_id,
	   INT_HAIRAdatok.ZarolasIdo,
	   INT_HAIRAdatok.Tranz_id,
	   INT_HAIRAdatok.UIAccessLog_id
	   from 
		 INT_HAIRAdatok as INT_HAIRAdatok 
	   where
		 INT_HAIRAdatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
