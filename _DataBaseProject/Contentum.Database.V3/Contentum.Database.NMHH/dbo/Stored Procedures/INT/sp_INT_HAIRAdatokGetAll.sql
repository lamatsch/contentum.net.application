/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_HAIRAdatokGetAll')
            and   type = 'P')
   drop procedure sp_INT_HAIRAdatokGetAll
go
*/
create procedure sp_INT_HAIRAdatokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_HAIRAdatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_HAIRAdatok.Id,
	   INT_HAIRAdatok.Obj_Id,
	   INT_HAIRAdatok.ObjTip_Id,
	   INT_HAIRAdatok.Tipus,
	   INT_HAIRAdatok.Ertek,
	   INT_HAIRAdatok.Ver,
	   INT_HAIRAdatok.Note,
	   INT_HAIRAdatok.Stat_id,
	   INT_HAIRAdatok.ErvKezd,
	   INT_HAIRAdatok.ErvVege,
	   INT_HAIRAdatok.Letrehozo_id,
	   INT_HAIRAdatok.LetrehozasIdo,
	   INT_HAIRAdatok.Modosito_id,
	   INT_HAIRAdatok.ModositasIdo,
	   INT_HAIRAdatok.Zarolo_id,
	   INT_HAIRAdatok.ZarolasIdo,
	   INT_HAIRAdatok.Tranz_id,
	   INT_HAIRAdatok.UIAccessLog_id  
   from 
     INT_HAIRAdatok as INT_HAIRAdatok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go