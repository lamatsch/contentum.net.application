
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_LogGet')
            and   type = 'P')
   drop procedure sp_INT_LogGet
go
*/
create procedure sp_INT_LogGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_Log.Id,
	   INT_Log.Org,
	   INT_Log.Modul_id,
	   INT_Log.Sync_StartDate,
	   INT_Log.Parancs,
	   INT_Log.Machine,
	   INT_Log.Sync_EndDate,
	   INT_Log.HibaKod,
	   INT_Log.HibaUzenet,
	   INT_Log.ExternalId,
	   INT_Log.Status,
	   INT_Log.Dokumentumok_Id_Sent,
	   INT_Log.Dokumentumok_Id_Error,
	   INT_Log.PackageHash,
	   INT_Log.PackageVer,
	   INT_Log.Ver,
	   INT_Log.Note,
	   INT_Log.Stat_id,
	   INT_Log.ErvKezd,
	   INT_Log.ErvVege,
	   INT_Log.Letrehozo_id,
	   INT_Log.LetrehozasIdo,
	   INT_Log.Modosito_id,
	   INT_Log.ModositasIdo,
	   INT_Log.Zarolo_id,
	   INT_Log.ZarolasIdo,
	   INT_Log.Tranz_id,
	   INT_Log.UIAccessLog_id
	   from 
		 INT_Log as INT_Log 
	   where
		 INT_Log.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
