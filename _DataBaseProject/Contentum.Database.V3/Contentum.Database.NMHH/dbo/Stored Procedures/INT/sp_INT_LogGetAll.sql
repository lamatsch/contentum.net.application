/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_LogGetAll')
            and   type = 'P')
   drop procedure sp_INT_LogGetAll
go
*/
create procedure sp_INT_LogGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_Log.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_Log.Id,
	   INT_Log.Org,
	   INT_Log.Modul_id,
	   INT_Log.Sync_StartDate,
	   INT_Log.Parancs,
	   INT_Log.Machine,
	   INT_Log.Sync_EndDate,
	   INT_Log.HibaKod,
	   INT_Log.HibaUzenet,
	   INT_Log.ExternalId,
	   INT_Log.Status,
	   INT_Log.Dokumentumok_Id_Sent,
	   INT_Log.Dokumentumok_Id_Error,
	   INT_Log.PackageHash,
	   INT_Log.PackageVer,
	   INT_Log.Ver,
	   INT_Log.Note,
	   INT_Log.Stat_id,
	   INT_Log.ErvKezd,
	   INT_Log.ErvVege,
	   INT_Log.Letrehozo_id,
	   INT_Log.LetrehozasIdo,
	   INT_Log.Modosito_id,
	   INT_Log.ModositasIdo,
	   INT_Log.Zarolo_id,
	   INT_Log.ZarolasIdo,
	   INT_Log.Tranz_id,
	   INT_Log.UIAccessLog_id  
   from 
     INT_Log as INT_Log      
    Where INT_Log.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go