/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_LogInsert')
            and   type = 'P')
   drop procedure sp_INT_LogInsert
go
*/
create procedure sp_INT_LogInsert    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
	            @Modul_id     uniqueidentifier,
                @Sync_StartDate     datetime  = null,
	            @Parancs     Nvarchar(400),
                @Machine     Nvarchar(100)  = null,
                @Sync_EndDate     datetime  = null,
                @HibaKod     Nvarchar(4000)  = null,
                @HibaUzenet     Nvarchar(4000)  = null,
                @ExternalId     Nvarchar(4000)  = null,
                @Status     Nvarchar(100)  = null,
                @Dokumentumok_Id_Sent     uniqueidentifier  = null,
                @Dokumentumok_Id_Error     uniqueidentifier  = null,
                @PackageHash     Nvarchar(100)  = null,
                @PackageVer     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Modul_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modul_id'
            SET @insertValues = @insertValues + ',@Modul_id'
         end 
       
         if @Sync_StartDate is not null
         begin
            SET @insertColumns = @insertColumns + ',Sync_StartDate'
            SET @insertValues = @insertValues + ',@Sync_StartDate'
         end 
       
         if @Parancs is not null
         begin
            SET @insertColumns = @insertColumns + ',Parancs'
            SET @insertValues = @insertValues + ',@Parancs'
         end 
       
         if @Machine is not null
         begin
            SET @insertColumns = @insertColumns + ',Machine'
            SET @insertValues = @insertValues + ',@Machine'
         end 
       
         if @Sync_EndDate is not null
         begin
            SET @insertColumns = @insertColumns + ',Sync_EndDate'
            SET @insertValues = @insertValues + ',@Sync_EndDate'
         end 
       
         if @HibaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaKod'
            SET @insertValues = @insertValues + ',@HibaKod'
         end 
       
         if @HibaUzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaUzenet'
            SET @insertValues = @insertValues + ',@HibaUzenet'
         end 
       
         if @ExternalId is not null
         begin
            SET @insertColumns = @insertColumns + ',ExternalId'
            SET @insertValues = @insertValues + ',@ExternalId'
         end 
       
         if @Status is not null
         begin
            SET @insertColumns = @insertColumns + ',Status'
            SET @insertValues = @insertValues + ',@Status'
         end 
       
         if @Dokumentumok_Id_Sent is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentumok_Id_Sent'
            SET @insertValues = @insertValues + ',@Dokumentumok_Id_Sent'
         end 
       
         if @Dokumentumok_Id_Error is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentumok_Id_Error'
            SET @insertValues = @insertValues + ',@Dokumentumok_Id_Error'
         end 
       
         if @PackageHash is not null
         begin
            SET @insertColumns = @insertColumns + ',PackageHash'
            SET @insertValues = @insertValues + ',@PackageHash'
         end 
       
         if @PackageVer is not null
         begin
            SET @insertColumns = @insertColumns + ',PackageVer'
            SET @insertValues = @insertValues + ',@PackageVer'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into INT_Log ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Modul_id uniqueidentifier,@Sync_StartDate datetime,@Parancs Nvarchar(400),@Machine Nvarchar(100),@Sync_EndDate datetime,@HibaKod Nvarchar(4000),@HibaUzenet Nvarchar(4000),@ExternalId Nvarchar(4000),@Status Nvarchar(100),@Dokumentumok_Id_Sent uniqueidentifier,@Dokumentumok_Id_Error uniqueidentifier,@PackageHash Nvarchar(100),@PackageVer int,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Modul_id = @Modul_id,@Sync_StartDate = @Sync_StartDate,@Parancs = @Parancs,@Machine = @Machine,@Sync_EndDate = @Sync_EndDate,@HibaKod = @HibaKod,@HibaUzenet = @HibaUzenet,@ExternalId = @ExternalId,@Status = @Status,@Dokumentumok_Id_Sent = @Dokumentumok_Id_Sent,@Dokumentumok_Id_Error = @Dokumentumok_Id_Error,@PackageHash = @PackageHash,@PackageVer = @PackageVer,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'INT_Log',@ResultUid
					,'INT_LogHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
