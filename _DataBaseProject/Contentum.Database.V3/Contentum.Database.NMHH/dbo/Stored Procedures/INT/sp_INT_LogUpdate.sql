
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_LogUpdate')
            and   type = 'P')
   drop procedure sp_INT_LogUpdate
go
*/
create procedure sp_INT_LogUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Modul_id     uniqueidentifier  = null,         
             @Sync_StartDate     datetime  = null,         
             @Parancs     Nvarchar(400)  = null,         
             @Machine     Nvarchar(100)  = null,         
             @Sync_EndDate     datetime  = null,         
             @HibaKod     Nvarchar(4000)  = null,         
             @HibaUzenet     Nvarchar(4000)  = null,         
             @ExternalId     Nvarchar(4000)  = null,         
             @Status     Nvarchar(100)  = null,         
             @Dokumentumok_Id_Sent     uniqueidentifier  = null,         
             @Dokumentumok_Id_Error     uniqueidentifier  = null,         
             @PackageHash     Nvarchar(100)  = null,         
             @PackageVer     int  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Modul_id     uniqueidentifier         
     DECLARE @Act_Sync_StartDate     datetime         
     DECLARE @Act_Parancs     Nvarchar(400)         
     DECLARE @Act_Machine     Nvarchar(100)         
     DECLARE @Act_Sync_EndDate     datetime         
     DECLARE @Act_HibaKod     Nvarchar(4000)         
     DECLARE @Act_HibaUzenet     Nvarchar(4000)         
     DECLARE @Act_ExternalId     Nvarchar(4000)         
     DECLARE @Act_Status     Nvarchar(100)         
     DECLARE @Act_Dokumentumok_Id_Sent     uniqueidentifier         
     DECLARE @Act_Dokumentumok_Id_Error     uniqueidentifier         
     DECLARE @Act_PackageHash     Nvarchar(100)         
     DECLARE @Act_PackageVer     int         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Modul_id = Modul_id,
     @Act_Sync_StartDate = Sync_StartDate,
     @Act_Parancs = Parancs,
     @Act_Machine = Machine,
     @Act_Sync_EndDate = Sync_EndDate,
     @Act_HibaKod = HibaKod,
     @Act_HibaUzenet = HibaUzenet,
     @Act_ExternalId = ExternalId,
     @Act_Status = Status,
     @Act_Dokumentumok_Id_Sent = Dokumentumok_Id_Sent,
     @Act_Dokumentumok_Id_Error = Dokumentumok_Id_Error,
     @Act_PackageHash = PackageHash,
     @Act_PackageVer = PackageVer,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from INT_Log
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Modul_id')=1
         SET @Act_Modul_id = @Modul_id
   IF @UpdatedColumns.exist('/root/Sync_StartDate')=1
         SET @Act_Sync_StartDate = @Sync_StartDate
   IF @UpdatedColumns.exist('/root/Parancs')=1
         SET @Act_Parancs = @Parancs
   IF @UpdatedColumns.exist('/root/Machine')=1
         SET @Act_Machine = @Machine
   IF @UpdatedColumns.exist('/root/Sync_EndDate')=1
         SET @Act_Sync_EndDate = @Sync_EndDate
   IF @UpdatedColumns.exist('/root/HibaKod')=1
         SET @Act_HibaKod = @HibaKod
   IF @UpdatedColumns.exist('/root/HibaUzenet')=1
         SET @Act_HibaUzenet = @HibaUzenet
   IF @UpdatedColumns.exist('/root/ExternalId')=1
         SET @Act_ExternalId = @ExternalId
   IF @UpdatedColumns.exist('/root/Status')=1
         SET @Act_Status = @Status
   IF @UpdatedColumns.exist('/root/Dokumentumok_Id_Sent')=1
         SET @Act_Dokumentumok_Id_Sent = @Dokumentumok_Id_Sent
   IF @UpdatedColumns.exist('/root/Dokumentumok_Id_Error')=1
         SET @Act_Dokumentumok_Id_Error = @Dokumentumok_Id_Error
   IF @UpdatedColumns.exist('/root/PackageHash')=1
         SET @Act_PackageHash = @PackageHash
   IF @UpdatedColumns.exist('/root/PackageVer')=1
         SET @Act_PackageVer = @PackageVer
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE INT_Log
SET
     Org = @Act_Org,
     Modul_id = @Act_Modul_id,
     Sync_StartDate = @Act_Sync_StartDate,
     Parancs = @Act_Parancs,
     Machine = @Act_Machine,
     Sync_EndDate = @Act_Sync_EndDate,
     HibaKod = @Act_HibaKod,
     HibaUzenet = @Act_HibaUzenet,
     ExternalId = @Act_ExternalId,
     Status = @Act_Status,
     Dokumentumok_Id_Sent = @Act_Dokumentumok_Id_Sent,
     Dokumentumok_Id_Error = @Act_Dokumentumok_Id_Error,
     PackageHash = @Act_PackageHash,
     PackageVer = @Act_PackageVer,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'INT_Log',@Id
					,'INT_LogHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
