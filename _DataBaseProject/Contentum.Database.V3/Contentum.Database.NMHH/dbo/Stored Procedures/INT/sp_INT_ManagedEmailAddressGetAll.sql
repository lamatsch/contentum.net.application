﻿


create procedure [dbo].[sp_INT_ManagedEmailAddressGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_ManagedEmailAddress.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	SET NOCOUNT ON
    
	--FOR EF COLUMN INFO GENERATION
	IF (1=0)
	BEGIN
		SELECT 
		   CAST(NULL AS INT) AS RowNumber,
  		   INT_ManagedEmailAddress.Id,
		   INT_ManagedEmailAddress.Org,
		   INT_ManagedEmailAddress.Felhasznalo_id,
		   INT_ManagedEmailAddress.Nev,
		   INT_ManagedEmailAddress.Ertek,
		   INT_ManagedEmailAddress.Karbantarthato,
		   INT_ManagedEmailAddress.Ver,
		   INT_ManagedEmailAddress.Note,
		   INT_ManagedEmailAddress.Stat_id,
		   INT_ManagedEmailAddress.ErvKezd,
		   INT_ManagedEmailAddress.ErvVege,
		   INT_ManagedEmailAddress.Letrehozo_id,
		   INT_ManagedEmailAddress.LetrehozasIdo,
		   INT_ManagedEmailAddress.Modosito_id,
		   INT_ManagedEmailAddress.ModositasIdo,
		   INT_ManagedEmailAddress.Zarolo_id,
		   INT_ManagedEmailAddress.ZarolasIdo,
		   INT_ManagedEmailAddress.Tranz_id,
		   INT_ManagedEmailAddress.UIAccessLog_id  
	   FROM 
		 INT_ManagedEmailAddress 
	END
	--------------------------------

	DECLARE @Org uniqueidentifier
	if @ExecutorUserId = '00000000-0000-0000-0000-000000000000'
	begin
		-- 'szuper' (közös) paraméterek
		SET @Org = '00000000-0000-0000-0000-000000000000'
	end
	else
	begin
		SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
		if (@Org is null)
		begin
			RAISERROR('[50202]',16,1)
		end
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   INT_ManagedEmailAddress.Id into #result
		from INT_ManagedEmailAddress as INT_ManagedEmailAddress
    Where INT_ManagedEmailAddress.Org=@Org'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   INT_ManagedEmailAddress.Id,
	   INT_ManagedEmailAddress.Org,
	   INT_ManagedEmailAddress.Felhasznalo_id,
	   INT_ManagedEmailAddress.Nev,
	   INT_ManagedEmailAddress.Ertek,
	   INT_ManagedEmailAddress.Karbantarthato,
	   INT_ManagedEmailAddress.Ver,
	   INT_ManagedEmailAddress.Note,
	   INT_ManagedEmailAddress.Stat_id,
	   INT_ManagedEmailAddress.ErvKezd,
	   INT_ManagedEmailAddress.ErvVege,
	   INT_ManagedEmailAddress.Letrehozo_id,
	   INT_ManagedEmailAddress.LetrehozasIdo,
	   INT_ManagedEmailAddress.Modosito_id,
	   INT_ManagedEmailAddress.ModositasIdo,
	   INT_ManagedEmailAddress.Zarolo_id,
	   INT_ManagedEmailAddress.ZarolasIdo,
	   INT_ManagedEmailAddress.Tranz_id,
	   INT_ManagedEmailAddress.UIAccessLog_id  
   from 
     INT_ManagedEmailAddress as INT_ManagedEmailAddress      
     	inner join #result on #result.Id = INT_ManagedEmailAddress.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;  
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end