﻿
create procedure sp_INT_ManagedEmailAddressHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from INT_ManagedEmailAddressHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_id' as ColumnName,               cast(Old.Felhasznalo_id as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Felhasznalo_id != New.Felhasznalo_id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ertek' as ColumnName,               cast(Old.Ertek as nvarchar(99)) as OldValue,
               cast(New.Ertek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Ertek != New.Ertek 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Karbantarthato' as ColumnName,               cast(Old.Karbantarthato as nvarchar(99)) as OldValue,
               cast(New.Karbantarthato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from INT_ManagedEmailAddressHistory Old
         inner join INT_ManagedEmailAddressHistory New on Old.Ver = (select top 1 Tmp.Ver from INT_ManagedEmailAddressHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Karbantarthato != New.Karbantarthato 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end