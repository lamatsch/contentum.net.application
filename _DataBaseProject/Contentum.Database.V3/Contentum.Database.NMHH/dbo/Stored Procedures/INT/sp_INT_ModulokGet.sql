
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ModulokGet')
            and   type = 'P')
   drop procedure sp_INT_ModulokGet
go
*/
create procedure sp_INT_ModulokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_Modulok.Id,
	   INT_Modulok.Org,
	   INT_Modulok.Nev,
	   INT_Modulok.Statusz,
	   INT_Modulok.Leiras,
	   INT_Modulok.Parancs,
	   INT_Modulok.Gyakorisag,
	   INT_Modulok.GyakorisagMertekegyseg,
	   INT_Modulok.UtolsoFutas,
	   INT_Modulok.Ver,
	   INT_Modulok.Note,
	   INT_Modulok.Stat_id,
	   INT_Modulok.ErvKezd,
	   INT_Modulok.ErvVege,
	   INT_Modulok.Letrehozo_id,
	   INT_Modulok.LetrehozasIdo,
	   INT_Modulok.Modosito_id,
	   INT_Modulok.ModositasIdo,
	   INT_Modulok.Zarolo_id,
	   INT_Modulok.ZarolasIdo,
	   INT_Modulok.Tranz_id,
	   INT_Modulok.UIAccessLog_id
	   from 
		 INT_Modulok as INT_Modulok 
	   where
		 INT_Modulok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
