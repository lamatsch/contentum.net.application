/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ModulokGetAll')
            and   type = 'P')
   drop procedure sp_INT_ModulokGetAll
go
*/
create procedure sp_INT_ModulokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_Modulok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_Modulok.Id,
	   INT_Modulok.Org,
	   INT_Modulok.Nev,
	   INT_Modulok.Statusz,
	   INT_Modulok.Leiras,
	   INT_Modulok.Parancs,
	   INT_Modulok.Gyakorisag,
	   INT_Modulok.GyakorisagMertekegyseg,
	   INT_Modulok.UtolsoFutas,
	   INT_Modulok.Ver,
	   INT_Modulok.Note,
	   INT_Modulok.Stat_id,
	   INT_Modulok.ErvKezd,
	   INT_Modulok.ErvVege,
	   INT_Modulok.Letrehozo_id,
	   INT_Modulok.LetrehozasIdo,
	   INT_Modulok.Modosito_id,
	   INT_Modulok.ModositasIdo,
	   INT_Modulok.Zarolo_id,
	   INT_Modulok.ZarolasIdo,
	   INT_Modulok.Tranz_id,
	   INT_Modulok.UIAccessLog_id  
   from 
     INT_Modulok as INT_Modulok      
    Where INT_Modulok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go