
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ParameterekGet')
            and   type = 'P')
   drop procedure sp_INT_ParameterekGet
go
*/
create procedure sp_INT_ParameterekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_Parameterek.Id,
	   INT_Parameterek.Org,
	   INT_Parameterek.Modul_id,
	   INT_Parameterek.Felhasznalo_id,
	   INT_Parameterek.Nev,
	   INT_Parameterek.Ertek,
	   INT_Parameterek.Karbantarthato,
	   INT_Parameterek.Ver,
	   INT_Parameterek.Note,
	   INT_Parameterek.Stat_id,
	   INT_Parameterek.ErvKezd,
	   INT_Parameterek.ErvVege,
	   INT_Parameterek.Letrehozo_id,
	   INT_Parameterek.LetrehozasIdo,
	   INT_Parameterek.Modosito_id,
	   INT_Parameterek.ModositasIdo,
	   INT_Parameterek.Zarolo_id,
	   INT_Parameterek.ZarolasIdo,
	   INT_Parameterek.Tranz_id,
	   INT_Parameterek.UIAccessLog_id
	   from 
		 INT_Parameterek as INT_Parameterek 
	   where
		 INT_Parameterek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
