/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ParameterekGetAll')
            and   type = 'P')
   drop procedure sp_INT_ParameterekGetAll
go
*/
create procedure sp_INT_ParameterekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_Parameterek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_Parameterek.Id,
	   INT_Parameterek.Org,
	   INT_Parameterek.Modul_id,
	   INT_Parameterek.Felhasznalo_id,
	   INT_Parameterek.Nev,
	   INT_Parameterek.Ertek,
	   INT_Parameterek.Karbantarthato,
	   INT_Parameterek.Ver,
	   INT_Parameterek.Note,
	   INT_Parameterek.Stat_id,
	   INT_Parameterek.ErvKezd,
	   INT_Parameterek.ErvVege,
	   INT_Parameterek.Letrehozo_id,
	   INT_Parameterek.LetrehozasIdo,
	   INT_Parameterek.Modosito_id,
	   INT_Parameterek.ModositasIdo,
	   INT_Parameterek.Zarolo_id,
	   INT_Parameterek.ZarolasIdo,
	   INT_Parameterek.Tranz_id,
	   INT_Parameterek.UIAccessLog_id  
   from 
     INT_Parameterek as INT_Parameterek      
    Where INT_Parameterek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go