﻿create procedure [dbo].[sp_KRT_AdatkiegeszitesekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Adatkiegeszitesek.Id,
	   KRT_Adatkiegeszitesek.ObjTip_Id,
	   KRT_Adatkiegeszitesek.Obj_Id,
	   KRT_Adatkiegeszitesek.ObjTip_Id_Kapcsolt,
	   KRT_Adatkiegeszitesek.Obj_Id_Kapcsolt,
	   KRT_Adatkiegeszitesek.ObjTip_Id_Adat,
	   KRT_Adatkiegeszitesek.Obj_Id_Adat,
	   KRT_Adatkiegeszitesek.Ver,
	   KRT_Adatkiegeszitesek.Note,
	   KRT_Adatkiegeszitesek.Stat_id,
	   KRT_Adatkiegeszitesek.ErvKezd,
	   KRT_Adatkiegeszitesek.ErvVege,
	   KRT_Adatkiegeszitesek.Letrehozo_id,
	   KRT_Adatkiegeszitesek.LetrehozasIdo,
	   KRT_Adatkiegeszitesek.Modosito_id,
	   KRT_Adatkiegeszitesek.ModositasIdo,
	   KRT_Adatkiegeszitesek.Zarolo_id,
	   KRT_Adatkiegeszitesek.ZarolasIdo,
	   KRT_Adatkiegeszitesek.Tranz_id,
	   KRT_Adatkiegeszitesek.UIAccessLog_id
	   from 
		 KRT_Adatkiegeszitesek as KRT_Adatkiegeszitesek 
	   where
		 KRT_Adatkiegeszitesek.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end