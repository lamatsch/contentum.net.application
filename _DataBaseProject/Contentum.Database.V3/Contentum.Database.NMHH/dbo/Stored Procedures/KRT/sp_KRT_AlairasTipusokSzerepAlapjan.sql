﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_AlairasTipusokSzerepAlapjan')
            and   type = 'P')
   drop procedure sp_KRT_AlairasTipusokSzerepAlapjan
go
*/
CREATE PROCEDURE [dbo].[sp_KRT_AlairasTipusokSzerepAlapjan]
  @AlairoSzerep			NVARCHAR(64),
  @AlairasSzint			INT = NULL,
  @ExecutorUserId		UNIQUEIDENTIFIER = NULL

AS

BEGIN

BEGIN TRY

   SET NOCOUNT ON
   
   SELECT ALT.* 
   FROM KRT_AlairasSzabalyok ALSZA
   LEFT JOIN KRT_AlairasTipusok ALT ON ALSZA.AlairasTipus_Id = ALT.Id
   WHERE (@AlairasSzint IS NULL OR  ALT.AlairasSzint = @AlairasSzint)
	AND ALSZA.AlairoSzerep = @AlairoSzerep

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end