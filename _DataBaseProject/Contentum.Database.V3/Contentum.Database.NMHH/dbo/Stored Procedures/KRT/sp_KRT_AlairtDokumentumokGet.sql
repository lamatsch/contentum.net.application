﻿create procedure [dbo].[sp_KRT_AlairtDokumentumokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_AlairtDokumentumok.Id,
	   KRT_AlairtDokumentumok.AlairasMod,
	   KRT_AlairtDokumentumok.AlairtFajlnev,
	   KRT_AlairtDokumentumok.AlairtTartalomHash,
	   KRT_AlairtDokumentumok.IratAlairasSzabaly_Id,
	   KRT_AlairtDokumentumok.AlairasRendben,
	   KRT_AlairtDokumentumok.KivarasiIdoVege,
	   KRT_AlairtDokumentumok.AlairasVeglegRendben,
	   KRT_AlairtDokumentumok.Idopecset,
	   KRT_AlairtDokumentumok.Csoport_Id_Alairo,
	   KRT_AlairtDokumentumok.AlairasTulajdonos,
	   KRT_AlairtDokumentumok.Tanusitvany_Id,
	   KRT_AlairtDokumentumok.External_Link,
	   KRT_AlairtDokumentumok.External_Id,
	   KRT_AlairtDokumentumok.External_Source,
	   KRT_AlairtDokumentumok.External_Info,
	   KRT_AlairtDokumentumok.Allapot,
	   KRT_AlairtDokumentumok.Ver,
	   KRT_AlairtDokumentumok.Note,
	   KRT_AlairtDokumentumok.Stat_id,
	   KRT_AlairtDokumentumok.ErvKezd,
	   KRT_AlairtDokumentumok.ErvVege,
	   KRT_AlairtDokumentumok.Letrehozo_id,
	   KRT_AlairtDokumentumok.LetrehozasIdo,
	   KRT_AlairtDokumentumok.Modosito_id,
	   KRT_AlairtDokumentumok.ModositasIdo,
	   KRT_AlairtDokumentumok.Zarolo_id,
	   KRT_AlairtDokumentumok.ZarolasIdo,
	   KRT_AlairtDokumentumok.Tranz_id,
	   KRT_AlairtDokumentumok.UIAccessLog_id
	   from 
		 KRT_AlairtDokumentumok as KRT_AlairtDokumentumok 
	   where
		 KRT_AlairtDokumentumok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end