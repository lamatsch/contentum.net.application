﻿create procedure [dbo].[sp_KRT_AlkalmazasokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Alkalmazasok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Alkalmazasok.Id,
	   KRT_Alkalmazasok.KRT_Id,
	   KRT_Alkalmazasok.Kod,
	   KRT_Alkalmazasok.Nev,
	   KRT_Alkalmazasok.Kulso,
	   KRT_Alkalmazasok.Org,
	   KRT_Alkalmazasok.Ver,
	   KRT_Alkalmazasok.Note,
	   KRT_Alkalmazasok.Stat_id,
	   KRT_Alkalmazasok.ErvKezd,
	   KRT_Alkalmazasok.ErvVege,
	   KRT_Alkalmazasok.Letrehozo_id,
	   KRT_Alkalmazasok.LetrehozasIdo,
	   KRT_Alkalmazasok.Modosito_id,
	   KRT_Alkalmazasok.ModositasIdo,
	   KRT_Alkalmazasok.Zarolo_id,
	   KRT_Alkalmazasok.ZarolasIdo,
	   KRT_Alkalmazasok.Tranz_id,
	   KRT_Alkalmazasok.UIAccessLog_id  
   from 
     KRT_Alkalmazasok as KRT_Alkalmazasok      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end