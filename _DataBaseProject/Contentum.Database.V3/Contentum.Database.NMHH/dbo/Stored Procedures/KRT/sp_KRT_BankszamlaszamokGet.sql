﻿create procedure [dbo].[sp_KRT_BankszamlaszamokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Bankszamlaszamok.Id,
	   KRT_Bankszamlaszamok.Partner_Id,
	   KRT_Bankszamlaszamok.Bankszamlaszam,
	   KRT_Bankszamlaszamok.Partner_Id_Bank,
	   KRT_Bankszamlaszamok.Ver,
	   KRT_Bankszamlaszamok.Note,
	   KRT_Bankszamlaszamok.Stat_id,
	   KRT_Bankszamlaszamok.ErvKezd,
	   KRT_Bankszamlaszamok.ErvVege,
	   KRT_Bankszamlaszamok.Letrehozo_id,
	   KRT_Bankszamlaszamok.LetrehozasIdo,
	   KRT_Bankszamlaszamok.Modosito_id,
	   KRT_Bankszamlaszamok.ModositasIdo,
	   KRT_Bankszamlaszamok.Zarolo_id,
	   KRT_Bankszamlaszamok.ZarolasIdo,
	   KRT_Bankszamlaszamok.Tranz_id,
	   KRT_Bankszamlaszamok.UIAccessLog_id
	   from 
		 KRT_Bankszamlaszamok as KRT_Bankszamlaszamok 
	   where
		 KRT_Bankszamlaszamok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end