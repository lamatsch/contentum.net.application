﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_BankszamlaszamokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_BankszamlaszamokHistoryGetRecord
go
*/
create procedure sp_KRT_BankszamlaszamokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_BankszamlaszamokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_BankszamlaszamokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_BankszamlaszamokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(New.Partner_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_BankszamlaszamokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Parameterek FTOld on FTOld.Id = Old.Partner_Id and FTOld.Ver = Old.Ver
         left join KRT_Parameterek FTNew on FTNew.Id = New.Partner_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Bankszamlaszam' as ColumnName,               
               cast(Old.Bankszamlaszam as nvarchar(99)) as OldValue,
               cast(New.Bankszamlaszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_BankszamlaszamokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Bankszamlaszam as nvarchar(max)),'') != ISNULL(CAST(New.Bankszamlaszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Bank' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(Old.Partner_Id_Bank) as OldValue,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(New.Partner_Id_Bank) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_BankszamlaszamokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_Bank as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_Bank as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Parameterek FTOld on FTOld.Id = Old.Partner_Id_Bank and FTOld.Ver = Old.Ver
         left join KRT_Parameterek FTNew on FTNew.Id = New.Partner_Id_Bank and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go