﻿create procedure [dbo].[sp_KRT_BarkodokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Barkodok.Id,
	   KRT_Barkodok.Kod,
	   KRT_Barkodok.Obj_Id,
	   KRT_Barkodok.ObjTip_Id,
	   KRT_Barkodok.Obj_type,
	   KRT_Barkodok.KodType,
	   KRT_Barkodok.Allapot,
	   KRT_Barkodok.Ver,
	   KRT_Barkodok.Note,
	   KRT_Barkodok.Stat_id,
	   KRT_Barkodok.ErvKezd,
	   KRT_Barkodok.ErvVege,
	   KRT_Barkodok.Letrehozo_id,
	   KRT_Barkodok.LetrehozasIdo,
	   KRT_Barkodok.Modosito_id,
	   KRT_Barkodok.ModositasIdo,
	   KRT_Barkodok.Zarolo_id,
	   KRT_Barkodok.ZarolasIdo,
	   KRT_Barkodok.Tranz_id,
	   KRT_Barkodok.UIAccessLog_id
	   from 
		 KRT_Barkodok as KRT_Barkodok 
	   where
		 KRT_Barkodok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end