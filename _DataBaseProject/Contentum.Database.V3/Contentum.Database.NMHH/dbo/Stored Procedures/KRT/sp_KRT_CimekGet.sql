﻿create procedure [dbo].[sp_KRT_CimekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Cimek.Id,
	   KRT_Cimek.Org,
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus,
	   KRT_Cimek.Nev,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   Coalesce (KRT_Cimek.Orszag_Id, (Select t.Orszag_Id From KRT_Telepulesek t Where t.Id = KRT_Cimek.Telepules_Id)) "Orszag_Id",
	   Coalesce (KRT_Cimek.OrszagNev, (Select o.Nev From KRT_Telepulesek t inner join KRT_Orszagok o on o.Id = t.Orszag_Id Where t.Id = KRT_Cimek.Telepules_Id)) "OrszagNev",
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi,
	   KRT_Cimek.Ver,
	   KRT_Cimek.Note,
	   KRT_Cimek.Stat_id,
	   KRT_Cimek.ErvKezd,
	   KRT_Cimek.ErvVege,
	   KRT_Cimek.Letrehozo_id,
	   KRT_Cimek.LetrehozasIdo,
	   KRT_Cimek.Modosito_id,
	   KRT_Cimek.ModositasIdo,
	   KRT_Cimek.Zarolo_id,
	   KRT_Cimek.ZarolasIdo,
	   KRT_Cimek.Tranz_id,
	   KRT_Cimek.UIAccessLog_id
	   from 
		 KRT_Cimek as KRT_Cimek 
	   where
		 KRT_Cimek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end