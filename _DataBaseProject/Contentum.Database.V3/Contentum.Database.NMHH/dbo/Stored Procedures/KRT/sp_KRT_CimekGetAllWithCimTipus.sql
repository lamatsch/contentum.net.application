﻿create procedure [dbo].[sp_KRT_CimekGetAllWithCimTipus]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Cimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
      

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Cimek.Id,
	   KRT_Cimek.Org,
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus,
       KRT_KodTarak.Nev as TipusNev,
	   KRT_Cimek.Nev,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   KRT_Cimek.Orszag_Id,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi,
	   KRT_Cimek.Ver,
	   KRT_Cimek.Note,
	   KRT_Cimek.Stat_id,
	   KRT_Cimek.ErvKezd,
	   KRT_Cimek.ErvVege,
	   KRT_Cimek.Letrehozo_id,
	   KRT_Cimek.LetrehozasIdo,
	   KRT_Cimek.Modosito_id,
	   KRT_Cimek.ModositasIdo,
	   KRT_Cimek.Zarolo_id,
	   KRT_Cimek.ZarolasIdo,
	   KRT_Cimek.Tranz_id,
	   KRT_Cimek.UIAccessLog_id  
   from 
     KRT_Cimek as KRT_Cimek
     LEFT JOIN KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CIM_TIPUS''
     LEFT JOIN KRT_KodTarak on KRT_Cimek.Tipus = KRT_KodTarak.Kod and KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''     
            Where KRT_Cimek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end