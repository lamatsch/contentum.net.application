﻿create procedure [dbo].[sp_KRT_CimekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Cimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	END

   DECLARE @sqlcmd nvarchar(MAX)
	DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
	
  /************************************************************
	* Szurési tábla összeállítása								*
	************************************************************/
		
  SET @sqlcmd = 'SELECT distinct KRT_Cimek.Id into #filter from KRT_Cimek
                 Where KRT_Cimek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
  
  if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
  end
  
  /************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
        
  SET @sqlcmd = @sqlcmd + N'
	   select 
	   row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Cimek.Id into #result
       from KRT_Cimek as KRT_Cimek
       left join KRT_Kodcsoportok as KodCsoportCimTipus on KodCsoportCimTipus.Kod = ''CIM_TIPUS''
	   left join KRT_Kodtarak as KodTarakCimTipus on KodTarakCimTipus.Kodcsoport_Id = KodCsoportCimTipus.Id and KodTarakCimTipus.Kod = KRT_Cimek.Tipus
			and KodTarakCimTipus.Org=''' + CAST(@Org as Nvarchar(40)) + '''
       where KRT_Cimek.Id in (select Id from #filter); '
       
       
  if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END'
		ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'
				
  /************************************************************
	* Tényleges select											*
	************************************************************/				     
       
  set @sqlcmd = @sqlcmd + N'
	   select 
	   #result.RowNumber,
	   #result.Id,
	   KRT_Cimek.Org,
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus,
	   dbo.fn_KodtarErtekNeve(''CIM_TIPUS'',KRT_Cimek.Tipus,''' + CAST(@Org as Nvarchar(40)) + ''') as TipusNev,
	   KRT_Cimek.Nev,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   KRT_Cimek.Orszag_Id,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi,
	   KRT_Cimek.Ver,
	   KRT_Cimek.Note,
	   KRT_Cimek.Stat_id,
	   KRT_Cimek.ErvKezd,
	   KRT_Cimek.ErvVege,
	   KRT_Cimek.Letrehozo_id,
	   KRT_Cimek.LetrehozasIdo,
	   KRT_Cimek.Modosito_id,
	   KRT_Cimek.ModositasIdo,
	   KRT_Cimek.Zarolo_id,
	   KRT_Cimek.ZarolasIdo,
	   KRT_Cimek.Tranz_id,
	   KRT_Cimek.UIAccessLog_id  
   from 
     KRT_Cimek as KRT_Cimek
     inner join #result on #result.Id = KRT_Cimek.Id
     where RowNumber between @firstRow and @lastRow
	 ORDER BY #result.RowNumber;'
   
   
   -- találatok száma és oldalszám
   set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

   execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end