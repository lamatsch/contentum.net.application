﻿create procedure [dbo].[sp_KRT_CimekInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
	            @Kategoria     char(1),
                @Tipus     nvarchar(64)  = null,
                @Nev     Nvarchar(100)  = null,
                @KulsoAzonositok     Nvarchar(100)  = null,
                @Forras     char(1)  = null,
                @Orszag_Id     uniqueidentifier  = null,
                @OrszagNev     Nvarchar(400)  = null,
                @Telepules_Id     uniqueidentifier  = null,
                @TelepulesNev     Nvarchar(100)  = null,
                @IRSZ     Nvarchar(20)  = null,
                @CimTobbi     Nvarchar(100)  = null,
                @Kozterulet_Id     uniqueidentifier  = null,
                @KozteruletNev     Nvarchar(100)  = null,
                @KozteruletTipus_Id     uniqueidentifier  = null,
                @KozteruletTipusNev     Nvarchar(100)  = null,
                @Hazszam     Nvarchar(100)  = null,
                @Hazszamig     Nvarchar(100)  = null,
                @HazszamBetujel     Nvarchar(10)  = null,
                @MindketOldal     char(1)  = null,
                @HRSZ     Nvarchar(100)  = null,
                @Lepcsohaz     Nvarchar(100)  = null,
                @Szint     Nvarchar(10)  = null,
                @Ajto     Nvarchar(20)  = null,
                @AjtoBetujel     Nvarchar(20)  = null,
                @Tobbi     Nvarchar(100)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
  
SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
if (@Org is null)
begin
	RAISERROR('[50302]',16,1)
end
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Kategoria is not null
         begin
            SET @insertColumns = @insertColumns + ',Kategoria'
            SET @insertValues = @insertValues + ',@Kategoria'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @KulsoAzonositok is not null
         begin
            SET @insertColumns = @insertColumns + ',KulsoAzonositok'
            SET @insertValues = @insertValues + ',@KulsoAzonositok'
         end 
       
         if @Forras is not null
         begin
            SET @insertColumns = @insertColumns + ',Forras'
            SET @insertValues = @insertValues + ',@Forras'
         end 
       
         if @Orszag_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Orszag_Id'
            SET @insertValues = @insertValues + ',@Orszag_Id'
         end 
       
         if @OrszagNev is not null
         begin
            SET @insertColumns = @insertColumns + ',OrszagNev'
            SET @insertValues = @insertValues + ',@OrszagNev'
         end 
       
         if @Telepules_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Telepules_Id'
            SET @insertValues = @insertValues + ',@Telepules_Id'
         end 
       
         if @TelepulesNev is not null
         begin
            SET @insertColumns = @insertColumns + ',TelepulesNev'
            SET @insertValues = @insertValues + ',@TelepulesNev'
         end 
       
         if @IRSZ is not null
         begin
            SET @insertColumns = @insertColumns + ',IRSZ'
            SET @insertValues = @insertValues + ',@IRSZ'
         end 
       
         if @CimTobbi is not null
         begin
            SET @insertColumns = @insertColumns + ',CimTobbi'
            SET @insertValues = @insertValues + ',@CimTobbi'
         end 
       
         if @Kozterulet_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kozterulet_Id'
            SET @insertValues = @insertValues + ',@Kozterulet_Id'
         end 
       
         if @KozteruletNev is not null
         begin
            SET @insertColumns = @insertColumns + ',KozteruletNev'
            SET @insertValues = @insertValues + ',@KozteruletNev'
         end 
       
         if @KozteruletTipus_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KozteruletTipus_Id'
            SET @insertValues = @insertValues + ',@KozteruletTipus_Id'
         end 
       
         if @KozteruletTipusNev is not null
         begin
            SET @insertColumns = @insertColumns + ',KozteruletTipusNev'
            SET @insertValues = @insertValues + ',@KozteruletTipusNev'
         end 
       
         if @Hazszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Hazszam'
            SET @insertValues = @insertValues + ',@Hazszam'
         end 
       
         if @Hazszamig is not null
         begin
            SET @insertColumns = @insertColumns + ',Hazszamig'
            SET @insertValues = @insertValues + ',@Hazszamig'
         end 
       
         if @HazszamBetujel is not null
         begin
            SET @insertColumns = @insertColumns + ',HazszamBetujel'
            SET @insertValues = @insertValues + ',@HazszamBetujel'
         end 
       
         if @MindketOldal is not null
         begin
            SET @insertColumns = @insertColumns + ',MindketOldal'
            SET @insertValues = @insertValues + ',@MindketOldal'
         end 
       
         if @HRSZ is not null
         begin
            SET @insertColumns = @insertColumns + ',HRSZ'
            SET @insertValues = @insertValues + ',@HRSZ'
         end 
       
         if @Lepcsohaz is not null
         begin
            SET @insertColumns = @insertColumns + ',Lepcsohaz'
            SET @insertValues = @insertValues + ',@Lepcsohaz'
         end 
       
         if @Szint is not null
         begin
            SET @insertColumns = @insertColumns + ',Szint'
            SET @insertValues = @insertValues + ',@Szint'
         end 
       
         if @Ajto is not null
         begin
            SET @insertColumns = @insertColumns + ',Ajto'
            SET @insertValues = @insertValues + ',@Ajto'
         end 
       
         if @AjtoBetujel is not null
         begin
            SET @insertColumns = @insertColumns + ',AjtoBetujel'
            SET @insertValues = @insertValues + ',@AjtoBetujel'
         end 
       
         if @Tobbi is not null
         begin
            SET @insertColumns = @insertColumns + ',Tobbi'
            SET @insertValues = @insertValues + ',@Tobbi'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Cimek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Kategoria char(1),@Tipus nvarchar(64),@Nev Nvarchar(100),@KulsoAzonositok Nvarchar(100),@Forras char(1),@Orszag_Id uniqueidentifier,@OrszagNev Nvarchar(400),@Telepules_Id uniqueidentifier,@TelepulesNev Nvarchar(100),@IRSZ Nvarchar(20),@CimTobbi Nvarchar(100),@Kozterulet_Id uniqueidentifier,@KozteruletNev Nvarchar(100),@KozteruletTipus_Id uniqueidentifier,@KozteruletTipusNev Nvarchar(100),@Hazszam Nvarchar(100),@Hazszamig Nvarchar(100),@HazszamBetujel Nvarchar(10),@MindketOldal char(1),@HRSZ Nvarchar(100),@Lepcsohaz Nvarchar(100),@Szint Nvarchar(10),@Ajto Nvarchar(20),@AjtoBetujel Nvarchar(20),@Tobbi Nvarchar(100),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Kategoria = @Kategoria,@Tipus = @Tipus,@Nev = @Nev,@KulsoAzonositok = @KulsoAzonositok,@Forras = @Forras,@Orszag_Id = @Orszag_Id,@OrszagNev = @OrszagNev,@Telepules_Id = @Telepules_Id,@TelepulesNev = @TelepulesNev,@IRSZ = @IRSZ,@CimTobbi = @CimTobbi,@Kozterulet_Id = @Kozterulet_Id,@KozteruletNev = @KozteruletNev,@KozteruletTipus_Id = @KozteruletTipus_Id,@KozteruletTipusNev = @KozteruletTipusNev,@Hazszam = @Hazszam,@Hazszamig = @Hazszamig,@HazszamBetujel = @HazszamBetujel,@MindketOldal = @MindketOldal,@HRSZ = @HRSZ,@Lepcsohaz = @Lepcsohaz,@Szint = @Szint,@Ajto = @Ajto,@AjtoBetujel = @AjtoBetujel,@Tobbi = @Tobbi,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Cimek',@ResultUid
					,'KRT_CimekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH