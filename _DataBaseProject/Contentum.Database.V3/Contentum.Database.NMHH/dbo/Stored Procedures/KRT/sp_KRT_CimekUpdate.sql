﻿create procedure [dbo].[sp_KRT_CimekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Kategoria     char(1)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Nev     Nvarchar(100)  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @Forras     char(1)  = null,         
             @Orszag_Id     uniqueidentifier  = null,         
             @OrszagNev     Nvarchar(400)  = null,         
             @Telepules_Id     uniqueidentifier  = null,         
             @TelepulesNev     Nvarchar(100)  = null,         
             @IRSZ     Nvarchar(20)  = null,         
             @CimTobbi     Nvarchar(100)  = null,         
             @Kozterulet_Id     uniqueidentifier  = null,         
             @KozteruletNev     Nvarchar(100)  = null,         
             @KozteruletTipus_Id     uniqueidentifier  = null,         
             @KozteruletTipusNev     Nvarchar(100)  = null,         
             @Hazszam     Nvarchar(100)  = null,         
             @Hazszamig     Nvarchar(100)  = null,         
             @HazszamBetujel     Nvarchar(10)  = null,         
             @MindketOldal     char(1)  = null,         
             @HRSZ     Nvarchar(100)  = null,         
             @Lepcsohaz     Nvarchar(100)  = null,         
             @Szint     Nvarchar(10)  = null,         
             @Ajto     Nvarchar(20)  = null,         
             @AjtoBetujel     Nvarchar(20)  = null,         
             @Tobbi     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Kategoria     char(1)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_KulsoAzonositok     Nvarchar(100)         
     DECLARE @Act_Forras     char(1)         
     DECLARE @Act_Orszag_Id     uniqueidentifier         
     DECLARE @Act_OrszagNev     Nvarchar(400)         
     DECLARE @Act_Telepules_Id     uniqueidentifier         
     DECLARE @Act_TelepulesNev     Nvarchar(100)         
     DECLARE @Act_IRSZ     Nvarchar(20)         
     DECLARE @Act_CimTobbi     Nvarchar(100)         
     DECLARE @Act_Kozterulet_Id     uniqueidentifier         
     DECLARE @Act_KozteruletNev     Nvarchar(100)         
     DECLARE @Act_KozteruletTipus_Id     uniqueidentifier         
     DECLARE @Act_KozteruletTipusNev     Nvarchar(100)         
     DECLARE @Act_Hazszam     Nvarchar(100)         
     DECLARE @Act_Hazszamig     Nvarchar(100)         
     DECLARE @Act_HazszamBetujel     Nvarchar(10)         
     DECLARE @Act_MindketOldal     char(1)         
     DECLARE @Act_HRSZ     Nvarchar(100)         
     DECLARE @Act_Lepcsohaz     Nvarchar(100)         
     DECLARE @Act_Szint     Nvarchar(10)         
     DECLARE @Act_Ajto     Nvarchar(20)         
     DECLARE @Act_AjtoBetujel     Nvarchar(20)         
     DECLARE @Act_Tobbi     Nvarchar(100)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Kategoria = Kategoria,
     @Act_Tipus = Tipus,
     @Act_Nev = Nev,
     @Act_KulsoAzonositok = KulsoAzonositok,
     @Act_Forras = Forras,
     @Act_Orszag_Id = Orszag_Id,
     @Act_OrszagNev = OrszagNev,
     @Act_Telepules_Id = Telepules_Id,
     @Act_TelepulesNev = TelepulesNev,
     @Act_IRSZ = IRSZ,
     @Act_CimTobbi = CimTobbi,
     @Act_Kozterulet_Id = Kozterulet_Id,
     @Act_KozteruletNev = KozteruletNev,
     @Act_KozteruletTipus_Id = KozteruletTipus_Id,
     @Act_KozteruletTipusNev = KozteruletTipusNev,
     @Act_Hazszam = Hazszam,
     @Act_Hazszamig = Hazszamig,
     @Act_HazszamBetujel = HazszamBetujel,
     @Act_MindketOldal = MindketOldal,
     @Act_HRSZ = HRSZ,
     @Act_Lepcsohaz = Lepcsohaz,
     @Act_Szint = Szint,
     @Act_Ajto = Ajto,
     @Act_AjtoBetujel = AjtoBetujel,
     @Act_Tobbi = Tobbi,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Cimek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Kategoria')=1
         SET @Act_Kategoria = @Kategoria
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/KulsoAzonositok')=1
         SET @Act_KulsoAzonositok = @KulsoAzonositok
   IF @UpdatedColumns.exist('/root/Forras')=1
         SET @Act_Forras = @Forras
   IF @UpdatedColumns.exist('/root/Orszag_Id')=1
         SET @Act_Orszag_Id = @Orszag_Id
   IF @UpdatedColumns.exist('/root/OrszagNev')=1
         SET @Act_OrszagNev = @OrszagNev
   IF @UpdatedColumns.exist('/root/Telepules_Id')=1
         SET @Act_Telepules_Id = @Telepules_Id
   IF @UpdatedColumns.exist('/root/TelepulesNev')=1
         SET @Act_TelepulesNev = @TelepulesNev
   IF @UpdatedColumns.exist('/root/IRSZ')=1
         SET @Act_IRSZ = @IRSZ
   IF @UpdatedColumns.exist('/root/CimTobbi')=1
         SET @Act_CimTobbi = @CimTobbi
   IF @UpdatedColumns.exist('/root/Kozterulet_Id')=1
         SET @Act_Kozterulet_Id = @Kozterulet_Id
   IF @UpdatedColumns.exist('/root/KozteruletNev')=1
         SET @Act_KozteruletNev = @KozteruletNev
   IF @UpdatedColumns.exist('/root/KozteruletTipus_Id')=1
         SET @Act_KozteruletTipus_Id = @KozteruletTipus_Id
   IF @UpdatedColumns.exist('/root/KozteruletTipusNev')=1
         SET @Act_KozteruletTipusNev = @KozteruletTipusNev
   IF @UpdatedColumns.exist('/root/Hazszam')=1
         SET @Act_Hazszam = @Hazszam
   IF @UpdatedColumns.exist('/root/Hazszamig')=1
         SET @Act_Hazszamig = @Hazszamig
   IF @UpdatedColumns.exist('/root/HazszamBetujel')=1
         SET @Act_HazszamBetujel = @HazszamBetujel
   IF @UpdatedColumns.exist('/root/MindketOldal')=1
         SET @Act_MindketOldal = @MindketOldal
   IF @UpdatedColumns.exist('/root/HRSZ')=1
         SET @Act_HRSZ = @HRSZ
   IF @UpdatedColumns.exist('/root/Lepcsohaz')=1
         SET @Act_Lepcsohaz = @Lepcsohaz
   IF @UpdatedColumns.exist('/root/Szint')=1
         SET @Act_Szint = @Szint
   IF @UpdatedColumns.exist('/root/Ajto')=1
         SET @Act_Ajto = @Ajto
   IF @UpdatedColumns.exist('/root/AjtoBetujel')=1
         SET @Act_AjtoBetujel = @AjtoBetujel
   IF @UpdatedColumns.exist('/root/Tobbi')=1
         SET @Act_Tobbi = @Tobbi
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Cimek
SET
     Org = @Act_Org,
     Kategoria = @Act_Kategoria,
     Tipus = @Act_Tipus,
     Nev = @Act_Nev,
     KulsoAzonositok = @Act_KulsoAzonositok,
     Forras = @Act_Forras,
     Orszag_Id = @Act_Orszag_Id,
     OrszagNev = @Act_OrszagNev,
     Telepules_Id = @Act_Telepules_Id,
     TelepulesNev = @Act_TelepulesNev,
     IRSZ = @Act_IRSZ,
     CimTobbi = @Act_CimTobbi,
     Kozterulet_Id = @Act_Kozterulet_Id,
     KozteruletNev = @Act_KozteruletNev,
     KozteruletTipus_Id = @Act_KozteruletTipus_Id,
     KozteruletTipusNev = @Act_KozteruletTipusNev,
     Hazszam = @Act_Hazszam,
     Hazszamig = @Act_Hazszamig,
     HazszamBetujel = @Act_HazszamBetujel,
     MindketOldal = @Act_MindketOldal,
     HRSZ = @Act_HRSZ,
     Lepcsohaz = @Act_Lepcsohaz,
     Szint = @Act_Szint,
     Ajto = @Act_Ajto,
     AjtoBetujel = @Act_AjtoBetujel,
     Tobbi = @Act_Tobbi,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Cimek',@Id
					,'KRT_CimekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH