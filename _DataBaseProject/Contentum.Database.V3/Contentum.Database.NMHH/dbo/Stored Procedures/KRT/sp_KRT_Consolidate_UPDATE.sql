CREATE procedure [dbo].[sp_KRT_Consolidate_Update]
        @RecordId							uniqueidentifier,	
		@ConsolidatedRecordId				uniqueidentifier,
		@ExecutorUserId						uniqueidentifier
as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction
set nocount on

DECLARE @row_ids varchar(MAX)
DECLARE @ExecutionTime datetime 
SET @ExecutionTime = GETDATE()

DECLARE @lds TABLE(Id uniqueidentifier)

-----------------------EREC_eBeadvanyok-----------------------
DELETE @lds

INSERT INTO @lds
SELECT Id 
FROM EREC_eBeadvanyok
WHERE Partner_Id = @ConsolidatedRecordId

UPDATE EREC_eBeadvanyok
SET
     Partner_Id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_eBeadvanyok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime
end
-----------------------EREC_eBeadvanyok-----------------------
-----------------------****************-----------------------


-----------------------EREC_eMailBoritekCimei-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_eMailBoritekCimei
WHERE Partner_Id = @ConsolidatedRecordId

UPDATE EREC_eMailBoritekCimei
SET
     Partner_Id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
    set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_eMailBoritekCimei'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime
end

-----------------------EREC_eMailBoritekCimei-----------------------
-----------------------**********************-----------------------

-----------------------EREC_IraElosztoivTetelek-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_IraElosztoivTetelek
WHERE Partner_Id = @ConsolidatedRecordId

UPDATE EREC_IraElosztoivTetelek
SET
     Partner_Id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
    set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_IraElosztoivTetelek'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end

-----------------------EREC_IraElosztoivTetelek-----------------------
-----------------------************************-----------------------

-----------------------EREC_IraIratok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_IraIratok
WHERE Partner_Id_VisszafizetesCimzet = @ConsolidatedRecordId

UPDATE EREC_IraIratok
SET
     Partner_Id_VisszafizetesCimzet = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_VisszafizetesCimzet = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
     set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_IraIratok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime     
end

-----------------------EREC_IraIratok-----------------------
-----------------------**************-----------------------

-----------------------EREC_IraJegyzekek-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_IraJegyzekek
WHERE Partner_Id_LeveltariAtvevo = @ConsolidatedRecordId

UPDATE EREC_IraJegyzekek
SET
     Partner_Id_LeveltariAtvevo = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_LeveltariAtvevo = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
     set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_IraJegyzekek'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime 
end

-----------------------EREC_IraJegyzekek-----------------------
-----------------------*****************-----------------------

-----------------------EREC_KuldBekuldok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_KuldBekuldok
WHERE Partner_Id_Bekuldo = @ConsolidatedRecordId

UPDATE EREC_KuldBekuldok
SET
     Partner_Id_Bekuldo = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Bekuldo = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
    set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_KuldBekuldok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime 
end

---------------------EREC_KuldBekuldok-----------------------
---------------------*****************-----------------------

---------------------EREC_KuldKuldemenyek-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_KuldKuldemenyek
WHERE Partner_Id_Bekuldo = @ConsolidatedRecordId

UPDATE EREC_KuldKuldemenyek
SET
     Partner_Id_Bekuldo = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Bekuldo = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
    set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_KuldKuldemenyek'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end

DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_KuldKuldemenyek
WHERE Partner_Id_BekuldoKapcsolt = @ConsolidatedRecordId

UPDATE EREC_KuldKuldemenyek
SET
     Partner_Id_BekuldoKapcsolt = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_BekuldoKapcsolt = @ConsolidatedRecordId


if @@rowcount != 0
begin   /* History Log */
    set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_KuldKuldemenyek'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end


---------------------EREC_KuldKuldemenyek-----------------------
---------------------********************-----------------------

---------------------EREC_PldIratPeldanyok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_PldIratPeldanyok
WHERE Partner_Id_Cimzett = @ConsolidatedRecordId

UPDATE EREC_PldIratPeldanyok
SET
     Partner_Id_Cimzett = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Cimzett = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
   set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_PldIratPeldanyok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end

-----------------------EREC_PldIratPeldanyok-----------------------
-----------------------*********************-----------------------

-----------------------EREC_Szamlak-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_Szamlak
WHERE Partner_Id_Szallito = @ConsolidatedRecordId


UPDATE EREC_Szamlak
SET
     Partner_Id_Szallito = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Szallito = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_Szamlak'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime     
end

-----------------------EREC_Szamlak-----------------------
-----------------------************-----------------------

-----------------------EREC_UgyUgyiratok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM EREC_UgyUgyiratok
WHERE Partner_Id_Ugyindito = @ConsolidatedRecordId

UPDATE EREC_UgyUgyiratok
SET
     Partner_Id_Ugyindito = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Ugyindito = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_UgyUgyiratok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime    
end

-----------------------EREC_UgyUgyiratok-----------------------
-----------------------*****************-----------------------

-----------------------KRT_Bankszamlaszamok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM KRT_Bankszamlaszamok
WHERE Partner_Id = @ConsolidatedRecordId

UPDATE KRT_Bankszamlaszamok
SET
     Partner_Id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'KRT_Bankszamlaszamok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime    
end

-----------------------KRT_Bankszamlaszamok-----------------------
-----------------------********************-----------------------

-----------------------KRT_Bankszamlaszamok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM KRT_Bankszamlaszamok
WHERE Partner_Id_Bank = @ConsolidatedRecordId

UPDATE KRT_Bankszamlaszamok
SET
     Partner_Id_Bank = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_Id_Bank = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
   set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'KRT_Bankszamlaszamok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime       
end

-----------------------KRT_Bankszamlaszamok-----------------------
-----------------------********************-----------------------

-----------------------KRT_Partner_Dokumentumok-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM KRT_Partner_Dokumentumok
WHERE Partner_id = @ConsolidatedRecordId

UPDATE KRT_Partner_Dokumentumok
SET
     Partner_id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
     set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'KRT_Partner_Dokumentumok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end

-----------------------KRT_Partner_Dokumentumok-----------------------
-----------------------************************-----------------------


-----------------------KRT_PartnerMinositesek-----------------------
DELETE @lds
INSERT INTO @lds
SELECT Id 
FROM KRT_PartnerMinositesek
WHERE Partner_id = @ConsolidatedRecordId

UPDATE KRT_PartnerMinositesek
SET
     Partner_id = @RecordId,
	 ModositasIdo = GETDATE(),
	 Modosito_id = @ExecutorUserId
WHERE
	 Partner_id = @ConsolidatedRecordId

if @@rowcount != 0
begin   /* History Log */
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @lds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
	
	set @row_ids = @row_ids + '''';

    exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'KRT_PartnerMinositesek'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @ExecutionTime   
end

-----------------------KRT_PartnerMinositesek-----------------------
-----------------------**********************-----------------------

--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

