﻿create procedure [dbo].[sp_KRT_CsoportTagokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_CsoportTagok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end  

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_CsoportTagok.Id,
	   KRT_CsoportTagok.Tipus,
       KRT_KodTarak.Nev as TipusNev,
	   KRT_CsoportTagok.Csoport_Id,
       KRT_Csoportok.Nev as Csoport_Nev,
       KRT_Csoportok_Jogalany.Nev as Csoport_Jogalany_Nev,
	   KRT_CsoportTagok.Csoport_Id_Jogalany,
	   KRT_CsoportTagok.ObjTip_Id_Jogalany,
	   KRT_CsoportTagok.ErtesitesMailCim,
	   KRT_CsoportTagok.ErtesitesKell,
	   KRT_CsoportTagok.System,
	   KRT_CsoportTagok.Orokolheto,
	   KRT_CsoportTagok.Ver,
	   KRT_CsoportTagok.Note,
	   KRT_CsoportTagok.Stat_id,
	   KRT_CsoportTagok.ErvKezd,
	   KRT_CsoportTagok.ErvVege,
	   KRT_CsoportTagok.Letrehozo_id,
	   KRT_CsoportTagok.LetrehozasIdo,
	   KRT_CsoportTagok.Modosito_id,
	   KRT_CsoportTagok.ModositasIdo,
	   KRT_CsoportTagok.Zarolo_id,
	   KRT_CsoportTagok.ZarolasIdo,
	   KRT_CsoportTagok.Tranz_id,
	   KRT_CsoportTagok.UIAccessLog_id  
   from 
     KRT_CsoportTagok as KRT_CsoportTagok
     left join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
     left join KRT_Csoportok as KRT_Csoportok_Jogalany on KRT_Csoportok_Jogalany.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
     left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CSOPORTTAGSAG_TIPUS''
         left join KRT_KodTarak on KRT_CsoportTagok.Tipus = KRT_KodTarak.Kod and KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id
			and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
where
KRT_Csoportok.Org=''' + cast(@Org as NVarChar(40)) + '''
and KRT_Csoportok_Jogalany.Org=''' + cast(@Org as NVarChar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end