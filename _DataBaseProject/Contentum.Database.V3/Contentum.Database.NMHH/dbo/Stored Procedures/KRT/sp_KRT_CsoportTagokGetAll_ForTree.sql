﻿create procedure [dbo].[sp_KRT_CsoportTagokGetAll_ForTree]
  @SelectedId				uniqueidentifier = null
	, @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

	set nocount ON

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	IF @SelectedId IS NULL
	BEGIN
		SELECT * FROM
		(
			SELECT	Csoport_Id,
					Csoport.Nev AS Csoport_Nev,
					Csoport.Tipus AS Csoport_Tipus,
					Csoport_Id_Jogalany,
					Csoport_Jogalany.Nev AS Csoport_Jogalany_Nev,
					Csoport_Jogalany.Tipus AS Csoport_Jogalany_Tipus,
					KRT_CsoportTagok.Tipus
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok AS Csoport
						ON KRT_CsoportTagok.Csoport_Id = Csoport.Id
						AND GETDATE() BETWEEN Csoport.ErvKezd AND Csoport.ErvVege
					INNER JOIN KRT_Csoportok AS Csoport_Jogalany
						ON KRT_CsoportTagok.Csoport_Id_Jogalany = Csoport_Jogalany.Id
						AND GETDATE() BETWEEN Csoport_Jogalany.ErvKezd AND Csoport_Jogalany.ErvVege
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND Csoport.Tipus IN ('0','2','4','6') and Csoport.Org=@Org
					AND Csoport_Jogalany.Tipus IN ('0','2','4','6') and Csoport_Jogalany.Org=@Org
			UNION ALL
			SELECT	NULL,
					NULL,
					NULL,
					Id AS Csoport_Id_Jogalany,
					Nev AS Csoport_Jogalany_Nev,
					Tipus AS Csoport_Jogalany_Tipus,
					NULL
				FROM KRT_Csoportok
				WHERE Id NOT IN
					(
						SELECT Csoport_Id_Jogalany FROM KRT_CsoportTagok
							WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					)
					AND Tipus IN ('0','2','4','6') and KRT_Csoportok.Org=@Org
					AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
		) AS res
		ORDER BY Csoport_Jogalany_Nev
	END
	ELSE
	BEGIN
		WITH jogalanyok AS
		(
			SELECT CAST(NULL AS UNIQUEIDENTIFIER) AS Id, CAST(@SelectedId AS UNIQUEIDENTIFIER) AS jogalanyId
			
			UNION ALL
			
			SELECT KRT_CsoportTagok.Csoport_Id AS Id, KRT_CsoportTagok.Csoport_Id_Jogalany AS jogalanyId
				FROM KRT_CsoportTagok
					INNER JOIN jogalanyok ON jogalanyok.jogalanyId = KRT_CsoportTagok.Csoport_Id
					INNER JOIN KRT_Csoportok
						ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
						AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND KRT_Csoportok.Tipus IN ('0','2','4','6','R') and KRT_Csoportok.Org=@Org
			
			UNION ALL
			
			SELECT KRT_CsoportTagok.Csoport_Id AS Id, KRT_CsoportTagok.Csoport_Id_Jogalany AS jogalanyId
				FROM KRT_CsoportTagok
					INNER JOIN jogalanyok ON jogalanyok.jogalanyId = KRT_CsoportTagok.Csoport_Id
					INNER JOIN KRT_Csoportok
						ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
						AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND KRT_Csoportok.Tipus = '1' AND jogalanyok.jogalanyId = @SelectedId
					and KRT_Csoportok.Org=@Org
		) SELECT Id,jogalanyId INTO #jogalany_Ids FROM jogalanyok WHERE Id IS NOT NULL;

		WITH felettesek AS
		(
			SELECT CAST(@SelectedId AS UNIQUEIDENTIFIER) AS Id, CAST(NULL AS UNIQUEIDENTIFIER) AS jogalanyId
			
			UNION ALL
			
			SELECT KRT_CsoportTagok.Csoport_Id AS Id, KRT_CsoportTagok.Csoport_Id_Jogalany AS jogalanyId
				FROM KRT_CsoportTagok
					INNER JOIN felettesek ON felettesek.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
					INNER JOIN KRT_Csoportok
						ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
						AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND KRT_Csoportok.Tipus IN ('0','2','4','6','R') and KRT_Csoportok.Org=@Org
		) SELECT Id, jogalanyId INTO #felettes_Ids FROM felettesek WHERE jogalanyId IS NOT NULL;
		
		IF NOT EXISTS (SELECT 1 FROM #jogalany_Ids UNION SELECT 1 FROM #felettes_Ids)
		BEGIN
			SELECT	NULL AS Csoport_Id,
					NULL AS Csoport_Nev,
					NULL AS Csoport_Tipus,
					Id AS Csoport_Id_Jogalany,
					Nev AS Csoport_Jogalany_Nev,
					Tipus AS Csoport_Jogalany_Tipus,
					NULL AS Tipus
				FROM KRT_Csoportok
				WHERE Id = @SelectedId
			
			RETURN;
		END
		
		SELECT * FROM
		(
			SELECT	Csoport_Id,
					Csoport.Nev AS Csoport_Nev,
					Csoport.Tipus AS Csoport_Tipus,
					Csoport_Id_Jogalany,
					Csoport_Jogalany.Nev AS Csoport_Jogalany_Nev,
					Csoport_Jogalany.Tipus AS Csoport_Jogalany_Tipus,
					KRT_CsoportTagok.Tipus
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok AS Csoport ON KRT_CsoportTagok.Csoport_Id = Csoport.Id
					INNER JOIN KRT_Csoportok AS Csoport_Jogalany ON KRT_CsoportTagok.Csoport_Id_Jogalany = Csoport_Jogalany.Id
					INNER JOIN (SELECT * FROM #jogalany_Ids UNION SELECT * FROM #felettes_Ids) AS hierarch ON hierarch.Id = KRT_CsoportTagok.Csoport_Id
						AND hierarch.jogalanyId = KRT_CsoportTagok.Csoport_Id_Jogalany
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				and Csoport.Org=@Org
				and Csoport_Jogalany.Org=@Org
			UNION ALL
			SELECT	NULL,
					NULL,
					NULL,
					Id AS Csoport_Id_Jogalany,
					Nev AS Csoport_Jogalany_Nev,
					Tipus AS Csoport_Jogalany_Tipus,
					NULL
				FROM KRT_Csoportok
				WHERE Id NOT IN
					(
						SELECT Csoport_Id_Jogalany FROM KRT_CsoportTagok
							WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					)
					AND Id IN
					(
						SELECT Id FROM #jogalany_Ids
						UNION
						SELECT Id FROM #felettes_Ids
					)
					AND Tipus IN ('0','2','4','6','R') and KRT_Csoportok.Org=@Org
		) AS res
		ORDER BY Csoport_Jogalany_Nev
	END
	

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end