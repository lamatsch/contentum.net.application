﻿create procedure [dbo].[sp_KRT_CsoportTagokGetSzervezet]
	@ExecutorUserId      uniqueidentifier    
AS
BEGIN TRY
	select * from KRT_CsoportTagok
		inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
	where KRT_CsoportTagok.Csoport_Id_Jogalany = @ExecutorUserId AND KRT_CsoportTagok.Tipus is not null AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	ORDER BY [KRT_CsoportTagok].[Tipus] DESC, KRT_CsoportTagok.Id ASC
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH