﻿create procedure [dbo].[sp_KRT_CsoportTagokInsert]
                @Id      uniqueidentifier = null,    
                @Tipus     nvarchar(64)  = null,
                @Csoport_Id     uniqueidentifier  = null,
                @Csoport_Id_Jogalany     uniqueidentifier  = null,
                @ObjTip_Id_Jogalany     uniqueidentifier  = null,
                @ErtesitesMailCim     Nvarchar(100)  = null,
                @ErtesitesKell     char(1)  = null,
                @System     char(1)  = null,
                @Orokolheto     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
if @Csoport_Id = @Csoport_Id_Jogalany return;

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Csoport_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id'
            SET @insertValues = @insertValues + ',@Csoport_Id'
         end 
       
         if @Csoport_Id_Jogalany is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Jogalany'
            SET @insertValues = @insertValues + ',@Csoport_Id_Jogalany'
         end 
       
         if @ObjTip_Id_Jogalany is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id_Jogalany'
            SET @insertValues = @insertValues + ',@ObjTip_Id_Jogalany'
         end 
       
         if @ErtesitesMailCim is not null
         begin
            SET @insertColumns = @insertColumns + ',ErtesitesMailCim'
            SET @insertValues = @insertValues + ',@ErtesitesMailCim'
         end 
       
         if @ErtesitesKell is not null
         begin
            SET @insertColumns = @insertColumns + ',ErtesitesKell'
            SET @insertValues = @insertValues + ',@ErtesitesKell'
         end 
       
         if @System is not null
         begin
            SET @insertColumns = @insertColumns + ',System'
            SET @insertValues = @insertValues + ',@System'
         end 
       
         if @Orokolheto is not null
         begin
            SET @insertColumns = @insertColumns + ',Orokolheto'
            SET @insertValues = @insertValues + ',@Orokolheto'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
   
/* kor ellenL‘rzA©se */
	
		;WITH temp AS
		(
			SELECT @Csoport_Id_Jogalany AS Id
			UNION ALL
			SELECT KRT_Csoporttagok.Csoport_Id_Jogalany
				FROM KRT_Csoporttagok
					INNER JOIN temp ON temp.Id = KRT_Csoporttagok.Csoport_Id
				WHERE GETDATE() BETWEEN ErvKezd AND ErvVege
					AND Tipus IS NOT NULL
		) SELECT Id INTO #kor FROM temp WHERE Id = @Csoport_Id
		
		IF EXISTS (SELECT 1 FROM #kor)
			RAISERROR('[50303]',16,1)
			
		DROP TABLE #kor
	
/* TA­pus ellenL‘rzA©s */
if	( (@Tipus = '2' or @Tipus = '3') and not exists (select 1 from KRT_Csoportok where Id = @Csoport_Id_Jogalany and Tipus = '1'))
	OR
	-- CR#820: bizottsA?g, hivatlavezetA©s, projekt
	(@Tipus = '1' and not exists (select 1 from KRT_Csoportok where Id = @Csoport_Id_Jogalany and Tipus in ('0','4','5','6')))
	RAISERROR('[50305]',16,1)
if @Tipus = '3' and exists (select 1 from KRT_Csoporttagok where Csoport_Id = @Csoport_Id AND Tipus = '3' and getdate() between ErvKezd and ErvVege)
	RAISERROR('[50304]',16,1)


DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_CsoportTagok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Tipus nvarchar(64),@Csoport_Id uniqueidentifier,@Csoport_Id_Jogalany uniqueidentifier,@ObjTip_Id_Jogalany uniqueidentifier,@ErtesitesMailCim Nvarchar(100),@ErtesitesKell char(1),@System char(1),@Orokolheto char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Tipus = @Tipus,@Csoport_Id = @Csoport_Id,@Csoport_Id_Jogalany = @Csoport_Id_Jogalany,@ObjTip_Id_Jogalany = @ObjTip_Id_Jogalany,@ErtesitesMailCim = @ErtesitesMailCim,@ErtesitesKell = @ErtesitesKell,@System = @System,@Orokolheto = @Orokolheto,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT

If @@error = 0
BEGIN
			
      /* History Log */
   exec sp_LogRecordToHistory 'KRT_CsoportTagok',@ResultUid
					,'KRT_CsoportTagokHistory',0,@Letrehozo_id,@LetrehozasIdo   
END
ELSE
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH