﻿

CREATE procedure [dbo].[sp_KRT_CsoportokGetAllByIktatokonyvIktathat]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @IraIktatokonyv_Id    uniqueidentifier = null

AS
BEGIN

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	set @sqlcmd = N'
		;WITH csopHierarch AS
		(
			SELECT Csoport_Id_Jogalany AS Id
				FROM KRT_Jogosultak
				WHERE Obj_Id = ''' + cast(@IraIktatokonyv_Id as nvarchar(40)) + '''
			
			UNION ALL
			
			SELECT Csoport_Id_Iktathat 
			FROM [dbo].[EREC_Irat_Iktatokonyvei]
			WHERE [IraIktatokonyv_Id] = ''' + cast(@IraIktatokonyv_Id as nvarchar(40)) + '''
			AND GETDATE() BETWEEN [EREC_Irat_Iktatokonyvei].ErvKezd AND [EREC_Irat_Iktatokonyvei].ErvVege

			UNION ALL

			SELECT Csoport_Id_Jogalany 
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
					INNER JOIN csopHierarch ON csopHierarch.Id = KRT_CsoportTagok.Csoport_Id
				WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND KRT_CsoportTagok.Tipus IS NOT NULL
			
			)';
	
	
	set @sqlcmd = @sqlcmd + '
		SELECT DISTINCT ' + @LocalTopRow + '
			KRT_Csoportok.Id, KRT_Csoportok.Nev
			FROM KRT_Csoportok
				INNER JOIN csopHierarch ON csopHierarch.Id = KRT_Csoportok.Id';

    set @sqlcmd = @sqlcmd + ' WHERE KRT_Csoportok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

				
	SET @sqlcmd = @sqlcmd + @OrderBy
	
	PRINT @sqlcmd;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

END