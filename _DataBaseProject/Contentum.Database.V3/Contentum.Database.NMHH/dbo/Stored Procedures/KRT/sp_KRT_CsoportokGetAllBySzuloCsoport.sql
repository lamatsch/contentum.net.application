﻿create procedure [dbo].[sp_KRT_CsoportokGetAllBySzuloCsoport]
  @SzuloCsoportId uniqueidentifier,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_CsoportTagok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_CsoportTagok.Id,
	   KRT_CsoportTagok.Tipus,
			KRT_KodTarakCsTagTipus.Nev as CsoportTag_Tipus,
	   KRT_CsoportTagok.Csoport_Id,
	   KRT_CsoportTagok.Csoport_Id_Jogalany,
	   KRT_CsoportTagok.System,
	   KRT_CsoportTagok.ObjTip_Id_Jogalany,
	   KRT_Csoportok.Nev as Csoport_Nev,
	   KRT_Csoportok.Kod as Csoport_Kod,
	   KRT_KodTarak.Nev as Csoport_Tipus,
	   KRT_CsoportTagok.Ver,
	   KRT_CsoportTagok.Note,
	   KRT_CsoportTagok.Stat_id,
	   KRT_CsoportTagok.ErvKezd,
	   KRT_CsoportTagok.ErvVege,
	   KRT_CsoportTagok.Letrehozo_id,
	   KRT_CsoportTagok.LetrehozasIdo,
	   KRT_CsoportTagok.Modosito_id,
	   KRT_CsoportTagok.ModositasIdo,
	   KRT_CsoportTagok.Zarolo_id,
	   KRT_CsoportTagok.ZarolasIdo,
	   KRT_CsoportTagok.Tranz_id,
	   KRT_CsoportTagok.UIAccessLog_id  
   from 
     KRT_CsoportTagok as KRT_CsoportTagok 
	join KRT_Csoportok as KRT_Csoportok 
		on KRT_CsoportTagok.Csoport_Id_Jogalany = KRT_Csoportok.Id 
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CSOPORTTIPUS''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_Csoportok.Tipus = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
	left join KRT_KodCsoportok KRT_KodCsoportokCsTagTipus on KRT_KodCsoportokCsTagTipus.Kod=''CSOPORTTAGSAG_TIPUS''
	left join KRT_KodTarak KRT_KodTarakCsTagTipus on KRT_KodCsoportokCsTagTipus.Id = KRT_KodTarakCsTagTipus.KodCsoport_Id and KRT_CsoportTagok.Tipus = KRT_KodTarakCsTagTipus.Kod
		 and KRT_KodTarakCsTagTipus.Org=''' + cast(@Org as NVarChar(40)) + '''
  
   Where KRT_CsoportTagok.Csoport_Id = ''' + CAST(@SzuloCsoportId as Nvarchar(40)) + '''
	'
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end