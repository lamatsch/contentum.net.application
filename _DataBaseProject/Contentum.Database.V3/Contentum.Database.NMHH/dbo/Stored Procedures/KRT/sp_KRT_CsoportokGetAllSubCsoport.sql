﻿create procedure [dbo].[sp_KRT_CsoportokGetAllSubCsoport] 
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' KRT_Csoportok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null,
  @kozvetlenulHozzarendelt	tinyint,
  @szervezetId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  if @kozvetlenulHozzarendelt != 1
	  SET @sqlcmd = '
		;WITH temp AS
		(
			SELECT CAST(''' + CAST(@szervezetId AS NVARCHAR(36)) + ''' AS uniqueidentifier) AS Id
			
			UNION ALL
			
			SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
				FROM KRT_CsoportTagok
					INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
				WHERE KRT_CsoportTagok.Tipus IS NOT NULL
					AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
		)
	  '
	ELSE
		SET @sqlcmd = '';
   
  SET @sqlcmd = @sqlcmd + 
  'select distinct ' + @LocalTopRow + '
  	   KRT_Csoportok.Id,
	   KRT_Csoportok.Org,
	   KRT_Csoportok.Kod,
	   KRT_Csoportok.Nev,
	   KRT_Csoportok.Tipus,
	   KRT_Csoportok.Jogalany,
	   KRT_Csoportok.ErtesitesEmail,
	   KRT_Csoportok.System,
	   KRT_Csoportok.Adatforras,
	   KRT_Csoportok.ObjTipus_Id_Szulo,
	   KRT_Csoportok.Kiszolgalhato,
	   KRT_Csoportok.JogosultsagOroklesMod,
	   KRT_Csoportok.Ver,
	   KRT_Csoportok.Note,
	   KRT_Csoportok.Stat_id,
	   KRT_Csoportok.ErvKezd,
	   KRT_Csoportok.ErvVege,
	   KRT_Csoportok.Letrehozo_id,
	   KRT_Csoportok.LetrehozasIdo,
	   KRT_Csoportok.Modosito_id,
	   KRT_Csoportok.ModositasIdo,
	   KRT_Csoportok.Zarolo_id,
	   KRT_Csoportok.ZarolasIdo,
	   KRT_Csoportok.Tranz_id,
	   KRT_Csoportok.UIAccessLog_id  
   from 
     KRT_Csoportok as KRT_Csoportok '
    if @kozvetlenulHozzarendelt = 1
		set @sqlcmd = @sqlcmd + '
		inner join KRT_Csoporttagok 
			on KRT_Csoporttagok.Csoport_Id = '''+cast(@szervezetId as nvarchar(36))+''' 
			and KRT_Csoporttagok.Tipus is not null 
	where KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany '
			--and getdate() between KRT_Csoporttagok.ErvKezd and KRT_Csoporttagok.ErvVege '      
	else
		set @sqlcmd = @sqlcmd + '
		inner join temp	on temp.Id = KRT_Csoportok.Id '
	
	set @sqlcmd = @sqlcmd + ' and KRT_Csoportok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   if @OrderBy IS NOT NULL AND @OrderBy != ''
   SET @sqlcmd = @sqlcmd + ' order by ' + @OrderBy;
	print @sqlcmd;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end