﻿CREATE PROCEDURE [dbo].[sp_KRT_CsoportokGetLeader]
	@CsoportId		uniqueidentifier
AS
BEGIN
begin try
	SET NOCOUNT ON;

	select KRT_Csoportok.* from KRT_Csoportok
		left join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @CsoportId AND KRT_Csoporttagok.Tipus = '3' and getdate() between KRT_Csoporttagok.ErvKezd and KRT_Csoporttagok.ErvVege
	where KRT_Csoportok.Id = KRT_Csoporttagok.Csoport_Id_Jogalany
end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END