﻿create procedure [dbo].[sp_KRT_CsoportokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Kod     Nvarchar(100)  = null,         
             @Nev     Nvarchar(400)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Jogalany     char(1)  = null,         
             @ErtesitesEmail     Nvarchar(100)  = null,         
             @System     char(1)  = null,         
             @Adatforras     char(1)  = null,         
             @ObjTipus_Id_Szulo     uniqueidentifier  = null,         
             @Kiszolgalhato     char(1)  = null,         
             @JogosultsagOroklesMod     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Kod     Nvarchar(100)         
     DECLARE @Act_Nev     Nvarchar(400)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Jogalany     char(1)         
     DECLARE @Act_ErtesitesEmail     Nvarchar(100)         
     DECLARE @Act_System     char(1)         
     DECLARE @Act_Adatforras     char(1)         
     DECLARE @Act_ObjTipus_Id_Szulo     uniqueidentifier         
     DECLARE @Act_Kiszolgalhato     char(1)         
     DECLARE @Act_JogosultsagOroklesMod     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Kod = Kod,
     @Act_Nev = Nev,
     @Act_Tipus = Tipus,
     @Act_Jogalany = Jogalany,
     @Act_ErtesitesEmail = ErtesitesEmail,
     @Act_System = System,
     @Act_Adatforras = Adatforras,
     @Act_ObjTipus_Id_Szulo = ObjTipus_Id_Szulo,
     @Act_Kiszolgalhato = Kiszolgalhato,
     @Act_JogosultsagOroklesMod = JogosultsagOroklesMod,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Csoportok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Kod')=1
         SET @Act_Kod = @Kod
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Jogalany')=1
         SET @Act_Jogalany = @Jogalany
   IF @UpdatedColumns.exist('/root/ErtesitesEmail')=1
         SET @Act_ErtesitesEmail = @ErtesitesEmail
   IF @UpdatedColumns.exist('/root/System')=1
         SET @Act_System = @System
   IF @UpdatedColumns.exist('/root/Adatforras')=1
         SET @Act_Adatforras = @Adatforras
   IF @UpdatedColumns.exist('/root/ObjTipus_Id_Szulo')=1
         SET @Act_ObjTipus_Id_Szulo = @ObjTipus_Id_Szulo
   IF @UpdatedColumns.exist('/root/Kiszolgalhato')=1
         SET @Act_Kiszolgalhato = @Kiszolgalhato
   IF @UpdatedColumns.exist('/root/JogosultsagOroklesMod')=1
         SET @Act_JogosultsagOroklesMod = @JogosultsagOroklesMod
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Csoportok
SET
     Org = @Act_Org,
     Kod = @Act_Kod,
     Nev = @Act_Nev,
     Tipus = @Act_Tipus,
     Jogalany = @Act_Jogalany,
     ErtesitesEmail = @Act_ErtesitesEmail,
     System = @Act_System,
     Adatforras = @Act_Adatforras,
     ObjTipus_Id_Szulo = @Act_ObjTipus_Id_Szulo,
     Kiszolgalhato = @Act_Kiszolgalhato,
     JogosultsagOroklesMod = @Act_JogosultsagOroklesMod,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Csoportok',@Id
					,'KRT_CsoportokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH