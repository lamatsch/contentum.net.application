﻿create procedure [dbo].[sp_KRT_DokuAlairasKapcsolatokHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_DokuAlairasKapcsolatokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokuAlairasKapcsolatokHistory Old
         inner join KRT_DokuAlairasKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokuAlairasKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_id' as ColumnName,               cast(Old.Dokumentum_id as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokuAlairasKapcsolatokHistory Old
         inner join KRT_DokuAlairasKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokuAlairasKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Dokumentum_id != New.Dokumentum_id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokumentumAlairas_Id' as ColumnName,               cast(Old.DokumentumAlairas_Id as nvarchar(99)) as OldValue,
               cast(New.DokumentumAlairas_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokuAlairasKapcsolatokHistory Old
         inner join KRT_DokuAlairasKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokuAlairasKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.DokumentumAlairas_Id != New.DokumentumAlairas_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokuKapcsolatSorrend' as ColumnName,               cast(Old.DokuKapcsolatSorrend as nvarchar(99)) as OldValue,
               cast(New.DokuKapcsolatSorrend as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokuAlairasKapcsolatokHistory Old
         inner join KRT_DokuAlairasKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokuAlairasKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.DokuKapcsolatSorrend != New.DokuKapcsolatSorrend 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end