﻿create procedure [dbo].[sp_KRT_DokumentumAlairasokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_DokumentumAlairasok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_DokumentumAlairasok.Id,
	   KRT_DokumentumAlairasok.Dokumentum_Id_Alairt,
	   KRT_DokumentumAlairasok.AlairasMod,
	   KRT_DokumentumAlairasok.AlairasSzabaly_Id,
	   KRT_DokumentumAlairasok.AlairasRendben,
	   KRT_DokumentumAlairasok.KivarasiIdoVege,
	   KRT_DokumentumAlairasok.AlairasVeglegRendben,
	   KRT_DokumentumAlairasok.Idopecset,
	   KRT_DokumentumAlairasok.Csoport_Id_Alairo,
	   KRT_DokumentumAlairasok.AlairasTulajdonos,
	   KRT_DokumentumAlairasok.Tanusitvany_Id,
	   KRT_DokumentumAlairasok.Allapot,
	   KRT_DokumentumAlairasok.Ver,
	   KRT_DokumentumAlairasok.Note,
	   KRT_DokumentumAlairasok.Stat_id,
	   KRT_DokumentumAlairasok.ErvKezd,
	   KRT_DokumentumAlairasok.ErvVege,
	   KRT_DokumentumAlairasok.Letrehozo_id,
	   KRT_DokumentumAlairasok.LetrehozasIdo,
	   KRT_DokumentumAlairasok.Modosito_id,
	   KRT_DokumentumAlairasok.ModositasIdo,
	   KRT_DokumentumAlairasok.Zarolo_id,
	   KRT_DokumentumAlairasok.ZarolasIdo,
	   KRT_DokumentumAlairasok.Tranz_id,
	   KRT_DokumentumAlairasok.UIAccessLog_id  
   from 
     KRT_DokumentumAlairasok as KRT_DokumentumAlairasok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end