﻿create procedure [dbo].[sp_KRT_DokumentumAlairasokInsert]    
                @Id      uniqueidentifier = null,    
                               @Dokumentum_Id_Alairt     uniqueidentifier  = null,
                @AlairasMod     nvarchar(64)  = null,
                @AlairasSzabaly_Id     uniqueidentifier  = null,
	            @AlairasRendben     char(1),
	            @KivarasiIdoVege     datetime,
	            @AlairasVeglegRendben     char(1),
                @Idopecset     datetime  = null,
                @Csoport_Id_Alairo     uniqueidentifier  = null,
	            @AlairasTulajdonos     Nvarchar(100),
                @Tanusitvany_Id     uniqueidentifier  = null,
                @Allapot     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Dokumentum_Id_Alairt is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id_Alairt'
            SET @insertValues = @insertValues + ',@Dokumentum_Id_Alairt'
         end 
       
         if @AlairasMod is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasMod'
            SET @insertValues = @insertValues + ',@AlairasMod'
         end 
       
         if @AlairasSzabaly_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasSzabaly_Id'
            SET @insertValues = @insertValues + ',@AlairasSzabaly_Id'
         end 
       
         if @AlairasRendben is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasRendben'
            SET @insertValues = @insertValues + ',@AlairasRendben'
         end 
       
         if @KivarasiIdoVege is not null
         begin
            SET @insertColumns = @insertColumns + ',KivarasiIdoVege'
            SET @insertValues = @insertValues + ',@KivarasiIdoVege'
         end 
       
         if @AlairasVeglegRendben is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasVeglegRendben'
            SET @insertValues = @insertValues + ',@AlairasVeglegRendben'
         end 
       
         if @Idopecset is not null
         begin
            SET @insertColumns = @insertColumns + ',Idopecset'
            SET @insertValues = @insertValues + ',@Idopecset'
         end 
       
         if @Csoport_Id_Alairo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Alairo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Alairo'
         end 
       
         if @AlairasTulajdonos is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasTulajdonos'
            SET @insertValues = @insertValues + ',@AlairasTulajdonos'
         end 
       
         if @Tanusitvany_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tanusitvany_Id'
            SET @insertValues = @insertValues + ',@Tanusitvany_Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_DokumentumAlairasok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Dokumentum_Id_Alairt uniqueidentifier,@AlairasMod nvarchar(64),@AlairasSzabaly_Id uniqueidentifier,@AlairasRendben char(1),@KivarasiIdoVege datetime,@AlairasVeglegRendben char(1),@Idopecset datetime,@Csoport_Id_Alairo uniqueidentifier,@AlairasTulajdonos Nvarchar(100),@Tanusitvany_Id uniqueidentifier,@Allapot nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Dokumentum_Id_Alairt = @Dokumentum_Id_Alairt,@AlairasMod = @AlairasMod,@AlairasSzabaly_Id = @AlairasSzabaly_Id,@AlairasRendben = @AlairasRendben,@KivarasiIdoVege = @KivarasiIdoVege,@AlairasVeglegRendben = @AlairasVeglegRendben,@Idopecset = @Idopecset,@Csoport_Id_Alairo = @Csoport_Id_Alairo,@AlairasTulajdonos = @AlairasTulajdonos,@Tanusitvany_Id = @Tanusitvany_Id,@Allapot = @Allapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_DokumentumAlairasok',@ResultUid
					,'KRT_DokumentumAlairasokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH