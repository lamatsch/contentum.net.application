﻿create procedure [dbo].[sp_KRT_DokumentumCsatolasokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_DokumentumCsatolasok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_DokumentumCsatolasok.Id,
	   KRT_DokumentumCsatolasok.ObjTip_Id_Dokumentum,
	   KRT_DokumentumCsatolasok.Obj_Id_Dokumentum,
	   KRT_DokumentumCsatolasok.ObjTip_Id,
	   KRT_DokumentumCsatolasok.Obj_Id,
	   KRT_DokumentumCsatolasok.Tipus,
	   KRT_DokumentumCsatolasok.Tipus_Id,
	   KRT_DokumentumCsatolasok.Leiras,
	   KRT_DokumentumCsatolasok.Ver,
	   KRT_DokumentumCsatolasok.Note,
	   KRT_DokumentumCsatolasok.Stat_id,
	   KRT_DokumentumCsatolasok.ErvKezd,
	   KRT_DokumentumCsatolasok.ErvVege,
	   KRT_DokumentumCsatolasok.Letrehozo_id,
	   KRT_DokumentumCsatolasok.LetrehozasIdo,
	   KRT_DokumentumCsatolasok.Modosito_id,
	   KRT_DokumentumCsatolasok.ModositasIdo,
	   KRT_DokumentumCsatolasok.Zarolo_id,
	   KRT_DokumentumCsatolasok.ZarolasIdo,
	   KRT_DokumentumCsatolasok.Tranz_id,
	   KRT_DokumentumCsatolasok.UIAccessLog_id  
   from 
     KRT_DokumentumCsatolasok as KRT_DokumentumCsatolasok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end