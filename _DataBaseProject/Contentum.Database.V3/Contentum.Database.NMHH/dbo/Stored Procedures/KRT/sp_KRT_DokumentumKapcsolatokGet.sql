﻿create procedure [dbo].[sp_KRT_DokumentumKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_DokumentumKapcsolatok.Id,
	   KRT_DokumentumKapcsolatok.Tipus,
	   KRT_DokumentumKapcsolatok.Dokumentum_Id_Fo,
	   KRT_DokumentumKapcsolatok.Dokumentum_Id_Al,
	   KRT_DokumentumKapcsolatok.DokumentumKapcsolatJelleg,
	   KRT_DokumentumKapcsolatok.DokuKapcsolatSorrend,
	   KRT_DokumentumKapcsolatok.Ver,
	   KRT_DokumentumKapcsolatok.Note,
	   KRT_DokumentumKapcsolatok.Stat_id,
	   KRT_DokumentumKapcsolatok.ErvKezd,
	   KRT_DokumentumKapcsolatok.ErvVege,
	   KRT_DokumentumKapcsolatok.Letrehozo_id,
	   KRT_DokumentumKapcsolatok.LetrehozasIdo,
	   KRT_DokumentumKapcsolatok.Modosito_id,
	   KRT_DokumentumKapcsolatok.ModositasIdo,
	   KRT_DokumentumKapcsolatok.Zarolo_id,
	   KRT_DokumentumKapcsolatok.ZarolasIdo,
	   KRT_DokumentumKapcsolatok.Tranz_id,
	   KRT_DokumentumKapcsolatok.UIAccessLog_id
	   from 
		 KRT_DokumentumKapcsolatok as KRT_DokumentumKapcsolatok 
	   where
		 KRT_DokumentumKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end