﻿create procedure [dbo].[sp_KRT_DokumentumKapcsolatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_DokumentumKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_DokumentumKapcsolatok.Id,
	   KRT_DokumentumKapcsolatok.Tipus,
	   KRT_DokumentumKapcsolatok.Dokumentum_Id_Fo,
	   KRT_DokumentumKapcsolatok.Dokumentum_Id_Al,
	   KRT_DokumentumKapcsolatok.DokumentumKapcsolatJelleg,
	   KRT_DokumentumKapcsolatok.DokuKapcsolatSorrend,
	   KRT_DokumentumKapcsolatok.Ver,
	   KRT_DokumentumKapcsolatok.Note,
	   KRT_DokumentumKapcsolatok.Stat_id,
	   KRT_DokumentumKapcsolatok.ErvKezd,
	   KRT_DokumentumKapcsolatok.ErvVege,
	   KRT_DokumentumKapcsolatok.Letrehozo_id,
	   KRT_DokumentumKapcsolatok.LetrehozasIdo,
	   KRT_DokumentumKapcsolatok.Modosito_id,
	   KRT_DokumentumKapcsolatok.ModositasIdo,
	   KRT_DokumentumKapcsolatok.Zarolo_id,
	   KRT_DokumentumKapcsolatok.ZarolasIdo,
	   KRT_DokumentumKapcsolatok.Tranz_id,
	   KRT_DokumentumKapcsolatok.UIAccessLog_id  
   from 
     KRT_DokumentumKapcsolatok as KRT_DokumentumKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end