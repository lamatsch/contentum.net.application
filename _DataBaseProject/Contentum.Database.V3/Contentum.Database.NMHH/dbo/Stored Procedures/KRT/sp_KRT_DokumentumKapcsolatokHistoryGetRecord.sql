﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_DokumentumKapcsolatokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_DokumentumKapcsolatokHistoryGetRecord
go
*/
create procedure sp_KRT_DokumentumKapcsolatokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_DokumentumKapcsolatokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_DokumentumKapcsolatokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id_Fo' as ColumnName,               
               cast(Old.Dokumentum_Id_Fo as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id_Fo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id_Fo as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id_Fo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id_Al' as ColumnName,               
               cast(Old.Dokumentum_Id_Al as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id_Al as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id_Al as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id_Al as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokumentumKapcsolatJelleg' as ColumnName,               
               cast(Old.DokumentumKapcsolatJelleg as nvarchar(99)) as OldValue,
               cast(New.DokumentumKapcsolatJelleg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DokumentumKapcsolatJelleg as nvarchar(max)),'') != ISNULL(CAST(New.DokumentumKapcsolatJelleg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokuKapcsolatSorrend' as ColumnName,               
               cast(Old.DokuKapcsolatSorrend as nvarchar(99)) as OldValue,
               cast(New.DokuKapcsolatSorrend as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumKapcsolatokHistory Old
         inner join KRT_DokumentumKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DokuKapcsolatSorrend as nvarchar(max)),'') != ISNULL(CAST(New.DokuKapcsolatSorrend as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go