﻿create procedure [dbo].[sp_KRT_DokumentumokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Dokumentumok.Id,
	   KRT_Dokumentumok.Org,
	   KRT_Dokumentumok.FajlNev,
	   KRT_Dokumentumok.VerzioJel,
	   KRT_Dokumentumok.Nev,
	   KRT_Dokumentumok.Tipus,
	   KRT_Dokumentumok.Formatum,
	   KRT_Dokumentumok.Leiras,
	   KRT_Dokumentumok.Meret,
	   KRT_Dokumentumok.TartalomHash,
	   KRT_Dokumentumok.AlairtTartalomHash,
	   KRT_Dokumentumok.KivonatHash,
	   KRT_Dokumentumok.Dokumentum_Id,
	   KRT_Dokumentumok.Dokumentum_Id_Kovetkezo,
	   KRT_Dokumentumok.Alkalmazas_Id,
	   KRT_Dokumentumok.Csoport_Id_Tulaj,
	   KRT_Dokumentumok.KulsoAzonositok,
	   KRT_Dokumentumok.External_Source,
	   KRT_Dokumentumok.External_Link,
	   KRT_Dokumentumok.External_Id,
	   KRT_Dokumentumok.External_Info,
	   KRT_Dokumentumok.CheckedOut,
	   KRT_Dokumentumok.CheckedOutTime,
	   KRT_Dokumentumok.BarCode,
	   KRT_Dokumentumok.OCRPrioritas,
	   KRT_Dokumentumok.OCRAllapot,
	   KRT_Dokumentumok.Allapot,
	   KRT_Dokumentumok.WorkFlowAllapot,
	   KRT_Dokumentumok.Megnyithato,
	   KRT_Dokumentumok.Olvashato,
	   KRT_Dokumentumok.ElektronikusAlairas,
	   KRT_Dokumentumok.AlairasFelulvizsgalat,
	   KRT_Dokumentumok.Titkositas,
	   KRT_Dokumentumok.SablonAzonosito,
	   KRT_Dokumentumok.Ver,
	   KRT_Dokumentumok.Note,
	   KRT_Dokumentumok.Stat_id,
	   KRT_Dokumentumok.ErvKezd,
	   KRT_Dokumentumok.ErvVege,
	   KRT_Dokumentumok.Letrehozo_id,
	   KRT_Dokumentumok.LetrehozasIdo,
	   KRT_Dokumentumok.Modosito_id,
	   KRT_Dokumentumok.ModositasIdo,
	   KRT_Dokumentumok.Zarolo_Id,
	   KRT_Dokumentumok.ZarolasIdo,
	   KRT_Dokumentumok.Tranz_Id,
	   KRT_Dokumentumok.UIAccessLog_Id,
	   KRT_Dokumentumok.AlairasFelulvizsgalatEredmeny
	   from 
		 KRT_Dokumentumok as KRT_Dokumentumok 
	   where
		 KRT_Dokumentumok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end