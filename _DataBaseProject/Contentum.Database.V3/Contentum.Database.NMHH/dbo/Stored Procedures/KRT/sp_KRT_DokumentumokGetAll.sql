﻿create procedure [dbo].[sp_KRT_DokumentumokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Dokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Dokumentumok.Id,
	   KRT_Dokumentumok.Org,
	   KRT_Dokumentumok.FajlNev,
	   KRT_Dokumentumok.VerzioJel,
	   KRT_Dokumentumok.Nev,
	   KRT_Dokumentumok.Tipus,
	   KRT_Dokumentumok.Formatum,
	   KRT_Dokumentumok.Leiras,
	   KRT_Dokumentumok.Meret,
	   KRT_Dokumentumok.TartalomHash,
	   KRT_Dokumentumok.AlairtTartalomHash,
	   KRT_Dokumentumok.KivonatHash,
	   KRT_Dokumentumok.Dokumentum_Id,
	   KRT_Dokumentumok.Dokumentum_Id_Kovetkezo,
	   KRT_Dokumentumok.Alkalmazas_Id,
	   KRT_Dokumentumok.Csoport_Id_Tulaj,
	   KRT_Dokumentumok.KulsoAzonositok,
	   KRT_Dokumentumok.External_Source,
	   KRT_Dokumentumok.External_Link,
	   KRT_Dokumentumok.External_Id,
	   KRT_Dokumentumok.External_Info,
	   KRT_Dokumentumok.CheckedOut,
	   KRT_Dokumentumok.CheckedOutTime,
	   KRT_Dokumentumok.BarCode,
	   KRT_Dokumentumok.OCRPrioritas,
	   KRT_Dokumentumok.OCRAllapot,
	   KRT_Dokumentumok.Allapot,
	   KRT_Dokumentumok.WorkFlowAllapot,
	   KRT_Dokumentumok.Megnyithato,
	   KRT_Dokumentumok.Olvashato,
	   KRT_Dokumentumok.ElektronikusAlairas,
	   KRT_Dokumentumok.AlairasFelulvizsgalat,
	   KRT_Dokumentumok.Titkositas,
	   KRT_Dokumentumok.SablonAzonosito,
	   KRT_Dokumentumok.Ver,
	   KRT_Dokumentumok.Note,
	   KRT_Dokumentumok.Stat_id,
	   KRT_Dokumentumok.ErvKezd,
	   KRT_Dokumentumok.ErvVege,
	   KRT_Dokumentumok.Letrehozo_id,
	   KRT_Dokumentumok.LetrehozasIdo,
	   KRT_Dokumentumok.Modosito_id,
	   KRT_Dokumentumok.ModositasIdo,
	   KRT_Dokumentumok.Zarolo_Id,
	   KRT_Dokumentumok.ZarolasIdo,
	   KRT_Dokumentumok.Tranz_Id,
	   KRT_Dokumentumok.UIAccessLog_Id,
	   KRT_Dokumentumok.AlairasFelulvizsgalatEredmeny  
   from 
     KRT_Dokumentumok as KRT_Dokumentumok      
    Where KRT_Dokumentumok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end