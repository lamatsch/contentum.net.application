﻿
create procedure [dbo].[sp_KRT_DokumentumokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Dokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
  @Jogosultak		char(1) = '0',
  @Filter_FileContent nvarchar(400) = NULL,
  @Filter_BarCode nvarchar(100) = NULL,
  @Altalanos_FTS	nvarchar(MAX) = '',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow INT


   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	END

	
	set @sqlcmd = ''

	if (@Where is null)
		set @Where=''
	
	/************************************************************
	* @Altalanos_FTS tartalmából szűrőfeltételek összeállítása	*
	************************************************************/
    declare @Where_Altalanos nvarchar(MAX)
    set @Where_Altalanos = ''
    
    if @Altalanos_FTS is not null and @Altalanos_FTS != ''
    begin	
										
        set @Where_Altalanos = ' (contains(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''' + @Altalanos_FTS + ''' )'
                             + ' or contains(EREC_KuldKuldemenyek.Targy, ''' + @Altalanos_FTS + ''' )' 
                             + ' or contains(EREC_IraIratok.Targy, ''' + @Altalanos_FTS + ''' )'
                             + ' or contains(EREC_UgyUgyiratok.Targy, ''' + @Altalanos_FTS + ''' )'
                             + ' or contains(EREC_UgyUgyiratok.NevSTR_Ugyindito, ''' + @Altalanos_FTS + ''' ))' 
    end
	/************************************************************/
	
	
	/******************************
	* Van-e szűrés fájltartalomra:	
	*******************************/
	declare @sqlcmd_FileContentFilter NVARCHAR(1000)
	set @sqlcmd_FileContentFilter = ''
	
	if @Filter_FileContent is not null and @Filter_FileContent != ''
	begin
		-- eDocumentWebServiceUrl lekérdezése:
		declare @eDocumentWebServiceUrl NVARCHAR(400)
		set @eDocumentWebServiceUrl = (SELECT Ertek FROM KRT_Parameterek where Nev = 'eDocumentWebServiceUrl' and Org=@Org)
		if @eDocumentWebServiceUrl is null 
			RAISERROR('Hiányzó rendszerparaméter: eDocumentWebServiceUrl',16,1)
		
		declare @documentStoreType NVARCHAR(400)
		set @documentStoreType = (SELECT Ertek FROM KRT_Parameterek where Nev = 'IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS' and Org=@Org)

		IF @documentStoreType = 'UCM'
		BEGIN
	    set @sqlcmd_FileContentFilter = ' JOIN dbo.[UCMFullTextSearch]('''
				+ @eDocumentWebServiceUrl +''',@ExecutorUserId, ''' + @Filter_FileContent + ''') sps
				ON sps.PATH = [KRT_Dokumentumok].[External_Info]'
	   END
	   ELSE
	   BEGIN
		set @sqlcmd_FileContentFilter = ' JOIN dbo.[SharepointFileContentSearch]('''
				+ @eDocumentWebServiceUrl +''',@ExecutorUserId, ''' + @Filter_FileContent + ''') sps
				ON sps.PATH = [KRT_Dokumentumok].[External_Link]'
	   END
	end
	/******************************/

	
	/******************************
	* Van-e szűrés vonalkódra:	
	*******************************/
	declare @sqlcmd_BarCodeFilter NVARCHAR(1000)
	set @sqlcmd_BarCodeFilter = ''
	
	if @Filter_BarCode is not null and @Filter_BarCode != ''
	begin
		declare @Id_DokumentumByBarcode nvarchar(40)
		set @Id_DokumentumByBarcode = (select top 1 convert(nvarchar(40), d.Id) from dbo.KRT_Dokumentumok d
			where BarCode=@Filter_BarCode and getdate() between d.ErvKezd and d.ErvVege)


		if @Id_DokumentumByBarcode is null -- ha nem található, megnézzük a mellékletek felől
		begin
			set @Id_DokumentumByBarcode = (select top 1 convert(nvarchar(40), d.Id) from dbo.KRT_Dokumentumok d
				inner join EREC_Csatolmanyok cs on cs.Dokumentum_Id=d.Id
				inner join EREC_IratelemKapcsolatok ik on ik.Csatolmany_Id = cs.Id
				inner join EREC_Mellekletek m on m.Id = ik.Melleklet_Id
				where m.BarCode=@Filter_BarCode
					and getdate() between d.ErvKezd and d.ErvVege
					and getdate() between cs.ErvKezd and cs.ErvVege
					and getdate() between ik.ErvKezd and ik.ErvVege
					and getdate() between m.ErvKezd and m.ErvVege
				order by case when d.Formatum='tif' then 1 -- ha van több is, előbb a tif-ekből a legutolsót,
						else 2  -- aztán a többiből a legutolsót próbáljuk kiválasztani
					end, d.LetrehozasIdo DESC
				)
		end

		if @Id_DokumentumByBarcode is null -- ha nem található, megnézzük az iratkezelési elemek felől
		begin
			;with BarkodRecord as
			(
				select IsNull(i.Id, k.Id) Obj_id from dbo.KRT_BarKodok b
				left join EREC_KuldKuldemenyek k on k.Id=b.Obj_Id and b.Obj_type='EREC_KuldKuldemenyek'
				left join EREC_PldIratPeldanyok p on p.Id=b.Obj_Id and b.Obj_type='EREC_PldIratPeldanyok'
				left join EREC_IraIratok i on i.Id=p.IraIrat_Id and p.Sorszam=1
				where b.Kod=@Filter_BarCode
					and getdate() between b.ErvKezd and b.ErvVege
					and IsNull(i.Id, k.Id) is not null
			)
			select top 1 @Id_DokumentumByBarcode = convert(nvarchar(40), d.Id) from dbo.KRT_Dokumentumok d
				inner join EREC_Csatolmanyok cs on cs.Dokumentum_Id=d.Id
				inner join BarkodRecord bcr on bcr.Obj_id in (cs.IraIrat_Id, cs.KuldKuldemeny_Id)
			where getdate() between d.ErvKezd and d.ErvVege
			order by case when d.Formatum='tif' then 1 -- ha van több is, előbb a tif-ekből a legutolsót,
					else 2  -- aztán a többiből a legutolsót próbáljuk kiválasztani
				end, d.LetrehozasIdo DESC
		end


		if @Id_DokumentumByBarcode is not null
		begin
			set @sqlcmd_BarCodeFilter = ' KRT_Dokumentumok.Id=''' + @Id_DokumentumByBarcode + ''''
		end
		else
		begin
			set @sqlcmd_BarCodeFilter = ' 1=0' -- lehetetlen feltétel
		end
	end
	/******************************/

	/************************************************************
	* Bizalmas iratok őrző szerinti szűréséhez csoporttagok lekérése*
	************************************************************/
--	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
--	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
--	begin
----	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier)
----insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
----			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
----			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
----'
--	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
--
--	set @sqlcmd = @sqlcmd + N'; declare @kuldemenyek_executor table (Id uniqueidentifier primary key)
--insert into @kuldemenyek_executor select Id from dbo.fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
--	end

--	declare @sqlcmd_filterBizalmasIratCsatolmanyok NVARCHAR(1000)
--	set @sqlcmd_filterBizalmasIratCsatolmanyok = ''

---- bizalmas irat és végrehajtó felhasználó nem az őrző vagy annak vezetője
--	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
--	begin
--		SET @sqlcmd_filterBizalmasIratCsatolmanyok = @sqlcmd_filterBizalmasIratCsatolmanyok + '
--	(Csatolmanyok_Irat.IraIrat_Id is null or 
--	dbo.fn_CheckIrat_IsConfidential(Csatolmanyok_Irat.IraIrat_Id) = 0
--	or Csatolmanyok_Irat.IraIrat_Id in (select Id from @iratok_executor))
--'
--	end
	/************************************************************/
	
	
	if @Where is not null and @Where != '' and @Where_Altalanos is not null and @Where_Altalanos != ''
	begin
		set @Where_Altalanos = ' AND ' + @Where_Altalanos
	end

	if ((@Where is not null and @Where != '') or (@Where_Altalanos is not null and @Where_Altalanos != ''))
		and @sqlcmd_BarCodeFilter is not null and @sqlcmd_BarCodeFilter != ''
	begin
		set @sqlcmd_BarCodeFilter = ' AND ' + @sqlcmd_BarCodeFilter
	end

--	if ((@Where is not null and @Where != '') or (@Where_Altalanos is not null and @Where_Altalanos != ''))
--		and (@sqlcmd_filterBizalmasIratCsatolmanyok is not null and @sqlcmd_filterBizalmasIratCsatolmanyok != '')
--	begin
--		set @sqlcmd_filterBizalmasIratCsatolmanyok = ' AND ' + @sqlcmd_filterBizalmasIratCsatolmanyok
--	end

	declare @OrgFilter nvarchar(400)
	set @OrgFilter=''
	IF (select count(Id) from KRT_Orgok)>1
	BEGIN
		IF ((@Where is not null and @Where != '') or (@Where_Altalanos is not null and @Where_Altalanos != '')
--			or (@sqlcmd_filterBizalmasIratCsatolmanyok is not null and @sqlcmd_filterBizalmasIratCsatolmanyok != '')
)
		BEGIN
			set @OrgFilter=' AND '
		END
		SET @OrgFilter = @OrgFilter + 'KRT_Dokumentumok.Org=@Org
'
	END

	SET @sqlcmd = @sqlcmd + 'SELECT KRT_Dokumentumok.Id, EREC_IraIratok.Id Irat_Id, EREC_IraIratok.Minosites Minosites_Irat, EREC_KuldKuldemenyek.Id Kuldemeny_Id, EREC_KuldKuldemenyek.Minosites Minosites_Kuld
into #filter 
			FROM KRT_Dokumentumok 
			left join EREC_Csatolmanyok as Csatolmanyok_Irat
				on Csatolmanyok_Irat.[Dokumentum_Id] = KRT_Dokumentumok.Id and getdate() between Csatolmanyok_Irat.ErvKezd and Csatolmanyok_Irat.ErvVege
			  left join EREC_IraIratok
				on Csatolmanyok_Irat.IraIrat_Id = EREC_IraIratok.Id
			  left join EREC_UgyUgyiratok
				on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
			  left join EREC_IraIktatokonyvek
				on EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			  left join EREC_Csatolmanyok as Csatolmanyok_Kuld
				on Csatolmanyok_Kuld.[Dokumentum_Id] = KRT_Dokumentumok.Id and getdate() between Csatolmanyok_Kuld.ErvKezd and Csatolmanyok_Kuld.ErvVege
			  left join EREC_KuldKuldemenyek
				on Csatolmanyok_Kuld.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
			  left join EREC_IraIktatokonyvek as Erkeztetokonyv
				on EREC_KuldKuldemenyek.IraIktatokonyv_Id = Erkeztetokonyv.Id'
			+ @sqlcmd_FileContentFilter				
			+ ' WHERE ' + @Where + @Where_Altalanos
			+ @sqlcmd_BarCodeFilter
--			+ @sqlcmd_filterBizalmasIratCsatolmanyok
			+ @OrgFilter;
		

	-- Jogosultságkezelés (az EREC_Csatolmanyok táblán keresztül az iratra vagy a küldeményre van-e jogosultsága)										
   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   BEGIN
--		SET @sqlcmd = @sqlcmd + ' DELETE FROM #filter WHERE
--									 #filter.Id NOT IN
--									 (
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIratok.Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--											WHERE EREC_IraIratok.Ugyirat_Id IN 
--											(
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
--												UNION ALL
--												SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
--											)
--										UNION ALL --CR1716: lenézünk a példányba
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--											INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
--											WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''' and EREC_PldIratpeldanyok.Csoport_Id_Felelos = ''' + CAST(@ExecutorUserId AS CHAR(36)) + '''
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											WHERE EREC_Csatolmanyok.IraIrat_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_IraIratok''))
--										
--										UNION ALL';
--										
--		SET @sqlcmd = @sqlcmd + '		SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--												ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--												ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
--										UNION ALL										
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
--											INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
--											WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
--												AND EREC_IraIratok.Ugyirat_Id IN 
--											(
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
--														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
--												UNION ALL
--												SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
--											)
--										UNION ALL
--										SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--											WHERE EREC_Csatolmanyok.KuldKuldemeny_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_KuldKuldemenyek''))
--									)'


		SET @sqlcmd = @sqlcmd + '
;with CsoportTagokAll as
(
	select Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id)
),
jogosult_objektumok as
(
	select krt_jogosultak.Obj_Id from krt_jogosultak
		INNER JOIN CsoportTagokAll 
	ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
),
jogosult_ugyiratok as
(
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	UNION ALL
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
),
jogosult_peldanyok as
(
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
	WHERE EREC_PldIratPeldanyok.Id IN
	(
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_PldIratPeldanyok.Id
		UNION ALL
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			INNER JOIN CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos
		UNION ALL
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			INNER JOIN CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo
		UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_PldIratPeldanyok'')	
	)
),
csatolmany_kuldemenyek as
(
		SELECT EREC_Csatolmanyok.Dokumentum_Id, EREC_KuldKuldemenyek.Id, EREC_KuldKuldemenyek.IraIratok_Id, EREC_KuldKuldemenyek.IraIktatokonyv_Id, EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo, EREC_KuldKuldemenyek.Csoport_Id_Felelos
			FROM EREC_Csatolmanyok
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
),
csatolmany_iratok as
(
	select EREC_Csatolmanyok.Dokumentum_Id, EREC_IraIratok.Id, EREC_IraIratok.Ugyirat_Id FROM EREC_Csatolmanyok
		INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
)
DELETE FROM #filter WHERE
 #filter.Id NOT IN
 (
	SELECT csatolmany_iratok.Dokumentum_Id FROM csatolmany_iratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = csatolmany_iratok.Id
	UNION ALL
	SELECT csatolmany_iratok.Dokumentum_Id FROM csatolmany_iratok
		WHERE csatolmany_iratok.Ugyirat_Id IN 
		(
			SELECT Id FROM jogosult_ugyiratok
		)
'

		SET @sqlcmd = @sqlcmd + '	UNION ALL --CR1716: lenézünk a példányba, CR 2728: aki láthatja a példányt az láthatja az iratot is
	SELECT csatolmany_iratok.Dokumentum_Id FROM csatolmany_iratok
		INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = csatolmany_iratok.Id
		WHERE EREC_PldIratpeldanyok.Id IN
		(
			Select Id From jogosult_peldanyok
		)
	UNION ALL
	SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
		WHERE EREC_Csatolmanyok.IraIrat_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_IraIratok''))
	UNION ALL
	SELECT Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = csatolmany_kuldemenyek.Id
	UNION ALL
	SELECT Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = csatolmany_kuldemenyek.IraIktatokonyv_Id
	UNION ALL
	SELECT Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = csatolmany_kuldemenyek.IraIratok_Id
	UNION ALL
	SELECT Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = csatolmany_kuldemenyek.Csoport_Id_Felelos
	UNION ALL
	SELECT Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = csatolmany_kuldemenyek.FelhasznaloCsoport_Id_Orzo
	UNION ALL										
	SELECT  Dokumentum_Id FROM csatolmany_kuldemenyek
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = csatolmany_kuldemenyek.IraIratok_Id
		WHERE csatolmany_kuldemenyek.IraIratok_Id IS NOT NULL
			AND EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT Id FROM jogosult_ugyiratok
		)
	UNION ALL
	SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
		WHERE EREC_Csatolmanyok.KuldKuldemeny_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek''))
);
'

-- bizalmas irat és végrehajtó felhasználó nem az őrző vagy annak vezetője
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	begin
--		SET @sqlcmd = @sqlcmd + ';DELETE FROM #filter where #filter.Id in
--(select #filter.Id from #filter
--inner join EREC_Csatolmanyok on #filter.Id=EREC_Csatolmanyok.[Dokumentum_Id]
--WHERE
--EREC_Csatolmanyok.IraIrat_Id is not null
--and dbo.fn_CheckIrat_IsConfidential(EREC_Csatolmanyok.IraIrat_Id) = 1
---- végrehajtó felhasználó nem az őrző vagy annak vezetője
--	--and dbo.fn_CheckIrat_ExecutorIsOrzoOrHisLeader(EREC_Csatolmanyok.IraIrat_Id, ''' + CAST( @ExecutorUserId as CHAR(36)) + ''', ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''') = 0
--	and EREC_Csatolmanyok.IraIrat_Id not in (select Id from @iratok_executor)
--UNION ALL
--select #filter.Id from #filter
--inner join EREC_Csatolmanyok on #filter.Id=EREC_Csatolmanyok.[Dokumentum_Id]
--WHERE
--EREC_Csatolmanyok.IraIrat_Id is null and EREC_Csatolmanyok.KuldKuldemeny_Id is not null
--and dbo.fn_CheckKuldemeny_IsConfidential(EREC_Csatolmanyok.KuldKuldemeny_Id) = 1
---- végrehajtó felhasználó nem az őrző vagy annak vezetője
--	--and dbo.fn_CheckKuldemeny_ExecutorIsOrzoOrHisLeader(EREC_Csatolmanyok.KuldKuldemeny_Id, ''' + CAST( @ExecutorUserId as CHAR(36)) + ''', ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''') = 0	
--	and EREC_Csatolmanyok.KuldKuldemeny_Id not in (select Id from @kuldemenyek_executor)
--)
--'
		SET @sqlcmd = @sqlcmd + ';DECLARE @csoporttagsag_tipus nvarchar(64)
	SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
						where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
						and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
						and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
						)

	; CREATE TABLE #jogosultak (Id uniqueidentifier)
	IF @csoporttagsag_tipus = ''3''
	BEGIN
		INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
							inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id =@FelhasznaloSzervezet_Id 
							and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
							where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
							and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO #jogosultak select @ExecutorUserId
	END
'

		SET @sqlcmd = @sqlcmd + '
	DECLARE @confidential varchar(4000)
	set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
							and Org=@Org and getdate() between ErvKezd and ErvVege)

	declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
	insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''));
'
		SET @sqlcmd = @sqlcmd + 'DELETE FROM #filter
	WHERE Minosites_Irat in (select val from @Bizalmas)
	AND NOT EXISTS
			(select 1
				from KRT_Jogosultak where
					KRT_Jogosultak.Obj_Id=#filter.Irat_Id
					and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
					and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
	AND NOT EXISTS
		(select 1
			from EREC_PldIratPeldanyok
			where EREC_PldIratPeldanyok.IraIrat_Id=#filter.Irat_Id
				and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
		);
'

		SET @sqlcmd = @sqlcmd + 'DELETE FROM #filter
	WHERE Irat_Id is null AND Minosites_Kuld in (select val from @Bizalmas)
	AND NOT EXISTS
		(select 1
			from EREC_KuldKuldemenyek where
				EREC_KuldKuldemenyek.Id=#filter.Kuldemeny_Id
				and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo IN (select Id from #jogosultak)
		)
	AND NOT EXISTS
		(select 1
			from KRT_Jogosultak where
				KRT_Jogosultak.Obj_Id=#filter.Kuldemeny_Id
				and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
				and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany);
'
	END

--	/************************************************************
--	* Bizalmas iratok őrző szerinti szűréséhez csoporttagok lekérése*
--	************************************************************/
--	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
--	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
--	begin
----	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier)
----insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
----			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
----			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
----'
--	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
--	end
--
---- bizalmas irat és végrehajtó felhasználó nem az őrző vagy annak vezetője
--		SET @sqlcmd = @sqlcmd + ';  DELETE FROM #filter WHERE
--	#filter.Id IN
--	(
--			SELECT EREC_Csatolmanyok.Dokumentum_Id FROM EREC_Csatolmanyok
--				INNER JOIN EREC_IraIratok 
--					ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--	and dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 1
--	and EREC_IraIratok.Id not in (select Id from @iratok_executor)
--	)
--'									 
									 
									 
									 
									 
--										SELECT EREC_Csatolmanyok.[Dokumentum_Id]
--										FROM EREC_Csatolmanyok
--											INNER JOIN EREC_IraIratok 
--												ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--											INNER JOIN krt_jogosultak 
--												ON krt_jogosultak.Obj_Id = EREC_IraIratok.Id
--											INNER JOIN dbo.fn_GetCsoportTagokAll(@ExecutorUserId) as CsoportTagokAll 
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.[Dokumentum_Id]
--										FROM EREC_Csatolmanyok
--											INNER JOIN EREC_IraIratok 
--												ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id										
--											INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
--											WHERE EREC_UgyUgyiratdarabok.UgyUgyirat_Id IN 
--											(
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
--													INNER JOIN dbo.fn_GetCsoportTagokAll(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--												UNION ALL
--												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
--													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
--													INNER JOIN dbo.fn_GetCsoportTagokAll(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany																							
--											)
--										UNION ALL
--										SELECT EREC_Csatolmanyok.[Dokumentum_Id]
--										FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek
--												ON EREC_Csatolmanyok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id										
--											INNER JOIN krt_jogosultak 
--												ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
--											INNER JOIN dbo.fn_GetCsoportTagokAll(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''') as CsoportTagokAll 
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--										UNION ALL
--										SELECT EREC_Csatolmanyok.[Dokumentum_Id]
--										FROM EREC_Csatolmanyok
--											INNER JOIN EREC_KuldKuldemenyek
--												ON EREC_Csatolmanyok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id										
--											INNER JOIN krt_jogosultak 
--												ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
--											INNER JOIN dbo.fn_GetCsoportTagokAll(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''') as CsoportTagokAll 
--												ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--									 )
--									 
--									 '
	END
         
    /************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/		
		SET @sqlcmd = @sqlcmd + N'		
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Dokumentumok.Id into #result
       FROM KRT_Dokumentumok as KRT_Dokumentumok
         left join KRT_Kodcsoportok as FormatumKodCsoport on FormatumKodCsoport.Kod = ''DOKUMENTUM_FORMATUM'' and FormatumKodCsoport.Org=@Org
				left join KRT_Kodtarak as FormatumKodTarak on FormatumKodTarak.Kodcsoport_Id = FormatumKodCsoport.Id and FormatumKodTarak.Kod = KRT_Dokumentumok.[Formatum] and FormatumKodTarak.Org=@Org
	    -- BUG_2677
		left join KRT_Kodcsoportok as TipusKodCsoport on TipusKodCsoport.Kod = ''DOKUMENTUMTIPUS'' and TipusKodCsoport.Org=@Org 
				left join KRT_Kodtarak as TipusKodTarak on TipusKodTarak.Kodcsoport_Id = TipusKodCsoport.Id and TipusKodTarak.Kod = KRT_Dokumentumok.[Tipus]  collate Hungarian_CS_AS and TipusKodTarak.Org=@Org 
		left join KRT_Partner_Dokumentumok on KRT_Dokumentumok.Id = KRT_Partner_Dokumentumok.Dokumentum_Id
		left join KRT_Partnerek on KRT_Partner_Dokumentumok.Partner_Id = KRT_Partnerek.Id  
	   WHERE KRT_Dokumentumok.Id in (select Id from #filter); '
		
		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = @SelectedRowId)
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END'
		ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ; '


     
          
  SET @sqlcmd = @sqlcmd +
  ' select
	#result.RowNumber,
  	   KRT_Dokumentumok.Id,
	   KRT_Dokumentumok.Org,
	   KRT_Dokumentumok.FajlNev,
	   KRT_Dokumentumok.VerzioJel,
	   KRT_Dokumentumok.Nev,
	   KRT_Dokumentumok.Tipus,
	   -- BUG_2677
		dbo.fn_KodtarErtekNeve(''DOKUMENTUMTIPUS'', KRT_Dokumentumok.Tipus,@Org) as Tipus_Nev,
	   KRT_Dokumentumok.Formatum,
	 dbo.fn_KodtarErtekNeve(''DOKUMENTUM_FORMATUM'', KRT_Dokumentumok.Formatum,@Org) as Formatum_Nev,
	   KRT_Dokumentumok.Leiras,
	   KRT_Dokumentumok.Meret,
	   KRT_Dokumentumok.TartalomHash,
	   KRT_Dokumentumok.KivonatHash,
	   KRT_Dokumentumok.Dokumentum_Id,
	   KRT_Dokumentumok.Dokumentum_Id_Kovetkezo,
	   KRT_Dokumentumok.Alkalmazas_Id,
	   KRT_Dokumentumok.Csoport_Id_Tulaj,
	   KRT_Dokumentumok.KulsoAzonositok,
	   KRT_Dokumentumok.External_Source,
	   KRT_Dokumentumok.External_Link,
	   KRT_Dokumentumok.External_Id,
	   KRT_Dokumentumok.External_Info,
	   KRT_Dokumentumok.CheckedOut,
	   KRT_Dokumentumok.CheckedOutTime,
	   KRT_Dokumentumok.BarCode,
	   KRT_Dokumentumok.Allapot,
	   KRT_Dokumentumok.WorkFlowAllapot,
	   KRT_Dokumentumok.Megnyithato,
	   Megnyithato_Nev = 
	CASE KRT_Dokumentumok.Megnyithato
		WHEN ''1'' THEN ''igen''
		ELSE ''nem''
	END,
	   KRT_Dokumentumok.Olvashato,
	Olvashato_Nev = 
	CASE KRT_Dokumentumok.Olvashato
		WHEN ''1'' THEN ''igen''
		ELSE ''nem''
	END,
	   KRT_Dokumentumok.ElektronikusAlairas,
	   KRT_Dokumentumok.AlairasFelulvizsgalat,
	   KRT_Dokumentumok.Titkositas,
	   KRT_Dokumentumok.SablonAzonosito,
  (SELECT COUNT(*) FROM [EREC_Csatolmanyok] 
   WHERE [EREC_Csatolmanyok].[Dokumentum_Id] = [KRT_Dokumentumok].[Id]) AS CsatolmanyCount,
	EREC_IraIratok.Azonosito as IktatoSzam_Merge,
	EREC_IraIratok.Targy as Targy1,
	dbo.fn_KodtarErtekNeve(''IRAT_ALLAPOT'', EREC_IraIratok.Allapot,@Org) as Allapot_Nev,
	EREC_Csatolmanyok.KapcsolatJelleg,
	(select top 1 KRT_Partnerek.Nev 
	from KRT_Partner_Dokumentumok join KRT_Partnerek on KRT_Partner_Dokumentumok.Partner_id = KRT_Partnerek.Id
	where KRT_Partner_Dokumentumok.Dokumentum_Id = KRT_Dokumentumok.Id) as PartnerNev,
	   KRT_Dokumentumok.Ver,
	   KRT_Dokumentumok.Note,
	   KRT_Dokumentumok.Stat_id,
	   KRT_Dokumentumok.ErvKezd,
	   KRT_Dokumentumok.ErvVege,
	   KRT_Dokumentumok.Letrehozo_id,
	   KRT_Dokumentumok.LetrehozasIdo,
	   KRT_Dokumentumok.Modosito_id,
	   KRT_Dokumentumok.ModositasIdo,
	   KRT_Dokumentumok.Zarolo_Id,
	   KRT_Dokumentumok.ZarolasIdo,
	   KRT_Dokumentumok.Tranz_Id,
	   KRT_Dokumentumok.UIAccessLog_Id,  
	   KRT_Dokumentumok.AlairasFelulvizsgalatEredmeny
   FROM 
     KRT_Dokumentumok as KRT_Dokumentumok   
		inner join #result on #result.Id = KRT_Dokumentumok.Id 
		left join EREC_Csatolmanyok as EREC_Csatolmanyok
			on EREC_Csatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id and getdate() between EREC_Csatolmanyok.ErvKezd and EREC_Csatolmanyok.ErvVege
		left join EREC_IraIratok as EREC_IraIratok
			on EREC_IraIratok.Id = EREC_Csatolmanyok.IraIrat_Id
		LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok
			ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id		
   WHERE RowNumber between @firstRow and @lastRow
   ORDER BY #result.RowNumber; '
   
   
   -- találatok száma és oldalszám
   set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

   execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber INT, @SelectedRowId UNIQUEIDENTIFIER, @ExecutorUserId UNIQUEIDENTIFIER, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier',
		@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id=@FelhasznaloSzervezet_Id, @Org=@Org;
   

END TRY
BEGIN CATCH
	
	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH

end