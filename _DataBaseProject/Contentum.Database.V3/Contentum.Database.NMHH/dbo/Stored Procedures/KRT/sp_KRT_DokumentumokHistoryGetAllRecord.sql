﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_DokumentumokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_DokumentumokHistoryGetAllRecord
go
*/
create procedure sp_KRT_DokumentumokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_DokumentumokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FajlNev' as ColumnName,               cast(Old.FajlNev as nvarchar(99)) as OldValue,
               cast(New.FajlNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FajlNev as nvarchar(max)),'') != ISNULL(CAST(New.FajlNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VerzioJel' as ColumnName,               cast(Old.VerzioJel as nvarchar(99)) as OldValue,
               cast(New.VerzioJel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VerzioJel as nvarchar(max)),'') != ISNULL(CAST(New.VerzioJel as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Formatum' as ColumnName,               cast(Old.Formatum as nvarchar(99)) as OldValue,
               cast(New.Formatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Formatum as nvarchar(max)),'') != ISNULL(CAST(New.Formatum as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Meret' as ColumnName,               cast(Old.Meret as nvarchar(99)) as OldValue,
               cast(New.Meret as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Meret as nvarchar(max)),'') != ISNULL(CAST(New.Meret as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TartalomHash' as ColumnName,               cast(Old.TartalomHash as nvarchar(99)) as OldValue,
               cast(New.TartalomHash as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TartalomHash as nvarchar(max)),'') != ISNULL(CAST(New.TartalomHash as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairtTartalomHash' as ColumnName,               cast(Old.AlairtTartalomHash as nvarchar(99)) as OldValue,
               cast(New.AlairtTartalomHash as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairtTartalomHash as nvarchar(max)),'') != ISNULL(CAST(New.AlairtTartalomHash as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KivonatHash' as ColumnName,               cast(Old.KivonatHash as nvarchar(99)) as OldValue,
               cast(New.KivonatHash as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KivonatHash as nvarchar(max)),'') != ISNULL(CAST(New.KivonatHash as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,               cast(Old.Dokumentum_Id as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id_Kovetkezo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(Old.Dokumentum_Id_Kovetkezo) as OldValue,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(New.Dokumentum_Id_Kovetkezo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id_Kovetkezo as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id_Kovetkezo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Dokumentumok FTOld on FTOld.Id = Old.Dokumentum_Id_Kovetkezo --and FTOld.Ver = Old.Ver
         left join KRT_Dokumentumok FTNew on FTNew.Id = New.Dokumentum_Id_Kovetkezo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alkalmazas_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_AlkalmazasokAzonosito(Old.Alkalmazas_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_AlkalmazasokAzonosito(New.Alkalmazas_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Alkalmazas_Id as nvarchar(max)),'') != ISNULL(CAST(New.Alkalmazas_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Alkalmazasok FTOld on FTOld.Id = Old.Alkalmazas_Id --and FTOld.Ver = Old.Ver
         left join KRT_Alkalmazasok FTNew on FTNew.Id = New.Alkalmazas_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Tulaj' as ColumnName,               cast(Old.Csoport_Id_Tulaj as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Tulaj as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Tulaj as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Tulaj as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulsoAzonositok' as ColumnName,               cast(Old.KulsoAzonositok as nvarchar(99)) as OldValue,
               cast(New.KulsoAzonositok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KulsoAzonositok as nvarchar(max)),'') != ISNULL(CAST(New.KulsoAzonositok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Source' as ColumnName,               cast(Old.External_Source as nvarchar(99)) as OldValue,
               cast(New.External_Source as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Source as nvarchar(max)),'') != ISNULL(CAST(New.External_Source as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Link' as ColumnName,               cast(Old.External_Link as nvarchar(99)) as OldValue,
               cast(New.External_Link as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Link as nvarchar(max)),'') != ISNULL(CAST(New.External_Link as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Id' as ColumnName,               cast(Old.External_Id as nvarchar(99)) as OldValue,
               cast(New.External_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Id as nvarchar(max)),'') != ISNULL(CAST(New.External_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Info' as ColumnName,               cast(Old.External_Info as nvarchar(99)) as OldValue,
               cast(New.External_Info as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Info as nvarchar(max)),'') != ISNULL(CAST(New.External_Info as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CheckedOut' as ColumnName,               cast(Old.CheckedOut as nvarchar(99)) as OldValue,
               cast(New.CheckedOut as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CheckedOut as nvarchar(max)),'') != ISNULL(CAST(New.CheckedOut as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CheckedOutTime' as ColumnName,               cast(Old.CheckedOutTime as nvarchar(99)) as OldValue,
               cast(New.CheckedOutTime as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CheckedOutTime as nvarchar(max)),'') != ISNULL(CAST(New.CheckedOutTime as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'OCRPrioritas' as ColumnName,               cast(Old.OCRPrioritas as nvarchar(99)) as OldValue,
               cast(New.OCRPrioritas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.OCRPrioritas as nvarchar(max)),'') != ISNULL(CAST(New.OCRPrioritas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'OCRAllapot' as ColumnName,               cast(Old.OCRAllapot as nvarchar(99)) as OldValue,
               cast(New.OCRAllapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.OCRAllapot as nvarchar(max)),'') != ISNULL(CAST(New.OCRAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WorkFlowAllapot' as ColumnName,               cast(Old.WorkFlowAllapot as nvarchar(99)) as OldValue,
               cast(New.WorkFlowAllapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.WorkFlowAllapot as nvarchar(max)),'') != ISNULL(CAST(New.WorkFlowAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megnyithato' as ColumnName,               cast(Old.Megnyithato as nvarchar(99)) as OldValue,
               cast(New.Megnyithato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Megnyithato as nvarchar(max)),'') != ISNULL(CAST(New.Megnyithato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Olvashato' as ColumnName,               cast(Old.Olvashato as nvarchar(99)) as OldValue,
               cast(New.Olvashato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Olvashato as nvarchar(max)),'') != ISNULL(CAST(New.Olvashato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElektronikusAlairas' as ColumnName,               cast(Old.ElektronikusAlairas as nvarchar(99)) as OldValue,
               cast(New.ElektronikusAlairas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElektronikusAlairas as nvarchar(max)),'') != ISNULL(CAST(New.ElektronikusAlairas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasFelulvizsgalat' as ColumnName,               cast(Old.AlairasFelulvizsgalat as nvarchar(99)) as OldValue,
               cast(New.AlairasFelulvizsgalat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairasFelulvizsgalat as nvarchar(max)),'') != ISNULL(CAST(New.AlairasFelulvizsgalat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Titkositas' as ColumnName,               cast(Old.Titkositas as nvarchar(99)) as OldValue,
               cast(New.Titkositas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Titkositas as nvarchar(max)),'') != ISNULL(CAST(New.Titkositas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SablonAzonosito' as ColumnName,               cast(Old.SablonAzonosito as nvarchar(99)) as OldValue,
               cast(New.SablonAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumokHistory Old
         inner join KRT_DokumentumokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_DokumentumokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SablonAzonosito as nvarchar(max)),'') != ISNULL(CAST(New.SablonAzonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go