﻿
CREATE procedure [dbo].[sp_KRT_DokumentumokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @FajlNev     Nvarchar(400)  = null,         
             @VerzioJel     Nvarchar(100)  = null,         
             @Nev     Nvarchar(100)  = null,         
             @Tipus     Nvarchar(100)  = null,         
             @Formatum     nvarchar(64)  = null,         
             @Leiras     Nvarchar(400)  = null,         
             @Meret     int  = null,         
             @TartalomHash     Nvarchar(100)  = null,         
             @AlairtTartalomHash     varbinary(4000)  = null,         
             @KivonatHash     Nvarchar(100)  = null,         
             @Dokumentum_Id     uniqueidentifier  = null,         
             @Dokumentum_Id_Kovetkezo     uniqueidentifier  = null,         
             @Alkalmazas_Id     uniqueidentifier  = null,         
             @Csoport_Id_Tulaj     uniqueidentifier  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @External_Source     Nvarchar(100)  = null,         
             @External_Link     Nvarchar(400)  = null,         
             @External_Id     uniqueidentifier  = null,         
             @External_Info     Nvarchar(4000)  = null,         
             @CheckedOut     uniqueidentifier  = null,         
             @CheckedOutTime     datetime  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @OCRPrioritas     int  = null,         
             @OCRAllapot     char(1)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @WorkFlowAllapot     nvarchar(64)  = null,         
             @Megnyithato     char(1)  = null,         
             @Olvashato     char(1)  = null,         
             @ElektronikusAlairas     nvarchar(64)  = null,         
             @AlairasFelulvizsgalat     datetime  = null,         
             @Titkositas     char(1)  = null,         
             @SablonAzonosito     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_Id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_Id     uniqueidentifier  = null,         
             @UIAccessLog_Id     uniqueidentifier  = null,         
             @AlairasFelulvizsgalatEredmeny     Nvarchar(4000)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_FajlNev     Nvarchar(400)         
     DECLARE @Act_VerzioJel     Nvarchar(100)         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_Tipus     Nvarchar(100)         
     DECLARE @Act_Formatum     nvarchar(64)         
     DECLARE @Act_Leiras     Nvarchar(400)         
     DECLARE @Act_Meret     int         
     DECLARE @Act_TartalomHash     Nvarchar(100)         
     DECLARE @Act_AlairtTartalomHash     varbinary(4000)         
     DECLARE @Act_KivonatHash     Nvarchar(100)         
     DECLARE @Act_Dokumentum_Id     uniqueidentifier         
     DECLARE @Act_Dokumentum_Id_Kovetkezo     uniqueidentifier         
     DECLARE @Act_Alkalmazas_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Tulaj     uniqueidentifier         
     DECLARE @Act_KulsoAzonositok     Nvarchar(100)         
     DECLARE @Act_External_Source     Nvarchar(100)         
     DECLARE @Act_External_Link     Nvarchar(400)         
     DECLARE @Act_External_Id     uniqueidentifier         
     DECLARE @Act_External_Info     Nvarchar(4000)         
     DECLARE @Act_CheckedOut     uniqueidentifier         
     DECLARE @Act_CheckedOutTime     datetime         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_OCRPrioritas     int         
     DECLARE @Act_OCRAllapot     char(1)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_WorkFlowAllapot     nvarchar(64)         
     DECLARE @Act_Megnyithato     char(1)         
     DECLARE @Act_Olvashato     char(1)         
     DECLARE @Act_ElektronikusAlairas     nvarchar(64)         
     DECLARE @Act_AlairasFelulvizsgalat     datetime         
     DECLARE @Act_Titkositas     char(1)         
     DECLARE @Act_SablonAzonosito     Nvarchar(100)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_Id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_Id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_Id     uniqueidentifier  
	 DECLARE @ACT_AlairasFelulvizsgalatEredmeny		Nvarchar(4000)            
  
set nocount on

select 
     @Act_Org = Org,
     @Act_FajlNev = FajlNev,
     @Act_VerzioJel = VerzioJel,
     @Act_Nev = Nev,
     @Act_Tipus = Tipus,
     @Act_Formatum = Formatum,
     @Act_Leiras = Leiras,
     @Act_Meret = Meret,
     @Act_TartalomHash = TartalomHash,
     @Act_AlairtTartalomHash = AlairtTartalomHash,
     @Act_KivonatHash = KivonatHash,
     @Act_Dokumentum_Id = Dokumentum_Id,
     @Act_Dokumentum_Id_Kovetkezo = Dokumentum_Id_Kovetkezo,
     @Act_Alkalmazas_Id = Alkalmazas_Id,
     @Act_Csoport_Id_Tulaj = Csoport_Id_Tulaj,
     @Act_KulsoAzonositok = KulsoAzonositok,
     @Act_External_Source = External_Source,
     @Act_External_Link = External_Link,
     @Act_External_Id = External_Id,
     @Act_External_Info = External_Info,
     @Act_CheckedOut = CheckedOut,
     @Act_CheckedOutTime = CheckedOutTime,
     @Act_BarCode = BarCode,
     @Act_OCRPrioritas = OCRPrioritas,
     @Act_OCRAllapot = OCRAllapot,
     @Act_Allapot = Allapot,
     @Act_WorkFlowAllapot = WorkFlowAllapot,
     @Act_Megnyithato = Megnyithato,
     @Act_Olvashato = Olvashato,
     @Act_ElektronikusAlairas = ElektronikusAlairas,
     @Act_AlairasFelulvizsgalat = AlairasFelulvizsgalat,
     @Act_Titkositas = Titkositas,
     @Act_SablonAzonosito = SablonAzonosito,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_Id = Zarolo_Id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_Id = Tranz_Id,
     @Act_UIAccessLog_Id = UIAccessLog_Id,
	 @ACT_AlairasFelulvizsgalatEredmeny = AlairasFelulvizsgalatEredmeny
from KRT_Dokumentumok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/FajlNev')=1
         SET @Act_FajlNev = @FajlNev
   IF @UpdatedColumns.exist('/root/VerzioJel')=1
         SET @Act_VerzioJel = @VerzioJel
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Formatum')=1
         SET @Act_Formatum = @Formatum
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/Meret')=1
         SET @Act_Meret = @Meret
   IF @UpdatedColumns.exist('/root/TartalomHash')=1
         SET @Act_TartalomHash = @TartalomHash
   IF @UpdatedColumns.exist('/root/AlairtTartalomHash')=1
         SET @Act_AlairtTartalomHash = @AlairtTartalomHash
   IF @UpdatedColumns.exist('/root/KivonatHash')=1
         SET @Act_KivonatHash = @KivonatHash
   IF @UpdatedColumns.exist('/root/Dokumentum_Id')=1
         SET @Act_Dokumentum_Id = @Dokumentum_Id
   IF @UpdatedColumns.exist('/root/Dokumentum_Id_Kovetkezo')=1
         SET @Act_Dokumentum_Id_Kovetkezo = @Dokumentum_Id_Kovetkezo
   IF @UpdatedColumns.exist('/root/Alkalmazas_Id')=1
         SET @Act_Alkalmazas_Id = @Alkalmazas_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id_Tulaj')=1
         SET @Act_Csoport_Id_Tulaj = @Csoport_Id_Tulaj
   IF @UpdatedColumns.exist('/root/KulsoAzonositok')=1
         SET @Act_KulsoAzonositok = @KulsoAzonositok
   IF @UpdatedColumns.exist('/root/External_Source')=1
         SET @Act_External_Source = @External_Source
   IF @UpdatedColumns.exist('/root/External_Link')=1
         SET @Act_External_Link = @External_Link
   IF @UpdatedColumns.exist('/root/External_Id')=1
         SET @Act_External_Id = @External_Id
   IF @UpdatedColumns.exist('/root/External_Info')=1
         SET @Act_External_Info = @External_Info
   IF @UpdatedColumns.exist('/root/CheckedOut')=1
         SET @Act_CheckedOut = @CheckedOut
   IF @UpdatedColumns.exist('/root/CheckedOutTime')=1
         SET @Act_CheckedOutTime = @CheckedOutTime
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/OCRPrioritas')=1
         SET @Act_OCRPrioritas = @OCRPrioritas
   IF @UpdatedColumns.exist('/root/OCRAllapot')=1
         SET @Act_OCRAllapot = @OCRAllapot
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/WorkFlowAllapot')=1
         SET @Act_WorkFlowAllapot = @WorkFlowAllapot
   IF @UpdatedColumns.exist('/root/Megnyithato')=1
         SET @Act_Megnyithato = @Megnyithato
   IF @UpdatedColumns.exist('/root/Olvashato')=1
         SET @Act_Olvashato = @Olvashato
   IF @UpdatedColumns.exist('/root/ElektronikusAlairas')=1
         SET @Act_ElektronikusAlairas = @ElektronikusAlairas
   IF @UpdatedColumns.exist('/root/AlairasFelulvizsgalat')=1
         SET @Act_AlairasFelulvizsgalat = @AlairasFelulvizsgalat
   IF @UpdatedColumns.exist('/root/Titkositas')=1
         SET @Act_Titkositas = @Titkositas
   IF @UpdatedColumns.exist('/root/SablonAzonosito')=1
         SET @Act_SablonAzonosito = @SablonAzonosito
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_Id')=1
         SET @Act_Zarolo_Id = @Zarolo_Id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_Id')=1
         SET @Act_Tranz_Id = @Tranz_Id
   IF @UpdatedColumns.exist('/root/UIAccessLog_Id')=1
         SET @Act_UIAccessLog_Id = @UIAccessLog_Id   
   IF @UpdatedColumns.exist('/root/AlairasFelulvizsgalatEredmeny')=1
         SET @Act_AlairasFelulvizsgalatEredmeny = @AlairasFelulvizsgalatEredmeny   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Dokumentumok
SET
     Org = @Act_Org,
     FajlNev = @Act_FajlNev,
     VerzioJel = @Act_VerzioJel,
     Nev = @Act_Nev,
     Tipus = @Act_Tipus,
     Formatum = @Act_Formatum,
     Leiras = @Act_Leiras,
     Meret = @Act_Meret,
     TartalomHash = @Act_TartalomHash,
     AlairtTartalomHash = @Act_AlairtTartalomHash,
     KivonatHash = @Act_KivonatHash,
     Dokumentum_Id = @Act_Dokumentum_Id,
     Dokumentum_Id_Kovetkezo = @Act_Dokumentum_Id_Kovetkezo,
     Alkalmazas_Id = @Act_Alkalmazas_Id,
     Csoport_Id_Tulaj = @Act_Csoport_Id_Tulaj,
     KulsoAzonositok = @Act_KulsoAzonositok,
     External_Source = @Act_External_Source,
     External_Link = @Act_External_Link,
     External_Id = @Act_External_Id,
     External_Info = @Act_External_Info,
     CheckedOut = @Act_CheckedOut,
     CheckedOutTime = @Act_CheckedOutTime,
     BarCode = @Act_BarCode,
     OCRPrioritas = @Act_OCRPrioritas,
     OCRAllapot = @Act_OCRAllapot,
     Allapot = @Act_Allapot,
     WorkFlowAllapot = @Act_WorkFlowAllapot,
     Megnyithato = @Act_Megnyithato,
     Olvashato = @Act_Olvashato,
     ElektronikusAlairas = @Act_ElektronikusAlairas,
     AlairasFelulvizsgalat = @Act_AlairasFelulvizsgalat,
     Titkositas = @Act_Titkositas,
     SablonAzonosito = @Act_SablonAzonosito,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_Id = @Act_Zarolo_Id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_Id = @Act_Tranz_Id,
     UIAccessLog_Id = @Act_UIAccessLog_Id,
	 AlairasFelulvizsgalatEredmeny = @Act_AlairasFelulvizsgalatEredmeny
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Dokumentumok',@Id
					,'KRT_DokumentumokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH