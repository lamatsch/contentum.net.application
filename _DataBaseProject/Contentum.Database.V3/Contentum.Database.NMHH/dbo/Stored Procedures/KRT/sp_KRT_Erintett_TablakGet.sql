﻿create procedure [dbo].[sp_KRT_Erintett_TablakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Erintett_Tablak.Id,
	   KRT_Erintett_Tablak.Tranzakci_id,
	   KRT_Erintett_Tablak.ObjTip_Id,
	   KRT_Erintett_Tablak.Ver,
	   KRT_Erintett_Tablak.Note,
	   KRT_Erintett_Tablak.Stat_id,
	   KRT_Erintett_Tablak.ErvKezd,
	   KRT_Erintett_Tablak.ErvVege,
	   KRT_Erintett_Tablak.Letrehozo_id,
	   KRT_Erintett_Tablak.LetrehozasIdo,
	   KRT_Erintett_Tablak.Modosito_id,
	   KRT_Erintett_Tablak.ModositasIdo,
	   KRT_Erintett_Tablak.Zarolo_id,
	   KRT_Erintett_Tablak.ZarolasIdo,
	   KRT_Erintett_Tablak.Tranz_id,
	   KRT_Erintett_Tablak.UIAccessLog_id
	   from 
		 KRT_Erintett_Tablak as KRT_Erintett_Tablak 
	   where
		 KRT_Erintett_Tablak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end