﻿create procedure [dbo].[sp_KRT_Erintett_TablakGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Erintett_Tablak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Erintett_Tablak.Id,
	   KRT_Erintett_Tablak.Tranzakci_id,
	   KRT_Erintett_Tablak.ObjTip_Id,
	   KRT_Erintett_Tablak.Ver,
	   KRT_Erintett_Tablak.Note,
	   KRT_Erintett_Tablak.Stat_id,
	   KRT_Erintett_Tablak.ErvKezd,
	   KRT_Erintett_Tablak.ErvVege,
	   KRT_Erintett_Tablak.Letrehozo_id,
	   KRT_Erintett_Tablak.LetrehozasIdo,
	   KRT_Erintett_Tablak.Modosito_id,
	   KRT_Erintett_Tablak.ModositasIdo,
	   KRT_Erintett_Tablak.Zarolo_id,
	   KRT_Erintett_Tablak.ZarolasIdo,
	   KRT_Erintett_Tablak.Tranz_id,
	   KRT_Erintett_Tablak.UIAccessLog_id  
   from 
     KRT_Erintett_Tablak as KRT_Erintett_Tablak      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end