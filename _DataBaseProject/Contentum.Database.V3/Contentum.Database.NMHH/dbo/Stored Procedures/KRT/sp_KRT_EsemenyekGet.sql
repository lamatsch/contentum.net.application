
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_EsemenyekGet')
            and   type = 'P')
   drop procedure sp_KRT_EsemenyekGet
go
*/
create procedure sp_KRT_EsemenyekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Esemenyek.Id,
	   KRT_Esemenyek.Obj_Id,
	   KRT_Esemenyek.ObjTip_Id,
	   KRT_Esemenyek.Azonositoja,
	   KRT_Esemenyek.Felhasznalo_Id_User,
	   KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze,
	   KRT_Esemenyek.Felhasznalo_Id_Login,
	   KRT_Esemenyek.Csoport_Id_Cel,
	   KRT_Esemenyek.Helyettesites_Id,
	   KRT_Esemenyek.Tranzakcio_Id,
	   KRT_Esemenyek.Funkcio_Id,
	   KRT_Esemenyek.CsoportHierarchia,
	   KRT_Esemenyek.KeresesiFeltetel,
	   KRT_Esemenyek.TalalatokSzama,
	   KRT_Esemenyek.Ver,
	   KRT_Esemenyek.Note,
	   KRT_Esemenyek.Stat_id,
	   KRT_Esemenyek.ErvKezd,
	   KRT_Esemenyek.ErvVege,
	   KRT_Esemenyek.Letrehozo_id,
	   KRT_Esemenyek.LetrehozasIdo,
	   KRT_Esemenyek.Modosito_id,
	   KRT_Esemenyek.ModositasIdo,
	   KRT_Esemenyek.Zarolo_id,
	   KRT_Esemenyek.ZarolasIdo,
	   KRT_Esemenyek.Tranz_id,
	   KRT_Esemenyek.UIAccessLog_id,
	   KRT_Esemenyek.Munkaallomas,
	   KRT_Esemenyek.MuveletKimenete,
	   KRT_Esemenyek.CheckSum
	   from 
		 KRT_Esemenyek as KRT_Esemenyek 
	   where
		 KRT_Esemenyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
