/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_EsemenyekGetAll')
            and   type = 'P')
   drop procedure sp_KRT_EsemenyekGetAll
go
*/
create procedure sp_KRT_EsemenyekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Esemenyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_Esemenyek.Id,
	   KRT_Esemenyek.Obj_Id,
	   KRT_Esemenyek.ObjTip_Id,
	   KRT_Esemenyek.Azonositoja,
	   KRT_Esemenyek.Felhasznalo_Id_User,
	   KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze,
	   KRT_Esemenyek.Felhasznalo_Id_Login,
	   KRT_Esemenyek.Csoport_Id_Cel,
	   KRT_Esemenyek.Helyettesites_Id,
	   KRT_Esemenyek.Tranzakcio_Id,
	   KRT_Esemenyek.Funkcio_Id,
	   KRT_Esemenyek.CsoportHierarchia,
	   KRT_Esemenyek.KeresesiFeltetel,
	   KRT_Esemenyek.TalalatokSzama,
	   KRT_Esemenyek.Ver,
	   KRT_Esemenyek.Note,
	   KRT_Esemenyek.Stat_id,
	   KRT_Esemenyek.ErvKezd,
	   KRT_Esemenyek.ErvVege,
	   KRT_Esemenyek.Letrehozo_id,
	   KRT_Esemenyek.LetrehozasIdo,
	   KRT_Esemenyek.Modosito_id,
	   KRT_Esemenyek.ModositasIdo,
	   KRT_Esemenyek.Zarolo_id,
	   KRT_Esemenyek.ZarolasIdo,
	   KRT_Esemenyek.Tranz_id,
	   KRT_Esemenyek.UIAccessLog_id,
	   KRT_Esemenyek.Munkaallomas,
	   KRT_Esemenyek.MuveletKimenete,
	   KRT_Esemenyek.CheckSum  
   from 
     KRT_Esemenyek as KRT_Esemenyek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go