﻿create procedure [dbo].[sp_KRT_EsemenyekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_EsemenyekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               
               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               
               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.ObjTip_Id != New.ObjTip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonositoja' as ColumnName,               
               cast(Old.Azonositoja as nvarchar(99)) as OldValue,
               cast(New.Azonositoja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Azonositoja != New.Azonositoja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_User' as ColumnName,               
               cast(Old.Felhasznalo_Id_User as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_User as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_User != New.Felhasznalo_Id_User 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_FelelosUserSzerveze' as ColumnName,               
               cast(Old.Csoport_Id_FelelosUserSzerveze as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_FelelosUserSzerveze as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Csoport_Id_FelelosUserSzerveze != New.Csoport_Id_FelelosUserSzerveze 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_Login' as ColumnName,               
               cast(Old.Felhasznalo_Id_Login as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_Login as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_Login != New.Felhasznalo_Id_Login 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Cel' as ColumnName,               
               cast(Old.Csoport_Id_Cel as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Cel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Csoport_Id_Cel != New.Csoport_Id_Cel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tranzakcio_Id' as ColumnName,               
               cast(Old.Tranzakcio_Id as nvarchar(99)) as OldValue,
               cast(New.Tranzakcio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Tranzakcio_Id != New.Tranzakcio_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id' as ColumnName,               
               cast(Old.Funkcio_Id as nvarchar(99)) as OldValue,
               cast(New.Funkcio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Funkcio_Id != New.Funkcio_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CsoportHierarchia' as ColumnName,               
               cast(Old.CsoportHierarchia as nvarchar(99)) as OldValue,
               cast(New.CsoportHierarchia as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.CsoportHierarchia != New.CsoportHierarchia 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KeresesiFeltetel' as ColumnName,               
               cast(Old.KeresesiFeltetel as nvarchar(99)) as OldValue,
               cast(New.KeresesiFeltetel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.KeresesiFeltetel != New.KeresesiFeltetel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TalalatokSzama' as ColumnName,               
               cast(Old.TalalatokSzama as nvarchar(99)) as OldValue,
               cast(New.TalalatokSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_EsemenyekHistory Old
         inner join KRT_EsemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_EsemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.TalalatokSzama != New.TalalatokSzama 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
end