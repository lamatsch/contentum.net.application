
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_EsemenyekUpdate')
            and   type = 'P')
   drop procedure sp_KRT_EsemenyekUpdate
go
*/
create procedure sp_KRT_EsemenyekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @Azonositoja     Nvarchar(400)  = null,         
             @Felhasznalo_Id_User     uniqueidentifier  = null,         
             @Csoport_Id_FelelosUserSzerveze     uniqueidentifier  = null,         
             @Felhasznalo_Id_Login     uniqueidentifier  = null,         
             @Csoport_Id_Cel     uniqueidentifier  = null,         
             @Helyettesites_Id     uniqueidentifier  = null,         
             @Tranzakcio_Id     uniqueidentifier  = null,         
             @Funkcio_Id     uniqueidentifier  = null,         
             @CsoportHierarchia     Nvarchar(4000)  = null,         
             @KeresesiFeltetel     Nvarchar(4000)  = null,         
             @TalalatokSzama     int  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @Munkaallomas     Nvarchar(100)  = null,         
             @MuveletKimenete     Nvarchar(100)  = null,         
             @CheckSum     Nvarchar(4000)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_Azonositoja     Nvarchar(400)         
     DECLARE @Act_Felhasznalo_Id_User     uniqueidentifier         
     DECLARE @Act_Csoport_Id_FelelosUserSzerveze     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_Login     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Cel     uniqueidentifier         
     DECLARE @Act_Helyettesites_Id     uniqueidentifier         
     DECLARE @Act_Tranzakcio_Id     uniqueidentifier         
     DECLARE @Act_Funkcio_Id     uniqueidentifier         
     DECLARE @Act_CsoportHierarchia     Nvarchar(4000)         
     DECLARE @Act_KeresesiFeltetel     Nvarchar(4000)         
     DECLARE @Act_TalalatokSzama     int         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_Munkaallomas     Nvarchar(100)         
     DECLARE @Act_MuveletKimenete     Nvarchar(100)         
     DECLARE @Act_CheckSum     Nvarchar(4000)           
  
set nocount on

select 
     @Act_Obj_Id = Obj_Id,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_Azonositoja = Azonositoja,
     @Act_Felhasznalo_Id_User = Felhasznalo_Id_User,
     @Act_Csoport_Id_FelelosUserSzerveze = Csoport_Id_FelelosUserSzerveze,
     @Act_Felhasznalo_Id_Login = Felhasznalo_Id_Login,
     @Act_Csoport_Id_Cel = Csoport_Id_Cel,
     @Act_Helyettesites_Id = Helyettesites_Id,
     @Act_Tranzakcio_Id = Tranzakcio_Id,
     @Act_Funkcio_Id = Funkcio_Id,
     @Act_CsoportHierarchia = CsoportHierarchia,
     @Act_KeresesiFeltetel = KeresesiFeltetel,
     @Act_TalalatokSzama = TalalatokSzama,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_Munkaallomas = Munkaallomas,
     @Act_MuveletKimenete = MuveletKimenete,
     @Act_CheckSum = CheckSum
from KRT_Esemenyek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/Azonositoja')=1
         SET @Act_Azonositoja = @Azonositoja
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_User')=1
         SET @Act_Felhasznalo_Id_User = @Felhasznalo_Id_User
   IF @UpdatedColumns.exist('/root/Csoport_Id_FelelosUserSzerveze')=1
         SET @Act_Csoport_Id_FelelosUserSzerveze = @Csoport_Id_FelelosUserSzerveze
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_Login')=1
         SET @Act_Felhasznalo_Id_Login = @Felhasznalo_Id_Login
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cel')=1
         SET @Act_Csoport_Id_Cel = @Csoport_Id_Cel
   IF @UpdatedColumns.exist('/root/Helyettesites_Id')=1
         SET @Act_Helyettesites_Id = @Helyettesites_Id
   IF @UpdatedColumns.exist('/root/Tranzakcio_Id')=1
         SET @Act_Tranzakcio_Id = @Tranzakcio_Id
   IF @UpdatedColumns.exist('/root/Funkcio_Id')=1
         SET @Act_Funkcio_Id = @Funkcio_Id
   IF @UpdatedColumns.exist('/root/CsoportHierarchia')=1
         SET @Act_CsoportHierarchia = @CsoportHierarchia
   IF @UpdatedColumns.exist('/root/KeresesiFeltetel')=1
         SET @Act_KeresesiFeltetel = @KeresesiFeltetel
   IF @UpdatedColumns.exist('/root/TalalatokSzama')=1
         SET @Act_TalalatokSzama = @TalalatokSzama
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/Munkaallomas')=1
         SET @Act_Munkaallomas = @Munkaallomas
   IF @UpdatedColumns.exist('/root/MuveletKimenete')=1
         SET @Act_MuveletKimenete = @MuveletKimenete
   IF @UpdatedColumns.exist('/root/CheckSum')=1
         SET @Act_CheckSum = @CheckSum   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Esemenyek
SET
     Obj_Id = @Act_Obj_Id,
     ObjTip_Id = @Act_ObjTip_Id,
     Azonositoja = @Act_Azonositoja,
     Felhasznalo_Id_User = @Act_Felhasznalo_Id_User,
     Csoport_Id_FelelosUserSzerveze = @Act_Csoport_Id_FelelosUserSzerveze,
     Felhasznalo_Id_Login = @Act_Felhasznalo_Id_Login,
     Csoport_Id_Cel = @Act_Csoport_Id_Cel,
     Helyettesites_Id = @Act_Helyettesites_Id,
     Tranzakcio_Id = @Act_Tranzakcio_Id,
     Funkcio_Id = @Act_Funkcio_Id,
     CsoportHierarchia = @Act_CsoportHierarchia,
     KeresesiFeltetel = @Act_KeresesiFeltetel,
     TalalatokSzama = @Act_TalalatokSzama,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     Munkaallomas = @Act_Munkaallomas,
     MuveletKimenete = @Act_MuveletKimenete,
     CheckSum = @Act_CheckSum
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end



--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
