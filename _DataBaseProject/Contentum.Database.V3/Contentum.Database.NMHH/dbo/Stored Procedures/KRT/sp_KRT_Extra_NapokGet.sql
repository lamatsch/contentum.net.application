﻿create procedure [dbo].[sp_KRT_Extra_NapokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Extra_Napok.Id,
	   KRT_Extra_Napok.Datum,
	   KRT_Extra_Napok.Jelzo,
	   KRT_Extra_Napok.Megjegyzes,
	   KRT_Extra_Napok.Ver,
	   KRT_Extra_Napok.Note,
	   KRT_Extra_Napok.Stat_id,
	   KRT_Extra_Napok.ErvKezd,
	   KRT_Extra_Napok.ErvVege,
	   KRT_Extra_Napok.Letrehozo_id,
	   KRT_Extra_Napok.LetrehozasIdo,
	   KRT_Extra_Napok.Modosito_id,
	   KRT_Extra_Napok.ModositasIdo,
	   KRT_Extra_Napok.Zarolo_id,
	   KRT_Extra_Napok.ZarolasIdo,
	   KRT_Extra_Napok.Tranz_id,
	   KRT_Extra_Napok.UIAccessLog_id
	   from 
		 KRT_Extra_Napok as KRT_Extra_Napok 
	   where
		 KRT_Extra_Napok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end