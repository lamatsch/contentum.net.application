﻿create procedure [dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap]
		 @DatumTol DATETIME = NULL,
		 @Sorszam INT = NULL,
         @ExecutorUserId UNIQUEIDENTIFIER = NULL

         
as

begin
BEGIN TRY

	set nocount ON
	
	SELECT dbo.[fn_kovetkezo_munkanap](@DatumTol, @Sorszam)
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end