﻿create procedure [dbo].[sp_KRT_FelhasznaloProfilokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_FelhasznaloProfilok.Id,
	   KRT_FelhasznaloProfilok.Org,
	   KRT_FelhasznaloProfilok.Tipus,
	   KRT_FelhasznaloProfilok.Felhasznalo_id,
	   KRT_FelhasznaloProfilok.Obj_Id,
	   KRT_FelhasznaloProfilok.Nev,
	   KRT_FelhasznaloProfilok.XML,
	   KRT_FelhasznaloProfilok.Ver,
	   KRT_FelhasznaloProfilok.Note,
	   KRT_FelhasznaloProfilok.Stat_id,
	   KRT_FelhasznaloProfilok.ErvKezd,
	   KRT_FelhasznaloProfilok.ErvVege,
	   KRT_FelhasznaloProfilok.Letrehozo_id,
	   KRT_FelhasznaloProfilok.LetrehozasIdo,
	   KRT_FelhasznaloProfilok.Modosito_id,
	   KRT_FelhasznaloProfilok.ModositasIdo,
	   KRT_FelhasznaloProfilok.Zarolo_id,
	   KRT_FelhasznaloProfilok.ZarolasIdo,
	   KRT_FelhasznaloProfilok.Tranz_id,
	   KRT_FelhasznaloProfilok.UIAccessLog_id
	   from 
		 KRT_FelhasznaloProfilok as KRT_FelhasznaloProfilok 
	   where
		 KRT_FelhasznaloProfilok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end