﻿create procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Felhasznalo_Szerepkor.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Felhasznalo_Szerepkor.Id,
	   KRT_Felhasznalo_Szerepkor.CsoportTag_Id,
	   KRT_Felhasznalo_Szerepkor.Csoport_Id,
	   KRT_Felhasznalo_Szerepkor.Felhasznalo_Id,
	   KRT_Felhasznalo_Szerepkor.Szerepkor_Id,
	   KRT_Felhasznalo_Szerepkor.Helyettesites_Id,
	   KRT_Felhasznalo_Szerepkor.Ver,
	   KRT_Felhasznalo_Szerepkor.Note,
	   KRT_Felhasznalo_Szerepkor.Stat_id,
	   KRT_Felhasznalo_Szerepkor.ErvKezd,
	   KRT_Felhasznalo_Szerepkor.ErvVege,
	   KRT_Felhasznalo_Szerepkor.Letrehozo_id,
	   KRT_Felhasznalo_Szerepkor.LetrehozasIdo,
	   KRT_Felhasznalo_Szerepkor.Modosito_id,
	   KRT_Felhasznalo_Szerepkor.ModositasIdo,
	   KRT_Felhasznalo_Szerepkor.Zarolo_id,
	   KRT_Felhasznalo_Szerepkor.ZarolasIdo,
	   KRT_Felhasznalo_Szerepkor.Tranz_id,
	   KRT_Felhasznalo_Szerepkor.UIAccessLog_id  
   from 
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end