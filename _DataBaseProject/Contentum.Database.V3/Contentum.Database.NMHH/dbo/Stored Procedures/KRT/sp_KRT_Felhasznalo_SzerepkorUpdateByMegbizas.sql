﻿create procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateByMegbizas]
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

   -- NULLITÁS-vizsgálat
   if @Id is NULL
      RAISERROR('[50405]',16,1)

   if @ExecutorUserId is NULL
      RAISERROR('[53503]!',16,1) -- Az @ExecutorUserId paraméter nem lehet NULL!

    declare @strNow nvarchar(20)
    set @strNow = convert(nvarchar, getdate(), 120);

    declare @sqlcmd nvarchar(MAX)
    set @sqlcmd = 'declare @MegbizasId uniqueidentifier
declare @MegbizasKezd datetime
	, @MegbizasVege datetime
	, @MegbizasErvVege datetime
	, @Megbizo uniqueidentifier
	, @Megbizott uniqueidentifier
	, @MegbizoCsoportTagId uniqueidentifier
	, @Megbizas_Ver int

select @MegbizasId=Id, @MegbizasKezd=HelyettesitesKezd, @MegbizasVege=HelyettesitesVege, @MegbizasErvVege=ErvVege,
@Megbizo=Felhasznalo_ID_helyettesitett, @MegbizoCsoportTagId=CsoportTag_ID_helyettesitett, @Megbizott=Felhasznalo_ID_helyettesito,
@Megbizas_Ver = Ver
from KRT_Helyettesitesek
where Id = ''' + convert(nvarchar(36), @Id) + ''' and HelyettesitesMod=''2''

if (@MegbizasId is NULL)
    RAISERROR(''[50101]'',16,1)

if (@MegbizasErvVege < getdate())
    RAISERROR(''[50403]'',16,1)
'

set @sqlcmd = @sqlcmd + '
declare @resultTableUpdate table (Id uniqueidentifier)
declare @resultTableInvalidate table (Id uniqueidentifier)

declare @fsz_Id uniqueidentifier
	, @Szerepkor_Id uniqueidentifier
	, @fsz_ErvKezd datetime
	, @fsz_ErvVege datetime
	, @fsz_Ver int
	, @sz_Id uniqueidentifier
	, @sz_ErvKezd datetime
	, @sz_ErvVege datetime

declare @KezdDate datetime
	, @VegeDate datetime

declare felhasznalo_szerepkor_row cursor local fast_forward for
	select fsz.Id, fsz.Szerepkor_Id, fsz.ErvKezd, fsz.ErvVege, fsz.Ver
	from KRT_Felhasznalo_Szerepkor fsz
	where fsz.Helyettesites_Id = @MegbizasId and fsz.ErvVege >= getdate();
'

set @sqlcmd = @sqlcmd + '
open felhasznalo_szerepkor_row
fetch next from felhasznalo_szerepkor_row
	into @fsz_Id, @Szerepkor_Id, @fsz_ErvKezd, @fsz_ErvVege, @fsz_Ver

while (@@Fetch_Status = 0)
begin
	select top 1 @sz_Id=Id, @sz_ErvKezd=ErvKezd, @sz_ErvVege=ErvVege
		from KRT_Felhasznalo_Szerepkor
		where Felhasznalo_Id=@Megbizo and (CsoportTag_Id=@MegbizoCsoportTagId or CsoportTag_Id is null)
			  and Helyettesites_Id is null and Szerepkor_Id=@Szerepkor_Id and getdate() < ErvVege
			  order by ErvVege DESC, CsoportTag_Id DESC

	if (@sz_Id is null)
	begin
		exec sp_KRT_Felhasznalo_SzerepkorInvalidate
		@Id=@fsz_Id,
		@ExecutorUserId='''+convert(nvarchar(36),@ExecutorUserId)+''',
		@ExecutionTime='''+@strNow+'''

		insert into @resultTableInvalidate (Id) values (@fsz_Id)
	end'
set @sqlcmd = @sqlcmd + '
	else
	begin
		set @KezdDate = @MegbizasKezd
		if (@MegbizasKezd > getdate() and @KezdDate < @sz_ErvKezd)
		begin
			set @KezdDate = @sz_ErvKezd
		end

		set @VegeDate = @Megbizasvege
		if (@VegeDate > @sz_ErvVege)
			set @VegeDate = @sz_ErvVege

		exec sp_KRT_Felhasznalo_SzerepkorUpdate
		@Id=@fsz_Id,
		@ExecutorUserId='''+convert(nvarchar(36),@ExecutorUserId)+''',
		@ExecutionTime='''+@strNow+''',
		@ErvKezd=@KezdDate,
		@ErvVege=@VegeDate,
		@Ver=@fsz_Ver,
		@UpdatedColumns=''<root><ErvKezd/><ErvVege/></root>''

		insert into @resultTableUpdate (Id) values (@fsz_Id)
	end
	fetch next from felhasznalo_szerepkor_row
		into @fsz_Id, @Szerepkor_Id, @fsz_ErvKezd, @fsz_ErvVege, @fsz_Ver
end

close felhasznalo_szerepkor_row
deallocate felhasznalo_szerepkor_row

select * from @resultTableUpdate
select * from @resultTableInvalidate
'
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
   exec (@sqlcmd)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end