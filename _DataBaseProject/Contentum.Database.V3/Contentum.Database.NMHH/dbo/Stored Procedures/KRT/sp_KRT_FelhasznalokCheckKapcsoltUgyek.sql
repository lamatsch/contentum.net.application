﻿CREATE procedure [dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek]
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier
  
as

begin

-- CR3328 : Felhasználóhoz tartozó Ügyirat, Irat és Küldeményellenörzés

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
	Declare @UgyIratok int
	Declare @Iratok int
	Declare @Iratpeldanyok int
	Declare @Kuldemenyek int
	Declare @Felhaszn_nev nvarchar(100)

	SELECT @felhaszn_nev = nev from [dbo].[KRT_Felhasznalok]
		where [Id] = @id
		and getdate() between ErvKezd and ErvVege
	
	SELECT @Ugyiratok = count(id) from [EREC_UgyUgyiratok]
		where [FelhasznaloCsoport_Id_Orzo] = @id
		AND EREC_UgyUgyiratok.Allapot NOT IN ('0','10','30','31','32','33','55','56','90')
			AND (EREC_UgyUgyiratok.TovabbitasAlattAllapot IS NULL OR
		EREC_UgyUgyiratok.TovabbitasAlattAllapot NOT IN ('0','10','30','31','32','33','55','56','90'))
		and getdate() between ErvKezd and ErvVege

	--SELECT @Iratok = count(id) from [EREC_IraIratok]
	--	where [FelhasznaloCsoport_Id_Orzo] = @id
	--	and getdate() between ErvKezd and ErvVege

	declare @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT_AND_TOVABBITAS_ALATTI nvarchar(100)
	declare @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT_AND_TOVABBITAS_ALATTI nvarchar(100)
	declare @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT nvarchar(100)
	declare @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT nvarchar(100)

	SELECT @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT_AND_TOVABBITAS_ALATTI = (CONCAT((SELECT Ertek FROM KRT_Parameterek
	WHERE Nev = 'ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT'),',50') COLLATE Hungarian_CI_AS )
	PRINT @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT

	SELECT @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT = (SELECT Ertek FROM KRT_Parameterek
	WHERE Nev = 'ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT') COLLATE Hungarian_CI_AS 
	PRINT @ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT

	SELECT @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT_AND_TOVABBITAS_ALATTI = (CONCAT((SELECT Ertek FROM KRT_Parameterek
	WHERE Nev = 'ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT'),',50') COLLATE Hungarian_CI_AS )
	PRINT @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT_AND_TOVABBITAS_ALATTI

	SELECT @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT = (SELECT Ertek FROM KRT_Parameterek
	WHERE Nev = 'ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT ') COLLATE Hungarian_CI_AS 
	PRINT @ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT

	SELECT @Iratok = count(IraIrat_Id) from [EREC_PldIratPeldanyok]
		where [FelhasznaloCsoport_Id_Orzo] = @id
		and [Allapot] COLLATE Hungarian_CI_AS IN (SELECT * FROM  fn_Split(@ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT_AND_TOVABBITAS_ALATTI, ',') )
		and ([TovabbitasAlattAllapot] IS NULL 
		OR [TovabbitasAlattAllapot]  COLLATE Hungarian_CI_AS  IN (SELECT * FROM  fn_Split(@ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT, ',') ))
		and getdate() between ErvKezd and ErvVege


	SELECT @Iratpeldanyok = count(id) from [EREC_PldIratPeldanyok]
		where [FelhasznaloCsoport_Id_Orzo] = @id
		AND Allapot NOT IN ('31','32','90')
			AND (TovabbitasAlattAllapot IS NULL OR
			TovabbitasAlattAllapot NOT IN ('31','32','90'))
		and getdate() between ErvKezd and ErvVege

	SELECT @Kuldemenyek = count(id) from [EREC_KuldKuldemenyek]
		where [FelhasznaloCsoport_Id_Orzo] = @id
		AND PostazasIranya IN ('0','1')
		AND EREC_KuldKuldemenyek.Allapot COLLATE Hungarian_CI_AS IN (SELECT * FROM  fn_Split(@ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT_AND_TOVABBITAS_ALATTI, ',') )
		AND (EREC_KuldKuldemenyek.TovabbitasAlattAllapot IS NULL OR
			EREC_KuldKuldemenyek.TovabbitasAlattAllapot COLLATE Hungarian_CI_AS  IN (SELECT * FROM  fn_Split(@ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT, ',') ))
		AND IktatastNemIgenyel = '1'
		and getdate() between ErvKezd and ErvVege


	SELECT @id as felhasznaloId, @felhaszn_nev as nev,  @Ugyiratok as ugyiratok_count, @Iratok as iratok_count, @Iratpeldanyok as iratpeldanyok_count, @Kuldemenyek as kuldemenyek_count

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
