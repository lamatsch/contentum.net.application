﻿Create procedure [dbo].[sp_KRT_FelhasznalokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Felhasznalok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow != '' and @TopRow != '0')
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* SzL+rt adatokhoz rendezAcs Acs sorszA?m AsszeA?llAđtA?sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Felhasznalok.Id into #result
		from KRT_Felhasznalok as KRT_Felhasznalok
    Where KRT_Felhasznalok.Org=@Org'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* TAcnyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Felhasznalok.Id,
	   KRT_Felhasznalok.Org,
	   KRT_Felhasznalok.Partner_id,
	   KRT_Felhasznalok.Tipus,
	   KRT_Felhasznalok.UserNev,
	   KRT_Felhasznalok.Nev,
	   KRT_Felhasznalok.Jelszo,
	   KRT_Felhasznalok.JelszoLejaratIdo,
	   KRT_Felhasznalok.System,
	   KRT_Felhasznalok.Kiszolgalo,
	   KRT_Felhasznalok.DefaultPrivatKulcs,
	   KRT_Felhasznalok.Partner_Id_Munkahely,
	   KRT_Felhasznalok.MaxMinosites,
	   KRT_Felhasznalok.EMail,
	   KRT_Felhasznalok.Engedelyezett,
	   KRT_Felhasznalok.Telefonszam,
	   KRT_Felhasznalok.Beosztas,
	   KRT_Felhasznalok.Ver,
	   KRT_Felhasznalok.Note,
	   KRT_Felhasznalok.WrongPassCnt,
	   KRT_Felhasznalok.Stat_id,
	   KRT_Felhasznalok.ErvKezd,
	   KRT_Felhasznalok.ErvVege,
	   KRT_Felhasznalok.Letrehozo_id,
	   KRT_Felhasznalok.LetrehozasIdo,
	   KRT_Felhasznalok.Modosito_id,
	   KRT_Felhasznalok.ModositasIdo,
	   KRT_Felhasznalok.ZarolasIdo,
	   KRT_Felhasznalok.Zarolo_id,
	   KRT_Felhasznalok.Tranz_id,
	   KRT_Felhasznalok.UIAccessLog_id  
   from 
     KRT_Felhasznalok as KRT_Felhasznalok      
     	inner join #result on #result.Id = KRT_Felhasznalok.Id
'
	if (@firstRow is not null and @lastRow is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + '	where RowNumber between @firstRow and @lastRow
'
	END

	set @sqlcmd = @sqlcmd + '	ORDER BY #result.RowNumber;'

	-- talA?latok szA?ma Acs oldalszA?m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;  
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end