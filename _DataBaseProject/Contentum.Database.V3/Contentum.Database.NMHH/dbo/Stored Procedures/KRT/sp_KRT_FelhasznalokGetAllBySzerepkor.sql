﻿create procedure [dbo].[sp_KRT_FelhasznalokGetAllBySzerepkor]
  @Szerepkor_Id UniqueIdentifier,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by KRT_Felhasznalok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Felhasznalo_Szerepkor.Id,
	   KRT_Felhasznalo_Szerepkor.CsoportTag_Id,
	   KRT_Felhasznalo_Szerepkor.Csoport_Id,
	   KRT_Felhasznalo_Szerepkor.Felhasznalo_Id,
	   KRT_Felhasznalo_Szerepkor.Szerepkor_Id,
	   KRT_Felhasznalo_Szerepkor.Helyettesites_Id,
	   KRT_Felhasznalok.Nev as Felhasznalo_Nev,
	   case when KRT_Felhasznalo_Szerepkor.Helyettesites_Id is null then KRT_Csoportok.Nev
           else KRT_Csoportok_helyettesitett.Nev end as Csoport_Nev,
	   KRT_Felhasznalok_helyettesitett.Nev as Felhasznalo_Nev_helyettesitett,
	   KRT_Csoportok_helyettesitett.Id as Csoport_ID_helyettesitett,
	   KRT_Csoportok_helyettesitett.Nev as Csoport_Nev_helyettesitett,
	   KRT_Felhasznalo_Szerepkor.Ver,
	   KRT_Felhasznalo_Szerepkor.Note,
	   KRT_Felhasznalo_Szerepkor.Stat_id,
	   KRT_Felhasznalo_Szerepkor.ErvKezd,
	   KRT_Felhasznalo_Szerepkor.ErvVege,
	   KRT_Felhasznalo_Szerepkor.Letrehozo_id,
	   KRT_Felhasznalo_Szerepkor.LetrehozasIdo,
	   KRT_Felhasznalo_Szerepkor.Modosito_id,
	   KRT_Felhasznalo_Szerepkor.ModositasIdo,
	   KRT_Felhasznalo_Szerepkor.Zarolo_id,
	   KRT_Felhasznalo_Szerepkor.ZarolasIdo,
	   KRT_Felhasznalo_Szerepkor.Tranz_id,
	   KRT_Felhasznalo_Szerepkor.UIAccessLog_id  
   from 
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor
	 left outer join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Felhasznalo_Szerepkor.Felhasznalo_Id = KRT_Felhasznalok.Id
	 left outer join KRT_Csoportok as KRT_Csoportok on KRT_Felhasznalo_Szerepkor.Csoport_Id = KRT_Csoportok.Id
	 left outer join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Felhasznalo_Szerepkor.Helyettesites_Id = KRT_Helyettesitesek.Id
        left outer join KRT_Felhasznalok as KRT_Felhasznalok_helyettesitett on KRT_Helyettesitesek.Felhasznalo_Id_helyettesitett = KRT_Felhasznalok_helyettesitett.Id
		left outer join KRT_CsoportTagok as KRT_CsoportTagok on KRT_Helyettesitesek.CsoportTag_ID_helyettesitett = KRT_CsoportTagok.Id
		left outer join KRT_Csoportok as KRT_Csoportok_helyettesitett on KRT_CsoportTagok.Csoport_ID = KRT_Csoportok_helyettesitett.Id        
	WHERE 
		KRT_Felhasznalo_Szerepkor.Szerepkor_Id = ''' + CAST(@Szerepkor_Id as Nvarchar(40)) + '''

 '
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
 
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end