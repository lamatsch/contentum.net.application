﻿CREATE procedure [dbo].[sp_KRT_FelhasznalokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Partner_id     uniqueidentifier  = null,         
             @Tipus     Nvarchar(10)  = null,         
             @UserNev     Nvarchar(100)  = null,         
             @Nev     Nvarchar(100)  = null,         
             @Jelszo     Nvarchar(100)  = null,         
             @JelszoLejaratIdo     datetime  = null,         
             @System     char(1)  = null,         
             @Kiszolgalo     char(1)  = null,         
             @DefaultPrivatKulcs     Nvarchar(4000)  = null,         
             @Partner_Id_Munkahely     uniqueidentifier  = null,         
             @MaxMinosites     Nvarchar(2)  = null,         
             @EMail     Nvarchar(100)  = null,         
             @Engedelyezett     char(1)  = null,         
             @Telefonszam     Nvarchar(100)  = null,         
             @Beosztas     Nvarchar(100)  = null,         
             @Ver     int  = null,         
			 @WrongPassCnt     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @ZarolasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Partner_id     uniqueidentifier         
     DECLARE @Act_Tipus     Nvarchar(10)         
     DECLARE @Act_UserNev     Nvarchar(100)         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_Jelszo     Nvarchar(100)         
     DECLARE @Act_JelszoLejaratIdo     datetime         
     DECLARE @Act_System     char(1)         
     DECLARE @Act_Kiszolgalo     char(1)         
     DECLARE @Act_DefaultPrivatKulcs     Nvarchar(4000)         
     DECLARE @Act_Partner_Id_Munkahely     uniqueidentifier         
     DECLARE @Act_MaxMinosites     Nvarchar(2)         
     DECLARE @Act_EMail     Nvarchar(100)         
     DECLARE @Act_Engedelyezett     char(1)         
     DECLARE @Act_Telefonszam     Nvarchar(100)         
     DECLARE @Act_Beosztas     Nvarchar(100)         
     DECLARE @Act_Ver     int         
	 DECLARE @Act_WrongPassCnt     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Partner_id = Partner_id,
     @Act_Tipus = Tipus,
     @Act_UserNev = UserNev,
     @Act_Nev = Nev,
     @Act_Jelszo = Jelszo,
     @Act_JelszoLejaratIdo = JelszoLejaratIdo,
     @Act_System = System,
     @Act_Kiszolgalo = Kiszolgalo,
     @Act_DefaultPrivatKulcs = DefaultPrivatKulcs,
     @Act_Partner_Id_Munkahely = Partner_Id_Munkahely,
     @Act_MaxMinosites = MaxMinosites,
     @Act_EMail = EMail,
     @Act_Engedelyezett = Engedelyezett,
     @Act_Telefonszam = Telefonszam,
     @Act_Beosztas = Beosztas,
     @Act_Ver = Ver,
	 @Act_WrongPassCnt = WrongPassCnt,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Felhasznalok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL'rzAcs:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Partner_id')=1
         SET @Act_Partner_id = @Partner_id
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/UserNev')=1
         SET @Act_UserNev = @UserNev
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Jelszo')=1
         SET @Act_Jelszo = @Jelszo
   IF @UpdatedColumns.exist('/root/JelszoLejaratIdo')=1
         SET @Act_JelszoLejaratIdo = @JelszoLejaratIdo
   IF @UpdatedColumns.exist('/root/System')=1
         SET @Act_System = @System
   IF @UpdatedColumns.exist('/root/Kiszolgalo')=1
         SET @Act_Kiszolgalo = @Kiszolgalo
   IF @UpdatedColumns.exist('/root/WrongPassCnt')=1
         SET @Act_WrongPassCnt = @WrongPassCnt
   IF @UpdatedColumns.exist('/root/DefaultPrivatKulcs')=1
         SET @Act_DefaultPrivatKulcs = @DefaultPrivatKulcs
   IF @UpdatedColumns.exist('/root/Partner_Id_Munkahely')=1
         SET @Act_Partner_Id_Munkahely = @Partner_Id_Munkahely
   IF @UpdatedColumns.exist('/root/MaxMinosites')=1
         SET @Act_MaxMinosites = @MaxMinosites
   IF @UpdatedColumns.exist('/root/EMail')=1
         SET @Act_EMail = @EMail
   IF @UpdatedColumns.exist('/root/Engedelyezett')=1
         SET @Act_Engedelyezett = @Engedelyezett
   IF @UpdatedColumns.exist('/root/Telefonszam')=1
         SET @Act_Telefonszam = @Telefonszam
   IF @UpdatedColumns.exist('/root/Beosztas')=1
         SET @Act_Beosztas = @Beosztas
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Felhasznalok
SET
     Org = @Act_Org,
     Partner_id = @Act_Partner_id,
     Tipus = @Act_Tipus,
     UserNev = @Act_UserNev,
     Nev = @Act_Nev,
     Jelszo = @Act_Jelszo,
     JelszoLejaratIdo = @Act_JelszoLejaratIdo,
     System = @Act_System,
     Kiszolgalo = @Act_Kiszolgalo,
     DefaultPrivatKulcs = @Act_DefaultPrivatKulcs,
     Partner_Id_Munkahely = @Act_Partner_Id_Munkahely,
     MaxMinosites = @Act_MaxMinosites,
     EMail = @Act_EMail,
     Engedelyezett = @Act_Engedelyezett,
     Telefonszam = @Act_Telefonszam,
     Beosztas = @Act_Beosztas,
     Ver = @Act_Ver,
	 WrongPassCnt = @Act_WrongPassCnt,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     ZarolasIdo = @Act_ZarolasIdo,
     Zarolo_id = @Act_Zarolo_id,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Felhasznalok',@Id
					,'KRT_FelhasznalokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH