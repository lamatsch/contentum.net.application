﻿
CREATE procedure [dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Felhasznalok_Halozati_Nyomtatoi.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Felhasznalok_Halozati_Nyomtatoi.Id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Ver,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Note,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Stat_id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.ErvKezd,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.ErvVege,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Letrehozo_id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.LetrehozasIdo,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Modosito_id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.ModositasIdo,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Zarolo_id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.ZarolasIdo,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.Tranz_id,
	   KRT_Felhasznalok_Halozati_Nyomtatoi.UIAccessLog_id
   from 
     KRT_Felhasznalok_Halozati_Nyomtatoi as KRT_Felhasznalok_Halozati_Nyomtatoi      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end