﻿
CREATE procedure [dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_Felhasznalok_Halozati_NyomtatoiHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalok_Halozati_NyomtatoiHistory Old
         inner join KRT_Felhasznalok_Halozati_NyomtatoiHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Felhasznalok_Halozati_NyomtatoiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(Old.Felhasznalo_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(New.Felhasznalo_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalok_Halozati_NyomtatoiHistory Old
         inner join KRT_Felhasznalok_Halozati_NyomtatoiHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Felhasznalok_Halozati_NyomtatoiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Felhasznalo_Id != New.Felhasznalo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_ObjTipusok FTOld on FTOld.Id = Old.Felhasznalo_Id --and FTOld.Ver = Old.Ver
         left join KRT_ObjTipusok FTNew on FTNew.Id = New.Felhasznalo_Id --and FTNew.Ver = New.Ver      
)
          
            union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Halozati_Nyomtato_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(Old.Halozati_Nyomtato_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(New.Halozati_Nyomtato_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalok_Halozati_NyomtatoiHistory Old
         inner join KRT_Felhasznalok_Halozati_NyomtatoiHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Felhasznalok_Halozati_NyomtatoiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Halozati_Nyomtato_Id != New.Halozati_Nyomtato_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_ObjTipusok FTOld on FTOld.Id = Old.Felhasznalo_Id --and FTOld.Ver = Old.Ver
         left join KRT_ObjTipusok FTNew on FTNew.Id = New.Halozati_Nyomtato_Id --and FTNew.Ver = New.Ver      
)
end