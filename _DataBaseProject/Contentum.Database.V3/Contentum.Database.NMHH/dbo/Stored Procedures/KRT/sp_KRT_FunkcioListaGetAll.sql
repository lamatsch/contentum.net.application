﻿create procedure [dbo].[sp_KRT_FunkcioListaGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_FunkcioLista.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_FunkcioLista.Id,
	   KRT_FunkcioLista.Funkcio_Id,
	   KRT_FunkcioLista.Funkcio_Id_Hivott,
	   KRT_FunkcioLista.FutasiSorrend,
	   KRT_FunkcioLista.Ver,
	   KRT_FunkcioLista.Note,
	   KRT_FunkcioLista.Stat_id,
	   KRT_FunkcioLista.ErvKezd,
	   KRT_FunkcioLista.ErvVege,
	   KRT_FunkcioLista.Letrehozo_id,
	   KRT_FunkcioLista.LetrehozasIdo,
	   KRT_FunkcioLista.Modosito_id,
	   KRT_FunkcioLista.ModositasIdo,
	   KRT_FunkcioLista.Zarolo_id,
	   KRT_FunkcioLista.ZarolasIdo,
	   KRT_FunkcioLista.UIAccessLog_id  
   from 
     KRT_FunkcioLista as KRT_FunkcioLista      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end