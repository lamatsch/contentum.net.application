﻿create procedure [dbo].[sp_KRT_FunkciokGetAllByCsoporttagSajatJogu]
  @CsoportTag_Id UniqueIdentifier,
  @OrderBy nvarchar(200) = ' order by KRT_Funkciok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

	if (@CsoportTag_Id is null)
	begin
		RAISERROR('[53505]',16,1) -- A @CsoportTag_Id paraméter értéke nem lehet null!
	end


-- csoporttag rekord lekérdezése:
declare @Id uniqueidentifier
declare @Csoport_Id_Jogalany uniqueidentifier
declare @Csoport_Id uniqueidentifier
declare @ErvKezd datetime
declare @ErvVege datetime

select @Id = Id, @Csoport_Id = Csoport_Id, @Csoport_Id_Jogalany = Csoport_Id_Jogalany, @ErvKezd = ErvKezd, @ErvVege = ErvVege
from KRT_CsoportTagok where Id = @CsoportTag_Id

-- a rekord nem található
if (@Id is null)
begin
    RAISERROR('[50101]',16,1)
end

if not(getdate() between @ErvKezd and @ErvVege)
begin
    RAISERROR('[53501]',16,1) -- Nem létezo csoporttagság!
end
-- felhasználó ellenorzése:
declare @Felhasznalo_Id uniqueidentifier
select @Felhasznalo_Id = Id
    from KRT_Felhasznalok
    where Id = @Csoport_Id_Jogalany
    and getdate() between ErvKezd and ErvVege

if (@Felhasznalo_Id is null)
begin
    RAISERROR('[53300]',16,1) -- A felhasználó nem található!
end


  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	KRT_Funkciok.Id as Id,
	KRT_Funkciok.Kod as Kod	
   from 
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor,
	 KRT_Szerepkorok as KRT_Szerepkorok,
	 KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio,
	 KRT_Funkciok as KRT_Funkciok
	where
'

  SET @sqlcmd = @sqlcmd + '
	KRT_Felhasznalo_Szerepkor.Felhasznalo_Id = ''' + convert(nvarchar(36), @Csoport_Id_Jogalany) + '''
	and IsNull(KRT_Felhasznalo_Szerepkor.CsoportTag_Id, ''' + convert(nvarchar(36), @CsoportTag_Id) + ''') = ''' + convert(nvarchar(36), @CsoportTag_Id) + '''
	and IsNull(KRT_Felhasznalo_Szerepkor.Csoport_Id, ''' + convert(nvarchar(36), @Csoport_Id) + ''') = ''' + convert(nvarchar(36), @Csoport_Id) + '''
    and KRT_Felhasznalo_Szerepkor.Helyettesites_Id is null
	and KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id
	and KRT_Szerepkorok.Id = KRT_Szerepkor_Funkcio.Szerepkor_Id
	and KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id
	-- default ervenyesseg ellenorzes
	and KRT_Felhasznalo_Szerepkor.ErvKezd <= getdate() and KRT_Felhasznalo_Szerepkor.ErvVege >= getdate()
	and KRT_Szerepkorok.ErvKezd <= getdate() and KRT_Szerepkorok.ErvVege >= getdate()
	and KRT_Szerepkor_Funkcio.ErvKezd <= getdate() and KRT_Szerepkor_Funkcio.ErvVege >= getdate()
	and KRT_Funkciok.ErvKezd <= getdate() and KRT_Funkciok.ErvVege >= getdate()
	--and KRT_Funkciok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

   SET @sqlcmd = @sqlcmd + @OrderBy
  
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end