﻿create procedure [dbo].[sp_KRT_FunkciokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @WhereObjTipus nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Funkciok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Funkciok.Id,
	   KRT_Funkciok.Kod,
	   KRT_Funkciok.Nev,
	   KRT_Funkciok.ObjTipus_Id_AdatElem,
	   KRT_ObjTipusok.Kod AS ObjTipus_Id_AdatElem_Kod,
	   KRT_Funkciok.ObjStat_Id_Kezd,
	   KRT_Funkciok.Alkalmazas_Id,
	   KRT_Funkciok.Muvelet_Id,
	   KRT_Funkciok.ObjStat_Id_Veg,
	   KRT_Funkciok.Leiras,
	   KRT_Funkciok.Funkcio_Id_Szulo,
	   KRT_Funkciok.Csoportosito,
	   KRT_Funkciok.Modosithato,
	   KRT_Funkciok.Org,
	   KRT_Funkciok.MunkanaploJelzo,
	   KRT_Funkciok.FeladatJelzo,
	   KRT_Funkciok.KeziFeladatJelzo,
	   KRT_Funkciok.Ver,
	   KRT_Funkciok.Note,
	   KRT_Funkciok.Stat_id,
	   KRT_Funkciok.ErvKezd,
	   KRT_Funkciok.ErvVege,
	   KRT_Funkciok.Letrehozo_id,
	   KRT_Funkciok.LetrehozasIdo,
	   KRT_Funkciok.Modosito_id,
	   KRT_Funkciok.ModositasIdo,
	   KRT_Funkciok.Zarolo_id,
	   KRT_Funkciok.ZarolasIdo,
	   KRT_Funkciok.Tranz_id,
	   KRT_Funkciok.UIAccessLog_id  
   from 
     KRT_Funkciok as KRT_Funkciok
     left join KRT_ObjTipusok ON KRT_Funkciok.ObjTipus_Id_AdatElem = KRT_ObjTipusok.Id
            Where 1=1 --KRT_Funkciok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	END
	
	if @WhereObjTipus is not null and @WhereObjTipus!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @WhereObjTipus
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end