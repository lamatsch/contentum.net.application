﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_FunkciokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_FunkciokHistoryGetRecord
go
*/
create procedure sp_KRT_FunkciokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_FunkciokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_FunkciokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kod' as ColumnName,               
               cast(Old.Kod as nvarchar(99)) as OldValue,
               cast(New.Kod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kod as nvarchar(max)),'') != ISNULL(CAST(New.Kod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTipus_Id_AdatElem' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(Old.ObjTipus_Id_AdatElem) as OldValue,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(New.ObjTipus_Id_AdatElem) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ObjTipus_Id_AdatElem as nvarchar(max)),'') != ISNULL(CAST(New.ObjTipus_Id_AdatElem as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_ObjTipusok FTOld on FTOld.Id = Old.ObjTipus_Id_AdatElem and FTOld.Ver = Old.Ver
         left join KRT_ObjTipusok FTNew on FTNew.Id = New.ObjTipus_Id_AdatElem and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjStat_Id_Kezd' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KodTarakAzonosito(Old.ObjStat_Id_Kezd) as OldValue,
/*FK*/           dbo.fn_GetKRT_KodTarakAzonosito(New.ObjStat_Id_Kezd) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ObjStat_Id_Kezd as nvarchar(max)),'') != ISNULL(CAST(New.ObjStat_Id_Kezd as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodTarak FTOld on FTOld.Id = Old.ObjStat_Id_Kezd and FTOld.Ver = Old.Ver
         left join KRT_KodTarak FTNew on FTNew.Id = New.ObjStat_Id_Kezd and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alkalmazas_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_AlkalmazasokAzonosito(Old.Alkalmazas_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_AlkalmazasokAzonosito(New.Alkalmazas_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Alkalmazas_Id as nvarchar(max)),'') != ISNULL(CAST(New.Alkalmazas_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Alkalmazasok FTOld on FTOld.Id = Old.Alkalmazas_Id and FTOld.Ver = Old.Ver
         left join KRT_Alkalmazasok FTNew on FTNew.Id = New.Alkalmazas_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Muvelet_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_MuveletekAzonosito(Old.Muvelet_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_MuveletekAzonosito(New.Muvelet_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Muvelet_Id as nvarchar(max)),'') != ISNULL(CAST(New.Muvelet_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Muveletek FTOld on FTOld.Id = Old.Muvelet_Id and FTOld.Ver = Old.Ver
         left join KRT_Muveletek FTNew on FTNew.Id = New.Muvelet_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjStat_Id_Veg' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KodTarakAzonosito(Old.ObjStat_Id_Veg) as OldValue,
/*FK*/           dbo.fn_GetKRT_KodTarakAzonosito(New.ObjStat_Id_Veg) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ObjStat_Id_Veg as nvarchar(max)),'') != ISNULL(CAST(New.ObjStat_Id_Veg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodTarak FTOld on FTOld.Id = Old.ObjStat_Id_Veg and FTOld.Ver = Old.Ver
         left join KRT_KodTarak FTNew on FTNew.Id = New.ObjStat_Id_Veg and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               
               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FunkciokAzonosito(Old.Funkcio_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetKRT_FunkciokAzonosito(New.Funkcio_Id_Szulo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Funkcio_Id_Szulo as nvarchar(max)),'') != ISNULL(CAST(New.Funkcio_Id_Szulo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Funkciok FTOld on FTOld.Id = Old.Funkcio_Id_Szulo and FTOld.Ver = Old.Ver
         left join KRT_Funkciok FTNew on FTNew.Id = New.Funkcio_Id_Szulo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoportosito' as ColumnName,               
               cast(Old.Csoportosito as nvarchar(99)) as OldValue,
               cast(New.Csoportosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoportosito as nvarchar(max)),'') != ISNULL(CAST(New.Csoportosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Modosithato' as ColumnName,               
               cast(Old.Modosithato as nvarchar(99)) as OldValue,
               cast(New.Modosithato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Modosithato as nvarchar(max)),'') != ISNULL(CAST(New.Modosithato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               
               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MunkanaploJelzo' as ColumnName,               
               cast(Old.MunkanaploJelzo as nvarchar(99)) as OldValue,
               cast(New.MunkanaploJelzo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MunkanaploJelzo as nvarchar(max)),'') != ISNULL(CAST(New.MunkanaploJelzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladatJelzo' as ColumnName,               
               cast(Old.FeladatJelzo as nvarchar(99)) as OldValue,
               cast(New.FeladatJelzo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FeladatJelzo as nvarchar(max)),'') != ISNULL(CAST(New.FeladatJelzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KeziFeladatJelzo' as ColumnName,               
               cast(Old.KeziFeladatJelzo as nvarchar(99)) as OldValue,
               cast(New.KeziFeladatJelzo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_FunkciokHistory Old
         inner join KRT_FunkciokHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_FunkciokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KeziFeladatJelzo as nvarchar(max)),'') != ISNULL(CAST(New.KeziFeladatJelzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)
	  union all
       (select  New.HistoryId as RowId,
              New.Ver as Ver,
              case New.HistoryMuvelet_Id
                 when 1 then 'Módosítás'
                 when 2 then 'Érvénytelenítés'
              end as Operation,
              'ErvVege' as ColumnName,
              cast(FORMAT(Old.ErvVege, 'yyyy-MM-dd hh:mm:ss') as nvarchar(99)) as OldValue,
              cast(FORMAT(New.ErvVege, 'yyyy-MM-dd hh:mm:ss')as nvarchar(99)) as NewValue,
              U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
     from KRT_FunkciokHistory Old
        inner join KRT_FunkciokHistory New on Old.Ver = New.Ver-1
           and Old.Id = New.Id
           and ISNULL(CAST(Old.ErvVege as nvarchar(max)),'') != ISNULL(CAST(New.ErvVege as nvarchar(max)),'')
           and Old.Id = New.Id
        inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
     where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go