﻿

CREATE procedure [dbo].[sp_KRT_GetAllAtmenetiIrattar]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' KRT_Partnerek.Nev',
  @TopRow nvarchar(5) = '',  
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select  ' + @LocalTopRow + '
  	   KRT_PartnerKapcsolatok.Id,
	   KRT_PartnerKapcsolatok.Tipus,
	   KRT_KodTarak1.Nev as TipusNev,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt,
	   KRT_Partnerek.Nev as Partner_kapcsolt_Nev,
	   KRT_Partnerek.Nev as Nev,	
	   KRT_KodTarak2.Nev as Partner_kapcsolt_TipusNev,	
	   KRT_PartnerKapcsolatok.Ver,
	   KRT_PartnerKapcsolatok.Note,
	   KRT_PartnerKapcsolatok.Stat_id,
	   KRT_PartnerKapcsolatok.ErvKezd,
	   KRT_PartnerKapcsolatok.ErvVege,
	   KRT_PartnerKapcsolatok.Letrehozo_id,
	   KRT_PartnerKapcsolatok.LetrehozasIdo,
	   KRT_PartnerKapcsolatok.Modosito_id,
	   KRT_PartnerKapcsolatok.ModositasIdo,
	   KRT_PartnerKapcsolatok.Zarolo_id,
	   KRT_PartnerKapcsolatok.ZarolasIdo,
	   KRT_PartnerKapcsolatok.Tranz_id,
	   KRT_PartnerKapcsolatok.UIAccessLog_id  
   from 
     KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok
	 left join KRT_KodCsoportok as KRT_KodCsoportok1 on KRT_KodCsoportok1.Kod=''PARTNERKAPCSOLAT_TIPUSA''
     left join KRT_KodTarak as KRT_Kodtarak1 on KRT_PartnerKapcsolatok.Tipus = KRT_KodTarak1.Kod and KRT_KodCsoportok1.Id = KRT_KodTarak1.KodCsoport_Id
		and KRT_KodTarak1.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_Partnerek as KRT_Partnerek on KRT_PartnerKapcsolatok.Partner_id_kapcsolt = KRT_Partnerek.Id
	 left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''PARTNER_TIPUS''
     left join KRT_KodTarak as KRT_KodTarak2 on KRT_Partnerek.Tipus = KRT_KodTarak2.Kod and KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id
		and KRT_KodTarak2.Org=''' + cast(@Org as NVarChar(40)) + '''
     where KRT_Partnerek.Org=''' + cast(@Org as NVarChar(40)) + '''
'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end     
      
   SET @sqlcmd = @sqlcmd + ' order by ' + @OrderBy
    exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end