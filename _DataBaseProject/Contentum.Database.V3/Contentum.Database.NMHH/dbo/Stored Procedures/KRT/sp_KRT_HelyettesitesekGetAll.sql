﻿create procedure [dbo].[sp_KRT_HelyettesitesekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Helyettesitesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Helyettesitesek.Id,
	   KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett,
	   KRT_Helyettesitesek.CsoportTag_ID_helyettesitett,
	   KRT_Helyettesitesek.Felhasznalo_ID_helyettesito,
	   KRT_Helyettesitesek.HelyettesitesMod,
	   KRT_Helyettesitesek.HelyettesitesKezd,
	   KRT_Helyettesitesek.HelyettesitesVege,
	   KRT_Helyettesitesek.Megjegyzes,
	   KRT_Helyettesitesek.Ver,
	   KRT_Helyettesitesek.Note,
	   KRT_Helyettesitesek.Stat_id,
	   KRT_Helyettesitesek.ErvKezd,
	   KRT_Helyettesitesek.ErvVege,
	   KRT_Helyettesitesek.Letrehozo_id,
	   KRT_Helyettesitesek.LetrehozasIdo,
	   KRT_Helyettesitesek.Modosito_id,
	   KRT_Helyettesitesek.ModositasIdo,
	   KRT_Helyettesitesek.Zarolo_id,
	   KRT_Helyettesitesek.ZarolasIdo,
	   KRT_Helyettesitesek.Tranz_id,
	   KRT_Helyettesitesek.UIAccessLog_id  
   from 
     KRT_Helyettesitesek as KRT_Helyettesitesek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end