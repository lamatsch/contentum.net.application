﻿CREATE procedure [dbo].[sp_KRT_HelyettesitesekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Helyettesitesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @CsoportTag_Id uniqueidentifier = null,	-- vezeto: saját szervezet dolgozói, dolgozó: csak ahol o a helyettesíto vagy helyettesített
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

  if @CsoportTag_Id is not null
  begin
    declare @CsoportTagok_Tipus nvarchar(64)
    declare @CsoportTagok_Id uniqueidentifier
    declare @CsoportTagok_Csoport_Id uniqueidentifier
    select @CsoportTagok_Id = Id, @CsoportTagok_Tipus = Tipus, @CsoportTagok_Csoport_Id = Csoport_Id
    from KRT_CsoportTagok where Id = @CsoportTag_Id
                  and Csoport_Id_Jogalany = @ExecutorUserId
                  and getdate() between ErvKezd and ErvVege

    if @CsoportTagok_Id is null
		RAISERROR('[53501]',16,1) -- Nem létezo csoporttagság!

    if @CsoportTagok_Tipus is null
		RAISERROR('[53502]',16,1) -- A felhasználó csoporttagságának típusa nem azonosítható!

    if @CsoportTagok_Tipus = '2' -- dolgozó
	begin
-- dolgozó csoportjába tartozó dolgozók lekérése
		set @sqlcmd = 'select KRT_Csoportok.Id as Id into #tmp_Dolgozok
from KRT_CsoportTagok
join KRT_Csoportok
on KRT_CsoportTagok.Csoport_Id_Jogalany = KRT_Csoportok.Id
where KRT_CsoportTagok.Csoport_Id = @CsoportTagok_Csoport_Id
and KRT_Csoportok.Tipus = ''1''
and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
'
	end
	else
	if @CsoportTagok_Tipus = '3' -- vezeto
	begin
-- vezeto szervezetee alatti dolgozók lekérése
		  set @sqlcmd = @sqlcmd + '
WITH temp AS
(
	SELECT @CsoportTagok_Csoport_Id as Id
				
	UNION ALL
		
	SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
		FROM KRT_CsoportTagok
			INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
		WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
			AND KRT_CsoportTagok.Tipus IS NOT NULL
)'
		  set @sqlcmd = @sqlcmd + '
select KRT_Csoportok.Id into #tmp_Dolgozok from KRT_Csoportok
where KRT_Csoportok.Id in (select Id from temp where Tipus= ''1'')
and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
'
	end
  end


	/************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Helyettesitesek.Id into #result
		from KRT_Helyettesitesek as KRT_Helyettesitesek
   inner join KRT_Felhasznalok as KRT_Felhasznalok1
   on KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett = KRT_Felhasznalok1.Id
		and KRT_Felhasznalok1.Org=@Org
   inner join KRT_Felhasznalok as KRT_Felhasznalok2
   on KRT_Helyettesitesek.Felhasznalo_ID_helyettesito = KRT_Felhasznalok2.Id
		and KRT_Felhasznalok2.Org=@Org
   left join KRT_CsoportTagok on KRT_Helyettesitesek.CsoportTag_ID_helyettesitett = KRT_CsoportTagok.Id
   left join KRT_Csoportok on KRT_CsoportTagok.Csoport_Id = KRT_Csoportok.Id
'
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

   if @CsoportTag_Id is not null
	begin 
		SET @sqlcmd = @sqlcmd + '
and (KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett in (select Id from #tmp_Dolgozok)
or KRT_Helyettesitesek.Felhasznalo_ID_helyettesito in (select Id from #tmp_Dolgozok))
'
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Helyettesitesek.Id,
       KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett,
	   KRT_Felhasznalok1.Nev as Felhasznalo_Nev_helyettesitett,
	   KRT_Helyettesitesek.CsoportTag_ID_helyettesitett,
       KRT_Csoportok.Nev as Csoport_Nev_helyettesitett,
       KRT_Csoportok.Id as Csoport_Id_helyettesitett,
	   KRT_Felhasznalok2.Nev as Felhasznalo_Nev_helyettesito,
       KRT_Helyettesitesek.Felhasznalo_ID_helyettesito,
       KRT_Helyettesitesek.HelyettesitesMod,
	   KRT_Helyettesitesek.HelyettesitesKezd,
	   KRT_Helyettesitesek.HelyettesitesVege,
	   KRT_Helyettesitesek.Megjegyzes,
	   KRT_Helyettesitesek.Ver,
	   KRT_Helyettesitesek.Note,
	   KRT_Helyettesitesek.Stat_id,
	   KRT_Helyettesitesek.ErvKezd,
	   KRT_Helyettesitesek.ErvVege,
	   KRT_Helyettesitesek.Letrehozo_id,
	   KRT_Helyettesitesek.LetrehozasIdo,
	   KRT_Helyettesitesek.Modosito_id,
	   KRT_Helyettesitesek.ModositasIdo,
	   KRT_Helyettesitesek.Zarolo_id,
	   KRT_Helyettesitesek.ZarolasIdo,
	   KRT_Helyettesitesek.Tranz_id,
	   KRT_Helyettesitesek.UIAccessLog_id  
   from 
     KRT_Helyettesitesek as KRT_Helyettesitesek
     	inner join #result on #result.Id = KRT_Helyettesitesek.Id 
   inner join KRT_Felhasznalok as KRT_Felhasznalok1
   on KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett = KRT_Felhasznalok1.Id
		and KRT_Felhasznalok1.Org=@Org
   inner join KRT_Felhasznalok as KRT_Felhasznalok2
   on KRT_Helyettesitesek.Felhasznalo_ID_helyettesito = KRT_Felhasznalok2.Id
		and KRT_Felhasznalok2.Org=@Org
   left join KRT_CsoportTagok on KRT_Helyettesitesek.CsoportTag_ID_helyettesitett = KRT_CsoportTagok.Id
   left join KRT_Csoportok on KRT_CsoportTagok.Csoport_Id = KRT_Csoportok.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier, @CsoportTagok_Csoport_Id uniqueidentifier'
		,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org, @CsoportTagok_Csoport_Id = @CsoportTagok_Csoport_Id;  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end