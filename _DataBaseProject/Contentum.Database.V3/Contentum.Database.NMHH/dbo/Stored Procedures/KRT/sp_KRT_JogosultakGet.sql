﻿create procedure [dbo].[sp_KRT_JogosultakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Jogosultak.Id,
	   KRT_Jogosultak.Csoport_Id_Jogalany,
	   KRT_Jogosultak.Jogszint,
	   KRT_Jogosultak.Obj_Id,
	   KRT_Jogosultak.Orokolheto,
	   KRT_Jogosultak.Kezi,
	   KRT_Jogosultak.Tipus,
	   KRT_Jogosultak.Ver,
	   KRT_Jogosultak.Note,
	   KRT_Jogosultak.Stat_id,
	   KRT_Jogosultak.ErvKezd,
	   KRT_Jogosultak.ErvVege,
	   KRT_Jogosultak.Letrehozo_id,
	   KRT_Jogosultak.LetrehozasIdo,
	   KRT_Jogosultak.Modosito_id,
	   KRT_Jogosultak.ModositasIdo,
	   KRT_Jogosultak.Zarolo_id,
	   KRT_Jogosultak.ZarolasIdo,
	   KRT_Jogosultak.Tranz_id,
	   KRT_Jogosultak.UIAccessLog_id
	   from 
		 KRT_Jogosultak as KRT_Jogosultak 
	   where
		 KRT_Jogosultak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end