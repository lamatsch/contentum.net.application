﻿create procedure [dbo].[sp_KRT_JogosultakGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Jogosultak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Jogosultak.Id,
	   KRT_Jogosultak.Csoport_Id_Jogalany,
	   KRT_Jogosultak.Jogszint,
	   KRT_Jogosultak.Obj_Id,
	   KRT_Jogosultak.Orokolheto,
	   KRT_Jogosultak.Kezi,
	   KRT_Jogosultak.Tipus,
	   KRT_Jogosultak.Ver,
	   KRT_Jogosultak.Note,
	   KRT_Jogosultak.Stat_id,
	   KRT_Jogosultak.ErvKezd,
	   KRT_Jogosultak.ErvVege,
	   KRT_Jogosultak.Letrehozo_id,
	   KRT_Jogosultak.LetrehozasIdo,
	   KRT_Jogosultak.Modosito_id,
	   KRT_Jogosultak.ModositasIdo,
	   KRT_Jogosultak.Zarolo_id,
	   KRT_Jogosultak.ZarolasIdo,
	   KRT_Jogosultak.Tranz_id,
	   KRT_Jogosultak.UIAccessLog_id  
   from 
     KRT_Jogosultak as KRT_Jogosultak      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end