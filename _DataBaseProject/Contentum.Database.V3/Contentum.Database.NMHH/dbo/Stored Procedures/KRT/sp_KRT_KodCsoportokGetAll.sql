﻿create procedure [dbo].[sp_KRT_KodCsoportokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_KodCsoportok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* SzL±rt adatokhoz rendezA©s A©s sorszA?m A¶sszeA?llA­tA?sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_KodCsoportok.Id into #result
		from KRT_KodCsoportok as KRT_KodCsoportok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* TA©nyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_KodCsoportok.Id,
	   KRT_KodCsoportok.Org,
	   KRT_KodCsoportok.KodTarak_Id_KodcsoportTipus,
	   KRT_KodCsoportok.Kod,
	   KRT_KodCsoportok.Nev,
	   KRT_KodCsoportok.Modosithato,
	   KRT_KodCsoportok.Hossz,
	   KRT_KodCsoportok.Csoport_Id_Tulaj,
	   KRT_KodCsoportok.Leiras,
	   KRT_KodCsoportok.BesorolasiSema,
	   KRT_KodCsoportok.KiegAdat,
	   KRT_KodCsoportok.KiegMezo,
	   KRT_KodCsoportok.KiegAdattipus,
	   KRT_KodCsoportok.KiegSzotar,
	   KRT_KodCsoportok.Ver,
	   KRT_KodCsoportok.Note,
	   KRT_KodCsoportok.Stat_id,
	   KRT_KodCsoportok.ErvKezd,
	   KRT_KodCsoportok.ErvVege,
	   KRT_KodCsoportok.Letrehozo_id,
	   KRT_KodCsoportok.LetrehozasIdo,
	   KRT_KodCsoportok.Modosito_id,
	   KRT_KodCsoportok.ModositasIdo,
	   KRT_KodCsoportok.Zarolo_id,
	   KRT_KodCsoportok.ZarolasIdo,
	   KRT_KodCsoportok.Tranz_id,
	   KRT_KodCsoportok.UIAccessLog_id  
   from 
     KRT_KodCsoportok as KRT_KodCsoportok      
     	inner join #result on #result.Id = KRT_KodCsoportok.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

	-- talA?latok szA?ma A©s oldalszA?m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end