﻿create procedure [dbo].[sp_KRT_KodCsoportokInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @KodTarak_Id_KodcsoportTipus     uniqueidentifier  = null,
	            @Kod     nvarchar(64),
	            @Nev     Nvarchar(400),
	            @Modosithato     char(1),
                @Hossz     int  = null,
                @Csoport_Id_Tulaj     uniqueidentifier  = null,
                @Leiras     Nvarchar(4000)  = null,
	            @BesorolasiSema     char(1),
                @KiegAdat     char(1)  = null,
                @KiegMezo     Nvarchar(400)  = null,
                @KiegAdattipus     char(1)  = null,
                @KiegSzotar     Nvarchar(100)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @KodTarak_Id_KodcsoportTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',KodTarak_Id_KodcsoportTipus'
            SET @insertValues = @insertValues + ',@KodTarak_Id_KodcsoportTipus'
         end 
       
         if @Kod is not null
         begin
            SET @insertColumns = @insertColumns + ',Kod'
            SET @insertValues = @insertValues + ',@Kod'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Modosithato is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosithato'
            SET @insertValues = @insertValues + ',@Modosithato'
         end 
       
         if @Hossz is not null
         begin
            SET @insertColumns = @insertColumns + ',Hossz'
            SET @insertValues = @insertValues + ',@Hossz'
         end 
       
         if @Csoport_Id_Tulaj is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Tulaj'
            SET @insertValues = @insertValues + ',@Csoport_Id_Tulaj'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @BesorolasiSema is not null
         begin
            SET @insertColumns = @insertColumns + ',BesorolasiSema'
            SET @insertValues = @insertValues + ',@BesorolasiSema'
         end 
       
         if @KiegAdat is not null
         begin
            SET @insertColumns = @insertColumns + ',KiegAdat'
            SET @insertValues = @insertValues + ',@KiegAdat'
         end 
       
         if @KiegMezo is not null
         begin
            SET @insertColumns = @insertColumns + ',KiegMezo'
            SET @insertValues = @insertValues + ',@KiegMezo'
         end 
       
         if @KiegAdattipus is not null
         begin
            SET @insertColumns = @insertColumns + ',KiegAdattipus'
            SET @insertValues = @insertValues + ',@KiegAdattipus'
         end 
       
         if @KiegSzotar is not null
         begin
            SET @insertColumns = @insertColumns + ',KiegSzotar'
            SET @insertValues = @insertValues + ',@KiegSzotar'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_KodCsoportok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@KodTarak_Id_KodcsoportTipus uniqueidentifier,@Kod nvarchar(64),@Nev Nvarchar(400),@Modosithato char(1),@Hossz int,@Csoport_Id_Tulaj uniqueidentifier,@Leiras Nvarchar(4000),@BesorolasiSema char(1),@KiegAdat char(1),@KiegMezo Nvarchar(400),@KiegAdattipus char(1),@KiegSzotar Nvarchar(100),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@KodTarak_Id_KodcsoportTipus = @KodTarak_Id_KodcsoportTipus,@Kod = @Kod,@Nev = @Nev,@Modosithato = @Modosithato,@Hossz = @Hossz,@Csoport_Id_Tulaj = @Csoport_Id_Tulaj,@Leiras = @Leiras,@BesorolasiSema = @BesorolasiSema,@KiegAdat = @KiegAdat,@KiegMezo = @KiegMezo,@KiegAdattipus = @KiegAdattipus,@KiegSzotar = @KiegSzotar,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_KodCsoportok',@ResultUid
					,'KRT_KodCsoportokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH