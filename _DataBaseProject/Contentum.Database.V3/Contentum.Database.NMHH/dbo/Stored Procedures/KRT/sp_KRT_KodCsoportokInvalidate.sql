﻿create procedure [dbo].[sp_KRT_KodCsoportokInvalidate]
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
--BEGIN TRANSACTION InvalidateTransaction

  
	set nocount on

SET @ExecutionTime = getdate()

	-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'KRT_KodCsoportok',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN
   
      DECLARE @Act_ErvKezd datetime
      DECLARE @Act_ErvVege datetime
      DECLARE @Act_Ver int
      DECLARE @Modosithato char(1)
      
      select
         @Act_ErvKezd = ErvKezd,
         @Act_ErvVege = ErvVege,
         @Act_Ver = Ver,
         @Modosithato = Modosithato
      from KRT_KodCsoportok
      where Id = @Id
      
      if @@rowcount = 0
	   begin
	    	RAISERROR('[50602]',16,1)
	   end
      
      if @Act_ErvVege <= @ExecutionTime
      begin
         -- mA?r A©rvA©nytelenA­tve van
         RAISERROR('[50603]',16,1)
      end

      if @Modosithato NOT IN ('I','i','1')
      begin
        -- nem A©rvA©nytelenA­thetL‘
	    RAISERROR('[50604]',16,1)
      end

	  
      
      IF @Act_Ver is null
	   	SET @Act_Ver = 2	
      ELSE
	   	SET @Act_Ver = @Act_Ver+1
      
      if @Act_ErvKezd > @ExecutionTime
      begin
      
         -- ha a jA¶vL‘ben kezdL‘dA¶tt volna el, az ErvKezd-et is beA?llA­tjuk
      
		   UPDATE KRT_KodCsoportok
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                ErvKezd = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end
      else
      begin
   
		   UPDATE KRT_KodCsoportok
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end

		if @@rowcount != 1
		begin
			RAISERROR('[50601]',16,1)
		end		
        else
        begin           /* History Log */
           exec sp_LogRecordToHistory 'KRT_KodCsoportok',@Id
                 ,'KRT_KodCsoportokHistory',2,@ExecutorUserId,@ExecutionTime           
        end
	END
	ELSE BEGIN
		RAISERROR('[50699]',16,1)
	END


--COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   
--   IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH