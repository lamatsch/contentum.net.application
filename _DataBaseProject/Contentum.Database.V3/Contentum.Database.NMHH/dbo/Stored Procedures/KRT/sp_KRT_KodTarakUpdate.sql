﻿create procedure [dbo].[sp_KRT_KodTarakUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @KodCsoport_Id     uniqueidentifier  = null,         
             @ObjTip_Id_AdatElem     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @Kod     nvarchar(64)  = null,         
             @Nev     Nvarchar(400)  = null,         
             @RovidNev     Nvarchar(10)  = null,         
             @Egyeb     Nvarchar(4000)  = null,         
             @Modosithato     char(1)  = null,         
             @Sorrend     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_KodCsoport_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id_AdatElem     uniqueidentifier         
     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_Kod     nvarchar(64)         
     DECLARE @Act_Nev     Nvarchar(400)         
     DECLARE @Act_RovidNev     Nvarchar(10)         
     DECLARE @Act_Egyeb     Nvarchar(4000)         
     DECLARE @Act_Modosithato     char(1)         
     DECLARE @Act_Sorrend     Nvarchar(100)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_KodCsoport_Id = KodCsoport_Id,
     @Act_ObjTip_Id_AdatElem = ObjTip_Id_AdatElem,
     @Act_Obj_Id = Obj_Id,
     @Act_Kod = Kod,
     @Act_Nev = Nev,
     @Act_RovidNev = RovidNev,
     @Act_Egyeb = Egyeb,
     @Act_Modosithato = Modosithato,
     @Act_Sorrend = Sorrend,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_KodTarak
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
--if @ExecutionTime > @Act_ErvVege
--   begin
--       RAISERROR('[50403]',16,1)
--       return @@error
--   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

-- mAldosA­thatAlsA?g ellenL‘rzA©se
if @Modosithato NOT IN ('I','i','1')
begin
  RAISERROR('[50404]',16,1)
end

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/KodCsoport_Id')=1
         SET @Act_KodCsoport_Id = @KodCsoport_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id_AdatElem')=1
         SET @Act_ObjTip_Id_AdatElem = @ObjTip_Id_AdatElem
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/Kod')=1
         SET @Act_Kod = @Kod
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/RovidNev')=1
         SET @Act_RovidNev = @RovidNev
   IF @UpdatedColumns.exist('/root/Egyeb')=1
         SET @Act_Egyeb = @Egyeb
   IF @UpdatedColumns.exist('/root/Modosithato')=1
         SET @Act_Modosithato = @Modosithato
   IF @UpdatedColumns.exist('/root/Sorrend')=1
         SET @Act_Sorrend = @Sorrend
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_KodTarak
SET
     Org = @Act_Org,
     KodCsoport_Id = @Act_KodCsoport_Id,
     ObjTip_Id_AdatElem = @Act_ObjTip_Id_AdatElem,
     Obj_Id = @Act_Obj_Id,
     Kod = @Act_Kod,
     Nev = @Act_Nev,
     RovidNev = @Act_RovidNev,
     Egyeb = @Act_Egyeb,
     Modosithato = @Act_Modosithato,
     Sorrend = @Act_Sorrend,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_KodTarak',@Id
					,'KRT_KodTarakHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH