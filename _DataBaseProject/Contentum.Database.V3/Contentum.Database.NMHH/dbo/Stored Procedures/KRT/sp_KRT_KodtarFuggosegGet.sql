﻿
create procedure sp_KRT_KodtarFuggosegGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_KodtarFuggoseg.Id,
	   KRT_KodtarFuggoseg.Org,
	   KRT_KodtarFuggoseg.Vezerlo_KodCsoport_Id,
	   KRT_KodtarFuggoseg.Fuggo_KodCsoport_Id,
	   KRT_KodtarFuggoseg.Adat,
	   KRT_KodtarFuggoseg.Aktiv,
	   KRT_KodtarFuggoseg.Ver,
	   KRT_KodtarFuggoseg.Note,
	   KRT_KodtarFuggoseg.Stat_id,
	   KRT_KodtarFuggoseg.ErvKezd,
	   KRT_KodtarFuggoseg.ErvVege,
	   KRT_KodtarFuggoseg.Letrehozo_id,
	   KRT_KodtarFuggoseg.LetrehozasIdo,
	   KRT_KodtarFuggoseg.Modosito_id,
	   KRT_KodtarFuggoseg.ModositasIdo,
	   KRT_KodtarFuggoseg.Zarolo_id,
	   KRT_KodtarFuggoseg.ZarolasIdo,
	   KRT_KodtarFuggoseg.Tranz_id,
	   KRT_KodtarFuggoseg.UIAccessLog_id
	   from 
		 KRT_KodtarFuggoseg as KRT_KodtarFuggoseg 
	   where
		 KRT_KodtarFuggoseg.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end