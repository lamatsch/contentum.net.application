﻿
create procedure sp_KRT_KodtarFuggosegGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_KodtarFuggoseg.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_KodtarFuggoseg.Id,
	   KRT_KodtarFuggoseg.Org,
	   KRT_KodtarFuggoseg.Vezerlo_KodCsoport_Id,
	   KRT_KodtarFuggoseg.Fuggo_KodCsoport_Id,
	   KRT_KodtarFuggoseg.Adat,
	   KRT_KodtarFuggoseg.Aktiv,
	   KRT_KodtarFuggoseg.Ver,
	   KRT_KodtarFuggoseg.Note,
	   KRT_KodtarFuggoseg.Stat_id,
	   KRT_KodtarFuggoseg.ErvKezd,
	   KRT_KodtarFuggoseg.ErvVege,
	   KRT_KodtarFuggoseg.Letrehozo_id,
	   KRT_KodtarFuggoseg.LetrehozasIdo,
	   KRT_KodtarFuggoseg.Modosito_id,
	   KRT_KodtarFuggoseg.ModositasIdo,
	   KRT_KodtarFuggoseg.Zarolo_id,
	   KRT_KodtarFuggoseg.ZarolasIdo,
	   KRT_KodtarFuggoseg.Tranz_id,
	   KRT_KodtarFuggoseg.UIAccessLog_id  
   from 
     KRT_KodtarFuggoseg as KRT_KodtarFuggoseg      
    Where KRT_KodtarFuggoseg.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end