﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_KodtarFuggosegHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_KodtarFuggosegHistoryGetRecord
go
*/
create procedure sp_KRT_KodtarFuggosegHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_KodtarFuggosegHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_KodtarFuggosegHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               
               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Vezerlo_KodCsoport_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KodCsoportokAzonosito(Old.Vezerlo_KodCsoport_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_KodCsoportokAzonosito(New.Vezerlo_KodCsoport_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Vezerlo_KodCsoport_Id as nvarchar(max)),'') != ISNULL(CAST(New.Vezerlo_KodCsoport_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodCsoportok FTOld on FTOld.Id = Old.Vezerlo_KodCsoport_Id and FTOld.Ver = Old.Ver
         left join KRT_KodCsoportok FTNew on FTNew.Id = New.Vezerlo_KodCsoport_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fuggo_KodCsoport_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(Old.Fuggo_KodCsoport_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(New.Fuggo_KodCsoport_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Fuggo_KodCsoport_Id as nvarchar(max)),'') != ISNULL(CAST(New.Fuggo_KodCsoport_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_ObjTipusok FTOld on FTOld.Id = Old.Fuggo_KodCsoport_Id and FTOld.Ver = Old.Ver
         left join KRT_ObjTipusok FTNew on FTNew.Id = New.Fuggo_KodCsoport_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Adat' as ColumnName,               
               cast(Old.Adat as nvarchar(99)) as OldValue,
               cast(New.Adat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Adat as nvarchar(max)),'') != ISNULL(CAST(New.Adat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Aktiv' as ColumnName,               
               cast(Old.Aktiv as nvarchar(99)) as OldValue,
               cast(New.Aktiv as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodtarFuggosegHistory Old
         inner join KRT_KodtarFuggosegHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_KodtarFuggosegHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Aktiv as nvarchar(max)),'') != ISNULL(CAST(New.Aktiv as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go