﻿create procedure [dbo].[sp_KRT_KozteruletekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Kozteruletek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Kozteruletek.Id,
	   KRT_Kozteruletek.Org,
	   KRT_Kozteruletek.Nev,
	   KRT_Kozteruletek.Ver,
	   KRT_Kozteruletek.Note,
	   KRT_Kozteruletek.Stat_id,
	   KRT_Kozteruletek.ErvKezd,
	   KRT_Kozteruletek.ErvVege,
	   KRT_Kozteruletek.Letrehozo_id,
	   KRT_Kozteruletek.LetrehozasIdo,
	   KRT_Kozteruletek.Modosito_id,
	   KRT_Kozteruletek.ModositasIdo,
	   KRT_Kozteruletek.Zarolo_id,
	   KRT_Kozteruletek.ZarolasIdo,
	   KRT_Kozteruletek.Tranz_id,
	   KRT_Kozteruletek.UIAccessLog_id  
   from 
     KRT_Kozteruletek as KRT_Kozteruletek      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end