CREATE PROCEDURE [dbo].[sp_KRT_Log_AuditLogExport]
	-- Add the parameters for the stored procedure here
	@Where nvarchar(4000) = '',
	@Where_Log_Login nvarchar(4000) = '',
	@Where_Log_Page nvarchar(4000) = '',
	@Where_Log_WebService	nvarchar(4000) = '',
	@Where_Log_StoredProcedure nvarchar(4000) = '',
	@OrderBy nvarchar(200) = ' order by KRT_Log_Login.Date',
	@TopRow nvarchar(5) = '',
	@ExecutorUserId				uniqueidentifier,
  @FromHistory char(1) = '',
  @Id    uniqueidentifier = null

AS
BEGIN
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @ExecutorUserId uniqueidentifier = '07361BD2-64A9-E911-80D8-00155D027E24'
	DECLARE @Org uniqueidentifier
    SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)

	if (@Org is null)
		begin
			RAISERROR('[50202]',16,1)
		end

	DECLARE @sqlcmd nvarchar(4000)

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
 
	SET @sqlcmd = 'SELECT ' + @LocalTopRow + ' KRT_Log_Login.*, KRT_Log_Page.*, KRT_Log_WebService.*, KRT_Log_StoredProcedure.*,kodtarnev.nev as HelyettesitesMod_Nev
	FROM KRT_Log_Login
	left join KRT_Log_Page on KRT_Log_Page.Login_Tranz_id = KRT_Log_Login.Tranz_id
	left join KRT_Log_WebService on KRT_Log_WebService.Tranz_id = KRT_Log_Page.Tranz_id
	left join KRT_Log_StoredProcedure on KRT_Log_StoredProcedure.Tranz_id = KRT_Log_WebService.Tranz_id
	left join (select dbo.krt_kodtarak.kod, dbo.krt_kodtarak.nev from dbo.krt_kodtarak 
	inner join dbo.krt_kodcsoportok on dbo.krt_kodcsoportok.id = dbo.krt_kodtarak.kodcsoport_id
	where dbo.krt_kodcsoportok.kod = ''HELYETTESITES_MOD''
	and dbo.krt_kodtarak.Org= ''' + CAST(@Org as NVarChar(40)) +'''
    and GETDATE() between dbo.KRT_KodTarak.ErvKezd and dbo.KRT_KodTarak.ErvVege
	) kodtarnev 
	on kodtarnev.kod = KRT_Log_Login.Helyettesites_Mod collate Hungarian_CI_AS'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

    if @Where_Log_Login is not null and @Where_Log_Login!=''
		begin
			if @Where is not null and @Where!=''
				begin
					SET @sqlcmd = @sqlcmd + ' AND ' + @Where_Log_Login
				end
			else
				begin
				SET @sqlcmd = @sqlcmd + ' Where ' + @Where_Log_Login
				end
		end

    if @Where_Log_Page is not null and @Where_Log_Page!=''
		begin
			if (@Where is not null and @Where!='') or (@Where_Log_Login is not null and @Where_Log_Login!='')
				begin
				SET @sqlcmd = @sqlcmd + ' AND ' + @Where_Log_Page
				end
			else
				begin
				SET @sqlcmd = @sqlcmd + ' Where ' + @Where_Log_Page
				end
		end
   
   
    if   @Where_Log_WebService is not null and @Where_Log_WebService !=''
		begin
			if (@Where is not null and @Where!='') or (@Where_Log_Login is not null and @Where_Log_Login!='') or (@Where_Log_Page is not null and @Where_Log_Page!='')
				begin
				SET @sqlcmd = @sqlcmd + ' AND ' + @Where_Log_WebService
				end
			else
				begin
				SET @sqlcmd = @sqlcmd + ' Where ' + @Where_Log_WebService
				end
		end
       

	if   @Where_Log_StoredProcedure is not null and @Where_Log_StoredProcedure !=''
		begin
			if (@Where is not null and @Where!='') or (@Where_Log_Login is not null and @Where_Log_Login!='') or (@Where_Log_Page is not null and @Where_Log_Page!='') or (@Where_Log_WebService is not null and @Where_Log_WebService !='')
				begin
				SET @sqlcmd = @sqlcmd + ' AND ' + @Where_Log_StoredProcedure
				end
			else
				begin
				SET @sqlcmd = @sqlcmd + ' Where ' + @Where_Log_StoredProcedure
				end
		end

	SET @sqlcmd = @sqlcmd + ' ORDER BY KRT_Log_Login.Date DESC, KRT_Log_Page.Date ASC, KRT_Log_WebService.Date ASC, KRT_Log_StoredProcedure.Date ASC' 

	exec(@sqlcmd)

END TRY
BEGIN CATCH
DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH
END


GO

