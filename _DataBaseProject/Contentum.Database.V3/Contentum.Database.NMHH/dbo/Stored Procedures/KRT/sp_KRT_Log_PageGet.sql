﻿create procedure [dbo].[sp_KRT_Log_PageGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Log_Page.Id,
	   KRT_Log_Page.ModulId,
	   KRT_Log_Page.LoginId,
	   KRT_Log_Page.Name,
	   KRT_Log_Page.Url,
	   KRT_Log_Page.PrevUrl,
	   KRT_Log_Page.QueryString,
	   KRT_Log_Page.Command,
	   KRT_Log_Page.IsPostBack,
	   KRT_Log_Page.IsAsync,
	   KRT_Log_Page.StartDate,
	   KRT_Log_Page.EndDate,
	   KRT_Log_Page.HibaKod,
	   KRT_Log_Page.HibaUzenet,
	   KRT_Log_Page.Severity,
	   KRT_Log_Page.Ver,
	   KRT_Log_Page.Note,
	   KRT_Log_Page.Letrehozo_id,
	   KRT_Log_Page.Tranz_id
	   from 
		 KRT_Log_Page as KRT_Log_Page 
	   where
		 KRT_Log_Page.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end