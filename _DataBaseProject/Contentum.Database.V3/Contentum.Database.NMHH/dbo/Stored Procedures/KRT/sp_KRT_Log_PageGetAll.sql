﻿create procedure [dbo].[sp_KRT_Log_PageGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Log_Page.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Log_Page.Date,
	   KRT_Log_Page.Status,
	   KRT_Log_Page.StartDate,
	   KRT_Log_Page.Modul_Id,
	   KRT_Log_Page.Login_Tranz_id,
	   KRT_Log_Page.Name,
	   KRT_Log_Page.Url,
	   KRT_Log_Page.QueryString,
	   KRT_Log_Page.Command,
	   KRT_Log_Page.IsPostBack,
	   KRT_Log_Page.IsAsync,
	   KRT_Log_Page.Level,
	   KRT_Log_Page.Letrehozo_id,
	   KRT_Log_Page.Tranz_id,
	   KRT_Log_Page.Message,
	   KRT_Log_Page.HibaKod,
	   KRT_Log_Page.HibaUzenet  
   from 
     KRT_Log_Page as KRT_Log_Page      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end