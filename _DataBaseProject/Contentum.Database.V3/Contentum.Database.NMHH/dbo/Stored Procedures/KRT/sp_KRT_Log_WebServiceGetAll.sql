﻿create procedure [dbo].[sp_KRT_Log_WebServiceGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Log_WebService.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Log_WebService.Date,
	   KRT_Log_WebService.Status,
	   KRT_Log_WebService.StartDate,
	   KRT_Log_WebService.ParentWS_StartDate,
	   KRT_Log_WebService.Machine,
	   KRT_Log_WebService.Url,
	   KRT_Log_WebService.Name,
	   KRT_Log_WebService.Method,
	   KRT_Log_WebService.Level,
	   KRT_Log_WebService.Letrehozo_id,
	   KRT_Log_WebService.Tranz_id,
	   KRT_Log_WebService.Message,
	   KRT_Log_WebService.HibaKod,
	   KRT_Log_WebService.HibaUzenet  
   from 
     KRT_Log_WebService as KRT_Log_WebService      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end