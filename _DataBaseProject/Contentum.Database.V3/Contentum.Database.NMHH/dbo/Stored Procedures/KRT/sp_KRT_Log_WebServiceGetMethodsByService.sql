CREATE PROCEDURE sp_KRT_Log_WebServiceGetMethodsByService
  @Where nvarchar(4000) = '',
  @ExecutorUserId uniqueidentifier
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 'select row_number() over (order by ws.Method asc) as Id, Method from (select distinct KRT_Log_WebService.Method from KRT_Log_WebService' 
	+ case when @Where = '' then '' else ' where ' + @Where end + ') as ws'
	exec sp_executesql @sqlcmd;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

GO
