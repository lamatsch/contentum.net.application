﻿create procedure [dbo].[sp_KRT_Log_WebServiceInsert]    
                @Id      uniqueidentifier = null,    
                               @PageId     uniqueidentifier  = null,
                @PrevId     uniqueidentifier  = null,
                @Url     Nvarchar(4000)  = null,
                @MethodName     Nvarchar(100)  = null,
                @StartDate     datetime  = null,
                @EndDate     datetime  = null,
                @HibaKod     Nvarchar(4000)  = null,
                @HibaUzenet     Nvarchar(4000)  = null,
                @ServiceName     Nvarchar(100)  = null,
                @Severity     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
                @Tranz_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = ''

		 IF(@Id IS NOT NULL)
		 BEGIN
			SET @insertColumns = 'Id'
			SET @insertValues = '@Id'
		 END 
       
         if @PageId is not null
         begin
            SET @insertColumns = @insertColumns + ',PageId'
            SET @insertValues = @insertValues + ',@PageId'
         end 
       
         if @PrevId is not null
         begin
            SET @insertColumns = @insertColumns + ',PrevId'
            SET @insertValues = @insertValues + ',@PrevId'
         end 
       
         if @Url is not null
         begin
            SET @insertColumns = @insertColumns + ',Url'
            SET @insertValues = @insertValues + ',@Url'
         end 
       
         if @MethodName is not null
         begin
            SET @insertColumns = @insertColumns + ',MethodName'
            SET @insertValues = @insertValues + ',@MethodName'
         end 
       
         if @StartDate is not null
         begin
            SET @insertColumns = @insertColumns + ',StartDate'
            SET @insertValues = @insertValues + ',@StartDate'
         end 
       
         if @EndDate is not null
         begin
            SET @insertColumns = @insertColumns + ',EndDate'
            SET @insertValues = @insertValues + ',@EndDate'
         end 
       
         if @HibaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaKod'
            SET @insertValues = @insertValues + ',@HibaKod'
         end 
       
         if @HibaUzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaUzenet'
            SET @insertValues = @insertValues + ',@HibaUzenet'
         end 
       
         if @ServiceName is not null
         begin
            SET @insertColumns = @insertColumns + ',ServiceName'
            SET @insertValues = @insertValues + ',@ServiceName'
         end 
       
         if @Severity is not null
         begin
            SET @insertColumns = @insertColumns + ',Severity'
            SET @insertValues = @insertValues + ',@Severity'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end      

IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )               

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Log_WebService ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@PageId uniqueidentifier,@PrevId uniqueidentifier,@Url Nvarchar(4000),@MethodName Nvarchar(100),@StartDate datetime,@EndDate datetime,@HibaKod Nvarchar(4000),@HibaUzenet Nvarchar(4000),@ServiceName Nvarchar(100),@Severity int,@Ver int,@Note Nvarchar(4000),@Letrehozo_id uniqueidentifier,@Tranz_id uniqueidentifier, @ResultUid uniqueidentifier OUTPUT'
     ,@Id = @Id,@PageId = @PageId,@PrevId = @PrevId,@Url = @Url,@MethodName = @MethodName,@StartDate = @StartDate,@EndDate = @EndDate,@HibaKod = @HibaKod,@HibaUzenet = @HibaUzenet,@ServiceName = @ServiceName,@Severity = @Severity,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@Tranz_id = @Tranz_id, @ResultUid = @ResultUid OUTPUT

If @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH