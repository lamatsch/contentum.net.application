﻿create procedure [dbo].[sp_KRT_MappaSzuleiGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_MappaSzulei.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_MappaSzulei.Id,
	   KRT_MappaSzulei.Mappa_Id,
	   KRT_MappaSzulei.Mappa_Id_Szulo,
	   KRT_MappaSzulei.Ver,
	   KRT_MappaSzulei.Note,
	   KRT_MappaSzulei.Stat_id,
	   KRT_MappaSzulei.ErvKezd,
	   KRT_MappaSzulei.ErvVege,
	   KRT_MappaSzulei.Letrehozo_id,
	   KRT_MappaSzulei.LetrehozasIdo,
	   KRT_MappaSzulei.Modosito_id,
	   KRT_MappaSzulei.ModositasIdo,
	   KRT_MappaSzulei.Zarolo_id,
	   KRT_MappaSzulei.ZarolasIdo,
	   KRT_MappaSzulei.Tranz_id,
	   KRT_MappaSzulei.UIAccessLog_id  
   from 
     KRT_MappaSzulei as KRT_MappaSzulei      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end