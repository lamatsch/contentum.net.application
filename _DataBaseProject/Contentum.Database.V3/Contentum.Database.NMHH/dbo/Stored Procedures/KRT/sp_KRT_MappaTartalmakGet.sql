﻿create procedure [dbo].[sp_KRT_MappaTartalmakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_MappaTartalmak.Id,
	   KRT_MappaTartalmak.Mappa_Id,
	   KRT_MappaTartalmak.Obj_Id,
	   KRT_MappaTartalmak.Obj_type,
	   KRT_MappaTartalmak.Leiras,
	   KRT_MappaTartalmak.Ver,
	   KRT_MappaTartalmak.Note,
	   KRT_MappaTartalmak.Stat_id,
	   KRT_MappaTartalmak.ErvKezd,
	   KRT_MappaTartalmak.ErvVege,
	   KRT_MappaTartalmak.Letrehozo_id,
	   KRT_MappaTartalmak.LetrehozasIdo,
	   KRT_MappaTartalmak.Modosito_id,
	   KRT_MappaTartalmak.ModositasIdo,
	   KRT_MappaTartalmak.Zarolo_id,
	   KRT_MappaTartalmak.ZarolasIdo,
	   KRT_MappaTartalmak.Tranz_id,
	   KRT_MappaTartalmak.UIAccessLog_id
	   from 
		 KRT_MappaTartalmak as KRT_MappaTartalmak 
	   where
		 KRT_MappaTartalmak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end