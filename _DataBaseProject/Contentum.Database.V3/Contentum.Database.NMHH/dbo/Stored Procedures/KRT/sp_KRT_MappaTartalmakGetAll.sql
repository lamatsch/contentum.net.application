﻿create procedure [dbo].[sp_KRT_MappaTartalmakGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_MappaTartalmak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_MappaTartalmak.Id,
	   KRT_MappaTartalmak.Mappa_Id,
	   KRT_MappaTartalmak.Obj_Id,
	   KRT_MappaTartalmak.Obj_type,
	   KRT_MappaTartalmak.Leiras,
	   KRT_MappaTartalmak.Ver,
	   KRT_MappaTartalmak.Note,
	   KRT_MappaTartalmak.Stat_id,
	   KRT_MappaTartalmak.ErvKezd,
	   KRT_MappaTartalmak.ErvVege,
	   KRT_MappaTartalmak.Letrehozo_id,
	   KRT_MappaTartalmak.LetrehozasIdo,
	   KRT_MappaTartalmak.Modosito_id,
	   KRT_MappaTartalmak.ModositasIdo,
	   KRT_MappaTartalmak.Zarolo_id,
	   KRT_MappaTartalmak.ZarolasIdo,
	   KRT_MappaTartalmak.Tranz_id,
	   KRT_MappaTartalmak.UIAccessLog_id  
   from 
     KRT_MappaTartalmak as KRT_MappaTartalmak      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end