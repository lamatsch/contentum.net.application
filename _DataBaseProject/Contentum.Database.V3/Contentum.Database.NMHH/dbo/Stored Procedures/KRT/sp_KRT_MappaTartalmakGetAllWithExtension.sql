﻿create procedure [dbo].[sp_KRT_MappaTartalmakGetAllWithExtension]
  @Where					nvarchar(MAX) = '', 
  @OrderBy					nvarchar(200) = ' order by   KRT_MappaTartalmak.LetrehozasIdo',
  @TopRow					nvarchar(5) = '',
  @ExecutorUserId			uniqueidentifier,
  @FelhasznaloSzervezet_Id	UNIQUEIDENTIFIER,
  @Jogosultak				CHAR(1) = '0'
  

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_MappaTartalmak.Id,
	   KRT_MappaTartalmak.Mappa_Id,
	   KRT_Mappak.Nev as Mappa_Nev,
	   KRT_Csoportok.Nev as Mappa_Tulajdonos_Nev,
	   KRT_KodTarak.Nev as Mappa_Tipus,
	   KRT_Mappak.Barcode as Mappa_Barcode,
	   KRT_MappaTartalmak.Obj_Id,
	   KRT_MappaTartalmak.Obj_type,
	   KRT_MappaTartalmak.Leiras,
	   KRT_MappaTartalmak.Ver,
	   KRT_MappaTartalmak.Note,
	   KRT_MappaTartalmak.Stat_id,
	   KRT_MappaTartalmak.ErvKezd,
	   KRT_MappaTartalmak.ErvVege,
	   KRT_MappaTartalmak.Letrehozo_id,
	   KRT_MappaTartalmak.LetrehozasIdo,
	   KRT_MappaTartalmak.Modosito_id,
	   KRT_MappaTartalmak.ModositasIdo,
	   KRT_MappaTartalmak.Zarolo_id,
	   KRT_MappaTartalmak.ZarolasIdo,
	   KRT_MappaTartalmak.Tranz_id,
	   KRT_MappaTartalmak.UIAccessLog_id  
   from 
     KRT_MappaTartalmak as KRT_MappaTartalmak
		inner join KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			and GETDATE() BETWEEN KRT_Mappak.ErvKezd and KRT_Mappak.ErvVege '

    IF @Jogosultak IS NOT NULL AND @Jogosultak != '0'
    BEGIN 
		SET @sqlcmd = @sqlcmd +
		'
			AND
			(
				KRT_Mappak.Id IN (SELECT Obj_Id from KRT_Jogosultak WHERE Csoport_Id_Jogalany IN (''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + '''))
				OR
				KRT_Mappak.Csoport_Id_Tulaj IN (''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
			)
		'
    END
    
	SET @sqlcmd = @sqlcmd +
		'
		left join KRT_Csoportok on KRT_Csoportok.Id = KRT_Mappak.Csoport_Id_Tulaj
		left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''MAPPA_TIPUS''
			left join KRT_KodTarak on KRT_KodTarak.Kodcsoport_Id = KRT_Kodcsoportok.Id
				AND KRT_KodTarak.Kod = KRT_Mappak.Tipus and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
           '
    
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

   SET @sqlcmd = @sqlcmd + @OrderBy

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end