﻿CREATE PROCEDURE [dbo].[sp_KRT_MappaTartalmakInsertTomeges]
	@Ugyirat_Ids			nvarchar(MAX) = NULL,
	@IratPeldany_Ids		nvarchar(MAX) = NULL,
	@Kuldemeny_Ids			nvarchar(MAX) = NULL,
	@Mappa_Id				uniqueidentifier,
	@Obj_type				nvarchar(100),
	@Leiras					nvarchar(400),
	@ExecutorUserId			uniqueidentifier,
	@TranzId				uniqueidentifier = NULL
AS
BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

	declare @execTime DateTime
	set @execTime = getdate();

	declare @insertedRows table(id uniqueidentifier)

	declare @it	int;
	declare @curId	nvarchar(36);

	-- Ellenorzés fizikai mappa esetén, hogy mozgásban van-e
	IF EXISTS
	(
		SELECT 1 FROM dbo.fn_GetDosszieStatus(@Mappa_Id) AS Statusz
			WHERE Statusz.Tipus = '01'
				AND Statusz.Allapot = 1
	)
	BEGIN
		RAISERROR('[64030]',16,1);
	END
	
	-- Ügyiratok
	declare @ugyiratTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@Ugyirat_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ugyirat_Ids,@it*39+2,37);
		insert into @ugyiratTable(id) values(@curId);
		set @it = @it + 1;
	END
	
	-- Iratpéldányok
	declare @iratPeldanyTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@IratPeldany_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@IratPeldany_Ids,@it*39+2,37);
		insert into @iratPeldanyTable(id) values(@curId);
		set @it = @it + 1;
	END
	
	-- Küldemények
	declare @kuldemenyTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@Kuldemeny_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Kuldemeny_Ids,@it*39+2,37);
		insert into @kuldemenyTable(id) values(@curId);
		set @it = @it + 1;
	END


	if @tranzId IS NULL
	BEGIN 
		set @tranzId = newid();
	END
	
	
	-- Ellenorzés, nincs-e már benne az objektum a mappában
	-- Ügyiratok
	IF EXISTS
	(
		SELECT 1 FROM dbo.fn_GetAllUgyirat_InDosszie(@Mappa_Id,@Ugyirat_Ids)
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllUgyirat_InDosszie(@Mappa_Id,@Ugyirat_Ids)
		
		RAISERROR('[64002]',16,1);
	END
	-- Kuldemenyek
	IF EXISTS
	(
		SELECT 1 FROM dbo.fn_GetAllKuldemeny_InDosszie(@Mappa_Id,@Kuldemeny_Ids)
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllKuldemeny_InDosszie(@Mappa_Id,@Kuldemeny_Ids)
		
		RAISERROR('[64012]',16,1);
	END
	-- Iratpéldány
	IF EXISTS
	(
		SELECT 1 FROM dbo.fn_GetAllIratPeldany_InDosszie(@Mappa_Id,@IratPeldany_Ids)
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllIratPeldany_InDosszie(@Mappa_Id,@IratPeldany_Ids)
		
		RAISERROR('[64022]',16,1);
	END
	

	-- Ellenorzések fizikai mappa esetén
	IF EXISTS (SELECT 1 FROM KRT_Mappak WHERE Id = @Mappa_Id AND Tipus = '01')
	BEGIN

		-- Ellenorzés, nincs-e benne már más fizikai mappában
		-- Ügyiratok
		IF EXISTS
		(
			SELECT 1 FROM dbo.KRT_MappaTartalmak t
				inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
				where m.Tipus = '01'
				and t.Obj_Id in (select id from @ugyiratTable)
				and getdate() between t.ErvKezd and t.ErvVege
				and getdate() between m.ErvKezd and m.ErvVege
		)
		BEGIN
			SELECT Id from @ugyiratTable ut
				where exists(
				SELECT 1 FROM dbo.KRT_MappaTartalmak t
					inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
					where m.Tipus = '01'
					and t.Obj_Id = ut.Id
					and getdate() between t.ErvKezd and t.ErvVege
					and getdate() between m.ErvKezd and m.ErvVege
				)
			-- Az ügyirat már benne van más fizikai dossziéban! Egyidejuleg csak egy fizikai dosszié tartalmazhatja a tételt.
			RAISERROR('[64041]',16,1);
		END
		-- Kuldemenyek
		IF EXISTS
		(
			SELECT 1 FROM dbo.KRT_MappaTartalmak t
				inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
				where m.Tipus = '01'
				and t.Obj_Id in (select id from @kuldemenyTable)
				and getdate() between t.ErvKezd and t.ErvVege
				and getdate() between m.ErvKezd and m.ErvVege
		)
		BEGIN
			SELECT Id from @kuldemenyTable kt
				where exists(
				SELECT 1 FROM dbo.KRT_MappaTartalmak t
					inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
					where m.Tipus = '01'
					and t.Obj_Id = kt.Id
					and getdate() between t.ErvKezd and t.ErvVege
					and getdate() between m.ErvKezd and m.ErvVege
				)
			-- A küldemény már benne van más fizikai dossziéban! Egyidejuleg csak egy fizikai dosszié tartalmazhatja a tételt.
			RAISERROR('[64042]',16,1);
		END
		-- Iratpéldány
		IF EXISTS
		(
			SELECT 1 FROM dbo.KRT_MappaTartalmak t
				inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
				where m.Tipus = '01'
				and t.Obj_Id in (select id from @iratPeldanyTable)
				and getdate() between t.ErvKezd and t.ErvVege
				and getdate() between m.ErvKezd and m.ErvVege
		)
		BEGIN
			SELECT Id from @iratPeldanyTable it
				where exists(
				SELECT 1 FROM dbo.KRT_MappaTartalmak t
					inner join dbo.KRT_Mappak m on t.Mappa_Id = m.Id
					where m.Tipus = '01'
					and t.Obj_Id = it.Id
					and getdate() between t.ErvKezd and t.ErvVege
					and getdate() between m.ErvKezd and m.ErvVege
				)
			-- Az iratpéldány már benne van más fizikai dossziéban! Egyidejuleg csak egy fizikai dosszié tartalmazhatja a tételt.
			RAISERROR('[64043]',16,1);
		END




		-- Ügyiratok
		IF EXISTS
		(
			SELECT 1
				FROM @ugyiratTable AS ugyiratTemp
					INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = ugyiratTemp.id
				WHERE EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT ugyiratTemp.Id
				FROM @ugyiratTable AS ugyiratTemp
					INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = ugyiratTemp.id
				WHERE EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64003]',16,1);
		END
		-- Kuldemeny
		IF EXISTS
		(
			SELECT 1
				FROM @kuldemenyTable AS kuldemenyTemp
					INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = kuldemenyTemp.id
				WHERE EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT kuldemenyTemp.Id
				FROM @kuldemenyTable AS kuldemenyTemp
					INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = kuldemenyTemp.id
				WHERE EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64013]',16,1);
		END
		-- Iratpeldany
		IF EXISTS
		(
			SELECT 1
				FROM @iratPeldanyTable AS iratpeldanyTemp
					INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = iratpeldanyTemp.id
				WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT iratpeldanyTemp.Id
				FROM @iratPeldanyTable AS iratpeldanyTemp
					INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = iratpeldanyTemp.id
				WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64023]',16,1);
		END

	END
	
	
	-- Ügyiratok berakása a dossziéba
	INSERT INTO KRT_MappaTartalmak (Obj_Id,Mappa_Id,Obj_type,Leiras,Tranz_id,Letrehozo_id,LetrehozasIdo)
	SELECT
		id,
		@Mappa_Id,
		'EREC_UgyUgyiratok',
		@Leiras,
		@tranzId,
		@ExecutorUserId,
		@execTime
		FROM @ugyiratTable;
		
	-- Kuldemenyek berakása a dossziéba
	INSERT INTO KRT_MappaTartalmak (Obj_Id,Mappa_Id,Obj_type,Leiras,Tranz_id,Letrehozo_id,LetrehozasIdo)
	SELECT
		id,
		@Mappa_Id,
		'EREC_KuldKuldemenyek',
		@Leiras,
		@tranzId,
		@ExecutorUserId,
		@execTime
		FROM @kuldemenyTable;

	-- Iratpeldanyok berakása a dossziéba
	INSERT INTO KRT_MappaTartalmak (Obj_Id,Mappa_Id,Obj_type,Leiras,Tranz_id,Letrehozo_id,LetrehozasIdo)
	SELECT
		id,
		@Mappa_Id,
		'EREC_PldIratPeldanyok',
		@Leiras,
		@tranzId,
		@ExecutorUserId,
		@execTime
		FROM @iratPeldanyTable;



	/* History Log */
	declare @row_ids varchar(MAX)
	/*** KRT_MappaTartalmak history log ***/

	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from KRT_MappaTartalmak where Tranz_id = @tranzId FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'KRT_MappaTartalmak'
	,@Row_Ids = @row_ids
	,@Muvelet = 0
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @execTime


--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH