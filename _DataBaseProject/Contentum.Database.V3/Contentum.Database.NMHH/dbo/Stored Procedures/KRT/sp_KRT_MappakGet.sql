﻿create procedure [dbo].[sp_KRT_MappakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Mappak.Id,
	   KRT_Mappak.SzuloMappa_Id,
	   KRT_Mappak.BarCode,
	   KRT_Mappak.Tipus,
	   KRT_Mappak.Nev,
	   KRT_Mappak.Leiras,
	   KRT_Mappak.Csoport_Id_Tulaj,
	   KRT_Mappak.Ver,
	   KRT_Mappak.Note,
	   KRT_Mappak.Stat_id,
	   KRT_Mappak.ErvKezd,
	   KRT_Mappak.ErvVege,
	   KRT_Mappak.Letrehozo_id,
	   KRT_Mappak.LetrehozasIdo,
	   KRT_Mappak.Modosito_id,
	   KRT_Mappak.ModositasIdo,
	   KRT_Mappak.Zarolo_id,
	   KRT_Mappak.ZarolasIdo,
	   KRT_Mappak.Tranz_id,
	   KRT_Mappak.UIAccessLog_id
	   from 
		 KRT_Mappak as KRT_Mappak 
	   where
		 KRT_Mappak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end