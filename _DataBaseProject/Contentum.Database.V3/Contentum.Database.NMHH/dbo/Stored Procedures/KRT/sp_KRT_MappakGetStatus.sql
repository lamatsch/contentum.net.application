﻿CREATE PROCEDURE [dbo].[sp_KRT_MappakGetStatus]
	@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SELECT * FROM dbo.fn_GetDosszieStatus(@Id);
END