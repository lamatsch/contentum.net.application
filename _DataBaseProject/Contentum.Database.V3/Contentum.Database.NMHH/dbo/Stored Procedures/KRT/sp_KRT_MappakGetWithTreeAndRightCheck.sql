﻿create procedure [dbo].[sp_KRT_MappakGetWithTreeAndRightCheck]
		@Id uniqueidentifier,
		@ExecutorUserId UNIQUEIDENTIFIER,
		@FelhasznaloSzervezet_Id	uniqueidentifier
as

begin
BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 and not exists
	if dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 and not exists
	(
		SELECT 1 FROM KRT_Mappak
			
			where KRT_Mappak.Id = @Id AND KRT_Mappak.Csoport_Id_Tulaj IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
	) AND NOT EXISTS
	(
		SELECT 1 FROM KRT_Mappak
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
			where KRT_Mappak.Id = @Id AND krt_jogosultak.Csoport_Id_Jogalany IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
	)
		RAISERROR('[50102]',16,1);
	
	WITH temp AS
	(
		SELECT KRT_Mappak.Id, KRT_Mappak.SzuloMappa_Id, KRT_Mappak.Nev, KRT_Mappak.Tipus, KRT_Mappak.LetrehozasIdo
			FROM KRT_Mappak
			WHERE Id = @Id-- AND KRT_Mappak.Csoport_Id_Tulaj IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)

		UNION ALL

		SELECT KRT_Mappak.Id, KRT_Mappak.SzuloMappa_Id, KRT_Mappak.Nev, KRT_Mappak.Tipus, KRT_Mappak.LetrehozasIdo
			FROM KRT_Mappak
				INNER JOIN temp ON temp.SzuloMappa_Id = KRT_Mappak.Id
			WHERE KRT_Mappak.Csoport_Id_Tulaj IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
				OR KRT_Mappak.Id IN (SELECT Obj_Id FROM KRT_Jogosultak WHERE Csoport_Id_Jogalany IN (@ExecutorUserId,@FelhasznaloSzervezet_Id))
	)
	SELECT Id, SzuloMappa_Id, Nev, Tipus, LetrehozasIdo FROM temp;

	-- Org szurés
	IF not exists(select 1 from KRT_Csoportok
		where KRT_Csoportok.Org=@Org
		and KRT_Csoportok.Id=
		(select KRT_Mappak.Csoport_Id_Tulaj from KRT_Mappak
		where KRT_Mappak.Id=@Id
		))
	BEGIN
		RAISERROR('[50101]',16,1)
	END

	SELECT * FROM KRT_Mappak WHERE Id = @Id


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end