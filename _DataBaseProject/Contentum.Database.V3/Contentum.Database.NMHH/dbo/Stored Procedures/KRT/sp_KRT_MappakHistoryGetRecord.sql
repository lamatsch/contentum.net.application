﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_MappakHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_MappakHistoryGetRecord
go
*/
create procedure sp_KRT_MappakHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_MappakHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_MappakHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuloMappa_Id' as ColumnName,               
               cast(Old.SzuloMappa_Id as nvarchar(99)) as OldValue,
               cast(New.SzuloMappa_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuloMappa_Id as nvarchar(max)),'') != ISNULL(CAST(New.SzuloMappa_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               
               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Tulaj' as ColumnName,               
               cast(Old.Csoport_Id_Tulaj as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Tulaj as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_MappakHistory Old
         inner join KRT_MappakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_MappakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Tulaj as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Tulaj as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go