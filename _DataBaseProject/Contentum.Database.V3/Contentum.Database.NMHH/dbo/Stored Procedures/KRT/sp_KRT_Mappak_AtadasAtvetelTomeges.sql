﻿create procedure [dbo].[sp_KRT_Mappak_AtadasAtvetelTomeges]
	@Ids				nvarchar(MAX),
	@KovetkezoFelelos	uniqueidentifier = null,
	@KovetkezoOrzo		uniqueidentifier = null,
	@ExecutorUserId		uniqueidentifier
as

BEGIN TRY
	set nocount on
	declare @execTime datetime;
	declare @tempTable table(id uniqueidentifier);--table(id uniqueidentifier, ver int);

	set @execTime = getdate();

	-- temp tábla töltése az idkal
	declare @it	int;
	declare @curId	nvarchar(36);
	declare @sqlcmd	nvarchar(500);

	set @it = 0;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		insert into @tempTable(id) values(@curId);
		set @it = @it + 1;
	END

	-- Ellenorzés: csak fizikai dossziék adhatók át!
	if exists(select 1 from KRT_Mappak
		where Id in (select Id from @tempTable) and Tipus = '02' -- virtualis
	)
	begin
		select Id from KRT_Mappak
			where Id in (select Id from @tempTable)
				and Tipus = '02' -- virtualis
		-- Csak fizikai dossziékat lehet átadni!
		RAISERROR('[64046]', 16,1)
	end

	-- Ellenorzés: Fizikai dossziéban lévo dosszié nem adható át, csak szülojével együtt!
	declare @tempTableFizikaiSzulo table(id uniqueidentifier)
	;with SzuloHierarchy as
	(
		-- Base case
		select Id, SzuloMappa_Id, Tipus
			from KRT_Mappak where Id in (select SzuloMappa_Id from KRT_Mappak where Id in (select Id from @tempTable))

		UNION ALL

		-- Recursive step
		select szh.Id, m.SzuloMappa_Id, m.Tipus
			FROM KRT_Mappak m
			INNER JOIN SzuloHierarchy szh
			on szh.SzuloMappa_Id = m.Id		
			and szh.Tipus = '02' -- virtualis
			and getdate() between m.ErvKezd and m.ErvVege
	)
	insert into @tempTableFizikaiSzulo
		select distinct Id from SzuloHierarchy
		where Tipus = '01' -- fizikai

	if exists(select 1 from @tempTableFizikaiSzulo)
	begin
		select Id from @tempTableFizikaiSzulo

		-- Fizikai dossziéban lévo dosszié nem adható át, csak szülojével együtt!
		-- Vegye ki a dossziét a tartalmazó fizikai dossziéból,
		-- vagy adja át azt a tartalmazó fizikai dossziéval együtt!
		RAISERROR('[64045]', 16,1)
	end

	-- Hierarchia együttmozgatása:
	declare @tempTableWithChildren table(id uniqueidentifier)
	;WITH MappaHierarchy
	as	(
		-- Base case
--		select m.Id
--		from KRT_Mappak m
--			where m.SzuloMappa_Id in (select Id from @tempTable)
--			and getdate() between m.ErvKezd and m.ErvVege
		select Id from @tempTable

		UNION ALL

		-- Recursive step
		select m.Id
		from KRT_Mappak m
			inner join MappaHierarchy mh
			on mh.Id = m.SzuloMappa_Id
			where getdate() between m.ErvKezd and m.ErvVege
	)
	insert into @tempTableWithChildren(id)
		select mh.Id from MappaHierarchy mh --where mh.Id not in (select Id from @tempTable)

	IF (@KovetkezoFelelos IS NOT NULL)
	BEGIN
		-- Ellenorzés (csak átadáskor): szerelt ügyiratok csak szülojükkel együtt adhatók át:
		-- Ügyiratok összeszedése
		IF exists(
			SELECT 1
				FROM KRT_MappaTartalmak
					INNER JOIN EREC_UgyUgyiratok u ON u.Id = KRT_MappaTartalmak.Obj_Id
				WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
					AND KRT_MappaTartalmak.Obj_Type = 'EREC_UgyUgyiratok'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND u.Allapot = '60' -- Szerelt
					AND u.UgyUgyirat_Id_Szulo not in (SELECT EREC_UgyUgyiratok.Id
						FROM KRT_MappaTartalmak
							INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
						WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
							AND KRT_MappaTartalmak.Obj_Type = 'EREC_UgyUgyiratok'
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					)
					
		)
		BEGIN
			-- A dosszié nem adható át, mert szerelt ügyiratokat tartalmaz utóiratuk nélkül!
			RAISERROR('[64036]', 16, 1)
		END

		-- Ellenorzés (csak átadáskor): csak a felhasználónál lévo tételek adhatók át
		-- Ügyiratok
		IF exists(
			SELECT 1
				FROM KRT_MappaTartalmak
					INNER JOIN EREC_UgyUgyiratok u ON u.Id = KRT_MappaTartalmak.Obj_Id
				WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
					AND KRT_MappaTartalmak.Obj_Type = 'EREC_UgyUgyiratok'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND (u.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId OR u.Csoport_Id_felelos != @ExecutorUserId)
		)
		BEGIN
			-- A dossziét nem adhatja át, mert olyan ügyiratokat tartalmaz, melyek nem Önnél vannak!
			RAISERROR('[64047]', 16, 1)
		END

		-- Kuüldemények
		IF exists(
			SELECT 1
				FROM KRT_MappaTartalmak
					INNER JOIN EREC_KuldKuldemenyek k ON k.Id = KRT_MappaTartalmak.Obj_Id
				WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
					AND KRT_MappaTartalmak.Obj_Type = 'EREC_KuldKuldemenyek'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND (k.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId OR k.Csoport_Id_felelos != @ExecutorUserId)
		)
		BEGIN
			-- A dossziét nem adhatja át, mert olyan küldeményeket tartalmaz, melyek nem Önnél vannak!
			RAISERROR('[64048]', 16, 1)
		END

		-- Iratpéldányok
		IF exists(
			SELECT 1
				FROM KRT_MappaTartalmak
					INNER JOIN EREC_PldIratPeldanyok p ON p.Id = KRT_MappaTartalmak.Obj_Id
				WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
					AND KRT_MappaTartalmak.Obj_Type = 'EREC_PldIratPeldanyok'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND (p.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId OR p.Csoport_Id_felelos != @ExecutorUserId)
		)
		BEGIN
			-- A dossziét nem adhatja át, mert olyan iratpéldányokat tartalmaz, melyek nem Önnél vannak!
			RAISERROR('[64049]', 16, 1)
		END
	END
	
	DECLARE @ugyiratIds NVARCHAR(MAX)
	DECLARE @ugyiratVers NVARCHAR(MAX)
	DECLARE @kuldemenyIds NVARCHAR(MAX)
	DECLARE @kuldemenyVers NVARCHAR(MAX)
	DECLARE @iratpeldanyIds NVARCHAR(MAX)
	DECLARE @iratpeldanyVers NVARCHAR(MAX)
	
	SET @ugyiratIds = '''';
	SET @ugyiratVers = '';
	SET @kuldemenyIds = '''';
	SET @kuldemenyVers = '';
	SET @iratpeldanyIds = '''';
	SET @iratpeldanyVers = '';

	-- Ügyiratok összeszedése
	SELECT	@ugyiratIds = @ugyiratIds + CAST(EREC_UgyUgyiratok.Id AS CHAR(36)) + ''',''',
			@ugyiratVers = @ugyiratVers + CAST(EREC_UgyUgyiratok.Ver AS NVARCHAR(MAX)) + ','
		FROM KRT_MappaTartalmak
			INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
		WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
			AND KRT_MappaTartalmak.Obj_Type = 'EREC_UgyUgyiratok'
			AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	
	IF (LEN(@ugyiratIds) > 1)
	BEGIN
		SET @ugyiratIds = LEFT(@ugyiratIds, LEN(@ugyiratIds)-2);
		SET @ugyiratVers = LEFT(@ugyiratVers, LEN(@ugyiratVers)-1);	
	END
	ELSE
	BEGIN
		SET @ugyiratIds = NULL;
		SET @ugyiratVers = NULL;
	END
	

	IF (@ugyiratIds IS NOT NULL)
	BEGIN
		EXECUTE sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges
			@Ids = @ugyiratIds,
			@Vers = @ugyiratVers,
			@KovetkezoFelelos = @KovetkezoFelelos,
			@KovetkezoOrzo = @KovetkezoOrzo,
			@ExecutorUserId = @ExecutorUserId,
			@KezFeljegyzesTipus = '',
			@Leiras = '',
			@CheckMappa = 0
	END
	
	-- Küldemények összeszedése
	SELECT	@kuldemenyIds = @kuldemenyIds + CAST(EREC_KuldKuldemenyek.Id AS CHAR(36)) + ''',''',
			@kuldemenyVers = @kuldemenyVers + CAST(EREC_KuldKuldemenyek.Ver AS NVARCHAR(MAX)) + ','
		FROM KRT_MappaTartalmak
			INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
		WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
			AND KRT_MappaTartalmak.Obj_Type = 'EREC_KuldKuldemenyek'
			AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	
	IF (LEN(@kuldemenyIds) > 1)
	BEGIN
		SET @kuldemenyIds = LEFT(@kuldemenyIds, LEN(@kuldemenyIds)-2);
		SET @kuldemenyVers = LEFT(@kuldemenyVers, LEN(@kuldemenyVers)-1);	
	END
	ELSE
	BEGIN
		SET @kuldemenyIds = NULL;
		SET @kuldemenyVers = NULL;
	END
	
	IF (@kuldemenyIds IS NOT NULL)
	BEGIN
		EXECUTE sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges
			@Ids = @kuldemenyIds,
			@Vers = @kuldemenyVers,
			@KovetkezoFelelos = @KovetkezoFelelos,
			@KovetkezoOrzo = @KovetkezoOrzo,
			@ExecutorUserId = @ExecutorUserId,
			@KezFeljegyzesTipus = '',
			@Leiras = '',
			@CheckMappa = 0
	END
	
	-- Iratpéldányok összeszedése
	SELECT	@iratpeldanyIds = @iratpeldanyIds + CAST(EREC_PldIratPeldanyok.Id AS CHAR(36)) + ''',''',
			@iratpeldanyVers = @iratpeldanyVers + CAST(EREC_PldIratPeldanyok.Ver AS NVARCHAR(MAX)) + ','
		FROM KRT_MappaTartalmak
			INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
		WHERE KRT_MappaTartalmak.Mappa_Id IN (SELECT Id FROM @tempTableWithChildren)
			AND KRT_MappaTartalmak.Obj_Type = 'EREC_PldIratPeldanyok'
			AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	
	IF (LEN(@iratpeldanyIds) > 1)
	BEGIN
		SET @iratpeldanyIds = LEFT(@iratpeldanyIds, LEN(@iratpeldanyIds)-2);
		SET @iratpeldanyVers = LEFT(@iratpeldanyVers, LEN(@iratpeldanyVers)-1);	
	END
	ELSE
	BEGIN
		SET @iratpeldanyIds = NULL;
		SET @iratpeldanyVers = NULL;
	END

	IF (@iratpeldanyIds IS NOT NULL)
	BEGIN
		EXECUTE sp_EREC_PldIratPeldanyok_AtadasAtvetelTomeges
			@Ids = @iratpeldanyIds,
			@Vers = @iratpeldanyVers,
			@KovetkezoFelelos = @KovetkezoFelelos,
			@KovetkezoOrzo = @KovetkezoOrzo,
			@ExecutorUserId = @ExecutorUserId,
			@KezFeljegyzesTipus = '',
			@Leiras = '',
			@CheckMappa = 0
	END

	IF (@KovetkezoFelelos IS NOT NULL)
	BEGIN
		-- Kézbesítési tételek létrehozása
		insert into EREC_IraKezbesitesiTetelek(
				Obj_Id,
				Obj_type,
				AtadasDat,
				Felhasznalo_Id_Atado_LOGIN,
				Felhasznalo_Id_Atado_USER,
				Csoport_Id_Cel,
				Allapot)
			select
				Id,
				'KRT_Mappak',
				@execTime,
				@ExecutorUserId,
				@ExecutorUserId,
				@KovetkezoFelelos,
				'1'
			from ( SELECT Id FROM @tempTable) AS temp
		
			
		/*** EREC_IraKezbesitesiTetelek history log ***/
		INSERT INTO EREC_IraKezbesitesiTetelekHistory 
			SELECT	NEWID(),
					'0',
					@ExecutorUserId,
					@execTime,
					*
					FROM EREC_IraKezbesitesiTetelek
					WHERE Obj_id in (select id from @tempTable ) and AtadasDat = @execTime
	END
	
	IF (@KovetkezoOrzo IS NOT NULL)
	BEGIN
		-- Mappa tulaját az átvevo userre állítani
		UPDATE KRT_Mappak
			SET Csoport_Id_Tulaj = @ExecutorUserId,
				Ver = Ver + 1,
				Modosito_id = @ExecutorUserId,
				ModositasIdo = GETDATE()
			WHERE KRT_Mappak.Id IN (SELECT Id FROM @tempTableWithChildren)
			and Tipus <> '02' -- virtualis
	
	END
	
END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH