﻿create procedure [dbo].[sp_KRT_Mentett_Talalati_ListakGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Mentett_Talalati_Listak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

    SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + ' KRT_Mentett_Talalati_Listak.Id,
	   KRT_Mentett_Talalati_Listak.ListName,
	   KRT_Mentett_Talalati_Listak.SaveDate,
	   KRT_Mentett_Talalati_Listak.Searched_Table,
	    KRT_Mentett_Talalati_Listak.HeaderXML,
	   KRT_Mentett_Talalati_Listak.BodyXML,
	   KRT_Mentett_Talalati_Listak.WorkStation,
	   KRT_Mentett_Talalati_Listak.Ver,
	   KRT_Mentett_Talalati_Listak.Note,
	   KRT_Mentett_Talalati_Listak.Stat_id,
	   KRT_Mentett_Talalati_Listak.ErvKezd,
	   KRT_Mentett_Talalati_Listak.ErvVege,
	   KRT_Mentett_Talalati_Listak.Letrehozo_id,
	   KRT_Mentett_Talalati_Listak.LetrehozasIdo,
	   KRT_Mentett_Talalati_Listak.Modosito_id,
	   KRT_Mentett_Talalati_Listak.ModositasIdo,
	   KRT_Mentett_Talalati_Listak.Zarolo_id,
	   KRT_Mentett_Talalati_Listak.ZarolasIdo,
	   KRT_Mentett_Talalati_Listak.Tranz_id,
	   KRT_Mentett_Talalati_Listak.UIAccessLog_id,
	   KRT_Felhasznalok.Nev as Requestor
   from 
     KRT_Mentett_Talalati_Listak  as KRT_Mentett_Talalati_Listak   
	 INNER JOIN KRT_Felhasznalok as KRT_Felhasznalok ON KRT_Mentett_Talalati_Listak.Requestor = KRT_Felhasznalok.Id'

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end