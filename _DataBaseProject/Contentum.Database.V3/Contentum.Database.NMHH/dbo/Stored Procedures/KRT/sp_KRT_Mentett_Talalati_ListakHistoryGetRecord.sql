﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_Mentett_Talalati_ListakHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_Mentett_Talalati_ListakHistoryGetRecord
go
*/
create procedure sp_KRT_Mentett_Talalati_ListakHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_Mentett_Talalati_ListakHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_Mentett_Talalati_ListakHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ListName' as ColumnName,               
               cast(Old.ListName as nvarchar(99)) as OldValue,
               cast(New.ListName as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ListName as nvarchar(max)),'') != ISNULL(CAST(New.ListName as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SaveDate' as ColumnName,               
               cast(Old.SaveDate as nvarchar(99)) as OldValue,
               cast(New.SaveDate as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SaveDate as nvarchar(max)),'') != ISNULL(CAST(New.SaveDate as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Searched_Table' as ColumnName,               
               cast(Old.Searched_Table as nvarchar(99)) as OldValue,
               cast(New.Searched_Table as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Searched_Table as nvarchar(max)),'') != ISNULL(CAST(New.Searched_Table as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Requestor' as ColumnName,               
               cast(Old.Requestor as nvarchar(99)) as OldValue,
               cast(New.Requestor as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Requestor as nvarchar(max)),'') != ISNULL(CAST(New.Requestor as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HeaderXML' as ColumnName,               
               cast(Old.HeaderXML as nvarchar(99)) as OldValue,
               cast(New.HeaderXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HeaderXML as nvarchar(max)),'') != ISNULL(CAST(New.HeaderXML as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BodyXML' as ColumnName,               
               cast(Old.BodyXML as nvarchar(99)) as OldValue,
               cast(New.BodyXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BodyXML as nvarchar(max)),'') != ISNULL(CAST(New.BodyXML as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WorkStation' as ColumnName,               
               cast(Old.WorkStation as nvarchar(99)) as OldValue,
               cast(New.WorkStation as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Mentett_Talalati_ListakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.WorkStation as nvarchar(max)),'') != ISNULL(CAST(New.WorkStation as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go