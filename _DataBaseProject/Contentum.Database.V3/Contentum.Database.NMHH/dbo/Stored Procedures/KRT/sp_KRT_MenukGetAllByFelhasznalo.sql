﻿create procedure [dbo].[sp_KRT_MenukGetAllByFelhasznalo]
  @Felhasznalo_Id uniqueidentifier ,
  @Alkalmazas_Kod NVARCHAR(100) , -- 'eAdmin', 'eRecord', ...
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

  SET @sqlcmd = 
  '
	select m.Id as KRT_Menuk_Id,m.Menu_Id_Szulo as KRT_Menuk_Menu_Id_Szulo
	 , m.kod as KRT_Menuk_Kod,m.nev as KRT_Menuk_Nev,m.parameter as KRT_Menuk_Parameter,m.imageurl as KRT_Menuk_ImageURL
	 ,f.Kod as KRT_Funkciok_Kod,mdl.kod as KRT_Modulok_Kod 
from krt_menuk m
	left outer join krt_funkciok f
	on (m.Funkcio_Id = f.id)
	left outer join krt_modulok mdl on m.Modul_Id = mdl.id	
where m.Kod = '''+@Alkalmazas_Kod+'''
	and getdate() between m.ErvKezd and m.ErvVege
	and (getdate() between f.ErvKezd and f.ErvVege or m.Funkcio_Id is null)
	and (getdate() between mdl.ErvKezd and mdl.ErvVege or mdl.Id is null)
	and (m.Funkcio_id is null or f.kod in 
		(select f_.kod 
		 from krt_funkciok f_
			join krt_szerepkor_funkcio szf_ on (szf_.Funkcio_Id = f_.Id)
			join krt_szerepkorok sz_ on (szf_.Szerepkor_Id = sz_.id)
			join krt_felhasznalo_szerepkor fsz_ on (fsz_.Szerepkor_Id = sz_.Id)
		 where fsz_.Felhasznalo_Id = ''' + CAST(@Felhasznalo_Id as Nvarchar(40)) + '''
			and (getdate() between f_.ErvKezd and f_.ErvVege)
			and (getdate() between szf_.ErvKezd and szf_.ErvVege)
			and (getdate() between sz_.ErvKezd and sz_.ErvVege)
			and (getdate() between fsz_.ErvKezd and fsz_.ErvVege)
		))
	--and m.Org=''' + CAST(@Org as Nvarchar(40)) + '''
	order by m.sorrend
  '
  
  
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end