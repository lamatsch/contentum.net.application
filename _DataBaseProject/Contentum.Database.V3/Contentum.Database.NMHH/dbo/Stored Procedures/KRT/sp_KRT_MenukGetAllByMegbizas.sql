﻿create procedure [dbo].[sp_KRT_MenukGetAllByMegbizas]
  @Helyettesites_Id uniqueidentifier ,
  @Alkalmazas_Kod NVARCHAR(100) , -- 'eAdmin', 'eRecord', ...
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

if (@Helyettesites_Id is null)
begin
    RAISERROR('[53506]',16,1) -- A Helyettesites_Id paraméter értéke nem lehet null!
end

-- helyettesítés rekord lekérdezése:
declare @Id uniqueidentifier
declare @Felhasznalo_ID_helyettesitett uniqueidentifier
declare @CsoportTag_ID_helyettesitett uniqueidentifier
declare @Felhasznalo_ID_helyettesito uniqueidentifier
declare @HelyettesitesKezd datetime
declare @HelyettesitesVege datetime
declare @HelyettesitesMod nvarchar(64)
declare @ErvKezd datetime
declare @ErvVege datetime

select @Id = Id, @ErvKezd = ErvKezd, @ErvVege = ErvVege,
@Felhasznalo_ID_helyettesitett = Felhasznalo_ID_helyettesitett,
@CsoportTag_ID_helyettesitett = CsoportTag_ID_helyettesitett,
@Felhasznalo_ID_helyettesito = Felhasznalo_ID_helyettesito, @HelyettesitesMod = HelyettesitesMod,
@HelyettesitesKezd = HelyettesitesKezd, @HelyettesitesVege = HelyettesitesVege
from KRT_Helyettesitesek where Id = @Helyettesites_Id

-- a rekord nem található
if (@Id is null)
begin
    RAISERROR('[50101]',16,1)
end

if (@HelyettesitesMod <> '2')
begin
    RAISERROR('[53507]',16,1) -- A helyettesítés nem megbízás!
end

if not(getdate() between @ErvKezd and @ErvVege)
begin
    RAISERROR('[53508]',16,1) -- A megbízás érvénytelen!
end

if not(getdate() between @HelyettesitesKezd and @HelyettesitesVege)
begin
    RAISERROR('[53509]',16,1) -- A megbízás nem áll fenn!
end
-- felhasználók ellenorzése:
declare @Helyettesitett_Id uniqueidentifier
select @Helyettesitett_Id = Id
    from KRT_Felhasznalok
    where Id = @Felhasznalo_ID_helyettesitett
    and getdate() between ErvKezd and ErvVege

if (@Helyettesitett_Id is null)
begin
    RAISERROR('[53510]',16,1) -- A helyettesített felhasználó nem létezik!
end

declare @Helyettesito_Id uniqueidentifier
select @Helyettesito_Id = Id
    from KRT_Felhasznalok
    where Id = @Felhasznalo_ID_helyettesito
    and getdate() between ErvKezd and ErvVege

if (@Helyettesito_Id is null)
begin
    RAISERROR('[53511]',16,1) -- A helyettesíto felhasználó nem létezik!
end

-- csoporttagság fennállásának ellenorzése
declare @CsoportTag_Id uniqueidentifier
select @CsoportTag_Id = KRT_CsoportTagok.Id
	from KRT_CsoportTagok
	join KRT_Csoportok on KRT_CsoportTagok.Csoport_Id = KRT_Csoportok.Id
	where KRT_CsoportTagok.Id = @CsoportTag_Id_helyettesitett
	and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege

if (@CsoportTag_Id is null)
begin
    RAISERROR('[53501]',16,1) -- A csoporttagság nem áll fenn!
end

  SET @sqlcmd = 
  '
	select m.Id as KRT_Menuk_Id,m.Menu_Id_Szulo as KRT_Menuk_Menu_Id_Szulo
	 , m.kod as KRT_Menuk_Kod,m.nev as KRT_Menuk_Nev,m.parameter as KRT_Menuk_Parameter,m.imageurl as KRT_Menuk_ImageURL
	 ,f.Kod as KRT_Funkciok_Kod,mdl.kod as KRT_Modulok_Kod 
from krt_menuk m
	left outer join krt_funkciok f
	on (m.Funkcio_Id = f.id)
	left outer join krt_modulok mdl on m.Modul_Id = mdl.id	
where m.Kod = '''+@Alkalmazas_Kod+'''
	and getdate() between m.ErvKezd and m.ErvVege
	and (getdate() between f.ErvKezd and f.ErvVege or m.Funkcio_Id is null)
	and (getdate() between mdl.ErvKezd and mdl.ErvVege or mdl.Id is null)
	and (m.Funkcio_id is null or f.kod in 
		(select f_.kod 
		 from krt_funkciok f_
			join krt_szerepkor_funkcio szf_ on (szf_.Funkcio_Id = f_.Id)
			join krt_szerepkorok sz_ on (szf_.Szerepkor_Id = sz_.id)
			join krt_felhasznalo_szerepkor fsz_ on (fsz_.Szerepkor_Id = sz_.Id)
		 where fsz_.Helyettesites_Id = ''' + convert(nvarchar(36), @Helyettesites_Id) + '''
			and fsz_.Felhasznalo_Id = ''' + convert(nvarchar(36), @Felhasznalo_Id_helyettesito) + '''
			and (getdate() between f_.ErvKezd and f_.ErvVege)
			and (getdate() between szf_.ErvKezd and szf_.ErvVege)
			and (getdate() between sz_.ErvKezd and sz_.ErvVege)
			and (getdate() between fsz_.ErvKezd and fsz_.ErvVege)
		))
	--and m.Org=''' + CAST(@Org as Nvarchar(40)) + '''
	order by m.sorrend
  '
--print (@sqlcmd)
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end