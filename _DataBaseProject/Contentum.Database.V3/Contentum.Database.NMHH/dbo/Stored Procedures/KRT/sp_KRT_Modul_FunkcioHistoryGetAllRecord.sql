﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_Modul_FunkcioHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_Modul_FunkcioHistoryGetAllRecord
go
*/
create procedure sp_KRT_Modul_FunkcioHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_Modul_FunkcioHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Modul_FunkcioHistory Old
         inner join KRT_Modul_FunkcioHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Modul_FunkcioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FunkciokAzonosito(Old.Funkcio_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_FunkciokAzonosito(New.Funkcio_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Modul_FunkcioHistory Old
         inner join KRT_Modul_FunkcioHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Modul_FunkcioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Funkcio_Id as nvarchar(max)),'') != ISNULL(CAST(New.Funkcio_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Funkciok FTOld on FTOld.Id = Old.Funkcio_Id --and FTOld.Ver = Old.Ver
         left join KRT_Funkciok FTNew on FTNew.Id = New.Funkcio_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Modul_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(Old.Modul_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(New.Modul_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Modul_FunkcioHistory Old
         inner join KRT_Modul_FunkcioHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_Modul_FunkcioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Modul_Id as nvarchar(max)),'') != ISNULL(CAST(New.Modul_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Modulok FTOld on FTOld.Id = Old.Modul_Id --and FTOld.Ver = Old.Ver
         left join KRT_Modulok FTNew on FTNew.Id = New.Modul_Id --and FTNew.Ver = New.Ver      
)
            
end
go