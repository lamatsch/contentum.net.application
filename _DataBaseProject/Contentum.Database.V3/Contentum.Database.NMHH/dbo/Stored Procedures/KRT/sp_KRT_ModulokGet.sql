﻿create procedure [dbo].[sp_KRT_ModulokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Modulok.Id,
	   KRT_Modulok.Org,
	   KRT_Modulok.Tipus,
	   KRT_Modulok.Kod,
	   KRT_Modulok.Nev,
	   KRT_Modulok.Leiras,
	   KRT_Modulok.SelectString,
	   KRT_Modulok.Csoport_Id_Tulaj,
	   KRT_Modulok.Parameterek,
	   KRT_Modulok.ObjTip_Id_Adatelem,
	   KRT_Modulok.ObjTip_Adatelem,
	   KRT_Modulok.Ver,
	   KRT_Modulok.Note,
	   KRT_Modulok.Stat_id,
	   KRT_Modulok.ErvKezd,
	   KRT_Modulok.ErvVege,
	   KRT_Modulok.Letrehozo_id,
	   KRT_Modulok.LetrehozasIdo,
	   KRT_Modulok.Modosito_id,
	   KRT_Modulok.ModositasIdo,
	   KRT_Modulok.Zarolo_id,
	   KRT_Modulok.ZarolasIdo,
	   KRT_Modulok.Tranz_id,
	   KRT_Modulok.UIAccessLog_id
	   from 
		 KRT_Modulok as KRT_Modulok 
	   where
		 KRT_Modulok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end