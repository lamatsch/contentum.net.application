﻿create procedure [dbo].[sp_KRT_ModulokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Modulok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Modulok.Id,
	   KRT_Modulok.Org,
	   KRT_Modulok.Tipus,
	   KRT_Modulok.Kod,
	   KRT_Modulok.Nev,
	   KRT_Modulok.Leiras,
	   KRT_Modulok.SelectString,
	   KRT_Modulok.Csoport_Id_Tulaj,
	   KRT_Modulok.Parameterek,
	   KRT_Modulok.ObjTip_Id_Adatelem,
	   KRT_Modulok.ObjTip_Adatelem,
	   KRT_Modulok.Ver,
	   KRT_Modulok.Note,
	   KRT_Modulok.Stat_id,
	   KRT_Modulok.ErvKezd,
	   KRT_Modulok.ErvVege,
	   KRT_Modulok.Letrehozo_id,
	   KRT_Modulok.LetrehozasIdo,
	   KRT_Modulok.Modosito_id,
	   KRT_Modulok.ModositasIdo,
	   KRT_Modulok.Zarolo_id,
	   KRT_Modulok.ZarolasIdo,
	   KRT_Modulok.Tranz_id,
	   KRT_Modulok.UIAccessLog_id  
   from 
     KRT_Modulok as KRT_Modulok      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end