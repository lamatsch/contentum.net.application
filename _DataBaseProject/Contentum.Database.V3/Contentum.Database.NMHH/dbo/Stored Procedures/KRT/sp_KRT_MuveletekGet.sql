﻿create procedure [dbo].[sp_KRT_MuveletekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Muveletek.Id,
	   KRT_Muveletek.Org,
	   KRT_Muveletek.Kod,
	   KRT_Muveletek.Nev,
	   KRT_Muveletek.Ver,
	   KRT_Muveletek.Note,
	   KRT_Muveletek.Stat_id,
	   KRT_Muveletek.ErvKezd,
	   KRT_Muveletek.ErvVege,
	   KRT_Muveletek.Letrehozo_id,
	   KRT_Muveletek.LetrehozasIdo,
	   KRT_Muveletek.Modosito_id,
	   KRT_Muveletek.ModositasIdo,
	   KRT_Muveletek.Zarolo_id,
	   KRT_Muveletek.ZarolasIdo,
	   KRT_Muveletek.Tranz_id,
	   KRT_Muveletek.UIAccessLog_id
	   from 
		 KRT_Muveletek as KRT_Muveletek 
	   where
		 KRT_Muveletek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end