﻿create procedure [dbo].[sp_KRT_NezetKontrollokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_NezetKontrollok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_NezetKontrollok.Id,
	   KRT_NezetKontrollok.Nezet_Id,
	   KRT_NezetKontrollok.Muvelet_Id,
	   KRT_NezetKontrollok.Csoport_Id,
	   KRT_NezetKontrollok.Feltetel,
	   KRT_NezetKontrollok.ObjTipus_id_UIElem,
	   KRT_NezetKontrollok.Tiltas,
	   KRT_NezetKontrollok.ObjTipus_id_Adatelem,
	   KRT_NezetKontrollok.ObjStat_Id_Adatelem,
	   KRT_NezetKontrollok.Ver,
	   KRT_NezetKontrollok.Note,
	   KRT_NezetKontrollok.Stat_id,
	   KRT_NezetKontrollok.ErvKezd,
	   KRT_NezetKontrollok.ErvVege,
	   KRT_NezetKontrollok.Letrehozo_id,
	   KRT_NezetKontrollok.LetrehozasIdo,
	   KRT_NezetKontrollok.Modosito_id,
	   KRT_NezetKontrollok.ModositasIdo,
	   KRT_NezetKontrollok.Zarolo_id,
	   KRT_NezetKontrollok.ZarolasIdo,
	   KRT_NezetKontrollok.Tranz_id,
	   KRT_NezetKontrollok.UIAccessLog_id  
   from 
     KRT_NezetKontrollok as KRT_NezetKontrollok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end