﻿create procedure [dbo].[sp_KRT_NezetKontrollokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Nezet_Id     uniqueidentifier  = null,         
             @Muvelet_Id     uniqueidentifier  = null,         
             @Csoport_Id     uniqueidentifier  = null,         
             @Feltetel     Nvarchar(4000)  = null,         
             @ObjTipus_id_UIElem     uniqueidentifier  = null,         
             @Tiltas     Nvarchar(100)  = null,         
             @ObjTipus_id_Adatelem     uniqueidentifier  = null,         
             @ObjStat_Id_Adatelem     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Nezet_Id     uniqueidentifier         
     DECLARE @Act_Muvelet_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id     uniqueidentifier         
     DECLARE @Act_Feltetel     Nvarchar(4000)         
     DECLARE @Act_ObjTipus_id_UIElem     uniqueidentifier         
     DECLARE @Act_Tiltas     Nvarchar(100)         
     DECLARE @Act_ObjTipus_id_Adatelem     uniqueidentifier         
     DECLARE @Act_ObjStat_Id_Adatelem     uniqueidentifier         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Nezet_Id = Nezet_Id,
     @Act_Muvelet_Id = Muvelet_Id,
     @Act_Csoport_Id = Csoport_Id,
     @Act_Feltetel = Feltetel,
     @Act_ObjTipus_id_UIElem = ObjTipus_id_UIElem,
     @Act_Tiltas = Tiltas,
     @Act_ObjTipus_id_Adatelem = ObjTipus_id_Adatelem,
     @Act_ObjStat_Id_Adatelem = ObjStat_Id_Adatelem,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_NezetKontrollok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Nezet_Id')=1
         SET @Act_Nezet_Id = @Nezet_Id
   IF @UpdatedColumns.exist('/root/Muvelet_Id')=1
         SET @Act_Muvelet_Id = @Muvelet_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id')=1
         SET @Act_Csoport_Id = @Csoport_Id
   IF @UpdatedColumns.exist('/root/Feltetel')=1
         SET @Act_Feltetel = @Feltetel
   IF @UpdatedColumns.exist('/root/ObjTipus_id_UIElem')=1
         SET @Act_ObjTipus_id_UIElem = @ObjTipus_id_UIElem
   IF @UpdatedColumns.exist('/root/Tiltas')=1
         SET @Act_Tiltas = @Tiltas
   IF @UpdatedColumns.exist('/root/ObjTipus_id_Adatelem')=1
         SET @Act_ObjTipus_id_Adatelem = @ObjTipus_id_Adatelem
   IF @UpdatedColumns.exist('/root/ObjStat_Id_Adatelem')=1
         SET @Act_ObjStat_Id_Adatelem = @ObjStat_Id_Adatelem
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_NezetKontrollok
SET
     Nezet_Id = @Act_Nezet_Id,
     Muvelet_Id = @Act_Muvelet_Id,
     Csoport_Id = @Act_Csoport_Id,
     Feltetel = @Act_Feltetel,
     ObjTipus_id_UIElem = @Act_ObjTipus_id_UIElem,
     Tiltas = @Act_Tiltas,
     ObjTipus_id_Adatelem = @Act_ObjTipus_id_Adatelem,
     ObjStat_Id_Adatelem = @Act_ObjStat_Id_Adatelem,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_NezetKontrollok',@Id
					,'KRT_NezetKontrollokHistory',1,@ExecutorUserId,@ExecutionTime   
end


COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH