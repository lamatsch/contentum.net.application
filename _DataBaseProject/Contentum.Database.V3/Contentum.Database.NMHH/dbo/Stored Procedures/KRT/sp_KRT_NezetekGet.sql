﻿
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_NezetekGet')
            and   type = 'P')
   drop procedure sp_KRT_NezetekGet
go
*/
CREATE procedure sp_KRT_NezetekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Nezetek.Id,
	   KRT_Nezetek.Org,
	   KRT_Nezetek.Form_Id,
	   KRT_Nezetek.Nev,
	   KRT_Nezetek.Leiras,
	   KRT_Nezetek.Csoport_Id,
	   KRT_Nezetek.Muvelet_Id,
	   KRT_Nezetek.DisableControls,
	   KRT_Nezetek.InvisibleControls,
	   KRT_Nezetek.ReadOnlyControls,
	   KRT_Nezetek.TabIndexControls,
	   KRT_Nezetek.Prioritas,
	   KRT_Nezetek.Ver,
	   KRT_Nezetek.Note,
	   KRT_Nezetek.Stat_id,
	   KRT_Nezetek.ErvKezd,
	   KRT_Nezetek.ErvVege,
	   KRT_Nezetek.Letrehozo_id,
	   KRT_Nezetek.LetrehozasIdo,
	   KRT_Nezetek.Modosito_id,
	   KRT_Nezetek.ModositasIdo,
	   KRT_Nezetek.Zarolo_id,
	   KRT_Nezetek.ZarolasIdo,
	   KRT_Nezetek.Tranz_id,
	   KRT_Nezetek.UIAccessLog_id
	   from 
		 KRT_Nezetek as KRT_Nezetek 
	   where
		 KRT_Nezetek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end