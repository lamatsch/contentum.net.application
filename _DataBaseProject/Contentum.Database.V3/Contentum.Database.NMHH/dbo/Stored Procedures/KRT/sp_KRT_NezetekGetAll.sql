﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_NezetekGetAll')
            and   type = 'P')
   drop procedure sp_KRT_NezetekGetAll
go
*/
CREATE procedure sp_KRT_NezetekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Nezetek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_Nezetek.Id,
	   KRT_Nezetek.Org,
	   KRT_Nezetek.Form_Id,
	   KRT_Nezetek.Nev,
	   KRT_Nezetek.Leiras,
	   KRT_Nezetek.Csoport_Id,
	   KRT_Nezetek.Muvelet_Id,
	   KRT_Nezetek.DisableControls,
	   KRT_Nezetek.InvisibleControls,
	   KRT_Nezetek.ReadOnlyControls,
	   KRT_Nezetek.TabIndexControls,
	   KRT_Nezetek.Prioritas,
	   KRT_Nezetek.Ver,
	   KRT_Nezetek.Note,
	   KRT_Nezetek.Stat_id,
	   KRT_Nezetek.ErvKezd,
	   KRT_Nezetek.ErvVege,
	   KRT_Nezetek.Letrehozo_id,
	   KRT_Nezetek.LetrehozasIdo,
	   KRT_Nezetek.Modosito_id,
	   KRT_Nezetek.ModositasIdo,
	   KRT_Nezetek.Zarolo_id,
	   KRT_Nezetek.ZarolasIdo,
	   KRT_Nezetek.Tranz_id,
	   KRT_Nezetek.UIAccessLog_id  
   from 
     KRT_Nezetek as KRT_Nezetek      
    Where KRT_Nezetek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end