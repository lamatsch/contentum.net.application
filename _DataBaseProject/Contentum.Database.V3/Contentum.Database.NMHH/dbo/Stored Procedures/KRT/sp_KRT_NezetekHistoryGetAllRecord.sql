﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_NezetekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_NezetekHistoryGetAllRecord
go
*/
CREATE procedure sp_KRT_NezetekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_NezetekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Form_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(Old.Form_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(New.Form_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Form_Id as nvarchar(max)),'') != ISNULL(CAST(New.Form_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Modulok FTOld on FTOld.Id = Old.Form_Id --and FTOld.Ver = Old.Ver
         left join KRT_Modulok FTNew on FTNew.Id = New.Form_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id' as ColumnName,               cast(Old.Csoport_Id as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Muvelet_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_MuveletekAzonosito(Old.Muvelet_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_MuveletekAzonosito(New.Muvelet_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Muvelet_Id as nvarchar(max)),'') != ISNULL(CAST(New.Muvelet_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Muveletek FTOld on FTOld.Id = Old.Muvelet_Id --and FTOld.Ver = Old.Ver
         left join KRT_Muveletek FTNew on FTNew.Id = New.Muvelet_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DisableControls' as ColumnName,               cast(Old.DisableControls as nvarchar(99)) as OldValue,
               cast(New.DisableControls as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DisableControls as nvarchar(max)),'') != ISNULL(CAST(New.DisableControls as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'InvisibleControls' as ColumnName,               cast(Old.InvisibleControls as nvarchar(99)) as OldValue,
               cast(New.InvisibleControls as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.InvisibleControls as nvarchar(max)),'') != ISNULL(CAST(New.InvisibleControls as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ReadOnlyControls' as ColumnName,               cast(Old.ReadOnlyControls as nvarchar(99)) as OldValue,
               cast(New.ReadOnlyControls as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ReadOnlyControls as nvarchar(max)),'') != ISNULL(CAST(New.ReadOnlyControls as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TabIndexControls' as ColumnName,               cast(Old.TabIndexControls as nvarchar(99)) as OldValue,
               cast(New.TabIndexControls as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TabIndexControls as nvarchar(max)),'') != ISNULL(CAST(New.TabIndexControls as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Prioritas' as ColumnName,               cast(Old.Prioritas as nvarchar(99)) as OldValue,
               cast(New.Prioritas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_NezetekHistory Old
         inner join KRT_NezetekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_NezetekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Prioritas as nvarchar(max)),'') != ISNULL(CAST(New.Prioritas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go