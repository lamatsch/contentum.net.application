﻿create procedure [dbo].[sp_KRT_ObjTip_TargySzoGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_ObjTip_TargySzo.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	   KRT_ObjTip_TargySzo.Id,
	   KRT_ObjTip_TargySzo.TargySzavak,
	   KRT_ObjTip_TargySzo.AlapertelmezettErtek,
	   KRT_ObjTip_TargySzo.Tipus,
	   KRT_ObjTip_TargySzo.BelsoAzonosito,
	   KRT_ObjTip_TargySzo.SPSSzinkronizalt,
	   KRT_ObjTip_TargySzo.SPS_Field_Id,
	   KRT_ObjTip_TargySzo.Csoport_Id_Tulaj,
	   KRT_ObjTip_TargySzo.RegExp,
	   KRT_ObjTip_TargySzo.ToolTip,
	   KRT_ObjTip_TargySzo.ControlTypeSource,
	   KRT_ObjTip_TargySzo.ControlTypeDataSource,
	   KRT_ObjTip_TargySzo.Targyszo_Id_Parent,
	   KRT_ObjTip_TargySzo.XSD,
	   KRT_ObjTip_TargySzo.Ver,
	   KRT_ObjTip_TargySzo.Note,
	   KRT_ObjTip_TargySzo.Stat_id,
	   KRT_ObjTip_TargySzo.ErvKezd,
	   KRT_ObjTip_TargySzo.ErvVege,
	   KRT_ObjTip_TargySzo.Letrehozo_id,
	   KRT_ObjTip_TargySzo.LetrehozasIdo,
	   KRT_ObjTip_TargySzo.Modosito_id,
	   KRT_ObjTip_TargySzo.ModositasIdo,
	   KRT_ObjTip_TargySzo.Zarolo_id,
	   KRT_ObjTip_TargySzo.ZarolasIdo,
	   KRT_ObjTip_TargySzo.Tranz_id,
	   KRT_ObjTip_TargySzo.UIAccessLog_id
	   FROM 
		 EREC_TargySzavak as KRT_ObjTip_TargySzo     
       '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

