﻿create procedure [dbo].[sp_KRT_ObjTipusokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_ObjTipusok.Id,
	   KRT_ObjTipusok.ObjTipus_Id_Tipus,
	   KRT_ObjTipusok.Kod,
	   KRT_ObjTipusok.Nev,
	   KRT_ObjTipusok.Obj_Id_Szulo,
	   KRT_ObjTipusok.KodCsoport_Id,
	   KRT_ObjTipusok.Ver,
	   KRT_ObjTipusok.Note,
	   KRT_ObjTipusok.Stat_id,
	   KRT_ObjTipusok.ErvKezd,
	   KRT_ObjTipusok.ErvVege,
	   KRT_ObjTipusok.Letrehozo_id,
	   KRT_ObjTipusok.LetrehozasIdo,
	   KRT_ObjTipusok.Modosito_id,
	   KRT_ObjTipusok.ModositasIdo,
	   KRT_ObjTipusok.Zarolo_id,
	   KRT_ObjTipusok.ZarolasIdo,
	   KRT_ObjTipusok.Tranz_id,
	   KRT_ObjTipusok.UIAccessLog_id
	   from 
		 KRT_ObjTipusok as KRT_ObjTipusok 
	   where
		 KRT_ObjTipusok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end