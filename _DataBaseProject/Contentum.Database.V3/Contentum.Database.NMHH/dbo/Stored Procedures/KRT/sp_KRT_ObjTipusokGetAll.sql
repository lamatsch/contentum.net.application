﻿create procedure [dbo].[sp_KRT_ObjTipusokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_ObjTipusok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_ObjTipusok.Id,
	   KRT_ObjTipusok.ObjTipus_Id_Tipus,
	   KRT_ObjTipusok.Kod,
	   KRT_ObjTipusok.Nev,
	   KRT_ObjTipusok.Obj_Id_Szulo,
	   KRT_ObjTipusok.KodCsoport_Id,
	   KRT_ObjTipusok.Ver,
	   KRT_ObjTipusok.Note,
	   KRT_ObjTipusok.Stat_id,
	   KRT_ObjTipusok.ErvKezd,
	   KRT_ObjTipusok.ErvVege,
	   KRT_ObjTipusok.Letrehozo_id,
	   KRT_ObjTipusok.LetrehozasIdo,
	   KRT_ObjTipusok.Modosito_id,
	   KRT_ObjTipusok.ModositasIdo,
	   KRT_ObjTipusok.Zarolo_id,
	   KRT_ObjTipusok.ZarolasIdo,
	   KRT_ObjTipusok.Tranz_id,
	   KRT_ObjTipusok.UIAccessLog_id  
   from 
     KRT_ObjTipusok as KRT_ObjTipusok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end