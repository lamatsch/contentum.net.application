﻿create procedure [dbo].[sp_KRT_ObjTipusokGetAllByTableColumn]
  @Table nvarchar(100) = '',
  @Column nvarchar(100) = '',
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

   if @Table is null or @Table = ''
       RAISERROR('[55001]',16,1) -- 'Nincs megadva tábla!'

   if @Column is null or @Column = ''
       RAISERROR('[55002]',16,1) -- 'Nincs megadva oszlop!'
   
    declare @ColumnFilter nvarchar(10)
    set @ColumnFilter = 'DBColumn'

   DECLARE @sqlcmd nvarchar(4000)
     
         
  SET @sqlcmd = 
  'select 
  	   KRT_ObjTipusok.Id,
	   KRT_ObjTipusok.ObjTipus_Id_Tipus,
	   KRT_ObjTipusok.Kod,
	   KRT_ObjTipusok.Nev,
	   KRT_ObjTipusok.Obj_Id_Szulo,
	   KRT_ObjTipusok.KodCsoport_Id,
	   KRT_ObjTipusok.Ver,
	   KRT_ObjTipusok.Note,
	   KRT_ObjTipusok.ErvKezd,
	   KRT_ObjTipusok.ErvVege,
	   KRT_ObjTipusok.Letrehozo_id,
	   KRT_ObjTipusok.LetrehozasIdo,
	   KRT_ObjTipusok.Modosito_id,
	   KRT_ObjTipusok.ModositasIdo,
	   KRT_ObjTipusok.Zarolo_id,
	   KRT_ObjTipusok.ZarolasIdo,
	   ot2.Kod AS SzuloObjTipusKod,
	   ot2.Nev AS SzuloObjTipusNev,
	   ot3.Kod AS TipusObjTipusKod,
	   ot3.Nev AS TipusObjTipusNev
   from 
     KRT_ObjTipusok as KRT_ObjTipusok
	 LEFT JOIN
     KRT_ObjTipusok as ot2
     ON KRT_ObjTipusok.Obj_Id_Szulo = ot2.Id
         AND getdate() between KRT_ObjTipusok.ErvKezd and KRT_ObjTipusok.ErvVege
         AND getdate() between ot2.ErvKezd and ot2.ErvVege
     LEFT JOIN
     KRT_ObjTipusok as ot3
     ON KRT_ObjTipusok.ObjTipus_Id_Tipus = ot3.Id
         AND getdate() between ot3.ErvKezd and ot3.ErvVege
     '

	SET @sqlcmd = @sqlcmd + 'where KRT_ObjTipusok.Kod = ''' + @Column + ''''
                          + ' and ot2.Kod =''' + @Table + ''''
                          + ' and ot3.Kod =''' + @ColumnFilter + ''''
 

  --print (@sqlcmd)
  exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end