﻿create procedure [dbo].[sp_KRT_ObjTipusokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_ObjTipusok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null,
  @KodFilter nvarchar(100) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
         
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_ObjTipusok.Id,
	   KRT_ObjTipusok.ObjTipus_Id_Tipus,
	   KRT_ObjTipusok.Kod,
	   KRT_ObjTipusok.Nev,
	   KRT_ObjTipusok.Obj_Id_Szulo,
	   KRT_ObjTipusok.KodCsoport_Id,
	   KRT_ObjTipusok.Ver,
	   KRT_ObjTipusok.Note,
	   KRT_ObjTipusok.Stat_id,
	   KRT_ObjTipusok.ErvKezd,
	   KRT_ObjTipusok.ErvVege,
	   KRT_ObjTipusok.Letrehozo_id,
	   KRT_ObjTipusok.LetrehozasIdo,
	   KRT_ObjTipusok.Modosito_id,
	   KRT_ObjTipusok.ModositasIdo,
	   KRT_ObjTipusok.Zarolo_id,
	   KRT_ObjTipusok.ZarolasIdo,
	   KRT_ObjTipusok.Tranz_id,
	   KRT_ObjTipusok.UIAccessLog_id,
--	   TipusTable.TipusLetezik as TipusLetezik,
--	   TipusTable.TipusMeret as TipusMeret,
	   (select ot2.Kod FROM KRT_ObjTipusok as ot2 WHERE KRT_ObjTipusok.Obj_Id_Szulo = ot2.Id) AS SzuloObjTipusKod,
	   (select ot2.Nev FROM KRT_ObjTipusok as ot2 WHERE KRT_ObjTipusok.Obj_Id_Szulo = ot2.Id) AS SzuloObjTipusNev,
	   (select ot2.Kod FROM KRT_ObjTipusok as ot2 WHERE KRT_ObjTipusok.ObjTipus_Id_Tipus = ot2.Id) AS TipusObjTipusKod,
	   (select ot2.Nev FROM KRT_ObjTipusok as ot2 WHERE KRT_ObjTipusok.ObjTipus_Id_Tipus = ot2.Id) AS TipusObjTipusNev,
       TipusTable.OszlopDefinicioLetezik as OszlopDefinicioLetezik 
   from 
     KRT_ObjTipusok as KRT_ObjTipusok
	 JOIN 
    (Select KRT_ObjTipusok.Id,
--        case when COALESCE(COL_LENGTH(KRT_ObjTipusok.Kod , ''Tipus''), 0) > 0 then 1 else 0 end AS TipusLetezik,
--        COALESCE(COL_LENGTH(KRT_ObjTipusok.Kod , ''Tipus''),0) AS TipusMeret
    case when exists(select ot2.Id
                   from KRT_ObjTipusok as ot2
                   where ot2.Obj_Id_Szulo = KRT_ObjTipusok.Id
                   and ot2.ObjTipus_Id_Tipus = (select ot3.Id from KRT_ObjTipusok ot3 where ot3.Kod=''DBColumn'')
                  )
        then 1 else 0 end as OszlopDefinicioLetezik
        FROM KRT_ObjTipusok) AS TipusTable
        ON KRT_ObjTipusok.Id = TipusTable.Id      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if @KodFilter is not null AND @KodFilter != ''
	begin
		if @Where is null or @Where=''
		BEGIN
			SET @sqlcmd = @sqlcmd + ' Where '
		END
		ELSE
		BEGIN
			SET @sqlcmd = @sqlcmd + ' AND '
		END

		SET @sqlcmd = @sqlcmd + ' ObjTipus_Id_Tipus IN (SELECT Id FROM KRT_ObjTipusok WHERE Kod=''' + @KodFilter + ''')'
	end    
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

  exec (@sqlcmd);
-- select @sqlcmd
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end