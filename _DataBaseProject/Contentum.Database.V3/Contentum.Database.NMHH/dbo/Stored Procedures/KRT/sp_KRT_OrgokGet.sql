﻿create procedure [dbo].[sp_KRT_OrgokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Orgok.Id,
	   KRT_Orgok.Kod,
	   KRT_Orgok.Nev,
	   KRT_Orgok.Sorszam,
	   KRT_Orgok.Partner_id_szulo,
	   KRT_Orgok.Ver,
	   KRT_Orgok.Note,
	   KRT_Orgok.Stat_id,
	   KRT_Orgok.ErvKezd,
	   KRT_Orgok.ErvVege,
	   KRT_Orgok.Letrehozo_id,
	   KRT_Orgok.LetrehozasIdo,
	   KRT_Orgok.Modosito_id,
	   KRT_Orgok.ModositasIdo,
	   KRT_Orgok.Zarolo_id,
	   KRT_Orgok.ZarolasIdo,
	   KRT_Orgok.Tranz_id,
	   KRT_Orgok.UIAccessLog_id
	   from 
		 KRT_Orgok as KRT_Orgok 
	   where
		 KRT_Orgok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end