﻿create procedure [dbo].[sp_KRT_PartnerCimekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_PartnerCimek.Id,
	   KRT_PartnerCimek.Partner_id,
	   KRT_PartnerCimek.Cim_Id,
	   KRT_PartnerCimek.UtolsoHasznalatSiker,
	   KRT_PartnerCimek.UtolsoHasznalatiIdo,
	   KRT_PartnerCimek.eMailKuldes,
	   KRT_PartnerCimek.Fajta,
	   KRT_PartnerCimek.Ver,
	   KRT_PartnerCimek.Note,
	   KRT_PartnerCimek.Stat_id,
	   KRT_PartnerCimek.ErvKezd,
	   KRT_PartnerCimek.ErvVege,
	   KRT_PartnerCimek.Letrehozo_id,
	   KRT_PartnerCimek.LetrehozasIdo,
	   KRT_PartnerCimek.Modosito_id,
	   KRT_PartnerCimek.ModositasIdo,
	   KRT_PartnerCimek.Zarolo_id,
	   KRT_PartnerCimek.ZarolasIdo,
	   KRT_PartnerCimek.Tranz_id,
	   KRT_PartnerCimek.UIAccessLog_id
	   from 
		 KRT_PartnerCimek as KRT_PartnerCimek 
	   where
		 KRT_PartnerCimek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end