﻿create procedure [dbo].[sp_KRT_PartnerKapcsolatokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerKapcsolatok.Id,
	   KRT_PartnerKapcsolatok.Tipus,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt,
	   KRT_PartnerKapcsolatok.Ver,
	   KRT_PartnerKapcsolatok.Note,
	   KRT_PartnerKapcsolatok.Stat_id,
	   KRT_PartnerKapcsolatok.ErvKezd,
	   KRT_PartnerKapcsolatok.ErvVege,
	   KRT_PartnerKapcsolatok.Letrehozo_id,
	   KRT_PartnerKapcsolatok.LetrehozasIdo,
	   KRT_PartnerKapcsolatok.Modosito_id,
	   KRT_PartnerKapcsolatok.ModositasIdo,
	   KRT_PartnerKapcsolatok.Zarolo_id,
	   KRT_PartnerKapcsolatok.ZarolasIdo,
	   KRT_PartnerKapcsolatok.Tranz_id,
	   KRT_PartnerKapcsolatok.UIAccessLog_id  
   from 
     KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end