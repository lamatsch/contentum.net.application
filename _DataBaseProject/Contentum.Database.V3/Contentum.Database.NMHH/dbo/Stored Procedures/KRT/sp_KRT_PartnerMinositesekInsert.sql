﻿create procedure [dbo].[sp_KRT_PartnerMinositesekInsert]    
                @Id      uniqueidentifier = null,    
               	            @ALLAPOT     char(1),
	            @Partner_id     uniqueidentifier,
	            @Felhasznalo_id_kero     uniqueidentifier,
	            @KertMinosites     nvarchar(64),
                @KertKezdDat     datetime  = null,
                @KertVegeDat     datetime  = null,
                @KerelemAzonosito     Nvarchar(100)  = null,
	            @KerelemBeadIdo     datetime,
	            @KerelemDontesIdo     datetime,
	            @Felhasznalo_id_donto     uniqueidentifier,
                @DontesAzonosito     Nvarchar(100)  = null,
                @Minosites     Nvarchar(2)  = null,
                @MinositesKezdDat     datetime  = null,
                @MinositesVegDat     datetime  = null,
                @SztornirozasDat     datetime  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @ALLAPOT is not null
         begin
            SET @insertColumns = @insertColumns + ',ALLAPOT'
            SET @insertValues = @insertValues + ',@ALLAPOT'
         end 
       
         if @Partner_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_id'
            SET @insertValues = @insertValues + ',@Partner_id'
         end 
       
         if @Felhasznalo_id_kero is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_id_kero'
            SET @insertValues = @insertValues + ',@Felhasznalo_id_kero'
         end 
       
         if @KertMinosites is not null
         begin
            SET @insertColumns = @insertColumns + ',KertMinosites'
            SET @insertValues = @insertValues + ',@KertMinosites'
         end 
       
         if @KertKezdDat is not null
         begin
            SET @insertColumns = @insertColumns + ',KertKezdDat'
            SET @insertValues = @insertValues + ',@KertKezdDat'
         end 
       
         if @KertVegeDat is not null
         begin
            SET @insertColumns = @insertColumns + ',KertVegeDat'
            SET @insertValues = @insertValues + ',@KertVegeDat'
         end 
       
         if @KerelemAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',KerelemAzonosito'
            SET @insertValues = @insertValues + ',@KerelemAzonosito'
         end 
       
         if @KerelemBeadIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',KerelemBeadIdo'
            SET @insertValues = @insertValues + ',@KerelemBeadIdo'
         end 
       
         if @KerelemDontesIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',KerelemDontesIdo'
            SET @insertValues = @insertValues + ',@KerelemDontesIdo'
         end 
       
         if @Felhasznalo_id_donto is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_id_donto'
            SET @insertValues = @insertValues + ',@Felhasznalo_id_donto'
         end 
       
         if @DontesAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',DontesAzonosito'
            SET @insertValues = @insertValues + ',@DontesAzonosito'
         end 
       
         if @Minosites is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosites'
            SET @insertValues = @insertValues + ',@Minosites'
         end 
       
         if @MinositesKezdDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesKezdDat'
            SET @insertValues = @insertValues + ',@MinositesKezdDat'
         end 
       
         if @MinositesVegDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesVegDat'
            SET @insertValues = @insertValues + ',@MinositesVegDat'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_PartnerMinositesek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@ALLAPOT char(1),@Partner_id uniqueidentifier,@Felhasznalo_id_kero uniqueidentifier,@KertMinosites nvarchar(64),@KertKezdDat datetime,@KertVegeDat datetime,@KerelemAzonosito Nvarchar(100),@KerelemBeadIdo datetime,@KerelemDontesIdo datetime,@Felhasznalo_id_donto uniqueidentifier,@DontesAzonosito Nvarchar(100),@Minosites Nvarchar(2),@MinositesKezdDat datetime,@MinositesVegDat datetime,@SztornirozasDat datetime,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@ALLAPOT = @ALLAPOT,@Partner_id = @Partner_id,@Felhasznalo_id_kero = @Felhasznalo_id_kero,@KertMinosites = @KertMinosites,@KertKezdDat = @KertKezdDat,@KertVegeDat = @KertVegeDat,@KerelemAzonosito = @KerelemAzonosito,@KerelemBeadIdo = @KerelemBeadIdo,@KerelemDontesIdo = @KerelemDontesIdo,@Felhasznalo_id_donto = @Felhasznalo_id_donto,@DontesAzonosito = @DontesAzonosito,@Minosites = @Minosites,@MinositesKezdDat = @MinositesKezdDat,@MinositesVegDat = @MinositesVegDat,@SztornirozasDat = @SztornirozasDat,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_PartnerMinositesek',@ResultUid
					,'KRT_PartnerMinositesekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH