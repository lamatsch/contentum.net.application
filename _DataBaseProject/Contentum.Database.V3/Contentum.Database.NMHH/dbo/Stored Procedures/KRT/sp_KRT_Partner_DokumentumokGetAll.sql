create procedure sp_KRT_Partner_DokumentumokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partner_Dokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_Partner_Dokumentumok.Id,
	   KRT_Partner_Dokumentumok.Partner_id,
	   KRT_Partner_Dokumentumok.Dokumentum_Id,
	   KRT_Partner_Dokumentumok.Ver,
	   KRT_Partner_Dokumentumok.Note,
	   KRT_Partner_Dokumentumok.Stat_id,
	   KRT_Partner_Dokumentumok.ErvKezd,
	   KRT_Partner_Dokumentumok.ErvVege,
	   KRT_Partner_Dokumentumok.Letrehozo_id,
	   KRT_Partner_Dokumentumok.LetrehozasIdo,
	   KRT_Partner_Dokumentumok.Modosito_id,
	   KRT_Partner_Dokumentumok.ModositasIdo,
	   KRT_Partner_Dokumentumok.Zarolo_id,
	   KRT_Partner_Dokumentumok.ZarolasIdo,
	   KRT_Partner_Dokumentumok.Tranz_id,
	   KRT_Partner_Dokumentumok.UIAccessLog_id  
   from 
     KRT_Partner_Dokumentumok as KRT_Partner_Dokumentumok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go