create procedure sp_KRT_Partner_DokumentumokGetAllWithExtension
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partner_Dokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
       KRT_Partner_Dokumentumok.Dokumentum_Id as Id,
  	   KRT_Partner_Dokumentumok.Id as Partner_Dokumentumok_Id,
	   KRT_Partner_Dokumentumok.Partner_id,
	   KRT_Partner_Dokumentumok.Dokumentum_Id,
	   KRT_Dokumentumok.FajlNev,
	   KRT_Dokumentumok.VerzioJel,
	   KRT_Dokumentumok.Nev,
	   KRT_Dokumentumok.Tipus,
	   dbo.fn_KodtarErtekNeve(''DOKUMENTUMTIPUS'', KRT_Dokumentumok.Tipus, ''' + convert(nvarchar(50),@Org) + ''') as Tipus_Nev,
	   KRT_Dokumentumok.Formatum,
	   dbo.fn_KodtarErtekNeve(''DOKUMENTUM_FORMATUM'', KRT_Dokumentumok.Formatum, ''' + convert(nvarchar(50),@Org) + ''') as Formatum_Nev,
	   KRT_Dokumentumok.Leiras,
	   KRT_Dokumentumok.Meret,
	   KRT_Dokumentumok.TartalomHash,
	   KRT_Dokumentumok.KivonatHash,
	   KRT_Dokumentumok.External_Source,
	   KRT_Dokumentumok.External_Link,
	   KRT_Dokumentumok.External_Id,
	   KRT_Dokumentumok.External_Info,
	   KRT_Partnerek.Nev as PartnerNev,
	   KRT_Partner_Dokumentumok.Ver,
	   KRT_Partner_Dokumentumok.Note,
	   KRT_Partner_Dokumentumok.Stat_id,
	   KRT_Partner_Dokumentumok.ErvKezd,
	   KRT_Partner_Dokumentumok.ErvVege,
	   KRT_Partner_Dokumentumok.Letrehozo_id,
	   KRT_Partner_Dokumentumok.LetrehozasIdo,
	   KRT_Partner_Dokumentumok.Modosito_id,
	   KRT_Partner_Dokumentumok.ModositasIdo,
	   KRT_Partner_Dokumentumok.Zarolo_id,
	   KRT_Partner_Dokumentumok.ZarolasIdo,
	   KRT_Partner_Dokumentumok.Tranz_id,
	   KRT_Partner_Dokumentumok.UIAccessLog_id  
   from 
     KRT_Partner_Dokumentumok as KRT_Partner_Dokumentumok      
	 inner join KRT_Dokumentumok on KRT_Partner_Dokumentumok.Dokumentum_Id = KRT_Dokumentumok.Id
	 inner join KRT_Partnerek on KRT_Partner_Dokumentumok.Partner_id = KRT_Partnerek.Id     
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go