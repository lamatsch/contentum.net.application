﻿CREATE procedure [dbo].[sp_KRT_PartnerekFindByPartnerAndCim]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerCimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerCimek.Id,
	   KRT_PartnerCimek.Partner_id,
	   KRT_PartnerCimek.Cim_Id,
	   KRT_PartnerCimek.Fajta,
	   KRT_KodTarak.Nev as Fajta_Nev
   from 
     KRT_PartnerCimek as KRT_PartnerCimek 
	inner join KRT_Partnerek as KRT_Partnerek on KRT_PartnerCimek.Partner_Id = KRT_Partnerek.Id 
	inner join KRT_Cimek on KRT_Cimek.Id =Krt_PartnerCimek.Cim_Id
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CIM_FAJTA''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_PartnerCimek.Fajta = KRT_KodTarak.Kod
		and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
		where 1=1 '
 
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


