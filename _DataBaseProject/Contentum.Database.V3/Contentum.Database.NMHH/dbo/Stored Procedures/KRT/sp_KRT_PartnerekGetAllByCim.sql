﻿create procedure [dbo].[sp_KRT_PartnerekGetAllByCim]
  @Cim_Id uniqueidentifier,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerCimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerCimek.Id,
	   KRT_PartnerCimek.Partner_id,
	   KRT_PartnerCimek.Cim_Id,
		KRT_Partnerek.Nev as Partner_Nev,
	   KRT_PartnerCimek.UtolsoHasznalatSiker,
	   KRT_PartnerCimek.UtolsoHasznalatiIdo,
	   KRT_PartnerCimek.eMailKuldes,
	   KRT_PartnerCimek.Fajta,
		KRT_KodTarak.Nev as Fajta_Nev,
	   KRT_PartnerCimek.Ver,
	   KRT_PartnerCimek.Note,
	   KRT_PartnerCimek.Stat_id,
	   KRT_PartnerCimek.ErvKezd,
	   KRT_PartnerCimek.ErvVege,
	   KRT_PartnerCimek.Letrehozo_id,
	   KRT_PartnerCimek.LetrehozasIdo,
	   KRT_PartnerCimek.Modosito_id,
	   KRT_PartnerCimek.ModositasIdo,
	   KRT_PartnerCimek.Zarolo_id,
	   KRT_PartnerCimek.ZarolasIdo,
	   KRT_PartnerCimek.Tranz_id,
	   KRT_PartnerCimek.UIAccessLog_id,
	   KRT_Partnerek.UtolsoHasznalat
   from 
     KRT_PartnerCimek as KRT_PartnerCimek 
	join KRT_Partnerek as KRT_Partnerek on KRT_PartnerCimek.Partner_Id = KRT_Partnerek.Id 
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CIM_FAJTA''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_PartnerCimek.Fajta = KRT_KodTarak.Kod
		and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
   Where KRT_PartnerCimek.Cim_Id = ''' + CAST(@Cim_Id as Nvarchar(40)) + ''''

     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end