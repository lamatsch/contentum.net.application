﻿
CREATE procedure [dbo].[sp_KRT_PartnerekGetAllForSzamla]
  @Where nvarchar(MAX) = '',
  @Cim_Id uniqueidentifier = null,
  @Bankszamlaszam_Id uniqueidentifier = null,
  @OrderBy nvarchar(200) = ' order by KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select' + @LocalTopRow + '
		KRT_Partnerek.Id,
		KRT_Partnerek.Nev,
		KRT_Cimek_Postai.Id as Cim_Id,
		dbo.fn_MergeCim
		(KRT_Cimek_Postai.Tipus,
			null,
			null,
			KRT_Cimek_Postai.TelepulesNev,
			KRT_Cimek_Postai.KozteruletNev,
			KRT_Cimek_Postai.KozteruletTipusNev,
			KRT_Cimek_Postai.Hazszam,
			KRT_Cimek_Postai.Hazszamig,
			KRT_Cimek_Postai.HazszamBetujel,
			KRT_Cimek_Postai.Lepcsohaz,
			KRT_Cimek_Postai.Szint,
			KRT_Cimek_Postai.Ajto,
			KRT_Cimek_Postai.AjtoBetujel,
			-- BLG_1347
			KRT_Cimek_Postai.HRSZ,
			KRT_Cimek_Postai.CimTobbi) as CimNev,     
		KRT_Cimek_Postai.TelepulesNev,
		KRT_Cimek_Postai.IRSZ,
		KRT_Cimek_Postai.HRSZ,
		KRT_Cimek_Postai.CimTobbi,
	(select top 1 KRT_Cimek.CimTobbi from KRT_Cimek join KRT_PartnerCimek
		on KRT_Cimek.Id=KRT_PartnerCimek.Cim_Id and KRT_Cimek.Tipus=''03''
		and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege
		and KRT_PartnerCimek.Partner_id = KRT_Partnerek.Id
		order by KRT_PartnerCimek.Fajta
	) as EmailCim,
	(select top 1 KRT_Cimek.CimTobbi from KRT_Cimek join KRT_PartnerCimek
		on KRT_Cimek.Id=KRT_PartnerCimek.Cim_Id and KRT_Cimek.Tipus=''04''
		and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege
		and KRT_PartnerCimek.Partner_id = KRT_Partnerek.Id
		order by KRT_PartnerCimek.Fajta
	) as Telefonszam,
	(select top 1 KRT_Cimek.CimTobbi from KRT_Cimek join KRT_PartnerCimek
		on KRT_Cimek.Id=KRT_PartnerCimek.Cim_Id and KRT_Cimek.Tipus=''05''
		and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege
		and KRT_PartnerCimek.Partner_id = KRT_Partnerek.Id
		order by KRT_PartnerCimek.Fajta
	) as Fax,'

set @sqlcmd = @sqlcmd + '
BelfoldiAdoszam = case KRT_Partnerek.Tipus
	when ''10'' then (select case when (KRT_Vallalkozasok.KulfoldiAdoszamJelolo is null) or (KRT_Vallalkozasok.KulfoldiAdoszamJelolo<>''1'') then KRT_Vallalkozasok.Adoszam else null end)
	when ''20'' then (select case when KRT_Szemelyek.KulfoldiAdoszamJelolo<>''1'' then KRT_Szemelyek.Adoszam else null end)
	else null end,
KozossegiAdoszam = case KRT_Partnerek.Tipus
	when ''10'' then (select case KRT_Vallalkozasok.KulfoldiAdoszamJelolo when ''1'' then KRT_Vallalkozasok.Adoszam else null end)
	when ''20'' then (select case KRT_Szemelyek.KulfoldiAdoszamJelolo when ''1'' then KRT_Szemelyek.Adoszam else null end)
	else null end,
AdoazonositoJel = case KRT_Partnerek.Tipus
	when ''10'' then (select cast(null as nvarchar(20)))
	when ''20'' then (select KRT_Szemelyek.Adoazonosito)
	else null end,'

if @Bankszamlaszam_Id is not null
begin
set @sqlcmd = @sqlcmd + '
KRT_Bankszamlaszamok.Bankszamlaszam as Bankszamlaszam,
Bankszamlaszam1 = case
	when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=8 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,1,8)
	else null end,
Bankszamlaszam2 = case
	when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=16 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,9,8)
	else null end,
Bankszamlaszam3 = case
	when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=24 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,17,8)
	else null end'
end
else
begin
set @sqlcmd = @sqlcmd + '
null as Bankszamlaszam,
null as Bankszamlaszam1,
null as Bankszamlaszam2,
null as Bankszamlaszam3'
end

set @sqlcmd = @sqlcmd + '
from 
	KRT_Partnerek as KRT_Partnerek
'

if @Bankszamlaszam_Id is not null
begin
set @sqlcmd = @sqlcmd + '
	left join KRT_Bankszamlaszamok on KRT_Bankszamlaszamok.Partner_Id=KRT_Partnerek.Id
		and getdate() between KRT_Bankszamlaszamok.ErvKezd and KRT_Bankszamlaszamok.ErvVege
		and KRT_Bankszamlaszamok.Id=''' + CAST(@Bankszamlaszam_Id as Nvarchar(36)) + ''''
end

set @sqlcmd = @sqlcmd + '
	left join (select Rank() over (partition by KRT_PartnerCimek.Partner_Id order by KRT_PartnerCimek.Fajta, KRT_Cimek.Id) as Rank,
			KRT_Cimek.Id,
			KRT_Cimek.Tipus,
			KRT_Cimek.OrszagNev,
			KRT_Cimek.IRSZ,
			KRT_Cimek.TelepulesNev,
			KRT_Cimek.KozteruletNev,
			KRT_Cimek.KozteruletTipusNev,
			KRT_Cimek.Hazszam,
			KRT_Cimek.Hazszamig,
			KRT_Cimek.HazszamBetujel,
			KRT_Cimek.Lepcsohaz,
			KRT_Cimek.Szint,
			KRT_Cimek.Ajto,
			KRT_Cimek.AjtoBetujel,
			KRT_Cimek.CimTobbi,
			KRT_Cimek.HRSZ,
			KRT_PartnerCimek.Partner_id
		from KRT_Cimek join KRT_PartnerCimek
		on KRT_Cimek.Id=KRT_PartnerCimek.Cim_Id and KRT_Cimek.Tipus=''01''
		and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege'

if @Cim_Id is not null
begin
set @sqlcmd = @sqlcmd + '
	and KRT_Cimek.Id=''' + CAST(@Cim_Id as Nvarchar(36)) + ''''
end

set @sqlcmd = @sqlcmd + '
	) KRT_Cimek_Postai on KRT_Cimek_Postai.Rank=1 and KRT_Cimek_Postai.Partner_id = KRT_Partnerek.Id
left join KRT_Vallalkozasok on KRT_Vallalkozasok.Partner_Id=KRT_Partnerek.Id
	and getdate() between KRT_Vallalkozasok.ErvKezd and KRT_Vallalkozasok.ErvVege and KRT_Partnerek.Tipus=''10''
left join KRT_Szemelyek on KRT_Szemelyek.Partner_Id=KRT_Partnerek.Id
	and getdate() between KRT_Szemelyek.ErvKezd and KRT_Szemelyek.ErvVege and KRT_Partnerek.Tipus=''20''
	Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
 
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end