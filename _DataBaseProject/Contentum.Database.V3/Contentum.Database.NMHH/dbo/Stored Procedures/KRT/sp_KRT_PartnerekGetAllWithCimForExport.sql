﻿CREATE procedure [dbo].[sp_KRT_PartnerekGetAllWithCimForExport]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @CimTipus nvarchar(64) = 'all',
  @CimFajta nvarchar(64) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @sqlcmdSzemelyek nvarchar(MAX)
   DECLARE @sqlcmdSzervezetek nvarchar(MAX)

   --A partnercímek miatt redundáns sorok lesznek ezekkel meg kell növelni a lekérdezés toprow-ját, hogy jól jöjjön ki
   DECLARE @AllRecordRowCount nvarchar(10)

   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
  --Cim fajtája szerinti szűrés 
  DECLARE @WhereCimFajta nvarchar(100) 
  
  if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and KRT_PartnerCimek.Fajta = ' + @CimFajta 
   end
  else
   begin
      SET @WhereCimFajta = ''
   end

  --Cim tipus szerinti szűrés
  DECLARE @WhereCimTipus nvarchar(100) 
  
  if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus = ' + @CimTipus 
   end
  else
   begin
      SET @WhereCimTipus = ''
   end      

 
	EXECUTE dbo.sp_KRT_PartnerekGetAllWithCimForExportRowCount 
	@Where=@Where,
	@OrderBy=N' order by KRT_Partnerek.Nev ASC',
	@TopRow=@TopRow,
	@ExecutorUserId='54E861A5-36ED-44CA-BAA7-C287D125B309',
	@WithAllCim ='1',
	@CimTipus = 'all',
	@CimFajta = '',
	@TotalCount = @AllRecordRowCount  OUTPUT

	if (@AllRecordRowCount = '' or @AllRecordRowCount = '0')
     BEGIN
       SET @AllRecordRowCount = ''
     END
   ELSE
     BEGIN
       SET @AllRecordRowCount = ' TOP ' + @AllRecordRowCount
     END

  SET @sqlcmd =
  ' select '+ @AllRecordRowCount + '
       KRT_Partnerek.Nev as Név,
	   OrszagokSzervezet.Nev as Ország,
       KRT_Szemelyek.UjTitulis as Titulus,
	   KRT_Szemelyek.UjCsaladiNev as ''Családi név'',
	   KRT_Szemelyek.UjUtonev as Utónév,
       KRT_Szemelyek.UjTovabbiUtonev as ''További utónevek'',
	   KRT_Partnerek.Allapot as Állapot,
	   (SELECT KRT_KodTarak.Nev FROM KRT_KodTarak WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = ''PARTNER_FORRAS'') AND KRT_KodTarak.Kod = KRT_Partnerek.Forras collate Hungarian_CI_AS and getdate() between KRT_KodTarak.ErvKezd and KRT_KodTarak.ErvVege ) as Forrás,
	   KRT_Vallalkozasok.Cegjegyzekszam as Cégjegyzékszám,
	   KRT_Vallalkozasok.TB_Torzsszam as "TB Törzsszám",
	   (SELECT KRT_KodTarak.Nev FROM KRT_KodTarak WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = ''GFO_KOD'') AND KRT_KodTarak.Kod = CAST(CAST(KRT_Vallalkozasok.Tipus AS INTEGER) AS VARCHAR) and getdate() between KRT_KodTarak.ErvKezd and KRT_KodTarak.ErvVege ) AS Cégforma,
	   CASE KRT_Partnerek.Belso WHEN 1 THEN ''I'' WHEN 0 THEN ''N'' END AS Belső,
	   CONVERT( varchar, KRT_Partnerek.ErvKezd, 23 ) as ''Érv.kezd.'',
	   CONVERT( varchar, KRT_Partnerek.ErvVege, 23 )as ''Érv.vége'',
	   (SELECT MAX( CONVERT( varchar, EREC_KuldKuldemenyek.LetrehozasIdo, 23 ) ) FROM EREC_KuldKuldemenyek  WHERE EREC_KuldKuldemenyek.Partner_Id_Bekuldo = KRT_Partnerek.Id ) as ''Utolsó használat dátuma'',
	   CONVERT( varchar, COALESCE(KRT_Partnerek.ModositasIdo, KRT_Partnerek.LetrehozasIdo), 120) as ''Állapotváltozás ideje'',
	   Letrehozo.Nev as Létrehozó,
	   CONVERT( varchar, KRT_Partnerek.LetrehozasIdo, 23 ) as ''Létrehozás ideje'',
	   Modosito.Nev as Módositó,
	   CONVERT( varchar, Modosito.ModositasIdo, 23 ) as ''Módosítás ideje'',
	   KRT_Szemelyek.AnyjaNeveCsaladiNev as ''Anyja családi neve'',
       KRT_Szemelyek.AnyjaNeveElsoUtonev as ''Anyja utóneve'',
       KRT_Szemelyek.AnyjaNeveTovabbiUtonev as ''Anyja további utónevei'',
	   KRT_Szemelyek.SzuletesiCsaladiNev as ''Születési családi neve'',
       KRT_Szemelyek.SzuletesiElsoUtonev as ''Születési utóneve'',
       KRT_Szemelyek.SzuletesiTovabbiUtonev as ''Születési további utónevei'',
	   KRT_Szemelyek.SzuletesiIdo as ''Születési idő'',
	   KRT_Szemelyek.SzuletesiOrszag as ''Születési ország'',
	   KRT_Szemelyek.SzuletesiHely as ''Születési hely'',
	   KRT_Szemelyek.TAJSzam as ''TAJ szám'','
	   
	   --4000-es limit miatt
	   set @sqlcmd = @sqlcmd + 'KRT_Szemelyek.SZIGSzam as ''Személyazonosító igazolvány okmányazonosítója'',
	   CASE KRT_Szemelyek.Neme WHEN 1 THEN ''F'' WHEN 0 THEN ''N'' END as Neme,
	   KRT_Szemelyek.SzemelyiAzonosito as ''Személy-azonosító jel'',
	   KRT_Szemelyek.Adoazonosito as ''Adóazonosító jel'',
	   KRT_Szemelyek.Adoszam as ''Adószám'',
	   KRT_Vallalkozasok.Adoszam as ''SzervezetAdószám'',
	   CASE KRT_Szemelyek.KulfoldiAdoszamJelolo WHEN 1 THEN ''I'' WHEN 0 THEN ''N'' END as "Külf.",
	   KRT_Szemelyek.Beosztas as Beosztás,
	   KRT_Szemelyek.MinositesiSzint as ''Minősítési szint'',
	   (SELECT KRT_KodTarak.Nev FROM KRT_KodTarak WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = ''CIM_FAJTA'') AND KRT_KodTarak.Kod = KRT_PartnerCimek.Fajta and getdate() between KRT_KodTarak.ErvKezd and KRT_KodTarak.ErvVege ) as Címfajta,
	   KRT_Orszagok.Nev as ''Cím ország'',
	   KRT_Cimek.IRSZ as Irányítószám,
	   KRT_Cimek.TelepulesNev as Település,
	   KRT_Cimek.KozteruletNev as Közterület,
	   KRT_Cimek.KozteruletTipusNev as ''Közterület típusa'',
	   CASE KRT_Cimek.Tipus WHEN ''02'' THEN ''Pf:'' + KRT_Cimek.Hazszam ELSE KRT_Cimek.Hazszam END as Házszám,
	   KRT_Cimek.HazszamBetujel as ''Házszám betűjele'',
	   KRT_Cimek.Lepcsohaz as Lépcsőház,
	   KRT_Cimek.Szint as Emelet,
	   KRT_Cimek.Ajto as Ajtó,
	   KRT_Cimek.AjtoBetujel as ''Ajtó betűjele'',
	   KRT_Cimek.HRSZ as ''Helyrajzi szám'',
	   KRT_Partnerek.Tipus as PartnerTipus
	   INTO #ALL
   from 
     KRT_Partnerek as KRT_Partnerek
     left join KRT_PartnerCimek
     join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege ' + @WhereCimFajta + ' ' + @WhereCimTipus + '
     on KRT_Partnerek.Id = KRT_PartnerCimek.Partner_id and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege 
     left join KRT_Szemelyek as KRT_Szemelyek on KRT_Partnerek.Id = KRT_Szemelyek.Partner_Id
	 left join KRT_Orszagok on KRT_Orszagok.Id = KRT_Cimek.Orszag_Id
	 left join KRT_Orszagok OrszagokSzervezet on OrszagokSzervezet.Id = KRT_Partnerek.Orszag_Id
	 left join KRT_Felhasznalok Letrehozo on KRT_Partnerek.Letrehozo_id = Letrehozo.Id
	 left join KRT_Felhasznalok Modosito on KRT_Partnerek.Modosito_id = Modosito.Id
	 left join KRT_Vallalkozasok on KRT_Vallalkozasok.Partner_Id = KRT_Partnerek.Id
	        Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
 
   --PartnerTipus = 10  - Szervezetek
   --PartnerTipus = 20  - Személyek
   SET @sqlcmd = @sqlcmd + 
   ' select [Név], 
            [Ország], 
			[Állapot], 
			[Állapotváltozás ideje],
			[Forrás], 
			[Belső], 
			[Utolsó használat dátuma],
			[SzervezetAdószám] as Adószám, 
			[Külf.], 
			[Cégjegyzékszám], 
			[TB Törzsszám], 
			[Cégforma], 
			[Érv.kezd.], 
			[Érv.vége], 
			[Létrehozó], 
			[Létrehozás ideje], 
			[Módositó], 
			[Módosítás ideje], 
			[Címfajta], 
			[Cím ország], 
			[Irányítószám], 
			[Település], 
			[Közterület], 
			[Közterület típusa], 
			[Házszám], 
			[Házszám betűjele],
			[Lépcsőház],
			[Emelet],
			[Ajtó],
			[Helyrajzi szám]
			FROM #ALL WHERE PartnerTipus = 10  
			
			select [Név],
				   [Titulus], 
				   [Családi név],
				   [Utónév],
				   [További utónevek],
				   [Ország],
				   [Állapot],
				   [Állapotváltozás ideje],
				   [Forrás],
				   [Belső],
				   [Utolsó használat dátuma],
				   [Érv.kezd.], 
				   [Érv.vége], 
				   [Létrehozó], 
				   [Létrehozás ideje], 
				   [Módositó], 
				   [Módosítás ideje], 
				   [Anyja családi neve],
				   [Anyja utóneve],
				   [Anyja további utónevei],
				   [Születési családi neve],
				   [Születési utóneve],
				   [Születési további utónevei],
				   [Születési idő],
	               [Születési ország],
	               [Születési hely],
				   [TAJ szám],
				   [Személyazonosító igazolvány okmányazonosítója],
				   [Neme],
				   [Személy-azonosító jel],
				   [Adóazonosító jel],
				   [Adószám],
				   [Külf.],
				   [Beosztás],
				   [Minősítési szint],
				   [Címfajta],
				   [Cím ország],
				   [Irányítószám],
				   [Település],
				   [Közterület], 
				   [Közterület típusa], 
				   [Házszám], 
				   [Házszám betűjele],
				   [Lépcsőház],
				   [Emelet],
				   [Ajtó],
				   [Helyrajzi szám]
				   FROM #ALL WHERE PartnerTipus = 20'
 
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
