﻿CREATE procedure [dbo].[sp_KRT_PartnerekGetAllWithCimForExportRowCount]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @CimTipus nvarchar(64) = 'all',
  @CimFajta nvarchar(64) = '',
  @TotalCount nvarchar(64) OUTPUT

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
  

   DECLARE @LocalTopRow nvarchar(10)
   

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
  --Cim fajtája szerinti szűrés 
  DECLARE @WhereCimFajta nvarchar(100) 
  
  if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and KRT_PartnerCimek.Fajta = ' + @CimFajta 
   end
  else
   begin
      SET @WhereCimFajta = ''
   end

  --Cim tipus szerinti szűrés
  DECLARE @WhereCimTipus nvarchar(100) 
  
  if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus = ' + @CimTipus 
   end
  else
   begin
      SET @WhereCimTipus = ''
   end      

   SET @sqlcmd = 
  'select' + @LocalTopRow + '
       KRT_Partnerek.Id
	   INTO #AllRecordIds
   from 
     KRT_Partnerek 
	        Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
   --SET @sqlcmd = @sqlcmd + ' select count(*) from #AllRecordIds'

   SET @sqlcmd = @sqlcmd +
   ' select count(*) as count into #CountTable from #AllRecordIds  
     left join KRT_PartnerCimek
     join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege ' + @WhereCimFajta + ' ' + @WhereCimTipus + '
     on #AllRecordIds.Id = KRT_PartnerCimek.Partner_id and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege '
   
   
   SET @sqlcmd = @sqlcmd + '  select @ResultUid = count from #CountTable';
   exec sp_executesql @sqlcmd,N'@ResultUid int OUTPUT',@ResultUid = @TotalCount OUTPUT
   
   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
