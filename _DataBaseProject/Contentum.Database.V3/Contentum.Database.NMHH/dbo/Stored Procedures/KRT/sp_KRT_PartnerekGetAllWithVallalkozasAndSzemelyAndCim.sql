﻿CREATE procedure [dbo].[sp_KRT_PartnerekGetAllWithVallalkozasAndSzemelyAndCim]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @CimTipus nvarchar(64) = 'all',
  @CimFajta nvarchar(64) = '05', -- Levelezesi cim  
  @partnerId nvarchar(128) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null
as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
    DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end  
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
  --Cim fajtája szerinti szűrés 
  DECLARE @WhereCimFajta nvarchar(100) 
  
  if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and KRT_PartnerCimek.Fajta = ' + @CimFajta 
   end
  else
   begin
      SET @WhereCimFajta = ''
   end

  --Cim tipus szerinti szűrés
  DECLARE @WhereCimTipus nvarchar(100) 
  
  if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus = ' + @CimTipus 
   end
  else
   begin
      SET @WhereCimTipus = ''
   end      

  DECLARE @WherePartnerId nvarchar(100) 
  
  if @partnerId != '0'
   begin
      SET @WherePartnerId = ' and dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(KRT_Partnerek.Id) = ' + @partnerId 
   end
  else
   begin
      SET @WherePartnerId = ''
   end      
   
   		SET @sqlcmd = N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	    KRT_Partnerek.Id into #result
          from KRT_Partnerek as KRT_Partnerek
     left join KRT_Szemelyek as KRT_Szemelyek on KRT_Partnerek.Id = KRT_Szemelyek.Partner_Id
	 left join KRT_Vallalkozasok as KRT_Vallalkozasok on KRT_Partnerek.Id = KRT_Vallalkozasok.Partner_Id	 
     left join KRT_PartnerCimek as KRT_PartnerCimek on KRT_Partnerek.Id = KRT_PartnerCimek.Partner_id and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege 
     left join KRT_Cimek KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege ' + @WhereCimFajta + ' ' + @WhereCimTipus + '
            Where KRT_Partnerek.Org=@Org ' + @WherePartnerId
	        
			  
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

		if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'


  SET @sqlcmd = @sqlcmd +
  'select
    #result.RowNumber,	
	KRT_Partnerek.Id as PartnerOrigId,
  	   dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(KRT_Partnerek.Id) as Id,
	   KRT_Partnerek.Org,
	   KRT_Partnerek.Orszag_Id,
	   KRT_Partnerek.Tipus,
	   CASE WHEN (KRT_Szemelyek.Id IS NOT NULL AND KRT_Szemelyek.UjTitulis IS NOT NULL)
		   THEN 
			 (  ISNULL(KRT_Szemelyek.UjTitulis,'''') + '' '' +
			    ISNULL(KRT_Szemelyek.UjCsaladiNev,'''') +'' '' +
			    ISNULL(KRT_Szemelyek.UjUtonev,'''') + '' '' +
			    ISNULL(KRT_Szemelyek.UjTovabbiUtonev,'''')
			 )
		   ELSE
			  KRT_Partnerek.Nev
		   END 
	   AS Nev,	   
	   KRT_Partnerek.LetrehozoSzervezet,
	   KRT_Partnerek.MaxMinosites,	   
	   KRT_Partnerek.Belso,
	   KRT_Partnerek.Forras,
	   KRT_Partnerek.Ver,
	   KRT_Partnerek.Note,
	   KRT_Partnerek.Stat_id,
	   KRT_Partnerek.ErvKezd,
	   KRT_Partnerek.ErvVege,
	   KRT_Partnerek.Letrehozo_id,
	   KRT_Partnerek.LetrehozasIdo,
	   KRT_Partnerek.Modosito_id,
	   KRT_Partnerek.ModositasIdo,
	   KRT_Partnerek.Zarolo_id,
	   KRT_Partnerek.ZarolasIdo,
	   KRT_Partnerek.Tranz_id,
	   KRT_Partnerek.UIAccessLog_id,
	   KRT_Partnerek.UtolsoHasznalat,
	   CASE   
		WHEN KRT_Partnerek.ErvVege > getdate() THEN 1   
		ELSE 0   
		END as  IsCancelled,
       KRT_Cimek.Id as CimId,
	   dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(KRT_Cimek.Id) as CimIdASPAdo,
       dbo.fn_MergeCim
          (KRT_Cimek.Tipus,
           KRT_Cimek.OrszagNev,
           KRT_Cimek.IRSZ,
           KRT_Cimek.TelepulesNev,
           KRT_Cimek.KozteruletNev,
           KRT_Cimek.KozteruletTipusNev,
           KRT_Cimek.Hazszam,
           KRT_Cimek.Hazszamig,
           KRT_Cimek.HazszamBetujel,
	       KRT_Cimek.Lepcsohaz,
	       KRT_Cimek.Szint,
	       KRT_Cimek.Ajto,
	       KRT_Cimek.AjtoBetujel,
		   -- BLG_1347
			KRT_Cimek.HRSZ,
	       KRT_Cimek.CimTobbi) as CimNev,     
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus as CimTipus,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   KRT_Cimek.Orszag_Id,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi,
	   KRT_Szemelyek.SzuletesiIdo,
	   KRT_Szemelyek.SzuletesiHely,
	   KRT_Szemelyek.SzuletesiNev,
	   KRT_Szemelyek.UjCsaladiNev,
	   KRT_Szemelyek.UjUtonev,
	   KRT_Szemelyek.UjTovabbiUtonev,	   
	   KRT_Szemelyek.AnyjaNeve,
	   KRT_Szemelyek.UjTitulis,	   
	   KRT_Szemelyek.Adoazonosito,
	   KRT_Vallalkozasok.Cegjegyzekszam,
	   COALESCE(KRT_Szemelyek.Adoszam,KRT_Vallalkozasok.Adoszam) as Adoszam,
	   KRT_Cimek.ErvVege as CimekErvVege,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.IRSZ,
	   KRT_Orszagok.Kod as OrszagKod
   from 
     KRT_Partnerek as KRT_Partnerek
	 inner join #result on #result.Id = KRT_Partnerek.Id
     left join KRT_PartnerCimek on KRT_Partnerek.Id = KRT_PartnerCimek.Partner_id and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege      
     left join KRT_Szemelyek as KRT_Szemelyek on KRT_Partnerek.Id = KRT_Szemelyek.Partner_Id
	 left join KRT_Vallalkozasok as KRT_Vallalkozasok on KRT_Partnerek.Id = KRT_Vallalkozasok.Partner_Id	 
	 join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege 
	 left join KRT_Orszagok as KRT_Orszagok on KRT_Cimek.Orszag_Id = KRT_Orszagok.Id
	 WHERE RowNumber between @firstRow and @lastRow ' + @WhereCimFajta + ' ' + @WhereCimTipus + '
	ORDER BY #result.RowNumber; '	        
  

   -- Találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';

	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end