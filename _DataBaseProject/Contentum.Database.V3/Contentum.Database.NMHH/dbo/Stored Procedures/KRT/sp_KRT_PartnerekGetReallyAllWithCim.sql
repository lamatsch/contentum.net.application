-- set ANSI_NULLS ON
-- set QUOTED_IDENTIFIER ON
-- go

-- if exists (select 1
            -- from  sysobjects
           -- where  id = object_id('sp_KRT_PartnerekGetReallyAllWithCim')
            -- and   type = 'P')
   -- drop procedure sp_KRT_PartnerekGetReallyAllWithCim
-- go

CREATE procedure  [dbo].[sp_KRT_PartnerekGetReallyAllWithCim]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by Nev ASC, CimNev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @WithKuldemeny char(1) = '1',
  @CimTipus nvarchar(4000) = 'all',
  @CimFajta nvarchar(64) = '05', -- Levelezesi cim,
  @WithKapcsoltPartner char(1) ='0'

as
begin

SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
BEGIN TRANSACTION;
set nocount on

BEGIN TRY
   
   DECLARE @sqlcmd           nvarchar(4000)

   DECLARE @where_Partner_Cim_delim nvarchar(25) 
   DECLARE @where_begin      nvarchar(25)
   DECLARE @where_end        nvarchar(400)
   DECLARE @whereOrig        nvarchar(400)
   DECLARE @whereCimNev      nvarchar(400)
   DECLARE @whereCimpartDelimPos INT;
   DECLARE @whereCimpartEndPos INT;
   
   DECLARE @LocalTopRow      nvarchar(10)

   DECLARE @Org              uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
		RAISERROR('[50202]',16,1)
   end

	--T�bb c�m jelenjen-e meg egy partnerhez a gyorslist�ban, vagy csak egy
   DECLARE @TobbcimEgyPartnerhez int
   
   IF((select top 1 Ertek from KRT_Parameterek where Nev='TOBB_CIM_PARTNER_KERESOBEN'
                     and Org=@Org and getdate() between ErvKezd and ErvVege) = 1)
	  set @TobbcimEgyPartnerhez = 1
    ELSE
      set @TobbcimEgyPartnerhez = 0
   
   --Cim fajt�ja szerinti sz�r�s 
   DECLARE @WhereCimFajta    nvarchar(100) 

   SET @where_Partner_Cim_delim = '|'
   SET @whereOrig = @where
   SET @whereCimpartDelimPos = Charindex(@where_Partner_Cim_delim, @whereOrig)
   IF @whereCimpartDelimPos !=0
    --BEGIN
   	-- Print 'nincs c�m r�sz...' 
    --END
    --ELSE
   BEGIN
   	-- Print 'van c�m r�sz, poz�ci�:' 
   	-- Print @whereCimpartDelimPos
   	SET @whereCimpartEndPos = Charindex('%'')', @whereOrig, @whereCimpartDelimPos )
   	-- Print '%'') poz�ci�:' 
   	-- Print @whereCimpartEndPos
   	SET @whereCimNev = SUBSTRING(@whereOrig,@whereCimpartDelimPos+1,@whereCimpartEndPos - @whereCimpartDelimPos -1)
   	SET @where = SUBSTRING(@whereOrig,1, @whereCimpartDelimPos-1 ) + SUBSTRING(@whereOrig,@whereCimpartEndPos, 500)
    -- Print '@�j where: ' + @where
    -- Print '@whereCimNev r�sz: ' + @whereCimNev
   END

   SET @where_begin = substring(@Where,1,22)
   SET @where_end   = substring(@Where,23,len(@Where))
    
	--print @where_begin
	--print @where_end

   if (@TopRow = '' or @TopRow = '0')
       SET @LocalTopRow = ''
   ELSE
       SET @LocalTopRow = ' TOP ' + cast(cast((cast(@TopRow as int) * 10) as int) as nvarchar(10))
  
   DECLARE @delimiter nvarchar(20) = ' '

   if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and IsNull(KRT_PartnerCimek.Fajta,''' + @CimFajta + ''') = ' + @CimFajta 
   end
   else begin
      SET @WhereCimFajta = ''
	  IF @WithAllCim = '2' SET @delimiter = ', '
   end

   --Cim tipus szerinti sz�r�s
   DECLARE @WhereCimTipus nvarchar(4000) 
  
   if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus in (' + @CimTipus + ')'
   end
   else
   begin
      SET @WhereCimTipus = ''
   end
   -------------------------------------------------------------------------------------------
   -- 'AJAXOSReallyPartnerkereso_Kuldemenyek' param�ter vizsg�lata
   declare @kellKuldemenyHalmaz nvarchar(100)
   if (@WithKuldemeny = '0')
   begin
       set @kellKuldemenyHalmaz='Nem'
   end
   else
   begin
	   select @kellKuldemenyHalmaz=Ertek from dbo.KRT_Parameterek  
			  where Nev='AJAXOSReallyPartnerkereso_KellKuldemeny'
				and Org=@Org
	   set @kellKuldemenyHalmaz = isnull(@kellKuldemenyHalmaz,'IGEN')
	   if not @kellKuldemenyHalmaz in ('Igen','Nem') set @kellKuldemenyHalmaz='Igen'
   end


   -- Print '-----------------------'
   -- Print @kellKuldemenyHalmaz
   -- Print '-----------------------'
   ------------------------------------------------------------------------------------------


create table #t
(
  Id uniqueidentifier, 
  Nev nvarchar(400), 
  Knev nvarchar(400),
  CimId uniqueidentifier,
  CimNev nvarchar(400), 
  Forras char(1) ,  
  Belso char(1)
)

-- BUG_5197 (nekrisz)
Declare @NevStr nvarchar(400)
Declare @KapcsoltNevFilter nvarchar(400)
DECLARE @whereKapcsNevFiltDelimPos INT
DECLARE @whereKapcsNevFiltEndPos INT

IF @WithKapcsoltPartner = '1'

BEGIN
	SET @NevStr= 'KRT_Partnerek.Nev + CASE 
					  WHEN KapcsoltNev IS NULL THEN '''' 
					  WHEN KapcsoltNev = ''''    THEN ''''
					  ELSE '' - '' + KapcsoltNev
				  END AS Nev,
				  KapcsoltNev as Knev,
				  '
   
   SET @whereKapcsNevFiltDelimPos = Charindex('KRT_Partnerek.Nev like' , @where) +23
   IF @whereKapcsNevFiltDelimPos !=0
   BEGIN
	--print @where
   	-- Print @whereKapcsNevFiltDelimPos
   	SET @whereKapcsNevFiltEndPos = Charindex('%'')', @where, @whereKapcsNevFiltDelimPos )+2
   	-- Print '%'') poz�ci�:' 
   	-- Print @whereKapcsNevFiltEndPos
   	SET @KapcsoltNevFilter = SUBSTRING(@where,@whereKapcsNevFiltDelimPos,@whereKapcsNevFiltEndPos-@whereKapcsNevFiltDelimPos)
	--print @KapcsoltNevFilter
	declare @where2 nvarchar(400) 
	SET @where2 = LEFT(@where,@whereKapcsNevFiltEndPos-1) + ' or KapcsoltNev like '+@KapcsoltNevFilter +' ' + SUBSTRING(@where,@whereKapcsNevFiltEndPos,Len(@where)-(@whereKapcsNevFiltEndPos-1)) 
	--print @where
	--print @where2
	SET @where = @where2 +' '
   END

END
ELSE
BEGIN
	SET @NevStr= 'KRT_Partnerek.Nev,
				 '''' AS Knev,
				 '
END
IF @TobbcimEgyPartnerhez = 1
BEGIN
	SET @sqlcmd = 'insert into #t '+
	'select distinct ' + @LocalTopRow + ' KRT_Partnerek.Id as Id, '+
	-- BUG_5197 (nekrisz)
			@NevStr +'
		   cim.Id as CimId,                       
			dbo.fn_MergeCim2
			  (cim.Tipus,
			   cim.OrszagNev,
			   cim.IRSZ,
			   cim.TelepulesNev,
			   cim.KozteruletNev,
			   cim.KozteruletTipusNev,
			   cim.Hazszam,
			   cim.Hazszamig,
			   cim.HazszamBetujel,
			   cim.Lepcsohaz,
			   cim.Szint,
			   cim.Ajto,
			   cim.AjtoBetujel,
			   -- BLG_1347
			   cim.HRSZ,
			   cim.CimTobbi, 
			   ''' + @delimiter + ''', '' '') as CimNev,
			   KRT_Partnerek.Forras as Forras,
			   KRT_Partnerek.Belso as Belso
	from
	(SELECT DISTINCT Id, Nev, null as kapcsoltId, null as Kapcsoltnev, Forras, Belso, Org, KRT_Partnerek.ErvKezd, KRT_Partnerek.ErvVege from KRT_Partnerek
	UNION
	SELECT KRT_Partnerek.Id, KRT_Partnerek.Nev, PK.Partner_id_kapcsolt as kapcsoltId, PA1.nev as Kapcsoltnev, KRT_Partnerek.Forras, KRT_Partnerek.Belso, KRT_Partnerek.Org, KRT_Partnerek.ErvKezd, KRT_Partnerek.ErvVege  FROM KRT_Partnerek
	INNER JOIN KRT_PartnerKapcsolatok PK 
			  ON KRT_Partnerek.Id = PK.Partner_id_kapcsolt AND PK.Tipus = ''6'' and getdate() between PK.ErvKezd and PK.ErvVege
	INNER JOIN KRT_Partnerek PA1 
			  ON PK.Partner_id = PA1.Id 
			  ) KRT_Partnerek 
	left join 
	(select KRT_Cimek.*, KRT_PartnerCimek.partner_id from KRT_PartnerCimek
	 left join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id
		   '+ @WhereCimFajta + ' ' + @WhereCimTipus + '
		   and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		   and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege
		   ) cim
	on KRT_Partnerek.Id = cim.partner_id
		   Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
		  
END
ELSE
BEGIN
	SET @sqlcmd = 'insert into #t '+
	'select distinct ' + @LocalTopRow + ' KRT_Partnerek.Id as Id, ' +
	-- BUG_5197 (nekrisz)
			@NevStr +'
		   (select top 1 KRT_Cimek.Id
			 from KRT_PartnerCimek join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id 
				  and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
				and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege '
					+ @WhereCimFajta + ' ' + @WhereCimTipus +             
				  '
				  and KRT_PartnerCimek.partner_id=Krt_Partnerek.Id 
				  ) as CimId,
		   (select top 1 dbo.fn_MergeCim2
			  (KRT_Cimek.Tipus,
			   KRT_Cimek.OrszagNev,
			   KRT_Cimek.IRSZ,
			   KRT_Cimek.TelepulesNev,
			   KRT_Cimek.KozteruletNev,
			   KRT_Cimek.KozteruletTipusNev,
			   KRT_Cimek.Hazszam,
			   KRT_Cimek.Hazszamig,
			   KRT_Cimek.HazszamBetujel,
			   KRT_Cimek.Lepcsohaz,
			   KRT_Cimek.Szint,
			   KRT_Cimek.Ajto,
			   KRT_Cimek.AjtoBetujel,
			   -- BLG_1347
			   KRT_Cimek.HRSZ,
			   KRT_Cimek.CimTobbi,
			   ''' + @delimiter + ''', '' '') as CimNev
			 from KRT_PartnerCimek 

			 inner join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id 
				and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
				and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege '
				  + @WhereCimFajta + ' ' + @WhereCimTipus + ' and KRT_PartnerCimek.partner_id=Krt_Partnerek.Id ) 
		   as CimNev,
		   KRT_Partnerek.Forras as Forras
	,	   KRT_Partnerek.Belso as Belso
		from (SELECT DISTINCT Id, Nev, null as kapcsoltId, null as Kapcsoltnev, Forras, Belso, Org, KRT_Partnerek.ErvKezd, KRT_Partnerek.ErvVege from KRT_Partnerek
	UNION
	SELECT KRT_Partnerek.Id, KRT_Partnerek.Nev, PK.Partner_id_kapcsolt as kapcsoltId, PA1.nev as Kapcsoltnev, KRT_Partnerek.Forras, KRT_Partnerek.Belso, KRT_Partnerek.Org, KRT_Partnerek.ErvKezd, KRT_Partnerek.ErvVege  FROM KRT_Partnerek
	INNER JOIN KRT_PartnerKapcsolatok PK 
			  ON KRT_Partnerek.Id = PK.Partner_id_kapcsolt AND PK.Tipus = ''6'' and getdate() between PK.ErvKezd and PK.ErvVege
	INNER JOIN KRT_Partnerek PA1 
			  ON PK.Partner_id = PA1.Id 
			  ) KRT_Partnerek  
		   Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
END
		   
if @Where is not null and @Where != ''
begin 
-- Warning: az al�bbiakban felt�telezz�k, h a nev oszlop az 1. 5 karakterben van. Mivel a t�rolt elj�r�s
-- k�t helyr�l (k�ldem�ny bek�ld�, p�ld�ny c�mzett) van megh�vva, ez biztos�that�
	SET @sqlcmd = @sqlcmd + ' and ' + @where 
end

/* Ignor�lja az @OrderBy bemen� param�tert */
set @sqlcmd = @sqlcmd + ' order by Nev ASC, CimNev'
--print @sqlcmd
exec (@sqlcmd)
----------------------------------------------------------------------------------------
if @kellKuldemenyHalmaz ='Igen'
begin
  set @sqlcmd = 'insert into #t '+ ' select distinct '  + @LocalTopRow + '
       EREC_KuldKuldemenyek.Partner_Id_Bekuldo as Id,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo as Nev,
	   '''' as Knev,
       EREC_KuldKuldemenyek.Cim_Id as CimId,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo as CimNev,
       ''K'' as Forras,
       ''0'' as Belso
   FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
   WHERE EREC_KuldKuldemenyek.Letrehozo_Id in (select Id from KRT_Felhasznalok where Org=''' + CAST(@Org as Nvarchar(40)) + ''')
	AND (EREC_KuldKuldemenyek.Partner_Id_Bekuldo is NULL
   OR (EREC_KuldKuldemenyek.Cim_Id is NULL AND EREC_KuldKuldemenyek.CimSTR_Bekuldo != ''''))'


  if @Where is not null and @Where != ''
  begin 
    SET @sqlcmd = @sqlcmd + ' and ' + replace(@where_begin,'KRT_Partnerek.Nev','EREC_KuldKuldemenyek.NevSTR_Bekuldo ') 
                      + replace(@where_end,'KRT_Partnerek.','')

  end
  set @sqlcmd = @sqlcmd + ' order by Nev ASC, CimNev'
  exec (@sqlcmd)
end
------------------------------------------------------------------------------------------------
   set @sqlcmd = 'select  distinct top ' + @TopRow + ' Id, Nev, Knev, CimId, CimNev, Forras , Belso
    from #t ' 
   IF  @whereCimpartDelimPos > 0
   BEGIN
   	set @sqlcmd = @sqlcmd + ' where CIMNev LIKE ''%' + @whereCimNev + '%'''
   END
    
    set @sqlcmd = @sqlcmd + @OrderBy 
 -- Print @sqlcmd
   exec (@sqlcmd)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    

	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
ROLLBACK TRANSACTION;

end