﻿--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE PROCEDURE 
sp_KRT_PartnerekGetSzervezetWithKapcsolattarto
  @TopRow				INT					= 100,
  @ExecutorUserId		UNIQUEIDENTIFIER		 ,
  @Like					NVARCHAR(MAX)		= ''		
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
BEGIN TRANSACTION;
SET NOCOUNT ON

BEGIN TRY

   DECLARE @Org              uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
		RAISERROR('[50202]',16,1)
   end

	SELECT DISTINCT TOP(@TopRow) 
		   P.Id 		AS Id, 
		   P.Nev 
				+ CASE 
					  WHEN PA1.Nev IS NULL THEN '' 
					  WHEN PA1.Nev = ''    THEN ''
					  ELSE ' - ' + PA1.Nev
				  END
			AS Nev,
			KC.Id 		AS CimId,                       
			DBO.fn_MergeCim
			  (KC.Tipus,
			   KC.OrszagNev,
			   KC.IRSZ,
			   KC.TelepulesNev,
			   KC.KozteruletNev,
			   KC.KozteruletTipusNev,
			   KC.Hazszam,
			   KC.Hazszamig,
			   KC.HazszamBetujel,
			   KC.Lepcsohaz,
			   KC.Szint,
			   KC.Ajto,
			   KC.AjtoBetujel,
			   KC.HRSZ,
			   KC.CimTobbi) AS CimNev,
		   KC.Tipus			AS CimTipus,
		   KC.CimTobbi		AS CimTobbi,
		   KC.OrszagNev		AS OrszagNev,
		   KC.IRSZ			AS IRSZ,
		   KC.TelepulesNev	AS TelepulesNev,
		   KC.KozteruletNev	AS KozteruletNev,
		   KC.KozteruletTipusNev AS KozteruletTipusNev,
		   KC.Hazszam		AS Hazszam,
		   KC.Hazszamig		AS Hazszamig,
		   KC.HazszamBetujel	 AS	HazszamBetujel,
		   KC.Lepcsohaz			 AS Lepcsohaz,
		   KC.Szint			AS Szint,
		   KC.Ajto			AS Ajto,
		   KC.AjtoBetujel	AS AjtoBetujel,
		   KC.HRSZ			AS HRSZ,
		   P.Forras			AS Forras
	FROM KRT_Partnerek P
	LEFT JOIN KRT_PartnerKapcsolatok PK 
			  ON P.Id = PK.Partner_id_kapcsolt AND PK.Tipus = '6'
	LEFT JOIN KRT_Partnerek PA1 
			  ON PK.Partner_id = PA1.Id 
	LEFT JOIN KRT_PartnerCimek KPC ON P.Id=KPC.Partner_id 
	LEFT JOIN KRT_Cimek KC on KPC.Cim_Id = KC.Id
	WHERE 
		P.ErvKezd <= GETDATE()
	and P.ErvVege >= GETDATE()
	and PA1.ErvKezd <= GETDATE()
	and PA1.ErvVege >= GETDATE()
	and PK.ErvKezd <= GETDATE()
	and PK.ErvVege >= GETDATE()
	and P.Tipus = '10'
	and (P.Nev like @like +'%' OR PA1.Nev like @like +'%')
	--and PK.Tipus = '6'
	ORDER BY Nev ASC

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    

	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
ROLLBACK TRANSACTION;
END