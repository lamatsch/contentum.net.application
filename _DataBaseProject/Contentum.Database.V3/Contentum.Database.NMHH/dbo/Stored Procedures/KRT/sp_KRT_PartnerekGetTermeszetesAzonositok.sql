﻿create procedure [dbo].[sp_KRT_PartnerekGetTermeszetesAzonositok]
         @Nev nvarchar(100) = '',
		 @EmailCim nvarchar(100) = ''
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select sz.*  
		from krt_partnerek as p 
		, krt_partnercimek as pc 
		, krt_cimek as c 
		, krt_szemelyek as sz 
		where pc.partner_id = p.id 
		and pc.cim_id = c.id 
		and sz.partner_id = p.id 
		and getdate() < p.ervvege 
		and c.tipus=''03'' 
		and ltrim(rtrim(lower(p.nev))) = ''' + @Nev +'''  
		and ltrim(rtrim(lower(c.CimTobbi))) = ''' + @EmailCim + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end