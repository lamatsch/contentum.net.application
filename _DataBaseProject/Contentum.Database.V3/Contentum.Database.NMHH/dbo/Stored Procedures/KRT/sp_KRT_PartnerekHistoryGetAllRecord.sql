﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_PartnerekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_PartnerekHistoryGetAllRecord
go
*/
create procedure sp_KRT_PartnerekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_PartnerekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Id --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Orszag_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_OrszagokAzonosito(Old.Orszag_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_OrszagokAzonosito(New.Orszag_Id) as NewValue,  
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Orszag_Id as nvarchar(max)),'') != ISNULL(CAST(New.Orszag_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'PARTNERKAPCSOLAT TIPUSA JDV'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Tipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Tipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hierarchia' as ColumnName,               cast(Old.Hierarchia as nvarchar(99)) as OldValue,
               cast(New.Hierarchia as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Hierarchia as nvarchar(max)),'') != ISNULL(CAST(New.Hierarchia as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulsoAzonositok' as ColumnName,               cast(Old.KulsoAzonositok as nvarchar(99)) as OldValue,
               cast(New.KulsoAzonositok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KulsoAzonositok as nvarchar(max)),'') != ISNULL(CAST(New.KulsoAzonositok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PublikusKulcs' as ColumnName,               cast(Old.PublikusKulcs as nvarchar(99)) as OldValue,
               cast(New.PublikusKulcs as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PublikusKulcs as nvarchar(max)),'') != ISNULL(CAST(New.PublikusKulcs as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kiszolgalo' as ColumnName,               cast(Old.Kiszolgalo as nvarchar(99)) as OldValue,
               cast(New.Kiszolgalo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kiszolgalo as nvarchar(max)),'') != ISNULL(CAST(New.Kiszolgalo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LetrehozoSzervezet' as ColumnName,               cast(Old.LetrehozoSzervezet as nvarchar(99)) as OldValue,
               cast(New.LetrehozoSzervezet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LetrehozoSzervezet as nvarchar(max)),'') != ISNULL(CAST(New.LetrehozoSzervezet as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MaxMinosites' as ColumnName,               cast(Old.MaxMinosites as nvarchar(99)) as OldValue,
               cast(New.MaxMinosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MaxMinosites as nvarchar(max)),'') != ISNULL(CAST(New.MaxMinosites as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesKezdDat' as ColumnName,               cast(Old.MinositesKezdDat as nvarchar(99)) as OldValue,
               cast(New.MinositesKezdDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesKezdDat as nvarchar(max)),'') != ISNULL(CAST(New.MinositesKezdDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesVegDat' as ColumnName,               cast(Old.MinositesVegDat as nvarchar(99)) as OldValue,
               cast(New.MinositesVegDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesVegDat as nvarchar(max)),'') != ISNULL(CAST(New.MinositesVegDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MaxMinositesSzervezet' as ColumnName,               cast(Old.MaxMinositesSzervezet as nvarchar(99)) as OldValue,
               cast(New.MaxMinositesSzervezet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MaxMinositesSzervezet as nvarchar(max)),'') != ISNULL(CAST(New.MaxMinositesSzervezet as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Belso' as ColumnName,               cast(Old.Belso as nvarchar(99)) as OldValue,
               cast(New.Belso as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Belso as nvarchar(max)),'') != ISNULL(CAST(New.Belso as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Forras' as ColumnName,               cast(Old.Forras as nvarchar(99)) as OldValue,
               cast(New.Forras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Forras as nvarchar(max)),'') != ISNULL(CAST(New.Forras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ErvKezd' as ColumnName,               cast(FORMAT(Old.ErvKezd, 'yyyy-MM-dd hh:mm:ss') as nvarchar(99)) as OldValue,
               cast(FORMAT(New.ErvKezd, 'yyyy-MM-dd hh:mm:ss') as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ErvKezd as nvarchar(max)),'') != ISNULL(CAST(New.ErvKezd as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ErvVege' as ColumnName,               cast(FORMAT(Old.ErvVege, 'yyyy-MM-dd hh:mm:ss') as nvarchar(99)) as OldValue,
               cast(FORMAT(New.ErvVege, 'yyyy-MM-dd hh:mm:ss') as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerekHistory Old
         inner join KRT_PartnerekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_PartnerekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ErvVege as nvarchar(max)),'') != ISNULL(CAST(New.ErvVege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go