﻿create procedure [dbo].[sp_KRT_PartnerekRevalidate]
      @Id	               uniqueidentifier,
      @ExecutorUserId		uniqueidentifier,
      @ExecutionTime			datetime	= NULL,
      @ErvKezd             DATETIME = null,
      @ErvVege             DATETIME = null		

as

BEGIN TRY

	set nocount on
   
   SET @ExecutionTime = getdate()
  
   IF @ErvKezd IS NULL
   BEGIN
      SET @ErvKezd = GETDATE()
   END
	
	IF @ErvVege IS NULL
	BEGIN
		SET @ErvVege = '4700-12-31'
	END
   
   DECLARE @Act_ErvKezd datetime
   DECLARE @Act_ErvVege datetime
   DECLARE @Act_Ver int
      
   select
      @Act_ErvKezd = ErvKezd,
      @Act_ErvVege = ErvVege,
      @Act_Ver = Ver
   from KRT_Partnerek
   where Id = @Id
      
   if @@rowcount = 0
	begin
	 RAISERROR('[50652]',16,1)
	end
      
   if GETDATE() BETWEEN @Act_ErvKezd AND @Act_ErvVege
   begin
     -- érvényes
     RAISERROR('[50653]',16,1)
   end
      
   IF @Act_Ver is null
	   SET @Act_Ver = 2	
   ELSE
	   SET @Act_Ver = @Act_Ver+1
      
	UPDATE KRT_Partnerek
	SET		 					
	  ErvKezd = @ErvKezd,
     ErvVege = @ErvVege,
     Ver     = @Act_Ver
   WHERE
	  Id = @Id

   if @@rowcount != 1
   begin
	   RAISERROR('[50651]',16,1)
   end   
   else
   BEGIN
      DECLARE @tempPartnerKapcsolatok TABLE(Id UNIQUEIDENTIFIER)
	    
      INSERT INTO @tempPartnerKapcsolatok (Id) 
      SELECT KRT_PartnerKapcsolatok.Id
	  FROM KRT_PartnerKapcsolatok
	  INNER JOIN KRT_Partnerek p1 ON p1.Id = KRT_PartnerKapcsolatok.Partner_id AND GETDATE() BETWEEN p1.ErvKezd AND p1.ErvVege
	  INNER JOIN KRT_Partnerek p2 ON p2.Id = KRT_PartnerKapcsolatok.Partner_id_kapcsolt AND GETDATE() BETWEEN p2.ErvKezd AND p2.ErvVege
	  WHERE (Partner_id = @Id OR Partner_id_kapcsolt = @Id)
	  AND KRT_PartnerKapcsolatok.ErvVege = @Act_ErvVege
	  
	  UPDATE KRT_PartnerKapcsolatok 
  	  SET ErvKezd = @ErvKezd, ErvVege = @ErvVege
	  WHERE Id IN (SELECT Id FROM @tempPartnerKapcsolatok)
	  
      /* KRT_PartnerKapcsolatok */
	  DECLARE @PartnerKapcsolatId UNIQUEIDENTIFIER
	  DECLARE cur CURSOR FOR
	  SELECT Id FROM @tempPartnerKapcsolatok
  	
	  OPEN cur;
	  FETCH NEXT FROM cur INTO @PartnerKapcsolatId;
	  WHILE @@FETCH_STATUS = 0
	  BEGIN
		exec sp_LogRecordToHistory 'KRT_PartnerKapcsolatok',@PartnerKapcsolatId
		 ,'KRT_PartnerKapcsolatokHistory',-2,@ExecutorUserId,@ExecutionTime
         
		FETCH NEXT FROM cur INTO @PartnerKapcsolatId;
	  END
  	
	  CLOSE cur
	  DEALLOCATE cur
  		
      /* History Log */
      exec sp_LogRecordToHistory 'KRT_Partnerek',@Id
      ,'KRT_PartnerekHistory',-2,@ExecutorUserId,@ExecutionTime    
   end
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH