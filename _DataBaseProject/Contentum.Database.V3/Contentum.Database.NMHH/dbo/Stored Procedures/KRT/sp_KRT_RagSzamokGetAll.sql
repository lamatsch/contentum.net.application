﻿
create procedure sp_KRT_RagSzamokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_RagSzamok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_RagSzamok.Id,
	   KRT_RagSzamok.Kod,
	   KRT_RagSzamok.KodNum,
	   KRT_RagSzamok.Postakonyv_Id,
	   KRT_RagSzamok.RagszamSav_Id,
	   KRT_RagSzamok.Obj_Id,
	   KRT_RagSzamok.ObjTip_Id,
	   KRT_RagSzamok.Obj_type,
	   KRT_RagSzamok.KodType,
	   KRT_RagSzamok.Allapot,
	   CASE KRT_RagSzamok.Allapot 
       		  when ''A'' then ''Allokált'' 
       		  when ''F'' then ''Felhasznált'' 
			  when ''H'' then ''Használható'' 
       		  else ''Törölt'' 
       	END AllapotName,
	   KRT_RagSzamok.Ver,
	   KRT_RagSzamok.Note,
	   KRT_RagSzamok.Stat_id,
	   KRT_RagSzamok.ErvKezd,
	   KRT_RagSzamok.ErvVege,
	   KRT_RagSzamok.Letrehozo_id,
	   KRT_RagSzamok.LetrehozasIdo,
	   KRT_RagSzamok.Modosito_id,
	   KRT_RagSzamok.ModositasIdo,
	   KRT_RagSzamok.Zarolo_id,
	   KRT_RagSzamok.ZarolasIdo,
	   KRT_RagSzamok.Tranz_id,
	   KRT_RagSzamok.UIAccessLog_id  
   from 
     KRT_RagSzamok as KRT_RagSzamok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end