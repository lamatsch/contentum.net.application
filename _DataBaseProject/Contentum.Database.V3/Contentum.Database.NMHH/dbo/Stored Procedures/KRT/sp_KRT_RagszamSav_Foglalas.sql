CREATE procedure [dbo].[sp_KRT_RagszamSav_Foglalas]    
			 @RagszamSav_Id         UNIQUEIDENTIFIER	=	NULL,
			 @Postakonyv_Id			UNIQUEIDENTIFIER	=	NULL,
			 @LetrehozoSzervezet_Id UNIQUEIDENTIFIER			,
			 @Letrehozo_Id			UNIQUEIDENTIFIER	=	NULL,
	         @FoglalandoDarab		INTEGER						,
			 @FoglalandoAllapota	CHAR(1)				=	'H' ,			 
             @ResultUid				NVARCHAR(MAX) OUTPUT
         
AS
BEGIN TRY

SET NOCOUNT ON;		
 
		declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
		select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

		if @sajat_tranz = 1  
		   BEGIN TRANSACTION 
 
		select  @ResultUid = NULL

		declare @error           int
		declare @rowcount        int

		------- 		
		
		IF @RagszamSav_Id IS NULL AND @Postakonyv_Id IS NULL
		     --RAISERROR('Hiányos ragszám paraméterek!',16,1)
			  RAISERROR('[64082]',16,1)
			 
		IF @RagszamSav_Id IS NOT NULL
			BEGIN
					IF NOT EXISTS(SELECT 1 FROM DBO.KRT_RagszamSavok 
								  WHERE ID = @RagszamSav_Id
					     			    AND SAVALLAPOT = 'H')
						--RAISERROR('Nem engedélyezett a ragszámsávon az igénylés!',16,1)
						 RAISERROR('[64083]',16,1)
			END


		IF @RagszamSav_Id IS NULL AND @Postakonyv_Id IS NOT NULL
			SELECT TOP 1 @RagszamSav_Id = ID 
			FROM DBO.KRT_RagszamSavok 
			WHERE Postakonyv_Id = @Postakonyv_Id
				AND SAVALLAPOT = 'H'
		
		IF @RagszamSav_Id IS NULL
		     -- RAISERROR('Nem található felhasználható ragszámsáv!',16,1)
			  RAISERROR('[64084]',16,1)

		IF (@Postakonyv_Id IS NULL)
			SELECT @Postakonyv_Id= Postakonyv_Id
			FROM KRT_RagszamSavok
			WHERE Id = @RagszamSav_Id

		--------------------------------
		-- Darab ellenőrzés - utolsó számjegy checksum!
		--------------------------------
		DECLARE @MAX_RAGSZAM_COUNT	INT

		SELECT @MAX_RAGSZAM_COUNT = COUNT(KodNum) FROM KRT_RAGSZAMOK WHERE RagszamSav_Id = @RagszamSav_Id and Allapot='H'

		IF @FoglalandoDarab > @MAX_RAGSZAM_COUNT
			 --RAISERROR('A foglalni kívánt darabszám nagyobb mint ami rendelkezésre áll!',16,1)
			 RAISERROR('[64085]',16,1)
		

		DECLARE @ragszamok TABLE(
		Id UNIQUEIDENTIFIER
		);
		
		INSERT INTO @ragszamok
		SELECT Top (@FoglalandoDarab) Id
		FROM KRT_RAGSZAMOK
		WHERE RagszamSav_Id=@RagszamSav_Id
		AND Allapot='H'
		order by Kod

		SELECT @ResultUid = COALESCE(@ResultUid + ', ', '') + CAST(Id AS NVARCHAR(MAX))
		FROM @ragszamok		

		UPDATE KRT_RAGSZAMOK
		SET Allapot='F'
		WHERE Id in
		(
		select Id from @ragszamok
		)

		IF NOT EXISTS(select 1 FROM KRT_RagSzamok WHERE RagszamSav_Id=@RagszamSav_Id AND Allapot='H')
		BEGIN
				UPDATE KRT_RagszamSavok 
				SET SavAllapot='F'
				WHERE Id=@RagszamSav_Id
		END

		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		--SET @ResultUid = @I_Id		
 
END TRY
-----------
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1	   

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
