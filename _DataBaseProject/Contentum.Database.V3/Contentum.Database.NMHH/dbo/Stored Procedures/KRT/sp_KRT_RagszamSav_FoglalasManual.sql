﻿CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_FoglalasManual]    
			 @Ragszam		        NVARCHAR(MAX),
			 @RagszamNum	     	FLOAT = NULL,
			 @SavType		        NVARCHAR(100)		=	NULL,
			 @Postakonyv_Id			UNIQUEIDENTIFIER	=	NULL,
			 @Org_Id				UNIQUEIDENTIFIER,
			 @Letrehozo_Id			UNIQUEIDENTIFIER,
             @ResultUid				NVARCHAR(MAX) OUTPUT
AS     
BEGIN TRY

SET NOCOUNT ON;
 
		DECLARE @SAJAT_TRANZ     INT  -- SAJÁT TRANZAKCIÓ KEZELÉS? (0 = NEM, 1 = IGEN)
		SELECT  @SAJAT_TRANZ   = (CASE WHEN @@TRANCOUNT > 0 THEN 0 ELSE 1 END)

		IF @SAJAT_TRANZ = 1  
		   BEGIN TRANSACTION 
 
		SELECT  @RESULTUID = NULL

		DECLARE @ERROR           INT
		DECLARE @ROWCOUNT        INT

		DECLARE @RagszamSav_Id	UNIQUEIDENTIFIER
		-------------------------------------------------------------------------------

		IF NOT EXISTS( SELECT 1 FROM KRT_RAGSZAMOK WHERE Kod = @Ragszam  AND Allapot='H')
			  --RAISERROR('A ragszám még nem létezik!',16,1)
			  RAISERROR('[64080]', 16,1)
			  

		--IF NOT EXISTS(
		-- 		 SELECT 1
		--		 FROM KRT_RagszamSavok
		--		 WHERE 
		--		 --@RagszamNum BETWEEN SavKezdNum and SavVegeNum
		--			--AND 
		--			   SavAllapot = 'H'
		--		 )
		--	 RAISERROR('[64081]', 16,1)
		--	 --RAISERROR('A ragszámhoz nem tartozik megfelelő ragszámsáv !',16,1)

		 SELECT @Postakonyv_Id = Postakonyv_Id,
				@RagszamSav_Id = Id, 
				@SavType = SavType
		 FROM KRT_RagszamSavok
		 WHERE 
		 --@RagszamNum BETWEEN SavKezdNum and SavVegeNum
			--AND 
				SavAllapot = 'H'
				AND Id=(Select top 1 RagszamSav_Id from KRT_RagSzamok where Kod=@Ragszam)
	
					--------------------------------
		-- Darab ellenőrzés - utolsó számjegy checksum!
		--------------------------------
		DECLARE @MAX_RAGSZAM_COUNT	INT

		SELECT @MAX_RAGSZAM_COUNT = COUNT(KodNum) FROM KRT_RAGSZAMOK WHERE RagszamSav_Id = @RagszamSav_Id and Allapot='H'

		IF 1 > @MAX_RAGSZAM_COUNT
			 --RAISERROR('A foglalni kívánt ragszám nagyobb mint ami rendelkezésre áll!',16,1)
			 RAISERROR('[64085]',16,1)
	
	UPDATE KRT_RAGSZAMOK
		SET Allapot='F'
		WHERE Kod= @Ragszam
		
		IF NOT EXISTS(select 1 FROM KRT_RagSzamok WHERE RagszamSav_Id=@RagszamSav_Id AND Allapot='H')
		BEGIN
				UPDATE KRT_RagszamSavok 
				SET SavAllapot='F'
				WHERE Id=@RagszamSav_Id
		END

		------------------------------
		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		SELECT @ResultUid = Id
		FROM KRT_RagSzamok
		WHERE Kod=@Ragszam
 
END TRY
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1	   

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
