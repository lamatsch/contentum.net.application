--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_Igenyles_WithoutInsert]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[sp_KRT_RagszamSav_Igenyles_WithoutInsert]
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER OFF
--GO

CREATE procedure [dbo].[sp_KRT_RagszamSav_Igenyles_WithoutInsert]    
			 @RagszamSav_Id UNIQUEIDENTIFIER,
			 @Kod         Nvarchar(100),
			 @KodNum         float,
			 @Postakonyv_Id			UNIQUEIDENTIFIER	=	NULL,
			 @LetrehozoSzervezet_Id UNIQUEIDENTIFIER			,
			 @Letrehozo_Id			UNIQUEIDENTIFIER	=	NULL,
	         @FoglalandoDarab		INTEGER						,
			 @FoglalandoAllapota	CHAR(1)				=	'H' ,
             @ResultUid				NVARCHAR(MAX) OUTPUT
         
AS
BEGIN TRY

SET NOCOUNT ON;
		select  @ResultUid = NULL

		declare @error           int
		declare @rowcount        int

		IF (@Postakonyv_Id IS NULL)
			SELECT @Postakonyv_Id= Postakonyv_Id
			FROM KRT_RagszamSavok
			WHERE Id = @RagszamSav_Id
	
	DECLARE @ragszamok TABLE
		(
		I_Id uniqueidentifier,
		I_Kod nvarchar(100),
		I_KodNum float,
		I_Postakonyv_Id uniqueidentifier,
		I_RagszamSav_Id uniqueidentifier,
		I_Obj_Id uniqueidentifier,
		I_ObjTip_Id uniqueidentifier,
		I_Obj_type nvarchar(100),
		I_KodType char(1),
		I_Allapot nvarchar(64),
		I_Ver int,
		I_Note nvarchar(4000),
		I_Stat_id uniqueidentifier,
		I_ErvKezd datetime,
		I_ErvVege datetime,
		I_Letrehozo_id uniqueidentifier,
		I_LetrehozasIdo datetime,
		I_Modosito_id uniqueidentifier,
		I_ModositasIdo datetime,
		I_Zarolo_id uniqueidentifier,
		I_ZarolasIdo datetime,
		I_Tranz_id uniqueidentifier,
		I_UIAccessLog_id uniqueidentifier,
		I_UpdatedColumns xml,
		I_ResultUid uniqueidentifier
		)

		SET @KodNum = [dbo].[fn_GetNumeric] (@Kod)

		--------------------------------
		-- Foglalás
		--------------------------------
		DECLARE @cnt INT = 0;
		DECLARE @VAN_MAR_RAGSZAM BIT = 0

		DECLARE @I_SAVKEZD			NVARCHAR(100)
		DECLARE @I_SAVKEZD_NEXT		NVARCHAR(100)

		DECLARE @I_SAVKEZD_NUM		FLOAT
		DECLARE @I_SAVKEZD_NUM_NEXT	FLOAT

		IF (EXISTS(SELECT 1 FROM @ragszamok WHERE  I_RagszamSav_Id = @RagszamSav_Id))
		BEGIN
			SET @VAN_MAR_RAGSZAM = 1

			SELECT TOP 1 @I_SAVKEZD			= I_Kod, 
						 @I_SAVKEZD_NUM		= I_KodNum
					FROM @ragszamok
					WHERE I_RagszamSav_Id = @RagszamSav_Id
					ORDER BY I_KodNum DESC
		END		
		
		--INSERT PARAMS
		DECLARE @I_Id uniqueidentifier
		DECLARE @I_Kod nvarchar(100)
		DECLARE @I_KodNum float
		DECLARE @I_Postakonyv_Id uniqueidentifier
		DECLARE @I_RagszamSav_Id uniqueidentifier
		DECLARE @I_Obj_Id uniqueidentifier
		DECLARE @I_ObjTip_Id uniqueidentifier
		DECLARE @I_Obj_type nvarchar(100)
		DECLARE @I_KodType char(1)
		DECLARE @I_Allapot nvarchar(64)
		DECLARE @I_Ver int
		DECLARE @I_Note nvarchar(4000)
		DECLARE @I_Stat_id uniqueidentifier
		DECLARE @I_ErvKezd datetime
		DECLARE @I_ErvVege datetime
		DECLARE @I_Letrehozo_id uniqueidentifier
		DECLARE @I_LetrehozasIdo datetime
		DECLARE @I_Modosito_id uniqueidentifier
		DECLARE @I_ModositasIdo datetime
		DECLARE @I_Zarolo_id uniqueidentifier
		DECLARE @I_ZarolasIdo datetime
		DECLARE @I_Tranz_id uniqueidentifier
		DECLARE @I_UIAccessLog_id uniqueidentifier
		DECLARE @I_UpdatedColumns xml
		DECLARE @I_ResultUid uniqueidentifier
		--bug 2124 - checksum készítése
		DECLARE @I_SAVKEZD_NUM_SEGED FLOAT
		DECLARE @I_CDV int
		DECLARE @I_CDV_CALC int
		DECLARE @I_CDV_IND int
		DECLARE @I_SAVKEZD_NUM_NEXT_LENGTH int
		DECLARE @I_CDV_CALVULATE_CHAR char(1)
		DECLARE @I_CDV_CALVULATE_INT int
		--DECLARE @I_SAVKEZD_NUM_NEXT_CONVERTED nvarchar(100)
		--
		DECLARE @I_DATE DATETIME
		SET @I_DATE = GETDATE()
		DECLARE @ELSO BIT = 1
		WHILE @cnt < @FoglalandoDarab
		BEGIN
			IF (@VAN_MAR_RAGSZAM = 1)
				BEGIN				
							
					SET @I_SAVKEZD_NUM_SEGED = FLOOR(@I_SAVKEZD_NUM/10)
					SET @I_SAVKEZD_NUM_SEGED = @I_SAVKEZD_NUM_SEGED + 1;
					--SET @I_SAVKEZD_NUM_NEXT = @I_SAVKEZD_NUM +1
					--calculate checksum
					SET @I_CDV_CALC = 0
					SET @I_CDV_IND = 1
					SET @I_SAVKEZD_NUM_NEXT_LENGTH = LEN(LTRIM(STR(@I_SAVKEZD_NUM_SEGED, 25, 0)))
					WHILE @I_CDV_IND<= @I_SAVKEZD_NUM_NEXT_LENGTH
						BEGIN
							--első 4 számjeggyel nem foglalkozunk, mert az az irányítószám
							IF @I_CDV_IND > 4
								BEGIN
									SET @I_CDV_CALVULATE_CHAR = SUBSTRING(LTRIM(STR(@I_SAVKEZD_NUM_SEGED, 25, 0)), @I_CDV_IND, 1)
									SET @I_CDV_CALVULATE_INT = CAST(@I_CDV_CALVULATE_CHAR AS INT)
									if (@I_CDV_IND % 2 = 1)
										SET @I_CDV_CALC = @I_CDV_CALC + @I_CDV_CALVULATE_INT * 3
									ELSE
										SET @I_CDV_CALC = @I_CDV_CALC + @I_CDV_CALVULATE_INT
								END
							SET @I_CDV_IND= @I_CDV_IND+ 1
						END
					SET @I_CDV = @I_CDV_CALC % 10

					SET @I_SAVKEZD_NUM_NEXT = @I_SAVKEZD_NUM_SEGED * 10 + @I_CDV

					SET @I_SAVKEZD_NEXT = REPLACE(@I_SAVKEZD,LTRIM(STR(@I_SAVKEZD_NUM, 25, 0)),LTRIM(STR(@I_SAVKEZD_NUM_NEXT, 25, 0)))

					IF (@I_SAVKEZD_NUM_NEXT IS NULL OR @I_SAVKEZD_NEXT IS NULL)
						--RAISERROR('Ragszám kód hiba!',16,1)
						RAISERROR('[64086]',16,1)

					SET @I_Id				= NEWID()
					SET @I_Kod				= @I_SAVKEZD_NEXT
					SET @I_KodNum			= @I_SAVKEZD_NUM_NEXT
					SET @I_Postakonyv_Id	= @Postakonyv_Id
					SET @I_RagszamSav_Id	= @RagszamSav_Id
					SET @I_Obj_type			= 'KRT_RagszamSavok'
					SET @I_Allapot			= @FoglalandoAllapota
					SET @I_Ver				= '1'
					SET @I_ErvKezd			= @I_DATE	
					SET @I_ErvVege			= '4700-12-31 00:00:00.000'
					SET @I_Letrehozo_id		= @Letrehozo_Id
					SET @I_LetrehozasIdo	= @I_DATE
					
					insert into @ragszamok values(
							 @I_Id
							,@I_Kod
							,@I_KodNum
							,@I_Postakonyv_Id
							,@I_RagszamSav_Id
							,@I_Obj_Id
							,@I_ObjTip_Id
							,@I_Obj_type
							,@I_KodType
							,@I_Allapot
							,@I_Ver
							,@I_Note
							,@I_Stat_id
							,@I_ErvKezd
							,@I_ErvVege
							,@I_Letrehozo_id
							,@I_LetrehozasIdo
							,@I_Modosito_id
							,@I_ModositasIdo
							,@I_Zarolo_id
							,@I_ZarolasIdo
							,@I_Tranz_id
							,@I_UIAccessLog_id
							,@I_UpdatedColumns
							,@I_ResultUid
							)

					IF @ELSO = 1
						BEGIN
							SET @ELSO = 0
							SET @ResultUid =  CAST(@I_Id AS NVARCHAR(MAX))
						END
					ELSE
						SET @ResultUid = @ResultUid + ',' + CAST(@I_Id AS NVARCHAR(MAX))

					SET @I_SAVKEZD			= @I_SAVKEZD_NEXT
					SET @I_SAVKEZD_NEXT		= NULL
					SET @I_SAVKEZD_NUM		= @I_SAVKEZD_NUM_NEXT
					SET @I_SAVKEZD_NUM_NEXT	= NULL

				END
			ELSE
				BEGIN
					SET @I_SAVKEZD		= NULL
					set @I_SAVKEZD_NUM	= NULL

					--LOAD FIRST FROM RAGSZAMSAV
					set @I_SAVKEZD = @Kod
					set @I_SAVKEZD_NUM = @KodNum 					

					IF (@I_SAVKEZD IS NULL OR @I_SAVKEZD_NUM IS NULL)
						-- RAISERROR('Nem található felhasználható ragszámsáv!',16,1)
						RAISERROR('[64084]',16,1)

					SET @I_Id				= NEWID()
					SET @I_Kod				= @I_SAVKEZD
					SET @I_KodNum			= @I_SAVKEZD_NUM
					SET @I_Postakonyv_Id	= @Postakonyv_Id
					SET @I_RagszamSav_Id	= @RagszamSav_Id
					SET @I_Obj_type			= 'KRT_RagszamSavok'
					SET @I_Allapot			= @FoglalandoAllapota
					SET @I_Ver				= '1'
					SET @I_ErvKezd			= @I_DATE
					SET @I_ErvVege			= '4700-12-31 00:00:00.000'
					SET @I_Letrehozo_id		= @Letrehozo_Id
					SET @I_LetrehozasIdo	= @I_DATE	

					insert into @ragszamok values(
							 @I_Id
							,@I_Kod
							,@I_KodNum
							,@I_Postakonyv_Id
							,@I_RagszamSav_Id
							,@I_Obj_Id
							,@I_ObjTip_Id
							,@I_Obj_type
							,@I_KodType
							,@I_Allapot
							,@I_Ver
							,@I_Note
							,@I_Stat_id
							,@I_ErvKezd
							,@I_ErvVege
							,@I_Letrehozo_id
							,@I_LetrehozasIdo
							,@I_Modosito_id
							,@I_ModositasIdo
							,@I_Zarolo_id
							,@I_ZarolasIdo
							,@I_Tranz_id
							,@I_UIAccessLog_id
							,@I_UpdatedColumns
							,@I_ResultUid 
							)
					IF @ELSO = 1
						BEGIN
							SET @ELSO = 0
							SET @ResultUid =  CAST(@I_Id AS NVARCHAR(MAX))
						END
					ELSE
						SET @ResultUid = @ResultUid + ',' + CAST(@I_Id AS NVARCHAR(MAX))

					--SET CONDITION
					SET @VAN_MAR_RAGSZAM = 1
				END

		   SET @cnt = @cnt + 1;
		END;

				

		select TOP 1 I_Kod from @ragszamok
		order by I_Kod desc
 
END TRY
-----------
-----------
BEGIN CATCH
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------