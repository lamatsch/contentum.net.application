﻿/*

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_RagszamSav_Letrehozas')
            and   type = 'P')
   drop procedure sp_KRT_RagszamSav_Letrehozas
go
*/

CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_Letrehozas]    
        @RagszamSav_Id         UNIQUEIDENTIFIER,
        @SavKezd               NVARCHAR(100),
        @SavVege               NVARCHAR(100),
		@SavKezdNum            FLOAT,
        @SavVegeNum            FLOAT,
		@IgenyeltDarabszam		FLOAT,
        @LetrehozoSzervezet_Id UNIQUEIDENTIFIER,
	    @Letrehozo_Id		   UNIQUEIDENTIFIER	=	NULL,
		@Postakonyv_Id	       UNIQUEIDENTIFIER,
	    @SavType			   NVARCHAR(100)	=	NULL,
        @ResultUid             UNIQUEIDENTIFIER OUTPUT
         
AS

BEGIN TRY

SET NOCOUNT ON;

		declare @teszt           int
		select  @teszt = 0
 
		declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
		select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

		if @sajat_tranz = 1  
		   BEGIN TRANSACTION 
 
		select  @ResultUid = NULL

		declare @error           int
		declare @rowcount        int

		--------------------------------
		-- input paraméterek ellenőrzése
		--------------------------------

		-- input paraméterek kiiratása
		if @teszt = 1
		   print '@RagszamSav_Id = '+ isnull( convert(varchar(50),@RagszamSav_Id), 'NULL') + ', ' +
				 '@SavKezd = '+ isnull(@SavKezd, 'NULL') + ', ' +
				 '@SavVege = '+ isnull(@SavVege, 'NULL') + ', ' +
				 '@LetrehozoSzervezet_Id = '+ isnull( convert(varchar(50),@LetrehozoSzervezet_Id), 'NULL')

		DECLARE @Org uniqueidentifier
		SET @Org = dbo.fn_GetOrgByCsoportId(@LetrehozoSzervezet_Id)
		if (@Org is null)
		begin
			RAISERROR('[50202]',16,1)
		END

		-- NULLITÁS-vizsgálat
		if @RagszamSav_Id is NULL
		   set @RagszamSav_Id = NEWID()

		if @SavKezd is NULL
		   RAISERROR('[54116]',16,1)
		   --RAISERROR('@SavKezd paraméter nem lehet NULL!',16,1)

		if @Postakonyv_Id is NULL
		   RAISERROR('A hiányzik a postakönyv!',16,1)

		if @SavVege is NULL
		   RAISERROR('[54117]',16,1)
		   --RAISERROR('@SavVege paraméter nem lehet NULL!',16,1)

		if @SavKezdNum > @SavVegeNum
		   RAISERROR('[54121]',16,1)
		   --RAISERROR('@SavKezd > @SavVege nem lehet!',16,1)
 
		if @@error <> 0
		   RAISERROR('[54122]',16,1)
		   --RAISERROR('KRT_RagszamSavok select hiba!',16,1)

		if @LetrehozoSzervezet_Id is NULL
		   RAISERROR('[54123]',16,1)
		   --RAISERROR('A megadott sáv nem létezik!',16,1)

		SET @SavKezdNum = [dbo].[fn_GetNumeric] (@SavKezd)
		SET @SavVegeNum = [dbo].[fn_GetNumeric] (@SavVege)
		-------------------------------------------------------------------------------
		-- A megadott sáv már használatban van?
		-------------------------------------------------------------------------------
		IF EXISTS
			(
				SELECT 1 FROM dbo.KRT_RagszamSavok
				WHERE @SavKezdNum BETWEEN SavKezdNum AND SavVegeNum
					--AND Postakonyv_Id = @Postakonyv_Id
					--AND SavType = @SavType
			)
			 RAISERROR('A megadott sáv már használatban van !',16,1)
		ELSE IF EXISTS
			(
				SELECT 1 FROM dbo.KRT_RagszamSavok
				WHERE @SavVegeNum BETWEEN SavKezdNum AND SavVegeNum
					--AND Postakonyv_Id = @Postakonyv_Id
					--AND SavType = @SavType
			)
			 RAISERROR('A megadott sáv már használatban van !',16,1)
		-------------------------------------------------------------------------------
		-- a megadott sáv létrehozása
		-------------------------------------------------------------------------------


		-------------------------------------------------------------------------------
		--SÁV ÁLLAPOTA
		-------------------------------------------------------------------------------
		DECLARE @SAVALLAPOT CHAR(1)
		SET @SAVALLAPOT = 'A'		
		-------------------------------------------------------------------------------
			
		if @SavKezdNum < @SavVegeNum
		begin 

		   insert into dbo.KRT_RagszamSavok
				  (Id ,Org, Csoport_Id_Felelos, Postakonyv_id,SavKezd, SavVege, SavType,  SavAllapot, SavKezdNum, SavVegeNum, IgenyeltDarabszam, Letrehozo_id )
		   values (@RagszamSav_Id, @Org, @LetrehozoSzervezet_Id, @Postakonyv_id ,@SavKezd, @SavVege, @SavType, @SAVALLAPOT,@SavKezdNum, @SavVegeNum, @IgenyeltDarabszam, @Letrehozo_id)
 
		   if @@error <> 0
			  RAISERROR('[5413A]',16,1) 
		end

		if @SavKezdNum > @SavVegeNum
		begin 

		   insert into dbo.KRT_RagszamSavok
				  (Id ,Org, Csoport_Id_Felelos, Postakonyv_id,SavKezd, SavVege, SavType,  SavAllapot, SavKezdNum, SavVegeNum, IgenyeltDarabszam, Letrehozo_id )
		   values (@RagszamSav_Id, @Org, @LetrehozoSzervezet_Id, @Postakonyv_id , @SavVege ,@SavKezd, @SavType, @SAVALLAPOT,@SavKezdNum, @SavVegeNum, @IgenyeltDarabszam , @Letrehozo_id)
		 
		   if @@error <> 0
			  RAISERROR('[5413A]',16,1) 
		end

		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		select @ResultUid = @RagszamSav_Id
END TRY
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------