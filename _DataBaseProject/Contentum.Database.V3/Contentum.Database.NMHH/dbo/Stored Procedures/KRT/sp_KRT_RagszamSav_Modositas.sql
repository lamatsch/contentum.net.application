﻿/*

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_RagszamSav_Modositas')
            and   type = 'P')
   drop procedure sp_KRT_RagszamSav_Modositas
go
*/

CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_Modositas]    
        @RagszamSav_Id         UNIQUEIDENTIFIER,
        @SavKezd               NVARCHAR(100),
        @SavVege               NVARCHAR(100),
		@SavKezdNum            FLOAT,
        @SavVegeNum            FLOAT,
		@IgenyeltDarabszam		FLOAT,
		@SavAllapot			   CHAR(1)				=	'H',
		@SavType			   NVARCHAR(100)		=	NULL,
        @LetrehozoSzervezet_Id UNIQUEIDENTIFIER,
		@Modosito_Id		   UNIQUEIDENTIFIER,
		@Postakonyv_Id	       UNIQUEIDENTIFIER,
        @ResultUid             UNIQUEIDENTIFIER OUTPUT
         
AS

BEGIN TRY

SET NOCOUNT ON;

		declare @teszt           int
		select  @teszt = 0
 
		declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
		select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

		if @sajat_tranz = 1  
		   BEGIN TRANSACTION 
 
		select  @ResultUid = NULL

		declare @error           int
		declare @rowcount        int

		--------------------------------
		-- INPUT PARAMÉTEREK ELLENŐRZÉSE
		--------------------------------

		-- NULLITÁS-vizsgálat
		if @RagszamSav_Id is NULL
		   RAISERROR('A hiányzik a ragszámsáv azonosítója !',16,1)
	
		IF @Postakonyv_Id IS NULL
			SELECT @Postakonyv_Id = Postakonyv_Id FROM KRT_RagszamSavok WHERE ID = @RagszamSav_Id
		--------------------------------
		-- MÓDOSÍTÁSOK
		--------------------------------
		----A TÖBBI SÁVÁLLAPOTÁNAK MÓDOSÍTÁSA
		--IF @SavAllapot = 'A'
		--	BEGIN
		--			IF EXISTS(SELECT 1 
		--					  FROM KRT_RagszamSavok 
		--					  WHERE Postakonyv_Id = @Postakonyv_Id 
		--							AND SavAllapot = 'A'
		--							AND ID <> @RagszamSav_Id)
		--				BEGIN
		--					  UPDATE KRT_RagszamSavok 
		--					  SET  SavAllapot = 'H'
		--						  ,Modosito_id = @Modosito_Id
		--						  ,ModositasIdo = GETDATE()
		--					  WHERE Postakonyv_Id = @Postakonyv_Id 
		--							AND @SavAllapot = 'A'
		--							AND ID <> @RagszamSav_Id
									
		--				END
		--	END

		--MÓDOSÍTÁS
		UPDATE KRT_RagszamSavok 
		SET  SavAllapot = @SavAllapot
			 ,Postakonyv_Id=@Postakonyv_Id
			 ,SavType=@SavType
			,Modosito_id = @Modosito_Id
			,ModositasIdo = GETDATE()
		WHERE ID = @RagszamSav_Id

		--------------------------------
		-- TRANZAKCIÓ
		--------------------------------
		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		select @ResultUid = @RagszamSav_Id
END TRY
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------