﻿
CREATE procedure [dbo].[sp_KRT_RagszamSav_PrintListazas]    
                @RagszamSav_Id         uniqueidentifier,
                @PrinteloSzervezet_Id uniqueidentifier-- = NULL
         
AS
	SELECT * 
	FROM KRT_RAGSZAMSAVOK S
	WHERE S.Id = @RagszamSav_Id
			AND 
		  S.Csoport_Id_Felelos = @PrinteloSzervezet_Id

BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int
declare @rowcount        int
 
declare @Csoport_Id_Felelos uniqueidentifier
declare @SavKezd         nvarchar(100)
declare @SavVege         nvarchar(100)
declare @SavType         char(1)
declare @SavAllapot      char(1)

declare @SavKezd_num     numeric(12)
declare @SavVege_num     numeric(12)
declare @AktKod_num      numeric(12)
declare @AktKod          nvarchar(100)

--------------------------------
-- input paraméterek ellenőrzése
--------------------------------

-- input paraméterek kiiratása
if @teszt = 1
   print '@RagszamSav_Id = '+ isnull( convert(varchar(50),@RagszamSav_Id), 'NULL') + ', ' +
         '@PrinteloSzervezet_Id = '+ isnull( convert(varchar(50),@PrinteloSzervezet_Id), 'NULL')

-- NULLITÁS-vizsgálat
if @RagszamSav_Id is NULL
   RAISERROR('[54132]',16,1)
   --RAISERROR('@RagszamSav_Id paraméter nem lehet NULL!',16,1)

-- sáv létezés ellenőrzés
select @Csoport_Id_Felelos = NULL, @SavKezd = NULL, @SavVege = NULL, @SavType = NULL, @SavAllapot = NULL 
select @Csoport_Id_Felelos = Csoport_Id_Felelos,
       @SavKezd            = SavKezd,
       @SavVege            = SavVege,
       @SavType            = SavType,
       @SavAllapot         = SavAllapot
  from dbo.KRT_RagszamSavok
 where Id = @RagszamSav_Id
 
if @@error <> 0
   RAISERROR('[54133]',16,1)
   --RAISERROR('KRT_RagszamSavok select hiba!',16,1)

-- a megadott sáv paramétereinek kiiratása
if @teszt = 1
   print '@Csoport_Id_Felelos = '+ isnull( convert(varchar(50),@Csoport_Id_Felelos), 'NULL') + ', ' +
         '@SavKezd = '+ isnull(@SavKezd, 'NULL') + ', ' +
         '@SavVege = '+ isnull(@SavVege, 'NULL') + ', ' +
         '@SavType = '+ isnull(@SavType, 'NULL') + ', ' +
         '@SavAllapot = '+ isnull(@SavAllapot, 'NULL')

if @Csoport_Id_Felelos is NULL
   RAISERROR('[54134]',16,1)
   --RAISERROR('A megadott vonalkódsáv nem létezik!',16,1)

--if @PrinteloSzervezet_Id is NULL
--   select @PrinteloSzervezet_Id = @Csoport_Id_Felelos

-- CR#1971 (EB 2009.03.11): Kliens oldalon ellenőrizzük, hogy a végrehajtó felhasználó tagje-e a felelős csoportnak (vagy azonos vele)
---- kérdés: ugyanazt a szervezetet követeljük-e meg,
----         vagy lehet a printelő más is és akkor ő lesz a tulajdonos,
----         vagy ne is foglalkozzunk szervezettel ... ????
--if @PrinteloSzervezet_Id <> @Csoport_Id_Felelos
--   RAISERROR('[54135]',16,1)
--   --RAISERROR('Az igénylő és printelő szervezetnek azonosnak kell lennie!',16,1)

if @SavAllapot <> 'A'
   RAISERROR('[54137]',16,1)
   --RAISERROR('A megadott vonalkódsáv már printelt vagy törölt!',16,1)

--ellenörző vektor lekérése
DECLARE @vektor NVARCHAR(12)
SET @vektor = dbo.fn_GetRendszerparameterBySzervezet(@PrinteloSzervezet_Id,'VONALKOD_ELLENORZO_VEKTOR')  

IF @vektor = NULL
BEGIN
	--rendszerparaméter nem található 
	RAISERROR('[54159]',16,1)
END

IF @vektor = 'Error_50202'
BEGIN
	--org nem határozható meg
	RAISERROR('[50202]',16,1)
END 

if @teszt = 1
	PRINT '@vektor = ' + @vektor
	
------------------------------------------------------------------------
-- a megadott vonalkódsáv vonalkódjainak előállítása (csak munkatáblába)
------------------------------------------------------------------------
select @SavKezd_num = convert(numeric(12),substring(@SavKezd,1,12)),
       @SavVege_num = convert(numeric(12),substring(@SavVege,1,12))

-- munkatábla kreálás (megy ez tranzakcióban????)
create table #Ragszam_Lista
(
-- Id                   uniqueidentifier     not null,
   Kod                  Nvarchar(100)        not null --,
-- KodType              char(1)              null,
-- Allapot              nvarchar(64)         null
)

-- vonalkódok felírása ciklusban
select @AktKod_num  = @SavKezd_num 
while  @AktKod_num <= @SavVege_num
begin

   select @AktKod = dbo.fn_ragszam_checksum_addn( @AktKod_num, @vektor )

 --insert into #BarKod_Lista (Id,Kod,KodType,Allapot) values (newid(),@AktKod,@SavType,'S')
   insert into #Ragszam_Lista (Kod) values (@AktKod)

   if @@error <> 0
      RAISERROR('[54138]',16,1)
      --RAISERROR('#BarKod_Lista insert hiba!',16,1)
  
   select @AktKod_num = @AktKod_num + 1

end

--------------------------------------------------------
-- eredmény (vonalkódsáv lista) visszaadás munkatáblából
--------------------------------------------------------

select * from #Ragszam_Lista order by Kod

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------