﻿/*

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_RagszamSavokGet')
            and   type = 'P')
   drop procedure sp_KRT_RagszamSavokGet
go

*/
create procedure sp_KRT_RagszamSavokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_RagszamSavok.Id,
	   KRT_RagszamSavok.Org,
	   KRT_RagszamSavok.Csoport_Id_Felelos,
	   KRT_RagszamSavok.Postakonyv_Id,
	   KRT_RagszamSavok.SavKezd,
	   KRT_RagszamSavok.SavKezdNum,
	   KRT_RagszamSavok.SavVege,
	   KRT_RagszamSavok.SavVegeNum,
	   KRT_RagszamSavok.IgenyeltDarabszam,
	   KRT_RagszamSavok.SavType,
	   KRT_RagszamSavok.SavAllapot,
	   KRT_RagszamSavok.Ver,
	   KRT_RagszamSavok.Note,
	   KRT_RagszamSavok.Stat_id,
	   KRT_RagszamSavok.ErvKezd,
	   KRT_RagszamSavok.ErvVege,
	   KRT_RagszamSavok.Letrehozo_id,
	   KRT_RagszamSavok.LetrehozasIdo,
	   KRT_RagszamSavok.Modosito_id,
	   KRT_RagszamSavok.ModositasIdo,
	   KRT_RagszamSavok.Zarolo_id,
	   KRT_RagszamSavok.ZarolasIdo,
	   KRT_RagszamSavok.Tranz_id,
	   KRT_RagszamSavok.UIAccessLog_id
	   from 
		 KRT_RagszamSavok as KRT_RagszamSavok 
	   where
		 KRT_RagszamSavok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end