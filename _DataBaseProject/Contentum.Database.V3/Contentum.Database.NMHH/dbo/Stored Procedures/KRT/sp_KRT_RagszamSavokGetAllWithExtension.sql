﻿/*
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_RagszamSavokGetAllWithExtension')
            and   type = 'P')
   drop procedure sp_KRT_RagszamSavokGetAllWithExtension
go
*/

create procedure [dbo].[sp_KRT_RagszamSavokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by KRT_RagszamSavok.SavKezd',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
       KRT_RagszamSavok.Id,
       KRT_RagszamSavok.Csoport_Id_Felelos,
       KRT_Csoportok.Nev as Csoport_Felelos,
	   KRT_RagszamSavok.PostaKonyv_Id,
	   EREC_IraIktatoKonyvek.Nev as Postakonyv_Nev,
       KRT_RagszamSavok.SavKezd,
       KRT_RagszamSavok.SavVege,
	   KRT_RagszamSavok.IgenyeltDarabszam as IgenyeltDarab,
	   dbo.Fn_RagszamSavFelhasznaltElemSzam(KRT_RagszamSavok.Id) as FelhasznaltDarab,
       KRT_RagszamSavok.SavType,
	   dbo.Fn_Get_KRT_Kodtar_Nev(''KIMENO_KULDEMENY_FAJTA'',KRT_RagszamSavok.SavType) as SavTypeName,
       KRT_RagszamSavok.SavAllapot,
       	  CASE KRT_RagszamSavok.SavAllapot 
       		  when ''A'' then ''Allokált'' 
       		  when ''F'' then ''Felhasznált'' 
			  when ''H'' then ''Használható'' 
       		  else ''Törölt'' 
       	  END SavAllapotName,
       KRT_RagszamSavok.Ver,
       KRT_RagszamSavok.Note,
       KRT_RagszamSavok.Stat_id,
       KRT_RagszamSavok.ErvKezd,
       KRT_RagszamSavok.ErvVege,
       KRT_RagszamSavok.Letrehozo_id,
       KRT_RagszamSavok.LetrehozasIdo,
       KRT_RagszamSavok.Modosito_id,
       KRT_RagszamSavok.ModositasIdo,
       KRT_RagszamSavok.Zarolo_id,
       KRT_RagszamSavok.ZarolasIdo,
       KRT_RagszamSavok.Tranz_id,
       KRT_RagszamSavok.UIAccessLog_id
  FROM dbo.KRT_RagszamSavok as KRT_RagszamSavok,
       dbo.KRT_Csoportok as KRT_Csoportok,
	   dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek
  WHERE 
		KRT_RagszamSavok.Csoport_Id_Felelos = KRT_Csoportok.Id
	AND
		KRT_RagszamSavok.PostaKonyv_Id = EREC_IraIktatoKonyvek.Id
	AND 
		KRT_RagszamSavok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end