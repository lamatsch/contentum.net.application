﻿
create procedure sp_KRT_SoapMessageLogGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_SoapMessageLog.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_SoapMessageLog.Id,
	   KRT_SoapMessageLog.Ver,
	   KRT_SoapMessageLog.Note,
	   KRT_SoapMessageLog.Stat_id,
	   KRT_SoapMessageLog.ErvKezd,
	   KRT_SoapMessageLog.ErvVege,
	   KRT_SoapMessageLog.Letrehozo_id,
	   KRT_SoapMessageLog.LetrehozasIdo,
	   KRT_SoapMessageLog.Modosito_id,
	   KRT_SoapMessageLog.ModositasIdo,
	   KRT_SoapMessageLog.Zarolo_id,
	   KRT_SoapMessageLog.ZarolasIdo,
	   KRT_SoapMessageLog.Tranz_id,
	   KRT_SoapMessageLog.UIAccessLog_id,
	   KRT_SoapMessageLog.ForrasRendszer,
	   KRT_SoapMessageLog.Url,
	   KRT_SoapMessageLog.CelRendszer,
	   KRT_SoapMessageLog.MetodusNeve,
	   KRT_SoapMessageLog.KeresFogadas,
	   KRT_SoapMessageLog.KeresKuldes,
	   KRT_SoapMessageLog.RequestData,
	   KRT_SoapMessageLog.ResponseData,
	   KRT_SoapMessageLog.FuttatoFelhasznalo,
	   KRT_SoapMessageLog.Local,
	   KRT_SoapMessageLog.Hiba,
	   KRT_SoapMessageLog.Sikeres  
   from 
     KRT_SoapMessageLog as KRT_SoapMessageLog      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end