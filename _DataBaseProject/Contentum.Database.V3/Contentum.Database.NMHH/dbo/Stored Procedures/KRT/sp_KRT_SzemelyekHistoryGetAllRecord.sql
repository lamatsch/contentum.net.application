﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SzemelyekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_SzemelyekHistoryGetAllRecord
go
*/
create procedure sp_KRT_SzemelyekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_SzemelyekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeve' as ColumnName,               cast(Old.AnyjaNeve as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AnyjaNeve as nvarchar(max)),'') != ISNULL(CAST(New.AnyjaNeve as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveCsaladiNev' as ColumnName,               cast(Old.AnyjaNeveCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AnyjaNeveCsaladiNev as nvarchar(max)),'') != ISNULL(CAST(New.AnyjaNeveCsaladiNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveElsoUtonev' as ColumnName,               cast(Old.AnyjaNeveElsoUtonev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveElsoUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AnyjaNeveElsoUtonev as nvarchar(max)),'') != ISNULL(CAST(New.AnyjaNeveElsoUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveTovabbiUtonev' as ColumnName,               cast(Old.AnyjaNeveTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AnyjaNeveTovabbiUtonev as nvarchar(max)),'') != ISNULL(CAST(New.AnyjaNeveTovabbiUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ApjaNeve' as ColumnName,               cast(Old.ApjaNeve as nvarchar(99)) as OldValue,
               cast(New.ApjaNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ApjaNeve as nvarchar(max)),'') != ISNULL(CAST(New.ApjaNeve as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiNev' as ColumnName,               cast(Old.SzuletesiNev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiNev as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiCsaladiNev' as ColumnName,               cast(Old.SzuletesiCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiCsaladiNev as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiCsaladiNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiElsoUtonev' as ColumnName,               cast(Old.SzuletesiElsoUtonev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiElsoUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiElsoUtonev as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiElsoUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiTovabbiUtonev' as ColumnName,               cast(Old.SzuletesiTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiTovabbiUtonev as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiTovabbiUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiOrszag' as ColumnName,               cast(Old.SzuletesiOrszag as nvarchar(99)) as OldValue,
               cast(New.SzuletesiOrszag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiOrszag as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiOrszag as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiOrszagId' as ColumnName,               cast(Old.SzuletesiOrszagId as nvarchar(99)) as OldValue,
               cast(New.SzuletesiOrszagId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiOrszagId as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiOrszagId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjTitulis' as ColumnName,               cast(Old.UjTitulis as nvarchar(99)) as OldValue,
               cast(New.UjTitulis as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjTitulis as nvarchar(max)),'') != ISNULL(CAST(New.UjTitulis as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjCsaladiNev' as ColumnName,               cast(Old.UjCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.UjCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjCsaladiNev as nvarchar(max)),'') != ISNULL(CAST(New.UjCsaladiNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjUtonev' as ColumnName,               cast(Old.UjUtonev as nvarchar(99)) as OldValue,
               cast(New.UjUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjUtonev as nvarchar(max)),'') != ISNULL(CAST(New.UjUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjTovabbiUtonev' as ColumnName,               cast(Old.UjTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.UjTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjTovabbiUtonev as nvarchar(max)),'') != ISNULL(CAST(New.UjTovabbiUtonev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiHely' as ColumnName,               cast(Old.SzuletesiHely as nvarchar(99)) as OldValue,
               cast(New.SzuletesiHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiHely as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiHely as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiHely_id' as ColumnName,               cast(Old.SzuletesiHely_id as nvarchar(99)) as OldValue,
               cast(New.SzuletesiHely_id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiHely_id as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiHely_id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiIdo' as ColumnName,               cast(Old.SzuletesiIdo as nvarchar(99)) as OldValue,
               cast(New.SzuletesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzuletesiIdo as nvarchar(max)),'') != ISNULL(CAST(New.SzuletesiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allampolgarsag' as ColumnName,               cast(Old.Allampolgarsag as nvarchar(99)) as OldValue,
               cast(New.Allampolgarsag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allampolgarsag as nvarchar(max)),'') != ISNULL(CAST(New.Allampolgarsag as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TAJSzam' as ColumnName,               cast(Old.TAJSzam as nvarchar(99)) as OldValue,
               cast(New.TAJSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TAJSzam as nvarchar(max)),'') != ISNULL(CAST(New.TAJSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SZIGSzam' as ColumnName,               cast(Old.SZIGSzam as nvarchar(99)) as OldValue,
               cast(New.SZIGSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SZIGSzam as nvarchar(max)),'') != ISNULL(CAST(New.SZIGSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Neme' as ColumnName,               cast(Old.Neme as nvarchar(99)) as OldValue,
               cast(New.Neme as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Neme as nvarchar(max)),'') != ISNULL(CAST(New.Neme as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzemelyiAzonosito' as ColumnName,               cast(Old.SzemelyiAzonosito as nvarchar(99)) as OldValue,
               cast(New.SzemelyiAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzemelyiAzonosito as nvarchar(max)),'') != ISNULL(CAST(New.SzemelyiAzonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Adoazonosito' as ColumnName,               cast(Old.Adoazonosito as nvarchar(99)) as OldValue,
               cast(New.Adoazonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Adoazonosito as nvarchar(max)),'') != ISNULL(CAST(New.Adoazonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Adoszam' as ColumnName,               cast(Old.Adoszam as nvarchar(99)) as OldValue,
               cast(New.Adoszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Adoszam as nvarchar(max)),'') != ISNULL(CAST(New.Adoszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulfoldiAdoszamJelolo' as ColumnName,               cast(Old.KulfoldiAdoszamJelolo as nvarchar(99)) as OldValue,
               cast(New.KulfoldiAdoszamJelolo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KulfoldiAdoszamJelolo as nvarchar(max)),'') != ISNULL(CAST(New.KulfoldiAdoszamJelolo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Beosztas' as ColumnName,               cast(Old.Beosztas as nvarchar(99)) as OldValue,
               cast(New.Beosztas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Beosztas as nvarchar(max)),'') != ISNULL(CAST(New.Beosztas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesiSzint' as ColumnName,               cast(Old.MinositesiSzint as nvarchar(99)) as OldValue,
               cast(New.MinositesiSzint as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_SzemelyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesiSzint as nvarchar(max)),'') != ISNULL(CAST(New.MinositesiSzint as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go