﻿
create procedure sp_KRT_SzemelyekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Partner_Id     uniqueidentifier  = null,         
             @AnyjaNeve     Nvarchar(100)  = null,         
             @AnyjaNeveCsaladiNev     Nvarchar(100)  = null,         
             @AnyjaNeveElsoUtonev     Nvarchar(100)  = null,         
             @AnyjaNeveTovabbiUtonev     Nvarchar(100)  = null,         
             @ApjaNeve     Nvarchar(100)  = null,         
             @SzuletesiNev     Nvarchar(100)  = null,         
             @SzuletesiCsaladiNev     Nvarchar(100)  = null,         
             @SzuletesiElsoUtonev     Nvarchar(100)  = null,         
             @SzuletesiTovabbiUtonev     Nvarchar(100)  = null,         
             @SzuletesiOrszag     Nvarchar(100)  = null,         
             @SzuletesiOrszagId     uniqueidentifier  = null,         
             @UjTitulis     Nvarchar(10)  = null,         
             @UjCsaladiNev     Nvarchar(100)  = null,         
             @UjUtonev     Nvarchar(100)  = null,         
             @UjTovabbiUtonev     Nvarchar(100)  = null,         
             @SzuletesiHely     Nvarchar(400)  = null,         
             @SzuletesiHely_id     uniqueidentifier  = null,         
             @SzuletesiIdo     datetime  = null,         
             @Allampolgarsag     Nvarchar(100)  = null,         
             @TAJSzam     Nvarchar(20)  = null,         
             @SZIGSzam     Nvarchar(20)  = null,         
             @Neme     char(1)  = null,         
             @SzemelyiAzonosito     Nvarchar(20)  = null,         
             @Adoazonosito     Nvarchar(20)  = null,         
             @Adoszam     Nvarchar(20)  = null,         
             @KulfoldiAdoszamJelolo     char(1)  = null,         
             @Beosztas     Nvarchar(100)  = null,         
             @MinositesiSzint     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Partner_Id     uniqueidentifier         
     DECLARE @Act_AnyjaNeve     Nvarchar(100)         
     DECLARE @Act_AnyjaNeveCsaladiNev     Nvarchar(100)         
     DECLARE @Act_AnyjaNeveElsoUtonev     Nvarchar(100)         
     DECLARE @Act_AnyjaNeveTovabbiUtonev     Nvarchar(100)         
     DECLARE @Act_ApjaNeve     Nvarchar(100)         
     DECLARE @Act_SzuletesiNev     Nvarchar(100)         
     DECLARE @Act_SzuletesiCsaladiNev     Nvarchar(100)         
     DECLARE @Act_SzuletesiElsoUtonev     Nvarchar(100)         
     DECLARE @Act_SzuletesiTovabbiUtonev     Nvarchar(100)         
     DECLARE @Act_SzuletesiOrszag     Nvarchar(100)         
     DECLARE @Act_SzuletesiOrszagId     uniqueidentifier         
     DECLARE @Act_UjTitulis     Nvarchar(10)         
     DECLARE @Act_UjCsaladiNev     Nvarchar(100)         
     DECLARE @Act_UjUtonev     Nvarchar(100)         
     DECLARE @Act_UjTovabbiUtonev     Nvarchar(100)         
     DECLARE @Act_SzuletesiHely     Nvarchar(400)         
     DECLARE @Act_SzuletesiHely_id     uniqueidentifier         
     DECLARE @Act_SzuletesiIdo     datetime         
     DECLARE @Act_Allampolgarsag     Nvarchar(100)         
     DECLARE @Act_TAJSzam     Nvarchar(20)         
     DECLARE @Act_SZIGSzam     Nvarchar(20)         
     DECLARE @Act_Neme     char(1)         
     DECLARE @Act_SzemelyiAzonosito     Nvarchar(20)         
     DECLARE @Act_Adoazonosito     Nvarchar(20)         
     DECLARE @Act_Adoszam     Nvarchar(20)         
     DECLARE @Act_KulfoldiAdoszamJelolo     char(1)         
     DECLARE @Act_Beosztas     Nvarchar(100)         
     DECLARE @Act_MinositesiSzint     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Partner_Id = Partner_Id,
     @Act_AnyjaNeve = AnyjaNeve,
     @Act_AnyjaNeveCsaladiNev = AnyjaNeveCsaladiNev,
     @Act_AnyjaNeveElsoUtonev = AnyjaNeveElsoUtonev,
     @Act_AnyjaNeveTovabbiUtonev = AnyjaNeveTovabbiUtonev,
     @Act_ApjaNeve = ApjaNeve,
     @Act_SzuletesiNev = SzuletesiNev,
     @Act_SzuletesiCsaladiNev = SzuletesiCsaladiNev,
     @Act_SzuletesiElsoUtonev = SzuletesiElsoUtonev,
     @Act_SzuletesiTovabbiUtonev = SzuletesiTovabbiUtonev,
     @Act_SzuletesiOrszag = SzuletesiOrszag,
     @Act_SzuletesiOrszagId = SzuletesiOrszagId,
     @Act_UjTitulis = UjTitulis,
     @Act_UjCsaladiNev = UjCsaladiNev,
     @Act_UjUtonev = UjUtonev,
     @Act_UjTovabbiUtonev = UjTovabbiUtonev,
     @Act_SzuletesiHely = SzuletesiHely,
     @Act_SzuletesiHely_id = SzuletesiHely_id,
     @Act_SzuletesiIdo = SzuletesiIdo,
     @Act_Allampolgarsag = Allampolgarsag,
     @Act_TAJSzam = TAJSzam,
     @Act_SZIGSzam = SZIGSzam,
     @Act_Neme = Neme,
     @Act_SzemelyiAzonosito = SzemelyiAzonosito,
     @Act_Adoazonosito = Adoazonosito,
     @Act_Adoszam = Adoszam,
     @Act_KulfoldiAdoszamJelolo = KulfoldiAdoszamJelolo,
     @Act_Beosztas = Beosztas,
     @Act_MinositesiSzint = MinositesiSzint,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Szemelyek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Partner_Id')=1
         SET @Act_Partner_Id = @Partner_Id
   IF @UpdatedColumns.exist('/root/AnyjaNeve')=1
         SET @Act_AnyjaNeve = @AnyjaNeve
   IF @UpdatedColumns.exist('/root/AnyjaNeveCsaladiNev')=1
         SET @Act_AnyjaNeveCsaladiNev = @AnyjaNeveCsaladiNev
   IF @UpdatedColumns.exist('/root/AnyjaNeveElsoUtonev')=1
         SET @Act_AnyjaNeveElsoUtonev = @AnyjaNeveElsoUtonev
   IF @UpdatedColumns.exist('/root/AnyjaNeveTovabbiUtonev')=1
         SET @Act_AnyjaNeveTovabbiUtonev = @AnyjaNeveTovabbiUtonev
   IF @UpdatedColumns.exist('/root/ApjaNeve')=1
         SET @Act_ApjaNeve = @ApjaNeve
   IF @UpdatedColumns.exist('/root/SzuletesiNev')=1
         SET @Act_SzuletesiNev = @SzuletesiNev
   IF @UpdatedColumns.exist('/root/SzuletesiCsaladiNev')=1
         SET @Act_SzuletesiCsaladiNev = @SzuletesiCsaladiNev
   IF @UpdatedColumns.exist('/root/SzuletesiElsoUtonev')=1
         SET @Act_SzuletesiElsoUtonev = @SzuletesiElsoUtonev
   IF @UpdatedColumns.exist('/root/SzuletesiTovabbiUtonev')=1
         SET @Act_SzuletesiTovabbiUtonev = @SzuletesiTovabbiUtonev
   IF @UpdatedColumns.exist('/root/SzuletesiOrszag')=1
         SET @Act_SzuletesiOrszag = @SzuletesiOrszag
   IF @UpdatedColumns.exist('/root/SzuletesiOrszagId')=1
         SET @Act_SzuletesiOrszagId = @SzuletesiOrszagId
   IF @UpdatedColumns.exist('/root/UjTitulis')=1
         SET @Act_UjTitulis = @UjTitulis
   IF @UpdatedColumns.exist('/root/UjCsaladiNev')=1
         SET @Act_UjCsaladiNev = @UjCsaladiNev
   IF @UpdatedColumns.exist('/root/UjUtonev')=1
         SET @Act_UjUtonev = @UjUtonev
   IF @UpdatedColumns.exist('/root/UjTovabbiUtonev')=1
         SET @Act_UjTovabbiUtonev = @UjTovabbiUtonev
   IF @UpdatedColumns.exist('/root/SzuletesiHely')=1
         SET @Act_SzuletesiHely = @SzuletesiHely
   IF @UpdatedColumns.exist('/root/SzuletesiHely_id')=1
         SET @Act_SzuletesiHely_id = @SzuletesiHely_id
   IF @UpdatedColumns.exist('/root/SzuletesiIdo')=1
         SET @Act_SzuletesiIdo = @SzuletesiIdo
   IF @UpdatedColumns.exist('/root/Allampolgarsag')=1
         SET @Act_Allampolgarsag = @Allampolgarsag
   IF @UpdatedColumns.exist('/root/TAJSzam')=1
         SET @Act_TAJSzam = @TAJSzam
   IF @UpdatedColumns.exist('/root/SZIGSzam')=1
         SET @Act_SZIGSzam = @SZIGSzam
   IF @UpdatedColumns.exist('/root/Neme')=1
         SET @Act_Neme = @Neme
   IF @UpdatedColumns.exist('/root/SzemelyiAzonosito')=1
         SET @Act_SzemelyiAzonosito = @SzemelyiAzonosito
   IF @UpdatedColumns.exist('/root/Adoazonosito')=1
         SET @Act_Adoazonosito = @Adoazonosito
   IF @UpdatedColumns.exist('/root/Adoszam')=1
         SET @Act_Adoszam = @Adoszam
   IF @UpdatedColumns.exist('/root/KulfoldiAdoszamJelolo')=1
         SET @Act_KulfoldiAdoszamJelolo = @KulfoldiAdoszamJelolo
   IF @UpdatedColumns.exist('/root/Beosztas')=1
         SET @Act_Beosztas = @Beosztas
   IF @UpdatedColumns.exist('/root/MinositesiSzint')=1
         SET @Act_MinositesiSzint = @MinositesiSzint
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Szemelyek
SET
     Partner_Id = @Act_Partner_Id,
     AnyjaNeve = @Act_AnyjaNeve,
     AnyjaNeveCsaladiNev = @Act_AnyjaNeveCsaladiNev,
     AnyjaNeveElsoUtonev = @Act_AnyjaNeveElsoUtonev,
     AnyjaNeveTovabbiUtonev = @Act_AnyjaNeveTovabbiUtonev,
     ApjaNeve = @Act_ApjaNeve,
     SzuletesiNev = @Act_SzuletesiNev,
     SzuletesiCsaladiNev = @Act_SzuletesiCsaladiNev,
     SzuletesiElsoUtonev = @Act_SzuletesiElsoUtonev,
     SzuletesiTovabbiUtonev = @Act_SzuletesiTovabbiUtonev,
     SzuletesiOrszag = @Act_SzuletesiOrszag,
     SzuletesiOrszagId = @Act_SzuletesiOrszagId,
     UjTitulis = @Act_UjTitulis,
     UjCsaladiNev = @Act_UjCsaladiNev,
     UjUtonev = @Act_UjUtonev,
     UjTovabbiUtonev = @Act_UjTovabbiUtonev,
     SzuletesiHely = @Act_SzuletesiHely,
     SzuletesiHely_id = @Act_SzuletesiHely_id,
     SzuletesiIdo = @Act_SzuletesiIdo,
     Allampolgarsag = @Act_Allampolgarsag,
     TAJSzam = @Act_TAJSzam,
     SZIGSzam = @Act_SZIGSzam,
     Neme = @Act_Neme,
     SzemelyiAzonosito = @Act_SzemelyiAzonosito,
     Adoazonosito = @Act_Adoazonosito,
     Adoszam = @Act_Adoszam,
     KulfoldiAdoszamJelolo = @Act_KulfoldiAdoszamJelolo,
     Beosztas = @Act_Beosztas,
     MinositesiSzint = @Act_MinositesiSzint,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Szemelyek',@Id
					,'KRT_SzemelyekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH