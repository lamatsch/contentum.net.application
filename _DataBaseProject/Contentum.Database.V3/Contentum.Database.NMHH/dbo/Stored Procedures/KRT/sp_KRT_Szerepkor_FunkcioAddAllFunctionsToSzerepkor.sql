﻿create procedure [dbo].[sp_KRT_Szerepkor_FunkcioAddAllFunctionsToSzerepkor]    
	            @Szerepkor_Id     uniqueidentifier,
                @ExecutorUserId uniqueidentifier,
                @Tranz_id uniqueidentifier
 AS

BEGIN TRY

set nocount on;

	declare @Id uniqueidentifier
	declare @ErvKezd datetime
	declare @ErvVege datetime

	select @Id = Id,
		@ErvKezd = ErvKezd,
		@ErvVege = ErvVege
	from KRT_Szerepkorok
	where Id = @Szerepkor_Id

	if @Id is null
		RAISERROR('[50101]',16,1) -- A rekord nem talA?lhatAl

	-- Elvileg ilyen nem lehet
	if @ErvKezd is null
		set @ErvKezd = getdate()
	if @ErvVege is null
		set @ErvVege = '4700-12-31'


	select Id
		into #tmp_Funkcio_Ids
	from KRT_Funkciok where getdate() between ErvKezd and ErvVege
		and not exists(select 1 from KRT_Szerepkor_Funkcio where Szerepkor_Id=@Szerepkor_Id and KRT_Szerepkor_Funkcio.Funkcio_Id=KRT_Funkciok.Id)


	insert into KRT_Szerepkor_Funkcio (Szerepkor_Id, Funkcio_Id, Ver, ErvKezd, ErvVege, Letrehozo_id, LetrehozasIdo, Tranz_id)
		select @Szerepkor_Id as Szerepkor_Id, Id as Funkcio_Id, 1 as Ver
			, ErvKezd, ErvVege, @ExecutorUserId as Letrehozo_id, getdate() as LetrehozasIdo
			, @Tranz_id as Tranz_id
		from
		(
			select KRT_Funkciok.Id as Id,
				ErvKezd = case when ErvKezd > @ErvKezd then ErvKezd else @ErvKezd end,
				ErvVege = case when ErvVege < @ErvVege then ErvVege else @ErvVege end
			from KRT_Funkciok join #tmp_Funkcio_Ids
				on KRT_Funkciok.Id=#tmp_Funkcio_Ids.Id
		) tmp


	---------------------------------------------------------
	-- History Log
	---------------------------------------------------------
	declare @row_ids varchar(MAX)
	declare @VegrehajtasIdo datetime
	set @VegrehajtasIdo = getdate()

	select KRT_Szerepkor_Funkcio.Id as Id
		into #tmp_Ids
		from KRT_Szerepkor_Funkcio
			join #tmp_Funkcio_Ids on KRT_Szerepkor_Funkcio.Funkcio_Id= #tmp_Funkcio_Ids.Id
		where Szerepkor_Id=@Szerepkor_Id
		and (@Tranz_id is null or Tranz_id = @Tranz_id)


		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #tmp_Ids FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		drop table #tmp_Funkcio_Ids
		drop table #tmp_Ids

		exec sp_LogRecordsToHistory_Tomeges
		 @TableName = 'KRT_Szerepkor_Funkcio'
		,@Row_Ids = @row_ids
		,@Muvelet = 0
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @VegrehajtasIdo

   
END TRY
BEGIN CATCH
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH