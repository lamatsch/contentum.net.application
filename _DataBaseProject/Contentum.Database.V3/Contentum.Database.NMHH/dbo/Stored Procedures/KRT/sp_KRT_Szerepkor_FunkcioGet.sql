﻿create procedure [dbo].[sp_KRT_Szerepkor_FunkcioGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Szerepkor_Funkcio.Id,
	   KRT_Szerepkor_Funkcio.Funkcio_Id,
	   KRT_Szerepkor_Funkcio.Szerepkor_Id,
	   KRT_Szerepkor_Funkcio.Ver,
	   KRT_Szerepkor_Funkcio.Note,
	   KRT_Szerepkor_Funkcio.Stat_id,
	   KRT_Szerepkor_Funkcio.ErvKezd,
	   KRT_Szerepkor_Funkcio.ErvVege,
	   KRT_Szerepkor_Funkcio.Letrehozo_id,
	   KRT_Szerepkor_Funkcio.LetrehozasIdo,
	   KRT_Szerepkor_Funkcio.Modosito_id,
	   KRT_Szerepkor_Funkcio.ModositasIdo,
	   KRT_Szerepkor_Funkcio.Zarolo_id,
	   KRT_Szerepkor_Funkcio.ZarolasIdo,
	   KRT_Szerepkor_Funkcio.Tranz_id,
	   KRT_Szerepkor_Funkcio.UIAccessLog_id
	   from 
		 KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio 
	   where
		 KRT_Szerepkor_Funkcio.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end