﻿create procedure [dbo].[sp_KRT_TartomanyokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Tartomanyok.Id,
	   KRT_Tartomanyok.TartomanyKezd,
	   KRT_Tartomanyok.TartomanyHossz,
	   KRT_Tartomanyok.FoglaltDarab,
	   KRT_Tartomanyok.Ver,
	   KRT_Tartomanyok.Note,
	   KRT_Tartomanyok.Stat_id,
	   KRT_Tartomanyok.ErvKezd,
	   KRT_Tartomanyok.ErvVege,
	   KRT_Tartomanyok.Letrehozo_id,
	   KRT_Tartomanyok.LetrehozasIdo,
	   KRT_Tartomanyok.Modosito_id,
	   KRT_Tartomanyok.ModositasIdo,
	   KRT_Tartomanyok.Zarolo_id,
	   KRT_Tartomanyok.ZarolasIdo,
	   KRT_Tartomanyok.Tranz_id,
	   KRT_Tartomanyok.UIAccessLog_id
	   from 
		 KRT_Tartomanyok as KRT_Tartomanyok 
	   where
		 KRT_Tartomanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end