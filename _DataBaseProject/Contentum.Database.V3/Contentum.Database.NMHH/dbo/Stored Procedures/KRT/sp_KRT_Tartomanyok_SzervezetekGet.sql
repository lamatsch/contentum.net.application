﻿create procedure [dbo].[sp_KRT_Tartomanyok_SzervezetekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Tartomanyok_Szervezetek.Id,
	   KRT_Tartomanyok_Szervezetek.Tartomany_Id,
	   KRT_Tartomanyok_Szervezetek.Csoport_Id_Felelos,
	   KRT_Tartomanyok_Szervezetek.TartType,
	   KRT_Tartomanyok_Szervezetek.Ver,
	   KRT_Tartomanyok_Szervezetek.Note,
	   KRT_Tartomanyok_Szervezetek.Stat_id,
	   KRT_Tartomanyok_Szervezetek.ErvKezd,
	   KRT_Tartomanyok_Szervezetek.ErvVege,
	   KRT_Tartomanyok_Szervezetek.Letrehozo_id,
	   KRT_Tartomanyok_Szervezetek.LetrehozasIdo,
	   KRT_Tartomanyok_Szervezetek.Modosito_id,
	   KRT_Tartomanyok_Szervezetek.ModositasIdo,
	   KRT_Tartomanyok_Szervezetek.Zarolo_id,
	   KRT_Tartomanyok_Szervezetek.ZarolasIdo,
	   KRT_Tartomanyok_Szervezetek.Tranz_id,
	   KRT_Tartomanyok_Szervezetek.UIAccessLog_id
	   from 
		 KRT_Tartomanyok_Szervezetek as KRT_Tartomanyok_Szervezetek 
	   where
		 KRT_Tartomanyok_Szervezetek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end