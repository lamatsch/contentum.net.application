﻿create procedure [dbo].[sp_KRT_TelepulesekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Telepulesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Telepulesek.Id,
	   KRT_Telepulesek.Org,
	   KRT_Telepulesek.IRSZ,
	   KRT_Telepulesek.Telepules_Id_Fo,
	   KRT_Telepulesek.Nev,
	   KRT_Telepulesek.Orszag_Id,
	   KRT_Telepulesek.Megye,
	   KRT_Telepulesek.Regio,
	   KRT_Telepulesek.Ver,
	   KRT_Telepulesek.Note,
	   KRT_Telepulesek.Stat_id,
	   KRT_Telepulesek.ErvKezd,
	   KRT_Telepulesek.ErvVege,
	   KRT_Telepulesek.Letrehozo_id,
	   KRT_Telepulesek.LetrehozasIdo,
	   KRT_Telepulesek.Modosito_id,
	   KRT_Telepulesek.ModositasIdo,
	   KRT_Telepulesek.Zarolo_id,
	   KRT_Telepulesek.ZarolasIdo,
	   KRT_Telepulesek.Tranz_id,
	   KRT_Telepulesek.UIAccessLog_id  
   from 
     KRT_Telepulesek as KRT_Telepulesek      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end