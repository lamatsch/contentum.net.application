﻿create procedure [dbo].[sp_KRT_TemplateManagerGetAllWithFK]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_TemplateManager.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_TemplateManager.Id,
	   KRT_TemplateManager.Nev,
	   KRT_TemplateManager.Tipus,
       KRT_KodTarak.Nev as TipusNev,
	   KRT_TemplateManager.KRT_Modul_Id,
       KRT_Modulok.Nev as ModulNev,
	   KRT_TemplateManager.KRT_Dokumentum_Id,
       -- KRT_Dokumentumok.Nev as DokumentumNev,
       KRT_TemplateManager.KRT_Dokumentum_Id as DokumentumNev,
	   KRT_TemplateManager.Ver,
	   KRT_TemplateManager.Note,
	   KRT_TemplateManager.Stat_id,
	   KRT_TemplateManager.ErvKezd,
	   KRT_TemplateManager.ErvVege,
	   KRT_TemplateManager.Letrehozo_id,
	   KRT_TemplateManager.LetrehozasIdo,
	   KRT_TemplateManager.Modosito_id,
	   KRT_TemplateManager.ModositasIdo,
	   KRT_TemplateManager.Zarolo_id,
	   KRT_TemplateManager.ZarolasIdo,
	   KRT_TemplateManager.Tranz_id,
	   KRT_TemplateManager.UIAccessLog_id  
   from 
     KRT_TemplateManager as KRT_TemplateManager
     LEFT JOIN KRT_KodCsoportok on KRT_KodCsoportok.Kod=''TEMPMAN_FILETIPUS''
     LEFT JOIN KRT_KodTarak on KRT_TemplateManager.Tipus = KRT_KodTarak.Kod and KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
     LEFT JOIN KRT_Modulok on KRT_TemplateManager.KRT_Modul_Id = KRT_Modulok.Id
     -- LEFT JOIN Krt_Dokumentumok on KRT_TemplateManager.KRT_Dokumentum_Id = KRT_Dokumentumok.Id      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end