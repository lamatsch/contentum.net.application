﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_TemplateManagerHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_KRT_TemplateManagerHistoryGetRecord
go
*/
create procedure sp_KRT_TemplateManagerHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_TemplateManagerHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_TemplateManagerHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory Old
         inner join KRT_TemplateManagerHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_TemplateManagerHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory Old
         inner join KRT_TemplateManagerHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_TemplateManagerHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory Old
         inner join KRT_TemplateManagerHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_TemplateManagerHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KRT_Modul_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(Old.KRT_Modul_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ModulokAzonosito(New.KRT_Modul_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory Old
         inner join KRT_TemplateManagerHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_TemplateManagerHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KRT_Modul_Id as nvarchar(max)),'') != ISNULL(CAST(New.KRT_Modul_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Modulok FTOld on FTOld.Id = Old.KRT_Modul_Id and FTOld.Ver = Old.Ver
         left join KRT_Modulok FTNew on FTNew.Id = New.KRT_Modul_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KRT_Dokumentum_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(Old.KRT_Dokumentum_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(New.KRT_Dokumentum_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_TemplateManagerHistory Old
         inner join KRT_TemplateManagerHistory New on Old.Ver = (select top 1 Tmp.Ver from KRT_TemplateManagerHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KRT_Dokumentum_Id as nvarchar(max)),'') != ISNULL(CAST(New.KRT_Dokumentum_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Dokumentumok FTOld on FTOld.Id = Old.KRT_Dokumentum_Id and FTOld.Ver = Old.Ver
         left join KRT_Dokumentumok FTNew on FTNew.Id = New.KRT_Dokumentum_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go