﻿create procedure [dbo].[sp_KRT_TranzakciokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Tranzakciok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Tranzakciok.Id,
	   KRT_Tranzakciok.Alkalmazas_Id,
	   KRT_Tranzakciok.Felhasznalo_Id,
	   KRT_Tranzakciok.Ver,
	   KRT_Tranzakciok.Note,
	   KRT_Tranzakciok.Stat_id,
	   KRT_Tranzakciok.ErvKezd,
	   KRT_Tranzakciok.ErvVege,
	   KRT_Tranzakciok.Letrehozo_id,
	   KRT_Tranzakciok.LetrehozasIdo,
	   KRT_Tranzakciok.Modosito_id,
	   KRT_Tranzakciok.ModositasIdo,
	   KRT_Tranzakciok.Zarolo_id,
	   KRT_Tranzakciok.ZarolasIdo,
	   KRT_Tranzakciok.Tranz_id,
	   KRT_Tranzakciok.UIAccessLog_id  
   from 
     KRT_Tranzakciok as KRT_Tranzakciok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end