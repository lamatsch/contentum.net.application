﻿
create procedure [dbo].[sp_KRT_UIMezoObjektumErtekekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_UIMezoObjektumErtekek.Id,
	   KRT_UIMezoObjektumErtekek.TemplateTipusNev,
	   KRT_UIMezoObjektumErtekek.Nev,
	   KRT_UIMezoObjektumErtekek.Alapertelmezett,
	   KRT_UIMezoObjektumErtekek.Felhasznalo_Id,
	   KRT_UIMezoObjektumErtekek.TemplateXML,
	   KRT_UIMezoObjektumErtekek.UtolsoHasznIdo,
	   KRT_UIMezoObjektumErtekek.Org_Id,
	   KRT_UIMezoObjektumErtekek.Publikus,
	   KRT_UIMezoObjektumErtekek.Szervezet_Id,
	   KRT_UIMezoObjektumErtekek.Ver,
	   KRT_UIMezoObjektumErtekek.Note,
	   KRT_UIMezoObjektumErtekek.Stat_id,
	   KRT_UIMezoObjektumErtekek.ErvKezd,
	   KRT_UIMezoObjektumErtekek.ErvVege,
	   KRT_UIMezoObjektumErtekek.Letrehozo_id,
	   KRT_UIMezoObjektumErtekek.LetrehozasIdo,
	   KRT_UIMezoObjektumErtekek.Modosito_id,
	   KRT_UIMezoObjektumErtekek.ModositasIdo,
	   KRT_UIMezoObjektumErtekek.Zarolo_id,
	   KRT_UIMezoObjektumErtekek.ZarolasIdo,
	   KRT_UIMezoObjektumErtekek.Tranz_id,
	   KRT_UIMezoObjektumErtekek.UIAccessLog_id
	   from 
		 KRT_UIMezoObjektumErtekek as KRT_UIMezoObjektumErtekek 
	   where
		 KRT_UIMezoObjektumErtekek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end