﻿
CREATE procedure [dbo].[sp_KRT_UIMezoObjektumErtekekInsert]    
                @Id      uniqueidentifier = null,    
               	            @TemplateTipusNev     Nvarchar(100),
	            @Nev     Nvarchar(100),
	            @Felhasznalo_Id     uniqueidentifier,
                @TemplateXML     xml  = null,
                @UtolsoHasznIdo     datetime  = null,
                @Alapertelmezett     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
				@Org_Id uniqueidentifier  = null,
				@Publikus char(1) ='0',
				@Szervezet_Id uniqueidentifier = null,
		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS
 
 -- Ha alapertelmezettkent vesszuk fel a template-et, az eddigi alapertelmezettet torolni kell
 if( @Alapertelmezett = 1)
 BEGIN
	UPDATE KRT_UIMezoObjektumErtekek
		SET Alapertelmezett = 0
	WHERE
	    Felhasznalo_Id = @Felhasznalo_Id AND
	    TemplateTipusNev = @TemplateTipusNev AND
	    (getdate() between ErvKezd AND ErvVege) AND
		Alapertelmezett = 1	
 END

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @TemplateTipusNev is not null
         begin
            SET @insertColumns = @insertColumns + ',TemplateTipusNev'
            SET @insertValues = @insertValues + ',@TemplateTipusNev'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Alapertelmezett is not null
         begin
            SET @insertColumns = @insertColumns + ',Alapertelmezett'
            SET @insertValues = @insertValues + ',@Alapertelmezett'
         end 
       
         if @Felhasznalo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id'
         end 
       
         if @TemplateXML is not null
         begin
            SET @insertColumns = @insertColumns + ',TemplateXML'
            SET @insertValues = @insertValues + ',@TemplateXML'
         end 
       
         if @UtolsoHasznIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoHasznIdo'
            SET @insertValues = @insertValues + ',@UtolsoHasznIdo'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
		 if @Org_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Org_Id'
            SET @insertValues = @insertValues + ',@Org_Id'
         end    
		 if @Publikus is not null
         begin
            SET @insertColumns = @insertColumns + ',Publikus'
            SET @insertValues = @insertValues + ',@Publikus'
         end
         if @Szervezet_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Szervezet_Id'
            SET @insertValues = @insertValues + ',@Szervezet_Id'
         end      
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_UIMezoObjektumErtekek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@TemplateTipusNev Nvarchar(100),@Nev Nvarchar(100),@Alapertelmezett char(1),@Felhasznalo_Id uniqueidentifier,@TemplateXML xml,@UtolsoHasznIdo datetime,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier, @Org_Id uniqueidentifier, @Publikus char(1), @Szervezet_Id uniqueidentifier, @ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@TemplateTipusNev = @TemplateTipusNev,@Nev = @Nev,@Alapertelmezett = @Alapertelmezett,@Felhasznalo_Id = @Felhasznalo_Id,@TemplateXML = @TemplateXML,@UtolsoHasznIdo = @UtolsoHasznIdo,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id, @Org_Id = @Org_Id, @Publikus = @Publikus, @Szervezet_Id = @Szervezet_Id, @ResultUid = @ResultUid OUTPUT

If @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH