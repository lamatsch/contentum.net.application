﻿create procedure [dbo].[sp_KRT_USER_SETTINGSGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_USER_SETTINGS.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_USER_SETTINGS.Id,
	   KRT_USER_SETTINGS.Csoport_Id,
	   KRT_USER_SETTINGS.BEALLITASTIPUS,
	   KRT_USER_SETTINGS.BEALLITASOK_XML,
	   KRT_USER_SETTINGS.Ver,
	   KRT_USER_SETTINGS.Note,
	   KRT_USER_SETTINGS.Stat_id,
	   KRT_USER_SETTINGS.ErvKezd,
	   KRT_USER_SETTINGS.ErvVege,
	   KRT_USER_SETTINGS.Letrehozo_id,
	   KRT_USER_SETTINGS.LetrehozasIdo,
	   KRT_USER_SETTINGS.Modosito_id,
	   KRT_USER_SETTINGS.ModositasIdo,
	   KRT_USER_SETTINGS.Zarolo_id,
	   KRT_USER_SETTINGS.ZarolasIdo,
	   KRT_USER_SETTINGS.Tranz_id,
	   KRT_USER_SETTINGS.UIAccessLog_id  
   from 
     KRT_USER_SETTINGS as KRT_USER_SETTINGS      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end