﻿create procedure [dbo].[sp_KRT_VallalkozasokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Vallalkozasok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Vallalkozasok.Id,
	   KRT_Vallalkozasok.Partner_Id,
	   KRT_Vallalkozasok.Adoszam,
	   KRT_Vallalkozasok.KulfoldiAdoszamJelolo,
	   KRT_Vallalkozasok.TB_Torzsszam,
	   KRT_Vallalkozasok.Cegjegyzekszam,
	   KRT_Vallalkozasok.Tipus,
	   KRT_Vallalkozasok.Ver,
	   KRT_Vallalkozasok.Note,
	   KRT_Vallalkozasok.Stat_id,
	   KRT_Vallalkozasok.ErvKezd,
	   KRT_Vallalkozasok.ErvVege,
	   KRT_Vallalkozasok.Letrehozo_id,
	   KRT_Vallalkozasok.LetrehozasIdo,
	   KRT_Vallalkozasok.Modosito_id,
	   KRT_Vallalkozasok.ModositasIdo,
	   KRT_Vallalkozasok.Zarolo_id,
	   KRT_Vallalkozasok.ZarolasIdo,
	   KRT_Vallalkozasok.Tranz_id,
	   KRT_Vallalkozasok.UIAccessLog_id  
   from 
     KRT_Vallalkozasok as KRT_Vallalkozasok      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end