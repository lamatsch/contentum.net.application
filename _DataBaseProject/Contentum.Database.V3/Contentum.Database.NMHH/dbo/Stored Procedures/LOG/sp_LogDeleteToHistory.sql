﻿CREATE PROCEDURE [dbo].[sp_LogDeleteToHistory] 
		 @TableName NVARCHAR(100)
		,@Row_Id uniqueidentifier
		,@HistoryTableName NVARCHAR(100)
		--,@Muvelet int
		,@ExecutorUserId uniqueidentifier
		,@ExecutionTime datetime
		
AS
begin
BEGIN TRY
	set nocount on

	DECLARE @sqlCommand NVARCHAR(1000)
	DECLARE @MuveletKod NVARCHAR(100)
	DECLARE @Muvelet_Id uniqueidentifier

--	if (@Muvelet = 9) SET @MuveletKod = 'Delete'
	SET @MuveletKod = 'Delete'

	SET @Muvelet_Id = (Select Id from KRT_Muveletek Where Kod = @MuveletKod)	

	if @Muvelet_Id is null RAISERROR('[50002]',16,1)
-- modified by mg: az insert into-ban eredetileg @Muvelet_id volt a mezoneveknel
/*
  old part:

  insert into '+@HistoryTableName+' (MasterId, @Muvelet_Id, Ver, Record, Row_Id, Vegrehajto_Id, VegrehajtasIdo)
		values (newid(), @Muvelet_Id, -1, @XMLRecord, @Row_Id, @ExecutorUserId, @ExecutionTime)'

*/
	SET @sqlCommand = 
	   'DECLARE @XMLRecord xml
	    SET @XMLRecord =
		''<'+@TableName+'>
			<Id>'+convert(NVARCHAR(36),@Row_Id)+'</Id>
			<Modosito_id>'+convert(NVARCHAR(36),@ExecutorUserId)+'</Modosito_id>
			<ModositasIdo>'+convert(NVARCHAR(100),@ExecutionTime)+'</ModositasIdo>
		</'+@TableName+'>''

		insert into '+@HistoryTableName+' (HistoryId, HistoryMuvelet_Id, Ver,  Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo)
		values (newid(), 9, -1, @Row_Id, @ExecutorUserId, @ExecutionTime)'
	
	exec sp_executesql @sqlCommand, 
					N'@Row_Id uniqueidentifier,@Muvelet_Id uniqueidentifier,@ExecutorUserId uniqueidentifier,@ExecutionTime datetime'
					,@Row_Id = @Row_Id,@Muvelet_Id = @Muvelet_Id, @ExecutorUserId = @ExecutorUserId,@ExecutionTime = @ExecutionTime

	if @@rowcount != 1
	begin
		RAISERROR('[50002]',16,1)
	end
	

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end