﻿create procedure [dbo].[sp_LogEREC_IraElosztoivTetelekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraElosztoivTetelek where Id = @Row_Id;
          declare @ElosztoIv_Id_Ver int;
       -- select @ElosztoIv_Id_Ver = max(Ver) from EREC_IraElosztoivek where Id in (select ElosztoIv_Id from #tempTable);
              declare @Partner_Id_Ver int;
       -- select @Partner_Id_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id from #tempTable);
          
   insert into EREC_IraElosztoivTetelekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,ElosztoIv_Id       
 ,ElosztoIv_Id_Ver     
 ,Sorszam     
 ,Partner_Id       
 ,Partner_Id_Ver     
 ,Cim_Id     
 ,CimSTR     
 ,NevSTR     
 ,Kuldesmod     
 ,Visszavarolag     
 ,VisszavarasiIdo     
 ,Vissza_Nap_Mulva     
 ,AlairoSzerep     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,ElosztoIv_Id            
 ,@ElosztoIv_Id_Ver     
 ,Sorszam          
 ,Partner_Id            
 ,@Partner_Id_Ver     
 ,Cim_Id          
 ,CimSTR          
 ,NevSTR          
 ,Kuldesmod          
 ,Visszavarolag          
 ,VisszavarasiIdo          
 ,Vissza_Nap_Mulva          
 ,AlairoSzerep          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end