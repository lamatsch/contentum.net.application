﻿create procedure [dbo].[sp_LogEREC_IraIktatoKonyvekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraIktatoKonyvek where Id = @Row_Id;
      
   insert into EREC_IraIktatoKonyvekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Ev     
 ,Azonosito     
 ,DefaultIrattariTetelszam     
 ,Nev     
 ,MegkulJelzes     
 ,Iktatohely     
 ,KozpontiIktatasJelzo     
 ,UtolsoFoszam     
 ,UtolsoSorszam     
 ,Csoport_Id_Olvaso     
 ,Csoport_Id_Tulaj     
 ,IktSzamOsztas     
 ,FormatumKod     
 ,Titkos     
 ,IktatoErkezteto     
 ,LezarasDatuma     
 ,PostakonyvVevokod     
 ,PostakonyvMegallapodasAzon     
 ,EFeladoJegyzekUgyfelAdatok     
 ,HitelesExport_Dok_ID     
 ,Ver     
 ,Note     
 ,Ujranyitando  -- BLG_2549 (nekrisz javította)
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,Statusz     
 ,Terjedelem     
 ,SelejtezesDatuma     
 ,KezelesTipusa   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Ev          
 ,Azonosito          
 ,DefaultIrattariTetelszam          
 ,Nev          
 ,MegkulJelzes          
 ,Iktatohely          
 ,KozpontiIktatasJelzo          
 ,UtolsoFoszam          
 ,UtolsoSorszam          
 ,Csoport_Id_Olvaso          
 ,Csoport_Id_Tulaj          
 ,IktSzamOsztas          
 ,FormatumKod          
 ,Titkos          
 ,IktatoErkezteto          
 ,LezarasDatuma          
 ,PostakonyvVevokod          
 ,PostakonyvMegallapodasAzon          
 ,EFeladoJegyzekUgyfelAdatok          
 ,HitelesExport_Dok_ID          
 ,Ver          
 ,Note          
  ,Ujranyitando  -- BLG_2549 (nekrisz javította)
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,Statusz          
 ,Terjedelem          
 ,SelejtezesDatuma          
 ,KezelesTipusa        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end