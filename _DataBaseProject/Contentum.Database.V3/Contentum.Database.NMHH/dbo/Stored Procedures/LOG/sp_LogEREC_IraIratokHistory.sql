/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogEREC_IraIratokHistory')
            and   type = 'P')
   drop procedure sp_LogEREC_IraIratokHistory
go
*/
create procedure sp_LogEREC_IraIratokHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraIratok where Id = @Row_Id;
          declare @UgyUgyIratDarab_Id_Ver int;
       -- select @UgyUgyIratDarab_Id_Ver = max(Ver) from EREC_UgyUgyiratdarabok where Id in (select UgyUgyIratDarab_Id from #tempTable);
          
   insert into EREC_IraIratokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,PostazasIranya     
 ,Alszam     
 ,Sorszam     
 ,UtolsoSorszam     
 ,UgyUgyIratDarab_Id       
 ,UgyUgyIratDarab_Id_Ver     
 ,Kategoria     
 ,HivatkozasiSzam     
 ,IktatasDatuma     
 ,ExpedialasDatuma     
 ,ExpedialasModja     
 ,FelhasznaloCsoport_Id_Expedial     
 ,Targy     
 ,Jelleg     
 ,SztornirozasDat     
 ,FelhasznaloCsoport_Id_Iktato     
 ,FelhasznaloCsoport_Id_Kiadmany     
 ,UgyintezesAlapja     
 ,AdathordozoTipusa     
 ,Csoport_Id_Felelos     
 ,Csoport_Id_Ugyfelelos     
 ,FelhasznaloCsoport_Id_Orzo     
 ,KiadmanyozniKell     
 ,FelhasznaloCsoport_Id_Ugyintez     
 ,Hatarido     
 ,MegorzesiIdo     
 ,IratFajta     
 ,Irattipus     
 ,KuldKuldemenyek_Id     
 ,Allapot     
 ,Azonosito     
 ,GeneraltTargy     
 ,IratMetaDef_Id     
 ,IrattarbaKuldDatuma      
 ,IrattarbaVetelDat     
 ,Ugyirat_Id     
 ,Minosites     
 ,Elintezett     
 ,IratHatasaUgyintezesre     
 ,FelfuggesztesOka     
 ,LezarasOka     
 ,Munkaallomas     
 ,UgyintezesModja     
 ,IntezesIdopontja     
 ,IntezesiIdo     
 ,IntezesiIdoegyseg     
 ,UzemzavarKezdete     
 ,UzemzavarVege     
 ,UzemzavarIgazoloIratSzama     
 ,FelfuggesztettNapokSzama     
 ,VisszafizetesJogcime     
 ,VisszafizetesOsszege     
 ,VisszafizetesHatarozatSzama     
 ,Partner_Id_VisszafizetesCimzet     
 ,Partner_Nev_VisszafizetCimzett     
 ,VisszafizetesHatarido     
 ,Minosito     
 ,MinositesErvenyessegiIdeje     
 ,TerjedelemMennyiseg     
 ,TerjedelemMennyisegiEgyseg     
 ,TerjedelemMegjegyzes     
 ,SelejtezesDat     
 ,FelhCsoport_Id_Selejtezo     
 ,LeveltariAtvevoNeve     
 ,ModositasErvenyessegKezdete     
 ,MegszuntetoHatarozat     
 ,FelulvizsgalatDatuma     
 ,Felulvizsgalo_id     
 ,MinositesFelulvizsgalatEredm     
 ,UgyintezesKezdoDatuma     
 ,EljarasiSzakasz     
 ,HatralevoNapok     
 ,HatralevoMunkaNapok     
 ,Ugy_Fajtaja     
 ,MinositoSzervezet     
 ,MinositesFelulvizsgalatBizTag     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,PostazasIranya          
 ,Alszam          
 ,Sorszam          
 ,UtolsoSorszam          
 ,UgyUgyIratDarab_Id            
 ,@UgyUgyIratDarab_Id_Ver     
 ,Kategoria          
 ,HivatkozasiSzam          
 ,IktatasDatuma          
 ,ExpedialasDatuma          
 ,ExpedialasModja          
 ,FelhasznaloCsoport_Id_Expedial          
 ,Targy          
 ,Jelleg          
 ,SztornirozasDat          
 ,FelhasznaloCsoport_Id_Iktato          
 ,FelhasznaloCsoport_Id_Kiadmany          
 ,UgyintezesAlapja          
 ,AdathordozoTipusa          
 ,Csoport_Id_Felelos          
 ,Csoport_Id_Ugyfelelos          
 ,FelhasznaloCsoport_Id_Orzo          
 ,KiadmanyozniKell          
 ,FelhasznaloCsoport_Id_Ugyintez          
 ,Hatarido          
 ,MegorzesiIdo          
 ,IratFajta          
 ,Irattipus          
 ,KuldKuldemenyek_Id          
 ,Allapot          
 ,Azonosito          
 ,GeneraltTargy          
 ,IratMetaDef_Id          
 ,IrattarbaKuldDatuma           
 ,IrattarbaVetelDat          
 ,Ugyirat_Id          
 ,Minosites          
 ,Elintezett          
 ,IratHatasaUgyintezesre          
 ,FelfuggesztesOka          
 ,LezarasOka          
 ,Munkaallomas          
 ,UgyintezesModja          
 ,IntezesIdopontja          
 ,IntezesiIdo          
 ,IntezesiIdoegyseg          
 ,UzemzavarKezdete          
 ,UzemzavarVege          
 ,UzemzavarIgazoloIratSzama          
 ,FelfuggesztettNapokSzama          
 ,VisszafizetesJogcime          
 ,VisszafizetesOsszege          
 ,VisszafizetesHatarozatSzama          
 ,Partner_Id_VisszafizetesCimzet          
 ,Partner_Nev_VisszafizetCimzett          
 ,VisszafizetesHatarido          
 ,Minosito          
 ,MinositesErvenyessegiIdeje          
 ,TerjedelemMennyiseg          
 ,TerjedelemMennyisegiEgyseg          
 ,TerjedelemMegjegyzes          
 ,SelejtezesDat          
 ,FelhCsoport_Id_Selejtezo          
 ,LeveltariAtvevoNeve          
 ,ModositasErvenyessegKezdete          
 ,MegszuntetoHatarozat          
 ,FelulvizsgalatDatuma          
 ,Felulvizsgalo_id          
 ,MinositesFelulvizsgalatEredm          
 ,UgyintezesKezdoDatuma          
 ,EljarasiSzakasz          
 ,HatralevoNapok          
 ,HatralevoMunkaNapok          
 ,Ugy_Fajtaja          
 ,MinositoSzervezet          
 ,MinositesFelulvizsgalatBizTag          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go