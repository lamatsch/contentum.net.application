﻿create procedure [dbo].[sp_LogEREC_IraIrattariTetelekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraIrattariTetelek where Id = @Row_Id;
          declare @AgazatiJel_Id_Ver int;
       -- select @AgazatiJel_Id_Ver = max(Ver) from EREC_AgazatiJelek where Id in (select AgazatiJel_Id from #tempTable);
          
   insert into EREC_IraIrattariTetelekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,IrattariTetelszam     
 ,Nev     
 ,IrattariJel     
 ,MegorzesiIdo     
 ,Idoegyseg     
 ,Folyo_CM     
 ,AgazatiJel_Id       
 ,AgazatiJel_Id_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,EvenTuliIktatas   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,IrattariTetelszam          
 ,Nev          
 ,IrattariJel          
 ,MegorzesiIdo          
 ,Idoegyseg          
 ,Folyo_CM          
 ,AgazatiJel_Id            
 ,@AgazatiJel_Id_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,EvenTuliIktatas        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end