﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogEREC_IraJegyzekTetelekHistory')
            and   type = 'P')
   drop procedure sp_LogEREC_IraJegyzekTetelekHistory
go
*/
CREATE procedure sp_LogEREC_IraJegyzekTetelekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraJegyzekTetelek where Id = @Row_Id;
          declare @Jegyzek_Id_Ver int;
       -- select @Jegyzek_Id_Ver = max(Ver) from EREC_IraJegyzekek where Id in (select Jegyzek_Id from #tempTable);
          
   insert into EREC_IraJegyzekTetelekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Jegyzek_Id       
 ,Jegyzek_Id_Ver     
 ,Sorszam     
 ,Obj_Id     
 ,ObjTip_Id     
 ,Obj_type     
 ,Allapot     
 ,Megjegyzes     
 ,Azonosito     
 ,SztornozasDat     
 ,AtadasDatuma     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Jegyzek_Id            
 ,@Jegyzek_Id_Ver     
 ,Sorszam          
 ,Obj_Id          
 ,ObjTip_Id          
 ,Obj_type          
 ,Allapot          
 ,Megjegyzes          
 ,Azonosito          
 ,SztornozasDat          
 ,AtadasDatuma          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end