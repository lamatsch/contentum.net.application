﻿
create procedure sp_LogEREC_IraJegyzekekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraJegyzekek where Id = @Row_Id;
      
   insert into EREC_IraJegyzekekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Tipus     
 ,Minosites     
 ,Csoport_Id_felelos     
 ,FelhasznaloCsoport_Id_Vegrehaj     
 ,Partner_Id_LeveltariAtvevo     
 ,LezarasDatuma     
 ,SztornirozasDat     
 ,Nev     
 ,VegrehajtasDatuma     
 ,Foszam     
 ,Azonosito     
 ,MegsemmisitesDatuma     
 ,Csoport_Id_FelelosSzervezet     
 ,IraIktatokonyv_Id 
 ,Allapot     
 ,VegrehajtasKezdoDatuma      
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Tipus          
 ,Minosites          
 ,Csoport_Id_felelos          
 ,FelhasznaloCsoport_Id_Vegrehaj          
 ,Partner_Id_LeveltariAtvevo          
 ,LezarasDatuma          
 ,SztornirozasDat          
 ,Nev          
 ,VegrehajtasDatuma          
 ,Foszam          
 ,Azonosito          
 ,MegsemmisitesDatuma          
 ,Csoport_Id_FelelosSzervezet          
 ,IraIktatokonyv_Id
 ,Allapot     
 ,VegrehajtasKezdoDatuma            
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end