﻿
create procedure sp_LogEREC_IratAlairokHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IratAlairok where Id = @Row_Id;
      
   insert into EREC_IratAlairokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,PldIratPeldany_Id     
 ,Objtip_Id     
 ,Obj_Id     
 ,AlairasDatuma     
 ,Leiras     
 ,AlairoSzerep     
 ,AlairasSorrend     
 ,FelhasznaloCsoport_Id_Alairo     
 ,FelhaszCsoport_Id_Helyettesito     
 ,Azonosito     
 ,AlairasMod     
 ,AlairasSzabaly_Id     
 ,Allapot     
 ,FelhaszCsop_Id_HelyettesAlairo     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,PldIratPeldany_Id          
 ,Objtip_Id          
 ,Obj_Id          
 ,AlairasDatuma          
 ,Leiras          
 ,AlairoSzerep          
 ,AlairasSorrend          
 ,FelhasznaloCsoport_Id_Alairo          
 ,FelhaszCsoport_Id_Helyettesito          
 ,Azonosito          
 ,AlairasMod          
 ,AlairasSzabaly_Id          
 ,Allapot          
 ,FelhaszCsop_Id_HelyettesAlairo          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end