﻿create procedure [dbo].[sp_LogEREC_IratKapcsolatokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IratKapcsolatok where Id = @Row_Id;
          declare @Irat_Irat_Beepul_Ver int;
       -- select @Irat_Irat_Beepul_Ver = max(Ver) from EREC_IraIratok where Id in (select Irat_Irat_Beepul from #tempTable);
              declare @Irat_Irat_Felepul_Ver int;
       -- select @Irat_Irat_Felepul_Ver = max(Ver) from EREC_IraIratok where Id in (select Irat_Irat_Felepul from #tempTable);
          
   insert into EREC_IratKapcsolatokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,KapcsolatTipus     
 ,Leiras     
 ,Kezi     
 ,Irat_Irat_Beepul       
 ,Irat_Irat_Beepul_Ver     
 ,Irat_Irat_Felepul       
 ,Irat_Irat_Felepul_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,KapcsolatTipus          
 ,Leiras          
 ,Kezi          
 ,Irat_Irat_Beepul            
 ,@Irat_Irat_Beepul_Ver     
 ,Irat_Irat_Felepul            
 ,@Irat_Irat_Felepul_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end