﻿create procedure [dbo].[sp_LogEREC_Irat_IktatokonyveiHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_Irat_Iktatokonyvei where Id = @Row_Id;
          declare @IraIktatokonyv_Id_Ver int;
       -- select @IraIktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select IraIktatokonyv_Id from #tempTable);
              declare @Csoport_Id_Iktathat_Ver int;
       -- select @Csoport_Id_Iktathat_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Iktathat from #tempTable);
          
   insert into EREC_Irat_IktatokonyveiHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,IraIktatokonyv_Id       
 ,IraIktatokonyv_Id_Ver     
 ,Csoport_Id_Iktathat       
 ,Csoport_Id_Iktathat_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,IraIktatokonyv_Id            
 ,@IraIktatokonyv_Id_Ver     
 ,Csoport_Id_Iktathat            
 ,@Csoport_Id_Iktathat_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end