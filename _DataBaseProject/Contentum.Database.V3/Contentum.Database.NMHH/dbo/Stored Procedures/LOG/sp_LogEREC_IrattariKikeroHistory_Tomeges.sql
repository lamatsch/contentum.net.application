﻿create procedure [dbo].[sp_LogEREC_IrattariKikeroHistory_Tomeges]
		 @Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IrattariKikero where Id IN (SELECT Id FROM #ResultUid)
          declare @UgyUgyirat_Id_Ver int;
       -- select @UgyUgyirat_Id_Ver = max(Ver) from EREC_UgyUgyiratok where Id in (select UgyUgyirat_Id from #tempTable);
              declare @Tertiveveny_Id_Ver int;
       -- select @Tertiveveny_Id_Ver = max(Ver) from EREC_KuldTertivevenyek where Id in (select Tertiveveny_Id from #tempTable);
          
   insert into EREC_IrattariKikeroHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Keres_note     
 ,FelhasznalasiCel     
 ,DokumentumTipus     
 ,Indoklas_note     
 ,FelhasznaloCsoport_Id_Kikero     
 ,KeresDatuma     
 ,KulsoAzonositok     
 ,UgyUgyirat_Id       
 ,UgyUgyirat_Id_Ver     
 ,Tertiveveny_Id       
 ,Tertiveveny_Id_Ver     
 ,Obj_Id     
 ,ObjTip_Id     
 ,KikerKezd     
 ,KikerVege     
 ,FelhasznaloCsoport_Id_Jovahagy     
 ,JovagyasDatuma     
 ,FelhasznaloCsoport_Id_Kiado     
 ,KiadasDatuma     
 ,FelhasznaloCsoport_Id_Visszaad     
 ,FelhasznaloCsoport_Id_Visszave     
 ,VisszaadasDatuma     
 ,SztornirozasDat     
 ,Allapot     
 ,BarCode     
 ,Irattar_Id     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Keres_note          
 ,FelhasznalasiCel          
 ,DokumentumTipus          
 ,Indoklas_note          
 ,FelhasznaloCsoport_Id_Kikero          
 ,KeresDatuma          
 ,KulsoAzonositok          
 ,UgyUgyirat_Id            
 ,@UgyUgyirat_Id_Ver     
 ,Tertiveveny_Id            
 ,@Tertiveveny_Id_Ver     
 ,Obj_Id          
 ,ObjTip_Id          
 ,KikerKezd          
 ,KikerVege          
 ,FelhasznaloCsoport_Id_Jovahagy          
 ,JovagyasDatuma          
 ,FelhasznaloCsoport_Id_Kiado          
 ,KiadasDatuma          
 ,FelhasznaloCsoport_Id_Visszaad          
 ,FelhasznaloCsoport_Id_Visszave          
 ,VisszaadasDatuma          
 ,SztornirozasDat          
 ,Allapot          
 ,BarCode          
 ,Irattar_Id          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end