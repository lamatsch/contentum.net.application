﻿create procedure [dbo].[sp_LogEREC_KuldBekuldokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_KuldBekuldok where Id = @Row_Id;
          declare @KuldKuldemeny_Id_Ver int;
       -- select @KuldKuldemeny_Id_Ver = max(Ver) from EREC_KuldKuldemenyek where Id in (select KuldKuldemeny_Id from #tempTable);
              declare @Partner_Id_Bekuldo_Ver int;
       -- select @Partner_Id_Bekuldo_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id_Bekuldo from #tempTable);
              declare @PartnerCim_Id_Bekuldo_Ver int;
       -- select @PartnerCim_Id_Bekuldo_Ver = max(Ver) from KRT_PartnerCimek where Id in (select PartnerCim_Id_Bekuldo from #tempTable);
          
   insert into EREC_KuldBekuldokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,KuldKuldemeny_Id       
 ,KuldKuldemeny_Id_Ver     
 ,Partner_Id_Bekuldo       
 ,Partner_Id_Bekuldo_Ver     
 ,PartnerCim_Id_Bekuldo       
 ,PartnerCim_Id_Bekuldo_Ver     
 ,NevSTR     
 ,CimSTR     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,KuldKuldemeny_Id            
 ,@KuldKuldemeny_Id_Ver     
 ,Partner_Id_Bekuldo            
 ,@Partner_Id_Bekuldo_Ver     
 ,PartnerCim_Id_Bekuldo            
 ,@PartnerCim_Id_Bekuldo_Ver     
 ,NevSTR          
 ,CimSTR          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end