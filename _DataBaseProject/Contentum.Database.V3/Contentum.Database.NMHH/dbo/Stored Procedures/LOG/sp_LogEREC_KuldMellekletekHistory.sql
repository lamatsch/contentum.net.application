﻿create procedure [dbo].[sp_LogEREC_KuldMellekletekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_KuldMellekletek where Id = @Row_Id;
          declare @KuldKuldemeny_Id_Ver int;
       --select @KuldKuldemeny_Id_Ver = max(Ver) from EREC_KuldKuldemenyek where Id in (select KuldKuldemeny_Id from #tempTable);   
   insert into EREC_KuldMellekletekHistory
   select newid(),@Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,KuldKuldemeny_Id            
 ,@KuldKuldemeny_Id_Ver     
 ,Mennyiseg          
 ,Megjegyzes          
 ,SztornirozasDat          
 ,AdathordozoTipus          
 ,MennyisegiEgyseg          
 ,BarCode          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end