﻿create procedure [dbo].[sp_LogEREC_KuldTertivevenyekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_KuldTertivevenyek where Id = @Row_Id;
          declare @Kuldemeny_Id_Ver int;
       -- select @Kuldemeny_Id_Ver = max(Ver) from EREC_KuldKuldemenyek where Id in (select Kuldemeny_Id from #tempTable);
          
   insert into EREC_KuldTertivevenyekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Kuldemeny_Id       
 ,Kuldemeny_Id_Ver     
 ,Ragszam     
 ,TertivisszaKod     
 ,TertivisszaDat     
 ,AtvevoSzemely     
 ,AtvetelDat     
 ,BarCode     
 ,Allapot     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,KezbVelelemBeallta     
 ,KezbVelelemDatuma   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Kuldemeny_Id            
 ,@Kuldemeny_Id_Ver     
 ,Ragszam          
 ,TertivisszaKod          
 ,TertivisszaDat          
 ,AtvevoSzemely          
 ,AtvetelDat          
 ,BarCode          
 ,Allapot          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,KezbVelelemBeallta          
 ,KezbVelelemDatuma        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end