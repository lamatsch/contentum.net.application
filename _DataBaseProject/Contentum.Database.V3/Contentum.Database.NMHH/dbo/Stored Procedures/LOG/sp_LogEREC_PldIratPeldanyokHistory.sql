﻿
create procedure sp_LogEREC_PldIratPeldanyokHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_PldIratPeldanyok where Id = @Row_Id;
      
   insert into EREC_PldIratPeldanyokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,IraIrat_Id     
 ,UgyUgyirat_Id_Kulso     
 ,Sorszam     
 ,SztornirozasDat     
 ,AtvetelDatuma     
 ,Eredet     
 ,KuldesMod     
 ,Ragszam     
 ,UgyintezesModja     
 ,VisszaerkezesiHatarido     
 ,Visszavarolag     
 ,VisszaerkezesDatuma     
 ,Cim_id_Cimzett     
 ,Partner_Id_Cimzett
 ,Partner_Id_CimzettKapcsolt     
 ,Csoport_Id_Felelos     
 ,FelhasznaloCsoport_Id_Orzo     
 ,Csoport_Id_Felelos_Elozo     
 ,CimSTR_Cimzett     
 ,NevSTR_Cimzett     
 ,Tovabbito     
 ,IraIrat_Id_Kapcsolt     
 ,IrattariHely     
 ,Gener_Id     
 ,PostazasDatuma     
 ,BarCode     
 ,Allapot     
 ,Azonosito     
 ,Kovetkezo_Felelos_Id     
 ,Elektronikus_Kezbesitesi_Allap     
 ,Kovetkezo_Orzo_Id     
 ,Fizikai_Kezbesitesi_Allapot     
 ,TovabbitasAlattAllapot     
 ,PostazasAllapot     
 ,ValaszElektronikus     
 ,SelejtezesDat     
 ,FelhCsoport_Id_Selejtezo     
 ,LeveltariAtvevoNeve      
 ,IrattarId     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id
 , Aktiv
 ,IratPeldanyMegsemmisitve 
 ,IratPeldanyMegsemmisitesDatuma 
 )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,IraIrat_Id          
 ,UgyUgyirat_Id_Kulso          
 ,Sorszam          
 ,SztornirozasDat          
 ,AtvetelDatuma          
 ,Eredet          
 ,KuldesMod          
 ,Ragszam          
 ,UgyintezesModja          
 ,VisszaerkezesiHatarido          
 ,Visszavarolag          
 ,VisszaerkezesDatuma          
 ,Cim_id_Cimzett          
 ,Partner_Id_Cimzett  
 ,Partner_Id_CimzettKapcsolt
 ,Csoport_Id_Felelos          
 ,FelhasznaloCsoport_Id_Orzo          
 ,Csoport_Id_Felelos_Elozo          
 ,CimSTR_Cimzett          
 ,NevSTR_Cimzett          
 ,Tovabbito          
 ,IraIrat_Id_Kapcsolt          
 ,IrattariHely          
 ,Gener_Id          
 ,PostazasDatuma          
 ,BarCode          
 ,Allapot          
 ,Azonosito          
 ,Kovetkezo_Felelos_Id          
 ,Elektronikus_Kezbesitesi_Allap          
 ,Kovetkezo_Orzo_Id          
 ,Fizikai_Kezbesitesi_Allapot          
 ,TovabbitasAlattAllapot          
 ,PostazasAllapot          
 ,ValaszElektronikus          
 ,SelejtezesDat          
 ,FelhCsoport_Id_Selejtezo          
 ,LeveltariAtvevoNeve                  
 ,IrattarId          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id
 ,UIAccessLog_id  
 , Aktiv
 ,IratPeldanyMegsemmisitve 
 ,IratPeldanyMegsemmisitesDatuma       
 from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end