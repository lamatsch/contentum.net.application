/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogEREC_TomegesIktatasTetelekHistory')
            and   type = 'P')
   drop procedure sp_LogEREC_TomegesIktatasTetelekHistory
go
*/
create procedure sp_LogEREC_TomegesIktatasTetelekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_TomegesIktatasTetelek where Id = @Row_Id;
      
   insert into EREC_TomegesIktatasTetelekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Folyamat_Id     
 ,Forras_Azonosito     
 ,IktatasTipus     
 ,Alszamra     
 ,Ugyirat_Id     
 ,Kuldemeny_Id     
 ,Iktatokonyv_Id     
 ,ExecParam     
 ,EREC_UgyUgyiratok     
 ,EREC_UgyUgyiratdarabok     
 ,EREC_IraIratok     
 ,EREC_PldIratPeldanyok     
 ,EREC_HataridosFeladatok     
 ,EREC_KuldKuldemenyek     
 ,IktatasiParameterek     
 ,ErkeztetesParameterek     
 ,Dokumentumok     
 ,Azonosito     
 ,Allapot     
 ,Irat_Id     
 ,Hiba     
 ,Expedialas     
 ,TobbIratEgyKuldemenybe     
 ,KuldemenyAtadas     
 ,HatosagiStatAdat     
 ,EREC_IratAlairok     
 ,Lezarhato     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Folyamat_Id          
 ,Forras_Azonosito          
 ,IktatasTipus          
 ,Alszamra          
 ,Ugyirat_Id          
 ,Kuldemeny_Id          
 ,Iktatokonyv_Id          
 ,ExecParam          
 ,EREC_UgyUgyiratok          
 ,EREC_UgyUgyiratdarabok          
 ,EREC_IraIratok          
 ,EREC_PldIratPeldanyok          
 ,EREC_HataridosFeladatok          
 ,EREC_KuldKuldemenyek          
 ,IktatasiParameterek          
 ,ErkeztetesParameterek          
 ,Dokumentumok          
 ,Azonosito          
 ,Allapot          
 ,Irat_Id          
 ,Hiba          
 ,Expedialas          
 ,TobbIratEgyKuldemenybe          
 ,KuldemenyAtadas          
 ,HatosagiStatAdat          
 ,EREC_IratAlairok          
 ,Lezarhato          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go