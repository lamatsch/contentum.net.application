﻿create procedure [dbo].[sp_LogEREC_eMailBoritekCsatolmanyokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_eMailBoritekCsatolmanyok where Id = @Row_Id;
          declare @eMailBoritek_Id_Ver int;
       -- select @eMailBoritek_Id_Ver = max(Ver) from EREC_eMailBoritekok where Id in (select eMailBoritek_Id from #tempTable);
              declare @Dokumentum_Id_Ver int;
       -- select @Dokumentum_Id_Ver = max(Ver) from KRT_Dokumentumok where Id in (select Dokumentum_Id from #tempTable);
          
   insert into EREC_eMailBoritekCsatolmanyokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,eMailBoritek_Id       
 ,eMailBoritek_Id_Ver     
 ,Dokumentum_Id       
 ,Dokumentum_Id_Ver     
 ,Nev     
 ,Tomoritve     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,eMailBoritek_Id            
 ,@eMailBoritek_Id_Ver     
 ,Dokumentum_Id            
 ,@Dokumentum_Id_Ver     
 ,Nev          
 ,Tomoritve          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end