﻿create procedure [dbo].[sp_LogEREC_eMailBoritekokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_eMailBoritekok where Id = @Row_Id;
      
   insert into EREC_eMailBoritekokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Felado     
 ,Cimzett     
 ,CC     
 ,Targy     
 ,FeladasDatuma     
 ,ErkezesDatuma     
 ,Fontossag     
 ,KuldKuldemeny_Id     
 ,IraIrat_Id     
 ,DigitalisAlairas     
 ,Uzenet     
 ,EmailForras     
 ,EmailGuid     
 ,FeldolgozasIdo     
 ,ForrasTipus     
 ,Allapot     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Felado          
 ,Cimzett          
 ,CC          
 ,Targy          
 ,FeladasDatuma          
 ,ErkezesDatuma          
 ,Fontossag          
 ,KuldKuldemeny_Id          
 ,IraIrat_Id          
 ,DigitalisAlairas          
 ,Uzenet          
 ,EmailForras          
 ,EmailGuid          
 ,FeldolgozasIdo          
 ,ForrasTipus          
 ,Allapot          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end