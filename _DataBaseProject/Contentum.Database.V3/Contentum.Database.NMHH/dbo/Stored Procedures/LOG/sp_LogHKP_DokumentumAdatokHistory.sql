﻿create procedure [dbo].[sp_LogHKP_DokumentumAdatokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from HKP_DokumentumAdatok where Id = @Row_Id;
      
   insert into HKP_DokumentumAdatokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Irany     
 ,Allapot     
 ,FeladoTipusa     
 ,KapcsolatiKod     
 ,Nev     
 ,Email     
 ,RovidNev     
 ,MAKKod     
 ,KRID     
 ,ErkeztetesiSzam     
 ,HivatkozasiSzam     
 ,DokTipusHivatal     
 ,DokTipusAzonosito     
 ,DokTipusLeiras     
 ,Megjegyzes     
 ,FileNev     
 ,ErvenyessegiDatum     
 ,ErkeztetesiDatum     
 ,Kezbesitettseg     
 ,Idopecset     
 ,ValaszTitkositas     
 ,ValaszUtvonal     
 ,Rendszeruzenet     
 ,Tarterulet     
 ,ETertiveveny     
 ,Lenyomat     
 ,Dokumentum_Id     
 ,KuldKuldemeny_Id     
 ,IraIrat_Id     
 ,IratPeldany_Id     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Irany          
 ,Allapot          
 ,FeladoTipusa          
 ,KapcsolatiKod          
 ,Nev          
 ,Email          
 ,RovidNev          
 ,MAKKod          
 ,KRID          
 ,ErkeztetesiSzam          
 ,HivatkozasiSzam          
 ,DokTipusHivatal          
 ,DokTipusAzonosito          
 ,DokTipusLeiras          
 ,Megjegyzes          
 ,FileNev          
 ,ErvenyessegiDatum          
 ,ErkeztetesiDatum          
 ,Kezbesitettseg          
 ,Idopecset          
 ,ValaszTitkositas          
 ,ValaszUtvonal          
 ,Rendszeruzenet          
 ,Tarterulet          
 ,ETertiveveny          
 ,Lenyomat          
 ,Dokumentum_Id          
 ,KuldKuldemeny_Id          
 ,IraIrat_Id          
 ,IratPeldany_Id          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end