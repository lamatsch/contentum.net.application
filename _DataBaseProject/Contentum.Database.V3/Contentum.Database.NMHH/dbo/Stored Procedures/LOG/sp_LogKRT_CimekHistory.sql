﻿create procedure [dbo].[sp_LogKRT_CimekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Cimek where Id = @Row_Id;
          declare @Orszag_Id_Ver int;
       -- select @Orszag_Id_Ver = max(Ver) from KRT_Orszagok where Id in (select Orszag_Id from #tempTable);
              declare @Telepules_Id_Ver int;
       -- select @Telepules_Id_Ver = max(Ver) from KRT_Telepulesek where Id in (select Telepules_Id from #tempTable);
              declare @Kozterulet_Id_Ver int;
       -- select @Kozterulet_Id_Ver = max(Ver) from KRT_Kozteruletek where Id in (select Kozterulet_Id from #tempTable);
              declare @KozteruletTipus_Id_Ver int;
       -- select @KozteruletTipus_Id_Ver = max(Ver) from KRT_KozteruletTipusok where Id in (select KozteruletTipus_Id from #tempTable);
          
   insert into KRT_CimekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Kategoria     
 ,Tipus     
 ,Nev     
 ,KulsoAzonositok     
 ,Forras     
 ,Orszag_Id       
 ,Orszag_Id_Ver     
 ,OrszagNev     
 ,Telepules_Id       
 ,Telepules_Id_Ver     
 ,TelepulesNev     
 ,IRSZ     
 ,CimTobbi     
 ,Kozterulet_Id       
 ,Kozterulet_Id_Ver     
 ,KozteruletNev     
 ,KozteruletTipus_Id       
 ,KozteruletTipus_Id_Ver     
 ,KozteruletTipusNev     
 ,Hazszam     
 ,Hazszamig     
 ,HazszamBetujel     
 ,MindketOldal     
 ,HRSZ     
 ,Lepcsohaz     
 ,Szint     
 ,Ajto     
 ,AjtoBetujel     
 ,Tobbi     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Kategoria          
 ,Tipus          
 ,Nev          
 ,KulsoAzonositok          
 ,Forras          
 ,Orszag_Id            
 ,@Orszag_Id_Ver     
 ,OrszagNev          
 ,Telepules_Id            
 ,@Telepules_Id_Ver     
 ,TelepulesNev          
 ,IRSZ          
 ,CimTobbi          
 ,Kozterulet_Id            
 ,@Kozterulet_Id_Ver     
 ,KozteruletNev          
 ,KozteruletTipus_Id            
 ,@KozteruletTipus_Id_Ver     
 ,KozteruletTipusNev          
 ,Hazszam          
 ,Hazszamig          
 ,HazszamBetujel          
 ,MindketOldal          
 ,HRSZ          
 ,Lepcsohaz          
 ,Szint          
 ,Ajto          
 ,AjtoBetujel          
 ,Tobbi          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end