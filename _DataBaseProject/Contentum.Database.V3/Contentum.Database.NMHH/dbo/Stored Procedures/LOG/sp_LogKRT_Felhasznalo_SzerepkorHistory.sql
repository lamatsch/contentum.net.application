﻿create procedure [dbo].[sp_LogKRT_Felhasznalo_SzerepkorHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Felhasznalo_Szerepkor where Id = @Row_Id;
          declare @Csoport_Id_Ver int;
       -- select @Csoport_Id_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id from #tempTable);
              declare @Felhasznalo_Id_Ver int;
       -- select @Felhasznalo_Id_Ver = max(Ver) from KRT_Felhasznalok where Id in (select Felhasznalo_Id from #tempTable);
              declare @Szerepkor_Id_Ver int;
       -- select @Szerepkor_Id_Ver = max(Ver) from KRT_Szerepkorok where Id in (select Szerepkor_Id from #tempTable);
          
   insert into KRT_Felhasznalo_SzerepkorHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,CsoportTag_Id     
 ,Csoport_Id       
 ,Csoport_Id_Ver     
 ,Felhasznalo_Id       
 ,Felhasznalo_Id_Ver     
 ,Szerepkor_Id       
 ,Szerepkor_Id_Ver     
 ,Helyettesites_Id     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,CsoportTag_Id          
 ,Csoport_Id            
 ,@Csoport_Id_Ver     
 ,Felhasznalo_Id            
 ,@Felhasznalo_Id_Ver     
 ,Szerepkor_Id            
 ,@Szerepkor_Id_Ver     
 ,Helyettesites_Id          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end