﻿create procedure [dbo].[sp_LogKRT_FunkcioListaHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_FunkcioLista where Id = @Row_Id;
          declare @Funkcio_Id_Ver int;
       -- select @Funkcio_Id_Ver = max(Ver) from KRT_Funkciok where Id in (select Funkcio_Id from #tempTable);
              declare @Funkcio_Id_Hivott_Ver int;
       -- select @Funkcio_Id_Hivott_Ver = max(Ver) from KRT_Funkciok where Id in (select Funkcio_Id_Hivott from #tempTable);
          
   insert into KRT_FunkcioListaHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Funkcio_Id       
 ,Funkcio_Id_Ver     
 ,Funkcio_Id_Hivott       
 ,Funkcio_Id_Hivott_Ver     
 ,FutasiSorrend     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Funkcio_Id            
 ,@Funkcio_Id_Ver     
 ,Funkcio_Id_Hivott            
 ,@Funkcio_Id_Hivott_Ver     
 ,FutasiSorrend          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end