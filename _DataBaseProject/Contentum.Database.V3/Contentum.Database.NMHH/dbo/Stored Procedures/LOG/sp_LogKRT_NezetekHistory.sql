﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogKRT_NezetekHistory')
            and   type = 'P')
   drop procedure sp_LogKRT_NezetekHistory
go
*/
CREATE procedure sp_LogKRT_NezetekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Nezetek where Id = @Row_Id;
          declare @Form_Id_Ver int;
       -- select @Form_Id_Ver = max(Ver) from KRT_Modulok where Id in (select Form_Id from #tempTable);
              declare @Muvelet_Id_Ver int;
       -- select @Muvelet_Id_Ver = max(Ver) from KRT_Muveletek where Id in (select Muvelet_Id from #tempTable);
          
   insert into KRT_NezetekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Form_Id       
 ,Form_Id_Ver     
 ,Nev     
 ,Leiras     
 ,Csoport_Id     
 ,Muvelet_Id       
 ,Muvelet_Id_Ver     
 ,DisableControls     
 ,InvisibleControls     
 ,ReadOnlyControls     
 ,TabIndexControls     
 ,Prioritas     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Form_Id            
 ,@Form_Id_Ver     
 ,Nev          
 ,Leiras          
 ,Csoport_Id          
 ,Muvelet_Id            
 ,@Muvelet_Id_Ver     
 ,DisableControls          
 ,InvisibleControls          
 ,ReadOnlyControls          
 ,TabIndexControls          
 ,Prioritas          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end