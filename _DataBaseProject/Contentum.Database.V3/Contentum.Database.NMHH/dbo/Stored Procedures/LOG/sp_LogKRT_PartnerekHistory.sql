﻿create procedure [dbo].[sp_LogKRT_PartnerekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Partnerek where Id = @Row_Id;
          declare @Id_Ver int;
       -- select @Id_Ver = max(Ver) from KRT_Csoportok where Id in (select Id from #tempTable);
          
   insert into KRT_PartnerekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id       
 ,Id_Ver     
 ,Org     
 ,Orszag_Id     
 ,Tipus     
 ,Nev     
 ,Hierarchia     
 ,KulsoAzonositok     
 ,PublikusKulcs     
 ,Kiszolgalo     
 ,LetrehozoSzervezet     
 ,MaxMinosites     
 ,MinositesKezdDat     
 ,MinositesVegDat     
 ,MaxMinositesSzervezet     
 ,Belso     
 ,Forras     
 ,Allapot     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id            
 ,@Id_Ver     
 ,Org          
 ,Orszag_Id          
 ,Tipus          
 ,Nev          
 ,Hierarchia          
 ,KulsoAzonositok          
 ,PublikusKulcs          
 ,Kiszolgalo          
 ,LetrehozoSzervezet          
 ,MaxMinosites          
 ,MinositesKezdDat          
 ,MinositesVegDat          
 ,MaxMinositesSzervezet          
 ,Belso          
 ,Forras          
 ,Allapot          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end