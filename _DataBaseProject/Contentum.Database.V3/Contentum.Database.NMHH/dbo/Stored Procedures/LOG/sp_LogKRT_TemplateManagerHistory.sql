﻿create procedure [dbo].[sp_LogKRT_TemplateManagerHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_TemplateManager where Id = @Row_Id;
          declare @KRT_Modul_Id_Ver int;
       -- select @KRT_Modul_Id_Ver = max(Ver) from KRT_Modulok where Id in (select KRT_Modul_Id from #tempTable);
              declare @KRT_Dokumentum_Id_Ver int;
       -- select @KRT_Dokumentum_Id_Ver = max(Ver) from KRT_Dokumentumok where Id in (select KRT_Dokumentum_Id from #tempTable);
          
   insert into KRT_TemplateManagerHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Nev     
 ,Tipus     
 ,KRT_Modul_Id       
 ,KRT_Modul_Id_Ver     
 ,KRT_Dokumentum_Id       
 ,KRT_Dokumentum_Id_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Nev          
 ,Tipus          
 ,KRT_Modul_Id            
 ,@KRT_Modul_Id_Ver     
 ,KRT_Dokumentum_Id            
 ,@KRT_Dokumentum_Id_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end