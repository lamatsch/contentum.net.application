﻿CREATE PROCEDURE [dbo].[sp_LogRecordsToHistory_Tomeges] 
		 @TableName varchar(100)
		,@Row_Ids varchar(MAX)
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		
AS
begin
BEGIN TRY
	set nocount on

	if @Row_Ids is not null
	BEGIN
		declare @table_ColumnNames varchar(4000)
		declare @sqlCommand varchar(MAX);

		if (@VegrehajtasIdo is null) set @VegrehajtasIdo = getdate();

		set @table_ColumnNames = dbo.fn_GetTableColumnNamesConcatenated(@TableName)
		if (@table_ColumnNames is not null)
		BEGIN
			set @sqlCommand = 'insert into ' + @TableName + 'History (HistoryMuvelet_Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo, '
				+ @table_ColumnNames
				+ ') select ' + convert(varchar(40),  @Muvelet) + ', ''' + convert(varchar(36), @Vegrehajto_Id) + ''', '''
				+ convert(varchar, @VegrehajtasIdo, 120) + ''','
				+ @table_ColumnNames
				+ ' from ' + @TableName + ' where Id in (' + @Row_Ids + ')'
				
			exec (@sqlCommand)
		END
	END

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end