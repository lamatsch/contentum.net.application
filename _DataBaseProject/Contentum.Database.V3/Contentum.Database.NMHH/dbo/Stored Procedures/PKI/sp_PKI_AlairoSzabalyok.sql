﻿create procedure [dbo].[sp_PKI_AlairoSzabalyok]    
                 @Felhasznalo_Id   uniqueidentifier,  -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod    nvarchar(100),     -- a külso alkalmazás kódja (egyébként üres)
                 @FolyamatKod      nvarchar(100),     -- a folyamat azonosító kódja
                 @MuveletKod       nvarchar(100),     -- a muvelet azonosító kódja
                 @AlairoSzerep     nvarchar(64)       -- az aláíró szerep kódja
AS
/*
-- FELADATA:
	A kijelölt felhasználó számára megengedett aláírási szabályok listájának lekérdezése,
    az esetlegesen megadott folyamat vagy muvelet paraméterek szerinti szukítéssel.

-- Példa:
declare @Felhasznalo_Idc  uniqueidentifier
select @Felhasznalo_Idc = convert(uniqueidentifier, '5B7FEE68-CC7A-42D5-9D58-95794E5129F4')
exec [dbo].[sp_PKI_AlairoSzabalyok] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'IratJovahagyas',    -- @FolyamatKod
	 NULL, 	              -- @MuveletKod
	 '2' 				  -- @AlairoSzerep
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod   nvarchar(16)
set     @HasznalatiMod = 'AlairasLista'

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @Felhasznalo_Id is NULL
   RAISERROR('A felhasználó azonosító megadása kötelezo!',16,1)

------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
declare @ResultAlairasEASZkd nvarchar(100),
	    @ResultTanusitvanyLn nvarchar(100),
	    @ResultAlairSzabaly  uniqueidentifier 

exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     NULL, --@AlairasSzabaly_Id
     NULL, --@AlairasKod,
     NULL, --@ObjektumTip,
     NULL, --@Objektum_Id,
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     @FolyamatKod,
     @MuveletKod,
     @AlairoSzerep,
     '0',  --@Titkositas
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @ResultAlairSzabaly  OUTPUT 

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------