﻿create procedure [dbo].[sp_PKI_ElojegyzettAlairas]    
                 @Felhasznalo_Id       uniqueidentifier,    -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod        nvarchar(100),       -- a külso alkalmazás kódja (egyébként üres)
                 @ObjektumTip          nvarchar(4),         -- az aláírandó objektum típusa (Irat/Dok)
                 @Objektum_Id          uniqueidentifier,    -- az aláítandó objektum EDOK azonosítója          
                 @ResultAlairasEASZkd  nvarchar(100)    OUTPUT, -- EASZ azonosító
                 @ResultTanusitvanyLn  nvarchar(100)    OUTPUT, -- tanúsítvány azonosító
                 @ResultAlairSzabaly   uniqueidentifier OUTPUT  -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az EDOK-ban nyilvántartott irat vagy külso alkalmazáshoz tartozó dokumentum aláírói között
    elojegyzett aláírási muvelet ellenorzése, az aláírás paramétereinek visszaadása.

-- Példa:
declare @Felhasznalo_Idc      uniqueidentifier,  @AlkalmazasKod       nvarchar(100),
        @Objektum_Idc         uniqueidentifier,  @ResultAlairasEASZkd nvarchar(100),
        @ResultTanusitvanyLn  nvarchar(100),     @ResultAlairSzabaly uniqueidentifier
select top 1 @Felhasznalo_Idc = FelhasznaloCsoport_Id_Alairo, @Objektum_Idc = Obj_id
 from EREC_IratAlairok
 where Id = '0DBAD60E-17B2-DD11-ACF3-005056C00008'
exec [dbo].[sp_PKI_ElojegyzettAlairas] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'Irat',              -- @ObjektumTip
     @Objektum_Idc,       -- @Objektum_Id
     @ResultAlairasEASZkd  output,
     @ResultTanusitvanyLn  output,
     @ResultAlairSzabaly  output
--
select * from EREC_IratAlairok where Allapot = '1'
 order by LetrehozasIdo desc
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod   nvarchar(16)
set     @HasznalatiMod = 'JegyzettAlairas'

set     @ResultAlairasEASZkd  = NULL
set     @ResultTanusitvanyLn  = NULL
set     @ResultAlairSzabaly   = NULL

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @ObjektumTip is NULL
   RAISERROR('Az aláírandó objektum típusának megadása kötelezo!',16,1)

if @Objektum_Id is NULL
   RAISERROR('Az aláírandó objektum azonosítójának megadása kötelezo!',16,1)

------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     NULL, --@AlairasSzabaly_Id
     NULL, --@AlairasKod
     @ObjektumTip,
     @Objektum_Id,       
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     NULL, --@FolyamatKod
     NULL, --@MuveletKod
     NULL, --@AlairoSzerep
     '0',  --@Titkositas
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @ResultAlairSzabaly OUTPUT

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------