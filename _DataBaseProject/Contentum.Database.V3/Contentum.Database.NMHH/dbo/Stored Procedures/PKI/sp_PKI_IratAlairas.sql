﻿create procedure [dbo].[sp_PKI_IratAlairas]    
                 @Felhasznalo_Id       uniqueidentifier,    -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod        nvarchar(100),       -- a külso alkalmazás kódja (egyébként üres)
                 @ObjektumTip          nvarchar(4),         -- az aláírandó objektum típusa (Irat/Dok)
                 @Objektum_Id          uniqueidentifier,    -- az aláítandó objektum EDOK azonosítója          
                 @FolyamatKod          nvarchar(100),       -- a folyamat azonosító kódja
                 @MuveletKod           nvarchar(100),       -- a muvelet azonosító kódja
                 @AlairoSzerep         nvarchar(64),        -- az aláíró szerep kódja
                 @ResultAlairasEASZkd  nvarchar(100)   OUTPUT, -- EASZ azonosító
                 @ResultTanusitvanyLn  nvarchar(100)   OUTPUT, -- Tanúsítvány azonosító
                 @ResultAlairSzabaly   uniqueidentifier OUTPUT -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az EDOK-ban nyilvántartott irat vagy külso alkalmazáshoz tartozó dokumentum (folyamat szerint)
	meghatározott aláírási muveletének ellenorzése, az aláírás paramétereinek visszaadása.

-- Példák:
declare @Felhasznalo_Idc     uniqueidentifier,  @AlkalmazasKod       nvarchar(100),
        @Objektum_Idc        uniqueidentifier,  @ResultAlairasEASZkd nvarchar(100),
        @ResultTanusitvanyLn nvarchar(100),     @ResultAlairSzabaly uniqueidentifier
select top 1 @Felhasznalo_Idc = convert(uniqueidentifier, '1ECFCE9F-E998-4EC5-99F4-A4DDE9608418'),
	   @Objektum_Idc = i.Obj_Id
 from EREC_IratAlairok i
 where Allapot = 1
 order by LetrehozasIdo desc
exec [dbo].[sp_PKI_IratAlairas] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'Irat',              -- @ObjektumTip
     @Objektum_Idc,       -- @Objektum_Id
     'IratJovahagyas',    -- @FolyamatKod
     'BelsoIratJovahagyas',  -- @MuveletKod
     2,                   -- @AlairoSzerep
     @ResultAlairasEASZkd output,
     @ResultTanusitvanyLn output,
     @ResultAlairSzabaly output
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod   nvarchar(16)

set     @HasznalatiMod = 'IratAlairas'

set     @ResultAlairasEASZkd = NULL
set     @ResultTanusitvanyLn = NULL
set     @ResultAlairSzabaly  = NULL

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @ObjektumTip is NULL
   RAISERROR('Az aláírandó objektum típusának megadása kötelezo!',16,1)

if @Objektum_Id is NULL
   RAISERROR('Az aláírandó objektum azonosítójának megadása kötelezo!',16,1)

if @FolyamatKod is NULL OR @MuveletKod is NULL
   RAISERROR('A folyamat paraméterek megadása kötelezo!',16,1)

------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     NULL, --@AlairasSzabaly_Id
     NULL, --@AlairasKod
     @ObjektumTip,
     @Objektum_Id,       
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     @FolyamatKod,
     @MuveletKod,
     @AlairoSzerep,
     '0',  --@Titkositas
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @ResultAlairSzabaly  OUTPUT 

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------