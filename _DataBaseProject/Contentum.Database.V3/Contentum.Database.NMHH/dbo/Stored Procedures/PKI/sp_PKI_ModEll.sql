﻿create procedure [dbo].[sp_PKI_ModEll]    
                 @Felhasznalo_Id        uniqueidentifier,
                 @MetaDefinicio_Id      uniqueidentifier,
				 @MetaDefinicioTipus    nvarchar(100),
                 @Folyamat_Id           uniqueidentifier,
                 @DokumentumMuvelet_Id  uniqueidentifier,
				 @IratSzerepJel         nvarchar(64),
				 @AlairoSzerep          nvarchar(64),
				 @Titkositas            char(1),
                 @AlairasSzabaly_Id     uniqueidentifier,
                 @HasznalatiMod         nvarchar(16),
				 @PKIMukodes            nvarchar(100),
                 @Org                   uniqueidentifier,
                 @EDOK                  char(1),
                 @ResultAlairasEASZkd   nvarchar(100)    OUTPUT, -- ha több van, ','-vel elválasztva
                 @ResultTanusitvanyLn   nvarchar(100)    OUTPUT, -- Tanúsítvány ujjlenyomat
                 @ResultAlairSzabaly    uniqueidentifier OUTPUT  -- Aláírási szabály azonosító
AS
/*
-- FELADATA:
	A megadott bemeno paraméterek szeinti aláírásadatok lekérdezése a Használati mód függvényében.
    Használati módok:
    JegyzettAlairas / IratAlairas / MuveletAlairas / DirektAlairas / SzabalyLista / AlairasLista
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @IsmetloSzabaly    uniqueidentifier,
		@SzabalySzint      nvarchar(64),
		@AlairasKod        nvarchar(10)

set     @SzabalySzint          = NULL
set     @ResultAlairasEASZkd   = NULL
set     @ResultTanusitvanyLn   = NULL
set     @ResultAlairSzabaly    = NULL

-----------------------------------
-- paraméter kiíratás
-----------------------------------
if @teszt = 1
begin
   print '----- sp_PKI_ModEll -----'
   print '@MetaDefinicio_Id     = '+ isnull( convert(varchar(50),@MetaDefinicio_Id),    'NULL')
   print '@MetaDefinicioTipus   = '+ isnull( convert(varchar(50),@MetaDefinicioTipus),  'NULL')
   print '@Folyamat_Id          = '+ isnull( convert(varchar(50),@Folyamat_Id),         'NULL')
   print '@DokumentumMuvelet_Id = '+ isnull( convert(varchar(50),@DokumentumMuvelet_Id),'NULL')
   print '@IratSzerepJel        = '+ isnull( convert(varchar(50),@IratSzerepJel),       'NULL')
   print '@AlairoSzerep         = '+ isnull( convert(varchar(50),@AlairoSzerep),        'NULL')
   print '@AlairasSzabaly_Id    = '+ isnull( convert(varchar(50),@AlairasSzabaly_Id),   'NULL')
   print '@HasznalatiMod        = '+ isnull( convert(varchar(50),@HasznalatiMod),       'NULL')
   print '@PKIMukodes           = '+ isnull( convert(varchar(50),@PKIMukodes),          'NULL')
   print '@EDOK                 = '+ isnull( convert(varchar(50),@EDOK),                'NULL')
end
--RAISERROR('Teszt_ModEll!',16,1)
------------------------------------
-- aláírás adatok munkatábla kreálás
------------------------------------
create table #AlairasParameterek
(
   AlairasTipus_Id   uniqueidentifier,
   AlairasKod        char(4),
   AlairasNev        nvarchar(100),
   AlairasMod        nvarchar(64),
   AlairasSzint      int,
   AlairasSzabaly_Id uniqueidentifier,
   EASZ_Id           uniqueidentifier,
   EASZKod           nvarchar(100),
   KivarasiIdo       int,
   KonfigTetel       nvarchar(10),
   Tanusitvany_Id    uniqueidentifier,
   Tarolas           nvarchar(64),
   Ujjlenyomat       nvarchar(100),
   IsmetloSzabaly_Id uniqueidentifier,
   Cn                int
)

---------------------------------------
-- Aláírási muvelet szabály lekérdezése
---------------------------------------
if @HasznalatiMod like '%Alairas'
begin

	---------------------------------------
	-- kijelölt szabály szerinti lekérdezés
	---------------------------------------
	if @HasznalatiMod = 'DirektAlairas'
	begin 

		insert into #AlairasParameterek
			  (AlairasTipus_Id,
			   AlairasKod,
			   AlairasNev,
			   AlairasMod,
			   AlairasSzint,
			   AlairasSzabaly_Id, Cn)
		select at.Id,
			   at.AlairasKod,
			   at.Nev,
			   at.AlairasMod,
			   at.AlairasSzint,
			   sz.Id, 1
		  from KRT_AlairasSzabalyok sz,
			   KRT_AlairasTipusok at
		 where sz.Id = @AlairasSzabaly_Id
		   and sz.AlairasTipus_Id = at.Id
		 
		if @@error <> 0 or @@rowcount <> 1
		   RAISERROR('#AlairasParameterek insert hiba1!',16,1)

	end -- kijelölt szabály szerinti lekérdezés

    else
	---------------------------------------
	-- muveleti szabály szerinti lekérdezés
	---------------------------------------
	begin

		-- szabályszint beállítás
		set @SzabalySzint = case when @MetaDefinicioTipus = 'EREC_Obj_MetaDefinicio' then '5'
								 else '0' end
		if @MetaDefinicio_Id is not null AND
		   @MetaDefinicioTipus = 'EREC_IratMetaDefinicio'
		begin 
			select @SzabalySzint = case when Irattipus is not NULL then '4'
										when Irattipus is NULL and EljarasiSzakasz is not NULL then '3'
										when Irattipus is NULL and EljarasiSzakasz is NULL and Ugytipus is not NULL then '2'
										else '1' end
			  from EREC_IratMetaDefinicio
			 where Id = @MetaDefinicio_Id

			if @@error <> 0
			   RAISERROR('Hiba az IratMetaDefinicio lekérdezésekor!',16,1)
		end

		if @HasznalatiMod = 'JegyzettAlairas'
			select @AlairasKod = at.AlairasKod
			  from KRT_AlairasSzabalyok sz,
				   KRT_AlairasTipusok at
			 where sz.Id = @AlairasSzabaly_Id
			   and sz.AlairasTipus_Id = at.Id

		-- aktuális szabály lekérdezése
		exec [dbo].[sp_PKI_Szabaly]
			 @MetaDefinicio_Id,
			 @MetaDefinicioTipus,
			 @DokumentumMuvelet_Id,
			 @AlairoSzerep,
			 @Titkositas,
			 @SzabalySzint,
			 @AlairasKod,
			 @HasznalatiMod,
			 @PKIMukodes,
			 @Org,
			 @EDOK,
			 @ResultAlairSzabaly output,
			 @IsmetloSzabaly output

		-- szabály adatok elmentése
		insert into #AlairasParameterek
			  (AlairasTipus_Id,
			   AlairasKod,
			   AlairasNev,
			   AlairasMod,
			   AlairasSzint,
			   AlairasSzabaly_Id,
			   IsmetloSzabaly_Id, Cn)
		select at.Id,
			   at.AlairasKod,
			   at.Nev,
			   at.AlairasMod,
			   at.AlairasSzint,
			   sz.Id,
			   @IsmetloSzabaly, 2
		  from KRT_AlairasSzabalyok sz,
			   KRT_AlairasTipusok at
		 where sz.Id = @ResultAlairSzabaly
		   and sz.AlairasTipus_Id = at.Id
		 
		if @@error <> 0 or @@rowcount <> 1
		   RAISERROR('#AlairasParameterek insert hiba2!',16,1)

	end -- muveleti szabály szerinti lekérdezés

end -- Aláírási muvelet szabály lekérdezése

---------------------------------------
-- Listás aláírás szabályok lekérdezése
---------------------------------------
if @HasznalatiMod like '%Lista'
begin

	declare	@DokumentumMuvelet  uniqueidentifier,
			@Szabaly_Id         uniqueidentifier

	-- szabályszint beállítás
	set @SzabalySzint = case when @MetaDefinicioTipus = 'EREC_Obj_MetaDefinicio' then '5'
                             else '0' end
	if @MetaDefinicioTipus = 'EREC_IratMetaDefinicio' AND
       @MetaDefinicio_Id is not null
	begin
		select @SzabalySzint = case when Irattipus is not NULL then '4'
									when Irattipus is NULL and EljarasiSzakasz is not NULL then '3'
									when Irattipus is NULL and EljarasiSzakasz is NULL and Ugytipus is not NULL then '2'
									else '1' end
		  from EREC_IratMetaDefinicio
		 where Id = @MetaDefinicio_Id

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba az IratMetaDefinicio lekérdezésekor!',16,1)
	end

	-- választható szabályok lekérdezése
	declare Szabaly cursor local for
	select distinct
		   sz.DokumentumMuvelet_Id,
		   sz.AlairoSzerep,
		   at.AlairasKod
	  from KRT_DokumentumMuveletek dm,
		   KRT_AlairasSzabalyok sz,
		   KRT_AlairasTipusok at
	 where dm.Folyamat_Id     = @Folyamat_Id
	   and dm.Id              = isnull(@DokumentumMuvelet_Id,dm.Id)
--	   and dm.AlairoSzerepjel = isnull(@IratSzerepJel,dm.AlairoSzerepjel)
	   and dbo.fn_Ervenyes(dm.ErvKezd,dm.ErvVege)='I'
	   and dm.Id              = sz.DokumentumMuvelet_Id
	   and sz.SzabalySzint   <= @SzabalySzint
	   and isnull(sz.AlairoSzerep,'_') = case when dm.MuveletKod = 'KozvetlenAlairas' then '_'
											  when dm.MuveletKod = 'PKISzunetelesAlairas' then '_'
											  else isnull(@AlairoSzerep,'_') end
	   and sz.Titkositas   <= @Titkositas
	   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'
	   and sz.AlairasTipus_Id = at.Id
	   and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
       and at.AlairasSzint    = case when @PKIMukodes = 'Nem' then 4 else at.AlairasSzint end

	open Szabaly
	fetch Szabaly into @DokumentumMuvelet, @AlairoSzerep, @AlairasKod

	while @@Fetch_Status = 0
		begin

			-- szabály lekérdezés
			exec [dbo].[sp_PKI_Szabaly]
				 @MetaDefinicio_Id,
				 @MetaDefinicioTipus,
				 @DokumentumMuvelet,
				 @AlairoSzerep,
				 @Titkositas,
				 @SzabalySzint,
				 @AlairasKod,
				 @HasznalatiMod,
				 @PKIMukodes,
				 @Org,
				 @EDOK,
				 @Szabaly_Id output,
				 @IsmetloSzabaly output

			-- szabály adatok elmentése
			if @Szabaly_Id is not null
			   begin
				   insert into #AlairasParameterek
					     (AlairasTipus_Id,
					      AlairasKod,
					      AlairasNev,
					      AlairasMod,
					      AlairasSzint,
						  AlairasSzabaly_Id,
						  IsmetloSzabaly_Id, Cn)
				   select at.Id,
						  at.AlairasKod,
						  at.Nev,
						  at.AlairasMod,
					      at.AlairasSzint,
						  sz.Id,
						  @IsmetloSzabaly, 3
					 from KRT_AlairasSzabalyok sz,
						  KRT_AlairasTipusok at
					where sz.Id              = @Szabaly_Id
					  and sz.AlairasTipus_Id = at.Id

				   if @@error <> 0
					  RAISERROR('#AlairasParameterek insert hiba3!',16,1)
			   end

			fetch Szabaly into @DokumentumMuvelet, @AlairoSzerep, @AlairasKod

		end -- cilkus

	close Szabaly
	deallocate Szabaly

end -- Listás aláírás szabályok lekérdezése

--------------------------------
-- Aláírás szabályok ellenorzése
--------------------------------
select @rowcount = count(*)
  from #AlairasParameterek

if @rowcount = 0
   RAISERROR('A megadott paraméterekkel nincs érvényes aláírási szabály!',16,1)

-------------------------------------------
-- elektronikus aláírás adatok feldolgozása
-------------------------------------------
if exists(select *
		    from #AlairasParameterek
		    where AlairasMod like('E_%'))
begin

	----------------------------------------------
	-- aláírói tanúsítvány létezésének ellenorzése
	----------------------------------------------
	select @rowcount = count(*)
	  from KRT_Tanusitvanyok tn,
		   KRT_TanusitvanyTipusok tt
	 where tn.Csoport_id          = @Felhasznalo_Id
	   and tn.TanusitvanyTipus_Id = tt.Id
	   and dbo.fn_Ervenyes(tn.ErvKezd,tn.ErvVege)='I'
	   and tt.Org                 = @Org
	   and tt.TanusitvanyTipus   <> 'TITKOS'
	   and dbo.fn_Ervenyes(tt.ErvKezd,tt.ErvVege)='I'
	   and (tn.Ervenytelenites is null or tn.Ervenytelenites > getdate())
	   and (tn.Visszavonas is null or tn.Visszavonas > getdate())
	    
	if @@error <> 0 
	   RAISERROR('Hiba a tanúsítványok lekérdezésekor!',16,1)

	if @rowcount = 0
	begin
		select @rowcount = count(*)
		  from KRT_Tanusitvanyok tn,
			   KRT_TanusitvanyTipusok tt
		 where tn.Csoport_id in (select Csoport_id
								   from KRT_Csoporttagok ct,
										KRT_Csoportok cs
								  where ct.Csoport_Id_Jogalany = @Felhasznalo_Id
									and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)='I'
									and ct.Csoport_Id          = cs.Id
									and cs.Org                 = @Org
									and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I')
		   and tn.TanusitvanyTipus_Id = tt.Id
		   and dbo.fn_Ervenyes(tn.ErvKezd,tn.ErvVege)='I'
		   and tt.Org                  = @Org
		   and tt.TanusitvanyTipus    <> 'TITKOS'
		   and dbo.fn_Ervenyes(tt.ErvKezd,tt.ErvVege)='I'
		   and (tn.Ervenytelenites is null or tn.Ervenytelenites > getdate())
		   and (tn.Visszavonas is null or tn.Visszavonas > getdate())

		if @rowcount = 0
		RAISERROR('Sem a felhasználóhoz, sem a csoportjához nincs hozzárendelve érvényes aláíró tanúsítvány!',16,1)
	end

	----------------------------------------------
	-- elektronikus aláírás paraméterek beállítása
	----------------------------------------------
	update #AlairasParameterek
	   set EASZ_Id        = ea.Id,
           EASZKod        = ea.EASZTipusKod,
           KivarasiIdo    = ea.KivarasiIdo,
		   KonfigTetel    = et.KonfigTetel,
		   Tanusitvany_Id = tn.Id,
		   Tarolas        = tn.Tarolas,
		   UjjLenyomat    = tn.UjjLenyomat
	  from KRT_Tanusitvanyok tn,
		   KRT_EASZTanusitvanyok et,
		   KRT_EASZTipus ea,
		   #AlairasParameterek at
	 where tn.Csoport_id in (select Csoport_id
							   from KRT_Csoporttagok ct,
									KRT_Csoportok cs
							  where ct.Csoport_Id_Jogalany = @Felhasznalo_Id
								and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)='I'
								and ct.Csoport_Id          = cs.Id
								and cs.Org                 = @Org
								and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I')
	   and (tn.Ervenytelenites is null or tn.Ervenytelenites > getdate())
	   and (tn.Visszavonas is null or tn.Visszavonas > getdate())
	   and dbo.fn_Ervenyes(tn.ErvKezd,tn.ErvVege)='I'
	   and tn.Id              = et.Tanusitvany_Id
	   and dbo.fn_Ervenyes(et.ErvKezd,et.ErvVege)='I'
	   and et.EASZ_Id         = ea.Id
	   and ea.Org             = @Org
	   and dbo.fn_Ervenyes(ea.ErvKezd,ea.ErvVege)='I'
	   and at.AlairasTipus_Id = ea.AlairasTipus_Id

	select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('#AlairasParameterek update hiba!',16,1)
	if @PKIMukodes <> 'Nem' AND @HasznalatiMod like '%Alairas' AND @rowcount = 0
	   RAISERROR('A felhasználó nem rendelkezik megfelelo tanúsítvánnyal az eloírt aláírási muvelethez!',16,1)

end -- elektronikus aláírás adatok feldolgozása

---------------------------
-- output paraméter kimenet
---------------------------
if @HasznalatiMod like '%Alairas'
begin

	-- output paraméter kimenet
	DECLARE @C_EASZKod nvarchar(100),
			@C_Tanusitvany uniqueidentifier,
			@C_Szabaly uniqueidentifier
	SET     @ResultAlairasEASZkd = ''

	DECLARE ALAIRAS CURSOR LOCAL FOR
	 SELECT EASZKod, Tanusitvany_Id, AlairasSzabaly_Id
	   FROM #AlairasParameterek
	  WHERE AlairasMod <> case @HasznalatiMod when 'SzabalyLista' then '_' 
							   else 'M_UTO' end
	  ORDER BY AlairasKod

	OPEN ALAIRAS
	FETCH ALAIRAS INTO @C_EASZKod, @C_Tanusitvany, @C_Szabaly
	WHILE @@Fetch_Status = 0
		BEGIN
			if (@@ERROR <> 0)
			   RAISERROR('Hiha az sp_PKI_ModEll kimeneti adatok képzésénél2!',16,1)

			select @ResultAlairasEASZkd = 
				   case when @ResultAlairasEASZkd = '' and @C_EASZKod is not NULL then 
							 @ResultAlairasEASZkd + rtrim(@C_EASZKod)
						when @C_EASZKod is not NULL then 
							 @ResultAlairasEASZkd + ',' + rtrim(@C_EASZKod)
						else @ResultAlairasEASZkd end,
				   @ResultTanusitvanyLn = isnull(@ResultTanusitvanyLn, @C_Tanusitvany),
				   @ResultAlairSzabaly = isnull(@ResultAlairSzabaly, @C_Szabaly)
           
			FETCH ALAIRAS INTO @C_EASZKod, @C_Tanusitvany, @C_Szabaly
		END

	CLOSE ALAIRAS
	DEALLOCATE ALAIRAS

end -- output paraméter kimenet

-----------------------
-- output lista kimenet
-----------------------
select row_number() over (order by dm.AlairoSzerepjel, dm.MuveletKod, sz.Sorrend, ap.AlairasSzint, ap.AlairasKod) Sorsz,
	   @PKIMukodes PKIMukodes,
	   ap.AlairasNev,
	   ap.AlairasMod,
	   ap.AlairasSzint,
	   ap.AlairasSzabaly_Id,
	   sz.AlairoSzerep,
	   ap.IsmetloSzabaly_Id,
	   sz.SzabalyTipus,
	   et.EASZTipusKod EASZ,
       ap.KivarasiIdo,
       ap.KonfigTetel,
	   ap.Tanusitvany_Id,
	   tn.Ujjlenyomat TanLenyomat,
	   al.Id Alairo_Id,
       substring(al.Nev,1,100) AlairoNev,
       substring(cs.Nev,1,100) AlairasTulajdonos,
       convert(nvarchar(7),case when ap.Tarolas in('BALE','LTAR') then 'Kliens'
                                else 'Szerver' end) Kivitelezes,
       convert(char(1),case when @HasznalatiMod like '%Alairas' then 1 else 0 end) DefSzabaly,
       ap.Cn
  from #AlairasParameterek ap
	   inner join KRT_AlairasSzabalyok sz on ap.AlairasSzabaly_Id = sz.Id
	   inner join KRT_DokumentumMuveletek dm on sz.DokumentumMuvelet_Id = dm.Id
 	   inner join KRT_Tanusitvanyok tn on ap.Tanusitvany_Id = tn.Id
       inner join KRT_EASZTipus et on ap.EASZ_Id = et.Id
	   inner join KRT_Csoportok cs on tn.Csoport_Id = cs.Id
	   inner join KRT_Csoportok al on al.Id = @Felhasznalo_Id
where ap.AlairasMod like('E_%')
  and ap.EASZ_Id is not NULL

union
select 10+row_number() over (order by dm.AlairoSzerepjel, dm.MuveletKod, sz.Sorrend, ap.AlairasSzint, ap.AlairasKod) Sorsz,
	   @PKIMukodes PKIMukodes,
	   ap.AlairasNev,
	   ap.AlairasMod,
	   ap.AlairasSzint,
	   ap.AlairasSzabaly_Id,
	   sz.AlairoSzerep,
	   ap.IsmetloSzabaly_Id,
	   sz.SzabalyTipus,
	   convert(nvarchar(100),NULL) EASZ,
       ap.KivarasiIdo,
       ap.KonfigTetel,
	   convert(uniqueidentifier,NULL) Tanusitvany_Id,
	   convert(nvarchar(100),NULL) TanLenyomat,
	   @Felhasznalo_Id Alairo_Id,
       substring(cs.Nev,1,100) AlairoNev,
       substring(cs.Nev,1,100) AlairasTulajdonos,
       convert(nvarchar(7),NULL) Kivitelezes,
       convert(char(1),case when @HasznalatiMod like '%Alairas' then 1 else 0 end) DefSzabaly,
       ap.Cn
  from #AlairasParameterek ap
	   inner join KRT_AlairasSzabalyok sz on ap.AlairasSzabaly_Id = sz.Id
	   inner join KRT_DokumentumMuveletek dm on sz.DokumentumMuvelet_Id = dm.Id
	   inner join KRT_Csoportok cs on cs.Id = @Felhasznalo_Id
where ap.AlairasMod like('M_%')
 order by Sorsz --dm.AlairoSzerepjel, dm.MuveletKod, sz.Sorrend, ap.AlairasSzint, ap.AlairasKod

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------