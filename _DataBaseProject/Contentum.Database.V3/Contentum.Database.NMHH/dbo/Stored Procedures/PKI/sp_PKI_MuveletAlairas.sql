﻿create procedure [dbo].[sp_PKI_MuveletAlairas]    
                 @Felhasznalo_Id       uniqueidentifier,    -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod        nvarchar(100),       -- a külso alkalmazás kódja (egyébként üres)
                 @FolyamatKod          nvarchar(100),       -- a folyamat azonosító kódja
                 @MuveletKod           nvarchar(100),       -- a muvelet azonosító kódja
                 @AlairoSzerep         nvarchar(64),        -- az aláíró szerep kódja
                 @Titkositas           char(1),             -- a titkosítás jele 0/1
                 @ResultAlairasEASZkd  nvarchar(100)   OUTPUT, -- EASZ azonosító
                 @ResultTanusitvanyLn  nvarchar(100)   OUTPUT, -- Tanúsítvány azonosító
                 @ResultAlairSzabaly   uniqueidentifier OUTPUT -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az EDOK	nyilvántartásban (még) nem szereplo dokumentum (folyamat szerint) meghatározott aláírási
    muveletének ellenorzése, az aláírás paramétereinek visszaadása.

-- Példák:
declare @Felhasznalo_Idc     uniqueidentifier,  @AlkalmazasKod       nvarchar(100),
        @ResultAlairasEASZkd nvarchar(100),     @ResultTanusitvanyLn nvarchar(100),
        @ResultAlairSzabaly uniqueidentifier
select @Felhasznalo_Idc = convert(uniqueidentifier, '5B7FEE68-CC7A-42D5-9D58-95794E5129F4')
exec [dbo].[sp_PKI_MuveletAlairas] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'EmailFogadas',      -- @FolyamatKod
     'KulsoEmailFogadas', -- @MuveletKod
     NULL,                -- @AlairoSzerep
     '0',                 -- @Titkositas
     @ResultAlairasEASZkd output,
     @ResultTanusitvanyLn output,
     @ResultAlairSzabaly output
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod   nvarchar(16)

set     @HasznalatiMod = 'MuveletAlairas'

set     @ResultAlairasEASZkd = NULL
set     @ResultTanusitvanyLn = NULL
set     @ResultAlairSzabaly  = NULL

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @FolyamatKod is NULL OR @MuveletKod is NULL
   RAISERROR('A folyamat paraméterek megadása kötelezo!',16,1)

------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     NULL, --@AlairasSzabaly_Id
     NULL, --@AlairasKod
     NULL, --@ObjektumTip
     NULL, --@Objektum_Id
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     @FolyamatKod,
     @MuveletKod,
     @AlairoSzerep,
	 @Titkositas,
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @ResultAlairSzabaly  OUTPUT 

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------