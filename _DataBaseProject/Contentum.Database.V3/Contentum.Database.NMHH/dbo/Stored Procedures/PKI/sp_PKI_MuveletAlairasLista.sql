﻿create procedure [dbo].[sp_PKI_MuveletAlairasLista]    
                 @Felhasznalo_Id       uniqueidentifier,    -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod        nvarchar(100),       -- a külso alkalmazás kódja (egyébként üres)
                 @FolyamatKod          nvarchar(100),       -- a folyamat azonosító kódja
                 @MuveletKod           nvarchar(100),       -- a muvelet azonosító kódja
                 @AlairoSzerep         nvarchar(64),        -- az aláíró szerep kódja
                 @Titkositas           char(1),             -- a titkosítás jele 0/1
                 @ResultAlairasEASZkd  nvarchar(100)   OUTPUT, -- EASZ azonosító
                 @ResultTanusitvanyLn  nvarchar(100)   OUTPUT, -- Tanúsítvány azonosító
                 @ResultAlairSzabaly   uniqueidentifier OUTPUT -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az EDOK	nyilvántartásban (még) nem szereplo dokumentum (folyamat szerint) meghatározott aláírási
    muveletének ellenorzése, a felhasználó számára megengedett aláírások paramétereinek visszaadása
    a megadott muvelet szerinti aláírás alapértelmezettként való megjelölésével.

-- Példa:
declare @Felhasznalo_Idc     uniqueidentifier,  @AlkalmazasKod       nvarchar(100),
        @ResultAlairasEASZkd nvarchar(100),     @ResultTanusitvanyLn nvarchar(100),
        @ResultAlairSzabaly uniqueidentifier
select @Felhasznalo_Idc = convert(uniqueidentifier, '5B7FEE68-CC7A-42D5-9D58-95794E5129F4')
exec [dbo].[sp_PKI_MuveletAlairasLista] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'EmailFogadas',      -- @FolyamatKod
     'KulsoEmailFogadas', -- @MuveletKod
     NULL,                -- @AlairoSzerep
     '0',                 -- @Titkositas
     @ResultAlairasEASZkd output,
     @ResultTanusitvanyLn output,
     @ResultAlairSzabaly output
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod     nvarchar(16),
        @AlairasSzabaly_Id uniqueidentifier

set     @HasznalatiMod = 'MuveletAlairas'

set     @ResultAlairasEASZkd = NULL
set     @ResultTanusitvanyLn = NULL
set     @ResultAlairSzabaly  = NULL

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @FolyamatKod is NULL OR @MuveletKod is NULL
   RAISERROR('A folyamat paraméterek megadása kötelezo!',16,1)

-- kimeneti temp tábla létrehozása
create table #KimenoAlairasAdatok
(
   Sorsz             int,
   PKIMukodes        nvarchar(100),
   AlairasNev        nvarchar(100),
   AlairasMod        nvarchar(64),
   AlairasSzint      int,
   AlairasSzabaly_Id uniqueidentifier,
   AlairoSzerep      nvarchar(100),
   IsmetloSzabaly_Id uniqueidentifier,
   SzabalyTipus      nvarchar(64),
   EASZ              nvarchar(100),
   KivarasiIdo       int,
   KonfigTetel       nvarchar(10),
   Tanusitvany_Id	 uniqueidentifier,
   TanLenyomat       nvarchar(100),
   Alairo_Id         uniqueidentifier,
   AlairoNev         nvarchar(100),
   AlairasTulajdonos nvarchar(100),
   Kivitelezes       nvarchar(7),
   DefSzabaly        char(1),
   Cn                int
)
------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
insert #KimenoAlairasAdatok
exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     NULL, --@AlairasSzabaly_Id
     NULL, --@AlairasKod
     NULL, --@ObjektumTip
     NULL, --@Objektum_Id
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     @FolyamatKod,
     @MuveletKod,
     @AlairoSzerep,
	 @Titkositas,
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @AlairasSzabaly_Id   OUTPUT 

---------------------------------------------------------
-- bovítés a felhasználó számára megengedett aláírásokkal
---------------------------------------------------------
if @ResultAlairasEASZkd is not NULL
begin

	set     @HasznalatiMod = 'AlairasLista'

	-- általános elofeldolgozó indítása
	declare @ResultMetaDef       uniqueidentifier,
			@ResultMetaDefTip    nvarchar(100),
			@ResultFolyamat      uniqueidentifier,
			@ResultDokuMuvelet   uniqueidentifier,
			@ResultIratSzerepJel nvarchar(64),
			@ResultAlairoSzerep  nvarchar(64),
			@ResultOrg           uniqueidentifier,
			@ResultEDOK          char(1),
			@ResultPKIMukodes    nvarchar(100)

	exec sp_PKI_ParEll
		 @Felhasznalo_Id,
		 @AlkalmazasKod,
		 NULL, --@AlairasSzabaly_Id,
		 NULL, --@AlairasKod,
		 NULL, --@Irat_Id,
		 NULL, --@Dokumentum_Id,
		 NULL, --@UgykorKod,
		 NULL, --@UgyTipusNev,
		 NULL, --@IratTipusNev,
		 NULL, --@EljarasiSzakasz,
		 NULL, --@FolyamatKod,
		 NULL, --@MuveletKod,
		 @AlairoSzerep,
		 @Titkositas,
		 @HasznalatiMod,
		 @ResultMetaDef       OUTPUT,
		 @ResultMetaDefTip    OUTPUT,
		 @ResultFolyamat      OUTPUT,
		 @ResultDokuMuvelet   OUTPUT,
		 @ResultIratSzerepJel OUTPUT,
		 @ResultAlairoSzerep  OUTPUT,
		 @ResultAlairSzabaly  OUTPUT,
		 @ResultOrg           OUTPUT,
		 @ResultEDOK          OUTPUT,
		 @ResultPKIMukodes    OUTPUT

	-- elektronikus aláírás adatok lekérdezése
    insert #KimenoAlairasAdatok
	exec sp_PKI_ModEll
		 @Felhasznalo_Id,
		 @ResultMetaDef,       --@MetaDefinicio_Id
		 @ResultMetaDefTip,    --@MetaDefinicioTipus
		 @ResultFolyamat,      --@Folyamat_Id
		 @ResultDokuMuvelet,   --@DokumentumMuvelet_Id
		 @ResultIratSzerepJel, --@IratSzerepJel
		 @ResultAlairoSzerep,  --@AlairoSzerep
		 @Titkositas,
		 @AlairasSzabaly_Id,
		 @HasznalatiMod,
		 @ResultPKIMukodes,
		 @ResultOrg,
		 @ResultEDOK,
		 @ResultAlairasEASZkd OUTPUT,
		 @ResultTanusitvanyLn OUTPUT,
		 @ResultAlairSzabaly  OUTPUT 

	-- felesleges tételek törlése
	delete #KimenoAlairasAdatok
	  from (select AlairasNev from #KimenoAlairasAdatok
             where DefSzabaly = '1'
           ) ka
	 where #KimenoAlairasAdatok.DefSzabaly = '0'
       and (#KimenoAlairasAdatok.AlairasNev = ka.AlairasNev or
            #KimenoAlairasAdatok.AlairasMod = 'E_DET')

end

-- kimenet
set @ResultAlairSzabaly = @AlairasSzabaly_Id
select * from #KimenoAlairasAdatok
 order by Sorsz

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------