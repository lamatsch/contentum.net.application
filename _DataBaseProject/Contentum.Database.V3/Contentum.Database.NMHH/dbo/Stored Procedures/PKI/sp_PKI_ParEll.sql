﻿create procedure [dbo].[sp_PKI_ParEll]    
                 @Felhasznalo_Id      uniqueidentifier,
                 @AlkalmazasKod       nvarchar(100) = null,
                 @AlairasSzabaly_Id   uniqueidentifier = null,
                 @AlairasKod          char(4) = null,
                 @Irat_Id             uniqueidentifier = null,
                 @Dokumentum_Id       uniqueidentifier = null,                 
                 @UgykorKod           nvarchar(10) = null,
                 @UgyTipusNev         nvarchar(400) = null,              
                 @EljarasiSzakasz     nvarchar(64) = null,
                 @IratTipusNev        nvarchar(400) = null,
                 @FolyamatKod         nvarchar(100) = null,
                 @MuveletKod          nvarchar(100) = null,
                 @AlairoSzerep        nvarchar(64)  = null,
                 @Titkositas          char(1)       = '0',
                 @HasznalatiMod       nvarchar(16), 
                 @ResultMetaDef       uniqueidentifier OUTPUT,
				 @ResultMetaDefTip    nvarchar(100)    OUTPUT,
                 @ResultFolyamat      uniqueidentifier OUTPUT,
                 @ResultDokuMuvelet   uniqueidentifier OUTPUT,
                 @ResultIratSzerepJel nvarchar(64)     OUTPUT,
                 @ResultAlairoSzerep  nvarchar(64)     OUTPUT,
                 @ResultAlairSzabaly  uniqueidentifier OUTPUT,
                 @ResultOrg           uniqueidentifier OUTPUT,
                 @ResultEDOK          char(1)          OUTPUT,
                 @ResultPKIMukodes    nvarchar(100)    OUTPUT
AS
/*
-- FELADATA:
	Az aláírás általános bemeno paramétereinek elenorzése, az aláírás módját meghatározó
    adatok visszaadása.
    Használati módok:
    JegyzettAlairas / IratAlairas / MuveletAlairas / DirektAlairas / SzabalyLista / AlairasLista
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

-- munkaváltozók
declare @Tipus             nvarchar(64),
        @UserNev           nvarchar(100),
        @Nev               nvarchar(100),
        @Alkalmazas_Id     uniqueidentifier,
        @EDOKAlkalmazas_Id uniqueidentifier,
        @AlairasMod        nvarchar(64),
        @IratAlairok_Id    uniqueidentifier,
        @AlairandoObj_Id   uniqueidentifier,
        @IratAlairasMod    nvarchar(64),
        @AlairasSorrend    int,
        @AlairasSzint      int,
        @SzabalySzint      nvarchar(64),
        @AlairoSzerepJel   nvarchar(64)

select @ResultDokuMuvelet   = NULL,
       @ResultMetaDef       = NULL,
       @ResultAlairoSzerep  = @AlairoSzerep,
       @ResultAlairSzabaly  = NULL,
       @AlairandoObj_Id     = NULL,
       @IratAlairok_Id      = NULL,
       @AlairasSzint        = NULL,
       @SzabalySzint        = NULL,
       @ResultIratSzerepJel = NULL

--------------------------------
-- input paraméterek kiiratása
--------------------------------
if @teszt = 1
begin
   print '----- sp_PKI_ParEll -----'
   print '@Felhasznalo_Id    = '+ isnull( convert(varchar(50),@Felhasznalo_Id),   'NULL')
   print '@AlkalmazasKod     = '+ isnull( convert(varchar(50),@AlkalmazasKod),    'NULL')
   print '@AlairasSzabaly_Id = '+ isnull( convert(varchar(50),@AlairasSzabaly_Id),'NULL')
   print '@AlairasKod        = '+ isnull( convert(varchar(50),@AlairasKod),       'NULL')
   print '@Irat_Id           = '+ isnull( convert(varchar(50),@Irat_Id),          'NULL')
   print '@Dokumentum_Id     = '+ isnull( convert(varchar(50),@Dokumentum_Id),    'NULL')
   print '@UgykorKod         = '+ isnull( convert(varchar(50),@UgykorKod),        'NULL')
   print '@UgyTipusNev       = '+ isnull( convert(varchar(50),@UgyTipusNev),      'NULL')
   print '@EljarasiSzakasz   = '+ isnull( convert(varchar(50),@EljarasiSzakasz),  'NULL')
   print '@IratTipusNev      = '+ isnull( convert(varchar(50),@IratTipusNev),     'NULL')
   print '@FolyamatKod       = '+ isnull( convert(varchar(50),@FolyamatKod),      'NULL')
   print '@MuveletKod        = '+ isnull( convert(varchar(50),@MuveletKod),       'NULL')
   print '@AlairoSzerep      = '+ isnull( convert(varchar(50),@AlairoSzerep),     'NULL')
   print '@Titkositas        = '+ isnull( convert(varchar(50),@Titkositas),       'NULL')
   print '@HasznalatiMod     = '+ isnull( convert(varchar(50),@HasznalatiMod),    'NULL')
end

--------------------------
-- paraméterezés vizsgálat
--------------------------
-- felhasználó
if @Felhasznalo_Id is NULL
   -- RAISERROR('[5xxxx]',16,1)
   RAISERROR('@Felhasznalo_Id paraméter nem lehet NULL!',16,1)

-- használati mód
if @HasznalatiMod not in
   ('JegyzettAlairas','IratAlairas','MuveletAlairas','DirektAlairas','SzabalyLista','AlairasLista')
   RAISERROR('Hibás a @HasznalatiMod paraméter megadása!',16,1)

-- használati mód
if @Titkositas not in ('0','1')
   RAISERROR('Hibás a @Titkositas paraméter megadása!',16,1)

-- irat ellenorzés
if @HasznalatiMod in('IratAlairas','DirektAlairas','SzabalyLista') AND
   (@Irat_Id is NULL AND @Dokumentum_Id is NULL)
   RAISERROR('A kijelölt aláírási funcióhoz az irat megadása kötelezo!',16,1)

-- muvelethez kötött aláírás
if @HasznalatiMod = 'MuveletAlairas' AND (@FolyamatKod is NULL OR @MuveletKod is NULL)
   RAISERROR('Az aláírást kezdeményezo muvelet paraméterezése hiányos!',16,1)

-- direkt aláírási fukció
if @HasznalatiMod like 'Direkt%' AND @AlairasKod is NULL 
   RAISERROR('A direkt aláírási funkcióhoz az aláírás típusának megadása kötelezo!',16,1)
if @HasznalatiMod not like 'Direkt%' AND @AlairasKod is not NULL 
   RAISERROR('Az aláírás típusát csak direkt aláírási funkciónál lehet megadni!',16,1)

-- lista paraméterek
if @HasznalatiMod like '%Lista' AND @MuveletKod is not NULL AND @FolyamatKod is NULL
    RAISERROR('Hiányos a folyamat paraméterezése!',16,1)

-- irattípus paraméterek
if @IratTipusNev is NOT NULL AND (@UgykorKod is NULL OR @UgyTipusNev is NULL)
   RAISERROR('Az irattípus megállapításához hiányos a paraméterezés!',16,1)

-- ügytípus paraméterek
if @UgyTipusNev is NOT NULL AND @UgykorKod is NULL
   RAISERROR('Az ügytípus megállapításához hiányos a paraméterezés!',16,1)

--------------------------
-- felhasználó ellenorzése
--------------------------
-- felhasználó létezésének ellenorzése
select @Nev = NULL, @Tipus = NULL,  @ResultOrg = NULL
select @Nev = Nev,  @Tipus = Tipus, @ResultOrg = Org
  from KRT_Csoportok
 where Id = @Felhasznalo_Id
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
    
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('KRT_Felhasznalok lekerdezes hiba!',16,1)
if @rowcount = 0
   RAISERROR('A megadott azonosítóval nem létezik érvényes felhasználó!',16,1)

-- a felhasználó paramétereinek kiiratása
if @teszt = 1
   print '@Nev = '       + isnull(@Nev, 'NULL') + ', ' +
         '@Tipus = '     + isnull(@Tipus, 'NULL') + ', ' +
         '@ResultOrg = ' + isnull(convert(varchar(50),@ResultOrg),'NULL')

-----------------------------------
-- PKI_INTEGRACIO paraméter kezelés
-----------------------------------
select @ResultPKIMukodes = Ertek
  from KRT_Parameterek
 where Nev = 'PKI_INTEGRACIO'
   and Ertek in('Igen','Nem','Szunetel') 
   and Org = @ResultOrg
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
   
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('Rendszer paraméter lekerdezes hiba!',16,1)
if @rowcount <> 1
   RAISERROR('A PKI_INTEGRACIO (Igen/Nem/Szunetel) rendszerparaméter nincs helyesen beállítva!',16,1)
if @ResultPKIMukodes <> 'Igen'AND @Titkositas = '1'
   RAISERROR('Az aktuális rendszerbeállítás szerint titkosítás nem lehetséges!',16,1)

-------------------------
-- alkalmazás ellenorzése
-------------------------
-- EDOK alkalmazás lekérdezése
select @EDOKAlkalmazas_Id = Id,
	   @ResultEDOK = 'I'
  from KRT_Alkalmazasok
 where Kod = 'eRecord'
   --and Org = @ResultOrg
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
   
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('KRT_Alkalmazasok lekérdezés hiba1!',16,1)
if @rowcount = 0
   RAISERROR('Az eRecord alkalmazás nem létezik',16,1)
if @rowcount > 1
   RAISERROR('Az eRecord alkalmazás többször definiált',16,1)

-- alkalmazás azonosító inicializálása
if @AlkalmazasKod is null OR
   @AlkalmazasKod = 'eRecord'
   set @Alkalmazas_Id = @EDOKAlkalmazas_Id
else
begin
	-- külso alkalmazás lekérdezése
	select @Alkalmazas_Id = Id,
		   @ResultEDOK = 'N'
	  from KRT_Alkalmazasok
	 where Kod = @AlkalmazasKod
	   --and Org = @ResultOrg
	   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
	   
	select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('KRT_Alkalmazasok lekérdezés hiba2!',16,1)
	if @rowcount = 0
	   RAISERROR('A megadott alkalmazás nem létezik',16,1)
	if @rowcount > 1
	   RAISERROR('A megadott alkalmazás többször definiált',16,1)
end

------------------------------
-- aláírás szabály ellenorzése
------------------------------
if @AlairasSzabaly_Id is not null
begin

	select @AlairasKod   = at.AlairasKod,
		   @AlairasMod   = at.AlairasMod,
		   @AlairasSzint = at.AlairasSzint
	  from KRT_AlairasSzabalyok sz,
		   KRT_AlairasTipusok at
	 where sz.Id = @AlairasSzabaly_Id
	   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'
	   and sz.AlairasTipus_Id = at.Id
	   and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
print ('Kod2='+isnull(@AlairasKod,'_'))
	   
	select @error = @@error, @rowcount = @@rowcount 
	if @@error <> 0
	   RAISERROR('KRT_AlairasSzabalyok lekérdezés hiba!',16,1)
	if @rowcount = 0
	   RAISERROR('A megadott típusú érvényes aláírás szabály nem létezik!',16,1)
	if @ResultPKIMukodes = 'Nem' AND @HasznalatiMod = 'DirektAlairas' AND @AlairasMod like 'E_%'
	   RAISERROR('Az aktuális rendszerbeállítás szerint elektronikus aláírás nem lehetséges!',16,1)

end -- Aláírás szabály ellenorzése

--------------------------------
-- közvetlen aláírás ellenorzése
--------------------------------
if @ResultAlairSzabaly is NULL AND
   @AlairasKod is not null
begin

	select @ResultAlairSzabaly = sz.Id,
		   @AlairasMod         = at.AlairasMod,
		   @AlairasSzint       = at.AlairasSzint
	  from KRT_AlairasTipusok at,
 		   KRT_AlairasSzabalyok sz
	 where at.AlairasKod           = @AlairasKod
	   and at.Org                  = @ResultOrg
	   and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
	   and at.id                   = sz.AlairasTipus_Id
	   and sz.SzabalySzint         = '0'  -- Alapszabály
	   and sz.Titkositas           = @Titkositas
	   and sz.DokumentumMuvelet_Id = (select dm.Id from KRT_DokumentumMuveletek dm
									   where dm.MuveletKod = 'KozvetlenAlairas'
										 and dbo.fn_Ervenyes(dm.ErvKezd,dm.ErvVege)='I')
	   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'

	select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('Hiba a KRT_AlairasTipusok lekérdezésnél1!',16,1)
	if @rowcount = 0
	   RAISERROR('Az aláírás típushoz nem tartozik közvetlen módú érvényes aláírás szabály!',16,1)
	if @rowcount > 1
	   RAISERROR('Az aláírás típussal többszörösen definiált direkt aláírás szabály!',16,1)
	if @ResultPKIMukodes = 'Nem' AND @HasznalatiMod = 'DirektAlairas' AND @AlairasMod like 'E_%'
	   RAISERROR('Az aktuális rendszerbeállítás szerint elektronikus aláírás nem lehetséges!',16,1)

end -- közvetlen aláírás ellenorzése

-----------------------------
-- irat adatainak ellenorzése
-----------------------------
if @Irat_Id is not NULL
begin 

    -- Irat létezés ellenorzése
    select @AlairandoObj_Id     = ir.Id,
		   @ResultMetaDef       = im.Id,
           @ResultMetaDefTip    = 'EREC_IratMetaDefinicio',
		   @ResultIratSzerepJel = case when ir.PostazasIranya   = '0' then '1' -- belso
									   when ir.KiadmanyozniKell = '1' then '1' -- kiadmányozandó
									   else '_' end,                           -- egyéb
           @SzabalySzint      = '4'  -- irattípus szint
	  from EREC_IraIratok ir
           left outer join EREC_IratMetaDefinicio im
             on ir.IratMetaDef_Id = im.Id
            and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
	 where ir.Id = @Irat_Id
	   and dbo.fn_Ervenyes(ir.ErvKezd,ir.ErvVege)='I'
  
    select @error = @@error, @rowcount = @@rowcount 
    if @error <> 0
	   RAISERROR('Hiba az EREC_IraIratok olvasásánál!',16,1)
    if @rowcount = 0
	   RAISERROR('A megadott azonosítójú irat nem létezik!',16,1)
   
end -- irat adatainak ellenorzése

------------------------------
-- iratbesorolás meghatározása
------------------------------
if @ResultPKIMukodes <> 'Nem' AND
   @Irat_Id is not NULL AND
   @ResultMetaDef is NULL
begin 

    -- iratbesorolás meghatározása ügyiratadatok alapján
    begin

       -- lekérdezés irattípus szerint
	   select @ResultMetaDef    = im.id,
              @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
              @SzabalySzint     = '4'  -- irattípus szint
		 from EREC_IratMetaDefinicio im,
              EREC_IraIratok ir,
              EREC_UgyUgyiratDarabok ud,
              EREC_UgyUgyiratok ui
		where ir.Id                  = @Irat_Id
          and ir.UgyUgyiratDarab_Id  = ud.id
          and ud.UgyUgyirat_id       = ui.id
          and ui.IraIrattariTetel_Id = im.Ugykor_Id
		  and ui.UgyTipus            = im.Ugytipus
          and ud.EljarasiSzakasz     = im.EljarasiSzakasz
          and ir.Irattipus           = im.Irattipus
          and im.Org                 = @ResultOrg
		  and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'

	   select @error = @@error, @rowcount = @@rowcount 
	   if @error <> 0
		  RAISERROR('Hiba az EREC_IratMetaDefinicio lekérdezésekor4!',16,1)
	   if @rowcount > 1
		  RAISERROR('A megadott azonosítójú irathoz többszörös irattípus definíció tartozik!',16,1)

       -- lekérdezés eljárási szakasz szerint
       if @ResultMetaDef is NULL
       begin

		   select @ResultMetaDef    = im.id,
				  @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
                  @SzabalySzint     = '3'  -- eljárási szakasz szint
			 from EREC_IratMetaDefinicio im,
				  EREC_IraIratok ir,
				  EREC_UgyUgyiratDarabok ud,
				  EREC_UgyUgyiratok ui
			where ir.Id                  = @Irat_Id
			  and ir.UgyUgyiratDarab_Id  = ud.id
			  and ud.UgyUgyirat_id       = ui.id
			  and ui.IraIrattariTetel_Id = im.Ugykor_Id
			  and ui.UgyTipus            = im.Ugytipus
			  and ud.EljarasiSzakasz     = im.EljarasiSzakasz
              and im.Org                 = @ResultOrg
			  and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
              and im.Irattipus is NULL

		   select @error = @@error, @rowcount = @@rowcount 
		   if @error <> 0
			  RAISERROR('Hiba az EREC_IratMetaDefinicio lekérdezésekor3!',16,1)
		   if @rowcount > 1
			  RAISERROR('A megadott azonosítójú irathoz többszörös eljárási szakasz definíció tartozik!',16,1)

       end -- lekérdezés eljárási szakasz szerint
	    
       -- lekérdezés ügytípus szerint
       if @ResultMetaDef is NULL
       begin

		   select @ResultMetaDef    = im.id,
				  @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
                  @SzabalySzint     = '2'  -- ügytípus szint
			 from EREC_IratMetaDefinicio im,
				  EREC_IraIratok ir,
				  EREC_UgyUgyiratDarabok ud,
				  EREC_UgyUgyiratok ui
			where ir.Id                  = @Irat_Id
			  and ir.UgyUgyiratDarab_Id  = ud.id
			  and ud.UgyUgyirat_id       = ui.id
			  and ui.IraIrattariTetel_Id = im.Ugykor_Id
			  and ui.UgyTipus            = im.Ugytipus
              and im.Org                 = @ResultOrg
			  and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
              and im.EljarasiSzakasz is NULL
              and im.Irattipus is NULL

		   select @error = @@error, @rowcount = @@rowcount 
		   if @error <> 0
			  RAISERROR('Hiba az EREC_IratMetaDefinicio lekérdezésekor2!',16,1)
		   if @rowcount > 1
			  RAISERROR('A megadott azonosítójú irathoz többszörös ügytípus definíció tartozik!',16,1)

       end -- lekérdezés ügytípus szerint
	    
       -- lekérdezés ügykör szerint
       if @ResultMetaDef is NULL
       begin

		   select @ResultMetaDef    = im.id,
				  @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
                  @SzabalySzint     = '1'  -- ügykör szint
			 from EREC_IratMetaDefinicio im,
				  EREC_IraIratok ir,
				  EREC_UgyUgyiratDarabok ud,
				  EREC_UgyUgyiratok ui
			where ir.Id                  = @Irat_Id
			  and ir.UgyUgyiratDarab_Id  = ud.id
			  and ud.UgyUgyirat_id       = ui.id
			  and ui.IraIrattariTetel_Id = im.Ugykor_Id
              and im.Org                 = @ResultOrg
			  and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
              and im.Ugytipus is null
              and im.EljarasiSzakasz is NULL
              and im.Irattipus is NULL

		   select @error = @@error, @rowcount = @@rowcount 
		   if @error <> 0
			  RAISERROR('Hiba az EREC_IratMetaDefinicio lekérdezésekor1!',16,1)
		   if @rowcount > 1
			  RAISERROR('A megadott azonosítójú irathoz többszörös ügykör definíció tartozik!',16,1)

        end -- lekérdezés ügykör szerint

    end -- iratbesorolás meghatározás ügyiratadatok alapján

end -- iratbesorolás meghatározás

----------------------------------------
-- iratbesorolás paraméterek ellenorzése
----------------------------------------
if @ResultPKIMukodes <> 'Nem' AND
   @Dokumentum_Id is NULL AND
   @Irat_Id is NULL AND
   @ResultMetaDef is NULL
begin

	-- iratbesorolás létezésének ellenorzése (Irattípus)
	if @IratTipusNev is not NULL
	begin 

		select @ResultMetaDef    = im.Id,
			   @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
               @SzabalySzint     = '4'  -- irattípus szint
		  from EREC_IratMetaDefinicio im,
			   EREC_IraIrattariTetelek it
		 where im.UgykorKod   = @UgykorKod
		   and im.UgyTipusNev = @UgyTipusNev
		   and im.IratTipus   = (select Kod from KRT_KodTarak kt
                                  where KodCsoport_id = 
                                        (select Id from KRT_KodCsoportok cs 
                                          where cs.Kod ='IRATTIPUS'
                                            and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I')
                                    and kt.nev = @IratTipusNev
                                    and kt.Org = @ResultOrg
                                    and dbo.fn_Ervenyes(kt.ErvKezd,kt.ErvVege)='I')
		   and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
		   and im.Ugykor_Id   = it.Id
           and im.Org         = @ResultOrg
		   and it.Org         = @ResultOrg
		   and dbo.fn_Ervenyes(it.ErvKezd,it.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0 or @rowcount = 0
		   RAISERROR('A megadott adatokkal rendelkezo irattípus definíció nem létezik!',16,1)
		if @rowcount <> 1
		   RAISERROR('A megadott adatokkal rendelkezo irattípus definíció többször definiált!',16,1)

	end -- iratbesorolás létezésének ellenorzése (Irattípus)

	-- iratbesorolás létezésének ellenorzése (Eljárási szakasz)
	if @EljarasiSzakasz is not NULL AND @IratTipusNev is NULL
	begin 

		select @ResultMetaDef    = im.Id,
			   @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
               @SzabalySzint     = '3'  -- eljárási szakasz szint
		  from EREC_IratMetaDefinicio im,
			   EREC_IraIrattariTetelek it
		 where im.UgykorKod       = @UgykorKod
		   and im.UgyTipusNev     = @UgyTipusNev
		   and im.EljarasiSzakasz = @EljarasiSzakasz
		   and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
		   and im.Ugykor_Id   = it.Id
		   and it.Org         = @ResultOrg
		   and dbo.fn_Ervenyes(it.ErvKezd,it.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0 or @rowcount = 0
		   RAISERROR('A megadott adatokkal rendelkezo eljárási szakasz definíció nem létezik!',16,1)
		if @rowcount <> 1
		   RAISERROR('A megadott adatokkal rendelkezo eljárási szakasz definíció többször definiált!',16,1)

	end -- iratbesorolás létezésének ellenorzése (Irattípus)

	-- iratbesorolás létezésének ellenorzése (Ügytípus)
	if @UgyTipusNev is not NULL AND @EljarasiSzakasz is NULL AND @IratTipusNev is NULL
	begin 

		select @ResultMetaDef    = im.Id,
			   @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
               @SzabalySzint     = '2'  -- ügytípus szint
		  from EREC_IratMetaDefinicio im,
			   EREC_IraIrattariTetelek it
		 where im.UgykorKod   = @UgykorKod
		   and im.UgyTipusNev = @UgyTipusNev
		   and im.IratTipus is NULL
		   and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
		   and im.Ugykor_Id = it.Id
           and im.Org = @ResultOrg
		   and it.Org       = @ResultOrg
		   and dbo.fn_Ervenyes(it.ErvKezd,it.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0 or @rowcount = 0
		   RAISERROR('A megadott adatokkal rendelkezo ügytípus definíció nem létezik!',16,1)
		if @rowcount <> 1
		   RAISERROR('A megadott adatokkal rendelkezo ügytípus definíció többször definiált!',16,1)

	end -- iratbesorolás létezésének létezés ellenorzése (Ügytípus)

	-- iratbesorolás létezésének ellenorzése (Ügykör)
	if @UgykorKod is not NULL AND @UgyTipusNev is NULL AND @EljarasiSzakasz is NULL AND @IratTipusNev is NULL
	begin 

		select @ResultMetaDef    = im.Id,
			   @ResultMetaDefTip = 'EREC_IratMetaDefinicio',
               @SzabalySzint     = '1'  -- ügykör szint
		  from EREC_IratMetaDefinicio im,
			   EREC_IraIrattariTetelek it
		 where im.UgykorKod = @UgykorKod
		   and im.UgyTipus  is NULL
		   and im.IratTipus is NULL
		   and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
		   and im.Ugykor_Id = it.Id
           and im.Org = @ResultOrg
		   and it.Org       = @ResultOrg
		   and dbo.fn_Ervenyes(it.ErvKezd,it.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0 or @rowcount = 0
		   RAISERROR('A megadott adatokkal rendelkezo ügykör definíció nem létezik!',16,1)
		if @rowcount <> 1
		   RAISERROR('A megadott adatokkal rendelkezo ügykör definíció többször definiált!',16,1)

	end -- iratbesorolás létezésének ellenorzése (Ügykör)

end -- iratbesorolás paraméterek ellenorzése

-----------------------------------
-- dokumentum adatainak lekérdezése
-----------------------------------
if @ResultMetaDef is NULL AND
   @Dokumentum_Id is not NULL
begin

    select @AlairandoObj_Id  = dk.Id,
           @Alkalmazas_Id    = Alkalmazas_Id,
		   @ResultMetaDef    = md.Id,
		   @ResultMetaDefTip = 'EREC_Obj_MetaDefinicio',
           @SzabalySzint     = '5' -- CTT szabály
	  from KRT_Dokumentumok dk
           left outer join EREC_Obj_MetaDefinicio md
             on dk.SablonAzonosito = md.ContentType
            and dbo.fn_Ervenyes(md.ErvKezd,md.ErvVege) = 'I'
	 where dk.Id = @Dokumentum_Id
	   and dk.Alkalmazas_Id = @Alkalmazas_Id
	   and dbo.fn_Ervenyes(dk.ErvKezd,dk.ErvVege) = 'I'
  
    select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('Hiba a KRT_Dokumentumok olvasásánál!',16,1)
	if @rowcount = 0
	   RAISERROR('A megadott azonosítójú dokumentum az alkalmazásnál nem létezik!',16,1)

end -- dokumentum adatainak lekérdezése

---------------------------
-- irat aláírók ellenorzése
---------------------------
if @AlairandoObj_Id is not NULL
begin

	declare @AlairasSzabaly_Fk uniqueidentifier

	-- irat aláíró adatok olvasása
	if @HasznalatiMod = 'JegyzettAlairas'
	begin

		select @IratAlairok_Id    = Id,
			   @AlairasSorrend    = AlairasSorrend,
			   @AlairoSzerep      = AlairoSzerep,
			   @AlairasSzabaly_Fk = AlairasSzabaly_Id,
			   @IratAlairasMod    = AlairasMod
		  from EREC_IratAlairok
		 where Obj_Id  = @AlairandoObj_Id
		   and Allapot = '1' -- Aláírandó
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
		   and (FelhasznaloCsoport_Id_Alairo = @Felhasznalo_Id
		   or FelhaszCsop_Id_HelyettesAlairo = @Felhasznalo_Id)

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba az irat aláírók olvasásánál1!',16,1)
		if @rowcount > 1
		   RAISERROR('A felhasználó többszöri aláírásra van elojegyezve az iratnál!',16,1)

		-- aláíró csoport vezeto hozzárendelés ellenorzése
		if @rowcount = 0
		begin

			select @IratAlairok_Id    = ia.Id,
				   @AlairasSorrend    = ia.AlairasSorrend,
				   @AlairoSzerep      = AlairoSzerep,
				   @AlairasSzabaly_Fk = ia.AlairasSzabaly_Id,
				   @IratAlairasMod    = AlairasMod
			  from EREC_IratAlairok ia
			 where ia.Obj_Id  = @AlairandoObj_Id
			   and ia.Allapot = '1' -- Aláírandó
			   and dbo.fn_Ervenyes(ia.ErvKezd,ia.ErvVege) = 'I'
			   and FelhasznaloCsoport_Id_Alairo = 
				   (select ct.Csoport_Id 
					  from KRT_CsoportTagok ct
					 where ct.Csoport_Id_jogalany = @Felhasznalo_Id
					   and ct.Tipus in ('02','04') -- vezeto, beosztott vezeto
					   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)='I')

			select @error = @@error, @rowcount = @@rowcount 
			if @error <> 0
			   RAISERROR('Hiba az irat aláírók olvasásánál2!',16,1)
			if @rowcount = 0 and @HasznalatiMod = 'IratAlairas'
			   RAISERROR('A felhasználó nincs aláírásra elojegyezve az iratnál!',16,1)
			if @rowcount > 1
			   RAISERROR('A felhasználó többszöri aláírásra van elojegyezve az iratnál!',16,1)

		end -- aláíró csoport vezeto hozzárendelés ellenorzése

		-- pótlólagos aláírás ellenorzése
		if @IratAlairasMod = '1' and @ResultPKIMukodes = 'Szunetel'
		RAISERROR('Az aláírás pótlása a PKI infrastruktúra helyreállítása után lehetséges!',16,1)

		-- elozetes aláírások ellenorzése
		select @rowcount = count(*)
		  from EREC_IratAlairok
		 where Obj_Id         = @AlairandoObj_Id
		   and Allapot        = '1' -- Aláírandó
		   and AlairasSorrend < @AlairasSorrend
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

		if @@error <> 0
		RAISERROR('Hiba az irat aláírók olvasásánál3!',16,1)
		if @rowcount > 0
		RAISERROR('Az adott dokumentum elozetes aláírásai hiányoznak!',16,1)

	end -- irat aláíró adatok olvasása

	-- eloírt szabály és aláírás szint lekérdezése
	if @AlairasSzabaly_Fk is not NULL
	begin 

	   declare @IratAlairasSzint int

	   select @ResultAlairSzabaly = isnull(@ResultAlairSzabaly, sz.Id),
			  @IratAlairasSzint   = at.AlairasSzint,
			  @AlairasKod         = at.AlairasKod
		 from KRT_AlairasSzabalyok sz
			  inner join KRT_DokumentumMuveletek dm
                on sz.DokumentumMuvelet_Id = dm.Id
			   and dbo.fn_Ervenyes(dm.ErvKezd,dm.ErvVege)='I'
			  inner join KRT_AlairasTipusok at
			    on sz.AlairasTipus_Id      = at.Id
			   and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
			  left outer join EREC_IratMetaDefinicio im
                on sz.MetaDefinicio_Id = im.Id
			   and dbo.fn_Ervenyes(im.ErvKezd,im.ErvVege)='I'
			  left outer join EREC_Obj_MetaDefinicio om
                on sz.MetaDefinicio_Id = om.Id
			   and dbo.fn_Ervenyes(om.ErvKezd,om.ErvVege)='I'
		where sz.Id = @AlairasSzabaly_Fk
		  and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'

	   if @@error <> 0
		  RAISERROR('Hiba a KRT_AlairasSzabalyok lekérdezésekor!',16,1)

	   if @IratAlairasSzint is not NULL AND @AlairasSzint is not NULL AND
		  @IratAlairasSzint < @AlairasSzint AND @ResultPKIMukodes = 'Igen'
		  RAISERROR('Az aláírás szintje nem lehet alacsonyabb, mint ami elo van jegyezve!',16,1)

	end -- iratnál eloírt szabály lekérdezése

end -- irat aláírók ellenorzése

------------------------------------
-- aláíró ellenorzése elojegyezésnél
------------------------------------
if @HasznalatiMod = 'SzabalyLista'
begin

	-- felhasználó többszörös elojegyzésének ellenorzése
	select @rowcount = count(*)
	  from EREC_IratAlairok
	 where Obj_Id  = @AlairandoObj_Id
	   and Allapot = '1' -- Aláírandó
	   and isnull(AlairoSzerep,'_') = isnull(@AlairoSzerep,isnull(AlairoSzerep,'_'))
	   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
	   and FelhasznaloCsoport_Id_Alairo = @Felhasznalo_Id

	if @@error <> 0
	   RAISERROR('Hiba az irat aláírók olvasásánál3!',16,1)
	if @rowcount > 0
	   RAISERROR('A felhasználó aláíróként már szerepel az iratnál!',16,1)

	-- aláíró csoport vezeto hozzárendelés ellenorzése
	if @rowcount = 0
	begin

		select @rowcount = count(*)
		  from EREC_IratAlairok ia
		 where ia.Obj_Id  = @AlairandoObj_Id
		   and ia.Allapot = '1' -- Aláírandó
		   and isnull(ia.AlairoSzerep,'_') = isnull(@AlairoSzerep,isnull(ia.AlairoSzerep,'_'))
		   and dbo.fn_Ervenyes(ia.ErvKezd,ia.ErvVege)='I'
		   and FelhasznaloCsoport_Id_Alairo = 
			   (select ct.Csoport_Id 
				  from KRT_CsoportTagok ct
				 where ct.Csoport_Id_jogalany = @Felhasznalo_Id
				   and ct.Tipus in ('02','04') -- vezeto, beosztott vezeto
				   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)='I')

		if @@error <> 0
		   RAISERROR('Hiba az irat aláírók olvasásánál4!',16,1)
		if @rowcount > 0
		   RAISERROR('A felhasználó aláíró csoportvezetoként már szerepel az iratnál!',16,1)

	end -- aláíró csoport vezeto hozzárendelés ellenorzése

end -- aláíró ellenorzése elojegyezésnél

-----------------------------------------
-- aláírás szabály szerinti muveletadatok
-----------------------------------------
if @ResultAlairSzabaly is not NULL
begin

	select @ResultFolyamat     = dm.Folyamat_Id,
		   @ResultDokuMuvelet  = dm.Id,
		   @ResultAlairoSzerep = sz.AlairoSzerep,
		   @SzabalySzint       = 4 -- legalsó szint
	  from KRT_AlairasSzabalyok sz,
		   KRT_DokumentumMuveletek dm
	 where sz.Id = @ResultAlairSzabaly
	   and sz.DokumentumMuvelet_Id = dm.Id
	   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'

	select @error = @@error, @rowcount = @@rowcount 
	if @@error <> 0
	   RAISERROR('KRT_AlairasSzabalyok lekérdezés hiba!',16,1)

end -- aláírás szabály szerinti muveletadatok

---------------------------------------
-- folyamat és muveletadatok beállítása
---------------------------------------
if @ResultAlairSzabaly is NULL
begin 

	----------------------------
	-- folyamatadatok beállítása
	----------------------------
	if @FolyamatKod is not NULL
	begin
	   -- paraméter szerinti folyamatadatok
	   select @ResultFolyamat = fm.Id
		 from KRT_Folyamatok fm
		where fm.FolyamatKod   = @FolyamatKod
		  and fm.Alkalmazas_Id = @Alkalmazas_Id
		  and dbo.fn_Ervenyes(fm.ErvKezd,fm.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba a KRT_Folyamatok olvasásakor!',16,1)
		if @rowcount = 0
		   RAISERROR('A megadott nevu folyamat nem létezik az alkalmazásnál!',16,1)

	end -- paraméter szerinti folyamatadatok

	else
	begin

	   -- alapértelmezett folyamatadatok
	   select @ResultFolyamat  = fm.Id
		 from KRT_Folyamatok fm
		where fm.FolyamatKod   = 'KozvetlenAlairas'
		  and fm.Alkalmazas_Id = @EDOKAlkalmazas_Id
		  and dbo.fn_Ervenyes(fm.ErvKezd,fm.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba a KRT_Folyamatok olvasásakor!',16,1)
		if @rowcount = 0
		   RAISERROR('A közvetlen aláírás folyamat nem definiált!',16,1)

	end -- alapértelmezett folyamatadatok

	-----------------------------------
	-- muveletadatok
	-----------------------------------
	if @MuveletKod is not NULL
	begin

		select @ResultDokuMuvelet = Id,
			   @AlairoSzerepJel   = AlairoSzerepJel
		  from KRT_DokumentumMuveletek
		 where Folyamat_Id  = @ResultFolyamat
		   and MuveletKod   = @MuveletKod
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba a KRT_DokumentumMuveletek olvasásakor!',16,1)
		if @rowcount = 0
		   RAISERROR('A megadott muvelet paraméterekhez nincs definiálva érvényes muvelet!',16,1)
		if @rowcount > 1
		   RAISERROR('A megadott muvelet paraméterekhez több érvényes muvelet van definiálva!',16,1)
		if @AlairoSzerepJel <> '0' and @AlairoSzerep is NULL
		   RAISERROR('Az adott folyamatban az aláírói szerep megadása kötelezo!',16,1)

	end -- paraméter szerinti muveletadatok

	-----------------------------------
	-- paraméter szerinti aláíró szerep
	-----------------------------------
	if @AlairoSzerep is not NULL
	begin

		select @rowcount = count(*)
		 from KRT_KodTarak kt
		 where KodCsoport_Id = (select id from KRT_KodCsoportok cs 
								 where cs.kod ='ALAIRO_SZEREP'
								   --and cs.org = @ResultOrg
								   and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I')
		   and kt.Kod        = @AlairoSzerep
		   and kt.Org        = @ResultOrg
		   and dbo.fn_Ervenyes(kt.ErvKezd,kt.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount 
		if @error <> 0
		   RAISERROR('Hiba a KRT_KodTarak lekérdezésénél!',16,1)
		if @rowcount = 0
		   RAISERROR('A megadott kóddal nem létezik érvényes aláírói szerep!',16,1)

	end -- paraméter szerinti aláíró szerep

end -- paraméterezés szerinti muveletadatok beállítása

--------------------------------
-- irat aláírhatóság ellenorzése
--------------------------------
if @HasznalatiMod like '%Alairas'
begin

	-------------------------------
	-- aláírási szabály lekérdezése
	-------------------------------
	declare @ResultIsmetloSzabaly uniqueidentifier
	exec [dbo].[sp_PKI_Szabaly]
		 @ResultMetaDef,
		 @ResultMetaDefTip,
		 @ResultDokuMuvelet,
		 @AlairoSzerep,
		 @Titkositas,
		 @SzabalySzint,
		 @AlairasKod,
		 @HasznalatiMod,
		 @ResultPKIMukodes,
		 @ResultOrg,
		 @ResultEDOK,
		 @ResultAlairSzabaly output,
		 @ResultIsmetloSzabaly output
 
--RAISERROR('Teszt_ParEll!',16,1)
	-------------------------------------
	-- befejezetlen aláírások ellenorzése
	-------------------------------------
	if @ResultPKIMukodes = 'Igen' AND
	   @ResultAlairSzabaly is not NULL AND
       @AlairandoObj_Id is not NULL
	begin 

		 select @rowcount = count(*)
		   from EREC_IratAlairok ia,
				KRT_AlairasSzabalyok sz,
				KRT_AlairasTipusok at,
				KRT_DokumentumKapcsolatok dk,
				KRT_DokumentumAlairasok da,
				(select Dokumentum_Id
				   from EREC_Csatolmanyok cs
				  where cs.IraIrat_Id    = @Irat_Id
				    and cs.IratAlairoJel = '1'
				 ) id
		  where ia.Obj_Id               = @AlairandoObj_Id
			and ia.Allapot              = '2' -- Aláírt
			and sz.Id                   = @ResultAlairSzabaly
			and sz.AlairasTipus_Id      = at.Id
			and at.AlairasMod           like('E_%')
			and (@Irat_Id is not NULL AND dk.Dokumentum_Id_Al = id.Dokumentum_Id OR
				 @Dokumentum_Id is not NULL AND dk.Dokumentum_Id_Al = @AlairandoObj_Id )
			and dk.Dokumentum_Id_Fo     = da.Dokumentum_Id_Alairt
			and da.AlairasVeglegRendben <> '1'

		if @@error <> 0
		   RAISERROR('Hiba az aláírt dokumentumok ellenorzésénél!',16,1)
		if @rowcount > 0
		   RAISERROR('Az elozo aláírás még nem véglegesen hiteles állapotú!',16,1)

	end -- befejezetlen aláírások ellenorzése

end -- irat aláírhatóság ellenorzése

---------------------------------
-- PKI integráció nélküli kimenet 
---------------------------------
if @HasznalatiMod like '%Alairas' AND
   @ResultAlairSzabaly is NULL
begin

	select convert(int,1)                 Sorsz,
		   @ResultPKIMukodes              PKIMukodes,
		   convert(nvarchar(100),NULL)    AlairasNev,
		   convert(nvarchar(64),NULL)     AlairasMod,
		   convert(int,NULL)              AlairasSzint,
		   convert(uniqueidentifier,NULL) AlairasSzabaly_Id,
		   convert(nvarchar(64),NULL)     AlairoSzerep,
		   convert(uniqueidentifier,NULL) IsmetloSzabaly_Id,
		   convert(nvarchar(64),NULL)     SzabalyTipus,
		   convert(nvarchar(100),NULL)    EASZ,
           convert(int,NULL)              KivarasiIdo,
		   convert(nvarchar(10),NULL)     KonfigTetel,
		   convert(uniqueidentifier,NULL) Tanusitvany_Id,
		   convert(nvarchar(100),NULL)    TanLenyomat,
		   convert(uniqueidentifier,NULL) Alairo_Id,
		   convert(nvarchar(100),NULL)    AlairoNev,
		   convert(nvarchar(100),NULL)    AlairasTulajdonos,
           convert(nvarchar(7),NULL)      Kivitelezes,
		   convert(char(1),NULL)          DefSzabaly,
		   convert(int,NULL)              Cn

end

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------