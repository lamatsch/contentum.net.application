﻿create procedure [dbo].[sp_PKI_Szabaly]    
                 @MetaDefinicio_Id      uniqueidentifier,
				 @MetaDefinicioTipus    nvarchar(100),
                 @DokumentumMuvelet_Id  uniqueidentifier,
                 @AlairoSzerep          nvarchar(64),
                 @Titkositas            char(1),
                 @SzabalySzint          nvarchar(64),
                 @AlairasKod            nvarchar(10),
                 @HasznalatiMod         nvarchar(16),
				 @PKIMukodes            nvarchar(100),
                 @Org                   uniqueidentifier,
                 @EDOK                  char(1),
                 @ResultAlairSzabaly    uniqueidentifier OUTPUT,
				 @ResultIsmetloSzabaly  uniqueidentifier OUTPUT
AS
/*
-- FELADATA:
	Irat aláírási szabály lekérdezése irattípus és muvelet adatok alapján,
    a PKI muködéssel kapcsolatos ellenorzések és beállítások végrehajtása.
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @AlairasSzabaly_Id uniqueidentifier,
        @MetaDef           uniqueidentifier,
        @Szint             int,
		@SzabalyTipus      nvarchar(64),
		@AlairasTipus_Id   uniqueidentifier,
		@AlairasMod        nvarchar(64)

set @ResultAlairSzabaly   = NULL
set @ResultIsmetloSzabaly = NULL

--------------------------------
-- input paraméterek kiiratása
--------------------------------
if @teszt = 1
begin
   print '----- sp_PKI_Szabaly -----'
   print '@MetaDefinicio_Id     = '+ isnull( convert(varchar(50),@MetaDefinicio_Id),    'NULL')
   print '@MetaDefinicioTipus   = '+ isnull( convert(varchar(50),@MetaDefinicioTipus),  'NULL')
   print '@DokumentumMuvelet_Id = '+ isnull( convert(varchar(50),@DokumentumMuvelet_Id),'NULL')
   print '@AlairoSzerep         = '+ isnull( convert(varchar(50),@AlairoSzerep),        'NULL')
   print '@Titkositas           = '+ isnull( convert(varchar(50),@Titkositas),          'NULL')
   print '@SzabalySzint         = '+ isnull( convert(varchar(50),@SzabalySzint),        'NULL')
   print '@AlairasKod           = '+ isnull( convert(varchar(50),@AlairasKod),          'NULL')
   print '@HasznalatiMod        = '+ isnull( convert(varchar(50),@HasznalatiMod),       'NULL')
   print '@PKIMukodes           = '+ isnull( convert(varchar(50),@PKIMukodes),          'NULL')
end

--------------------------------------------------
-- kizáró szabályok aláírás típusának munkatáblája
--------------------------------------------------
create table #KizaroSzabalyok
(
   AlairasSzabaly_Id uniqueidentifier,
   AlairasTipus_Id   uniqueidentifier
)

---------------------------------------------------
-- iratbesorolással és muvelettel definiált szabály
---------------------------------------------------
if @PKIMukodes <> 'Nem' AND
   @MetaDefinicio_Id is not NULL
begin

	-- iratbesorolás szerinti szabály beállítása
	select @MetaDef = @MetaDefinicio_Id,
		   @szint   = isnull(convert(int,@SzabalySzint),'4')
	while  @ResultAlairSzabaly is NULL and @Szint > 0
	begin

		-- szabály lekérdezés
		select @AlairasSzabaly_Id = sz.Id,
			   @SzabalyTipus      = sz.SzabalyTipus,
			   @AlairasMod        = at.AlairasMod,
			   @AlairasTipus_Id   = at.Id
		  from KRT_AlairasSzabalyok sz
			   inner join KRT_DokumentumMuveletek dm
				 on dm.Id = @DokumentumMuvelet_Id
			   left outer join KRT_AlairasTipusok at
				 on sz.AlairasTipus_Id = at.Id
			     and at.AlairasKod      = isnull(@AlairasKod,at.AlairasKod)
			     and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
		 where sz.DokumentumMuvelet_Id = @DokumentumMuvelet_Id
		   and sz.MetaDefinicio_Id     = @MetaDef
		   and isnull(sz.AlairoSzerep,'_') = case when dm.MuveletKod = 'KozvetlenAlairas' then '_'
												  when dm.MuveletKod = 'PKISzunetelesAlairas' then '_'
												  else isnull(@AlairoSzerep,'_') end
		   and sz.SzabalySzint         = convert(char(1),@Szint)
		   and sz.Titkositas           = @Titkositas
		   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'

		select @error = @@error, @rowcount = @@rowcount
		if @error <> 0
		   RAISERROR('Hiba az irattípusos AlairasSzabaly lekérdezésekor!',16,1)
		if @rowcount > 1
		  RAISERROR('A megadott irattípushoz és muvelethez többszörösen definiált szabály!',16,1)

		-- szabály elmentése
		if @rowcount = 1
			if @SzabalyTipus <> '04' -- nem kizáró szabály
				set @ResultAlairSzabaly = @AlairasSzabaly_Id
			else -- kizáró szabály
				insert #KizaroSzabalyok (AlairasSzabaly_Id, AlairasTipus_Id)
				values (@AlairasSzabaly_Id, @AlairasTipus_Id)

		-- következo szint beállítás IratMetaDefinicio esetén
		select @Szint = case when @MetaDefinicioTipus = 'EREC_IratMetaDefinicio' then @Szint - 1
                             else 0 end

		if (@rowcount = 0 or @rowcount = 1 and @SzabalyTipus = '04') and @Szint > 0
		begin

			select @MetaDef = mk.Id
			  from EREC_IratMetadefinicio me,
				   EREC_IratMetadefinicio mk
			 where me.Id = @MetaDef
			   and mk.ugykorKod = me.ugykorKod
			   and isnull(mk.ugytipus,'_')        = case when @Szint = 1 then '_'
											        else me.ugytipus end
			   and isnull(mk.EljarasiSzakasz,'_') = case when @Szint <= 2 then '_'
											        else me.EljarasiSzakasz end
			   and mk.irattipus is null
			   and me.Org = @Org and mk.Org = @Org           
			   and dbo.fn_Ervenyes(mk.ErvKezd,mk.ErvVege)='I'

			select @error = @@error, @rowcount = @@rowcount 
			if @error <> 0
			   RAISERROR('Hiba az IratMetadefinicio lekérdezésénél!',16,1)
--			if @rowcount = 0
--			  RAISERROR('A szülo IratMetadefinicio nem definiált!',16,1)
			if @rowcount > 1
			  RAISERROR('A szülo IratMetadefinicio többször definiált!',16,1)

		end -- következo szint beállítás
	  
	end -- ciklus

end -- iratbesorolással és muvelettel definiált szabály

-----------------------------------
-- muvelettel definiált alapszabály
-----------------------------------
if @ResultAlairSzabaly is NULL

begin

	select @ResultAlairSzabaly = sz.Id,
		   @SzabalyTipus       = sz.SzabalyTipus,
		   @AlairasMod         = at.AlairasMod
	  from KRT_DokumentumMuveletek dm
		   inner join KRT_AlairasSzabalyok sz
		      on dm.Id              = sz.DokumentumMuvelet_Id
		     and sz.SzabalySzint    = '0'   -- Alapszabály
		     and sz.SzabalyTipus   <> '04'  -- Kizáró szabály
		     and isnull(sz.AlairoSzerep,'_') = case when dm.MuveletKod = 'KozvetlenAlairas' then '_'
													when dm.MuveletKod = 'PKISzunetelesAlairas' then '_'
												    else isnull(@AlairoSzerep,'_') end
		     and sz.Titkositas           = @Titkositas
		     and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'
		   inner join KRT_AlairasTipusok at
		      on sz.AlairasTipus_Id = at.Id
		     and at.AlairasKod      = isnull(@AlairasKod,at.AlairasKod)
		     and dbo.fn_Ervenyes(at.ErvKezd,at.ErvVege)='I'
		     and at.AlairasMod like
			     case when @EDOK = 'N' and @PKIMukodes = 'Nem' then '_'
					  else at.AlairasMod end
	 where dm.Id = @DokumentumMuvelet_Id
       and sz.AlairasTipus_Id not in (select AlairasTipus_Id from #KizaroSzabalyok)

	select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('Hiba a muveleti AlairasSzabaly lekérdezésekor!',16,1)
	if @rowcount > 1
	   RAISERROR('A megadott muvelethez többszörösen definiált alapszabály!',16,1)

end -- muvelettel definiált alapszabály

----------------------------------
-- Használaton kívüli PKI kezelése
----------------------------------
if @ResultAlairSzabaly is not NULL AND
   @PKIMukodes <> 'Igen' AND
   @HasznalatiMod like '%Alairas%' AND
   (@AlairasMod like 'E_%' OR @AlairasMod is NULL)

begin

	-- kötelezo elektronikus aláírás ellenorzése
	if @PKIMukodes = 'Szunetel' AND
	   @SzabalyTipus in ('01')   -- kökelezo mód
		   RAISERROR('Az elektronikus aláírás végrehajtása a PKI muködés visszaállítása után lehetséges!',16,1)

	-- pótolandó (eredeti) szabály beállítása
	set @ResultIsmetloSzabaly = case when @PKIMukodes = 'Szunetel' and @SzabalyTipus = '02' -- pótolandó
										  then @ResultAlairSzabaly
									 else NULL end

	-- végrehajtandó szabály beállítása
	select @ResultAlairSzabaly = case when @PKIMukodes = 'Nem' and @HasznalatiMod = 'IratAlairas'
									  then sz.Id 
								 when @PKIMukodes = 'Nem' and @HasznalatiMod <> 'IratAlairas'
									  then NULL
								 when @PKIMukodes = 'Szunetel' and @HasznalatiMod like '%Lista'
									  then NULL
								 when @PKIMukodes = 'Szunetel' and @SzabalyTipus <> '02'
									  then NULL
								 when @SzabalyTipus in ('02','03') -- nem kötelezo
									  then sz.Id
								 else @ResultAlairSzabaly end
	  from KRT_DokumentumMuveletek dm,
		   KRT_AlairasSzabalyok sz
	 where dm.MuveletKod  = 'PKISzunetelesAlairas'
	   and dbo.fn_Ervenyes(dm.ErvKezd,dm.ErvVege)='I'
	   and dm.id          = sz.DokumentumMuvelet_Id
	   and sz.Titkositas  = @Titkositas
	   and dbo.fn_Ervenyes(sz.ErvKezd,sz.ErvVege)='I'

	select @error = @@error, @rowcount = @@rowcount 
	if @error <> 0
	   RAISERROR('Hiba a PKI szünetelés szabály lekérdezésnél!',16,1)
	if @rowcount = 0 and @PKIMukodes <> 'Nem'
	   RAISERROR('Nincs definiálva érvényes PKI szünetelés szabály!',16,1)
	if @rowcount > 1
	   RAISERROR('Többszörösen definiált PKI szünetelés szabály!',16,1)

end -- Használaton kívüli PKI kezelése

print '@ResultAlairSzabaly= '+ isnull( convert(varchar(50),@ResultAlairSzabaly),'NULL')

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------