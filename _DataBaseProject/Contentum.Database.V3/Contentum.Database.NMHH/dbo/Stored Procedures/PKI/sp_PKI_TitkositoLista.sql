﻿create procedure [dbo].[sp_PKI_TitkositoLista]    
                 @Felhasznalo_Id uniqueidentifier,  -- a titkosító felhasználó EDOK azonosítója
                 @ElosztoIv_Id   uniqueidentifier   -- a 'Titkosítás' típusú elosztóív EDOK azonosítója (opcionális)
AS
/*
-- FELADATA:
	Titkosítási lista lekérdezése a kijelölt eloszóívhez rendelt felhasználók aktuális adatai
    alapján, a titkosítást vézo felhasználó automatikus hozzáadásával. Elosztóív megadása nélkül
    az összes olyan felhasználó szerepel a listában, aki titkosító tanúsítvánnyal rendelkezik.
    A titkosítást végzo felhasználónak nem kell rendelkeznie feltétlenül titkosító tanúsítvánnyal.
    Titkosítás csak aláírt dokumentumra kezdeményezheto.

-- Példa:
declare @Felhasznalo_Idc  uniqueidentifier
select @Felhasznalo_Idc = c.Id
  from KRT_Tanusitvanyok t, KRT_Csoportok c
 where c.Id = t.Csoport_id and c.nev like 'Teszt felhasználó'
exec [dbo].[sp_PKI_TitkositoLista] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
	 NULL 	              -- @ElosztoIv_Id
*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @Org             uniqueidentifier,
        @PKIMukodes      nvarchar(100)

--------------------------
-- paraméterezés vizsgálat
--------------------------
if @Felhasznalo_Id is NULL
   RAISERROR('A felhasználó azonosító megadása kötelezo!',16,1)

--------------------------
-- felhasználó ellenorzése
--------------------------
-- felhasználó létezésének ellenorzése
select @Org = Org
  from KRT_Csoportok
 where Id = @Felhasznalo_Id
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
    
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('KRT_Felhasznalok lekerdezes hiba!',16,1)
if @rowcount = 0
   RAISERROR('A megadott azonosítóval nem létezik érvényes felhasználó!',16,1)

-- elosztóív ellenorzése
if @ElosztoIv_Id is not NULL
begin
	select @rowcount = count(*)
	  from EREC_IraElosztoIvek
	 where Id  = @ElosztoIv_Id
	   --and Org = @Org	-- (EB 2009.09.07. - Jelenleg meg nincs Org mezo az EREC_IraElosztoIvek tablaban)
	   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

	if @@error <> 0
	   RAISERROR('EREC_IraElosztoIvek lekerdezes hiba!',16,1)
	if @rowcount = 0
	   RAISERROR('A megadott azonosítóval nem létezik érvényes elosztóív!',16,1)
	if @rowcount > 1
	   RAISERROR('A megadott azonosítóval több érvényes elosztóív létezik!',16,1)
end

--------------------------
-- PKI muködés ellenorzése
--------------------------
select @PKIMukodes = Ertek
  from KRT_Parameterek
 where Nev = 'PKI_INTEGRACIO'
   and Ertek in('Igen','Nem','Szunetel') 
   and Org = @Org
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
   
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('Rendszer paraméter lekerdezes hiba!',16,1)
if @rowcount <> 1
   RAISERROR('A PKI_INTEGRACIO (Igen/Nem/Szunetel) rendszerparaméter nincs helyesen beállítva!',16,1)
if @PKIMukodes <> 'Igen'
   RAISERROR('Az aktuális rendszerbeállítás szerint titkosítás nem lehetséges!',16,1)

----------------------------
-- títkosítási lista kimenet
----------------------------
select fh.Id, fh.Nev, fh.EMail
  from KRT_Felhasznalok fh,
	   KRT_Tanusitvanyok tn,
	   KRT_TanusitvanyTipusok tt
 where dbo.fn_Ervenyes(fh.ErvKezd,fh.ErvVege)='I'
   and fh.Id                  = tn.Csoport_Id
   and dbo.fn_Ervenyes(tn.ErvKezd,tn.ErvVege)='I'
   and tn.TanusitvanyTipus_Id = tt.Id
   and tt.TanusitvanyTipus    = 'TITKOS'
   and dbo.fn_Ervenyes(tt.ErvKezd,tt.ErvVege)='I'
   and (@ElosztoIv_Id is NULL
        or
       (fh.Id = @Felhasznalo_Id or
        fh.Id in (select f.Id
                    from EREC_IraElosztoivTetelek t,
                         KRT_Felhasznalok f
				   where t.Elosztoiv_Id = @ElosztoIv_Id
				     and dbo.fn_Ervenyes(t.ErvKezd,t.ErvVege)='I'
				     and t.partner_Id   = f.Partner_Id
				     and dbo.fn_Ervenyes(f.ErvKezd,f.ErvVege)='I')))
    
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('Titkosító lista lekérdezés hiba!',16,1)
if @rowcount = 0
   RAISERROR('A megadott paraméterekkel nem létezik érvényes titkosító tanúsítvánnyal rendelkezo felhasználó!',16,1)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------