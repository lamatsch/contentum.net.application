﻿CREATE PROCEDURE [dbo].[sp_AddCsoportToJogtargy]
	@JogtargyId		uniqueidentifier,
	@CsoportId		uniqueidentifier,
	@ExecutorUserId uniqueidentifier,
	@Kezi			char(1) = 'I',
	@Jogszint		char(1) = 'I',
	@Tipus			char(1) = '0'
AS
/**********************************************************************************************************
* Összeköti a paraméterben megadott ACL-t a megadott Csoporttal (KRT_Jogosultak táblába felvesz egy rekordot).
*
* Errorcode [50402]: a kapcsolat már létezik
**********************************************************************************************************/
BEGIN
begin try
	SET NOCOUNT ON;

	if exists (select KRT_Jogosultak.Id from KRT_Jogosultak KRT_Jogosultak where KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId AND KRT_Jogosultak.Obj_Id = @JogtargyId AND KRT_Jogosultak.Kezi = @Kezi AND KRT_Jogosultak.Jogszint = @Jogszint AND KRT_Jogosultak.Tipus = @Tipus)
	begin
	  raiserror('[50402]',16,1);
	  return @@error;
	end

	insert into KRT_Jogosultak(Csoport_Id_Jogalany, Jogszint ,Obj_Id, Kezi, Letrehozo_id, Tipus) values
	  (@CsoportId, @Jogszint, @JogtargyId, @Kezi, @ExecutorUserId, @Tipus);
end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END