﻿ --set ANSI_NULLS ON
 --set QUOTED_IDENTIFIER ON
 --go

 --if exists (select 1
 --            from  sysobjects
 --           where  id = object_id('sp_AddCsoportToJogtargyTomeges')
 --            and   type = 'P')
 --   drop procedure sp_AddCsoportToJogtargyTomeges
 --go

CREATE PROCEDURE [dbo].[sp_AddCsoportToJogtargyTomeges]
	@JogtargyIds nvarchar(max),
    @CsoportIds nvarchar(max),
    @ExecutorUserId uniqueidentifier
AS
BEGIN
begin try
	SET NOCOUNT ON;

	declare @jogosultak table(Csoport_Id_Jogalany uniqueidentifier, Obj_id uniqueidentifier)
	insert into @jogosultak
	select CsoportTable.Value, JogtargyTable.Value 
	from dbo.fn_Split(@CsoportIds,',') CsoportTable,
	dbo.fn_Split(@JogtargyIds,',') JogtargyTable

	delete @jogosultak 
	from @jogosultak jog
	join KRT_Jogosultak krt_jog on jog.Csoport_Id_Jogalany = krt_jog.Csoport_Id_Jogalany
	and jog.Obj_Id = krt_jog.Obj_id
	where krt_jog.Kezi = 'I'

	insert into KRT_Jogosultak
	(Csoport_Id_Jogalany, Jogszint ,Obj_Id, Kezi, Tipus, Letrehozo_id) 
	select jog.Csoport_Id_Jogalany, 'O', jog.Obj_id,'I','O', @ExecutorUserId
	from @jogosultak jog

end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END