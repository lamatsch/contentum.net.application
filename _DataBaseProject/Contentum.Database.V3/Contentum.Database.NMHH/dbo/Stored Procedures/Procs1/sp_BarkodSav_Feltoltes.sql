﻿create procedure [dbo].[sp_BarkodSav_Feltoltes]    
                @BarkodSav_Id         uniqueidentifier,
                @FirstKod             nvarchar(100),
                @LastKod              nvarchar(100),
                @ReszSavFeltoltes     char(1)          = 'N',            -- N/I
                @FeltoltoSzervezet_Id uniqueidentifier, --= NULL,
                @ResultUid            uniqueidentifier OUTPUT
         
AS
/*
--
-- KÉSZÍTETTE:   Ádám András       2007.11.13
-- MÓDOSÍTOTTA:

-- FELADATA:
       a Nyomdától igényelt vagy helyileg Printelt vonalkódok feltöltése,
       azaz az adatbázisban való tényleges létrehozása,
       a bárkódsáv elso és utolsó vonalkódjának beolvastatása után;
       külön megadható hogy csak a teljes sáv vagy esetleg részsáv is feltöltheto legyen,
       ez utóbbi esetben valószínuleg születik 1 vagy 2 új (még feltöltetlen) sáv is,
       és külön megadható az esetleges új sávtulajdonos is!? 
       Ha ez nincs megadva vagy a létrehozó van megadva, akkor marad az eredeti tulajdonos,
       ha más akkor az lesz az új (vagy esetleg visszautasítjuk???).
       A FELADAT-ra csak olyan bárkódsáv jelolheto ki, mely P v. N - típusú (jelenleg mind ilyen),
       és A státuszú (tehát még nincs feltöltve, azaz amely még nem használtató, elhasznált vagy törölt)

-- pl:
declare @ID uniqueidentifier
exec [dbo].[sp_BarkodSav_Feltoltes] 
     '28653A1E-6BA2-4459-A51C-359CFAB5C690', -- @BarkodSav_Id
     '1000000000001',                        -- @FirstKod
     '1000000099991',                        -- @LastKod
     'N',                                    -- @ReszSavFeltoltes 
     'FF626847-F433-4B80-83D4-64AEA5C147FF', -- @FeltoltoSzervezet_Id 
     @ID output                              -- @ResultUid

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1
 
declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
select  @ResultUid = NULL
--
declare @myid            uniqueidentifier
select  @myid = @BarkodSav_Id

declare @error           int
declare @rowcount        int

------- 
declare @Csoport_Id_Felelos uniqueidentifier
declare @SavKezd         nvarchar(100)
declare @SavVege         nvarchar(100)
declare @SavType         char(1)
declare @SavAllapot      char(1)

declare @UjSavKezd       nvarchar(100)
declare @UjSavVege       nvarchar(100)

declare @FirstKod_num    numeric(12)
declare @LastKod_num     numeric(12)
declare @AktKod_num      numeric(12)
declare @AktKod          nvarchar(100)

--------------------------------
-- input paraméterek ellenorzése
--------------------------------

-- input paraméterek kiiratása
if @teszt = 1
   print '@BarkodSav_Id = '+ isnull( convert(varchar(50),@BarkodSav_Id), 'NULL') + ', ' +
         '@FirstKod = '+ isnull(@FirstKod, 'NULL') + ', ' +
         '@LastKod = '+ isnull(@LastKod, 'NULL') + ', ' +
         '@ReszSavFeltoltes = '+ isnull(@ReszSavFeltoltes, 'NULL') + ', ' +
         '@FeltoltoSzervezet_Id = '+ isnull( convert(varchar(50),@FeltoltoSzervezet_Id), 'NULL')

DECLARE @Org uniqueidentifier
SET @Org = dbo.fn_GetOrgByCsoportId(@FeltoltoSzervezet_Id)
if (@Org is null)
begin
	RAISERROR('[50202]',16,1)
END

-- NULLITÁS-vizsgálat
if @BarkodSav_Id is NULL
   RAISERROR('[54115]',16,1)
   --RAISERROR('@BarkodSav_Id paraméter nem lehet NULL!',16,1)

if @FirstKod is NULL
   RAISERROR('[54116]',16,1)
   --RAISERROR('@FirstKod paraméter nem lehet NULL!',16,1)

if @LastKod is NULL
   RAISERROR('[54117]',16,1)
   --RAISERROR('@LastKod paraméter nem lehet NULL!',16,1)

if @ReszSavFeltoltes is NULL
   select @ReszSavFeltoltes = 'N'

-- korrekt-ek az input paraméterek?
if @ReszSavFeltoltes not in ('N','I')
   RAISERROR('[54118]',16,1)
   --RAISERROR('@ReszSavFeltoltes paraméter nem korrekt! (csak N,I lehet)',16,1)

--ellenörzo vektor lekérése   
DECLARE @vektor NVARCHAR(12)
SET @vektor = dbo.fn_GetRendszerparameterBySzervezet(@FeltoltoSzervezet_Id,'VONALKOD_ELLENORZO_VEKTOR')  

IF @vektor = NULL
BEGIN
	--rendszerparaméter nem található 
	RAISERROR('[54159]',16,1)
END

IF @vektor = 'Error_50202'
BEGIN
	--org nem határozható meg
	RAISERROR('[50202]',16,1)
END 

if @teszt = 1
	PRINT '@vektor = ' + @vektor

-- BLG_1940
--if dbo.fn_barkod_check(@FirstKod, @vektor) <> 0
if dbo.fn_barkod_check(right(@FirstKod,13), @vektor) <> 0
   RAISERROR('[54119]',16,1)
   --RAISERROR('@FirstKod paraméter nem korrekt vonalkód!',16,1)

-- BLG_1940
--if dbo.fn_barkod_check(@LastKod, @vektor) <> 0
if dbo.fn_barkod_check(right(@LastKod,13), @vektor) <> 0
   RAISERROR('[54120]',16,1)
   --RAISERROR('@LastKod paraméter nem korrekt vonalkód!',16,1)

if @FirstKod > @LastKod
   RAISERROR('[54121]',16,1)
   --RAISERROR('@FirstKod > @LastKod nem lehet!',16,1)

-- sáv létezés ellenorzés
select @Csoport_Id_Felelos = NULL, @SavKezd = NULL, @SavVege = NULL, @SavType = NULL, @SavAllapot = NULL 
select @Csoport_Id_Felelos = Csoport_Id_Felelos,
       @SavKezd            = SavKezd,
       @SavVege            = SavVege,
       @SavType            = SavType,
       @SavAllapot         = SavAllapot
  from dbo.KRT_BarkodSavok
 where Id = @BarkodSav_Id
 
if @@error <> 0
   RAISERROR('[54122]',16,1)
   --RAISERROR('KRT_BarkodSavok select hiba!',16,1)

-- a megadott sáv paramétereinek kiiratása
if @teszt = 1
   print '@Csoport_Id_Felelos = '+ isnull( convert(varchar(50),@Csoport_Id_Felelos), 'NULL') + ', ' +
         '@SavKezd = '+ isnull(@SavKezd, 'NULL') + ', ' +
         '@SavVege = '+ isnull(@SavVege, 'NULL') + ', ' +
         '@SavType = '+ isnull(@SavType, 'NULL') + ', ' +
         '@SavAllapot = '+ isnull(@SavAllapot, 'NULL')

if @Csoport_Id_Felelos is NULL
   RAISERROR('[54123]',16,1)
   --RAISERROR('A megadott vonalkódsáv nem létezik!',16,1)

--if @FeltoltoSzervezet_Id is NULL
--   select @FeltoltoSzervezet_Id = @Csoport_Id_Felelos

-- CR#1971 (EB 2009.03.11): Kliens oldalon ellenorizzük, hogy a végrehajtó felhasználó tagje-e a felelos csoportnak (vagy azonos vele)
---- kérdés: ugyanazt a szervezetet követeljük-e meg,
----         vagy lehet a feltölto más is és akkor o lesz a tulajdonos,
----         vagy ne is foglalkozzunk szervezettel ... ????
--if @FeltoltoSzervezet_Id <> @Csoport_Id_Felelos
--   RAISERROR('[54124]',16,1)
--   --RAISERROR('Az igénylo és feltölto szervezetnek azonosnak kell lennie!',16,1)

if @SavAllapot <> 'A'
   RAISERROR('[5412A]',16,1) -- EZ VÉLETLEN KIMARADT, KÉNE MAJD NEKI EGY RENDES ÚJ SORSZÁM!!!! -- mj: AA
   --RAISERROR('A megadott vonalkódsáv már betöltött (vagy törölt)!',16,1)

if @SavType not in ('N','P')
   RAISERROR('[54125]',16,1)
   --RAISERROR('Csak Nyomdai vagy helyileg Printelt vonalkódsáv töltheto be!',16,1)

if @ReszSavFeltoltes = 'N'
begin -- részsávbetöltés nem engedélyezett
   
   if @FirstKod <> @SavKezd
      RAISERROR('[54126]',16,1)
      --RAISERROR('@FirstKod paraméter nem a megadott vonalkódsáv kezdete!',16,1)

   if @LastKod <> @SavVege
      RAISERROR('[54127]',16,1)
      --RAISERROR('@LastKod paraméter nem a megadott vonalkódsáv vége!',16,1)

end
else
begin -- részsávbetöltés engedélyezett

   if @FirstKod not between @SavKezd and @SavVege
      RAISERROR('[54128]',16,1)
      --RAISERROR('@FirstKod paraméter nem esik megadott vonalkódsávba!',16,1)
      
   if @LastKod not between @SavKezd and @SavVege
      RAISERROR('[54129]',16,1)
      --RAISERROR('@LastKod paraméter nem esik megadott vonalkódsávba!',16,1)

end

------------------------------------------------------
-- a megadott vonalkódsáv paramétereinek aktualizálása
------------------------------------------------------
-- (a konkurens kezelés miatt arra is ügyelve, hogy esetleg nem változtak-e meg ...)
update dbo.KRT_BarkodSavok
   set Csoport_Id_Felelos = @FeltoltoSzervezet_Id,  -- még megbeszélni, hogy átírható-e!?
       SavAllapot         = 'H',                    -- most már Használtó lesz a sáv
       SavKezd            = @FirstKod,
       SavVege            = @LastKod
 where Id                 = @BarkodSav_Id
   and Csoport_Id_Felelos = @Csoport_Id_Felelos
   and SavKezd            = @SavKezd
   and SavVege            = @SavVege
   and SavType            = @SavType
   and SavAllapot         = @SavAllapot

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   RAISERROR('[54130]',16,1)
   --RAISERROR('KRT_BarkodSavok update hiba!',16,1)

if @rowcount = 0
   RAISERROR('[54131]',16,1)
   --RAISERROR('A megadott vonalkódsáv paraméterei idoközben megváltoztak! Próbálkozzon újra!',16,1)

-------------------------------------------------------------------------------
-- a megadott vonalkódsávból esetlegesen létrejött új vonalkódsávok létrehozása
-------------------------------------------------------------------------------
-- részsávbetöltéskor az eredeti sávból keletkezik 1 betöltött részsáv, 
-- plusz 0,1 vagy 2 új betöltetlen részsáv ... ezeket most itt allokálni kell!!!

if @Savkezd < @FirstKod
begin 
-- "@SavKezd" -tol "@FirstKod-1" -ig!
   select @UjSavKezd = @SavKezd, 
		-- BLG_1940
          --@UjSavVege = dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@FirstKod,1,12))-1, @vektor )
          @UjSavVege = dbo.fn_GetSavosVonalkod_Prefix(@FeltoltoSzervezet_Id)+dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@FirstKod,len(@FirstKod)-12,12))-1, @vektor )

   insert into dbo.KRT_BarkodSavok
          (Org, Csoport_Id_Felelos,  SavKezd,    SavVege,    SavType,  SavAllapot)
   values (@Org, @Csoport_Id_Felelos, @UjSavKezd, @UjSavVege, @SavType, 'A'       )
 
   if @@error <> 0
      RAISERROR('[5413A]',16,1) -- EZ VÉLETLEN KIMARADT, KÉNE MAJD NEKI EGY RENDES ÚJ SORSZÁM!!!! -- mj: AA
      --RAISERROR('KRT_BarkodSavok insert hiba!',16,1)
end

if @SavVege > @LastKod
begin 
-- "@LastKod+1" -tol "@SavVege" -ig!
-- BLG_1940
   --select @UjSavKezd = dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@LastKod,1,12))+1, @vektor ),
     select @UjSavKezd = dbo.fn_GetSavosVonalkod_Prefix(@FeltoltoSzervezet_Id)+dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@LastKod,len(@LastKod)-12,12))+1, @vektor ), 
          @UjSavVege = @SavVege

   insert into dbo.KRT_BarkodSavok
          (Org, Csoport_Id_Felelos,  SavKezd,    SavVege,    SavType,  SavAllapot)
   values (@Org, @Csoport_Id_Felelos, @UjSavKezd, @UjSavVege, @SavType, 'A'       )
 
   if @@error <> 0
      RAISERROR('[5413A]',16,1) -- EZ VÉLETLEN KIMARADT, KÉNE MAJD NEKI EGY RENDES ÚJ SORSZÁM!!!! -- mj: AA
      --RAISERROR('KRT_BarkodSavok insert hiba!',16,1)
end
  
----------------------------------------------------------------------------
-- a betöltött vonalkódsáv (vagy vonalkódrészsáv) vonalkódjainak létrehozása
----------------------------------------------------------------------------
-- "@FirstKod" -tól "@LastKod" -ig
-- BLG_1940
--select @FirstKod_num = convert(numeric(12),substring(@FirstKod,1,12)),
--       @LastKod_num  = convert(numeric(12),substring(@LastKod,1,12))
select @FirstKod_num = convert(numeric(12),substring(@FirstKod,len(@FirstKod)-12,12)),
       @LastKod_num  = convert(numeric(12),substring(@LastKod,len(@LastKod)-12,12))

-- vonalkódok felírása ciklusban
--select @AktKod_num  = @FirstKod_num 
--while  @AktKod_num <= @LastKod_num
--begin
--
--   select @AktKod = dbo.fn_barkod_checksum_addn( @AktKod_num )
--
--   insert into dbo.KRT_Barkodok
--          (Id,      Kod,     KodType,  Allapot )
--   values (newid(), @AktKod, @SavType, 'S'     )
-- 
--   if @@error <> 0
--      RAISERROR('[5413B]',16,1) -- EZ VÉLETLEN KIMARADT, KÉNE MAJD NEKI EGY RENDES ÚJ SORSZÁM!!!! -- mj: AA
--      --RAISERROR('KRT_Barkodok insert hiba!',16,1)
--  
--   select @AktKod_num = @AktKod_num + 1
--
--end

-- vonalkódok felírása ciklusban Ver 2.0
DECLARE @i numeric;
DECLARE @db INT;
SET @db = @LastKod_num - @FirstKod_num + 1;

SELECT 0 AS KodNum INTO #tempTable;
SET @i = 1;
WHILE @i < @db
BEGIN
	INSERT INTO [#tempTable] (
		KodNum
	) VALUES ( @i);
	SET @i = @i+1;
END


insert into dbo.KRT_Barkodok
      (Kod,     KodType,  Allapot )
SELECT dbo.fn_GetSavosVonalkod_Prefix(@FeltoltoSzervezet_Id) + dbo.fn_barkod_checksum_addn( KodNum + @FirstKod_num, @vektor ), @SavType, 'S' FROM [#tempTable];
   
DROP TABLE [#tempTable];

------------------
if @sajat_tranz = 1  
   COMMIT TRANSACTION

select @ResultUid = @myid

if @teszt = 1
   print '@myid = '+ isnull( convert(varchar(50),@myid), 'NULL')
 
END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   if @teszt = 1
      print '@myid = '+ isnull( convert(varchar(50),@myid), 'NULL')

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------