﻿create procedure [dbo].[sp_BarkodSav_Igenyles]    
                 @Szervezet_Id         uniqueidentifier,
                 @SavType              char(1),                 -- 'N','P','G'
                 @FoglalandoDarab      integer,                 -- 1,..., -- a típustól függ
                 @ResultUid            uniqueidentifier OUTPUT
         
AS
/*
--
-- KÉSZÍTETTE:   Ádám András       2007.11.12
-- MÓDOSÍTOTTA:

-- FELADATA:
       Egy adott szervezet számára igényel Nyomdától ill. helyi Printelés által egy vonalkódsávot,
       amelynek csak a méretét (azaz az igényelt vonalkódok darabszámát) adja meg,
       vagy egyszeruen csak Generáltat azonnali felhasználásra egy darab vonalkódot.
       Sikeres végrehajtás esetén 
       vagy a vonalkódsáv id-je (KRT_BarkodSavok tábla)
       vagy a vonalkód id-je    (KRT_Barkodok tábla) 
       kerül visszaadásra.

       A létrejött KRT_BarkodSavok tétel típusa (SavType) 'N','P', állapota (SavAllapot) 'A' lesz,
       a sáv legkisebb ill. legnagyobb vonalkódját pedig SavKezd ill. SavVege tartalmazza.

       A létrejött KRT_Barkodok tétel típusa (KodType) 'G', állapota (Allapot) 'S' lesz,
       magát a generált vonalkódot pedig Kod tartalmazza.
           
-- mj: a max integer 2 147 483 648 azaz kb 2 milliárd, tehát elvileg kb ennyit lehetne egyszerre lefoglalni,
       azonban a vonalkód típusok szerinti vizsgálatnál ennél lényegesen kevesebbet engedünk majd meg ...

-- pl:
declare @ID uniqueidentifier
exec [dbo].[sp_BarkodSav_Igenyles] 
     'FF626847-F433-4B80-83D4-64AEA5C147FF', -- @Szervezet_Id
     'P',                                    -- @SavType
     10000,                                  -- @FoglalandoDarab
     @ID output                              -- @ResultUid

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1
 
declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
select  @ResultUid = NULL

declare @error           int
declare @rowcount        int

------- 
declare @Tartomany_Id    uniqueidentifier
declare @TartomanyKezd   numeric(12)
declare @TartomanyHossz  numeric(12)
declare @FoglaltDarab    numeric(12)

declare @SavKezd_num     numeric(12)
declare @SavVege_num     numeric(12)
declare @SavKezd         nvarchar(100)
declare @SavVege         nvarchar(100)

--------------------------------
-- input paraméterek ellenorzése
--------------------------------

-- NULLITÁS-vizsgálat
if @Szervezet_Id is NULL
   RAISERROR('[54101]',16,1)
   --RAISERROR('@Szervezet_Id paraméter nem lehet NULL!',16,1)

   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByCsoportId(@Szervezet_Id)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	END


if @SavType is NULL
   RAISERROR('[54102]',16,1)
   --RAISERROR('@SavType paraméter nem lehet NULL!',16,1)

if @FoglalandoDarab is NULL
   RAISERROR('[54103]',16,1)
   --RAISERROR('@FoglalandoDarab paraméter nem lehet NULL!',16,1)

-- létezo és megfelelo jogosítvánnyal bír a szervezet???
-- ...

-- korrekt a sávtípus?
if @SavType not in ('G','P','N')
   RAISERROR('[54104]',16,1)
   --RAISERROR('@SavType nem megengedett vonalkód típus! (csak G,P,N lehet)',16,1)

-- foglalandó darabszám ellenorzés
if @SavType = 'G' and @FoglalandoDarab <> 1
   RAISERROR('[54105]',16,1)
   --RAISERROR('Generált vonalkód egyszerre csak 1 igényelheto!',16,1)

if @SavType = 'P' and @FoglalandoDarab not between 1 and 100000
   RAISERROR('[54106]',16,1)
   --RAISERROR('Helyben nyomtatott vonalkód egyszerre min 1, max 100000 igényelheto!',16,1)

if @SavType = 'N' and @FoglalandoDarab not between 1 and 1000000
   RAISERROR('[54107]',16,1)
   --RAISERROR('Nyomdai vonalkód egyszerre min 1, max 1000000 igényelheto!',16,1)

--------------------------------------------------------------------------
-- a szervezetnek ill. a vonalkód típusnak megfelelo tartomány azonosítása
--------------------------------------------------------------------------
select @Tartomany_Id = NULL
select @Tartomany_Id = ts.Tartomany_Id
  from dbo.KRT_Tartomanyok_Szervezetek ts
 where ts.TartType = @SavType
   and ts.Csoport_Id_Felelos = @Szervezet_Id 
 

if @@error <> 0
   RAISERROR('[54108]',16,1)
   --RAISERROR('KRT_Tartomanyok_Szervezetek select hiba!',16,1)

if @Tartomany_Id is NULL
begin

   select @Tartomany_Id = ts.Tartomany_Id
     from dbo.KRT_Tartomanyok_Szervezetek ts
    where ts.TartType = @SavType
      and ts.Csoport_Id_Felelos is NULL
   
   if @@error <> 0
      RAISERROR('[54108]',16,1)
      --RAISERROR('KRT_Tartomanyok_Szervezetek select hiba!',16,1)
   
end

if @Tartomany_Id is NULL
   RAISERROR('[54109]',16,1)
   --RAISERROR('Az adott szervezetnek, adott típusú vonalkód tartománya nem létezik! Egyeztessen a rendszergazdával!!!',16,1)

if @teszt = 1
   print '@Tartomany_Id = '+ convert(varchar(100),@Tartomany_Id)

---------------------------------------------------------
-- foglalás (allokálás) a megfelelo vonalkód tartományból 
---------------------------------------------------------

-- a konkurens foglalások miatt ez itt egy nagyon lényeges muvelet,
-- amíg ez a tranzakció fut, addig egy másik erre kell, hogy várakozzék! így jó!!
update dbo.KRT_Tartomanyok
   set FoglaltDarab = FoglaltDarab + @FoglalandoDarab -- itt elvileg lehet túlcsordulás
 where Id = @Tartomany_Id
 
if @@error <> 0 OR @@rowcount = 0
   RAISERROR('[54110]',16,1)
   --RAISERROR('KRT_Tartomanyok update hiba!',16,1)
 
-- az update utáni értékek visszaolvasása ...
select @TartomanyKezd  = TartomanyKezd,
       @TartomanyHossz = TartomanyHossz,
       @FoglaltDarab   = FoglaltDarab
  from dbo.KRT_Tartomanyok
 where Id = @Tartomany_Id

if @@error <> 0 OR @@rowcount = 0
   RAISERROR('[54111]',16,1)
   --RAISERROR('KRT_Tartomanyok select hiba!',16,1)

-- ... és ellenorzése 
if @FoglaltDarab >  @TartomanyHossz  -- esetleg ki lehetne iratni, hogy mennyi szabadhely van még!?
   RAISERROR('[54112]',16,1)
   --RAISERROR('Az érintett vonalkód tartományban már nem fogalható le a kért számú vonalkód! Egyeztessen a rendszergazdával!!!',16,1)

-- a lefoglalandó sáv kezdo és vég vonalkód-alapjának meghatározása
-- (mj: ha nem korrekt a tartomány megadása, akkor itt is lehet túlcsordulás ...)
select @SavKezd_num = @TartomanyKezd + @FoglaltDarab - @FoglalandoDarab
select @SavVege_num = @TartomanyKezd + @FoglaltDarab - 1

if @teszt = 1
   print '@SavKezd_num = '+ convert(varchar(12),@SavKezd_num) + ', ' +
         '@SavVege_num = '+ convert(varchar(12),@SavVege_num)

--ellenörzo vektor lekérése
DECLARE @vektor NVARCHAR(12)
SET @vektor = dbo.fn_GetRendszerparameterBySzervezet(@Szervezet_Id,'VONALKOD_ELLENORZO_VEKTOR')  

IF @vektor = NULL
BEGIN
	--rendszerparaméter nem található 
	RAISERROR('[54159]',16,1)
END

IF @vektor = 'Error_50202'
BEGIN
	--org nem határozható meg
	RAISERROR('[50202]',16,1)
END 

if @teszt = 1
	PRINT '@vektor = ' + @vektor

	-- komplett karakteres vonalkódok eloállítása
	-- BLG_1940
	--select @SavKezd = dbo.fn_barkod_checksum_addn(@SavKezd_num, @vektor),
	--	   @SavVege = dbo.fn_barkod_checksum_addn(@SavVege_num, @vektor)
	-- komplett karakteres vonalkódok eloállítása
	select @SavKezd = dbo.fn_GetSavosVonalkod_Prefix(@Szervezet_Id) + dbo.fn_barkod_checksum_addn(@SavKezd_num, @vektor),
		   @SavVege = dbo.fn_GetSavosVonalkod_Prefix(@Szervezet_Id) + dbo.fn_barkod_checksum_addn(@SavVege_num, @vektor)

if @teszt = 1
   print '@SavKezd = '+ @SavKezd + ', ' +
         '@SavVege = '+ @SavVege

DECLARE @InsertedRow TABLE (id uniqueidentifier)

if @SavType in ('P','N')
begin
-- az új vonalkódsáv felírása (P vagy N sávtípus esetén)

   -- MAJD ELDÖNTENI, hogy miképp történjen az insert!!!!! (A = Allokált állapot)
   insert into dbo.KRT_BarkodSavok
          (Org, Csoport_Id_Felelos, SavKezd,  SavVege,  SavType,  SavAllapot ) OUTPUT INSERTED.Id INTO @InsertedRow
   values (@Org, @Szervezet_Id, @SavKezd, @SavVege, @SavType, 'A')
 
   if @@error <> 0
      RAISERROR('[54113]',16,1)
      --RAISERROR('KRT_BarkodSavok insert hiba!',16,1)
end
else
begin
-- az új generált vonalkód felírása (G sávtípus esetén)

   -- MAJD ELDÖNTENI, hogy miképp történjen az insert!!!!! (S = Szabad állapot)
   insert into dbo.KRT_Barkodok
          (Kod,      KodType,  Allapot ) OUTPUT INSERTED.Id INTO @InsertedRow
   values (@SavKezd, @SavType, 'S'     )
 
   if @@error <> 0
      RAISERROR('[54114]',16,1)
      --RAISERROR('KRT_Barkodok insert hiba!',16,1)

end

if @sajat_tranz = 1  
   COMMIT TRANSACTION
   
select @ResultUid = id from @InsertedRow   

if @teszt = 1
   print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')
 
END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   if @teszt = 1
      print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------