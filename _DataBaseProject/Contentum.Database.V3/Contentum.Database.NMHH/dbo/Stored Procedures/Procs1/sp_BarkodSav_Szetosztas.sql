﻿create procedure [dbo].[sp_BarkodSav_Szetosztas]    
                @BarkodSav_Id         uniqueidentifier,
                @FirstKod             nvarchar(100),
                @LastKod              nvarchar(100),
                @Szervezet_Id         uniqueidentifier,-- = NULL, -- jelenlegi tulajdonos szervezet
                @UjSzervezet_Id       uniqueidentifier,        -- új tulajdonos szervezet
                @ResultUid            uniqueidentifier OUTPUT
         
AS
/*
--
-- KÉSZÍTETTE:   Ádám András       2007.11.15
-- MÓDOSÍTOTTA:

-- FELADATA:
       A Nyomdától igényelt vagy helyileg Printelt vonalkódsáv szétosztása,
       pontosabban sáv egy részsávának (ami persze lehet a teljes sáv is) 
       valamely más szervezethez való hozzárendelése. 
       Az "átadást" csak a jelenlegi tulajdonos szervezet végezheti.
       Az "átadandó" részsáv kijelölése, a legkisebb és legnagyobb vonalkód megadásával történik.
       Az "átadás" után az eredeti sávból megmaradt esetleges részsávok természetesen 
       az eredeti tulajdonos szervezetnél maradnak. 
       A FELADAT-ra csak olyan vonalkódsáv jelölheto ki, mely P v. N - típusú (jelenleg mind ilyen),
       és H státuszú (tehát már fel van töltve, de nem elhasznált vagy törölt),
       és amelynek az "átadandó" részsávhoz tartozó összes vonalkódja S (szabad) státuszú 
       (tehát még nem felhasznált vagy törölt).

-- pl:
declare @ID uniqueidentifier
exec [dbo].[sp_BarkodSav_Szetosztas] 
     '28653A1E-6BA2-4459-A51C-359CFAB5C690', -- @BarkodSav_Id
     '1000000000001',                        -- @FirstKod
     '1000000099991',                        -- @LastKod
     'FF626847-F433-4B80-83D4-64AEA5C147FF', -- @Szervezet_Id
     'FF626847-F433-4B80-83D4-64AEA5C147FF', -- @UjSzervezet_Id
     @ID output                              -- @ResultUid

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1
 
declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
select  @ResultUid = NULL
--
declare @myid            uniqueidentifier
select  @myid = @BarkodSav_Id

declare @error           int
declare @rowcount        int

------- 
declare @Csoport_Id_Felelos uniqueidentifier
declare @SavKezd         nvarchar(100)
declare @SavVege         nvarchar(100)
declare @SavType         char(1)
declare @SavAllapot      char(1)

declare @UjSavKezd       nvarchar(100)
declare @UjSavVege       nvarchar(100)

declare @FirstKod_num    numeric(12)
declare @LastKod_num     numeric(12)
declare @AktKod          nvarchar(100)

declare @SzabadDarab     int
declare @ReszSavDarab    int

--------------------------------
-- input paraméterek ellenorzése
--------------------------------

-- input paraméterek kiiratása
if @teszt = 1
   print '@BarkodSav_Id = '+ isnull( convert(varchar(50),@BarkodSav_Id), 'NULL') + ', ' +
         '@FirstKod = '+ isnull(@FirstKod, 'NULL') + ', ' +
         '@LastKod = '+ isnull(@LastKod, 'NULL') + ', ' +
         '@Szervezet_Id = '+ isnull( convert(varchar(50),@Szervezet_Id), 'NULL') + ', ' +
         '@UjSzervezet_Id = '+ isnull( convert(varchar(50),@UjSzervezet_Id), 'NULL')

-- NULLITÁS-vizsgálat
if @BarkodSav_Id is NULL
   RAISERROR('[54139]',16,1)
   --RAISERROR('@BarkodSav_Id paraméter nem lehet NULL!',16,1)

DECLARE @Org uniqueidentifier
SET @Org = dbo.fn_GetOrgByCsoportId(@Szervezet_Id)
if (@Org is null)
begin
	RAISERROR('[50202]',16,1)
END

if @FirstKod is NULL
   RAISERROR('[54140]',16,1)
   --RAISERROR('@FirstKod paraméter nem lehet NULL!',16,1)

if @LastKod is NULL
   RAISERROR('[54141]',16,1)
   --RAISERROR('@LastKod paraméter nem lehet NULL!',16,1)

/*
if @Szervezet_Id NULL
   RAISERROR('@Szervezet_Id paraméter nem lehet NULL!',16,1)
*/

if @UjSzervezet_Id is NULL
   RAISERROR('[541xx]',16,1)
   --RAISERROR('@UjSzervezet_Id paraméter nem lehet NULL!',16,1)
   
--ellenörzo vektor lekérése
DECLARE @vektor NVARCHAR(12)
SET @vektor = dbo.fn_GetRendszerparameterBySzervezet(@Szervezet_Id,'VONALKOD_ELLENORZO_VEKTOR')  

IF @vektor = NULL
BEGIN
	--rendszerparaméter nem található 
	RAISERROR('[54159]',16,1)
END

IF @vektor = 'Error_50202'
BEGIN
	--org nem határozható meg
	RAISERROR('[50202]',16,1)
END 

if @teszt = 1
	PRINT '@vektor = ' + @vektor   

-- korrekt-ek az input paraméterek?
	-- BLG_1940
--if dbo.fn_barkod_check(@FirstKod, @vektor) <> 0
if dbo.fn_barkod_check(right(@FirstKod,13), @vektor) <> 0
   RAISERROR('[54142]',16,1)
   --RAISERROR('@FirstKod paraméter nem korrekt vonalkód!',16,1)

-- BLG_1940
--if dbo.fn_barkod_check(@LastKod, @vektor) <> 0
if dbo.fn_barkod_check(right(@LastKod,13), @vektor) <> 0
   RAISERROR('[54143]',16,1)
   --RAISERROR('@LastKod paraméter nem korrekt vonalkód!',16,1)

if @FirstKod > @LastKod
   RAISERROR('[54144]',16,1)
   --RAISERROR('@FirstKod > @LastKod nem lehet!',16,1)

-- sáv létezés ellenorzés
select @Csoport_Id_Felelos = NULL, @SavKezd = NULL, @SavVege = NULL, @SavType = NULL, @SavAllapot = NULL 
select @Csoport_Id_Felelos = Csoport_Id_Felelos,
       @SavKezd            = SavKezd,
       @SavVege            = SavVege,
       @SavType            = SavType,
       @SavAllapot         = SavAllapot
  from dbo.KRT_BarkodSavok
 where Id = @BarkodSav_Id
 
if @@error <> 0
   RAISERROR('[54145]',16,1)
   --RAISERROR('KRT_BarkodSavok select hiba!',16,1)

-- a megadott sáv paramétereinek kiiratása
if @teszt = 1
   print '@Csoport_Id_Felelos = '+ isnull( convert(varchar(50),@Csoport_Id_Felelos), 'NULL') + ', ' +
         '@SavKezd = '+ isnull(@SavKezd, 'NULL') + ', ' +
         '@SavVege = '+ isnull(@SavVege, 'NULL') + ', ' +
         '@SavType = '+ isnull(@SavType, 'NULL') + ', ' +
         '@SavAllapot = '+ isnull(@SavAllapot, 'NULL')

if @Csoport_Id_Felelos is NULL
   RAISERROR('[54146]',16,1)
   --RAISERROR('A megadott vonalkódsáv nem létezik!',16,1)

--if @Szervezet_Id is NULL
--   select @Szervezet_Id = @Csoport_Id_Felelos

-- CR#1971 (EB 2009.03.11): Kliens oldalon ellenorizzük, hogy a végrehajtó felhasználó tagje-e a felelos csoportnak (vagy azonos vele)
--if @Szervezet_Id <> @Csoport_Id_Felelos
--   RAISERROR('[54147]',16,1)
--   --RAISERROR('A sáv jelenlegi tulajdonosa oszthatja csak szét a sáv vonalkódjait!',16,1)

if @Szervezet_Id = @UjSzervezet_Id
   RAISERROR('[54148]',16,1)
   --RAISERROR('A sáv jelenlegi és új tulajdonos szervezete különbözo kell, hogy legyen!',16,1)

if @SavAllapot <> 'H'
   RAISERROR('[54149]',16,1)
   --RAISERROR('A megadott vonalkódsáv még/már nem osztható szét!',16,1)

if @SavType not in ('N','P')
   RAISERROR('[54150]',16,1)
   --RAISERROR('Nyomdai vagy helyileg Printelt vonalkódsáv osztható csak szét!',16,1)

if @FirstKod not between @SavKezd and @SavVege
   RAISERROR('[54151]',16,1)
   --RAISERROR('@FirstKod paraméter nem esik megadott vonalkódsávba!',16,1)
      
if @LastKod not between @SavKezd and @SavVege
   RAISERROR('[54152]',16,1)
   --RAISERROR('@LastKod paraméter nem esik megadott vonalkódsávba!',16,1)

-- az "átadandó" részsávban minden vonalkód szabad (S) még?
select @SzabadDarab = 0
select @SzabadDarab = count(*)
  from dbo.KRT_Barkodok
 where Kod between @FirstKod and @LastKod
   and Allapot = 'S'
 
if @@error <> 0
   RAISERROR('[54153]',16,1)
   --RAISERROR('KRT_Barkodok select hiba!',16,1)

-- BLG_1940
--select @FirstKod_num = convert(numeric(12),substring(@FirstKod,1,12)),
--       @LastKod_num  = convert(numeric(12),substring(@LastKod,1,12))
select @FirstKod_num = convert(numeric(12),substring(right(@FirstKod,13),1,12)),
       @LastKod_num  = convert(numeric(12),substring(right(@LastKod,13),1,12))

select @ReszSavDarab = @LastKod_num - @FirstKod_num + 1

if @ReszSavDarab <> @SzabadDarab
   RAISERROR('[54154]',16,1)
   --RAISERROR('A részsáv nem adható át, mert nem minden vonalkódja szabad!',16,1)

------------------------------------------------------
-- a megadott vonalkódsáv paramétereinek aktualizálása
------------------------------------------------------
-- (a konkurens kezelés miatt arra is ügyelve, hogy esetleg nem változtak-e meg ...)
update dbo.KRT_BarkodSavok
   set Csoport_Id_Felelos = @UjSzervezet_Id,
       SavAllapot         = @SavAllapot, 
       SavKezd            = @FirstKod,
       SavVege            = @LastKod
 where Id                 = @BarkodSav_Id
   and Csoport_Id_Felelos = @Csoport_Id_Felelos
   and SavKezd            = @SavKezd
   and SavVege            = @SavVege
   and SavType            = @SavType
   and SavAllapot         = @SavAllapot

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   RAISERROR('[54155]',16,1)
   --RAISERROR('KRT_BarkodSavok update hiba!',16,1)

if @rowcount = 0
   RAISERROR('[54156]',16,1)
   --RAISERROR('A megadott vonalkódsáv paraméterei idoközben megváltoztak! Próbálkozzon újra!',16,1)

-------------------------------------------------------------------------------
-- a megadott vonalkódsávból esetlegesen létrejött új vonalkódsávok létrehozása
-------------------------------------------------------------------------------
-- részsávátadásakor az eredeti sávból keletkezik 1 átadott részsáv (ami az új tulajé lesz), 
-- plusz 0,1 vagy 2 új részsáv (ami az régi tulajé marad)... ezeket most itt létre kell hozni!!!

if @Savkezd < @FirstKod
begin 
-- "@SavKezd" -tol "@FirstKod-1" -ig!
   select @UjSavKezd = @SavKezd, 
-- BLG_1940
          --@UjSavVege = dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@FirstKod,1,12))-1, @vektor )
          @UjSavVege = dbo.fn_GetSavosVonalkod_Prefix(@Szervezet_Id) + dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@FirstKod,len(@FirstKod)-12,12))-1, @vektor )

   insert into dbo.KRT_BarkodSavok
          (Org, Csoport_Id_Felelos,  SavKezd,    SavVege,    SavType,  SavAllapot )
   values (@Org, @Csoport_Id_Felelos, @UjSavKezd, @UjSavVege, @SavType, @SavAllapot)
 
   if @@error <> 0
      RAISERROR('[54157]',16,1)
      --RAISERROR('KRT_BarkodSavok insert hiba!',16,1)
end

if @SavVege > @LastKod
begin 
-- "@LastKod+1" -tol "@SavVege" -ig!
	-- BLG_1940
   --select @UjSavKezd = dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@LastKod,1,12))+1, @vektor ), 
   select @UjSavKezd = dbo.fn_GetSavosVonalkod_Prefix(@Szervezet_Id) + dbo.fn_barkod_checksum_addn( convert(numeric(12),substring(@LastKod,len(@LastKod)-12,12))+1, @vektor ), 
          @UjSavVege = @SavVege

   insert into dbo.KRT_BarkodSavok
          (Org, Csoport_Id_Felelos,  SavKezd,    SavVege,    SavType,  SavAllapot )
   values (@Org, @Csoport_Id_Felelos, @UjSavKezd, @UjSavVege, @SavType, @SavAllapot)
 
   if @@error <> 0
      RAISERROR('[54158]',16,1)
      --RAISERROR('KRT_BarkodSavok insert hiba!',16,1)
end
  
------------------
if @sajat_tranz = 1  
   COMMIT TRANSACTION

select @ResultUid = @myid

if @teszt = 1
   print '@myid = '+ isnull( convert(varchar(50),@myid), 'NULL')
 
END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   if @teszt = 1
      print '@myid = '+ isnull( convert(varchar(50),@myid), 'NULL')

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------