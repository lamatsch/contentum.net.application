﻿CREATE procedure [dbo].[sp_CSVExport_Postazas]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKuldemenyek.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY


set nocount on

   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  'select 
  	   ROW_NUMBER() over (ORDER BY EREC_KuldKuldemenyek.id) as Sorszám
      ,EREC_KuldKuldemenyek.NevSTR_Bekuldo as Címzett
      , 
	  CASE
      WHEN KRT_Cimek.TelepulesNev = '''' or KRT_Cimek.TelepulesNev is null
      THEN 
			LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 3 * 
			FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc)))
	  ELSE 
		    KRT_Cimek.TelepulesNev
	  END as Település
	  

	  ,
	   CASE
      WHEN KRT_Cimek.tipus = '''' or KRT_Cimek.tipus is null
      THEN 
			LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 4 * 
			FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc)))
	  ELSE 
		    case KRT_Cimek.tipus 
	when ''02'' then ''Pf. '' + KRT_Cimek.Hazszam
	else KRT_Cimek.KozteruletNev + '' '' + KRT_Cimek.KozteruletTipusNev + '' '' + KRT_Cimek.Hazszam
        end
	  END as Cím


	 ,
	   CASE
      WHEN KRT_Cimek.IRSZ = '''' or KRT_Cimek.IRSZ is null
      THEN 
			LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 2 * 
			FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc)))
	  ELSE 
		    KRT_Cimek.IRSZ
	  END as Irányítószám
	  
	  
	  , EREC_PldIratpeldanyok.Azonosito as Azonosító
    
    FROM dbo.EREC_KuldKuldemenyek EREC_KuldKuldemenyek
  left JOIN dbo.KRT_Cimek KRT_Cimek ON EREC_KuldKuldemenyek.Cim_Id = KRT_Cimek.Id
  FULL OUTER JOIN dbo.EREC_Kuldemeny_IratPeldanyai EREC_Kuldemeny_IratPeldanyai ON EREC_KuldKuldemenyek.Id = EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id 
  FULL OUTER JOIN dbo.EREC_PldIratpeldanyok EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.id = EREC_Kuldemeny_IratPeldanyai.Peldany_Id
	'
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
	print @sqlcmd
	exec (@sqlcmd);
	
	-- találatok száma és oldalszám

  

	
 


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

