﻿CREATE PROCEDURE [dbo].[sp_CallWebService]
	@Url nvarchar(4000),
	@UserName nvarchar(400) = '',
	@Password nvarchar(400) = ''
AS
BEGIN

	Declare @obj INT
	DECLARE @hResult int
	DECLARE @source varchar(255), @desc varchar(255) 
	
	exec sp_OACreate 'MSXML2.ServerXMLHTTP', @obj OUT

	-- tanúsítvány hibák figyelmen kívül hagyása
	exec sp_OAMethod @obj , 'setOption', NULL, 2, 13056
	
	exec @hResult = sp_OAMethod @obj, 'Open', NULL, 'GET', @Url, false, @UserName, @Password
	
	IF @hResult <> 0 
	BEGIN
		  EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		  exec sp_OADestroy @obj
		  Raiserror('Open failed: %s - %s',16,1,@desc, @Url)
		  return
	END

	exec @hResult = sp_OAMethod @obj, 'send'
	
	IF  @hResult <> 0 
	BEGIN
		  EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		  exec sp_OADestroy @obj
		  Raiserror('Send failed: %s - %s',16,1,@desc, @Url)
		  return
	 END

	declare @statusText varchar(1000), @status varchar(1000), @response VARCHAR(8000)
	-- Get status, response text 
	exec sp_OAGetProperty @obj, 'StatusText', @statusText out
	exec sp_OAGetProperty @obj, 'Status', @status out
	exec sp_OAGetProperty @obj, 'responseText', @response OUT
	select @status, @statusText, @response

	exec sp_OADestroy @obj
	
	if @status <> '200'
		Raiserror('Response failed: %s - %s - %s', 16, 1, @status, @statusText, @Url)
END