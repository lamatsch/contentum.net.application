﻿CREATE PROCEDURE [dbo].[sp_ChangeJogtargyRightInherit]
	@JogtargyId			uniqueidentifier,	--örökös jogtárgy ID-ja
	@SzuloJogtargyId	uniqueidentifier,	--kitol örököljön a jogtárgy
	@UjOroklesTipus		char(1) = 'S',		--'X': öröklodést elvág; 'S': öröklodést engedélyez
	@OroklesMasolas		char(1)	= 'N',		--'I': ACL felmenoinek jogainak másolása az elvágott ACL alá; 'N': nem történik semmi
	@OroklottJogszint	char(1) = 'I'		--örkölött jogszint
AS
/*********************************************************************************************************
* Elvágja/engedélyezi a KRT_ACL táblában a paraméterben megadott @ScopeId-ju ACL öröklodését.
* Ha a @p_oroklesMasolas 'I' értéket kap, az ACL összes felmenojéhez tartozó jogosultság másolása az ACL-re.
*
*********************************************************************************************************/
BEGIN
begin try
	SET NOCOUNT ON;

	if (@UjOroklesTipus ='X') 
	begin --öröklés leválasztása

		if (@OroklesMasolas = 'I')
		begin --öröklés másolás
			insert into KRT_Jogosultak(Csoport_Id_Jogalany,Jogszint,Obj_Id,Kezi)
				select KRT_Jogosultak.Csoport_Id_Jogalany,KRT_Jogosultak.Jogszint,@JogtargyId,'N'
				  from KRT_Jogosultak KRT_Jogosultak 
				  where KRT_Jogosultak.Obj_Id in
				  (
					select KRT_OBJEKTUM_ACLEK.Obj_Id
					  from KRT_OBJEKTUM_ACLEK
					  where KRT_OBJEKTUM_ACLEK.Obj_Id = @SzuloJogtargyId
				  )
		end --öröklés másolás

		/***********KRT_OBJEKTUMJOG_OROKLESEK-bol törlés**************************/
		delete from KRT_OBJEKTUMJOG_OROKLESEK where Obj_Id = @SzuloJogtargyId and Obj_Id_Orokos = @JogtargyId;

		/***********KRT_OBJEKTUM_ACLEK-bol törlés********************************/
		delete from KRT_OBJEKTUM_ACLEK where
			Obj_Id in
			(select Obj_Id from KRT_OBJEKTUM_ACLEK where Obj_Id_Orokos = @JogtargyId)
			AND Obj_Id_Orokos in
			(select Obj_Id_Orokos from KRT_OBJEKTUM_ACLEK where Obj_Id = @JogtargyId)
			AND Obj_Id != Obj_Id_Orokos;

		insert into KRT_OBJEKTUM_ACLEK(Obj_Id,Obj_Id_Orokos,OrokolhetoJogszint)
			select uj.* from
				(select distinct KRT_OBJEKTUM_ACLEKSzulok.Obj_Id,KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos,KRT_OBJEKTUM_ACLEKSzulok.OrokolhetoJogszint
					from KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKSzulok,
						 KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKGyerekek
					where KRT_OBJEKTUM_ACLEKSzulok.Obj_Id_Orokos in (select Obj_Id from KRT_OBJEKTUMJOG_OROKLESEK where Obj_Id_Orokos = @JogtargyId)
					  AND KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos = @JogtargyId) uj


--		delete from KRT_OBJEKTUM_ACLEK where ACL_ID in 
--		(
--			select KRT_OBJEKTUM_ACLEK.ACL_ID
--			  from KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK
--			  where KRT_OBJEKTUM_ACLEK.Obj_Id = @SzuloJogtargyId
--		) AND Obj_Id in
--		(
--		select KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id_Orokos
--		  from KRT_OBJEKTUMJOG_OROKLESEK 
--		  where KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id = @JogtargyId
--		)

	end --öröklés leválasztása

	else if (@UjOroklesTipus ='S')
		begin --öröklodés felépítése

		--**********KRT_OBJEKTUMJOG_OROKLESEK táblába 1 elem beillesztése
		insert into KRT_OBJEKTUMJOG_OROKLESEK(Obj_Id,Obj_Id_Orokos,JogszintOrokolheto)
			values(@SzuloJogtargyId,@JogtargyId,@OroklottJogszint);

		if @OroklottJogszint = 'I'
		begin
			--**********KRT_OBJEKTUM_ACLEK táblába oroklés cachelése
			insert into KRT_OBJEKTUM_ACLEK(Obj_Id,Obj_Id_Orokos,OrokolhetoJogszint)
				select KRT_OBJEKTUM_ACLEKSzulok.Obj_Id,KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos, KRT_OBJEKTUM_ACLEKSzulok.OrokolhetoJogszint
					from KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKSzulok,
						 KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKGyerekek
					where KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id = @JogtargyId
					  AND KRT_OBJEKTUM_ACLEKSzulok.Obj_Id_Orokos = @SzuloJogtargyId
					  AND not exists (select 1 from KRT_OBJEKTUM_ACLEK where Obj_Id = KRT_OBJEKTUM_ACLEKSzulok.Obj_Id and Obj_Id_Orokos = KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos)
		end
		else
		begin
			--**********KRT_OBJEKTUM_ACLEK táblába oroklés cachelése
			insert into KRT_OBJEKTUM_ACLEK(Obj_Id,Obj_Id_Orokos,OrokolhetoJogszint)
				select KRT_OBJEKTUM_ACLEKSzulok.Obj_Id,KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos, @OroklottJogszint
					from KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKSzulok,
						 KRT_OBJEKTUM_ACLEK as KRT_OBJEKTUM_ACLEKGyerekek
					where KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id = @JogtargyId
					  AND KRT_OBJEKTUM_ACLEKSzulok.Obj_Id_Orokos = @SzuloJogtargyId
					  AND not exists (select 1 from KRT_OBJEKTUM_ACLEK where Obj_Id = KRT_OBJEKTUM_ACLEKSzulok.Obj_Id and Obj_Id_Orokos = KRT_OBJEKTUM_ACLEKGyerekek.Obj_Id_Orokos)
		end

		end --öröklodés felépítése
		else 
		begin --hiba
			raiserror('[50401]',16,1);
			return @@error;
		end
end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END