﻿
Create procedure [dbo].[sp_CheckLogin]    
             @FelhasznaloNev     Nvarchar(100),
	         @Jelszo     Nvarchar(100),
             @Tipus     Nvarchar(100)
         
 AS

BEGIN TRY
 
set nocount on;


select * from KRT_Felhasznalok
where lower(UserNev) = lower(@FelhasznaloNev)
and Tipus = @Tipus
and ErvVege > getdate()
and ErvKezd < getdate()

   
END TRY
BEGIN CATCH
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH