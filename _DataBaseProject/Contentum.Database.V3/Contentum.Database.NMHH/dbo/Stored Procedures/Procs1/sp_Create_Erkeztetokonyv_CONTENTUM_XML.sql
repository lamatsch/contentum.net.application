﻿
CREATE PROCEDURE [dbo].[sp_Create_Erkeztetokonyv_CONTENTUM_XML]
       @Iktatokonyv_ID          uniqueidentifier
      ,@ExecutorUser_ID         uniqueidentifier	  
      ,@FelhasznaloSzervezet_Id uniqueidentifier               -- a bejelentkezéskor kiválasztott szervezet
	  ,@MetadataXML xml OUTPUT
AS 
BEGIN
/* ---
-- !!! NEM !!! a 3/2018. (II. 21.) BM rendelet 1.sz. melléklet alapján lett kidolgozva, hanem Laura/Kriszti észrevételei szerint
 --- */
BEGIN TRY

  set nocount on

  /* alapellenőrzések */
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUser_Id)
  if (@Org_ID is null)
  begin
    RAISERROR('[50202]',16,1)
  end;
  /* ez nem biztos, hogy kell !? elegendő hibakezelés, ha az XML = Null értékkel megy vissza ?
  if NOT Exists ( select 1 from [dbo].EREC_IraIktatokonyvek ik where 1 = 1 and ik.ID = @Iktatokonyv_ID )
  begin
    RAISERROR('[51501]',16,1)                                                 --- a hibakódok vmi resource file-ban vannak
  end;
  */
 
  declare @Org_KOD nvarchar(100)
  Set @Org_KOD = ( select KOD from dbo.KRT_Orgok where id = @Org_ID )  
  --- declare @Org_NEV nvarchar(400)
  ---Set @Org_NEV = dbo.fn_GetKRT_OrgokAzonosito(@Org_ID)
  
  declare @ExecutorUser_NEV nvarchar(400)
  Set @ExecutorUser_NEV = dbo.fn_GetFelhasznaloNev(@ExecutorUser_Id)
  declare @ExecutorSzervezet_NEV nvarchar(400)
  Set @ExecutorSzervezet_NEV = dbo.fn_GetKRT_CsoportokAzonosito(@FelhasznaloSzervezet_Id) 
  declare @AktDateTime datetime = getdate()
  
  /* --- Indul a mandula --- */
  
  /*
     @xml_prolog felrakása ???
	     <?xml version="1.0" encoding="UTF-8"?>
	 "
	 The XML data type (SQL Server) does not preserve processing information. 
	 So if you assign an XML document with processing information, it will be lost. 
	 If you want to return an XML document with processing information, you should return a well-formed XML string, instead of returning an XML data type value.
	 "
	 Amikor a GUI hívó átveszi az XML-t (karakter stringként !? - elég lesz a MAX hossz ?), akkor a VS már kiteszi a
	 "magában hordozott" prolog értéket az XML elejére !
  */
  declare @xml_prolog nvarchar(max);
  SET @xml_prolog = N'<?xml version="1.0" encoding="UTF-8"?>';

  /* --- */
  /* CONTENTUM_Header */
  declare @CONTENTUM_Hdr xml;
 		  
/* --- */			     
  /* Érkeztetőkönyv */
  declare @dmdSec_EK xml;
  SET @dmdSec_EK =  
			  ( select 
  	               IsNull( ik.EV,'')  as [ErkeztetoKonyv_Ev]
				  ,IsNull( ik.Iktatohely,'') as [ErkeztetoHely_EgyediAzonositoja]
				  ,IsNull( ik.NEV,'')        as [ErkeztetoKonyv_Megnevezese]				  
				  ,IsNull( ik.UtolsoFoszam,'') as [ErkeztetoKonyv_UtolsoErkeztetoszam]
				  ,IsNull( ik.AZONOSITO,'')  as [ErkeztetoKonyv_Azonositoja]
				  ,IsNull( format( ik.LetrehozasIdo, 'yyyy.MM.dd'),'') as [ErkeztetoKonyv_NyitasanakDatuma]
				  ,IsNull( format( ik.LezarasDatuma, 'yyyy.MM.dd'),'') as [ErkeztetoKonyv_LezarasanakDatuma]
                  ,case	when ik.Statusz = 1 then 'aktív'      --- 'nyitott'
				        else 'inaktív'                        --- 'lezárt'
				   end           as [ErkeztetoKonyv_Statusza]
                  from [dbo].EREC_IraIktatokonyvek ik
                 where ik.ID = @Iktatokonyv_ID
				 
                FOR XML PATH('ErkeztetoKonyv'), type
			  );  
			  
/* --- */

  --- @xml_prolog-ot 'magában hordozza'  a struktúra, de láthatólag csak a VS teszi ki 
  ;    /* --- szet kellett szednem, mert amikor egyben volt, akkor benyomta minden csomópontra a NAMESPACE literált; --- declare namespace -szel vajon lehetne egyszerűbben operálni ? --- */ 
  with XMLNAMESPACES( /*DEFAULT '???/',*/ 'http://www.w3.org/2001/XMLSchema' as xsd, 'http://www.w3.org/2001/XMLSchema-instance' as xsi  )  
  SELECT @MetadataXML = 
      ( select    @Org_Kod                as [Adattulajdonos]
				 ,@ExecutorUser_NEV       as [Keszitette] 
				 ,@ExecutorSzervezet_NEV  as [Szervezet]
				 ,format( @AktDateTime,  'yyyy.MM.dd HH.mm.ss') as [AdatexportIdopontja]
/* --- */
             /* CONTENTUM_Header */
			 ,' Data Exchange Schema Header data ' as [comment()]
             ,@CONTENTUM_Hdr
/* --- */			     
             /* dmdSec - Iktatókönyv */
			 ,' Érkeztetőkönyv ' as [comment()]
			 ,@dmdSec_EK
/* --- */
/* --- */			  
/*      --- ez igazából nem is kell ---
         from [dbo].EREC_IraIktatokonyvek ik
        where 1 = 1
          and ik.ID = @Iktatokonyv_ID
*/		  
      FOR XML PATH('CONTENTUM'), type
      );

  /* --- Utólagos csúnya patkolás a NAMESPACE-k helyes elhelyezése érdekében --- */
---  Set @MetadataXML = convert( xml, replace( convert(nvarchar(MAX), @MetadataXML), 'xmlns=""', '') )         --- DEFAULT kiadásából származó xmlns="" -ek eltüntetése

  /* --- Hibakezelés --- */
  BEGIN TRY
   if CHARINDEX ('</CONTENTUM>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
     /* or LEN( convert(nvarchar(max), @MetadataXML) ) >2000000 */
     SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );  
  END TRY
  BEGIN CATCH
    SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );
  END CATCH

END TRY
BEGIN CATCH
    DECLARE @errorSeverity INT,
        @errorState INT
    DECLARE @errorCode NVARCHAR(1000)    
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    IF ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE()
    ELSE 
        SET @errorCode = ERROR_MESSAGE()

    IF @errorState = 0 
        SET @errorState = 1

    RAISERROR ( @errorCode, @errorSeverity, @errorState )

END CATCH
END

