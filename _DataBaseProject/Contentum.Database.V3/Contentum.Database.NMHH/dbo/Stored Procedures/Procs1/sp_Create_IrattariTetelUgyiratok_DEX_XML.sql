﻿

CREATE PROCEDURE [dbo].[sp_Create_IrattariTetelUgyiratok_DEX_XML]
       @IrattariTetel_ID          uniqueidentifier
      ,@ExecutorUser_ID         uniqueidentifier	  
      ,@FelhasznaloSzervezet_Id uniqueidentifier               -- a bejelentkezéskor kiválasztott szervezet
	  ,@MetadataXML xml OUTPUT
AS 
BEGIN
/* ---
-- 3/2018. (II. 21.) BM rendelet 1.sz. melléklet alapján kidolgozva
 --- */

BEGIN TRY

  set nocount on

  /* alapellenőrzések */
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUser_Id)
  if (@Org_ID is null)
  begin
    RAISERROR('[50202]',16,1)
  end;
  /* ez nem biztos, hogy kell !? elegendő hibakezelés, ha az XML = Null értékkel megy vissza ?
  if NOT Exists ( select 1 from [dbo].EREC_IraIrattariTetelek itt where 1 = 1 and itt.ID = @IrattariTetel_ID )
  begin
    RAISERROR('[51501]',16,1)                                                 --- a hibakódok vmi resource file-ban vannak
  end;
  */
 
  declare @Org_KOD nvarchar(100)
  Set @Org_KOD = ( select KOD from dbo.KRT_Orgok where id = @Org_ID )  
  --- declare @Org_NEV nvarchar(400)
  ---Set @Org_NEV = dbo.fn_GetKRT_OrgokAzonosito(@Org_ID)
  
  declare @ExecutorUser_NEV nvarchar(400)
  Set @ExecutorUser_NEV = dbo.fn_GetFelhasznaloNev(@ExecutorUser_Id)
  declare @ExecutorSzervezet_NEV nvarchar(400)
  Set @ExecutorSzervezet_NEV = dbo.fn_GetKRT_CsoportokAzonosito(@FelhasznaloSzervezet_Id) 
  declare @AktDateTime datetime = getdate()
  
  /* --- Indul a mandula --- */
  
  /*
     @xml_prolog felrakása ???
	     <?xml version="1.0" encoding="UTF-8"?>
	 "
	 The XML data type (SQL Server) does not preserve processing information. 
	 So if you assign an XML document with processing information, it will be lost. 
	 If you want to return an XML document with processing information, you should return a well-formed XML string, instead of returning an XML data type value.
	 "
	 Amikor a GUI hívó átveszi az XML-t (karakter stringként !? - elég lesz a MAX hossz ?), akkor a VS már kiteszi a
	 "magában hordozott" prolog értéket az XML elejére !
  */
  declare @xml_prolog nvarchar(max);
  SET @xml_prolog = N'<?xml version="1.0" encoding="UTF-8"?>';

  /* --- */
  /* DEX_Header */
  declare @DEX_Hdr xml;
 		  
/* --- */			     
  /* Irattári tétel */
  declare @dmdSec_ITT xml;
  SET @dmdSec_ITT =  
			  ( select 
			       Isnull( itt.IrattariTetelszam,'') as [IrattariTetelszam]
				  ,Isnull( itt.Nev,'')               as [IrattariTetelszamMegnevezese]
				  --- ,IsNull( dbo.fn_KodtarErtekNeve('IRATTARI_JEL', itt.IrattariJel, @Org_ID),'') as [AlapertelmezettMegorzesiMod]
				  --- ,IsNull( dbo.fn_KodtarErtekNeve('IDOEGYSEG', itt.Idoegyseg, @Org_ID),'') as [AlapertelmezettMegorzesiIdoegyseg]
                  --- ,Isnull( itt.MegorzesiIdo,'')      as [AlapertelmezettMegorzesiIdoErtek]

				  from [dbo].EREC_IraIrattariTetelek itt 
				 where 1 = 1
				   and itt.ID = @IrattariTetel_ID
					   
                FOR XML PATH('IrattariTetel'),type 
			  
			  );  
			  
/* --- */
  /* Ügyiratok */
  /*
  declare @Application_Name nvarchar(400)
  SET @Application_Name = IsNull( [dbo].fn_GetKRT_ParameterekErtek('APPLICATION_VERSION', '00000000-0000-0000-0000-000000000000' /*@Org_ID*/),'');
  --- SET @Application_Name = substring(@Application_Name, 1, charindex(' v',@Application_Name)-1);
  */
  declare @dmdSec_U xml;

  SET @dmdSec_U =
			  ( select 
  	               
				  ' IV. IKTATÁS METAADATAI ' as [comment()]
				  ,Isnull( u.Azonosito,'') as [Iktatoszam]
				  ,case when u.Csoport_ID_Felelos is Null then ''
				        else dbo.fn_GetKRT_CsoportokAzonosito(u.Csoport_ID_Felelos) + ';' +
                            ( select TOP 1 cs.Nev from KRT_CsoportTagok cst
                               inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID
                               where cst.Csoport_ID_Jogalany = u.Csoport_ID_Felelos
							)
                   end				   as [UgyintezoSzemelySzervezet]				  				  
				  ,Isnull( u.Targy,'') as [UgyiratTargya]
				  ,isnull( ( select ot.Ertek + ';' as [data()]                         --- lehetne külön TAG-ben, de így is kezelhető és a mezőhossz elegendő ( valószínűleg )
		                       from dbo.EREC_ObjektumTargyszavai ot
		                      where ot.Obj_ID = u.ID
                                FOR XML PATH ('')
	                       ), '')             as [Targyszavak]
				  ,isnull( ( select ue.Azonosito + ';' as [data()]                     --- lehetne külön TAG-ben, de így is kezelhető és a mezőhossz elegendő ( valószínűleg )
		                       from dbo.EREC_UgyUgyiratok ue
		                      where ue.UgyUgyirat_ID_Szulo = u.ID
                                FOR XML PATH ('')
	                       ), '')             as [EloiratIktatoszamai]
                  ,isnull( ( select uu.Azonosito + ';' as [data()]                     --- lehetne külön TAG-ben, de így is kezelhető és a mezőhossz elegendő ( valószínűleg )
		                       from dbo.EREC_UgyUgyiratok uu
		                      where uu.ID = u.UgyUgyirat_ID_Szulo
                                FOR XML PATH ('')
	                       ), '')             as [UtoiratIktatoszamai]
   				  ,isnull( ( select ucs.Azonosito + ';' as [data()]                    --- lehetne külön TAG-ben, de így is kezelhető és a mezőhossz elegendő ( valószínűleg )
		                       from dbo.EREC_UgyUgyiratok ucs
							  inner join dbo.EREC_UgyiratObjKapcsolatok uk on uk.Obj_ID_Kapcsolt = ucs.ID and uk.Obj_ID_Elozmeny = u.ID 
                                FOR XML PATH ('')
	                       ), '')             as [CsatoltUgyirat]
			      ,IsNull( dbo.fn_GetKRT_CsoportokAzonosito( ( select TOP 1 uh.HistoryVegrehajto_Id from dbo.EREC_UgyUgyiratokHistory uh 
				                                                where uh.ID = u.ID and uh.Allapot = '03' /* azaz 'Szignált' */ 
																order by uh.HistoryVegrehajtasIdo
															 ) ),'')            as [Szignalo]				  
				  ,Isnull( format( ( select TOP 1 uh.HistoryVegrehajtasIdo from dbo.EREC_UgyUgyiratokHistory uh 
				                      where uh.ID = u.ID and uh.Allapot = '03' /* azaz 'Szignált' */ 
									  order by uh.HistoryVegrehajtasIdo
									), 'yyyy.MM.dd HH.mm.'),'')                 as [SzignalasIdopontja]				     --- ez lehet, hogy nem is kell !?
                  ,Isnull( format( u.Hatarido, 'yyyy.MM.dd HH.mm.'),'')         as [UgyiratUgyintezesiHatarideje]
				  ,Isnull( format( u.SkontrobaDat, 'yyyy.MM.dd HH.mm.'),'')     as [HataridobeTetelIdopontja]
				  ,case when u.Allapot = '07' /* azaz 'Skontróban' */ then Isnull( format( u.SkontroVege, 'yyyy.MM.dd HH.mm.'),'') 
				        else ''
				   end                                                          as [HataridobeTetelLejarata]
				  ,case when /* ez kell ? */ u.SkontrobaDat is NOT Null and u.Allapot <> '07' /* azaz már nincs 'Skontróban' */ then Isnull( format( u.SkontroVege, 'yyyy.MM.dd HH.mm.'),'') 
				        else ''
				   end                                                          as [HataridobolKivetelIdopontja]				  
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYINTEZES_ALAPJA', u.UgyintezesModja, @Org_ID),'') as [UgyintezesModja]
				  ,Isnull( format( u.ElintezesDat, 'yyyy.MM.dd HH.mm.'),'')     as [UgyiratElintezesiIdopontja]
				  ,Isnull( format( u.LezarasDat, 'yyyy.MM.dd HH.mm.'),'')       as [UgyiratLezarasiIdopontja]
				  ,Isnull( ( select itb.IrattariTetelszam from dbo.EREC_IraIrattariTetelek itb where itb.ID = u.IraIrattariTetel_ID ),'') as [IrattariTetelszam]			  

                  ,' VII. IRATTÁRI KEZELÉS METAADATAI ' as [comment()]				  
				  ,case when u.IrattariHely like 'Központi%' then ''
				        else Isnull( u.IrattariHely,'')
				   end                                  as [AtmenetiIrattarMegn]
				  ,case when u.IrattariHely like 'Központi%' then ''
				        else IsNull( dbo.fn_GetKRT_CsoportokAzonosito(u.FelhCsoport_Id_IrattariAtvevo),'')
				   end                                  as [AtmenetiIrattariAtvevo]
				  ,case when u.IrattariHely like 'Központi%' then ''
				        else Isnull( format( u.IrattarbaKuldDatuma, 'yyyy.MM.dd HH.mm.'),'')
				   end                                  as [AtmenetiIrattarbaAdasDatuma]
				  ,case when u.IrattariHely like 'Központi%' then Isnull( u.IrattariHely,'')
				        else ''
				   end                                  as [KozpontiIrattarMegn]
				  ,case when u.IrattariHely like 'Központi%' then IsNull( dbo.fn_GetKRT_CsoportokAzonosito(u.FelhCsoport_Id_IrattariAtvevo),'')
				        else ''
				   end                                  as [KozpontiIrattariAtvevo]
				  ,case when u.IrattariHely like 'Központi%' then Isnull( format( u.IrattarbaKuldDatuma, 'yyyy.MM.dd HH.mm.'),'')
				        else ''
				   end                                  as [KozpontiIrattarbaAdasDatuma]
				  ,case when u.IrattariHely like 'Központi%' then Isnull( format( u.IrattarbaVetelDat, 'yyyy.MM.dd HH.mm.'),'')
				        else ''
				   end                                  as [KozpontiIrattariAtvetelIdopontja]
				  /* inkább EREC_IrattariKikero tag-eket adunk át 
				  --- ,Isnull( format( u.KolcsonKiadDat, 'yyyy.MM.dd HH.mm.'),'')   as [KolcsonzesIdopontja]                 
				  --- ,Isnull( format( u.Kolcsonhatarido, 'yyyy.MM.dd HH.mm.'),'')  as [KolcsonzesiHatarido]
				  */
			      ,( select 
			           ' Kölcsönzés, visszaadás ' as [comment()]                             --- ??? vajon NEM kellene itt is mindíg kiírni minden XML tag-et ( IsNull(,'') )							   
					  ,format( ik.KiadasDatuma, 'yyyy.MM.dd HH.mm.')                       as [KolcsonzesIdopontja]
					  ,dbo.fn_GetKRT_CsoportokAzonosito(ik.FelhasznaloCsoport_ID_Kiado)    as [Kolcsonzo]
					  ,dbo.fn_GetKRT_CsoportokAzonosito(ik.FelhasznaloCsoport_ID_Jovahagy) as [KolcsonzestEngedelyezo]
					  ,format( ik.KikerVege, 'yyyy.MM.dd HH.mm.')                          as [KolcsonzesiHatarido]
				      ,format( ik.VisszaadasDatuma, 'yyyy.MM.dd HH.mm.')                   as [VisszavetelIdopontja]
					  ,dbo.fn_GetKRT_CsoportokAzonosito(ik.FelhasznaloCsoport_ID_Visszave) as [Visszavevo]
					  
			          from [dbo].EREC_IrattariKikero ik
				     where 1 = 1
				       and ik.UgyUgyirat_ID = u.ID
					   
                     FOR XML PATH('IrattariKikero'),type
				   )
				  			  
				  ,' VIII. SELEJTEZÉS, LEVÉLTÁRBA ADÁS METAADATAI ' as [comment()]
				  ,case when itt.IrattariJel = 'S' /* azaz 'Selejtezendő' */ and u.MegorzesiIdoVege is NOT Null then convert(varchar(4),year(u.MegorzesiIdoVege))
						else ''
				   end                                              as [SelejtezhetosegDatuma]
				  ,case when itt.IrattariJel = 'L' /* azaz 'Levéltárba adandó' */ and u.MegorzesiIdoVege is NOT Null then convert(varchar(4),year(u.MegorzesiIdoVege))
						else ''
				   end                                              as [LeveltarbaAdhatosagDatuma]
				  ,IsNull( dbo.fn_KodtarErtekNeve('IRATTARI_JEL', itt.IrattariJel, @Org_ID),'') as [TenylegesMegorzesiMod]                 --- az [AlapertelmezettMegorzesiMod] -ot emeljük be
				  ,IsNull( dbo.fn_KodtarErtekNeve('IDOEGYSEG', itt.Idoegyseg, @Org_ID),'')      as [TenylegesMegorzesiIdoegyseg]           --- az [AlapertelmezettMegorzesiIdoegyseg] -et emeljuk be
                  ,Isnull( itt.MegorzesiIdo,'')                                                 as [TenylegesMegorzesiIdoErtek]            --- az [AlapertelmezettMegorzesiIdoErtek] -et emeljuk be
                  ,Isnull( format( u.SelejtezesDat, 'yyyy.MM.dd HH.mm.'),'')                    as [SelejtezesIdopontja]
				  ,IsNull( dbo.fn_GetKRT_CsoportokAzonosito(u.FelhCsoport_ID_Selejtezo),'')     as [SelejtezesiBizottsagTagjai]            --- ???
				  ,Isnull( format( ( select ij.VegrehajtasDatuma from dbo.EREC_IraJegyzekek ij
				                      where ij.Tipus ='L' and ij.VegrehajtasDatuma is NOT Null
					                    and exists ( select 1 from dbo.EREC_IraJegyzektetelek ijt
						                              where ijt.Jegyzek_ID = ij.ID and ijt.Obj_ID = u.ID ) ), 'yyyy.MM.dd HH.mm.'),'')      as [LeveltarbaAdasIdopontja]
                  ,IsNull( dbo.fn_GetKRT_CsoportokAzonosito( ( select ij.FelhasznaloCsoport_ID_Vegrehaj from dbo.EREC_IraJegyzekek ij
				                                                where ij.Tipus ='L' and ij.VegrehajtasDatuma is NOT Null
					                                              and exists ( select 1 from dbo.EREC_IraJegyzektetelek ijt
						                                                        where ijt.Jegyzek_ID = ij.ID and ijt.Obj_ID = u.ID ) )),'') as [LeveltarnakAtado]
				  
				  ,' XV. ÜGYIRAT META adatok ' as [comment()]
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYIRAT_JELLEG', u.Jelleg, @Org_ID),'')   as [UgyiratTipusa]
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYIRAT_ALLAPOT', u.Allapot, @Org_ID),'') as [UgyiratAllapota]
				  ,Isnull( u.UgyTipus,'')      as [UgyTipusa]
                  ,IsNull( u.Ugyazonosito,u.Azonosito)  as [UgykezeloRendszerAzonosito]                           --- Isnull( @Application_Name,'') as [UgykezeloRendszerAzonosito] --- ???
				  ,'' /* Isnull( ???,'') */    as [HozzaferesiKorlatozasok]                          --- fejlesztés alatt #2203
				  ,'' /* Isnull( ???,'') */    as [FelhasznalasiKorlatozasok]				         --- fejlesztés alatt #2203

			      from [dbo].EREC_UgyUgyiratok u 
				  left join dbo.EREC_IraIrattariTetelek itt on itt.ID = u.IraIrattariTetel_ID
				 where 1 = 1
				   and u.IraIrattariTetel_ID = @IrattariTetel_ID
					   
                FOR XML PATH('Ugyirat'),type
			  );

  --- @xml_prolog-ot 'magában hordozza'  a struktúra, de láthatólag csak a VS teszi ki 
  ;    /* --- szet kellett szednem, mert amikor egyben volt, akkor benyomta minden csomópontra a NAMESPACE literált; --- declare namespace -szel vajon lehetne egyszerűbben operálni ? --- */ 
  with XMLNAMESPACES( /*DEFAULT '???/',*/ 'http://www.w3.org/2001/XMLSchema' as xsd, 'http://www.w3.org/2001/XMLSchema-instance' as xsi  )  
  SELECT @MetadataXML = 
      ( select    @Org_Kod                as [Adattulajdonos]
				 ,@ExecutorUser_NEV       as [Keszitette] 
				 ,@ExecutorSzervezet_NEV  as [Szervezet]
				 ,format( @AktDateTime,  'yyyy.MM.dd HH.mm.ss') as [AdatexportIdopontja]
/* --- */
             /* DEX_Header */
			 ,' Data Exchange Schema Header data ' as [comment()]
             ,@DEX_Hdr
/* --- */			     
             /* dmdSec - Irattári tétel */
			 ,' Irattári tétel ' as [comment()]
			 ,@dmdSec_ITT
/* --- */
             /* dmdSec - Ügyiratok */
			 ,' Ügyiratok ' as [comment()]
			 ,@dmdSec_U
/* --- */			  
/*      --- ez igazából nem is kell ---
		 from [dbo].EREC_IraIrattariTetelek itt 
		where 1 = 1
		  and itt.ID = @IrattariTetel_ID		 
*/		  
      FOR XML PATH('DEX'),type
      );

  /* --- Utólagos csúnya patkolás a NAMESPACE-k helyes elhelyezése érdekében --- */
---  Set @MetadataXML = convert( xml, replace( convert(nvarchar(MAX), @MetadataXML), 'xmlns=""', '') )         --- DEFAULT kiadásából származó xmlns="" -ek eltüntetése

  /* --- Hibakezelés --- */
  BEGIN TRY
   if CHARINDEX ('</DEX>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
     /* or LEN( convert(nvarchar(max), @MetadataXML) ) >2000000 */
     SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );  
  END TRY
  BEGIN CATCH
    SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );
  END CATCH

END TRY
BEGIN CATCH
    DECLARE @errorSeverity INT,
        @errorState INT
    DECLARE @errorCode NVARCHAR(1000)    
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    IF ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE()
    ELSE 
        SET @errorCode = ERROR_MESSAGE()

    IF @errorState = 0 
        SET @errorState = 1

    RAISERROR ( @errorCode, @errorSeverity, @errorState )

END CATCH
END

