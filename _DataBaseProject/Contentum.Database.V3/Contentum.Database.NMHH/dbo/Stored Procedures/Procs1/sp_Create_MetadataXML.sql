﻿
CREATE PROCEDURE [dbo].[sp_Create_MetadataXML]
       @IraJegyzek_ID   uniqueidentifier
      ,@Atadas_Statusz  varchar(15)       = 'NEW'     --- 'TEST', 'REPLACEMENT' 
	  ,@ExecutorUser_ID uniqueidentifier	  
	  ,@MetadataXML xml OUTPUT
AS 
BEGIN
/* ---
 -- #1674 - HU_SIP_METS.XSD szerint validálva ( XMLSpy ); csomópontok kiemelve; METS és Flocat (xlink) namespace beépítve
 ---- HU_SIP_EAD.xsd szerint NEM validált (mapping <=> XSD); ead namespace beépítése nem sikerült 
 --- */
BEGIN TRY

  set nocount on

  /* alapellenőrzések */
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUser_Id)
  if (@Org_ID is null)
  begin
    RAISERROR('[50202]',16,1)
  end;
  /* ez nem biztos, hogy kell !? elegendő hibakezelés, ha az XML = Null értékkel megy vissza ?
  if NOT Exists ( select 1 from [dbo].EREC_IraJegyzekek ij where 1 = 1 and ij.ID = @IraJegyzek_ID and ij.Tipus = 'L' )
  begin
    RAISERROR('[51501]',16,1)                                                 --- a hibakódok vmi resource file-ban vannak
  end;
  */
    
  /* KRT_Parameterek */
  DECLARE 
          @Atado_Rovidneve        nvarchar(400)
		 ,@Atadas_Tipus           nvarchar(400) 
		 ,@Atadas_Dokumentum      nvarchar(400)
		 ,@Atadas_Iktato          nvarchar(400)
		 ,@Iratkepzo_Neve         nvarchar(100)
		 ,@Iratkepzo_Azonositoja  nvarchar(400)
		 ,@Application_Name       nvarchar(400)
		 ,@Application_Version    nvarchar(400)
		 ,@Leveltar_Neve          nvarchar(400)
		 ,@Leveltar_Azonositoja   nvarchar(400)
         ;         
  SET @Atado_Rovidneve       = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE', @Org_ID);
  SET @Atadas_Tipus          = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_TIPUS', @Org_ID);
  SET @Atadas_Dokumentum     = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_DOKUMENTUM', @Org_ID);
  SET @Atadas_Iktato         = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IKTATO', @Org_ID);
  SET @Iratkepzo_Neve        = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_NEVE', @Org_ID);
  SET @Iratkepzo_Azonositoja = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA', @Org_ID);
  SET @Application_Name      = [dbo].fn_GetKRT_ParameterekErtek('APPLICATION_VERSION', '00000000-0000-0000-0000-000000000000' /*@Org_ID*/);
  SET @Application_Version   = substring(@Application_Name, charindex(' v',@Application_Name)+1,400);
  SET @Application_Name      = substring(@Application_Name, 1, charindex(' v',@Application_Name)-1);
  SET @Leveltar_Neve         = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_LEVELTAR_NEVE', @Org_ID);
  SET @Leveltar_Azonositoja  = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA', @Org_ID);

  DECLARE @SIP_type      nvarchar(10)   = 'SIP'
         ,@SIP_azonosito nvarchar(max)
         ,@Atad_Date     datetime       = getdate()
	     ,@Atad_Sorsz    int            = 1
         ;
  SET @Atad_Sorsz = ( select count(1) from [dbo].EREC_IraJegyzekek ij
                       where 1 = 1
					     and ij.Org = @Org_ID
						 and ij.Tipus = 'L'              --- 'L'evéltári átadás
						 and ij.VegrehajtasDatuma is NOT Null
						 and convert(nvarchar(8), ij.VegrehajtasDatuma, 112) = convert(nvarchar(8), getdate(), 112)
					);
  SET @Atad_Sorsz += 1;
  SET @SIP_azonosito = N'SIP' + '_' + convert(nvarchar(8), @Atad_Date, 112) + '_' + @Atado_Rovidneve + 
                        '_' + right( '000' + convert( nvarchar(3), @Atad_Sorsz ), 3)

  Declare @IraJegyzekVegrehaj_ID uniqueidentifier
  Set @IraJegyzekVegrehaj_ID = ( select ij.FelhasznaloCsoport_Id_Vegrehaj from EREC_IraJegyzekek ij 
                                  where 1 = 1
								    and ij.Org = @Org_ID 
									and ij.ID = @IraJegyzek_ID
                               )

  DECLARE @Metaadat_Sema     nvarchar(10)
         ,@CheckSum_Type     nvarchar(10)
		 ,@Atadas_Statusz_HU nvarchar(50)
		 ;
  SET @Metaadat_Sema = 'EAD';
  SET @CheckSum_Type = 'MD5';                 --- "a checkstumtype: CHECKSUMTYPE="MD5" "   --- 'SHA-1';
    
  SET @Atadas_Statusz_HU = ( select case when @Atadas_Statusz = 'NEW' then 'Új csomag'
                                         when @Atadas_Statusz = 'REPLACEMENT' then 'Helyettesítő csomag'
							             when @Atadas_Statusz = 'TEST' then 'Teszt'
								    else 'OTHER'
								    end
						   );

  declare @IrattariTerv_ID   uniqueidentifier = NEWID()                          --- (Hál' Istennek) egy Levéltári átadásban csak egy Irattári terv ( egy év ) anyaga lehet benne
  declare @maxdate           datetime = datetimefromparts(4700,12,31,0,0,0,0)
  declare @s_xml             nvarchar(MAX)

  /* --- Indul a mandula --- */
  
  /* a h... UUID kezelés miatt kell egy segédtábla */
  CREATE TABLE #tmp_UUID ( ObjType      varchar(25),
	                       ObjID        uniqueidentifier,
						   UUID         uniqueidentifier
						 );	
  declare @cur_UUID cursor
  declare @ObjID uniqueidentifier  
  
  /*
     @xml_prolog felrakása ???
	     <?xml version="1.0" encoding="UTF-8"?>
	 "
	 The XML data type (SQL Server) does not preserve processing information. 
	 So if you assign an XML document with processing information, it will be lost. 
	 If you want to return an XML document with processing information, you should return a well-formed XML string, instead of returning an XML data type value.
	 "
	 Amikor a GUI hívó átveszi az XML-t (karakter stringként !? - elég lesz a MAX hossz ?), akkor a VS már kiteszi a
	 "magában hordozott" prolog értéket az XML elejére !
  */
  declare @xml_prolog nvarchar(max);
  SET @xml_prolog = N'<?xml version="1.0" encoding="UTF-8"?>';
  
/* --- */
  /* metsHdr */
  declare @metsHdr xml;
  SET @metsHdr =      			  
			 ( select 
			      @Atadas_Statusz_HU                     as [@RECORDSTATUS]
				 ,convert(nvarchar(19), @Atad_Date, 126) as [@CREATEDATE]        --- xsd:dateTime; kell a 'T' betű a dátum és az időpont közé, DE a millisecond NEM (ezt csak 'levágni' tudtam); --- "Csomag létrehozásának időpontja másodpercpontosan" "Fontos a dátum formátumát követni!"
	             /* agent */	
	             ,'ORGANIZATION'          as [agent/@TYPE]
				 ,'ARCHIVIST'             as [agent/@ROLE]
	             ,@Iratkepzo_Neve         as [agent/name]
	             ,@Iratkepzo_Azonositoja  as [agent/note]				 
                 ,Null                                              --- elhatároló
	             ,'OTHER'                 as [agent/@TYPE]
				 ,'ARCHIVIST'             as [agent/@ROLE]	             
	             ,'SOFTWARE'              as [agent/@OTHERTYPE]
	             ,@Application_Name       as [agent/name]
	             ,@Application_Version    as [agent/note]
                 ,Null                                              --- elhatároló
	             ,'ORGANIZATION'          as [agent/@TYPE]
				 ,'CREATOR'               as [agent/@ROLE]	             
	             ,@Iratkepzo_Neve         as [agent/name]
	             ,@Iratkepzo_Azonositoja  as [agent/note]
                 ,Null                                              --- elhatároló
	             ,'ORGANIZATION'          as [agent/@TYPE]
				 ,'EDITOR'                as [agent/@ROLE]	             
	             ,''                      as [agent/name]           --- ez lehetne Null és akkor NEM jelenne meg, de itt a metsHdr-ben ez nem komoly méret
	             ,''                      as [agent/note]           --- Itt mindegy, hogy '' vagy Null szerepel - a @TYPE megadása miatt mindenképpen megjelenik a sor
                 ,Null                                              --- elhatároló
	             ,'OTHER'                 as [agent/@TYPE]
				 ,'CREATOR'               as [agent/@ROLE]
	             ,'SOFTWARE'              as [agent/@OTHERTYPE]
	             ,@Application_Name       as [agent/name]
	             ,@Application_Version    as [agent/note]
                 ,Null                                              --- elhatároló
	             ,'INDIVIDUAL'            as [agent/@TYPE]
				 ,'CREATOR'               as [agent/@ROLE]	             
	             ,(select f.Nev from [dbo].KRT_Felhasznalok f where f.ID = @IraJegyzekVegrehaj_ID and f.Org = @Org_Id) as [agent/name]           
	             ,(select IsNull(f.EMail,'')+' / '+ IsNull(f.Telefonszam,'') from [dbo].KRT_Felhasznalok f where f.ID = @IraJegyzekVegrehaj_ID and f.Org = @Org_Id) as [agent/note]           
                 ,Null                                              --- elhatároló
	             ,'ORGANIZATION'          as [agent/@TYPE]
				 ,'PRESERVATION'          as [agent/@ROLE]
	             ,@Leveltar_Neve          as [agent/name]
	             ,@Leveltar_Azonositoja   as [agent/note]
	             /* altRecordID */                                                                    /* <agent> és <altRecordID> sorrendje mindegy; DE a TAG nevekre Case sensitive az XML */
	             ,'DELIVERYTYPE'          as [altRecordID/@TYPE]
	             ,@Atadas_Tipus           as [altRecordID]
	             ,Null                                              --- elhatároló
                 ,'DELIVERYSPECIFICATION' as [altRecordID/@TYPE]
                 ,@Atadas_dokumentum      as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'SUBMISSIONAGREEMENT'   as [altRecordID/@TYPE]
	             ,@Atadas_Iktato          as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'DATASUBMISSIONSESSION' as [altRecordID/@TYPE]    --- ez így NEM okoz zürt ???
	             ,Null                    as [altRecordID]          
                 ,Null                                              --- elhatároló
	             ,'PACKAGENUMBER'         as [altRecordID/@TYPE]
	             ,Null                    as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'REFERENCECODE'         as [altRecordID/@TYPE]
	             ,Null                    as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'ACCESSRESTRICT'        as [altRecordID/@TYPE]
	             ,Null                    as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'APPRAISAL'             as [altRecordID/@TYPE]
	             ,( select case when @Atadas_Statusz = 'TEST' then 'Selejtezhető'
                                else 'Nem selejtezhető'
                           end
                  )						  as [altRecordID]
                 ,Null                                              --- elhatároló
				 /* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
	             ,Null                               --- <mets:altRecordID TYPE = PREVIOUSSUBMISSIONAGREEMENT>
	             ,Null                               --- <mets:altRecordID TYPE = PREVIOUS REFERENCE CODE>
				 */
            from [dbo].EREC_IraJegyzekek ij
           where 1 = 1
             and ij.ID = @IraJegyzek_ID

            FOR XML PATH('metsHdr') /* ,type */
		     );
			  
/* --- */			     
  /* dmdSec - Iktatókönyvek */
  --- az Iktatókönyv soroknak erre a feladásra kiosztott NEWID()-k 'letépése'    2018.09.26., BogI
  SET @cur_UUID = CURSOR FOR 
                  ( select ik.ID from [dbo].EREC_IraIktatokonyvek ik
                     where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                     inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                              and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                     where u.IraIktatokonyv_ID = ik.ID
				                  )
				  )             --- !!! ugyanazok a feltételek kellenek, mint az alábbiakban a @dmdsec_IK felgyűjtésében
  OPEN @cur_UUID
  FETCH NEXT FROM @cur_UUID INTO @ObjId
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    Insert INTO #tmp_UUID ( ObjType, ObjID, UUID ) values ( 'IKTATOKONYV', @ObjID, NEWID() )
    FETCH NEXT FROM @cur_UUID INTO @ObjID
  end;
  CLOSE @cur_UUID;  
  DEALLOCATE @cur_UUID;

  declare @dmdSec_IK xml;
  SET @dmdSec_IK =  
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'current'                                      as [@STATUS]                --- konstans 
				  --- ,'uuid-' + convert( nvarchar(40), ik.ID )       as [@ID]                --- 2018.09.26., BogI    --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,'uuid-' + convert( nvarchar(40), tmp.UUID )    as [@ID]                    --- az Iktatókönyv sornak erre a feladásra kiosztott NEWID() azonositója
				  ,'1'                                            as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,convert( nvarchar(19), @Atad_Date, 126 )       as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
                  
				  /* mdWrap */
			      ,( select 
  	                   ''                                         as [@LABEL] 	             --- megeszi az üres stringet 				  
					  ,'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- /* required; attribute;  An xsd:ID value must be an NCName. */  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,'MDTYPEVERSION'                            as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,@Metaadat_Sema                             as [@MDTYPE]

				      /* xmlData */
                      ,( select Null
		                /* EAD - Iktatókönyvek */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             @Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
								,'HU'                            as [eadid/@countrycode]
							    ,isnull(ik.Azonosito,'') + '/' + convert(varchar(4),ik.Ev) as [eadid] --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
							    ,Null                            --- elhatároló 
				                ,ik.Nev                          as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
								,Null                              as [profiledesc/descrules]         --- NEM kötelező, így elnyeljük, hogy minél kisebb legyen a METADATA.XML 
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select
						         'register'                as [@otherlevel]
								,'otherlevel'              as [@level]

						        /* did */
						        ,( select
						              isnull(ik.Azonosito,'') + '/' + convert(varchar(4),ik.Ev) as [unitid]
				                     ,ik.Nev                                                    as [unittitle] 
									 ,Null                 --- elhatároló
									 ,convert( date, ik.LetrehozasIdo )                         as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
									 ,'originated_date'                                         as [unitdate/@label]
						             ,convert( date, ik.LetrehozasIdo )                         as [unitdate]
								     ,Null                 --- elhatároló
						   		     ,convert( date, isNull( ik.LezarasDatuma,@maxdate) )       as [unitdate/@normal]       --- !!! NEM lehet üres 
									 ,'closed_date'                                             as [unitdate/@label]						             
									 ,convert( date, isNull( ik.LezarasDatuma,@maxdate) )       as [unitdate]
								     ,Null                 --- elhatároló
						   		     ,convert( date, datetimefromparts(ik.Ev,1,1,0,0,0,0) )     as [unitdate/@normal]
									 ,'coverage_from'                                           as [unitdate/@label]						             
									 ,convert( date, datetimefromparts(ik.Ev,1,1,0,0,0,0) )     as [unitdate]
								     ,Null                 --- elhatároló
						   		     ,convert( date, datetimefromparts(ik.Ev,12,31,0,0,0,0) )   as [unitdate/@normal]
									 ,'coverage_to'                                             as [unitdate/@label]									 
						             ,convert( date, datetimefromparts(ik.Ev,12,31,0,0,0,0) )   as [unitdate]
								     ,Null                 --- elhatároló
									 ,@Iratkepzo_Neve                                           as [origination/corpname]         --- XSD szerint Tag nem attribute
                                     ,Null                   as [abstract]                        --- NEM kötelező, így kihagyjuk
									 ,Null                   as [langmaterial/language]           --- NEM kötelező, így kihagyjuk
						             ,Null                   as [note/p]                          --- NEM kötelező, így kihagyjuk
									 								 
                                   FOR XML PATH('did'),type
								 )
						        ,Null                        as [bioghist/p]                        --- NEM is szerepel az IF (interface) leírásban !? csak az MNL mintában
								,Null                        as [scopecontent/p]                    --- NEM kötelező, így kihagyjuk
								,'Nincs korlátozás'        as [accessrestrict/p]        --- !!! fogalmam nincs mit kell ide írni

						        /* accessrestrict */                                    --- NEM is szerepel az IF (interface) leírásban ilyen blokk !? csak az MNL mintában 
						        ,( select
								     Null                    as [legalstatus]           --- '' 
                                    ,'rule'                as [note/@type]
									,Null                    as [note/p]                --- '' 
									 								 
                                   FOR XML PATH('accessrestrict'),type
								 )
								,'Nincs korlátozás'        as [userestrict/p]     --- !!! fogalmam nincs mit kell ide írni - de az XSD validáció elfogadta    --- XSD szerint Tag nem attribute
								,Null                        as [originalsloc/p]                        --- NEM kötelező, így kihagyjuk
								,Null                        as [altformavail/p]                        --- NEM kötelező, így kihagyjuk  
								,Null                        as [relatedmaterial/p]                     --- NEM kötelező, így kihagyjuk

						        /* index */                                      --- Az egyes földrajzi tárgyszavak
						        ,( select
								     Null                    as [indexentry/geogname]			         --- NEM kötelező, így kihagyjuk
                                   FOR XML PATH('index'),type
								 )                                                
								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
								,'' as [accruals/p]                    
								,'' as [acqinfo/p]
						        ,'' as [arrangement/p]
						        ,'' as [custodhist/p]
						        ,'' as [fileplan/p]
								,'' as [odd/p]
								*/
								
                              FOR XML PATH('archdesc'),type
			                )
						  
						   FOR XML PATH('ead'),type
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
                  from [dbo].EREC_IraIktatokonyvek ik
				 /*  az inner join megoldás elképesztően lassúnak bizonyult
                 inner join 
                    ( select distinct ikb.ID from [dbo].EREC_IraIktatokonyvek ikb
                       inner join [dbo].EREC_UgyUgyiratok u on u.IraIktatokonyv_ID = ikb.ID
                        inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = ij.ID
   		                                                                           and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			            where 1 = 1
                          /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                    ) iksub on iksub.ID = ik.ID
				 */
				 inner join #tmp_UUID tmp on tmp.ObjID = ik.ID and tmp.ObjType = 'IKTATOKONYV'                 --- 2018.09.26., BogI
                 where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                 inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                          and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                 where u.IraIktatokonyv_ID = ik.ID
				              )
                 order by ik.Ev, ik.Azonosito
				 
                FOR XML PATH('dmdSec') /* ,type */
			  );  
			  
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_IK)
  --- !!! Vigyázz < ead:ead xmlns=... xmlns:ead=... > megoldás NEM jó, mert akkor minden soron beteszi az ead: előtagot !?
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_IK = convert( xml, @s_xml )
			  
/* --- */			     
  /* dmdSec - Ügyiratok */
  --- az Ügyirat soroknak erre a feladásra kiosztott NEWID()-k 'letépése'    2018.09.26., BogI
  SET @cur_UUID = CURSOR FOR 
                  ( select u.ID from [dbo].EREC_UgyUgyiratok u
                     inner join [dbo].EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
				     inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID
                     inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                              and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			         where 1 = 1
                       /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */				  
				  )             --- !!! ugyanazok a feltételek kellenek, mint az alábbiakban a @dmdsec_U felgyűjtésében
  OPEN @cur_UUID
  FETCH NEXT FROM @cur_UUID INTO @ObjId
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    Insert INTO #tmp_UUID ( ObjType, ObjID, UUID ) values ( 'UGYIRAT', @ObjID, NEWID() )
    FETCH NEXT FROM @cur_UUID INTO @ObjID
  end;
  CLOSE @cur_UUID;  
  DEALLOCATE @cur_UUID;

  declare @dmdSec_U xml;
  SET @dmdSec_U =
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'current'                                      as [@STATUS]                --- konstans 
				  --- ,'uuid-' + convert( nvarchar(40), u.ID )        as [@ID]                --- 2018.09.26., BogI    --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,'uuid-' + convert( nvarchar(40), tmp.UUID )    as [@ID]                    --- az Ügyirat sornak erre a feladásra kiosztott NEWID() azonositója
				  ,'1'                                            as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,convert( nvarchar(19), @Atad_Date, 126 )       as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
                  
				  /* mdWrap */
			      ,( select 
  	                   ''                                         as [@LABEL] 	             --- megeszi az üres stringet 				  
					  ,'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- /* required; attribute;  An xsd:ID value must be an NCName. */  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,'MDTYPEVERSION'                            as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,@Metaadat_Sema                             as [@MDTYPE]

				      /* xmlData */
                      ,( select Null
		                /* EAD - Ügyiratok */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             @Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
								,'HU'                            as [eadid/@countrycode]
							    ,isnull(u.Azonosito,'')          as [eadid] --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
							    ,Null                            --- elhatároló 
				                ,u.Targy + ';' + u.Azonosito     as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
								,Null                              as [profiledesc/descrules]          --- NEM kötelező, így kihagyjuk
								--- ,''                              as [profiledesc]                     --- ??? ez biztosan kell ide üresen ???
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select
						         'file'                    as [@level]
 						        /* did */
						        ,( select
						              u.Azonosito           as [unitid]
				                     ,u.Targy               as [unittitle] 
								     ,Null                     --- elhatároló									 
									 ,convert( date, u.LetrehozasIdo )                 as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						   		     ,'originated_date'                                as [unitdate/@label]
						             ,convert( date, u.LetrehozasIdo )                 as [unitdate]
								     ,Null                     --- elhatároló
									 ,convert( date, isNull( u.LezarasDat,@maxdate) )  as [unitdate/@normal]       --- !!! NEM lehet üres 
						   		     ,'closed_date'                                    as [unitdate/@label]
						             ,convert( date, isNull( u.LezarasDat,@maxdate) )  as [unitdate]
								     ,Null                     --- elhatároló									 
									 ,convert( date, u.LetrehozasIdo )                 as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						   		     ,'coverage_from'                                  as [unitdate/@label]
						             ,convert( date, u.LetrehozasIdo )                 as [unitdate]
								     ,Null                     --- elhatároló									 
									 ,convert( date, isNull( u.LezarasDat,@maxdate) )  as [unitdate/@normal]       --- !!! NEM lehet üres 
						   		     ,'coverage_to'                                    as [unitdate/@label]
						             ,convert( date, isNull( u.LezarasDat,@maxdate) )  as [unitdate]
								     ,Null                     --- elhatároló									 
						   		     ,'pre_file_id'         as [note/@type]
	                                 ,/*isnull( */( select ue.Azonosito + ';' as [data()]
		                                          from EREC_UgyUgyiratok ue
		                                         where ue.UgyUgyirat_ID_Szulo = u.ID
                                                   FOR XML PATH ('')
	                                          )/* , '')*/       as [note/p]                      --- NEM kötelező, így - ha értéke Null, akkor - kihagyjuk         --- [note/p/ref]
								     ,Null                     --- elhatároló
						   		     ,'post_file_id'        as [note/@type]
	                                 ,/*isnull( */( select uu.Azonosito + ';' as [data()]
		                                          from EREC_UgyUgyiratok uu
		                                         where uu.ID = u.UgyUgyirat_ID_Szulo
                                                   FOR XML PATH ('')
	                                          )/*, '')*/        as [note/p]                     --- NEM kötelező, így - ha értéke Null, akkor - kihagyjuk         --- [note/p/ref]
								     ,Null                     --- elhatároló
						   		     ,'connected_file_ids'  as [note/@type]
									 ,Null                    as [note/list/item/ref]          --- NEM kötelező, így kihagyjuk
								     ,Null                     --- elhatároló
						   		     ,'owner'               as [origination/@label]                    --- XSD szerint kisbetű
						             ,(select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = u.Csoport_ID_Felelos and cs.Org = @Org_Id) as [origination/name]   --- ez XSD szerint kötelező -> NEM lehet üres !
								     ,Null                     --- elhatároló
						   		     ,'administrator'       as [origination/@label]            --- NEM kötelező, így lehet Null érték 
						             ,(select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = u.FelhasznaloCsoport_ID_Ugyintez and cs.Org = @Org_Id) as [origination/name]
								     ,Null                     --- elhatároló									 
						   		     ,convert( date, u.Hatarido )     as [unitdate/@normal]    --- NEM kötelező, így lehet Null érték
									 ,'administering_deadline'        as [unitdate/@label]
						             ,convert( date, u.Hatarido )     as [unitdate]
								     ,Null                     --- elhatároló
									 ,convert( date, u.ElintezesDat ) as [unitdate/@normal]
						   		     ,'administering_date'            as [unitdate/@label]
						             ,convert( date, u.ElintezesDat ) as [unitdate]
								     ,Null                     --- elhatároló									 
									 ,'administering_mode'            as [note/@type]
									 ,Null                              as [note/p]            --- NEM kötelező, így kihagyjuk
									 ,Null                     --- elhatároló									 
						   		     ,'inhouse_archiver'              as [origination/@label]  --- NEM kötelező, így lehet Null érték       
									 ,(select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = u.FelhCsoport_ID_IrattariAtvevo and cs.Org = @Org_Id) as [origination/name]
									 ,Null                     --- elhatároló			
									 ,convert( date, u.IrattarbaVetelDat ) as [unitdate/@normal]   --- NEM kötelező, így lehet Null érték 
						   		     ,'inhouse_archiving_date'             as [unitdate/@label]
						             ,convert( date, u.IrattarbaVetelDat ) as [unitdate]
								     ,Null                     --- elhatároló			
									 ,convert( date, u.MegorzesiIdoVege )  as [unitdate/@normal]   --- NEM kötelező, így lehet Null érték 						 
						   		     ,'transfer_deadline'                  as [unitdate/@label]
						             ,convert( date, u.MegorzesiIdoVege )  as [unitdate]
								     ,Null                     --- elhatároló									 
									 ,Null                    as [physloc]                         --- NEM kötelező, így kihagyjuk
									 ,@Iratkepzo_Neve       as [origination/corpname]					--- XSD szerint Tag nem attribute
                                     ,Null                    as [abstract]                        --- NEM kötelező, így kihagyjuk
									 ,Null                    as [langmaterial/language]           --- NEM kötelező, így kihagyjuk
						             ,Null                    as [note/p]                          --- NEM kötelező, így kihagyjuk
									 ,Null                     --- elhatároló
									 ,'handling_notes'      as [note/@type]
						             ,Null                    as [note/p]                          --- NEM kötelező, így kihagyjuk
									 ,Null                     --- elhatároló									 
								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
						   		     ,'destroy_deadline'    as [unitdate/@label]
						             ,u.SelejtezesDat       as [unitdate]
								     ,Null                     --- elhatároló									 
								*/
									 
                                   FOR XML PATH('did'),type
								 )								

						        /* fileplan */                      --- Ügyirat - Irattári tétel ( Irattári terv ) kapcsolat
						        ,( select Null
								     /* p */
								     ,( select 
									       it.IrattariTetelszam                 as [ref]   --- XSD szerint Tag és nem attribute
								          ,Null                     --- elhatároló
						   		          ,'classification_date'                as [date/@type]
						                  ,convert( date, u.IrattarbaVetelDat ) as [date]

                                        FOR XML PATH('p'),type
								      )
									 
                                   FOR XML PATH('fileplan'),type
								 )
                                /* menjen bele a default_..., mivel nálunk nincs külön tárolt actual_...
								,'actual_disposal_action'   as [note/@type]
						        ,''                         as [note/p]
								,Null                          --- elhatároló 
								,'actual_retention_period_interval' as [note/@type]
						        ,''                         as [note/p]
								,Null                          --- elhatároló 
								,'actual_retention_period_duration' as [note/@type]
						        ,''                         as [note/p]
								,Null                          --- elhatároló 
								*/
								,'actual_disposal_action'           as [note/@type]
						        ,/*ISNull( */( select kt.Nev from [dbo].KRT_KodTarak kt
                                            inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'IRATTARI_JEL'
                                            where getdate() between kt.ErvKezd and kt.ErvVege
										      and ( kt.Kod collate database_default = it.IrattariJel collate database_default) 
										      and kt.Org = @Org_Id )/*, '')*/  as [note/p]            --- NEM kötelező, így lehet Null érték
								,Null                 --- elhatároló 
								,'actual_retention_period_interval' as [note/@type]            --- ! ennek akkor kellene Null-nak lennie, ha az 'actual_retention_period_duration' is Null
						        ,case when it.IdoEgyseg = 'E' then 'Évek'
								      when it.IdoEgyseg = 'H' then 'Hónapok'
									  when it.Idoegyseg = 'W' then 'Hetek'                --- ??? Nem tudom, mi jelöli - én most ezzel jelölöm
									  else 'Napok'                                         --- NULL -t 'Napok' -nak tekintem
								 end                                 as [note/p]
								,Null                 --- elhatároló 
								,'actual_retention_period_duration' as [note/@type]
						        --- ,itt.MegorzesiIdo                    as [note/p]
						        ,/*ISNull(*/ ( select kt.Nev from [dbo].KRT_KodTarak kt
                                            inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'SELEJTEZESI_IDO'
                                            where getdate() between kt.ErvKezd and kt.ErvVege
										      and ( kt.Kod collate database_default = it.MegorzesiIdo collate database_default) 
										      and kt.Org = @Org_Id )/*, '')*/  as [note/p]             --- NEM kötelező, így lehet Null érték
                                ,Null                 --- elhatároló 

						        /* odd */
						        ,( select
						              'loan'                as [@type]                                   --- XSD szerint kisbetű
 						             /* chronlist */
						             ,( select Null
 						                   /* chronitem */
						                  ,( select 
										        'loan date'                       as [date/@type]                   --- XSD szerint kisbetű
											   ,convert( date, u.KolcsonKiadDat ) as [date]	            --- XSD szerint kötelező - de mit is jelent a Kölcsönzés és mi van, ha nem történt kölcsönzés !?
										       /* event */
						                       ,( select 
											         'borrower'        as [name/@role]      --- MNL minta szerint kisbetű
													,''                as [name]            --- XSD szerint kötelező !?
													,Null                 --- elhatároló
													,'loaner'          as [name/@role]
													,''                as [name]            --- XSD szerint kötelező !?
													,Null                 --- elhatároló
													,'loan_deadline'   as [date/@type]      --- MNL minta szerint kisbetű
													,convert( date, u.KolcsonHatarido ) as [date]       --- XSD szerint kötelező !?
													,Null                 --- elhatároló
													,'take_back_date'  as [date/@type]
													,convert( date, Null              ) as [date]
													,Null                 --- elhatároló                --- NEM kötelező, így lehet Null érték
													,'tb_person'       as [name/@role]
													,Null              as [name]                        --- NEM kötelező, így kihagyjuk
													,Null                 --- elhatároló
								
                                                  FOR XML PATH('event'),type /* ,ELEMENTS XSINIL  */
			                                    )  
								
                                             FOR XML PATH('chronitem'),type
			                               )
								
                                        FOR XML PATH('chronlist'),type
			                          )
								
                                   FOR XML PATH('odd'),type
			                     )
 				   		        ,'transferred_time'         as [processinfo/@type]          --- MNL ilyen TAG nevet kért; MNL minta szerint kisbetű
						        ,convert( date, @Atad_date )   as [processinfo/p/date]      --- MNL szerint ezt kell átadni
								,Null                          --- elhatároló									 
 				   		        ,'transfer_deliverer'       as [processinfo/@type]           --- NEM kötelező, így lehet Null érték
						        ,(select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = u.FelhCsoport_ID_Selejtezo and cs.Org = @Org_Id) as [processinfo/p/name]
								,Null                          --- elhatároló									 
 				   		        ,'transfer_recipient'       as [processinfo/@type]       --- !!! ide az a Név kellene, amit a GUI felületen megadunk az Iratjegyzék végrehajtásakor ( EREC_IraJegyzek.Note - ekkor még üres )
						        --- ,u.LeveltariAtvevoNeve      as [processinfo/p/name]      --- ebben az adatmezőben NEM tudom mi van
								,Null                         as [processinfo/p/name]          --- NEM kötelező, így kihagyjuk
								,Null                          --- elhatároló									 
								,Null                         as [scopecontent/p]               --- NEM kötelező, így kihagyjuk
						        /* index */                                      --- Az egyes földrajzi tárgyszavak
						        ,( select
								     Null                    as [indexentry/geogname]			 --- NEM kötelező, így kihagyjuk				 								 
                                   FOR XML PATH('index'),type
								 )                                                
								 ,Null                       as [custodhist/p]                  --- NEM kötelező, így kihagyjuk
								,'Nincs korlátozás'        as [accessrestrict/p]        --- !!! fogalmam nincs mit kell ide írni

						        /* accessrestrict */
						        ,( select
								     Null                    as [legalstatus]                     --- NEM kötelező, így kihagyjuk
                                    ,'rule'                as [note/@type]
									,Null                    as [note/p]                          --- NEM kötelező, így kihagyjuk
									 								 
                                   FOR XML PATH('accessrestrict'),type
								 )
						        ,Null                        as [acqinfo/p]                 --- NEM kötelező, így kihagyjuk
								,'Nincs korlátozás'        as [userestrict/p]           --- !!! fogalmam nincs mit kell ide írni, de XSD elfogadta      --- XSD szerint ez Tag és nem attribute
						        ,Null                        as [originalsloc/p]            --- NEM kötelező, így kihagyjuk
						        ,Null                        as [altformavail/p]            --- NEM kötelező, így kihagyjuk
						        ,Null                        as [relatedmaterial/p]         --- NEM kötelező, így kihagyjuk
						        ,Null                        as [arrangement/p]             --- NEM kötelező, így kihagyjuk
						        /* index */                                      --- tárgyszavak
						        ,( select
								     Null                    as [indexentry/subject]        --- NEM kötelező, így kihagyjuk
                                   FOR XML PATH('index'),type
								 )                                                
                                
								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
								,'' as [accruals/p]                                     --- XSD szerint kellenek üresen								 
						        ,'' as [bioghist/p]					        
								,Null                       --- <archdesc><processinfo TYPE="destroy_time”><p><date>
								,Null                       --- <archdesc><processinfo TYPE="destroyer”><p><name>
                                ,Null                       --- <archdesc><processinfo TYPE="supevisor”><p><name>
								*/								
                              FOR XML PATH('archdesc'),type
			                )
						  
						   FOR XML PATH('ead'),type
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
                  from [dbo].EREC_UgyUgyiratok u
				 inner join #tmp_UUID tmp on tmp.ObjID = u.ID and tmp.ObjType = 'UGYIRAT'                 --- 2018.09.26., BogI 
                 inner join [dbo].EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
				 inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID
                 inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                          and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			     where 1 = 1
                   /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
				 order by ik.Ev, ik.Azonosito, u.Foszam                                         --- legyen szinkronban filegrp rendezésével   --- u.Azonosito  

                FOR XML PATH('dmdSec') /* ,type */
			  );  
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_U)
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_U = convert( xml, @s_xml )
  
/* --- */
  /* dmdSec - Iratok */
  --- az Irat soroknak erre a feladásra kiosztott NEWID()-k 'letépése'    2018.09.26., BogI
  SET @cur_UUID = CURSOR FOR 
                  ( select i.ID from [dbo].EREC_IraIratok i
                     inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                     /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                     inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                     /* inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID */
                     inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                        and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
				  )             --- !!! ugyanazok a feltételek kellenek, mint az alábbiakban a @dmdsec_I felgyűjtésében
  OPEN @cur_UUID
  FETCH NEXT FROM @cur_UUID INTO @ObjId
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    Insert INTO #tmp_UUID ( ObjType, ObjID, UUID ) values ( 'IRAT', @ObjID, NEWID() )
    FETCH NEXT FROM @cur_UUID INTO @ObjID
  end;
  CLOSE @cur_UUID;  
  DEALLOCATE @cur_UUID;

  declare @dmdSec_I xml;
  SET @dmdSec_I =
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'current'                                      as [@STATUS]                --- konstans 
				  --- ,'uuid-' + convert( nvarchar(40), i.ID )        as [@ID]                --- 2018.09.26., BogI    --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,'uuid-' + convert( nvarchar(40), tmp.UUID )    as [@ID]                    --- az Irat sornak erre a feladásra kiosztott NEWID() azonositója
				  ,'1'                                            as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,convert( nvarchar(19), @Atad_Date, 126 )       as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
                  
				  /* mdWrap */
			      ,( select 
  	                   ''                                         as [@LABEL] 	             --- megeszi az üres stringet 				  
					  ,'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- /* required; attribute;  An xsd:ID value must be an NCName. */  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,'MDTYPEVERSION'                            as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,@Metaadat_Sema                             as [@MDTYPE]

				      /* xmlData */
                      ,( select Null
		                /* EAD - Iratok */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             @Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
								,'HU'                            as [eadid/@countrycode]
							    ,isnull(i.Azonosito,'')          as [eadid] --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
							    ,Null                            --- elhatároló 
				                ,i.Targy + ';' + i.Azonosito     as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
								,Null                              as [profiledesc/descrules]             --- NEM kötelező, így kihagyjuk
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select
						         'record'                  as [@otherlevel]
								,'otherlevel'              as [@level]

 						        /* did */
						        ,( select
						              i.Azonosito           as [unitid]
				                     ,i.Targy               as [unittitle] 
								     ,Null                     --- elhatároló									 
									 ,convert( date, i.IktatasDatuma )    as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						   		     ,'originated_date'                   as [unitdate/@label]
						             ,convert( date, i.IktatasDatuma )    as [unitdate]
								     ,Null                     --- elhatároló
									 ,convert( date, i.IktatasDatuma )    as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						   		     ,'coverage_from'                     as [unitdate/@label]
						             ,convert( date, i.IktatasDatuma )    as [unitdate]
								     ,Null                     --- elhatároló									 
									 ,convert( date, isNull( i.IntezesIdopontja, @maxdate) )  as [unitdate/@normal]       --- !!! NEM lehet üres 
						   		     ,'coverage_to'                       as [unitdate/@label]
						             ,convert( date, isNull( i.IntezesIdopontja, @maxdate) )  as [unitdate]
								     ,Null                     --- elhatároló									 
								     ,'case_id'             as [note/@type]
						             ,Null                    as [note/p/ref]                    --- NEM kötelező, így kihagyjuk
								     ,Null                     --- elhatároló 
								     ,'case_sys_id'         as [note/@type]
						             ,Null                    as [note/p/ref]                    --- NEM kötelező, így kihagyjuk
								     ,Null                     --- elhatároló 
						             ,'record_direction'    as [note/@type]                      --- XSD szerint kötelező   --- MNL szerint kisbetű
						             ,ISNull( ( select kt.Nev from [dbo].KRT_KodTarak kt
                                         inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'POSTAZAS_IRANYA'
                                         where getdate() between kt.ErvKezd and kt.ErvVege
										   and ( kt.Kod collate database_default = i.PostazasIranya collate database_default) 
										   and kt.Org = @Org_Id ), '')  as [note/p]
								     ,Null                     --- elhatároló 
									 ,convert( date, i.IktatasDatuma ) as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						   		     ,'arrived_date'        as [unitdate/@label]                --- NEM kötelező, így lehet Null az értéke
						             ,convert( date, i.IktatasDatuma ) as [unitdate]
								     ,Null                     --- elhatároló									 
								     ,'arriving_mode'       as [note/@type]                     --- NEM kötelező, így lehet Null az értéke
						             ,/*ISNull(*/ ( select kt.Nev from [dbo].KRT_KodTarak kt
                                         inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'KULDEMENY_KULDES_MODJA'
										 inner join [dbo].EREC_KuldKuldemenyek k on k.KuldesMod = kt.Kod and k.IraIratok_ID = i.ID                   --- remélem Iratok és Küldemenyek közt 1-1 a megfeleltetés
                                         where getdate() between kt.ErvKezd and kt.ErvVege
										   --- esetleg így lenne gyorsabb ? --- and ( kt.Kod collate database_default = ( select k.KuldesMod collate database_default from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID ) ) 
										   and kt.Org = @Org_Id )/*, '')*/  as [note/p]         --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló									 
								     ,'delivery_priority'   as [note/@type]                 
						             ,/*IsNull(*/ ( select kt.Nev from [dbo].KRT_KodTarak kt
                                         inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'SURGOSSEG'
										 inner join [dbo].EREC_KuldKuldemenyek k on k.Surgosseg = kt.Kod and k.IraIratok_ID = i.ID                   --- remélem Iratok és Küldemenyek közt 1-1 a megfeleltetés
                                         where getdate() between kt.ErvKezd and kt.ErvVege
										   and kt.Org = @Org_Id )/*, '')*/  as [note/p]        --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló									 
 				   		             ,'Taker'                   as [origination/@label]
						             ,/*IsNull(*/ (select cs.Nev from [dbo].KRT_Csoportok cs 
									    inner join EREC_KuldKuldemenyek k on k.Letrehozo_ID = cs.ID and k.IraIratok_ID = i.ID 
										where cs.Org = @Org_Id)/*, '')*/ as [origination/name]   --- NEM kötelező, így lehet Null az értéke
								     ,Null                          --- elhatároló									 
								     ,'arriving_id'         as [note/@type]
						             ,/*IsNull(*/ ( select k.Erkezteto_Szam from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID )/*, '')*/ as [note/p]    --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló									 
 				   		             ,'Sender'              as [origination/@label]
									 ,/*IsNull(*/ ( select k.NevSTR_Bekuldo from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID )/*, '')*/ as [origination/name]    --- NEM kötelező, így lehet Null az értéke
								     ,Null                          --- elhatároló									 
 				   		             ,'Sender'              as [origination/@label]
									 ,/*IsNull(*/ ( select k.NevSTR_Bekuldo from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID )/*, '')*/ as [origination/corpname]   --- !??? ide nem ez kellene valószínűleg    --- NEM kötelező, így lehet Null az értéke
								     ,Null                          --- elhatároló									 
 				   		             ,'Sender'              as [note/@type]
									 ,/*IsNull(*/ ( select k.CimSTR_Bekuldo from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID )/*, '')*/ as [note/p/address/addressline]    --- NEM kötelező, így lehet Null az értéke
								     ,Null                          --- elhatároló									 
                                     ,'medium_type'         as [physdesc/@label]
						             ,/*IsNull(*/ ( select kt.Nev from [dbo].KRT_KodTarak kt
                                         inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'UGYINTEZES_ALAPJA'
                                         where getdate() between kt.ErvKezd and kt.ErvVege
										   and ( kt.Kod collate database_default = i.AdathordozoTipusa collate database_default) 
										   and kt.Org = @Org_Id )/*, '')*/  as [physdesc]                   --- NEM kötelező, így lehet Null az értéke
								     ,Null                          --- elhatároló									 
 				   		             ,'opener'              as [origination/@label]
									 ,Null                    as [origination/name]                         --- NEM kötelező, így kihagyjuk
									 --- ha mégis kell, akkor legyen ez: ,( select k.NevSTR_Bekuldo from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID ) as [origination/name]
								     ,Null                          --- elhatároló									 
									 ,convert( date, ( select k.FelbontasDatuma from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID ) ) as [unitdate/@normal]     --- NEM kötelező, így lehet Null az értéke
						   		     ,'opening_timestamp'   as [unitdate/@label]
						             ,convert( date, ( select k.FelbontasDatuma from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID ) ) as [unitdate]
								     ,Null                     --- elhatároló									 
						   		     ,'opening_note'        as [note/@type]
						             ,/*IsNull(*/ ( select k.BontasiMegjegyzes from [dbo].EREC_KuldKuldemenyek k where k.IraIratok_ID = i.ID )/*, '')*/ as [note/p]      --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló									 
						   		     ,'record_creator'      as [origination/@label]                               --- XSD szerint ez kisbetű
						             ,/*IsNull(*/ (select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = i.FelhasznaloCsoport_ID_Iktato and cs.Org = @Org_Id)/*, '')*/ as [origination/name]     --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló
						             ,'record_status'       as [note/@type]                                       --- MNL szerint kisbetű
						             ,/*IsNull(*/ ( select kt.Nev from [dbo].KRT_KodTarak kt
                                         inner join [dbo].KRT_kodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'IRAT_ALLAPOT'
                                         where getdate() between kt.ErvKezd and kt.ErvVege
										   and kt.Kod = i.Allapot 
										   and kt.Org = @Org_Id)/*, '')*/ as [note/p]                          --- NEM kötelező, így lehet Null az értéke
						             ,Null                                 --- elhatároló						
						             ,'reference_record_id' as [note/@type]
									 --- ,IsNull(i.HivatkozasiSzam, '')     as [note/p/ref]              --- NEM kötelező, így lehet Null az értéke
									 ,i.HivatkozasiSzam     as [note/p/ref]              
						             ,Null                                 --- elhatároló															 
						             ,'Supplements'         as [note/@type]                          --- MNL szerint nagy kezdőbetűvel
									 ,Null                    as [note/p]                              --- ??? Nem tudom, mit adahatnék ide adatként   --- NEM kötelező, így kihagyjuk
						             ,Null                                 --- elhatároló															 
						             ,'supplement_no'       as [note/@type]
									 ,( select Count(1) from [dbo].EREC_Mellekletek m where m.IraIrat_ID = i.ID ) as [note/p]     --- NEM kötelező, így lehet Null az értéke 
						             ,Null                                 --- elhatároló															 
						             ,'ReferTo'             as [note/@type]                          --- MNL mintában szerepelt
									 ,Null                    as [note/list]                           --- megkérdeztem, mi kell ide ?    --- NEM kötelező, így kihagyjuk
						             ,Null                                 --- elhatároló															 
									 ,convert( date, i.Hatarido ) as [unitdate/@normal]              --- NEM kötelező, így lehet Null az értéke
						   		     ,'administering_deadline'    as [unitdate/@label]
						             ,convert( date, i.Hatarido ) as [unitdate]
								     ,Null                     --- elhatároló
									 ,convert( date, i.IntezesIdopontja ) as [unitdate/@normal]      --- NEM kötelező, így lehet Null az értéke
						   		     ,'adjustment_time'     as [unitdate/@label]
						             ,convert( date, i.IntezesIdopontja ) as [unitdate]
								     ,Null                     --- elhatároló
 				   		             ,'ApprovedBy'          as [origination/@label]
									 ,/*IsNull(*/ (select cs.Nev from [dbo].KRT_Csoportok cs where cs.ID = i.FelhasznaloCsoport_Id_Expedial and cs.Org = @Org_Id)/*, '')*/ as [origination/name]   --- NEM kötelező, így lehet Null az értéke
								     ,Null                     --- elhatároló
									 ,convert( date, Null ) as [unitdate/@normal]                    --- NEM kötelező, így lehet Null az értéke
						   		     ,'ApprovalDate'        as [unitdate/@label]
						             ,convert( date, Null ) as [unitdate]
								     ,Null                     --- elhatároló
									 ,@Iratkepzo_Neve       as [origination/corpname]                --- XSD szerint ez Tag és nem attribute
                                     ,Null                    as [abstract]                        --- NEM kötelező, így kihagyjuk
									 ,Null                    as [langmaterial/language]           --- NEM kötelező, így kihagyjuk
						             ,Null                    as [note/p]                          --- NEM kötelező, így kihagyjuk 
									 ,Null                     --- elhatároló
									 ,'handling_notes'      as [note/@type]
						             ,Null                    as [note/p]                          --- NEM kötelező, így kihagyjuk
									 ,Null                     --- elhatároló									 
								     /* --- XSD szerint ezek is kellenek - DE az MNL sablonban nincsenek benne 
                                     ,Null                  --- <archdesc><did><note type=”sendout_mode”>
						   		     ,'sendout_time'        as [unitdate/@label]
						             ,i.ExpedialasDatuma    as [unitdate]
								     ,Null                     --- elhatároló									 									 
									 */
									 
                                   FOR XML PATH('did'),type
								 )

						        /* index */                                      --- tárgyszavak
						        ,( select
								     Null                     as [indexentry/subject]              --- NEM kötelező, így kihagyjuk
                                   FOR XML PATH('index'),type
								 )

						        /* odd */
						        ,( select
						              'Recipient'           as [@type]            --- XSD szerint kisbetű
 						             /* p */
						             ,( select 
 						                   /*IsNull(*/ ( select p.NevSTR_Cimzett from [dbo].EREC_PldIratPeldanyok p where p.IraIrat_ID = i.ID and p.Sorszam = 1 )/*,'' )*/ as [persname]    --- NEM kötelező, így lehet Null az értéke
                                          ,Null               as [corpname]         --- ??? ide mit írhatnék            --- NEM kötelező, így kihagyjuk
										  ,IsNull( ( select p.CimSTR_Cimzett from [dbo].EREC_PldIratPeldanyok p where p.IraIrat_ID = i.ID and p.Sorszam = 1 ),'') as [address/addressline]
										  ,Null                          --- elhatároló									 
								
                                        FOR XML PATH('p'),type
			                          )
								
                                   FOR XML PATH('odd'),type
			                     )

						        /* odd */
						        ,( select
						              'other_approval'      as [@type]
 						             /* p */
						             ,( select 
 						                   Null               as [name]         --- ??? ide mit írhatnék  --- NEM kötelező, így kihagyjuk
                                          ,Null               as [date]                                   --- NEM kötelező, így kihagyjuk
										  ,Null                          --- elhatároló									 
								
                                        FOR XML PATH('p'),type
			                          )
								
                                   FOR XML PATH('odd'),type
			                     )

						        /* index */                                      --- Az egyes földrajzi tárgyszavak
						        ,( select
								     Null                     as [indexentry/geogname]					--- NEM kötelező, így kihagyjuk
                                   FOR XML PATH('index'),type
								 )                                                
						        ,Null                         as [custodhist/p]          --- NEM kötelező, így kihagyjuk
						        ,Null                         as [acqinfo/p]             --- NEM kötelező, így kihagyjuk
						        ,Null                         as [arrangement/p]         --- NEM kötelező, így kihagyjuk
								,'Nincs korlátozás'         as [accessrestrict/p]        --- !!! fogalmam nincs mit kell ide írni

						        /* accessrestrict */
						        ,( select
								     Null                     as [legalstatus]           --- NEM kötelező, így kihagyjuk
                                    ,'rule'                 as [note/@type]
									,Null                     as [note/p]                --- NEM kötelező, így kihagyjuk
									 								 
                                   FOR XML PATH('accessrestrict'),type
								 )
								,'Nincs korlátozás'         as [userestrict/p]         --- !!! fogalmam nincs mit kell ide írni, de XSD elfogadta     --- XSD szerint ez Tag és nem attribute
								,Null                         as [originalsloc/p]       --- NEM kötelező, így kihagyjuk
								,Null                         as [altformavail/p]       --- NEM kötelező, így kihagyjuk
						        ,Null                         as [relatedmaterial/p]    --- NEM kötelező, így kihagyjuk

								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
                                ,'' as [accruals/p]                    --- XSD szerint kellenek üresen
						        ,'' as [bioghist/p]
						        ,'' as [fileplan/p]
						        ,'' as [scopecontent/p]
								*/
								
                              FOR XML PATH('archdesc'),type
			                )
						  
						   FOR XML PATH('ead'),type
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
                  from [dbo].EREC_IraIratok i
                 inner join #tmp_UUID tmp on tmp.ObjID = i.ID and tmp.ObjType = 'IRAT'           --- 2018.09.26., BogI  				  
                 inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                     /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                 inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                 /* inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID */
                 inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                    and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                 /* ! Elég sok a sorok kifejtésekor az EREC_KuldKuldemenyek táblából való lekérdezés. Ha lassú lenne a feldolgozás, akkor be lehet ide JOIN-olni - 
				      DE csak LEFT join-nal, mivel a belső keletkezésű iratoknak nem biztosan van párja */
				 order by ik.Ev, ik.Azonosito, u.Foszam, i.Alszam                             --- azért, hogy az Ügyiratnak ( és fileSec-nek ) megfelelő sorrendben legyen   --- i.Azonosito  

                FOR XML PATH('dmdSec') /* ,type */
			  );  
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_I)
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_I = convert( xml, @s_xml )

/* --- */
  /* dmdSec - Irattári terv */                                --- Egyelőre NEM adjuk a teljes Irattári tervet, csak azokat az Irattári tétel sorokat, amelyekre adunk át ügyiratot - de nálunk nincs Irattári terv entitás, így a tételekből képzem az 'Irattári terv' átadandó adatait
  SET @IrattariTerv_ID = NEWID()                              --- !!! Ki kell kérni változóba egy NEWID()-t, mivel később még szükségünk lesz rá 
  declare @dmdSec_IT xml;
  SET @dmdSec_IT =
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'current'                                      as [@STATUS]                --- konstans 
				  ,'uuid-' + convert( nvarchar(40), @IrattariTerv_ID ) as [@ID]               --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,'1'                                            as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,convert( nvarchar(19), @Atad_Date, 126 )       as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
                  
				  /* mdWrap */
			      ,( select 
  	                   ''                                         as [@LABEL] 	             --- megeszi az üres stringet 				  
					  ,'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- /* required; attribute;  An xsd:ID value must be an NCName. */  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,'MDTYPEVERSION'                            as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,@Metaadat_Sema                             as [@MDTYPE]

				      /* xmlData */
                      ,( select Null
		                /* EAD - Irattári terv */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             @Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
								,'HU'                            as [eadid/@countrycode]
							    ,'IT' + convert(varchar(4),year(it.minErvKezd)) as [eadid]            --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
							    ,Null                            --- elhatároló 
				                ,'IT' + convert(varchar(4),year(it.minErvKezd)) + ' - irattári terv' as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select 
						         'classification'          as [@otherlevel]                         --- MNL ezt kéri, szerinte kisbetűvel  --- !? EAD.xsd validáció NEM veszi be
								,'otherlevel'              as [@level]

						        /* did */
						        ,( select
						              'IT' + convert(varchar(4),year(it.minErvKezd))       as [unitid]
				                     ,'IT' + convert(varchar(4),year(it.minErvKezd)) + ' - irattári terv' as [unittitle] 
									 ,convert( date, it.minLetrehozasIdo ) as [unitdate/@normal]
						   		     ,'originated_date'                    as [unitdate/@label]                       --- !!! NEM lehet üres
						             ,convert( date, it.minLetrehozasIdo ) as [unitdate]
								     ,Null                 --- elhatároló
									 ,convert( date, datetimefromparts(year(it.minErvKezd),1,1,0,0,0,0) ) as [unitdate/@normal]
						             ,'period_begin'                       as [unitdate/@label]                         --- !!! NEM lehet üres
						             ,convert( date, datetimefromparts(year(it.minErvKezd),1,1,0,0,0,0) ) as [unitdate]
								     ,Null                 --- elhatároló
									 ,convert( date, datetimefromparts(year(it.minErvKezd),12,31,0,0,0,0) ) as [unitdate/@normal] 	--- "Egyetlen elektronikus átvételi csomagban sem szerepelhet több irattári terv."
						             ,'period_end'                         as [unitdate/@label]                         --- !!! NEM lehet üres és éven belűl kell maradjon mindenképpen
						             ,convert( date, datetimefromparts(year(it.minErvKezd),12,31,0,0,0,0) ) as [unitdate] 	
									 ,Null                 --- elhatároló
                                     ,Null                   as [abstract]                       --- NEM kötelező, így kihagyjuk
									 ,Null                   as [note/p]                         --- NEM kötelező, így kihagyjuk
								     ,Null                 --- elhatároló
									 
                                   FOR XML PATH('did'),type
								 )
								,'log'                     as [odd/@type]
						        ,Null                        as [odd/chronlist]                 --- Nincs leírás róla, így kihagyjuk
								,Null                          --- elhatároló 
                                
								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
						        ,'' as [accruals/p]                    --- XSD szerint kellenek üresen       /* --- ( select Null FOR XML PATH(''),type ) as [accruals/p/ref] */
						        ,'' as [accessrestrict/p]
						        ,'' as [acqinfo/p]
						        ,'' as [altformavail/p]
						        ,'' as [arrangement/p]
						        ,'' as [bioghist/p]
						        ,'' as [custodhist/p]
						        ,'' as [fileplan/p]
						        ,'' as [odd/p]
						        ,'' as [originalsloc/p]
						        ,'' as [relatedmaterial/p]
						        ,'' as [scopecontent/p]
						        ,'' as [userestrict/p]
								*/
								 
                              FOR XML PATH('archdesc'),type
			                )
						  
						   FOR XML PATH('ead'),type                                        --- MNL minta szerint
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
				  from ( select min(it.LetrehozasIdo) as minLetrehozasIdo                   /* itt eleve AGGREGATE fv-ek lettek használva */
							   ,min(it.ErvKezd)       as minErvKezd
							   ,max(it.ErvVege)       as maxErvVege
						   from [dbo].EREC_IraIrattariTetelek it 
                          /* -- lecserélve
						  inner join [dbo].EREC_UgyUgyiratok u on u.IraIrattariTetel_ID = it.ID
						  /* ---
						   from [dbo].EREC_UgyUgyiratok u 
						  inner join [dbo].EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
						  --- */
                          inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
					                                                                 and ijt.Obj_type = 'EREC_UgyUgyiratok'                --- az Obj_type -ra tett feltétel nem biztos, hogy kell
 					      where 1 = 1
						  -- */
                          where 1 = 1 
						    and exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                          inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
                                                                                                           and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                          where u.IraIrattariTetel_ID = it.ID
                                       )

						    /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                          /* order by u.Foszam */
					   ) as it
                FOR XML PATH('dmdSec')/* ,type */
			  );
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_IT)
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_IT = convert( xml, @s_xml )

/* --- */
  /* dmdSec - Irattári tételek */             
  --- Egyelőre NEM adjuk a teljes Irattári tervet, csak azokat az Irattári tétel sorokat, amelyekre adunk át ügyiratot !?
  --- Így nem kell belekevernia z Ágazati jeleket és az Ágazati jelek hierarchiát - kérdés, hogy ez így jó lesz-e az MNL-nek ? ( meg üzleti logikailag ? )
  
  --- az Irattári tétel soroknak erre a feladásra kiosztott NEWID()-k 'letépése'    2018.09.26., BogI
  SET @cur_UUID = CURSOR FOR 
                  ( select itt.ID from [dbo].EREC_IraIrattariTetelek itt
                     where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                     inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                              and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                     where u.IraIrattariTetel_ID = itt.ID
				                  )
				  )             --- !!! ugyanazok a feltételek kellenek, mint az alábbiakban a @dmdsec_ITT felgyűjtésében
  OPEN @cur_UUID
  FETCH NEXT FROM @cur_UUID INTO @ObjId
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    Insert INTO #tmp_UUID ( ObjType, ObjID, UUID ) values ( 'IRATTARITETEL', @ObjID, NEWID() )
    FETCH NEXT FROM @cur_UUID INTO @ObjID
  end;
  CLOSE @cur_UUID;  
  DEALLOCATE @cur_UUID;
  
  declare @dmdSec_ITT xml;
  SET @dmdSec_ITT =
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'current'                                      as [@STATUS]                --- konstans 
				  --- ,'uuid-' + convert( nvarchar(40), itt.ID )      as [@ID]                --- 2018.09.26., BogI    --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,'uuid-' + convert( nvarchar(40), tmp.UUID )    as [@ID]                    --- az Irattári tétel sornak erre a feladásra kiosztott NEWID() azonositója
				  ,'1'                                            as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,convert( nvarchar(19), @Atad_Date, 126 )       as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
                  
				  /* mdWrap */
			      ,( select 
  	                   ''                                         as [@LABEL] 	             --- megeszi az üres stringet 				  
					  ,'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- /* required; attribute;  An xsd:ID value must be an NCName. */  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,'MDTYPEVERSION'                            as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,@Metaadat_Sema                             as [@MDTYPE]

				      /* xmlData */
                      ,( select Null
		                /* EAD - Irattári tételek */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             @Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
								,'HU'                            as [eadid/@countrycode]
							    ,itt.IrattariTetelszam           as [eadid]                           --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
				                ,itt.Nev                         as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select 
						         'class'                    as [@level]
						        /* did */
						        ,( select
						              itt.IrattariTetelszam as [unitid]
				                     ,itt.Nev               as [unittitle] 
									 ,convert( date, itt.LetrehozasIdo ) as [unitdate/@normal]
						   		     ,'originated_date'     as [unitdate/@label]                   --- kötelező és NEM lehet üres
						             ,convert( date, itt.LetrehozasIdo ) as [unitdate]
								     ,Null                 --- elhatároló
									 ,convert( date, itt.ErvKezd )       as [unitdate/@normal]
						             ,'period_begin'       as [unitdate/@label]                   --- kötelező és NEM lehet üres
						             ,convert( date, itt.ErvKezd )       as [unitdate]
								     ,Null                 --- elhatároló
									 ,convert( date, datetimefromparts(year(itt.ErvKezd),12,31,0,0,0,0) ) as [unitdate/@normal] 	
						             ,'period_end'         as [unitdate/@label]                   --- kötelező és NEM lehet üres  
						             ,convert( date, datetimefromparts(year(itt.ErvKezd),12,31,0,0,0,0) ) as [unitdate] 	
								     ,Null                 --- elhatároló
                                     ,Null                   as [abstract]                   --- NEM kötelező, íyg kihagyjuk
									 ,Null                   as [note/p]                     --- NEM kötelező, íyg kihagyjuk
								     ,Null                 --- elhatároló
									 
                                   FOR XML PATH('did'),type
								 )
								 
								,'default_disposal_action'           as [note/@type]          --- kötelező 
						        ,ISNull( ( select kt.Nev from [dbo].KRT_KodTarak kt
                                            inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'IRATTARI_JEL'
                                            where getdate() between kt.ErvKezd and kt.ErvVege
										      and ( kt.Kod collate database_default = itt.IrattariJel collate database_default) 
										      and kt.Org = @Org_Id ), '')  as [note/p]
								,Null                 --- elhatároló 
								,'default_retention_period_interval' as [note/@type]        --- kötelező 
						        ,case when itt.IdoEgyseg = 'E' then 'Évek'
								      when itt.IdoEgyseg = 'H' then 'Hónapok'
									  when itt.Idoegyseg = 'W' then 'Hetek'                --- ??? Nem tudom, mi jelöli - én most ezzel jelölöm
									  else 'Napok'                                         --- NULL -t 'Napok' -nak tekintem
								 end                                 as [note/p]
								,Null                 --- elhatároló 
								,'default_retention_period_duration' as [note/@type]         --- kötelező 
						        --- ,itt.MegorzesiIdo                    as [note/p]
						        ,ISNull( ( select kt.Nev from [dbo].KRT_KodTarak kt
                                            inner join [dbo].KRT_KodCsoportok kcs on kcs.id = kt.KodCsoport_ID and kcs.KOD = 'SELEJTEZESI_IDO'
                                            where getdate() between kt.ErvKezd and kt.ErvVege
										      and ( kt.Kod collate database_default = itt.MegorzesiIdo collate database_default) 
										      and kt.Org = @Org_Id ), '')  as [note/p]
								,Null                 --- elhatároló

								/* --- XSD szerint ezek is kellenek üresen - DE az MNL sablonban nincsenek benne 
						        ,'' as [accruals/p]                    --- XSD szerint kellenek üresen
						        ,'' as [accessrestrict/p]
						        ,'' as [acqinfo/p]
						        ,'' as [altformavail/p]
						        ,'' as [arrangement/p]
						        ,'' as [bioghist/p]
						        ,'' as [custodhist/p]
						        ,'' as [fileplan/p]
						        ,'' as [odd/p]
						        ,'' as [originalsloc/p]
						        ,'' as [relatedmaterial/p]
						        ,'' as [scopecontent/p]
						        ,'' as [userestrict/p]
								*/
								
                              FOR XML PATH('archdesc'),type
			                )						   
						  
						   FOR XML PATH('ead'),type
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
				  from [dbo].EREC_IraIrattariTetelek itt
                 /* --- lecserélve
				 inner join
                     ( select distinct itb.ID from [dbo].EREC_IraIrattariTetelek itb				 
			            inner join [dbo].EREC_UgyUgyiratok u on u.IraIrattariTetel_ID = itb.ID
                        inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = ij.ID
   		                                                                           and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			            where 1 = 1
                          /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
					 ) itsub on itsub.ID = itt.ID
				 */
				 inner join #tmp_UUID tmp on tmp.ObjID = itt.ID and tmp.ObjType = 'IRATTARITETEL'                 --- 2018.09.26., BogI 					 
                 where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                 inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                          and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                 where u.IraIrattariTetel_ID = itt.ID
				              )
                 order by itt.ErvKezd		 

                FOR XML PATH('dmdSec') /* ,type */
			  );
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_ITT)
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_ITT = convert( xml, @s_xml )
  
/* --- */
   /* fileSec */			   
  --- a Csatolmány soroknak erre a feladásra kiosztott NEWID()-k 'letépése'    2018.09.26., BogI
  SET @cur_UUID = CURSOR FOR 
                  ( select cs.ID from [dbo].EREC_Csatolmanyok cs 
				     inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID
                     inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                    /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                     inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                     inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
                     inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                        and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
					 where 1 = 1
					   and EXISTS ( select 1 from [dbo].KRT_Dokumentumok d      /* csak azokra képezzünk fileGrp-ot, amelyhez van dokumentum - DE mindhez kellene legyen !? */
                                     where d.ID = cs.Dokumentum_ID
									   and d.Org = @Org_Id
								  )
					   and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum
						/* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
				  )             --- !!! ugyanazok a feltételek kellenek, mint az alábbiakban a @dmdsec_ITT felgyűjtésében
  OPEN @cur_UUID
  FETCH NEXT FROM @cur_UUID INTO @ObjId
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    Insert INTO #tmp_UUID ( ObjType, ObjID, UUID ) values ( 'CSATOLMANY', @ObjID, NEWID() )
    FETCH NEXT FROM @cur_UUID INTO @ObjID
  end;
  CLOSE @cur_UUID;  
  DEALLOCATE @cur_UUID;
   
  declare @fileSec xml;
  SET @fileSec =
  			  ( select Null
                  
				  /* fileGrp */
			      ,( select 
  	                        'original'                                        as [@ID]              --- "A fileGrp tageknek így kéne statikusan kinéznie: <fileGrp ID="original" USE="original">"
						   ,'original'                                        as [@USE] 

				           /* file */
			               ,( select 
					                 --- 'uuid-' + convert( nvarchar(40), cs.ID )  as [@ID]         --- 2018.09.26., BogI     --- xsd:ID típusú   --- "egyedi uuid azonosító. Ez az azonosító A structMap-ban is szerepelni fog a fájlra utalva"
									 'uuid-' + convert( nvarchar(40), tmp.UUID ) as [@ID]           --- a Csatolmány sornak erre a feladásra kiosztott NEWID() azonositója
								    ,convert( nvarchar(19), @Atad_Date, 126 ) as [@CREATED]         --- MNL szerint: "Csomag létrehozásának időpontja másodpercpontosan" "Fontos a dátum formátumát követni!"
									--- XSD szerint ez kellett volna: ,d.LetrehozasIdo as [@CREATED]
									,d.Meret                                  as [@SIZE]            --- "a fájl mérete bájtban"
									,'1'                                      as [@SEQ]             --- MNL szerint akkor is, ha egy Irathoz több file tartozik                       
									,''                                       as [@MIMETYPE]        --- MNL szerint: "a mimeype-ot üresen kell hagyni: MIMETYPE='' " ;
									--- XSD szerint ez kellene: ,d.Formatum as [@MIMETYPE]          --- d.Formatum = IANA MIME type ???
									,@CheckSum_Type                           as [@CHECKSUMTYPE]    --- "A befogadás csak MD5-el készült checksum esetén sikeres"
									,d.KivonatHash                            as [@CHECKSUM]       --- "A fájl érintetlenségét igazoló „checksum” értéke. (MD5 CHECKSUM TYPE)"
									--- !!! DE d.KivonatHash SHA-1-ben lett kifejezve -> a file-ok fizikai csatolásakor a GUI felületen (Lami) felülírja ezt az értéket az akkor kiszámolt MD5 értékkel
															   
  				                    /* FLocat */                      
									,[dbo].fn_Get_MetadataXML_Flocat(d.ID)                 --- kitettem egy eljárásba, hogy csak ide tegye bele az xlink hivatkozást 
                                from [dbo].KRT_Dokumentumok d 
                               inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID and cs.IraIrat_ID = i.ID
							                                        and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum 
                               inner join #tmp_UUID tmp on tmp.ObjID = cs.ID and tmp.ObjType = 'CSATOLMANY'                 --- 2018.09.26., BogI   
							   /* --- inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID and i.Ugyirat_ID = u.ID --- */
							   where 1 = 1
							     and d.Org = @Org_Id
							   order by i.Alszam, cs.ID                                  --- elegendő az Alszam, mivel fileGrp -on belűl van

                              FOR XML PATH('file'),type						  
			                )

                       from [dbo].EREC_IraIratok i
                      inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                     /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                      inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                      inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
                      inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                         and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
					  where 1 = 1
					    and EXISTS ( select 1 from [dbo].KRT_Dokumentumok d      /* csak azokra képezzünk fileGrp-ot, amelyhez van dokumentum - DE mindhez kellene legyen !? */
                                             inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID
											                                       and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum
                                             inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID and i.Ugyirat_ID = u.ID and d.Org = @Org_Id
								   )
						/* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
				      order by ik.Ev, ik.Azonosito, u.Foszam, i.Alszam                             --- azért, hogy az Ügyiratnak ( és fileSec-nek ) megfelelő sorrendben legyen   --- i.Azonosito  

					  FOR XML PATH('fileGrp'),type
			       )
                /* --- mivel nincs plusz feltétel, így elegendő feljebb a direkt @IraJegyzek_ID szűrés 
                  from [dbo].EREC_IraJegyzekek ij
                 where 1 = 1
                   and ij.ID = @IraJegyzek_ID
                 --- */
                FOR XML PATH('fileSec') /* ,type */
			  );	
  /* --- <Flocat> tag-enként benne marad az xmlns:xlink NAMESPACE hivatkozás, de azt nem engedi kiölni  --- */

/* --- */				  
   /* structMap aggregation */			 
  declare @structMapAggr xml;                        --- ebben kerül leírásra az Iktatókönyvek -> Ügyiratok -> Iratok -> fizikai file-ok ( tényleges dokumentumok ) kapcsolat            
  SET @structMapAggr =
              /* structMap */
              ( select
			       'aggregation'                                    as [@TYPE]                --- MNL OL minta file alapján
                  
				  /* div */                          --- Iktatókönyvek szintje      
			      ,( select
						   'otherlevel;register'                    as [@TYPE]                --- MNL OL minta file szerint
						  --- ,'uuid-' + convert( nvarchar(40), ik.ID ) as [@DMDID]           --- 2018.09.26., BogI    --- DMDID = az Iktatókönyv egyedi azonosítója    --- xsd:ID típusú	  
						  ,'uuid-' + convert( nvarchar(40), tmpIK.UUID ) as [@DMDID]               --- DMDID = az Iktatókönyv sornak erre a feladásra kiosztott NEWID() azonositója
						  ,''                                       as [@ADMID]  

				          /* div */                  --- Ügyiratok szintje
			              ,( select
						           'file'                                   as [@TYPE]
						          --- ,'uuid-' + convert( nvarchar(40), u.ID )  as [@DMDID]        --- 2018.09.26., BogI  --- DMDID = az Ügyirat egyedi azonosítója
								  ,'uuid-' + convert( nvarchar(40), tmpu.UUID )  as [@DMDID]       --- DMDID = az Ügyirat sornak erre a feladásra kiosztott NEWID() azonositója
						          ,''                                       as [@ADMID]  

				                  /* div */          --- Iratok szintje
			                      ,( select
						                   'otherlevel;record'                      as [@TYPE]
						                  --- ,'uuid-' + convert( nvarchar(40), i.ID )  as [@DMDID]   --- 2018.09.26., BogI    --- DMDID = az Irat egyedi azonosítója
										  ,'uuid-' + convert( nvarchar(40), tmpi.UUID )  as [@DMDID]       --- DMDID = az Irat sornak erre a feladásra kiosztott NEWID() azonositója
						                  ,''                                       as [@ADMID]  
                          
				                          /* fptr */
			                              ,( select
					                               --- 'uuid-' + convert( nvarchar(40), cs.ID ) as [@FILEID]       --- 2018.09.26., BogI --- cs.ID -ra tudok hivatkozni structMap -ban is !!!  --- required; attribute;  --- FILEID = Csatolmány egyedi azonositója
												   'uuid-' + convert( nvarchar(40), tmpcs.UUID ) as [@FILEID]     --- FILEID = a Csatolmány sornak erre a feladásra kiosztott NEWID() azonositója
                                               from [dbo].EREC_Csatolmanyok cs 
											  inner join #tmp_UUID tmpcs on tmpcs.ObjID = cs.ID and tmpcs.ObjType = 'CSATOLMANY'                 --- 2018.09.26., BogI   
							                  where 1 = 1
							                    and cs.IraIrat_ID = i.ID
							                    and getdate() between cs.ErvKezd and cs.ErvVege           /* ez jelzi, ha már törölt egy dokumentum */
							                  order by cs.ID
							   
                                             FOR XML PATH('fptr'),type
			                               )
                                       from [dbo].EREC_IraIratok i
									  inner join #tmp_UUID tmpi on tmpi.ObjID = i.ID and tmpi.ObjType = 'IRAT'                 --- 2018.09.26., BogI  
			                          where 1 = 1
								        and i.Ugyirat_Id = u.ID
				                      order by i.Alszam                                       --- azért, hogy az Ügyiratnak ( és fileSec-nek ) megfelelő sorrendben legyen   --- i.Azonosito  

                                     FOR XML PATH('div'),type 							      --- Irat
			                       )
                               from [dbo].EREC_UgyUgyiratok u
							  inner join #tmp_UUID tmpu on tmpu.ObjID = u.ID and tmpu.ObjType = 'UGYIRAT'                 --- 2018.09.26., BogI 
                              inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
                                                                                               and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			                  where 1 = 1
						        and u.IraIktatokonyv_ID = ik.ID                               --- !!! ez is kell, hogy a sorrend jó legyen
				              order by u.Foszam                                               --- legyen szinkronban filegrp rendezésével   --- u.Azonosito  

                             FOR XML PATH('div'),type 							              --- Ügyirat
			               )
                       from [dbo].EREC_IraIktatokonyvek ik
					  inner join #tmp_UUID tmpik on tmpik.ObjID = ik.ID and tmpik.ObjType = 'IKTATOKONYV'                 --- 2018.09.26., BogI 
                      where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                      inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                              and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                      where u.IraIktatokonyv_ID = ik.ID
				                   )
                      order by ik.Ev, ik.Azonosito

                     FOR XML PATH('div'),type 							                      --- Iktatókönyv
			       )

                FOR XML PATH('structMap'),type 
			  );


/* --- */				  
   /* structMap classification */			 
  declare @structMapClass xml;                        --- ebben kerül leírásra az 'Irattári terv' -> Irattári tételek kapcsolat --- !!! csak az érintett Irattári tétel sorokat adom át (?)           
  SET @structMapClass =
              ( select
			       'classification'        as [@TYPE]
                  
				  /* div */
			      ,( select
  	                       'otherlevel;classification'                          as [@TYPE]                --- 
						   ,'uuid-' + convert( nvarchar(40), @IrattariTerv_ID ) as [@DMDID]               --- xsd:ID típusú						   
                           
				           /* div */
			               ,( select
					                 'class'                                    as [@TYPE]
                                    --- ,'uuid-' + convert( nvarchar(40), itt.ID )  as [@DMDID]           --- 2018.09.26., BogI
									,'uuid-' + convert( nvarchar(40), tmpitt.UUID ) as [@DMDID]           --- DMDID = az Irattári tétel sornak erre a feladásra kiosztott NEWID() azonositója
				                from [dbo].EREC_IraIrattariTetelek itt
							   inner join #tmp_UUID tmpitt on tmpitt.ObjID = itt.ID and tmpitt.ObjType = 'IRATTARITETEL'                 --- 2018.09.26., BogI 	
                               where exists ( select 1 from [dbo].EREC_UgyUgyiratok u 
                                               inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                                                        and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
                                               where u.IraIrattariTetel_ID = itt.ID
				                            )
                               order by itt.ErvKezd		 
							   
                              FOR XML PATH('div'),type
			                )
						
                     FOR XML PATH('div'),type							
			       )

                FOR XML PATH('structMap') /* ,type */
			  );

			  
  /*
  <xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://www.loc.gov/METS/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.loc.gov/METS/">
  <xsd:import schemaLocation="http://www.loc.gov/standards/xlink/xlink.xsd" namespace="http://www.w3.org/1999/xlink"/>                       '
  */

  --- @xml_prolog-ot 'magában hordozza'  a struktúra, de láthatólag csak a VS teszi ki 
  ;    /* --- szet kellett szednem, mert amikor egyben volt, akkor benyomta minden csomópontra a NAMESPACE literált; --- declare namespace -szel vajon lehetne egyszerűbben operálni ? --- */ 
  --- with XMLNAMESPACES( 'http://www.loc.gov/METS/' as [mets], 'http://www.w3.org/1999/xlink' as xlink, 'http://www.w3.org/2001/XMLSchema-instance' as xsi /* , DEFAULT 'http://www.loc.gov/METS/' */ )
  with XMLNAMESPACES( DEFAULT 'http://www.loc.gov/METS/', 'http://www.w3.org/1999/xlink' as xlink, 'http://www.w3.org/2001/XMLSchema-instance' as xsi  )  
  SELECT @MetadataXML = 
      ( select /* mets */
	           ij.Nev                         as [@LABEL]
			  ,@SIP_azonosito                 as [@OBJID]
			  ,@SIP_type /*'SIP'*/            as [@TYPE]
	          ,'mets'                         as [@xsi:type]             	 --- MNL mintában benne volt
/*
               'mets'                         as [@xsi:type]             	 --- MNL mintában benne volt
			  ,'SIP'                          as [@TYPE]
			  ,@SIP_azonosito                 as [@OBJID]
			  ,ij.Nev                         as [@LABEL]
*/
/* --- */
             /* metsHdr */
			 ,' METADATA.xml fejléc elemek ' as [comment()]
             ,@metsHdr
/* --- */			     
             /* dmdSec - Iktatókönyvek */
			 ,' Iktatókönyvek ' as [comment()]
			 ,@dmdSec_IK
/* --- */			     
             /* dmdSec - Ügyiratok */
			 ,' Ügyiratok ' as [comment()]
			 ,@dmdSec_U
/* --- */
             /* dmdSec - Iratok */
			 ,' Iratok ' as [comment()]
			 ,@dmdSec_I
/* --- */
             /* dmdSec - Irattári terv */                                
			 ,' Irattári terv ' as [comment()]
			 ,@dmdSec_IT
/* --- */
             /* dmdSec - Irattári tételek */
			 ,' Irattári tételek ' as [comment()]
			 ,@dmdSec_ITT
/* --- */
             /* fileSec */			 
			 ,' fileSec ' as [comment()]
			 ,@fileSec
/* --- */				  
             /* structMap */			 
			 ,' structMap aggregation ' as [comment()]                                                --- az XSD ellenőrzés alapján kötelező !?
			 ,@structMapAggr

			 ,' structMap classification ' as [comment()]
			 ,@structMapClass
/* --- */			  
         from [dbo].EREC_IraJegyzekek ij
        where 1 = 1
          and ij.ID = @IraJegyzek_ID
      FOR XML PATH('mets'), type
      );

  /* --- Utólagos csúnya patkolás a NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @MetadataXML = convert( xml, replace( convert(nvarchar(MAX), @MetadataXML), 'xmlns=""', '') )         --- DEFAULT kiadásából származó xmlns="" -ek eltüntetése

  /* munkatáblát el kell dobni */
  DROP table #tmp_UUID;
  
  /* --- Hibakezelés --- */
  BEGIN TRY
   if CHARINDEX ('</mets>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
   /* ---
   if CHARINDEX ('</mets:mets>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
   --- */
     /* or LEN( convert(nvarchar(max), @MetadataXML) ) >2000000 */
     SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );  
  END TRY
  BEGIN CATCH
    SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );
  END CATCH

END TRY
BEGIN CATCH
    DECLARE @errorSeverity INT,
        @errorState INT
    DECLARE @errorCode NVARCHAR(1000)    
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    IF ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE()
    ELSE 
        SET @errorCode = ERROR_MESSAGE()

    IF @errorState = 0 
        SET @errorState = 1

    RAISERROR ( @errorCode, @errorSeverity, @errorState )

END CATCH
END

