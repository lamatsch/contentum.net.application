﻿
CREATE PROCEDURE [dbo].[sp_Create_MetadataXML_Type4]
       @IraJegyzek_ID   uniqueidentifier
      ,@Atadas_Statusz  varchar(15)       = 'NEW'     --- 'TEST', 'REPLACEMENT' 
	  ,@ExecutorUser_ID uniqueidentifier	  
	  ,@MetadataXML xml OUTPUT

AS 
BEGIN
/*  ---
 -- #1674 - HU_SIP_METS.XSD szerint validálva ( XMLSpy ); csomópontok kiemelve; METS és Flocat (xlink) namespace beépítve
 -- HU_SIP_EAD.xsd szerint NEM validált (mapping <=> XSD):
 --     "Kedves István, ne használja többet a HU_SIP_EAD.xsd-t validációra! Ez egy nem ide illő xsd és ennek megfelelően nem lehet betölteni az SDB-be."
 -- Jelen megoldás az MNL OL által adott mintafájl alapján készült el, Mezei József útmutatásai szerint !!!
 -- ( Nagyjából fittyet hányva a törvényi előírásnak és az MNL korábban adott leírásainak !? )
 --- */
BEGIN TRY

  set nocount on

  /* alapellenőrzések */
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUser_Id)
  if (@Org_ID is null)
  begin
    RAISERROR('[50202]',16,1)
  end;
  /* ez nem biztos, hogy kell !? elegendő hibakezelés, ha az XML = Null értékkel megy vissza ?
  if NOT Exists ( select 1 from [dbo].EREC_IraJegyzekek ij where 1 = 1 and ij.ID = @IraJegyzek_ID and ij.Tipus = 'L' )
  begin
    RAISERROR('[51501]',16,1)                                                 --- a hibakódok vmi resource file-ban vannak
  end;
  */
    
  /* KRT_Parameterek */
  DECLARE 
          @Atado_Rovidneve        nvarchar(400)
		 ,@Atadas_Tipus           nvarchar(400) 
		 ,@Atadas_Dokumentum      nvarchar(400)
		 ,@Atadas_Iktato          nvarchar(400)
		 ,@Iratkepzo_Neve         nvarchar(100)
		 ,@Iratkepzo_Azonositoja  nvarchar(400)
		 ,@Application_Name       nvarchar(400)
		 ,@Application_Version    nvarchar(400)
		 ,@Leveltar_Neve          nvarchar(400)
		 ,@Leveltar_Azonositoja   nvarchar(400)
         ;         
  SET @Atado_Rovidneve       = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE', @Org_ID);
  SET @Atadas_Tipus          = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_TIPUS', @Org_ID);
  SET @Atadas_Dokumentum     = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_DOKUMENTUM', @Org_ID);
  SET @Atadas_Iktato         = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IKTATO', @Org_ID);
  SET @Iratkepzo_Neve        = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_NEVE', @Org_ID);
  SET @Iratkepzo_Azonositoja = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA', @Org_ID);
  SET @Application_Name      = [dbo].fn_GetKRT_ParameterekErtek('APPLICATION_VERSION', '00000000-0000-0000-0000-000000000000' /*@Org_ID*/);
  SET @Application_Version   = substring(@Application_Name, charindex(' v',@Application_Name)+1,400);
  SET @Application_Name      = substring(@Application_Name, 1, charindex(' v',@Application_Name)-1);
  SET @Leveltar_Neve         = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_LEVELTAR_NEVE', @Org_ID);
  SET @Leveltar_Azonositoja  = [dbo].fn_GetKRT_ParameterekErtek('LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA', @Org_ID);

  DECLARE @SIP_azonosito nvarchar(max)
         ,@Atad_Date     datetime       = getdate()
	     ,@Atad_Sorsz    int            = 1
         ;
  SET @Atad_Sorsz = ( select count(1) from [dbo].EREC_IraJegyzekek ij
                       where 1 = 1
					     and ij.Org = @Org_ID
						 and ij.Tipus = 'L'              --- 'L'evéltári átadás
						 and ij.VegrehajtasDatuma is NOT Null
						 and convert(nvarchar(8), ij.VegrehajtasDatuma, 112) = convert(nvarchar(8), getdate(), 112)
					);
  SET @Atad_Sorsz += 1;
  SET @SIP_azonosito = N'SIP' + '_' + convert(nvarchar(8), @Atad_Date, 112) + '_' + @Atado_Rovidneve + 
                        '_' + right( '000' + convert( nvarchar(3), @Atad_Sorsz ), 3)

  DECLARE @Metaadat_Sema     nvarchar(10)
         ,@CheckSum_Type     nvarchar(10)
		 ,@Atadas_Statusz_HU nvarchar(50)
		 ;
  SET @Metaadat_Sema = 'EAD';
  SET @CheckSum_Type = 'MD5';                 --- "a checkstumtype: CHECKSUMTYPE="MD5" "   --- 'SHA-1';
  
  SET @Atadas_Statusz_HU = ( select case when @Atadas_Statusz = 'NEW' then 'Új csomag'
                                         when @Atadas_Statusz = 'REPLACEMENT' then 'Helyettesítő csomag'
							             when @Atadas_Statusz = 'TEST' then 'Teszt'
								    else 'OTHER'
								    end
						   );
						   
  declare @maxdate           datetime = '4700-12-31'
						   

  /* --- Indul a mandula --- */
  
  /*
     @xml_prolog felrakása ???
	     <?xml version="1.0" encoding="UTF-8"?>
	 "
	 The XML data type (SQL Server) does not preserve processing information. 
	 So if you assign an XML document with processing information, it will be lost. 
	 If you want to return an XML document with processing information, you should return a well-formed XML string, instead of returning an XML data type value.
	 "
	 Amikor a GUI hívó átveszi az XML-t (karakter stringként !? - elég lesz a MAX hossz ?), akkor a VS már kiteszi a
	 "magában hordozott" prolog értéket az XML elejére.
  */
  declare @xml_prolog nvarchar(max);
  SET @xml_prolog = N'<?xml version="1.0" encoding="UTF-8"?>';
  
  declare @metsHdr xml;
  SET @metsHdr =      			  
			 ( select 
				  convert( nvarchar(19), @Atad_Date, 126 ) as [@CREATEDATE]             --- 'T' kell bele, de millisecond NEM --- "Csomag létrehozásának időpontja másodpercpontosan" "Fontos a dátum formátumát követni!"
	             ,@Atadas_Statusz_HU                       as [@RECORDSTATUS]           --- konstans      --- "ez mindig Új csomag!"
	             /* agent */				 
	             ,'ARCHIVIST'             as [agent/@ROLE]
	             ,'ORGANIZATION'          as [agent/@TYPE]
	             ,@Iratkepzo_Neve         as [agent/name]                               --- "Az intézmény akitől az irat származik (itt keletkezett)."
	             ,@Iratkepzo_Azonositoja  as [agent/note]				                --- "Rövid intézmény azonosító"
                 ,Null                                              --- elhatároló
	             ,'CREATOR'               as [agent/@ROLE]
	             ,'ORGANIZATION'          as [agent/@TYPE]
	             ,@Iratkepzo_Neve         as [agent/name]                               --- "Az átadási csomagot készítő szervezet neve (vagy az iratot átadó szervezet neve)" --- alapesetben megegyezik <ARCHIVIST><ORGANIZATION>-nel
				 ,@Iratkepzo_Azonositoja  as [agent/note]				                --- " Az átadási csomagot készítő szervezet egyedi azonosítója (alapértelmezetten azonos az iratképzővel)"				 				 
                 ,Null                                              --- elhatároló
	             /* altRecordID */                                                      --- <agent> és <altRecordID> sorrendje mindegy; DE a TAG nevekre Case sensitive az XML */
	             ,'DELIVERYTYPE'          as [altRecordID/@TYPE]
	             ,@Atadas_Tipus           as [altRecordID]
	             ,Null                                              --- elhatároló
                 ,'DELIVERYSPECIFICATION' as [altRecordID/@TYPE]
                 ,@Atadas_dokumentum      as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'SUBMISSIONAGREEMENT'   as [altRecordID/@TYPE]                        --- "z iratátadó és a levéltár között kötött iratátadási megállapodás iktatószáma és dátuma. A két adatot pontosvesszővel kell elválasztani."
	             ,@Atadas_Iktato          as [altRecordID]
                 ,Null                                              --- elhatároló
	             ,'APPRAISAL'             as [altRecordID/@TYPE]                        --- "Selejtezhetőség meghatározása: ... pl. TESZT típusú átvétel során lehet szükség. (Selejtezhető, Nem selejtezhető)"  
	             ,( select case when @Atadas_Statusz = 'TEST' then 'Selejtezhető'
                                else 'Nem selejtezhető'
                           end
                  )						  as [altRecordID]
                 ,Null                                              --- elhatároló
            from [dbo].EREC_IraJegyzekek ij
           where 1 = 1
             and ij.ID = @IraJegyzek_ID

            FOR XML PATH('metsHdr') /* ,type */
		     );

  /* ---
     Az MNL OL mintafile alapján csak egyetlen leíró szekció marad, amely egy átadott fizikai dokumentum csomagot nevesít. 
	 Mivel az Irat AZONOSITO egyben az Ugyirat AZONOSITO-t is tartalmazza, valamint egy Irathoz több fizikai dokumentum is tartozhat,
	 így az Irat alapján készül el ez a metaadat szekció. 
  --- */
  declare @dmdSec_I xml;
  SET @dmdSec_I =
              /* dmdSec */
			  ( select 
			       --- "Az itt megadott UUID azonosító a structMap-ban is szerepelni fog a mappára vonatkozóan"
  	               'uuid-' + convert( nvarchar(40), i.ID )          as [@ID]                    --- xsd:ID típusú    --- "Egyedi uuid azonosító https://www.uuidgenerator.net/"
				  ,convert( nvarchar(19), @Atad_Date, 126 )      as [@CREATED]               --- <metsHdr>/<CREATEDATE> 
				  ,'1'                                           as [@GROUPID]               --- változáskövetéshez lenne használható
				  ,'current'                                     as [@STATUS]                --- konstans 
                  
				  /* mdWrap */
			      ,( select 
  	                   'uuid-' + convert( nvarchar(40), NEWID() ) as [@ID]                    --- required; attribute;  An xsd:ID value must be an NCName.  --- " Egyedi uuid azonosító ... (ez máshol nem fog szerepelni)"
                      ,@Metaadat_Sema                            as [@MDTYPE]
					  ,'MDTYPEVERSION'                           as [@MDTYPEVERSION]         --- "Az SDB EAD2002-es sémával dolgozik" 
					  ,''                                        as [@LABEL] 	             --- megeszi az üres stringet 				  

				      /* xmlData */
                      ,( select Null
					  
		                /* EAD - Irat */
                        ,( select Null
						   --- amennyiben szükséges az EAD namespaces, akkor az alszekciókat muszáj leszek kitenni függvénybe !!!
						   
				           /* eadheader */
                           ,( select 						
  	                             'HU'                            as [eadid/@countrycode]
							    ,@Leveltar_Azonositoja           as [eadid/@mainagencycode]           --- "Ez az érték az illetékes levéltár azonosítója, ami az archiválást végzi"  ( 'HU-MNLOL' )
							    ,i.Azonosito                     as [eadid]                           --- ??? "Metaadat leíró mező: egyedi azonosító, ami csak erre az anyagra vonatkozik"
							    ,Null                            --- elhatároló 
				                ,i.Targy + ';' + i.Azonosito     as [filedesc/titlestmt/titleproper]  --- ??? "A metaadat leírás szöveges megnevezése. (alapesetben azonos az irategyüttes megnevezésével). "
						  
 						      FOR XML PATH('eadheader'),type
						    ) 

						   /* archdesc */
						   ,( select
						         'recordgrp'               as [@level]
								
						        /* did */
						        ,( select
								     /* --- MNL OL szerint sem egyértelmű nekem, hogy mi kell ide --- */
						              i.Azonosito           as [unitid]                                            --- "Irategyüttes azonosító"
				                     ,i.Targy               as [unittitle]                                         --- "Irategyüttes megnevezés"  --- "Az irategyüttes egyedi megnevezése"
									 /* ---
						              it.IrattariTetelszam  as [unitid]                                            --- "Az irattári tétel azonosítója"
				                     ,it.Nev                as [unittitle]                                         --- "Az irattári tétel megnevezése"
									 --- */
						   		     ,'originated_date'     as [unitdate/@label]                                   --- "Az eredeti irat keletkezése"
									 ,convert( date, i.IktatasDatuma )                 as [unitdate/@normal]       --- "a labelre és a dátum formátumára figyelni kell! Kötőjellel elválasztott év-hó-nap"
						             ,convert( date, i.IktatasDatuma )                 as [unitdate]               
								     ,Null                     --- elhatároló
						   		     ,'closed_date'         as [unitdate/@label]                                   --- "Az eredeti irat keletkezésének vége"
									 ,convert( date, isNull(i.IntezesIdopontja,@maxdate) ) as [unitdate/@normal]   --- "Kettő tagből kimaradt a záró dátum "									 
						             ,convert( date, isNull(i.IntezesIdopontja,@maxdate) ) as [unitdate]               
								     ,Null                     --- elhatároló
									 --- "Egy adatbázis esetén a két dátum szerintem lehet ugyanaz."
						   		     ,'coverage_from'       as [unitdate/@label]                                   --- "A digitalizálás kezdete"
									 ,convert( date, i.IktatasDatuma )                 as [unitdate/@normal]									 
						             ,convert( date, i.IktatasDatuma )                 as [unitdate]        
								     ,Null                     --- elhatároló									 
						   		     ,'coverage_to'         as [unitdate/@label]                                   --- "A digitalizálás vége"
									 ,convert( date, isNull(i.IntezesIdopontja,@maxdate) ) as [unitdate/@normal]   --- "Kettő tagből kimaradt a záró dátum "
						             ,convert( date, isNull(i.IntezesIdopontja,@maxdate) ) as [unitdate]
								     ,Null                     --- elhatároló									 
									 
                                   FOR XML PATH('did'),type
								 )
						        ,( select Null
								     /* p */
								     ,( select
                                           /*
										   --- MNL OL mintafile alapján nem tudom eldönteni, hogy kell-e ?
									       it.IrattariTetelszam                              as [ref]              --- XSD szerint Tag és nem attribute
								          ,Null                                              --- elhatároló										  
										  */
										   ( select Null FOR XML PATH('ref'), type )
						   		          ,'classification_date'                             as [date/@TYPE]
						                  ,convert( nvarchar(19), i.IrattarbaVetelDat, 126 ) as [date]

                                        FOR XML PATH('p'),type
								      )
									 
                                   FOR XML PATH('fileplan'),type
								 )
						
                              FOR XML PATH('archdesc'),type
			                )

						   FOR XML PATH('ead'),type
						 ) 

						 FOR XML PATH('xmlData'),type
			           )

                     FOR XML PATH('mdWrap'),type
			       )
                  from [dbo].EREC_IraIratok i
                 inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                     /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                 inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                 inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
                 inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                    and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
				 order by ik.Ev, u.Foszam, i.Alszam                             --- azért, hogy az Ügyiratnak ( és fileSec-nek ) megfelelő sorrendben legyen   --- i.Azonosito  

                FOR XML PATH('dmdSec') /* ,type */
			  );  
  
  /* --- Utólagos csúnya patkolás EAD NAMESPACE-k helyes elhelyezése érdekében --- */
  declare @s_xml nvarchar(MAX)
  Set @s_xml = convert(nvarchar(MAX), @dmdSec_I)
  Set @s_xml = replace( @s_xml, '<ead>', '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">')
  Set @s_xml = replace( @s_xml, '</ead>', '</ead:ead>' )                               --- </ead:ead xmlns:ead=''urn:isbn:1-931666-22-9'' xmlns=''urn:isbn:1-931666-22-9''>')
  Set @dmdSec_I = convert( xml, @s_xml )
  --- print '--- . ---'
  --- print substring(convert(nvarchar(MAX), @dmdSec_I),1,2000)
  --- print substring(convert(nvarchar(MAX), @dmdSec_I),2001,4000)
  --- print substring(convert(nvarchar(MAX), @dmdSec_I),4001,6000)
  --- print substring(convert(nvarchar(MAX), @dmdSec_I),6001,8000)
  --- print '--- . ---'
  
  declare @fileSec xml;
  SET @fileSec =
  			  ( select Null
                  
				  /* fileGrp */
			      ,( select 
  	                        'original'                                        as [@ID]             --- "A fileGrp tageknek így kéne statikusan kinéznie: <fileGrp ID="original" USE="original">"
						   ,'original'                                        as [@USE] 

				           /* file */
			               ,( select 
					                 'uuid-' + convert( nvarchar(40), cs.ID )  as [@ID]             --- xsd:ID típusú   --- "egyedi uuid azonosító. Ez az azonosító A structMap-ban is szerepelni fog a fájlra utalva"
								    --- MNL OL szerint NEM ez kell (?)  --- ,d.LetrehozasIdo as [@CREATED]
									,convert( nvarchar(19), @Atad_Date, 126 ) as [@CREATED]     --- "Csomag létrehozásának időpontja másodpercpontosan" "Fontos a dátum formátumát követni!"
									--- ,d.Formatum                               as [@MIMETYPE]       --- d.Formatum = IANA MIME type ???
									,''                                       as [@MIMETYPE]       --- "a mimeype-ot üresen kell hagyni: MIMETYPE='' " 
									,'1'                                      as [@SEQ]            --- MNL OL szerint akkor is, ha egy Irathoz több file tartozik                       
									,d.Meret                                  as [@SIZE]           --- "a fájl mérete bájtban"
									,@CheckSum_Type                           as [@CHECKSUMTYPE]   --- "A befogadás csak MD5-el készült checksum esetén sikeres" ??? ezt hogyan tudjuk megoldani ??? 
									,d.KivonatHash                            as [@CHECKSUM]       --- "A fájl érintetlenségét igazoló „checksum” értéke. (MD5 CHECKSUM TYPE)"
															   
  				                    /* FLocat */                      
									,[dbo].fn_Get_MetadataXML_Flocat(d.ID)                 --- kitettem egy eljárásba, hogy csak ide tegye bele az xlink hivatkozást 
                                from [dbo].KRT_Dokumentumok d 
                               inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID and cs.IraIrat_ID = i.ID
							                                        and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum 
                               /* --- inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID and i.Ugyirat_ID = u.ID --- */
							   where 1 = 1
							     and d.Org = @Org_Id
							   order by i.Alszam, cs.ID                                  --- elegendő az Alszam, mivel fileGrp -on belűl van
                              FOR XML PATH('file'),type						  
			                )

                       from [dbo].EREC_IraIratok i
                      inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID 
				                                     /* and u.Foszam between 100 AND 150         --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                      inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID		 --- csak a rendezés miatt kell											 
                      inner join EREC_IraIrattariTetelek it on it.ID = u.IraIrattariTetel_ID
                      inner join EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
   		                                                                         and ijt.Obj_type = 'EREC_UgyUgyiratok'   --- az Obj_type -ra tett feltétel nem biztos, hogy kell
					  where 1 = 1
					    and EXISTS ( select 1 from [dbo].KRT_Dokumentumok d      /* csak azokra képezzünk fileGrp-ot, amelyhez van dokumentum - DE mindhez kellene legyen !? */
                                             inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID
											                                       and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum
                                             inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID and i.Ugyirat_ID = u.ID and d.Org = @Org_Id)
						/* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
				      order by ik.Ev, u.Foszam, i.Alszam                             --- azért, hogy az Ügyiratnak ( és fileSec-nek ) megfelelő sorrendben legyen   --- i.Azonosito  

					  FOR XML PATH('fileGrp'),type
			       )
                /* --- mivel nincs plusz feltétel, így elegendő feljebb a direkt @IraJegyzek_ID szűrés 
                  from [dbo].EREC_IraJegyzekek ij
                 where 1 = 1
                   and ij.ID = @IraJegyzek_ID
                 --- */
                FOR XML PATH('fileSec') /* ,type */
			  );	

  declare @structMap xml;
  SET @structMap =
              /* structMap */
              ( select
			       'aggregation'                                         as [@TYPE]                                        --- MNL OL minta file alapján
                  
				  /* div */
			      ,( select
						   'recordgrp'                                   as [@TYPE]                --- MNL OL minta file szerint
						   ,'uuid-' + convert( nvarchar(40), i.ID )      as [@DMDID]               --- dmdSec ID, az Iratok blokkjában; xsd:ID típusú	  --- "DMDID="" ugyanaz mint a 15. sorban dmdSec-ben megadott uuid. Így kapcsolódik a kettő"
						   ,''                                           as [@ADMID]  
                           
				           /* fptr */
			               ,( select
					                 'uuid-' + convert( nvarchar(40), cs.ID ) as [@FILEID]    --- cs.ID -ra tudok hivatkozni structMap -ban is !!!  --- required; attribute;  --- "FILEID="" ugyanaz mint a 91. sorban megadott ID="" elem. Így kapcsolódik a kettő"
                                from [dbo].EREC_Csatolmanyok cs 
							   where 1 = 1
							     and cs.IraIrat_ID = i.ID
							     and getdate() between cs.ErvKezd and cs.ErvVege           /* ez jelzi, ha már törölt egy dokumentum */
							   order by cs.ID
							   
                              FOR XML PATH('fptr'),type
			                )
						/* amennyiben itt volt a 'from [dbo].EREC_IraIratok i ...', akkor egyetlen <div> alá hozta az <fptr> -eket, de ezt az eredményt az XSD összevetés ( XMLSpy ) nem fogadta el !? */
                     FOR XML PATH('div'),type							
			       )
                  from [dbo].EREC_IraIratok i
			     inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID
			     inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID                                       --- ez tulajdonképpen csak a rendezés miatt kell
                 inner join [dbo].EREC_IraJegyzekTetelek ijt on ijt.Obj_ID = u.ID and ijt.Jegyzek_ID = @IraJegyzek_ID     /* ij.ID */
				                                                                  and ijt.Obj_type = 'EREC_UgyUgyiratok'                --- az Obj_type -ra tett feltétel nem biztos, hogy kell
			     where 1 = 1
				   and EXISTS ( select 1 from [dbo].KRT_Dokumentumok d      /* csak azokra képezzünk fileGrp-ot, amelyhez van dokumentum - DE mindhez kellene legyen !? */
                                 inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID
											                          and getdate() between cs.ErvKezd and cs.ErvVege  --- ez jelzi, ha már törölt egy dokumentum								 
                                 inner join [dbo].EREC_IraIratok ib on ib.ID = cs.IraIrat_ID and ib.Ugyirat_ID = u.ID and d.Org = @Org_Id
				              )
				   /* and u.Foszam between 100 AND 150   --- !!! ez csak a Teszteléshez kell !!!, hogy NE legyen olyan sok az adat */
                 order by ik.Ev, u.Foszam, i.Alszam

                FOR XML PATH('structMap') /* ,type */
			  );
			  
  /*
  <xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://www.loc.gov/METS/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.loc.gov/METS/">
  <xsd:import schemaLocation="http://www.loc.gov/standards/xlink/xlink.xsd" namespace="http://www.w3.org/1999/xlink"/>                       '
  */

  /* --- !!! @dmdSEC_* hivatkozásokból még mindíg hiányzik az '<ead:ead xmlns:ead="urn:isbn:1-931666-22-9" xmlns="urn:isbn:1-931666-22-9">' namespace hivatkozás !!! - kell az vkinek ? --- */
  
  ;    /* --- szet kellett szednem, mert amikor egyben volt, akkor benyomta minden csomópontra a NAMESPACE literált; --- declare namespace -szel vajon lehetne egyszerűbben operálni ? --- */ 
  --- with XMLNAMESPACES( 'http://www.loc.gov/METS/' as [mets], 'http://www.w3.org/1999/xlink' as xlink, 'http://www.w3.org/2001/XMLSchema-instance' as xsi /* , DEFAULT 'http://www.loc.gov/METS/' */ )
with XMLNAMESPACES( DEFAULT 'http://www.loc.gov/METS/', 'http://www.w3.org/1999/xlink' as xlink, 'http://www.w3.org/2001/XMLSchema-instance' as xsi  )
  SELECT @MetadataXML = 
     ( select /* mets */
              'mets'                         as [@xsi:type]             	 --- MNL OL mintában benne volt
	         ,'SIP'                          as [@TYPE]			  
             ,@SIP_azonosito                 as [@OBJID]                     --- "A csomag azonosítója, amely egyedileg képes azonosítani magát a csomagot."
             ,ij.Nev                         as [@LABEL]                     --- "Rövid szöveg, ami leírja a csomag tartalmát."
/* --- */
             /* metsHdr */
			 ,' METADATA.xml fejléc elemek ' as [comment()]
             ,@metsHdr
/* ---  MNL OL által adott mintafájl alapján ezek a szekciók NEM kellenek 
             /* dmdSec - Irattári terv */                                
			 ,' Irattári terv ' as [comment()]
			 ,@dmdSec_IT

             /* dmdSec - Irattári tételek */
			 ,' Irattári tételek ' as [comment()]
			 ,@dmdSec_ITT

             /* dmdSec - Iktatókönyvek */
			 ,' Iktatókönyvek ' as [comment()]
			 ,@dmdSec_IK

             /* dmdSec - Ügyiratok */
			 ,' Ügyiratok ' as [comment()]
			 ,@dmdSec_U
 --- */
 /* --- */
             /* dmdSec - Iratok */
			 ,' Iratok ' as [comment()]
			 ,@dmdSec_I
/* --- */
             /* fileSec */			 
			 ,' fileSec ' as [comment()]
			 ,@fileSec
/* --- */				  
             /* structMap */			 
			 ,' structMap ' as [comment()]                                                --- az XSD ellenőrzés alapján kötelező !?
			 ,@structMap
/* --- */			  
         from [dbo].EREC_IraJegyzekek ij
        where 1 = 1
          and ij.ID = @IraJegyzek_ID
      FOR XML PATH('mets'), type
      );
	  
  /* --- Utólagos csúnya patkolás a NAMESPACE-k helyes elhelyezése érdekében --- */
  Set @MetadataXML = convert( xml, replace( convert(nvarchar(MAX), @MetadataXML), 'xmlns=""', '') )         --- DEFAULT kiadásából származó xmlns="" -ek eltüntetése
  --- print '--- . ---'
  --- print substring(convert(nvarchar(MAX), @MetadataXML),1,2000)
  --- print substring(convert(nvarchar(MAX), @MetadataXML),2001,4000)
  --- print substring(convert(nvarchar(MAX), @MetadataXML),4001,6000)
  --- print substring(convert(nvarchar(MAX), @MetadataXML),6001,8000)
  --- print '--- . ---'

  /* --- Hibakezelés --- */
  BEGIN TRY
   if CHARINDEX ('</mets>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
   /* ---
   if CHARINDEX ('</mets:mets>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
   --- */
     /* or LEN( convert(nvarchar(max), @MetadataXML) ) >2000000 */
     SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );  
  END TRY
  BEGIN CATCH
    SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );
  END CATCH

END TRY
BEGIN CATCH
    DECLARE @errorSeverity INT,
        @errorState INT
    DECLARE @errorCode NVARCHAR(1000)    
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    IF ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE()
    ELSE 
        SET @errorCode = ERROR_MESSAGE()

    IF @errorState = 0 
        SET @errorState = 1

    RAISERROR ( @errorCode, @errorSeverity, @errorState )

END CATCH
END

