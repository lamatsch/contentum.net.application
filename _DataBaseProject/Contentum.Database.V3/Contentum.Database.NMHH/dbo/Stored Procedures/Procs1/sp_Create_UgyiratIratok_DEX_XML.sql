﻿

CREATE PROCEDURE [dbo].[sp_Create_UgyiratIratok_DEX_XML]
       @Ugyirat_ID              uniqueidentifier
      ,@ExecutorUser_ID         uniqueidentifier	  
      ,@FelhasznaloSzervezet_Id uniqueidentifier               -- a bejelentkezéskor kiválasztott szervezet
	  ,@MetadataXML xml OUTPUT
AS 
BEGIN
/* ---
-- 3/2018. (II. 21.) BM rendelet 1.sz. melléklet alapján kidolgozva
 --- */

BEGIN TRY

  set nocount on

  /* alapellenőrzések */
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUser_Id)
  if (@Org_ID is null)
  begin
    RAISERROR('[50202]',16,1)
  end;
  /* ez nem biztos, hogy kell !? elegendő hibakezelés, ha az XML = Null értékkel megy vissza ?
  if NOT Exists ( select 1 from [dbo].EREC_UgyUgyiratok u where 1 = 1 and u.ID = @Ugyirat_ID )
  begin
    RAISERROR('[51501]',16,1)                                                 --- a hibakódok vmi resource file-ban vannak
  end;
  */
 
  declare @Org_KOD nvarchar(100)
  Set @Org_KOD = ( select KOD from dbo.KRT_Orgok where id = @Org_ID )  
  --- declare @Org_NEV nvarchar(400)
  ---Set @Org_NEV = dbo.fn_GetKRT_OrgokAzonosito(@Org_ID)
  
  declare @ExecutorUser_NEV nvarchar(400)
  Set @ExecutorUser_NEV = dbo.fn_GetFelhasznaloNev(@ExecutorUser_Id)
  declare @ExecutorSzervezet_NEV nvarchar(400)
  Set @ExecutorSzervezet_NEV = dbo.fn_GetKRT_CsoportokAzonosito(@FelhasznaloSzervezet_Id) 
  declare @AktDateTime datetime = getdate()
  
  /* --- Indul a mandula --- */
  
  /*
     @xml_prolog felrakása ???
	     <?xml version="1.0" encoding="UTF-8"?>
	 "
	 The XML data type (SQL Server) does not preserve processing information. 
	 So if you assign an XML document with processing information, it will be lost. 
	 If you want to return an XML document with processing information, you should return a well-formed XML string, instead of returning an XML data type value.
	 "
	 Amikor a GUI hívó átveszi az XML-t (karakter stringként !? - elég lesz a MAX hossz ?), akkor a VS már kiteszi a
	 "magában hordozott" prolog értéket az XML elejére !
  */
  declare @xml_prolog nvarchar(max);
  SET @xml_prolog = N'<?xml version="1.0" encoding="UTF-8"?>';

  /* --- */
  /* DEX_Header */
  declare @DEX_Hdr xml;
 		  
/* --- */			     
  /* Ügyirat */
  declare @dmdSec_U xml;
  SET @dmdSec_U =  
			  ( select 
				  ' IV. IKTATÁS METAADATAI ' as [comment()]
				  ,Isnull( u.Azonosito,'') as [Iktatoszam]
				  ,case when u.Csoport_ID_Felelos is Null then ''
				        else dbo.fn_GetKRT_CsoportokAzonosito(u.Csoport_ID_Felelos) + ';' +
                            ( select TOP 1 cs.Nev from KRT_CsoportTagok cst
                               inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID
                               where cst.Csoport_ID_Jogalany = u.Csoport_ID_Felelos
							)
                   end						   as [UgyintezoSzemelySzervezet]
				  ,Isnull( u.Targy,'') as [UgyiratTargya]
				  --- NEM akartam az Ügyirat teljes METADATA körét ide kiiratni, csak a legfontosabbakat
				  ,' XV. ÜGYIRAT META adatok ' as [comment()]
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYIRAT_JELLEG', u.Jelleg, @Org_ID),'')   as [UgyiratTipusa]
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYIRAT_ALLAPOT', u.Allapot, @Org_ID),'') as [UgyiratAllapota]
				  ,Isnull( u.UgyTipus,'')      as [UgyTipusa]
                  ,IsNull( u.Ugyazonosito,u.Azonosito)  as [UgykezeloRendszerAzonosito]
				  --- 'korlátozásokat' már itt sem írom ki

			      from [dbo].EREC_UgyUgyiratok u 
				 where 1 = 1
				   and u.ID = @Ugyirat_ID
					   
                FOR XML PATH('Ugyirat'),type			  
			  );  
			  
/* --- */
  /* Iratok */
  declare @dmdSec_I xml;

  SET @dmdSec_I =
			  ( select 
			       --- ELVBEN csak Bejövő Iratokra ad adatot  
  	               ' I. ÁTVÉTEL METAADATOK '      as [comment()]                             --- ??? vajon NEM kellene itt is mindíg kiírni minden XML tag-et ( IsNull(,'') )
				  ,format( bej.BeerkezesIdeje, 'yyyy.MM.dd HH.mm.') as [BeerkezesIdopontja]
				  ,dbo.fn_KodtarErtekNeve('ELSODLEGES_ADATHORDOZO', bej.ElsodlegesAdathordozoTipusa, @Org_ID) as [KuldemenyTipusa]
				  ,dbo.fn_KodtarErtekNeve('KULDEMENY_KULDES_MODJA', bej.KuldesMod, @Org_ID)                   as [BeerkezesModja]
				  ,bej.Ragszam        as [KuldemenyAzonosito]
	              ,case when part.Tipus = '20' /* azaz, 'Személy' */ then bej.NevSTR_Bekuldo                        
		                else ''
		           end                as [KuldoSzemelyNeve]
		          ,case when part.Tipus <> '20' /* azaz, 'Szervezet', 'Bizottság', ... */ then [dbo].fn_GetKRT_CsoportokAzonosito(bej.Partner_ID_Bekuldo)
                        else ''
                   end                as [KuldoSzervezetNeve]
				  ,c.IRSZ             as [KuldoCimeIrsz]
				  ,c.OrszagNev        as [KuldoCimeOrszag]
				  ,c.TelepulesNev     as [KuldoCimeHelyseg]
				  ,c.KozteruletNev    as [KuldoCimeKozteruletNeve]
				  ,c.KozteruletTipusNev  as [KuldoCimeKozteruletJellege]
				  ,c.Hazszam          as [KuldoCimeHazszam]
				  /* [KuldoCimeEpulet]                           -- része KRT:Cimek.Hazszam adatmezőnek, nincs külön tárolva */
				  ,c.Lepcsohaz        as [KuldoCimeLepcsohaz]
				  ,c.Szint            as [KuldoCimeemelet]
				  ,c.Ajto             as [KuldoCimeAjtoszam]
				  ,case when c.Tipus = '02' then c.Tobbi
				        else Null
				   end                as [KuldoCimePostafiokSzama]

		          ,case when bej.Cim_ID is Null then substring(bej.CimSTR_Bekuldo,1,20)      --- egyben tartalmazza és itt írjuk ki (Peti)
                        else case when c.Tipus = '01' then substring(c.Tobbi,1,20)              
		                          else ''
		                     end
                    end               as [KuldoCimeEgyeb]
				  ,case when c.Tipus = '03' then dbo.fn_KodtarErtekNeve('CIM_TIPUS', c.Tipus, @Org_ID)
				        else Null
				   end                as [KuldoElektrElerhTipusa]
				  ,case when c.Tipus = '03' then c.Tobbi 
				        else Null
				   end                as [KuldoElektrElerhetosege]
				  /* ,case when ( select d.AlairasFelulvizsgalatEredmenye from dbo.KRT_Dokumentumok d
				                   inner join dbo.EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID and cs.KuldKuldemeny_ID = bej.ID ) is Null then 'Nem érvényes'
				        else 'Érvényes'
				   end           as [AlairasEllEredmenye]                 --- fejlesztés alatt #2947 */   --- itt NEM '0'/'1' -et kellene kiadnom 
				  ,dbo.fn_GetKRT_CsoportokAzonosito(bej.Csoport_ID_Cimzett) as [CimzettSzemelyNeve]
				  /* ,bej.??? as [CimzettCime]                   -- nincs tárolva  */
				   
				   --- ELVBEN csak Bejövő Iratokra ad adatot  
  	              ,' II. BONTÁS METAADATOK '      as [comment()]                             --- ??? vajon NEM kellene itt is mindíg kiírni minden XML tag-et ( IsNull(,'') )
				  ,format( bej.FelbontasDatuma, 'yyyy.MM.dd HH.mm.') as [BontasIdopontja]				  
				  ,dbo.fn_GetKRT_CsoportokAzonosito(bej.FelhasznaloCsoport_Id_Bonto) as [KuldemenyBontoja]
				  ,bej.BontasiMegjegyzes          as [BontasiMegjegyzes]
				  --- ??? külön Tag-be kellenének a Küldemény elkülöníthető elemei - de akkor a számossága ?
				  ,( select sum(m.Mennyiseg) from dbo.EREC_Mellekletek m where m.KuldKuldemeny_ID = bej.ID/*KuldID*/ ) as [KuldemenyElemeinekSzama]
				  ,( select dbo.fn_KodtarErtekNeve('ADATHORDOZO_TIPUSA', m.AdathordozoTipus, @Org_ID) + ';' as [data()]
		               from ( select distinct mb.AdathordozoTipus as AdathordozoTipus from dbo.EREC_Mellekletek mb
		                       where mb.KuldKuldemeny_ID = bej.ID/*KuldID*/ ) as m
                        FOR XML PATH ('')
	               )                              as [ElemekTipusa]
				  ,case when bej.ID/*KuldID*/ is Null then Null                             --- ekkor - egyelőre - elnyelem 
				        when IsNull(bej.SerultKuldemeny,'0') = '1' then 'Igen'
				        else 'Nem'
				   end                            as [SerultKuldemeny]
				  ,case when bej.ID/*KuldID*/ is Null then Null                             --- ekkor - egyelőre - elnyelem 
				        when IsNull(bej.IktatastNemIgenyel,'0') = '1' then 'Igen'
				        else 'Nem'
				   end                            as [IktatastNemIgenyel]
				  
				   --- ELVBEN csak Bejövő Iratokra ad adatot  
				  ,' III. ÉRKEZTETÉS METAADATOK ' as [comment()]                             --- ??? vajon NEM kellene itt is mindíg kiírni minden XML tag-et ( IsNull(,'') )
				  ,bej.Erkezteto_Szam             as [ErkeztetesiAzonosito]
				  ,case when bej.ID/*KuldID*/ is Null then Null                             --- ekkor - egyelőre - elnyelem 
				        when IsNull(bej.TevesCimzes,'0') = '1' then 'Igen'
				        else 'Nem'
				   end                            as [TevesCimzes]
				  ,bej.HivatkozasiSzam            as [HivatkozasiSzam]
				  ,dbo.fn_GetKRT_CsoportokAzonosito(bej.Letrehozo_Id) as [Erkezteto]
				  
				  ,' IV. IKTATÁS METAADATAI ' as [comment()]
				  ,Isnull( i.Azonosito,'')   as [Iktatoszam]
				  ,Isnull( format( i.IktatasDatuma, 'yyyy.MM.dd HH.mm.'),'') as [IktatasIdopontja]
				  ,IsNull( dbo.fn_GetKRT_CsoportokAzonosito(i.FelhasznaloCsoport_ID_Iktato),'')     as [Iktato]
                  ,IsNull( dbo.fn_KodtarErtekNeve('POSTAZAS_IRANYA', i.PostazasIranya, @Org_ID),'') as [IratIranya]
				  --- ??? külön Tag-be kellenének a Mellékletek - de akkor a számossága ?
				  ,isnull( ( select im.Megjegyzes + ';' as [data()]
		                       from dbo.EREC_IratMellekletek im
		                      where im.IraIrat_ID = i.ID
                                FOR XML PATH ('')
	                       ), '')            as [IratMellekletei]
				  ,( select count(1) from dbo.EREC_IratMellekletek im where im.IraIrat_ID = i.ID ) as [MellekletekSzama]         --- ??? különböző típusú mellékletek vannak, az nem baj
				  ,case when i.FelhasznaloCsoport_ID_Ugyintez is Null then ''
				        else dbo.fn_GetKRT_CsoportokAzonosito(i.FelhasznaloCsoport_ID_Ugyintez) + ';' +
                            ( select TOP 1 cs.Nev from KRT_CsoportTagok cst
                               inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID
                               where cst.Csoport_ID_Jogalany = i.FelhasznaloCsoport_ID_Ugyintez
							)
                   end						 as [UgyintezoSzemelySzervezet]				  
 				  ,Isnull( i.Targy,'')       as [IratTargya]
				  ,isnull( ( select ot.Ertek + ';' as [data()]
		                       from dbo.EREC_ObjektumTargyszavai ot
		                      where ot.Obj_ID = i.ID  
								and ot.Targyszo like 'Tárgy%'             --- (Peti) 							  
                                FOR XML PATH ('')
	                       ), '')            as [Targyszavak]
			      ,IsNull( dbo.fn_GetKRT_CsoportokAzonosito( ( select TOP 1 ih.HistoryVegrehajto_Id from dbo.EREC_IraIratokHistory ih 
				                                                where ih.ID = i.ID and ih.Allapot = '05' /* azaz 'Szignált' */ 
																order by ih.HistoryVegrehajtasIdo
															 ) ),'') as [Szignalo]				  
				  ,Isnull( format( ( select TOP 1 ih.HistoryVegrehajtasIdo from dbo.EREC_IraIratokHistory ih 
				                      where ih.ID = i.ID and ih.Allapot = '05' /* azaz 'Szignált' */ 
									  order by ih.HistoryVegrehajtasIdo
									), 'yyyy.MM.dd HH.mm.'),'')      as [SzignalasIdopontja]
				  ,IsNull( i.Note,'')        as [SzignaloUtasitasa]                                                             --- ???
				  ,IsNull( dbo.fn_KodtarErtekNeve('FELADAT_ALTIPUS', ( select TOP 1 hf.Altipus from dbo.EREC_HataridosFeladatok hf
				                                                        where hf.Obj_ID = i.ID
																		order by hf.LetrehozasIdo desc
																	 ), @Org_ID),'') as [KezelesiUtasitasok]                    --- ??? Itt igazából s Szignáláshoz kapcsolódó Kezelési utasítás kellene
				  ,IsNull( ( select TOP 1 hf.Leiras from dbo.EREC_HataridosFeladatok hf                             
				              where hf.Obj_ID = i.ID
							  order by hf.LetrehozasIdo desc
						   ),'')                                                     as [KezelesiFeljegyzesek]                  --- ??? Itt igazából s Szignáláshoz kapcsolódó Kezelési utasítás kellene
				  ,Isnull( format( i.Hatarido, 'yyyy.MM.dd HH.mm.'),'')              as [IratUgyintezesiHatarideje]
				  ,IsNull( dbo.fn_KodtarErtekNeve('UGYINTEZES_ALAPJA', i.UgyintezesAlapja, @Org_ID),'') as [UgyintezesModja]
				  ,Isnull( format( i.IntezesIdopontja, 'yyyy.MM.dd HH.mm.'),'')      as [IratElintezesiIdopontja]
				  
				  ,' V. KIADMÁNYOZÁS METAADATAI ' as [comment()]
				  ,case when i.FelhasznaloCsoport_ID_Kiadmany is Null then ''
				        else dbo.fn_GetKRT_CsoportokAzonosito(i.FelhasznaloCsoport_ID_Kiadmany) + ';' +
                            ( select TOP 1 cs.Nev from KRT_CsoportTagok cst
                               inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID
                               where cst.Csoport_ID_Jogalany = i.FelhasznaloCsoport_ID_Kiadmany
                            )
                   end                            as [KiadmanyozoNeveSzervezete]                            --- ??? Igazából ezt is IratAláírók -ból kellene kiszedni
				  ,Isnull( format( ( select TOP 1 al.AlairasDatuma from dbo.EREC_Iratalairok al
				                      where al.Obj_ID = i.ID and al.AlairoSzerep = '1' /* azaz 'Kiadmányozó' */ 
									  order by al.AlairasDatuma
								   ), 'yyyy.MM.dd HH.mm.'),'')             as [KiadmanyozasIdopontja]   
			      ,( select 
			          ' Egyéb aláíró neve, szervezete, aláírás időpontja ' as [comment()]                             --- ??? vajon NEM kellene itt is mindíg kiírni minden XML tag-et ( IsNull(,'') )							   
				      ,dbo.fn_GetKRT_CsoportokAzonosito(al.FelhasznaloCsoport_ID_Alairo) + ';' +
                       ( select TOP 1 cs.Nev from KRT_CsoportTagok cst
                          inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID
                          where cst.Csoport_ID_Jogalany = al.FelhasznaloCsoport_ID_Alairo
                       )						                      as [EgyebalairoNeveSzervezet]
				      ,format( al.AlairasDatuma, 'yyyy.MM.dd HH.mm.') as [EgyebalairasDatuma]
					  
			          from [dbo].EREC_IratAlairok al
				     where 1 = 1
				       and al.Obj_ID = i.ID
					   and al.AlairoSzerep <> '1'                  --- azaz NEM 'Kiadmányozó'
					   
                     FOR XML PATH('EgyebAlairo'),type
				   )
				  
				  ,' VI. EXPEDIÁLÁS METAADATAI ' as [comment()]
				  ,' XI. KÉZBESÍTÉSI NYUGTA ADATOK ' as [comment()]				  
				  
			      from [dbo].EREC_IraIratok i
				 --- inner join dbo.EREC_PldIratPeldanyok p on p.IraIrat_ID = i.ID and p.Sorszam = 1             --- az 1. IratPéldányt kapcsoljuk hozzá (Laura)
				 --- left join dbo.EREC_KuldKuldemenyek k on k.ID = i.KuldKuldemenyek_ID
				 /*
				 left join ( select ib.ID as IratID, kb.ID as KuldID, kb.* from dbo.EREC_IraIratok ib            --- szűkíteni lehetne a Kuldemeny valóban szükséges oszlopaira 
				               left join dbo.EREC_KuldKuldemenyek kb on kb.ID = ib.KuldKuldemenyek_ID
							  where ib.PostazasIranya = '1' /* azaz Bejövő */ ) as bej on bej.IratID = i.ID
				  */
				  left join [dbo].[EREC_KuldKuldemenyek] bej on bej.ID = i.KuldKuldemenyek_ID and bej.PostazasIranya = '1' /* azaz Bejövő */ 	  
                  left join [dbo].[KRT_Partnerek] part on part.ID = bej.Partner_ID_Bekuldo				  
				  left join [dbo].[KRT_Cimek] c on c.ID = bej.Cim_ID
				 where 1 = 1
				   and i.Ugyirat_ID = @Ugyirat_ID
					   
                FOR XML PATH('Irat'),type
			  );

  --- @xml_prolog-ot 'magában hordozza'  a struktúra, de láthatólag csak a VS teszi ki 
  ;    /* --- szet kellett szednem, mert amikor egyben volt, akkor benyomta minden csomópontra a NAMESPACE literált; --- declare namespace -szel vajon lehetne egyszerűbben operálni ? --- */ 
  with XMLNAMESPACES( /*DEFAULT '???/',*/ 'http://www.w3.org/2001/XMLSchema' as xsd, 'http://www.w3.org/2001/XMLSchema-instance' as xsi  )  
  SELECT @MetadataXML = 
      ( select    @Org_Kod                as [Adattulajdonos]
				 ,@ExecutorUser_NEV       as [Keszitette] 
				 ,@ExecutorSzervezet_NEV  as [Szervezet]
				 ,format( @AktDateTime,  'yyyy.MM.dd HH.mm.ss') as [AdatexportIdopontja]
/* --- */
             /* DEX_Header */
			 ,' Data Exchange Schema Header data ' as [comment()]
             ,@DEX_Hdr
/* --- */			     
             /* dmdSec - Ügyirat */
			 ,' Ügyirat ' as [comment()]
			 ,@dmdSec_U
/* --- */
             /* dmdSec - Iratok */
			 ,' Iratok ' as [comment()]
			 ,@dmdSec_I
/* --- */			  
/*      --- ez igazából nem is kell ---
		 from [dbo].EREC_UgyUgyiratok u 
		where 1 = 1
		  and u.ID = @Ugyirat_ID		 
*/		  
      FOR XML PATH('DEX'),type
      );

  /* --- Utólagos csúnya patkolás a NAMESPACE-k helyes elhelyezése érdekében --- */
---  Set @MetadataXML = convert( xml, replace( convert(nvarchar(MAX), @MetadataXML), 'xmlns=""', '') )         --- DEFAULT kiadásából származó xmlns="" -ek eltüntetése

  /* --- Hibakezelés --- */
  BEGIN TRY
   if CHARINDEX ('</DEX>', convert(nvarchar(MAX), @MetadataXML) ) = 0 
     /* or LEN( convert(nvarchar(max), @MetadataXML) ) >2000000 */
     SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );  
  END TRY
  BEGIN CATCH
    SET @MetadataXML = convert( xml, '<ERROR><error_code>001</error_code><error_desc>Túl sok adat. Az XML mérete nagyobb, mint 2 GB</error_desc></ERROR>' );
  END CATCH

END TRY
BEGIN CATCH
    DECLARE @errorSeverity INT,
        @errorState INT
    DECLARE @errorCode NVARCHAR(1000)    
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    IF ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE()
    ELSE 
        SET @errorCode = ERROR_MESSAGE()

    IF @errorState = 0 
        SET @errorState = 1

    RAISERROR ( @errorCode, @errorSeverity, @errorState )

END CATCH
END

