﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_Feladatok_GetSummary')
--            and   type = 'P')
--   drop procedure sp_Feladatok_GetSummary
--go

create procedure [dbo].[sp_Feladatok_GetSummary]
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier = null,
  @KolcsonzesreKezelhetoIrattarak NVARCHAR(MAX) = '',
  @KikeresreKezelhetoIrattarak NVARCHAR(MAX) = ''
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	declare @UgyiratAllapot_IrattarbaKuldott nvarchar(2)
	set @UgyiratAllapot_IrattarbaKuldott = '11'
 
-- Átadandók (Átadásra kijelöltek):
	select Obj_type, Count(*) as Darab, '1' as Allapot
	from EREC_IraKezbesitesiTetelek
	where Allapot = '1' and Felhasznalo_Id_Atado_USER = @ExecutorUserId
		and getdate() between ErvKezd and ErvVege
	Group By Obj_type

-- Átveendők (Átadottak) személyre:
	select Obj_type, Count(*) as Darab, '2' as Allapot
	from EREC_IraKezbesitesiTetelek
	where Allapot = '2' and Csoport_Id_Cel = @ExecutorUserId
		and getdate() between ErvKezd and ErvVege
	Group By Obj_type

-- Átveendők (Átadottak) szervezetre:
-- EB(2008.11.04: ügyiratok esetén kiszűrjük az irattárba küldötteket, mert ezeket itt nem lehet átvenni, csak az irattárnál)
	select EREC_IraKezbesitesiTetelek.Obj_type, Count(*) as Darab, '2' as Allapot
	from EREC_IraKezbesitesiTetelek
	left join EREC_UgyUgyiratok
	on EREC_IraKezbesitesiTetelek.Obj_type = 'EREC_UgyUgyiratok'
		and EREC_IraKezbesitesiTetelek.Obj_Id = EREC_UgyUgyiratok.Id 
	where EREC_IraKezbesitesiTetelek.Allapot = '2'
		and EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = @FelhasznaloSzervezet_Id
		and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege
		and (Obj_type <> 'EREC_UgyUgyiratok' or EREC_UgyUgyiratok.Allapot <> @UgyiratAllapot_IrattarbaKuldott)
	Group By Obj_type

-- Saját ügyiratok:
-- (az irattáros állapotok közül néhány nem kell)
--(Iktatásra előkészített, Irattárban őrzött,
-- Jegyzékre helyezett, Lezárt jegyzékben lévő, Selejtezett,
-- Levéltárba adott, Irattárból elkért, Egedélyezett kikérőn lévő, Sztornózott)

	DECLARE @Sajat_Ugyiratok int
	SET @Sajat_Ugyiratok = (
		select Count(*)
		from EREC_UgyUgyiratok
		where FelhasznaloCsoport_Id_Orzo = @ExecutorUserId
			--AND Allapot IN ('03','04','06','07','09','11','13','50','52','60','90','99')
			AND EREC_UgyUgyiratok.Allapot NOT IN ('0','10','30','31','32','33','55','56','90')
			AND (EREC_UgyUgyiratok.TovabbitasAlattAllapot IS NULL OR
			EREC_UgyUgyiratok.TovabbitasAlattAllapot NOT IN ('0','10','30','31','32','33','55','56','90'))
			and getdate() between ErvKezd and ErvVege
		)

-- Saját küldemények:
-- (amik iktathatók (EB, 2008.11.26: ez a szűrés kivéve), és nem kimenő küldemény)
-- Néhány állapot nem kell:
-- (Iktatott, Postázott, Kimenő, összeállítás alatt, Expediált, Sztornózott)

	DECLARE @Sajat_Kuldemenyek int
	SET @Sajat_Kuldemenyek = (
		select Count(*)
		from EREC_KuldKuldemenyek
		where FelhasznaloCsoport_Id_Orzo = @ExecutorUserId
			AND PostazasIranya IN ('0','1')
		    --AND EREC_KuldKuldemenyek.Allapot IN ('01','03','05','00')
			--AND EREC_KuldKuldemenyek.IktatniKell = '1'
			AND EREC_KuldKuldemenyek.Allapot NOT IN ('04','06','51','52','90')
			AND (EREC_KuldKuldemenyek.TovabbitasAlattAllapot IS NULL OR
			EREC_KuldKuldemenyek.TovabbitasAlattAllapot NOT IN ('04','06','51','52','90'))
			AND getdate() BETWEEN ErvKezd AND ErvVege
	)

-- Saját iratpéldányok:
-- (Cimzett atvette, Sztornózott nem kell (listában sem hozzuk))
-- Néhány állapot nem kell:
-- (Lezárt jegyzéken, Jegyzékre helyezett, Címzett átvette, Postázott, Sztornózott)

	DECLARE @Sajat_IratPeldanyok INT
	SET @Sajat_IratPeldanyok = (
		select Count(*)
		from EREC_PldIratPeldanyok
		where FelhasznaloCsoport_Id_Orzo = @ExecutorUserId
			--AND Allapot NOT IN ('90','40')
			AND EREC_PldIratPeldanyok.Allapot NOT IN ('31','32','90') --('31','32','40','45','90')
			AND (EREC_PldIratPeldanyok.TovabbitasAlattAllapot IS NULL OR
			EREC_PldIratPeldanyok.TovabbitasAlattAllapot NOT IN ('31','32','90')) --('31','32','40','45','90')
			AND getdate() BETWEEN ErvKezd AND ErvVege
	)

-- Saját dossziék:
-- (Érvényesek, Csoport_Id_Tulaj szerint)

	DECLARE @Sajat_Mappak INT
	SET @Sajat_Mappak = (
		select Count(*)
		from KRT_Mappak
		where Csoport_Id_Tulaj = @ExecutorUserId
			AND getdate() BETWEEN ErvKezd AND ErvVege
	)

SELECT @Sajat_Ugyiratok as Sajat_Ugyiratok,
	@Sajat_Kuldemenyek as Sajat_Kuldemenyek,
	@Sajat_IratPeldanyok as Sajat_IratPeldanyok,
	@Sajat_Mappak as Sajat_Mappak

-- Jóváhagyandó ügyiratok:
-- Jóváhagyandó ügyiratok irattározásra:
	DECLARE @Jovahagyando_Ugyiratok_Irattarozasra int
	SET @Jovahagyando_Ugyiratok_Irattarozasra = (
		select Count(*)
		from EREC_UgyUgyiratok
		where --Csoport_Id_Felelos = @ExecutorUserId -- már nem adjuk át a vezetőnek
			Kovetkezo_Felelos_Id = @ExecutorUserId
			and getdate() between ErvKezd and ErvVege
            and (Allapot = '52') -- Irattározásra jóváhagyás alatt
		)

-- Jóváhagyandó ügyiratok kölcsönzésre:
-- 1. a futtató felhasználónak kölcsönzési jóváhagyási jogának kell lennie, másképp 0
-- 2. csak a futtató felhasználó által látható ügyiratokat számoljuk
-- 3. az ügyiratnak a központi irattárban kell lennie
DECLARE @Jovahagyando_Ugyiratok_Kolcsonzesre int
SET @Jovahagyando_Ugyiratok_Kolcsonzesre = 0
DECLARE @Jovahagyando_Ugyiratok_Kikeresre int
SET @Jovahagyando_Ugyiratok_Kikeresre = 0
-- központi irattár meghatározása
declare @KozpontiIrattar_Id uniqueidentifier
select @KozpontiIrattar_Id = Obj_Id from KRT_KodTarak
	where Kodcsoport_Id=(select Id from KRT_KodCsoportok where Kod = 'SPEC_SZERVEK')
	and Kod = 'SPEC_SZERVEK.KOZPONTIIRATTAR'
	and Org=@Org

if @KozpontiIrattar_Id is not null
begin
	declare @HasUserFunctionRight char(1)
	set @HasUserFunctionRight = '0'
-- Felhasználó funkciójogának ellenőrzése
	declare @KolcsonzesJovahagyas_Funkcio_Id uniqueidentifier
	select @KolcsonzesJovahagyas_Funkcio_Id = (select top 1 Id from KRT_Funkciok where Kod='KolcsonzesJovahagyas')
	if (@KolcsonzesJovahagyas_Funkcio_Id is not null)
	begin
		if exists(select fsz.Id from KRT_Felhasznalo_Szerepkor fsz
			join KRT_Szerepkorok sz on sz.Id = fsz.Szerepkor_Id
			join KRT_Szerepkor_Funkcio szf on sz.Id = szf.Szerepkor_Id
			join KRT_Funkciok f on f.Id = szf.Funkcio_Id
			where fsz.Felhasznalo_Id = @ExecutorUserId
			and f.Id = @KolcsonzesJovahagyas_Funkcio_Id
			and getdate() between fsz.ErvKezd and fsz.ErvVege
			and getdate() between sz.ErvKezd and sz.ErvVege
			and getdate() between szf.ErvKezd and szf.ErvVege
			and getdate() between f.ErvKezd and f.ErvVege)
		begin
			set @HasUserFunctionRight = '1'
		end
	end

   -- Ha van KolcsonzesJovahagyas joga, lekérdezés objektum jogosultságra szűréssel 
   -- UtilityIrattarList.GetIrattarList által visszaadott eredménnyel kell egyeznie (filter == Constants.JovahagyandokFilter.Kolcsonzesre)
	if @HasUserFunctionRight = '1'
	begin
		SET @Jovahagyando_Ugyiratok_Kolcsonzesre = 
				(
					SELECT COUNT(*)
						FROM EREC_UgyUgyiratok
					WHERE ([Allapot] IN ('10','55','56') OR [TovabbitasAlattAllapot] IN ('10','55','56'))
						AND ([Kovetkezo_Felelos_Id] = @ExecutorUserId)
						AND [FelhasznaloCsoport_Id_Orzo] = @KozpontiIrattar_Id
						AND (@KolcsonzesreKezelhetoIrattarak = '' OR [IrattarId] IN (SELECT convert(uniqueidentifier, Value) from dbo.fn_Split(@KolcsonzesreKezelhetoIrattarak,',')))
						AND GetDate() BETWEEN [ErvKezd] AND [ErvVege]
				)
	end

	--Kikeres
	set @HasUserFunctionRight = '0'
	declare @KikeresJovahagyas_Funkcio_Id uniqueidentifier
	select @KikeresJovahagyas_Funkcio_Id = (select top 1 Id from KRT_Funkciok where Kod='AtmenetiIrattarKikeresJovahagyas')
	if (@KikeresJovahagyas_Funkcio_Id is not null)
	begin
		if exists(select fsz.Id from KRT_Felhasznalo_Szerepkor fsz
			join KRT_Szerepkorok sz on sz.Id = fsz.Szerepkor_Id
			join KRT_Szerepkor_Funkcio szf on sz.Id = szf.Szerepkor_Id
			join KRT_Funkciok f on f.Id = szf.Funkcio_Id
			where fsz.Felhasznalo_Id = @ExecutorUserId
			and f.Id = @KikeresJovahagyas_Funkcio_Id
			and getdate() between fsz.ErvKezd and fsz.ErvVege
			and getdate() between sz.ErvKezd and sz.ErvVege
			and getdate() between szf.ErvKezd and szf.ErvVege
			and getdate() between f.ErvKezd and f.ErvVege)
		begin
			set @HasUserFunctionRight = '1'
		end
	end

	-- Ha van AtmenetiIrattarKikeresJovahagyas joga, lekérdezés objektum jogosultságra szűréssel 
	-- UtilityIrattarList.GetIrattarList által visszaadott eredménnyel kell egyeznie (filter == Constants.JovahagyandokFilter.Kikeresre)
	if @HasUserFunctionRight = '1'
	begin
		SET @Jovahagyando_Ugyiratok_Kikeresre = 
				(
					SELECT COUNT(*)
						FROM EREC_UgyUgyiratok
					WHERE ([Allapot] IN ('10', '54', '55', '56') OR [TovabbitasAlattAllapot] IN ('10', '54', '55', '56'))
						AND Allapot !=  '60' -- szereltek ne jöjjenek
						AND [Kovetkezo_Felelos_Id] = @ExecutorUserId
						AND [Csoport_Id_Felelos] <> @KozpontiIrattar_Id
						AND (@KikeresreKezelhetoIrattarak = '' OR [FelhasznaloCsoport_Id_Orzo] IN (SELECT convert(uniqueidentifier, Value) from dbo.fn_Split(@KikeresreKezelhetoIrattarak,',')))
						AND GetDate() BETWEEN [ErvKezd] AND [ErvVege]
				)
	end
end


-- EB 2009.03.27: Aláírandók kivéve
---- Jóváhagyandó ügyiratok aláírásra:
--	DECLARE @Jovahagyando_Ugyiratok_Alairasra int
--	SET @Jovahagyando_Ugyiratok_Alairasra = 0

-- Jóváhagyandó küldemények:
-- Jóváhagyandó küldemények irattározásra:
	DECLARE @Jovahagyando_Kuldemenyek_Irattarozasra int
	-- Küldeményekre nincs értelmezve a jóváhagyás
	/*SET @Jovahagyando_Kuldemenyek_Irattarozasra = (
		select Count(*)
		from EREC_KuldKuldemenyek
		where Csoport_Id_Felelos = @ExecutorUserId
			and getdate() between ErvKezd and ErvVege
	)*/
	SET @Jovahagyando_Kuldemenyek_Irattarozasra = 0

-- Jóváhagyandó küldemények kölcsönzésre:
	DECLARE @Jovahagyando_Kuldemenyek_Kolcsonzesre int
	SET @Jovahagyando_Kuldemenyek_Kolcsonzesre = 0

	DECLARE @Jovahagyando_Kuldemenyek_Kikeresre int
	SET @Jovahagyando_Kuldemenyek_Kikeresre = 0

-- EB 2009.03.27: Aláírandók kivéve
---- Jóváhagyandó küldemények aláírásra:
--	DECLARE @Jovahagyando_Kuldemenyek_Alairasra int
--	SET @Jovahagyando_Kuldemenyek_Alairasra = 0

-- Jóváhagyandó iratpéldányok:
-- Jóváhagyandó iratpéldányok irattározásra:
	DECLARE @Jovahagyando_IratPeldanyok_Irattarozasra int
	SET @Jovahagyando_IratPeldanyok_Irattarozasra = 0

-- Jóváhagyandó iratpéldányok kölcsönzésre:
	DECLARE @Jovahagyando_IratPeldanyok_Kolcsonzesre int
	SET @Jovahagyando_IratPeldanyok_Kolcsonzesre = 0

	DECLARE @Jovahagyando_IratPeldanyok_Kikeresre int
	SET @Jovahagyando_IratPeldanyok_Kikeresre = 0

-- EB 2009.03.27: Aláírandók kivéve
---- Jóváhagyandó iratpéldányok aláírásra:
--	DECLARE @Jovahagyando_IratPeldanyok_Alairasra int
--	SET @Jovahagyando_IratPeldanyok_Alairasra = (
--		select Count(*)
--		from EREC_PldIratPeldanyok
--		where Csoport_Id_Felelos = @ExecutorUserId
--			and getdate() between ErvKezd and ErvVege
--            and Allapot = '60' -- Jóváhagyás alatt
--	)

-- Jóváhagyandó dossziék:
-- Jóváhagyandó dossziék irattározásra:
	DECLARE @Jovahagyando_Mappak_Irattarozasra int
	SET @Jovahagyando_Mappak_Irattarozasra = 0

-- Jóváhagyandó dossziék kölcsönzésre:
	DECLARE @Jovahagyando_Mappak_Kolcsonzesre int
	SET @Jovahagyando_Mappak_Kolcsonzesre = 0

	DECLARE @Jovahagyando_Mappak_Kikeresre int
	SET @Jovahagyando_Mappak_Kikeresre = 0

-- Skontróba helyezés jóváhagyása	
DECLARE @Jovahagyando_Ugyiratok_SkontrobaHelyezes int
	SET @Jovahagyando_Ugyiratok_SkontrobaHelyezes = (
		select Count(*)
		from EREC_UgyUgyiratok
		where Kovetkezo_Felelos_Id = @ExecutorUserId
			and getdate() between ErvKezd and ErvVege
            and (Allapot = '70') -- Skontróba helyezés jóváhagyása
		)
		
DECLARE @Jovahagyando_Kuldemenyek_SkontrobaHelyezes INT
SET @Jovahagyando_Kuldemenyek_SkontrobaHelyezes = 0

DECLARE @Jovahagyando_IratPeldanyok_SkontrobaHelyezes INT
SET @Jovahagyando_IratPeldanyok_SkontrobaHelyezes = 0

DECLARE @Jovahagyando_Mappak_SkontrobaHelyezes INT
SET @Jovahagyando_Mappak_SkontrobaHelyezes = 0

SELECT @Jovahagyando_Ugyiratok_Irattarozasra as Jovahagyando_Ugyiratok_Irattarozasra,
	@Jovahagyando_Kuldemenyek_Irattarozasra as Jovahagyando_Kuldemenyek_Irattarozasra,
	@Jovahagyando_IratPeldanyok_Irattarozasra as Jovahagyando_IratPeldanyok_Irattarozasra,
	@Jovahagyando_Mappak_Irattarozasra as Jovahagyando_Mappak_Irattarozasra

SELECT @Jovahagyando_Ugyiratok_Kolcsonzesre as Jovahagyando_Ugyiratok_Kolcsonzesre,
	@Jovahagyando_Kuldemenyek_Kolcsonzesre as Jovahagyando_Kuldemenyek_Kolcsonzesre,
	@Jovahagyando_IratPeldanyok_Kolcsonzesre as Jovahagyando_IratPeldanyok_Kolcsonzesre,
	@Jovahagyando_Mappak_Kolcsonzesre as Jovahagyando_Mappak_Kolcsonzesre

-- EB 2009.03.27: Aláírandók kivéve
--SELECT @Jovahagyando_Ugyiratok_Alairasra as Jovahagyando_Ugyiratok_Alairasra,
--	@Jovahagyando_Kuldemenyek_Alairasra as Jovahagyando_Kuldemenyek_Alairasra,
--	@Jovahagyando_IratPeldanyok_Alairasra as Jovahagyando_IratPeldanyok_Alairasra
	
SELECT @Jovahagyando_Ugyiratok_SkontrobaHelyezes as Jovahagyando_Ugyiratok_SkontrobaHelyezes,
	@Jovahagyando_Kuldemenyek_SkontrobaHelyezes as Jovahagyando_Kuldemenyek_SkontrobaHelyezes,
	@Jovahagyando_IratPeldanyok_SkontrobaHelyezes as Jovahagyando_IratPeldanyok_SkontrobaHelyezes,
	@Jovahagyando_Mappak_SkontrobaHelyezes as Jovahagyando_Mappak_SkontrobaHelyezes
	
-- Jóváhagyandó ügyiratok elintézetté nyilvánításra:
	DECLARE @Jovahagyando_Ugyiratok_Elintezesre int
	SET @Jovahagyando_Ugyiratok_Elintezesre = (
		select Count(EREC_UgyUgyiratok.Id)
		from EREC_UgyUgyiratok EREC_UgyUgyiratok
		where EREC_UgyUgyiratok.Kovetkezo_Felelos_Id = @ExecutorUserId
			and getdate() between ErvKezd and ErvVege
            and (EREC_UgyUgyiratok.Allapot = '98') -- Elintézetté nyilvánítás jóváhagyása
		)

SELECT @Jovahagyando_Ugyiratok_Elintezesre	AS Jovahagyando_Ugyiratok_Elintezesre

DECLARE @EngedelyettKikeronLevo_Ugyiratok int
set @EngedelyettKikeronLevo_Ugyiratok = 0

SET @EngedelyettKikeronLevo_Ugyiratok = (
		select Count(*)
		from EREC_UgyUgyiratok
		where getdate() between ErvKezd and ErvVege
            and Allapot = '56' -- engedélyett kikérőn lévő
		)
		
SELECT @EngedelyettKikeronLevo_Ugyiratok AS EngedelyettKikeronLevo_Ugyiratok

SELECT @Jovahagyando_Ugyiratok_Kikeresre as Jovahagyando_Ugyiratok_Kikeresre,
	@Jovahagyando_Kuldemenyek_Kikeresre as Jovahagyando_Kuldemenyek_Kikeresre,
	@Jovahagyando_IratPeldanyok_Kikeresre as Jovahagyando_IratPeldanyok_Kikeresre,
	@Jovahagyando_Mappak_Kikeresre as Jovahagyando_Mappak_Kikeresre
	
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end