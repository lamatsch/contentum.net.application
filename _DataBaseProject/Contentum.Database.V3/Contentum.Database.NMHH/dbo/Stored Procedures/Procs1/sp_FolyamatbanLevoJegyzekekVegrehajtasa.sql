create procedure [dbo].[sp_FolyamatbanLevoJegyzekekVegrehajtasa]
			@EREC_IraJegyzekekServiceUrl nvarchar(max) -- http://ax-vfphtst03/NMHH_R2T/eRecordWebService/EREC_IraJegyzekekService.asmx
       
as
begin

declare @xmlOut varchar(8000)
declare @RequestText as nvarchar(max)

set @RequestText =
'<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <FolyamatbanLevoJegyzekekVegrehajtasa xmlns="Contentum.eRecord.WebService" />
  </soap:Body>
</soap:Envelope>'

exec dbo.sp_HTTPRequest 
	@EREC_IraJegyzekekServiceUrl, 'POST', @RequestText, 
	'Contentum.eRecord.WebService/FolyamatbanLevoJegyzekekVegrehajtasa', '', '', @xmlOut out

select @xmlOut

end