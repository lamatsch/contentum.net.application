﻿CREATE procedure [dbo].[sp_GetAllMetaAdatokHierarchiaFilter]
  @Where_AgazatiJelek	nvarchar(MAX) = '',
  @Where_IraIrattariTetelek	nvarchar(MAX) = '',
  @Where_IratMetaDefinicio			nvarchar(MAX) = '',
  @Where_Obj_MetaDefinicio	nvarchar(MAX) = '',
  @Where_Obj_MetaAdatai	nvarchar(MAX) = '',
  @Where_TargySzavak	nvarchar(MAX) = '',
  @ExecutorUserId	uniqueidentifier,
  @CsakSajatSzint char(1) = '1'
as

begin

BEGIN TRY
	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	DECLARE @sqlcmd nvarchar(MAX)

	set @sqlcmd = 'declare @isFilteredWithoutHit char(1)
set @isFilteredWithoutHit = ''0''
select top 0 Id into #tmp_TargySzavak from EREC_TargySzavak 
select top 0 Id, Obj_MetaDefinicio_Id into #tmp_ObjMetaAdatai from EREC_Obj_MetaAdatai
select top 0 Id, ColumnValue into #tmp_ObjMetaDefinicio from EREC_Obj_MetaDefinicio
select top 0 Id, Ugykor_Id, Ugytipus, EljarasiSzakasz, Irattipus,
       convert(uniqueidentifier, null) as IratMetaDefinicio_Id_Szulo,
       convert(int, null) as Szint
       into #tmp_IratMetaDefinicio from EREC_IratMetaDefinicio
select top 0 Id, AgazatiJel_Id into #tmp_IrattariTetelek from EREC_IraIrattariTetelek
select top 0 Id into #tmp_AgazatiJelek from EREC_AgazatiJelek
';

/************************************************************
* Szurési feltételek összeállítása
************************************************************/

-----------------------------------------
-- EREC_TargySzavak
-----------------------------------------
	
    if @Where_TargySzavak is not null and @Where_TargySzavak != ''
    begin 
        set @sqlcmd = @sqlcmd +
        'insert into #tmp_TargySzavak select Id
from EREC_TargySzavak 
where EREC_TragySzavak.Org=''' + cast(@Org as NVarChar(40)) + '''
and getdate() between EREC_TargySzavak.ErvKezd
and EREC_TargySzavak.ErvVege and ' + @Where_TargySzavak + ';
if (@@rowcount = 0)
begin
    set @isFilteredWithoutHit = ''1''
end
'
    end

-----------------------------------------
-- EREC_Obj_MetaAdatai
-----------------------------------------

if @Where_Obj_MetaAdatai is not null and @Where_Obj_MetaAdatai != ''
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0''
begin
    if exists(select Id from #tmp_TargySzavak)
    begin
		insert into #tmp_ObjMetaAdatai
		select EREC_Obj_MetaAdatai.Id, EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id 
		from EREC_Obj_MetaAdatai
		join #tmp_TargySzavak
		on EREC_Obj_MetaAdatai.Targyszavak_Id = #tmp_TargySzavak.Id
		where getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege and ' + @Where_Obj_MetaAdatai +';
    end
'
    set @sqlcmd = @sqlcmd + 
'    else
    begin
		insert into #tmp_ObjMetaAdatai
		select EREC_Obj_MetaAdatai.Id, EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id
		from EREC_Obj_MetaAdatai
		join EREC_TargySzavak
		on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
		where EREC_TargySzavak.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege and ' + @Where_Obj_MetaAdatai +';
    end
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end
else
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_TargySzavak)
begin
    insert into #tmp_ObjMetaAdatai
    select EREC_Obj_MetaAdatai.Id, EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id
    from EREC_Obj_MetaAdatai
    join #tmp_TargySzavak
    on EREC_Obj_MetaAdatai.Targyszavak_Id = #tmp_TargySzavak.Id
    where getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end

-----------------------------------------
-- EREC_Obj_MetaDefinicio
-----------------------------------------
-- felettesek, ha kell
--if @CsakSajatSzint = '0'
--begin
--    set @sqlcmd = @sqlcmd + '
--if @isFilteredWithoutHit = ''0''
--begin
--    select EREC_Obj_MetaDefinicio.Id as Id
--    into #tmp_ObjMetaDefinicio_A0
--		from EREC_Obj_MetaDefinicio
--		join #tmp_ObjMetaAdatai
--		on EREC_Obj_MetaDefinicio.Id = #tmp_ObjMetaAdatai.Obj_MetaDefinicio_Id
--		where getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
--		and EREC_Obj_MetaDefinicio.DefinicioTipus = ''A0'';
--end
--'
--end

if @CsakSajatSzint = '0'
begin
    set @sqlcmd = @sqlcmd + '
if @isFilteredWithoutHit = ''0''
begin
	with OMD_Hierarchia as
	(
		-- Base case
		select EREC_Obj_MetaDefinicio.Id, EREC_Obj_MetaDefinicio.Felettes_Obj_Meta, 0 as Szint
		from EREC_Obj_MetaDefinicio
		join #tmp_ObjMetaAdatai
		on EREC_Obj_MetaDefinicio.Id = #tmp_ObjMetaAdatai.Obj_MetaDefinicio_Id
		where EREC_Obj_MetaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
		and EREC_Obj_MetaDefinicio.DefinicioTipus = ''A0''

		union all

		-- Recursive step
		select omd.Id, omd.Felettes_Obj_Meta, omdh.Szint + 1 as Szint
		from EREC_Obj_MetaDefinicio as omd
		join OMD_Hierarchia as omdh
		on omdh.Felettes_Obj_Meta = omd.Id
		where omd.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between omd.ErvKezd and omd.ErvVege
		and omd.DefinicioTipus = ''A0''
    )
	select *
	into #tmp_ObjMetaDefinicio_A0
	from OMD_Hierarchia
end
'
end
------
if @Where_Obj_MetaDefinicio is not null and @Where_Obj_MetaDefinicio != ''
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0''
begin
    if exists(select Id from #tmp_ObjMetaAdatai)
    begin
		insert into #tmp_ObjMetaDefinicio
		select EREC_Obj_MetaDefinicio.Id, EREC_Obj_MetaDefinicio.ColumnValue
		from EREC_Obj_MetaDefinicio
		join #tmp_ObjMetaAdatai
		on EREC_Obj_MetaDefinicio.Id = #tmp_ObjMetaAdatai.Obj_MetaDefinicio_Id
		where EREC_Obj_MetaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
		and EREC_Obj_MetaDefinicio.DefinicioTipus = ''C2'' and ' + @Where_Obj_MetaDefinicio +';
    end
'
    set @sqlcmd = @sqlcmd + 
'    else
	begin
		insert into #tmp_ObjMetaDefinicio
		select EREC_Obj_MetaDefinicio.Id, EREC_Obj_MetaDefinicio.ColumnValue
		from EREC_Obj_MetaDefinicio
		where EREC_Obj_MetaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
		and EREC_Obj_MetaDefinicio.DefinicioTipus = ''C2'' and ' + @Where_Obj_MetaDefinicio +';
    end
    if @@rowcount = 0'

    if @CsakSajatSzint = '0'
    begin
        set @sqlcmd = @sqlcmd + ' and not exists(select Id from #tmp_ObjMetaDefinicio_A0)'
    end

    set @sqlcmd = @sqlcmd + '
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end
else
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_ObjMetaAdatai)
begin
    insert into #tmp_ObjMetaDefinicio
    select EREC_Obj_MetaDefinicio.Id, EREC_Obj_MetaDefinicio.ColumnValue
    from EREC_Obj_MetaDefinicio
    join #tmp_ObjMetaAdatai
    on EREC_Obj_MetaDefinicio.Id = #tmp_ObjMetaAdatai.Obj_MetaDefinicio_Id
    where getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_Metadefinicio.ErvVege
    and EREC_Obj_MetaDefinicio.DefinicioTipus = ''C2''
    if @@rowcount = 0'

    if @CsakSajatSzint = '0'
    begin
        set @sqlcmd = @sqlcmd + ' and not exists(select Id from #tmp_ObjMetaDefinicio_A0)'
    end

    set @sqlcmd = @sqlcmd + '
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end

-- a felettes objektum metadefiníciók meghatározása, ha szükséges
if @CsakSajatSzint = '0'
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_ObjMetaDefinicio_A0)
begin
    insert into #tmp_ObjMetaDefinicio
		select EREC_Obj_MetaDefinicio.Id, EREC_Obj_MetaDefinicio.ColumnValue
		from EREC_Obj_MetaDefinicio
		join #tmp_ObjMetaDefinicio_A0
		on #tmp_ObjMetaDefinicio_A0.Id = EREC_Obj_MetaDefinicio.Felettes_Obj_Meta
		and EREC_Obj_MetaDefinicio.DefinicioTipus = ''C2''
		where EREC_Obj_MetaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
	if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end


-----------------------------------------
-- EREC_IratMetaDefinicio
-----------------------------------------

if @Where_IratMetaDefinicio is not null and @Where_IratMetaDefinicio != ''
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0''
begin
    if exists(select Id from #tmp_ObjMetaDefinicio)
    begin
		insert into #tmp_IratMetaDefinicio 
		select EREC_IratMetaDefinicio.Id, EREC_IratMetaDefinicio.Ugykor_Id, EREC_IratMetaDefinicio.Ugytipus, EREC_IratMetaDefinicio.EljarasiSzakasz, EREC_IratMetaDefinicio.Irattipus,
		   (select imd.Id FROM EREC_IratMetaDefinicio imd WHERE EREC_IratMetaDefinicio.Ugykor_Id = imd.Ugykor_Id
		   and (
		     (EREC_IratMetaDefinicio.Irattipus is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz=EREC_IratMetaDefinicio.EljarasiSzakasz and imd.Irattipus is null)
		     OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		      OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is null and EREC_IratMetaDefinicio.Ugytipus is not null and imd.Ugytipus is null and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		   )
			and imd.Org=''' + cast(@Org as nVarChar(40)) + '''
		   and getdate() between imd.ErvKezd AND imd.ErvVege
		) AS IratMetaDefinicio_Id_Szulo,
		0 as Szint
		from EREC_IratMetaDefinicio
		join #tmp_ObjMetaDefinicio
		on convert(nvarchar(36), EREC_IratMetaDefinicio.Id) = #tmp_ObjMetaDefinicio.ColumnValue
		where EREC_IratMetaDefinicio.Org=''' + cast(@Org as nVarChar(40)) + '''
		and getdate() between EREC_IratMetaDefinicio.ErvKezd and EREC_IratMetaDefinicio.ErvVege and ' + @Where_IratMetaDefinicio +';
    end
'
    set @sqlcmd = @sqlcmd + 
'    else
    begin
		insert into #tmp_IratMetaDefinicio
		select EREC_IratMetaDefinicio.Id, EREC_IratMetaDefinicio.Ugykor_Id, EREC_IratMetaDefinicio.Ugytipus, EREC_IratMetaDefinicio.EljarasiSzakasz, EREC_IratMetaDefinicio.Irattipus,
		   (select imd.Id FROM EREC_IratMetaDefinicio imd WHERE EREC_IratMetaDefinicio.Ugykor_Id = imd.Ugykor_Id
		   and (
		     (EREC_IratMetaDefinicio.Irattipus is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz=EREC_IratMetaDefinicio.EljarasiSzakasz and imd.Irattipus is null)
		     OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		      OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is null and EREC_IratMetaDefinicio.Ugytipus is not null and imd.Ugytipus is null and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		   )
			and imd.Org=''' + cast(@Org as nVarChar(40)) + '''
		   and getdate() between imd.ErvKezd AND imd.ErvVege
		) AS IratMetaDefinicio_Id_Szulo,
		0 as Szint
		from EREC_IratMetaDefinicio
		where EREC_IratMetaDefinicio.Org=''' + cast(@Org as nVarChar(40)) + '''
		and getdate() between EREC_IratMetaDefinicio.ErvKezd and EREC_IratMetaDefinicio.ErvVege and ' + @Where_IratMetaDefinicio +';
    end
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end
else
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_ObjMetaDefinicio)
begin
    insert into #tmp_IratMetaDefinicio
    select EREC_IratMetaDefinicio.Id, EREC_IratMetaDefinicio.Ugykor_Id, EREC_IratMetaDefinicio.Ugytipus, EREC_IratMetaDefinicio.EljarasiSzakasz, EREC_IratMetaDefinicio.Irattipus,
       (select imd.Id FROM EREC_IratMetaDefinicio imd WHERE EREC_IratMetaDefinicio.Ugykor_Id = imd.Ugykor_Id
		   and (
		     (EREC_IratMetaDefinicio.Irattipus is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz=EREC_IratMetaDefinicio.EljarasiSzakasz and imd.Irattipus is null)
		     OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		     OR
		     (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is null and EREC_IratMetaDefinicio.Ugytipus is not null and imd.Ugytipus is null and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		   )
			and imd.Org=''' + cast(@Org as nVarChar(40)) + '''
		   and getdate() between imd.ErvKezd AND imd.ErvVege
		) AS IratMetaDefinicio_Id_Szulo,
		0 as Szint
    from EREC_IratMetaDefinicio
    join #tmp_ObjMetaDefinicio
    on convert(nvarchar(36), EREC_IratMetaDefinicio.Id) = #tmp_ObjMetaDefinicio.ColumnValue
    where EREC_IratMetaDefinicio.Org=''' + cast(@Org as nVarChar(40)) + '''
	and getdate() between EREC_IratMetaDefinicio.ErvKezd and EREC_IratMetaDefinicio.ErvVege
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end

-- Szülok lekérése
set @sqlcmd = @sqlcmd +
'if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_IratMetaDefinicio)
begin
    declare @Szint int
    set @Szint = 0
    declare @bFound char(1)
    set @bFound = ''1''
    while @bFound = ''1''
    begin
		set @Szint = @Szint + 1
		insert into #tmp_IratMetaDefinicio
		select EREC_IratMetaDefinicio.Id, EREC_IratMetaDefinicio.Ugykor_Id, EREC_IratMetaDefinicio.Ugytipus, EREC_IratMetaDefinicio.EljarasiSzakasz, EREC_IratMetaDefinicio.Irattipus,
		    (select imd.Id FROM EREC_IratMetaDefinicio imd WHERE EREC_IratMetaDefinicio.Ugykor_Id = imd.Ugykor_Id
		     and (
		       (EREC_IratMetaDefinicio.Irattipus is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz=EREC_IratMetaDefinicio.EljarasiSzakasz and imd.Irattipus is null)
		       OR
		       (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		       OR
		      (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is null and EREC_IratMetaDefinicio.Ugytipus is not null and imd.Ugytipus is null and imd.EljarasiSzakasz is null and imd.Irattipus is null)
		    )
			and imd.Org=''' + cast(@Org as nVarChar(40)) + '''
		    and getdate() between imd.ErvKezd AND imd.ErvVege
		    ) AS IratMetaDefinicio_Id_Szulo,
		    @Szint as Szint
		from EREC_IratMetaDefinicio
		where EREC_IratMetaDefinicio.Id in (select #tmp_IratMetaDefinicio.IratMetaDefinicio_Id_Szulo from #tmp_IratMetaDefinicio where #tmp_IratMetaDefinicio.Szint = @Szint-1)
		and EREC_IratMetaDefinicio.Id not in (select #tmp_IratMetaDefinicio.Id from #tmp_IratMetaDefinicio)

		if (@@rowcount = 0)
		begin
		    set @bFound = ''0''
		end
    end
end
'

-----------------------------------------
-- EREC_IraIrattariTetelek
-----------------------------------------

if @Where_IraIrattariTetelek is not null and @Where_IraIrattariTetelek != ''
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0''
begin
    if exists(select Ugykor_Id from #tmp_IratMetaDefinicio)
    begin
		insert into  #tmp_IrattariTetelek
		select EREC_IraIrattariTetelek.Id, EREC_IraIrattariTetelek.AgazatiJel_Id
		from EREC_IraIrattariTetelek
		join #tmp_IratMetaDefinicio
		on EREC_IraIrattariTetelek.Id = #tmp_IratMetaDefinicio.Ugykor_Id
		where EREC_IraIrattariTetelek.Org=''' + cast(@Org as NVarChar(40)) + ''' and getdate() between EREC_IraIrattariTetelek.ErvKezd and EREC_IraIrattariTetelek.ErvVege and ' + @Where_IraIrattariTetelek +';
    end
'
    set @sqlcmd = @sqlcmd + 
'    else
    begin
		insert into #tmp_IrattariTetelek
		select EREC_IraIrattariTetelek.Id, EREC_IraIrattariTetelek.AgazatiJel_Id
		from EREC_IraIrattariTetelek
		where EREC_IraIrattariTetelek.Org=''' + cast(@Org as NVarChar(40)) + ''' and  getdate() between EREC_IraIrattariTetelek.ErvKezd and EREC_IraIrattariTetelek.ErvVege and ' + @Where_IraIrattariTetelek +';
    end
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end
else
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_IratMetaDefinicio)
begin
    insert into #tmp_IrattariTetelek
    select EREC_IraIrattariTetelek.Id, EREC_IraIrattariTetelek.AgazatiJel_Id
    from EREC_IraIrattariTetelek
    join #tmp_IratMetaDefinicio
    on EREC_IraIrattariTetelek.Id = #tmp_IratMetaDefinicio.Ugykor_Id
    where EREC_IraIrattariTetelek.Org=''' + cast(@Org as NVarChar(40)) + ''' and getdate() between EREC_IraIrattariTetelek.ErvKezd and EREC_IraIrattariTetelek.ErvVege
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end

-----------------------------------------
-- EREC_AgazatiJelek
-----------------------------------------

if @Where_AgazatiJelek is not null and @Where_AgazatiJelek != ''
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0''
begin
    if exists(select Id from #tmp_IrattariTetelek)
    begin
		insert into #tmp_AgazatiJelek
		select EREC_AgazatiJelek.Id
		from EREC_AgazatiJelek
		join #tmp_IrattariTetelek
		on EREC_AgazatiJelek.Id = #tmp_IrattariTetelek.AgazatiJel_Id
		where EREC_AgazatiJelek.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_AgazatiJelek.ErvKezd and EREC_AgazatiJelek.ErvVege and ' + @Where_AgazatiJelek +';
    end
'
    set @sqlcmd = @sqlcmd + 
'    else
    begin
		insert into #tmp_AgazatiJelek
		select EREC_AgazatiJelek.Id
		from EREC_AgazatiJelek
		where EREC_AgazatiJelek.Org=''' + cast(@Org as NVarChar(40)) + '''
		and getdate() between EREC_AgazatiJelek.ErvKezd and EREC_AgazatiJelek.ErvVege and ' + @Where_AgazatiJelek +';
    end
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end
else
begin
    set @sqlcmd = @sqlcmd +
'
if @isFilteredWithoutHit = ''0'' and exists(select Id from #tmp_IrattariTetelek)
begin
    insert into #tmp_AgazatiJelek
    select EREC_AgazatiJelek.Id
    from EREC_AgazatiJelek
    join #tmp_IrattariTetelek
    on EREC_AgazatiJelek.Id = #tmp_IrattariTetelek.AgazatiJel_Id
    where getdate() between EREC_AgazatiJelek.ErvKezd and EREC_AgazatiJelek.ErvVege
    if (@@rowcount = 0)
    begin
		set @isFilteredWithoutHit = ''1''
    end
end
'
end

-----------------------------------------
-- Táblák lekérdezése
-----------------------------------------
set @sqlcmd = @sqlcmd +
'
select @isFilteredWithoutHit as IsFilteredWithoutHit
select distinct Id from #tmp_AgazatiJelek order by Id
select distinct Id from #tmp_IrattariTetelek order by Id
select distinct Id from #tmp_IratMetaDefinicio where Szint = 0 order by Id
select distinct Id from #tmp_IratMetaDefinicio where Irattipus is null and EljarasiSzakasz is null and Ugytipus is not null order by Id
select distinct Id from #tmp_IratMetaDefinicio where Irattipus is null and EljarasiSzakasz is not null order by Id
select distinct Id from #tmp_IratMetaDefinicio where Irattipus is not null order by Id
select distinct Id from #tmp_ObjMetaDefinicio order by Id
select distinct Id from #tmp_ObjMetaAdatai order by Id
select distinct Id from #tmp_TargySzavak order by Id
'

declare @startpoint int, @endpoint int, @length int
set @startpoint = 1
set @endpoint = 1
set @length = len(@sqlcmd)

---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
       
--print (@sqlcmd)
exec (@sqlcmd)



END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end