﻿create procedure [dbo].[sp_GetEMailBoritekCimTipusByFelado]
	@Felado	nvarchar(4000) = '',
	@ExecutorUserId uniqueidentifier
as
begin
	set nocount on
	declare @PID uniqueidentifier

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	if exists (select 1
                 from KRT_Felhasznalok as KRT_Felhasznalok
                where KRT_Felhasznalok.EMail = @Felado and KRT_Felhasznalok.Org=@Org)
		select '1' as BoritekCimTipus,
		       null as PartnerId
	else
		if exists (select 1
                     from EREC_eMailFiokok as EREC_eMailFiokok
                    where EREC_eMailFiokok.EmailCim = @Felado)
			select '2' as BoritekCimTipus,
			       null as PartnerId
	else
		if exists (select 1
                     from KRT_Cimek as KRT_Cimek,
                          KRT_PartnerCimek as KRT_PartnerCimek,
                          KRT_Partnerek as KRT_Partnerek
                    where KRT_Cimek.Nev = @Felado
                      and KRT_Cimek.Tipus = '03'
                      and KRT_Cimek.Id = KRT_PartnerCimek.Cim_Id
                      and KRT_PartnerCimek.Partner_Id = KRT_Partnerek.Id
                      and KRT_Partnerek.Org=@Org
					)
			select '3' as BoritekCimTipus,
			       KRT_Partnerek.Id as PartnerId
			  from KRT_Cimek as KRT_Cimek,
                   KRT_PartnerCimek as KRT_PartnerCimek,
                   KRT_Partnerek as KRT_Partnerek
             where KRT_Cimek.Nev = @Felado
               and KRT_Cimek.Tipus = '03'
               and KRT_Cimek.Id = KRT_PartnerCimek.Cim_Id
               and KRT_PartnerCimek.Partner_Id = KRT_Partnerek.Id
               and KRT_Partnerek.Org=@Org
	else
		select '4' as BoritekCimTipus,
			   null as PartnerId
end