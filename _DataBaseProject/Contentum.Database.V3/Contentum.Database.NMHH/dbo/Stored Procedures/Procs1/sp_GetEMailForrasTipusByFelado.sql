﻿create procedure [dbo].[sp_GetEMailForrasTipusByFelado]
	@Felado	nvarchar(4000) = '',
	@ExecutorUserId uniqueidentifier
as
begin
	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	if exists (select 1
                 from KRT_Felhasznalok as KRT_Felhasznalok
                where KRT_Felhasznalok.EMail = @Felado and KRT_Felhasznalok.Org=@Org)
		select 'B' as ForrasTipus
	else
		if exists (select 1
                     from KRT_Cimek as KRT_Cimek,
                          KRT_PartnerCimek as KRT_PartnerCimek,
                          KRT_Partnerek as KRT_Partnerek
                    where KRT_Cimek.Nev = @Felado
                      and KRT_Cimek.Tipus = '03'
                      and KRT_Cimek.Id = KRT_PartnerCimek.Cim_Id
                      and KRT_PartnerCimek.Partner_Id = KRT_Partnerek.Id
                      and KRT_Partnerek.Org=@Org
					)
			select 'K' as ForrasTipus
	else
		select 'K' as ForrasTipus
end