--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1 from  sysobjects
--           where  id = object_id('sp_GetElhiszAdatok')
--             and  type in ('P'))
--   drop procedure sp_GetElhiszAdatok
--go

create procedure [dbo].[sp_GetElhiszAdatok]
  @Where nvarchar(MAX) = '',
  @ExecutorUserId	uniqueidentifier
as

begin
BEGIN TRY


set nocount on

DECLARE @sqlcmd nvarchar(MAX)
     
          
 SET @sqlcmd =
'select 
       EREC_KuldKuldemenyek.Id as Kuldemeny_Id,
       EREC_IraIratok.Id as Irat_Id,
	   EREC_IraIratok.Azonosito as Irat_Azonosito,
	   KRT_Dokumentumok.Id as Dok_Id,
	   KRT_Dokumentumok.FajlNev as Dok_FajlNev,
	   KRT_Dokumentumok.External_Link as Dok_External_Link,
	   EREC_eBeadvanyok.Id as eBeadvany_Id,
	   EREC_eBeadvanyok.KR_HivatkozasiSzam as eBeadvany_KR_HivatkozasiSzam,
	   EREC_eBeadvanyok.KR_DokTipusHivatal as eBeadvany_KR_DokTipusHivatal,
	   EREC_eBeadvanyok.KR_DokTipusAzonosito as eBeadvany_KR_DokTipusAzonosito,
	   EREC_eBeadvanyok.KR_DokTipusLeiras as eBeadvany_KR_DokTipusLeiras,
	   EREC_eBeadvanyok.KR_Megjegyzes as eBeadvany_KR_Megjegyzes,
	   EREC_eBeadvanyok.KR_Valaszutvonal as eBeadvany_KR_Valaszutvonal,
	   EREC_eBeadvanyok.KR_FileNev as eBeadvany_KR_FileNev,
	   EREC_eBeadvanyok.KR_Valasztitkositas as eBeadvany_KR_Valasztitkositas,
	   EREC_eBeadvanyok.PR_Parameterek as eBeadvany_PR_Parameterek,
	   EREC_PldIratPeldanyok.CimSTR_Cimzett as Peldany_CimSTR_Cimzett,
	   EREC_PldIratPeldanyok.NevSTR_Cimzett as Peldany_NevSTR_Cimzett,
	   EREC_PldIratPeldanyok.Sorszam as Peldany_Sorszam,
	   EREC_KuldKuldemenyek.LetrehozasIdo as Kuldemeny_LetrehozasIdo,
	   EREC_IraIratok.Csoport_Id_Ugyfelelos Irat_Csoport_Id_Ugyfelelos,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez Irat_FelhasznaloCsoport_Id_Ugyintez
from EREC_KuldKuldemenyek
join EREC_Kuldemeny_IratPeldanyai on EREC_KuldKuldemenyek.Id = EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id and getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
join EREC_Csatolmanyok on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id and getdate() between EREC_Csatolmanyok.ErvKezd and EREC_Csatolmanyok.ErvVege
join KRT_Dokumentumok on EREC_Csatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id
left join EREC_eBeadvanyok on EREC_IraIratok.Id = EREC_eBeadvanyok.IraIrat_id and EREC_eBeadvanyok.Irany = ''2'''

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	exec (@sqlcmd);
	
	-- tal�latok sz�ma �s oldalsz�m

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

--where EREC_KuldKuldemenyek.Id in ('d7cec07e-1717-e811-80c9-00155d020dd3','0a2db889-2417-e811-80c9-00155d020dd3')