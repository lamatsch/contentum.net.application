﻿
create procedure [dbo].[sp_GetIdsFromStringForSSRS]
  @ids			nvarchar(max),
  @paratlan		int = null

as

begin

BEGIN TRY

set nocount on
	if @paratlan is null
		SELECT id as id_1 from  
		(select value as id, Pos AS rownum from fn_SplitWithPos(@ids,',')
		) AS t
	order by t.rownum
	else
		SELECT id as id_1 from  
		(select value as id, Pos AS rownum from fn_SplitWithPos(@ids,',')
		) AS t
		where (t.rownum % 2 = @paratlan)
		order by t.rownum

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

