﻿create procedure [dbo].[sp_GetIratszam]
  @ExecutorUserId				uniqueidentifier,
  @UgyiratId		uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
 
	SELECT COUNT([EREC_IraIratok].[Id]) as Iratszam
	FROM [EREC_UgyUgyiratdarabok]
	JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
	WHERE [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] = @UgyiratId
	AND ([EREC_IraIratok].[Allapot] = '04' OR [EREC_IraIratok].[Allapot] = '30') -- 04 - Iktatott, 30 - Kiadmányozott



END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end