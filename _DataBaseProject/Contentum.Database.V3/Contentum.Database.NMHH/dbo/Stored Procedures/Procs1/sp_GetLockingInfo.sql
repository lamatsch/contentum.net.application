﻿CREATE PROCEDURE [dbo].[sp_GetLockingInfo] 
		 @TableName NVARCHAR(100)
		,@Id uniqueidentifier
		,@ExecutorUserId uniqueidentifier
		,@IsLockedByOtherUser BIT OUTPUT
		,@Zarolo_id uniqueidentifier OUTPUT
AS
BEGIN
	DECLARE @selectCommand NVARCHAR(1000)
	SET @selectCommand = 
		'select 	    				
			 @Zarolo_id = Zarolo_id
		from '+@TableName+
		' where Id = @Id'
	
	exec sp_executesql @selectCommand, 
						 N'@Zarolo_id uniqueidentifier OUTPUT,@Id uniqueidentifier'
						,@Zarolo_id = @Zarolo_id OUTPUT,@Id = @Id
	--print @Zarolo_id

	if (@Zarolo_id is not null)
	begin
		if (@ExecutorUserId is null or @Zarolo_id != @ExecutorUserId)
		BEGIN
			SET @IsLockedByOtherUser = 1
		END
		else
		BEGIN
			SET @IsLockedByOtherUser = 0
		END	
	end
	else begin
		SET @IsLockedByOtherUser = 0
	end
END