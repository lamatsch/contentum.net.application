create procedure sp_GetPDFAdatok
			@DokumentumId uniqueidentifier,
         @ExecutorUserId uniqueidentifier

         
as

begin
BEGIN TRY

	set nocount on

	select EREC_eBeadvanyok.PR_ErkeztetesiSzam as ElhiszErkeztetoszam, 
		   EREC_UgyUgyiratok.Azonosito as Ugyiratszam,
		   EREC_KuldKuldemenyek.Azonosito as Erkeztetoszam,
		   isnull(EREC_KuldKuldemenyek.LetrehozasIdo, EREC_eBeadvanyok.LetrehozasIdo) as ErkezettDatum,
		   KRT_Dokumentumok.FajlNev as FajlNev,
		   KRT_Dokumentumok.External_Link
	from KRT_Dokumentumok
	join EREC_eBeadvanyCsatolmanyok on KRT_Dokumentumok.Id = EREC_eBeadvanyCsatolmanyok.Dokumentum_Id
	join EREC_eBeadvanyok on EREC_eBeadvanyCsatolmanyok.eBeadvany_Id = EREC_eBeadvanyok.Id
	left join EREC_KuldKuldemenyek on EREC_eBeadvanyok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
	left join EREC_IraIratok on EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id
	left join EREC_UgyUgyiratok on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
	where KRT_Dokumentumok.Id = @dokumentumId
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
