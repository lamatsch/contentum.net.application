﻿CREATE PROCEDURE [dbo].[sp_GetRightsByJogtargyIdAndCsoportId]
  (	@JogtargyId	uniqueidentifier,	-- a jogtárgy azonosítója
	@CsoportId	uniqueidentifier,	-- a csoport azonosítója
	@Jogszint	char(1)	= 'I',		-- vizsgált jogosultság szintje (I/O)
	@Tipus		char(1) = null		-- vizsgált jogosultság típusa (F,O,T,0,M)
  )
AS
/******************************************************************************************************** 
* Visszaadja, hogy a paraméterben megadott csoportnak van-e hozzáférése a megadott jogtárgyhoz.
* Visszatérési érték: 1, ha van jogosultsága, 0 egyébként.
********************************************************************************************************/
BEGIN
	select dbo.fn_GetRightsByJogtargyIdAndCsoportIdAndTipus(@JogtargyId,@CsoportId,@Jogszint,@Tipus);
END