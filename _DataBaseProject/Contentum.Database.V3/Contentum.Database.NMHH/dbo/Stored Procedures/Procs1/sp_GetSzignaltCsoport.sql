﻿CREATE PROCEDURE [dbo].[sp_GetSzignaltCsoport]
 ( @JogtargyId		uniqueidentifier,
   @CsoportId		uniqueidentifier,
   @ExecutorUserId	uniqueidentifier
)
AS
BEGIN
BEGIN TRY
	if not exists (select 1 from dbo.fn_GetAllSubCsoportByCsoportId(@ExecutorUserId) where Id = @CsoportId)
		raiserror('[52153]',16,1); 
	
	if exists (select 1 from KRT_Csoportok where Id = @CsoportId and Tipus = '0')
	BEGIN
		select KRT_Csoportok.*
			from KRT_Csoporttagok 
				inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Csoporttagok.Csoport_Id_Jogalany
			where KRT_Csoporttagok.Csoport_Id = @CsoportId and KRT_Csoporttagok.Tipus = '3'
	END		
	else
		select KRT_Csoportok.* from KRT_Csoportok where Id = @CsoportId;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH
END