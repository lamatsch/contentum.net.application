
create procedure [dbo].[sp_GetUgyfelelosSzervezetKod]
  @ExecutorUserId				uniqueidentifier,
  @UgyiratId		uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
 
	SELECT dbo.fn_GetCsoportKod(EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos) as UgyFelelos_SzervezetKod
	FROM EREC_UgyUgyiratok
	WHERE id = @UgyiratId 

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


