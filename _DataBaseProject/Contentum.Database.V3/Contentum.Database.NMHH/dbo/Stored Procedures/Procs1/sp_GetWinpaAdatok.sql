﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1 from  sysobjects
	   where  id = object_id('sp_GetWinpaAdatok')
		 and  type in ('P'))
drop procedure [dbo].[sp_GetWinpaAdatok]
go


CREATE procedure [dbo].[sp_GetWinpaAdatok]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKuldemenyek.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier
as

begin
BEGIN TRY


set nocount on

   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  ' with kuldemeny_peldanyok_work as
	(
		select EREC_KuldKuldemenyek.Id as KuldKuldemeny_Id, 
			    (
					select replace(EREC_PldIratpeldanyok.Azonosito, '' '','''') + '',''
					From EREC_Kuldemeny_IratPeldanyai EREC_Kuldemeny_IratPeldanyai
					join EREC_PldIratpeldanyok EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.id = EREC_Kuldemeny_IratPeldanyai.Peldany_Id
					where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
					and getdate() between EREC_PldIratpeldanyok.ErvKezd and EREC_PldIratpeldanyok.ErvVege
					and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
					ORDER BY EREC_PldIratpeldanyok.Azonosito
					For XML PATH ('''')
			    ) as Azonosito
		from EREC_KuldKuldemenyek EREC_KuldKuldemenyek
	),
	kuldemeny_peldanyok as
	(
		select KuldKuldemeny_Id, left(left(Azonosito, len(Azonosito) -1), 100) as Azonosito
		from kuldemeny_peldanyok_work
	)
		--BLG1780 - Ugyintezo monogram az UgyfelAzonosito mezobe
	,ugyintezok as
	(
	select k.id [kuld_id], isnull(s.szuletesinev,left(isnull(s.ujCsaladiNev,f.Nev),1) + left(isnull(s.ujUtonev,'' ''),1) + left(isnull(s.UjTovabbiUtonev,'' ''),1)) [monogram], u.Azonosito [iktatoszam] from EREC_KuldKuldemenyek k 
	inner join EREC_Kuldemeny_IratPeldanyai kp on k.id = kp.kuldkuldemeny_id
	inner join EREC_PldIratPeldanyok p on kp.peldany_id = p.id
	inner join EREC_IraIratok i on p.irairat_id = i.id
	inner join EREC_UgyUgyiratok u on i.ugyirat_id = u.id
	inner join KRT_Felhasznalok f on u.FelhasznaloCsoport_Id_Ugyintez = f.id
	inner join KRT_Szemelyek s on f.partner_id = s.partner_id
	),
	monogram as
	(
	Select distinct u2.kuld_id,
		substring(
			(
				Select distinct '' ''+u1.monogram  AS [text()]
				From ugyintezok u1
				Where u1.kuld_id = u2.kuld_id
				--order by u1.iktatoszam
				For XML PATH ('''')
			), 2, 1000) [UgyfelAzonosito]
	From ugyintezok u2
	)'
  SET @sqlcmd = @sqlcmd + '
    select 
	EREC_KuldKuldemenyek.Id as Id,
	kuldemeny_peldanyok.Azonosito IKTATOSZAM
  	,ROW_NUMBER() over (ORDER BY EREC_KuldKuldemenyek.id) OBJ_ID
	,EREC_KuldKuldemenyek.RagSzam RAGSZAM
    ,left(EREC_KuldKuldemenyek.NevSTR_Bekuldo, 100) NEV
    , KRT_Cimek.TelepulesNev TELEPULES
	, KRT_Cimek.IRSZ IRSZ
	, Cast(
		CASE
		WHEN ( /* közterület név van */
			(KRT_Cimek.KozteruletNev is NOT null and KRT_Cimek.KozteruletNev != '''')
		) THEN 
			 KRT_Cimek.KozteruletNev
		WHEN (/* nincs közterület név de van HRSZ */
			(KRT_Cimek.KozteruletNev is null or KRT_Cimek.KozteruletNev = '''')
			 AND
			(KRT_Cimek.HRSZ is NOT null and KRT_Cimek.HRSZ != '''')
		) THEN
			''HRSZ. '' + KRT_Cimek.HRSZ	
		END 	as varchar(200)) CIM
	, Cast(KRT_Cimek.KozteruletTipusNev as varchar(200)) KOZTERULET_JELLEG
	, Cast(CASE WHEN KRT_Cimek.Tipus = ''02'' THEN NULL 
											  ELSE
											  CASE 
											  WHEN /* nincs házszám de van HRSZ és közterület név*/			
												(KRT_Cimek.Hazszam is null OR KRT_Cimek.Hazszam = '''')
												 AND
												(KRT_Cimek.HRSZ is NOT null and  KRT_Cimek.HRSZ != '''')
												 AND
												(KRT_Cimek.KozteruletNev is NOT null and  KRT_Cimek.KozteruletNev != '''')
											  THEN 
												''HRSZ. '' + KRT_Cimek.HRSZ
											  ELSE
												KRT_Cimek.Hazszam + CASE WHEN  KRT_Cimek.Hazszamig is NULL OR  KRT_Cimek.Hazszamig = '''' THEN '''' ELSE '' - '' +  KRT_Cimek.Hazszamig END
											  END 
		   END as varchar(200)) HAZSZAM
	, Cast(KRT_Cimek.HazszamBetujel as varchar(200)) EPULET
	, Cast(KRT_Cimek.Lepcsohaz as varchar(200)) LEPCSOHAZ
	, Cast(KRT_Cimek.Szint as varchar(200)) EMELET
	, Cast(KRT_Cimek.Ajto + CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '''' THEN '''' ELSE '' / '' + KRT_Cimek.AjtoBetujel END as varchar(200)) AJTO
	, CAST(CASE WHEN KRT_Cimek.Tipus = ''02'' THEN KRT_Cimek.Hazszam ELSE NULL END as varchar(200)) POSTAFIOK
	--  
    , EREC_KuldKuldemenyek.KimenoKuldemenyFajta
    , EREC_KuldKuldemenyek.Elsobbsegi Elsobbsegi
	, EREC_KuldKuldemenyek.Ajanlott Ajanlott
	, EREC_KuldKuldemenyek.Tertiveveny Tertiveveny
	, EREC_KuldKuldemenyek.SajatKezbe SajatKezbe
	, EREC_KuldKuldemenyek.E_ertesites E_ertesites
	, EREC_KuldKuldemenyek.E_elorejelzes E_elorejelzes 
	, EREC_KuldKuldemenyek.PostaiLezaroSzolgalat PostaiLezaroSzolgalat
    , KRT_Orszagok.Viszonylatkod RENDELTETESI_HELY
	,EREC_KuldKuldemenyek.Tipus KULDEMENY_TIPUS
	, KRT_Orszagok.Nev ORSZAG_NEV
	, KRT_Orszagok.Kod ORSZAG_KOD
	--BLG1780 - Ugyintezo monogram az UgyfelAzonosito mezobe
	, left(monogram.UgyfelAzonosito,254) UGYFELAZONOSITO
    FROM dbo.EREC_KuldKuldemenyek EREC_KuldKuldemenyek
  left JOIN dbo.KRT_Cimek KRT_Cimek ON EREC_KuldKuldemenyek.Cim_Id = KRT_Cimek.Id
  left join dbo.KRT_Telepulesek KRT_Telepulesek ON KRT_Cimek.Telepules_Id = KRT_Telepulesek.Id
  -- BUG_3246
  -- left JOIN dbo.KRT_Orszagok KRT_Orszagok ON KRT_Telepulesek.Orszag_Id = KRT_Orszagok.Id 
    left JOIN dbo.KRT_Orszagok KRT_Orszagok ON KRT_Cimek.Orszag_Id = KRT_Orszagok.Id 
  join kuldemeny_peldanyok kuldemeny_peldanyok on EREC_KuldKuldemenyek.Id = kuldemeny_peldanyok.KuldKuldemeny_Id
   --BLG1780 - Ugyintezo monogram az UgyfelAzonosito mezobe
  join monogram monogram on monogram.kuld_id = EREC_KuldKuldemenyek.Id
  '
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
	print @sqlcmd
	exec (@sqlcmd);
	
	-- találatok száma és oldalszám

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
