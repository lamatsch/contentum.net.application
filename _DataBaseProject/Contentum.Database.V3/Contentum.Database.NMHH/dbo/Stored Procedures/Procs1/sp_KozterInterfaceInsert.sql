﻿create procedure [dbo].[sp_KozterInterfaceInsert]    
                @ktdok_id  uniqueidentifier = null,    
                @ktdok_kod     Nvarchar(9)  = null,
                @ktdok_iktszam     Nvarchar(14)  = null,
                @ktdok_vonalkod     Nvarchar(13)  = null,
                @ktdok_dokid     uniqueidentifier  = null,
                @ktdok_url     Nvarchar(1000)  = null,
                @ktdok_date     datetime  = null,
                @ktdok_siker     char(1)  = null,
                @ktdok_ok     Nvarchar(256)  = null,
                @ktdok_atvet     datetime  = null,
			    @UpdatedColumns xml = null,
		        @ResultUid uniqueidentifier = null OUTPUT
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
if @ktdok_id is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_id'
	SET @insertValues = @insertValues + ',@ktdok_id'
end

if @ktdok_kod is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_kod'
	SET @insertValues = @insertValues + ',@ktdok_kod'
end

if @ktdok_iktszam is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_iktszam'
	SET @insertValues = @insertValues + ',@ktdok_iktszam'
end

if @ktdok_vonalkod is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_vonalkod'
	SET @insertValues = @insertValues + ',@ktdok_vonalkod'
end

if @ktdok_dokid is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_dokid'
	SET @insertValues = @insertValues + ',@ktdok_dokid'
end

if @ktdok_url is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_url'
	SET @insertValues = @insertValues + ',@ktdok_url'
end

if @ktdok_date is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_date'
	SET @insertValues = @insertValues + ',@ktdok_date'
end

if @ktdok_siker is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_siker'
	SET @insertValues = @insertValues + ',@ktdok_siker'
end

if @ktdok_ok is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_ok'
	SET @insertValues = @insertValues + ',@ktdok_ok'
end

if @ktdok_atvet is not null
begin
	SET @insertColumns = @insertColumns + ',ktdok_atvet'
	SET @insertValues = @insertValues + ',@ktdok_atvet'
end

   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into tbl_KozterInterface ('+@insertColumns+') output inserted.ktdok_id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

select @InsertCommand

exec sp_executesql @InsertCommand, N'@ktdok_id  uniqueidentifier, @ktdok_kod Nvarchar(9), @ktdok_iktszam  Nvarchar(14), @ktdok_vonalkod Nvarchar(13), @ktdok_dokid uniqueidentifier, @ktdok_url Nvarchar(1000), @ktdok_date datetime, @ktdok_siker char(1), @ktdok_ok Nvarchar(256), @ktdok_atvet datetime, @ResultUid uniqueidentifier OUTPUT'
,@ktdok_id=@ktdok_id,@ktdok_kod=@ktdok_kod,@ktdok_iktszam=@ktdok_iktszam,@ktdok_vonalkod=@ktdok_vonalkod,@ktdok_dokid=@ktdok_dokid,@ktdok_url=@ktdok_url,@ktdok_date=@ktdok_date,@ktdok_siker=@ktdok_siker,@ktdok_ok=@ktdok_ok,@ktdok_atvet=@ktdok_atvet, @ResultUid = @ResultUid OUTPUT

--exec sp_executesql @InsertCommand, N'@ktdok_id  uniqueidentifier, @ktdok_kod Nvarchar(9), @ktdok_iktszam  Nvarchar(14), @ktdok_vonalkod Nvarchar(13), @ktdok_dokid uniqueidentifier, @ktdok_url Nvarchar(1000), @ktdok_date datetime, @ktdok_siker char(1), @ktdok_ok Nvarchar(256), @ktdok_atvet datetime'
--,@ktdok_id=@ktdok_id,@ktdok_kod=@ktdok_kod,@ktdok_iktszam=@ktdok_iktszam,@ktdok_vonalkod=@ktdok_vonalkod,@ktdok_dokid=@ktdok_dokid,@ktdok_url=@ktdok_url,@ktdok_date=@ktdok_date,@ktdok_siker=@ktdok_siker,@ktdok_ok=@ktdok_ok,@ktdok_atvet=@ktdok_atvet

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
           
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH