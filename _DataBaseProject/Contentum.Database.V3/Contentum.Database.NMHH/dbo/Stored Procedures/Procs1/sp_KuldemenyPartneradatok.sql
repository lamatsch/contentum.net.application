﻿CREATE procedure [dbo].[sp_KuldemenyPartneradatok]
-- @Param1 nvarchar(10) = 'default érték', -- ha van érték megadva, nem kötelezo
-- @Param2 datetime, -- ha nincs alapértemezett érték, mindig át kell adni híváskor
-- @Param3 uniqueidentifier
as

begin

BEGIN TRY -- hibafigyelo blokk eleje, csak akkor kell, ha kezeljük a hibákat

	-- küldemények partneradatai --
	SELECT DISTINCT 
		   nevstr_bekuldo as 'Partner név'
		 , COUNT(*) as 'Elofordulás'
	FROM   erec_kuldkuldemenyek k
	WHERE  partner_id_bekuldo is null
	AND    nevstr_bekuldo != 'NULL'
	AND    nevstr_bekuldo NOT IN
		(select distinct nevstr_bekuldo
		 from   erec_kuldkuldemenyek k
			  , krt_partnerek p
		 where  partner_id_bekuldo is not null
		 and    p.id = partner_id_bekuldo
		 and    nevstr_bekuldo != p.nev)
	GROUP BY nevstr_bekuldo
	HAVING   COUNT(*) > 2

	

END TRY -- hibafigyelo blokk vége

-- ha hiba lépett fel, továbbdobjuk az alkalmazás felé
BEGIN CATCH -- hibakezelési blokk eleje, csak akkor kell, ha fent volt BEGIN/END TRY blokk
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH -- hibakezelési blokk vége

end