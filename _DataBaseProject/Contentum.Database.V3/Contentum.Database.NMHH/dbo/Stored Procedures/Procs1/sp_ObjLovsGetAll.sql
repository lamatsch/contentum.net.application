﻿create procedure [dbo].[sp_ObjLovsGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   ObjLovs.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   ObjLovs.Id,
	   ObjLovs.ObjTip_id,
	   ObjLovs.Kod,
	   ObjLovs.Nev,
	   ObjLovs.Ver,
	   ObjLovs.Note,
	   ObjLovs.Stat_id,
	   ObjLovs.ErvKezd,
	   ObjLovs.ErvVege,
	   ObjLovs.Letrehozo_id,
	   ObjLovs.LetrehozasIdo,
	   ObjLovs.Modosito_id,
	   ObjLovs.ModositasIdo,
	   ObjLovs.Zarolo_id,
	   ObjLovs.ZarolasIdo,
	   ObjLovs.Tranz_id,
	   ObjLovs.UIAccessLog_id  
   from 
     ObjLovs as ObjLovs      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end