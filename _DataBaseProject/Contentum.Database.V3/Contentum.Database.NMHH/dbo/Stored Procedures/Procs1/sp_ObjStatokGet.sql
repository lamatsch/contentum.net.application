﻿create procedure [dbo].[sp_ObjStatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   ObjStatok.Id,
	   ObjStatok.Nev,
	   ObjStatok.ObjTip_Id,
	   ObjStatok.ObjStat_Id,
	   ObjStatok.Ver,
	   ObjStatok.Note,
	   ObjStatok.Stat_id,
	   ObjStatok.ErvKezd,
	   ObjStatok.ErvVege,
	   ObjStatok.Letrehozo_id,
	   ObjStatok.LetrehozasIdo,
	   ObjStatok.Modosito_id,
	   ObjStatok.ModositasIdo,
	   ObjStatok.Zarolo_id,
	   ObjStatok.ZarolasIdo,
	   ObjStatok.Tranz_id,
	   ObjStatok.UIAccessLog_id
	   from 
		 ObjStatok as ObjStatok 
	   where
		 ObjStatok.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end