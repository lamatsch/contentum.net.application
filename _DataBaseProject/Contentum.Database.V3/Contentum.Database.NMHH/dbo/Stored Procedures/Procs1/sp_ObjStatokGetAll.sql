﻿create procedure [dbo].[sp_ObjStatokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   ObjStatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   ObjStatok.Id,
	   ObjStatok.Nev,
	   ObjStatok.ObjTip_Id,
	   ObjStatok.ObjStat_Id,
	   ObjStatok.Ver,
	   ObjStatok.Note,
	   ObjStatok.Stat_id,
	   ObjStatok.ErvKezd,
	   ObjStatok.ErvVege,
	   ObjStatok.Letrehozo_id,
	   ObjStatok.LetrehozasIdo,
	   ObjStatok.Modosito_id,
	   ObjStatok.ModositasIdo,
	   ObjStatok.Zarolo_id,
	   ObjStatok.ZarolasIdo,
	   ObjStatok.Tranz_id,
	   ObjStatok.UIAccessLog_id  
   from 
     ObjStatok as ObjStatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end