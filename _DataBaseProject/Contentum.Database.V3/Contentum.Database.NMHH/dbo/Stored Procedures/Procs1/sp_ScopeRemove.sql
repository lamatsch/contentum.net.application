﻿create procedure [dbo].[sp_ScopeRemove]
  ( @Csoport_Id uniqueidentifier,
    @Tabla_Nev nvarchar(100),          --JogtA?rgy tA?bla neve (pl.: EREC_IraIktatoKonyvek, EREC_UgyUgyiratdarabok)
    @Rekord_Id uniqueidentifier        --JogtA?rgy sor egyedi azonosA­tAlja
  )
as
begin
begin try
--  begin transaction ScopeRemoveTransaction;
  set nocount on;
  --declare


  if ( 6 = 7 ) raiserror('[50404]', 16, 1); --egyik raiserror

  if ( 6 = 7 ) --mA?sik raiserror
  begin
    raiserror('[50404]', 16, 1);
    return @@error;
  end

--  commit transaction ScopeRemoveTransaction;

end try
begin catch
--  if @@TRANCOUNT > 0 rollback transaction ScopeRemoveTransaction;

  declare @errorSeverity int, @errorState int, @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + ERROR_MESSAGE();
  else
    set @errorCode = ERROR_MESSAGE();
  if @errorState = 0 set @errorState = 1;

  raiserror(@errorCode, @errorSeverity, @errorState);

end catch
end --sp_ScopeRemove