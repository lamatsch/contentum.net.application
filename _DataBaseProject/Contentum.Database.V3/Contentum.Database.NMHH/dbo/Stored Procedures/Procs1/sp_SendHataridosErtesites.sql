﻿create procedure [dbo].[sp_SendHataridosErtesites]
			@eAdminWebServiceUrl NVARCHAR(4000),
            @felhasznaloId nvarchar(100),
            @Funkcio_Id_Kivalto nvarchar(100),
            @Funkcio_Id_Inditando nvarchar(100),
            @Csoport_Id_FelelosFelhasznalo nvarchar(100),
            @Leiras NVARCHAR(400),
            @Prioritas int,
            @IntezkHatarido DATETIME,
            @Obj_type NVARCHAR(100),
            @ObjektumSzukites NVARCHAR(4000),
            @Result INT OUTPUT
            
         
as

begin
BEGIN TRY

	set nocount on

	SELECT @Result = dbo.fn_SendHataridosErtesites(@eAdminWebServiceUrl,@felhasznaloId, @Funkcio_Id_Kivalto, @Funkcio_Id_Inditando, @Csoport_Id_FelelosFelhasznalo, @Leiras, @Prioritas, @IntezkHatarido, @Obj_type, @ObjektumSzukites)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end