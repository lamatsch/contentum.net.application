﻿CREATE procedure [dbo].[sp_Statisztika]
-- @Param1 nvarchar(10) = 'default érték', -- ha van érték megadva, nem kötelezo
-- @Param2 datetime, -- ha nincs alapértemezett érték, mindig át kell adni híváskor
-- @Param3 uniqueidentifier
as

begin

BEGIN TRY -- hibafigyelo blokk eleje, csak akkor kell, ha kezeljük a hibákat

	select f.nev as FunkcioNev
		 , cs.nev as CsoportNev1
		 , cs2.nev as CsoportNev2
		 , case 
			 when cs2.nev in (select distinct cs4.nev
							 from   erec_irat_iktatokonyvei i
								  , krt_csoportok cs4
							 where  cs4.id = i.csoport_id_iktathat
							 and    cs4.id not in 
								   (select cs6.id
									from krt_csoporttagok t
									   , krt_csoportok cs6
									where 
									cs6.id=t.csoport_id_jogalany
									and (cs6.tipus = '0' or cs6.tipus= 'R')
									and t.csoport_id in
									(select cs5.id 
									 from krt_csoportok cs5
									 where cs5.nev in ('Adó Ügyosztály')))) then
				  cs2.nev
		   else   cs3.nev
		   end    as CsoportNev3  
 		 , convert(datetime, substring(cast(e.LetrehozasIdo as varchar(23)), 1, 11)) as Letrehozasido
--		 , count(*) as RekordSzam
	from   krt_esemenyek e
		 , krt_csoportok cs
		 , krt_csoportok cs2
		 , krt_ExcelUsers ex
		 , krt_csoporttagok t
		 , krt_csoportok cs3
         , krt_funkciok f
	where  e.funkcio_id = f.id
    and    f.id in 
			('1E6FB872-2C3C-47CD-97A2-1930290543DB',
			'D8F5DBF2-3D09-448E-8110-21F0E677D03B',
			'3A1FFA90-4155-4666-8690-2627F2930748',
			'AB0DA136-57E4-4989-832F-35DDEEB3F6D6',
			'FA78DA5D-0990-4DE7-A254-3D4FD193CB35',
			'B2BBF636-47FC-42CD-98C4-5C207524952A',
			'7DD6C002-7A9B-489A-8C1E-7012413C267A',
			'63A9E9ED-0574-4ED9-8A07-7F5355007213',
			'71C97493-DF5F-4CBA-80CF-B03D6A6FB711',
			'771498FE-C512-4136-837A-BEB0333077E3',
			'026555BD-FFA8-4824-8372-DB492A6C40EE',
			'38B849F7-E7C1-458A-BE76-E39CE4E2DC4A')
	and    cs.id = e.felhasznalo_id_login
--	and    cs.id = e.felhasznalo_id_user
	and    cs.ervvege > getdate()
	and    e.ervvege  > getdate()
	and    ex.suser_name = suser_name()
    and    e.csoport_id_felelosuserszerveze = cs2.id
	and    t.csoport_id_jogalany = cs2.id
	and    t.ervvege > getdate()
	and    t.csoport_id = cs3.id
    and    cs3.nev != 'FPH szervezete' 
    and    cs3.ervvege > getdate()
    and    cs3.tipus = '0'
    and    convert(datetime, substring(cast(e.LetrehozasIdo as varchar(23)), 1, 11)) = convert(datetime, substring(cast(getdate() as varchar(23)), 1, 11))
--    and    e.ervkezd > '2008-05-01'

END TRY -- hibafigyelo blokk vége

-- ha hiba lépett fel, továbbdobjuk az alkalmazás felé
BEGIN CATCH -- hibakezelési blokk eleje, csak akkor kell, ha fent volt BEGIN/END TRY blokk
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH -- hibakezelési blokk vége

end