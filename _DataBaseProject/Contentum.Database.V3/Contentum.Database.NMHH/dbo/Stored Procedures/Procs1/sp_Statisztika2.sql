﻿-- tárolt eljárás létrehozása (+ paraméterezés, ha kell)
CREATE procedure [dbo].[sp_Statisztika2]
-- @Param1 nvarchar(10) = 'default érték', -- ha van érték megadva, nem kötelezo
-- @Param2 datetime, -- ha nincs alapértemezett érték, mindig át kell adni híváskor
-- @Param3 uniqueidentifier
as

begin

BEGIN TRY -- hibafigyelo blokk eleje, csak akkor kell, ha kezeljük a hibákat

	select convert(datetime, substring(cast(beerkezesideje as varchar(23)), 1, 11)) as beerk_ideje
		 , cs.nev as iktato
		 , b.kod as vonalkod
		 , erkezteto_szam as erkszam
		 , hivatkozasiszam as hivszam
		 , ragszam as ragszam
		 , targy as targy
		 , nevstr_bekuldo as bekuldo
		 , cimstr_bekuldo as bekuldo_cime
		 , cs2.nev as cimzett
		 , k.allapot as allapot
	from   erec_kuldkuldemenyek k
		 , krt_csoportok cs
		 , krt_csoportok cs2
		 , krt_barkodok b
	where  cs.id = k.csoport_id_felelos
	and    k.allapot = '00'
--	and    (k.allapot = '00' or k.allapot = '01')
--	and    (k.allapot = '00' or k.allapot = '04')
	and    b.obj_id = k.id
	and    k.csoport_id_cimzett = cs2.id
	and    cs.ervvege > getdate()
	and    cs.tipus <> '0'
	and    cs.tipus <> '4'
	and    beerkezesideje >= '2008-04-28'
	and    iktatnikell = 1

END TRY -- hibafigyelo blokk vége

-- ha hiba lépett fel, továbbdobjuk az alkalmazás felé
BEGIN CATCH -- hibakezelési blokk eleje, csak akkor kell, ha fent volt BEGIN/END TRY blokk
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH -- hibakezelési blokk vége

end