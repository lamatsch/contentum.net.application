﻿/*
create procedure [dbo].[sp_Szervezet_Hierarchia]    
                 @Szemely_Id           uniqueidentifier
*/

/*
-- FONTOS MEGJEGYZÉS:
   a procit kicsit át kellett paraméterezni, hogy C#-bol gyorsan hívható legyen,
   ezért majd az alábbi módon hívják,
   ahol @ExecutorUserId tartalmazza majd a régi @Szemely_Id -t,
   a többi bemeno paraméterrel pedig, egyszeruen nem foglalkozunk!!!!!!!

*/

create procedure [dbo].[sp_Szervezet_Hierarchia]
                 @Where nvarchar(4000) = '',
                 @OrderBy nvarchar(200) = '',
                 @TopRow nvarchar(5) = '',    
                 @ExecutorUserId UNIQUEIDENTIFIER,
                 @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER = null

AS
/*
--
-- KÉSZÍTETTE:   Ádám András       2007.11.28
-- MÓDOSÍTOTTA:  Boda Eszter       2007.12.04
   --> csak futtatáskor érvényes csoporttagság figyelembe vétele
-- MÓDOSÍTOTTA:  Ádám András       2007.12.06
   --> új input paraméter a felhasználó (valamelyik) szervezetére,
       erre épül majd fel az hierarchia,
       ill. a visszaadott hierarchia-láncból kitörölni minden,
       'Fopolgármester%', 'Fojegyzo%', 'Aljegyzo%' névkezdetu szervezetet!
       (eddig csak akkor töröltem ezeket, ha pont ok alkották a felso 3 szintet ...) 
-- MÓDOSÍTOTTA:  Boda Eszter       2007.12.10
   --> a kezdo szervezetet nem töröljük, ha az ténylegesen szervezet
       (mert ehhez a szinthez is kell tudni pl. tárgyszavakat rendelni)
-- MÓDOSÍTOTTA:  Boda Eszter       2008.01.31
   --> nem kell egyértelmunek lennie e hierarchiának felfelé (egy szervezet
       tartozhat több másik alá)

-- FELADATA:
       Egy adott KRT_Csoportok -beli személyhez (tipus=1) vagy szervezethez (tipus=0),
       hierarchia szerint rendezve, visszaadja az összes felettes szervezetét (tipus=0)

-- pl:
select cs.Id, cs.Org, cs.Nev, cs.Tipus
  from dbo.KRT_Csoportok cs
 where cs.Tipus in ('1') -- '1'=személy, '0'=szervezet
 order by cs.Tipus,cs.Nev

exec [dbo].[sp_Szervezet_Hierarchia] '','','',
     'F43F1BB4-3D47-4186-BDB4-2281E7CA42EE', -- 'Agócsné Darai Terézia (Agocsne@budapest.hu)'
     null

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int
declare @rowcount        int
 
-- az olvasott KRT_Csoportok sor értékei
declare @Id              uniqueidentifier
declare @Org             uniqueidentifier
declare @Nev             nvarchar(400)
declare @Tipus           nvarchar(64)

declare @Szint           integer


--------------------------------
-- input paraméter ellenorzése
--------------------------------
-- input paraméterek kiiratása
if @teszt = 1
begin
   print '@ExecutorUserId          = '+ isnull( convert(varchar(50),@ExecutorUserId),          'NULL')
   print '@FelhasznaloSzervezet_Id = '+ isnull( convert(varchar(50),@FelhasznaloSzervezet_Id), 'NULL')
end
   
-- ha @FelhasznaloSzervezet_Id ki van töltve,  akkor erre szervezetre építjük a hierarchiát
-- ha @FelhasznaloSzervezet_Id nincs kitöltve, akkor pedig az @ExecutorUserId személyre ...

if @FelhasznaloSzervezet_Id is not NULL
begin 

   -- NULLITÁS-vizsgálat
   if @FelhasznaloSzervezet_Id is NULL
      RAISERROR('@FelhasznaloSzervezet_Id paraméter nem lehet NULL!',16,1)
   
   -- személy létezés ellenorzés
   select @Id = NULL,  @Org = NULL,   @Nev = NULL,   @Tipus = NULL     
   select @Id = cs.Id, @Org = cs.Org, @Nev = cs.Nev, @Tipus = cs.Tipus
     from dbo.KRT_Csoportok cs
    where cs.Id = @FelhasznaloSzervezet_Id
    
   if @@error <> 0
      RAISERROR('KRT_Csoportok select hiba!',16,1)
   
   -- a megadott személy paramétereinek kiiratása
   if @teszt = 1
      print '@Id      = '+ isnull( convert(varchar(50),@Id),  'NULL') + ', ' +
            '@Org     = '+ isnull( convert(varchar(50),@Org), 'NULL') + ', ' +
            '@Nev     = '+ isnull(@Nev, 'NULL') + ', ' +
            '@Tipus   = '+ isnull(@Tipus, 'NULL')
   
   if @Id is NULL
      RAISERROR('A megadott FelhasznaloSzervezet_Id szervezet nem létezik!',16,1)

end
else
begin

   -- NULLITÁS-vizsgálat
   if @ExecutorUserId is NULL
      RAISERROR('@ExecutorUserId paraméter nem lehet NULL!',16,1)
   
   -- személy létezés ellenorzés
   select @Id = NULL,  @Org = NULL,   @Nev = NULL,   @Tipus = NULL     
   select @Id = cs.Id, @Org = cs.Org, @Nev = cs.Nev, @Tipus = cs.Tipus
     from dbo.KRT_Csoportok cs
    where cs.Id = @ExecutorUserId
    
   if @@error <> 0
      RAISERROR('KRT_Csoportok select hiba!',16,1)
   
   -- a megadott személy paramétereinek kiiratása
   if @teszt = 1
      print '@Id      = '+ isnull( convert(varchar(50),@Id),  'NULL') + ', ' +
            '@Org     = '+ isnull( convert(varchar(50),@Org), 'NULL') + ', ' +
            '@Nev     = '+ isnull(@Nev, 'NULL') + ', ' +
            '@Tipus   = '+ isnull(@Tipus, 'NULL')
   
   if @Id is NULL
      RAISERROR('A megadott @ExecutorUserId személy nem létezik!',16,1)

end

-------------------------------------------------
-- a felettes szervezetek legyujtése munkatáblába
-------------------------------------------------
-- munkatábla kreálás
create table #Felettes_Szervezet
(
   Id                   uniqueidentifier     not null,
   Nev                  nvarchar(400)        null,
   Tipus                nvarchar(64)         null,
   Szint                integer              null
)

select @Szint = 1
-- a személy felírása munkatáblába (csak a teszt miatt)
insert into #Felettes_Szervezet
       (Id,  Nev,  Tipus,  Szint)      
values (@Id, @Nev, @Tipus, @Szint)      

if @@error <> 0
   RAISERROR('#Felettes_Szervezet insert hiba!',16,1)

-- a felettes szervezetek legyujtése (csak '0' típusú lehet!!!)
while 1=1
begin

   select @Id = cs.Id, @Nev = cs.Nev, @Tipus = cs.Tipus
     from dbo.KRT_CsoportTagok ct,
          dbo.KRT_Csoportok cs
    where ct.Csoport_Id_Jogalany = @Id
      and cs.Id                  = ct.Csoport_Id
      and cs.Tipus               = '0'
      and getdate() between ct.ErvKezd and ct.ErvVege

   select @error = @@error, @rowcount = @@rowcount 

   if @error <> 0
      RAISERROR('KRT_Csoportok & KRT_CsoportTagok select hiba!',16,1)

   --if @rowcount > 1
    --  RAISERROR('Nem egyértelmu a felettes szervezet!?',16,1)

   if @rowcount = 0
      break -- kiugrunk a ciklusból, mert nincs már több felettes ...

   select @Szint = @Szint + 1
   -- a következo felettes felírása a munkatáblába
   insert into #Felettes_Szervezet
          (Id,  Nev,  Tipus,  Szint)      
   values (@Id, @Nev, @Tipus, @Szint)      

   if @@error <> 0
      RAISERROR('#Felettes_Szervezet insert hiba!',16,1)

end

------------------------------------
-- eredmény visszaadás munkatáblából
------------------------------------
--
-- csak TESZT (még számozás megfordítás és org-visszaadás nélkül)
-- select * from #Felettes_Szervezet order by Szint
--

-- jelenleg 1 -tól @szint -ig vannak sorszámozva a sorok (összesen @szint darab)

-- a. elöször is fordítsuk meg a sorszámozást
update #Felettes_Szervezet
   set Szint = @szint - Szint + 1

-- b. az induló input szervezet törlése (most szint=@szint, mert már megfordítottuk a sorszámozást ...)
delete from #Felettes_Szervezet
 where Szint = @szint and not Tipus = '0'
 
-- c. minden 'Fopolgármester' & 'Fojegyzo' & 'Aljegyzo %'-szeru névvel megáldott szervezet törlése a hierarchiából
delete from #Felettes_Szervezet
 where 1=2
    OR Nev like 'Fopolgármester' 
    OR Nev like 'Fojegyzo' 
    OR Nev like 'Aljegyzo %' 

-- d. az eredeti input ORG-szervezetét betenni a 0.szintre!
insert into #Felettes_Szervezet
       (Id,  Nev,  Tipus,  Szint)      
select Id,   Nev,  Tipus,  0
  from dbo.KRT_Csoportok 
 where Tipus = '9'
   and Org   = @Org

select @error = @@error, @rowcount = @@rowcount 

if @error <> 0
   RAISERROR('#Felettes_Szervezet insert hiba!',16,1)

if @rowcount = 0
   RAISERROR('Az ORG-szervezet nem létezik!',16,1)

if @rowcount > 1
   RAISERROR('Az ORG-szervezet nem egyértelmuen azonosítható!?',16,1)

-- és most jön az eredmény visszaadása:
-- a törlések miatt nem lesz folytonos számozású a szint, de a hierchia sorrend az korrekt lesz!!!
-- (ha szükséges ITT megoldható a 0-tól kezdodo folytonos számozás is ...)
select *
  from #Felettes_Szervezet
 order by Szint


END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------