﻿create procedure [dbo].[sp_UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza]
	@UgyiratId UNIQUEIDENTIFIER,	
	@ExecutorUserId		UNIQUEIDENTIFIER,
	@Tranz_id UNIQUEIDENTIFIER = null,
	@IsVisszakuldes char(1) = 0 -- ha értéke 1, akkor a példányok továbbítás alatt maradnak és
	                            -- az elozo felelos lesz az új felelos, a jelenlegi pedig az elozo
as

BEGIN TRY
	set nocount ON
	
	declare @execTime datetime;
	set @execTime = getdate();

	-- Az ügyirat elsodleges iratpéldányainak Update-je:
	-- (Felelos és állapot visszaállítása)	
	
	 -- Módosítandó iratpéldányok id-jának meghatározása:
	 -- Csak azokat az iratpéldányokat kell, amik bent vannak az ügyiratban (ugyanaz az orzojük)
	 CREATE TABLE #pldIds(Id UNIQUEIDENTIFIER)
	 
	 INSERT INTO #pldIds SELECT [EREC_PldIratPeldanyok].[Id]
	 FROM [EREC_PldIratPeldanyok]
		JOIN [EREC_IraIratok] ON [EREC_PldIratPeldanyok].[IraIrat_Id] = [EREC_IraIratok].[Id]
		JOIN [EREC_UgyUgyiratdarabok] ON [EREC_IraIratok].[UgyUgyIratDarab_Id] = [EREC_UgyUgyiratdarabok].[Id]
		JOIN EREC_UgyUgyiratok ON [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] = EREC_UgyUgyiratok.Id
	 WHERE EREC_UgyUgyiratok.Id = @UgyiratId
	 AND [EREC_PldIratPeldanyok].[Allapot] = '50'
	 AND [EREC_UgyUgyiratok].FelhasznaloCsoport_Id_Orzo = [EREC_PldIratPeldanyok].FelhasznaloCsoport_Id_Orzo
	 

	IF @IsVisszakuldes = 1
	BEGIN
		UPDATE [EREC_PldIratPeldanyok]
		SET [Csoport_Id_Felelos] = IsNull([Csoport_Id_Felelos_Elozo], FelhasznaloCsoport_Id_Orzo)
			, [Csoport_Id_Felelos_Elozo] = [Csoport_Id_Felelos]
			, [Ver] = Ver + 1
			, [Modosito_id] = @ExecutorUserId
			, [ModositasIdo] = @execTime
			, [Tranz_id] = @Tranz_id
		WHERE [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM [#pldIds])
	END
	ELSE
	BEGIN
		UPDATE [EREC_PldIratPeldanyok]
		SET [Allapot] = [TovabbitasAlattAllapot]
			, [TovabbitasAlattAllapot] = NULL
			, [Csoport_Id_Felelos] = [Csoport_Id_Felelos_Elozo]
			, [Csoport_Id_Felelos_Elozo] = NULL
			, [Ver] = Ver + 1
			, [Modosito_id] = @ExecutorUserId
			, [ModositasIdo] = @execTime
			, [Tranz_id] = @Tranz_id
		WHERE [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM [#pldIds])		 
	END

	/*** EREC_PldIratpeldanyok history log ***/
--		declare iratpeldanyHistoryCursor cursor for 
--			select Id from #pldIds;
--		declare @iratpeldanyId uniqueidentifier;
--
--		open iratpeldanyHistoryCursor;
--		fetch next from iratpeldanyHistoryCursor into @iratpeldanyId;
--		while @@FETCH_STATUS = 0
--		BEGIN
--			exec sp_LogRecordToHistory 'EREC_PldIratpeldanyok',@iratpeldanyId
--						,'EREC_PldIratpeldanyokHistory',1,@ExecutorUserId,@execTime;
--			fetch next from iratpeldanyHistoryCursor into @iratpeldanyId;
--		END
--
--		CLOSE iratpeldanyHistoryCursor;
--		DEALLOCATE iratpeldanyHistoryCursor;

		declare @row_ids varchar(MAX)
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #pldIds FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec [sp_LogRecordsToHistory_Tomeges] 
				 @TableName = 'EREC_PldIratpeldanyok'
				,@Row_Ids = @row_ids
				,@Muvelet = 1
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @execTime
		
		-- iratpéldány mozgásának jelzése
		exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
		
		DROP TABLE [#pldIds]


END TRY
BEGIN CATCH

	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH