﻿CREATE PROCEDURE [dbo].[sp_UnlockRecord] 
		 @TableName NVARCHAR(100)
		,@Id uniqueidentifier
		,@ExecutorUserId uniqueidentifier	
		,@ExecutionTime datetime			
AS
BEGIN TRY
--BEGIN TRANSACTION UnLockTransaction
	set nocount on

	-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT	
	DECLARE @LastZarolo_id uniqueidentifier	
	exec sp_GetLockingInfo @TableName,@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,@LastZarolo_id OUTPUT

	if (@IsLockedByOtherUser = 0)
	BEGIN
		if (@LastZarolo_id is not null and @LastZarolo_id = @ExecutorUserId)
		begin 
		-- saját maga lockolja, lock feloldható
			DECLARE @sqlCommand NVARCHAR(1000)
			SET @sqlCommand = 
				'UPDATE '+@TableName+' SET Zarolo_id=null, ZarolasIdo = null '	
				+ ' WHERE Id = @Id'
			exec sp_executesql @sqlCommand, 
								 N'@Id uniqueidentifier'
								,@Id = @Id
			
			if @@rowcount != 1
			begin
				RAISERROR('[50001]',16,1)
			end
			else begin
				/* History Log */
			   DECLARE @HistoryTableName NVARCHAR(100)
			   SET @HistoryTableName = @TableName + 'History'

			   exec sp_LogRecordToHistory @TableName,@Id
					 ,@HistoryTableName,4,@ExecutorUserId,@ExecutionTime
			end	
		end
--		else
--		begin
--			-- nem lockolja senki, nem kell semmit csinálni
--		end


	END
	else BEGIN
		RAISERROR('[50098]',16,1)	
	END
	

--COMMIT TRANSACTION UnLockTransaction
   
END TRY
BEGIN CATCH
--   IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UnLockTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()	
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH