﻿--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_eBeadvanyCsatolmanyokLetoltese]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[sp_eBeadvanyCsatolmanyokLetoltese]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE PROCEDURE [dbo].[sp_eBeadvanyCsatolmanyokLetoltese]
	@eIntegratoWebSericeUrl nvarchar(4000),
	@outDirectory nvarchar(400),
	@manualWhere nvarchar(4000) = ''
AS
BEGIN
	declare @url nvarchar(4000)
    set @url = @eIntegratoWebSericeUrl

	IF RIGHT(@url, 1) != '\'
	BEGIN
		SET @url = @url + '\'
	END

	SET @url = @url  + 'eUzenetService.asmx/DownloadFileShareDokumentumok'

	SET @url = @url + '?destination=' + dbo.fn_UrlEncode(@outDirectory)
	SET @url = @url + '&filter=' + dbo.fn_UrlEncode(@manualWhere)

    exec sp_CallWebService @url
END