﻿				 
create procedure dbo.sp_VIEW_IrattariHelyLevelekGetAll
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

 

	SET @sqlcmd = @sqlcmd + 
  'select 
  	   VIEW_IrattariHelyLevelek.Id,
	   VIEW_IrattariHelyLevelek.Vonalkod,
	   VIEW_IrattariHelyLevelek.Ertek,
	   VIEW_IrattariHelyLevelek.IrattarTipus
   from 
     VIEW_IrattariHelyLevelek as VIEW_IrattariHelyLevelek      
	order by   VIEW_IrattariHelyLevelek.Ertek
   '

     
	SET @sqlcmd = @sqlcmd ;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end