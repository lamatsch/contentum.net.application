﻿create procedure [dbo].[sp_WF_AtmenetiIrattarKolcsonzesErtesito]    

AS
/*
-- FELADATA:
	Munkaanyag beiktatásához kapcsolódó értesítés adatainak beállítása.
    Kezelt funkciók: MunkapeldanyBeiktatas_Bejovo/MunkapeldanyBeiktatas_Belso
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000),
		@Targy NVARCHAR(4000),
		@Ugyirat_FelhasznaloCsoport_Id_Orzo UNIQUEIDENTIFIER,
		@UgyintezoId UNIQUEIDENTIFIER,
		@UgyintezoNev NVARCHAR(100)
	

DECLARE @Felelosok TABLE
(
  Csoport_Id_FelelosFelh UNIQUEIDENTIFIER,
  Csoport_Id_Felelos UNIQUEIDENTIFIER
)
		

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
--RAISERROR('Teszt!',16,1)

---------------------------
-- üzenet adatok beállítása
---------------------------
if @Funkcio_Kod_Kivalto = 'AtmenetiIrattarKolcsonzes'
begin

	-- Felelos objektum beállítása
	select @Ugyirat_FelhasznaloCsoport_Id_Orzo    = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
		   @ObjektumSzukites = EREC_UgyUgyiratok.Azonosito,
		   @Targy = EREC_UgyUgyiratok.Targy
	  from EREC_UgyUgyiratok EREC_UgyUgyiratok
	 where EREC_UgyUgyiratok.Id = @Obj_Id
	 
	 --felelosök beállítása
	 insert INTO @Felelosok (Csoport_Id_Felelos, Csoport_Id_FelelosFelh)
			SELECT DISTINCT @Ugyirat_FelhasznaloCsoport_Id_Orzo, KRT_Felhasznalok.Id FROM
			KRT_Funkciok 
			INNER JOIN KRT_Szerepkor_Funkcio
			ON KRT_Funkciok.Kod='IrattarKolcsonzesKiadasa' AND KRT_Funkciok.Id = KRT_Szerepkor_Funkcio.Funkcio_Id
			AND GETDATE() BETWEEN KRT_Szerepkor_Funkcio.ErvKezd AND KRT_Szerepkor_Funkcio.ErvVege
			INNER JOIN KRT_Szerepkorok
			ON KRT_Szerepkorok.Id = KRT_Szerepkor_Funkcio.Szerepkor_Id
			AND GETDATE() BETWEEN KRT_Szerepkorok.ErvKezd AND KRT_Szerepkorok.ErvVege
			INNER JOIN KRT_Felhasznalo_Szerepkor
			ON KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id
			AND GETDATE() BETWEEN KRT_Felhasznalo_Szerepkor.ErvKezd AND KRT_Felhasznalo_Szerepkor.ErvVege
			INNER JOIN KRT_Felhasznalok
			ON KRT_Felhasznalok.Id = KRT_Felhasznalo_Szerepkor.Felhasznalo_Id
			AND GETDATE() BETWEEN KRT_Felhasznalok.ErvKezd AND KRT_Felhasznalok.ErvVege
			INNER JOIN KRT_CsoportTagok
			ON KRT_CsoportTagok.Csoport_Id = @Ugyirat_FelhasznaloCsoport_Id_Orzo and  KRT_CsoportTagok.Csoport_Id_Jogalany = KRT_Felhasznalok.Id
			AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege

end -- üzenet adatok beállítása

--IrattarKolcsonzesKiadasa

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
	   @Obj_Id_Kezelendo,
	   f.Csoport_Id_Felelos,
	   f.Csoport_Id_FelelosFelh,
	   @FeladatHatarido,
	   @Leiras,
	   '0', --@FeladatAllapot
	   @Feladat_Id,
	   @ObjektumSzukites,
	   @Targy,
	   @UgyintezoId,
	   @UgyintezoNev
FROM @Felelosok AS f	   

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------