﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_WF_BejovoIratIktatasErtesito')
--            and   type = 'P')
--   drop procedure sp_WF_BejovoIratIktatasErtesito
--go

create procedure [dbo].[sp_WF_BejovoIratIktatasErtesito]    

AS
/*
-- FELADATA:
	Bejövo irat iktatása (BejovoIratIktatas, BejovoIratIktatasCsakAlszamra) esetén értesítés küldése, 
	ha olyan iratot iktatnak, amely olyan küldeményhez kapcsolódik, aminek a címzettje "Fopolgármester"
	Az értesítendo felhasználó: EREC_FeladatDefinicio.Obj_Id_Felelos
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000),
		@Targy NVARCHAR(4000),
		@UgyintezoId UNIQUEIDENTIFIER,
		@UgyintezoNev NVARCHAR(100)
		

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
--RAISERROR('Teszt!',16,1)

---------------------------
-- üzenet adatok beállítása
---------------------------
if @Funkcio_Kod_Kivalto = 'BejovoIratIktatas' OR @Funkcio_Kod_Kivalto = 'BejovoIratIktatasCsakAlszamra'
begin

	DECLARE @kuldemenyCimzett NVARCHAR(400)
	SET @kuldemenyCimzett ='Főpolgármester'
	
	DECLARE @kuldemenyAzonosito NVARCHAR(100)
	DECLARE @kuldemenyTargy NVARCHAR(4000)
	DECLARE @kuldemenyId UNIQUEIDENTIFIER
	DECLARE @kuldemenyBeerkezesIdeje DATETIME
	
	SELECT @kuldemenyId = EREC_KuldKuldemenyek.Id,
	       @kuldemenyAzonosito =  EREC_KuldKuldemenyek.Azonosito,
		   @kuldemenyTargy = EREC_KuldKuldemenyek.Targy,
		   @kuldemenyBeerkezesIdeje = EREC_KuldKuldemenyek.BeerkezesIdeje
		  FROM EREC_IraIratok EREC_IraIratok
		  INNER JOIN EREC_KuldKuldemenyek EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
		  INNER JOIN KRT_Csoportok KRT_Csoportok ON EREC_KuldKuldemenyek.Csoport_Id_Cimzett = KRT_Csoportok.Id
		  AND KRT_Csoportok.Nev = @kuldemenyCimzett
		  WHERE EREC_IraIratok.Id = @Obj_Id
		  
	IF @kuldemenyId IS NOT NULL
	BEGIN
		
		DECLARE @iratId UNIQUEIDENTIFIER
		DECLARE @iratAzonosito NVARCHAR(100)
		DECLARE @iratTargy NVARCHAR(4000)
		DECLARE @iratUgyintezoId UNIQUEIDENTIFIER
		DECLARE @ugyiratId UNIQUEIDENTIFIER
		
	    SELECT  @iratId = EREC_IraIratok.Id,
	            @iratAzonosito = EREC_IraIratok.Azonosito,
				@iratTargy = EREC_IraIratok.Targy,
				@iratUgyintezoId = EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez,
				@ugyiratId = EREC_IraIratok.Ugyirat_Id
	    FROM EREC_IraIratok EREC_IraIratok
	    WHERE EREC_IraIratok.Id = @Obj_Id
	    
	    --Leirás összeállítása
	    SET @Leiras = REPLACE(@Leiras,'{0}', convert(varchar, @kuldemenyBeerkezesIdeje, 102))
	    SET @Leiras = REPLACE(@Leiras,'{1}', @kuldemenyAzonosito)
	    SET @Leiras = REPLACE(@Leiras,'{2}', @iratAzonosito)
	    
	    --Irat iktatószáma
	    SET @ObjektumSzukites = @iratAzonosito
	    --Irat tárgya
		SET @Targy = @iratTargy
		SET @Obj_Id_Kezelendo = @iratId
		
		--Ügyintézo meghatározása (ha az iraté üres, az ügyirat ügyintézojét kérjük le)
	    IF @iratUgyintezoId IS NOT NULL
	    BEGIN
			SET @UgyintezoId = @iratUgyintezoId
		END
		ELSE
		BEGIN
			SELECT @UgyintezoId = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
			FROM EREC_UgyUgyiratok EREC_UgyUgyiratok
			WHERE EREC_UgyUgyiratok.Id = @ugyiratId
		END
		
		IF @UgyintezoId IS NOT NULL
		BEGIN
			SELECT @UgyintezoNev = KRT_Csoportok.Nev
			FROM KRT_Csoportok
			WHERE KRT_Csoportok.Id = @UgyintezoId
		END
	    
	    ---------------------------
		--------- kimenet ---------
		---------------------------
		select @FelelosObj_Id,
			   @BazisObj_Id,
	           @Bazisido,
	           @AtfutasiIdoMin,
	           @Obj_Id_Kezelendo,
	           @Csoport_Id_Felelos,
	           @Csoport_Id_FelelosFelh,
	           @FeladatHatarido,
	           @Leiras,
	           '0', --@FeladatAllapot
	           @Feladat_Id,
	           @ObjektumSzukites,
	           @Targy,
	           @UgyintezoId,
		       @UgyintezoNev
	   	
	END

end -- üzenet adatok beállítása

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------