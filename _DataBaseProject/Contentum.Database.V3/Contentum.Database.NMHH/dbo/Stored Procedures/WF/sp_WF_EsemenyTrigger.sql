﻿
create procedure [dbo].[sp_WF_EsemenyTrigger]    
                 @Esemeny_Id        uniqueidentifier,  -- az esemény azonosítója
                 @Obj_Id            uniqueidentifier,  -- az eseményhez tartozó objektum azonosítója
                 @Funkcio_Id        uniqueidentifier,  -- eseményt kiváltó funkció azonosítója
				 @FeladatSorszam    smallint = 1,      -- a feladatdefiníció sorszáma
				 @GetDefinicio      char(1) = 'N'      -- feladadefiníció lekérdezés
AS
--RAISERROR('Teszt!',16,1)
/*
-- FELADATA:
	Adott esemény bekövetkezésekor a kiváltó funkcióhoz tartozó, adott sorszámú feladatdefiníció
    szerinti WF program elindítása (tranzakción belül).

-- Példák:
--Eseményvezérelt feladatkezelés:
declare @Esemeny_Id         uniqueidentifier,
	    @Obj_Id             uniqueidentifier,
	    @Funkcio_Id         uniqueidentifier,
		@FeladatSorszam     smallint
select @Esemeny_Id = e.Id, @Obj_Id = e.Obj_Id, @Funkcio_Id = e.Funkcio_Id
  from KRT_Esemenyek e, KRT_Funkciok f, EREC_FeladatDefinicio d
  where e.Funkcio_Id = f.Id and f.Id = d.Funkcio_Id_Kivalto
    and e.Id = 'D9F36B2D-A23F-DE11-A5A5-0015AF225A5A'
--select @Esemeny_Id
exec [dbo].[sp_WF_EsemenyTrigger] 
     @Esemeny_Id, @Obj_Id, @Funkcio_Id, 1

----- Feladatkezelés -----
-- Ügyirat szignálás :   '83A40ADE-9C9A-DD11-8B9C-005056C00008' (ContentumDev)
-- Irat aláírás      :   'A58DB313-16B6-DD11-ACF3-005056C00008' (ContentumDev) - metás
-- Aláírás teljesítés:   '0A49934B-B9B6-DD11-ACF3-005056C00008' (ContentumDev)
D9F36B2D-A23F-DE11-A5A5-0015AF225A5A
----- Esemény értesítés -----
-- Munkaanyag iktatás 'F18CE3A3-CBF2-DD11-80EC-005056C00008'

-- Ütemezett ellenőrzéshez üzenetgenerálás:
declare @Funkcio_Id         uniqueidentifier
select @Funkcio_Id = Id
  from KRT_Funkciok where Kod = 'LejartAtvetelek'
exec [dbo].[sp_WF_EsemenyTrigger] 
     NULL, NULL, @Funkcio_Id, 1
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0

declare @error           int,
        @rowcount        int

declare @sqlcmd                 nvarchar(2000),
        @FeladatDefinicio_Id    uniqueidentifier,
		@Funkcio_Kod_Kivalto    nvarchar(100),
		@Funkcio_Kod_Futtatando nvarchar(100),
        @MuveletDefinicio       nvarchar(64),
        @SpecMuveletDefinicio   nvarchar(64),
        @FeladatDefinicioTipus  nvarchar(64)

declare @ObjTip_Id              uniqueidentifier,
		@Obj_Metadefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@MetaObj_Id             uniqueidentifier,
		@TablaNev               nvarchar(100),
		@MetaOszlopNev          nvarchar(100),
		@ColumnValue            nvarchar(100),
        @CountTargyszo          int,
		@MetadefCount			int

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_EsemenyTrigger -----'
   print '@Esemeny_Id     = '+ isnull(convert(varchar(50),@Esemeny_Id),    'NULL')
   print '@Obj_Id         = '+ isnull(convert(varchar(50),@Obj_Id),        'NULL')
   print '@Funkcio_Id     = '+ isnull(convert(varchar(50),@Funkcio_Id),    'NULL')
   print '@FeladatSorszam = '+ isnull(convert(varchar(50),@FeladatSorszam),'NULL')
   print '@GetDefinicio   = '+ isnull(convert(varchar(50),@GetDefinicio),  'NULL')
end

-------------------------------
-- input paraméterek ellenőrése
-------------------------------
if @Esemeny_Id is not NULL AND (@Obj_Id is NULL OR @Funkcio_Id is NULL)
   RAISERROR('Hibás eseménykezelő paraméterezés!',16,1)

-------------------------------------------
-- feladatdefiníciós metaadatok lekérdezése
-------------------------------------------
set @Obj_Metadefinicio_Id = convert(uniqueidentifier,NULL)

select @MetadefCount = count(*)
  from EREC_FeladatDefinicio
 where Funkcio_Id_Kivalto = @Funkcio_Id
   and FeladatDefinicioTipus in('2','3','4')
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

if @MetadefCount > 0
begin

	-------------------------------------
	-- releváns metadefiníció lekérdezése
	------------------------------------
	create table #MetaDefinicio
		   (Obj_Metadefinicio_Id uniqueidentifier,
			MetaObj_Id           uniqueidentifier)

	select @ObjTip_Id  = ObjTip_Id
	  from KRT_Esemenyek
	 where Id = @Esemeny_Id

	exec [dbo].[sp_WF_GetFeladatMetaDefinicio] 
		 @ObjTip_Id, @Obj_Id, @Funkcio_Id

	select @Obj_Metadefinicio_Id = Obj_Metadefinicio_Id,
		   @MetaObj_Id           = MetaObj_Id
	 from #MetaDefinicio

	------------------------------------
	-- metadefiníciós adatok lekérdezése
	------------------------------------
	select @ObjStateValue_Id = convert(uniqueidentifier,NULL),
		   @WorkFlow_Id      = convert(uniqueidentifier,NULL)

	select @ObjStateValue_Id     = ObjStateValue_Id,
		   @WorkFlow_Id          = WorkFlow_Id
	 from dbo.fn_WF_GetObjMetaAllapot(@Obj_Metadefinicio_Id, @MetaObj_Id)

--	print '------ fn_WF_GetObjMetaAllapot hívás ------'
--	print '@Obj_Metadefinicio_Id = '+ isnull(convert(varchar(50),@Obj_Metadefinicio_Id),'NULL')
--	print '@MetaObj_Id           = '+ isnull(convert(varchar(50),@MetaObj_Id),          'NULL')

end -- feladatdefiníciós metaadatok lekérdezése

-- RAISERROR('Teszt!',16,1)

---------------------------------
-- a feladatdefiníció lekérdezése
---------------------------------
select @FeladatDefinicio_Id    = fd.Id,
	   @FeladatDefinicioTipus  = fd.FeladatDefinicioTipus,
	   @Funkcio_Kod_Futtatando = ff.Kod,
	   @MuveletDefinicio       = fd.MuveletDefinicio,
	   @SpecMuveletDefinicio   = fd.SpecMuveletDefinicio
  from EREC_FeladatDefinicio fd,
	   KRT_Funkciok ff
 where fd.Funkcio_Id_Kivalto = @Funkcio_Id
   and fd.FeladatSorszam     = @FeladatSorszam
   and dbo.fn_Ervenyes(fd.ErvKezd,fd.ErvVege)='I'
   and fd.Funkcio_Id_Futtatando = ff.Id
   and dbo.fn_Ervenyes(ff.ErvKezd,ff.ErvVege)='I'
   and isnull(fd.Obj_Metadefinicio_Id,fd.Id) =
			 case when @Obj_Metadefinicio_Id is NULL then fd.Id
				  when @ObjStateValue_Id is NULL then fd.Id
				  else @Obj_Metadefinicio_Id end
   and isnull(fd.ObjStateValue_Id,fd.Id) =
			 case when @Obj_Metadefinicio_Id is NULL then fd.Id
				  when @ObjStateValue_Id is NULL then fd.Id
				  else @ObjStateValue_Id END		  

if @@rowcount = 0
   RAISERROR('Érvénytelen feladatdefiníció!',16,1)

---------------------------------------
-- az eseménytől függő program indítása
---------------------------------------
if @FeladatDefinicioTipus = '5'
	-- határidő ellenőrzés
	set @sqlcmd = 'exec [dbo].[' + @MuveletDefinicio+'] '
				  + 'NULL,' --@Esemeny_Id
				  + 'NULL,' --@Obj_Id
				  + 'NULL,' --@WorkFlow_Id 
				  + '''' + convert(varchar(50),@FeladatDefinicio_Id) + ''','''
				  + @SpecMuveletDefinicio + ''','''
				  + @Funkcio_Kod_Futtatando + ''','
				  + '''' + @GetDefinicio + ''''
else
	-- eseményvezérelt feladatkezelés
	set @sqlcmd = 'exec [dbo].[' + @MuveletDefinicio +']'''
				  + convert(varchar(50),@Esemeny_Id) + ''','''
				  + convert(varchar(50),@Obj_Id) + ''','
				  + case when @WorkFlow_Id is not NULL
						 then '''' + convert(varchar(50),@WorkFlow_Id) + ''','''
						 else 'NULL,''' end
				  + convert(varchar(50),@FeladatDefinicio_Id) + ''','''
				  + @SpecMuveletDefinicio + ''','''
				  + @Funkcio_Kod_Futtatando + ''','
				  + '''' + @GetDefinicio + ''''

--print (isnull(@sqlcmd,'Üres'))
--print '------ eseménytől függő program indítása ------'
--print '@FeladatDefinicioTipus  = '+ isnull(convert(varchar(50),@FeladatDefinicioTipus),'NULL')
--
--print '@MuveletDefinicio       = '+ isnull(convert(varchar(50),@MuveletDefinicio),'NULL')
--print '@Esemeny_Id             = '+ isnull(convert(varchar(50),@Esemeny_Id),'NULL')
--print '@Obj_Id                 = '+ isnull(convert(varchar(50),@Obj_Id),'NULL')
--print '@WorkFlow_Id            = '+ isnull(convert(varchar(50),@WorkFlow_Id),'NULL')
--print '@FeladatDefinicio_Id    = '+ isnull(convert(varchar(50),@FeladatDefinicio_Id),'NULL')
--print '@SpecMuveletDefinicio   = '+ isnull(convert(varchar(50),@SpecMuveletDefinicio),'NULL')
--print '@Funkcio_Kod_Futtatando = '+ isnull(convert(varchar(50),@Funkcio_Kod_Futtatando),'NULL')
--RAISERROR('Teszt!',16,1)
exec (@sqlcmd)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------