﻿
create procedure [dbo].[sp_WF_FeladatKezelo]    
                 @Esemeny_Id             uniqueidentifier, -- az esemény azonosítója
                 @Obj_Id                 uniqueidentifier, -- az eseményhez tartozó objektum azonosítója
				 @WorkFlow_Id            uniqueidentifier, -- a bázisobjektum workflow azonosítója
                 @FeladatDefinicio_Id    uniqueidentifier, -- a feladatdefiníció azonosítója
				 @SpecMuveletDefinicio   nvarchar(100),    -- a futtatandó speciális program neve
				 @Funkcio_Kod_Futtatando nvarchar(100),    -- a futtatandó funkció kódja
				 @GetDefinicio           char(1) = 'N'     -- a definiciós adatok lekérdezése
AS
/*
-- FELADATA:
	Határidős feladatok, értesítések kezelése, a definíciótól függő program hívása.

-- Példa:
declare @Esemeny_Id             uniqueidentifier,
		@Obj_Id                 uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicio_Id    uniqueidentifier,
        @SpecMuveletDefinicio   nvarchar(100),
        @Funkcio_Kod_Futtatando nvarchar(100)
select @Esemeny_Id = e.Id, @Obj_Id = e.Obj_Id, @FeladatDefinicio_Id = d.Id, 
       @SpecMuveletDefinicio = d.SpecMuveletDefinicio,@Funkcio_Kod_Futtatando = ff.Kod
  from KRT_Esemenyek e, KRT_Funkciok f, EREC_FeladatDefinicio d, KRT_Funkciok ff
  where e.Funkcio_Id = f.Id and f.Id = d.Funkcio_Id_Kivalto
    and d.FeladatDefinicioTipus = '1' and f.Kod Like 'UgyiratSzignalas'
    and ff.Id = d.Funkcio_Id_Futtatando
    and e.Id = '83A40ADE-9C9A-DD11-8B9C-005056C00008'
exec [dbo].[sp_WF_FeladatKezelo] 
     @Esemeny_Id, @Obj_Id, @WorkFlow_Id, @FeladatDefinicio_Id,
     @SpecMuveletDefinicio, @Funkcio_Kod_Futtatando

*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Obj_MetaDefinicio_Id   uniqueidentifier,
		@Funkcio_Kod_Kivalto    nvarchar(100),
		@FeladatJelzo           nvarchar(64),
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
		@FelelosTipus           nvarchar(64),
		@FelelosObj_Id          uniqueidentifier,
		@BazisObj_Id            uniqueidentifier,
		@Obj_Id_Felelos         uniqueidentifier,
		@Csoport_Id_Felelos     uniqueidentifier,
		@Csoport_Id_FelelosFelh uniqueidentifier,
        @Idobazis               nvarchar(64),
        @DateCol_Id             uniqueidentifier,
		@Leiras                 nvarchar(400),
        @Allapot                nvarchar(64),
        @ObjTip_Id              uniqueidentifier,
        @Obj_type               nvarchar(100),
		@ObjektumSzukites       nvarchar(2000)

declare @sqlcmd                 nvarchar(2000),
		@Felhasznalo_Id_kiado   uniqueidentifier,
		@Kezdesido              datetime,
		@Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@FeladatHatarido        datetime

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_FeladatKezelo -----'
   print '@Esemeny_Id             = '+ isnull(convert(varchar(50),@Esemeny_Id),            'NULL')
   print '@Obj_Id                 = '+ isnull(convert(varchar(50),@Obj_Id),                'NULL')
   print '@WorkFlow_Id            = '+ isnull(convert(varchar(50),@WorkFlow_Id),           'NULL')
   print '@FeladatDefinicio_Id    = '+ isnull(convert(varchar(50),@FeladatDefinicio_Id),   'NULL')
   print '@SpecMuveletDefinicio   = '+ isnull(convert(varchar(50),@SpecMuveletDefinicio),  'NULL')
   print '@Funkcio_Kod_Futtatando = '+ isnull(convert(varchar(50),@Funkcio_Kod_Futtatando),'NULL')
   print '@GetDefinicio           = '+ isnull(convert(varchar(50),@GetDefinicio),          'NULL')
end

--------------------------
-- kezdeti adatbeállítások
--------------------------
if @Esemeny_Id is not NULL
	select @BazisObj_Id   = @Obj_Id,
		   @FelelosObj_Id = @Obj_Id,
		   @Kezdesido     = ErvKezd
	  from KRT_Esemenyek
	 where Id = @Esemeny_Id

--------------------
-- segéd munkatáblák
--------------------
create table #Hatarido
       (Bazisido       datetime,
        Hatarido       datetime)

create table #Felelos
       (Csoport_Id_Felelos     uniqueidentifier,
        Csoport_Id_FelelosFelh uniqueidentifier)

--------------------------------------
-- határidős tételösszesítő munkatábla
--------------------------------------
create table #HataridosTetelek
       (Csoport_Id_Felelos     uniqueidentifier,
        Csoport_Id_FelelosFelh uniqueidentifier,
		LeirasKieg             nvarchar(100))

--------------------------------
-- speciális adatok munkatáblája
--------------------------------
create table #HataridosFeladatSpec
(
   Azonosito                smallint default 2,
   Funkcio_Kod_Kivalto      nvarchar(100),     -- kiváltó esemény
   Obj_MetaDefinicio_Id     uniqueidentifier,  -- objektum metadefiníció
   ObjStateValue_Id         uniqueidentifier,  -- objektum állapotdefiníció
   WorkFlow_Id              uniqueidentifier,  -- objektum workflow definíció
   FeladatDefinicioTipus	nvarchar(64),      -- határidős feladat típusa
   FelelosObj_Id            uniqueidentifier,  -- a felelős objektumazonosítója
   BazisObj_Id              uniqueidentifier,  -- a bázis objektumazonosító
   BazisIdo                 datetime,          -- bázisidő
   AtfutasiIdoMin           numeric(8),        -- áfutási idő percben
   Obj_Id                   uniqueidentifier,  -- érintett/kezelendő objektum*
   Csoport_Id_Felelos       uniqueidentifier,  -- felelős szervezete*
   Csoport_Id_FelelosFelh   uniqueidentifier,  -- felelős*
   FeladatHatarido          datetime,          -- határidő*
   Leiras					nvarchar(400),     -- határidős feladat leírás*
   Allapot					nvarchar(64),      -- határidős feladat állapot*
   Feladat_Id               uniqueidentifier,  -- határidős feladat azonosító**
   ObjTip_Id                uniqueidentifier,  -- az érintett objektumtípus azonosítója
   Obj_type                 nvarchar(100),     -- az érintett objektumtípus
   ReferenciaIdo			datetime,		   -- az ellenőrzés referencia időpontja
   ObjektumSzukites         nvarchar(2000),    -- a kimenő adatok halmazát szűkítő sql feltételek
   Targy                    NVARCHAR(4000),    -- az érintett objektum tárgya (munkapéldány beiktatása esetén használjuk)
   UgyintezoId				UNIQUEIDENTIFIER,   -- az érintett irat vagy ügyirat ügyintézőjének id-ja (bejövő irat iktatása esetén használjuk)
   UgyintezoNev				NVARCHAR(100)      -- az érintett irat vagy ügyirat ügyintézőjének neve (bejövő irat iktatása esetén használjuk)
 )
---------------------------------------
-- feladatdefiníciós adatok lekérdezése
---------------------------------------
select @Obj_MetaDefinicio_Id   = fd.Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = fd.ObjStateValue_Id,
	   @FeladatDefinicioTipus  = fd.FeladatDefinicioTipus,
	   @FelelosTipus           = fd.FelelosTipus,
	   @Obj_Id_Felelos         = fd.Obj_Id_Felelos,
	   @Idobazis               = fd.Idobazis,
       @DateCol_Id             = fd.ObjTip_Id_DateCol,
	   @AtfutasiIdoMin         = fd.AtfutasiIdo*convert(int,fd.Idoegyseg),
	   @Leiras                 = fd.FeladatLeiras,
	   @Allapot                = fd.Allapot,
	   @ObjTip_Id              = fd.ObjTip_Id,
	   @Obj_type               = fd.Obj_type,
	   @Funkcio_Kod_Kivalto    = fn.Kod,
	   @FeladatJelzo           = fn.FeladatJelzo
  from EREC_FeladatDefinicio fd,
	   KRT_Funkciok fn
 where fd.Id = @FeladatDefinicio_Id
   and fn.Id = fd.Funkcio_Id_Kivalto

------------------------
-- speciális feldolgozás
------------------------
if @FeladatJelzo = '1' or @GetDefinicio = 'I'
begin

	---------------------------------------
	-- definíció szerinti felelős beállítás
	---------------------------------------
	if @FelelosTipus = '1'
		select @Csoport_Id_Felelos     = Csoport_Id_Felelos,
			   @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh
		  from dbo.fn_WF_GetFelelos(@Obj_Id_Felelos, NULL)

	-----------------------------------------
	-- definíció szerinti határidő beállítása
	-----------------------------------------
	if @Idobazis in('1','2')
	begin
		exec [dbo].[sp_WF_GetHatarido] 
			 @Idobazis, NULL, NULL, NULL, @AtfutasiIdoMin

		select @Bazisido        = Bazisido,
			   @FeladatHatarido = Hatarido
		  from #Hatarido
	end -- feladatdefiníció szerinti határidő

	-----------------------------------------
	-- definíció szerinti beállítások mentése
	-----------------------------------------
	insert #HataridosFeladatSpec
		   (Azonosito, Funkcio_Kod_Kivalto, Obj_MetaDefinicio_Id, ObjStateValue_Id, WorkFlow_Id,
			FeladatDefinicioTipus, FelelosObj_Id, BazisObj_Id, BazisIdo, AtfutasiIdoMin, Obj_Id,
			Csoport_Id_Felelos, Csoport_Id_FelelosFelh, FeladatHatarido, Leiras, Allapot,
			ObjektumSzukites)
	select 1, @Funkcio_Kod_Kivalto, @Obj_MetaDefinicio_Id, @ObjStateValue_Id, @WorkFlow_Id,
		   @FeladatDefinicioTipus, @FelelosObj_Id, @BazisObj_Id, @Bazisido, @AtfutasiIdoMin, @Obj_Id,
		   @Csoport_Id_Felelos, @Csoport_Id_FelelosFelh, @FeladatHatarido, @Leiras, @Allapot,
		   @ObjektumSzukites

	-- átadott paraméterek kiírása
	if @teszt = 1
	begin
	   print '----- '+isnull(@SpecMuveletDefinicio,'NincsSpecMuvelet')+' -----'
	   print '@Funkcio_Kod_Kivalto    = '+ isnull(convert(varchar(50),@Funkcio_Kod_Kivalto),   'NULL')
	   print '@Obj_MetaDefinicio_Id   = '+ isnull(convert(varchar(50),@Obj_MetaDefinicio_Id),  'NULL')
	   print '@ObjStateValue_Id       = '+ isnull(convert(varchar(50),@ObjStateValue_Id),      'NULL')
	   print '@WorkFlow_Id            = '+ isnull(convert(varchar(50),@WorkFlow_Id),           'NULL')
	   print '@FeladatDefinicioTipus  = '+ isnull(convert(varchar(50),@FeladatDefinicioTipus), 'NULL')
	   print '@FelelosObj_Id          = '+ isnull(convert(varchar(50),@FelelosObj_Id),         'NULL')
	   print '@BazisObj_Id            = '+ isnull(convert(varchar(50),@BazisObj_Id),           'NULL')
	   print '@Bazisido               = '+ isnull(convert(varchar(50),@Bazisido),              'NULL')
	   print '@AtfutasiIdoMin         = '+ isnull(convert(varchar(50),@AtfutasiIdoMin),        'NULL')
	   print '@Obj_Id                 = '+ isnull(convert(varchar(50),@Obj_Id),                'NULL')
	   print '@Csoport_Id_Felelos     = '+ isnull(convert(varchar(50),@Csoport_Id_Felelos),    'NULL')
	   print '@Csoport_Id_FelelosFelh = '+ isnull(convert(varchar(50),@Csoport_Id_FelelosFelh),'NULL')
	   print '@FeladatHatarido        = '+ isnull(convert(varchar(50),@FeladatHatarido),       'NULL')
	   print '@Leiras                 = '+ isnull(convert(varchar(50),@Leiras),                'NULL')
	   print '@Allapot                = '+ isnull(convert(varchar(50),@Allapot),               'NULL')
	   print '@ObjektumSzukites       = '+ isnull(convert(varchar(50),@ObjektumSzukites),      'NULL')
	end
--RAISERROR('Egyedi eseménykezelés paraméterek!',16,1) -- teszt flag!

	--------------------------------
	-- eseményfüggő program indítása
	--------------------------------
	if @SpecMuveletDefinicio is not NULL and @GetDefinicio = 'N'
	begin
		--Munkaanyag beiktatasa esetén tárgy is kell
		IF @SpecMuveletDefinicio = 'sp_WF_MunkaanyagIktatasErtesito' OR @SpecMuveletDefinicio = 'sp_WF_AtmenetiIrattarKolcsonzesErtesito'
		OR @SpecMuveletDefinicio = 'sp_WF_BejovoIratIktatasErtesito' OR @SpecMuveletDefinicio = 'sp_WF_KozpontiIrattarKolcsonzesErtesito'
		BEGIN
		set @sqlcmd = 'insert #HataridosFeladatSpec
					  (FelelosObj_Id, BazisObj_Id, Bazisido, AtfutasiIdoMin, Obj_Id, ' +
					  'Csoport_Id_Felelos, Csoport_Id_FelelosFelh, FeladatHatarido, ' +
					  'Leiras, Allapot, Feladat_Id, ObjektumSzukites,Targy, UgyintezoId, UgyintezoNev)' +
					  'exec [dbo].[' + @SpecMuveletDefinicio+']'
		END
		ELSE
		BEGIN
			set @sqlcmd = 'insert #HataridosFeladatSpec
					  (FelelosObj_Id, BazisObj_Id, Bazisido, AtfutasiIdoMin, Obj_Id, ' +
					  'Csoport_Id_Felelos, Csoport_Id_FelelosFelh, FeladatHatarido, ' +
					  'Leiras, Allapot, Feladat_Id, ObjektumSzukites)' +
					  'exec [dbo].[' + @SpecMuveletDefinicio+']'
		END
		
		exec (@sqlcmd)

	end -- eseményfüggő program indítása

end -- speciális feldolgozás

-------------------------------------
-- kimenet objektumfüggő kiegészítése
-------------------------------------
if @FeladatJelzo = '1' and @GetDefinicio = 'N'
begin

	---------------------------------------------
	-- táblaoszlopban definiált felelős beállítás
	---------------------------------------------

	-- táblaoszlopban definiált felelős
	if @Esemeny_Id is not NULL and @FelelosTipus = '2'
	begin
		select @FelelosObj_Id          = FelelosObj_Id,
			   @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh
		  from #HataridosFeladatSpec
		 where Azonosito = 2

		if @Csoport_Id_FelelosFelh is NULL
		begin
			exec [dbo].[sp_WF_GetFelelos]
				 @FelelosObj_Id, @Obj_Id_Felelos

			select @Csoport_Id_Felelos     = Csoport_Id_Felelos,
				   @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh
			  from #Felelos
		end
	end -- táblaoszlopban definiált felelős

	-----------------------------------------------
	-- táblaoszlopban definiált határidő beállítása
	-----------------------------------------------
	if @Esemeny_Id is not NULL and @Idobazis not in('1','2')
	begin

		select @BazisObj_Id     = BazisObj_Id,
			   @Bazisido        = Bazisido,
			   @AtfutasiIdoMin  = AtfutasiIdoMin,
			   @FeladatHatarido = FeladatHatarido
		  from #HataridosFeladatSpec
		 where Azonosito = 2

		-- feladatdefiníció szerinti határidő
		if @FeladatHatarido is NULL and
		   @Idobazis is not NULL
		begin
			exec [dbo].[sp_WF_GetHatarido] 
				 @Idobazis, @BazisObj_Id, @DateCol_Id, @Bazisido, @AtfutasiIdoMin

			select @Bazisido        = Bazisido,
				   @FeladatHatarido = Hatarido
			  from #Hatarido
		end -- feladatdefiníció szerinti határidő

		-- workflow szerinti határidő
		if @FeladatHatarido is null and
		   @FeladatDefinicioTipus = '4' -- Workflow állapotfüggő
		begin

			select @Idobazis       = Idobazis,
				   @DateCol_Id     = ObjTip_Id_DateCol,
				   @AtfutasiIdoMin = AtfutasiIdo*convert(int,Idoegyseg)
			  from EREC_WorkFlowFazis
			 where ObjStateValue_Id = @ObjStateValue_Id

			if @Idobazis is not NULL
			begin
				exec [dbo].[sp_WF_GetHatarido] 
					 @Idobazis, @BazisObj_Id, @DateCol_Id, @Bazisido, @AtfutasiIdoMin

				select @Bazisido        = Bazisido,
					   @FeladatHatarido = Hatarido
				  from #Hatarido
			end

		end -- workflow szerinti határidő

	end -- definíció szerinti határidő beállítása

end -- kimenet kiegészítése 

----------
-- kimenet
----------
-- Feladat/Értesítés adatok
if @GetDefinicio = 'N'
	select @Funkcio_Kod_Futtatando        as Funkcio_Futtatando,
		   case when @Funkcio_Kod_Futtatando = 'FeladatNew'
				then es.Id
				when @Funkcio_Kod_Futtatando = 'EsemenyErtesites'
				then fd.Funkcio_Id_Kivalto
				else NULL end             as Esemeny_Id_Kivalto,
		   case when @Funkcio_Kod_Futtatando = 'FeladatModify'
				then es.Id
				else NULL end             as Esemeny_Id_Lezaro,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then fd.Funkcio_Id_Inditando
				else NULL end             as Funkcio_Id_Inditando,
		   isnull(hfs.Csoport_Id_Felelos,@Csoport_Id_Felelos)
										  as Csoport_Id_Felelos,
		   isnull(hfs.Csoport_Id_FelelosFelh,@Csoport_Id_FelelosFelh)
										  as Felhasznalo_Id_Felelos,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then es.Felhasznalo_Id_User
				else NULL end             as Felhasznalo_Id_Kiado,
		   convert(uniqueidentifier,NULL) as HataridosFeladat_Id,
		   convert(uniqueidentifier,NULL) as HataridosFeladat_Id_Fo,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then 1
				else NULL end             as ReszfeladatSorszam,
		   case when @Funkcio_Kod_Futtatando = 'FeladatNew'
				then fd.Id
				else NULL end             as FeladatDefinicio_Id,
		   case when @Funkcio_Kod_Futtatando = 'FeladatModify'
				then fd.Id
				else NULL end             as FeladatDefinicio_Id_Lezaro,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then '1'                         -- Automatikus
				else NULL end             as Forras,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then fd.Tipus
				else NULL end             as Tipus,
		   hfs.Leiras,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then convert(int,fd.Prioritas)
				else convert(int,NULL) end as Prioritas,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then convert(int,fd.LezarasPrioritas)
				else convert(int,NULL) end as LezarasPrioritas,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then isnull(@Kezdesido,@Bazisido)
				else NULL end             as KezdesiIdo,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then hfs.FeladatHatarido
				else NULL end             as IntezkHatarido,
		   hfs.Obj_Id,
		   hfs.Allapot,
		   hfs.Feladat_Id,
		   fd.ObjTip_Id,
		   fd.Obj_type,
		   convert(varchar(40),fd.Funkcio_Id_Kivalto) as str_Funkcio_Id_Kivalto,
		   convert(varchar(40),isnull(hfs.Csoport_Id_FelelosFelh,@Csoport_Id_FelelosFelh)) as str_Csoport_Id_Felelos,
		   hfs.ObjektumSzukites,
		   hfs.Targy,
		   hfs.UgyintezoId,
		   hfs.UgyintezoNev
	  from #HataridosFeladatSpec hfs
		   inner join EREC_FeladatDefinicio fd on fd.Id = @FeladatDefinicio_Id
		   left outer join KRT_Esemenyek es on es.Id = @Esemeny_Id
	 where hfs.Azonosito >= 2
	   and hfs.Allapot is not null
	   and (isnull(hfs.Csoport_Id_FelelosFelh,@Csoport_Id_FelelosFelh) is not null OR
	        @Funkcio_Kod_Futtatando = 'FeladatModify')

-- Feladatdefiníciós adatok
else
	select fd.Funkcio_Id_Inditando,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then isnull(@Kezdesido,@Bazisido)
				else NULL end             as KezdesiIdo,
		   case when @Funkcio_Kod_Futtatando <> 'FeladatModify'
				then hfs.FeladatHatarido
				else NULL end             as IntezkHatarido,
		   fd.Obj_type
	  from #HataridosFeladatSpec hfs
		   inner join EREC_FeladatDefinicio fd on fd.Id = @FeladatDefinicio_Id

--RAISERROR('Kimenet!',16,1)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------