﻿
create procedure [dbo].[sp_WF_GetFeladatMetaDefinicio]
				 @ObjTip_Id   uniqueidentifier,  -- objektumtípus azonosító
				 @Obj_Id      uniqueidentifier,  -- objektum azonosító
                 @Funkcio_Id  uniqueidentifier   -- eseményt kiváltó funkció azonosítója
AS
/*
-- FELADATA:
	Az adott paramétereknek megfelelő, a feladatdefinícióban hivatkozott objektum metadefiníció
    lekérdezése.

-- Példa:
create table #MetaDefinicio(Obj_Metadefinicio_Id uniqueidentifier,MetaObj_Id uniqueidentifier)
--
declare @ObjTip_Id uniqueidentifier, @Obj_Id uniqueidentifier, @Funkcio_Id uniqueidentifier
select @ObjTip_Id = ObjTip_Id, @Obj_Id = Obj_Id, @Funkcio_Id = Funkcio_Id
  from KRT_Esemenyek where Id = '84EAD4A7-14B0-DD11-ACF3-005056C00008'
 --'61F79CA7-FDB9-DD11-ACF3-005056C00008'
exec [dbo].[sp_WF_GetFeladatMetaDefinicio] 
     @ObjTip_Id, @Obj_Id, @Funkcio_Id
select * from #MetaDefinicio
drop table #MetaDefinicio

-- 
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0

declare @error           int,
        @rowcount        int

declare @sqlcmd                 nvarchar(1000),
		@Dtable                 nvarchar(100),
		@Dcolumn                nvarchar(100),
        @MetaObjTip_Id          uniqueidentifier,
        @MetaObj_Id             uniqueidentifier,
        @ObjTip_Id_Column       uniqueidentifier,
		@ColumnValue            nvarchar(100),
        @Obj_Metadefinicio_Id   uniqueidentifier,
        @Result_Metadef_Id      uniqueidentifier

declare @TabMetaDefinicio Table
        (numrows     int)

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_GetFeladatMetaDefinicio -----'
   print '@ObjTip_Id  = '+ isnull(convert(varchar(50),@ObjTip_Id), 'NULL')
   print '@Obj_Id     = '+ isnull(convert(varchar(50),@Obj_Id),    'NULL')
   print '@Funkcio_Id = '+ isnull(convert(varchar(50),@Funkcio_Id),'NULL')
end

-------------------------
-- munkatábla létrehozása
-------------------------
create table #MetaParameterek
	   (MetaObjTip_Id uniqueidentifier,
		MetaObj_Id    uniqueidentifier,
		Szint         int)

----------------------------------
-- alapértelmezett adatbeállítások
----------------------------------
select @Dtable            = Kod,
	   @MetaObjTip_Id     = @ObjTip_Id,
	   @MetaObj_Id        = @Obj_Id,
	   @Result_Metadef_Id = convert(uniqueidentifier, NULL)
  from KRT_ObjTipusok
 where Id = @ObjTip_Id

----------------------------
-- egyedi adatbeállítások --
----------------------------
-- EREC_IratAlairok adatbeállításai
if @Dtable = 'EREC_IratAlairok'
	select @Dtable        = Kod,
		   @MetaObjTip_Id = ot.Id,
		   @MetaObj_Id    = ia.Obj_Id
	  from EREC_IratAlairok ia,
		   KRT_ObjTipusok ot
	 where ia.Id  = @Obj_Id
	   and ot.Kod = 'EREC_IraIratok'

-----------------------------
-- metadefiniciós paraméterek
-----------------------------
insert #MetaParameterek
values (@MetaObjTip_Id,
        @MetaObj_Id,
        case when @Dtable = 'EREC_UgyUgyiratok' then 1
			 when @Dtable = 'EREC_UgyUgyiratDarabok' then 2
			 when @Dtable = 'EREC_IraIratok' then 3
			 else 0 end
      )

if @Dtable = 'EREC_UgyUgyiratDarabok'
begin
	insert #MetaParameterek
	select ot.Id,
		   ud.UgyUgyirat_Id,
		   1 Szint
	  from EREC_UgyUgyiratDarabok ud, 
		   KRT_ObjTipusok ot
	 where ud.Id  = @MetaObj_Id
	   and ot.Kod = 'EREC_UgyUgyiratok'
end

if @Dtable = 'EREC_IraIratok'
begin
	insert #MetaParameterek
	select ot.Id,
		   ir.UgyUgyiratDarab_Id,
		   2 Szint
	  from EREC_IraIratok ir, 
		   KRT_ObjTipusok ot
	 where ir.Id  = @MetaObj_Id
	   and ot.Kod = 'EREC_UgyUgyiratDarabok'
	union
	select ot.Id,
		   ud.UgyUgyirat_Id,
		   1 Szint
	  from EREC_IraIratok ir,
		   EREC_UgyUgyiratDarabok ud,
		   KRT_ObjTipusok ot
	 where ir.Id  = @MetaObj_Id
	   and ud.id  = ir.UgyUgyiratDarab_Id
	   and ot.Kod = 'EREC_UgyUgyiratok'
end

------------------------------------
-- metadefiníciós adatok lekérdezése
------------------------------------
declare Metadef cursor local for
 select md.Id,
		md.ObjTip_Id_Column,
		md.ColumnValue,
		mp.MetaObj_Id
   from EREC_FeladatDefinicio fd,
		EREC_Obj_MetaDefinicio md,
		#MetaParameterek mp
  where fd.Funkcio_Id_Kivalto   = @Funkcio_Id
	and dbo.fn_Ervenyes(fd.ErvKezd,fd.ErvVege)='I'
	and fd.Obj_MetaDefinicio_Id = md.Id
	and md.ObjTip_Id            = MetaObjTip_Id
  order by mp.Szint desc, md.DefinicioTipus desc, fd.LetrehozasIdo

open Metadef
fetch Metadef
 into @Obj_Metadefinicio_Id, @ObjTip_Id_Column, @ColumnValue, @MetaObj_Id
while @@Fetch_Status = 0 AND @Result_Metadef_Id is NULL

	begin
		if (@@ERROR <> 0)
		   RAISERROR('Hiba a metaadatok lekérdezésekor!',16,1)

		select @Dtable   = tb.Kod,
			   @Dcolumn  = tc.Kod
		  from KRT_ObjTipusok tb,
			   KRT_ObjTipusok tc
		 where tc.Id = @ObjTip_Id_Column
		   and tc.Obj_Id_Szulo = tb.Id

		set @sqlcmd = 'select count(*) from ' + @Dtable +
					  ' where Id = ' + 
					  '''' + convert(varchar(50),@MetaObj_Id) + '''' +
					  ' and ' + @Dcolumn + ' = ' + 
					  '''' + convert(varchar(50),@ColumnValue) + ''''

		insert @TabMetaDefinicio
		exec (@sqlcmd)

		if exists(select 1 from @TabMetaDefinicio where numrows > 0)
				set @Result_Metadef_Id = @Obj_Metadefinicio_Id

		fetch Metadef
		 into @Obj_Metadefinicio_Id, @ObjTip_Id_Column, @ColumnValue, @MetaObj_Id

	end

close Metadef
deallocate Metadef

-- kimenet
delete #MetaDefinicio
insert #MetaDefinicio (Obj_Metadefinicio_Id, MetaObj_Id)
		values (@Result_Metadef_Id, @MetaObj_Id)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------