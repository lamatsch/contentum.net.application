﻿
create procedure [dbo].[sp_WF_GetWorkFlowAdat]
				 @ContentType   nvarchar(100),    -- a CTT azonosító kódja
                 @FazisErtek    nvarchar(64),     -- az aktuális workflow fázis értéke
                 @AllapotErtek  nvarchar(64),     -- az aktuális belső állapot értéke
                 @Kimenet		nvarchar(64),     -- a fázis vagy belső állapot kimenetének jelzője
                 @BazisObj_Id   uniqueidentifier  -- a határidőszámitás alapobjektum azonosítója
AS
/*
-- FELADATA:
	Adott CTT-hez kapcsolt workflow-nál a megadott fázis és belső állapot adatainak, vagy a
    megadott fázist követő fázisok adatainak lekérdezése.

-- Példa:
exec [dbo].[sp_WF_GetWorkFlowAdat] 
     'Kozgyulesi-Eloterjesztes-Ugy', NULL, NULL, NULL, NULL
exec [dbo].[sp_WF_GetWorkFlowAdat] 
     'Kozgyulesi-Eloterjesztes-Ugy', 'Nyilvántartásba vétel', NULL, NULL, NULL
exec [dbo].[sp_WF_GetWorkFlowAdat] 
     'Kozgyulesi-Eloterjesztes-Ugy','Nyilvántartásba vétel','Iktatott',NULL,'EBDB4E26-9E63-DD11-9DE3-000F1FFFAE55'
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @AktFazis_Id         uniqueidentifier,
		@NextFazis_Id        uniqueidentifier,
		@KezdIdobazis        nvarchar(64),
		@KezdDateCol_Id      uniqueidentifier,
		@Idobazis            nvarchar(64),
		@DateCol_Id          uniqueidentifier,
		@AtfutasiIdoMin      numeric(8),
		@Kezdesido		     datetime,
		@Hatarido		     datetime,
		@Kod	             nvarchar(64),
		@Tipus               nvarchar(64),
		@Ertek	             nvarchar(100),
		@AktKimenet	         nvarchar(64)

declare @AktBelso_Id         uniqueidentifier,
		@Belso_Id            uniqueidentifier,
		@BelsoKezdIdobazis   nvarchar(64),
		@BelsoKezdDateCol_Id uniqueidentifier,
		@BelsoIdobazis       nvarchar(64),
		@BelsoDateCol_Id     uniqueidentifier,
		@BelsoAtfutasiIdoMin numeric(8),
		@BelsoIdoegyseg	     nvarchar(64),
		@BelsoKezdesido	     datetime,
		@BelsoHatarido	     datetime,
		@BelsoKod	         nvarchar(64),
		@BelsoTipus          nvarchar(64),
		@BelsoErtek	         nvarchar(100),
		@BelsoOsszIdo        numeric(8)

declare @TabFazisAdat   Table
        (Fazis_Id       uniqueidentifier,
		 Tipus          nvarchar(64),
		 Kod	        nvarchar(64),
		 Ertek          nvarchar(100),
		 Kezdesido      datetime,
		 Hatarido       datetime,
		 Belso_Id       uniqueidentifier,
		 BelsoTipus     nvarchar(64),
		 BelsoKod	    nvarchar(64),
		 BelsoErtek     nvarchar(100),
		 BelsoKezdesido datetime,
		 BelsoHatarido  datetime,
		 Kimenet        nvarchar(64))

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_GetFazisAdat -----'
   print '@ContentType  = '+ isnull(convert(varchar(50),@ContentType), 'NULL')
   print '@FazisErtek   = '+ isnull(convert(varchar(50),@FazisErtek),  'NULL')
   print '@AllapotErtek = '+ isnull(convert(varchar(50),@AllapotErtek),'NULL')
   print '@Kimenet      = '+ isnull(convert(varchar(50),@Kimenet),     'NULL')
   print '@BazisObj_Id  = '+ isnull(convert(varchar(50),@BazisObj_Id), 'NULL')
end

-------------------------------
-- Aktuális állapot lekérdezése
-------------------------------
select @AktFazis_Id    = isnull(wt.Id,wf.Id),
	   @Ertek          = wf.Ertek,
	   @KezdIdobazis   = wf.KezdIdobazis,
	   @KezdDateCol_Id = wf.KezdObjTip_Id_DateCol,
	   @Idobazis       = wf.Idobazis,
	   @DateCol_Id     = wf.ObjTip_Id_DateCol,
	   @AtfutasiIdoMin = wf.AtfutasiIdo*convert(int,wf.Idoegyseg),
	   @BelsoErtek     = wt.Ertek
  from EREC_WorkFlowFazis wf
	   inner join EREC_WorkFlow ww on wf.WorkFlow_Id = ww.Id
	   inner join EREC_Obj_MetaDefinicio md on ww.Obj_MetaDefinicio_Id = md.Id
	   inner join (select Ertek from EREC_WorkFlowFazis
							   where Tipus = '00') ef on wf.Ertek = isnull(@FazisErtek,ef.Ertek)
	   left outer join (select Id, Fazis_Id, Ertek from EREC_WorkFlowFazis
						 where Tipus >= '10' and Ertek = isnull(@AllapotErtek,Ertek)) wt on wf.Id = wt.Fazis_Id
where md.ContentType = @ContentType

if @@rowcount = 0
   RAISERROR('A ContentType-hoz nem kapcsolódik vagy hibás a workflow definíció!',16,1)

--------------------------
-- Fázisadatok lekérdezése
--------------------------
-- idő munkatábla
create table #Hatarido
	   (Bazisido    datetime,
		Hatarido    datetime)

-- első állapot adatai
if @FazisErtek is NULL
begin

	-- fázis kezdésidő számítás
	exec [dbo].[sp_WF_GetHatarido] 
		 @KezdIdobazis, @BazisObj_Id, @KezdDateCol_Id, NULL, NULL
	select @Kezdesido = Hatarido
	  from #Hatarido
	delete #Hatarido

	-- fázis határidő számítás
	exec [dbo].[sp_WF_GetHatarido] 
		 @Idobazis, @BazisObj_Id, @DateCol_Id, NULL, @AtfutasiIdoMin
	select @Hatarido = Hatarido
	  from #Hatarido
	delete #Hatarido

	insert @TabFazisAdat
		  (Fazis_Id,Tipus,Kod,Ertek,Kezdesido,Hatarido,Belso_Id,BelsoTipus,BelsoKod,BelsoErtek,BelsoKezdesido,BelsoHatarido,Kimenet)
	values(@NextFazis_Id,@Tipus,@Kod,@Ertek,@Kezdesido,@Hatarido,@Belso_Id,@BelsoTipus,@BelsoKod,@BelsoErtek,@BelsoKezdesido,@BelsoHatarido,@AktKimenet)

end

-- belső állapotok adatai
if @FazisErtek is not NULL
begin

	select @Hatarido     = NULL,
		   @BelsoOsszIdo = 0

	declare Fazisadat cursor local for
	 select wf.Id,
			nf.Tipus,
			nf.Kod,
			nf.Ertek,
			wf.KezdIdobazis,
			wf.KezdObjTip_Id_DateCol,
			wf.Idobazis,
			wf.ObjTip_Id_DateCol,
			wf.AtfutasiIdo*convert(int,wf.Idoegyseg) as AtfutasiIdoMin,
			nf.Belso_Id,
			nf.BelsoTipus,
			nf.BelsoKod,
			nf.BelsoErtek,
			ba.KezdIdobazis as BelsoKezdIdobazis,
			ba.KezdObjTip_Id_DateCol as BelsoKezdObjTip_Id_DateCol,
			ba.Idobazis,
			ba.ObjTip_Id_DateCol as BelsoDateCol_Id,
			ba.AtfutasiIdo*convert(int,ba.Idoegyseg) as BelsoAtfutasiIdoMin,
			nf.FazisKimenet
	   from EREC_WorkFlowFazis wf
			inner join dbo.fn_WF_GetNextFazis(@AktFazis_Id,@Kimenet) nf on wf.Id = nf.Id
			left outer join EREC_WorkFlowFazis ba on ba.Id = nf.Belso_Id

	open Fazisadat
	fetch Fazisadat
	 into @NextFazis_Id, @Tipus, @Kod, @Ertek,
		  @KezdIdobazis, @KezdDateCol_Id, @Idobazis, @DateCol_Id, @AtfutasiIdoMin,
		  @Belso_Id, @BelsoTipus, @BelsoKod, @BelsoErtek,
		  @BelsoKezdIdobazis, @BelsoKezdDateCol_Id, @BelsoIdobazis, @BelsoDateCol_Id, @BelsoAtfutasiIdoMin,
		  @AktKimenet
		  
	while @@Fetch_Status = 0

		begin
			if (@@ERROR <> 0)
			   RAISERROR('Hiba a fázisadatok lekérdezésekor!',16,1)

			-- kezdésidő számítás
			if @Hatarido is NULL
			begin

				-- fázis kezdésidő számítás
				exec [dbo].[sp_WF_GetHatarido] 
					 @KezdIdobazis, @BazisObj_Id, @KezdDateCol_Id, NULL, NULL
				select @Kezdesido = Hatarido
				  from #Hatarido
				delete #Hatarido

				-- belső kezdésidő számítás
				if @BelsoKezdIdobazis is not null
				begin
					exec [dbo].[sp_WF_GetHatarido] 
						 @BelsoKezdIdobazis, @BazisObj_Id, @BelsoKezdDateCol_Id, NULL, NULL
					select @BelsoKezdesido = Hatarido
					  from #Hatarido
					delete #Hatarido
				end

			end -- kezdésidő számítás

			-- fázishatáridő számítás
			if @Hatarido is NULL
			begin
				exec [dbo].[sp_WF_GetHatarido] 
					 @Idobazis, @BazisObj_Id, @DateCol_Id, NULL, @AtfutasiIdoMin
				select @Hatarido = Hatarido
				  from #Hatarido
				delete #Hatarido
			end

			-- belső határidő adatok számítása
			select @BelsoKezdesido = isnull(@BelsoHatarido,@BelsoKezdesido),
				   @BelsoOsszIdo   = 
				   @BelsoOsszIdo + isnull(@BelsoAtfutasiIdoMin,0)
			if @BelsoIdobazis is not NULL and
			   @DateCol_Id = @BelsoDateCol_Id
			-- fáziskezdéshez relatív határidő
			begin
				exec [dbo].[sp_WF_GetHatarido] 
					 @BelsoIdobazis, NULL, @BelsoDateCol_Id, NULL, @BelsoAtfutasiIdoMin
				select @BelsoHatarido = Hatarido
				  from #Hatarido
				delete #Hatarido
			end
			-- belső állapot beálltához relatív határidő
			else
				select @BelsoHatarido  = dateadd(mi,@BelsoOsszIdo,@Kezdesido)

			-- fázisadatok eltárolása
			insert @TabFazisAdat
				  (Fazis_Id,Tipus,Kod,Ertek,Kezdesido,Hatarido,Belso_Id,BelsoTipus,BelsoKod,BelsoErtek,BelsoKezdesido,BelsoHatarido,Kimenet)
			values(@NextFazis_Id,@Tipus,@Kod,@Ertek,@Kezdesido,@Hatarido,@Belso_Id,@BelsoTipus,@BelsoKod,@BelsoErtek,@BelsoKezdesido,@BelsoHatarido,@AktKimenet)

			fetch Fazisadat
			 into @NextFazis_Id, @Tipus, @Kod, @Ertek,
				  @KezdIdobazis, @KezdDateCol_Id, @Idobazis, @DateCol_Id, @AtfutasiIdoMin,
				  @Belso_Id, @BelsoTipus, @BelsoKod, @BelsoErtek,
				  @BelsoKezdIdobazis, @BelsoKezdDateCol_Id, @BelsoIdobazis, @BelsoDateCol_Id, @BelsoAtfutasiIdoMin,
				  @AktKimenet

		end

	close Fazisadat
	deallocate Fazisadat

end -- belső állapotok adatai

----------
-- kimenet
----------
select 
--	   Fazis_Id,
	   Ertek + 
		case when BelsoErtek is NULL then ''
			 else ' / '+BelsoErtek end as Allapot,
--	   Tipus as FazisTipus,
--	   Kod as FazisKod,
	   Ertek,
	   Kezdesido,
	   Hatarido,
--	   BelsoTipus,
--	   BelsoKod,
	   BelsoErtek,
	   BelsoKezdesido,
	   BelsoHatarido,
	   Kimenet
  from @TabFazisAdat
 order by Kod, Kimenet

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------