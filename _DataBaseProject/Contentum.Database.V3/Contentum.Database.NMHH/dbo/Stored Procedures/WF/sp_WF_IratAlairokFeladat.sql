﻿create procedure [dbo].[sp_WF_IratAlairokFeladat]    

AS
/*
-- FELADATA:
	EREC_IratAlairok változáshoz kapcsolódó feladat speciális adatainak beállítása.
    Kezelt funkciók: IraIratAlairoNew/IraIratAlairoModify/IraIratAlairoInvalidate
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
declare @Allapot   nvarchar(64)

-----------------------------
-- aláírás adatok lekérdezése
-----------------------------
if @Funkcio_Kod_Kivalto in ('IraIratAlairoNew')
begin
--RAISERROR('Teszt!',16,1)

	-- idoszámítás adatok beállítása
	select @BazisObj_Id = Obj_Id
	  from EREC_IratAlairok
	 where Id = @Obj_Id

end -- aláírás adatok lekérdezése

-------------------------
-- irat aláírás módosulás
-------------------------
if @Funkcio_Kod_Kivalto = 'IraIratAlairoModify'
begin

	-- aláírás állapot lekérdezése 
	select @Allapot          = Allapot,
		   @Obj_Id_Kezelendo = NULL,
		   @FeladatAllapot   = NULL
	  from EREC_IratAlairok
	 where Id = @Obj_Id
	   and Allapot in('2','3')

	if @@rowcount = 1

		select @FeladatAllapot = case when @Allapot = '2' then '3' -- lezárás
									  when @Allapot = '3' then '4' -- visszautasítás
									  else NULL end,
               @Leiras         = ef.Elozmeny_Leiras +
							     case when @Allapot = '2' then ' (Az aláírás megtörtént!)'
								      else ' (Aláírás visszautasítva!)' end,
			   @Feladat_Id     = ef.Elozmeny_Id
		  from dbo.fn_WF_GetElozmenyFeladat
			  (@Obj_Id,'IraIratAlairoNew',@Obj_MetaDefinicio_Id,@ObjStateValue_Id) ef

end -- IraIratAlairoModify

if @Funkcio_Kod_Kivalto = 'IraIratAlairoInvalidate'
begin

	-- aláírás állapot lekérdezése 
	select @Allapot          = Allapot,
		   @Obj_Id_Kezelendo = NULL,
		   @FeladatAllapot   = NULL
	  from EREC_IratAlairok
	 where Id = @Obj_Id
	   and Allapot in('1')

	if @@rowcount = 1

		select @FeladatAllapot = '4', -- sztornózott
               @Leiras         = ef.Elozmeny_Leiras + ' (Aláírás sztornózva!)',
			   @Feladat_Id     = ef.Elozmeny_Id
		  from dbo.fn_WF_GetElozmenyFeladat
			  (@Obj_Id,'IraIratAlairoNew',@Obj_MetaDefinicio_Id,@ObjStateValue_Id) ef

--print '---------- fn_WF_GetElozmenyFeladat ---------'
--print '@Obj_Id    = '+ isnull(convert(varchar(50),@Obj_Id),   'NULL')
--print '@Obj_MetaDefinicio_Id   = '+ isnull(convert(varchar(50),@Obj_MetaDefinicio_Id),  'NULL')
--print '@ObjStateValue_Id       = '+ isnull(convert(varchar(50),@ObjStateValue_Id),      'NULL')
--RAISERROR('Teszt!',16,1)

end -- IraIratAlairoInvalidate

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id, @BazisObj_Id, @Bazisido, @AtfutasiIdoMin, @Obj_Id_Kezelendo,
       @Csoport_Id_Felelos, @Csoport_Id_FelelosFelh, @FeladatHatarido, @Leiras,
       @FeladatAllapot, @Feladat_Id, NULL ObjektumSzukites

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------