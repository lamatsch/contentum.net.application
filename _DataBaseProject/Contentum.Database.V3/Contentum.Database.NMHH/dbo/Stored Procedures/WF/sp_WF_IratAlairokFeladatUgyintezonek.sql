﻿create procedure [dbo].[sp_WF_IratAlairokFeladatUgyintezonek]    

AS
/*
-- FELADATA:
	EREC_IratAlairok változáshoz kapcsolódó feladat speciális adatainak beállítása.
    Kezelt funkciók: IraIratAlairoNew/IraIratAlairoModify/IraIratAlairoInvalidate
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
declare @Allapot   nvarchar(64)

-----------------------------
-- aláírás adatok lekérdezése
-----------------------------
-- TODO: új feladat felvitele, ha valamelyik aláíró újabb aláírót vett fel
--if @Funkcio_Kod_Kivalto in ('IraIratAlairoNew')
--begin


--end -- aláírás adatok lekérdezése

-------------------------
-- irat aláírás módosulás
-------------------------
if @Funkcio_Kod_Kivalto = 'IraIratAlairoModify'
begin
	
	-- feladat szöveghez
	DECLARE @Alairo nvarchar(128)
	DECLARE @IratIktatoszam nvarchar(128)

	-- új feladat felviteléhez beállítás
	select @BazisObj_Id = Obj_Id
	  from EREC_IratAlairok
	 where Id = @Obj_Id

	-- aláírás állapot lekérdezése 
	select @Allapot          = a.Allapot,
		   @Obj_Id_Kezelendo = i.Id,		   
		   @Alairo = f.Nev,
		   @IratIktatoszam = i.Azonosito,
		   @Csoport_Id_FelelosFelh = i.FelhasznaloCsoport_Id_Ugyintez
	  from EREC_IratAlairok a inner join EREC_IraIratok i on a.Obj_Id = i.id inner join KRT_Felhasznalok f on a.FelhasznaloCsoport_Id_Alairo = f.id
	 where a.Id = @Obj_Id
	  --LZS 2019.10.17. BUG_9690 -- and (a.Allapot = '3' or (a.Allapot= '2' and a.AlairasSorrend = (select max(AlairasSorrend) from EREC_IratAlairok where Obj_Id = @BazisObj_Id)))
	 and (a.Allapot = '3' or a.Allapot= '2' ) 
	
	if @@rowcount = 1

		select @Leiras = REPLACE(
							REPLACE(
								REPLACE(@Leiras,'{2}', 
									 case when @Allapot = '2' then 'ELVÉGEZTE'
								     else 'VISSZAUTASÍTOTTA' end)
									,'{1}', @Alairo)
								,'{0}', @IratIktatoszam)

end -- IraIratAlairoModify

--TODO: feladat létrehozása, ha az invalidálás után nincs több aláíró
--if @Funkcio_Kod_Kivalto = 'IraIratAlairoInvalidate'
--begin


--end -- IraIratAlairoInvalidate

---------------------------
--------- kimenet ---------
---------------------------
	if @teszt = 1
	begin
	   print '----- [sp_WF_IratAlairokFeladatUgyintezonek] -----'
	   print '@@FelelosObj_Id           = '+ isnull(convert(varchar(50),@FelelosObj_Id),   'NULL')
	   print '@@BazisObj_Id             = '+ isnull(convert(varchar(50),@BazisObj_Id),  'NULL')
	   print '@@Bazisido                = '+ isnull(convert(varchar(50),@Bazisido),      'NULL')
	   print '@@AtfutasiIdoMin          = '+ isnull(convert(varchar(50),@AtfutasiIdoMin),           'NULL')
	   print '@@Obj_Id_Kezelendo        = '+ isnull(convert(varchar(50),@Obj_Id_Kezelendo), 'NULL')
	   print '@@Csoport_Id_Felelos      = '+ isnull(convert(varchar(50),@Csoport_Id_Felelos),         'NULL')
	   print '@@Csoport_Id_FelelosFelh  = '+ isnull(convert(varchar(50),@Csoport_Id_FelelosFelh),           'NULL')
	   print '@@FeladatHatarido         = '+ isnull(convert(varchar(50),@FeladatHatarido),              'NULL')
	   print '@@Leiras                  = '+ isnull(convert(varchar(300),@Leiras),        'NULL')
	   print '@@FeladatAllapot          = '+ isnull(convert(varchar(50),@FeladatAllapot),                'NULL')
	   print '@@Feladat_Id              = '+ isnull(convert(varchar(50),@Feladat_Id),    'NULL')	  
	end


select @FelelosObj_Id, @BazisObj_Id, @Bazisido, @AtfutasiIdoMin, @Obj_Id_Kezelendo,
       @Csoport_Id_Felelos, @Csoport_Id_FelelosFelh, @FeladatHatarido, @Leiras,
       @FeladatAllapot, @Feladat_Id, NULL ObjektumSzukites

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------



