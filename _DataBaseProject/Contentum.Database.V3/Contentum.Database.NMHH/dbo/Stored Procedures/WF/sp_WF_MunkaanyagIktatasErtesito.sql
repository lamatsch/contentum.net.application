﻿create procedure [dbo].[sp_WF_MunkaanyagIktatasErtesito]    

AS
/*
-- FELADATA:
	Munkaanyag beiktatásához kapcsolódó értesítés adatainak beállítása.
    Kezelt funkciók: MunkapeldanyBeiktatas_Bejovo/MunkapeldanyBeiktatas_Belso
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000),
		@Targy NVARCHAR(4000),
		@UgyintezoId UNIQUEIDENTIFIER,
		@UgyintezoNev NVARCHAR(100)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
--RAISERROR('Teszt!',16,1)

---------------------------
-- üzenet adatok beállítása
---------------------------
if @Funkcio_Kod_Kivalto in ('MunkapeldanyBeiktatas_Bejovo','MunkapeldanyBeiktatas_Belso')
begin

	-- Felelos objektum beállítása
	select @FelelosObj_Id    = p.Id,
		   @ObjektumSzukites = i.Azonosito,
		   @Targy = i.Targy
	  from EREC_IraIratok i,
		   EREC_PldIratpeldanyok p
	 where i.Id = @Obj_Id
	   and i.Id = p.IraIrat_Id
	   and p.Sorszam = 1

end -- üzenet adatok beállítása

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
	   @Obj_Id_Kezelendo,
	   @Csoport_Id_Felelos,
	   @Csoport_Id_FelelosFelh,
	   @FeladatHatarido,
	   @Leiras,
	   '0', --@FeladatAllapot
	   @Feladat_Id,
	   @ObjektumSzukites,
	   @Targy,
	   @UgyintezoId,
	   @UgyintezoNev

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------