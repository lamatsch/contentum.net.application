﻿create procedure [dbo].[sp_WF_SkontroHataridoEllenorzes]    

AS
/*
-- FELADATA:
	Skontroban lévo, lejárt skontro vége határideju ügyiratok ellenorzése.
    Kezelt funkció: LejartSkotroHatarido

-- Példák:
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartSkotroHatarido'
--
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
-- lejárt tételek
if @Funkcio_Kod_Kivalto in ('LejartSkotroHatarido')
	insert into #HataridosTetelek
	select NULL as Csoport_Id_Felelos,
		   hi.Csoport_Id_FelelosFelh,
		   ' (' + convert(nvarchar(6),hi.db)+ ' db)' as LeirasKieg
	  from (select FelhasznaloCsoport_Id_Orzo as Csoport_Id_FelelosFelh,
                   count(*) db
			  from EREC_UgyUgyiratok
			 where Allapot in(	'07',	--Skontróban
								'57' 	--Skontróból elkért
							  )
			   and SkontroVege < @FeladatHatarido
			 group by FelhasznaloCsoport_Id_Orzo
		   ) hi
		   inner join KRT_Csoportok cs on hi.Csoport_Id_FelelosFelh = cs.Id and cs.Tipus = '1'

---- kimeno adatok szukítése
set @ObjektumSzukites =
	'EREC_UgyUgyiratok.Allapot in (''07'',''57'')' + 
	' and EREC_UgyUgyiratok.SkontroVege <  ' + 
	'''' + convert(varchar(25),@FeladatHatarido,126) + ''''

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
       @Obj_Id_Kezelendo,
       Csoport_Id_Felelos,
	   Csoport_Id_FelelosFelh,
       @FeladatHatarido,
       @Leiras + LeirasKieg,
       '0', --@FeladatAllapot
       @Feladat_Id,
	   @ObjektumSzukites + ' and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo=''' + convert(varchar(36), Csoport_Id_FelelosFelh) + ''''
  from #HataridosTetelek

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------