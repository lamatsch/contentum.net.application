﻿create procedure [dbo].[sp_WF_UgyiratHataridoEllenorzes]    

AS
/*
-- FELADATA:
	Megadott idon belül lejáró vagy már lejárt intézkedési határideju lezáratlan ügyiratok
    ellenorzése.
    Kezelt funkció: LejaroUgyiratHatarido, LejartUgyiratHatarido

-- Példák:
exec [dbo].[sp_WF_UtemezettTrigger] 'LejaroUgyiratHatarido'
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartUgyiratHatarido'
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
-- adott határidon belül lejáró tételek
if @Funkcio_Kod_Kivalto in ('LejaroUgyiratHatarido', 'LejartUgyiratHatarido')
	insert into #HataridosTetelek
	select NULL as Csoport_Id_Felelos,
		   Csoport_Id_FelelosFelh,
		   ' (' + convert(nvarchar(6),hi.db)+ ' db)' as LeirasKieg
	  from (select isnull(FelhasznaloCsoport_Id_Ugyintez,FelhasznaloCsoport_Id_Orzo) as Csoport_Id_FelelosFelh,
                   count(*) db
			  from EREC_UgyUgyiratok ui
			 where Allapot in(	'03',	--Szignált
								'04',	--Iktatott
								'06',	--Ügyintézés alatt
								'07',	--Skontróban
								'50',	--Továbbítás alatt
								'57',	--Skontróból elkért
								'70'	--Skontróba helyezés jóváhagyása
								--'0'	Iktatásra elokészített
								--09	Lezárt
								--10	Irattárban orzött
								--11	Irattárba küldött
								--13	Kölcsönzött
								--30	Jegyzékre helyezett
								--31	Lezárt jegyzékben lévo
								--32	Selejtezett
								--33	Levéltárba adott
								--52	Irattározásra jóváhagyás alatt
								--55	Irattárból elkért
								--56	Engedélyezett kikéron lévo
								--60	Szerelt
								--90	Sztornózott
								--99	Elintézett
							)
			   and Hatarido >= case when @Funkcio_Kod_Kivalto = 'LejaroUgyiratHatarido'
									 then @Bazisido else Hatarido end
			   and Hatarido <  case when @Funkcio_Kod_Kivalto = 'LejaroUgyiratHatarido'
									 then @FeladatHatarido else @Bazisido end
			 group by isnull(FelhasznaloCsoport_Id_Ugyintez,FelhasznaloCsoport_Id_Orzo)
		   ) hi
		   inner join KRT_Csoportok cs on hi.Csoport_Id_FelelosFelh = cs.Id and cs.Tipus = '1'

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
       @Obj_Id_Kezelendo,
       Csoport_Id_Felelos,
	   Csoport_Id_FelelosFelh,
       @FeladatHatarido,
       @Leiras + LeirasKieg,
       '0', --@FeladatAllapot
       @Feladat_Id,
	   'EREC_UgyUgyiratok.Allapot in (''03'',''04'',''06'',''07'',''50'',''57'',''70'')' + 
	   ' and isnull(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez,EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo) = ' +
	   '''' + convert(varchar(40),Csoport_Id_FelelosFelh) + '''' + ')' +
	   ' and EREC_UgyUgyiratok.Hatarido >= ' +
	   case when @Funkcio_Kod_Kivalto = 'LejaroUgyiratHatarido'
			then '''' + convert(varchar(25),@Bazisido,126) + ''''
			else '''' + convert(varchar(25),@FeladatHatarido,126) + '''' end + 
	   ' and EREC_UgyUgyiratok.Hatarido < ' +
	   case when @Funkcio_Kod_Kivalto = 'LejaroUgyiratHatarido'
			then '''' + convert(varchar(25),@FeladatHatarido,126) + ''''
			else '''' + convert(varchar(25),@Bazisido,126) + '''' end
  from #HataridosTetelek

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------