﻿CREATE TABLE [dbo].[EREC_AgazatiJelek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Agazati__Id__13F1F5EB] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Agazat__Org__14E61A24] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Kod]            NVARCHAR (20)    NULL,
    [Nev]            NVARCHAR (400)   NULL,
    [AgazatiJel_Id]  UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_Agazat__Ver__15DA3E5D] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_Agaz__ErvKe__16CE6296] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_AgazatiJelek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_Agaz__Letre__18B6AB08] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_AGAZATIJELEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_EREC_AGA_AGAZATIJE_EREC_AGA] FOREIGN KEY ([AgazatiJel_Id]) REFERENCES [dbo].[EREC_AgazatiJelek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_FK]
    ON [dbo].[EREC_AgazatiJelek]([AgazatiJel_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [AgazatiJel_UK]
    ON [dbo].[EREC_AgazatiJelek]([Kod] ASC) WITH (FILLFACTOR = 85);

