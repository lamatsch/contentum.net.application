﻿CREATE TABLE [dbo].[EREC_Alairas_Folyamatok] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [AlairasStarted] DATETIME         DEFAULT (getdate()) NULL,
    [AlairasStopped] DATETIME         NULL,
    [AlairasStatus]  NVARCHAR (64)    NOT NULL,
    [Ver]            INT              DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_ALAIRAS_FOLYAMATOK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CK_EREC_Alairas_Folyamatok] CHECK ((0)=[dbo].[fn_EREC_Alairas_FolyamatokCheckUK]([Id],[ErvKezd],[ErvVege]))
);

