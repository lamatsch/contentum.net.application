﻿CREATE TABLE [dbo].[EREC_Csatolmanyok] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Csatolm__Id__1B9317B3] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id] UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]       UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id]    UNIQUEIDENTIFIER NOT NULL,
    [Leiras]           NVARCHAR (100)   NULL,
    [Lapszam]          INT              NULL,
    [KapcsolatJelleg]  NVARCHAR (64)    NULL,
    [DokumentumSzerep] NVARCHAR (64)    NULL,
    [IratAlairoJel]    CHAR (1)         NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_Csatol__Ver__1C873BEC] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Csat__ErvKe__1D7B6025] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_Csatolmanyok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Csat__Letre__1F63A897] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_CSATOLMANYOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_IRAT_CSATOLMANY_FK] FOREIGN KEY ([IraIrat_Id]) REFERENCES [dbo].[EREC_IraIratok] ([Id]),
    CONSTRAINT [EREC_KULDEMENY_CSATOLMANY_FK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Csatolmanyok_KuldId]
    ON [dbo].[EREC_Csatolmanyok]([KuldKuldemeny_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Csatolmanyok_IratId]
    ON [dbo].[EREC_Csatolmanyok]([IraIrat_Id] ASC) WITH (FILLFACTOR = 85);

