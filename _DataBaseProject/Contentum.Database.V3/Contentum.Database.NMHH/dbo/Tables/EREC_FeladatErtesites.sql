/*==============================================================*/
/* Table: EREC_FeladatErtesites                                 */
/*==============================================================*/
create table EREC_FeladatErtesites (
   Id                   uniqueidentifier     not null default 'newsequentialid()',
   FeladatDefinicio_id  uniqueidentifier     null,
   Felhasznalo_Id       uniqueidentifier     null,
   Ertesites            char(1)              null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_EREC_FELADATERTESITES primary key (Id)
)
go

alter table EREC_FeladatErtesites 
  ADD CONSTRAINT
	DF_EREC_FeladatErtesites_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go

alter table EREC_FeladatErtesites
   add constraint FK_FELADATERT_FELDEF foreign key (FeladatDefinicio_id)
      references EREC_FeladatDefinicio (Id)
go

alter table EREC_FeladatErtesites
   add constraint FK_FELADATERT_FELHASZNALO foreign key (Felhasznalo_Id)
      references KRT_Felhasznalok (Id)
go
