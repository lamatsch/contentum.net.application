﻿CREATE TABLE [dbo].[EREC_Hatarid_Objektumok] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Hatarid__Id__5BC376B8] DEFAULT (newsequentialid()) NOT NULL,
    [HataridosFeladat_Id] UNIQUEIDENTIFIER NOT NULL,
    [Obj_Id]              UNIQUEIDENTIFIER NOT NULL,
    [ObjTip_Id]           UNIQUEIDENTIFIER NOT NULL,
    [Obj_type]            NVARCHAR (100)   NULL,
    [Ver]                 INT              CONSTRAINT [DF__EREC_Hatari__Ver__5CB79AF1] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__EREC_Hata__ErvKe__5DABBF2A] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_EREC_Hatarid_Objektumok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__EREC_Hata__Letre__5F94079C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_HATARID_OBJEKTUMOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [FK_HataridosFeladat_Id]
    ON [dbo].[EREC_Hatarid_Objektumok]([HataridosFeladat_Id] ASC) WITH (FILLFACTOR = 85);

