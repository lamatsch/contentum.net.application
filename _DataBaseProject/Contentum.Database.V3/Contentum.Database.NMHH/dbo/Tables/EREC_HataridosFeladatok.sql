﻿CREATE TABLE [dbo].[EREC_HataridosFeladatok] (
    [Id]                         UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Hatarid__Id__42ACE4D4] DEFAULT (newsequentialid()) NOT NULL,
    [Esemeny_Id_Kivalto]         UNIQUEIDENTIFIER NULL,
    [Esemeny_Id_Lezaro]          UNIQUEIDENTIFIER NULL,
    [Funkcio_Id_Inditando]       UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]         UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Felelos]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Kiado]           UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Kiado]       UNIQUEIDENTIFIER NULL,
    [HataridosFeladat_Id]        UNIQUEIDENTIFIER NULL,
    [ReszFeladatSorszam]         INT              NULL,
    [FeladatDefinicio_Id]        UNIQUEIDENTIFIER NULL,
    [FeladatDefinicio_Id_Lezaro] UNIQUEIDENTIFIER NULL,
    [Forras]                     CHAR (1)         NULL,
    [Tipus]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Altipus]                    NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Memo]                       CHAR (1)         NULL,
    [Leiras]                     NVARCHAR (max)   NULL,
    [Prioritas]                  NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [LezarasPrioritas]           NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [KezdesiIdo]                 DATETIME         NULL,
    [IntezkHatarido]             DATETIME         NULL,
    [LezarasDatuma]              DATETIME         NULL,
    [Azonosito]                  NVARCHAR (100)   NULL,
    [Obj_Id]                     UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]                  UNIQUEIDENTIFIER NULL,
    [Obj_type]                   NVARCHAR (100)   NULL,
    [Allapot]                    NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Megoldas]                   NVARCHAR (400)   NULL,
    [Note]                       NVARCHAR (4000)  NULL,
    [Ver]                        INT              CONSTRAINT [DF__EREC_Hatari__Ver__43A1090D] DEFAULT ((1)) NULL,
    [Stat_id]                    UNIQUEIDENTIFIER NULL,
    [ErvKezd]                    DATETIME         CONSTRAINT [DF__EREC_Hata__ErvKe__44952D46] DEFAULT (getdate()) NULL,
    [ErvVege]                    DATETIME         CONSTRAINT [DF_EREC_HataridosFeladatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]               UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]              DATETIME         CONSTRAINT [DF__EREC_Hata__Letre__467D75B8] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                UNIQUEIDENTIFIER NULL,
    [ModositasIdo]               DATETIME         NULL,
    [Zarolo_id]                  UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                 DATETIME         NULL,
    [Tranz_id]                   UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_HATARIDOSFELADATOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Erec_Hataridos_Erec_Hataridos_FK] FOREIGN KEY ([HataridosFeladat_Id]) REFERENCES [dbo].[EREC_HataridosFeladatok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Obj_Id]
    ON [dbo].[EREC_HataridosFeladatok]([Obj_Id] ASC)
    INCLUDE([ErvKezd], [ErvVege]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_HataridosFeladat_Id]
    ON [dbo].[EREC_HataridosFeladatok]([HataridosFeladat_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [EREC_HataridosFeladat_UK]
    ON [dbo].[EREC_HataridosFeladatok]([Csoport_Id_Felelos] ASC, [Felhasznalo_Id_Kiado] ASC, [HataridosFeladat_Id] ASC, [IntezkHatarido] ASC) WITH (FILLFACTOR = 85);

