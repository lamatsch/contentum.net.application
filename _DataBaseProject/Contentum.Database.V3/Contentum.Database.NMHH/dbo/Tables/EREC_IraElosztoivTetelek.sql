﻿CREATE TABLE [dbo].[EREC_IraElosztoivTetelek] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraElos__Id__50FB042B] DEFAULT (newsequentialid()) NOT NULL,
    [ElosztoIv_Id]     UNIQUEIDENTIFIER NOT NULL,
    [Sorszam]          INT              NOT NULL,
    [Partner_Id]       UNIQUEIDENTIFIER NULL,
    [Cim_Id]           UNIQUEIDENTIFIER NULL,
    [CimSTR]           NVARCHAR (400)   NULL,
    [NevSTR]           NVARCHAR (400)   NULL,
    [Kuldesmod]        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Visszavarolag]    CHAR (1)         NULL,
    [VisszavarasiIdo]  DATETIME         NULL,
    [Vissza_Nap_Mulva] INT              NULL,
    [AlairoSzerep]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_IraElo__Ver__51EF2864] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_IraE__ErvKe__52E34C9D] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_IraElosztoivTetelek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_IraE__Letre__54CB950F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [ELIT_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EIVTetel_EIV_FK] FOREIGN KEY ([ElosztoIv_Id]) REFERENCES [dbo].[EREC_IraElosztoivek] ([Id]),
    CONSTRAINT [FK_EREC_IRA_IRA_EIVTE_KRT_PART] FOREIGN KEY ([Partner_Id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Partner_Id]
    ON [dbo].[EREC_IraElosztoivTetelek]([Partner_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_ElosztoIv_Id]
    ON [dbo].[EREC_IraElosztoivTetelek]([ElosztoIv_Id] ASC) WITH (FILLFACTOR = 85);

