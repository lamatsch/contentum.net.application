﻿CREATE TABLE [dbo].[EREC_IraElosztoivek] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraElos__Id__4959E263] DEFAULT (newsequentialid()) NOT NULL,
    [Org]              UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraElo__Org__4A4E069C] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Ev]               INT              NULL,
    [Fajta]            NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Kod]              NVARCHAR (10)    NOT NULL,
    [NEV]              NVARCHAR (100)   NOT NULL,
    [Hasznalat]        NVARCHAR (64)    NULL,
    [Csoport_Id_Tulaj] UNIQUEIDENTIFIER NOT NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_IraElo__Ver__4B422AD5] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_IraE__ErvKe__4C364F0E] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_IraElosztoivek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_IraE__Letre__4E1E9780] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [ELIV_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [FK_Csoport_Id_Tulaj]
    ON [dbo].[EREC_IraElosztoivek]([Csoport_Id_Tulaj] ASC) WITH (FILLFACTOR = 85);

