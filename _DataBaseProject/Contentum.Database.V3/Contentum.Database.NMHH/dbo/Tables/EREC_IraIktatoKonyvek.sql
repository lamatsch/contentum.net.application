﻿CREATE TABLE [dbo].[EREC_IraIktatoKonyvek] (
    [Id]                         UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraIkta__Id__57A801BA] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                        UNIQUEIDENTIFIER NOT NULL,
    [Ev]                         INT              NOT NULL,
    [Azonosito]                  NVARCHAR (20)    NULL,
    [DefaultIrattariTetelszam]   UNIQUEIDENTIFIER NULL,
    [Nev]                        NVARCHAR (100)   NOT NULL,
    [MegkulJelzes]               NVARCHAR (20)    NULL,
    [Iktatohely]                 NVARCHAR (100)   NULL,
    [KozpontiIktatasJelzo]       CHAR (1)         NULL,
    [UtolsoFoszam]               INT              NOT NULL,
    [UtolsoSorszam]              INT              NULL,
    [Csoport_Id_Olvaso]          UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Tulaj]           UNIQUEIDENTIFIER NULL,
    [IktSzamOsztas]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [FormatumKod]                CHAR (1)         NULL,
    [Titkos]                     CHAR (1)         CONSTRAINT [DF__EREC_IraI__Titko__589C25F3] DEFAULT ('N') NULL,
    [IktatoErkezteto]            CHAR (1)         NULL,
    [LezarasDatuma]              DATETIME         NULL,
    [PostakonyvVevokod]          NVARCHAR (100)   NULL,
    [PostakonyvMegallapodasAzon] NVARCHAR (100)   NULL,
    [EFeladoJegyzekUgyfelAdatok] NVARCHAR (4000)  NULL,
    [Ujranyitando]				 CHAR(1)		  NULL,
	[Ver]                        INT              CONSTRAINT [DF__EREC_IraIkt__Ver__59904A2C] DEFAULT ((1)) NULL,
    [Note]                       NVARCHAR (4000)  NULL,
    [Stat_id]                    UNIQUEIDENTIFIER NULL,
    [ErvKezd]                    DATETIME         CONSTRAINT [DF__EREC_IraI__ErvKe__5A846E65] DEFAULT (getdate()) NULL,
    [ErvVege]                    DATETIME         CONSTRAINT [DF_EREC_IraIktatoKonyvek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]               UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]              DATETIME         CONSTRAINT [DF__EREC_IraI__Letre__5C6CB6D7] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                UNIQUEIDENTIFIER NULL,
    [ModositasIdo]               DATETIME         NULL,
    [Zarolo_id]                  UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                 DATETIME         NULL,
    [Tranz_id]                   UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]             UNIQUEIDENTIFIER NULL,
    [Statusz]                    CHAR (1)         CONSTRAINT [DF__EREC_IraI__Statu__5D60DB10] DEFAULT ('1') NULL,
    [Terjedelem]                 NVARCHAR (20)    NULL,
    [SelejtezesDatuma]           DATETIME         NULL,
    [KezelesTipusa]              CHAR (1)         NULL,
    [HitelesExport_Dok_ID]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [IKTK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);




GO
CREATE NONCLUSTERED INDEX [Index_UK]
    ON [dbo].[EREC_IraIktatoKonyvek]([Org] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

