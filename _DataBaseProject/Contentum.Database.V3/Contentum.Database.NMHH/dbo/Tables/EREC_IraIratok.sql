﻿CREATE TABLE [dbo].[EREC_IraIratok] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraIrat__Id__603D47BB] DEFAULT (newsequentialid()) NOT NULL,
    [PostazasIranya]                 CHAR (1)         NULL,
    [Alszam]                         INT              NULL,
    [Sorszam]                        INT              NULL,
    [UtolsoSorszam]                  INT              NULL,
    [UgyUgyIratDarab_Id]             UNIQUEIDENTIFIER NULL,
    [Kategoria]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [HivatkozasiSzam]                NVARCHAR (100)   NULL,
    [IktatasDatuma]                  DATETIME         NULL,
    [ExpedialasDatuma]               DATETIME         NULL,
    [ExpedialasModja]                NVARCHAR (64)    NULL,
    [FelhasznaloCsoport_Id_Expedial] UNIQUEIDENTIFIER NULL,
    [Targy]                          NVARCHAR (4000)  NOT NULL,
    [Jelleg]                         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [FelhasznaloCsoport_Id_Iktato]   UNIQUEIDENTIFIER NOT NULL,
    [FelhasznaloCsoport_Id_Kiadmany] UNIQUEIDENTIFIER NULL,
    [UgyintezesAlapja]               NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [AdathordozoTipusa]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Ugyfelelos]          UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Orzo]     UNIQUEIDENTIFIER NULL,
    [KiadmanyozniKell]               CHAR (1)         NULL,
    [FelhasznaloCsoport_Id_Ugyintez] UNIQUEIDENTIFIER NULL,
    [Hatarido]                       DATETIME         NULL,
    [MegorzesiIdo]                   DATETIME         NULL,
    [IratFajta]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Irattipus]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [KuldKuldemenyek_Id]             UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [GeneraltTargy]                  NVARCHAR (400)   NULL,
    [IratMetaDef_Id]                 UNIQUEIDENTIFIER NULL,
    [IrattarbaKuldDatuma]            DATETIME         NULL,
    [IrattarbaVetelDat]              DATETIME         NULL,
    [Ugyirat_Id]                     UNIQUEIDENTIFIER NULL,
    [Minosites]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
	Elintezett           char(1)              null,
    IratHatasaUgyintezesre nvarchar(64)         collate Hungarian_CS_AS null,
    FelfuggesztesOka     Nvarchar(400)        null,
	LezarasOka           Nvarchar(400)        null,
    Munkaallomas         Nvarchar(100)        null,
    UgyintezesModja      Nvarchar(100)        null,
    IntezesIdopontja     datetime             null,
    IntezesiIdo          nvarchar(64)         collate Hungarian_CS_AS null,
    IntezesiIdoegyseg    nvarchar(64)         collate Hungarian_CS_AS null,
    UzemzavarKezdete     datetime             null,
    UzemzavarVege        datetime             null,
    UzemzavarIgazoloIratSzama Nvarchar(400)        null,
    FelfuggesztettNapokSzama int                  null,
    VisszafizetesJogcime nvarchar(64)         collate Hungarian_CS_AS null,
    VisszafizetesOsszege int                  null,
    VisszafizetesHatarozatSzama Nvarchar(400)        null,
    Partner_Id_VisszafizetesCimzet uniqueidentifier     null,
	Partner_Nev_VisszafizetCimzett Nvarchar(400)		null,  
    VisszafizetesHatarido datetime             null,   
	Minosito UNIQUEIDENTIFIER NULL,  
	MinositesErvenyessegiIdeje DATETIME NULL,  
	TerjedelemMennyiseg INT NULL,  
	TerjedelemMennyisegiEgyseg NVARCHAR(64) NULL,  
	TerjedelemMegjegyzes NVARCHAR(400) NULL,
	SelejtezesDat        datetime             null,
    FelhCsoport_Id_Selejtezo uniqueidentifier     null,
    LeveltariAtvevoNeve  Nvarchar(100)        null,  
    [AKTIV]                          AS               (CONVERT([bit],case when [ALLAPOT]='90' then (0) else (1) end)),
    [ModositasErvenyessegKezdete]	   DATETIME			NULL,
    [MegszuntetoHatarozat]             NVARCHAR (400)   NULL,
    [FelulvizsgalatDatuma]			   DATETIME			NULL,  
    [Felulvizsgalo_id]				   UNIQUEIDENTIFIER	NULL,  
    [MinositesFelulvizsgalatEredm]	NVARCHAR (10)   NULL,
    [UgyintezesKezdoDatuma]			 datetime         null,
    [EljarasiSzakasz]      			 nvarchar(64)     null,
    [HatralevoNapok]       			 AS (([dbo].[FN_IraIratok_HatralevoNapok](GETDATE(),[Hatarido]))),
    [HatralevoMunkaNapok]  			 AS (([dbo].[FN_IraIratok_HatralevoMunkaNapok](GETDATE(),[Hatarido]))),
	Ugy_Fajtaja 					 nvarchar(64),  
    [MinositoSzervezet] UNIQUEIDENTIFIER NULL,
	[MinositesFelulvizsgalatBizTag] Nvarchar(4000), 
   [Ver]                            INT              CONSTRAINT [DF__EREC_IraIra__Ver__61316BF4] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_IraI__ErvKe__6225902D] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_IraIratok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_IraI__Letre__640DD89F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL
    CONSTRAINT [IRA_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_IRAT_UGY_FK] FOREIGN KEY ([Ugyirat_Id]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [IraIrat_UgyiratDarab_FK] FOREIGN KEY ([UgyUgyIratDarab_Id]) REFERENCES [dbo].[EREC_UgyUgyiratdarabok] ([Id]),
    CONSTRAINT [Irat_Kuldemeny_FK] FOREIGN KEY ([KuldKuldemenyek_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraIratok_Aktiv]
    ON [dbo].[EREC_IraIratok]([AKTIV] ASC, [Id] ASC);


GO
CREATE NONCLUSTERED INDEX [LETREHOZO_IND]
    ON [dbo].[EREC_IraIratok]([Letrehozo_id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Irat]
    ON [dbo].[EREC_IraIratok]([ModositasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Irat_Ugyirat_Id]
    ON [dbo].[EREC_IraIratok]([Ugyirat_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Irat_Minosites]
    ON [dbo].[EREC_IraIratok]([Minosites] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IRA_PRT_FK_I]
    ON [dbo].[EREC_IraIratok]([FelhasznaloCsoport_Id_Iktato] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IRA_IRAU_UK]
    ON [dbo].[EREC_IraIratok]([Alszam] ASC, [UgyUgyIratDarab_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IND_UDB_ID]
    ON [dbo].[EREC_IraIratok]([UgyUgyIratDarab_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_IKTATAS_DATUM]
    ON [dbo].[EREC_IraIratok]([IktatasDatuma] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_KuldKuldemenyek_Id]
    ON [dbo].[EREC_IraIratok]([KuldKuldemenyek_Id] ASC) WITH (FILLFACTOR = 85);

