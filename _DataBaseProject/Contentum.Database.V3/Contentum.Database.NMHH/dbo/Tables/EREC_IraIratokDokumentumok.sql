﻿CREATE TABLE [dbo].[EREC_IraIratokDokumentumok] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Dokumentum_Id]    UNIQUEIDENTIFIER NOT NULL,
    [IraIrat_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Leiras]           NVARCHAR (100)   NULL,
    [Lapszam]          INT              NULL,
    [BarCode]          NVARCHAR (100)   NULL,
    [Forras]           NVARCHAR (64)    NULL,
    [Formatum]         NVARCHAR (64)    NULL,
    [Vonalkodozas]     CHAR (1)         NULL,
    [DokumentumSzerep] NVARCHAR (64)    NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_IraIra__Ver__60882BD5] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_IraI__ErvKe__617C500E] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_IraIratokDokumentumok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_IraI__Letre__63649880] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [IratDoku_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [I_Barkod]
    ON [dbo].[EREC_IraIratokDokumentumok]([BarCode] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id]
    ON [dbo].[EREC_IraIratokDokumentumok]([IraIrat_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_Dokumentum_Id]
    ON [dbo].[EREC_IraIratokDokumentumok]([Dokumentum_Id] ASC);

