﻿CREATE TABLE [dbo].[EREC_IraIrattariTetelek] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraIrat__Id__68D28DBC] DEFAULT (newsequentialid()) NOT NULL,
    [Org]               UNIQUEIDENTIFIER NULL,
    [IrattariTetelszam] NVARCHAR (20)    NOT NULL,
    [Nev]               NVARCHAR (400)   NOT NULL,
    [IrattariJel]       NVARCHAR (2)     COLLATE Hungarian_CS_AS NOT NULL,
    [MegorzesiIdo]      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Idoegyseg]         nvarchar(64)     NULL,
    [Folyo_CM]          NVARCHAR (10)    NULL,
    [AgazatiJel_Id]     UNIQUEIDENTIFIER NULL,
    [Ver]               INT              CONSTRAINT [DF__EREC_IraIra__Ver__69C6B1F5] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__EREC_IraI__ErvKe__6ABAD62E] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_EREC_IraIrattariTetelek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__EREC_IraI__Letre__6CA31EA0] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    [EvenTuliIktatas]   CHAR (1)         NULL,
    CONSTRAINT [ITSZ_PK] PRIMARY KEY NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [CK_EREC_IraIrattariTetelek] CHECK ((0)=[dbo].[fn_EREC_IraIrattariTetelekCheckUK]([Org],[IrattariTetelszam],[Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [IrattariTetel_AgazatiJel_FK] FOREIGN KEY ([AgazatiJel_Id]) REFERENCES [dbo].[EREC_AgazatiJelek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KIRL_IRA_IRATTARI_TETELEK_I_N]
    ON [dbo].[EREC_IraIrattariTetelek]([Nev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IraIrattariTetelek_UK]
    ON [dbo].[EREC_IraIrattariTetelek]([Org] ASC, [IrattariTetelszam] ASC) WITH (FILLFACTOR = 85);

