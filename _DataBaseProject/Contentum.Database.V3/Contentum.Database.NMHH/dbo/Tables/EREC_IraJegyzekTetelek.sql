﻿CREATE TABLE [dbo].[EREC_IraJegyzekTetelek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraJegy__Id__7908F585] DEFAULT (newsequentialid()) NOT NULL,
    [Jegyzek_Id]     UNIQUEIDENTIFIER NULL,
    [Sorszam]        INT              NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NOT NULL,
    [ObjTip_Id]      UNIQUEIDENTIFIER NULL,
    [Obj_type]       NVARCHAR (100)   NULL,
    [Allapot]        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Megjegyzes]     NVARCHAR (400)   NULL,
    [Azonosito]      NVARCHAR (100)   NULL,
    [SztornozasDat]  DATETIME         NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_IraJeg__Ver__79FD19BE] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_IraJ__ErvKe__7AF13DF7] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_IraJegyzekTetelek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_IraJ__Letre__7CD98669] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    [AtadasDatuma]   DATETIME         NULL,
    CONSTRAINT [eRec_IraJegyzekTetelek_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KIRL_IRA_JEGYZ_TETELEK_JGY_FK] FOREIGN KEY ([Jegyzek_Id]) REFERENCES [dbo].[EREC_IraJegyzekek] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [FK_Obj_Id]
    ON [dbo].[EREC_IraJegyzekTetelek]([Obj_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Jegyzek_Id]
    ON [dbo].[EREC_IraJegyzekTetelek]([Jegyzek_Id] ASC) WITH (FILLFACTOR = 85);

