﻿CREATE TABLE [dbo].[EREC_IraJegyzekek] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraJegy__Id__6F7F8B4B] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                            UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraJeg__Org__7073AF84] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Tipus]                          CHAR (1)         NULL,
    [Minosites]                      NVARCHAR (2)     NULL,
    [Csoport_Id_felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Vegrehaj] UNIQUEIDENTIFIER NULL,
    [Partner_Id_LeveltariAtvevo]     UNIQUEIDENTIFIER NULL,
    [LezarasDatuma]                  DATETIME         CONSTRAINT [DF__EREC_IraJ__Lezar__7167D3BD] DEFAULT (NULL) NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [Nev]                            NVARCHAR (100)   NULL,
    [VegrehajtasDatuma]              DATETIME         CONSTRAINT [DF__EREC_IraJ__Vegre__725BF7F6] DEFAULT (NULL) NULL,
    Foszam               int                  null,
    Azonosito            Nvarchar(100)        null,
    MegsemmisitesDatuma  datetime             null,
	Csoport_Id_FelelosSzervezet uniqueidentifier     null,
	IraIktatokonyv_Id uniqueidentifier     null,
	Allapot							 nvarchar(64)     collate Hungarian_CS_AS null,
    VegrehajtasKezdoDatuma			 datetime         null,
    [Ver]                            INT              CONSTRAINT [DF__EREC_IraJeg__Ver__73501C2F] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_IraJ__ErvKe__74444068] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_IraJegyzekek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_IraJ__Letre__762C88DA] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_IRA_JEGYZEKEK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [EREC_IraJegyzek_UK]
    ON [dbo].[EREC_IraJegyzekek]([Tipus] ASC, [Minosites] ASC, [Csoport_Id_felelos] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

