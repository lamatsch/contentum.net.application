﻿CREATE TABLE [dbo].[EREC_IraKezFeljegyzesek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraKezF__Id__0D0FEE32] DEFAULT (newsequentialid()) NOT NULL,
    [IraIrat_Id]     UNIQUEIDENTIFIER NOT NULL,
    [KezelesTipus]   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]         NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_IraKez__Ver__0E04126B] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_IraK__ErvKe__0EF836A4] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_IraKezFeljegyzesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_IraK__Letre__10E07F16] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_IRA_KEZ_FELJEGYZESEK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [IraKezFeljegyzes_IratPld_FK] FOREIGN KEY ([IraIrat_Id]) REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id]
    ON [dbo].[EREC_IraKezFeljegyzesek]([IraIrat_Id] ASC) WITH (FILLFACTOR = 85);

