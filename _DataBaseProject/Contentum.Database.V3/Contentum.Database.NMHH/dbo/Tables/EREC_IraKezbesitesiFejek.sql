﻿CREATE TABLE [dbo].[EREC_IraKezbesitesiFejek] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraKezb__Id__7FB5F314] DEFAULT (newsequentialid()) NOT NULL,
    [CsomagAzonosito]    NVARCHAR (100)   NULL,
    [Tipus]              CHAR (1)         NOT NULL,
    [UtolsoNyomtatasIdo] DATETIME         NULL,
    [UtolsoNyomtatas_Id] UNIQUEIDENTIFIER NULL,
    [Ver]                INT              CONSTRAINT [DF__EREC_IraKez__Ver__00AA174D] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__EREC_IraK__ErvKe__019E3B86] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_EREC_IraKezbesitesiFejek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__EREC_IraK__Letre__038683F8] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IRAKEZBESITESIFEJEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [Index_1]
    ON [dbo].[EREC_IraKezbesitesiFejek]([Tipus] ASC, [CsomagAzonosito] ASC) WITH (FILLFACTOR = 85);

