﻿CREATE TABLE [dbo].[EREC_IraKezbesitesiTetelek] (
    [Id]                         UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraKezb__Id__0662F0A3] DEFAULT (newsequentialid()) NOT NULL,
    [KezbesitesFej_Id]           UNIQUEIDENTIFIER NULL,
    [AtveteliFej_Id]             UNIQUEIDENTIFIER NULL,
    [Obj_Id]                     UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]                  UNIQUEIDENTIFIER NULL,
    [Obj_type]                   NVARCHAR (100)   NULL,
    [AtadasDat]                  DATETIME         NULL,
    [AtvetelDat]                 DATETIME         NULL,
    [Megjegyzes]                 NVARCHAR (400)   NULL,
    [SztornirozasDat]            DATETIME         NULL,
    [UtolsoNyomtatas_Id]         UNIQUEIDENTIFIER NULL,
    [UtolsoNyomtatasIdo]         DATETIME         NULL,
    [Felhasznalo_Id_Atado_USER]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Atado_LOGIN] UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Cel]             UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_AtvevoUser]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_AtvevoLogin] UNIQUEIDENTIFIER NULL,
    [Allapot]                    CHAR (1)         NULL,
    [Azonosito_szoveges]         NVARCHAR (100)   NULL,
    [UgyintezesModja]            NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]                        INT              CONSTRAINT [DF__EREC_IraKez__Ver__075714DC] DEFAULT ((1)) NULL,
    [Note]                       NVARCHAR (4000)  NULL,
    [Stat_id]                    UNIQUEIDENTIFIER NULL,
    [ErvKezd]                    DATETIME         CONSTRAINT [DF__EREC_IraK__ErvKe__084B3915] DEFAULT (getdate()) NULL,
    [ErvVege]                    DATETIME         CONSTRAINT [DF_EREC_IraKezbesitesiTetelek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]               UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]              DATETIME         CONSTRAINT [DF__EREC_IraK__Letre__0A338187] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                UNIQUEIDENTIFIER NULL,
    [ModositasIdo]               DATETIME         NULL,
    [Zarolo_id]                  UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                 DATETIME         NULL,
    [Tranz_id]                   UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KZT_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KezbesitesFek_KezbesitesTetelek_FK] FOREIGN KEY ([KezbesitesFej_Id]) REFERENCES [dbo].[EREC_IraKezbesitesiFejek] ([Id]),
    CONSTRAINT [KezbesitesiTelel_AtveteliFej_FK] FOREIGN KEY ([AtveteliFej_Id]) REFERENCES [dbo].[EREC_IraKezbesitesiFejek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KZT_UTSO_PRINT_ID]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([UtolsoNyomtatas_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KZT_UTSO_PRINT_DATUM]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([UtolsoNyomtatasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_CEL]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([Csoport_Id_Cel] ASC, [Allapot] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Obj_Id], [Obj_type]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_ATVEVO_USER]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([Felhasznalo_Id_AtvevoUser] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_ATADO_USER]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([Felhasznalo_Id_Atado_USER] ASC, [Allapot] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Obj_type]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KZT_ATVETEL_DATUMA]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([AtvetelDat] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Objektum]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([Obj_Id] ASC, [Obj_type] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Atvetel]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([AtveteliFej_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_AtadasDat]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([AtadasDat] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Atadas]
    ON [dbo].[EREC_IraKezbesitesiTetelek]([KezbesitesFej_Id] ASC) WITH (FILLFACTOR = 85);

