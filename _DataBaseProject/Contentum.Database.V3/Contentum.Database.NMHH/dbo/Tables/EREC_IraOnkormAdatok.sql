﻿CREATE TABLE [dbo].[EREC_IraOnkormAdatok] (
    [Id]                         UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IraOnko__Id__13BCEBC1] DEFAULT (newsequentialid()) NOT NULL,
    [IraIratok_Id]               UNIQUEIDENTIFIER NULL,
    [UgyFajtaja]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [DontestHozta]               NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [DontesFormaja]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [UgyintezesHataridore]       NVARCHAR (64)    NULL,
    [HataridoTullepes]           INT              NULL,
    [JogorvoslatiEljarasTipusa]  NVARCHAR (64)    NULL,
    [JogorvoslatiDontesTipusa]   NVARCHAR (64)    NULL,
    [JogorvoslatiDontestHozta]   NVARCHAR (64)    NULL,
    [JogorvoslatiDontesTartalma] NVARCHAR (64)    NULL,
    [JogorvoslatiDontes]         NVARCHAR (64)    NULL,
    [HatosagiEllenorzes]         CHAR (1)         NULL,
    [MunkaorakSzama]             FLOAT (53)       NULL,
    [EljarasiKoltseg]            INT              NULL,
    [KozigazgatasiBirsagMerteke] INT              NULL,
    [Ver]                        INT              CONSTRAINT [DF__EREC_IraOnk__Ver__14B10FFA] DEFAULT ((1)) NULL,
    [Note]                       NVARCHAR (4000)  NULL,
    [Stat_id]                    UNIQUEIDENTIFIER NULL,
    [ErvKezd]                    DATETIME         CONSTRAINT [DF__EREC_IraO__ErvKe__15A53433] DEFAULT (getdate()) NULL,
    [ErvVege]                    DATETIME         CONSTRAINT [DF_EREC_IraOnkormAdatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]               UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]              DATETIME         CONSTRAINT [DF__EREC_IraO__Letre__178D7CA5] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                UNIQUEIDENTIFIER NULL,
    [ModositasIdo]               DATETIME         NULL,
    [Zarolo_id]                  UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                 DATETIME         NULL,
    [Tranz_id]                   UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]             UNIQUEIDENTIFIER NULL,
    [SommasEljDontes]            NVARCHAR (64)    NULL,
    [NyolcNapBelulNemSommas]     NVARCHAR (64)    NULL,
    [FuggoHatalyuHatarozat]      NVARCHAR (64)    NULL,
    [FuggoHatHatalybaLepes]      NVARCHAR (64)    NULL,
    [FuggoHatalyuVegzes]         NVARCHAR (64)    NULL,
    [FuggoVegzesHatalyba]        NVARCHAR (64)    NULL,
    [HatAltalVisszafizOsszeg]    INT              NULL,
    [HatTerheloEljKtsg]          INT              NULL,
    [FelfuggHatarozat]           NVARCHAR (64)    NULL,
    CONSTRAINT [ONK_ID_PK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EREC_IRA_FK_EREC_I_EREC_IRA] FOREIGN KEY ([IraIratok_Id]) REFERENCES [dbo].[EREC_IraIratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Ugy_Id]
    ON [dbo].[EREC_IraOnkormAdatok]([IraIratok_Id] ASC);

