﻿CREATE TABLE [dbo].[EREC_IratAlairok] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IratAla__Id__27C3E46E] DEFAULT (newsequentialid()) NOT NULL,
    [PldIratPeldany_Id]              UNIQUEIDENTIFIER NULL,
    [Objtip_Id]                      UNIQUEIDENTIFIER NULL,
    [Obj_Id]                         UNIQUEIDENTIFIER NULL,
    [AlairasDatuma]                  DATETIME         NULL,
    [Leiras]                         NVARCHAR (4000)  NULL,
    [AlairoSzerep]                   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [AlairasSorrend]                 INT              CONSTRAINT [DF__EREC_Irat__Alair__28B808A7] DEFAULT ((1)) NULL,
    [FelhasznaloCsoport_Id_Alairo]   UNIQUEIDENTIFIER NOT NULL,
    [FelhaszCsoport_Id_Helyettesito] UNIQUEIDENTIFIER NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [AlairasMod]                     NVARCHAR (64)    CONSTRAINT [DF__EREC_Irat__Alair__29AC2CE0] DEFAULT ('0') NULL,
    [AlairasSzabaly_Id]              UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    NULL,
    [Ver]                            INT              CONSTRAINT [DF__EREC_IratAl__Ver__2AA05119] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__2B947552] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_IratAlairok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__2D7CBDC4] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [FelhaszCsop_Id_HelyettesAlairo] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [IRA_JH_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [IratAlairo_PldIratPeldany_FK] FOREIGN KEY ([PldIratPeldany_Id]) REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id]),
    CONSTRAINT [KRT_IRATALAIRO_ALAIRSZABALY_FK] FOREIGN KEY ([AlairasSzabaly_Id]) REFERENCES [dbo].[KRT_AlairasSzabalyok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id]
    ON [dbo].[EREC_IratAlairok]([Obj_Id] ASC) WITH (FILLFACTOR = 85);

