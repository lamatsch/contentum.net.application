﻿CREATE TABLE [dbo].[EREC_IratKapcsolatok] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IratKap__Id__6458BCB9] DEFAULT (newsequentialid()) NOT NULL,
    [KapcsolatTipus]    NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Leiras]            NVARCHAR (100)   NULL,
    [Kezi]              CHAR (1)         NULL,
    [Irat_Irat_Beepul]  UNIQUEIDENTIFIER NULL,
    [Irat_Irat_Felepul] UNIQUEIDENTIFIER NULL,
    [Ver]               INT              CONSTRAINT [DF__EREC_IratKa__Ver__654CE0F2] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__6641052B] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_EREC_IratKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__68294D9D] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_IRATKAPCSOLATOK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Irat_Irat_Beepules_FK] FOREIGN KEY ([Irat_Irat_Beepul]) REFERENCES [dbo].[EREC_IraIratok] ([Id]),
    CONSTRAINT [Irat_Irat_felepules] FOREIGN KEY ([Irat_Irat_Felepul]) REFERENCES [dbo].[EREC_IraIratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Felepul_ind]
    ON [dbo].[EREC_IratKapcsolatok]([Irat_Irat_Felepul] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Beepul_ind]
    ON [dbo].[EREC_IratKapcsolatok]([Irat_Irat_Beepul] ASC) WITH (FILLFACTOR = 85);

