﻿CREATE TABLE [dbo].[EREC_IratMellekletek] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [IraIrat_Id]         UNIQUEIDENTIFIER NOT NULL,
    [KuldMellekletek_Id] UNIQUEIDENTIFIER NULL,
    [AdathordozoTipus]   NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Megjegyzes]         NVARCHAR (400)   NULL,
    [SztornirozasDat]    DATETIME         NULL,
    [MennyisegiEgyseg]   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Mennyiseg]          INT              NOT NULL,
    [BarCode]            NVARCHAR (100)   NULL,
    [Ver]                INT              CONSTRAINT [DF__EREC_IratMe__Ver__38EE7070] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__39E294A9] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_EREC_IratMellekletek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__3BCADD1B] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [MEL_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [MEL_IRA_FK_I]
    ON [dbo].[EREC_IratMellekletek]([IraIrat_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [I_Barkod]
    ON [dbo].[EREC_IratMellekletek]([BarCode] ASC);

