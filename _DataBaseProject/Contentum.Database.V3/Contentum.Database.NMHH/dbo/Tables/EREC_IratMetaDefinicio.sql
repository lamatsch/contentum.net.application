﻿CREATE TABLE [dbo].[EREC_IratMetaDefinicio] (
    [Id]                       UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IratMet__Id__3EA749C6] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                      UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_IratMe__Org__3F9B6DFF] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Ugykor_Id]                UNIQUEIDENTIFIER NULL,
    [UgykorKod]                NVARCHAR (10)    NULL,
    [Ugytipus]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [UgytipusNev]              NVARCHAR (400)   NULL,
    [EljarasiSzakasz]          NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Irattipus]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [GeneraltTargy]            NVARCHAR (400)   NULL,
    [Rovidnev]                 NVARCHAR (100)   NULL,
    [UgyFajta]                 NVARCHAR (64)    NULL,
    [UgyiratIntezesiIdo]       INT              NULL,
    [Idoegyseg]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [UgyiratIntezesiIdoKotott] CHAR (1)         NULL,
    [UgyiratHataridoKitolas]   CHAR (1)         CONSTRAINT [DF__EREC_Irat__Ugyir__408F9238] DEFAULT ('1') NULL,
    [Ver]                      INT              CONSTRAINT [DF__EREC_IratMe__Ver__4183B671] DEFAULT ((1)) NULL,
    [Note]                     NVARCHAR (4000)  NULL,
    [Stat_id]                  UNIQUEIDENTIFIER NULL,
    [ErvKezd]                  DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__4277DAAA] DEFAULT (getdate()) NULL,
    [ErvVege]                  DATETIME         CONSTRAINT [DF_EREC_IratMetaDefinicio_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]             UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]            DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__4460231C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]              UNIQUEIDENTIFIER NULL,
    [ModositasIdo]             DATETIME         NULL,
    [Zarolo_id]                UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]               DATETIME         NULL,
    [Tranz_id]                 UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IRATMETADEFINICIO] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [CK_EREC_IratMetaDefinicio] CHECK ((0)=[dbo].[fn_EREC_IratMetaDefinicioCheckUK]([Org],[UgykorKod],[Ugytipus],[EljarasiSzakasz],[Irattipus],[Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [IratMeta_IrattariTetel_FK] FOREIGN KEY ([Ugykor_Id]) REFERENCES [dbo].[EREC_IraIrattariTetelek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [EREC_IratMetaDefinicio_UK]
    ON [dbo].[EREC_IratMetaDefinicio]([Org] ASC, [UgykorKod] ASC, [Ugytipus] ASC, [EljarasiSzakasz] ASC, [Irattipus] ASC) WITH (FILLFACTOR = 85);

