﻿CREATE TABLE [dbo].[EREC_Irat_Iktatokonyvei] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Irat_Ik__Id__1A69E950] DEFAULT (newsequentialid()) NOT NULL,
    [IraIktatokonyv_Id]   UNIQUEIDENTIFIER NOT NULL,
    [Csoport_Id_Iktathat] UNIQUEIDENTIFIER NOT NULL,
    [Ver]                 INT              CONSTRAINT [DF__EREC_Irat_I__Ver__1B5E0D89] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__1C5231C2] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_EREC_Irat_Iktatokonyvei_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__1E3A7A34] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KII_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [IratIktatoKonyv_Csoport_Iktatohely_FK] FOREIGN KEY ([Csoport_Id_Iktathat]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
    CONSTRAINT [KII_IKTK_FK] FOREIGN KEY ([IraIktatokonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [EREC_Irat_Iktatokonyv_UK]
    ON [dbo].[EREC_Irat_Iktatokonyvei]([IraIktatokonyv_Id] ASC, [Csoport_Id_Iktathat] ASC) WITH (FILLFACTOR = 85);

