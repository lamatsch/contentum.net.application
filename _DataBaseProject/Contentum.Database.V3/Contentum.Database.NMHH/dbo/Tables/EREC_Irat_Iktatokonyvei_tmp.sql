﻿CREATE TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Irat_Ik__Id__2116E6DF] DEFAULT (newsequentialid()) NOT NULL,
    [IraIktatokonyv_Id]   UNIQUEIDENTIFIER NOT NULL,
    [Csoport_Id_Iktathat] UNIQUEIDENTIFIER NOT NULL,
    [Ver]                 INT              CONSTRAINT [DF__EREC_Irat_I__Ver__220B0B18] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__22FF2F51] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_EREC_Irat_Iktatokonyvei_tmp_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__24E777C3] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KII_PK_tmp] PRIMARY KEY CLUSTERED ([Id] ASC)
);

