﻿CREATE TABLE [dbo].[EREC_IratelemKapcsolatok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Iratele__Id__30592A6F] DEFAULT (newsequentialid()) NOT NULL,
    [Melleklet_Id]   UNIQUEIDENTIFIER NULL,
    [Csatolmany_Id]  UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_Iratel__Ver__314D4EA8] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__324172E1] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_IratelemKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__3429BB53] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IRATELEMKAPCSOLATOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [CK_EREC_IratelemKapcsolatok] CHECK ((0)=[dbo].[fn_EREC_IratelemKapcsolatokCheckUK]([Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [EREC_CSATOLMANY_ELEMKAPCS_FK] FOREIGN KEY ([Csatolmany_Id]) REFERENCES [dbo].[EREC_Csatolmanyok] ([Id]),
    CONSTRAINT [EREC_MELLEKLET_ELEMKAPCS_FK] FOREIGN KEY ([Melleklet_Id]) REFERENCES [dbo].[EREC_Mellekletek] ([Id])
);

