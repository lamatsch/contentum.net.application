﻿CREATE TABLE [dbo].[EREC_IrattariKikero] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Irattar__Id__473C8FC7] DEFAULT (newsequentialid()) NOT NULL,
    [Keres_note]                     NVARCHAR (4000)  NULL,
    [FelhasznalasiCel]               CHAR (1)         NULL,
    [DokumentumTipus]                CHAR (1)         NULL,
    [Indoklas_note]                  NVARCHAR (400)   NULL,
    [FelhasznaloCsoport_Id_Kikero]   UNIQUEIDENTIFIER NULL,
    [KeresDatuma]                    DATETIME         NULL,
    [KulsoAzonositok]                NVARCHAR (100)   NULL,
    [UgyUgyirat_Id]                  UNIQUEIDENTIFIER NULL,
    [Tertiveveny_Id]                 UNIQUEIDENTIFIER NULL,
    [Obj_Id]                         UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]                      UNIQUEIDENTIFIER NULL,
    [KikerKezd]                      DATETIME         NULL,
    [KikerVege]                      DATETIME         NULL,
    [FelhasznaloCsoport_Id_Jovahagy] UNIQUEIDENTIFIER NULL,
    [JovagyasDatuma]                 DATETIME         NULL,
    [FelhasznaloCsoport_Id_Kiado]    UNIQUEIDENTIFIER NULL,
    [KiadasDatuma]                   DATETIME         NULL,
    [FelhasznaloCsoport_Id_Visszaad] UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Visszave] UNIQUEIDENTIFIER NULL,
    [VisszaadasDatuma]               DATETIME         NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [BarCode]                        NVARCHAR (100)   NULL,
    [Irattar_Id]                     UNIQUEIDENTIFIER NULL,
    [Ver]                            INT              CONSTRAINT [DF__EREC_Iratta__Ver__4830B400] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__4924D839] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_IrattariKikero_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__4B0D20AB] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IRATTARIKIKERO] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [IratKikero_Ugyirat_FK] FOREIGN KEY ([UgyUgyirat_Id]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [I_Barkod]
    ON [dbo].[EREC_IrattariKikero]([BarCode] ASC) WITH (FILLFACTOR = 85);

