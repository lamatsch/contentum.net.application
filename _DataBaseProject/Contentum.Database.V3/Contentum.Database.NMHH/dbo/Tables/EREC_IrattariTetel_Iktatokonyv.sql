﻿CREATE TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Irattar__Id__4DE98D56] DEFAULT (newsequentialid()) NOT NULL,
    [IrattariTetel_Id] UNIQUEIDENTIFIER NULL,
    [Iktatokonyv_Id]   UNIQUEIDENTIFIER NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_Iratta__Ver__4EDDB18F] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Irat__ErvKe__4FD1D5C8] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_IrattariTetel_Iktatokonyv_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Irat__Letre__51BA1E3A] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IRATTARITETEL_IKTATOKO] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Kapcs_Iktatokonyv_FK] FOREIGN KEY ([Iktatokonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id]),
    CONSTRAINT [Kapcs_IrattariTetel_FK] FOREIGN KEY ([IrattariTetel_Id]) REFERENCES [dbo].[EREC_IraIrattariTetelek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_2]
    ON [dbo].[EREC_IrattariTetel_Iktatokonyv]([IrattariTetel_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Index_1]
    ON [dbo].[EREC_IrattariTetel_Iktatokonyv]([Iktatokonyv_Id] ASC) WITH (FILLFACTOR = 85);

