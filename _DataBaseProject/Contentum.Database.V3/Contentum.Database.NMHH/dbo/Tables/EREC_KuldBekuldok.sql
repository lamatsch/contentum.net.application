﻿CREATE TABLE [dbo].[EREC_KuldBekuldok] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_KuldBek__Id__691D71D6] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NOT NULL,
    [Partner_Id_Bekuldo]    UNIQUEIDENTIFIER NULL,
    [PartnerCim_Id_Bekuldo] UNIQUEIDENTIFIER NULL,
    [NevSTR]                NVARCHAR (400)   NULL,
    [CimSTR]                NVARCHAR (400)   NULL,
    [Ver]                   INT              CONSTRAINT [DF__EREC_KuldBe__Ver__6A11960F] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__6B05BA48] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_EREC_KuldBekuldok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__6CEE02BA] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_KULD_BEKULDOK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KuldBekuldo_kuldemeny_FK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id]),
    CONSTRAINT [KuldBekuldo_Partner_FK] FOREIGN KEY ([Partner_Id_Bekuldo]) REFERENCES [dbo].[KRT_Partnerek] ([Id]),
    CONSTRAINT [KuldBekuldo_PartnerCim_FK] FOREIGN KEY ([PartnerCim_Id_Bekuldo]) REFERENCES [dbo].[KRT_Cimek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_PartnerCim_Id_Bekuldo]
    ON [dbo].[EREC_KuldBekuldok]([PartnerCim_Id_Bekuldo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Partner_Id_Bekuldo]
    ON [dbo].[EREC_KuldBekuldok]([Partner_Id_Bekuldo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_KuldKuldemeny_Id]
    ON [dbo].[EREC_KuldBekuldok]([KuldKuldemeny_Id] ASC) WITH (FILLFACTOR = 85);

