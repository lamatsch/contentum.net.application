﻿CREATE TABLE [dbo].[EREC_KuldDokumentumok] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Dokumentum_Id]    UNIQUEIDENTIFIER NOT NULL,
    [KuldKuldemeny_Id] UNIQUEIDENTIFIER NOT NULL,
    [Leiras]           NVARCHAR (100)   NULL,
    [Lapszam]          INT              NULL,
    [BarCode]          NVARCHAR (100)   NULL,
    [Forras]           NVARCHAR (64)    NULL,
    [Formatum]         NVARCHAR (64)    NULL,
    [Vonalkodozas]     CHAR (1)         NULL,
    [DokumentumSzerep] NVARCHAR (64)    NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_KuldDo__Ver__567ED357] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__5772F790] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_KuldDokumentumok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__595B4002] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KullDoku_PK] PRIMARY KEY NONCLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [I_Barcode]
    ON [dbo].[EREC_KuldDokumentumok]([BarCode] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_KulKuldemeny_Id]
    ON [dbo].[EREC_KuldDokumentumok]([KuldKuldemeny_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_Dokumentum_Id]
    ON [dbo].[EREC_KuldDokumentumok]([Dokumentum_Id] ASC);

