﻿CREATE TABLE [dbo].[EREC_KuldKapcsolatok] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_KuldKap__Id__6DE226F3] DEFAULT (newsequentialid()) NOT NULL,
    [KapcsolatTipus]    NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Leiras]            NVARCHAR (100)   NULL,
    [Kezi]              CHAR (1)         NULL,
    [Kuld_Kuld_Beepul]  UNIQUEIDENTIFIER NULL,
    [Kuld_Kuld_Felepul] UNIQUEIDENTIFIER NULL,
    [Ver]               INT              CONSTRAINT [DF__EREC_KuldKa__Ver__6ED64B2C] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__6FCA6F65] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_EREC_KuldKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__71B2B7D7] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_KULDKAPCSOLATOK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Kuldemeny_Kuldemeny_Beep_FK] FOREIGN KEY ([Kuld_Kuld_Beepul]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id]),
    CONSTRAINT [Kuldemeny_Kuldemeny_Felep_FK] FOREIGN KEY ([Kuld_Kuld_Felepul]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Felepul_ind]
    ON [dbo].[EREC_KuldKapcsolatok]([Kuld_Kuld_Felepul] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Beepul_ind]
    ON [dbo].[EREC_KuldKapcsolatok]([Kuld_Kuld_Beepul] ASC) WITH (FILLFACTOR = 85);

