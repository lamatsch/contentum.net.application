﻿CREATE TABLE [dbo].[EREC_KuldKezFeljegyzesek] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_KuldKez__Id__64CCF2AE] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id] UNIQUEIDENTIFIER NOT NULL,
    [KezelesTipus]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]           NVARCHAR (400)   NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_KuldKe__Ver__65C116E7] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__66B53B20] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_KuldKezFeljegyzesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__689D8392] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KKF_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KKKF_KUL_FK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KEZELESI_TIPUS_I]
    ON [dbo].[EREC_KuldKezFeljegyzesek]([KezelesTipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_KulKuldemeny_Id]
    ON [dbo].[EREC_KuldKezFeljegyzesek]([KuldKuldemeny_Id] ASC) WITH (FILLFACTOR = 85);

