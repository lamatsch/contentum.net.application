﻿CREATE TABLE [dbo].[EREC_KuldKuldemenyek] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_KuldKul__Id__6B79F03D] DEFAULT (newsequentialid()) NOT NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [BeerkezesIdeje]                 DATETIME         NOT NULL,
    [FelbontasDatuma]                DATETIME         NULL,
    [IraIktatokonyv_Id]              UNIQUEIDENTIFIER NULL,
    [KuldesMod]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Erkezteto_Szam]                 INT              NULL,
    [HivatkozasiSzam]                NVARCHAR (100)   NULL,
    [Targy]                          NVARCHAR (4000)  NULL,
    [Tartalom]                       NVARCHAR (4000)  NULL,
    [RagSzam]                        NVARCHAR (400)   NULL,
    [Surgosseg]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [BelyegzoDatuma]                 DATETIME         NULL,
    [UgyintezesModja]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [PostazasIranya]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Tovabbito]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [PeldanySzam]                    INT              NOT NULL,
    [IktatniKell]                    NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Iktathato]                      NVARCHAR (64)    NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [KuldKuldemeny_Id_Szulo]         UNIQUEIDENTIFIER NULL,
    [Erkeztetes_Ev]                  INT              NULL,
    [Csoport_Id_Cimzett]             UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Expedial] UNIQUEIDENTIFIER NULL,
    [ExpedialasIdeje]                DATETIME         NULL,
    [FelhasznaloCsoport_Id_Orzo]     UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Atvevo]   UNIQUEIDENTIFIER NULL,
    [Partner_Id_Bekuldo]             UNIQUEIDENTIFIER NULL,
    [Cim_Id]                         UNIQUEIDENTIFIER NULL,
    [CimSTR_Bekuldo]                 NVARCHAR (400)   NULL,
    [NevSTR_Bekuldo]                 NVARCHAR (400)   NULL,
    [AdathordozoTipusa]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [ElsodlegesAdathordozoTipusa]    NVARCHAR (64)    NULL,
    [FelhasznaloCsoport_Id_Alairo]   UNIQUEIDENTIFIER NULL,
    [BarCode]                        NVARCHAR (100)   NULL,
    [FelhasznaloCsoport_Id_Bonto]    UNIQUEIDENTIFIER NULL,
    [CsoportFelelosEloszto_Id]       UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [Kovetkezo_Felelos_Id]           UNIQUEIDENTIFIER NULL,
    [Elektronikus_Kezbesitesi_Allap] NVARCHAR (64)    NULL,
    [Kovetkezo_Orzo_Id]              UNIQUEIDENTIFIER NULL,
    [Fizikai_Kezbesitesi_Allapot]    NVARCHAR (64)    NULL,
    [IraIratok_Id]                   UNIQUEIDENTIFIER NULL,
    [BontasiMegjegyzes]              NVARCHAR (400)   NULL,
    [Tipus]                          NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Minosites]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [MegtagadasIndoka]               NVARCHAR (4000)  NULL,
    [Megtagado_Id]                   UNIQUEIDENTIFIER NULL,
    [MegtagadasDat]                  DATETIME         NULL,
    [KimenoKuldemenyFajta]           NVARCHAR (64)    NULL,
    [Elsobbsegi]                     CHAR (1)         NULL,
    [Ajanlott]                       CHAR (1)         NULL,
    [Tertiveveny]                    CHAR (1)         NULL,
    [SajatKezbe]                     CHAR (1)         NULL,
    [E_ertesites]                    CHAR (1)         NULL,
    [E_elorejelzes]                  CHAR (1)         NULL,
    [PostaiLezaroSzolgalat]          CHAR (1)         NULL,
    [Ar]                             NVARCHAR (100)   NULL,
    [KimenoKuld_Sorszam]             INT              NULL,
    [Ver]                            INT              CONSTRAINT [DF__EREC_KuldKu__Ver__6C6E1476] DEFAULT ((1)) NULL,
    [TovabbitasAlattAllapot]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [BoritoTipus]                    NVARCHAR (64)    NULL,
    [MegorzesJelzo]                  CHAR (1)         CONSTRAINT [DF__EREC_Kuld__Megor__6D6238AF] DEFAULT ('1') NULL,
    [KulsoAzonosito]                 NVARCHAR (400)   NULL,  
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__6E565CE8] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_KuldKuldemenyek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__703EA55A] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [IktatastNemIgenyel]             CHAR (1)         CONSTRAINT [DF__EREC_Kuld__Iktat__7132C993] DEFAULT ('0') NULL,
    [KezbesitesModja]                NVARCHAR (100)   NULL,
    [Munkaallomas]                   NVARCHAR (100)   NULL,
    [SerultKuldemeny]                CHAR (1)         CONSTRAINT [DF__EREC_Kuld__Serul__7226EDCC] DEFAULT ('0') NULL,
    [TevesCimzes]                    CHAR (1)         CONSTRAINT [DF__EREC_Kuld__Teves__731B1205] DEFAULT ('0') NULL,
    [TevesErkeztetes]                CHAR (1)         CONSTRAINT [DF__EREC_Kuld__Teves__740F363E] DEFAULT ('0') NULL,
    [CimzesTipusa]                   NVARCHAR (64)    NULL,
    [FutarJegyzekListaSzama]         NVARCHAR (100)   NULL,
    KezbesitoAtvetelIdopontja datetime             null,
    KezbesitoAtvevoNeve  Nvarchar(100)        null,
    [Minosito]                       UNIQUEIDENTIFIER NULL,
    [MinositesErvenyessegiIdeje]     DATETIME         NULL,
    [TerjedelemMennyiseg]            INT              NULL,
    [TerjedelemMennyisegiEgyseg]     NVARCHAR (64)    NULL,
    [TerjedelemMegjegyzes]           NVARCHAR (400)   NULL,
	[ModositasErvenyessegKezdete]	 DATETIME		  NULL,
	[MegszuntetoHatarozat]           NVARCHAR (400)   NULL,
	[FelulvizsgalatDatuma]			 DATETIME		  NULL,  
	[Felulvizsgalo_id]				 UNIQUEIDENTIFIER NULL,  
	[MinositesFelulvizsgalatEredm]	NVARCHAR (10)   NULL,
	[MinositoSzervezet]				UNIQUEIDENTIFIER	NULL,  
	[Partner_Id_BekuldoKapcsolt]	   UNIQUEIDENTIFIER	NULL,  
	[MinositesFelulvizsgalatBizTag] Nvarchar(4000),
    [Erteknyilvanitas] CHAR NULL, 
    [ErteknyilvanitasOsszege] INT NULL, 
    [IrattarId] UNIQUEIDENTIFIER NULL, 
    [IrattariHely] NVARCHAR(100) NULL, 
    CONSTRAINT [KuldKuldemenyek_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KUL_SZULO_FK] FOREIGN KEY ([KuldKuldemeny_Id_Szulo]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id]),
    CONSTRAINT [Kuldemeny_Iktatokonyv_FK] FOREIGN KEY ([IraIktatokonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
);






GO



GO



GO
CREATE NONCLUSTERED INDEX [TKUL_PRT_ID_ALAIRO]
    ON [dbo].[EREC_KuldKuldemenyek]([FelhasznaloCsoport_Id_Alairo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TKUL_BELYEGZO_DATUM]
    ON [dbo].[EREC_KuldKuldemenyek]([BelyegzoDatuma] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEST_KULD_PRT_ID_ORZO]
    ON [dbo].[EREC_KuldKuldemenyek]([FelhasznaloCsoport_Id_Orzo] ASC, [Allapot] ASC, [PostazasIranya] ASC, [ErvKezd] ASC, [ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEST_ELSO_ATVETIDO_I]
    ON [dbo].[EREC_KuldKuldemenyek]([BeerkezesIdeje] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KUL_TOVABBITO_SZERV_I]
    ON [dbo].[EREC_KuldKuldemenyek]([Tovabbito] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KUL_SZULO_FK_I]
    ON [dbo].[EREC_KuldKuldemenyek]([KuldKuldemeny_Id_Szulo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Ragszam]
    ON [dbo].[EREC_KuldKuldemenyek]([RagSzam] ASC, [ErvKezd] ASC, [ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Postazas_iranya]
    ON [dbo].[EREC_KuldKuldemenyek]([PostazasIranya] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_NevSTR]
    ON [dbo].[EREC_KuldKuldemenyek]([NevSTR_Bekuldo] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Partner_Id_Bekuldo], [Cim_Id], [CimSTR_Bekuldo]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Kuldemeny]
    ON [dbo].[EREC_KuldKuldemenyek]([ModositasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Letrehozo]
    ON [dbo].[EREC_KuldKuldemenyek]([Letrehozo_id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Kuldemeny_Partner_Id_Bekuldo]
    ON [dbo].[EREC_KuldKuldemenyek]([Partner_Id_Bekuldo] ASC, [NevSTR_Bekuldo] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Cim_Id], [CimSTR_Bekuldo]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Hivatkozsai_szam]
    ON [dbo].[EREC_KuldKuldemenyek]([HivatkozasiSzam] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Erkeztetoszam_UK]
    ON [dbo].[EREC_KuldKuldemenyek]([IraIktatokonyv_Id] ASC, [Erkezteto_Szam] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Allapot_MP]
    ON [dbo].[EREC_KuldKuldemenyek]([Allapot] ASC);


GO
CREATE NONCLUSTERED INDEX [I_PRT_ID_FELELOS]
    ON [dbo].[EREC_KuldKuldemenyek]([Csoport_Id_Felelos] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_PRT_ID_CIMZETT]
    ON [dbo].[EREC_KuldKuldemenyek]([Csoport_Id_Cimzett] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_IratId]
    ON [dbo].[EREC_KuldKuldemenyek]([IraIratok_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_Barcode]
    ON [dbo].[EREC_KuldKuldemenyek]([BarCode] ASC) WITH (FILLFACTOR = 85);

