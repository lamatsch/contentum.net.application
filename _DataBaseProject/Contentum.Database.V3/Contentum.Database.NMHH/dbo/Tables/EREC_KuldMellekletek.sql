﻿CREATE TABLE [dbo].[EREC_KuldMellekletek] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [KuldKuldemeny_Id] UNIQUEIDENTIFIER NOT NULL,
    [Mennyiseg]        INT              NOT NULL,
    [Megjegyzes]       NVARCHAR (400)   NULL,
    [SztornirozasDat]  DATETIME         NULL,
    [AdathordozoTipus] NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [MennyisegiEgyseg] NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [BarCode]          NVARCHAR (100)   NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_KuldMe__Ver__76EBA2E9] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__77DFC722] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_KuldMellekletek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__79C80F94] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KUM_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [KUM_KULD_FK_I]
    ON [dbo].[EREC_KuldMellekletek]([KuldKuldemeny_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [I_Barcode]
    ON [dbo].[EREC_KuldMellekletek]([BarCode] ASC);

