﻿CREATE TABLE [dbo].[EREC_KuldTertivevenyek] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_KuldTer__Id__7CA47C3F] DEFAULT (newsequentialid()) NOT NULL,
    [Kuldemeny_Id]       UNIQUEIDENTIFIER NULL,
    [Ragszam]            NVARCHAR (400)   NULL,
    [TertivisszaKod]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [TertivisszaDat]     DATETIME         NULL,
    [AtvevoSzemely]      NVARCHAR (400)   NULL,
    [AtvetelDat]         DATETIME         NULL,
    [BarCode]            NVARCHAR (100)   NULL,
    [Allapot]            NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]                INT              CONSTRAINT [DF__EREC_KuldTe__Ver__7D98A078] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__7E8CC4B1] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_EREC_KuldTertivevenyek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__00750D23] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    [KezbVelelemBeallta] CHAR (1)         CONSTRAINT [DF__EREC_Kuld__KezbV__0169315C] DEFAULT ('0') NULL,
    [KezbVelelemDatuma]  DATETIME         NULL,
    CONSTRAINT [KKT_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Tertiveveny_Kuldemeny_FK] FOREIGN KEY ([Kuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [I_Kuldemeny]
    ON [dbo].[EREC_KuldTertivevenyek]([Kuldemeny_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_Barcode]
    ON [dbo].[EREC_KuldTertivevenyek]([BarCode] ASC) WITH (FILLFACTOR = 85);

