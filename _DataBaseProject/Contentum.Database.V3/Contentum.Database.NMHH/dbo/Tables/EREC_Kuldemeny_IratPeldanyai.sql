﻿CREATE TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Kuldeme__Id__5C37ACAD] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id] UNIQUEIDENTIFIER NULL,
    [Peldany_Id]       UNIQUEIDENTIFIER NULL,
    [Ver]              INT              CONSTRAINT [DF__EREC_Kuldem__Ver__5D2BD0E6] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__EREC_Kuld__ErvKe__5E1FF51F] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_EREC_Kuldemeny_IratPeldanyai_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__EREC_Kuld__Letre__60083D91] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_KULDEMENY_IRATPELDANYA] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KuldemenyIratPeldany_IratPeldany_FK] FOREIGN KEY ([Peldany_Id]) REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id]),
    CONSTRAINT [KuldemenyIratPeldany_Kuldemeny_FK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Peldany_Id]
    ON [dbo].[EREC_Kuldemeny_IratPeldanyai]([Peldany_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_KuldKuldemeny_Id]
    ON [dbo].[EREC_Kuldemeny_IratPeldanyai]([KuldKuldemeny_Id] ASC) WITH (FILLFACTOR = 85);

