﻿CREATE TABLE [dbo].[EREC_Mellekletek] (
    [Id]                        UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Mellekl__Id__04459E07] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id]          UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]                UNIQUEIDENTIFIER NULL,
    [AdathordozoTipus]          NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Megjegyzes]                NVARCHAR (400)   NULL,
    [SztornirozasDat]           DATETIME         NULL,
    [MennyisegiEgyseg]          NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Mennyiseg]                 INT              NOT NULL,
    [BarCode]                   NVARCHAR (100)   NULL,
    [Ver]                       INT              CONSTRAINT [DF__EREC_Mellek__Ver__0539C240] DEFAULT ((1)) NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         CONSTRAINT [DF__EREC_Mell__ErvKe__062DE679] DEFAULT (getdate()) NULL,
    [ErvVege]                   DATETIME         CONSTRAINT [DF_EREC_Mellekletek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         CONSTRAINT [DF__EREC_Mell__Letre__08162EEB] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                DATETIME         NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    [MellekletAdathordozoTipus] NVARCHAR (64)    NULL,
    CONSTRAINT [PK_EREC_MELLEKLETEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_IRAT_MELLEKLET_FK] FOREIGN KEY ([IraIrat_Id]) REFERENCES [dbo].[EREC_IraIratok] ([Id]),
    CONSTRAINT [EREC_KULDEMENY_MELLEKLET_FK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Mellekletek_Irat_Id]
    ON [dbo].[EREC_Mellekletek]([IraIrat_Id] ASC, [ErvKezd] ASC, [ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Kuldemeny_Id]
    ON [dbo].[EREC_Mellekletek]([KuldKuldemeny_Id] ASC, [AdathordozoTipus] ASC, [BarCode] ASC) WITH (FILLFACTOR = 85);

