﻿CREATE TABLE [dbo].[EREC_ObjStateValue] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_ObjStat__Id__72A6DC10] DEFAULT (newsequentialid()) NOT NULL,
    [StateMetaDefinicio_Id] UNIQUEIDENTIFIER NULL,
    [ObjStateValue]         NVARCHAR (64)    NULL,
    [Ver]                   INT              CONSTRAINT [DF__EREC_ObjSta__Ver__739B0049] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__EREC_ObjS__ErvKe__748F2482] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__EREC_ObjS__Letre__758348BB] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_OBJSTATEVALUE] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [StateMetaDef_ObjStateVal_FK] FOREIGN KEY ([StateMetaDefinicio_Id]) REFERENCES [dbo].[EREC_StateMetaDefinicio] ([Id])
);

