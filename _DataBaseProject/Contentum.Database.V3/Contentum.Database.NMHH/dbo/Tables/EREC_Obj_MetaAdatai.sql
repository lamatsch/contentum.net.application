﻿CREATE TABLE [dbo].[EREC_Obj_MetaAdatai] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Obj_Met__Id__0AF29B96] DEFAULT (newsequentialid()) NOT NULL,
    [Obj_MetaDefinicio_Id]  UNIQUEIDENTIFIER NULL,
    [Targyszavak_Id]        UNIQUEIDENTIFIER NULL,
    [AlapertelmezettErtek]  NVARCHAR (100)   NULL,
    [Sorszam]               INT              NOT NULL,
    [Opcionalis]            CHAR (1)         NULL,
    [Ismetlodo]             CHAR (1)         NULL,
    [Funkcio]               NVARCHAR (100)   NULL,
    [ControlTypeSource]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [ControlTypeDataSource] NVARCHAR (4000)  NULL,
    [SPSSzinkronizalt]      CHAR (1)         CONSTRAINT [DF__EREC_Obj___SPSSz__0BE6BFCF] DEFAULT ('0') NULL,
    [SPS_Field_Id]          UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              CONSTRAINT [DF__EREC_Obj_Me__Ver__0CDAE408] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__EREC_Obj___ErvKe__0DCF0841] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_EREC_Obj_MetaAdatai_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_Id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__EREC_Obj___Letre__0FB750B3] DEFAULT (getdate()) NOT NULL,
    [Modosito_Id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_Id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_Id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_Id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KIRL_UGY_TARGYSZAVAI] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_OBJTARGYSZO_OBJMETADEF_FK] FOREIGN KEY ([Obj_MetaDefinicio_Id]) REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id]),
    CONSTRAINT [Obj_MetaAdatai_Targyszavak_FK] FOREIGN KEY ([Targyszavak_Id]) REFERENCES [dbo].[EREC_TargySzavak] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Targyszavak]
    ON [dbo].[EREC_Obj_MetaAdatai]([Targyszavak_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ObjMetadefinicio]
    ON [dbo].[EREC_Obj_MetaAdatai]([Obj_MetaDefinicio_Id] ASC) WITH (FILLFACTOR = 85);

