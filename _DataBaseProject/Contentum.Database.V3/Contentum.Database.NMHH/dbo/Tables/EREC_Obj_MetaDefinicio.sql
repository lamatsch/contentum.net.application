﻿CREATE TABLE [dbo].[EREC_Obj_MetaDefinicio] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Obj_Met__Id__1387E197] DEFAULT (newsequentialid()) NOT NULL,
    [Org]               UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Obj_Me__Org__147C05D0] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Objtip_Id]         UNIQUEIDENTIFIER NULL,
    [Objtip_Id_Column]  UNIQUEIDENTIFIER NULL,
    [ColumnValue]       NVARCHAR (100)   NULL,
    [DefinicioTipus]    NVARCHAR (64)    NULL,
    [Felettes_Obj_Meta] UNIQUEIDENTIFIER NULL,
    [MetaXSD]           XML              NULL,
    [ImportXML]         XML              NULL,
    [EgyebXML]          XML              NULL,
    [ContentType]       NVARCHAR (100)   NULL,
    [SPSSzinkronizalt]  CHAR (1)         CONSTRAINT [DF__EREC_Obj___SPSSz__15702A09] DEFAULT ('0') NULL,
    [SPS_CTT_Id]        NVARCHAR (400)   NULL,
    [WorkFlowVezerles]  CHAR (1)         CONSTRAINT [DF__EREC_Obj___WorkF__16644E42] DEFAULT ('0') NULL,
    [Ver]               INT              CONSTRAINT [DF__EREC_Obj_Me__Ver__1758727B] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__EREC_Obj___ErvKe__184C96B4] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_EREC_Obj_MetaDefinicio_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__EREC_Obj___Letre__1A34DF26] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_OBJ_METADEFINICIO] PRIMARY KEY NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_OBJMETA_FELETTESMETA_FK] FOREIGN KEY ([Felettes_Obj_Meta]) REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id]),
    CONSTRAINT [EREC_OBJMETA_OBJTIP_FK] FOREIGN KEY ([Objtip_Id]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id]),
    CONSTRAINT [EREC_OBJMETA_OBJTIPCOL_FK] FOREIGN KEY ([Objtip_Id_Column]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id]),
    CONSTRAINT [AK_OBJMETADEFINICIO] UNIQUE NONCLUSTERED ([Org] ASC, [ContentType] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [IX_ObjTipCol_FK]
    ON [dbo].[EREC_Obj_MetaDefinicio]([Objtip_Id_Column] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Objtip_FK]
    ON [dbo].[EREC_Obj_MetaDefinicio]([Objtip_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_FelettesMeta_FK]
    ON [dbo].[EREC_Obj_MetaDefinicio]([Felettes_Obj_Meta] ASC) WITH (FILLFACTOR = 85);

