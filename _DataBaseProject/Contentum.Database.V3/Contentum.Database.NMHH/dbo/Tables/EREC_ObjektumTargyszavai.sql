﻿CREATE TABLE [dbo].[EREC_ObjektumTargyszavai] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Objektu__Id__1D114BD1] DEFAULT (newsequentialid()) NOT NULL,
    [Targyszo_Id]           UNIQUEIDENTIFIER NULL,
    [Obj_Metaadatai_Id]     UNIQUEIDENTIFIER NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NOT NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NOT NULL,
    [Targyszo]              NVARCHAR (100)   NULL,
    [Forras]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SPSSzinkronizalt]      CHAR (1)         CONSTRAINT [DF__EREC_Obje__SPSSz__1E05700A] DEFAULT ('0') NULL,
    [Ertek]                 NVARCHAR (MAX)   NULL,
    [Sorszam]               INT              NULL,
    [Targyszo_XML]          XML              NULL,
    [Targyszo_XML_Ervenyes] CHAR (1)         NULL,
    [Ver]                   INT              CONSTRAINT [DF__EREC_Objekt__Ver__1EF99443] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__EREC_Obje__ErvKe__1FEDB87C] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_EREC_ObjektumTargyszavai_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__EREC_Obje__Letre__21D600EE] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_OBJEKTUMTARGYSZAVAI] PRIMARY KEY NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_OBJTARGYSZO_OBJMETA_FK] FOREIGN KEY ([Obj_Metaadatai_Id]) REFERENCES [dbo].[EREC_Obj_MetaAdatai] ([Id]),
    CONSTRAINT [ObjektumTargyszo_Targyszo_FK] FOREIGN KEY ([Targyszo_Id]) REFERENCES [dbo].[EREC_TargySzavak] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Obj_Objtip]
    ON [dbo].[EREC_ObjektumTargyszavai]([Obj_Id] ASC, [ObjTip_Id] ASC) WITH (FILLFACTOR = 85);

