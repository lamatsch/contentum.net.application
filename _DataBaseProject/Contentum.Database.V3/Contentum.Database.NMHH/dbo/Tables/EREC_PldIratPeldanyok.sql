﻿CREATE TABLE [dbo].[EREC_PldIratPeldanyok] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_PldIrat__Id__269AB60B] DEFAULT (newsequentialid()) NOT NULL,
    [IraIrat_Id]                     UNIQUEIDENTIFIER NOT NULL,
    [UgyUgyirat_Id_Kulso]            UNIQUEIDENTIFIER NULL,
    [Sorszam]                        INT              NOT NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [AtvetelDatuma]                  DATETIME         NULL,
    [Eredet]                         NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [KuldesMod]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ragszam]                        NVARCHAR (100)   NULL,
    [UgyintezesModja]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [VisszaerkezesiHatarido]         DATETIME         NULL,
    [Visszavarolag]                  NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [VisszaerkezesDatuma]            DATETIME         NULL,
    [Cim_id_Cimzett]                 UNIQUEIDENTIFIER NULL,
    [Partner_Id_Cimzett]             UNIQUEIDENTIFIER NULL,
	[Partner_Id_CimzettKapcsolt]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Orzo]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [CimSTR_Cimzett]                 NVARCHAR (400)   NULL,
    [NevSTR_Cimzett]                 NVARCHAR (400)   NULL,
    [Tovabbito]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [IraIrat_Id_Kapcsolt]            UNIQUEIDENTIFIER NULL,
    [IrattariHely]                   NVARCHAR (100)   NULL,
    [Gener_Id]                       UNIQUEIDENTIFIER NULL,
    [PostazasDatuma]                 DATETIME         NULL,
    [BarCode]                        NVARCHAR (100)   NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [Kovetkezo_Felelos_Id]           UNIQUEIDENTIFIER NULL,
    [Elektronikus_Kezbesitesi_Allap] NVARCHAR (64)    NULL,
    [Kovetkezo_Orzo_Id]              UNIQUEIDENTIFIER NULL,
    [Fizikai_Kezbesitesi_Allapot]    NVARCHAR (64)    NULL,
    [TovabbitasAlattAllapot]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [PostazasAllapot]                NVARCHAR (64)    NULL,
    [ValaszElektronikus]             CHAR (1)         NULL,
	SelejtezesDat        datetime             null,
   FelhCsoport_Id_Selejtezo uniqueidentifier     null,
   LeveltariAtvevoNeve  Nvarchar(100)        null,
    [Ver]                            INT              CONSTRAINT [DF__EREC_PldIra__Ver__278EDA44] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_PldI__ErvKe__2882FE7D] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_PldIratPeldanyok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_PldI__Letre__2A6B46EF] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [AKTIV]                          AS               (CONVERT([bit],case when [ALLAPOT]='90' then (0) else (1) end)),
    [IratPeldanyMegsemmisitve] CHAR NULL, 
    [IratPeldanyMegsemmisitesDatuma] DATETIME NULL, 
	IrattarId uniqueidentifier,  
    CONSTRAINT [PldIratpeldanyok__IratokSsz_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_PldIratpeldanyok_Ugyiratok_FK] FOREIGN KEY ([UgyUgyirat_Id_Kulso]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [IRP_IRA_FK] FOREIGN KEY ([IraIrat_Id]) REFERENCES [dbo].[EREC_IraIratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_PldIratPeldanyok_Aktiv]
    ON [dbo].[EREC_PldIratPeldanyok]([AKTIV] ASC, [Id] ASC);


GO
CREATE NONCLUSTERED INDEX [TIRP_ALLAPOT]
    ON [dbo].[EREC_PldIratPeldanyok]([Allapot] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KULDESMOD_I]
    ON [dbo].[EREC_PldIratPeldanyok]([KuldesMod] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ModositasIdo]
    ON [dbo].[EREC_PldIratPeldanyok]([ModositasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Iratpld_Sorszam]
    ON [dbo].[EREC_PldIratPeldanyok]([Sorszam] ASC, [FelhasznaloCsoport_Id_Orzo] ASC)
    INCLUDE([Id], [IraIrat_Id], [Allapot]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Iratpld_Kulso_ID]
    ON [dbo].[EREC_PldIratPeldanyok]([UgyUgyirat_Id_Kulso] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_FelhasznaloCsoport_Id_Orzo]
    ON [dbo].[EREC_PldIratPeldanyok]([FelhasznaloCsoport_Id_Orzo] ASC)
    INCLUDE([IraIrat_Id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ervkezd_ervvege]
    ON [dbo].[EREC_PldIratPeldanyok]([ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IRP_PRT_ID_CIMZETT_I]
    ON [dbo].[EREC_PldIratPeldanyok]([Partner_Id_Cimzett] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Iratpeldany_Letrehozo_I]
    ON [dbo].[EREC_PldIratPeldanyok]([Letrehozo_id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Iratpeldany_LetrehozasIdo_I]
    ON [dbo].[EREC_PldIratPeldanyok]([LetrehozasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IRA_KAPCSOLT_IND]
    ON [dbo].[EREC_PldIratPeldanyok]([IraIrat_Id_Kapcsolt] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_PRT_ID_FEL]
    ON [dbo].[EREC_PldIratPeldanyok]([Csoport_Id_Felelos] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_barcode]
    ON [dbo].[EREC_PldIratPeldanyok]([BarCode] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [EREC_PldIratPeldany_UK]
    ON [dbo].[EREC_PldIratPeldanyok]([IraIrat_Id] ASC, [Sorszam] ASC) WITH (FILLFACTOR = 85);

