﻿CREATE TABLE [dbo].[EREC_PldKapjakMeg] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_PldKapj__Id__2D47B39A] DEFAULT (newsequentialid()) NOT NULL,
    [PldIratPeldany_Id] UNIQUEIDENTIFIER NOT NULL,
    [Csoport_Id]        UNIQUEIDENTIFIER NOT NULL,
    [Ver]               INT              CONSTRAINT [DF__EREC_PldKap__Ver__2E3BD7D3] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__EREC_PldK__ErvKe__2F2FFC0C] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_EREC_PldKapjakMeg_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__EREC_PldK__Letre__3118447E] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_PLDKAPJAKMEG] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_KAPJAKMEG_CSOPORT_FK] FOREIGN KEY ([Csoport_Id]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
    CONSTRAINT [EREC_KAPJAKMEG_PLD_FK] FOREIGN KEY ([PldIratPeldany_Id]) REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
);

