﻿CREATE TABLE [dbo].[EREC_StateMetaDefinicio] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_StateMe__Id__76776CF4] DEFAULT (newsequentialid()) NOT NULL,
    [Obj_Metadefinicio_Id] UNIQUEIDENTIFIER NULL,
    [Obj_Id_StateColumn]   UNIQUEIDENTIFIER NULL,
    [Obj_StateColumnType]  NVARCHAR (64)    NULL,
    [Ver]                  INT              CONSTRAINT [DF__EREC_StateM__Ver__776B912D] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__EREC_Stat__ErvKe__785FB566] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__EREC_Stat__Letre__7953D99F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_STATEMETADEFINICIO] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EREC_STATEMETA_OBJMETADEF_FK] FOREIGN KEY ([Obj_Metadefinicio_Id]) REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id])
);

