﻿CREATE TABLE [dbo].[EREC_Szamlak] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Szamlak__Id__35DCF99B] DEFAULT (newsequentialid()) NOT NULL,
    [KuldKuldemeny_Id]    UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id]       UNIQUEIDENTIFIER NULL,
    [Partner_Id_Szallito] UNIQUEIDENTIFIER NULL,
    [Cim_Id_Szallito]     UNIQUEIDENTIFIER NULL,
	NevSTR_Szallito Nvarchar(400),  
    CimSTR_Szallito Nvarchar(400),
    [Bankszamlaszam_Id]   UNIQUEIDENTIFIER NULL,
    [SzamlaSorszam]       VARCHAR (50)     NOT NULL,
    [FizetesiMod]         VARCHAR (4)      NOT NULL,
    [TeljesitesDatuma]    SMALLDATETIME    NULL,
    [BizonylatDatuma]     SMALLDATETIME    NULL,
    [FizetesiHatarido]    SMALLDATETIME    NULL,
    [DevizaKod]           VARCHAR (3)      CONSTRAINT [DF__EREC_Szam__Deviz__36D11DD4] DEFAULT ('HUF') NULL,
    [VisszakuldesDatuma]  SMALLDATETIME    NULL,
    [EllenorzoOsszeg]     FLOAT (53)       NOT NULL,
    [PIRAllapot]          CHAR (1)         NULL,
    [Ver]                 INT              CONSTRAINT [DF__EREC_Szamla__Ver__37C5420D] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__EREC_Szam__ErvKe__38B96646] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_EREC_Szamlak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__EREC_Szam__Letre__3AA1AEB8] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    [VevoBesorolas]       NVARCHAR (20)    NULL,
    CONSTRAINT [PK_EREC_SZAMLAK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK] FOREIGN KEY ([KuldKuldemeny_Id]) REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id]),
    CONSTRAINT [FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK] FOREIGN KEY ([Bankszamlaszam_Id]) REFERENCES [dbo].[KRT_Bankszamlaszamok] ([Id]),
    CONSTRAINT [FK_EREC_SZAMLAK_KRT_CIMEK] FOREIGN KEY ([Cim_Id_Szallito]) REFERENCES [dbo].[KRT_Cimek] ([Id]),
    CONSTRAINT [FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK] FOREIGN KEY ([Dokumentum_Id]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id]),
    CONSTRAINT [FK_EREC_SZAMLAK_KRT_PARTNEREK] FOREIGN KEY ([Partner_Id_Szallito]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Szamlak_KuldemenyId]
    ON [dbo].[EREC_Szamlak]([KuldKuldemeny_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Szamlak_DokumentumId]
    ON [dbo].[EREC_Szamlak]([Dokumentum_Id] ASC);

