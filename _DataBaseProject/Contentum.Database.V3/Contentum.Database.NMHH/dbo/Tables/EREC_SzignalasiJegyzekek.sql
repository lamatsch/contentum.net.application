﻿CREATE TABLE [dbo].[EREC_SzignalasiJegyzekek] (
    [Id]                      UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Szignal__Id__3D7E1B63] DEFAULT (newsequentialid()) NOT NULL,
    [UgykorTargykor_Id]       UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]      UNIQUEIDENTIFIER NULL,
    [SzignalasTipusa]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SzignalasiKotelezettseg] NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
	[HataridosFeladatId] UNIQUEIDENTIFIER NULL, 
    [Ver]                     INT              CONSTRAINT [DF__EREC_Szigna__Ver__3E723F9C] DEFAULT ((1)) NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         CONSTRAINT [DF__EREC_Szig__ErvKe__3F6663D5] DEFAULT (getdate()) NULL,
    [ErvVege]                 DATETIME         CONSTRAINT [DF_EREC_SzignalasiJegyzekek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         CONSTRAINT [DF__EREC_Szig__Letre__414EAC47] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL
    CONSTRAINT [PK_EREC_SZIGNALASIJEGYZEKEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Szagnalas_Csoport_FK] FOREIGN KEY ([Csoport_Id_Felelos]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
    CONSTRAINT [Szignalas_Iratmeta_FK] FOREIGN KEY ([UgykorTargykor_Id]) REFERENCES [dbo].[EREC_IratMetaDefinicio] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_2]
    ON [dbo].[EREC_SzignalasiJegyzekek]([UgykorTargykor_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Index_1]
    ON [dbo].[EREC_SzignalasiJegyzekek]([Csoport_Id_Felelos] ASC) WITH (FILLFACTOR = 85);

