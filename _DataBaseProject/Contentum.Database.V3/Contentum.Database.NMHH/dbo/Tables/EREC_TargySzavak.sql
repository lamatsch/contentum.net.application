﻿CREATE TABLE [dbo].[EREC_TargySzavak] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_TargySz__Id__442B18F2] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Tipus]                 CHAR (1)         NULL,
    [TargySzavak]           NVARCHAR (100)   NOT NULL,
    [AlapertelmezettErtek]  NVARCHAR (100)   NULL,
    [BelsoAzonosito]        NVARCHAR (100)   NULL,
    [SPSSzinkronizalt]      CHAR (1)         CONSTRAINT [DF__EREC_Targ__SPSSz__451F3D2B] DEFAULT ('0') NULL,
    [SPS_Field_Id]          UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Tulaj]      UNIQUEIDENTIFIER NULL,
    [RegExp]                NVARCHAR (4000)  NULL,
    [ToolTip]               NVARCHAR (400)   NULL,
    [ControlTypeSource]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [ControlTypeDataSource] NVARCHAR (4000)  NULL,
	[Targyszo_Id_Parent]    UNIQUEIDENTIFIER NULL,
    [XSD]                   XML              NULL,
    [Ver]                   INT              CONSTRAINT [DF__EREC_TargyS__Ver__46136164] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__EREC_Targ__ErvKe__4707859D] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_EREC_TargySzavak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__EREC_Targ__Letre__48EFCE0F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [TRSZ_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Targyszavak_Csoport_Tulaj_FK] FOREIGN KEY ([Csoport_Id_Tulaj]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
	CONSTRAINT [Targyszavak_Targyszo_FK] FOREIGN KEY ([Targyszo_Id_Parent]) REFERENCES [dbo].[EREC_TargySzavak] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [TRSZ_PRT_FK_I]
    ON [dbo].[EREC_TargySzavak]([Csoport_Id_Tulaj] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [EREC_TargySzavak_UK]
    ON [dbo].[EREC_TargySzavak]([Org] ASC, [TargySzavak] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [EREC_BelsoAzonosito_UK]
    ON [dbo].[EREC_TargySzavak]([Org] ASC, [BelsoAzonosito] ASC) WITH (FILLFACTOR = 85);

