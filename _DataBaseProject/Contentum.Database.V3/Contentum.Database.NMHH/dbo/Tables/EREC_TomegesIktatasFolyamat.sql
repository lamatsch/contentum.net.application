/* if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_TomegesIktatasFolyamat')
            and   type = 'U')
   drop table EREC_TomegesIktatasFolyamat
go */

/*==============================================================*/
/* Table: EREC_TomegesIktatasFolyamat                           */
/*==============================================================*/
create table EREC_TomegesIktatasFolyamat (
   Id                   uniqueidentifier     not null default newsequentialid(),
   Forras               Nvarchar(100)        null,
   Forras_Azonosito     Nvarchar(100)        null,
   Prioritas            nvarchar(64)         null default '2',
   RQ_Dokumentum_Id     uniqueidentifier     null,
   RS_Dokumentum_Id     uniqueidentifier     null,
   Allapot              nvarchar(64)         COLLATE Hungarian_CS_AS null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_EREC_TOMEGESIKTATASFOLYAMAT primary key (Id)
)
go

alter table EREC_TomegesIktatasFolyamat 
  ADD CONSTRAINT
	DF_EREC_TomegesIktatasFolyamat_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go
