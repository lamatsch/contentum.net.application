/* if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_TomegesIktatasTetelek')
            and   type = 'U')
   drop table EREC_TomegesIktatasTetelek
go */

/*==============================================================*/
/* Table: EREC_TomegesIktatasTetelek                            */
/*==============================================================*/
create table EREC_TomegesIktatasTetelek (
   Id                   uniqueidentifier     not null default newsequentialid(),
   Folyamat_Id          uniqueidentifier     null,
   Forras_Azonosito     Nvarchar(100)        null,
   IktatasTipus         int                  null,
   Alszamra             int                  null,
   Iktatokonyv_Id       uniqueidentifier     null,
   Ugyirat_Id           uniqueidentifier     null,
   Kuldemeny_Id         uniqueidentifier     null,
   ExecParam            Nvarchar(Max)        null,
   EREC_UgyUgyiratok    Nvarchar(Max)        null,
   EREC_UgyUgyiratdarabok Nvarchar(Max)        null,
   EREC_IraIratok       Nvarchar(Max)        null,
   EREC_PldIratPeldanyok Nvarchar(Max)        null,
   EREC_HataridosFeladatok Nvarchar(Max)        null,
   EREC_KuldKuldemenyek Nvarchar(Max)        null,
   IktatasiParameterek  Nvarchar(Max)        null,
   ErkeztetesParameterek Nvarchar(Max)        null,
   Dokumentumok         Nvarchar(Max)        null,
   Azonosito            Nvarchar(100)        null,
   Allapot              nvarchar(64)         null,
   Irat_Id              uniqueidentifier     null,
    Expedialas           int                  null,
   TobbIratEgyKuldemenybe int                  null,
   KuldemenyAtadas      uniqueidentifier     null,
   Hiba                 Nvarchar(4000)       null,
   HatosagiStatAdat Nvarchar(Max) null,  
   EREC_IratAlairok Nvarchar(Max) null,  
   Lezarhato int null,  
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_EREC_TOMEGESIKTATASTETELEK primary key (Id)
)
go

alter table EREC_TomegesIktatasTetelek 
  ADD CONSTRAINT
	DF_EREC_TomegesIktatasTetelek_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go

alter table EREC_TomegesIktatasTetelek
   add constraint EREC_TomegesIktatasTetelek_Folyamat_FK foreign key (Folyamat_Id)
      references EREC_TomegesIktatasFolyamat (Id)
go

alter table EREC_TomegesIktatasTetelek
   add constraint EREC_TomegesIktatasTetelek_Iktatokonyv_FK foreign key (Iktatokonyv_Id)
      references EREC_IraIktatoKonyvek (Id)
go

alter table EREC_TomegesIktatasTetelek
   add constraint EREC_TomegesIktatasTetelek_Irat_FK foreign key (Irat_Id)
      references EREC_IraIratok (Id)
go

alter table EREC_TomegesIktatasTetelek
   add constraint EREC_TomegesIktatasTetelek_Kuldemeny_FK foreign key (Kuldemeny_Id)
      references EREC_KuldKuldemenyek (Id)
go

alter table EREC_TomegesIktatasTetelek
   add constraint EREC_TomegesIktatasTetelek_Ugyirat_FK foreign key (Ugyirat_Id)
      references EREC_UgyUgyiratok (Id)
go


CREATE INDEX [IX_EREC_TomegesIktatasTetelek_Azonostio] ON [dbo].[EREC_TomegesIktatasTetelek] ([Azonosito] ASC)

go