﻿CREATE TABLE [dbo].[EREC_UgyKezFeljegyzesek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_UgyKezF__Id__546180BB] DEFAULT (newsequentialid()) NOT NULL,
    [UgyUgyirat_Id]  UNIQUEIDENTIFIER NOT NULL,
    [KezelesTipus]   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]         NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_UgyKez__Ver__5555A4F4] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_UgyK__ErvKe__5649C92D] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_UgyKezFeljegyzesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_UgyK__Letre__5832119F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KUF_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KUF_EIV_FK] FOREIGN KEY ([UgyUgyirat_Id]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [TKUF_KEZELESI_TIPUS_I]
    ON [dbo].[EREC_UgyKezFeljegyzesek]([KezelesTipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Letrehozo_I]
    ON [dbo].[EREC_UgyKezFeljegyzesek]([Letrehozo_id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_UgyUgyirat_Id]
    ON [dbo].[EREC_UgyKezFeljegyzesek]([UgyUgyirat_Id] ASC) WITH (FILLFACTOR = 85);

