﻿CREATE TABLE [dbo].[EREC_UgyUgyiratdarabok] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_UgyUgyi__Id__5B0E7E4A] DEFAULT (newsequentialid()) NOT NULL,
    [UgyUgyirat_Id]                  UNIQUEIDENTIFIER NOT NULL,
    [EljarasiSzakasz]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Sorszam]                        INT              NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [Hatarido]                       DATETIME         NULL,
    [FelhasznaloCsoport_Id_Ugyintez] UNIQUEIDENTIFIER NULL,
    [ElintezesDat]                   DATETIME         NULL,
    [LezarasDat]                     DATETIME         NULL,
    [ElintezesMod]                   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [LezarasOka]                     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]                         NVARCHAR (4000)  NULL,
    [UgyUgyirat_Id_Elozo]            UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id]              UNIQUEIDENTIFIER NULL,
    [Foszam]                         INT              NULL,
    [UtolsoAlszam]                   INT              NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [IratMetadefinicio_Id]           UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]                            INT              CONSTRAINT [DF__EREC_UgyUgy__Ver__5C02A283] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_UgyU__ErvKe__5CF6C6BC] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_UgyUgyiratdarabok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_UgyU__Letre__5EDF0F2E] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KIRL_UGY_UGYIRATDARABOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [UgyiratDarab_Ugy_FK] FOREIGN KEY ([UgyUgyirat_Id]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [UgyUgyiratDarab_IraIktatokonyv_FK] FOREIGN KEY ([IraIktatokonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IND_UDB_UGY_ID]
    ON [dbo].[EREC_UgyUgyiratdarabok]([UgyUgyirat_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_EljarasiSzakasz_UK]
    ON [dbo].[EREC_UgyUgyiratdarabok]([UgyUgyirat_Id] ASC, [LetrehozasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_IktatokonyvId]
    ON [dbo].[EREC_UgyUgyiratdarabok]([IraIktatokonyv_Id] ASC) WITH (FILLFACTOR = 85);

