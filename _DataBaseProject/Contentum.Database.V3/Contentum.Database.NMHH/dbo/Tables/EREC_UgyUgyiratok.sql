﻿CREATE TABLE [dbo].[EREC_UgyUgyiratok] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_UgyUgyi__Id__61BB7BD9] DEFAULT (newsequentialid()) NOT NULL,
    [Foszam]                         INT              NULL,
    [Sorszam]                        INT              NULL,
    [Ugyazonosito]                   NVARCHAR (100)   NULL,
    [Alkalmazas_Id]                  UNIQUEIDENTIFIER NULL,
    [UgyintezesModja]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Hatarido]                       DATETIME         NULL,
    [SkontrobaDat]                   DATETIME         NULL,
    [LezarasDat]                     DATETIME         NULL,
    [IrattarbaKuldDatuma]            DATETIME         NULL,
    [IrattarbaVetelDat]              DATETIME         NULL,
    [FelhCsoport_Id_IrattariAtvevo]  UNIQUEIDENTIFIER NULL,
    [SelejtezesDat]                  DATETIME         NULL,
    [FelhCsoport_Id_Selejtezo]       UNIQUEIDENTIFIER NULL,
    [LeveltariAtvevoNeve]            NVARCHAR (100)   NULL,
    [FelhCsoport_Id_Felulvizsgalo]   UNIQUEIDENTIFIER NULL,
    [FelulvizsgalatDat]              DATETIME         NULL,
    [IktatoszamKieg]                 NVARCHAR (2)     NULL,
    [Targy]                          NVARCHAR (4000)  NULL,
    [UgyUgyirat_Id_Szulo]            UNIQUEIDENTIFIER NULL,
    [UgyUgyirat_Id_Kulso]            UNIQUEIDENTIFIER NULL,
    [UgyTipus]                       NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [IrattariHely]                   NVARCHAR (100)   NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Orzo]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Cimzett]             UNIQUEIDENTIFIER NULL,
    [Jelleg]                         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [IraIrattariTetel_Id]            UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id]              UNIQUEIDENTIFIER NULL,
    [SkontroOka]                     NVARCHAR (400)   NULL,
    [SkontroVege]                    DATETIME         NULL,
    [Surgosseg]                      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SkontrobanOsszesen]             INT              NULL,
    [MegorzesiIdoVege]               DATETIME         CONSTRAINT [DF_EREC_UgyUgyiratok_MegorzesiIdoVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Partner_Id_Ugyindito]           UNIQUEIDENTIFIER NULL,
    [NevSTR_Ugyindito]               NVARCHAR (400)   NULL,
    [ElintezesDat]                   DATETIME         NULL,
    [FelhasznaloCsoport_Id_Ugyintez] UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [KolcsonKikerDat]                DATETIME         CONSTRAINT [DF__EREC_UgyU__Kolcs__63A3C44B] DEFAULT (NULL) NULL,
    [KolcsonKiadDat]                 DATETIME         CONSTRAINT [DF__EREC_UgyU__Kolcs__6497E884] DEFAULT (NULL) NULL,
    [Kolcsonhatarido]                DATETIME         CONSTRAINT [DF__EREC_UgyU__Kolcs__658C0CBD] DEFAULT (NULL) NULL,
    [BARCODE]                        NVARCHAR (100)   NULL,
    [IratMetadefinicio_Id]           UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [TovabbitasAlattAllapot]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Megjegyzes]                     NVARCHAR (400)   NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [Fizikai_Kezbesitesi_Allapot]    NVARCHAR (64)    NULL,
    [Kovetkezo_Orzo_Id]              UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Ugyfelelos]          UNIQUEIDENTIFIER NULL,
    [Elektronikus_Kezbesitesi_Allap] NVARCHAR (64)    NULL,
    [Kovetkezo_Felelos_Id]           UNIQUEIDENTIFIER NULL,
    [UtolsoAlszam]                   INT              NULL,
    [UtolsoSorszam]                  INT              NULL,
    [IratSzam]                       INT              NULL,
    [ElintezesMod]                   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [RegirendszerIktatoszam]         NVARCHAR (100)   NULL,
    [GeneraltTargy]                  NVARCHAR (400)   NULL,
    [Cim_Id_Ugyindito]               UNIQUEIDENTIFIER NULL,
    [CimSTR_Ugyindito]               NVARCHAR (400)   NULL,
   
    [UjOrzesiIdo]                    INT              NULL,
	 UjOrzesiIdoIdoegyseg			nvarchar(64), 
    [IrattarId]                      UNIQUEIDENTIFIER NULL,
    [LezarasOka]                     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SkontroOka_Kod]                 NVARCHAR (64)    NULL,
    [UgyintezesKezdete]              DATETIME         NULL,
    [FelfuggesztettNapokSzama]       INT              NULL,
    [IntezesiIdo]                    NVARCHAR(64)              NULL,
    [IntezesiIdoegyseg]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ugy_Fajtaja]                    NVARCHAR (64)    NULL,
    [FelfuggesztesOka]               NVARCHAR (400)   COLLATE Hungarian_CS_AS NULL,
	SzignaloId uniqueidentifier,  
	SzignalasIdeje datetime, 
    [AKTIV]                          AS               (CONVERT([bit],case when [ALLAPOT]='90' then (0) else (1) end)),
    [HatralevoNapok]			     AS (([dbo].[FN_UgyUgyirat_HatralevoNapok](GETDATE(),[Hatarido]))),
    [HatralevoMunkaNapok] 			 AS (([dbo].[FN_UgyUgyirat_HatralevoMunkaNapok](GETDATE(),[Hatarido]))),
    ElteltIdo						 nvarchar(64)         null default '1',
    ElteltIdoIdoEgyseg				 nvarchar(64)         null,
    ElteltidoAllapot				 int                  null default 0,
    SakkoraAllapot					 nvarchar(64)         null,
	[ElozoAllapot]                   NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
	[IrattarHelyfoglalas] 		 	 FLOAT NULL,
    [Ver]                            INT              CONSTRAINT [DF__EREC_UgyUgy__Ver__668030F6] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__EREC_UgyU__ErvKe__6774552F] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_EREC_UgyUgyiratok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__EREC_UgyU__Letre__695C9DA1] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
	
	[ElteltIdoUtolsoModositas] DATETIME2 NULL, 
    CONSTRAINT [EIV_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [EIV_EIV_FK] FOREIGN KEY ([UgyUgyirat_Id_Szulo]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK] FOREIGN KEY ([UgyUgyirat_Id_Kulso]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [Ugy_Csoport_Felelos_FK] FOREIGN KEY ([Csoport_Id_Felelos]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
    CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK] FOREIGN KEY ([IraIktatokonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratok_Aktiv]
    ON [dbo].[EREC_UgyUgyiratok]([AKTIV] ASC, [Id] ASC);


GO
CREATE NONCLUSTERED INDEX [UgyUgyirat_LetrehozasIdo_I]
    ON [dbo].[EREC_UgyUgyiratok]([LetrehozasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_SURGOSSEG_I]
    ON [dbo].[EREC_UgyUgyiratok]([Surgosseg] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_SKONTROBADATUM_I]
    ON [dbo].[EREC_UgyUgyiratok]([SkontrobaDat] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_PRT_ID_ORZO_I]
    ON [dbo].[EREC_UgyUgyiratok]([FelhasznaloCsoport_Id_Orzo] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id], [Allapot], [TovabbitasAlattAllapot]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_KATEGORIA]
    ON [dbo].[EREC_UgyUgyiratok]([UgyTipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_IRATTARI_HELY]
    ON [dbo].[EREC_UgyUgyiratok]([IrattariHely] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TEIV_HATARIDO_I]
    ON [dbo].[EREC_UgyUgyiratok]([Hatarido] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Ugyirat_Kulso_ID]
    ON [dbo].[EREC_UgyUgyiratok]([UgyUgyirat_Id_Kulso] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Ugyirat_CimSTR_Ugyindito]
    ON [dbo].[EREC_UgyUgyiratok]([CimSTR_Ugyindito] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Ugyirat_Cim_Id_Ugyindito]
    ON [dbo].[EREC_UgyUgyiratok]([Cim_Id_Ugyindito] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Ugyinteto_Id]
    ON [dbo].[EREC_UgyUgyiratok]([FelhasznaloCsoport_Id_Ugyintez] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Ugyirat]
    ON [dbo].[EREC_UgyUgyiratok]([ModositasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Foszam]
    ON [dbo].[EREC_UgyUgyiratok]([Foszam] ASC, [Allapot] ASC, [ErvKezd] ASC, [ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_IRATTARI_TETELSZAM]
    ON [dbo].[EREC_UgyUgyiratok]([IraIrattariTetel_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [I_barkod]
    ON [dbo].[EREC_UgyUgyiratok]([BARCODE] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_UgyUgyirat_Id_Szulo]
    ON [dbo].[EREC_UgyUgyiratok]([UgyUgyirat_Id_Szulo] ASC)
    INCLUDE([Id], [Foszam], [Ugyazonosito], [Alkalmazas_Id], [UgyintezesModja], [Hatarido], [SkontrobaDat], [LezarasDat], [IrattarbaKuldDatuma], [IrattarbaVetelDat], [FelhCsoport_Id_IrattariAtvevo], [SelejtezesDat], [FelhCsoport_Id_Selejtezo], [LeveltariAtvevoNeve], [FelhCsoport_Id_Felulvizsgalo], [FelulvizsgalatDat], [IktatoszamKieg], [Targy], [UgyUgyirat_Id_Kulso], [UgyTipus], [IrattariHely], [SztornirozasDat], [Csoport_Id_Felelos], [FelhasznaloCsoport_Id_Orzo], [Csoport_Id_Cimzett], [Jelleg], [IraIrattariTetel_Id], [IraIktatokonyv_Id], [SkontroOka], [SkontroVege], [Surgosseg], [SkontrobanOsszesen], [MegorzesiIdoVege], [Partner_Id_Ugyindito], [NevSTR_Ugyindito], [ElintezesDat], [FelhasznaloCsoport_Id_Ugyintez], [Csoport_Id_Felelos_Elozo], [KolcsonKikerDat], [KolcsonKiadDat], [Kolcsonhatarido], [BARCODE], [IratMetadefinicio_Id], [Allapot], [TovabbitasAlattAllapot], [Megjegyzes], [Azonosito], [Fizikai_Kezbesitesi_Allapot], [Kovetkezo_Orzo_Id], [Csoport_Id_Ugyfelelos], [Elektronikus_Kezbesitesi_Allap], [Kovetkezo_Felelos_Id], [UtolsoAlszam], [IratSzam], [ElintezesMod], [RegirendszerIktatoszam], [GeneraltTargy], [Cim_Id_Ugyindito], [CimSTR_Ugyindito], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Csoport_Id_Felelos]
    ON [dbo].[EREC_UgyUgyiratok]([Csoport_Id_Felelos] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [EIV_FOSZAM_UK]
    ON [dbo].[EREC_UgyUgyiratok]([IraIktatokonyv_Id] ASC, [Foszam] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [ALLAPOT_I]
    ON [dbo].[EREC_UgyUgyiratok]([Allapot] ASC, [ErvKezd] ASC, [ErvVege] ASC, [ModositasIdo] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 85);

