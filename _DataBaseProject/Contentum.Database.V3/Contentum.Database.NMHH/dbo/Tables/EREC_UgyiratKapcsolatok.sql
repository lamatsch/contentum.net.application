﻿CREATE TABLE [dbo].[EREC_UgyiratKapcsolatok] (
    [Id]                      UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Ugyirat__Id__7A47FDD8] DEFAULT (newsequentialid()) NOT NULL,
    [KapcsolatTipus]          NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Leiras]                  NVARCHAR (100)   NULL,
    [Kezi]                    CHAR (1)         NULL,
    [Ugyirat_Ugyirat_Beepul]  UNIQUEIDENTIFIER NULL,
    [Ugyirat_Ugyirat_Felepul] UNIQUEIDENTIFIER NULL,
    [Ver]                     INT              CONSTRAINT [DF__EREC_Ugyira__Ver__7B3C2211] DEFAULT ((1)) NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         CONSTRAINT [DF__EREC_Ugyi__ErvKe__7C30464A] DEFAULT (getdate()) NULL,
    [ErvVege]                 DATETIME         CONSTRAINT [DF_EREC_UgyiratKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         CONSTRAINT [DF__EREC_Ugyi__Letre__7E188EBC] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_UGYIRATKAPCS_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [UgyiratKapcs_Ugyirat_Beep_FK] FOREIGN KEY ([Ugyirat_Ugyirat_Beepul]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id]),
    CONSTRAINT [UgyiratKapcs_Ugyirat_Felep_FK] FOREIGN KEY ([Ugyirat_Ugyirat_Felepul]) REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Felep_ind]
    ON [dbo].[EREC_UgyiratKapcsolatok]([Ugyirat_Ugyirat_Felepul] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Dup_UK]
    ON [dbo].[EREC_UgyiratKapcsolatok]([Ugyirat_Ugyirat_Beepul] ASC, [Ugyirat_Ugyirat_Felepul] ASC, [KapcsolatTipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Beep_ind]
    ON [dbo].[EREC_UgyiratKapcsolatok]([Ugyirat_Ugyirat_Beepul] ASC) WITH (FILLFACTOR = 85);

