﻿CREATE TABLE [dbo].[EREC_UgyiratObjKapcsolatok] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_Ugyirat__Id__4DB4832C] DEFAULT (newsequentialid()) NOT NULL,
    [KapcsolatTipus]      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]              NVARCHAR (100)   NULL,
    [Kezi]                CHAR (1)         NULL,
    [Obj_Id_Elozmeny]     UNIQUEIDENTIFIER NOT NULL,
    [Obj_Tip_Id_Elozmeny] UNIQUEIDENTIFIER NULL,
    [Obj_Type_Elozmeny]   NVARCHAR (100)   NULL,
    [Obj_Id_Kapcsolt]     UNIQUEIDENTIFIER NOT NULL,
    [Obj_Tip_Id_Kapcsolt] UNIQUEIDENTIFIER NULL,
    [Obj_Type_Kapcsolt]   NVARCHAR (100)   NULL,
    [Ver]                 INT              CONSTRAINT [DF__EREC_Ugyira__Ver__4EA8A765] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__EREC_Ugyi__ErvKe__4F9CCB9E] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_EREC_UgyiratObjKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__EREC_Ugyi__Letre__51851410] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [AK_PK_UGYIRATOBJKAPCS_EREC_UGY] UNIQUE NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Kapcsolt]
    ON [dbo].[EREC_UgyiratObjKapcsolatok]([Obj_Id_Kapcsolt] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Elozmeny]
    ON [dbo].[EREC_UgyiratObjKapcsolatok]([Obj_Id_Elozmeny] ASC) WITH (FILLFACTOR = 85);

