﻿CREATE TABLE [dbo].[EREC_WorkFlow] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_WorkFlo__Id__7F0CB2F5] DEFAULT (newsequentialid()) NOT NULL,
    [Obj_MetaDefinicio_Id] UNIQUEIDENTIFIER NULL,
    [Kod]                  NVARCHAR (100)   NULL,
    [Nev]                  NVARCHAR (400)   NULL,
    [Org]                  UNIQUEIDENTIFIER NULL,
    [Ver]                  INT              CONSTRAINT [DF__EREC_WorkFl__Ver__0000D72E] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__EREC_Work__ErvKe__00F4FB67] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__EREC_Work__Letre__01E91FA0] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_WORKFLOW] PRIMARY KEY CLUSTERED ([Id] ASC)
);

