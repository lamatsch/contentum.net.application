﻿CREATE TABLE [dbo].[EREC_WorkFlowFazisAtmenet] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_WorkFlo__Id__07A1F8F6] DEFAULT (newsequentialid()) NOT NULL,
    [BealltFazis_Id] UNIQUEIDENTIFIER NULL,
    [BealloFazis_Id] UNIQUEIDENTIFIER NULL,
    [FazisKimenet]   NVARCHAR (64)    NULL,
    [IdoKorrekcio]   NUMERIC (8)      CONSTRAINT [DF__EREC_Work__IdoKo__08961D2F] DEFAULT ((0)) NULL,
    [Idoegyseg]      NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_WorkFl__Ver__098A4168] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_Work__ErvKe__0A7E65A1] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_Work__Letre__0B7289DA] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_WORKFLOWFAZISATMENET] PRIMARY KEY CLUSTERED ([Id] ASC)
);

