﻿CREATE TABLE [dbo].[EREC_eMailBoritekCimei] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_eMailBo__Id__22401542] DEFAULT (newsequentialid()) NOT NULL,
    [eMailBoritek_Id] UNIQUEIDENTIFIER NOT NULL,
    [Sorszam]         INT              NOT NULL,
    [Tipus]           NVARCHAR (64)    NOT NULL,
    [MailCim]         NVARCHAR (100)   NULL,
    [Partner_Id]      UNIQUEIDENTIFIER NULL,
    [Ver]             INT              CONSTRAINT [DF__EREC_eMailB__Ver__2334397B] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__EREC_eMai__ErvKe__24285DB4] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         CONSTRAINT [DF_EREC_eMailBoritekCimei_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__EREC_eMai__Letre__2610A626] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [XBC_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [BoritekCim_Partner_FK] FOREIGN KEY ([Partner_Id]) REFERENCES [dbo].[KRT_Partnerek] ([Id]),
    CONSTRAINT [XBC_POP3_FK] FOREIGN KEY ([eMailBoritek_Id]) REFERENCES [dbo].[EREC_eMailBoritekok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_PartnerId]
    ON [dbo].[EREC_eMailBoritekCimei]([Partner_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_eMailBoritek_Id]
    ON [dbo].[EREC_eMailBoritekCimei]([eMailBoritek_Id] ASC) WITH (FILLFACTOR = 85);

