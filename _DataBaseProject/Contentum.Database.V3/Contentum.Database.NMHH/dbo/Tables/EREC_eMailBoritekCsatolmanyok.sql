﻿CREATE TABLE [dbo].[EREC_eMailBoritekCsatolmanyok] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_eMailBo__Id__28ED12D1] DEFAULT (newsequentialid()) NOT NULL,
    [eMailBoritek_Id] UNIQUEIDENTIFIER NOT NULL,
    [Dokumentum_Id]   UNIQUEIDENTIFIER NOT NULL,
    [Nev]             NVARCHAR (4000)  NOT NULL,
    [Tomoritve]       CHAR (1)         NULL,
    [Ver]             INT              CONSTRAINT [DF__EREC_eMailB__Ver__29E1370A] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__EREC_eMai__ErvKe__2AD55B43] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         CONSTRAINT [DF_EREC_eMailBoritekCsatolmanyok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__EREC_eMai__Letre__2CBDA3B5] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_EMAILBORITEKCSATOLMANY] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [eMailBoritekCsatol_Dokumentum_FK] FOREIGN KEY ([Dokumentum_Id]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id]),
    CONSTRAINT [EREC_EMAILCSAT_EMAILBOR_FK] FOREIGN KEY ([eMailBoritek_Id]) REFERENCES [dbo].[EREC_eMailBoritekok] ([Id])
);

