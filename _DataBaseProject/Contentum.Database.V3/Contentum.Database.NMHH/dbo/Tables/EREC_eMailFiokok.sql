﻿CREATE TABLE [dbo].[EREC_eMailFiokok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__EREC_eMailFi__Id__56FEC19B] DEFAULT (newsequentialid()) NOT NULL,
    [Nev]            NVARCHAR (100)   NULL,
    [UserNev]        NVARCHAR (100)   NULL,
    [Csoportok_Id]   UNIQUEIDENTIFIER NULL,
    [Jelszo]         NVARCHAR (20)    NULL,
    [Tipus]          CHAR (1)         NULL,
    [NapiMax]        INT              NULL,
    [NapiTeny]       INT              NULL,
    [EmailCim]       NVARCHAR (100)   NULL,
    [Modul_Id]       UNIQUEIDENTIFIER NULL,
    [MappaNev]       NVARCHAR (100)   NULL,
    [Ver]            INT              CONSTRAINT [DF__EREC_eMailF__Ver__57F2E5D4] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__EREC_eMai__ErvKe__58E70A0D] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_EREC_eMailFiokok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__EREC_eMai__Letre__5ACF527F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KIRL_POP3_FIOKOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [EREC_eMailFiok_UK]
    ON [dbo].[EREC_eMailFiokok]([Nev] ASC) WITH (FILLFACTOR = 85);

