﻿CREATE TABLE [dbo].[INT_AutoEUzenetSzabalyNaplo] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__INT_EKF__Id__66D10805] DEFAULT ('newsequentialid()') NOT NULL,
    [ESzabalyId]     UNIQUEIDENTIFIER NOT NULL,
    [EUzenetId]      UNIQUEIDENTIFIER NULL,
    [EUzenetTipusId] NVARCHAR (100)   NULL,
    [Megjegyzes]     NVARCHAR (MAX)   NULL,
    [Ver]            INT              CONSTRAINT [DF__INT_EKF__Ver__67C52C3E] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__INT_EKF__ErvKe__68B95077] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_INT_EKFrek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__INT_EKF__Letre__6AA198E9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_INT_AUTOEUZENETSZABALYNAPLO] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CK_INT_AutoEUzenetSzabalyNaplo] CHECK ((0)=[dbo].[fn_INT_AutoEUzenetSzabalyNaploCheckUK]([Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [FK_INT_AUTO_INTAUTOEU_INT_AUTO] FOREIGN KEY ([ESzabalyId]) REFERENCES [dbo].[INT_AutoEUzenetSzabaly] ([Id])
);

