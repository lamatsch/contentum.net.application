﻿CREATE TABLE [dbo].[INT_ETDR_Objektumok]
(
	   ETDR_Id int IDENTITY(1,1) PRIMARY KEY,
       Obj_Tip_Id uniqueidentifier not null references KRT_ObjTipusok (Id),
       Obj_Id uniqueidentifier not null, 
       [ETDR_Data] NVARCHAR(MAX) NULL, 
	   [ETDR_Note] NVARCHAR(MAX) NULL, 
)
GO
CREATE UNIQUE INDEX INT_ETDR_Objektumok_Idx_Obj_Id 
	On INT_ETDR_Objektumok (Obj_Tip_Id, Obj_Id) ;
