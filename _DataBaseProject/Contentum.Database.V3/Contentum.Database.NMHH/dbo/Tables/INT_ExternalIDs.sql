﻿create table INT_ExternalIDs (
   Id                   uniqueidentifier     not null,
   Modul_Id             uniqueidentifier     null,
   Edok_Type            Nvarchar(20)         null,
   Edok_Id              uniqueidentifier     null,
   Edok_ExpiredDate     datetime             null,
   External_Group       Nvarchar(20)         null,
   External_Id          uniqueidentifier     null,
   Deleted              char(1)              null,
   LastSync             datetime             null,
   State                Nvarchar(10)         null,
   Ver                  int                  null constraint DF__INT_Ext__Ver__67C52C3E default (1),
   Note                 Nvarchar(4000)       collate Hungarian_CI_AS null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null constraint DF__INT_Ext__ErvKe__68B95077 default getdate(),
   ErvVege              datetime             null constraint DF_INT_Ext_ErvVege default CONVERT([datetime],'4700-12-31',(102)),
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null constraint DF__INT_Ext__Letre__6AA198E9 default getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_INT_EXTERNALIDS primary key (Id)
)
go

alter table INT_ExternalIDs
   add constraint INTExternalIDs_Modul_FK foreign key (Modul_Id)
      references INT_Modulok (Id)
go
