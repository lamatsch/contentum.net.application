-- =========================================
-- Create table for INT_HAIRAdatok
-- =========================================
 /*
IF OBJECT_ID('INT_HAIRAdatok', 'U') IS NOT NULL
  DROP TABLE INT_HAIRAdatok
GO
 */
CREATE TABLE INT_HAIRAdatok
(  
 Id uniqueidentifier DEFAULT newsequentialid(),  
 Obj_Id uniqueidentifier,  
 ObjTip_Id uniqueidentifier,  
 Tipus Nvarchar(100) collate Hungarian_CI_AS null,
 Ertek Nvarchar(4000) collate Hungarian_CI_AS null,
 Ver                  int                  null constraint DF__INT_HAIRAdatok__Ver__67C52C3E default (1),
 Note                 Nvarchar(4000)       collate Hungarian_CI_AS null,
 Stat_id              uniqueidentifier     null,
 ErvKezd              datetime             null constraint DF__INT_HAIRAdatok__ErvKe__68B95077 default getdate(),
 ErvVege              datetime             null constraint DF_INT_HAIRAdatok_ErvVege default CONVERT([datetime],'4700-12-31',(102)),
 Letrehozo_id         uniqueidentifier     null,
 LetrehozasIdo        datetime             not null constraint DF__INT_HAIRAdatok__Letre__6AA198E9 default getdate(),
 Modosito_id          uniqueidentifier     null,
 ModositasIdo         datetime             null,
 Zarolo_id            uniqueidentifier     null,
 ZarolasIdo           datetime             null,
 Tranz_id             uniqueidentifier     null,
 UIAccessLog_id       uniqueidentifier     null,  
 PRIMARY KEY (Id)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_HAIRAdatok_ID ON INT_HAIRAdatok
(
	Id ASC
)
GO