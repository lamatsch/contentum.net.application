﻿create table INT_Log (
   Id                   uniqueidentifier     not null default 'newsequentialid()',
   Org                  uniqueidentifier     null,
   Modul_id             uniqueidentifier     not null,
   Sync_StartDate       datetime             null,
   Parancs              Nvarchar(400)        collate Hungarian_CI_AS not null,
   Machine              Nvarchar(100)        collate Hungarian_CI_AS null,
   Sync_EndDate         datetime             null,
   HibaKod              Nvarchar(4000)       collate Hungarian_CI_AS null,
   HibaUzenet           Nvarchar(4000)       collate Hungarian_CI_AS null,
   ExternalId 			Nvarchar(4000)		 collate Hungarian_CI_AS null,  
   Status 				Nvarchar(100)        collate Hungarian_CI_AS null,  
   Dokumentumok_Id_Sent uniqueidentifier,  
   Dokumentumok_Id_Error uniqueidentifier,  
   PackageHash Nvarchar(100),  
   PackageVer int,
   Ver                  int                  null constraint DF__INT_Log__Ver__67C52C3E default (1),
   Note                 Nvarchar(4000)       collate Hungarian_CI_AS null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null constraint DF__INT_Log__ErvKe__68B95077 default getdate(),
   ErvVege              datetime             null constraint DF_INT_Log_ErvVege default CONVERT([datetime],'4700-12-31',(102)),
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null constraint DF__INT_Log__Letre__6AA198E9 default getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint LOG_PK primary key (Id)
)
go

alter table INT_Log
   add constraint INTLog_Modul_FK foreign key (Modul_id)
      references INT_Modulok (Id)
go