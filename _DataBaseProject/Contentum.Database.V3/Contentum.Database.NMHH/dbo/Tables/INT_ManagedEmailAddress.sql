﻿CREATE TABLE [dbo].[INT_ManagedEmailAddress] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__INT_ManagedEmailAddress__Id__66D10805] DEFAULT ('newsequentialid()') NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id] UNIQUEIDENTIFIER NULL,
    [Nev]            NVARCHAR (400)   NOT NULL,
    [Ertek]          NVARCHAR (MAX)   NULL,
    [Karbantarthato] CHAR (1)         NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__INT_ManagedEmailAddress__Ver__67C52C3E] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__INT_ManagedEmailAddress__ErvKe__68B95077] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_INT_ManagedEmail_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__INT_ManagedEmailAddress__Letre__6AA198E9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_INT_MANAGEDEMAILADDRESS] PRIMARY KEY CLUSTERED ([Id] ASC)
);

