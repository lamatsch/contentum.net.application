﻿CREATE TABLE [dbo].[INT_Modulok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__INT_Modulok__Id__01342732] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Nev]            NVARCHAR (400)   NOT NULL,
    [Statusz]        CHAR (1)         NULL,
    [Leiras]         NVARCHAR (400)   NULL,
    [Parancs]        NVARCHAR (400)   NULL,
	[Gyakorisag] 	 INT,  
 	[GyakorisagMertekegyseg] NVARCHAR (20),  
 	[UtolsoFutas]    DATETIME,  
    [Ver]            INT              CONSTRAINT [DF__INT_Modulok__Ver__02284B6B] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__INT_Modul__ErvKe__031C6FA4] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_INT_Modulok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__INT_Modul__Letre__0504B816] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [MOD_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);

