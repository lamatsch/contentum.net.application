﻿CREATE TABLE [dbo].[IrattariTetelTemp] (
    [Ágazat betujele]            NVARCHAR (255)   NULL,
    [Ágazat megnevezése]         NVARCHAR (255)   NULL,
    [Tetelszam]                  NVARCHAR (255)   NULL,
    [Tétel megnevezése]          NVARCHAR (255)   NULL,
    [Selejtezési ido (év)]       FLOAT (53)       NULL,
    [Lt#]                        FLOAT (53)       NULL,
    [Ügytípus kód]               NVARCHAR (255)   NULL,
    [UgytipusKod]                NVARCHAR (255)   NULL,
    [Ügytípus neve]              NVARCHAR (255)   NULL,
    [Ügyfajta]                   NVARCHAR (255)   NULL,
    [Ügyint# ido]                NVARCHAR (255)   NULL,
    [Ügyint# ido mérték-egysége] NVARCHAR (255)   NULL,
    [TetelMegnev]                NVARCHAR (255)   NULL,
    [megnev]                     NVARCHAR (255)   NULL,
    [Tetel]                      NVARCHAR (255)   NULL,
    [irattaritetelGUID]          UNIQUEIDENTIFIER NULL,
    [iratmetaGUID]               UNIQUEIDENTIFIER NULL,
    [Szamkeret]                  NVARCHAR (4)     NULL
);

