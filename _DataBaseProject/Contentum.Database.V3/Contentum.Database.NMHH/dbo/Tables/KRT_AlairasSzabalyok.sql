﻿CREATE TABLE [dbo].[KRT_AlairasSzabalyok] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_AlairasS__Id__162F4418] DEFAULT (newsequentialid()) NOT NULL,
    [DokumentumMuvelet_Id] UNIQUEIDENTIFIER NOT NULL,
    [MetaDefinicio_Id]     UNIQUEIDENTIFIER NULL,
    [MetaDefinicioTipus]   NVARCHAR (100)   NULL,
    [AlairoSzerep]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [SzabalySzint]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Nev]                  NVARCHAR (100)   NULL,
    [AlairasTipus_Id]      UNIQUEIDENTIFIER NULL,
    [SzabalyTipus]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Titkositas]           CHAR (1)         NULL,
    [Sorrend]              INT              CONSTRAINT [DF__KRT_Alair__Sorre__17236851] DEFAULT ((1)) NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Alairas__Ver__18178C8A] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Alair__ErvKe__190BB0C3] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Alair__Letre__19FFD4FC] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ALAIRASSZABALYOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_IRATALAIR_ALAIRASTIP_FK] FOREIGN KEY ([AlairasTipus_Id]) REFERENCES [dbo].[KRT_AlairasTipusok] ([Id]),
    CONSTRAINT [KRT_IRATALAIR_DOKUMUV_FK] FOREIGN KEY ([DokumentumMuvelet_Id]) REFERENCES [dbo].[KRT_DokumentumMuveletek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_AlairasSzabaly_UK]
    ON [dbo].[KRT_AlairasSzabalyok]([DokumentumMuvelet_Id] ASC, [AlairoSzerep] ASC, [AlairasTipus_Id] ASC, [Titkositas] ASC) WITH (FILLFACTOR = 85);

