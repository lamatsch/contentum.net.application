﻿CREATE TABLE [dbo].[KRT_AlairasTipusok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_AlairasT__Id__10766AC2] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Nev]            NVARCHAR (100)   NOT NULL,
    [AlairasKod]     NVARCHAR (10)    NULL,
    [AlairasMod]     NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [AlairasSzint]   INT              NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Alairas__Ver__116A8EFB] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Alair__ErvKe__125EB334] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Alair__Letre__1352D76D] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ALAIRASTIPUSOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);

