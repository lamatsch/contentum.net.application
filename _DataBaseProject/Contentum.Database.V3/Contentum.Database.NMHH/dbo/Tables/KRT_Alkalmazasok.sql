﻿CREATE TABLE [dbo].[KRT_Alkalmazasok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Alkalmaz__Id__1CDC41A7] DEFAULT (newsequentialid()) NOT NULL,
    [KRT_Id]         UNIQUEIDENTIFIER NULL,
    [Kod]            NVARCHAR (100)   NOT NULL,
    [Nev]            NVARCHAR (100)   NULL,
    [Kulso]          CHAR (1)         NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Alkalma__Ver__1DD065E0] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Alkal__ErvKe__1EC48A19] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Alkalmazasok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Alkal__Letre__20ACD28B] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ALKALMAZASOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_KRT_ALKA_ALKALMAZA_KRT_ALKA] FOREIGN KEY ([KRT_Id]) REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Alkalmazas_UK]
    ON [dbo].[KRT_Alkalmazasok]([Org] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

