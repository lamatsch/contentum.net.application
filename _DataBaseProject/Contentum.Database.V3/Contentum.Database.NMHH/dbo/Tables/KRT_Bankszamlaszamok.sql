﻿CREATE TABLE [dbo].[KRT_Bankszamlaszamok] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Bankszam__Id__23893F36] DEFAULT (newsequentialid()) NOT NULL,
    [Partner_Id]      UNIQUEIDENTIFIER NULL,
    [Bankszamlaszam]  VARCHAR (34)     NOT NULL,
    [Partner_Id_Bank] UNIQUEIDENTIFIER NULL,
    [Ver]             INT              CONSTRAINT [DF__KRT_Banksza__Ver__247D636F] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__KRT_Banks__ErvKe__257187A8] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         CONSTRAINT [DF_KRT_Bankszamlaszamok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__KRT_Banks__Letre__2759D01A] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_BANKSZAMLASZAMOK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK] FOREIGN KEY ([Partner_Id_Bank]) REFERENCES [dbo].[KRT_Partnerek] ([Id]),
    CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK] FOREIGN KEY ([Partner_Id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);

