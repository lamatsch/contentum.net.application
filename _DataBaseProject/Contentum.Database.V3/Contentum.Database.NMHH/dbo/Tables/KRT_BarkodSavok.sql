﻿CREATE TABLE [dbo].[KRT_BarkodSavok] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_BarkodSa__Id__34B3CB38] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_BarkodS__Org__35A7EF71] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Csoport_Id_Felelos] UNIQUEIDENTIFIER NOT NULL,
    [SavKezd]            NVARCHAR (100)   NOT NULL,
    [SavVege]            NVARCHAR (100)   NOT NULL,
    [SavType]            CHAR (1)         NULL,
    [SavAllapot]         CHAR (1)         NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_BarkodS__Ver__369C13AA] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Barko__ErvKe__379037E3] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_BarkodSavok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Barko__Letre__39788055] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_BARKODSAVOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [Sav_Vege_I]
    ON [dbo].[KRT_BarkodSavok]([SavVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Sav_Kezd_I]
    ON [dbo].[KRT_BarkodSavok]([SavKezd] ASC) WITH (FILLFACTOR = 85);

