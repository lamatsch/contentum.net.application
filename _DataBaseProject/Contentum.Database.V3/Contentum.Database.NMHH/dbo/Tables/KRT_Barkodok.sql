﻿CREATE TABLE [dbo].[KRT_Barkodok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Barkodok__Id__2A363CC5] DEFAULT (newsequentialid()) NOT NULL,
    [Kod]            NVARCHAR (100)   NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]      UNIQUEIDENTIFIER NULL,
    [Obj_type]       NVARCHAR (100)   NULL,
    [KodType]        CHAR (1)         NULL,
    [Allapot]        NVARCHAR (64)    NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Barkodo__Ver__2B2A60FE] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Barko__ErvKe__2C1E8537] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Barkodok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Barko__Letre__2E06CDA9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_BARKODOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [Barkod_UK]
    ON [dbo].[KRT_Barkodok]([Kod] ASC) WITH (FILLFACTOR = 85);

