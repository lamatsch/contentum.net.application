﻿CREATE TABLE [dbo].[KRT_Barkodok_Mentes] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [Kod]            NVARCHAR (100)   NOT NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]      UNIQUEIDENTIFIER NULL,
    [Obj_type]       NVARCHAR (100)   NULL,
    [KodType]        CHAR (1)         NULL,
    [Allapot]        NVARCHAR (64)    NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Barkodo__Ver__2FEF161B] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Barko__ErvKe__30E33A54] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Barko__Letre__31D75E8D] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL
);

