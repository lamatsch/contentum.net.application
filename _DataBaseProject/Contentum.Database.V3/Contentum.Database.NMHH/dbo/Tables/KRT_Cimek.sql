﻿CREATE TABLE [dbo].[KRT_Cimek] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Cimek__Id__3C54ED00] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER NULL,
    [Kategoria]          CHAR (1)         NOT NULL,
    [Tipus]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Nev]                NVARCHAR (100)   NULL,
    [KulsoAzonositok]    NVARCHAR (100)   NULL,
    [Forras]             CHAR (1)         NULL,
    [Orszag_Id]          UNIQUEIDENTIFIER NULL,
    [OrszagNev]          NVARCHAR (400)   NULL,
    [Telepules_Id]       UNIQUEIDENTIFIER NULL,
    [TelepulesNev]       NVARCHAR (100)   NULL,
    [IRSZ]               NVARCHAR (20)    NULL,
    [CimTobbi]           NVARCHAR (100)   NULL,
    [Kozterulet_Id]      UNIQUEIDENTIFIER NULL,
    [KozteruletNev]      NVARCHAR (100)   NULL,
    [KozteruletTipus_Id] UNIQUEIDENTIFIER NULL,
    [KozteruletTipusNev] NVARCHAR (100)   NULL,
    [Hazszam]            NVARCHAR (100)   NULL,
    [Hazszamig]          NVARCHAR (100)   NULL,
    [HazszamBetujel]     NVARCHAR (10)    NULL,
    [MindketOldal]       CHAR (1)         NULL,
    [HRSZ]               NVARCHAR (100)   NULL,
    [Lepcsohaz]          NVARCHAR (100)   NULL,
    [Szint]              NVARCHAR (10)    NULL,
    [Ajto]               NVARCHAR (20)    NULL,
    [AjtoBetujel]        NVARCHAR (20)    NULL,
    [Tobbi]              NVARCHAR (100)   NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_Cimek__Ver__3D491139] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Cimek__ErvKe__3E3D3572] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_Cimek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Cimek__Letre__40257DE4] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [CIM_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Cim_Kozterulet_FK] FOREIGN KEY ([Kozterulet_Id]) REFERENCES [dbo].[KRT_Kozteruletek] ([Id]),
    CONSTRAINT [Cim_Orszag_FK] FOREIGN KEY ([Orszag_Id]) REFERENCES [dbo].[KRT_Orszagok] ([Id]),
    CONSTRAINT [Cim_telepules_FK] FOREIGN KEY ([Telepules_Id]) REFERENCES [dbo].[KRT_Telepulesek] ([Id]),
    CONSTRAINT [FK_KRT_CIME_CIM_KOZTE_KRT_KOZT] FOREIGN KEY ([KozteruletTipus_Id]) REFERENCES [dbo].[KRT_KozteruletTipusok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Cimek]
    ON [dbo].[KRT_Cimek]([Nev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Cimek_TelepKozter]
    ON [dbo].[KRT_Cimek]([TelepulesNev] ASC, [KozteruletNev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Cimek_LetrehozasIdo]
    ON [dbo].[KRT_Cimek]([LetrehozasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Cimek_ErvKezdVege]
    ON [dbo].[KRT_Cimek]([ErvKezd] ASC, [ErvVege] ASC, [LetrehozasIdo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [_dta_index_KRT_Cimek_5_1550016653__K7]
    ON [dbo].[KRT_Cimek]([Forras] ASC);

