﻿CREATE TABLE [dbo].[KRT_CsoportTagok] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_CsoportT__Id__49AEE81E] DEFAULT (newsequentialid()) NOT NULL,
    [Tipus]               NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Csoport_Id]          UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Jogalany] UNIQUEIDENTIFIER NULL,
    [ObjTip_Id_Jogalany]  UNIQUEIDENTIFIER NULL,
    [ErtesitesMailCim]    NVARCHAR (100)   NULL,
    [ErtesitesKell]       CHAR (1)         NULL,
    [System]              CHAR (1)         NULL,
    [Orokolheto]          CHAR (1)         CONSTRAINT [DF__KRT_Csopo__Oroko__4AA30C57] DEFAULT ('0') NULL,
    [ObjektumTulajdonos]  CHAR (1)         NULL,
    [Ver]                 INT              CONSTRAINT [DF__KRT_Csoport__Ver__4B973090] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__KRT_Csopo__ErvKe__4C8B54C9] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_KRT_CsoportTagok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__KRT_Csopo__Letre__4E739D3B] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_CSOPORTTAGOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Csoporttag_Csoport_FK] FOREIGN KEY ([Csoport_Id]) REFERENCES [dbo].[KRT_Csoportok] ([Id]),
    CONSTRAINT [CsoportTag_Csoport_Jogalany_FK] FOREIGN KEY ([Csoport_Id_Jogalany]) REFERENCES [dbo].[KRT_Csoportok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_CSOPORTTAGOK_ID_JOGALANY]
    ON [dbo].[KRT_CsoportTagok]([Csoport_Id] ASC, [Csoport_Id_Jogalany] ASC);


GO
CREATE NONCLUSTERED INDEX [KRT_CSOPORTOK_FK_I]
    ON [dbo].[KRT_CsoportTagok]([Csoport_Id] ASC) WITH (FILLFACTOR = 85);

