﻿CREATE TABLE [dbo].[KRT_Csoportok] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Csoporto__Id__4301EA8F] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                   UNIQUEIDENTIFIER NOT NULL,
    [Kod]                   NVARCHAR (100)   NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Tipus]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Jogalany]              CHAR (1)         NULL,
    [ErtesitesEmail]        NVARCHAR (100)   NULL,
    [System]                CHAR (1)         NULL,
    [Adatforras]            CHAR (1)         NULL,
    [ObjTipus_Id_Szulo]     UNIQUEIDENTIFIER NULL,
    [Kiszolgalhato]         CHAR (1)         NULL,
    [JogosultsagOroklesMod] CHAR (1)         NULL,
    [Ver]                   INT              CONSTRAINT [DF__KRT_Csoport__Ver__43F60EC8] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__KRT_Csopo__ErvKe__44EA3301] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_KRT_Csoportok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__KRT_Csopo__Letre__46D27B73] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_CSOPORTOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Csoport_ObjTipus_FK] FOREIGN KEY ([ObjTipus_Id_Szulo]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_TIPUS]
    ON [dbo].[KRT_Csoportok]([Tipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_ERVVDAT]
    ON [dbo].[KRT_Csoportok]([ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_ERVKDAT]
    ON [dbo].[KRT_Csoportok]([ErvKezd] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Csoportok_Jogalany]
    ON [dbo].[KRT_Csoportok]([Jogalany] ASC, [ErvKezd] ASC, [ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Csoportok_UK]
    ON [dbo].[KRT_Csoportok]([Org] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

