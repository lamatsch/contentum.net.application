﻿CREATE TABLE [dbo].[KRT_DokumentumMegosztas] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Dokument__Id__18CC84F8] DEFAULT (newsequentialid()) NOT NULL,
    [Dokumentum_Id_Titkos] UNIQUEIDENTIFIER NULL,
    [Csoport_Id]           UNIQUEIDENTIFIER NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Dokumen__Ver__19C0A931] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Dokum__ErvKe__1AB4CD6A] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Dokum__Letre__1BA8F1A3] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_DOKUMENTUMMEGOSZTAS] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [DokuMegosztas_Dokumentum_FK] FOREIGN KEY ([Dokumentum_Id_Titkos]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id]),
    CONSTRAINT [KRT_ALAIRMEGOSZT_CSOPORTOK] FOREIGN KEY ([Csoport_Id]) REFERENCES [dbo].[KRT_Csoportok] ([Id])
);

