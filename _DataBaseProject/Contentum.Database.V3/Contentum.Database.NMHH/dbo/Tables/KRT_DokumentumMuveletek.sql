﻿CREATE TABLE [dbo].[KRT_DokumentumMuveletek] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Dokument__Id__5708E33C] DEFAULT (newsequentialid()) NOT NULL,
    [Folyamat_Id]     UNIQUEIDENTIFIER NULL,
    [MuveletKod]      NVARCHAR (100)   NULL,
    [Nev]             NVARCHAR (100)   NOT NULL,
    [AlairoSzerepJel] NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Ver]             INT              CONSTRAINT [DF__KRT_Dokumen__Ver__57FD0775] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__KRT_Dokum__ErvKe__58F12BAE] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__KRT_Dokum__Letre__59E54FE7] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_DOKUMENTUMMUVELETEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_DOKUMUV_FOLYAMAT_FK] FOREIGN KEY ([Folyamat_Id]) REFERENCES [dbo].[KRT_Folyamatok] ([Id])
);

