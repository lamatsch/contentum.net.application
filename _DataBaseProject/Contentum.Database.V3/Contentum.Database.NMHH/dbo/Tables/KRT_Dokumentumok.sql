﻿CREATE TABLE [dbo].[KRT_Dokumentumok] (
    [Id]                      UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Dokument__Id__5CC1BC92] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                     UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Dokumen__Org__5DB5E0CB] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [FajlNev]                 NVARCHAR (400)   NOT NULL,
    [VerzioJel]               NVARCHAR (100)   NULL,
    [Nev]                     NVARCHAR (100)   NULL,
    [Tipus]                   NVARCHAR (100)   NOT NULL,
    [Formatum]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Leiras]                  NVARCHAR (400)   NULL,
    [Meret]                   INT              NULL,
    [TartalomHash]            NVARCHAR (100)   NULL,
    [AlairtTartalomHash]      VARBINARY (4000) NULL,
    [KivonatHash]             NVARCHAR (100)   NULL,
    [Dokumentum_Id]           UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id_Kovetkezo] UNIQUEIDENTIFIER NULL,
    [Alkalmazas_Id]           UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Tulaj]        UNIQUEIDENTIFIER NOT NULL,
    [KulsoAzonositok]         NVARCHAR (100)   NULL,
    [External_Source]         NVARCHAR (100)   NULL,
    [External_Link]           NVARCHAR (400)   NULL,
    [External_Id]             UNIQUEIDENTIFIER NULL,
    [External_Info]           NVARCHAR (4000)  NULL,
    [CheckedOut]              UNIQUEIDENTIFIER NULL,
    [CheckedOutTime]          DATETIME         NULL,
    [BarCode]                 NVARCHAR (100)   NULL,
    [OCRPrioritas]            INT              NULL,
    [OCRAllapot]              CHAR (1)         NULL,
    [Allapot]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [WorkFlowAllapot]         NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Megnyithato]             CHAR (1)         NULL,
    [Olvashato]               CHAR (1)         NULL,
    [ElektronikusAlairas]     NVARCHAR (64)    COLLATE Hungarian_CS_AS CONSTRAINT [DF__KRT_Dokum__Elekt__5EAA0504] DEFAULT ('0') NULL,
    [AlairasFelulvizsgalat]   DATETIME         NULL,
    [Titkositas]              CHAR (1)         CONSTRAINT [DF__KRT_Dokum__Titko__5F9E293D] DEFAULT ('0') NULL,
    [SablonAzonosito]         NVARCHAR (100)   NULL,
    [Ver]                     INT              CONSTRAINT [DF__KRT_Dokumen__Ver__60924D76] DEFAULT ((1)) NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         CONSTRAINT [DF__KRT_Dokum__ErvKe__618671AF] DEFAULT (getdate()) NULL,
    [ErvVege]                 DATETIME         CONSTRAINT [DF_KRT_Dokumentumok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         CONSTRAINT [DF__KRT_Dokum__Letre__636EBA21] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_Id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_Id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_Id]          UNIQUEIDENTIFIER NULL,
    [AlairasFelulvizsgalatEredmeny]                    NVARCHAR (4000)  NULL,
    CONSTRAINT [KRT_DOKUMENTUMOK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_ALKALMAZAS_DOKU_FK] FOREIGN KEY ([Alkalmazas_Id]) REFERENCES [dbo].[KRT_Alkalmazasok] ([Id]),
    CONSTRAINT [KRT_DOKUMENTUMOK_KOVETKEZO_FK] FOREIGN KEY ([Dokumentum_Id_Kovetkezo]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id]),
    CONSTRAINT [KRT_DOKUMENTUMOK_OSDOKU_FK] FOREIGN KEY ([Dokumentum_Id]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_KivHash]
    ON [dbo].[KRT_Dokumentumok]([KivonatHash] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_FajlNev]
    ON [dbo].[KRT_Dokumentumok]([FajlNev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_ExtLink]
    ON [dbo].[KRT_Dokumentumok]([External_Link] ASC) WITH (FILLFACTOR = 85);

