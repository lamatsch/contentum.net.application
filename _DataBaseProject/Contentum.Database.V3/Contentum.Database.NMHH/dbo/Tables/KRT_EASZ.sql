﻿CREATE TABLE [dbo].[KRT_EASZ] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_EASZ__Id__1C9D15DC] DEFAULT (newsequentialid()) NOT NULL,
    [EASZKod]        NVARCHAR (100)   NULL,
    [EASZTipus_Id]   UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_EASZ__Ver__1D913A15] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_EASZ__ErvKez__1E855E4E] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_EASZ__Letreh__1F798287] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_EASZ] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_EASZ_EASZTIP_FK] FOREIGN KEY ([EASZTipus_Id]) REFERENCES [dbo].[KRT_EASZTipus] ([Id])
);

