﻿CREATE TABLE [dbo].[KRT_EASZTanusitvanyok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_EASZTanu__Id__68336F3E] DEFAULT (newsequentialid()) NOT NULL,
    [EASZ_Id]        UNIQUEIDENTIFIER NULL,
    [Tanusitvany_Id] UNIQUEIDENTIFIER NULL,
    [KonfigTetel]    NVARCHAR (10)    NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_EASZTan__Ver__69279377] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_EASZT__ErvKe__6A1BB7B0] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_EASZT__Letre__6B0FDBE9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_EASZTANUSITVANYOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_EASZTAN_EASZ_FK] FOREIGN KEY ([EASZ_Id]) REFERENCES [dbo].[KRT_EASZTipus] ([Id]),
    CONSTRAINT [KRT_EASZTAN_TANUSITV_FK] FOREIGN KEY ([Tanusitvany_Id]) REFERENCES [dbo].[KRT_Tanusitvanyok] ([Id])
);

