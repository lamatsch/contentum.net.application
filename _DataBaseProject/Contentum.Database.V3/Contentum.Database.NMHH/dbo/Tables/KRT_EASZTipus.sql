﻿CREATE TABLE [dbo].[KRT_EASZTipus] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_EASZTipu__Id__6DEC4894] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER NULL,
    [Nev]                NVARCHAR (100)   NOT NULL,
    [AlairasTipus_Id]    UNIQUEIDENTIFIER NULL,
    [EASZTipusKod]       NVARCHAR (100)   NULL,
    [KivarasiIdo]        INT              NULL,
    [AktualisEASZVerzio] INT              NULL,
    [URL]                NVARCHAR (400)   NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_EASZTip__Ver__6EE06CCD] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_EASZT__ErvKe__6FD49106] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_EASZT__Letre__70C8B53F] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_EASZTIPUS] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_EASZTIP_ALAIRASTIP_FK] FOREIGN KEY ([AlairasTipus_Id]) REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
);

