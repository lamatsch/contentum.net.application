﻿CREATE TABLE [dbo].[KRT_Erintett_Tablak] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Erintett__Id__73A521EA] DEFAULT (newsequentialid()) NOT NULL,
    [Tranzakci_id]   UNIQUEIDENTIFIER NOT NULL,
    [ObjTip_Id]      UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Erintet__Ver__74994623] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Erint__ErvKe__758D6A5C] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Erintett_Tablak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Erint__Letre__7775B2CE] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ERINTETT_TABLAK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [ErintettTabla_Tranzakcio_FK] FOREIGN KEY ([Tranzakci_id]) REFERENCES [dbo].[KRT_Tranzakciok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Erintett_Tabla_Ind01]
    ON [dbo].[KRT_Erintett_Tablak]([ObjTip_Id] ASC, [Tranzakci_id] ASC) WITH (FILLFACTOR = 85);

