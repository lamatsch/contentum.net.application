﻿CREATE TABLE [dbo].[KRT_Esemenyek] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Esemenye__Id__7A521F79] DEFAULT (newsequentialid()) NOT NULL,
    [Obj_Id]                         UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]                      UNIQUEIDENTIFIER NULL,
    [Azonositoja]                    NVARCHAR (400)   NULL,
    [Felhasznalo_Id_User]            UNIQUEIDENTIFIER NULL,
    [Csoport_Id_FelelosUserSzerveze] UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Login]           UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Cel]                 UNIQUEIDENTIFIER NULL,
    [Helyettesites_Id]               UNIQUEIDENTIFIER NULL,
    [Tranzakcio_Id]                  UNIQUEIDENTIFIER NULL,
    [Funkcio_Id]                     UNIQUEIDENTIFIER NULL,
    [CsoportHierarchia]              NVARCHAR (4000)  NULL,
    [KeresesiFeltetel]               NVARCHAR (4000)  NULL,
    [TalalatokSzama]                 INT              NULL,
    [Ver]                            INT              CONSTRAINT [DF__KRT_Esemeny__Ver__7B4643B2] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         CONSTRAINT [DF__KRT_Eseme__ErvKe__7C3A67EB] DEFAULT (getdate()) NULL,
    [ErvVege]                        DATETIME         CONSTRAINT [DF_KRT_Esemenyek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__KRT_Eseme__Letre__7E22B05D] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [Munkaallomas]                   NVARCHAR (100)   NULL,
    [MuveletKimenete]                NVARCHAR (100)   NULL,
	[CheckSum]						 NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_KIRL_ESEMENYEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Esemeny_Funkcio_FK] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Esemenyek_Obj_Id]
    ON [dbo].[KRT_Esemenyek]([Obj_Id] ASC, [Id] ASC)
    INCLUDE([Csoport_Id_FelelosUserSzerveze], [Felhasznalo_Id_User], [Funkcio_Id]) WITH (FILLFACTOR = 85);

