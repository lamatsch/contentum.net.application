﻿CREATE TABLE [dbo].[KRT_Extra_Napok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Extra_Na__Id__01F34141] DEFAULT (newsequentialid()) NOT NULL,
    [Datum]          DATETIME         NULL,
    [Jelzo]          INT              NULL,
    [Megjegyzes]     NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Extra_N__Ver__02E7657A] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Extra__ErvKe__03DB89B3] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Extra_Napok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Extra__Letre__05C3D225] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_EXTRA_NAPOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [UI_KRT_EXTRA_NAPOK]
    ON [dbo].[KRT_Extra_Napok]([Datum] ASC) WITH (FILLFACTOR = 85);

