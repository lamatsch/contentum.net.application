﻿CREATE TABLE [dbo].[KRT_FelhasznaloProfilok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Felhaszn__Id__206DA6C0] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NOT NULL,
    [Tipus]          NVARCHAR (64)    NULL,
    [Felhasznalo_id] UNIQUEIDENTIFIER NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NULL,
    [Nev]            NVARCHAR (100)   NULL,
    [XML]            NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Felhasz__Ver__2161CAF9] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Felha__ErvKe__2255EF32] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_FelhasznaloProfilok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Felha__Letre__243E37A4] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_FELHASZNALO_PROFILOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FelhasznaloProfil_ObjTip_FK] FOREIGN KEY ([Obj_Id]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id]),
    CONSTRAINT [KRT_FELHASZNALO_FK] FOREIGN KEY ([Felhasznalo_id]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_FelhasznaloProfil_Ind01]
    ON [dbo].[KRT_FelhasznaloProfilok]([Org] ASC, [Felhasznalo_id] ASC, [Obj_Id] ASC) WITH (FILLFACTOR = 85);

