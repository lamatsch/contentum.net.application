﻿CREATE TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Felhaszn__Id__25325BDD] DEFAULT (newsequentialid()) NOT NULL,
    [Felhasznalo_Id] UNIQUEIDENTIFIER NOT NULL,
    [Mappa_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Sorrend]        INT              NULL,
    [Leiras]         NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Felhasz__Ver__26268016] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Felha__ErvKe__271AA44F] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_FelhasznaloSpecialisMappai_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Felha__Letre__2902ECC1] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KIRL_EDOC_FELH_SPEC_MAPPAI_PK] PRIMARY KEY NONCLUSTERED ([Id] ASC),
    CONSTRAINT [FelhasznSpecMappai_Mappa_FK] FOREIGN KEY ([Mappa_Id]) REFERENCES [dbo].[KRT_Mappak] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Mappa_Id]
    ON [dbo].[KRT_FelhasznaloSpecialisMappai]([Mappa_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_Felhasznalo_Id]
    ON [dbo].[KRT_FelhasznaloSpecialisMappai]([Felhasznalo_Id] ASC);

