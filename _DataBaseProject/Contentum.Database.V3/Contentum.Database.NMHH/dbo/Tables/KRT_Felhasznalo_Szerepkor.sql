﻿CREATE TABLE [dbo].[KRT_Felhasznalo_Szerepkor] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Felhaszn__Id__08A03ED0] DEFAULT (newsequentialid()) NOT NULL,
    [CsoportTag_Id]    UNIQUEIDENTIFIER NULL,
    [Csoport_Id]       UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id]   UNIQUEIDENTIFIER NOT NULL,
    [Szerepkor_Id]     UNIQUEIDENTIFIER NOT NULL,
    [Helyettesites_Id] UNIQUEIDENTIFIER NULL,
    [Ver]              INT              CONSTRAINT [DF__KRT_Felhasz__Ver__09946309] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__KRT_Felha__ErvKe__0A888742] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_KRT_Felhasznalo_Szerepkor_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__KRT_Felha__Letre__0C70CFB4] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FES_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Csoporttag_Szerepkor_Fk] FOREIGN KEY ([CsoportTag_Id]) REFERENCES [dbo].[KRT_CsoportTagok] ([Id]),
    CONSTRAINT [Felhasznalo_Szerepkor_FK] FOREIGN KEY ([Szerepkor_Id]) REFERENCES [dbo].[KRT_Szerepkorok] ([Id]),
    CONSTRAINT [FES_FEL_FK] FOREIGN KEY ([Felhasznalo_Id]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [KRT_FELHASZNSZEREP_HELYETTES_FK] FOREIGN KEY ([Helyettesites_Id]) REFERENCES [dbo].[KRT_Helyettesitesek] ([Id]),
    CONSTRAINT [Szerepkor_csoport_FK] FOREIGN KEY ([Csoport_Id]) REFERENCES [dbo].[KRT_Csoportok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FES_SRK_FK_I]
    ON [dbo].[KRT_Felhasznalo_Szerepkor]([Szerepkor_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FES_FEL_FK_I]
    ON [dbo].[KRT_Felhasznalo_Szerepkor]([Felhasznalo_Id] ASC) WITH (FILLFACTOR = 85);

