﻿CREATE TABLE [dbo].[KRT_Felhasznalok] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Felhaszn__Id__0F4D3C5F] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [Org]                  UNIQUEIDENTIFIER NULL,
    [Partner_id]           UNIQUEIDENTIFIER NULL,
    [Tipus]                NVARCHAR (10)    NULL,
    [UserNev]              NVARCHAR (100)   NOT NULL,
    [Nev]                  NVARCHAR (100)   NULL,
    [Jelszo]               NVARCHAR (100)   NULL,
    [JelszoLejaratIdo]     DATETIME         CONSTRAINT [DF_KRT_Felhasznalok_JelszoLejaratIdo] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NOT NULL,
    [System]               CHAR (1)         NULL,
    [Kiszolgalo]           CHAR (1)         NULL,
    [DefaultPrivatKulcs]   NVARCHAR (4000)  NULL,
    [Partner_Id_Munkahely] UNIQUEIDENTIFIER NULL,
    [MaxMinosites]         NVARCHAR (2)     NULL,
    [EMail]                NVARCHAR (100)   NULL,
    [Engedelyezett]        CHAR (1)         CONSTRAINT [DF__KRT_Felha__Enged__113584D1] DEFAULT ('1') NULL,
    [Telefonszam]          NVARCHAR (100)   NULL,
    [Beosztas]             NVARCHAR (100)   NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Felhasz__Ver__1229A90A] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Felha__ErvKe__131DCD43] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         CONSTRAINT [DF_KRT_Felhasznalok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Felha__Letre__150615B5] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    [WrongPassCnt]         INT              NULL,
    CONSTRAINT [FEL_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [CK_KRT_Felhasznalok] CHECK ((0)=[dbo].[fn_KRT_FelhasznalokCheckUK]([Org],[UserNev],[Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [Felhasznalo_Partner__munkahely_FK] FOREIGN KEY ([Partner_Id_Munkahely]) REFERENCES [dbo].[KRT_Partnerek] ([Id]),
    CONSTRAINT [Felhasznalo_Partner_FK] FOREIGN KEY ([Partner_id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Felhasznalo_ErvKezd_I]
    ON [dbo].[KRT_Felhasznalok]([ErvKezd] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_FELH_USERN_UK]
    ON [dbo].[KRT_Felhasznalok]([Org] ASC, [UserNev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FEL_PRT_FK_I]
    ON [dbo].[KRT_Felhasznalok]([Partner_id] ASC) WITH (FILLFACTOR = 85);

