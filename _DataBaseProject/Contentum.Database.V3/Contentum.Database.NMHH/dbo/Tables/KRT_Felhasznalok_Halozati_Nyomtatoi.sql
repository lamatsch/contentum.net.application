﻿CREATE TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Felhaszn__Id__615232DC] DEFAULT (newsequentialid()) NOT NULL,
    [Felhasznalo_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Halozati_Nyomtato_Id] UNIQUEIDENTIFIER NOT NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Felhasz__Ver__62465715] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Felha__ErvKe__633A7B4E] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         CONSTRAINT [DF_KRT_Felhasznalok_Halozati_Nyomtatoi_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Felha__Letre__6522C3C0] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Felhasznalok_Halozati_Nyomtatoi] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK] FOREIGN KEY ([Felhasznalo_Id]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK] FOREIGN KEY ([Halozati_Nyomtato_Id]) REFERENCES [dbo].[KRT_Halozati_Nyomtatok] ([Id])
);

