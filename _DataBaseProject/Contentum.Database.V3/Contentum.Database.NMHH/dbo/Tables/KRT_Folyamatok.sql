﻿CREATE TABLE [dbo].[KRT_Folyamatok] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Folyamat__Id__1BB31344] DEFAULT (newsequentialid()) NOT NULL,
    [FolyamatCsoportKod] NVARCHAR (100)   NULL,
    [FolyamatKod]        NVARCHAR (100)   NULL,
    [Nev]                NVARCHAR (100)   NOT NULL,
    [FolyamatSorszam]    INT              NULL,
    [Alkalmazas_Id]      UNIQUEIDENTIFIER NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_Folyama__Ver__1CA7377D] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Folya__ErvKe__1D9B5BB6] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Folya__Letre__1E8F7FEF] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_FOLYAMATOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_FOLYAMAT_ALKALMAZAS_FK] FOREIGN KEY ([Alkalmazas_Id]) REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
);

