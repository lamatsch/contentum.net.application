﻿CREATE TABLE [dbo].[KRT_FunkcioInditas] (
    [Id]                     UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_FunkcioI__Id__29F710FA] DEFAULT (newsequentialid()) NOT NULL,
    [Funkcio_Id]             UNIQUEIDENTIFIER NOT NULL,
    [AlapertelmezettFunkcio] CHAR (1)         NOT NULL,
    [URLDefinicio]           NVARCHAR (4000)  NULL,
    [URLMegnyitas]           CHAR (1)         NULL,
    [Ver]                    INT              CONSTRAINT [DF__KRT_Funkcio__Ver__2AEB3533] DEFAULT ((1)) NULL,
    [Note]                   NVARCHAR (4000)  NULL,
    [Stat_id]                UNIQUEIDENTIFIER NULL,
    [ErvKezd]                DATETIME         CONSTRAINT [DF__KRT_Funkc__ErvKe__2BDF596C] DEFAULT (getdate()) NULL,
    [ErvVege]                DATETIME         NULL,
    [Letrehozo_id]           UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]          DATETIME         CONSTRAINT [DF__KRT_Funkc__Letre__2CD37DA5] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]            UNIQUEIDENTIFIER NULL,
    [ModositasIdo]           DATETIME         NULL,
    [Zarolo_id]              UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]             DATETIME         NULL,
    [Tranz_id]               UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_FUNKCIOINDITAS] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Funkciok_FunkcioInditas_Fk] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id])
);

