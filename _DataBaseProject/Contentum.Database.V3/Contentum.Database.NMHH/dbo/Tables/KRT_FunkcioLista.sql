﻿CREATE TABLE [dbo].[KRT_FunkcioLista] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_FunkcioL__Id__2DC7A1DE] DEFAULT (newsequentialid()) NOT NULL,
    [Funkcio_Id]        UNIQUEIDENTIFIER NULL,
    [Funkcio_Id_Hivott] UNIQUEIDENTIFIER NULL,
    [FutasiSorrend]     INT              NULL,
    [Ver]               INT              CONSTRAINT [DF__KRT_Funkcio__Ver__2EBBC617] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__KRT_Funkc__ErvKe__2FAFEA50] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_KRT_FunkcioLista_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__KRT_Funkc__Letre__319832C2] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_FUNKCIOLISTA] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id]),
    CONSTRAINT [Funkciolista_Funkcio_FK] FOREIGN KEY ([Funkcio_Id_Hivott]) REFERENCES [dbo].[KRT_Funkciok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_FunkcioLista_Ind01]
    ON [dbo].[KRT_FunkcioLista]([Funkcio_Id] ASC, [Funkcio_Id_Hivott] ASC, [FutasiSorrend] ASC) WITH (FILLFACTOR = 85);

