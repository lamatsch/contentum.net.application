﻿CREATE TABLE [dbo].[KRT_Funkciok] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Funkciok__Id__2354350C] DEFAULT (newsequentialid()) NOT NULL,
    [Kod]                  NVARCHAR (100)   NOT NULL,
    [Nev]                  NVARCHAR (100)   NOT NULL,
    [ObjTipus_Id_AdatElem] UNIQUEIDENTIFIER NULL,
    [ObjStat_Id_Kezd]      UNIQUEIDENTIFIER NULL,
    [Alkalmazas_Id]        UNIQUEIDENTIFIER NULL,
    [Muvelet_Id]           UNIQUEIDENTIFIER NULL,
    [ObjStat_Id_Veg]       UNIQUEIDENTIFIER NULL,
    [Leiras]               NVARCHAR (4000)  NULL,
    [Funkcio_Id_Szulo]     UNIQUEIDENTIFIER NULL,
    [Csoportosito]         CHAR (1)         NULL,
    [Modosithato]          CHAR (1)         NULL,
    [Org]                  UNIQUEIDENTIFIER NULL,
    [MunkanaploJelzo]      CHAR (1)         CONSTRAINT [DF__KRT_Funkc__Munka__24485945] DEFAULT ('0') NULL,
    [FeladatJelzo]         CHAR (1)         CONSTRAINT [DF__KRT_Funkc__Felad__253C7D7E] DEFAULT ('0') NULL,
    [KeziFeladatJelzo]     CHAR (1)         NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Funkcio__Ver__2630A1B7] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Funkc__ErvKe__2724C5F0] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         CONSTRAINT [DF_KRT_Funkciok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Funkc__Letre__290D0E62] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FUN_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_KRT_FUNK_ALKALMAZA_KRT_ALKA] FOREIGN KEY ([Alkalmazas_Id]) REFERENCES [dbo].[KRT_Alkalmazasok] ([Id]),
    CONSTRAINT [FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN] FOREIGN KEY ([Tranz_id]) REFERENCES [dbo].[KRT_Tranzakciok] ([Id]),
    CONSTRAINT [Funkcio_funkcio_szulo_FK] FOREIGN KEY ([Funkcio_Id_Szulo]) REFERENCES [dbo].[KRT_Funkciok] ([Id]),
    CONSTRAINT [Funkcio_Muvelet_FK] FOREIGN KEY ([Muvelet_Id]) REFERENCES [dbo].[KRT_Muveletek] ([Id]),
    CONSTRAINT [Funkcio_ObjStat_Kezd_FK] FOREIGN KEY ([ObjStat_Id_Kezd]) REFERENCES [dbo].[KRT_KodTarak] ([Id]),
    CONSTRAINT [Funkcio_ObjStat_Veg_FK] FOREIGN KEY ([ObjStat_Id_Veg]) REFERENCES [dbo].[KRT_KodTarak] ([Id]),
    CONSTRAINT [Funkcio_ObjTip_FK] FOREIGN KEY ([ObjTipus_Id_AdatElem]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Funkcio_Kod_Ervkezd_Ervvege]
    ON [dbo].[KRT_Funkciok]([Kod] ASC, [ErvKezd] ASC, [ErvVege] ASC);


GO
CREATE NONCLUSTERED INDEX [KRT_Funkcio_Ind01]
    ON [dbo].[KRT_Funkciok]([Alkalmazas_Id] ASC, [Muvelet_Id] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

