﻿CREATE TABLE [dbo].[KRT_Halozati_Nyomtatok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Halozati__Id__5AA5354D] DEFAULT (newsequentialid()) NOT NULL,
    [Nev]            NVARCHAR (100)   NOT NULL,
    [Cim]            NVARCHAR (100)   NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Halozat__Ver__5B995986] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Haloz__ErvKe__5C8D7DBF] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Halozati_Nyomtatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Haloz__Letre__5E75C631] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Halozati_Nyomtatok] PRIMARY KEY CLUSTERED ([Id] ASC)
);

