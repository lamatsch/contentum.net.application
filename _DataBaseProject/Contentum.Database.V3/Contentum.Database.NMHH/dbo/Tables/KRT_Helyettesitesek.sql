﻿CREATE TABLE [dbo].[KRT_Helyettesitesek] (
    [Id]                            UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Helyette__Id__2DD1C37F] DEFAULT (newsequentialid()) NOT NULL,
    [Felhasznalo_ID_helyettesitett] UNIQUEIDENTIFIER NOT NULL,
    [CsoportTag_ID_helyettesitett]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_ID_helyettesito]   UNIQUEIDENTIFIER NOT NULL,
    [HelyettesitesMod]              NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [HelyettesitesKezd]             DATETIME         NULL,
    [HelyettesitesVege]             DATETIME         NULL,
    [Megjegyzes]                    NVARCHAR (4000)  NULL,
    [Ver]                           INT              CONSTRAINT [DF__KRT_Helyett__Ver__2EC5E7B8] DEFAULT ((1)) NULL,
    [Note]                          NVARCHAR (4000)  NULL,
    [Stat_id]                       UNIQUEIDENTIFIER NULL,
    [ErvKezd]                       DATETIME         CONSTRAINT [DF__KRT_Helye__ErvKe__2FBA0BF1] DEFAULT (getdate()) NULL,
    [ErvVege]                       DATETIME         CONSTRAINT [DF_KRT_Helyettesitesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                  UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                 DATETIME         CONSTRAINT [DF__KRT_Helye__Letre__31A25463] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                   UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                  DATETIME         NULL,
    [Zarolo_id]                     UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                    DATETIME         NULL,
    [Tranz_id]                      UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                UNIQUEIDENTIFIER NULL,
    CONSTRAINT [HEL_PK_I] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [HEL_HELYETTESITETT_FK] FOREIGN KEY ([Felhasznalo_ID_helyettesitett]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [HEL_HELYETTESITO_FK] FOREIGN KEY ([Felhasznalo_ID_helyettesito]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [KRT_HELYETTESITES_CSOPORTTAG_FK] FOREIGN KEY ([CsoportTag_ID_helyettesitett]) REFERENCES [dbo].[KRT_CsoportTagok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [HEL_HELYETTESITO_FK_I]
    ON [dbo].[KRT_Helyettesitesek]([Felhasznalo_ID_helyettesito] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [HEL_HELYETTESITETT_FK_I]
    ON [dbo].[KRT_Helyettesitesek]([Felhasznalo_ID_helyettesitett] ASC) WITH (FILLFACTOR = 85);

