﻿CREATE TABLE [dbo].[KRT_Jogosultak] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Jogosult__Id__347EC10E] DEFAULT (newsequentialid()) NOT NULL,
    [Csoport_Id_Jogalany] UNIQUEIDENTIFIER NOT NULL,
    [Jogszint]            NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Obj_Id]              UNIQUEIDENTIFIER NOT NULL,
    [Orokolheto]          CHAR (1)         CONSTRAINT [DF__KRT_Jogos__Oroko__3572E547] DEFAULT ('0') NULL,
    [Kezi]                CHAR (1)         NOT NULL,
    [Tipus]               CHAR (1)         CONSTRAINT [DF__KRT_Jogos__Tipus__36670980] DEFAULT ('0') NULL,
    [Ver]                 INT              CONSTRAINT [DF__KRT_Jogosul__Ver__375B2DB9] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__KRT_Jogos__ErvKe__384F51F2] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_KRT_Jogosultak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__KRT_Jogos__Letre__3A379A64] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_JOGOSULTAK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_JOGOSULTAK_CSOP_FK] FOREIGN KEY ([Csoport_Id_Jogalany]) REFERENCES [dbo].[KRT_Csoportok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_JOGOSULTAK_CSOP_I]
    ON [dbo].[KRT_Jogosultak]([Csoport_Id_Jogalany] ASC, [Obj_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_JOGOSULTAK_ACL_I]
    ON [dbo].[KRT_Jogosultak]([Tipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Csoport_Id_Jogalany]
    ON [dbo].[KRT_Jogosultak]([Obj_Id] ASC)
    INCLUDE([Csoport_Id_Jogalany]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Jogosultak_Csoport_Id_Jogalany]
    ON [dbo].[KRT_Jogosultak]([Csoport_Id_Jogalany] ASC, [Obj_Id] ASC)
    INCLUDE([Tipus]) WITH (FILLFACTOR = 85);

