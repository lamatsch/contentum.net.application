﻿CREATE TABLE [dbo].[KRT_KodCsoportok] (
    [Id]                          UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_KodCsopo__Id__3D14070F] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                         UNIQUEIDENTIFIER NULL,
    [KodTarak_Id_KodcsoportTipus] UNIQUEIDENTIFIER NULL,
    [Kod]                         NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Nev]                         NVARCHAR (400)   NOT NULL,
    [Modosithato]                 CHAR (1)         NOT NULL,
    [Hossz]                       INT              NULL,
    [Csoport_Id_Tulaj]            UNIQUEIDENTIFIER NULL,
    [Leiras]                      NVARCHAR (4000)  NULL,
    [BesorolasiSema]              CHAR (1)         NOT NULL,
    [KiegAdat]                    CHAR (1)         NULL,
    [KiegMezo]                    NVARCHAR (400)   NULL,
    [KiegAdattipus]               CHAR (1)         NULL,
    [KiegSzotar]                  NVARCHAR (100)   NULL,
    [Ver]                         INT              CONSTRAINT [DF__KRT_KodCsop__Ver__3E082B48] DEFAULT ((1)) NULL,
    [Note]                        NVARCHAR (4000)  NULL,
    [Stat_id]                     UNIQUEIDENTIFIER NULL,
    [ErvKezd]                     DATETIME         CONSTRAINT [DF__KRT_KodCs__ErvKe__3EFC4F81] DEFAULT (getdate()) NULL,
    [ErvVege]                     DATETIME         CONSTRAINT [DF_KRT_KodCsoportok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]                UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]               DATETIME         CONSTRAINT [DF__KRT_KodCs__Letre__40E497F3] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                 UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                DATETIME         NULL,
    [Zarolo_id]                   UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                  DATETIME         NULL,
    [Tranz_id]                    UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]              UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KCS_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_KodCsoport_UK]
    ON [dbo].[KRT_KodCsoportok]([Org] ASC, [Kod] ASC) WITH (FILLFACTOR = 85);

