﻿CREATE TABLE [dbo].[KRT_KodTarak] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_KodTarak__Id__43C1049E] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER NULL,
    [KodCsoport_Id]      UNIQUEIDENTIFIER NOT NULL,
    [ObjTip_Id_AdatElem] UNIQUEIDENTIFIER NULL,
    [Obj_Id]             UNIQUEIDENTIFIER NULL,
    [Kod]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Nev]                NVARCHAR (400)   NOT NULL,
    [RovidNev]           NVARCHAR (10)    NULL,
    [Egyeb]              NVARCHAR (4000)  NULL,
    [Modosithato]        CHAR (1)         NOT NULL,
    [Sorrend]            NVARCHAR (100)   NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_KodTara__Ver__44B528D7] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_KodTa__ErvKe__45A94D10] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_KodTarak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_KodTa__Letre__47919582] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_KODTARAK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [CK_KRT_KodTarak] CHECK ((0)=[dbo].[fn_KRT_KodTarakCheckUK]([Org],[KodCsoport_Id],[Kod],[Id],[ErvKezd],[ErvVege])),
    CONSTRAINT [KodTar_Kodcsoport_FK] FOREIGN KEY ([KodCsoport_Id]) REFERENCES [dbo].[KRT_KodCsoportok] ([Id]),
    CONSTRAINT [Kodtar_ObjTipus_FK] FOREIGN KEY ([ObjTip_Id_AdatElem]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KTR_KCS_FK_I]
    ON [dbo].[KRT_KodTarak]([KodCsoport_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Objektum]
    ON [dbo].[KRT_KodTarak]([Obj_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_kodok_UK]
    ON [dbo].[KRT_KodTarak]([Org] ASC, [KodCsoport_Id] ASC, [Kod] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IX_Kod]
    ON [dbo].[KRT_KodTarak]([Org] ASC, [Kod] ASC) WITH (FILLFACTOR = 85);

