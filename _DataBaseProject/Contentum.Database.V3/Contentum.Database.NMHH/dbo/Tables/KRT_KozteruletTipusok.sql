﻿CREATE TABLE [dbo].[KRT_KozteruletTipusok] (
    [Id]                        UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Kozterul__Id__511AFFBC] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                       UNIQUEIDENTIFIER NULL,
    [KozterTipNev_Id_Szinonima] UNIQUEIDENTIFIER NULL,
    [Nev]                       NVARCHAR (100)   NOT NULL,
    [Ver]                       INT              CONSTRAINT [DF__KRT_Kozteru__Ver__520F23F5] DEFAULT ((1)) NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         CONSTRAINT [DF__KRT_Kozte__ErvKe__5303482E] DEFAULT (getdate()) NULL,
    [ErvVege]                   DATETIME         CONSTRAINT [DF_KRT_KozteruletTipusok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         CONSTRAINT [DF__KRT_Kozte__Letre__54EB90A0] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KozteruletTipusNevek] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK] FOREIGN KEY ([KozterTipNev_Id_Szinonima]) REFERENCES [dbo].[KRT_KozteruletTipusok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KozteruletTipusNev_Nev_I]
    ON [dbo].[KRT_KozteruletTipusok]([Nev] ASC) WITH (FILLFACTOR = 85);

