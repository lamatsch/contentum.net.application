﻿CREATE TABLE [dbo].[KRT_Log_Login] (
    [Date]              DATETIME         NOT NULL,
    [Status]            CHAR (1)         NULL,
    [StartDate]         DATETIME         NULL,
    [Felhasznalo_Id]    UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Nev]   NVARCHAR (100)   NULL,
    [CsoportTag_Id]     UNIQUEIDENTIFIER NULL,
    [Helyettesito_Id]   UNIQUEIDENTIFIER NULL,
    [Helyettesito_Nev]  NVARCHAR (100)   NULL,
    [Helyettesites_Id]  UNIQUEIDENTIFIER NULL,
    [Helyettesites_Mod] CHAR (2)         NULL,
    [LoginType]         NVARCHAR (100)   NULL,
    [UserHostAddress]   NVARCHAR (100)   NULL,
    [Level]             NVARCHAR (10)    NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [Message]           NVARCHAR (4000)  NULL,
    [HibaKod]           NVARCHAR (4000)  NULL,
    [HibaUzenet]        NVARCHAR (4000)  NULL
);

