﻿CREATE TABLE [dbo].[KRT_Log_Page] (
    [Date]           DATETIME         NOT NULL,
    [Status]         CHAR (1)         NULL,
    [StartDate]      DATETIME         NULL,
    [Modul_Id]       UNIQUEIDENTIFIER NULL,
    [Login_Tranz_id] UNIQUEIDENTIFIER NULL,
    [Name]           NVARCHAR (100)   NULL,
    [Url]            NVARCHAR (4000)  NULL,
    [QueryString]    NVARCHAR (4000)  NULL,
    [Command]        NVARCHAR (100)   NULL,
    [IsPostBack]     CHAR (1)         NULL,
    [IsAsync]        CHAR (1)         NULL,
    [Level]          NVARCHAR (10)    NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [Message]        NVARCHAR (4000)  NULL,
    [HibaKod]        NVARCHAR (4000)  NULL,
    [HibaUzenet]     NVARCHAR (4000)  NULL
);

