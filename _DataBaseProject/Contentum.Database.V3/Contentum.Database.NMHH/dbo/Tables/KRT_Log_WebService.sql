﻿CREATE TABLE [dbo].[KRT_Log_WebService] (
    [Date]               DATETIME         NOT NULL,
    [Status]             CHAR (1)         NULL,
    [StartDate]          DATETIME         NULL,
    [ParentWS_StartDate] DATETIME         NULL,
    [Machine]            NVARCHAR (100)   NULL,
    [Url]                NVARCHAR (4000)  NULL,
    [Name]               NVARCHAR (100)   NULL,
    [Method]             NVARCHAR (100)   NULL,
    [Level]              NVARCHAR (10)    NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [Message]            NVARCHAR (4000)  NULL,
    [HibaKod]            NVARCHAR (4000)  NULL,
    [HibaUzenet]         NVARCHAR (4000)  NULL
);

