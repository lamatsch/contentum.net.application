﻿CREATE TABLE [dbo].[KRT_MappaTartalmak] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_MappaTar__Id__62458BBE] DEFAULT (newsequentialid()) NOT NULL,
    [Mappa_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NOT NULL,
    [Obj_type]       NVARCHAR (100)   NOT NULL,
    [Leiras]         NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_MappaTa__Ver__6339AFF7] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Mappa__ErvKe__642DD430] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_MappaTartalmak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Mappa__Letre__66161CA2] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_MAPPATARTALMAK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_Mapatartalom_Mappa_FK] FOREIGN KEY ([Mappa_Id]) REFERENCES [dbo].[KRT_Mappak] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_UK]
    ON [dbo].[KRT_MappaTartalmak]([Mappa_Id] ASC, [Obj_type] ASC) WITH (FILLFACTOR = 85);

