﻿CREATE TABLE [dbo].[KRT_MappaUtak] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_MappaUta__Id__328C56FB] DEFAULT (newsequentialid()) NOT NULL,
    [Mappa_Id]       UNIQUEIDENTIFIER NOT NULL,
    [PathFromORG]    NVARCHAR (400)   NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_MappaUt__Ver__33807B34] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Mappa__ErvKe__34749F6D] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_MappaUtak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Mappa__Letre__365CE7DF] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_MAPPA_UTAK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [IX_Mappa_Id]
    ON [dbo].[KRT_MappaUtak]([Mappa_Id] ASC) WITH (FILLFACTOR = 85);

