﻿CREATE TABLE [dbo].[KRT_Mappak] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Mappak__Id__5B988E2F] DEFAULT (newsequentialid()) NOT NULL,
    [SzuloMappa_Id]    UNIQUEIDENTIFIER NULL,
    [BarCode]          NVARCHAR (100)   NULL,
    [Tipus]            NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [Nev]              NVARCHAR (400)   NOT NULL,
    [Leiras]           NVARCHAR (4000)  NULL,
    [Csoport_Id_Tulaj] UNIQUEIDENTIFIER NOT NULL,
    [Ver]              INT              CONSTRAINT [DF__KRT_Mappak__Ver__5C8CB268] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__KRT_Mappa__ErvKe__5D80D6A1] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_KRT_Mappak_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__KRT_Mappa__Letre__5F691F13] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_MAPPAK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);

