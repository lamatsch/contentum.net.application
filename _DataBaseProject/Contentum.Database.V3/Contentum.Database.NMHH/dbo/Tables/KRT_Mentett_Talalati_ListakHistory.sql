﻿CREATE TABLE [dbo].[KRT_Mentett_Talalati_ListakHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Mente__Histo__3B219CFC] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [ListName]              NVARCHAR (400)   NULL,
    [SaveDate]              DATETIME         NULL,
    [Searched_Table]        NVARCHAR (400)   NULL,
    [Requestor]             NVARCHAR (400)   NULL,
    [HeaderXML]             NVARCHAR (MAX)   NULL,
    [BodyXML]               NVARCHAR (MAX)   NULL,
    [WorkStation]           NVARCHAR (400)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__KRT_Ment__4D7B4ABD6CC31A31] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_Mentett_Talalati_ListakHistory_ID_VER]
    ON [dbo].[KRT_Mentett_Talalati_ListakHistory]([Id] ASC, [Ver] ASC);

