﻿CREATE TABLE [dbo].[KRT_Menuk] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Menuk__Id__7093AB15] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Menu_Id_Szulo]  UNIQUEIDENTIFIER NULL,
    [Kod]            NVARCHAR (20)    NOT NULL,
    [Nev]            NVARCHAR (100)   NOT NULL,
    [Funkcio_Id]     UNIQUEIDENTIFIER NULL,
    [Modul_Id]       UNIQUEIDENTIFIER NULL,
    [Parameter]      NVARCHAR (400)   NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Menuk__Ver__7187CF4E] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ImageURL]       NVARCHAR (400)   NULL,
    [Sorrend]        NVARCHAR (100)   NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Menuk__ErvKe__727BF387] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Menuk_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Menuk__Letre__74643BF9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [MNU_1_UK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_KRT_MENU_MODUL_MEN_KRT_MODU] FOREIGN KEY ([Modul_Id]) REFERENCES [dbo].[KRT_Modulok] ([Id]),
    CONSTRAINT [Menu_Funkcio_FK] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id]),
    CONSTRAINT [Menu_id_szulo_FK] FOREIGN KEY ([Menu_Id_Szulo]) REFERENCES [dbo].[KRT_Menuk] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [MNU_MDL_FK_I]
    ON [dbo].[KRT_Menuk]([Funkcio_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Menu_UK]
    ON [dbo].[KRT_Menuk]([Org] ASC, [Menu_Id_Szulo] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

