﻿CREATE TABLE [dbo].[KRT_Modul_Funkcio] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Modul_Fu__Id__7740A8A4] DEFAULT (newsequentialid()) NOT NULL,
    [Funkcio_Id]     UNIQUEIDENTIFIER NOT NULL,
    [Modul_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Modul_F__Ver__7834CCDD] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Modul__ErvKe__7928F116] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Modul_Funkcio_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Modul__Letre__7B113988] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_MODUL_FUNKCIO] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Modul_Funkcio__Funkcio_FK] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id]),
    CONSTRAINT [Modul_Funkcio__Modul_FK] FOREIGN KEY ([Modul_Id]) REFERENCES [dbo].[KRT_Modulok] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Funkcio_Modul_UK]
    ON [dbo].[KRT_Modul_Funkcio]([Funkcio_Id] ASC, [Modul_Id] ASC) WITH (FILLFACTOR = 85);

