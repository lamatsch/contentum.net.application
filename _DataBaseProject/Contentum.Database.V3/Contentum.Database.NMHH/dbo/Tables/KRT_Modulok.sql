﻿CREATE TABLE [dbo].[KRT_Modulok] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Modulok__Id__7DEDA633] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER NULL,
    [Tipus]              NVARCHAR (10)    COLLATE Hungarian_CS_AS NULL,
    [Kod]                NVARCHAR (100)   NULL,
    [Nev]                NVARCHAR (100)   NOT NULL,
    [Leiras]             NVARCHAR (4000)  NULL,
    [SelectString]       NVARCHAR (4000)  NULL,
    [Csoport_Id_Tulaj]   UNIQUEIDENTIFIER NULL,
    [Parameterek]        NVARCHAR (4000)  NULL,
    [ObjTip_Id_Adatelem] UNIQUEIDENTIFIER NULL,
    [ObjTip_Adatelem]    NVARCHAR (100)   NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_Modulok__Ver__7EE1CA6C] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Modul__ErvKe__7FD5EEA5] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_Modulok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Modul__Letre__01BE3717] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ASPXLapok] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_Modul_Ind01]
    ON [dbo].[KRT_Modulok]([Org] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

