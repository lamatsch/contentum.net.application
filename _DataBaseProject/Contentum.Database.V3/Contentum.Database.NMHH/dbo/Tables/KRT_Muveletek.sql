﻿CREATE TABLE [dbo].[KRT_Muveletek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Muvelete__Id__049AA3C2] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Kod]            NVARCHAR (20)    NOT NULL,
    [Nev]            NVARCHAR (100)   NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Muvelet__Ver__058EC7FB] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Muvel__ErvKe__0682EC34] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Muveletek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Muvel__Letre__086B34A6] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_MUVELETEK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_Muvelet_Ind01]
    ON [dbo].[KRT_Muveletek]([Org] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

