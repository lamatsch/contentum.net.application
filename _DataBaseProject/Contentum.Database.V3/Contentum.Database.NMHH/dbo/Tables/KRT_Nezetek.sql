﻿CREATE TABLE [dbo].[KRT_Nezetek] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Nezetek__Id__3C15C135] DEFAULT (newsequentialid()) NOT NULL,
    [Org]               UNIQUEIDENTIFIER NOT NULL,
    [Form_Id]           UNIQUEIDENTIFIER NULL,
    [Nev]               NVARCHAR (100)   NOT NULL,
    [Leiras]            NVARCHAR (4000)  NULL,
    [Csoport_Id]        UNIQUEIDENTIFIER NULL,
    [Muvelet_Id]        UNIQUEIDENTIFIER NULL,
    [DisableControls]   NVARCHAR (4000)  NULL,
    [InvisibleControls] NVARCHAR (4000)  NULL,
    [ReadOnlyControls]  NVARCHAR (4000)  NULL,
    [Prioritas]         INT              NULL,
    [Ver]               INT              CONSTRAINT [DF__KRT_Nezetek__Ver__3D09E56E] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__KRT_Nezet__ErvKe__3DFE09A7] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_KRT_Nezetek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__KRT_Nezet__Letre__3FE65219] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    [TabIndexControls]  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_KRT_NEZETEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Nezet_Lap_FK] FOREIGN KEY ([Form_Id]) REFERENCES [dbo].[KRT_Modulok] ([Id]),
    CONSTRAINT [Nezet_Muvelet_FK] FOREIGN KEY ([Muvelet_Id]) REFERENCES [dbo].[KRT_Muveletek] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [KRT_Nezet_Ind01]
    ON [dbo].[KRT_Nezetek]([Org] ASC, [Form_Id] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

