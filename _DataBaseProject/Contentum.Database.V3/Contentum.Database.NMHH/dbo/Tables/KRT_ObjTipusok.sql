﻿CREATE TABLE [dbo].[KRT_ObjTipusok] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_ObjTipus__Id__0D2FE9C3] DEFAULT (newsequentialid()) NOT NULL,
    [ObjTipus_Id_Tipus] UNIQUEIDENTIFIER NULL,
    [Kod]               NVARCHAR (100)   NOT NULL,
    [Nev]               NVARCHAR (100)   NOT NULL,
    [Obj_Id_Szulo]      UNIQUEIDENTIFIER NULL,
    [KodCsoport_Id]     UNIQUEIDENTIFIER NULL,
    [Ver]               INT              CONSTRAINT [DF__KRT_ObjTipu__Ver__0E240DFC] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__KRT_ObjTi__ErvKe__0F183235] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         CONSTRAINT [DF_KRT_ObjTipusok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__KRT_ObjTi__Letre__11007AA7] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [KRT_OBJEKTUMTIPUSOK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_ObjTip_ObjTip_Szulo_FK] FOREIGN KEY ([Obj_Id_Szulo]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id]),
    CONSTRAINT [KRT_ObjTip_ObjTip_Tipus_FK] FOREIGN KEY ([ObjTipus_Id_Tipus]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id]),
    CONSTRAINT [ObjTipus_KodCsoport_FK] FOREIGN KEY ([KodCsoport_Id]) REFERENCES [dbo].[KRT_KodCsoportok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160309-175338]
    ON [dbo].[KRT_ObjTipusok]([ObjTipus_Id_Tipus] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160309-175102]
    ON [dbo].[KRT_ObjTipusok]([ObjTipus_Id_Tipus] ASC, [Kod] ASC);


GO
CREATE NONCLUSTERED INDEX [KRT_ObjTipusok_UK]
    ON [dbo].[KRT_ObjTipusok]([Obj_Id_Szulo] ASC, [Kod] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_ObjTipusok_kod]
    ON [dbo].[KRT_ObjTipusok]([Kod] ASC);

