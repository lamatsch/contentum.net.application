﻿CREATE TABLE [dbo].[KRT_Orgok] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Orgok__Id__13DCE752] DEFAULT (newsequentialid()) NOT NULL,
    [Kod]              NVARCHAR (100)   NOT NULL,
    [Nev]              NVARCHAR (100)   NULL,
    [Sorszam]          INT              NULL,
    [Partner_id_szulo] UNIQUEIDENTIFIER NULL,
    [Ver]              INT              CONSTRAINT [DF__KRT_Orgok__Ver__14D10B8B] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__KRT_Orgok__ErvKe__15C52FC4] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_KRT_Orgok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__KRT_Orgok__Letre__17AD7836] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [SYS_C0047926] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [Org_UK]
    ON [dbo].[KRT_Orgok]([Kod] ASC) WITH (FILLFACTOR = 85);

