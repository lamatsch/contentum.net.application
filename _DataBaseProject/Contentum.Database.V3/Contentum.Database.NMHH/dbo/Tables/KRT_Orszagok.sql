﻿CREATE TABLE [dbo].[KRT_Orszagok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Orszagok__Id__1A89E4E1] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Kod]            NVARCHAR (10)    NULL,
    [Nev]            NVARCHAR (100)   NOT NULL,
    [Viszonylatkod]  NVARCHAR (2)     NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Orszago__Ver__1B7E091A] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Orsza__ErvKe__1C722D53] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Orszagok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Orsza__Letre__1E5A75C5] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Orszagok] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_Orszag_UK]
    ON [dbo].[KRT_Orszagok]([Org] ASC, [Kod] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

