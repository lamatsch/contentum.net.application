﻿CREATE TABLE [dbo].[KRT_Parameterek] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Paramete__Id__2136E270] DEFAULT (newsequentialid()) NOT NULL,
    [Org]            UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id] UNIQUEIDENTIFIER NULL,
    [Nev]            NVARCHAR (400)   NOT NULL,
    [Ertek]          NVARCHAR (400)   NULL,
    [Karbantarthato] CHAR (1)         NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Paramet__Ver__222B06A9] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Param__ErvKe__231F2AE2] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Parameterek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Param__Letre__25077354] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PAR_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Parameter_Felhasznalo_FK] FOREIGN KEY ([Felhasznalo_id]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_PAR_PAR_NEV_UK]
    ON [dbo].[KRT_Parameterek]([Org] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Felhasznalo_id]
    ON [dbo].[KRT_Parameterek]([Felhasznalo_id] ASC) WITH (FILLFACTOR = 85);

