﻿CREATE TABLE [dbo].[KRT_PartnerCimek] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_PartnerC__Id__27E3DFFF] DEFAULT (newsequentialid()) NOT NULL,
    [Partner_id]           UNIQUEIDENTIFIER NULL,
    [Cim_Id]               UNIQUEIDENTIFIER NOT NULL,
    [UtolsoHasznalatSiker] CHAR (1)         NULL,
    [UtolsoHasznalatiIdo]  DATETIME         NULL,
    [eMailKuldes]          CHAR (1)         NULL,
    [Fajta]                NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Partner__Ver__28D80438] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Partn__ErvKe__29CC2871] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         CONSTRAINT [DF_KRT_PartnerCimek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Partn__Letre__2BB470E3] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PTC_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [PartnerCimek_Partner_FK] FOREIGN KEY ([Partner_id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [PTC_PRT_FK_UK]
    ON [dbo].[KRT_PartnerCimek]([Partner_id] ASC, [Cim_Id] ASC, [Fajta] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Cim_Id]
    ON [dbo].[KRT_PartnerCimek]([Cim_Id] ASC) WITH (FILLFACTOR = 85);

