﻿CREATE TABLE [dbo].[KRT_PartnerKapcsolatok] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_PartnerK__Id__3726238F] DEFAULT (newsequentialid()) NOT NULL,
    [Tipus]               NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Partner_id]          UNIQUEIDENTIFIER NULL,
    [Partner_id_kapcsolt] UNIQUEIDENTIFIER NULL,
    [Ver]                 INT              CONSTRAINT [DF__KRT_Partner__Ver__381A47C8] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__KRT_Partn__ErvKe__390E6C01] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         CONSTRAINT [DF_KRT_PartnerKapcsolatok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__KRT_Partn__Letre__3AF6B473] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PTK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [FK_KRT_PART_PTK_PRT_P_KRT_PART] FOREIGN KEY ([Partner_id_kapcsolt]) REFERENCES [dbo].[KRT_Partnerek] ([Id]),
    CONSTRAINT [PTK_PRT_FK] FOREIGN KEY ([Partner_id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [KRT_Partner_KapcsoltPartner_UK]
    ON [dbo].[KRT_PartnerKapcsolatok]([Partner_id] ASC, [Partner_id_kapcsolt] ASC, [Tipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_Partner_KapcsoltPartner_Tipus_I2]
    ON [dbo].[KRT_PartnerKapcsolatok]([Partner_id] ASC, [Tipus] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Partner_id_kapcsolt]
    ON [dbo].[KRT_PartnerKapcsolatok]([Partner_id_kapcsolt] ASC) WITH (FILLFACTOR = 85);

