﻿CREATE TABLE [dbo].[KRT_PartnerMinositesek] (
    [Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_PartnerM__Id__40DA7652] DEFAULT (newsequentialid()) NOT NULL,
    [ALLAPOT]              CHAR (1)         COLLATE Hungarian_CS_AS NOT NULL,
    [Partner_id]           UNIQUEIDENTIFIER NOT NULL,
    [Felhasznalo_id_kero]  UNIQUEIDENTIFIER NOT NULL,
    [KertMinosites]        NVARCHAR (64)    COLLATE Hungarian_CS_AS NOT NULL,
    [KertKezdDat]          DATETIME         NULL,
    [KertVegeDat]          DATETIME         NULL,
    [KerelemAzonosito]     NVARCHAR (100)   NULL,
    [KerelemBeadIdo]       DATETIME         CONSTRAINT [DF__KRT_Partn__Kerel__41CE9A8B] DEFAULT (getdate()) NOT NULL,
    [KerelemDontesIdo]     DATETIME         CONSTRAINT [DF_KRT_PartnerMinositesek_KerelemDontesIdo] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NOT NULL,
    [Felhasznalo_id_donto] UNIQUEIDENTIFIER NOT NULL,
    [DontesAzonosito]      NVARCHAR (100)   NULL,
    [Minosites]            NVARCHAR (2)     COLLATE Hungarian_CS_AS NULL,
    [MinositesKezdDat]     DATETIME         NULL,
    [MinositesVegDat]      DATETIME         NULL,
    [SztornirozasDat]      DATETIME         NULL,
    [Ver]                  INT              CONSTRAINT [DF__KRT_Partner__Ver__43B6E2FD] DEFAULT ((1)) NULL,
    [Note]                 NVARCHAR (4000)  NULL,
    [Stat_id]              UNIQUEIDENTIFIER NULL,
    [ErvKezd]              DATETIME         CONSTRAINT [DF__KRT_Partn__ErvKe__44AB0736] DEFAULT (getdate()) NULL,
    [ErvVege]              DATETIME         CONSTRAINT [DF_KRT_PartnerMinositesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]         UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]        DATETIME         CONSTRAINT [DF__KRT_Partn__Letre__46934FA8] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]          UNIQUEIDENTIFIER NULL,
    [ModositasIdo]         DATETIME         NULL,
    [Zarolo_id]            UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]           DATETIME         NULL,
    [Tranz_id]             UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PSEC_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [PartnerMinositesek_Felhasznalo_DontoFK] FOREIGN KEY ([Felhasznalo_id_donto]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [PartnerMinositesek_KeroFelhasznalo_FK] FOREIGN KEY ([Felhasznalo_id_kero]) REFERENCES [dbo].[KRT_Felhasznalok] ([Id]),
    CONSTRAINT [PartnerMinositesek_Partner_FK] FOREIGN KEY ([Partner_id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [TPSEC_MINOSITES_I]
    ON [dbo].[KRT_PartnerMinositesek]([Partner_id] ASC, [Felhasznalo_id_kero] ASC, [Felhasznalo_id_donto] ASC, [ALLAPOT] ASC, [Minosites] ASC, [KertMinosites] ASC) WITH (FILLFACTOR = 85);

