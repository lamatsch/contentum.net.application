﻿/*==============================================================*/
/* Table: KRT_Partner_Dokumentumok                              */
/*==============================================================*/
create table KRT_Partner_Dokumentumok (
   Id                   uniqueidentifier     not null default (newsequentialid()),
   Partner_id           uniqueidentifier     null,
   Dokumentum_Id        uniqueidentifier     null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_KRT_PARTNER_DOKUMENTUMOK primary key (Id)
)
go

/*==============================================================*/
/* Index: IX_PartnerDokumentumok_PartnerId                      */
/*==============================================================*/
create index IX_PartnerDokumentumok_PartnerId on KRT_Partner_Dokumentumok (
Partner_id ASC
)
go
