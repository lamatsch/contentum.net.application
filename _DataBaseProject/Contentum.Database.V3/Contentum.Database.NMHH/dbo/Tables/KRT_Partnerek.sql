﻿CREATE TABLE [dbo].[KRT_Partnerek] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Partnere__Id__2E90DD8E] DEFAULT (newsequentialid()) NOT NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Orszag_Id]             UNIQUEIDENTIFIER NULL,
    [Tipus]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Hierarchia]            NVARCHAR (400)   NULL,
    [KulsoAzonositok]       NVARCHAR (100)   NULL,
    [PublikusKulcs]         NVARCHAR (4000)  NULL,
    [Kiszolgalo]            UNIQUEIDENTIFIER NULL,
    [LetrehozoSzervezet]    UNIQUEIDENTIFIER NULL,
    [MaxMinosites]          NVARCHAR (2)     NULL,
    [MinositesKezdDat]      DATETIME         CONSTRAINT [DF__KRT_Partn__Minos__2F8501C7] DEFAULT (getdate()) NULL,
    [MinositesVegDat]       DATETIME         CONSTRAINT [DF_KRT_Partnerek_MinositesVegDat] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [MaxMinositesSzervezet] NVARCHAR (2)     NULL,
    [Belso]                 CHAR (1)         NULL,
    [Forras]                CHAR (1)         NULL,
	[Allapot]               Nvarchar(100)    NULL,
	[AspAdoTorolve]         CHAR (1)		 '0',
    [Ver]                   INT              CONSTRAINT [DF__KRT_Partner__Ver__316D4A39] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__KRT_Partn__ErvKe__32616E72] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_KRT_Partnerek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__KRT_Partn__Letre__3449B6E4] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
	[UtolsoHasznalat]  AS ([dbo].[fn_GetPartnerUtolsoHasznalat]([Id])),
    CONSTRAINT [PRT_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [Partner_ErvVege_I]
    ON [dbo].[KRT_Partnerek]([ErvVege] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Partner_ErvKezd_I]
    ON [dbo].[KRT_Partnerek]([ErvKezd] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_PRT_MEGNEVEZES]
    ON [dbo].[KRT_Partnerek]([Org] ASC, [Nev] ASC, [Orszag_Id] ASC, [ErvKezd] ASC, [ErvVege] ASC)
    INCLUDE([Id], [Belso], [Forras]) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [KRT_Partner_Hier_I]
    ON [dbo].[KRT_Partnerek]([Hierarchia] ASC) WITH (FILLFACTOR = 85);
GO
CREATE NONCLUSTERED INDEX [_dta_index_KRT_Partnerek_5_1600112841__K16]
    ON [dbo].[KRT_Partnerek]([Forras] ASC);
GO

create index Partner_Allapot on KRT_Partnerek (
Allapot ASC
)
GO

