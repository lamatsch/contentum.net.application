﻿CREATE TABLE [dbo].[KRT_RagSzamok] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Kod]            NVARCHAR (100)   NOT NULL,
    [KodNum]         FLOAT (53)       NULL,
    [Postakonyv_Id]  UNIQUEIDENTIFIER NULL,
    [RagszamSav_Id]  UNIQUEIDENTIFIER NULL,
    [Obj_Id]         UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]      UNIQUEIDENTIFIER NULL,
    [Obj_type]       NVARCHAR (100)   NULL,
    [KodType]        CHAR (1)         NULL,
    [Allapot]        NVARCHAR (64)    NULL,
    [Ver]            INT              DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_RagSzamok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_RAGSZAMOK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA] FOREIGN KEY ([Postakonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_2]
    ON [dbo].[KRT_RagSzamok]([RagszamSav_Id] ASC, [KodNum] DESC);


GO
CREATE NONCLUSTERED INDEX [Ragszam_UK]
    ON [dbo].[KRT_RagSzamok]([Kod] ASC);

