﻿CREATE TABLE [dbo].[KRT_RagszamSavok] (
    [Id]                 UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Org]                UNIQUEIDENTIFIER DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [Csoport_Id_Felelos] UNIQUEIDENTIFIER NOT NULL,
    [Postakonyv_Id]      UNIQUEIDENTIFIER NULL,
    [SavKezd]            NVARCHAR (100)   NOT NULL,
    [SavKezdNum]         FLOAT (53)       NULL,
    [SavVege]            NVARCHAR (100)   NOT NULL,
    [SavVegeNum]         FLOAT (53)       NULL,
	[IgenyeltDarabszam]	 FLOAT (53)		  NULL, 	
    [SavType]            NVARCHAR (100)   NULL,
    [SavAllapot]         CHAR (1)         NULL,
    [Ver]                INT              DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_RagszamSavok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_RAGSZAMSAVOK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_KRT_RAGS_EREC_IRA_IKTKONYV] FOREIGN KEY ([Postakonyv_Id]) REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id]),
    CONSTRAINT [FK_KRT_RAGS_KRT_CSOP] FOREIGN KEY ([Csoport_Id_Felelos]) REFERENCES [dbo].[KRT_Csoportok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Sav_Vege_I]
    ON [dbo].[KRT_RagszamSavok]([SavVege] ASC);


GO
CREATE NONCLUSTERED INDEX [Sav_Kezd_I]
    ON [dbo].[KRT_RagszamSavok]([SavKezd] ASC);

