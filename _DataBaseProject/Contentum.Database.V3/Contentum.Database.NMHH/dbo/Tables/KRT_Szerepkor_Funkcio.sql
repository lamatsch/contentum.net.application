﻿CREATE TABLE [dbo].[KRT_Szerepkor_Funkcio] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Szerepko__Id__475C8B58] DEFAULT (newsequentialid()) NOT NULL,
    [Funkcio_Id]     UNIQUEIDENTIFIER NOT NULL,
    [Szerepkor_Id]   UNIQUEIDENTIFIER NOT NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Szerepk__Ver__4850AF91] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Szere__ErvKe__4944D3CA] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Szerepkor_Funkcio_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Szere__Letre__4B2D1C3C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [SKF_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [SKF_FUN_FK] FOREIGN KEY ([Funkcio_Id]) REFERENCES [dbo].[KRT_Funkciok] ([Id]),
    CONSTRAINT [SzerepkorFunkcio_Szerepkor_FK] FOREIGN KEY ([Szerepkor_Id]) REFERENCES [dbo].[KRT_Szerepkorok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Szerepkor_Funkcio_UK]
    ON [dbo].[KRT_Szerepkor_Funkcio]([Szerepkor_Id] ASC, [Funkcio_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Funkcio_Id]
    ON [dbo].[KRT_Szerepkor_Funkcio]([Funkcio_Id] ASC) WITH (FILLFACTOR = 85);

