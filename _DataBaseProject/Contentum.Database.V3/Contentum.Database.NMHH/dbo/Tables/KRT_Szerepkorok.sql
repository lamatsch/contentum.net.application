﻿CREATE TABLE [dbo].[KRT_Szerepkorok] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Szerepko__Id__4E0988E7] DEFAULT (newsequentialid()) NOT NULL,
    [Org]             UNIQUEIDENTIFIER NULL,
    [Nev]             NVARCHAR (100)   NOT NULL,
    [KulsoAzonositok] NVARCHAR (100)   NULL,
    [Ver]             INT              CONSTRAINT [DF__KRT_Szerepk__Ver__4EFDAD20] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__KRT_Szere__ErvKe__4FF1D159] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         CONSTRAINT [DF_KRT_Szerepkorok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__KRT_Szere__Letre__51DA19CB] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [SRK_PK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_Szerepkor_UK]
    ON [dbo].[KRT_Szerepkorok]([Org] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

