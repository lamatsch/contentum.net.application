﻿CREATE TABLE [dbo].[KRT_TanusitvanyTipusAlairas] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tanusitv__Id__5A6F5FCC] DEFAULT (newsequentialid()) NOT NULL,
    [AlairasTipus_Id]     UNIQUEIDENTIFIER NULL,
    [TanusitvanyTipus_Id] UNIQUEIDENTIFIER NOT NULL,
    [Ver]                 INT              CONSTRAINT [DF__KRT_Tanusit__Ver__5B638405] DEFAULT ((1)) NULL,
    [Note]                NVARCHAR (4000)  NULL,
    [Stat_id]             UNIQUEIDENTIFIER NULL,
    [ErvKezd]             DATETIME         CONSTRAINT [DF__KRT_Tanus__ErvKe__5C57A83E] DEFAULT (getdate()) NULL,
    [ErvVege]             DATETIME         NULL,
    [Letrehozo_id]        UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]       DATETIME         CONSTRAINT [DF__KRT_Tanus__Letre__5D4BCC77] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]         UNIQUEIDENTIFIER NULL,
    [ModositasIdo]        DATETIME         NULL,
    [Zarolo_id]           UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]          DATETIME         NULL,
    [Tranz_id]            UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TANUSITVANYTIPUSALAIRAS] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_TANALAIRAS_ALAIRASTIP_FK] FOREIGN KEY ([AlairasTipus_Id]) REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
);

