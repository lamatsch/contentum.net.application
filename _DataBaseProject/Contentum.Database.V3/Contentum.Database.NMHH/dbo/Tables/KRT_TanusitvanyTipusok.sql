﻿CREATE TABLE [dbo].[KRT_TanusitvanyTipusok] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tanusitv__Id__60283922] DEFAULT (newsequentialid()) NOT NULL,
    [Org]              UNIQUEIDENTIFIER NULL,
    [Nev]              NVARCHAR (100)   NOT NULL,
    [TanusitvanyTipus] NVARCHAR (8)     NULL,
    [Ver]              INT              CONSTRAINT [DF__KRT_Tanusit__Ver__611C5D5B] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__KRT_Tanus__ErvKe__62108194] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__KRT_Tanus__Letre__6304A5CD] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TANUSITVANYTIPUSOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);

