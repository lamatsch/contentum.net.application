﻿CREATE TABLE [dbo].[KRT_Tartomanyok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tartoman__Id__65E11278] DEFAULT (newsequentialid()) NOT NULL,
    [TartomanyKezd]  NUMERIC (12)     NULL,
    [TartomanyHossz] NUMERIC (12)     NULL,
    [FoglaltDarab]   NUMERIC (12)     NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Tartoma__Ver__66D536B1] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Tarto__ErvKe__67C95AEA] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Tartomanyok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Tarto__Letre__69B1A35C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TARTOMANYOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);

