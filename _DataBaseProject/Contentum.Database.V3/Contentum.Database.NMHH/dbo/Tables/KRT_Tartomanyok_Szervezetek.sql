﻿CREATE TABLE [dbo].[KRT_Tartomanyok_Szervezetek] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tartoman__Id__6C8E1007] DEFAULT (newsequentialid()) NOT NULL,
    [Tartomany_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Csoport_Id_Felelos] UNIQUEIDENTIFIER NULL,
    [TartType]           CHAR (1)         NOT NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_Tartoma__Ver__6D823440] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Tarto__ErvKe__6E765879] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_Tartomanyok_Szervezetek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Tarto__Letre__705EA0EB] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TARTOMANYOK_SZERVEZETEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_TARTSZERV_BARKODTART_FK] FOREIGN KEY ([Tartomany_Id]) REFERENCES [dbo].[KRT_Tartomanyok] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Tartomany_Szervezet]
    ON [dbo].[KRT_Tartomanyok_Szervezetek]([TartType] ASC, [Csoport_Id_Felelos] ASC) WITH (FILLFACTOR = 85);

