﻿CREATE TABLE [dbo].[KRT_Telepulesek] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Telepule__Id__733B0D96] DEFAULT (newsequentialid()) NOT NULL,
    [Org]             UNIQUEIDENTIFIER NULL,
    [IRSZ]            NVARCHAR (10)    NOT NULL,
    [Telepules_Id_Fo] UNIQUEIDENTIFIER NULL,
    [Nev]             NVARCHAR (100)   NOT NULL,
    [Orszag_Id]       UNIQUEIDENTIFIER NULL,
    [Megye]           NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Regio]           NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]             INT              CONSTRAINT [DF__KRT_Telepul__Ver__742F31CF] DEFAULT ((1)) NULL,
    [Note]            NVARCHAR (4000)  NULL,
    [Stat_id]         UNIQUEIDENTIFIER NULL,
    [ErvKezd]         DATETIME         CONSTRAINT [DF__KRT_Telep__ErvKe__75235608] DEFAULT (getdate()) NULL,
    [ErvVege]         DATETIME         CONSTRAINT [DF_KRT_Telepulesek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]   DATETIME         CONSTRAINT [DF__KRT_Telep__Letre__770B9E7A] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]    DATETIME         NULL,
    [Zarolo_id]       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]      DATETIME         NULL,
    [Tranz_id]        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Telepulesek] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Telepules_Orszag_FK] FOREIGN KEY ([Orszag_Id]) REFERENCES [dbo].[KRT_Orszagok] ([Id]),
    CONSTRAINT [Telepules_telepules_fo_FK] FOREIGN KEY ([Telepules_Id_Fo]) REFERENCES [dbo].[KRT_Telepulesek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IND_MEGYE_MEGN_TLP]
    ON [dbo].[KRT_Telepulesek]([Megye] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [IND_MEGN_IRSZ_TLP_UK]
    ON [dbo].[KRT_Telepulesek]([Org] ASC, [IRSZ] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Telepules_Id_Fo]
    ON [dbo].[KRT_Telepulesek]([Telepules_Id_Fo] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [FK_Orszag_Id]
    ON [dbo].[KRT_Telepulesek]([Orszag_Id] ASC) WITH (FILLFACTOR = 85);

