﻿CREATE TABLE [dbo].[KRT_TemplateManager] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Template__Id__478773E1] DEFAULT (newsequentialid()) NOT NULL,
    [Nev]               NVARCHAR (100)   NOT NULL,
    [Tipus]             NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [KRT_Modul_Id]      UNIQUEIDENTIFIER NULL,
    [KRT_Dokumentum_Id] UNIQUEIDENTIFIER NULL,
    [Ver]               INT              CONSTRAINT [DF__KRT_Templat__Ver__487B981A] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (4000)  NULL,
    [Stat_id]           UNIQUEIDENTIFIER NULL,
    [ErvKezd]           DATETIME         CONSTRAINT [DF__KRT_Templ__ErvKe__496FBC53] DEFAULT (getdate()) NULL,
    [ErvVege]           DATETIME         NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__KRT_Templ__Letre__4A63E08C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Zarolo_id]         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]        DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TEMPLATEMANAGER] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [KRT_TemplateManager_KRT_Dokumentum_FK] FOREIGN KEY ([KRT_Dokumentum_Id]) REFERENCES [dbo].[KRT_Dokumentumok] ([Id]),
    CONSTRAINT [KRT_TemplateManager_KRT_Modulok_FK] FOREIGN KEY ([KRT_Modul_Id]) REFERENCES [dbo].[KRT_Modulok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [Index_2]
    ON [dbo].[KRT_TemplateManager]([KRT_Modul_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE NONCLUSTERED INDEX [Index_1]
    ON [dbo].[KRT_TemplateManager]([KRT_Dokumentum_Id] ASC) WITH (FILLFACTOR = 85);

