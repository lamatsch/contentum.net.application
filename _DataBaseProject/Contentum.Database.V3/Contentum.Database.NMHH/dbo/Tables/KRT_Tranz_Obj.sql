﻿CREATE TABLE [dbo].[KRT_Tranz_Obj] (
    [Id]                 UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tranz_Ob__Id__4B5804C5] DEFAULT (newsequentialid()) NOT NULL,
    [ObjTip_Id_AdatElem] UNIQUEIDENTIFIER NOT NULL,
    [Obj_Id]             UNIQUEIDENTIFIER NULL,
    [Ver]                INT              CONSTRAINT [DF__KRT_Tranz_O__Ver__4C4C28FE] DEFAULT ((1)) NULL,
    [Note]               NVARCHAR (4000)  NULL,
    [Stat_id]            UNIQUEIDENTIFIER NULL,
    [ErvKezd]            DATETIME         CONSTRAINT [DF__KRT_Tranz__ErvKe__4D404D37] DEFAULT (getdate()) NULL,
    [ErvVege]            DATETIME         CONSTRAINT [DF_KRT_Tranz_Obj_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]       UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]      DATETIME         CONSTRAINT [DF__KRT_Tranz__Letre__4F2895A9] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]        UNIQUEIDENTIFIER NULL,
    [ModositasIdo]       DATETIME         NULL,
    [Zarolo_id]          UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]         DATETIME         NULL,
    [Tranz_id]           UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TRANZ_OBJ] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Tranz_Obj_ObjTip_Adatelem_FK] FOREIGN KEY ([ObjTip_Id_AdatElem]) REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
);

