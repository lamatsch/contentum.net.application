﻿CREATE TABLE [dbo].[KRT_Tranzakciok] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Tranzakc__Id__7DB89C09] DEFAULT (newsequentialid()) NOT NULL,
    [Alkalmazas_Id]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id] UNIQUEIDENTIFIER NULL,
    [Ver]            INT              CONSTRAINT [DF__KRT_Tranzak__Ver__7EACC042] DEFAULT ((1)) NULL,
    [Note]           NVARCHAR (4000)  NULL,
    [Stat_id]        UNIQUEIDENTIFIER NULL,
    [ErvKezd]        DATETIME         CONSTRAINT [DF__KRT_Tranz__ErvKe__7FA0E47B] DEFAULT (getdate()) NULL,
    [ErvVege]        DATETIME         CONSTRAINT [DF_KRT_Tranzakciok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]  DATETIME         CONSTRAINT [DF__KRT_Tranz__Letre__01892CED] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]   DATETIME         NULL,
    [Zarolo_id]      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]     DATETIME         NULL,
    [Tranz_id]       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TRANZAKCIOK] PRIMARY KEY NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [TRANZ_ALKALM_FK] FOREIGN KEY ([Alkalmazas_Id]) REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Alkalmazas_Id]
    ON [dbo].[KRT_Tranzakciok]([Alkalmazas_Id] ASC) WITH (FILLFACTOR = 85);


GO
CREATE UNIQUE CLUSTERED INDEX [FK_Felhasznalo_Id]
    ON [dbo].[KRT_Tranzakciok]([Felhasznalo_Id] ASC) WITH (FILLFACTOR = 85);

