﻿CREATE TABLE [dbo].[KRT_UIMezoObjektumErtekek] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_UIMezoOb__Id__04659998] DEFAULT (newsequentialid()) NOT NULL,
    [TemplateTipusNev] NVARCHAR (100)   NOT NULL,
    [Nev]              NVARCHAR (100)   NOT NULL,
    [Alapertelmezett]  CHAR (1)         NULL,
    [Felhasznalo_Id]   UNIQUEIDENTIFIER NOT NULL,
    [TemplateXML]      XML              NULL,
    [UtolsoHasznIdo]   DATETIME         NULL,
    [Ver]              INT              CONSTRAINT [DF__KRT_UIMezoO__Ver__0559BDD1] DEFAULT ((1)) NULL,
    [Note]             NVARCHAR (4000)  NULL,
    [Stat_id]          UNIQUEIDENTIFIER NULL,
    [ErvKezd]          DATETIME         CONSTRAINT [DF__KRT_UIMez__ErvKe__064DE20A] DEFAULT (getdate()) NULL,
    [ErvVege]          DATETIME         CONSTRAINT [DF_KRT_UIMezoObjektumErtekek_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]     UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]    DATETIME         CONSTRAINT [DF__KRT_UIMez__Letre__08362A7C] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]      UNIQUEIDENTIFIER NULL,
    [ModositasIdo]     DATETIME         NULL,
    [Zarolo_id]        UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]       DATETIME         NULL,
    [Tranz_id]         UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]   UNIQUEIDENTIFIER NULL,
    [Org_Id]           UNIQUEIDENTIFIER NULL,
    [Publikus]         CHAR (1)         DEFAULT ((0)) NULL,
    [Szervezet_Id]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_UIMEZOOBJEKTUMERTEKEK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85)
);


GO
CREATE NONCLUSTERED INDEX [KRT_UIMezoObjektumErtekek_TemplatTip_Nev_FelhId_UK]
    ON [dbo].[KRT_UIMezoObjektumErtekek]([TemplateTipusNev] ASC, [Felhasznalo_Id] ASC, [Nev] ASC) WITH (FILLFACTOR = 85);

