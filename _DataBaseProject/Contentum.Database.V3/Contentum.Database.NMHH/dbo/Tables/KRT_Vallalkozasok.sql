﻿CREATE TABLE [dbo].[KRT_Vallalkozasok] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF__KRT_Vallalko__Id__0B129727] DEFAULT (newsequentialid()) NOT NULL,
    [Partner_Id]            UNIQUEIDENTIFIER NOT NULL,
    [Adoszam]               NVARCHAR (20)    NULL,
    [KulfoldiAdoszamJelolo] CHAR (1)         NULL,
    [TB_Torzsszam]          NVARCHAR (20)    NULL,
    [Cegjegyzekszam]        NVARCHAR (100)   NULL,
    [Tipus]                 NVARCHAR (64)    COLLATE Hungarian_CS_AS NULL,
    [Ver]                   INT              CONSTRAINT [DF__KRT_Vallalk__Ver__0C06BB60] DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         CONSTRAINT [DF__KRT_Valla__ErvKe__0CFADF99] DEFAULT (getdate()) NULL,
    [ErvVege]               DATETIME         CONSTRAINT [DF_KRT_Vallalkozasok_ErvVege] DEFAULT (CONVERT([datetime],'4700-12-31',(102))) NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         CONSTRAINT [DF__KRT_Valla__Letre__0EE3280B] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_VALLALKOZASOK] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 85),
    CONSTRAINT [Vallalkozas_Partner_FK] FOREIGN KEY ([Partner_Id]) REFERENCES [dbo].[KRT_Partnerek] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [FK_Partner_Id]
    ON [dbo].[KRT_Vallalkozasok]([Partner_Id] ASC) WITH (FILLFACTOR = 85);

