SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'MAP_META_IraOnkomAdatok') 
)
	CREATE TABLE [dbo].[MAP_META_IraOnkomAdatok](
		[BelsoAzonosito] [nvarchar](100) NULL,
		[TSZO_ID] [uniqueidentifier] NULL,
		[OMA_ID] [uniqueidentifier] NULL
	) ON [PRIMARY]

GO

TRUNCATE TABLE MAP_META_IraOnkomAdatok

GO

INSERT INTO MAP_META_IraOnkomAdatok
	-- Dontest_hoza_allamigazgatasi paraméter --
	SELECT 'Dontest_hoza_allamigazgatasi' AS BelsoAzonosito, 
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontest_hoza_allamigazgatasi') AS TSZO_ID,
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontest_hoza_allamigazgatasi') AS OMA_ID
	UNION ALL
	-- Dontes_formaja_allamigazgatasi paraméter --
	SELECT 'Dontes_formaja_allamigazgatasi', 
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_formaja_allamigazgatasi'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_formaja_allamigazgatasi')
	UNION ALL
	-- Dontest_hozta paraméter (ez az önkormányzati) --
	SELECT 'Dontest_hozta',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Onkormanyzati_hatosagi_ugy')
						and BelsoAzonosito = 'Dontest_hozta'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Onkormanyzati_hatosagi_ugy')
						and BelsoAzonosito = 'Dontest_hozta')
	UNION ALL
	-- Dontes_formaja_onkormanyzati paraméter --
	SELECT 'Dontes_formaja_onkormanyzati',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Onkormanyzati_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_formaja_onkormanyzati'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Onkormanyzati_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_formaja_onkormanyzati')
	UNION ALL
	-- Ugyintezes_idotartama paraméter --
	SELECT 'Ugyintezes_idotartama',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Ugyintezes_idotartama'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Ugyintezes_idotartama')
	UNION ALL
	-- Jogorv_eljarasban_dontes_valtozasa paraméter --
	SELECT 'Jogorv_eljarasban_dontes_valtozasa',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa')
	
	UNION ALL
	-- Jogorvoslati_eljaras_tipusa paraméter --
	SELECT 'Jogorvoslati_eljaras_tipusa',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa')
	UNION ALL
	-- Jogorv_eljarasban_dontes_tipusa paraméter --
	SELECT 'Jogorv_eljarasban_dontes_tipusa',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa')

	UNION ALL
	-- Jogorv_eljarasban_dontest_hozta paraméter --
	SELECT 'Jogorv_eljarasban_dontest_hozta',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta')

	UNION ALL
	-- Jogorv_eljarasban_dontes_tartalma paraméter --
	SELECT 'Jogorv_eljarasban_dontes_tartalma',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma')

	UNION ALL
	-- Hatosagi_ellenorzes_tortent paraméter --
	SELECT 'Hatosagi_ellenorzes_tortent', 
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosagi_ellenorzes_tortent'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosagi_ellenorzes_tortent')

	UNION ALL
	-- Dontes_munkaorak_szama paraméter --
	SELECT 'Dontes_munkaorak_szama',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_munkaorak_szama'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Dontes_munkaorak_szama')

	UNION ALL
	-- Megallapitott_eljarasi_koltseg paraméter --
	SELECT 'Megallapitott_eljarasi_koltseg',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Megallapitott_eljarasi_koltseg'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Megallapitott_eljarasi_koltseg')

	UNION ALL
	-- Kiszabott_kozigazgatasi_birsag paraméter --
	SELECT 'Kiszabott_kozigazgatasi_birsag',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag')

	UNION ALL
	-- Sommas_eljarasban_hozott_dontes paraméter --
	SELECT  'Sommas_eljarasban_hozott_dontes',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes')

	UNION ALL
	-- 8napon_belul_lezart_nem_sommas paraméter --
	SELECT '8napon_belul_lezart_nem_sommas',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = '8napon_belul_lezart_nem_sommas'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = '8napon_belul_lezart_nem_sommas')

	UNION ALL
	-- Fuggo_hatalyu_hatarozat paraméter --
	SELECT 'Fuggo_hatalyu_hatarozat',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Fuggo_hatalyu_hatarozat'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Fuggo_hatalyu_hatarozat')

	UNION ALL
	-- Hatarozat_hatalyba_lepese paraméter --
	SELECT 'Hatarozat_hatalyba_lepese',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatarozat_hatalyba_lepese'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatarozat_hatalyba_lepese')

	UNION ALL
	-- Fuggo_hatalyu_vegzes paraméter --
	SELECT 'Fuggo_hatalyu_vegzes',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Fuggo_hatalyu_vegzes'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Fuggo_hatalyu_vegzes')

	UNION ALL
	-- Vegzes_hatalyba_lepese paraméter --
	SELECT 'Vegzes_hatalyba_lepese',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Vegzes_hatalyba_lepese'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Vegzes_hatalyba_lepese')

	UNION ALL
	-- Hatosag_altal_visszafizetett_osszeg paraméter --
	SELECT 'Hatosag_altal_visszafizetett_osszeg',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg')

	UNION ALL
	-- Hatosagot_terhelo_eljarasi_koltseg paraméter --
	SELECT 'Hatosagot_terhelo_eljarasi_koltseg',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg')

	UNION ALL
	-- Felfuggesztett_hatarozat paraméter --
	SELECT 'Felfuggesztett_hatarozat',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Felfuggesztett_hatarozat'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Felfuggesztett_hatarozat')

	UNION ALL
	-- Hatarido_tullepes_napokban paraméter --
	SELECT 'Hatarido_tullepes_napokban',
	(select t.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatarido_tullepes_napokban'),
	(select oma.Id
						from dbo.EREC_Obj_MetaAdatai oma
						JOIN dbo.EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
						where Obj_MetaDefinicio_Id = (SELECT Id FROM dbo.EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')
						and BelsoAzonosito = 'Hatarido_tullepes_napokban')
