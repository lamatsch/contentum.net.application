 CREATE TABLE [dbo].[WORK_HirdetmenyekSzam]
 (
 	[Cim] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Jovahagyasi_fazis] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Hirdetmeny_szovege] [nvarchar] (max) COLLATE Hungarian_CI_AS NULL,
 	[Hirdeto_szervezet] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Jogszabalyi_hivatkozas] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Szam] [int] NULL,
 	[Elemtipus] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Eleresi_ut] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL
 )