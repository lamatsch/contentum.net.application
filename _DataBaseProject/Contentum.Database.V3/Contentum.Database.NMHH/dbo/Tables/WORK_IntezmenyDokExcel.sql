 CREATE TABLE [dbo].[WORK_IntezmenyDokExcel]
 (
 	[M�dos�tva] [datetime] NULL,
 	[M�dos�totta] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Ehhez tartozik ID] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[�v] [int] NULL,
 	[Sz�m] [int] NULL,
 	[C�m] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Dokumentum t�pus] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Le�r�s] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[N�v] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Publik�ci�s iker Id] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[Elemt�pus] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL,
 	[El�r�si �t] [nvarchar] (255) COLLATE Hungarian_CI_AS NULL
 )