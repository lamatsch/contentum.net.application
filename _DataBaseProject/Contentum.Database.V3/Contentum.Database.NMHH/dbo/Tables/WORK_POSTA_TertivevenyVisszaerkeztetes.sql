﻿CREATE TABLE [dbo].[WORK_POSTA_TertivevenyVisszaerkeztetes] (
    [createdby]                           NVARCHAR (4000) NULL,
    [createdbyname]                       NVARCHAR (4000) NULL,
    [createdbyyominame]                   NVARCHAR (4000) NULL,
    [createdon]                           NVARCHAR (4000) NULL,
    [createdonutc]                        NVARCHAR (4000) NULL,
    [createdonbehalfby]                   NVARCHAR (4000) NULL,
    [createdonbehalfbyname]               NVARCHAR (4000) NULL,
    [createdonbehalfbyyominame]           NVARCHAR (4000) NULL,
    [importsequencenumber]                NVARCHAR (4000) NULL,
    [modifiedby]                          NVARCHAR (4000) NULL,
    [modifiedbyname]                      NVARCHAR (4000) NULL,
    [modifiedbyyominame]                  NVARCHAR (4000) NULL,
    [modifiedon]                          NVARCHAR (4000) NULL,
    [modifiedonutc]                       NVARCHAR (4000) NULL,
    [modifiedonbehalfby]                  NVARCHAR (4000) NULL,
    [modifiedonbehalfbyname]              NVARCHAR (4000) NULL,
    [modifiedonbehalfbyyominame]          NVARCHAR (4000) NULL,
    [overriddencreatedon]                 NVARCHAR (4000) NULL,
    [overriddencreatedonutc]              NVARCHAR (4000) NULL,
    [ownerid]                             NVARCHAR (4000) NULL,
    [owneriddsc]                          NVARCHAR (4000) NULL,
    [owneridname]                         NVARCHAR (4000) NULL,
    [owneridtype]                         NVARCHAR (4000) NULL,
    [owneridyominame]                     NVARCHAR (4000) NULL,
    [owningbusinessunit]                  NVARCHAR (4000) NULL,
    [owningteam]                          NVARCHAR (4000) NULL,
    [owninguser]                          NVARCHAR (4000) NULL,
    [posta_atveteldatuma]                 NVARCHAR (4000) NULL,
    [posta_atveteldatumautc]              NVARCHAR (4000) NULL,
    [posta_atveteljogcime]                NVARCHAR (4000) NULL,
    [posta_atvevoszemely]                 NVARCHAR (4000) NULL,
    [posta_ismertcimzettkuldemeny]        NVARCHAR (4000) NULL,
    [posta_ismertcimzettkuldemenyname]    NVARCHAR (4000) NULL,
    [posta_kezbesiteseredmenye]           NVARCHAR (4000) NULL,
    [posta_kezbesiteseredmenyename]       NVARCHAR (4000) NULL,
    [posta_tertivevenyvisszaerkeztetesid] NVARCHAR (4000) NULL,
    [posta_tertivevenyvonalkodja]         NVARCHAR (4000) NULL,
    [statecode]                           NVARCHAR (4000) NULL,
    [statecodename]                       NVARCHAR (4000) NULL,
    [statuscode]                          NVARCHAR (4000) NULL,
    [statuscodename]                      NVARCHAR (4000) NULL,
    [timezoneruleversionnumber]           NVARCHAR (4000) NULL,
    [utcconversiontimezonecode]           NVARCHAR (4000) NULL,
    [versionnumber]                       NVARCHAR (4000) NULL
);

