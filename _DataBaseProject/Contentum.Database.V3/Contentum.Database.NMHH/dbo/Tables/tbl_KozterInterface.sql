﻿CREATE TABLE [dbo].[tbl_KozterInterface] (
    [ktdok_id]       UNIQUEIDENTIFIER NOT NULL,
    [ktdok_kod]      NVARCHAR (9)     NULL,
    [ktdok_iktszam]  NVARCHAR (14)    NULL,
    [ktdok_vonalkod] NVARCHAR (13)    NULL,
    [ktdok_dokid]    UNIQUEIDENTIFIER NULL,
    [ktdok_url]      NVARCHAR (MAX)   NULL,
    [ktdok_date]     DATETIME         NULL,
    [ktdok_siker]    CHAR (1)         NULL,
    [ktdok_ok]       NVARCHAR (256)   NULL,
    [ktdok_atvet]    DATETIME         NULL
);

