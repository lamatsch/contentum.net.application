﻿CREATE TABLE [dbo].[tmp_UgyiratMegorzIdoMod] (
    [id]                    UNIQUEIDENTIFIER NOT NULL,
    [megorzIdoVege_uj]      DATETIME         NOT NULL,
    [megorzIdo_ev]          INT              NULL,
    [lezarasDat]            DATETIME         NULL,
    [megorzIdoVege_eredeti] DATETIME         NOT NULL,
    [ModositoId_eredeti]    UNIQUEIDENTIFIER NULL,
    [ModositasIdo_eredeti]  DATETIME         NULL,
    [Ver_eredeti]           INT              NULL
);

