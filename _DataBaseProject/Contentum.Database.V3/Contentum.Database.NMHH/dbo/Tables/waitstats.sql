﻿CREATE TABLE [dbo].[waitstats] (
    [wait_type]           NVARCHAR (60) NOT NULL,
    [waiting_tasks_count] BIGINT        NOT NULL,
    [wait_time_ms]        BIGINT        NOT NULL,
    [max_wait_time_ms]    BIGINT        NOT NULL,
    [signal_wait_time_ms] BIGINT        NOT NULL,
    [now]                 DATETIME      CONSTRAINT [DF__waitstats__now__3717A178] DEFAULT (getdate()) NOT NULL
);

