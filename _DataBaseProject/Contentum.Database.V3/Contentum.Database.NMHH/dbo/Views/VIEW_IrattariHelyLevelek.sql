﻿create view [dbo].[VIEW_IrattariHelyLevelek] as 
with C as
(
  select ID,
		 --Id rootId,
		 Vonalkod,
         Ertek,
         --Szuloid,
        Convert(nvarchar(1000),'')  as ParentNames,
		IrattarTipus
		--,
		--Convert(nvarchar(1000),'')  as ParentIds
  from EREC_IrattariHelyek
  where szuloid is null and ErvKezd<=getdate() and ErvVege >=GETDATE()
  
  union all
  select T.ID,
		-- C.rootId,
		 T.Vonalkod,
         T.Ertek,
         --T.Szuloid,
         CONVERT(nvarchar(1000),C.ParentNames + '/' +C.Ertek),
		 T.IrattarTipus
		 --,
		 --CONVERT(nvarchar(1000),Convert(nvarchar(1000),C.ParentIds) + '/' +Convert(nvarchar(1000),C.Id))
  from EREC_IrattariHelyek as T         
    inner join C
      on C.ID = T.Szuloid
   where ErvKezd<=getdate() and ErvVege >=GETDATE()
)      
--select C.*,
--	case
--		when EREC_UgyUgyiratok.Id is null then '1'
--		else '0'
--    end Modosithato from
--(
select distinct C.Id,
	--C.rootId,
	
	case
		when C.ParentNames like '/%' then stuff(C.ParentNames+'/'+C.Ertek, 1, 1, '')
		else C.Ertek
    end Ertek,
	C.Vonalkod,
	C.IrattarTipus
	--,
	--case
	--	when C.ParentIds like '/%' then stuff(C.ParentIds+'/'+Convert(nvarchar(1000),C.Id), 1, 1, '')
	--	else Convert(nvarchar(1000),C.Id)
 --   end IdPath
from C
where C.Id NOT IN (
    SELECT DISTINCT szuloid FROM EREC_IrattariHelyek WHERE SzuloId IS NOT NULL)
--) C
--left join 
--( SELECT * from 
--EREC_UgyUgyiratok where  EREC_UgyUgyiratok.IrattariHely is not null )EREC_UgyUgyiratok on EREC_UgyUgyiratok.IrattariHely = C.Ertek
--group by c.Id,c.Ertek,c.Vonalkod,c.IdPath,
--	case
--		when EREC_UgyUgyiratok.Id is null then '1'
--		else '0'
--    end
;