IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VW_EREC_IraOnkormAdatok]'))
DROP VIEW [dbo].[VW_EREC_IraOnkormAdatok]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	CREATE VIEW [dbo].[VW_EREC_IraOnkormAdatok]	
	WITH SCHEMABINDING
	AS
			SELECT 
			newid() as Id,
			EREC_IraIratok.Id AS IraIratok_Id,
			EREC_IraIratok.Ugy_Fajtaja as UgyFajtaja,
			CASE EREC_IraIratok.Ugy_Fajtaja
				WHEN '1' THEN (SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Dontest_hoza_allamigazgatasi' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja)
				WHEN '2' THEN (SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Dontest_hozta' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
			END AS DontestHozta,		
			CASE EREC_IraIratok.Ugy_Fajtaja
				WHEN '1' THEN (SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Dontes_formaja_allamigazgatasi' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja)
				WHEN '2' THEN (SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Dontes_formaja_onkormanyzati' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
			END AS DontesFormaja,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Ugyintezes_idotartama' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS UgyintezesHataridore,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Hatarido_tullepes_napokban' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS HataridoTullepes,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS JogorvoslatiEljarasTipusa,    
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS JogorvoslatiDontesTipusa,    
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS JogorvoslatiDontestHozta,	
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 		 
				AS JogorvoslatiDontesTartalma,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS JogorvoslatiDontes,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Hatosagi_ellenorzes_tortent' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS HatosagiEllenorzes,
			(SELECT cast(REPLACE(Ertek, ',', '.') as float) as Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Dontes_munkaorak_szama' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS MunkaorakSzama,
			(SELECT cast(REPLACE(Ertek, ',', '.') as float) as Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Megallapitott_eljarasi_koltseg' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS EljarasiKoltseg,
			(SELECT cast(REPLACE(Ertek, ',', '.') as float) as Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS KozigazgatasiBirsagMerteke,
			(SELECT CASE WHEN ISNULL(Ertek,'0') in ('IGEN', '1') THEN 'IGEN' ELSE 'NEM' END FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS SommasEljDontes,
			(SELECT CASE WHEN ISNULL(Ertek,'0') in ('IGEN', '1') THEN 'IGEN' ELSE 'NEM' END FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = '8napon_belul_lezart_nem_sommas' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS NyolcNapBelulNemSommas,
			(SELECT CASE WHEN ISNULL(Ertek,'0') in ('IGEN', '1') THEN 'IGEN' ELSE 'NEM' END FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Fuggo_hatalyu_hatarozat' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS FuggoHatalyuHatarozat,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Hatarozat_hatalyba_lepese' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS FuggoHatHatalybaLepes,
			(SELECT CASE WHEN ISNULL(Ertek,'0') in ('IGEN', '1') THEN 'IGEN' ELSE 'NEM' END FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Fuggo_hatalyu_vegzes' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS FuggoHatalyuVegzes,
			(SELECT Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Vegzes_hatalyba_lepese' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS FuggoVegzesHatalyba,
			(SELECT cast(REPLACE(Ertek, ',', '.') as float) as Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS HatAltalVisszafizOsszeg,
			(SELECT cast(REPLACE(Ertek, ',', '.') as float) as Ertek FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS HatTerheloEljKtsg,
			(SELECT CASE WHEN ISNULL(Ertek,'0') in ('IGEN', '1') THEN 'IGEN' ELSE 'NEM' END FROM dbo.EREC_ObjektumTargyszavai ot with (nolock) inner join dbo.EREC_Obj_MetaAdatai oma with (nolock) on ot.Obj_Metaadatai_Id = oma.Id inner join dbo.EREC_Obj_MetaDefinicio omd with (nolock) on oma.Obj_MetaDefinicio_Id = omd.id INNER JOIN dbo.MAP_META_IraOnkomAdatok mt on ot.Targyszo_Id = mt.TSZO_ID and mt.BelsoAzonosito = 'Felfuggesztett_hatarozat' and ot.Obj_Metaadatai_Id is not NULL and ot.ErvVege > getdate() and ot.Obj_Id = EREC_IraIratok.Id and omd.ColumnValue = EREC_IraIratok.Ugy_Fajtaja) 
				AS FelfuggHatarozat,
			EREC_IraIratok.Ver as Ver,
			EREC_IraIratok.Note as Note,
			EREC_IraIratok.Stat_id as Stat_Id,
			EREC_IraIratok.ErvKezd as ErvKezd,
			EREC_IraIratok.ErvVege as ErvVege,
			EREC_IraIratok.Letrehozo_id as Letrehozo_Id,
			EREC_IraIratok.LetrehozasIdo as LetrehozasIdo,
			EREC_IraIratok.Modosito_id as Modosito_id,
			EREC_IraIratok.ModositasIdo as ModositasIdo,
			EREC_IraIratok.Zarolo_id as Zarolo_id,
			EREC_IraIratok.ZarolasIdo as ZarolasIdo,
			EREC_IraIratok.Tranz_id as Tranz_id,
			EREC_IraIratok.UIAccessLog_id as UIAccessLog_id
		FROM
			dbo.EREC_IraIratok EREC_IraIratok WITH (NOLOCK)
		WHERE 
			ISNULL(EREC_IraIratok.Ugy_Fajtaja,'0') in ('1','2')			
GO