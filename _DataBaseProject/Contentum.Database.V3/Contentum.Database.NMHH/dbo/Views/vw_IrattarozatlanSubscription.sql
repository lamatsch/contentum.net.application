﻿CREATE VIEW [dbo].[vw_IrattarozatlanSubscription] as
      WITH SzervezetHierarchy  AS
      (
         -- Base case
         SELECT cs.Id as UserId
                  , cs.Nev as UserNev
                  , cs.Id as Id
                  , cs.Nev as Nev
                  , csFelettes.Id FelettesId
                  , csFelettes.Nev FelettesNev
                  , csFelettes.Kiszolgalhato FelettesKiszolgalhato
                  ,  0 as HierarchyLevel
         FROM KRT_csoportok cs
            inner join KRT_CsoportTagok cst on cst.Csoport_Id_Jogalany=cs.Id
            inner join KRT_Csoportok csFelettes on cst.Csoport_Id=csFelettes.Id
            where cs.Tipus = '1'
            and getdate() between cs.ErvKezd and cs.ErvVege
            and getdate() between cst.ErvKezd and cst.ErvVege
            and getdate() between csFelettes.ErvKezd and csFelettes.ErvVege

            UNION ALL

         SELECT SzervezetHierarchy.UserId as UserId
                        , SzervezetHierarchy.Nev as UserNev
                        , cs.Id as Id
                        , cs.Nev as Nev
                        , csFelettes.Id FelettesId
                        , csFelettes.Nev FelettesNev
                        , csFelettes.Kiszolgalhato FelettesKiszolgalhato
                        , SzervezetHierarchy.HierarchyLevel + 1 as HierarchyLevel
         FROM SzervezetHierarchy
            inner join KRT_csoportok cs on SzervezetHierarchy.FelettesId = cs.Id
            inner join KRT_CsoportTagok cst on cst.Csoport_Id_Jogalany=cs.Id and getdate() between cst.ErvKezd and cst.ErvVege
            inner join KRT_Csoportok csFelettes on cst.Csoport_Id=csFelettes.Id
            where IsNull(SzervezetHierarchy.FelettesKiszolgalhato, '1') <> '2'
            and getdate() between cs.ErvKezd and cs.ErvVege
            and getdate() between cst.ErvKezd and cst.ErvVege
            and getdate() between csFelettes.ErvKezd and csFelettes.ErvVege
),
Potlista as
(
         SELECT cs.Id as UserId
                  , cs.Nev as UserNev
                  , cs.Id as Id
                  , cs.Nev as Nev
                  , null FelettesId
                  , 'Potlista' FelettesNev
                  , '2' FelettesKiszolgalhato
                  ,  0 as HierarchyLevel
         FROM KRT_csoportok cs
            where cs.Tipus = '1'
				and not exists(select 1 from KRT_CsoportTagok cst where cst.Csoport_Id_Jogalany=cs.Id
				and getdate() between cst.ErvKezd and cst.ErvVege)
)
select DISTINCT
replace(SUBSTRING(krt_csoportok.ErtesitesEmail, 1, CHARINDEX('@', krt_csoportok.ErtesitesEmail)-1),'.','') as [FileName],
-- a user login név mint email cím helyet, megjelentek a családnév.keresztnév(dr.) 
-- ezért a fájlnév és a kiterjesztése hibát okoz.
--SUBSTRING(krt_csoportok.ErtesitesEmail, 1, CHARINDEX('@', krt_csoportok.ErtesitesEmail)-1) as [FileName],
'\\SQL3\Irattarozatlan'+'\' + Replace(dbo.fn_ReplaceSpecialChars(szh.Felettesnev), ' ', '_')
-- '\\SQL3\RS_'+db_name()+'\' + Replace(dbo.fn_ReplaceSpecialChars(szh.Felettesnev), ' ', '_')
--+ EREC_IraIktatokonyvek.Iktatohely
as Path,
IsNull(krt_csoportok_szervezet.nev, szh.FelettesNev) SzervezetNev,
'PDF' as Format,
'SQLCL3\EDOK_RS' as UserName,
'EDOK2011rs' as Password,
'(EREC_UgyUgyiratok.ErvKezd <= getdate() and EREC_UgyUgyiratok.ErvVege >= getdate()) and (EREC_UgyUgyiratok.Allapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52''))) and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(krt_csoportok.id AS nvarchar(40)) + ''' and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN ''1992-01-01 00:00:00'' and ''' + convert(nvarchar(10), DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), 120) + '''  and EREC_UgyUgyiratok.IratSzam > 0 ' as _Where,
--'(EREC_UgyUgyiratok.ErvKezd <= getdate() and EREC_UgyUgyiratok.ErvVege >= getdate()) and (EREC_UgyUgyiratok.Allapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''99''))) and EREC_UgyUgyiratok.IraIktatokonyv_Id in (select Id from EREC_IraIktatoKonyvek where Iktatohely= ''' + EREC_IraIktatokonyvek.Iktatohely + ''' and Ev between 2000 and ' + cast(YEAR(getdate()) as nvarchar(4)) + ') and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(krt_csoportok.id AS nvarchar(40)) + ''' and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN ''2000-01-01 00:00:00'' and ''2010-12-31 23:59:00''  and EREC_UgyUgyiratok.IratSzam > 0 ' as _Where,
--'order by EREC_IraIktatokonyvek.Ev,EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam' as OrderBy,
'order by EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam' as OrderBy,
--krt_csoportok.id as ExecutorUserId,
--krt_csoportok_szervezet.id as FelhasznaloSzervezetId,
(select top 1 cs.Id from KRT_Csoportok cs
	inner join KRT_CsoportTagok cst on cs.Id=cst.Csoport_Id_Jogalany
	where cs.Tipus='1' and cst.Tipus='3' -- futtató szervezet vezeto
	and cst.Csoport_Id='9A570CA9-BEB7-4235-9AB8-E87982948273'
    and getdate() between cs.ErvKezd and cs.ErvVege
    and getdate() between cst.ErvKezd and cst.ErvVege
) as ExecutorUserId,
'9A570CA9-BEB7-4235-9AB8-E87982948273' as FelhasznaloSzervezetId, -- futattó szervezet
'10000' as TopRow,
'((MIG_Foszam.Csatolva_Id is null) and (MIG_Foszam.Edok_Utoirat_Id is null) and (MIG_Foszam.UGYHOL in (''3'',''I'')) and (MIG_Foszam.Edok_Ugyintezo_Csoport_Id=''' + convert(nvarchar(36), krt_csoportok.Id) + ''' or (MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null and MIG_Eloado.NAME=''' + IsNull((select Nev from KRT_Felhasznalok where Id = krt_csoportok.id), krt_csoportok.Nev) + ''')))' as MIG_Where,
'order by MIG_Foszam.EdokSav,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM' as MIG_OrderBy,
'10000' as MIG_TopRow,
--EREC_IraIktatokonyvek.Nev as Iktatokonyv,
krt_csoportok.nev as Ugyintezo,
'1992.01.01. - ' + convert(nvarchar(10), DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), 102) + '.' as Idoszak
from krt_csoportok
left join krt_csoporttagok on krt_csoporttagok.csoport_id_jogalany = krt_csoportok.id and getdate() between krt_csoporttagok.ErvKezd and krt_csoporttagok.ErvVege
left join krt_csoportok as krt_csoportok_szervezet on krt_csoportok_szervezet.id = krt_csoporttagok.csoport_id and getdate() between krt_csoportok_szervezet.ErvKezd and krt_csoportok_szervezet.ErvVege
left join (select * from SzervezetHierarchy UNION ALL select * from Potlista) szh on szh.UserId = krt_csoportok.Id
--left join EREC_Irat_Iktatokonyvei on EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat = krt_csoportok_szervezet.Id and getdate() between EREC_Irat_Iktatokonyvei.ErvKezd and EREC_Irat_Iktatokonyvei.ErvVege
--left join EREC_IraIktatokonyvek on EREC_IraIktatokonyvek.Id = EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id and getdate() between EREC_IraIktatokonyvek.ErvKezd and EREC_IraIktatokonyvek.ErvVege
where (krt_csoporttagok.csoport_id in (select id from krt_csoportok where tipus in ('0', '6'))
	or (krt_csoporttagok.id is null and krt_csoportok.id in (select UserId from Potlista)))
and krt_csoportok.tipus = '1'
and ((getdate() between krt_csoportok.ErvKezd and krt_csoportok.ErvVege) or (krt_csoportok.Id in (select UserId from Potlista)))
and krt_csoportok.ErtesitesEmail IS NOT NULL
--and EREC_IraIktatokonyvek.Ev between 2000 and 2011 
--and EREC_IraIktatokonyvek.IktatoErkezteto = 'I'
and szh.FelettesKiszolgalhato = '2'
and (exists(select 1 from EREC_UgyUgyiratok 
            where ((((EREC_UgyUgyiratok.ErvKezd <= getdate()) and (EREC_UgyUgyiratok.ErvVege >= getdate()))) 
            and (EREC_UgyUgyiratok.Allapot in ('0','03','04','06','09','11','13','52') 
                  or (EREC_Ugyugyiratok.Allapot='50' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in ('0','03','04','06','09','11','13','52')))
            --and EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id  
            and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = krt_csoportok.id  
            and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN '1992-01-01 00:00:00' and DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)) 
and EREC_UgyUgyiratok.IratSzam > 0 )))
group by
szh.Felettesnev,
krt_csoportok_szervezet.nev,
krt_csoportok.id,
krt_csoportok.ErtesitesEmail,
krt_csoportok_szervezet.id,
krt_csoportok.nev
--,
--EREC_IraIktatokonyvek.Nev,
--EREC_IraIktatoKonyvek.Iktatohely