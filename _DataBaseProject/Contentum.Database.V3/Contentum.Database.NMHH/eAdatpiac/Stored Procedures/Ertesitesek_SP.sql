﻿create procedure [eAdatpiac].[Ertesitesek_SP] (
  @datum as datetime, 
  @Szervezet as nvarchar(100),                    -- a szervezet GUID-ja string formában
  @Felhasznalo as nvarchar(100),                  -- a felhasználó GUID, string formátumban
  @PrioritasTol as int = null, 
  @PrioritasIg as int = null,
  @LejaratKatTol as int = null, 
  @LejaratKatIg as int = null
)
as
begin

-- kategória sorrendfordítás
declare @LejaratFordTol int,
        @LejaratFordIg int
select  @LejaratFordTol = @LejaratKatIg,
        @LejaratFordIg  = @LejaratKatTol

select 
  dat.datum
, fel.FrissitesIdo
, fel.SzervezetNev
, fel.FelhasznaloNev
, ft.feladatkod
, ft.Kategoria
, ft.Altipus1
, ft.Altipus2 
, ft.KategoriaKibontas
, ft.URLJel
, ft.URLGlobDefinicio +
  case when ft.FeladatKod like '12%'
			then '&Date='+convert(varchar(10), @datum, 102)
	   else '' end as URLGlobDefinicio
, case when ft.FeladatKod not like '1%' then ft.URLTetDefinicio
	   else NULL end as URLKiegeszites_Altipus1 
, case when ft.FeladatKod like '1%' then ft.URLTetDefinicio
	   else NULL end as URLKiegeszites_Altipus2 
, case when ft.FeladatKod not like '2%' and
            ft.FeladatKod not like '4%' and
            ft.FeladatKod not like '9%'
			then '&Filter=Szervezet'
	   else NULL end as SzervezetiURL
, '&Filter=Sajat' as SzemelyesURL
, sum(Telelszam )                             as Osszes
, sum(case when fstat.keletkezesKod='U' then Telelszam else 0 end) as Uj
, sum(case when fel.FelelosTipusKod='C' then Telelszam else 0 end) as Szervezeti
, sum(case when fel.FelelosTipusKod='F' then Telelszam else 0 end) as Szemelyes

, sum(case when lekat.LejaratKod='T' then Telelszam else 0 end)                      as Lejart
, sum(case when lekat.LejaratKod='E' then Telelszam else 0 end)                      as Lejaratlan
, sum(case when lekat.TetelJel=1 and fstat.FeladatJel='E' then Telelszam else 0 end) as Aktualis

 from eAdatpiac.Fact_Feladatok fact
 inner join eAdatpiac.Dim_FeladatTipus ft on fact.FeladatTipus_Id = ft.id
 inner join eAdatpiac.Dim_FrissitesDatum dat on fact.FrissitesDatum_Id = dat.datum_id
 inner join eAdatpiac.Dim_FeladatFelelos fel on fact.FeladatFelelos_Id = fel.id
 inner join eAdatpiac.Dim_FeladatStatusz fstat on fact.FeladatStatusz_Id = fstat.id
 inner join eAdatpiac.Dim_LejaratKategoria lekat  on fact.LejaratKategoria_Id = lekat.id
 inner join eAdatpiac.Dim_Prioritas pr on fact.Prioritas_Id = pr.id
where (fel.Felhasznalo_Id = @Felhasznalo or (fel.Felhasznalo_Id = '00000000-0000-0000-0000-000000000000' and ft.SzervezetJel = 1))
      and fel.Szervezet_Id = @Szervezet
      and dat.datum = @datum    
      and 1=case when @PrioritasTol is null and @PrioritasIg is null then 1 
                 when @PrioritasTol is not null and @PrioritasIg is not null 
                      then case when pr.Prioritas between  @PrioritasTol and @PrioritasIg then 1 else 0 end 
                 when @PrioritasTol is not null and @PrioritasIg is null 
                      then case when pr.Prioritas >= @PrioritasTol then 1 else 0 end
                 when @PrioritasTol is null and @PrioritasIg is not null 
                      then case when pr.Prioritas <= @PrioritasIg  then 1 else 0 end
            end        
      and 1=case when @LejaratFordTol is null and @LejaratFordIg is null then 1 
                 when @LejaratFordTol is not null and @LejaratFordIg is not null
                      then case when lekat.Sorrend between @LejaratFordTol and @LejaratFordIg then 1 else 0 end
                 when @LejaratFordTol is not null and @LejaratFordIg is null
                      then case when lekat.Sorrend >= @LejaratFordTol then 1 else 0 end
                 when @LejaratKatTol is null and @LejaratKatIg is not null
                      then case when lekat.Sorrend <= @LejaratFordIg then 1 else 0 end                 
            end

group by   dat.datum, fel.FrissitesIdo, fel.SzervezetNev
, fel.FelhasznaloNev, ft.feladatkod, ft.KategoriaKibontas, ft.Kategoria, ft.URLJel
, ft.Altipus1, ft.Altipus2
, ft.URLGlobDefinicio +
  case when ft.FeladatKod like '12%'
			then '&Date='+convert(varchar(10), @datum, 102)
	   else '' end
, case when ft.FeladatKod not like '1%' then ft.URLTetDefinicio
	   else NULL end
, case when ft.FeladatKod like '1%' then ft.URLTetDefinicio
	   else NULL end
, case when ft.FeladatKod not like '2%' and
            ft.FeladatKod not like '4%' and
            ft.FeladatKod not like '9%'
			then '&Filter=Szervezet'
	   else NULL end
order by ft.feladatkod
end