﻿create procedure [eAdatpiac].[SP_factTorlesByDateUser]
(
    @FrissitesDat datetime,               -- frissites datuma
    @szervezet nvarchar(100)   = '-',     -- szervezet guid
    @felhasznalo nvarchar(100) = '-'      -- felhasznalo guid
)
as
begin

declare @rekordszam int
declare @felh_id as uniqueidentifier, @szerv_id as uniqueidentifier
set @felh_id = case when @felhasznalo ='-' then NULL 
                    else convert(uniqueidentifier, @felhasznalo) end
set @szerv_id = case when @szervezet ='-' then NULL
                    else convert(uniqueidentifier, @szervezet) end

delete eAdatpiac.Fact_Feladatok 
 from eAdatpiac.Fact_Feladatok fact
    inner join eAdatpiac.Dim_FrissitesDatum dat on fact.FrissitesDatum_Id = dat.datum_id
    inner join eAdatpiac.Dim_FeladatFelelos fel on fact.FeladatFelelos_Id = fel.id
 where dat.datum = @FrissitesDat 
       and 1 = case when @felh_id is null then 1 
                    when fel.Felhasznalo_Id = @felh_id then 1 else 0 end
       and 1 = case when @szerv_id is null then 1 
                    when fel.Szervezet_Id = @szerv_id then 1 else 0 end
end