﻿create procedure [eAdatpiac].[sp_Dim_FeladatFelelosGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   Dim_FeladatFelelos.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   Dim_FeladatFelelos.Id,
	   Dim_FeladatFelelos.FelelosTipusKod,
	   Dim_FeladatFelelos.FelelosTipusNev,
	   Dim_FeladatFelelos.Szervezet_Id,
	   Dim_FeladatFelelos.SzervezetNev,
	   Dim_FeladatFelelos.Felhasznalo_Id,
	   Dim_FeladatFelelos.FelhasznaloNev,
	   Dim_FeladatFelelos.FrissitesIdo,
	   Dim_FeladatFelelos.Modositas,
	   Dim_FeladatFelelos.ETL_Load_Id  
   from 
     eAdatpiac.Dim_FeladatFelelos as Dim_FeladatFelelos      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end