﻿create procedure [eAdatpiac].[sp_Fact_FeladatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   Fact_Feladatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   Fact_Feladatok.FeladatFelelos_Id,
	   Fact_Feladatok.FrissitesDatum_Id,
	   Fact_Feladatok.FeladatTipus_Id,
	   Fact_Feladatok.FeladatStatusz_Id,
	   Fact_Feladatok.LejaratKategoria_Id,
	   Fact_Feladatok.Prioritas_Id,
	   Fact_Feladatok.Telelszam,
	   Fact_Feladatok.Obj_Id,
	   Fact_Feladatok.ETL_Load_Id,
	   Fact_Feladatok.LetrehozasIdo  
   from 
     eAdatpiac.Fact_Feladatok as Fact_Feladatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end