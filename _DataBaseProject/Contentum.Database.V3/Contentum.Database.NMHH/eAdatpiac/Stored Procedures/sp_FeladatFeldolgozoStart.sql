﻿create procedure [eAdatpiac].[sp_FeladatFeldolgozoStart] 
  @keres_id int output
as
  begin

    update eAdatpiac.FeladatIgeny  
      set KezdesIdo = getdate(), 
          allapot = case
			when allapot = 0 then 1	-- folyamatban
			else allapot
            end -- case
    where keres_id = @keres_id
  end