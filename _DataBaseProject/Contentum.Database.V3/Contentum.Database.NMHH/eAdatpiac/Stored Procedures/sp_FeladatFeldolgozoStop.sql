﻿create procedure [eAdatpiac].[sp_FeladatFeldolgozoStop]
@keres_id int
as
begin
    update eAdatpiac.FeladatIgeny  
      set VegeIdo = getdate(),
        allapot = case
			when allapot = 1 then 2	-- befejezve
			else allapot
		end --case
    where keres_id = @keres_id
end