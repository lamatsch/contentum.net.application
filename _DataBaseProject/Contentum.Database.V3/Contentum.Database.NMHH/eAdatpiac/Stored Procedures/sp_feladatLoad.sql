﻿create procedure [eAdatpiac].[sp_feladatLoad] (
    @szervezet as varchar(100)= '-',     
    @felhasznalo as varchar(100) = '-'    
)
as
begin

declare @szerv_id  as uniqueidentifier
declare @felh_id as uniqueidentifier

set @szerv_id = case when @szervezet ='-' then NULL 
                     else cast(@szervezet as uniqueidentifier) end

set @felh_id = case when @felhasznalo ='-' then NULL 
                     else cast(@felhasznalo  as uniqueidentifier) end

SELECT [FrissitesIdo]
      ,[FeladatKod]
      ,[FelelosSzervezet_Id]
      ,[FelelosFelhasznalo_Id]
      ,cast([Prioritas] as int) as Prioritas
      ,[LejaratDatum]
      ,[FeladatJel]
      ,[EredetKod]
      ,[KeletkezesKod]
      ,[KezelesSzintKod]
      ,[Funkcio_Id_Inditando]
      ,Obj_Id as guid_obj_id
      ,[Obj_Type]
  FROM [eAdatpiac].[FeladatLoad]
where 1 = case when @szerv_id is null then 1 
               when @szerv_id = [FelelosSzervezet_Id] then 1 else 0 end
      and
      1 = case when @felh_id is null then 1
               when @felh_id = [FelelosFelhasznalo_Id] then 1 else 0 end
end