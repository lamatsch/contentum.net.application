﻿CREATE TABLE [eAdatpiac].[Dim_FeladatFelelos] (
    [Id]              INT              IDENTITY (1, 1) NOT NULL,
    [FelelosTipusKod] CHAR (1)         NULL,
    [FelelosTipusNev] NVARCHAR (10)    NULL,
    [Szervezet_Id]    UNIQUEIDENTIFIER NULL,
    [SzervezetNev]    NVARCHAR (100)   NULL,
    [Felhasznalo_Id]  UNIQUEIDENTIFIER NULL,
    [FelhasznaloNev]  NVARCHAR (100)   NULL,
    [FrissitesIdo]    DATETIME         NULL,
    [Modositas]       DATETIME         NULL,
    [ETL_Load_Id]     INT              NULL,
    CONSTRAINT [PK_DIM_FELADATFELELOS] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_szervezet_szervezet_id]
    ON [eAdatpiac].[Dim_FeladatFelelos]([Szervezet_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_szervezet_nev]
    ON [eAdatpiac].[Dim_FeladatFelelos]([SzervezetNev] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_szervezet_felhnev]
    ON [eAdatpiac].[Dim_FeladatFelelos]([FelhasznaloNev] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_szervezet_felhaszn_id]
    ON [eAdatpiac].[Dim_FeladatFelelos]([Felhasznalo_Id] ASC);

