﻿CREATE TABLE [eAdatpiac].[Dim_FeladatStatusz] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [FeladatJel]      CHAR (1)      NULL,
    [FeladatJelNev]   NVARCHAR (10) NULL,
    [KeletkezesKod]   CHAR (1)      NULL,
    [KeletkezesNev]   NVARCHAR (10) NULL,
    [EredetKod]       CHAR (1)      NULL,
    [EredetNev]       NVARCHAR (20) NULL,
    [KezelesSzintKod] CHAR (1)      NULL,
    [KezelesSzintNev] NVARCHAR (20) NULL,
    CONSTRAINT [PK_DIM_FELADATSTATUSZ] PRIMARY KEY CLUSTERED ([Id] ASC)
);

