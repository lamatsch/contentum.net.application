﻿CREATE TABLE [eAdatpiac].[Dim_FeladatTipus] (
    [Id]                INT              IDENTITY (1, 1) NOT NULL,
    [FeladatKod]        CHAR (4)         NULL,
    [Kategoria]         NVARCHAR (100)   NOT NULL,
    [KategoriaKibontas] INT              NULL,
    [Altipus1]          NVARCHAR (100)   NULL,
    [Altipus1Kibontas]  INT              NULL,
    [Altipus2]          NVARCHAR (100)   NULL,
    [Altipus2Kibontas]  INT              NULL,
    [Altipus3]          NVARCHAR (100)   NULL,
    [Altipus3Kibontas]  INT              NULL,
    [Obj_Type]          NVARCHAR (100)   NULL,
    [AlapPrioritas]     INT              NULL,
    [Funkcio_Id]        UNIQUEIDENTIFIER NULL,
    [SzervezetJel]      INT              NULL,
    [URLJel]            INT              NULL,
    [URLGlobDefinicio]  NVARCHAR (4000)  NULL,
    [URLTetDefinicio]   NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_DIM_FELADATTIPUS] PRIMARY KEY CLUSTERED ([Id] ASC)
);

