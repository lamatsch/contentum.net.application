﻿CREATE TABLE [eAdatpiac].[Dim_FrissitesDatum] (
    [Datum_Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Datum]        DATETIME      NULL,
    [Ev]           INT           NULL,
    [Ho]           INT           NULL,
    [Het]          INT           NULL,
    [Nap]          INT           NULL,
    [Negyedev]     INT           NULL,
    [Felev]        INT           NULL,
    [Evnap]        INT           NULL,
    [Hetnap]       INT           NULL,
    [NegyedRovid]  NVARCHAR (10) NULL,
    [NegyedHosszu] NVARCHAR (10) NULL,
    [HoRovid]      NVARCHAR (10) NULL,
    [HoHosszu]     NVARCHAR (10) NULL,
    [NapHosszu]    NVARCHAR (10) NULL,
    [NapRovid]     NVARCHAR (10) NULL,
    CONSTRAINT [PK_DIM_FRISSITESDATUM] PRIMARY KEY CLUSTERED ([Datum_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_ido_evhonap]
    ON [eAdatpiac].[Dim_FrissitesDatum]([Ev] ASC, [Ho] ASC, [Nap] ASC);

