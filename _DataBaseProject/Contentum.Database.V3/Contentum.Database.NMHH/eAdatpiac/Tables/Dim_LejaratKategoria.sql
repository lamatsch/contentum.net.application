﻿CREATE TABLE [eAdatpiac].[Dim_LejaratKategoria] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Sorrend]      INT            NULL,
    [TetelJel]     INT            NULL,
    [LejaratKod]   CHAR (1)       NULL,
    [LejaratNev]   NVARCHAR (10)  NULL,
    [ElteresErtek] INT            NULL,
    [ElteresNev]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_DIM_LEJARATKATEGORIA] PRIMARY KEY CLUSTERED ([Id] ASC)
);

