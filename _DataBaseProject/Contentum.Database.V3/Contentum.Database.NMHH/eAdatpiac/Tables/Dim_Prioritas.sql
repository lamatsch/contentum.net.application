﻿CREATE TABLE [eAdatpiac].[Dim_Prioritas] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [PrioritasSzint]    INT           NULL,
    [PrioritasSzintNev] NVARCHAR (10) NULL,
    [Prioritas]         INT           NULL,
    [PrioritasNev]      NVARCHAR (10) NULL,
    [ETL_Load_Id]       INT           NULL,
    CONSTRAINT [PK_DIM_PRIORITAS] PRIMARY KEY CLUSTERED ([Id] ASC)
);

