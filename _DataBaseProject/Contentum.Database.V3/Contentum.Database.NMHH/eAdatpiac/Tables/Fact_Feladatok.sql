﻿CREATE TABLE [eAdatpiac].[Fact_Feladatok] (
    [FeladatFelelos_Id]   INT           NULL,
    [FrissitesDatum_Id]   INT           NULL,
    [FeladatTipus_Id]     INT           NULL,
    [FeladatStatusz_Id]   INT           NULL,
    [LejaratKategoria_Id] INT           NULL,
    [Prioritas_Id]        INT           NULL,
    [Telelszam]           INT           NULL,
    [Obj_Id]              NVARCHAR (64) NULL,
    [ETL_Load_Id]         INT           NULL,
    [LetrehozasIdo]       DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_fact_statusz]
    ON [eAdatpiac].[Fact_Feladatok]([FeladatStatusz_Id] ASC, [LejaratKategoria_Id] ASC, [FeladatTipus_Id] ASC, [Prioritas_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_fact_objid]
    ON [eAdatpiac].[Fact_Feladatok]([Obj_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_fact_felelos]
    ON [eAdatpiac].[Fact_Feladatok]([FrissitesDatum_Id] ASC, [FeladatFelelos_Id] ASC);

