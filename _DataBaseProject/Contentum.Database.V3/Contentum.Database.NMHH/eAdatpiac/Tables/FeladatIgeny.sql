﻿CREATE TABLE [eAdatpiac].[FeladatIgeny] (
    [Keres_id]       INT           IDENTITY (1, 1) NOT NULL,
    [Indito_Id]      NVARCHAR (64) NULL,
    [Szervezet_id]   NVARCHAR (64) NULL,
    [Felhasznalo_id] NVARCHAR (64) NULL,
    [KeresDatuma]    DATETIME      NULL,
    [KeresEvHoNap]   NVARCHAR (64) NULL,
    [KeresIndito]    NVARCHAR (64) NULL,
    [Allapot]        INT           NULL,
    [KezdesIdo]      DATETIME      NULL,
    [VegeIdo]        DATETIME      NULL,
    [Osszesre]       BIT           NULL,
    CONSTRAINT [PK_FELADATIGENY] PRIMARY KEY CLUSTERED ([Keres_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_OsszesitesIgeny]
    ON [eAdatpiac].[FeladatIgeny]([Allapot] ASC, [Szervezet_id] ASC, [Felhasznalo_id] ASC);

