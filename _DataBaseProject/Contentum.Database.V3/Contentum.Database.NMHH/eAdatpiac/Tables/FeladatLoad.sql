﻿CREATE TABLE [eAdatpiac].[FeladatLoad] (
    [FrissitesIdo]          DATETIME         NULL,
    [FeladatKod]            VARCHAR (4)      NULL,
    [FelelosSzervezet_Id]   UNIQUEIDENTIFIER NULL,
    [FelelosFelhasznalo_Id] UNIQUEIDENTIFIER NULL,
    [Prioritas]             SMALLINT         NULL,
    [LejaratDatum]          DATETIME         NULL,
    [FeladatJel]            CHAR (1)         NULL,
    [EredetKod]             CHAR (1)         NULL,
    [KeletkezesKod]         CHAR (1)         NULL,
    [KezelesSzintKod]       CHAR (1)         NULL,
    [Funkcio_Id_Inditando]  UNIQUEIDENTIFIER NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NULL,
    [Obj_Type]              VARCHAR (100)    NULL,
    [Kiado_Id]              UNIQUEIDENTIFIER NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_SzervezetFelhasznalo]
    ON [eAdatpiac].[FeladatLoad]([FelelosSzervezet_Id] ASC, [FelelosFelhasznalo_Id] ASC);

