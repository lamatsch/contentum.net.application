 ---BLG_8087--------------------
USE $(DataBaseName)
SET QUOTED_IDENTIFIER ON
SET NOCOUNT ON


UPDATE MIG_FOSZAM
SET Ugyirat_tipus = 'papír alapú'
WHERE Ugyirat_tipus is NULL OR 
  Ugyirat_tipus != 'elektronikus'
  AND Ugyirat_tipus != 'papír alapú'
  AND Ugyirat_tipus != 'vegyes' 

GO
---END-BLG_8087----------------