﻿------BUG 10549 EMigration irattárba küldés frissítést követő hibás adattartalom. -------
--Kérem megvizsgálni (Emigration), hogy DB-ből kiszedhető-e az információ, 
--hogy mely tételeknél került átírásra Irattárba adás dátuma  (iktatási év.12.31-re ) úgy, 
--hogy volt benne már dátum érték. 
--Amennyiben igen, akkor azon tételeknél, ahol már volt benne érték írjuk vissza az eredeti dátumot. 
--Csak azon tételeknél végrehajtandó, ahol az Irat helye Irattárban.

--Az év beállítása, hogy szakaszosan is futtatható legyen.
--Ha egyben akarjuk az egész db-re futtani, akkor null-ra kell állítani.
DECLARE @Year INT = null

--Ha már létezik a #Temp tábla akkor azt droppoljuk:
IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#Temp%') BEGIN
   DROP TABLE #Temp;
END;

--Ha már létezik a #Write tábla akkor azt droppoljuk:
IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#Write%') BEGIN
   DROP TABLE #Write;
END;

--Létrehozzuk a #Write táblát, amelybe be fogjuk szúrni a rekodokat.
CREATE TABLE #Write(
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[UI_SAV] [int] NULL,
	[UI_YEAR] [int] NULL,
	[UI_NUM] [int] NULL,
	[UI_NAME] [nvarchar](1000) NULL,
	[UI_IRSZ] [nvarchar](10) NULL,
	[UI_UTCA] [nvarchar](25) NULL,
	[UI_HSZ] [nvarchar](12) NULL,
	[UI_HRSZ] [nvarchar](20) NULL,
	[UI_TYPE] [nvarchar](4) NULL,
	[UI_IRJ] [nvarchar](20) NULL,
	[UI_PERS] [nvarchar](6) NULL,
	[UI_OT_ID] [nvarchar](64) NULL,
	[MEMO] [nvarchar](1000) NULL,
	[IRSZ_PLUSS] [int] NULL,
	[IRJ2000] [nvarchar](20) NULL,
	[Conc] [nvarchar](255) NULL,
	[Selejtezve] [int] NULL,
	[Selejtezes_Datuma] [datetime] NULL,
	[Csatolva_Rendszer] [int] NULL,
	[Csatolva_Id] [uniqueidentifier] NULL,
	[MIG_Sav_Id] [uniqueidentifier] NULL,
	[MIG_Varos_Id] [uniqueidentifier] NULL,
	[MIG_IktatasTipus_Id] [uniqueidentifier] NULL,
	[MIG_Eloado_Id] [uniqueidentifier] NULL,
	[Edok_Utoirat_Id] [uniqueidentifier] NULL,
	[Edok_Utoirat_Azon] [nvarchar](100) NULL,
	[UGYHOL] [nvarchar](1) NULL,
	[IRATTARBA] [datetime] NULL,
	[SCONTRO] [datetime] NULL,
	[Edok_Ugyintezo_Csoport_Id] [uniqueidentifier] NULL,
	[Edok_Ugyintezo_Nev] [nvarchar](100) NULL,
	[Ver] [int] NULL,
	[EdokSav] [nvarchar](20) NULL,
	[MIG_Forras_id] [int] NULL,
	[Ugyirat_tipus] [nvarchar](20) NULL,
	[Eltelt_napok] [int] NULL,
	[Ugy_kezdete] [datetime] NULL,
	[Ugykezeles_modja] [nvarchar](64) NULL,
	[Egyeb_adat2] [nvarchar](100) NULL,
	[Munkanapos] [nvarchar](1) NULL,
	[Hatarido] [datetime] NULL,
	[Feladat] [nvarchar](64) NULL,
	[Targyszavak] [nvarchar](1000) NULL,
	[Kulso_eloirat] [nvarchar](125) NULL,
	[Felfuggesztve] [nvarchar](1) NULL,
	[Szervezet] [nvarchar](100) NULL,
	[Sztorno_datum] [datetime] NULL,
	[IrattarbolKikeroNev] [nvarchar](100) NULL,
	[IrattarbolKikeres_Datuma] [datetime] NULL,
	[MegorzesiIdo] [datetime] NULL,
	[IrattarId] [uniqueidentifier] NULL,
	[IrattariHely] [nvarchar](100) NULL
	)

--Iterációhoz
SET ROWCOUNT 0

--Lekérdezzük azokat az ügyiratokhoz tartozó history rekodokat, ahol az IRATTARBA dátum az adott év 12.31.- re van állítva, és 2-nél több rekord van a historyban. (Ver > 2)
--Ezeket a rekordokat a #Temp táblába másoljuk. Ezen a #Temp táblán fogunk végigmenni egy ciklussal.
SELECT * INTO #Temp
FROM MIG_FoszamHistory
WHERE Id IN ( SELECT DISTINCT Id  
              FROM MIG_FoszamHistory  WHERE 
			  (
				@Year is not null 
				AND (SELECT Month(MIG_FoszamHistory.IRATTARBA)) = '12' 
				AND (SELECT Day(MIG_FoszamHistory.IRATTARBA)) = '31'
				AND (SELECT Year(MIG_FoszamHistory.IRATTARBA)) = @Year
				AND  MIG_FoszamHistory.Ver > 2
			  )
			  OR
			  (
				@Year is null 
				AND (SELECT Month(MIG_FoszamHistory.IRATTARBA)) = '12' 
				AND (SELECT Day(MIG_FoszamHistory.IRATTARBA)) = '31'
				AND  MIG_FoszamHistory.Ver > 2
			  )
			)
ORDER BY Id 																									  

--Iterációhoz @id változó, az aktuális iterációban az iratId-t tárolja.
DECLARE @id UNIQUEIDENTIFIER
--Iterációhoz @lastVersion változó, amely az iterációban az aktuális iratId-hoz tartozó history rekordok közül a legutolsót tárolja.
DECLARE @lastVersion INT
--Iterációhoz @lastVersionIRATTARBA változó, amely az iterációban az aktuális iratId-hoz tartozó history rekordok közül a legutolsónak az IRATTARBA értékét tárolja majd.
DECLARE @lastVersionIRATTARBA DATETIME
--Iterációban a ciklusok számát tárolja.
DECLARE @counter INT = 0

--Iterációhoz
SET ROWCOUNT 1
--Aktuális @id (iratId) kijelölése a ciklus elején
SELECT @id = id FROM #Temp

--Ciklus
WHILE @@rowcount <> 0
BEGIN
    SET ROWCOUNT 0
	
	--Ciklus számláló növelése:
	SET @counter = @counter + 1;

	--Minden 10.-et iratunk ki a konzolra, hogy lássuk hol járunk:
	IF (@counter % 10) = 0
	BEGIN
		PRINT 'Folyamat: ' + CAST(@counter as varchar(255)) + '...';
	END

	--Iterációhoz @lastVersion változó, amely az iterációban az aktuális iratId-hoz tartozó history rekordok közül a legutolsót tárolja.
	SET @lastVersion = (SELECT MAX(VER) FROM #Temp WHERE id = @id)
	--Leszűrjük az adott id-hoz azokat a history rekordokat, ahol az IRATTARBA mezoérték nem egyezik meg a legkésobbi verziószámú history rekord IRATTARBA mezojének értékével.
	SET @lastVersionIRATTARBA =
					   (SELECT IRATTARBA FROM #Temp WHERE id = @id AND Ver = @lastVersion)

		
	--Ugyanabbol a set-bol kell kiválasztani a legkésobbi verziót, ezt tároljuk el, ez lesz az az IRATTARBA érték, amit majd vissza kell írjunk.
	--Ehhez le kell tárolni az egész kiválasztott rekordot. A #Write táblába.
	INSERT INTO #Write SELECT * FROM #Temp 
	WHERE id = @id 
	AND IRATTARBA <> @lastVersionIRATTARBA
	AND Ver = (SELECT MAX(Ver) FROM #Temp  
			   WHERE id = @id 
			   AND IRATTARBA <> @lastVersionIRATTARBA )
	
	--Törlöm a #Temp táblából az iratId-t (@id) amelyiken már végigmentünk.
    DELETE #Temp WHERE id = @id

    SET ROWCOUNT 1

	--Kijelölöm a következő @id-t. (iratId)
    SELECT @id = id FROM #Temp
	
END
SET ROWCOUNT 0

--A #Write tábla kiiratása 
SELECT * FROM #Write

SET ROWCOUNT 0
SELECT @id = id FROM #Write

WHILE @@ROWCOUNT <> 0
BEGIN
    SET ROWCOUNT 0
   
   --NewVer a legutóbbi + 1
	DECLARE @NewVer INT = 0
	SELECT @NewVer = MAX(Ver) FROM MIG_FoszamHistory WHERE MIG_FoszamHistory.Id = @id
	SET @NewVer = @NewVer + 1

	--Beszúrjuk a #Write táblában található history rekordok alapján az új módosított history rekordokat. (Minden ügyirathoz létrehozunk egy új bejegyzést, amiben visszaírjuk a korábbi értéket.)
	INSERT INTO MIG_FoszamHistory 
	(
		HistoryId, HistoryMuvelet_Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo, Id, UI_SAV, UI_YEAR, UI_NUM, UI_NAME, UI_IRSZ, UI_UTCA,
		UI_HSZ,	UI_HRSZ, UI_TYPE, UI_IRJ, UI_PERS, UI_OT_ID, MEMO, IRSZ_PLUSS, IRJ2000, Conc, Selejtezve, Selejtezes_Datuma, Csatolva_Rendszer,	Csatolva_Id, MIG_Sav_Id,
		MIG_Varos_Id, MIG_IktatasTipus_Id, MIG_Eloado_Id, Edok_Utoirat_Id, Edok_Utoirat_Azon, UGYHOL, IRATTARBA, SCONTRO, Edok_Ugyintezo_Csoport_Id, Edok_Ugyintezo_Nev,
		Ver, EdokSav, MIG_Forras_id, Ugyirat_tipus, Eltelt_napok, Ugy_kezdete, Ugykezeles_modja, Egyeb_adat2, Munkanapos, Hatarido,	Feladat, Targyszavak, Kulso_eloirat,
		Felfuggesztve, Szervezet, Sztorno_datum, IrattarbolKikeroNev, IrattarbolKikeres_Datuma,	MegorzesiIdo, IrattarId, IrattariHely
	)
	VALUES 
	(
		NEWID(),
		1,
		'54E861A5-36ED-44CA-BAA7-C287D125B309',
		GETDATE(),
		@id,
		(SELECT UI_SAV FROM #Write WHERE Id = @id),
		(SELECT UI_YEAR FROM #Write WHERE Id = @id),
		(SELECT UI_NUM FROM #Write WHERE Id = @id),
		(SELECT UI_NAME FROM #Write WHERE Id = @id),
		(SELECT UI_IRSZ FROM #Write WHERE Id = @id), 
		(SELECT UI_UTCA FROM #Write WHERE Id = @id), 
		(SELECT UI_HSZ FROM #Write WHERE Id = @id),
		(SELECT UI_HRSZ FROM #Write WHERE Id = @id), 
		(SELECT UI_TYPE FROM #Write WHERE Id = @id), 
		(SELECT UI_IRJ FROM #Write WHERE Id = @id),
		(SELECT UI_PERS FROM #Write WHERE Id = @id),
		(SELECT UI_OT_ID FROM #Write WHERE Id = @id),
		(SELECT MEMO FROM #Write WHERE Id = @id),
		(SELECT IRSZ_PLUSS FROM #Write WHERE Id = @id),
		(SELECT IRJ2000 FROM #Write WHERE Id = @id),
		(SELECT Conc FROM #Write WHERE Id = @id),
		(SELECT Selejtezve FROM #Write WHERE Id = @id),
		(SELECT Selejtezes_Datuma FROM #Write WHERE Id = @id),
		(SELECT Csatolva_Rendszer FROM #Write WHERE Id = @id),
		(SELECT Csatolva_Id FROM #Write WHERE Id = @id),
		(SELECT MIG_Sav_Id FROM #Write WHERE Id = @id),
		(SELECT MIG_Varos_Id FROM #Write WHERE Id = @id),
		(SELECT MIG_IktatasTipus_Id FROM #Write WHERE Id = @id),
		(SELECT MIG_Eloado_Id FROM #Write WHERE Id = @id),
		(SELECT Edok_Utoirat_Id FROM #Write WHERE Id = @id),
		(SELECT Edok_Utoirat_Azon FROM #Write WHERE Id = @id),
		(SELECT UGYHOL FROM #Write WHERE Id = @id),
		(SELECT IRATTARBA FROM #Write WHERE Id = @id),
		(SELECT SCONTRO FROM #Write WHERE Id = @id),
		(SELECT Edok_Ugyintezo_Csoport_Id FROM #Write WHERE Id = @id),
		(SELECT Edok_Ugyintezo_Nev FROM #Write WHERE Id = @id),
		@NewVer,
		(SELECT EdokSav FROM #Write WHERE Id = @id),
		(SELECT MIG_Forras_id FROM #Write WHERE Id = @id),
		(SELECT Ugyirat_tipus FROM #Write WHERE Id = @id),
		(SELECT Eltelt_napok FROM #Write WHERE Id = @id),
		(SELECT Ugy_kezdete FROM #Write WHERE Id = @id),
		(SELECT Ugykezeles_modja FROM #Write WHERE Id = @id),
		(SELECT Egyeb_adat2 FROM #Write WHERE Id = @id),
		(SELECT Munkanapos FROM #Write WHERE Id = @id),
		(SELECT Hatarido FROM #Write WHERE Id = @id),
		(SELECT Feladat FROM #Write WHERE Id = @id),
		(SELECT Targyszavak FROM #Write WHERE Id = @id),
		(SELECT Kulso_eloirat FROM #Write WHERE Id = @id),
		(SELECT Felfuggesztve FROM #Write WHERE Id = @id),
		(SELECT Szervezet FROM #Write WHERE Id = @id),
		(SELECT Sztorno_datum FROM #Write WHERE Id = @id),
		(SELECT IrattarbolKikeroNev FROM #Write WHERE Id = @id),
		(SELECT IrattarbolKikeres_Datuma FROM #Write WHERE Id = @id),
		(SELECT MegorzesiIdo FROM #Write WHERE Id = @id),
		(SELECT IrattarId FROM #Write WHERE Id = @id),
		(SELECT IrattariHely FROM #Write WHERE Id = @id)
	) 

	--UPDATE-eljük az Foszam táblában az ügyirato IRATTARBA értékét is, mert eddig csak a history rekordokat szúrtuk be.
	UPDATE MIG_Foszam
	SET IRATTARBA = (SELECT IRATTARBA FROM #Write WHERE Id = @id),
		Ver = @NewVer
	WHERE Id = @id

    DELETE #Write WHERE id = @id

    SET ROWCOUNT 1
    SELECT @id = id FROM #Write
END
SET ROWCOUNT 0
