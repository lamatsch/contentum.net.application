﻿SET QUOTED_IDENTIFIER ON

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 14840 - adóirodai egyenlegközlő ügyiratok sztornózása_2020.09.10.
-- BOPMH éles környezetben kell elvégezni a megadott tartományban található ügyiratok sztornózását, iktatókönyv: XVIII
-- 42170 - 42184
-- 41912 - 41956
-- 41901,41903,41905,41908,41910,42186,42188,42190,42192,42194,42196,42198,42200,42202,42204,42206,42208,42210,42212,42252,42250
-- BOPMH-ban

declare @IraIktatoKonyv_Id uniqueidentifier;
declare @Modosito_id uniqueidentifier;

set @Modosito_id = (select Id from KRT_Felhasznalok where UserNev = 'Admin')

IF(@ORG_KOD ='BOPMH')
BEGIN 
	PRINT 'Executing: BUG_14840_adoirodai_egyenlegkozlo_ugyiratok_sztornozasa_20200910'

	-- Iktatókönyv keresés
	select @IraIktatoKonyv_Id = Id from [EREC_IraIktatoKonyvek] where 
		Org = @ORG_ID 
		and Ev = '2020' 
		and Iktatohely='XVIII' 
		and getdate() between ErvKezd and ErvVege
		
	IF @IraIktatoKonyv_Id is not null
	BEGIN
	
		-- Iratok sztornózása
		update [EREC_IraIratok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_IraIratok]
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 42170 and 42184
			or
			[EREC_UgyUgyiratok].Foszam between 41912 and 41956
			or
			[EREC_UgyUgyiratok].Foszam in (41901,41903,41905,41908,41910,42186,42188,42190,42192,42194,42196,42198,42200,42202,42204,42206,42208,42210,42212,42252,42250)
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_IraIratok].Allapot<>'90'
		

		-- Iratpéldányok sztornózása
		update [EREC_PldIratPeldanyok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_PldIratPeldanyok]
		inner join [EREC_IraIratok] on [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 42170 and 42184
			or
			[EREC_UgyUgyiratok].Foszam between 41912 and 41956
			or
			[EREC_UgyUgyiratok].Foszam in (41901,41903,41905,41908,41910,42186,42188,42190,42192,42194,42196,42198,42200,42202,42204,42206,42208,42210,42212,42252,42250)
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_PldIratPeldanyok].Allapot<>'90'
		

		-- Küldemények sztornózása
		update [EREC_KuldKuldemenyek] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_KuldKuldemenyek]
		inner join [EREC_IraIratok] on [EREC_KuldKuldemenyek].IraIratok_Id = [EREC_IraIratok].Id
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 42170 and 42184
			or
			[EREC_UgyUgyiratok].Foszam between 41912 and 41956
			or
			[EREC_UgyUgyiratok].Foszam in (41901,41903,41905,41908,41910,42186,42188,42190,42192,42194,42196,42198,42200,42202,42204,42206,42208,42210,42212,42252,42250)
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_KuldKuldemenyek].Allapot<>'90'
		
		-- Ügyiratok sztornózása
		update [EREC_UgyUgyiratok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_UgyUgyiratok] where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 42170 and 42184
			or
			[EREC_UgyUgyiratok].Foszam between 41912 and 41956
			or
			[EREC_UgyUgyiratok].Foszam in (41901,41903,41905,41908,41910,42186,42188,42190,42192,42194,42196,42198,42200,42202,42204,42206,42208,42210,42212,42252,42250)
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'

	END

	PRINT 'End: BUG_14840_adoirodai_egyenlegkozlo_ugyiratok_sztornozasa_20200910'

END
GO