DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG_15257 XVIII/000096/2017_EREC_UgyUgyiratok_UPDATE
--
-- Barcode: XVIII/000096/2017
-- Id: 'E6E463B8-89D2-E611-90F7-0050569A6FBA'
-- Allapot: 50
-- TovabbitasAlattAllapot: 56
--
-- Kérném Éles rendszeren az XVIII/96/2017 ügyiraton módosítani a következőket:
--   Kezelő: Központi irattár
--   Állapot: Irattárba küldött
-- BOPMH-ban

declare @IraIktatoKonyv_Id uniqueidentifier;
declare @CSOPORT_ID_FELELOS uniqueidentifier;

IF(@ORG_KOD ='BOPMH')
BEGIN 
	PRINT 'Executing: BUG_15257 XVIII/000096/2017_EREC_UgyUgyiratok_UPDATE'

	-- Iktatókönyv keresés
	select @IraIktatoKonyv_Id = Id 
	from [EREC_IraIktatoKonyvek] 
	where 
		Org = @ORG_ID 
		and Ev = '2017' 
		and Iktatohely = 'XVIII' 
		and getdate() between ErvKezd and ErvVege
		
	-- Kezelő
	SELECT @CSOPORT_ID_FELELOS = Id 
	FROM KRT_Csoportok 
	WHERE Nev like 'Központi%'
		
	IF (@IraIktatoKonyv_Id is not null) AND (@CSOPORT_ID_FELELOS is not null)
	BEGIN		
		-- Ügyirat visszaállítása Irattárba küldött-re
		-- update [EREC_UgyUgyiratok] set
		UPDATE [dbo].[EREC_UgyUgyiratok]
			SET [Csoport_Id_Felelos] = @CSOPORT_ID_FELELOS
				,[Allapot] = 11
				,[TovabbitasAlattAllapot] = ''
			WHERE 
				[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id AND
				[EREC_UgyUgyiratok].Foszam = 000096 AND
				[EREC_UgyUgyiratok].Allapot= '50' AND
				[EREC_UgyUgyiratok].TovabbitasAlattAllapot = 56
			
	END

	PRINT 'End: BUG_15257 XVIII/000096/2017_EREC_UgyUgyiratok_UPDATE'

END
GO