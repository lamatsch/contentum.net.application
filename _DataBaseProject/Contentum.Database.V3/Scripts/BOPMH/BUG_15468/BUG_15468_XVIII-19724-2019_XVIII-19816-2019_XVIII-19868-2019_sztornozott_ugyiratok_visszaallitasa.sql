DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 15468 - XVIII-19724-2019_XVIII-19816-2019_XVIII-19868-2019 sztornozott ügyiratok visszaállítása
-- XVIII/19724/2019
-- XVIII/19816/2019
-- XVIII/19868/2019 ügyiratai sztornozott állapotúak, kérjük visszaállítani ügyintézésre
-- BOPMH-ban

declare @IraIktatoKonyv_Id uniqueidentifier;
declare @Modosito_id uniqueidentifier;

set @Modosito_id = (select Id from KRT_Felhasznalok where UserNev = 'Admin')

IF(@ORG_KOD ='BOPMH')
BEGIN 
	PRINT 'Executing: BUG_15468_XVIII-19724-2019_XVIII-19816-2019_XVIII-19868-2019_sztornozott_ugyiratok_visszaallitasa'

	-- Iktatókönyv keresés
	select @IraIktatoKonyv_Id = Id from [EREC_IraIktatoKonyvek] where 
		Org = @ORG_ID 
		and Ev = '2019' 
		and Iktatohely='XVIII' 
		and getdate() between ErvKezd and ErvVege
		
	IF @IraIktatoKonyv_Id is not null
	BEGIN

		-- Ügyiratdarabok visszaállítása ügyintézettre
		update [EREC_UgyUgyiratdarabok] set
			Allapot='06',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_UgyUgyiratdarabok] where 
			[EREC_UgyUgyiratdarabok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and [EREC_UgyUgyiratdarabok].Foszam in (19724, 19816, 19868)
			and [EREC_UgyUgyiratdarabok].Allapot='09'
		
		-- Ügyiratok visszaállítása ügyintézettre
		update [EREC_UgyUgyiratok] set
			SztornirozasDat = NULL,
			Allapot='06',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_UgyUgyiratok] where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and [EREC_UgyUgyiratok].Foszam in (19724, 19816, 19868)
			and [EREC_UgyUgyiratok].Allapot='90'

			
	END

	PRINT 'End: BUG_15468_XVIII-19724-2019_XVIII-19816-2019_XVIII-19868-2019_sztornozott_ugyiratok_visszaallitasa'

END
GO