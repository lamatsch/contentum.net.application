DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 15671 - Sztornozott ügyiratok átállítsa elintézettre 
-- XVIII/16002/2019
-- XVIII/16012/2019
-- XVIII/14839/2019 ügyiratai sztornozott állapotúak, kérjük átállítani elintézettre
-- BOPMH-ban

declare @Modosito_id uniqueidentifier;

set @Modosito_id = (select Id from KRT_Felhasznalok where UserNev = 'Admin')

IF(@ORG_KOD ='BOPMH')
BEGIN 
	PRINT 'Executing: BUG_15671_16002_2019_16012_2019_14839_2019_ugyiratok_atallitasa_elintezettre'

	-- Ügyiratok átállítsa elintézettre
	update [EREC_UgyUgyiratok] set
		SztornirozasDat = NULL,
		Allapot='99',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	where [EREC_UgyUgyiratok].Id in ('E9319F4C-3E80-E911-B966-0050569A6FBA', '416DBC8D-2982-E911-B966-0050569A6FBA', '3F6B129C-2F82-E911-B966-0050569A6FBA') 
		  and [EREC_UgyUgyiratok].Allapot='90'

    -- Iratok átállítása elintézettre
		update [EREC_IraIratok] set
		Allapot='30',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	where [EREC_IraIratok].Ugyirat_Id in ('E9319F4C-3E80-E911-B966-0050569A6FBA', '416DBC8D-2982-E911-B966-0050569A6FBA', '3F6B129C-2F82-E911-B966-0050569A6FBA')
		  and [EREC_IraIratok].Allapot = '90'
			
	PRINT 'End: BUG_15671_16002_2019_16012_2019_14839_2019_ugyiratok_atallitasa_elintezettre'

END
GO