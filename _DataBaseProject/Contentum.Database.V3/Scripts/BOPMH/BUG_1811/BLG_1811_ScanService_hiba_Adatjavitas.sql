-- BUG_1811 Csatolmányok átiktatott iratoknál
-- Javító script
update ck set ck.IraIrat_Id = ir.id, ck.note = isnull(ck.note,'') + 'BLG_1811 ScanService hiba miatti adatjavitas'
--select ck.Id csat_id, isnull(ck.Note,'') + 'BLG_1811 ScanService hiba miatti adatjavts', ir.Id ir_id, ir.Allapot 
from EREC_Csatolmanyok ck 
inner join EREC_IraIratok ir on ck.KuldKuldemeny_Id = ir.KuldKuldemenyek_Id
where ir.Allapot not in ('90','06') 
and ck.ErvVege > GETDATE()
and ck.KuldKuldemeny_Id in 
(
select i.KuldKuldemenyek_Id
from EREC_IraIratok i
where i.Allapot not in ('90','06')
and i.id in 
(
select id from EREC_IraIratok 
except
select distinct IraIrat_Id from EREC_Csatolmanyok
)
)
and ck.IraIrat_Id is null
