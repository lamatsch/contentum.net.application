SET NOCOUNT ON

declare @url nvarchar(max)
	SET @url =
		CASE DB_NAME()
		WHEN 'CONTENTUM_BOPMH_R2T' THEN 'http://ax-vfphtst03/bopmh_v3_r2t/eRecordWebService/EREC_IratAlairokService.asmx'
		WHEN 'CONTENTUM_BOPMH_v3' THEN 'https://ax-vcontapp01.axis.hu/bopmh/eRecordWebService/EREC_IratAlairokService.asmx'
		WHEN 'CONTENTUM_TEST' THEN 'https://contentumtest/eRecordWebService/EREC_IratAlairokService.asmx'
		WHEN 'CONTENTUM_PROD' THEN 'https://contentum/eRecordWebService/EREC_IratAlairokService.asmx'
		ELSE NULL
	END
						
IF (@url IS NOT NULL) OR (LEN(@url) > 0)
	BEGIN
		
	Declare @xmlOut varchar(8000)
	Declare @xml xml
	Declare @RequestText as nvarchar(max)

	DECLARE @iktatoszam nvarchar(1000)
	DECLARE @alairoNev nvarchar(1000)
	DECLARE @alairo_id nvarchar(1000)

	DECLARE @alairok CURSOR;


		
			SET @alairok = CURSOR FOR
				select i.Azonosito, c.Nev, ia.id from EREC_IraIratok i inner join EREC_IratAlairok ia on ia.Obj_Id = i.Id
				inner join KRT_Csoportok c on ia.FelhasznaloCsoport_Id_Alairo = c.Id
				where i.PostazasIranya = 1
				and ia.Allapot = 1
				and ia.ErvVege > GETDATE()
				and ia.AlairasMod != 'M_UTO'
				order by c.Nev, i.Azonosito


			OPEN @alairok
				FETCH NEXT FROM @alairok INTO @iktatoszam, @alairoNev, @alairo_id
			WHILE @@FETCH_STATUS = 0
				BEGIN							
					set @RequestText=
						'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
							<soap:Body>
								<MultiInvalidate xmlns="Contentum.eRecord.WebService">
									<ExecParams>
										<ExecParam>
											<Typed>
												<Alkalmazas_Id>272277bb-27b7-4689-be45-db5994a0fba9</Alkalmazas_Id>
												<Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
												<LoginUser_Id>54e861a5-36ed-44ca-baa7-c287d125b309</LoginUser_Id>
												<Helyettesites_Id xsi:nil="true" />
												<FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
												<CsoportTag_Id>35ea1e60-74c3-e611-90f7-0050569a6fba</CsoportTag_Id>
												<Record_Id>' + @alairo_id + '</Record_Id>
												<UIAccessLog_Id xsi:nil="true" />
												<Page_Id>e3d8c0dc-9251-4bf5-a81e-34aafa7e8e66</Page_Id>
												<CallingChain>
													<ArrayOfString xmlns="" />
												</CallingChain>
												<Paging>
													<PageNumber>-1</PageNumber>
													<PageSize>-1</PageSize>
													<SelectedRowId />
												</Paging>
												<Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
											</Typed>
											<Fake>false</Fake>
											<Alkalmazas_Id>272277bb-27b7-4689-be45-db5994a0fba9</Alkalmazas_Id>
											<Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
											<LoginUser_Id>54e861a5-36ed-44ca-baa7-c287d125b309</LoginUser_Id>
											<Helyettesites_Id />
											<FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
											<CsoportTag_Id>35ea1e60-74c3-e611-90f7-0050569a6fba</CsoportTag_Id>
											<UserHostAddress>mpmob3.axis.hu</UserHostAddress>
											<Record_Id>' + @alairo_id + '</Record_Id>
											<UIAccessLog_Id />
											<Page_Id>e3d8c0dc-9251-4bf5-a81e-34aafa7e8e66</Page_Id>
											<CallingChain>
												<ArrayOfString xmlns="" />
											</CallingChain>
											<Paging>
												<PageNumber>-1</PageNumber>
												<PageSize>-1</PageSize>
												<SelectedRowId />
											</Paging>
											<Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
											<FunkcioKod>IraIratModify</FunkcioKod>
										</ExecParam>				
									</ExecParams>
								</MultiInvalidate>
							</soap:Body>
						</soap:Envelope>
						'
				  								
						exec sp_HTTPRequest 
						@url,
						'POST', 
						@RequestText,
						'Contentum.eRecord.WebService/MultiInvalidate',
						'', '', @xmlOut out
					
						declare @eredmeny as nvarchar(max)
						SET @eredmeny = ''
						
						BEGIN TRY
							SET @xml = convert(xml, @xmlOut)
							;with xmlnamespaces('http://schemas.xmlsoap.org/soap/envelope/' as [soap],
											default 'Contentum.eRecord.WebService')
							select @eredmeny = @xml.value('(/soap:Envelope/soap:Body/MultiInvalidateResponse/MultiInvalidateResult/node())[1]', 'nvarchar(max)')
							print 'Al��r�: ' + @alairoNev + ', Iktat�sz�m: ' + @iktatoszam + ', Eredm�ny: ' + CASE WHEN @eredmeny = 0 THEN 'SIKER' ELSE 'HIBA:' + @eredmeny END
						END TRY
						BEGIN CATCH
						END CATCH
								
				FETCH NEXT FROM @alairok INTO @iktatoszam, @alairoNev, @alairo_id
			END										
		CLOSE @alairok
		DEALLOCATE @alairok
	END
SET NOCOUNT OFF
