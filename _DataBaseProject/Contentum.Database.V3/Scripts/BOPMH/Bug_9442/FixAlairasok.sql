declare @eDocumentWebserviceUrl  nvarchar(100) = 'https://contentum/eDocumentWebService/'

declare @webserviceUrl  nvarchar(100)
set @webserviceUrl = @eDocumentWebserviceUrl + 'DokumentumAlairasService.asmx'

Declare @xmlOut       varchar(8000)
Declare @RequestText  varchar(max)

Declare @RequestTextTemplate  varchar(max)
set @RequestTextTemplate = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:con="Contentum.eDocument.WebService" xmlns:xs="http://www.w3.org/2001/XMLSchema">
   <soap:Header/>
   <soap:Body>
      <con:IratCsatolmanyokAlairas>
         <con:execParam>
            <con:Felhasznalo_Id>#felhId#</con:Felhasznalo_Id>
         </con:execParam>
         <con:csatolmanyIdsArray>
            <con:string>#csatId#</con:string>
         </con:csatolmanyIdsArray>
		 <con:megjegyzes></con:megjegyzes>
      </con:IratCsatolmanyokAlairas>
   </soap:Body>
</soap:Envelope>'


declare @felhId uniqueidentifier
declare @csatId uniqueidentifier

Declare folyamat_cursor CURSOR FOR 
select Csatolmany_Id, Letrehozo_id from EREC_Alairas_Folyamat_Tetelek
where AlairasFolyamat_Id = 'DD2D1234-1BA2-E911-8A68-0050569A6FBA'
and AlairasStatus = '02'

OPEN folyamat_cursor 
FETCH NEXT FROM folyamat_cursor into @csatId, @felhId

WHILE @@FETCH_STATUS = 0
BEGIN
	print 'csatolmány: ' + cast(@csatId as nvarchar(36))

	SET @RequestText = replace(@RequestTextTemplate,'#felhId#',cast(@felhId as nvarchar(36)))
	SET @RequestText = replace(@RequestText,'#csatId#',cast(@csatId as nvarchar(36)))

	--print @RequestText

	exec sp_HTTPRequest @webserviceUrl, 'POST', @RequestText, 'Contentum.eDocument.WebService/IratCsatolmanyokAlairas', '', '', @xmlOut out

	select @xmlOut

	FETCH NEXT FROM folyamat_cursor into @csatId, @felhId
END

close folyamat_cursor
deallocate folyamat_cursor