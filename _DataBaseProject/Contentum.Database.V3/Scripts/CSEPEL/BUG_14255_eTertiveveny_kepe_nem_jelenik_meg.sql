SET QUOTED_IDENTIFIER ON

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 14255 - eTértivevény képe nem jelenik meg
-- CSPH-ban

IF(@ORG_KOD ='CSPH')
BEGIN 
	PRINT 'Executing: BUG_14255_eTertiveveny_kepe_nem_jelenik_meg'

	update KRT_Dokumentumok set
		Alkalmazas_Id = '272277BB-27B7-4689-BE45-DB5994A0FBA9'
	where Id in (
		select 
			Id 
		from KRT_Dokumentumok d1 
		Where 
			FajlNev like 'RL%.pdf' and 
			Alkalmazas_Id is null and
			not exists (
				Select 
					d2.Id 
				from 
					KRT_Dokumentumok d2 
				Where 
					d1.Fajlnev = d2.FajlNev and
					d2.Alkalmazas_Id = '272277BB-27B7-4689-BE45-DB5994A0FBA9'
			)
	)			

	PRINT 'End: BUG_14255_eTertiveveny_kepe_nem_jelenik_meg'

END
GO