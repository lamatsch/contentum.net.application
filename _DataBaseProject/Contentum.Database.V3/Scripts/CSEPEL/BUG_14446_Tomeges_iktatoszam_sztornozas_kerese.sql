﻿SET QUOTED_IDENTIFIER ON

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 14446 - Tömeges iktatószám sztornózás kérése
-- Az alábbi tartományokban található irat és ügyiratok sztornózását kérnénk szépen az ADO 2020-as iktatókönyvből:
-- 1879 - 2028
-- 2185 - 2188
-- 2190 - 2733
-- CSPH-ban

declare @IraIktatoKonyv_Id uniqueidentifier;
declare @Modosito_id uniqueidentifier;

set @Modosito_id = (select Id from KRT_Felhasznalok where UserNev = 'Admin')

IF(@ORG_KOD ='CSPH')
BEGIN 
	PRINT 'Executing: BUG_14446_Tomeges_iktatoszam_sztornozas_kerese'

	-- Iktatókönyv keresés
	select @IraIktatoKonyv_Id = Id from [EREC_IraIktatoKonyvek] where 
		Org = @ORG_ID 
		and Ev = '2020' 
		and Iktatohely='ADO' 
		and getdate() between ErvKezd and ErvVege
		
	IF @IraIktatoKonyv_Id is not null
	BEGIN

		-- Iratok sztornózása
		update [EREC_IraIratok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_IraIratok]
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 1879 and 2028
			or
			[EREC_UgyUgyiratok].Foszam between 2185 and 2188
			or
			[EREC_UgyUgyiratok].Foszam between 2190 and 2733
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_IraIratok].Allapot<>'90'
		

		-- Iratpéldányok sztornózása
		update [EREC_PldIratPeldanyok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_PldIratPeldanyok]
		inner join [EREC_IraIratok] on [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 1879 and 2028
			or
			[EREC_UgyUgyiratok].Foszam between 2185 and 2188
			or
			[EREC_UgyUgyiratok].Foszam between 2190 and 2733
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_PldIratPeldanyok].Allapot<>'90'
		

		-- Küldemények sztornózása
		update [EREC_KuldKuldemenyek] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_KuldKuldemenyek]
		inner join [EREC_IraIratok] on [EREC_KuldKuldemenyek].IraIratok_Id = [EREC_IraIratok].Id
		inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
		inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
		where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 1879 and 2028
			or
			[EREC_UgyUgyiratok].Foszam between 2185 and 2188
			or
			[EREC_UgyUgyiratok].Foszam between 2190 and 2733
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'
			and [EREC_KuldKuldemenyek].Allapot<>'90'
		
		-- Ügyiratok sztornózása
		update [EREC_UgyUgyiratok] set
			SztornirozasDat = getdate(),
			Allapot='90',
			ModositasIdo=getdate(),
			Modosito_id=@Modosito_id

		from [EREC_UgyUgyiratok] where 
			[EREC_UgyUgyiratok].IraIktatoKonyv_Id = @IraIktatoKonyv_Id 
			and 
			(
			[EREC_UgyUgyiratok].Foszam between 1879 and 2028
			or
			[EREC_UgyUgyiratok].Foszam between 2185 and 2188
			or
			[EREC_UgyUgyiratok].Foszam between 2190 and 2733
			)
			and [EREC_UgyUgyiratok].Allapot<>'90'

	END

	PRINT 'End: BUG_14446_Tomeges_iktatoszam_sztornozas_kerese'

END
GO