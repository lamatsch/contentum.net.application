﻿SET QUOTED_IDENTIFIER ON

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BUG 14696 - 2019-es iktatószámok sztornózása
-- CSPH-ban

declare @IraIktatoKonyv_Id uniqueidentifier;
declare @Modosito_id uniqueidentifier;

set @Modosito_id = (select Id from KRT_Felhasznalok where UserNev = 'Admin')

IF(@ORG_KOD ='CSPH')
BEGIN 
	PRINT 'Executing: BUG_14696_2019-es_iktatoszamok_sztornozasa'

	IF OBJECT_ID(N'tempdb..#iktatokonyvek', N'U') IS NOT NULL   
		DROP TABLE #iktatokonyvek;  

	create table #iktatokonyvek (Id uniqueidentifier)

	-- Iktatókönyv keresés	
	insert into #iktatokonyvek select Id from [EREC_IraIktatoKonyvek] where 
		Org = @ORG_ID 
		and Ev in ('2019', '2020')
		and Iktatohely='ADO' 
		and getdate() between ErvKezd and ErvVege
			

	-- Iratpéldányok sztornózása
	update [EREC_PldIratPeldanyok] set
		SztornirozasDat = getdate(),
		Allapot='90',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	from [EREC_PldIratPeldanyok]
	inner join [EREC_IraIratok] on [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
	inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
	inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
	where 
		[EREC_UgyUgyiratok].IraIktatoKonyv_Id in (select Id from #iktatokonyvek)


		and [EREC_UgyUgyiratok].Allapot is not null
		and [EREC_UgyUgyiratok].ErvKezd <= getdate() and [EREC_UgyUgyiratok].ErvVege >= getdate()
		and ([EREC_UgyUgyiratok].Allapot = N'20' or [EREC_UgyUgyiratok].TovabbitasAlattAllapot = N'20')
	

	-- Küldemények sztornózása
	update [EREC_KuldKuldemenyek] set
		SztornirozasDat = getdate(),
		Allapot='90',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	from [EREC_KuldKuldemenyek]
	inner join [EREC_IraIratok] on [EREC_KuldKuldemenyek].IraIratok_Id = [EREC_IraIratok].Id
	inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
	inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
	where 
		[EREC_UgyUgyiratok].IraIktatoKonyv_Id in (select Id from #iktatokonyvek)


		and [EREC_UgyUgyiratok].Allapot is not null
		and [EREC_UgyUgyiratok].ErvKezd <= getdate() and [EREC_UgyUgyiratok].ErvVege >= getdate()
		and ([EREC_UgyUgyiratok].Allapot = N'20' or [EREC_UgyUgyiratok].TovabbitasAlattAllapot = N'20')


	-- Iratok sztornózása
	update [EREC_IraIratok] set
		SztornirozasDat = getdate(),
		Allapot='90',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	from [EREC_IraIratok]
	inner join [EREC_UgyUgyiratdarabok] on [EREC_IraIratok].UgyUgyIratDarab_Id = [EREC_UgyUgyiratdarabok].Id
	inner join [EREC_UgyUgyiratok] on [EREC_UgyUgyiratdarabok].UgyUgyirat_Id = [EREC_UgyUgyiratok].Id
	where 
		[EREC_UgyUgyiratok].IraIktatoKonyv_Id in (select Id from #iktatokonyvek)


		and [EREC_UgyUgyiratok].Allapot is not null
		and [EREC_UgyUgyiratok].ErvKezd <= getdate() and [EREC_UgyUgyiratok].ErvVege >= getdate()
		and ([EREC_UgyUgyiratok].Allapot = N'20' or [EREC_UgyUgyiratok].TovabbitasAlattAllapot = N'20')

	
	-- Ügyiratok sztornózása
	update [EREC_UgyUgyiratok] set
		SztornirozasDat = getdate(),
		Allapot='90',
		ModositasIdo=getdate(),
		Modosito_id=@Modosito_id

	from [EREC_UgyUgyiratok] where 
		[EREC_UgyUgyiratok].IraIktatoKonyv_Id in (select Id from #iktatokonyvek)

		and [EREC_UgyUgyiratok].Allapot is not null
		and [EREC_UgyUgyiratok].ErvKezd <= getdate() and [EREC_UgyUgyiratok].ErvVege >= getdate()
		and ([EREC_UgyUgyiratok].Allapot = N'20' or [EREC_UgyUgyiratok].TovabbitasAlattAllapot = N'20')

	PRINT 'End: BUG_14696_2019-es_iktatoszamok_sztornozasa'

END
GO