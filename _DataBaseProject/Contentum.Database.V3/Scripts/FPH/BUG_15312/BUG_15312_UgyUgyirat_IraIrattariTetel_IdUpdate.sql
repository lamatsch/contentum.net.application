﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
​
-------------------------------------------------------------------------------
-- BUG 15312 - FPH058 /107 /2019 ügyirat IraIrattariTetel_Id kitöltese
-- FPH-ban
​
declare @IraIrattariTetel_Id uniqueidentifier;
declare @UgyUgyirat_Id uniqueidentifier;
​​
IF(@ORG_KOD ='FPH')
BEGIN 
	PRINT 'Executing: BUG_15312_FPH058_107_2019_ügyirat_IraIrattariTetel_Id_kitöltese'
​
	-- Irattári tétel és frissítendű ügyirat keresés
	select @IraIrattariTetel_Id = Id from [EREC_IraIrattariTetelek] where Id = 'C44A32DA-C3BB-E611-9441-00155D1098F2'
	select @UgyUgyirat_Id = Id from [EREC_UgyUgyiratok] where Id = '956F3644-3313-E911-9481-00155DF11484' and IraIrattariTetel_Id is null
		
	IF @IraIrattariTetel_Id is not null and @UgyUgyirat_Id is not null
	BEGIN​
		-- Ha létezik az ügyirat és nincs kitöltve az IraIrattariTetel_Id, valamint létezik az irattári tétel, akkor update
		update [EREC_UgyUgyiratok] set [IraIrattariTetel_Id] = 'C44A32DA-C3BB-E611-9441-00155D1098F2' where Id = '956F3644-3313-E911-9481-00155DF11484'
​
	END
​
	PRINT 'End: BUG_15312_FPH058_107_2019_ügyirat_IraIrattariTetel_Id_kitöltese'
​
END
GO