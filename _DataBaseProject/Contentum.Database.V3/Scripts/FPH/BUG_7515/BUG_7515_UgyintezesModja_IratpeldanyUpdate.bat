@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on
%sqlcmd% -S %1 -v DataBaseName="%2" eRecordWebserviceUrl="%3" -i BUG_7515_UgyintezesModja_IratpeldanyUpdate.sql 

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eRecordWebserviceUrl
echo.
echo. Pelda:
echo. BUG_7515_UgyintezesModja_IratpeldanyUpdate.bat INTCSQL2T EDOK http://edok/eRecordWebService

:EOF