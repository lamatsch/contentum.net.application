
USE $(DataBaseName)
SET QUOTED_IDENTIFIER ON
SET NOCOUNT ON

Declare @xmlOut varchar(8000)
Declare @xml xml 
Declare @RequestText as nvarchar(max)
Declare @ResultText nvarchar(max)

DECLARE @iratpeldanyok CURSOR;
DECLARE @id uniqueidentifier;
DECLARE @ver int;
Declare @ugyintezesmodja nvarchar(100)

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate](
		[IratPld_Id] [uniqueidentifier],
		[VegrehajtasIdeje] [datetime] NULL, 
		[ErrorTxt] [nvarchar](max) NULL
	)
END


		SET @iratpeldanyok = CURSOR FOR
			SELECT pld.id, pld.ver, pld.ugyintezesModja
			  FROM [dbo].[EREC_IraIratok] i
			  inner join [dbo].[EREC_PldIratPeldanyok] as pld on pld.IraIrat_Id = i.Id
--			  inner join KRT_KodCsoportok as kcs on kcs.kod ='KULDEMENY_KULDES_MODJA'
--			  inner join [dbo].[KRT_KodTarak] as kt on kt.KodCsoport_Id= kcs.id and kt.kod=pld.kuldesMod and PostazasDatuma between kt.ErvKezd and kt.ErvVege
			  where i.jelleg = 3 -- word
			  and pld.ugyintezesModja = '1'  -- Elektronikus
			  and i.adathordozotipusa <> '1' --  Elektronikus (irat)
			  and pld.allapot= 45 -- Post�zott
			  and i.postazasIranya<>1 -- nem bej�v�
			  and KuldesMod<>19  -- hivatali kapu 
	  	      and KuldesMod<>11  -- email 
	--	   order by pld.ervkezd 
		OPEN @iratpeldanyok
			FETCH NEXT FROM @iratpeldanyok INTO @id, @ver,@ugyintezesmodja
		WHILE @@FETCH_STATUS = 0
			BEGIN		
						
			--SET @ver = @ver+1							
				set @RequestText=
					'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					<soap:Body>
						<Update xmlns="Contentum.eRecord.WebService">
							<ExecParam>
								<Typed>
									<Alkalmazas_Id>272277bb-27b7-4689-be45-db5994a0fba9</Alkalmazas_Id>
									<Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
									<LoginUser_Id>54e861a5-36ed-44ca-baa7-c287d125b309</LoginUser_Id>
									<FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
									<CsoportTag_Id>96db3159-bdf1-4104-b723-d432453f63e7</CsoportTag_Id>
									<Record_Id>' + CAST(@id AS nvarchar(100)) + '</Record_Id>
									<Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
								</Typed>
								<Fake>false</Fake>
								<Alkalmazas_Id>272277bb-27b7-4689-be45-db5994a0fba9</Alkalmazas_Id>
								<Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
								<LoginUser_Id>54e861a5-36ed-44ca-baa7-c287d125b309</LoginUser_Id>
								<FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
								<CsoportTag_Id>96db3159-bdf1-4104-b723-d432453f63e7</CsoportTag_Id>
								<Record_Id>' + CAST(@id AS nvarchar(100)) + '</Record_Id>
								<Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
							</ExecParam>
							<Record>
								<Typed>
									<UgyintezesModja>0</UgyintezesModja>
								</Typed>
								<Base>
									<Typed>
										<Ver>' + CAST(@ver AS nvarchar(10)) +'</Ver>
									</Typed>
									<Updated>
										<Ver>true</Ver>
										<Letrehozo_id>false</Letrehozo_id>
										<LetrehozasIdo>false</LetrehozasIdo>
									</Updated>
									<Ver>' + CAST(@ver AS nvarchar(10)) + '</Ver>
								</Base>
								<Updated>
									<Id>false</Id>
					<IraIrat_Id>false</IraIrat_Id>
					<UgyUgyirat_Id_Kulso>false</UgyUgyirat_Id_Kulso>
					<Sorszam>false</Sorszam>
					<SztornirozasDat>false</SztornirozasDat>
					<AtvetelDatuma>false</AtvetelDatuma>
					<Eredet>false</Eredet>
					<KuldesMod>false</KuldesMod>
					<Ragszam>false</Ragszam>
					<UgyintezesModja>true</UgyintezesModja>
					<VisszaerkezesiHatarido>false</VisszaerkezesiHatarido>
					<Visszavarolag>false</Visszavarolag>
					<VisszaerkezesDatuma>false</VisszaerkezesDatuma>
					<Cim_id_Cimzett>false</Cim_id_Cimzett>
					<Partner_Id_Cimzett>false</Partner_Id_Cimzett>
					<Csoport_Id_Felelos>false</Csoport_Id_Felelos>
					<FelhasznaloCsoport_Id_Orzo>false</FelhasznaloCsoport_Id_Orzo>
					<Csoport_Id_Felelos_Elozo>false</Csoport_Id_Felelos_Elozo>
					<CimSTR_Cimzett>false</CimSTR_Cimzett>
					<NevSTR_Cimzett>false</NevSTR_Cimzett>
					<Tovabbito>false</Tovabbito>
					<IraIrat_Id_Kapcsolt>false</IraIrat_Id_Kapcsolt>
					<IrattariHely>false</IrattariHely>
					<Gener_Id>false</Gener_Id>
					<PostazasDatuma>false</PostazasDatuma>
					<BarCode>false</BarCode>
					<Allapot>false</Allapot>
					<Azonosito>false</Azonosito>
					<Kovetkezo_Felelos_Id>false</Kovetkezo_Felelos_Id>
					<Elektronikus_Kezbesitesi_Allap>false</Elektronikus_Kezbesitesi_Allap>
					<Kovetkezo_Orzo_Id>false</Kovetkezo_Orzo_Id>
					<Fizikai_Kezbesitesi_Allapot>false</Fizikai_Kezbesitesi_Allapot>
					<TovabbitasAlattAllapot>false</TovabbitasAlattAllapot>
					<PostazasAllapot>false</PostazasAllapot>
					<ValaszElektronikus>false</ValaszElektronikus>
					<SelejtezesDat>false</SelejtezesDat>
					<FelhCsoport_Id_Selejtezo>false</FelhCsoport_Id_Selejtezo>
					<LeveltariAtvevoNeve>false</LeveltariAtvevoNeve>
					<ErvKezd>false</ErvKezd>
					<ErvVege>false</ErvVege>
					<IratPeldanyMegsemmisitesDatuma>false</IratPeldanyMegsemmisitesDatuma>
					<IratPeldanyMegsemmisitve>false</IratPeldanyMegsemmisitve>
								</Updated>
								<UgyintezesModja>0</UgyintezesModja>
							</Record>
						</Update>
					</soap:Body>
				</soap:Envelope>
					'

					exec sp_HTTPRequest 
					--'http://ax-vcontapp01/FPH/eRecordWebService/EREC_PldIratPeldanyokService.asmx',
					'$(eRecordWebserviceUrl)/EREC_PldIratPeldanyokService.asmx',
					'POST', 
					@RequestText,
					'Contentum.eRecord.WebService/Update',
					'', '', @xmlOut out
					
					--BEGIN TRY
					--select 'hah�'
						SET @xml = convert(xml, @xmlOut)
							;with xmlnamespaces('http://schemas.xmlsoap.org/soap/envelope/' as [soap], default 'Contentum.eRecord.WebService')	
							Select @ResultText = @xml.value('(/soap:Envelope/soap:Body/UpdateResponse/UpdateResult/ErrorMessage/node())[1]', 'nvarchar(max)')
						--select @id, @xml.value('(/soap:Envelope/soap:Body/UpdateResponse/UpdateResult/ErrorMessage/node())[1]', 'varchar(8000)')
						select @id
						select @ResultText
						--select 'hh�'
						IF EXISTS(SELECT IratPld_Id from  dbo.TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate where IratPld_Id = @id)
						BEGIN
							--select 'update'
							
							UPDATE dbo.TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate
							SET VegrehajtasIdeje = GETDATE(),
								ErrorTxt = @ResultText
							WHERE IratPld_Id = @id
						END 
						ELSE
						BEGIN
							--select 'insert'
							Insert into dbo.TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate (IratPld_Id, VegrehajtasIdeje, ErrorTxt)
							values(@id, GETDATE(), @ResultText)
						END
						 
					--END TRY
					--BEGIN CATCH
					--END CATCH

				FETCH NEXT FROM @iratpeldanyok INTO @id, @ver,@ugyintezesmodja
			END						
				
		CLOSE @iratpeldanyok
		DEALLOCATE @iratpeldanyok
		SET NOCOUNT OFF

		--SELECT * FROM dbo.TEMP_BUG_7515_UgyintezesModja_IratpeldanyUpdate
		
 
GO
