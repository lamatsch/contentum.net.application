
----Befuttatva
if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_UgyUgyiratok_ErvKezd_ErvVege_Id_IraIktatokonyv_Id' AND object_id = OBJECT_ID('EREC_UgyUgyiratok'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratok_ErvKezd_ErvVege_Id_IraIktatokonyv_Id]
	ON [dbo].[EREC_UgyUgyiratok] ([ErvKezd],[ErvVege])
	INCLUDE ([Id],[IraIktatokonyv_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IraIratok_Alszam_INCLUDE_Ugyirat_Id' AND object_id = OBJECT_ID('[EREC_IraIratok]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_IraIratok_Alszam_INCLUDE_Ugyirat_Id]
	ON [dbo].[EREC_IraIratok] ([Alszam])
	INCLUDE ([Ugyirat_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_PldIratPeldanyok_IraIratId_INCLUDE_FelhasznaloCsoport_Id_Orzo' AND object_id = OBJECT_ID('[EREC_PldIratPeldanyok]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_PldIratPeldanyok_IraIratId_INCLUDE_FelhasznaloCsoport_Id_Orzo] ON [dbo].[EREC_PldIratPeldanyok]
	(
		[IraIrat_Id] ASC
	) INCLUDE (FelhasznaloCsoport_Id_Orzo) 
END

if not exists (select 1 FROM sys.indexes WHERE name='KRT_JOGOSULTAK_CSOP_I_INCLUDE_Id' AND object_id = OBJECT_ID('[KRT_Jogosultak]'))
BEGIN
	CREATE NONCLUSTERED INDEX [KRT_JOGOSULTAK_CSOP_I_INCLUDE_Id] ON [dbo].[KRT_Jogosultak]
	(
		[Csoport_Id_Jogalany] ASC,
		[Obj_Id] ASC
	)
	INCLUDE( Id )
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_IratId_INCLUDE_Id' AND object_id = OBJECT_ID('[EREC_Csatolmanyok]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_Csatolmanyok_IratId_INCLUDE_Id] ON [dbo].[EREC_Csatolmanyok]
	(
		[IraIrat_Id] ASC
	)
	INCLUDE ( Id ) 
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_ErvKezd_ErvKVege_INCLUDE_Id' AND object_id = OBJECT_ID('[EREC_Csatolmanyok]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_Csatolmanyok_ErvKezd_ErvKVege_INCLUDE_Id] ON [dbo].[EREC_Csatolmanyok]
	(
		ErvKezd ASC, ErvVege ASC
	)
	INCLUDE ( Id ) 
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_IraIrat_Id_Dokumentum_Id' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	CREATE INDEX [IX_EREC_Csatolmanyok_IraIrat_Id_Dokumentum_Id] ON [dbo].[EREC_Csatolmanyok] ([IraIrat_Id], [Dokumentum_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_Dokumentum_Id_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	CREATE INDEX [IX_EREC_Csatolmanyok_Dokumentum_Id_ErvKezd_ErvVege] ON [dbo].[EREC_Csatolmanyok] ([Dokumentum_Id],[ErvKezd], [ErvVege]) INCLUDE ([KuldKuldemeny_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_KuldKuldemenyek_Erkezteto_Szam_PostazasIranya_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_KuldKuldemenyek'))
BEGIN
	CREATE INDEX [IX_EREC_KuldKuldemenyek_Erkezteto_Szam_PostazasIranya_ErvKezd_ErvVege] ON [dbo].[EREC_KuldKuldemenyek] ([Erkezteto_Szam],[PostazasIranya], [ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_CsoportTagok_Tipus_Csoport_Id_Jogalany_ErvKezd_ErvVege' AND object_id = OBJECT_ID('KRT_CsoportTagok'))
BEGIN
	CREATE INDEX [IX_KRT_CsoportTagok_Tipus_Csoport_Id_Jogalany_ErvKezd_ErvVege] ON [dbo].[KRT_CsoportTagok] ([Tipus], [Csoport_Id_Jogalany],[ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IrattariKikero_UgyUgyirat_Id_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_IrattariKikero'))
BEGIN
	CREATE INDEX [IX_EREC_IrattariKikero_UgyUgyirat_Id_ErvKezd_ErvVege] ON [dbo].[EREC_IrattariKikero] ([UgyUgyirat_Id],[ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IrattariKikero_FelhasznaloCsoport_Id_Jovahagy_Allapot_Irattar_Id_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_IrattariKikero'))
BEGIN
	CREATE INDEX [IX_EREC_IrattariKikero_FelhasznaloCsoport_Id_Jovahagy_Allapot_Irattar_Id_ErvKezd_ErvVege] ON [dbo].[EREC_IrattariKikero] ([FelhasznaloCsoport_Id_Jovahagy], [Allapot], [Irattar_Id],[ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IrattariKikero_UgyUgyirat_Id_Allapot_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_IrattariKikero'))
BEGIN
	CREATE INDEX [IX_EREC_IrattariKikero_UgyUgyirat_Id_Allapot_ErvKezd_ErvVege] ON [dbo].[EREC_IrattariKikero] ([UgyUgyirat_Id],[Allapot], [ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IrattariKikero_Allapot_UgyUgyirat_Id' AND object_id = OBJECT_ID('EREC_IrattariKikero'))
BEGIN
	CREATE INDEX [IX_EREC_IrattariKikero_Allapot_UgyUgyirat_Id] ON [dbo].[EREC_IrattariKikero] ([Allapot],[UgyUgyirat_Id]) INCLUDE ([FelhasznalasiCel], [FelhasznaloCsoport_Id_Kikero], [KikerKezd], [KikerVege], [FelhasznaloCsoport_Id_Kiado], [KiadasDatuma], [Irattar_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_Dokumentumok_BarCode_ErvKezd_ErvVege' AND object_id = OBJECT_ID('KRT_Dokumentumok'))
BEGIN
	CREATE INDEX [IX_KRT_Dokumentumok_BarCode_ErvKezd_ErvVege] ON [dbo].[KRT_Dokumentumok] ([BarCode],[ErvKezd], [ErvVege]) INCLUDE ([Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_UgyUgyiratok_IraIktatokonyv_Id_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_UgyUgyiratok'))
BEGIN
	CREATE INDEX [IX_EREC_UgyUgyiratok_IraIktatokonyv_Id_ErvKezd_ErvVege] ON [dbo].[EREC_UgyUgyiratok] ([IraIktatokonyv_Id],[ErvKezd], [ErvVege]) INCLUDE ([Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IraIktatoKonyvek_Ev_Iktatohely' AND object_id = OBJECT_ID('EREC_IraIktatoKonyvek'))
BEGIN
	CREATE INDEX [IX_EREC_IraIktatoKonyvek_Ev_Iktatohely] ON [dbo].[EREC_IraIktatoKonyvek] ([Ev], [Iktatohely])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_eBeadvanyok_Cel' AND object_id = OBJECT_ID('EREC_eBeadvanyok'))
BEGIN
	CREATE INDEX [IX_EREC_eBeadvanyok_Cel] ON [dbo].[EREC_eBeadvanyok] ([Cel]) INCLUDE ([Id], [KR_HivatkozasiSzam], [ErvVege], [KR_DokTipusLeiras])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_CsoportTagok_Csoport_Id_Jogalany_Tipus_ErvKezd_ErvVege' AND object_id = OBJECT_ID('KRT_CsoportTagok'))
BEGIN
	CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_Tipus_ErvKezd_ErvVege] ON [dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[Tipus], [ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_CsoportTagok_Csoport_Id_Jogalany_ErvKezd_ErvVege' AND object_id = OBJECT_ID('KRT_CsoportTagok'))
BEGIN
	CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_ErvKezd_ErvVege] ON [dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[ErvKezd], [ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_UgyUgyiratok_Allapot_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_UgyUgyiratok'))
BEGIN
	CREATE INDEX [IX_EREC_UgyUgyiratok_Allapot_ErvKezd_ErvVege] ON [dbo].[EREC_UgyUgyiratok] ([Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [IraIktatokonyv_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_UgyUgyiratok_IraIktatokonyv_Id_Allapot_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_UgyUgyiratok'))
BEGIN
	CREATE INDEX [IX_EREC_UgyUgyiratok_IraIktatokonyv_Id_Allapot_ErvKezd_ErvVege] ON [dbo].[EREC_UgyUgyiratok] ([IraIktatokonyv_Id],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_UIMezoObjektumErtekek_TemplateTipusNev_ErvKezd_ErvVege' AND object_id = OBJECT_ID('KRT_UIMezoObjektumErtekek'))
BEGIN
	CREATE INDEX [IX_KRT_UIMezoObjektumErtekek_TemplateTipusNev_ErvKezd_ErvVege] ON [dbo].[KRT_UIMezoObjektumErtekek] ([TemplateTipusNev],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Nev], [Alapertelmezett], [Felhasznalo_Id], [TemplateXML], [UtolsoHasznIdo], [Ver], [Note], [Stat_id], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Org_Id], [Publikus], [Szervezet_Id])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KRT_Dokumentumok_TartalomHash' AND object_id = OBJECT_ID('KRT_Dokumentumok'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_KRT_Dokumentumok_TartalomHash] ON [dbo].[KRT_Dokumentumok] ([TartalomHash])
END

-- EREC_Csatolmanyok �r�s gyors�t�s. 3 m�sodperc az �tlag. Felesleges 2 index. 
if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_IratId_DokumentumId_INCLUDE_Id' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_EREC_Csatolmanyok_IratId_DokumentumId_INCLUDE_Id ON [dbo].[EREC_Csatolmanyok]
	(
		IraIrat_Id ASC
		, Dokumentum_Id
	)
	INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

if exists (select 1 FROM sys.indexes WHERE name='IX_Csatolmanyok_IratId' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	DROP INDEX [IX_Csatolmanyok_IratId] ON [dbo].[EREC_Csatolmanyok]
END

if exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_IraIrat_Id_Dokumentum_Id' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	DROP INDEX [IX_EREC_Csatolmanyok_IraIrat_Id_Dokumentum_Id] ON [dbo].[EREC_Csatolmanyok]
END

if exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_IratId_INCLUDE_Id' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	DROP INDEX [IX_EREC_Csatolmanyok_IratId_INCLUDE_Id] ON [dbo].[EREC_Csatolmanyok]
END


-- Nincs index a top 1-es t�bla olvas�sra, amit AlairasSorrendben cs�kken�k�nt k�r�nk le
if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_IratAlairok_AlairasSorrend' AND object_id = OBJECT_ID('EREC_IratAlairok'))
BEGIN
	CREATE INDEX IX_EREC_IratAlairok_AlairasSorrend ON dbo.EREC_IratAlairok ( AlairasSorrend DESC )
END

-- l�trehoz�si id� szerint is sorrendez�nk
if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_Csatolmanyok_LetrehozasIdo' AND object_id = OBJECT_ID('EREC_Csatolmanyok'))
BEGIN
	CREATE INDEX IX_EREC_Csatolmanyok_LetrehozasIdo ON dbo.EREC_Csatolmanyok ( LetrehozasIdo )
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_CsoportTagokALL' AND object_id = OBJECT_ID('EREC_HataridosFeladatok'))
BEGIN
	print 'CREATE NONCLUSTERED INDEX [IX_CsoportTagokALL]'
	CREATE NONCLUSTERED INDEX [IX_CsoportTagokALL]
	ON [dbo].[EREC_HataridosFeladatok] ([Obj_type],[Allapot],[ErvKezd],[ErvVege])
	INCLUDE ([Felhasznalo_Id_Felelos],[Obj_Id])
END
GO


----Hianyzik
--CREATE INDEX [IX_EREC_eBeadvanyok_Irany_KuldKuldemeny_Id_Cel_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eBeadvanyok] ([Irany], [KuldKuldemeny_Id], [Cel],[ErvKezd], [ErvVege]) INCLUDE ([Id], [KR_ErkeztetesiDatum])

--CREATE INDEX [IX_EREC_eMailBoritekCsatolmanyok_eMailBoritek_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eMailBoritekCsatolmanyok] ([eMailBoritek_Id],[ErvKezd], [ErvVege])

--CREATE INDEX [IX_EREC_eMailBoritekok_KuldKuldemeny_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eMailBoritekok] ([KuldKuldemeny_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_eMailBoritekok_IraIrat_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eMailBoritekok] ([IraIrat_Id],[ErvKezd], [ErvVege])

--CREATE INDEX [IX_EREC_IraIratok_Irattipus_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IraIratok] ([Irattipus],[ErvKezd], [ErvVege]) INCLUDE ([Id], [UgyUgyIratDarab_Id], [Ugyirat_Id])
--CREATE INDEX [IX_EREC_PldIratPeldanyok_FelhasznaloCsoport_Id_Orzo_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_PldIratPeldanyok] ([FelhasznaloCsoport_Id_Orzo],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [IraIrat_Id])
--CREATE INDEX [IX_EREC_IraKezbesitesiTetelek_Obj_type_Felhasznalo_Id_Atado_USER_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IraKezbesitesiTetelek] ([Obj_type], [Felhasznalo_Id_Atado_USER], [Allapot],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Obj_Id], [Csoport_Id_Cel])
--CREATE INDEX [IX_EREC_PldIratPeldanyok_FelhasznaloCsoport_Id_Orzo_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_PldIratPeldanyok] ([FelhasznaloCsoport_Id_Orzo],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([TovabbitasAlattAllapot])
--CREATE INDEX [IX_EREC_PldIratPeldanyok_FelhasznaloCsoport_Id_Orzo_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_PldIratPeldanyok] ([FelhasznaloCsoport_Id_Orzo],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [IraIrat_Id], [TovabbitasAlattAllapot])
--CREATE INDEX [IX_EREC_IraKezbesitesiTetelek_Felhasznalo_Id_Atado_USER_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IraKezbesitesiTetelek] ([Felhasznalo_Id_Atado_USER], [Allapot],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Obj_Id], [Obj_type], [Csoport_Id_Cel])
--CREATE INDEX [IX_EREC_Csatolmanyok_Dokumentum_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_Csatolmanyok] ([Dokumentum_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_IratelemKapcsolatok_Melleklet_Id] ON [EDOK].[dbo].[EREC_IratelemKapcsolatok] ([Melleklet_Id]) INCLUDE ([Csatolmany_Id])
--CREATE INDEX [IX_EREC_PldIratPeldanyok_FelhasznaloCsoport_Id_Orzo_Allapot_TovabbitasAlattAllapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_PldIratPeldanyok] ([FelhasznaloCsoport_Id_Orzo],[Allapot], [TovabbitasAlattAllapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [IraIrat_Id])
--CREATE INDEX [IX_HKP_DokumentumAdatok_KuldKuldemeny_Id] ON [EDOK].[dbo].[HKP_DokumentumAdatok] ([KuldKuldemeny_Id]) INCLUDE ([Id])
--CREATE INDEX [IX_KRT_Felhasznalok_Org_EMail] ON [EDOK].[dbo].[KRT_Felhasznalok] ([Org], [EMail])
--CREATE INDEX [IX_EREC_eBeadvanyok_KuldoRendszer_KuldKuldemeny_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eBeadvanyok] ([KuldoRendszer], [KuldKuldemeny_Id],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Irany], [Allapot], [UzenetTipusa], [FeladoTipusa], [PartnerKapcsolatiKod], [PartnerNev], [PartnerEmail], [PartnerRovidNev], [PartnerMAKKod], [PartnerKRID], [Partner_Id], [Cim_Id], [KR_HivatkozasiSzam], [KR_ErkeztetesiSzam], [Contentum_HivatkozasiSzam], [PR_HivatkozasiSzam], [PR_ErkeztetesiSzam], [KR_DokTipusHivatal], [KR_DokTipusAzonosito], [KR_DokTipusLeiras], [KR_Megjegyzes], [KR_ErvenyessegiDatum], [KR_ErkeztetesiDatum], [KR_FileNev], [KR_Kezbesitettseg], [KR_Idopecset], [KR_Valasztitkositas], [KR_Valaszutvonal], [KR_Rendszeruzenet], [KR_Tarterulet], [KR_ETertiveveny], [KR_Lenyomat], [IraIrat_Id], [IratPeldany_Id], [Cel], [Ver], [Note], [Stat_id], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [PR_Parameterek], [KR_Fiok])
--CREATE INDEX [IX_EREC_IraKezbesitesiTetelek_Felhasznalo_Id_Atado_USER_Allapot_Obj_type_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IraKezbesitesiTetelek] ([Felhasznalo_Id_Atado_USER], [Allapot],[Obj_type], [ErvKezd], [ErvVege]) INCLUDE ([Id], [Obj_Id], [Csoport_Id_Cel])
--CREATE INDEX [IX_EREC_eBeadvanyok_Cel_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_eBeadvanyok] ([Cel],[ErvKezd], [ErvVege]) INCLUDE ([Id], [LetrehozasIdo])
--CREATE INDEX [IX_EREC_HataridosFeladatok_Tipus_Felhasznalo_Id_Felelos_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_HataridosFeladatok] ([Tipus],[Felhasznalo_Id_Felelos], [ErvKezd], [ErvVege]) INCLUDE ([Id], [HataridosFeladat_Id])
--CREATE INDEX [IX_EREC_IratMetaDefinicio_Org_Ugykor_Id_EljarasiSzakasz_Irattipus_Ugytipus] ON [EDOK].[dbo].[EREC_IratMetaDefinicio] ([Org], [Ugykor_Id], [EljarasiSzakasz], [Irattipus],[Ugytipus])
--CREATE INDEX [IX_EREC_IratelemKapcsolatok_Csatolmany_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IratelemKapcsolatok] ([Csatolmany_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_Mellekletek_BarCode_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_Mellekletek] ([BarCode],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_KRT_Csoportok_Org_ErtesitesEmail_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Csoportok] ([Org], [ErtesitesEmail],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_IraIratok_FelhasznaloCsoport_Id_Ugyintez] ON [EDOK].[dbo].[EREC_IraIratok] ([FelhasznaloCsoport_Id_Ugyintez]) INCLUDE ([Ugyirat_Id])
--CREATE INDEX [IX_KRT_Funkciok_ObjTipus_Id_AdatElem_Muvelet_Id] ON [EDOK].[dbo].[KRT_Funkciok] ([ObjTipus_Id_AdatElem], [Muvelet_Id])
--CREATE INDEX [IX_EREC_IrattariTetel_Iktatokonyv_IrattariTetel_Id_Iktatokonyv_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IrattariTetel_Iktatokonyv] ([IrattariTetel_Id], [Iktatokonyv_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_KRT_Felhasznalo_Szerepkor_CsoportTag_Id_Felhasznalo_Id_Helyettesites_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Felhasznalo_Szerepkor] ([CsoportTag_Id], [Felhasznalo_Id], [Helyettesites_Id],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Csoport_Id], [Szerepkor_Id], [Ver], [Note], [Stat_id], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])
--CREATE INDEX [IX_KRT_Felhasznalo_Szerepkor_CsoportTag_Id_Felhasznalo_Id_Helyettesites_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Felhasznalo_Szerepkor] ([CsoportTag_Id], [Felhasznalo_Id], [Helyettesites_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_UgyUgyiratok_FelhasznaloCsoport_Id_Ugyintez_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_UgyUgyiratok] ([FelhasznaloCsoport_Id_Ugyintez],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [IraIktatokonyv_Id], [TovabbitasAlattAllapot])
--CREATE INDEX [IX_KRT_Helyettesitesek_Felhasznalo_ID_helyettesito_HelyettesitesKezd_HelyettesitesVege_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Helyettesitesek] ([Felhasznalo_ID_helyettesito],[HelyettesitesKezd], [HelyettesitesVege], [ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_IraKezbesitesiTetelek_Obj_type_Csoport_Id_Cel_Allapot_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IraKezbesitesiTetelek] ([Obj_type], [Csoport_Id_Cel],[Allapot], [ErvKezd], [ErvVege]) INCLUDE ([Id], [Obj_Id], [Felhasznalo_Id_Atado_USER])
--CREATE INDEX [IX_KRT_Felhasznalok_Org_EMail_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Felhasznalok] ([Org], [EMail],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_IrattariTetel_Iktatokonyv_IrattariTetel_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_IrattariTetel_Iktatokonyv] ([IrattariTetel_Id],[ErvKezd], [ErvVege]) INCLUDE ([Iktatokonyv_Id])

--CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_Tipus] ON [EDOK].[dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[Tipus])
--CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_Tipus_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[Tipus], [ErvKezd], [ErvVege])
--CREATE INDEX [IX_KRT_CsoportTagok_Csoport_Id_Jogalany_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_CsoportTagok] ([Csoport_Id_Jogalany],[ErvKezd], [ErvVege]) INCLUDE ([Id], [Tipus], [Csoport_Id], [ObjTip_Id_Jogalany], [ErtesitesMailCim], [ErtesitesKell], [System], [Orokolheto], [Ver], [Note], [Stat_id], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])
--CREATE INDEX [IX_KRT_UIMezoObjektumErtekek_Alapertelmezett_Felhasznalo_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_UIMezoObjektumErtekek] ([Alapertelmezett], [Felhasznalo_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_EREC_UgyUgyiratok_Allapot_Kovetkezo_Felelos_Id_ErvKezd_ErvVege] ON [EDOK].[dbo].[EREC_UgyUgyiratok] ([Allapot], [Kovetkezo_Felelos_Id],[ErvKezd], [ErvVege])
--CREATE INDEX [IX_KRT_Helyettesitesek_Felhasznalo_ID_helyettesito_HelyettesitesKezd_HelyettesitesVege_ErvKezd_ErvVege] ON [EDOK].[dbo].[KRT_Helyettesitesek] ([Felhasznalo_ID_helyettesito],[HelyettesitesKezd], [HelyettesitesVege], [ErvKezd], [ErvVege]) INCLUDE ([Id], [Felhasznalo_ID_helyettesitett])
--CREATE INDEX [IX_KRT_Tanusitvanyok_Csoport_Id_Ervenytelenites_Visszavonas] ON [EDOK].[dbo].[KRT_Tanusitvanyok] ([Csoport_Id],[Ervenytelenites], [Visszavonas])
