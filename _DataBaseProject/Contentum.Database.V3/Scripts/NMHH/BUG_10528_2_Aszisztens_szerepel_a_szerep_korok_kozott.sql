--------------------------------------------------#10528---------------------------------------------------------
--Azokat a szerepkoroket amik nem fejleszto vagy alkalmazasgazda es nincs csoporthoz rendelve ervenyteleniti-----
-----------------------------------------------------------------------------------------------------------------

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF(@org_kod = 'NMHH')
BEGIN
	UPDATE krt_Felhasznalo_Szerepkor SET ervvege = GETDATE() 
	WHERE 
		Helyettesites_Id is null 
		AND Csoport_Id is null 
		AND Szerepkor_Id not in (SELECT id FROM KRT_Szerepkorok WHERE nev IN ('FEJLESZTO','ALKALMAZASGAZDA'))
END
-------------------------------------------------------------------------------------------------------------------