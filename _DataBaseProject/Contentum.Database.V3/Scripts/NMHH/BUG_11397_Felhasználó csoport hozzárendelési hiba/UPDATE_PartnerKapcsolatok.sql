DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

------------------------------------ NMHH -------------------------------------------------------
IF @org_kod = 'NMHH'
BEGIN
	PRINT @org_kod 

	--Ha m�r l�tezik a #Temp t�bla akkor azt droppoljuk:
	IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] like '#Temp%') BEGIN
	   DROP TABLE #Temp;
	END;

	SELECT DISTINCT f.Partner_id AS partner_id_felhasznalo, fsz.Csoport_Id as partner_id_szervezet 
	INTO #Temp
	FROM KRT_Felhasznalo_Szerepkor fsz INNER JOIN KRT_Felhasznalok f ON f.Id = fsz.Felhasznalo_Id  WHERE  fsz.Id NOT IN
	(
	SELECT fsz.id FROM KRT_CsoportTagok cst INNER JOIN KRT_Felhasznalo_Szerepkor fsz ON fsz.Csoport_Id = cst.Csoport_Id AND fsz.Felhasznalo_Id = cst.Csoport_Id_Jogalany
	WHERE fsz.Csoport_Id IS NOT NULL AND fsz.ErvVege > GETDATE()
	)
	AND fsz.Csoport_Id IS NOT NULL AND fsz.ErvVege > GETDATE()
	AND f.ErvVege > GETDATE()

	--SELECT * FROM #Temp

	--Iter�ci�hoz @partner_id_felhasznalo v�ltoz�, az aktu�lis iter�ci�ban az partner_id_felhasznalo-t t�rolja.
	DECLARE @partner_id_felhasznalo NVARCHAR(MAX)
	--Iter�ci�hoz @partner_id_szervezet v�ltoz�, az aktu�lis iter�ci�ban az partner_id_szervezet-t t�rolja.
	DECLARE @partner_id_szervezet NVARCHAR(MAX)
	--Iter�ci�ban a ciklusok sz�m�t t�rolja.
	DECLARE @counter INT = 0

	WHILE @@ROWCOUNT <> 0
	BEGIN
		SET ROWCOUNT 0
	
		--PRINT @partner_id_felhasznalo 
		--PRINT @partner_id_szervezet 
	
		SET ROWCOUNT 1

		SELECT @partner_id_felhasznalo = partner_id_felhasznalo FROM #Temp
		SELECT @partner_id_szervezet = partner_id_szervezet FROM #Temp

		--UPDATE-elj�k az KRT_PartnerKapcsolatok t�bl�t WS h�v�ssal (KRT_PartnerKapcsolatokService.Insert) 
		DECLARE @REQUESTTEXT VARCHAR(8000)
		DECLARE @RESPONSETEXT VARCHAR(8000)
		SET @REQUESTTEXT =
		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			 <soap:Body>
				<Insert xmlns="Contentum.eAdmin.WebService">
					 <ExecParam>
						<Fake>false</Fake>
						<Felhasznalo_Id>54E861A5-36ED-44CA-BAA7-C287D125B309</Felhasznalo_Id>
						<FelhasznaloSzervezet_Id>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</FelhasznaloSzervezet_Id>
					 </ExecParam>
					 <Record>
						<Updated>
						   <Partner_id>true</Partner_id>
						   <Partner_id_kapcsolt>true</Partner_id_kapcsolt>
						   <Tipus>true</Tipus>
						</Updated>
						<Partner_id>' + @partner_id_felhasznalo + '</Partner_id>
						<Partner_id_kapcsolt>' + @partner_id_szervezet + '</Partner_id_kapcsolt>
						<Tipus>2</Tipus>
					 </Record>
				</Insert>
			 </soap:Body>
		</soap:Envelope>'

	

		exec SP_HTTPREQUEST 
		'$(eAdminWebServiceUrl)/KRT_PartnerKapcsolatokService.asmx',
			'POST', 
			@REQUESTTEXT,
			'Contentum.eAdmin.WebService/Insert',
			'', 
			'', 
			@RESPONSETEXT out

			PRINT @REQUESTTEXT
			PRINT @RESPONSETEXT
			PRINT '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'

		--Kit�r�lj�k az aktu�lis sort a #Temp t�bl�b�l
		DELETE #Temp 
		WHERE partner_id_felhasznalo = @partner_id_felhasznalo and partner_id_szervezet = @partner_id_szervezet
	
	END
	SET ROWCOUNT 0

END