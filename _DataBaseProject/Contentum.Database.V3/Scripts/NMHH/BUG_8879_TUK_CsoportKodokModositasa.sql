﻿-- BUG_8879 TÜK Csoportkódok módosítása
UPDATE [KRT_Csoportok]
	SET Kod = 'NY'
WHERE nev ='Nyilvántartó'

UPDATE [KRT_Csoportok]
	SET Kod = 'KR'
WHERE nev ='Reviczky utcai kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'KV'
WHERE nev ='Visegrádi utcai kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'KV'
WHERE nev ='Visegrád utcai kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'CD'
WHERE nev ='Debreceni kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'CM'
WHERE nev ='Miskolci kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'CP'
WHERE nev ='Pécsi kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'CS'
WHERE nev ='Soproni kezelő pont'

UPDATE [KRT_Csoportok]
	SET Kod = 'CZ'
WHERE nev ='Szegedi kezelő pont'
