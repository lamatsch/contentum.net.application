DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' 
and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 


IF @isTUK = '1'
BEGIN

	declare @azonosito nvarchar(20)
	set @azonosito = '405'

	DECLARE @record_id uniqueidentifier 
	SET @record_id = 'E202E3EE-A8C8-E811-80CE-00155D020DD3'

	IF EXISTS (SELECT 1 from EREC_IraIktatoKonyvek WHERE Azonosito = @azonosito and IktatoErkezteto = 'P')
	BEGIN
		PRINT 'UPATE KRT_Parameterek - ELEKTRONIKUS_POSTAKONYV_AZONOSITO'
		UPDATE KRT_Parameterek
		 SET Ertek=@azonosito
		 WHERE Id=@record_id 	
	END

END

GO 