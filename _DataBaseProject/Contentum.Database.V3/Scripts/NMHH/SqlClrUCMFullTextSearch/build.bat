@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help


echo. !!!!! Adatbázis módosító szkriptek futtatása az alábbi beállításokkal !!!!!
echo Szerver: %1 Adatbázis: %2 App Url: %3


@echo on
%sqlcmd% -S %1 -d %2 -i enable_clr.sql -v DatabaseName=%2
%sqlcmd% -S %1 -d %2 -i SeteDocumentWebServiceUrl.sql -v eDocumentWebServiceUrl="%3/eDocumentWebService"
%sqlcmd% -S %1 -d %2 -i SqlClrUCMFullTextSearch.publish.sql
@echo off


goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: app url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://edoktest
echo. build.bat INCSQL2 EDOK http://edok

:EOF