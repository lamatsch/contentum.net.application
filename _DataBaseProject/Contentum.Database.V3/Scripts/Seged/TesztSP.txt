
EXEC [sp_EREC_UgyUgyiratokGetAllWithExtension] 
@Where = N'(( (EREC_UgyUgyiratok.ErvKezd <= getdate()) and (EREC_UgyUgyiratok.ErvVege >= getdate())))'
	,@TopRow = N'0'
	,@ExecutorUserId = 'E3F5ED92-425C-4C20-8E41-2B95160113EE'
	,@FelhasznaloSzervezet_Id = '9A570CA9-BEB7-4235-9AB8-E87982948273'
	,@Where_KuldKuldemenyek = N''
	,@Where_UgyUgyiratdarabok = N''
	,@Where_IraIratok = N''
	,@Where_IraIktatokonyvek = N'((((EREC_IraIktatoKonyvek.Ev between 2018 and 2018) and (EREC_IraIktatoKonyvek.Iktatohely = ''FPH003''))))'
	,@Where_Dosszie = N''
	,@Jogosultak = '1'
	,@Where_EREC_IratMetaDefinicio = N''
	,@Csoporttagokkal = 1
	,@CsakAktivIrat = 0