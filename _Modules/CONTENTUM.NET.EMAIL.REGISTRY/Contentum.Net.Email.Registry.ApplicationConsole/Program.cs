﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Manager;
using System;
using System.Threading;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.ApplicationConsole
{
    class Program
    {
        static string CONST_MUTEX_NAME = "Contentum.Net.Email.Registry.TestConsole";
        static Mutex MyMutex;
        static void Main(string[] args)
        {
         //   SetManagedEmail();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            if (!Program.IsSingleInstance())
            {
                Console.WriteLine("MORE THAN ONE INSTANCE");
                Environment.Exit(1);
            }
            Console.WriteLine("START APP");
            Start();
            Console.WriteLine("STOP APP");
            Thread.Sleep(3000);
        }
        private static void Start()
        {
            MainManager man = new MainManager();
            man.Start(); // man.StartParallel();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exc = e.ExceptionObject as Exception;
            Console.WriteLine(exc.Message);
            Console.WriteLine(exc.StackTrace);
            if (exc.InnerException != null)
            {
                Console.WriteLine(exc.InnerException.Message);
                Console.WriteLine(exc.InnerException.StackTrace);
            }
        }

        private static bool IsSingleInstance()
        {
            try
            {
                // Try to open existing mutex.
                Mutex.OpenExisting(CONST_MUTEX_NAME);
            }
            catch
            {
                // If exception occurred, there is no such mutex.
                Program.MyMutex = new Mutex(true, CONST_MUTEX_NAME);

                // Only one instance.
                return true;
            }
            // More than one instance.
            return false;
        }
        private static void SetManagedEmail()
        {
            DBO_ManagedEmailParameters item = new DBO_ManagedEmailParameters();

            item.EmailAddress = "edoksupport@axis.hu";
            item.ImapLoginPassword = "********";
            item.ImapLoginUser = "edoksupport@axis.hu";
            item.ImapServerAddress = "mail.axis.hu";
            item.ImapServerPort = 993;
            item.ImapUseSSL = true;

            item.FelhasznaloId = "54E861A5-36ED-44CA-BAA7-C287D125B309";
            item.FelhasznaloSzervezetId = "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6";

            item.KeresesiParameterErkeztetoKonyvAzonosito = "AUTO";
            item.KeresesiParameterIktatoKonyvAzonosito = "AUTO";
            item.KeresesiParameterIrattáriTételAzonosító = "it0001";

            item.DokumentumFeldolgozasTipusa = (ushort)AxisConstants.EnumDocumentTaskType.ErkeztetEsIktat;// 2;

            item.ValaszEmailAutomatikusan = true;
            item.ValaszEmailFeladoCim = "edoksupport@axis.hu";
            item.ValaszEmailTargy = "Automatikus e-mail érkeztetés/iktatás értesítés";
            item.ValaszEmailSablonErkeztetes =
                  "Tisztelt <b>{0}</b>"
                + "<br/>"
                + "<br/>"
                + "Ön {1} napon <b>{2}</b> tárgyban küldeményt küldött be: "
                + "<br/>"
                //+ "Érkezett dokumentumok:"
                //+ "<br/>"
                //+ "{3}"
                //+ "<br/>"
                + "<br/>"
                + "Ezúton tájékoztatom, hogy levelét érkeztettük az <b>{5}</b>-ös érkeztetőszámon."
                + "<br/>"
                + "Felelős személy: {6}"
                + "<br/>"
                + "<br/>"
                + "<br/>"
                + "<br/>"
                + "<br/>"
                + "Üdvözlettel:"
                + "<br/>"
                + "Fővárosi Polgármesteri Hivatal,"
                + "<br/>"
                + "Igazgatási és Hatósági Főosztály"
                + "<br/>"
                + "Ügyfélszolgálati Iroda"
                + "<br/>"
                + "Tel: 327-12-08"
                + "<br/>"
                + "E-mail.: ugyfelszolgalat@budapest.hu";

            item.ValaszEmailSablonIktatas =
                   "Tisztelt <b>{0}</b>"
                 + "<br/>"
                 + "<br/>"
                 + "Ön {1} napon <b>{2}</b> tárgyban küldeményt küldött be: "
                 + "<br/>"
                 //+ "Érkezett dokumentumok:"
                 //+ "<br/>"
                 //+ "{3}"
                 //+ "<br/>"
                 + "<br/>"
                 + "Ezúton tájékoztatom, hogy levelét iktattuk az <b>{5}</b>-ös iktatószámon."
                 + "<br/>"
                 + "Felelős személy: {6}"
                 + "<br/>"
                 + "<br/>"
                 + "<br/>"
                 + "<br/>"
                 + "<br/>"
                 + "Üdvözlettel:"
                 + "<br/>"
                 + "Fővárosi Polgármesteri Hivatal,"
                 + "<br/>"
                 + "Igazgatási és Hatósági Főosztály"
                 + "<br/>"
                 + "Ügyfélszolgálati Iroda"
                 + "<br/>"
                 + "Tel: 327-12-08"
                 + "<br/>"
                 + "E-mail.: ugyfelszolgalat@budapest.hu";

            item.Kuldemenyek_Csoport_Id_Cimzett = "54E861A5-36ED-44CA-BAA7-C287D125B309";
            item.Kuldemenyek_Csoport_Id_Felelos = "54E861A5-36ED-44CA-BAA7-C287D125B309";

            item.UgyUgyiratok_Csoport_Id_Felelos = "54E861A5-36ED-44CA-BAA7-C287D125B309";
            item.UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez = "54E861A5-36ED-44CA-BAA7-C287D125B309";
            item.UgyUgyiratok_iraIrattariTetel_Id = "92B20248-62FA-E511-80BA-00155D020B4B";

            item.IraIratok_FelhasznaloCsoport_Id_Ugyintez = "54E861A5-36ED-44CA-BAA7-C287D125B309";

            item.IktatasiParameterek_UgykorId = "e9758301-8465-e211-859e-001ec9e754bc";

            string json = JSONFunctions.SerializeManagedEmailParameters(item);
            // DataBaseProvider.AddManagedEmailAddress(new Guid("54E861A5-36ED-44CA-BAA7-C287D125B309"), item.EmailAddress, json, null);

        }
    }
}

