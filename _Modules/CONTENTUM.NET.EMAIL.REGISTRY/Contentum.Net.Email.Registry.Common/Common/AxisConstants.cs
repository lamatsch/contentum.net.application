﻿using System.ComponentModel;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common
{
    public static class AxisConstants
    {
        public static string EmailFileExtension = ".eml";
        public static string PendingEmailFileExtension = ".pending";
        public static string EmailFileName = "Email.eml";

        public enum EnumDocumentTaskType : short
        {
            Erkeztet = 1,
            ErkeztetEsIktat = 2,
            ErkeztetEsIktatEsIrattaroz = 3
        };

        public enum EnumKonyvType : short
        {
            [Description("ÉrkeztetőKönyv")]
            E = 1,
            [Description("IktatóKönyv")]
            I = 2,
            [Description("PostaKönyv")]
            P = 3
        };

        #region PARAMETEREK
        public static string ConstParameterekRendszerfelugyeletEmail = "RENDSZERFELUGYELET_EMAIL_CIM";
        #endregion

    }
}
