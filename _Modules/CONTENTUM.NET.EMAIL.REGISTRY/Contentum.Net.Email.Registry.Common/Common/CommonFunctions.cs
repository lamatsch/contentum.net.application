﻿using MimeKit;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common
{
    public class CommonFunctions
    {
        /// <summary>
        /// Create local directory
        /// </summary>
        /// <param name="folder"></param>
        public static string CreateEmailDirectory(string folder)
        {
            string mailPath = Path.Combine(AssemblyDirectory, folder);
            if (!Directory.Exists(mailPath))
                Directory.CreateDirectory(mailPath);
            return mailPath;
        }
        /// <summary>
        /// Create local directory
        /// </summary>
        /// <param name="folder"></param>
        public static string GetEmailDirectory(string folder)
        {
            string mailPath = Path.Combine(AssemblyDirectory, folder);
            if (!Directory.Exists(mailPath))
                return null;
            return mailPath;
        }
        /// <summary>
        /// Return current assembly path
        /// </summary>
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
        /// <summary>
        /// Return email message unique file name
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string GetImapMessageFileName(AxisImapMessage message)
        {
            return string.Format("{0}{1}{2}", message.Uid, message.Message.MessageId, AxisConstants.EmailFileExtension);
        }
        /// <summary>
        /// Return email message unique file name
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string GetImapMessageFileName(MimeMessage message)
        {
            return string.Format("{0}{1}", message.MessageId, AxisConstants.EmailFileExtension);
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetShortPathName(
            [MarshalAs(UnmanagedType.LPTStr)]
                   string path,
            [MarshalAs(UnmanagedType.LPTStr)]
                   StringBuilder shortPath,
            int shortPathLength
            );
        /// <summary>
        /// Return path in short format
        /// </summary>
        /// <param name="longPath"></param>
        /// <returns></returns>
        public static string GetShortPathName(string longPath)
        {
            StringBuilder shortPath = new StringBuilder(255);
            GetShortPathName(longPath, shortPath, shortPath.Capacity);
            return shortPath.ToString();
        }

        #region MIMEKIT - MAILKIT
        /// <summary>
        /// Convert mail attachments to byte array list
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public static List<byte[]> ConvertMailAttachmentsToByteArray(MimeMessage mail)
        {
            List<byte[]> result = new List<byte[]>();
            var attachments = mail.Attachments.ToList();
            foreach (var attachment in attachments)
            {
                result.Add(ConvertMailAttachmentToByteArray(attachment));
            }
            return result;
        }

        /// <summary>
        /// Convert mail attachment to byte array
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public static byte[] ConvertMailAttachmentToByteArray(MimeEntity attachment)
        {
            using (var memory = new MemoryStream())
            {
                if (attachment is MimePart)
                    ((MimePart)attachment).ContentObject.DecodeTo(memory);
                else
                    ((MessagePart)attachment).Message.WriteTo(memory);
                return memory.ToArray();
            }
        }

        /// <summary>
        /// Convert mail to string
        /// </summary>
        /// <param name="message"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string ConvertMailToString(MimeMessage message, Encoding encoding)
        {
            using (var memory = new MemoryStream())
            {
                message.WriteTo(memory);

                var buffer = memory.GetBuffer();
                int count = (int)memory.Length;

                return encoding.GetString(buffer, 0, count);
            }
        }

        /// <summary>
        /// Convert entire mail to byte array
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static byte[] ConvertMailToByteArray(MimeMessage message)
        {
            using (var memory = new MemoryStream())
            {
                message.WriteTo(memory);
                return memory.ToArray();
            }
        }
        #endregion

        #region SHA1
        public static string SHA1Hash(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }
        #endregion
    }
}
