﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO
{
    public partial class DBO_ManagedEmailParameters
    {
        /// <summary>
        /// Email address
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Email cím")]
        [Description("Email cím")]
        public string EmailAddress { get; set; }

        #region SERVER
        /// <summary>
        /// Imap szerver címe (etc: imap.gmail.com)
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Imap szerevr címe")]
        [Description("Imap szerevr címe")]

        public string ImapServerAddress { get; set; }
        /// <summary>
        /// Imap szerver port száma (etc: 993)
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Imap szerevr port száma")]
        [Description("Imap szerevr port száma")]
        public int ImapServerPort { get; set; }

        /// <summary>
        /// Kell ssl
        /// </summary>
        [Browsable(true)]
        [DisplayName("Kell SSL")]
        [Description("Kell SSL")]
        public bool ImapUseSSL { get; set; }

        /// <summary>
        /// Imap felhasználó neve
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Imap felhasználó azonosítója")]
        [Description("Imap felhasználó azonosítója")]
        public string ImapLoginUser { get; set; }

        /// <summary>
        /// Imap felhasználó jelszava
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Imap felhasználó jelszava")]
        [Description("Imap felhasználó jelszava")]
        public string ImapLoginPassword { get; set; }
        #endregion

        /// <summary>
        /// Dokumentum kezelés típusa
        /// </summary>
        [Required]
        [Browsable(true)]
        [DisplayName("Dokumentum kezelés típusa")]
        [Description("Dokumentum kezelés típusa")]
        public int? DokumentumFeldolgozasTipusa { get; set; }

        /// <summary>
        /// Automatikus válasz?
        /// </summary>
        [Browsable(true)]
        [DisplayName("Automatikus válasz kell")]
        [Description("Automatikus válasz kell")]
        public bool? ValaszEmailAutomatikusan { get; set; }

        ///// <summary>
        ///// Felelős
        ///// </summary>
        //[Browsable(true)]
        //[DisplayName("Felelős felhasználó")]
        //[Description("Felelős felhasználó")]
        //public string Responsible { get; set; }

        [Browsable(true)]
        [DisplayName("Technikai felhasználó nevében")]
        [Description("Technikai felhasználó nevében")]
        public bool TechnikaiFelhasznaloNeveben { get; set; }

        #region IDENTITIES
        /// <summary>
        /// Érkeztető könyv azonosító
        /// </summary>
        [Browsable(true)]
        [DisplayName("Érkeztető könyv azonosító")]
        [Description("Érkeztető könyv azonosító")]
        public string KeresesiParameterErkeztetoKonyvAzonosito { get; set; }

        /// <summary>
        /// Iktató könyv azonosító
        /// </summary>
        [Browsable(true)]
        [DisplayName("Iktató könyv azonosító")]
        [Description("Iktató könyv azonosító")]
        public string KeresesiParameterIktatoKonyvAzonosito { get; set; }

        /// <summary>
        /// Irattári tétel azonosító
        /// </summary>
        [Browsable(true)]
        [DisplayName("Irattári tétel azonosító")]
        [Description("Irattári tétel azonosító")]
        public string KeresesiParameterIrattáriTételAzonosító { get; set; }
        #endregion

        #region EXEC PARAM
        /// <summary>
        /// 
        /// </summary>
        [Browsable(true)]
        [DisplayName("Felhasználó azonosító")]
        [Description("Felhasználó azonosító")]
        public string FelhasznaloId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Browsable(true)]
        [DisplayName("FelhasznaloSzervezet azonosító")]
        [Description("FelhasznaloSzervezet azonosító")]
        public string FelhasznaloSzervezetId { get; set; }
        #endregion

        #region RESPONSE
        /// <summary>
        /// Válasz levél küldő mail címe
        /// </summary>
        [Browsable(true)]
        [DisplayName("Válasz levél küldő mail címe")]
        [Description("Válasz levél küldő mail címe")]
        public string ValaszEmailFeladoCim { get; set; }

        /// <summary>
        /// Válasz levél tárgy mezője
        /// </summary>
        [Browsable(true)]
        [DisplayName("Válasz levél tárgy mezője")]
        [Description("Válasz levél tárgy mezője")]
        public string ValaszEmailTargy { get; set; }

        /// <summary>
        ///  Az iktatott küldeményekre küldött válasz levél sablon szövege
        /// </summary>
        [Browsable(true)]
        [DisplayName("Iktatás válasz levél sablonja")]
        [Description("Iktatás válasz levél sablonja")]
        public string ValaszEmailSablonIktatas { get; set; }

        /// <summary>
        /// Az érkeztetett küldeményekre küldött válasz levél sablon szövege
        /// </summary>
        [Browsable(true)]
        [DisplayName("Érkeztetés válasz levél sablonja")]
        [Description("Érkeztetés válasz levél sablonja")]
        public string ValaszEmailSablonErkeztetes { get; set; }
        #endregion

        #region KULDEMENY
        /// <summary>
        /// Kuldemenyek_Csoport_Id_Cimzett
        /// </summary>
        [Browsable(true)]
        [DisplayName("Kuldemenyek_Csoport_Id_Cimzett")]
        [Description("Kuldemenyek_Csoport_Id_Cimzett")]
        public string Kuldemenyek_Csoport_Id_Cimzett { get; set; }
        /// <summary>
        /// Kuldemenyek_Csoport_Id_Felelos
        /// </summary>
        [Browsable(true)]
        [DisplayName("Kuldemenyek_Csoport_Id_Felelos")]
        [Description("Kuldemenyek_Csoport_Id_Felelos")]
        public string Kuldemenyek_Csoport_Id_Felelos { get; set; }
        #endregion

        #region UGYIRAT
        /// <summary>
        /// UgyUgyiratok_Csoport_Id_Felelos
        /// </summary>
        [Browsable(true)]
        [DisplayName("UgyUgyiratok_Csoport_Id_Felelos")]
        [Description("UgyUgyiratok_Csoport_Id_Felelos")]
        public string UgyUgyiratok_Csoport_Id_Felelos { get; set; }

        /// <summary>
        /// UgyUgyiratok_iraIrattariTetel_Id
        /// </summary>
        [Browsable(true)]
        [DisplayName("UgyUgyiratok_iraIrattariTetel_Id")]
        [Description("UgyUgyiratok_iraIrattariTetel_Id")]
        public string UgyUgyiratok_iraIrattariTetel_Id { get; set; }

        /// <summary>
        /// UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez
        /// </summary>
        [Browsable(true)]
        [DisplayName("UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez")]
        [Description("UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez")]
        public string UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez { get; set; }
        #endregion

        #region IRAIRATOK
        /// <summary>
        /// IraIratok_FelhasznaloCsoport_Id_Ugyintez
        /// </summary>
        [Browsable(true)]
        [DisplayName("IraIratok_FelhasznaloCsoport_Id_Ugyintez")]
        [Description("IraIratok_FelhasznaloCsoport_Id_Ugyintez")]
        public string IraIratok_FelhasznaloCsoport_Id_Ugyintez { get; set; }
        #endregion

        #region IKTATASI PARAMÉTEREK
        /// <summary>
        /// IktatasiParameterek_UgykorId
        /// </summary>
        [Browsable(true)]
        [DisplayName("IktatasiParameterek_UgykorId")]
        [Description("IktatasiParameterek_UgykorId")]
        public string IktatasiParameterek_UgykorId { get; set; }
        #endregion
    }

    /// <summary>
    /// Non serialized attributes
    /// </summary>
    public partial class DBO_ManagedEmailParameters
    {
        [JsonIgnore]
        [NonSerialized]
        private string iraErkeztetoKonyvId;
        [JsonIgnore]
        [DisplayName("IraErkeztetoKonyvId")]
        [Description("IraErkeztetoKonyvId")]
        public string IraErkeztetoKonyvId
        {
            get { return iraErkeztetoKonyvId; }
            set { iraErkeztetoKonyvId = value; }
        }

        [JsonIgnore]
        [NonSerialized]
        private string iraIktatoKonyvId;
        [JsonIgnore]
        [DisplayName("IraIktatoKonyvId")]
        [Description("IraIktatoKonyvId")]
        public string IraIktatoKonyvId
        {
            get { return iraIktatoKonyvId; }
            set { iraIktatoKonyvId = value; }
        }

        [JsonIgnore]
        [NonSerialized]
        private string iraPostaKonyvId;
        [JsonIgnore]
        [DisplayName("IraPostaKonyvId")]
        [Description("IraPostaKonyvId")]
        public string IraPostaKonyvId
        {
            get { return iraPostaKonyvId; }
            set { iraPostaKonyvId = value; }
        }

        [JsonIgnore]
        [NonSerialized]
        private string felelosErkeztetoNeve;
        [JsonIgnore]
        [DisplayName("Felelős érkeztető")]
        [Description("Felelős érkeztető")]
        public string FelelosErkeztetoNeve
        {
            get { return felelosErkeztetoNeve; }
            set { felelosErkeztetoNeve = value; }
        }

        [JsonIgnore]
        [NonSerialized]
        private string felelosIktatoNeve;
        [JsonIgnore]
        [DisplayName("Felelős iktató")]
        [Description("Felelős iktató")]
        public string FelelosIktatoNeve
        {
            get { return felelosIktatoNeve; }
            set { felelosIktatoNeve = value; }
        }

        [JsonIgnore]
        [NonSerialized]
        private string rendszerfelugyeletiEmailCim;
        [JsonIgnore]
        [DisplayName("Rendszerfelugyeleti Email Cim")]
        [Description("Rendszerfelugyeleti Email Cim")]
        public string RendszerfelugyeletiEmailCim
        {
            get { return rendszerfelugyeletiEmailCim; }
            set { rendszerfelugyeletiEmailCim = value; }
        }
    }
}
