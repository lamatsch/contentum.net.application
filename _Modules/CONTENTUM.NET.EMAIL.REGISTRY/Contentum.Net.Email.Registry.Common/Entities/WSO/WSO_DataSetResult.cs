﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_DataSetResult
    {
        public string errorCode;

        public string errorMessage;

        public string errorType;

        //public ErrorDetails errorDetail;

        public string uid;

        public System.Data.DataSet ds;

        public object record;

        public bool isLogExist;

        public bool isUpdated;

        //public SerializableSqlCommand sqlCommand;
    }
}
