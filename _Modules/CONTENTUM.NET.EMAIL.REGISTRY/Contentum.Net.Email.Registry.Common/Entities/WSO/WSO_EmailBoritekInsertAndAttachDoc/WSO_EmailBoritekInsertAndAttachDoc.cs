﻿using System.Collections.Generic;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailBoritekInsertAndAttachDoc
    {
        public string KuldesMod { get; set; }
        public string EgyebparancsXml { get; set; }
        public WSO_EmailBoritek_ExecParam MyExecParam { get; set; }
        public WSO_EmailBoritekInsertAndAttachDoc_MailEnvelopes MyMailEnvelopes { get; set; }
        public List<WSO_EmailBoritekInsertAndAttachDoc_Attachment> MyAttachments { get; set; }

        public WSO_EmailBoritekInsertAndAttachDoc()
        {
            MyExecParam = new WSO_EmailBoritek_ExecParam();
            MyMailEnvelopes = new WSO_EmailBoritekInsertAndAttachDoc_MailEnvelopes();
            MyAttachments = new List<WSO_EmailBoritekInsertAndAttachDoc_Attachment>();
        }
    }
}
