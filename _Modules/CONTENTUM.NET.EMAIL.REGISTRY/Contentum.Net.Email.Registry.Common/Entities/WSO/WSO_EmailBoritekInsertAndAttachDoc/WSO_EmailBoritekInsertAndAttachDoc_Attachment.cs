﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailBoritekInsertAndAttachDoc_Attachment
    {
        public string NevField { get; set; }

        public byte[] TartalomField { get; set; }

        public string TartalomHashField { get; set; }

        public string SourceSharePathField { get; set; }

        public string MegnyithatoField { get; set; }

        public string OlvashatoField { get; set; }

        public string TitkositasField { get; set; }

        public string ElektronikusAlairasField { get; set; }

        public string BarCodeField { get; set; }

        //public object AlairasAdatokField { get; set; }

        //public object OCRAdatokField { get; set; }
    }
}
