﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailBoritekInsertAndAttachDoc_MailEnvelopes
    {
        //public object typedField { get; set; }

        //public object baseField { get; set; }

        //public object updatedField { get; set; }

        public string idField { get; set; }

        public string feladoField { get; set; }

        public string cimzettField { get; set; }

        public string ccField { get; set; }

        public string targyField { get; set; }

        public string feladasDatumaField { get; set; }

        public string erkezesDatumaField { get; set; }

        public string fontossagField { get; set; }

        public string kuldKuldemeny_IdField { get; set; }

        public string iraIrat_IdField { get; set; }

        public string digitalisAlairasField { get; set; }

        public string uzenetField { get; set; }

        public string emailForrasField { get; set; }

        public string emailGuidField { get; set; }

        public string feldolgozasIdoField { get; set; }

        public string forrasTipusField { get; set; }

        public string allapotField { get; set; }

        public string ervKezdField { get; set; }

        public string ervVegeField { get; set; }
    }
}
