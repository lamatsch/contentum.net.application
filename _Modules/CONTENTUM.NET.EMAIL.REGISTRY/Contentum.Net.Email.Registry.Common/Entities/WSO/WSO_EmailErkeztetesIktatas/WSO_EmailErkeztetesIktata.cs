﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas
    {
        public WSO_EmailErkeztetesIktatas_ExecParam MyEXEC_Param { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek MyEREC_KuldKuldemenyek { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_eMailBoritekok MyEREC_eMailBoritekok { get; set; }

        /// <summary>
        /// Alszámra iktatás esetén
        /// </summary>
        public string MyEREC_UygUgyiratId { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok MyEREC_UgyUgyiratok { get; set; }

        public WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratdarabok MyEREC_UgyUgyiratdarabok { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_HataridosFeladatok MyEREC_HataridosFeladatok { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_IraIratok MyEREC_IraIratok { get; set; }
        public WSO_EmailErkeztetesIktatas_IktatasiParameterek MyIktatasiParameterek { get; set; }
        public WSO_EmailErkeztetesIktatas_ErkeztetesParameterek MyErkeztetesParametereK { get; set; }

        public string MyEREC_IraIktatoKonyvek_Id { get; set; }

        public WSO_EmailErkeztetesIktatas()
        {
            MyEXEC_Param = new WSO_EmailErkeztetesIktatas_ExecParam();
            MyEREC_KuldKuldemenyek = new WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek();
            MyEREC_eMailBoritekok = new WSO_EmailErkeztetesIktatas_EREC_eMailBoritekok();
            MyEREC_UgyUgyiratok = new WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok();
            MyEREC_UgyUgyiratdarabok = new WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratdarabok();
            MyEREC_HataridosFeladatok = new WSO_EmailErkeztetesIktatas_EREC_HataridosFeladatok();
            MyEREC_IraIratok = new WSO_EmailErkeztetesIktatas_EREC_IraIratok();
            MyIktatasiParameterek = new WSO_EmailErkeztetesIktatas_IktatasiParameterek();
            MyErkeztetesParametereK = new WSO_EmailErkeztetesIktatas_ErkeztetesParameterek();
        }
    }
}
