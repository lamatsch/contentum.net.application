﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_BaseDocument
    {
        //private BaseDocumentTyped typedField;

        //private BaseDocumentUpdated updatedField;

        public string ver;

        public string note;

        public string stat_id;

        public string letrehozo_id;

        public string letrehozasIdo;

        public string modosito_id;

        public string modositasIdo;

        public string zarolo_id;

        public string zarolasIdo;

        public string tranz_id;

        public string uIAccessLog_id;

    }
}
