﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_EREC_HataridosFeladatok
    {
        //public BaseEREC_HataridosFeladatokBaseTyped typed;

        //public BaseDocument base;

        //public BaseEREC_HataridosFeladatokBaseUpdated updated;

        public string id { get; set; }

        public string esemeny_Id_Kivalto { get; set; }

        public string esemeny_Id_Lezaro { get; set; }

        public string funkcio_Id_Inditando { get; set; }

        public string csoport_Id_Felelos { get; set; }

        public string felhasznalo_Id_Felelos { get; set; }

        public string csoport_Id_Kiado { get; set; }

        public string felhasznalo_Id_Kiado { get; set; }

        public string hataridosFeladat_Id { get; set; }

        public string reszFeladatSorszam { get; set; }

        public string feladatDefinicio_Id { get; set; }

        public string feladatDefinicio_Id_Lezaro { get; set; }

        public string forras { get; set; }

        public string tipus { get; set; }

        public string altipus { get; set; }

        public string memo { get; set; }

        public string leiras { get; set; }

        public string prioritas { get; set; }

        public string lezarasPrioritas { get; set; }

        public string kezdesiIdo { get; set; }

        public string intezkHatarido { get; set; }

        public string lezarasDatuma { get; set; }

        public string azonosito { get; set; }

        public string obj_Id { get; set; }

        public string objTip_Id { get; set; }

        public string obj_type { get; set; }

        public string allapot { get; set; }

        public string megoldas { get; set; }

        public string ervKezd { get; set; }

        public string ervVege { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_HataridosFeladatok()
        {

        }
    }
}
