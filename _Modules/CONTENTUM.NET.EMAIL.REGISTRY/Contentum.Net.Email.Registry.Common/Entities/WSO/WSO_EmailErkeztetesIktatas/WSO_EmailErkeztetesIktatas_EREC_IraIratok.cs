﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_EREC_IraIratok
    {
        //public BaseEREC_IraIratokBaseTyped typed;

        //public BaseDocument base;

        //public BaseEREC_IraIratokBaseUpdated updated;

        public string Id { get; set; }

        public string PostazasIranya { get; set; }

        public string Alszam { get; set; }

        public string Sorszam { get; set; }

        public string UtolsoSorszam { get; set; }

        public string UgyUgyIratDarab_Id { get; set; }

        public string Kategoria { get; set; }

        public string HivatkozasiSzam { get; set; }

        public string IktatasDatuma { get; set; }

        public string ExpedialasDatuma { get; set; }

        public string ExpedialasModja { get; set; }

        public string FelhasznaloCsoport_Id_Expedial { get; set; }

        public string Targy { get; set; }

        public string Jelleg { get; set; }

        public string SztornirozasDat { get; set; }

        public string FelhasznaloCsoport_Id_Iktato { get; set; }

        public string FelhasznaloCsoport_Id_Kiadmany { get; set; }

        public string UgyintezesAlapja { get; set; }

        public string AdathordozoTipusa { get; set; }

        public string Csoport_Id_Felelos { get; set; }

        public string Csoport_Id_Ugyfelelos { get; set; }

        public string FelhasznaloCsoport_Id_Orzo { get; set; }

        public string KiadmanyozniKell { get; set; }

        public string FelhasznaloCsoport_Id_Ugyintez { get; set; }

        public string Hatarido { get; set; }

        public string MegorzesiIdo { get; set; }

        public string IratFajta { get; set; }

        public string Irattipus { get; set; }

        public string KuldKuldemenyek_Id { get; set; }

        public string Allapot { get; set; }

        public string Azonosito { get; set; }

        public string GeneraltTargy { get; set; }

        public string IratMetaDef_Id { get; set; }

        public string IrattarbaKuldDatuma { get; set; }

        public string IrattarbaVetelDat { get; set; }

        public string Ugyirat_Id { get; set; }

        public string Minosites { get; set; }

        public string ErvKezd { get; set; }

        public string ErvVege { get; set; }

        public string Munkaallomas { get; set; }

        public string UgyintezesModja { get; set; }

        public string IntezesIdopontja { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_IraIratok()
        {

        }
    }
}
