﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek
    {
        //private BaseEREC_KuldKuldemenyekBaseTyped typed;

        //private BaseDocument base;

        //private BaseEREC_KuldKuldemenyekBaseUpdated updated;

        public string Id { get; set; }

        public string Allapot { get; set; }

        public string BeerkezesIdeje { get; set; }

        public string FelbontasDatuma { get; set; }

        public string IraIktatokonyv_Id { get; set; }

        public string KuldesMod { get; set; }

        public string Erkezteto_Szam { get; set; }

        public string HivatkozasiSzam { get; set; }

        public string Targy { get; set; }

        public string Tartalom { get; set; }

        public string RagSzam { get; set; }

        public string Surgosseg { get; set; }

        public string BelyegzoDatuma { get; set; }

        public string UgyintezesModja { get; set; }

        public string PostazasIranya { get; set; }

        public string Tovabbito { get; set; }

        public string PeldanySzam { get; set; }

        public string IktatniKell { get; set; }

        public string Iktathato { get; set; }

        public string SztornirozasDat { get; set; }

        public string KuldKuldemeny_Id_Szulo { get; set; }

        public string Erkeztetes_Ev { get; set; }

        public string Csoport_Id_Cimzett { get; set; }

        public string Csoport_Id_Felelos { get; set; }

        public string FelhasznaloCsoport_Id_Expedial { get; set; }

        public string ExpedialasIdeje { get; set; }

        public string FelhasznaloCsoport_Id_Orzo { get; set; }

        public string FelhasznaloCsoport_Id_Atvevo { get; set; }

        public string Partner_Id_Bekuldo { get; set; }

        public string Cim_Id { get; set; }

        public string CimSTR_Bekuldo { get; set; }

        public string NevSTR_Bekuldo { get; set; }

        public string AdathordozoTipusa { get; set; }

        public string ElsodlegesAdathordozoTipusa { get; set; }

        public string FelhasznaloCsoport_Id_Alairo { get; set; }

        public string BarCode { get; set; }

        public string FelhasznaloCsoport_Id_Bonto { get; set; }

        public string CsoportFelelosEloszto_Id { get; set; }

        public string Csoport_Id_Felelos_Elozo { get; set; }

        public string Kovetkezo_Felelos_Id { get; set; }

        public string Elektronikus_Kezbesitesi_Allap { get; set; }

        public string Kovetkezo_Orzo_Id { get; set; }

        public string Fizikai_Kezbesitesi_Allapot { get; set; }

        public string IraIratok_Id { get; set; }

        public string BontasiMegjegyzes { get; set; }

        public string Tipus { get; set; }

        public string Minosites { get; set; }

        public string MegtagadasIndoka { get; set; }

        public string Megtagado_Id { get; set; }

        public string MegtagadasDat { get; set; }

        public string KimenoKuldemenyFajta { get; set; }

        public string Elsobbsegi { get; set; }

        public string Ajanlott { get; set; }

        public string Tertiveveny { get; set; }

        public string SajatKezbe { get; set; }

        public string E_ertesites { get; set; }

        public string E_elorejelzes { get; set; }

        public string PostaiLezaroSzolgalat { get; set; }

        public string Ar { get; set; }

        public string KimenoKuld_Sorszam { get; set; }

        public string TovabbitasAlattAllapot { get; set; }

        public string Azonosito { get; set; }

        public string BoritoTipus { get; set; }

        public string MegorzesJelzo { get; set; }

        public string ErvKezd { get; set; }

        public string ErvVege { get; set; }

        public string IktatastNemIgenyel { get; set; }

        public string KezbesitesModja { get; set; }

        public string Munkaallomas { get; set; }

        public string SerultKuldemeny { get; set; }

        public string TevesCimzes { get; set; }

        public string TevesErkeztetes { get; set; }

        public string CimzesTipusa { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek()
        {

        }
    }
}