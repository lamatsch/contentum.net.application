﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{

    public class WSO_EmailErkeztetesIktatas_EREC_Mellekletek
    {
        //public BaseEREC_MellekletekBaseTyped typed;

        //public BaseDocument base;

        //public BaseEREC_MellekletekBaseUpdated updated;

        public string id { get; set; }

        public string kuldKuldemeny_Id { get; set; }

        public string iraIrat_Id { get; set; }

        public string adathordozoTipus { get; set; }

        public string megjegyzes { get; set; }

        public string sztornirozasDat { get; set; }

        public string mennyisegiEgyseg { get; set; }

        public string mennyiseg { get; set; }

        public string barCode { get; set; }

        public string ervKezd { get; set; }

        public string ervVege { get; set; }

        public string mellekletAdathordozoTipus { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_Mellekletek()
        {

        }
    }
}
