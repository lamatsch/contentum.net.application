﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratdarabok
    {
        //public BaseEREC_UgyUgyiratdarabokBaseTyped typedField;

        //public BaseDocument baseField;

        //public BaseEREC_UgyUgyiratdarabokBaseUpdated updatedField;

        public string idField { get; set; }

        public string ugyUgyirat_IdField { get; set; }

        public string eljarasiSzakaszField { get; set; }

        public string sorszamField { get; set; }

        public string azonositoField { get; set; }

        public string hataridoField { get; set; }

        public string felhasznaloCsoport_Id_UgyintezField { get; set; }

        public string elintezesDatField { get; set; }

        public string lezarasDatField { get; set; }

        public string elintezesModField { get; set; }

        public string lezarasOkaField { get; set; }

        public string leirasField { get; set; }

        public string ugyUgyirat_Id_ElozoField { get; set; }

        public string iraIktatokonyv_IdField { get; set; }

        public string foszamField { get; set; }

        public string utolsoAlszamField { get; set; }

        public string csoport_Id_FelelosField { get; set; }

        public string csoport_Id_Felelos_ElozoField { get; set; }

        public string allapotField { get; set; }

        public string ervKezdField { get; set; }

        public string ervVegeField { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratdarabok()
        {

        }
    }
}
