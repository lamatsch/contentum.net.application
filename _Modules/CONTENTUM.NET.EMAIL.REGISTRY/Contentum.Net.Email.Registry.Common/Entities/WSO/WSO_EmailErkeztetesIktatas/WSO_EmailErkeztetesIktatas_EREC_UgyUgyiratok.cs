﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok
    {
        //      public BaseEREC_UgyUgyiratokBaseTyped typed {get;set;}

        //public BaseDocument base {get;set;}

        //public BaseEREC_UgyUgyiratokBaseUpdated updated {get;set;}

        public string id { get; set; }

        public string foszam { get; set; }

        public string sorszam { get; set; }

        public string ugyazonosito { get; set; }

        public string alkalmazas_Id { get; set; }

        public string ugyintezesModja { get; set; }

        public string hatarido { get; set; }

        public string skontrobaDat { get; set; }

        public string lezarasDat { get; set; }

        public string irattarbaKuldDatuma { get; set; }

        public string irattarbaVetelDat { get; set; }

        public string felhCsoport_Id_IrattariAtvevo { get; set; }

        public string selejtezesDat { get; set; }

        public string felhCsoport_Id_Selejtezo { get; set; }

        public string leveltariAtvevoNeve { get; set; }

        public string felhCsoport_Id_Felulvizsgalo { get; set; }

        public string felulvizsgalatDat { get; set; }

        public string iktatoszamKieg { get; set; }

        public string targy { get; set; }

        public string ugyUgyirat_Id_Szulo { get; set; }

        public string ugyUgyirat_Id_Kulso { get; set; }

        public string ugyTipus { get; set; }

        public string irattariHely { get; set; }

        public string sztornirozasDat { get; set; }

        public string csoport_Id_Felelos { get; set; }

        public string felhasznaloCsoport_Id_Orzo { get; set; }

        public string csoport_Id_Cimzett { get; set; }

        public string jelleg { get; set; }

        public string iraIrattariTetel_Id { get; set; }

        public string iraIktatokonyv_Id { get; set; }

        public string skontroOka { get; set; }

        public string skontroVege { get; set; }

        public string surgosseg { get; set; }

        public string skontrobanOsszesen { get; set; }

        public string megorzesiIdoVege { get; set; }

        public string partner_Id_Ugyindito { get; set; }

        public string nevSTR_Ugyindito { get; set; }

        public string elintezesDat { get; set; }

        public string felhasznaloCsoport_Id_Ugyintez { get; set; }

        public string csoport_Id_Felelos_Elozo { get; set; }

        public string kolcsonKikerDat { get; set; }

        public string kolcsonKiadDat { get; set; }

        public string kolcsonhatarido { get; set; }

        public string bARCODE { get; set; }

        public string iratMetadefinicio_Id { get; set; }

        public string allapot { get; set; }

        public string tovabbitasAlattAllapot { get; set; }

        public string megjegyzes { get; set; }

        public string azonosito { get; set; }

        public string fizikai_Kezbesitesi_Allapot { get; set; }

        public string kovetkezo_Orzo_Id { get; set; }

        public string csoport_Id_Ugyfelelos { get; set; }

        public string elektronikus_Kezbesitesi_Allap { get; set; }

        public string kovetkezo_Felelos_Id { get; set; }

        public string utolsoAlszam { get; set; }

        public string utolsoSorszam { get; set; }

        public string iratSzam { get; set; }

        public string elintezesMod { get; set; }

        public string regirendszerIktatoszam { get; set; }

        public string generaltTargy { get; set; }

        public string cim_Id_Ugyindito { get; set; }

        public string cimSTR_Ugyindito { get; set; }

        public string ervKezd { get; set; }

        public string ervVege { get; set; }

        public string ujOrzesiIdo { get; set; }
        public WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok()
        {

        }
    }
}
