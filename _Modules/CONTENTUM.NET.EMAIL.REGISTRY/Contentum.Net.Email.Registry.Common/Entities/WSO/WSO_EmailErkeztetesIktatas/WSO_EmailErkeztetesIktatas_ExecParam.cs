﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_ExecParam
    {
        //private BaseExecParamTyped typedField;

        public bool FakeField { get; set; }

        public string Alkalmazas_IdField { get; set; }

        public string Felhasznalo_IdField { get; set; }

        public string LoginUser_IdField { get; set; }

        public string Helyettesites_IdField { get; set; }

        public string FelhasznaloSzervezet_IdField { get; set; }

        public string CsoportTag_IdField { get; set; }

        public string UserHostAddressField { get; set; }

        public string Record_IdField { get; set; }

        public string UIAccessLog_IdField { get; set; }

        public string Page_IdField { get; set; }

        public System.Data.DataSet CallingChainField { get; set; }

        //public Paging pagingField;

        public string Rrg_IdField { get; set; }

        public string funkcioKodField { get; set; }
        public WSO_EmailErkeztetesIktatas_ExecParam()
        {

        }
    }
}
