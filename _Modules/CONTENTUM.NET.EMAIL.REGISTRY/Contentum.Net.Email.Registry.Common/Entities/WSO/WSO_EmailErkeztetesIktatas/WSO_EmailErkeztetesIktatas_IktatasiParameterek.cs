﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailErkeztetesIktatas_IktatasiParameterek
    {
        public string Iratpeldany_Vonalkod { get; set; }

        public bool IratpeldanyVonalkodGeneralasHaNincs { get; set; }

        public string RegiAdatAzonosito { get; set; }

        public string RegiAdatId { get; set; }

        public string SzerelendoUgyiratId { get; set; }

        public string Adoszam { get; set; }

        public string UgykorId { get; set; }

        public string Ugytipus { get; set; }

        public string KuldemenyId { get; set; }

        public bool UgyiratUjranyitasaHaLezart { get; set; }

        public bool UgyiratPeldanySzukseges { get; set; }

        public bool KeszitoPeldanyaSzukseges { get; set; }

        public bool Atiktatas { get; set; }

        public string IratId { get; set; }

        public bool EmptyUgyiratSztorno { get; set; }

        public string UgyiratUjHatarido { get; set; }

        public bool MunkaPeldany { get; set; }

        //public Csatolmany dokumentum;
        public WSO_EmailErkeztetesIktatas_IktatasiParameterek()
        {

        }
    }
}
