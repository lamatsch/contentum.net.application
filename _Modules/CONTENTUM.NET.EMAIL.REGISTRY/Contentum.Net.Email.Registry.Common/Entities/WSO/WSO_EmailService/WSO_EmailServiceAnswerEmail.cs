﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailServiceAnswerEmail
    {
        public string Kuldemeny_id { get; set; }
        public string Ugyirat_id { get; set; }
        public WSO_EmailService_ExecParam MyExecParam { get; set; }

        public WSO_EmailServiceAnswerEmail()
        {
            MyExecParam = new WSO_EmailService_ExecParam();
        }
    }
}
