﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailService_ExecParam
    {
        //private BaseExecParamTyped typedField;

        public bool fakeField { get; set; }

        public string alkalmazas_IdField { get; set; }

        public string felhasznalo_IdField { get; set; }

        public string loginUser_IdField { get; set; }

        public string helyettesites_IdField { get; set; }

        public string felhasznaloSzervezet_IdField { get; set; }

        public string csoportTag_IdField { get; set; }

        public string userHostAddressField { get; set; }

        public string record_IdField { get; set; }

        public string uIAccessLog_IdField { get; set; }

        public string page_IdField { get; set; }

        public System.Data.DataSet callingChainField { get; set; }

        //public Paging pagingField{get;set;}

        public string org_IdField { get; set; }

        public string funkcioKodField { get; set; }
        public WSO_EmailService_ExecParam()
        {

        }
    }
}
