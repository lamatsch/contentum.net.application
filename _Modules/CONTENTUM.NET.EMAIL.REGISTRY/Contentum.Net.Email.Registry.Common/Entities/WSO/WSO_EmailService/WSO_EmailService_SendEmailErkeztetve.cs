﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_EmailService_SendEmailErkeztetve
    {
        public WSO_EmailService_ExecParam MyExecParam { get; set; }
        public WSO_EmailService_EREC_eMailBoritekok MyEmailBoritek { get; set; }

        public string MyErkeztetoSzam { get; set; }
        public WSO_EmailService_SendEmailErkeztetve()
        {
            MyExecParam = new WSO_EmailService_ExecParam();
            MyEmailBoritek = new WSO_EmailService_EREC_eMailBoritekok();
        }
    }
}
