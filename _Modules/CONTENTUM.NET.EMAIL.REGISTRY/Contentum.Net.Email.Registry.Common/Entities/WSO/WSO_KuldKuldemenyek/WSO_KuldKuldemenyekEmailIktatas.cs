﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_KuldKuldemenyekEmailIktatas
    {
        public WSO_KuldKuldemenyek_ExecParam MyEXEC_Param { get; set; }
        public WSO_KuldKuldemenyek_EREC_KuldKuldemenyek MyEREC_KuldKuldemenyek { get; set; }
        public WSO_KuldKuldemenyek_EREC_eMailBoritekok MyEREC_eMailBoritekok { get; set; }
        public WSO_KuldKuldemenyek_ErkeztetesParameterek MyEREC_ErkeztetesParameterek { get; set; }
        public WSO_KuldKuldemenyek_EREC_HataridosFeladatok MyEREC_HataridosFeladato { get; set; }

        public WSO_KuldKuldemenyekEmailIktatas()
        {
            MyEXEC_Param = new WSO_KuldKuldemenyek_ExecParam();
            MyEREC_KuldKuldemenyek = new WSO_KuldKuldemenyek_EREC_KuldKuldemenyek();
            MyEREC_eMailBoritekok = new WSO_KuldKuldemenyek_EREC_eMailBoritekok();
            MyEREC_ErkeztetesParameterek = new WSO_KuldKuldemenyek_ErkeztetesParameterek();
            MyEREC_HataridosFeladato = new WSO_KuldKuldemenyek_EREC_HataridosFeladatok();
        }
    }
}
