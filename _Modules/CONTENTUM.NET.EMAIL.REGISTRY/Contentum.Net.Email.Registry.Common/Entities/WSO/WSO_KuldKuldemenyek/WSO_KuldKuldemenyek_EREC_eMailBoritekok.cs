﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO
{
    public class WSO_KuldKuldemenyek_EREC_eMailBoritekok
    {
        //private BaseEREC_eMailBoritekokBaseTyped typedField;

        public WSO_KuldKuldemenyek_BaseDocument BaseDocument;

        //private BaseEREC_eMailBoritekokBaseUpdated updatedField;

        public string IdField { get; set; }

        public string FeladoField { get; set; }

        public string CimzettField { get; set; }

        public string CcField { get; set; }

        public string TargyField { get; set; }

        public string FeladasDatumaField { get; set; }

        public string ErkezesDatumaField { get; set; }

        public string FontossagField { get; set; }

        public string KuldKuldemeny_IdField { get; set; }

        public string IraIrat_IdField { get; set; }

        public string DigitalisAlairasField { get; set; }

        public string UzenetField { get; set; }

        public string EmailForrasField { get; set; }

        public string EmailGuidField { get; set; }

        public string FeldolgozasIdoField { get; set; }

        public string ForrasTipusField { get; set; }

        public string AllapotField { get; set; }

        public string ErvKezdField { get; set; }

        public string ErvVegeField { get; set; }
        public WSO_KuldKuldemenyek_EREC_eMailBoritekok()
        {
            BaseDocument = new WSO_KuldKuldemenyek_BaseDocument();
        }
    }
}
