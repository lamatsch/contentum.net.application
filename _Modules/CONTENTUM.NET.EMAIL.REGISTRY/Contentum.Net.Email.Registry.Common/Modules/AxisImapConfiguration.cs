﻿namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common
{
    /// <summary>
    /// Imap server configuration
    /// </summary>
    public class AxisImapConfiguration
    {
        /// <summary>
        /// Imap server address (etc: imap.gmail.com)
        /// </summary>
        public string ImapServerAddress { get; set; }
        /// <summary>
        /// Imap server port number (etc: 993)
        /// </summary>
        public int ImapServerPort { get; set; }
        /// <summary>
        /// Use ssl on connection
        /// </summary>
        public bool UseSSL { get; set; }

        /// <summary>
        /// Login imap user
        /// </summary>
        public string ImapLoginUser { get; set; }
        /// <summary>
        /// Login imap user password
        /// </summary>
        public string ImapLoginPassword { get; set; }

        public AxisImapConfiguration()
        {
            UseSSL = true;
        }
    }
}
