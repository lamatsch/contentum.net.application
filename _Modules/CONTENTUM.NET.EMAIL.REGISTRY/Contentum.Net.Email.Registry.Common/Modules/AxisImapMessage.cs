﻿using MimeKit;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common
{
    public class AxisImapMessage
    {
        public uint Uid { get; private set; }
        public MimeMessage Message { get; private set; }

        public AxisImapMessage(uint id, MimeMessage message)
        {
            this.Uid = id;
            this.Message = message;
        }
    }
}
