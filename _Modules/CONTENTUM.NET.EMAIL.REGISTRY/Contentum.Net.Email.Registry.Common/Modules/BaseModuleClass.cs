﻿using NLog;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Common
{
    public class BaseModuleClass
    {
        public Logger MyLogger { get; set; }
    }
}
