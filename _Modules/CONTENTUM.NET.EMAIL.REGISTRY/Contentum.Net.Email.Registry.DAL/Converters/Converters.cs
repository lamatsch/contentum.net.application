﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using System.Collections.Generic;
using System.Linq;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.DAL
{
    public static class Converters
    {
        public static List<INT_ManagedEmailAddress> ConvertToINT_ManagedEmailAddress(List<sp_INT_ManagedEmailAddressGetAll_Result> items)
        {
            if (items == null || items.Count < 1)
                return null;
            List<INT_ManagedEmailAddress> result = new List<INT_ManagedEmailAddress>();
            INT_ManagedEmailAddress temp;
            foreach (var item in items)
            {
                temp = new INT_ManagedEmailAddress();
                temp.Ertek = item.Ertek;
                temp.ErvKezd = item.ErvKezd;
                temp.ErvVege = item.ErvVege;
                temp.Felhasznalo_id = item.Felhasznalo_id;
                temp.Id = item.Id;
                temp.Karbantarthato = item.Karbantarthato;
                temp.LetrehozasIdo = item.LetrehozasIdo;
                temp.Letrehozo_id = item.Letrehozo_id;
                temp.ModositasIdo = item.ModositasIdo;
                temp.Modosito_id = item.Modosito_id;
                temp.Nev = item.Nev;
                temp.Note = item.Note;
                temp.Org = item.Org;
                temp.Stat_id = item.Stat_id;
                temp.Tranz_id = item.Tranz_id;
                temp.UIAccessLog_id = item.UIAccessLog_id;
                temp.Ver = item.Ver;
                temp.ZarolasIdo = item.ZarolasIdo;
                temp.Zarolo_id = item.Zarolo_id;

                result.Add(temp);
            }
            return result;
        }

        public static List<DBO_ManagedEmailParameters> ConvertToCommonManagedEmailParameters(List<INT_ManagedEmailAddress> items)
        {
            if (items == null || items.Count < 1)
                return null;
            List<DBO_ManagedEmailParameters> result = new List<DBO_ManagedEmailParameters>();
            DBO_ManagedEmailParameters temp;
            foreach (var item in items.Where(i => i.Ertek != null))
            {
                temp = JSONFunctions.DeSerializeManagedEmailParameters(item.Ertek);
                temp.EmailAddress = item.Nev;

                result.Add(temp);
            }

            return result;
        }
    }
}