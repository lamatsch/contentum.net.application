﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.DAL
{
    public static partial class DataBaseProvider
    {
        [Obsolete]
        public static List<INT_ManagedEmailAddress> GetTableAllManagedEmailAddress()
        {
            List<INT_ManagedEmailAddress> temp = null;
            using (EDOKEntities context = new EDOKEntities(true))
            {
                temp = context.INT_ManagedEmailAddress
                    .Where(m => m.ErvVege >= DateTime.Now)
                    .ToList();
            }

            return temp;
        }

        public static int AddManagedEmailAddress(Guid executionUserId, string name, string value, string note)
        {
            int result;
            using (EDOKEntities context = new EDOKEntities(true))
            {
                result = context.sp_INT_ManagedEmailAddressInsert(
                    id: null,
                    org: null,
                    felhasznalo_id: executionUserId,
                    nev: name,
                    ertek: value,
                    karbantarthato: "1",
                    ver: null,
                    note: note,
                    stat_id: null,
                    ervKezd: null,
                    ervVege: null,
                    letrehozo_id: executionUserId,
                    letrehozasIdo: null,
                    modosito_id: null,
                    modositasIdo: null,
                    zarolo_id: null,
                    zarolasIdo: null,
                    tranz_id: null,
                    uIAccessLog_id: null,
                    updatedColumns: null,
                    resultUid: new System.Data.Entity.Core.Objects.ObjectParameter("ResultUid", Guid.Empty));
            }
            return result;
        }
    }
}
