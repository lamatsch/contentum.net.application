﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using System;
using System.Data.Entity;
using System.IO;
using System.Reflection;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.DAL
{
    public partial class EDOKEntities : DbContext
    {
        public EDOKEntities(bool useDynamicPath)
          : base("name=EDOKEntities")
        {
            this.Database.Connection.ConnectionString =
                string.Format(this.Database.Connection.ConnectionString, AssemblyDirectory);
        }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return CommonFunctions.GetShortPathName(Path.GetDirectoryName(path));
            }
        }
    }
}
