//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class INT_ManagedEmailAddress
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INT_ManagedEmailAddress()
        {
            this.INT_ManagedEmailAddress1 = new HashSet<INT_ManagedEmailAddress>();
        }
    
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> Org { get; set; }
        public Nullable<System.Guid> Felhasznalo_id { get; set; }
        public string Nev { get; set; }
        public string Ertek { get; set; }
        public string Karbantarthato { get; set; }
        public Nullable<int> Ver { get; set; }
        public string Note { get; set; }
        public Nullable<System.Guid> Stat_id { get; set; }
        public Nullable<System.DateTime> ErvKezd { get; set; }
        public Nullable<System.DateTime> ErvVege { get; set; }
        public Nullable<System.Guid> Letrehozo_id { get; set; }
        public System.DateTime LetrehozasIdo { get; set; }
        public Nullable<System.Guid> Modosito_id { get; set; }
        public Nullable<System.DateTime> ModositasIdo { get; set; }
        public Nullable<System.Guid> Zarolo_id { get; set; }
        public Nullable<System.DateTime> ZarolasIdo { get; set; }
        public Nullable<System.Guid> Tranz_id { get; set; }
        public Nullable<System.Guid> UIAccessLog_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INT_ManagedEmailAddress> INT_ManagedEmailAddress1 { get; set; }
        public virtual INT_ManagedEmailAddress INT_ManagedEmailAddress2 { get; set; }
    }
}
