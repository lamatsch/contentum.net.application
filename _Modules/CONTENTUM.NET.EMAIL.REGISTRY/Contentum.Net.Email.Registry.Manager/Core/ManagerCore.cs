﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.ModuleDownloader;
using MimeKit;
using NLog;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Manager
{
    /// <summary>
    /// Manager core class
    /// </summary>
    internal class ManagerCore
    {
        #region PROPERTIES
        private Logger MyLogger { get; set; }
        private ModuleEmailDownloader MyDownloader { get; set; }
        private ModuleEmailLoader MyLoader { get; set; }
        private DBO_ManagedEmailParameters MyEmailParameters { get; set; }
        private ModuleWebService MyModuleWebService { get; set; }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="inParameters"></param>
        /// <param name="inLogger"></param>
        public ManagerCore(DBO_ManagedEmailParameters inParameters, Logger inLogger)
        {
            MyLogger = inLogger;
            MyEmailParameters = inParameters;
            MyDownloader = new ModuleEmailDownloader(MyEmailParameters, MyLogger);
            MyLoader = new ModuleEmailLoader(MyEmailParameters.EmailAddress, MyLogger);
            MyModuleWebService = new ModuleWebService();

            LoadActiveIktatoKonyvek();
        }
        private void LoadActiveIktatoKonyvek()
        {
            #region LOAD IKTATOKONYVEK
            Tuple<WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult> result = MyModuleWebService.LoadParameters(MyEmailParameters);
            bool onlyErkeztet = MyEmailParameters.DokumentumFeldolgozasTipusa == (ushort)AxisConstants.EnumDocumentTaskType.Erkeztet;
            MyLogger.Info("DokumentumFeldolgozasTipusa: " + MyEmailParameters.DokumentumFeldolgozasTipusa);
            #region ERROR CHECK
            if (result.Item1 == null)
            {
                MyLogger.Error("Érkeztetőkönvy lekérdezés hiba");
                throw new ArgumentNullException("Érkeztetőkönvy lekérdezés hiba");
            }
            if (result.Item2 == null && !onlyErkeztet)
            {
                MyLogger.Error("Iktatókönyv lekérdezés hiba");
                throw new ArgumentNullException("Iktatókönyv lekérdezés hiba");
            }
            if (result.Item3 == null && !onlyErkeztet)
            {
                MyLogger.Error("Irattári tétel lekérdezés hiba");
                throw new ArgumentNullException("Irattári tétel lekérdezés hiba");
            }
            if (result.Item4 == null)
            {
                MyLogger.Error("Felelős érkeztető lekérdezés hiba");
                throw new ArgumentNullException("Felelős érkeztető lekérdezés hiba");
            }
            if (result.Item5 == null && !onlyErkeztet && !onlyErkeztet)
            {
                MyLogger.Error("Felelős iktató lekérdezés hiba");
                throw new ArgumentNullException("Felelős iktató lekérdezés hiba");
            }
            if (result.Item6 == null)
            {
                MyLogger.Error("RENDSZERFELUGYELET_EMAIL_CIM lekérdezés hiba");
                throw new ArgumentNullException("RENDSZERFELUGYELET_EMAIL_CIM lekérdezés hiba");
            }


            if (!string.IsNullOrEmpty(result.Item1.errorCode))
            {
                string msg = "Érkeztetőkönvy lekérdezés hiba:" + result.Item2.errorCode + " " + result.Item2.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!string.IsNullOrEmpty(result.Item2.errorCode) && !onlyErkeztet)
            {
                string msg = "Iktatókönyv lekérdezés hiba:" + result.Item2.errorCode + " " + result.Item2.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!string.IsNullOrEmpty(result.Item3.errorCode) && !onlyErkeztet)
            {
                string msg = "Irattári tétel lekérdezés hiba:" + result.Item3.errorCode + " " + result.Item3.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!string.IsNullOrEmpty(result.Item4.errorCode))
            {
                string msg = "Felelős érkeztető lekérdezés hiba:" + result.Item4.errorCode + " " + result.Item4.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!string.IsNullOrEmpty(result.Item5.errorCode) && !onlyErkeztet)
            {
                string msg = "Felelős iktató lekérdezés hiba:" + result.Item5.errorCode + " " + result.Item5.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!string.IsNullOrEmpty(result.Item6.errorCode))
            {
                string msg = "RENDSZERFELUGYELET_EMAIL_CIM lekérdezés hiba:" + result.Item6.errorCode + " " + result.Item6.errorMessage;
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }


            if (result.Item1.ds == null || result.Item1.ds.Tables.Count < 1 || result.Item1.ds.Tables[0].Rows.Count != 1)
            {
                string msg = "Érkeztetőkönvy lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!onlyErkeztet && (result.Item2.ds == null || result.Item2.ds.Tables.Count < 1 || result.Item2.ds.Tables[0].Rows.Count != 1))
            {
                string msg = "Iktatókönyv lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!onlyErkeztet && (result.Item3.ds == null || result.Item3.ds.Tables.Count < 1 || result.Item3.ds.Tables[0].Rows.Count != 1))
            {
                string msg = "Irattári tétel lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (result.Item4.ds == null || result.Item4.ds.Tables.Count < 1 || result.Item4.ds.Tables[0].Rows.Count != 1)
            {
                string msg = "Felelős érkeztető lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (!onlyErkeztet && (result.Item5.ds == null || result.Item5.ds.Tables.Count < 1 || result.Item5.ds.Tables[0].Rows.Count != 1))
            {
                string msg = "Felelős iktató lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            if (result.Item6.ds == null || result.Item6.ds.Tables.Count < 1 || result.Item6.ds.Tables[0].Rows.Count != 1)
            {
                string msg = "RENDSZERFELUGYELET_EMAIL_CIM lekérdezés hiba: Nem megfelelő a keresésnek eredménye";
                MyLogger.Error(msg);
                throw new ArgumentNullException(msg);
            }
            #endregion
            if (!onlyErkeztet)
            {
                MyEmailParameters.IraIktatoKonyvId = result.Item2.ds.Tables[0].Rows[0]["Id"].ToString();
                MyEmailParameters.UgyUgyiratok_iraIrattariTetel_Id = result.Item3.ds.Tables[0].Rows[0]["Id"].ToString();
                MyEmailParameters.FelelosIktatoNeve =
              string.Format("{0} ({1})", result.Item5.ds.Tables[0].Rows[0]["Nev"].ToString(), result.Item5.ds.Tables[0].Rows[0]["ErtesitesEmail"].ToString());

            }
            MyEmailParameters.IraErkeztetoKonyvId = result.Item1.ds.Tables[0].Rows[0]["Id"].ToString();
            MyEmailParameters.FelelosErkeztetoNeve = result.Item4.ds.Tables[0].Rows[0]["Nev"].ToString();   // string.Format("{0} ({1})", result.Item4.ds.Tables[0].Rows[0]["Nev"].ToString(), result.Item4.ds.Tables[0].Rows[0]["EMail"].ToString());
            MyEmailParameters.RendszerfelugyeletiEmailCim = result.Item6.ds.Tables[0].Rows[0]["Ertek"].ToString();
            #endregion
        }

        /// <summary>
        /// Start Core procedure flow
        /// </summary>
        /// <returns></returns>
        public bool StartProcedure()
        {
            MyLogger.Debug("Start Core: " + MyEmailParameters.EmailAddress);
            try
            {
                MyDownloader.StartDownload();

                MyLoader.StartLoader();
                if (MyLoader.MailMessages != null && MyLoader.MailMessages.Count > 0)
                {
                    MyLogger.Debug("Új emailek száma: " + MyLoader.MailMessages.Count);
                    foreach (var message in MyLoader.MailMessages)
                    {
                        try
                        {
                            #region WS

                            #region CSATOLÁS
                            MyLogger.Debug("Start InsertAndAttachDocument: " + message.Key + AxisConstants.PendingEmailFileExtension);
                            WSO_BaseResult emailBoritekResult = MyModuleWebService.Email_InsertAndAttachDocument(message.Value, MyEmailParameters);
                            if (emailBoritekResult == null || string.IsNullOrEmpty(emailBoritekResult.uid))
                            {
                                string msg = string.Format("Nem sikerült a EmailBoritek létrehozás. Feldolgozás vége!  Email={0} Hiba={1} ", message.Value.From + " " + message.Value.Subject, emailBoritekResult.errorCode + " " + emailBoritekResult.errorMessage);
                                MyLogger.Error(msg);
                                MyModuleWebService.SendEmail_Error(message.Value, MyEmailParameters, MyEmailParameters.RendszerfelugyeletiEmailCim, null, emailBoritekResult.errorCode, emailBoritekResult.errorMessage, null, msg);
                                continue;
                            }
                            MyLogger.Debug("Email_InsertAndAttachDocument ID:  " + emailBoritekResult.uid);
                            MyLogger.Debug("Stop InsertAndAttachDocument: " + message.Key + AxisConstants.PendingEmailFileExtension);
                            #endregion

                            switch (MyEmailParameters.DokumentumFeldolgozasTipusa)
                            {
                                case (ushort)AxisConstants.EnumDocumentTaskType.Erkeztet:
                                    MyLogger.Debug("Start Email_Erkeztetes: " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    WSO_BaseResult erkeztetResult = MyModuleWebService.Email_Erkeztetes(message.Value, emailBoritekResult.uid, MyEmailParameters);
                                    if (erkeztetResult == null || string.IsNullOrEmpty(erkeztetResult.uid))
                                    {
                                        string msg = string.Format("Nem sikerült az email Érkeztetés. Feldolgozás vége!  Email={0} Hiba={1} ", message.Value.From + " " + message.Value.Subject, erkeztetResult == null ? string.Empty : erkeztetResult.errorCode + " " + erkeztetResult.errorMessage);
                                        MyLogger.Error(msg);
                                        Console.WriteLine(msg);
                                        MyModuleWebService.SendEmail_Error(message.Value, MyEmailParameters, MyEmailParameters.RendszerfelugyeletiEmailCim, null, erkeztetResult.errorCode, erkeztetResult.errorMessage, null, msg);
                                        continue;
                                    }
                                    MyLogger.Debug("Email_Erkeztetes IDs:  " + erkeztetResult.uid);
                                    MyLogger.Debug("Stop Email_Erkeztetes: " + message.Key + AxisConstants.PendingEmailFileExtension);

                                    #region GET AZONOSITO
                                    string erkeztetoAzonosito = GetErkeztetoAzonosito(MyEmailParameters, erkeztetResult.uid);
                                    #endregion

                                    #region SEND MAIL
                                    MyLogger.Debug("Start_Email_Küldés_Erkeztetes: " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    //V2
                                    bool emailErkeztetresult = GenerateAndSendErkeztetIktatEmail(message.Value, MyEmailParameters, erkeztetResult.uid, null, erkeztetoAzonosito);
                                    //V1
                                    // bool emailresult = MyModuleWebService.SendEmail_EmailErkeztetve(message.Value, MyEmailParameters, erkeztetResult.uid, emailBoritekResult.uid);
                                    MyLogger.Debug("Stop_Email_Küldés_Erkeztetes eredmény:  " + emailErkeztetresult);
                                    MyLogger.Debug("Stop_Email_Küldés_Erkeztetes:  " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    #endregion
                                    break;
                                case (ushort)AxisConstants.EnumDocumentTaskType.ErkeztetEsIktat:
                                    MyLogger.Debug("Start Email_ErkeztetesIktatas: " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    WSO_BaseResult iktatasResult = MyModuleWebService.Email_ErkeztetesIktatas(message.Value, emailBoritekResult.uid, MyEmailParameters);
                                    if (iktatasResult == null || string.IsNullOrEmpty(iktatasResult.uid))
                                    {
                                        string msg = string.Format("Nem sikerült az email ÉrkeztetésIktatás. Feldolgozás vége!  Email={0} Hiba={1} ", message.Value.From + " " + message.Value.Subject, iktatasResult == null ? string.Empty : iktatasResult.errorCode + " " + iktatasResult.errorMessage);
                                        MyLogger.Error(msg);
                                        Console.WriteLine(msg);
                                        MyModuleWebService.SendEmail_Error(message.Value, MyEmailParameters, MyEmailParameters.RendszerfelugyeletiEmailCim, null, iktatasResult.errorCode, iktatasResult.errorMessage, null, msg);
                                        continue;
                                    }

                                    #region GET AZONOSITO
                                    string iktatoAzonosito = GetIktatoAzonosito(MyEmailParameters, iktatasResult.uid);
                                    #endregion

                                    #region SEND MAIL
                                    MyLogger.Debug("Start_Email_Küldés_Iktatas: " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    bool emailresult = GenerateAndSendErkeztetIktatEmail(message.Value, MyEmailParameters, null, iktatasResult.uid, iktatoAzonosito);
                                    MyLogger.Debug("Start_Email_Küldés_Iktatas eredmény:  " + emailresult);
                                    MyLogger.Debug("Start_Email_Küldés_Iktatas:  " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    #endregion

                                    MyLogger.Debug("Email_ErkeztetesIktata IDs:  " + iktatasResult.uid);
                                    MyLogger.Debug("Stop Email_ErkeztetesIktatas: " + message.Key + AxisConstants.PendingEmailFileExtension);
                                    break;
                                case (ushort)AxisConstants.EnumDocumentTaskType.ErkeztetEsIktatEsIrattaroz:
                                    break;
                                default:
                                    break;
                            }
                            #endregion

                            #region DELETE PROCESSED FILE
                            MyLoader.DeleteFileAfterProcessed(message.Key + AxisConstants.PendingEmailFileExtension);
                            MyLogger.Debug("Deleted: " + message.Key + AxisConstants.PendingEmailFileExtension);
                            #endregion
                        }
                        catch (Exception exc)
                        {
                            MyLogger.Error(exc);
                            Console.WriteLine(exc.Message);
                            return false;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                MyLogger.Error(exc);
                return false;
            }
            MyLogger.Debug("Stop Core:" + MyEmailParameters.EmailAddress);
            return true;
        }
        /// <summary>
        /// Start async Core procedure flow
        /// </summary>
        public async Task<bool> StartProcedureAsync()
        {
            bool result = await Task.Run(() => StartProcedure());
            return result;
        }

        private bool GenerateAndSendErkeztetIktatEmail(MimeMessage message, DBO_ManagedEmailParameters emailParameters, string erkeztetesId, string iktatasId, string azonosito)
        {
            string body = GenerateResponseEmailToSender(message, MyEmailParameters, erkeztetesId, iktatasId, azonosito);
            return MyModuleWebService.SendEmail_EmailToSender(
               emailParameters.FelhasznaloId,
               emailParameters.FelhasznaloSzervezetId,
               emailParameters.ValaszEmailFeladoCim,
               new string[] { message.From.Mailboxes.FirstOrDefault().Address },
               emailParameters.ValaszEmailTargy,
               body,
               true,
               null,
               null);
        }
        private string GenerateResponseEmailToSender(MimeMessage message, DBO_ManagedEmailParameters emailParameters, string erkeztetesId, string iktatasId, string azonosito)
        {
            #region PROCESSEDD TASKS
            string bodyFormat;
            string felelosNeve;
            if (string.IsNullOrEmpty(iktatasId))
            {
                bodyFormat = emailParameters.ValaszEmailSablonErkeztetes;
                felelosNeve = emailParameters.FelelosErkeztetoNeve;
            }
            else
            {
                bodyFormat = emailParameters.ValaszEmailSablonIktatas;
                felelosNeve = emailParameters.FelelosIktatoNeve;
            }
            #endregion
            #region ATTACHEMNST
            StringBuilder attachmentsList = new StringBuilder();
            if (message.Attachments == null || message.Attachments.Count() < 1)
                attachmentsList.Append("---");
            else
                foreach (MimeEntity attachment in message.Attachments)
                {
                    string filename;
                    if (attachment is MimePart part)
                        filename = part.FileName;
                    else
                        filename = "Ismeretlen";
                    attachmentsList.Append(filename + "<br/>");
                }
            #endregion

            string body = string.Format(
                 bodyFormat,
                 message.From.Mailboxes.FirstOrDefault().Name,
                 message.Date.ToString("yyyy.MM.dd hh:mm"),
                 message.Subject,
                 attachmentsList,
                 string.IsNullOrEmpty(iktatasId) ? erkeztetesId : iktatasId,
                 azonosito,
                 felelosNeve
            );
            return body;
        }
        /// <summary>
        /// Return érkeztető azonosito from id
        /// </summary>
        /// <param name="emailParameters"></param>
        /// <param name="erkeztetesId"></param>
        /// <returns></returns>
        private string GetErkeztetoAzonosito(DBO_ManagedEmailParameters emailParameters, string erkeztetesId)
        {
            var result = MyModuleWebService.GetKuldKuldemenyById(emailParameters, erkeztetesId);
            string azonosito = null;
            if (result.ds != null && result.ds.Tables.Count >= 1 && result.ds.Tables[0].Rows != null && result.ds.Tables[0].Rows.Count >= 1)
            {
                azonosito = result.ds.Tables[0].Rows[0]["Azonosito"].ToString();
            }
            return azonosito;
        }
        /// <summary>
        /// Return iktato azonosito from id
        /// </summary>
        /// <param name="emailParameters"></param>
        /// <param name="iktatoId"></param>
        /// <returns></returns>
        private string GetIktatoAzonosito(DBO_ManagedEmailParameters emailParameters, string iktatoId)
        {
            var result = MyModuleWebService.GetIraIratokById(emailParameters, iktatoId);
            string azonosito = null;
            if (result.ds != null && result.ds.Tables.Count >= 1 && result.ds.Tables[0].Rows != null && result.ds.Tables[0].Rows.Count >= 1)
            {
                azonosito = result.ds.Tables[0].Rows[0]["Azonosito"].ToString();
            }
            return azonosito;
        }
    }
}
