﻿HELP INFO
=========

1) GMAIL esetén szükség lehet a Kevésbé biztonságos alkalmazások bekapcsolása opció:
   https://www.google.com/settings/security/lesssecureapps?rfn=27&rfnc=1&et=0&asae=2

2)  Axis email címek:
    edoksupport@axis.hu
    elvi_epitesi_engedely@axis.hu

3) NUGET
   Install-Package EntityFramework
   Install-Package MimeKit
   Install-Package MailKit
   Install-Package NLog -Version 4.4.3
   Install-Package NLog.Config -Version 4.4.3
   Install-Package Newtonsoft.Json
   Install-Package System.Data.SqlClient
