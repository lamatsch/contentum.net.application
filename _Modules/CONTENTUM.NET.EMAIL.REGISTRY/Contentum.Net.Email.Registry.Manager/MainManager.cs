﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.DAL;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Manager
{
    /// <summary>
    /// Main manager class
    /// </summary>
    public class MainManager : IDisposable
    {
        #region PROPERTIES
        private static Logger MyLogger { get; set; }
        private List<DBO_ManagedEmailParameters> MyParameters { get; set; }
        private List<ManagerCore> MyManagerCore { get; set; }
        private DBO_ManagedEmailParameters MyAppSettings { get; set; }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public MainManager()
        {
            InitializeLogger();
            MyLogger.Debug("START INIT MANAGER");
            Initialize();
            InitializeAppSettings();
            //InitializeParametersFromDataBase();
            InitializeManagedEmailAddressParametersFromWebSvc();
            InitializeManagerCore();
            MyLogger.Debug("STOP INIT MANAGER");
        }
        private void Initialize()
        {
            MyManagerCore = new List<ManagerCore>();
        }
        /// <summary>
        /// Initialize logger
        /// </summary>
        private void InitializeLogger()
        {
            MyLogger = LogManager.GetCurrentClassLogger();
        }
        /// <summary>
        /// Load main parameters from database
        /// </summary>
        [Obsolete]
        private void InitializeParametersFromDataBase()
        {
            List<INT_ManagedEmailAddress> items = null;
            try
            {
                items = DataBaseProvider.GetTableAllManagedEmailAddress();
            }
            catch (Exception exc)
            {
                MyLogger.Error(exc);
            }
            if (items != null)
                MyParameters = Converters.ConvertToCommonManagedEmailParameters(items);
        }
        private bool InitializeAppSettings()
        {
            MyLogger.Debug("START LOAD APPSETTINGS");
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["EXEC_Felhasznalo_Id"])
                ||
                string.IsNullOrEmpty(ConfigurationManager.AppSettings["EXEC_FelhasznaloSzervezet_Id"]))
            {
                throw new ArgumentException("Nem megfelelő kezdeti paraméterek: EXEC_Felhasznalo_Id, EXEC_FelhasznaloSzervezet_Id");
            }
            MyAppSettings = new DBO_ManagedEmailParameters()
            {
                FelhasznaloId = ConfigurationManager.AppSettings["EXEC_Felhasznalo_Id"].ToString(),
                FelhasznaloSzervezetId = ConfigurationManager.AppSettings["EXEC_FelhasznaloSzervezet_Id"].ToString()
            };
            MyLogger.Debug("STOP LOAD APPSETTINGS");
            return true;
        }
        /// <summary>
        /// Initialize managed email address from web service
        /// </summary>
        private void InitializeManagedEmailAddressParametersFromWebSvc()
        {
            ModuleWebService MyModuleWebService = new ModuleWebService();
            List<INT_ManagedEmailAddress> items = null;
            try
            {
                var result = MyModuleWebService.GetAllManagedEmailAddress(MyAppSettings);
                if (result != null && result.ds != null && result.ds.Tables.Count > 0 && result.ds.Tables[0].Rows.Count > 0)
                {
                    items = new List<INT_ManagedEmailAddress>();
                    int count = result.ds.Tables[0].Rows.Count;
                    for (int i = 0; i < count; i++)
                    {
                        items.Add(new INT_ManagedEmailAddress()
                        {
                            Id = Guid.Parse(result.ds.Tables[0].Rows[i]["Id"].ToString()),
                            Nev = result.ds.Tables[0].Rows[i]["Nev"].ToString(),
                            Ertek = result.ds.Tables[0].Rows[i]["Ertek"].ToString(),
                        });
                    }
                }
            }
            catch (Exception exc)
            {
                MyLogger.Error(exc);
            }
            if (items != null)
                MyParameters = Converters.ConvertToCommonManagedEmailParameters(items);
        }

        /// <summary>
        /// Initialize manager core modules by parameters
        /// </summary>
        private void InitializeManagerCore()
        {
            if (MyParameters == null || MyParameters.Count < 1)
            {
                MyLogger.Debug("Nincs feldolgozandó menedzselt email cím !");
                return;
            }
            ManagerCore temp = null;
            foreach (DBO_ManagedEmailParameters item in MyParameters)
            {
                temp = new ManagerCore(item, MyLogger);
                MyManagerCore.Add(temp);
            }
        }

        /// <summary>
        /// Start each sub manager core modules
        /// </summary>
        public bool Start()
        {
            MyLogger.Debug("START MANAGER");
            if (MyManagerCore == null || MyManagerCore.Count < 1)
                return false;

            foreach (ManagerCore item in MyManagerCore)
            {
                item.StartProcedure();
            }
            MyLogger.Debug("STOP MANAGER");
            return true;
        }
        /// <summary>
        /// Start each sub manager core modules
        /// </summary>
        //public async Task<bool> StartAsync()
        //{
        //    if (MyManagerCore == null || MyManagerCore.Count < 1)
        //        return false;

        //    var tasks = MyManagerCore.Select(async item =>
        //    {
        //        var response = await item.StartProcedureAsync();
        //    });
        //    await Task.WhenAll(tasks);

        //    return true;
        //}
        public bool StartParallel()
        {
            MyLogger.Debug("START MANAGER");
            if (MyManagerCore == null || MyManagerCore.Count < 1)
                return false;

            Parallel.ForEach(MyManagerCore, item => item.StartProcedure());
            MyLogger.Debug("STOP MANAGER");
            return true;
        }

        public void Dispose()
        {
            MyManagerCore = null;
            MyParameters = null;
            MyLogger = null;
        }
    }
}