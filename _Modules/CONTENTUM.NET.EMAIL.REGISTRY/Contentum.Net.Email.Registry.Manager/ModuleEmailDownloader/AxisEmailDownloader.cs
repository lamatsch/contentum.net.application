﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using AxisRendszerhaz.Contentum.Net.Email.Registry.ModuleImap;
using System.IO;
using System.Linq;
using NLog;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.ModuleDownloader
{
    /// <summary>
    /// Email downloading module
    /// </summary>
    internal class ModuleEmailDownloader : BaseModuleClass
    {
        public DBO_ManagedEmailParameters MyParameters { get; set; }
        public string LocalMailFolder { get; set; }
        public ModuleEmailDownloader(DBO_ManagedEmailParameters inParameters, Logger inlogger)
        {
            MyParameters = inParameters;
            base.MyLogger = inlogger;
            LocalMailFolder = CommonFunctions.CreateEmailDirectory(MyParameters.EmailAddress);
        }
        public void StartDownload()
        {
            base.MyLogger.Debug("Start: EmailDownloader", this.GetType().ToString());
            AxisImapClient client = new AxisImapClient(MyParameters, base.MyLogger);
            client.StartDownloadImap();
            if (client.ReceivedMessages != null)
                SaveEmailMessageToLocal(client);
            base.MyLogger.Debug("Stop: EmailDownloader", this.GetType().ToString());
        }

        private void SaveEmailMessageToLocal(AxisImapClient client)
        {
            while (client.ReceivedMessages.Any())
            {
                var mail = client.ReceivedMessages.Dequeue();
                string fileName = Path.Combine(LocalMailFolder, CommonFunctions.GetImapMessageFileName(mail));
                mail.Message.WriteTo(fileName);

                // Console.WriteLine(mail.Message.Date + " " + mail.Message.Sender + " " + mail.Message.Subject);
            }
        }
    }

}
