﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using MimeKit;
using NLog;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.ModuleDownloader
{
    internal class ModuleEmailLoader : BaseModuleClass
    {
        public Dictionary<string, MimeMessage> MailMessages = new Dictionary<string, MimeMessage>();
        private string Path { get; set; }
        public ModuleEmailLoader(string inFolderName, Logger inlogger)
        {
            Path = CommonFunctions.GetEmailDirectory(SteriliseFolder(inFolderName));
            base.MyLogger = inlogger;
        }

        public void StartLoader()
        {
            base.MyLogger.Debug("Start: EmailLoader");
            string[] files = null;

            if (Path != null)
                files = Directory.GetFiles(Path)
                    .Where(f => f.EndsWith(AxisConstants.EmailFileExtension))
                    .ToArray();

            if (files != null)
            {
                LoadFiles(files);
                RenameFiles(files);
            }
            base.MyLogger.Debug("Stop: EmailLoader");
        }

        /// <summary>
        /// Load files to the collection
        /// </summary>
        /// <param name="files"></param>
        private void LoadFiles(string[] files)
        {
            MimeMessage msg = null;
            foreach (string file in files)
            {
                msg = MimeMessage.Load(file);
                if (msg != null)
                    MailMessages.Add(file, msg);
            }
        }
        /// <summary>
        /// Rename file extension to ".pending"
        /// </summary>
        /// <param name="file"></param>
        private void RenameFiles(string[] files)
        {
            foreach (string file in files)
            {
                if (File.Exists(file))
                    RenameFileNameToPending(file);
            }
        }
        /// <summary>
        /// Rename file extension to ".pending"
        /// </summary>
        /// <param name="file"></param>
        private void RenameFileNameToPending(string file)
        {
            try
            {
                base.MyLogger.Debug("Rename start:" + file + System.Environment.NewLine + file + AxisConstants.PendingEmailFileExtension);
                File.Move(file, file + AxisConstants.PendingEmailFileExtension);
                base.MyLogger.Debug("Rename stop:");
            }
            catch (System.Exception exc)
            {
                base.MyLogger.Error(exc);
            }
        }
        public void DeleteFileAfterProcessed(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch (System.Exception exc)
            {
                base.MyLogger.Error(exc);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        private string SteriliseFolder(string folderName)
        {
            if (folderName.Contains(@"\"))
            {
                string[] splitted = folderName.Split(new string[] { @"\" }, System.StringSplitOptions.RemoveEmptyEntries);
                return splitted[1];
            }
            else if (folderName.Contains(@"/"))
            {
                string[] splitted = folderName.Split(new string[] { @"/" }, System.StringSplitOptions.RemoveEmptyEntries);
                return splitted[1];
            }
            return folderName;
        }
    }
}
