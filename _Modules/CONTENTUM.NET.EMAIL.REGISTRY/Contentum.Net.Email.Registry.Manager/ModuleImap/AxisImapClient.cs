﻿#region MailKit
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit.Security;
using NLog;
#endregion

using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using System;
using System.Collections.Generic;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.ModuleImap
{
    /// <summary>
    /// Imap client class
    /// </summary>
    internal class AxisImapClient : BaseModuleClass, IDisposable
    {
        #region EVENTS
        public delegate void ImapMessageReceived(MimeKit.MimeMessage receivedMessage);
        public event ImapMessageReceived OnImapMessageReceived;
        #endregion

        #region PROPERTIES
        public DBO_ManagedEmailParameters MyParameters { get; private set; }
        public Queue<AxisImapMessage> ReceivedMessages { get; set; }

        #endregion

        public AxisImapClient(DBO_ManagedEmailParameters inParameters, Logger inlogger)
        {
            MyParameters = inParameters ?? throw new ArgumentNullException("Missing the imap configuration !");
            ReceivedMessages = new Queue<AxisImapMessage>();
            base.MyLogger = inlogger;
        }
        private DateTime GetAndUpdateLastRun()
        {
            string lastRunTimeStamp = ConfigurationManager.AppSettings["LAST_RUN"];
            DateTime resultTimeStamp;
            bool canBeParsed = DateTime.TryParse(lastRunTimeStamp, out resultTimeStamp);
            if (!canBeParsed)
            {
                base.MyLogger.Debug("Cannot parse LastRun time stamp from the config fetching mails current time -1 hour.");
                resultTimeStamp = DateTime.Now.AddHours(-1);
            }
            ConfigurationManager.AppSettings.Set("LastRun", DateTime.Now.ToString());
            base.MyLogger.Debug("Getting unseen messages delivered after: " + resultTimeStamp.ToString());
            return resultTimeStamp;
        }

        public void StartDownloadImap()
        {
            base.MyLogger.Debug("Start: DownloadImap");
            using (var client = new ImapClient())
            {
                client.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                client.Connect(MyParameters.ImapServerAddress, MyParameters.ImapServerPort, SecureSocketOptions.SslOnConnect);
                client.Authenticate(MyParameters.ImapLoginUser, MyParameters.ImapLoginPassword);

                IMailFolder folderToUse = null;
                string folderNameToUse = ConfigurationManager.AppSettings["IMAP_FOLDER"];
                if (!string.IsNullOrEmpty(folderNameToUse))
                {
                    var personal = client.GetFolder(client.PersonalNamespaces[0]);
                    foreach (var folder in personal.GetSubfolders(false))
                    {
                        if (folder.Name == folderNameToUse)
                        {
                            folderToUse = folder;
                            break;
                        }
                    }
                }

                if (folderToUse == null) { folderToUse = client.Inbox; }
               
                base.MyLogger.Debug("Checking folder: " + folderToUse.Name);
                var lastRun = GetAndUpdateLastRun();                                

                folderToUse.Open(FolderAccess.ReadWrite);
                var uids = folderToUse.Search(SearchQuery.DeliveredAfter(lastRun).And(SearchQuery.NotSeen));
                foreach (UniqueId uid in uids)
                {
                    var message = client.Inbox.GetMessage(uid);

                    ReceivedMessages.Enqueue(new AxisImapMessage(uid.Id, message));
                    OnImapMessageReceived?.Invoke(message);

                    base.MyLogger.Debug(string.Format("{0} | {1} | {2}", message.Date, message.From, message.Subject));
                }
                if (uids != null && uids.Count > 0)
                {
                    client.Inbox.AddFlags(uids, MessageFlags.Seen, true);
                }
                client.Disconnect(true);
            }
            base.MyLogger.Debug("Stop: DownloadImap");
        }
        bool CustomCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            // if there are errors in the certificate chain, look at each error to determine the cause.
            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    foreach (var status in chain.ChainStatus)
                    {
                        if ((certificate.Subject == certificate.Issuer) && (status.Status == X509ChainStatusFlags.UntrustedRoot))
                        {
                            // self-signed certificates with an untrusted root are valid. 
                            continue;
                        }
                        else if (status.Status != X509ChainStatusFlags.NoError)
                        {
                            // if there are any other errors in the certificate chain, the certificate is invalid,
                            // so the method returns false.
                            return false;
                        }
                    }
                }
                // When processing reaches this line, the only errors in the certificate chain are 
                // untrusted root errors for self-signed certificates. These certificates are valid
                // for default Exchange server installations, so return true.
                return true;
            }
            return false;
        }
        public void Dispose()
        {
            if (ReceivedMessages != null)
            {
                ReceivedMessages.Clear();
                ReceivedMessages = null;
            }
            MyParameters = null;
            OnImapMessageReceived = null;
        }
    }
}
