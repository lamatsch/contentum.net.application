﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy;
using MimeKit;
using System;
using System.Linq;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Manager
{
    /// <summary>
    /// Module for web service communication
    /// </summary>
    public partial class ModuleWebService
    {
        #region SEND EMAIL
        public void SendEmail_EmailAnswer(MimeMessage message, string ugyiratId, string kuldemenyId, DBO_ManagedEmailParameters emailParameters)
        {
            WSO_EmailServiceAnswerEmail param = SetParameters_EmailAnswer(message, ugyiratId, kuldemenyId, emailParameters);
            ProxyEmail.SendAnswerEmail(param);
        }
        public bool SendEmail_EmailToSender(string felhasznlaoId, string FelhasznaloSzervezetId, string from, string[] to, string subject, string body, bool ishtml, string[] cc = null, string[] bcc = null)
        {
            return ProxyEmail.SendResponseEmailToSender(felhasznlaoId, FelhasznaloSzervezetId, from, to, subject, body, ishtml, cc, bcc);
        }
        public bool SendEmail_EmailErkeztetve(MimeMessage message, DBO_ManagedEmailParameters emailParameters, string erkeztetesId, string emailBoritekId)
        {
            WSO_EmailService_SendEmailErkeztetve param = SetParameters_SendEmail_EmailErkeztetve(message, emailParameters, erkeztetesId, emailBoritekId);
            return ProxyEmail.SendEmailErkeztetve(param);
        }
        /// <summary>
        /// Send error mail
        /// </summary>
        /// <param name="message"></param>
        /// <param name="emailParameters"></param>
        /// <param name="inFelhasznaloId"></param>
        /// <param name="inFelhasznaloSzervezeteId"></param>
        /// <param name="notifiedEmailAddress"></param>
        /// <param name="inUrl"></param>
        /// <param name="inErrorCode"></param>
        /// <param name="inErrorMessage"></param>
        /// <param name="inLogReference"></param>
        /// <param name="inDescription"></param>
        /// <returns></returns>
        public bool SendEmail_Error(MimeMessage message, DBO_ManagedEmailParameters emailParameters, string notifiedEmailAddress, string url, string errorCode, string errorMessage, string logReference = null, string description = null)
        {
            return ProxyEmail.SendErrorInMail(
                inFelhasznaloId: emailParameters.FelhasznaloId,
                inFelhasznaloSzervezeteId: emailParameters.FelhasznaloSzervezetId,
                inNotifiedEmailAddress: message.From.Mailboxes.FirstOrDefault().Address,
                inUrl: url,
                inErrorCode: errorCode,
                inErrorMessage: errorMessage,
                inLogReference: logReference,
                inDescription: description
                );
        }
        #endregion

        #region SET PARAMS
        private WSO_EmailServiceAnswerEmail SetParameters_EmailAnswer(MimeMessage message, string ugyiratId, string kuldemenyId, DBO_ManagedEmailParameters emailParameters)
        {
            WSO_EmailServiceAnswerEmail param = new WSO_EmailServiceAnswerEmail();
            SetParameters_EmailAnswer_ExecParam(ref param, emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId);

            param.Kuldemeny_id = kuldemenyId;
            param.Ugyirat_id = ugyiratId;
            return param;
        }
        private void SetParameters_EmailAnswer_ExecParam(ref WSO_EmailServiceAnswerEmail param, string inUserId, string inUserGroupId)
        {
            if (param.MyExecParam == null)
                param.MyExecParam = new WSO_EmailService_ExecParam();

            param.MyExecParam.felhasznalo_IdField = inUserId;
            param.MyExecParam.felhasznaloSzervezet_IdField = inUserGroupId;
        }
        private WSO_EmailService_SendEmailErkeztetve SetParameters_SendEmail_EmailErkeztetve(MimeMessage message, DBO_ManagedEmailParameters emailParameters, string erkeztetesId, string emailBoritekId)
        {
            WSO_EmailService_SendEmailErkeztetve param = new WSO_EmailService_SendEmailErkeztetve();
            param.MyExecParam = new WSO_EmailService_ExecParam();
            param.MyExecParam.felhasznalo_IdField = emailParameters.FelhasznaloId;
            param.MyExecParam.felhasznaloSzervezet_IdField = emailParameters.FelhasznaloSzervezetId;

            param.MyErkeztetoSzam = erkeztetesId;

            param.MyEmailBoritek = new WSO_EmailService_EREC_eMailBoritekok();
            param.MyEmailBoritek.IdField = emailBoritekId;
            param.MyEmailBoritek.FeladoField = emailParameters.ValaszEmailFeladoCim;
            param.MyEmailBoritek.FeladasDatumaField = message.Date == null ? DateTime.Now.ToString() : message.Date.ToString();
            param.MyEmailBoritek.ErkezesDatumaField = message.Date == null ? DateTime.Now.ToString() : message.Date.ToString();
            param.MyEmailBoritek.CimzettField = message.To.Mailboxes.FirstOrDefault().Address;
            param.MyEmailBoritek.KuldKuldemeny_IdField = emailBoritekId;
            param.MyEmailBoritek.TargyField = message.Body.ToString();
            param.MyEmailBoritek.UzenetField = message.Body.ToString();
            param.MyEmailBoritek.BaseDocumentField = new WSO_EmailService_BaseDocument() { ver = "1" };

            return param;
        }
        #endregion
    }
}