﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.E_ADMIN;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.Manager
{
    /// <summary>
    /// Module for web service communication
    /// </summary>
    public partial class ModuleWebService
    {
        public const string ERKEZTETO_NAME = "Automatikus érkeztető";

        #region LOAD MANAGED EMAILS
        public WSO_DataSetResult GetAllManagedEmailAddress(DBO_ManagedEmailParameters emailParameters)
        {
            return ProxyIntManagedEmailAddress.GetAllManagedEmailAddress(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId);
        }
        #endregion
        public Tuple<WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult> LoadParameters(DBO_ManagedEmailParameters emailParameters)
        {
            WSO_DataSetResult erkeztetoResult = ProxyIraIktatokonyvek.GetAllIktatoKonyv(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, AxisConstants.EnumKonyvType.E.ToString()[0], emailParameters.KeresesiParameterErkeztetoKonyvAzonosito);
            WSO_DataSetResult iktatoResult = ProxyIraIktatokonyvek.GetAllIktatoKonyv(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, AxisConstants.EnumKonyvType.I.ToString()[0], emailParameters.KeresesiParameterIktatoKonyvAzonosito);
            WSO_DataSetResult irattariTetelResult = ProxyIrattariTetelek.GetAll(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, emailParameters.KeresesiParameterIrattáriTételAzonosító);
            WSO_DataSetResult erkeztetesFelelosFelhasznaloResult = GetCsoportById(emailParameters, emailParameters.Kuldemenyek_Csoport_Id_Felelos);
            WSO_DataSetResult irattarozasFelelosFelhasznaloResult = GetCsoportById(emailParameters, emailParameters.UgyUgyiratok_Csoport_Id_Felelos);

            WSO_DataSetResult rendszerfelugyeletEmailCimekResult = GetParameterekByNev(emailParameters, AxisConstants.ConstParameterekRendszerfelugyeletEmail);

            return new Tuple<WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult, WSO_DataSetResult>(erkeztetoResult, iktatoResult, irattariTetelResult, erkeztetesFelelosFelhasznaloResult, irattarozasFelelosFelhasznaloResult, rendszerfelugyeletEmailCimekResult);
        }
        public WSO_BaseResult Email_InsertAndAttachDocument(MimeMessage message, DBO_ManagedEmailParameters emailParameters)
        {
            WSO_EmailBoritekInsertAndAttachDoc param = SetParameters_EmailBoritekok(message, emailParameters);
            var result = ProxyEmailBoritekok.InsertAndAttachDocument(param);

            return result;
        }
        public WSO_BaseResult Email_ErkeztetesIktatas(MimeMessage message, string emailBoritekId, DBO_ManagedEmailParameters emailParameters, bool alszamraIktatas = false)
        {
            WSO_EmailErkeztetesIktatas param = SetParameters_EmailErkeztetesIktatas(emailBoritekId, emailParameters, message);
            WSO_BaseResult result = ProxyEmailErkeztetesIktatas.EmailErkeztetesIktatas(param, alszamraIktatas);
            return result;
        }
        public WSO_BaseResult Email_Erkeztetes(MimeMessage message, string emailBoritekId, DBO_ManagedEmailParameters emailParameters)
        {
            WSO_KuldKuldemenyekEmailIktatas data = SetParameters_EmailErkeztetes(emailBoritekId, emailParameters, message);
            WSO_BaseResult result = ProxyKuldKuldemenyek.ErkeztetesEmailbol(emailParameters, data);
            return result;
        }

        private string GetSenderName(InternetAddressList list)
        {
           if(list.Count ==0) { return ERKEZTETO_NAME; }
            var item = list.First();
            if(item is MailboxAddress)
            {
                return (item as MailboxAddress).Name;
            }
            return item.Name;
        }

        private string GetSenderAddress(InternetAddressList list)
        {          
            if (list.Count == 0) { return ERKEZTETO_NAME; }
            var item = list.First();
            if (item is MailboxAddress)
            {
                return (item as MailboxAddress).Address;
            }
            return item.Name;
        }

        #region SET PARAMS - ERKEZTET, IKTAT
        private WSO_EmailErkeztetesIktatas SetParameters_EmailErkeztetesIktatas(string emailBoritekId, DBO_ManagedEmailParameters emailParameters, MimeMessage message)
        {
            WSO_EmailErkeztetesIktatas param = new WSO_EmailErkeztetesIktatas();
            SetParameters_EmailErkeztetesIktatas_ExecParam(ref param, emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId);

            param.MyEREC_KuldKuldemenyek = new WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek();
            param.MyEREC_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            param.MyEREC_KuldKuldemenyek.IraIktatokonyv_Id = emailParameters.IraIktatoKonyvId;
            param.MyEREC_KuldKuldemenyek.KuldesMod = "11";
            param.MyEREC_KuldKuldemenyek.Targy = message.Subject;
            param.MyEREC_KuldKuldemenyek.Surgosseg = "30";
            param.MyEREC_KuldKuldemenyek.UgyintezesModja = "1";
            param.MyEREC_KuldKuldemenyek.PeldanySzam = "1";
            param.MyEREC_KuldKuldemenyek.UgyintezesModja = "1";
            param.MyEREC_KuldKuldemenyek.PeldanySzam = "1";
            param.MyEREC_KuldKuldemenyek.IktatniKell = "1";
            // BUG_14099
            param.MyEREC_KuldKuldemenyek.IktatastNemIgenyel = "1";

            param.MyEREC_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            param.MyEREC_KuldKuldemenyek.Csoport_Id_Cimzett = emailParameters.Kuldemenyek_Csoport_Id_Cimzett;
            param.MyEREC_KuldKuldemenyek.Csoport_Id_Felelos = emailParameters.Kuldemenyek_Csoport_Id_Felelos;
            param.MyEREC_KuldKuldemenyek.CimSTR_Bekuldo = GetSenderAddress(message.From);
            param.MyEREC_KuldKuldemenyek.NevSTR_Bekuldo = GetSenderName(message.From);
            param.MyEREC_KuldKuldemenyek.AdathordozoTipusa = "1";
            param.MyEREC_KuldKuldemenyek.KezbesitesModja = "04";
            param.MyEREC_KuldKuldemenyek.Munkaallomas = ERKEZTETO_NAME;
            param.MyEREC_KuldKuldemenyek.CimzesTipusa = "01";

            param.MyEREC_eMailBoritekok = new WSO_EmailErkeztetesIktatas_EREC_eMailBoritekok();
            param.MyEREC_eMailBoritekok.IdField = emailBoritekId;
            param.MyEREC_eMailBoritekok.BaseDocument = new WSO_EmailErkeztetesIktatas_BaseDocument() { ver = "1" };

            param.MyEREC_IraIktatoKonyvek_Id = emailParameters.IraIktatoKonyvId;

            param.MyEREC_UgyUgyiratok = new WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok();
            param.MyEREC_UgyUgyiratok.hatarido = DateTime.Now.AddDays(30).ToString();
            param.MyEREC_UgyUgyiratok.targy = message.Subject;
            param.MyEREC_UgyUgyiratok.ugyTipus = "001";
            param.MyEREC_UgyUgyiratok.csoport_Id_Felelos = emailParameters.UgyUgyiratok_Csoport_Id_Felelos;
            param.MyEREC_UgyUgyiratok.csoport_Id_Ugyfelelos = emailParameters.FelhasznaloSzervezetId;
            param.MyEREC_UgyUgyiratok.iraIrattariTetel_Id = emailParameters.UgyUgyiratok_iraIrattariTetel_Id;
            param.MyEREC_UgyUgyiratok.nevSTR_Ugyindito = ERKEZTETO_NAME;
            param.MyEREC_UgyUgyiratok.felhasznaloCsoport_Id_Ugyintez = emailParameters.UgyUgyiratok_felhasznaloCsoport_Id_Ugyintez;
            param.MyEREC_IraIratok = new WSO_EmailErkeztetesIktatas_EREC_IraIratok();
            param.MyEREC_IraIratok.Targy = message.Subject;
            param.MyEREC_IraIratok.Jelleg = "2";
            param.MyEREC_IraIratok.AdathordozoTipusa = "1";
            param.MyEREC_IraIratok.KiadmanyozniKell = "0";
            param.MyEREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez = emailParameters.IraIratok_FelhasznaloCsoport_Id_Ugyintez;
            param.MyEREC_IraIratok.Irattipus = "LEV";

            param.MyIktatasiParameterek = new WSO_EmailErkeztetesIktatas_IktatasiParameterek();
            param.MyIktatasiParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
            param.MyIktatasiParameterek.Adoszam = null;
            param.MyIktatasiParameterek.UgykorId = emailParameters.IktatasiParameterek_UgykorId;
            param.MyIktatasiParameterek.Ugytipus = "001";
            param.MyIktatasiParameterek.UgyiratUjranyitasaHaLezart = false;
            param.MyIktatasiParameterek.UgyiratPeldanySzukseges = false;
            param.MyIktatasiParameterek.KeszitoPeldanyaSzukseges = false;
            param.MyIktatasiParameterek.Atiktatas = false;
            param.MyIktatasiParameterek.EmptyUgyiratSztorno = false;
            param.MyIktatasiParameterek.MunkaPeldany = false;

            return param;
        }
        private WSO_KuldKuldemenyekEmailIktatas SetParameters_EmailErkeztetes(string emailBoritekId, DBO_ManagedEmailParameters emailParameters, MimeMessage message)
        {
            WSO_KuldKuldemenyekEmailIktatas param = new WSO_KuldKuldemenyekEmailIktatas();

            param.MyEXEC_Param = new WSO_KuldKuldemenyek_ExecParam();
            param.MyEXEC_Param.Felhasznalo_IdField = emailParameters.FelhasznaloId;
            param.MyEXEC_Param.FelhasznaloSzervezet_IdField = emailParameters.FelhasznaloSzervezetId;

            param.MyEREC_KuldKuldemenyek = new WSO_KuldKuldemenyek_EREC_KuldKuldemenyek();
            param.MyEREC_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            param.MyEREC_KuldKuldemenyek.IraIktatokonyv_Id = emailParameters.IraErkeztetoKonyvId;
            param.MyEREC_KuldKuldemenyek.KuldesMod = "11";
            param.MyEREC_KuldKuldemenyek.Targy = message.Subject;
            param.MyEREC_KuldKuldemenyek.Surgosseg = "30";
            param.MyEREC_KuldKuldemenyek.UgyintezesModja = "1";
            param.MyEREC_KuldKuldemenyek.PeldanySzam = "1";
            param.MyEREC_KuldKuldemenyek.UgyintezesModja = "1";
            param.MyEREC_KuldKuldemenyek.PeldanySzam = "1";
            param.MyEREC_KuldKuldemenyek.IktatniKell = "1";
            // BUG_14099
            param.MyEREC_KuldKuldemenyek.IktatastNemIgenyel = "1";

            param.MyEREC_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            param.MyEREC_KuldKuldemenyek.Csoport_Id_Cimzett = emailParameters.Kuldemenyek_Csoport_Id_Cimzett;
            param.MyEREC_KuldKuldemenyek.Csoport_Id_Felelos = emailParameters.Kuldemenyek_Csoport_Id_Felelos;
            param.MyEREC_KuldKuldemenyek.CimSTR_Bekuldo = GetSenderAddress(message.From);
            param.MyEREC_KuldKuldemenyek.NevSTR_Bekuldo = GetSenderName(message.From);
            param.MyEREC_KuldKuldemenyek.AdathordozoTipusa = "1";
            param.MyEREC_KuldKuldemenyek.KezbesitesModja = "04";
            param.MyEREC_KuldKuldemenyek.Munkaallomas = ERKEZTETO_NAME;
            param.MyEREC_KuldKuldemenyek.CimzesTipusa = "01";

            param.MyEREC_eMailBoritekok = new WSO_KuldKuldemenyek_EREC_eMailBoritekok();
            param.MyEREC_eMailBoritekok.IdField = emailBoritekId;
            param.MyEREC_eMailBoritekok.BaseDocument = new WSO_KuldKuldemenyek_BaseDocument() { ver = "1" };
            return param;
        }
        private void SetParameters_EmailErkeztetesIktatas_ExecParam(ref WSO_EmailErkeztetesIktatas param, string inUserId, string inUserGroupId)
        {
            if (param.MyEXEC_Param == null)
                param.MyEXEC_Param = new WSO_EmailErkeztetesIktatas_ExecParam();

            param.MyEXEC_Param.Felhasznalo_IdField = inUserId;
            param.MyEXEC_Param.FelhasznaloSzervezet_IdField = inUserGroupId;
        }
        #endregion

        #region SET PARAMS - EMAILBORITEK
        private WSO_EmailBoritekInsertAndAttachDoc SetParameters_EmailBoritekok(MimeMessage message, DBO_ManagedEmailParameters emailParameters)
        {
            WSO_EmailBoritekInsertAndAttachDoc param = new WSO_EmailBoritekInsertAndAttachDoc();
            SetParameters_EmailBoritekok_Attachments(message, ref param);
            SetParameters_EmailBoritekok_Envelopes(message, ref param);
            SetParameters_EmailBoritekok_ExecParam(ref param, emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId);
            param.EgyebparancsXml = string.Format("<params><tartalomSHA1>{0}</tartalomSHA1></params>", CommonFunctions.SHA1Hash(GetMailBodyString(message)));
            param.KuldesMod = "Felhasznalo";
            return param;
        }
        private void SetParameters_EmailBoritekok_Attachments(MimeMessage message, ref WSO_EmailBoritekInsertAndAttachDoc param)
        {
            WSO_EmailBoritekInsertAndAttachDoc_Attachment tempAttachment = null;

            #region ATTACHMENTS
            if (message.Attachments != null && message.Attachments.Count() > 0)
            {
                param.MyAttachments = new List<WSO_EmailBoritekInsertAndAttachDoc_Attachment>();

                foreach (MimeEntity attachment in message.Attachments)
                {
                    tempAttachment = new WSO_EmailBoritekInsertAndAttachDoc_Attachment();
                    tempAttachment.TartalomField = CommonFunctions.ConvertMailAttachmentToByteArray(attachment);
                    if (attachment is MimePart part)
                        tempAttachment.NevField = part.FileName;
                    else
                        tempAttachment.NevField = "Ismeretlen";

                    param.MyAttachments.Add(tempAttachment);
                }
            }
            #endregion

            #region EMAIL ITSELF AS ATTACHMENT
            tempAttachment = new WSO_EmailBoritekInsertAndAttachDoc_Attachment();
            tempAttachment.TartalomField = CommonFunctions.ConvertMailToByteArray(message);
            tempAttachment.NevField = AxisConstants.EmailFileName;
            param.MyAttachments.Add(tempAttachment);
            #endregion
        }
        private void SetParameters_EmailBoritekok_Envelopes(MimeMessage message, ref WSO_EmailBoritekInsertAndAttachDoc param)
        {
            if (param.MyMailEnvelopes == null)
                param.MyMailEnvelopes = new WSO_EmailBoritekInsertAndAttachDoc_MailEnvelopes();

            param.MyMailEnvelopes.allapotField = null;
            param.MyMailEnvelopes.ccField = message.Cc.ToString();
            param.MyMailEnvelopes.cimzettField = message.To.Mailboxes.FirstOrDefault().Address;
            param.MyMailEnvelopes.digitalisAlairasField = null;
            param.MyMailEnvelopes.emailForrasField = null;
            param.MyMailEnvelopes.emailGuidField = message.MessageId;
            param.MyMailEnvelopes.erkezesDatumaField = message.Date == null ? null : message.Date.ToString();
            param.MyMailEnvelopes.ervKezdField = null;
            param.MyMailEnvelopes.ervVegeField = null;
            param.MyMailEnvelopes.feladasDatumaField = "";
            param.MyMailEnvelopes.feladoField = message.From == null ? null : message.From.Mailboxes.FirstOrDefault().Address;
            param.MyMailEnvelopes.feldolgozasIdoField = null;
            param.MyMailEnvelopes.fontossagField = message.Importance.ToString();
            param.MyMailEnvelopes.idField = null;
            param.MyMailEnvelopes.iraIrat_IdField = null;
            param.MyMailEnvelopes.kuldKuldemeny_IdField = null;
            param.MyMailEnvelopes.targyField = message.Subject;
            param.MyMailEnvelopes.uzenetField = GetMailBodyString(message);
        }

        private string GetMailBodyString(MimeMessage message)
        {
            if (message.Body == null)
                return null;
            else if (message.Body.GetType() == typeof(TextPart))
                return ((TextPart)message.Body).Text;
            else if (message.Body.GetType() == typeof(MultipartRelated))
                return message.Body.ToString();
            else
                return message.Body.ToString();
        }
        private void SetParameters_EmailBoritekok_ExecParam(ref WSO_EmailBoritekInsertAndAttachDoc param, string inUserId, string inUserGroupId)
        {
            if (param.MyExecParam == null)
                param.MyExecParam = new WSO_EmailBoritek_ExecParam();

            param.MyExecParam.Felhasznalo_IdField = inUserId;
            param.MyExecParam.FelhasznaloSzervezet_IdField = inUserGroupId;
        }
        #endregion

        #region ERKEZTETO, IKTATO AZONOSITO BY ID
        public WSO_DataSetResult GetKuldKuldemenyById(DBO_ManagedEmailParameters emailParameters, string id)
        {
            return ProxyKuldKuldemenyek.GetKuldKuldemenyById(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, id);
        }
        public WSO_DataSetResult GetIraIratokById(DBO_ManagedEmailParameters emailParameters, string id)
        {
            return ProxyIraIratok.GetIraIratokById(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, id);
        }
        #endregion

        #region EADMIN
        #region KRT_FELHASZNALOK
        public WSO_DataSetResult GetFelhasznaloById(DBO_ManagedEmailParameters emailParameters, string id)
        {
            return ProxyKrtFelhasznalok.GetFelhasznaloById(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, id);
        }

        public WSO_DataSetResult GetCsoportById(DBO_ManagedEmailParameters emailParameters, string id)
        {
            return ProxyKrtCsoportok.GetCsoportById(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, id);
        }
        #endregion

        #region KRT_PARAMETEREK
        public WSO_DataSetResult GetParameterekByNev(DBO_ManagedEmailParameters emailParameters, string nev)
        {
            return ProxyKrtParameterek.GetParameterekByNev(emailParameters.FelhasznaloId, emailParameters.FelhasznaloSzervezetId, nev);
        }
        #endregion
        #endregion
    }
}