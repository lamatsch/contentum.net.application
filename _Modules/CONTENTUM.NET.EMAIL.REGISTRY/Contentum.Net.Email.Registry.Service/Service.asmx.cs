﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Contentum.Net.Email.Registry.Service
{

    [WebService(Namespace = "Contentum.Net.Email.Registry.Service")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
   // [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod]
        public string ProcessEmails()
        {
            string result = "";
            try
            {
                MainManager man = new MainManager();
                man.Start();
                result = "Lefutott a végrehajtás.";
            }
            catch(Exception e)
            {
                result += string.Format("Hiba a feldolgozás során: {0} Stack: {1}", e.Message, e.StackTrace);
            }
           
            return result;
        }
    }
}
