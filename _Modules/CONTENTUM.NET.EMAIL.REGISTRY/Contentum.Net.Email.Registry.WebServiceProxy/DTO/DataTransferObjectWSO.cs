﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using System.Collections.Generic;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO
{
    public static class DataTransferObjectForWSO
    {
        public static ServiceReferenceEmailBoritekok.ExecParam Convert(WSO_EmailBoritek_ExecParam item)
        {
            ServiceReferenceEmailBoritekok.ExecParam result = new ServiceReferenceEmailBoritekok.ExecParam();
            result.Alkalmazas_Id = item.Alkalmazas_IdField;
            result.CallingChain = item.CallingChainField;
            result.CsoportTag_Id = item.CsoportTag_IdField;
            result.Fake = item.FakeField;
            result.FelhasznaloSzervezet_Id = item.FelhasznaloSzervezet_IdField;
            result.Felhasznalo_Id = item.Felhasznalo_IdField;
            result.FunkcioKod = item.FunkcioKodField;
            result.Helyettesites_Id = item.Helyettesites_IdField;
            result.LoginUser_Id = item.LoginUser_IdField;
            result.Org_Id = item.Org_IdField;
            result.Page_Id = item.Page_IdField;
            result.Paging = null;// item.PagingField;
            result.Record_Id = item.Record_IdField;
            result.Typed = null;// item.TypedField;
            result.UIAccessLog_Id = item.UIAccessLog_IdField;
            result.UserHostAddress = item.UserHostAddressField;

            return result;
        }
        public static ServiceReferenceEmailBoritekok.EREC_eMailBoritekok Convert(WSO_EmailBoritekInsertAndAttachDoc_MailEnvelopes item)
        {
            ServiceReferenceEmailBoritekok.EREC_eMailBoritekok result = new ServiceReferenceEmailBoritekok.EREC_eMailBoritekok();
            result.Allapot = item.allapotField;
            result.Base = null;// item.BaseField;
            result.CC = item.ccField;
            result.Cimzett = item.cimzettField;
            result.DigitalisAlairas = item.digitalisAlairasField;
            result.EmailForras = item.emailForrasField;
            result.EmailGuid = item.emailGuidField;
            result.ErkezesDatuma = item.erkezesDatumaField;
            result.ErvKezd = item.ervKezdField;
            result.ErvVege = item.ervVegeField;
            result.FeladasDatuma = item.feladasDatumaField;
            result.Felado = item.feladoField;
            result.FeldolgozasIdo = item.feldolgozasIdoField;
            result.Fontossag = item.fontossagField;
            result.ForrasTipus = item.forrasTipusField;
            result.Id = item.idField;
            result.IraIrat_Id = item.iraIrat_IdField;
            result.KuldKuldemeny_Id = item.kuldKuldemeny_IdField;
            result.Targy = item.targyField;
            result.Typed = null;// item.TypedField;
            result.Updated = null;// item.UpdatedField;
            result.Uzenet = item.uzenetField;
            return result;
        }
        public static ServiceReferenceEmailBoritekok.Csatolmany Convert(WSO_EmailBoritekInsertAndAttachDoc_Attachment item)
        {
            ServiceReferenceEmailBoritekok.Csatolmany result = new ServiceReferenceEmailBoritekok.Csatolmany();
            result.AlairasAdatok = null;// item.AlairasAdatok;
            result.BarCode = item.BarCodeField;
            result.ElektronikusAlairas = item.ElektronikusAlairasField;
            result.Megnyithato = item.MegnyithatoField;
            result.Nev = item.NevField;
            result.OCRAdatok = null;// item.OCRAdatok;
            result.Olvashato = item.OlvashatoField;
            result.SourceSharePath = item.SourceSharePathField;
            result.Tartalom = item.TartalomField;
            result.TartalomHash = item.TartalomHashField;
            result.Titkositas = item.TitkositasField;

            return result;
        }

        public static List<ServiceReferenceEmailBoritekok.Csatolmany> Convert(List<WSO_EmailBoritekInsertAndAttachDoc_Attachment> items)
        {
            if (items == null || items.Count < 1)
                return null;
            List<ServiceReferenceEmailBoritekok.Csatolmany> result = new List<ServiceReferenceEmailBoritekok.Csatolmany>();

            foreach (var item in items)
                result.Add(Convert(item));

            return result;
        }

        #region IKTATAS
        public static ServiceReferenceEmailErkeztetesIktatas.ExecParam Convert(WSO_EmailErkeztetesIktatas_ExecParam item)
        {
            ServiceReferenceEmailErkeztetesIktatas.ExecParam result = new ServiceReferenceEmailErkeztetesIktatas.ExecParam();
            result.Alkalmazas_Id = item.Alkalmazas_IdField;
            result.CallingChain = item.CallingChainField;
            result.CsoportTag_Id = item.CsoportTag_IdField;
            result.Fake = item.FakeField;
            result.FelhasznaloSzervezet_Id = item.FelhasznaloSzervezet_IdField;
            result.Felhasznalo_Id = item.Felhasznalo_IdField;
            result.FunkcioKod = item.funkcioKodField;
            result.Helyettesites_Id = item.Helyettesites_IdField;
            result.LoginUser_Id = item.LoginUser_IdField;
            result.Org_Id = null;
            result.Page_Id = item.Page_IdField;
            result.Paging = null;// item.PagingField;
            result.Record_Id = item.Record_IdField;
            result.Typed = null;// item.TypedField;
            result.UIAccessLog_Id = item.UIAccessLog_IdField;
            result.UserHostAddress = item.UserHostAddressField;
            return result;
        }

        public static ServiceReferenceEmailErkeztetesIktatas.EREC_KuldKuldemenyek Convert(WSO_EmailErkeztetesIktatas_EREC_KuldKuldemenyek item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_KuldKuldemenyek result = new ServiceReferenceEmailErkeztetesIktatas.EREC_KuldKuldemenyek();
            result.AdathordozoTipusa = item.AdathordozoTipusa;
            result.Ajanlott = item.Ajanlott;
            result.Allapot = item.Allapot;
            result.Ar = item.Ar;
            result.Azonosito = item.Azonosito;
            result.BarCode = item.BarCode;
            result.Base = null;// item.Base;
            result.BeerkezesIdeje = item.BeerkezesIdeje;
            result.BelyegzoDatuma = item.BelyegzoDatuma;
            result.BontasiMegjegyzes = item.BontasiMegjegyzes;
            result.BoritoTipus = item.BoritoTipus;
            result.CimSTR_Bekuldo = item.CimSTR_Bekuldo;
            result.CimzesTipusa = item.CimzesTipusa;
            result.Cim_Id = item.Cim_Id;
            result.CsoportFelelosEloszto_Id = item.CsoportFelelosEloszto_Id;
            result.Csoport_Id_Cimzett = item.Csoport_Id_Cimzett;
            result.Csoport_Id_Felelos = item.Csoport_Id_Felelos;
            result.Csoport_Id_Felelos_Elozo = item.Csoport_Id_Felelos_Elozo;
            result.Elektronikus_Kezbesitesi_Allap = item.Elektronikus_Kezbesitesi_Allap;
            result.Elsobbsegi = item.Elsobbsegi;
            result.ElsodlegesAdathordozoTipusa = item.ElsodlegesAdathordozoTipusa;
            result.Erkeztetes_Ev = item.Erkeztetes_Ev;
            result.Erkezteto_Szam = item.Erkezteto_Szam;
            result.ErvKezd = item.ErvKezd;
            result.ErvVege = item.ErvVege;
            result.ExpedialasIdeje = item.ExpedialasIdeje;
            result.E_elorejelzes = item.E_elorejelzes;
            result.E_ertesites = item.E_ertesites;
            result.FelbontasDatuma = item.FelbontasDatuma;
            result.FelhasznaloCsoport_Id_Alairo = item.FelhasznaloCsoport_Id_Alairo;
            result.FelhasznaloCsoport_Id_Atvevo = item.FelhasznaloCsoport_Id_Atvevo;
            result.FelhasznaloCsoport_Id_Bonto = item.FelhasznaloCsoport_Id_Bonto;
            result.FelhasznaloCsoport_Id_Expedial = item.FelhasznaloCsoport_Id_Expedial;
            result.FelhasznaloCsoport_Id_Orzo = item.FelhasznaloCsoport_Id_Orzo;
            result.Fizikai_Kezbesitesi_Allapot = item.Fizikai_Kezbesitesi_Allapot;
            result.HivatkozasiSzam = item.HivatkozasiSzam;
            result.Id = item.Id;
            result.IktatastNemIgenyel = item.IktatastNemIgenyel;
            result.Iktathato = item.Iktathato;
            result.IktatniKell = item.IktatniKell;
            result.IraIktatokonyv_Id = item.IraIktatokonyv_Id;
            result.IraIratok_Id = item.IraIratok_Id;
            result.KezbesitesModja = item.KezbesitesModja;
            result.KimenoKuldemenyFajta = item.KimenoKuldemenyFajta;
            result.KimenoKuld_Sorszam = item.KimenoKuld_Sorszam;
            result.Kovetkezo_Felelos_Id = item.Kovetkezo_Felelos_Id;
            result.Kovetkezo_Orzo_Id = item.Kovetkezo_Orzo_Id;
            result.KuldesMod = item.KuldesMod;
            result.KuldKuldemeny_Id_Szulo = item.KuldKuldemeny_Id_Szulo;
            result.MegorzesJelzo = item.MegorzesJelzo;
            result.MegtagadasDat = item.MegtagadasDat;
            result.MegtagadasIndoka = item.MegtagadasIndoka;
            result.Megtagado_Id = item.Megtagado_Id;
            result.Minosites = item.Minosites;
            result.Munkaallomas = item.Munkaallomas;
            result.NevSTR_Bekuldo = item.NevSTR_Bekuldo;
            result.Partner_Id_Bekuldo = item.Partner_Id_Bekuldo;
            result.PeldanySzam = item.PeldanySzam;
            result.PostaiLezaroSzolgalat = item.PostaiLezaroSzolgalat;
            result.PostazasIranya = item.PostazasIranya;
            result.RagSzam = item.RagSzam;
            result.SajatKezbe = item.SajatKezbe;
            result.SerultKuldemeny = item.SerultKuldemeny;
            result.Surgosseg = item.Surgosseg;
            result.SztornirozasDat = item.SztornirozasDat;
            result.Targy = item.Targy;
            result.Tartalom = item.Tartalom;
            result.Tertiveveny = item.Tertiveveny;
            result.TevesCimzes = item.TevesCimzes;
            result.TevesErkeztetes = item.TevesErkeztetes;
            result.Tipus = item.Tipus;
            result.TovabbitasAlattAllapot = item.TovabbitasAlattAllapot;
            result.Tovabbito = item.Tovabbito;
            result.Typed = null;// item.Typed;
            result.UgyintezesModja = item.UgyintezesModja;
            result.Updated = null;// item.Updated;

            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_eMailBoritekok Convert(WSO_EmailErkeztetesIktatas_EREC_eMailBoritekok item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_eMailBoritekok result = new ServiceReferenceEmailErkeztetesIktatas.EREC_eMailBoritekok();
            result.Allapot = item.AllapotField;
            result.Base = null;
            result.CC = item.CcField;
            result.Cimzett = item.CimzettField;
            result.DigitalisAlairas = item.DigitalisAlairasField;
            result.EmailForras = item.EmailForrasField;
            result.EmailGuid = item.EmailGuidField;
            result.ErkezesDatuma = item.ErkezesDatumaField;
            result.ErvKezd = item.ErvKezdField;
            result.ErvVege = item.ErvVegeField;
            result.FeladasDatuma = item.FeladasDatumaField;
            result.Felado = item.FeladoField;
            result.FeldolgozasIdo = item.FeldolgozasIdoField;
            result.Fontossag = item.FontossagField;
            result.ForrasTipus = item.ForrasTipusField;
            result.Id = item.IdField;
            result.IraIrat_Id = item.IraIrat_IdField;
            result.KuldKuldemeny_Id = item.KuldKuldemeny_IdField;
            result.Targy = item.TargyField;
            result.Typed = null;
            result.Updated = null;
            result.Uzenet = item.UzenetField;

            result.Base = Convert(item.BaseDocument);

            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratok Convert(WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratok item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratok result = new ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratok();

            result.Alkalmazas_Id = item.alkalmazas_Id;
            result.Allapot = item.allapot;
            result.Azonosito = item.azonosito;
            result.BARCODE = item.bARCODE;
            result.Base = null;// item.Base;
            result.CimSTR_Ugyindito = item.cimSTR_Ugyindito;
            result.Cim_Id_Ugyindito = item.cim_Id_Ugyindito;
            result.Csoport_Id_Cimzett = item.csoport_Id_Cimzett;
            result.Csoport_Id_Felelos = item.csoport_Id_Felelos;
            result.Csoport_Id_Felelos_Elozo = item.csoport_Id_Felelos_Elozo;
            result.Csoport_Id_Ugyfelelos = item.csoport_Id_Ugyfelelos;
            result.Elektronikus_Kezbesitesi_Allap = item.elektronikus_Kezbesitesi_Allap;
            result.ElintezesDat = item.elintezesDat;
            result.ElintezesMod = item.ervKezd;
            result.ErvKezd = item.ervKezd;
            result.ErvVege = item.ervVege;
            result.FelhasznaloCsoport_Id_Orzo = item.felhasznaloCsoport_Id_Orzo;
            result.FelhasznaloCsoport_Id_Ugyintez = item.felhasznaloCsoport_Id_Ugyintez;
            result.FelhCsoport_Id_Felulvizsgalo = item.felhCsoport_Id_Felulvizsgalo;
            result.FelhCsoport_Id_IrattariAtvevo = item.felhCsoport_Id_IrattariAtvevo;
            result.FelhCsoport_Id_Selejtezo = item.felhCsoport_Id_Selejtezo;
            result.FelulvizsgalatDat = item.felulvizsgalatDat;
            result.Fizikai_Kezbesitesi_Allapot = item.fizikai_Kezbesitesi_Allapot;
            result.Foszam = item.foszam;
            result.GeneraltTargy = item.generaltTargy;
            result.Hatarido = item.hatarido;
            result.Id = item.id;
            result.IktatoszamKieg = item.iktatoszamKieg;
            result.IraIktatokonyv_Id = item.iraIktatokonyv_Id;
            result.IraIrattariTetel_Id = item.iraIrattariTetel_Id;
            result.IratMetadefinicio_Id = item.iratMetadefinicio_Id;
            result.IratSzam = item.iratSzam;
            result.IrattarbaKuldDatuma = item.irattarbaKuldDatuma;
            result.IrattarbaVetelDat = item.irattarbaVetelDat;
            result.IrattariHely = item.irattariHely;
            result.Jelleg = item.jelleg;
            result.Kolcsonhatarido = item.kolcsonhatarido;
            result.KolcsonKiadDat = item.kolcsonKiadDat;
            result.KolcsonKikerDat = item.kolcsonKikerDat;
            result.Kovetkezo_Felelos_Id = item.kovetkezo_Felelos_Id;
            result.Kovetkezo_Orzo_Id = item.kovetkezo_Orzo_Id;
            result.LeveltariAtvevoNeve = item.leveltariAtvevoNeve;
            result.LezarasDat = item.lezarasDat;
            result.Megjegyzes = item.megjegyzes;
            result.MegorzesiIdoVege = item.megorzesiIdoVege;
            result.NevSTR_Ugyindito = item.nevSTR_Ugyindito;
            result.Partner_Id_Ugyindito = item.partner_Id_Ugyindito;
            result.RegirendszerIktatoszam = item.regirendszerIktatoszam;
            result.SelejtezesDat = item.selejtezesDat;
            result.SkontrobaDat = item.skontrobaDat;
            result.SkontrobanOsszesen = item.skontrobanOsszesen;
            result.SkontroOka = item.skontroOka;
            result.SkontroVege = item.skontroVege;
            result.Sorszam = item.sorszam;
            result.Surgosseg = item.surgosseg;
            result.SztornirozasDat = item.sztornirozasDat;
            result.Targy = item.targy;
            result.TovabbitasAlattAllapot = item.tovabbitasAlattAllapot;
            result.Typed = null;// item.Typed;
            result.Ugyazonosito = item.ugyazonosito;
            result.UgyintezesModja = item.ugyintezesModja;
            result.UgyTipus = item.ugyTipus;
            result.UgyUgyirat_Id_Kulso = item.ugyUgyirat_Id_Kulso;
            result.UgyUgyirat_Id_Szulo = item.ugyUgyirat_Id_Szulo;
            result.UjOrzesiIdo = item.ujOrzesiIdo;
            result.Updated = null;// item.Updated;
            result.UtolsoAlszam = item.utolsoAlszam;
            result.UtolsoSorszam = item.utolsoSorszam;

            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratdarabok Convert(WSO_EmailErkeztetesIktatas_EREC_UgyUgyiratdarabok item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratdarabok result = new ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratdarabok();
            result.Allapot = item.allapotField;
            result.Azonosito = item.azonositoField;
            result.Base = null;//.    ;
            result.Csoport_Id_Felelos = item.csoport_Id_FelelosField;
            result.Csoport_Id_Felelos_Elozo = item.csoport_Id_Felelos_ElozoField;
            result.ElintezesDat = item.elintezesDatField;
            result.ElintezesMod = item.elintezesModField;
            result.EljarasiSzakasz = item.eljarasiSzakaszField;
            result.ErvKezd = item.ervKezdField;
            result.ErvVege = item.ervVegeField;
            result.FelhasznaloCsoport_Id_Ugyintez = item.felhasznaloCsoport_Id_UgyintezField;
            result.Foszam = item.foszamField;
            result.Hatarido = item.hataridoField;
            result.Id = item.idField;
            result.IraIktatokonyv_Id = item.iraIktatokonyv_IdField;
            result.Leiras = item.leirasField;
            result.LezarasDat = item.lezarasDatField;
            result.LezarasOka = item.lezarasOkaField;
            result.Sorszam = item.sorszamField;
            result.Typed = null; //item.Typed;
            result.UgyUgyirat_Id = item.ugyUgyirat_IdField;
            result.UgyUgyirat_Id_Elozo = item.ugyUgyirat_Id_ElozoField;
            result.Updated = null;// item.Updated;
            result.UtolsoAlszam = item.utolsoAlszamField;

            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratok Convert(WSO_EmailErkeztetesIktatas_EREC_IraIratok item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratok result = new ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratok();
            result.AdathordozoTipusa = item.AdathordozoTipusa;
            result.Allapot = item.Allapot;
            result.Alszam = item.Alszam;
            result.Azonosito = item.Azonosito;
            result.Base = null;
            result.Csoport_Id_Felelos = item.Csoport_Id_Felelos;
            result.Csoport_Id_Ugyfelelos = item.Csoport_Id_Ugyfelelos;
            result.ErvKezd = item.ErvKezd;
            result.ErvVege = item.ErvVege;
            result.ExpedialasDatuma = item.ExpedialasDatuma;
            result.ExpedialasModja = item.ExpedialasModja;
            result.FelhasznaloCsoport_Id_Expedial = item.FelhasznaloCsoport_Id_Expedial;
            result.FelhasznaloCsoport_Id_Iktato = item.FelhasznaloCsoport_Id_Iktato;
            result.FelhasznaloCsoport_Id_Kiadmany = item.FelhasznaloCsoport_Id_Kiadmany;
            result.FelhasznaloCsoport_Id_Orzo = item.FelhasznaloCsoport_Id_Orzo;
            result.FelhasznaloCsoport_Id_Ugyintez = item.FelhasznaloCsoport_Id_Ugyintez;
            result.GeneraltTargy = item.GeneraltTargy;
            result.Hatarido = item.Hatarido;
            result.HivatkozasiSzam = item.HivatkozasiSzam;
            result.Id = item.Id;
            result.IktatasDatuma = item.IktatasDatuma;
            result.IntezesIdopontja = item.IntezesIdopontja;
            result.IratFajta = item.IratFajta;
            result.IratMetaDef_Id = item.IratMetaDef_Id;
            result.IrattarbaKuldDatuma = item.IrattarbaKuldDatuma;
            result.IrattarbaVetelDat = item.IrattarbaVetelDat;
            result.Irattipus = item.Irattipus;
            result.Jelleg = item.Jelleg;
            result.Kategoria = item.Kategoria;
            result.KiadmanyozniKell = item.KiadmanyozniKell;
            result.KuldKuldemenyek_Id = item.KuldKuldemenyek_Id;
            result.MegorzesiIdo = item.MegorzesiIdo;
            result.Minosites = item.Minosites;
            result.Munkaallomas = item.Munkaallomas;
            result.PostazasIranya = item.PostazasIranya;
            result.Sorszam = item.Sorszam;
            result.SztornirozasDat = item.SztornirozasDat;
            result.Targy = item.Targy;
            result.Typed = null;// item.Typed;
            result.UgyintezesAlapja = item.UgyintezesAlapja;
            result.UgyintezesModja = item.UgyintezesModja;
            result.Ugyirat_Id = item.Ugyirat_Id;
            result.UgyUgyIratDarab_Id = item.UgyUgyIratDarab_Id;
            result.Updated = null;// item.Updated;
            result.UtolsoSorszam = item.UtolsoSorszam;

            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_HataridosFeladatok Convert(WSO_EmailErkeztetesIktatas_EREC_HataridosFeladatok item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_HataridosFeladatok result = new ServiceReferenceEmailErkeztetesIktatas.EREC_HataridosFeladatok();
            result.Allapot = item.allapot;
            result.Altipus = item.altipus;
            result.Azonosito = item.azonosito;
            result.Base = null;
            result.Csoport_Id_Felelos = item.csoport_Id_Felelos;
            result.Csoport_Id_Kiado = item.csoport_Id_Kiado;
            result.ErvKezd = item.ervKezd;
            result.ErvVege = item.ervVege;
            result.Esemeny_Id_Kivalto = item.esemeny_Id_Kivalto;
            result.Esemeny_Id_Lezaro = item.esemeny_Id_Lezaro;
            result.FeladatDefinicio_Id = item.feladatDefinicio_Id;
            result.FeladatDefinicio_Id_Lezaro = item.feladatDefinicio_Id_Lezaro;
            result.Felhasznalo_Id_Felelos = item.felhasznalo_Id_Felelos;
            result.Felhasznalo_Id_Kiado = item.felhasznalo_Id_Kiado;
            result.Forras = item.forras;
            result.Funkcio_Id_Inditando = item.funkcio_Id_Inditando;
            result.HataridosFeladat_Id = item.hataridosFeladat_Id;
            result.Id = item.id;
            result.IntezkHatarido = item.intezkHatarido;
            result.KezdesiIdo = item.kezdesiIdo;
            result.Leiras = item.leiras;
            result.LezarasDatuma = item.lezarasDatuma;
            result.LezarasPrioritas = item.lezarasPrioritas;
            result.Megoldas = item.megoldas;
            result.Memo = item.memo;
            result.ObjTip_Id = item.objTip_Id;
            result.Obj_Id = item.obj_Id;
            result.Obj_type = item.obj_type;
            result.Prioritas = item.prioritas;
            result.ReszFeladatSorszam = item.reszFeladatSorszam;
            result.Tipus = null;
            result.Typed = null;
            result.Updated = null;
            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.IktatasiParameterek Convert(WSO_EmailErkeztetesIktatas_IktatasiParameterek item)
        {
            ServiceReferenceEmailErkeztetesIktatas.IktatasiParameterek result = new ServiceReferenceEmailErkeztetesIktatas.IktatasiParameterek();
            result.Adoszam = item.Adoszam;
            result.Atiktatas = item.Atiktatas;
            result.Dokumentum = null;// item.Dokumentum;
            result.EmptyUgyiratSztorno = item.EmptyUgyiratSztorno;
            result.IratId = item.IratId;
            result.IratpeldanyVonalkodGeneralasHaNincs = item.IratpeldanyVonalkodGeneralasHaNincs;
            result.Iratpeldany_Vonalkod = item.Iratpeldany_Vonalkod;
            result.KeszitoPeldanyaSzukseges = item.KeszitoPeldanyaSzukseges;
            result.KuldemenyId = item.KuldemenyId;
            result.MunkaPeldany = item.MunkaPeldany;
            result.RegiAdatAzonosito = item.RegiAdatAzonosito;
            result.RegiAdatId = item.RegiAdatId;
            result.SzerelendoUgyiratId = item.SzerelendoUgyiratId;
            result.UgyiratPeldanySzukseges = item.UgyiratPeldanySzukseges;
            result.UgyiratUjHatarido = item.UgyiratUjHatarido;
            result.UgyiratUjranyitasaHaLezart = item.UgyiratUjranyitasaHaLezart;
            result.UgykorId = item.UgykorId;
            result.Ugytipus = item.Ugytipus;
            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.ErkeztetesParameterek Convert(WSO_EmailErkeztetesIktatas_ErkeztetesParameterek item)
        {
            ServiceReferenceEmailErkeztetesIktatas.ErkeztetesParameterek result = new ServiceReferenceEmailErkeztetesIktatas.ErkeztetesParameterek();
            result.Mellekletek = Convert(item.Mellekletek);
            return result;
        }

        public static ServiceReferenceEmailErkeztetesIktatas.BaseDocument Convert(WSO_EmailErkeztetesIktatas_BaseDocument item)
        {
            if (item == null)
                return null;

            ServiceReferenceEmailErkeztetesIktatas.BaseDocument result = new ServiceReferenceEmailErkeztetesIktatas.BaseDocument();
            result.LetrehozasIdo = item.letrehozasIdo;
            result.Letrehozo_id = item.letrehozo_id;
            result.ModositasIdo = item.modositasIdo;
            result.Modosito_id = item.modosito_id;
            result.Note = item.note;
            result.Stat_id = item.stat_id;
            result.Tranz_id = item.tranz_id;
            result.Typed = null;// item.Typed;
            result.UIAccessLog_id = item.uIAccessLog_id;
            result.Updated = null;// item.Updated;
            result.Ver = item.ver;
            result.ZarolasIdo = item.zarolasIdo;
            result.Zarolo_id = item.zarolo_id;
            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek Convert(WSO_EmailErkeztetesIktatas_EREC_Mellekletek item)
        {
            ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek result = new ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek();
            result.AdathordozoTipus = item.adathordozoTipus;
            result.BarCode = item.barCode;
            result.Base = null;
            result.ErvKezd = item.ervKezd;
            result.ErvVege = item.ervVege;
            result.Id = item.id;
            result.IraIrat_Id = item.iraIrat_Id;
            result.KuldKuldemeny_Id = item.kuldKuldemeny_Id;
            result.Megjegyzes = item.megjegyzes;
            result.MellekletAdathordozoTipus = item.mellekletAdathordozoTipus;
            result.Mennyiseg = item.mennyiseg;
            result.MennyisegiEgyseg = item.mennyisegiEgyseg;
            result.SztornirozasDat = item.sztornirozasDat;
            result.Typed = null;
            result.Updated = null;
            return result;
        }
        public static ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek[] Convert(WSO_EmailErkeztetesIktatas_EREC_Mellekletek[] items)
        {
            if (items == null)
                return null;

            List<ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek> result = new List<ServiceReferenceEmailErkeztetesIktatas.EREC_Mellekletek>();
            foreach (var item in items)
            {
                result.Add(Convert(item));
            }
            return result.ToArray();
        }

        #endregion

        public static WSO_DataSetResult Convert(ServiceReferenceIraIktatokonyvekService.Result item)
        {
            if (item == null)
                return null;

            WSO_DataSetResult result = new WSO_DataSetResult();
            result.ds = item.Ds;
            result.errorCode = item.ErrorCode;
            result.errorMessage = item.ErrorMessage;
            result.errorType = item.ErrorType;
            result.isLogExist = item.IsLogExist;
            result.isUpdated = item.IsUpdated;
            result.record = item.Record;
            result.uid = item.Uid;
            return result;
        }

        public static WSO_DataSetResult Convert(ServiceReferenceIrattariTetelekService.Result item)
        {
            if (item == null)
                return null;

            WSO_DataSetResult result = new WSO_DataSetResult();
            result.ds = item.Ds;
            result.errorCode = item.ErrorCode;
            result.errorMessage = item.ErrorMessage;
            result.errorType = item.ErrorType;
            result.isLogExist = item.IsLogExist;
            result.isUpdated = item.IsUpdated;
            result.record = item.Record;
            result.uid = item.Uid;
            return result;
        }

        #region KULDKULDEMENYEK
        public static ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyek Convert(WSO_KuldKuldemenyek_EREC_KuldKuldemenyek item)
        {
            ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyek result = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyek();
            result.AdathordozoTipusa = item.AdathordozoTipusa;
            result.Ajanlott = item.Ajanlott;
            result.Allapot = item.Allapot;
            result.Ar = item.Ar;
            result.Azonosito = item.Azonosito;
            result.BarCode = item.BarCode;
            result.Base = null;// item.Base;
            result.BeerkezesIdeje = item.BeerkezesIdeje;
            result.BelyegzoDatuma = item.BelyegzoDatuma;
            result.BontasiMegjegyzes = item.BontasiMegjegyzes;
            result.BoritoTipus = item.BoritoTipus;
            result.CimSTR_Bekuldo = item.CimSTR_Bekuldo;
            result.CimzesTipusa = item.CimzesTipusa;
            result.Cim_Id = item.Cim_Id;
            result.CsoportFelelosEloszto_Id = item.CsoportFelelosEloszto_Id;
            result.Csoport_Id_Cimzett = item.Csoport_Id_Cimzett;
            result.Csoport_Id_Felelos = item.Csoport_Id_Felelos;
            result.Csoport_Id_Felelos_Elozo = item.Csoport_Id_Felelos_Elozo;
            result.Elektronikus_Kezbesitesi_Allap = item.Elektronikus_Kezbesitesi_Allap;
            result.Elsobbsegi = item.Elsobbsegi;
            result.ElsodlegesAdathordozoTipusa = item.ElsodlegesAdathordozoTipusa;
            result.Erkeztetes_Ev = item.Erkeztetes_Ev;
            result.Erkezteto_Szam = item.Erkezteto_Szam;
            result.ErvKezd = item.ErvKezd;
            result.ErvVege = item.ErvVege;
            result.ExpedialasIdeje = item.ExpedialasIdeje;
            result.E_elorejelzes = item.E_elorejelzes;
            result.E_ertesites = item.E_ertesites;
            result.FelbontasDatuma = item.FelbontasDatuma;
            result.FelhasznaloCsoport_Id_Alairo = item.FelhasznaloCsoport_Id_Alairo;
            result.FelhasznaloCsoport_Id_Atvevo = item.FelhasznaloCsoport_Id_Atvevo;
            result.FelhasznaloCsoport_Id_Bonto = item.FelhasznaloCsoport_Id_Bonto;
            result.FelhasznaloCsoport_Id_Expedial = item.FelhasznaloCsoport_Id_Expedial;
            result.FelhasznaloCsoport_Id_Orzo = item.FelhasznaloCsoport_Id_Orzo;
            result.Fizikai_Kezbesitesi_Allapot = item.Fizikai_Kezbesitesi_Allapot;
            result.HivatkozasiSzam = item.HivatkozasiSzam;
            result.Id = item.Id;
            result.IktatastNemIgenyel = item.IktatastNemIgenyel;
            result.Iktathato = item.Iktathato;
            result.IktatniKell = item.IktatniKell;
            result.IraIktatokonyv_Id = item.IraIktatokonyv_Id;
            result.IraIratok_Id = item.IraIratok_Id;
            result.KezbesitesModja = item.KezbesitesModja;
            result.KimenoKuldemenyFajta = item.KimenoKuldemenyFajta;
            result.KimenoKuld_Sorszam = item.KimenoKuld_Sorszam;
            result.Kovetkezo_Felelos_Id = item.Kovetkezo_Felelos_Id;
            result.Kovetkezo_Orzo_Id = item.Kovetkezo_Orzo_Id;
            result.KuldesMod = item.KuldesMod;
            result.KuldKuldemeny_Id_Szulo = item.KuldKuldemeny_Id_Szulo;
            result.MegorzesJelzo = item.MegorzesJelzo;
            result.MegtagadasDat = item.MegtagadasDat;
            result.MegtagadasIndoka = item.MegtagadasIndoka;
            result.Megtagado_Id = item.Megtagado_Id;
            result.Minosites = item.Minosites;
            result.Munkaallomas = item.Munkaallomas;
            result.NevSTR_Bekuldo = item.NevSTR_Bekuldo;
            result.Partner_Id_Bekuldo = item.Partner_Id_Bekuldo;
            result.PeldanySzam = item.PeldanySzam;
            result.PostaiLezaroSzolgalat = item.PostaiLezaroSzolgalat;
            result.PostazasIranya = item.PostazasIranya;
            result.RagSzam = item.RagSzam;
            result.SajatKezbe = item.SajatKezbe;
            result.SerultKuldemeny = item.SerultKuldemeny;
            result.Surgosseg = item.Surgosseg;
            result.SztornirozasDat = item.SztornirozasDat;
            result.Targy = item.Targy;
            result.Tartalom = item.Tartalom;
            result.Tertiveveny = item.Tertiveveny;
            result.TevesCimzes = item.TevesCimzes;
            result.TevesErkeztetes = item.TevesErkeztetes;
            result.Tipus = item.Tipus;
            result.TovabbitasAlattAllapot = item.TovabbitasAlattAllapot;
            result.Tovabbito = item.Tovabbito;
            result.Typed = null;// item.Typed;
            result.UgyintezesModja = item.UgyintezesModja;
            result.Updated = null;// item.Updated;

            return result;
        }
        public static ServiceReferenceKuldKuldemenyek.EREC_eMailBoritekok Convert(WSO_KuldKuldemenyek_EREC_eMailBoritekok item)
        {
            ServiceReferenceKuldKuldemenyek.EREC_eMailBoritekok result = new ServiceReferenceKuldKuldemenyek.EREC_eMailBoritekok();
            result.Allapot = item.AllapotField;
            result.Base = Convert(item.BaseDocument);
            result.CC = item.CcField;
            result.Cimzett = item.CimzettField;
            result.DigitalisAlairas = item.DigitalisAlairasField;
            result.EmailForras = item.EmailForrasField;
            result.EmailGuid = item.EmailGuidField;
            result.ErkezesDatuma = item.ErkezesDatumaField;
            result.ErvKezd = item.ErvKezdField;
            result.ErvVege = item.ErvVegeField;
            result.FeladasDatuma = item.FeladasDatumaField;
            result.Felado = item.FeladoField;
            result.FeldolgozasIdo = item.FeldolgozasIdoField;
            result.Fontossag = item.FontossagField;
            result.ForrasTipus = item.ForrasTipusField;
            result.Id = item.IdField;
            result.IraIrat_Id = item.IraIrat_IdField;
            result.KuldKuldemeny_Id = item.KuldKuldemeny_IdField;
            result.Targy = item.TargyField;
            result.Typed = null;
            result.Updated = null;
            result.Uzenet = item.UzenetField;

            //result.Base = Convert(item.BaseDocument);

            return result;
        }

        public static ServiceReferenceKuldKuldemenyek.EREC_HataridosFeladatok Convert(WSO_KuldKuldemenyek_EREC_HataridosFeladatok item)
        {
            ServiceReferenceKuldKuldemenyek.EREC_HataridosFeladatok result = new ServiceReferenceKuldKuldemenyek.EREC_HataridosFeladatok();
            result.Allapot = item.allapot;
            result.Altipus = item.altipus;
            result.Azonosito = item.azonosito;
            result.Base = null;
            result.Csoport_Id_Felelos = item.csoport_Id_Felelos;
            result.Csoport_Id_Kiado = item.csoport_Id_Kiado;
            result.ErvKezd = item.ervKezd;
            result.ErvVege = item.ervVege;
            result.Esemeny_Id_Kivalto = item.esemeny_Id_Kivalto;
            result.Esemeny_Id_Lezaro = item.esemeny_Id_Lezaro;
            result.FeladatDefinicio_Id = item.feladatDefinicio_Id;
            result.FeladatDefinicio_Id_Lezaro = item.feladatDefinicio_Id_Lezaro;
            result.Felhasznalo_Id_Felelos = item.felhasznalo_Id_Felelos;
            result.Felhasznalo_Id_Kiado = item.felhasznalo_Id_Kiado;
            result.Forras = item.forras;
            result.Funkcio_Id_Inditando = item.funkcio_Id_Inditando;
            result.HataridosFeladat_Id = item.hataridosFeladat_Id;
            result.Id = item.id;
            result.IntezkHatarido = item.intezkHatarido;
            result.KezdesiIdo = item.kezdesiIdo;
            result.Leiras = item.leiras;
            result.LezarasDatuma = item.lezarasDatuma;
            result.LezarasPrioritas = item.lezarasPrioritas;
            result.Megoldas = item.megoldas;
            result.Memo = item.memo;
            result.ObjTip_Id = item.objTip_Id;
            result.Obj_Id = item.obj_Id;
            result.Obj_type = item.obj_type;
            result.Prioritas = item.prioritas;
            result.ReszFeladatSorszam = item.reszFeladatSorszam;
            result.Tipus = null;
            result.Typed = null;
            result.Updated = null;
            return result;
        }
        public static ServiceReferenceKuldKuldemenyek.ErkeztetesParameterek Convert(WSO_KuldKuldemenyek_ErkeztetesParameterek item)
        {
            ServiceReferenceKuldKuldemenyek.ErkeztetesParameterek result = new ServiceReferenceKuldKuldemenyek.ErkeztetesParameterek();
            result.Mellekletek = Convert(item.Mellekletek);
            return result;
        }
        public static ServiceReferenceKuldKuldemenyek.EREC_Mellekletek Convert(WSO_KuldKuldemenyek_EREC_Mellekletek item)
        {
            ServiceReferenceKuldKuldemenyek.EREC_Mellekletek result = new ServiceReferenceKuldKuldemenyek.EREC_Mellekletek();
            result.AdathordozoTipus = item.adathordozoTipus;
            result.BarCode = item.barCode;
            result.Base = null;
            result.ErvKezd = item.ervKezd;
            result.ErvVege = item.ervVege;
            result.Id = item.id;
            result.IraIrat_Id = item.iraIrat_Id;
            result.KuldKuldemeny_Id = item.kuldKuldemeny_Id;
            result.Megjegyzes = item.megjegyzes;
            result.MellekletAdathordozoTipus = item.mellekletAdathordozoTipus;
            result.Mennyiseg = item.mennyiseg;
            result.MennyisegiEgyseg = item.mennyisegiEgyseg;
            result.SztornirozasDat = item.sztornirozasDat;
            result.Typed = null;
            result.Updated = null;
            return result;
        }
        public static ServiceReferenceKuldKuldemenyek.EREC_Mellekletek[] Convert(WSO_KuldKuldemenyek_EREC_Mellekletek[] items)
        {
            if (items == null)
                return null;

            List<ServiceReferenceKuldKuldemenyek.EREC_Mellekletek> result = new List<ServiceReferenceKuldKuldemenyek.EREC_Mellekletek>();
            foreach (var item in items)
            {
                result.Add(Convert(item));
            }
            return result.ToArray();
        }
        public static ServiceReferenceKuldKuldemenyek.BaseDocument Convert(WSO_KuldKuldemenyek_BaseDocument item)
        {
            if (item == null)
                return null;

            ServiceReferenceKuldKuldemenyek.BaseDocument result = new ServiceReferenceKuldKuldemenyek.BaseDocument();
            result.LetrehozasIdo = item.letrehozasIdo;
            result.Letrehozo_id = item.letrehozo_id;
            result.ModositasIdo = item.modositasIdo;
            result.Modosito_id = item.modosito_id;
            result.Note = item.note;
            result.Stat_id = item.stat_id;
            result.Tranz_id = item.tranz_id;
            result.Typed = null;// item.Typed;
            result.UIAccessLog_id = item.uIAccessLog_id;
            result.Updated = null;// item.Updated;
            result.Ver = item.ver;
            result.ZarolasIdo = item.zarolasIdo;
            result.Zarolo_id = item.zarolo_id;
            return result;
        }
        #endregion

        #region EMAIL
        public static ServiceReferenceEmailService.ExecParam Convert(WSO_EmailService_ExecParam item)
        {
            ServiceReferenceEmailService.ExecParam result = new ServiceReferenceEmailService.ExecParam();
            result.Alkalmazas_Id = item.alkalmazas_IdField;
            result.CallingChain = item.callingChainField;
            result.CsoportTag_Id = item.csoportTag_IdField;
            result.Fake = item.fakeField;
            result.FelhasznaloSzervezet_Id = item.felhasznaloSzervezet_IdField;
            result.Felhasznalo_Id = item.felhasznalo_IdField;
            result.FunkcioKod = item.funkcioKodField;
            result.Helyettesites_Id = item.helyettesites_IdField;
            result.LoginUser_Id = item.loginUser_IdField;
            result.Org_Id = null;
            result.Page_Id = item.page_IdField;
            result.Paging = null;// item.PagingField;
            result.Record_Id = item.record_IdField;
            result.Typed = null;// item.TypedField;
            result.UIAccessLog_Id = item.uIAccessLog_IdField;
            result.UserHostAddress = item.userHostAddressField;
            return result;
        }
        public static ServiceReferenceEmailService.EREC_eMailBoritekok Convert(WSO_EmailService_EREC_eMailBoritekok item)
        {
            ServiceReferenceEmailService.EREC_eMailBoritekok result = new ServiceReferenceEmailService.EREC_eMailBoritekok();
            result.Allapot = item.AllapotField;

            result.CC = item.CcField;
            result.Cimzett = item.CimzettField;
            result.DigitalisAlairas = item.DigitalisAlairasField;
            result.EmailForras = item.EmailForrasField;
            result.EmailGuid = item.EmailGuidField;
            result.ErkezesDatuma = item.ErkezesDatumaField;
            result.ErvKezd = item.ErvKezdField;
            result.ErvVege = item.ErvVegeField;
            result.FeladasDatuma = item.FeladasDatumaField;
            result.Felado = item.FeladoField;
            result.FeldolgozasIdo = item.FeldolgozasIdoField;
            result.Fontossag = item.FontossagField;
            result.ForrasTipus = item.ForrasTipusField;
            result.Id = item.IdField;
            result.IraIrat_Id = item.IraIrat_IdField;
            result.KuldKuldemeny_Id = item.KuldKuldemeny_IdField;
            result.Targy = item.TargyField;
            result.Typed = null;
            result.Updated = null;
            result.Uzenet = item.UzenetField;

            result.Base = Convert(item.BaseDocumentField);

            return result;
        }
        public static ServiceReferenceEmailService.BaseDocument Convert(WSO_EmailService_BaseDocument item)
        {
            if (item == null)
                return null;

            ServiceReferenceEmailService.BaseDocument result = new ServiceReferenceEmailService.BaseDocument();
            result.LetrehozasIdo = item.letrehozasIdo;
            result.Letrehozo_id = item.letrehozo_id;
            result.ModositasIdo = item.modositasIdo;
            result.Modosito_id = item.modosito_id;
            result.Note = item.note;
            result.Stat_id = item.stat_id;
            result.Tranz_id = item.tranz_id;
            result.Typed = null;// item.Typed;
            result.UIAccessLog_id = item.uIAccessLog_id;
            result.Updated = null;// item.Updated;
            result.Ver = item.ver;
            result.ZarolasIdo = item.zarolasIdo;
            result.Zarolo_id = item.zarolo_id;
            return result;
        }
        #endregion
    }
}

