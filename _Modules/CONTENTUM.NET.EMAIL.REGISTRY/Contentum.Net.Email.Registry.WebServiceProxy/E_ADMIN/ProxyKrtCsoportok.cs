﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.E_ADMIN
{
    public class ProxyKrtCsoportok
    {
        public static WSO_DataSetResult GetCsoportById(string exeCutorFelhasznaloId, string executorFelhasznloSzervezetId, string id)
        {
            ServiceReferenceEAdminKRTCsoportokService.ExecParam execParam = new ServiceReferenceEAdminKRTCsoportokService.ExecParam();
            execParam.Felhasznalo_Id = exeCutorFelhasznaloId;
            execParam.FelhasznaloSzervezet_Id = executorFelhasznloSzervezetId;

            ServiceReferenceEAdminKRTCsoportokService.KRT_CsoportokSearch search = new ServiceReferenceEAdminKRTCsoportokService.KRT_CsoportokSearch();
            search.Id = new ServiceReferenceEAdminKRTCsoportokService.Field();
            search.Id.Name = "KRT_Csoportok.Id";
            search.Id.Operator = "=";
            search.Id.Value = id;
            search.Id.Group = "0";
            search.Id.GroupOperator = "and";
            search.Id.Type = "String";

            ServiceReferenceEAdminKRTCsoportokService.Result result;
            using (ServiceReferenceEAdminKRTCsoportokService.KRT_CsoportokServiceSoapClient client = new ServiceReferenceEAdminKRTCsoportokService.KRT_CsoportokServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }
    }
}
