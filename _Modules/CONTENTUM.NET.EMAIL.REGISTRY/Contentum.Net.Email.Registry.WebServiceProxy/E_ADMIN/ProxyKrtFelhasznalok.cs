﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyKrtFelhasznalok
    {
        public static WSO_DataSetResult GetFelhasznaloById(string felhasznaloId, string felhasznloSzervezetId, string id)
        {
            ServiceReferenceEAdminKRTFelhasznlokService.ExecParam execParam = new ServiceReferenceEAdminKRTFelhasznlokService.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceEAdminKRTFelhasznlokService.KRT_FelhasznalokSearch search = new ServiceReferenceEAdminKRTFelhasznlokService.KRT_FelhasznalokSearch();
            search.Id = new ServiceReferenceEAdminKRTFelhasznlokService.Field();
            search.Id.Name = "KRT_Felhasznalok.Id";
            search.Id.Operator = "=";
            search.Id.Value = id;
            search.Id.Group = "0";
            search.Id.GroupOperator = "and";
            search.Id.Type = "String";

            ServiceReferenceEAdminKRTFelhasznlokService.Result result;
            using (ServiceReferenceEAdminKRTFelhasznlokService.KRT_FelhasznalokServiceSoapClient client = new ServiceReferenceEAdminKRTFelhasznlokService.KRT_FelhasznalokServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }
    }
}
