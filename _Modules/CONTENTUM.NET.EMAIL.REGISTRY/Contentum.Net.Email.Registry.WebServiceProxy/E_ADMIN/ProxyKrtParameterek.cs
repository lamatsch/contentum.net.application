﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyKrtParameterek
    {
        public static WSO_DataSetResult GetParameterekByNev(string felhasznaloId, string felhasznloSzervezetId, string nev)
        {
            ServiceReferenceEAdminKRTParameterekService.ExecParam execParam = new ServiceReferenceEAdminKRTParameterekService.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceEAdminKRTParameterekService.KRT_ParameterekSearch search = new ServiceReferenceEAdminKRTParameterekService.KRT_ParameterekSearch();
            search.Nev = new ServiceReferenceEAdminKRTParameterekService.Field();
            search.Nev.Name = "KRT_Parameterek.Nev";
            search.Nev.Operator = "=";
            search.Nev.Value = nev;
            search.Nev.Group = "0";
            search.Nev.GroupOperator = "and";
            search.Nev.Type = "String";

            ServiceReferenceEAdminKRTParameterekService.Result result;
            using (ServiceReferenceEAdminKRTParameterekService.KRT_ParameterekServiceSoapClient client = new ServiceReferenceEAdminKRTParameterekService.KRT_ParameterekServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }
    }
}
