﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyIntManagedEmailAddress
    {
        public static WSO_DataSetResult GetAllManagedEmailAddress(string felhasznaloId, string felhasznloSzervezetId)
        {
            ServiceReferenceINTManagedEmailAddress.ExecParam execParam = new ServiceReferenceINTManagedEmailAddress.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceINTManagedEmailAddress.INT_ManagedEmailAddressSearch search = new ServiceReferenceINTManagedEmailAddress.INT_ManagedEmailAddressSearch();
            search.ErvVege = new ServiceReferenceINTManagedEmailAddress.Field();
            search.ErvVege.Name = "INT_ManagedEmailAddress.ErvVege";
            search.ErvVege.Operator = ">=";
            search.ErvVege.Value = "getdate()";
            search.ErvVege.Group = "0";
            search.ErvVege.GroupOperator = "and";
            search.ErvVege.Type = "DateTime";

            ServiceReferenceINTManagedEmailAddress.Result result;
            using (ServiceReferenceINTManagedEmailAddress.NT_ManagedEmailAddressServiceSoapClient client = new ServiceReferenceINTManagedEmailAddress.NT_ManagedEmailAddressServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }
    }
}
