﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyEmail
    {
        /// <summary>
        /// SendAnswerEmail
        /// </summary>
        /// <param name="item"></param>
        public static void SendAnswerEmail(WSO_EmailServiceAnswerEmail item)
        {
            ServiceReferenceEmailService.ExecParam execParam = null;
            using (ServiceReferenceEmailService.EmailServiceSoapClient client = new ServiceReferenceEmailService.EmailServiceSoapClient())
            {
                var result = client.SendAnswerEmail(execParam, item.Kuldemeny_id, item.Ugyirat_id);
            }
        }

        /// <summary>
        /// SendAnswerEmail
        /// </summary>
        /// <param name="item"></param>
        public static bool SendResponseEmailToSender(string felhasznlaoId, string FelhasznaloSzervezetId, string from, string[] to, string subject, string body, bool ishtml, string[] cc = null, string[] bcc = null)
        {
            ServiceReferenceEmailService.ExecParam execParam = new ServiceReferenceEmailService.ExecParam();
            execParam.Felhasznalo_Id = felhasznlaoId;
            execParam.FelhasznaloSzervezet_Id = FelhasznaloSzervezetId;

            bool result;
            using (ServiceReferenceEmailService.EmailServiceSoapClient client = new ServiceReferenceEmailService.EmailServiceSoapClient())
            {
                 result = client.SendEmail(execParam, from, to, subject, cc, bcc, ishtml, body);
            }
            return result;
        }

        /// <summary>
        /// SendEmailErkeztetve
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool SendEmailErkeztetve(WSO_EmailService_SendEmailErkeztetve item)
        {
            ServiceReferenceEmailService.ExecParam execParam = null;
            ServiceReferenceEmailService.EREC_eMailBoritekok boritek = null;
            bool result;
            try
            {
                execParam = DataTransferObjectForWSO.Convert(item.MyExecParam);
                boritek = DataTransferObjectForWSO.Convert(item.MyEmailBoritek);

                using (ServiceReferenceEmailService.EmailServiceSoapClient client = new ServiceReferenceEmailService.EmailServiceSoapClient())
                {
                    result = client.SendEmailErkeztetve(execParam, boritek, item.MyErkeztetoSzam);
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// SendErrorInMail
        /// </summary>
        /// <param name="inFelhasznaloId"></param>
        /// <param name="inFelhasznaloSzervezeteId"></param>
        /// <param name="notifiedEmailAddress"></param>
        /// <param name="inUrl"></param>
        /// <param name="inErrorCode"></param>
        /// <param name="inErrorMessage"></param>
        /// <param name="inLogReference"></param>
        /// <param name="inDescription"></param>
        /// <returns></returns>
        public static bool SendErrorInMail(string inFelhasznaloId, string inFelhasznaloSzervezeteId, string inNotifiedEmailAddress, string inUrl, string inErrorCode, string inErrorMessage, string inLogReference = null, string inDescription = null)
        {
            ServiceReferenceEmailService.ExecParam execParam = new ServiceReferenceEmailService.ExecParam()
            {
                Felhasznalo_Id = inFelhasznaloId,
                FelhasznaloSzervezet_Id = inFelhasznaloSzervezeteId
            };
            bool result;
            try
            {
                using (ServiceReferenceEmailService.EmailServiceSoapClient client = new ServiceReferenceEmailService.EmailServiceSoapClient())
                {
                    result = client.SendErrorInMail(execParam, inNotifiedEmailAddress, inUrl, inErrorCode, inErrorMessage, inLogReference, inDescription);
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            return result;
        }
    }
}
