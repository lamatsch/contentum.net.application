﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;
using System;
using System.Collections.Generic;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyEmailBoritekok
    {
        public static WSO_BaseResult InsertAndAttachDocument(WSO_EmailBoritekInsertAndAttachDoc item)
        {
            ServiceReferenceEmailBoritekok.ExecParam paramExec = DataTransferObjectForWSO.Convert(item.MyExecParam);
            ServiceReferenceEmailBoritekok.EREC_eMailBoritekok paramEmailBoritek = DataTransferObjectForWSO.Convert(item.MyMailEnvelopes);
            List<ServiceReferenceEmailBoritekok.Csatolmany> paramCsatolmanyok = DataTransferObjectForWSO.Convert(item.MyAttachments);

            ServiceReferenceEmailBoritekok.Result result = null;

            using (ServiceReferenceEmailBoritekok.EREC_eMailBoritekokServiceSoapClient client = new ServiceReferenceEmailBoritekok.EREC_eMailBoritekokServiceSoapClient())
            {
                result = client.InsertAndAttachDocument(paramExec, paramEmailBoritek, paramCsatolmanyok.ToArray(), item.KuldesMod, item.EgyebparancsXml);
            }
            return new WSO_BaseResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType, 
                record = result.Record
            };
        }

        public static object GetItem(WSO_EmailBoritek_ExecParam item)
        {
            ServiceReferenceEmailBoritekok.ExecParam paramExec = DataTransferObjectForWSO.Convert(item);

            ServiceReferenceEmailBoritekok.Result result = null;

            using (ServiceReferenceEmailBoritekok.EREC_eMailBoritekokServiceSoapClient client = new ServiceReferenceEmailBoritekok.EREC_eMailBoritekokServiceSoapClient())
            {
                result = client.Get(paramExec);
            }
            return result;
        }
    }
}
