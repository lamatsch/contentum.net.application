﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public class ProxyEmailErkeztetesIktatas
    {
        public static WSO_BaseResult EmailErkeztetesIktatas(WSO_EmailErkeztetesIktatas item, bool alszamraIktatas = false)
        {
            ServiceReferenceEmailErkeztetesIktatas.Result result = null;
            ServiceReferenceEmailErkeztetesIktatas.ExecParam execParam = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_KuldKuldemenyek kuldemenyAdatok = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_eMailBoritekok emailBoritekAdatok = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratok erec_UgyUgyiratok = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratok erec_IraIratok = null;
            ServiceReferenceEmailErkeztetesIktatas.EREC_HataridosFeladatok erec_HataridosFeladatok = null;
            ServiceReferenceEmailErkeztetesIktatas.IktatasiParameterek ip = null;
            ServiceReferenceEmailErkeztetesIktatas.ErkeztetesParameterek _ErkeztetesParameterek = null;

            string erec_IraIktatoKonyvek_Id;
            try
            {
                execParam = DataTransferObjectForWSO.Convert(item.MyEXEC_Param);
                kuldemenyAdatok = DataTransferObjectForWSO.Convert(item.MyEREC_KuldKuldemenyek);
                emailBoritekAdatok = DataTransferObjectForWSO.Convert(item.MyEREC_eMailBoritekok);
                erec_UgyUgyiratok = DataTransferObjectForWSO.Convert(item.MyEREC_UgyUgyiratok);
                erec_UgyUgyiratdarabok = DataTransferObjectForWSO.Convert(item.MyEREC_UgyUgyiratdarabok);
                erec_IraIratok = DataTransferObjectForWSO.Convert(item.MyEREC_IraIratok);
                erec_HataridosFeladatok = DataTransferObjectForWSO.Convert(item.MyEREC_HataridosFeladatok);
                ip = DataTransferObjectForWSO.Convert(item.MyIktatasiParameterek);
                _ErkeztetesParameterek = DataTransferObjectForWSO.Convert(item.MyErkeztetesParametereK);

                erec_IraIktatoKonyvek_Id = item.MyEREC_IraIktatoKonyvek_Id;
                if (alszamraIktatas)
                    using (ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratokServiceSoapClient client = new ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratokServiceSoapClient())
                    {
                        result = client.EmailErkeztetesIktatas_Alszamra(
                            execParam,
                            kuldemenyAdatok,
                            emailBoritekAdatok,
                            erec_IraIktatoKonyvek_Id,
                            item.MyEREC_UygUgyiratId,
                            erec_UgyUgyiratdarabok,
                            erec_IraIratok,
                            erec_HataridosFeladatok,
                            ip,
                            _ErkeztetesParameterek
                            );
                    }
                else
                    using (ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratokServiceSoapClient client = new ServiceReferenceEmailErkeztetesIktatas.EREC_IraIratokServiceSoapClient())
                    {
                        result = client.EmailErkeztetesIktatas(
                            execParam,
                            kuldemenyAdatok,
                            emailBoritekAdatok,
                            erec_IraIktatoKonyvek_Id,
                            erec_UgyUgyiratok,
                            erec_UgyUgyiratdarabok,
                            erec_IraIratok,
                            erec_HataridosFeladatok,
                            ip,
                            _ErkeztetesParameterek
                            );
                    }
            }
            catch (System.Exception)
            {
                throw;
            }
            if (result == null)
                return new WSO_BaseResult();

            return new WSO_BaseResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record
            };
        }
    }
}
