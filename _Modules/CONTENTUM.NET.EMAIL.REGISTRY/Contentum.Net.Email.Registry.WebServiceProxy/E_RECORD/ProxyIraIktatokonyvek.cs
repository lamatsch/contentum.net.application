﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyIraIktatokonyvek
    {
        /// <summary>
        /// Get all érkeztetőkönyv / iktatókönyv / postakönyv
        /// </summary>
        /// <param name="felhasznaloId"></param>
        /// <param name="felhasznloSzervezetId"></param>
        /// <param name="searchParam"></param>
        /// <param name="konyvTipusa">E = érkeztetőkönyv, I = iktatókönyv, P = postakönyv</param>
        /// <returns></returns>
        public static WSO_DataSetResult GetAllIktatoKonyv(string felhasznaloId, string felhasznloSzervezetId, char konyvTipusa, string searchParam)
        {
            ServiceReferenceIraIktatokonyvekService.ExecParam execParam = new ServiceReferenceIraIktatokonyvekService.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceIraIktatokonyvekService.EREC_IraIktatoKonyvekSearch search = new ServiceReferenceIraIktatokonyvekService.EREC_IraIktatoKonyvekSearch();
            search.Ev = new ServiceReferenceIraIktatokonyvekService.Field();
            search.Ev.Name = "EREC_IraIktatoKonyvek.Ev";
            search.Ev.Operator = "=";
            search.Ev.Value = System.DateTime.Now.Year.ToString();
            search.Ev.Group = "0";
            search.Ev.GroupOperator = "and";
            search.Ev.Type = "Int32";

            search.IktatoErkezteto = new ServiceReferenceIraIktatokonyvekService.Field();
            search.IktatoErkezteto.Name = "EREC_IraIktatoKonyvek.IktatoErkezteto";
            search.IktatoErkezteto.Operator = "=";
            search.IktatoErkezteto.Value = konyvTipusa.ToString();
            search.IktatoErkezteto.Group = "0";
            search.IktatoErkezteto.GroupOperator = "and";
            search.IktatoErkezteto.Type = "Char";

            search.ErvKezd = new ServiceReferenceIraIktatokonyvekService.Field();
            search.ErvKezd.Name = "EREC_IraIktatoKonyvek.ErvKezd";
            search.ErvKezd.Operator = "<=";
            search.ErvKezd.Value = "getdate()";
            search.ErvKezd.Group = "0";
            search.ErvKezd.GroupOperator = "and";
            search.ErvKezd.Type = "DateTime";

            search.ErvVege = new ServiceReferenceIraIktatokonyvekService.Field();
            search.ErvVege.Name = "EREC_IraIktatoKonyvek.ErvVege";
            search.ErvVege.Operator = ">=";
            search.ErvVege.Value = "getdate()";
            search.ErvVege.Group = "0";
            search.ErvVege.GroupOperator = "and";
            search.ErvVege.Type = "DateTime";

            search.Statusz = new ServiceReferenceIraIktatokonyvekService.Field();
            search.Statusz.Name = "EREC_IraIktatoKonyvek.Statusz";
            search.Statusz.Operator = "=";
            search.Statusz.Value = "1";
            search.Statusz.Group = "0";
            search.Statusz.GroupOperator = "and";
            search.Statusz.Type = "Char";

            if (!string.IsNullOrEmpty(searchParam))
            {
                switch (konyvTipusa)
                {
                    case 'E':
                        search.MegkulJelzes = new ServiceReferenceIraIktatokonyvekService.Field();
                        search.MegkulJelzes.Name = "EREC_IraIktatoKonyvek.MegkulJelzes";
                        search.MegkulJelzes.Operator = "=";
                        search.MegkulJelzes.Value = searchParam;
                        search.MegkulJelzes.Group = "0";
                        search.MegkulJelzes.GroupOperator = "and";
                        search.MegkulJelzes.Type = "String";
                        break;
                    case 'I':
                        search.Iktatohely = new ServiceReferenceIraIktatokonyvekService.Field();
                        search.Iktatohely.Name = "EREC_IraIktatoKonyvek.Iktatohely";
                        search.Iktatohely.Operator = "=";
                        search.Iktatohely.Value = searchParam;
                        search.Iktatohely.Group = "0";
                        search.Iktatohely.GroupOperator = "and";
                        search.Iktatohely.Type = "String";
                        break;
                    default:
                        break;
                }
             
            }
            search.TopRow = 0;

            ServiceReferenceIraIktatokonyvekService.Result svcResult;
            using (ServiceReferenceIraIktatokonyvekService.EREC_IraIktatoKonyvekServiceSoapClient client = new ServiceReferenceIraIktatokonyvekService.EREC_IraIktatoKonyvekServiceSoapClient())
            {
                svcResult = client.GetAllWithIktathat(execParam, search);
            }
            WSO_DataSetResult result = DataTransferObjectForWSO.Convert(svcResult);
            return result;
        }
    }
}
