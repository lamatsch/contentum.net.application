﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyIraIratok
    {
        public static WSO_DataSetResult GetIraIratokById(string felhasznaloId, string felhasznloSzervezetId, string id)
        {
            ServiceReferenceIraIratokService.ExecParam execParam = new ServiceReferenceIraIratokService.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceIraIratokService.EREC_IraIratokSearch search = new ServiceReferenceIraIratokService.EREC_IraIratokSearch();
            search.Id = new ServiceReferenceIraIratokService.Field();
            search.Id.Name = "EREC_IraIratok.Id";
            search.Id.Operator = "=";
            search.Id.Value = id;
            search.Id.Group = "0";
            search.Id.GroupOperator = "and";
            search.Id.Type = "String";

            ServiceReferenceIraIratokService.Result result;
            using (ServiceReferenceIraIratokService.EREC_IraIratokServiceSoapClient client = new ServiceReferenceIraIratokService.EREC_IraIratokServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }
    }
}
