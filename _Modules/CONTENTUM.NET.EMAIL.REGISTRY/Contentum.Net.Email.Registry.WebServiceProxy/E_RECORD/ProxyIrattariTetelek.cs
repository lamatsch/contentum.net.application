﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyIrattariTetelek
    {
        public static WSO_DataSetResult GetAll(string felhasznaloId, string felhasznloSzervezetId, string azonosito)
        {
            ServiceReferenceIrattariTetelekService.ExecParam execParam = new ServiceReferenceIrattariTetelekService.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceIrattariTetelekService.EREC_IraIrattariTetelekSearch search = new ServiceReferenceIrattariTetelekService.EREC_IraIrattariTetelekSearch();
            search.IrattariTetelszam = new ServiceReferenceIrattariTetelekService.Field();
            search.IrattariTetelszam.Name = "EREC_IraIrattariTetelek.IrattariTetelszam";
            search.IrattariTetelszam.Operator = "=";
            search.IrattariTetelszam.Value = azonosito;
            search.IrattariTetelszam.Group = "0";
            search.IrattariTetelszam.GroupOperator = "and";
            search.IrattariTetelszam.Type = "String";

            search.ErvKezd = new ServiceReferenceIrattariTetelekService.Field();
            search.ErvKezd.Name = "EREC_IraIrattariTetelek.ErvKezd";
            search.ErvKezd.Operator = "<=";
            search.ErvKezd.Value = "getdate()";
            search.ErvKezd.Group = "0";
            search.ErvKezd.GroupOperator = "and";
            search.ErvKezd.Type = "DateTime";

            search.ErvVege = new ServiceReferenceIrattariTetelekService.Field();
            search.ErvVege.Name = "EREC_IraIrattariTetelek.ErvVege";
            search.ErvVege.Operator = ">=";
            search.ErvVege.Value = "getdate()";
            search.ErvVege.Group = "0";
            search.ErvVege.GroupOperator = "and";
            search.ErvVege.Type = "DateTime";

            ServiceReferenceIrattariTetelekService.Result serviceResult;
            using (ServiceReferenceIrattariTetelekService.EREC_IraIrattariTetelekServiceSoapClient client = new ServiceReferenceIrattariTetelekService.EREC_IraIrattariTetelekServiceSoapClient())
            {
                serviceResult = client.GetAllWithIktathat(execParam, search);
            }
            WSO_DataSetResult result = DataTransferObjectForWSO.Convert(serviceResult);
            return result;
        }
    }
}
