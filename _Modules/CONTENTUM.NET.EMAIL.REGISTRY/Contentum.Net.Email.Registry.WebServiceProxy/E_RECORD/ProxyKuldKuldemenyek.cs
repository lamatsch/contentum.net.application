﻿using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.DBO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.Common.WSO;
using AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy.DTO;

namespace AxisRendszerhaz.Contentum.Net.Email.Registry.WebServiceProxy
{
    public static class ProxyKuldKuldemenyek
    {
        //public static object GetAll(string felhasznaloId, string felhasznloSzervezetId, string azonosito)
        //{
        //    ServiceReferenceKuldKuldemenyek.ExecParam execParam = new ServiceReferenceKuldKuldemenyek.ExecParam();
        //    execParam.Felhasznalo_Id = felhasznaloId;
        //    execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

        //    ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekSearch search = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekSearch();
        //    search.Azonosito = new ServiceReferenceKuldKuldemenyek.Field();
        //    search.Azonosito.Name = "EREC_KuldKuldemenyek.Azonosito";
        //    search.Azonosito.Operator = "=";
        //    search.Azonosito.Value = azonosito;
        //    search.Azonosito.Group = "0";
        //    search.Azonosito.GroupOperator = "and";
        //    search.Azonosito.Type = "String";

        //    search.ErvKezd = new ServiceReferenceKuldKuldemenyek.Field();
        //    search.ErvKezd.Name = "EREC_KuldKuldemenyek.ErvKezd";
        //    search.ErvKezd.Operator = "<=";
        //    search.ErvKezd.Value = "getdate()";
        //    search.ErvKezd.Group = "0";
        //    search.ErvKezd.GroupOperator = "and";
        //    search.Azonosito.Type = "DateTime";

        //    search.ErvVege = new ServiceReferenceKuldKuldemenyek.Field();
        //    search.ErvVege.Name = "EREC_KuldKuldemenyek.ErvVege";
        //    search.ErvVege.Operator = ">=";
        //    search.ErvVege.Value = "getdate()";
        //    search.ErvVege.Group = "0";
        //    search.ErvVege.GroupOperator = "and";
        //    search.ErvVege.Type = "DateTime";

        //    ServiceReferenceKuldKuldemenyek.Result result;
        //    using (ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient client = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient())
        //    {
        //        result = client.GetAll(execParam, search);
        //    }
        //    return result;
        //}
        public static WSO_DataSetResult GetKuldKuldemenyById(string felhasznaloId, string felhasznloSzervezetId, string id)
        {
            ServiceReferenceKuldKuldemenyek.ExecParam execParam = new ServiceReferenceKuldKuldemenyek.ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.FelhasznaloSzervezet_Id = felhasznloSzervezetId;

            ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekSearch search = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekSearch();
            search.Id = new ServiceReferenceKuldKuldemenyek.Field();
            search.Id.Name = "EREC_KuldKuldemenyek.Id";
            search.Id.Operator = "=";
            search.Id.Value = id;
            search.Id.Group = "0";
            search.Id.GroupOperator = "and";
            search.Id.Type = "String";

            ServiceReferenceKuldKuldemenyek.Result result;
            using (ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient client = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient())
            {
                result = client.GetAll(execParam, search);
            }
            if (result == null)
                return new WSO_DataSetResult();

            return new WSO_DataSetResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record,
                ds = result.Ds
            };
        }

        public static WSO_BaseResult ErkeztetesEmailbol(DBO_ManagedEmailParameters emailParameters, WSO_KuldKuldemenyekEmailIktatas item)
        {
            ServiceReferenceKuldKuldemenyek.ExecParam execParam = new ServiceReferenceKuldKuldemenyek.ExecParam();
            execParam.Felhasznalo_Id = emailParameters.FelhasznaloId;
            execParam.FelhasznaloSzervezet_Id = emailParameters.FelhasznaloSzervezetId;

            ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyek kuld = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyek();
            ServiceReferenceKuldKuldemenyek.EREC_eMailBoritekok boritek = new ServiceReferenceKuldKuldemenyek.EREC_eMailBoritekok();
            ServiceReferenceKuldKuldemenyek.EREC_HataridosFeladatok feladatok = new ServiceReferenceKuldKuldemenyek.EREC_HataridosFeladatok();
            ServiceReferenceKuldKuldemenyek.ErkeztetesParameterek erkeztetesiParameterek = new ServiceReferenceKuldKuldemenyek.ErkeztetesParameterek();
            ServiceReferenceKuldKuldemenyek.Result result;
            try
            {
                kuld = DataTransferObjectForWSO.Convert(item.MyEREC_KuldKuldemenyek);
                boritek = DataTransferObjectForWSO.Convert(item.MyEREC_eMailBoritekok);
                feladatok = DataTransferObjectForWSO.Convert(item.MyEREC_HataridosFeladato);
                erkeztetesiParameterek = DataTransferObjectForWSO.Convert(item.MyEREC_ErkeztetesParameterek);

                using (ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient client = new ServiceReferenceKuldKuldemenyek.EREC_KuldKuldemenyekServiceSoapClient())
                {
                    result = client.ErkeztetesEmailbol(execParam, kuld, boritek, feladatok, erkeztetesiParameterek);
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            if (result == null)
                return new WSO_BaseResult();
            return new WSO_BaseResult()
            {
                uid = result.Uid,
                errorCode = result.ErrorCode,
                errorMessage = result.ErrorMessage,
                errorType = result.ErrorType,
                record = result.Record
            };
        }
    }
}
