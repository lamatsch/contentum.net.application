﻿using Contentum.Net.Pdf.Extractor.PdfLibrary;
using System;

namespace Contentum.Net.Pdf.Extractor.ApplicationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            PDFExtractor<string> pdfe = new PDFExtractor<string>();
            //var res = pdfe.ExtractAttachments(@"C:\Users\zslakk\Documents\teszt\SFTP_0010107104_55035004_20191004_105204 HIBÁS.pdf");
            var res = pdfe.ExtractAttachments(@"C:\Users\zslakk\Documents\teszt\SFTP_10100823_10461176_20150729_152224 (2).pdf");
            
            if (res != null)
                foreach (var item in res)
                {
                    Console.WriteLine(item.Name + " (" + item.ShortName + ")");
                }
            Console.ReadLine();
        }
    }
}
