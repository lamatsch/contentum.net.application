﻿namespace Contentum.Net.Pdf.Extractor.Common
{
    public class PdfFileAttachment
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public byte[] Content { get; set; }
    }
}
