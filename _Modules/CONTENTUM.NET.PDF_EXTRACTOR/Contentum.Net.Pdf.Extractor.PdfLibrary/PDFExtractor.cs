﻿using Contentum.Net.Pdf.Extractor.Common;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.IO;

namespace Contentum.Net.Pdf.Extractor.PdfLibrary
{
    /// <summary>
    /// Pdf attachments extractor class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PDFExtractor<T>
    {
        /// <summary>
        /// Extract pdf attachments
        /// </summary>
        /// <param name="source">Source: string , byte[]</param>
        /// <returns></returns>
        public List<PdfFileAttachment> ExtractAttachments(T source)
        {
            PdfReader reader = null;
            if (typeof(T) == typeof(string))
            {
                string tmp = (string)(object)source;
                reader = new PdfReader(tmp);
            }
            else if (typeof(T) == typeof(byte[]))
            {
                byte[] tmp = (byte[])(object)source;
                reader = new PdfReader(tmp);
            }

            return Core(reader);
        }

        /// <summary>
        /// Core function
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        internal List<PdfFileAttachment> Core(PdfReader reader)
        {
            #region PROPERTIES
            PdfDictionary documentNames = null;
            PdfDictionary embeddedFiles = null;
            #endregion

            PdfDictionary catalog = reader.Catalog;
            documentNames = (PdfDictionary)PdfReader.GetPdfObject(catalog.Get(PdfName.NAMES));


            if (documentNames == null)
                return null;

            embeddedFiles = (PdfDictionary)PdfReader.GetPdfObject(documentNames.Get(PdfName.EMBEDDEDFILES));
            if (embeddedFiles == null)
                return null;

            #region PROPERTIES
            PdfDictionary file = null;
            PdfDictionary fileArray = null;
            //LZS
            PdfDictionary kidsArray = null;
            PdfArray kidsFilespecs = null;
            List<PdfFileAttachment> allAttachments = new List<PdfFileAttachment>();


            PRStream stream = null;
            #endregion


            PdfArray filespecs = embeddedFiles.GetAsArray(PdfName.NAMES);

            //LZS
            //Nagyobb fájlok beolvasása esetén (> 900 kb) szétszedve érjük el a csatolmányokat. Ilyenkor ezek a /KIDS node alatt lesznek elérhetőek.
            //Ezeken is végig kell lépkedni.
            if (filespecs == null)
            {
                kidsFilespecs = embeddedFiles.GetAsArray(PdfName.KIDS);

                for (int i = 0; i < kidsFilespecs.Size; i++)
                {
                    kidsArray = kidsFilespecs.GetAsDict(i);

                    for (int j = 0; j < kidsArray.Size; j++)
                    {
                        filespecs = kidsArray.GetAsArray(PdfName.NAMES);
                    }

                    allAttachments.AddRange(FileSpecHandler(ref file, ref fileArray, ref stream, filespecs));
                }
            }
            else
            {
                allAttachments = FileSpecHandler(ref file, ref fileArray, ref stream, filespecs);
            }

            return allAttachments;
        }

        private static List<PdfFileAttachment> FileSpecHandler(ref PdfDictionary file, ref PdfDictionary fileArray, ref PRStream stream, PdfArray filespecs)
        {
            byte[] attachedFileBytes;
            List<PdfFileAttachment> attachments = new List<PdfFileAttachment>();
            string attachedFileName;

            for (int i = 0; i < filespecs.Size; i++)
            {
                i++;
                fileArray = filespecs.GetAsDict(i);
                file = fileArray.GetAsDict(PdfName.EF);

                foreach (PdfName key in file.Keys)
                {
                    stream = (PRStream)PdfReader.GetPdfObject(file.GetAsIndirectObject(key));
                    attachedFileName = fileArray.GetAsString(key).ToString();
                    attachedFileBytes = PdfReader.GetStreamBytes(stream);

                    #region WRITE FILES TO DISK
                    //File.WriteAllBytes(attachedFileName, attachedFileBytes);
                    #endregion

                    attachments.Add(new PdfFileAttachment()
                    {
                        Name = attachedFileName,
                        ShortName = Path.GetFileNameWithoutExtension(attachedFileName),
                        Content = attachedFileBytes,
                    });
                }
            }

            return attachments;
        }
    }
}
