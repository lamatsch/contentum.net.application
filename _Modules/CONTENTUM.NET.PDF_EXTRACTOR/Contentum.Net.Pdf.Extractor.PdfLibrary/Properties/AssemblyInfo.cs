﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Contentum.Net.Pdf.Extractor.PdfLibrary")]
[assembly: AssemblyDescription("Contentum.Net.Pdf.Extractor.PdfLibrary")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Axis Rendszerhaz Kft.")]
[assembly: AssemblyProduct("Contentum.Net.Pdf.Extractor.PdfLibrary")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("Axis Rendszerhaz Kft.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("30f0272b-2ad2-4454-86fa-b86f19d3e348")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: AssemblyFileVersion("1.0.1.0")]
