﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HALK_RunApp
{
    public class Logger
    {
        private bool IsEnabled()
        {
            if (string.IsNullOrEmpty(GetLogFolder()))
            {
                return false;
            }
            return true;
        }
        private const string ENV_KEY = "HALK_LOG_FOLDER";
        public string GetLogFolder()
        {
            string fromEnvironmentVariable = Environment.GetEnvironmentVariable(ENV_KEY, EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(fromEnvironmentVariable))
            {
                fromEnvironmentVariable = Environment.GetEnvironmentVariable(ENV_KEY, EnvironmentVariableTarget.User);
            }

            if (string.IsNullOrEmpty(fromEnvironmentVariable)) { return fromEnvironmentVariable; }
            return fromEnvironmentVariable.Trim('/', '\\');
        }

        public Logger()
        {
            //Ha nincs beállítva mappa akkor nem loggolunk
            if (string.IsNullOrEmpty(GetLogFolder())) { return; }
            bool isFolderExists = Directory.Exists(GetLogFolder());
            if (!isFolderExists)
            {
                Directory.CreateDirectory(GetLogFolder());
            }
        }

        public void Debug(string message)
        {
            WriteLog(" DEBUG => " + message);
        }
        public void Debug(string message,string param)
        {
           if (param == null)
            {
                param = "NULL";
            }
           this.Debug(message + " " + param);
        }
        public void Error(string message)
        {
            WriteLog(" ERROR => " + message);
        }
        public void Exception(Exception exception)
        {
            WriteLog(" EXCEPTION => " + exception.Message + " " + exception.StackTrace);
        }
        private void WriteLog(string message)
        {
            try
            {
                string currentDate = DateTime.Today.ToString("yyyy-dd-M");
                string logFileName = Path.Combine(GetLogFolder() + "\\Halk-" + currentDate + ".log");
                System.IO.StreamWriter streamWriter = new StreamWriter(logFileName, true);
                streamWriter.WriteLine(DateTime.Now.ToShortTimeString() + ":" + message);
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception e)
            {

            }
        }
    }
}
