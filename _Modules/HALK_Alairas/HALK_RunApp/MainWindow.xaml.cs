﻿#define DEBUG 
#define RELEASE
using HALK_RunApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace HALK_RunApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static class Constants
        {
            public const string msg_NoQueryStringParameter = "Nincs QueryStringParameter megadva!";
            public const string msg_NoProc_IdQueryStringParameter = "Nincs Proc_Id a paraméterek között.";
            public const string msg_PutSignedDocumentsSuccessfuly = "Az aláírt dokumentumok feltöltése megtörtént.";
            public const string msg_PutSignedDocumentsUnSuccessfuly = "Az aláírt dokumentumok feltöltése sikertelen volt.";
            public const string msg_RunHalk_InterfaceUnSuccessfuly = "Az aláírás sikertelen volt.";
            public const string msg_NoTouchedDocuments = "Nincs aláírandó dokumentum.";
            public const string msg_SaveToBeSignedDocumentsUnSuccesfuly = "Az aláírandó dokumentumok helyi mentése sikertelen volt";
            public const string msg_XMLSerializeUnsuccesfuly = "A HALK paraméterfile elkészítése sikertelen volt";
        }
        private Logger _logger = new Logger();

        public Guid proc_Id;
        public TouchedDocuments touchedDocuments;
        public TouchedDocuments toBeSignedDocuments;
        public TouchedDocuments signedDocuments;
        public HALKArguments HALK_Parameters;
        public string HALK_ParametersXML_FileName;
        public string sessionId;
        public string toBeSignedDocumentsPath;
        public string signedDocumentsPath;
        public string statusFilePath;
        public string Felhasznalo_Id;
        public string FelhasznaloSzervezet_Id;


        //using testmod with test id -s
        //"517C8886-E4A8-E711-80C6-00155D020BD9" - docx
        private Boolean testMode = false;
        //private string testProcId = "5C6E8E5E-E5A8-E711-80C6-00155D020BD9";
        //private string testProcId = "4E58169E-AECB-E711-80C7-00155D027E9B";
        private string testProcId = "8C6B5DC6-9FE2-E811-80CF-00155D020DD3";
        private string testFelhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        private string testFelhasznaloSzervezet_Id = /*"BD00C8D0-CF99-4DFC-8792-33220B7BFCC6"*/ "9E714610-5E6F-4338-B410-FA25CD14A715";

        //BLG 2947 verify mode
        private bool isVerifyMode = false;

        public MainWindow()
        {
            _logger.Debug("MainWindow started");
#if (!DEBUG)
            _logger.Debug("testMode is false");
                testMode = false;
#endif

            InitializeComponent();

            String XMLFileName = "HALK_ParametersXML.xml";
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\HALK_ParametersXML.xml"))
            {
                XMLFileName = "halkparam.xml";
            }
            HALK_ParametersXML_FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\" + XMLFileName;
            _logger.Debug("HALK_ParametersXML_FileName " + HALK_ParametersXML_FileName);

            string TempPath = System.IO.Path.GetTempPath();
            string HalkPath = TempPath.EndsWith("\\") ? TempPath + "HALK" : TempPath + "\\HALK";
            toBeSignedDocumentsPath = Directory.Exists(HalkPath + "\\Input") ? HalkPath + "\\Input\\" : (Directory.CreateDirectory(HalkPath + "\\Input")).FullName + "\\";
            signedDocumentsPath = Directory.Exists(HalkPath + "\\Output") ? HalkPath + "\\Output\\" : (Directory.CreateDirectory(HalkPath + "\\Output")).FullName + "\\";
            statusFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
            _logger.Debug("TempPath: " + TempPath);
            _logger.Debug("HalkPath: " + HalkPath);
            _logger.Debug("toBeSignedDocumentsPath: " + toBeSignedDocumentsPath);
            _logger.Debug("signedDocumentsPath: " + signedDocumentsPath);
            _logger.Debug("statusFilePath: " + statusFilePath);

            //kezéds előtt kiürítjük a használt mappákat
            try
            {
                DeleteDocumentsFromOutputDirectory();
            }
            catch (Exception e) { _logger.Exception(e); }
            try
            {
                DeleteDocumentsFromInputDirectory();
            }
            catch (Exception e) { _logger.Exception(e); }

            sessionId = Guid.NewGuid().ToString().Replace("-", "");

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                Version version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                this.Title = "Digitális aláírás indítása - Verzió: " + version.ToString();
            }

            NameValueCollection queryString = null;
            try
            {
                queryString = GetQueryStringParameters();
                if (queryString == null || queryString.Keys.Count == 0)
                {
                    ShowTopMostMessage(Constants.msg_NoQueryStringParameter);
                    this.Close();
                    return;
                }
                else
                {
                    proc_Id = Guid.NewGuid();

                    var keys = queryString.AllKeys;
                    #region BLG 2947 verify mode
                    if (keys.Contains("Muvelet"))
                    {
                        String muvelet = queryString["Muvelet"];
                        isVerifyMode = "verify".Equals(muvelet, StringComparison.InvariantCultureIgnoreCase);
                    }
                    #endregion
                    if (!keys.Contains("Proc_Id"))
                    {
                        ShowTopMostMessage(Constants.msg_NoProc_IdQueryStringParameter);
                        this.Close();
                        return;
                    }
                    else
                    {
                        //To Do:  törölni a comment-ezést
                        proc_Id = new Guid(queryString["Proc_Id"]);
                        Felhasznalo_Id = keys.Contains("Felh_Id") ? queryString["Felh_Id"] : "";
                        FelhasznaloSzervezet_Id = keys.Contains("FelhSz_Id") ? queryString["FelhSz_Id"] : "";
                        touchedDocuments = GetTouchedDocuments(proc_Id);
                        if (touchedDocuments.Documents.Count == 0)
                        {
                            ShowTopMostMessage(Constants.msg_NoTouchedDocuments);
                            this.Close();
                            return;
                        }
                        toBeSignedDocuments = CompleteTouchedDocumentsFileNameWithFile_Id(touchedDocuments);
                        if (!SaveDocumentsToInputDirectory())
                        {
                            ShowTopMostMessage(Constants.msg_SaveToBeSignedDocumentsUnSuccesfuly);
                            this.Close();
                            return;
                        }
                        ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient ws = new ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient("EREC_Alairas_FolyamatokServiceSoap");
                        ContentumServiceReference.ExecParam execParam = new ContentumServiceReference.ExecParam();

                        //BLG 2947 verify mode -ban más státuszok használata!
                        String SuccessStatusCode = isVerifyMode ? "12" : "02";
                        String ErrorStatusCode = isVerifyMode ? "13" : "03";

                        if (!RunHALK_Interface(proc_Id))
                        {
                            ContentumServiceReference.Result rs = ws.setSignedStatus(execParam, proc_Id, ErrorStatusCode);
                            ShowTopMostMessage(Constants.msg_RunHalk_InterfaceUnSuccessfuly);
                            this.Close();
                            return;
                        }
                        else
                        {
                            bool isSuccessFull = PutSignedDocumentsToDatabase(); ;

                            if (isSuccessFull)
                            {
                                ShowTopMostMessage(Constants.msg_PutSignedDocumentsSuccessfuly, "", true);
                            }
                            else
                            {
                                ShowTopMostMessage(Constants.msg_PutSignedDocumentsUnSuccessfuly);
                            }
                            ContentumServiceReference.Result rs = ws.setSignedStatus(execParam, proc_Id, SuccessStatusCode);

                        }


                        DeleteDocumentsFromOutputDirectory();
                        DeleteDocumentsFromInputDirectory();
                    }

                    //foreach (var item in toBeSignedDocuments.Documents)
                    //{
                    //    System.Windows.MessageBox.Show(item.File_Name);
                    //}
                }

            }
            catch (Exception e)
            {
                _logger.Exception(e);
                ShowTopMostMessage(e.Message);
            }
            this.Close();
        }

        public TouchedDocuments CompleteTouchedDocumentsFileNameWithFile_Id(TouchedDocuments touchedDocuments)
        {
            _logger.Debug("CompleteTouchedDocumentsFileNameWithFile_Id");
            toBeSignedDocuments = new TouchedDocuments();
            toBeSignedDocuments.Proc_Id = touchedDocuments.Proc_Id;
            toBeSignedDocuments.Documents = new List<Document>();

            foreach (var document in touchedDocuments.Documents)
            {
                _logger.Debug("Before complete: ");
                _logger.Debug(document.ToString());
                var toBeSignedDocument = new Document();

                toBeSignedDocument.File_Id = document.File_Id;
                toBeSignedDocument.File_Name = document.File_Id.ToString() + "_Prefix_" + document.File_Name;
               // toBeSignedDocument.File_Path = document.File_Path;
                toBeSignedDocument.URL = document.URL;
                toBeSignedDocument.File_Content = document.File_Content;
                toBeSignedDocument.Irat_Id = document.Irat_Id;
                toBeSignedDocument.Csatolmany_Id = document.Csatolmany_Id;

                _logger.Debug("After complete: ");
                _logger.Debug(document.ToString());
                toBeSignedDocuments.Documents.Add(toBeSignedDocument);
            }
            return toBeSignedDocuments;
        }
        public bool RunHALK_Interface(Guid proc_id)
        {
            bool ret_Value;
            ret_Value = CreateHALK_ParamXML();
            if (ret_Value)
            {
                ret_Value = RunHALK_JarWithParamXML();
            }
            return ret_Value;
        }
        public bool SaveDocumentsToInputDirectory()
        {
            _logger.Debug("SaveDocumentsToInputDirectory");
            foreach (var document in toBeSignedDocuments.Documents)
            {
                var fileName = toBeSignedDocumentsPath + document.File_Name;
                _logger.Debug("Saving document to: ", fileName);
                using (FileStream file = new FileStream(fileName, FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(document.File_Content, 0, document.File_Content.Length);
                }

            }
            return true;
        }
        public bool CreateHALK_ParamXML()
        {
            _logger.Debug("CreateHALK_ParamXML");
            HALK_Parameters = new HALKArguments();

            HALKArgumentsOperation Operation = new HALKArgumentsOperation();
            HALKArgumentsFile[] Input = new HALKArgumentsFile[toBeSignedDocuments.Documents.Count];
            HALKArgumentsOutput Output = new HALKArgumentsOutput();
            HALKArgumentsStatus Status = new HALKArgumentsStatus();
            try
            {

                HALK_Parameters.Version = "1.0";
                HALK_Parameters.Operation = Operation;
                HALK_Parameters.Input = Input;
                HALK_Parameters.Output = Output;
                HALK_Parameters.Status = Status;

                HALKArgumentsOperationOptions OperationOptions = new HALKArgumentsOperationOptions();

                #region BLG 2947

                if (isVerifyMode)
                {
                    OperationOptions.SignUseDetachedFiles = false;
                    OperationOptions.SignUseDetachedFilesSpecified = true;
                    OperationOptions.SignPerFileSigning = false;
                    OperationOptions.SignPerFileSigningSpecified = true;
                    OperationOptions.SignUseTimestamp = false;
                    OperationOptions.SignUseTimestampSpecified = true;
                    OperationOptions.StartupDisplaySplash = true;
                }
                else
                {
                    OperationOptions.SignOutputFormat = "PDF";
                    OperationOptions.SignUseTimestamp = true;
                    OperationOptions.SignPerFileSigning = true;
                }

                Operation.Type = isVerifyMode ? HALKArgumentsOperationType.VERIFY : HALKArgumentsOperationType.SIGN;
                #endregion

                Operation.GUIMode = isVerifyMode ? HALKArgumentsOperationGUIMode.RO : HALKArgumentsOperationGUIMode.OFF;
                Operation.Options = OperationOptions;
                Operation.SessionID = sessionId;

                int index = 0;
                HALKArgumentsFile file;

                foreach (var document in toBeSignedDocuments.Documents)
                {
                    file = new HALKArgumentsFile();
                    file.Id = (document.File_Id.ToString()).Replace("-", "");
                    file.Path = toBeSignedDocumentsPath + document.File_Name;
                    Input[index] = file;
                    index++;
                }

                Output.Path = signedDocumentsPath;
                if (isVerifyMode)
                {
                    Output.File = "verify_sign.xml";
                }
                Status.ResultFile = statusFilePath + "\\result.xml";
                Status.OutputFile = statusFilePath + "\\log.xml";

                using (var streamWriter = new System.IO.StreamWriter(HALK_ParametersXML_FileName))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(HALKArguments));
                    xmlSerializer.Serialize(streamWriter, HALK_Parameters);
                    streamWriter.Flush();
                }
            }
            catch (Exception e)
            {
                _logger.Exception(e);
                ShowTopMostMessage(Constants.msg_XMLSerializeUnsuccesfuly);
                return false;
            }
            return true;
        }
        public bool RunHALK_JarWithParamXML()
        {
            _logger.Debug("RunHALK_JarWithParamXML");
            //string longPathToJarFile = "C:\\Program Files\\Noscan\\HALK4";
            //string sortPathToJarFile = GetShortPathName(longPathToJarFile);
            //string installPath = GetJavaInstallationPath();
            //string filePath = System.IO.Path.Combine(installPath, "bin\\Java.exe");
            //ProcessStartInfo startInfo = new ProcessStartInfo(filePath, "-jar " + sortPathToJarFile + "\\halk4.jar -cmdfile " + HALK_ParametersXML_FileName);
            String batchFileName = Environment.Is64BitOperatingSystem ? "halk4_64.bat" : "halk4.bat";
            ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Noscan\\HALK4\\" + batchFileName, " -cmdfile " + HALK_ParametersXML_FileName);
            _logger.Debug("batchFileName: ", batchFileName);
            _logger.Debug("startInfo: " + startInfo.FileName + " " + startInfo.Arguments);
            startInfo.UseShellExecute = false;
            // *** Redirect the output ***
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;

            Process process = Process.Start(startInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            int exitCode = process.ExitCode;

            if (exitCode != 0)
            {
                HALKStatus Status;
                Status = ReadResultXML();
                if (Status.Operation.SessionID == sessionId & Status.Operation.Type == HALKStatusOperationType.SIGN)
                {
                    ShowTopMostMessage("Hibakód: " + exitCode.ToString(), "HALK program futtatása");
                    return false;
                }
            }

            process.Close();

            return true;
        }
        public string CutOffFileIdFromSignedDocumentsFileName(string filename)
        {
            string resultFileName = string.Empty;

            int fileNameLength = filename.Length;
            int pos_Prefix = filename.IndexOf("_Prefix_");
            resultFileName = filename.Substring(pos_Prefix + 8);
            return resultFileName;
        }
        public bool PutSignedDocumentsToDatabase()
        {
            _logger.Debug("PutSignedDocumentsToDatabase");
            signedDocuments = new TouchedDocuments();
            signedDocuments.Proc_Id = toBeSignedDocuments.Proc_Id;
            List<Document> signedDocumentsList = ReadSignedDocuments();
            foreach (var toBeSignedDocument in toBeSignedDocuments.Documents)
            {
                var fileName = System.IO.Path.GetFileNameWithoutExtension(toBeSignedDocumentsPath + "\\" + toBeSignedDocument.File_Name);
                _logger.Debug("FileName: " + fileName);
                foreach (var signedDocument in signedDocumentsList)
                {
                    if (signedDocument.File_Name.Contains(fileName))
                    {
                        signedDocument.File_Id = toBeSignedDocument.File_Id;
                        signedDocument.File_Name = CutOffFileIdFromSignedDocumentsFileName(signedDocument.File_Name);
                        signedDocument.URL = toBeSignedDocument.URL;//toBeSignedDocument.File_Path + "\\" + signedDocument.File_Name;
                        signedDocument.Irat_Id = toBeSignedDocument.Irat_Id;
                        signedDocument.Csatolmany_Id = toBeSignedDocument.Csatolmany_Id;
                        _logger.Debug("SignedDocument: ");
                        _logger.Debug(signedDocument.ToString());
                    }
                }
            }
            signedDocuments.Documents = signedDocumentsList;
            ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient ws = new ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient("EREC_Alairas_FolyamatokServiceSoap");
            ContentumServiceReference.ExecParam execParam = new ContentumServiceReference.ExecParam();
            execParam.Felhasznalo_Id = Felhasznalo_Id;
            execParam.FelhasznaloSzervezet_Id = FelhasznaloSzervezet_Id;
            Dictionary<string, string> resultErrors = new Dictionary<string, string>();
            foreach (Document d in signedDocumentsList)
            {
                ContentumServiceReference.Result rs = ws.uploadDocumentsWithoutSignWithVerify(execParam, d.Irat_Id, d.Csatolmany_Id.ToString(), d.File_Content, d.File_Name, isVerifyMode);
                if (!String.IsNullOrEmpty(rs.ErrorMessage))
                {
                    resultErrors.Add(d.File_Name, rs.ErrorMessage);
                }
                //ws.uploadDocuments
            }
            if (resultErrors.Count > 0)
            {
                String errorMessage = "Feltöltés során az alábbi állományokkal történtek hibák:\n";
                foreach (KeyValuePair<string, string> pair in resultErrors)
                {
                    errorMessage += pair.Key + ": " + pair.Value + "\n";
                }
                ShowTopMostMessage(errorMessage, "HALK feltöltés");
            }
            //upload
            //ContentumServiceReference.
            return true;
        }
        public List<Document> ReadSignedDocuments()
        {
            List<Document> signedDocumentsList = new List<Document>();
            try
            {
                byte[] bytes;
                string[] files = Directory.GetFiles(signedDocumentsPath);
                foreach (string file in files)
                {
                    var newDocument = new Document();
                    newDocument.File_Name = file;
                    using (FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read))
                    {
                        bytes = new byte[fileStream.Length];
                        fileStream.Read(bytes, 0, (int)fileStream.Length);
                    }
                    newDocument.File_Content = bytes;
                    signedDocumentsList.Add(newDocument);
                }
            }
            catch (Exception e)
            {
                _logger.Exception(e);
                ShowTopMostMessage(e.ToString());
            }
            return signedDocumentsList;
        }
        public void DeleteDocumentsFromOutputDirectory()
        {
            _logger.Debug("DeleteDocumentsFromOutputDirectory: ", signedDocumentsPath);
            string[] files = Directory.GetFiles(signedDocumentsPath);
            foreach (string file in files)
            {
                System.IO.File.Delete(file);
            }
        }
        public void DeleteDocumentsFromInputDirectory()
        {
            _logger.Debug("DeleteDocumentsFromInputDirectory: ", toBeSignedDocumentsPath);
            string[] files = Directory.GetFiles(toBeSignedDocumentsPath);
            foreach (string file in files)
            {
                System.IO.File.Delete(file);
            }
        }

        public TouchedDocuments GetTouchedDocuments(Guid proc_id)
        {
            if (proc_Id != null)
            {
                _logger.Debug("GetTouchedDocuments() proc_id: " + proc_Id);
            }
            TouchedDocuments retValue = new TouchedDocuments();
            List<Document> dokLista = new List<Document>();
            retValue.Documents = dokLista;

            ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient ws = new ContentumServiceReference.EREC_Alairas_FolyamatokServiceSoapClient("EREC_Alairas_FolyamatokServiceSoap");
            ContentumServiceReference.ExecParam execParam = new ContentumServiceReference.ExecParam();
            execParam.Felhasznalo_Id = Felhasznalo_Id;
            execParam.FelhasznaloSzervezet_Id = FelhasznaloSzervezet_Id;
            _logger.Debug("Calling EREC_Alairas_FolyamatokServiceSoapClient with. Felhasznalo_Id: ", Felhasznalo_Id);
            _logger.Debug("Calling EREC_Alairas_FolyamatokServiceSoapClient with. FelhasznaloSzervezet_Id: ", FelhasznaloSzervezet_Id);

            ContentumServiceReference.Result result = ws.GetToBeSignedDocumentsURL(execParam, proc_id);
            if (result.Ds == null)
            {
                _logger.Debug("Zero returned documents from service");
                return retValue;
            }
            ContentumServiceReference.Result rs = ws.setSignedStatus(execParam, proc_id, "01");
            retValue.Proc_Id = proc_id;

            List<Document> documentlist = new List<Document>();
            byte[] bytes = null;
            var rows = result.Ds.Tables[0].Rows;

            if (rows.Count == 0)
            {
                _logger.Debug("Zero returned documents from service");
            }
            foreach (DataRow row in rows)
            {
                try
                {
                    string documentUrl = (string)row["External_Link"];
                    //Ha az External_Link mezőben szerepel url paraméter is, azt levágja!
                    //string filePathFromURL = System.IO.Path.GetDirectoryName(documentUrl);
                    _logger.Debug("ExternalLink before cut: ", documentUrl);
                    string filePathFromURL = documentUrl.Contains("?") ? documentUrl.Substring(0, documentUrl.IndexOf('?')) : documentUrl;
                    _logger.Debug("ExternalLink after cut: ", filePathFromURL);

                    string fileName = System.IO.Path.GetFileName(filePathFromURL);
                    if (fileName.ToLower().EndsWith(".pdf"))
                    {
                        bytes = ws.GetToBeSignedDocument(execParam, documentUrl);
                        _logger.Debug("Saving document from service");
                        if (bytes != null)
                        {
                            Document document = new Document();
                            document.File_Id = (Guid)row["Id"];
                            document.URL = documentUrl;
                            document.File_Name = (string) row["FajlNev"];
                           // document.File_Path = System.IO.Path.GetDirectoryName(documentUrl);
                            document.File_Content = bytes;
                            document.Irat_Id = (Guid)row["IraIrat_Id"];
                            document.Csatolmany_Id = (Guid)row["Csatolmany_Id"];
                            _logger.Debug(document.ToString());
                            documentlist.Add(document);
                        }
                        else
                        {
                            _logger.Debug("Downloading document with webclient from: ", documentUrl);
                            byte[] response = new System.Net.WebClient().DownloadData(documentUrl);
                            //byte[] response = wc.DownloadData(documentUrl);
                            if (response != null)
                            {
                                _logger.Debug("Got response");
                                Document document = new Document();
                                document.File_Id = (Guid)row["Id"];
                                document.URL = documentUrl;
                                document.File_Name = fileName;
                               // document.File_Path = System.IO.Path.GetDirectoryName(documentUrl);
                                document.File_Content = response;
                                document.Irat_Id = (Guid)row["IraIrat_Id"];
                                document.Csatolmany_Id = (Guid)row["Csatolmany_Id"];
                                documentlist.Add(document);
                            }
                            else
                            {
                                _logger.Debug("Response is null.");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Exception(ex);
                    String exceptionStr = ex.Message;
                }
            }

            retValue.Documents = documentlist;

            return retValue;
        }
        public HALKStatus ReadResultXML()
        {
            HALKStatus hALKStatus = new HALKStatus();
            using (var stream = System.IO.File.OpenRead(statusFilePath + "\\result.xml"))
            {
                var serializer = new XmlSerializer(typeof(HALKStatus));
                hALKStatus = serializer.Deserialize(stream) as HALKStatus;
            }

            return hALKStatus;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string longPathToJarFile = "C:\\Program Files\\Noscan\\HALK4";
            string sortPathToJarFile = GetShortPathName(longPathToJarFile);
            ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_131\\bin\\java.exe", "-jar " + sortPathToJarFile + "\\halk4.jar -cmdfile E:\\HALK\\HALK_Param_Arpad.xml");

            startInfo.UseShellExecute = false;
            // *** Redirect the output ***
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;

            Process process = Process.Start(startInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            int exitCode = process.ExitCode;
            _logger.Debug("HalkJar  output: " + (string.IsNullOrEmpty(output) ? "(none)" : output));
            _logger.Debug("HalkJar  error: " + (string.IsNullOrEmpty(error) ? "(none)" : error));

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
        }
        [DllImport("kernel32.dll", EntryPoint = "GetShortPathName", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetShortPathName([MarshalAs(UnmanagedType.LPTStr)] string path,
                                                   [MarshalAs(UnmanagedType.LPTStr)] StringBuilder shortPath,
                                                                                     int shortPathLength);

        public static string GetShortPathName(string longPath)
        {
            StringBuilder shortPath = new StringBuilder(longPath.Length + 1);
            int len = GetShortPathName(longPath, shortPath, shortPath.Capacity);
            if (len == 0) throw new System.ComponentModel.Win32Exception();
            return shortPath.ToString();
        }

        private NameValueCollection GetQueryStringParameters()
        {
            NameValueCollection nameValueTable = new NameValueCollection();

            if (ApplicationDeployment.CurrentDeployment.ActivationUri == null)
            {
                _logger.Debug("ApplicationDeployment.CurrentDeployment.ActivationUri is null");
            }
            else
            {
                _logger.Debug("ApplicationDeployment.CurrentDeployment.ActivationUri :", ApplicationDeployment.CurrentDeployment.ActivationUri.ToString());
            }

            _logger.Debug("IsNetworkDeployed: " + ApplicationDeployment.IsNetworkDeployed);
            //TEST add Proc_Id = 4672167C-3FA8-E711-80C6-00155D020BD9
            if (testMode || ApplicationDeployment.IsNetworkDeployed)
            {
                if (testMode || ApplicationDeployment.CurrentDeployment.ActivationUri != null)
                {
                    string queryString = "";
                    try
                    {
                        queryString = testMode ? "" : ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                    }
                    catch (Exception e)
                    {
                        _logger.Exception(e);
                    }
                    if (testMode)
                    {
                        string testQueryString = "Proc_Id=" + testProcId + "&Felh_Id=" + testFelhasznalo_Id + "&FelhSz_Id=" + testFelhasznaloSzervezet_Id;
                        queryString += string.IsNullOrEmpty(queryString) ? testQueryString : "&" + testQueryString;
                    }
                    _logger.Debug("QueryString: ", queryString);
                    if (!string.IsNullOrEmpty(queryString))
                        nameValueTable = HttpUtility.ParseQueryString(queryString);
                }
            }

            return (nameValueTable);
        }



        /// <summary>
        /// This method creates a message box which will be TopMost to the other windows
        /// </summary>
        /// <param name="message"></param>
        public void ShowTopMostMessage(string message, string title = "", bool isSuccessFull = false)
        {
            _logger.Debug(message);
            new MessageWindow(message, title, isSuccessFull).ShowDialog();
        }

        private string GetJavaInstallationPath()
        {
            string environmentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
            if (!string.IsNullOrEmpty(environmentPath))
                return environmentPath;

            string javaKey = "SOFTWARE\\JavaSoft\\Java Runtime Environment\\";
            using (Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(javaKey))
            {
                string currentVersion = rk.GetValue("CurrentVersion").ToString();
                using (Microsoft.Win32.RegistryKey key = rk.OpenSubKey(currentVersion))
                {
                    return key.GetValue("JavaHome").ToString();
                }
            }
        }
    }
}