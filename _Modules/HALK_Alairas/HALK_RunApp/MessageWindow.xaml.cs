﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace HALK_RunApp
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window
    {
        public MessageWindow(string message, string title,bool isSuccessFull)
        {
            InitializeComponent();
            this.messageLabel.Content = message;
            this.messageLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
            this.Title = string.IsNullOrEmpty(title) ? "HALK" : title;
            this.Topmost = true;
            this.Width = 250;
            this.Height = 100;
            this.ResizeMode = ResizeMode.NoResize;

            this.Icon = isSuccessFull ? SystemIcons.Information.ToImageSource() : SystemIcons.Warning.ToImageSource();
        }

        private void BtnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
