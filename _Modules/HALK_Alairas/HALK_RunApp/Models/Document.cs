﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HALK_RunApp.Models
{
    public class Document
    {
        public Guid File_Id { get; set; }
        public string File_Name { get; set; }
        // public string File_Path { get; set; }
        public string URL { get; set; }
        public byte[] File_Content { get; set; }
        public Guid Irat_Id { get; set; }
        public Guid Csatolmany_Id { get; set; }
        public override string ToString()
        {
            string result = "";
            result += nameof(File_Name) + " : ";
            result += File_Name == null ? "null" : File_Name;
            result += "\n " + nameof(File_Id) + " : ";
            result += File_Id == null ? "null" : File_Id.ToString();
            result += "\n " + nameof(URL) + " : ";
            result += URL == null ? "null" : URL.ToString();
            result += "\n " + nameof(Irat_Id) + " : ";
            result += Irat_Id == null ? "null" : Irat_Id.ToString();
            result += "\n " + nameof(Csatolmany_Id) + " : ";
            result += Csatolmany_Id == null ? "null" : Csatolmany_Id.ToString();
            return result;
        }
    }

    public class TouchedDocuments
    {
        public Guid Proc_Id { get; set; }
        public List<Document> Documents { get; set; }
        public override string ToString()
        {
            string result = "";
            result += nameof(Proc_Id) + " : ";
            result += Proc_Id == null ? "null" : Proc_Id.ToString();           
            return result;
        }
    }
}
