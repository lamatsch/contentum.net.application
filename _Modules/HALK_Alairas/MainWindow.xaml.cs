﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Deployment.Application;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HALK_RunApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Guid proc_Id;
        public TouchedDocuments touchedDocuments;
        public TouchedDocuments toBeSignedDocuments;
        public TouchedDocuments SignedDocuments;
        public MainWindow()
        {

            InitializeComponent();

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                Version version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                this.Title = "Digitális aláírás indítása - Verzió: " + version.ToString();
            }

            NameValueCollection nvc = null;
            try
            {
                nvc = GetQueryStringParameters();

            }
            catch (Exception e)
            {

                System.Windows.MessageBox.Show(e.Message);
            }
            if (nvc == null || nvc.Keys.Count == 0)
            {
                System.Windows.MessageBox.Show("Nincs QueryStringParameter megadva.");
            }
            else
            { 
                var keys = nvc.AllKeys;
                if (!keys.Contains("Proc_Id"))
                {
                    System.Windows.MessageBox.Show("Nincs Proc_Id a paraméterek között.");
                    proc_Id = Guid.NewGuid();
                }
                else
                {
                    proc_Id = new Guid(nvc["Proc_Id"]);

                    //this.Close();
                }
                // Aláírandó dokumentumok felolvasása
                touchedDocuments = GetAlairandoDokumentumok(proc_Id);

                toBeSignedDocuments = CompliteTouchedDocumentsFileNameWithFile_Id(touchedDocuments);
                foreach (var item in toBeSignedDocuments.Documents)
                {
                    System.Windows.MessageBox.Show(item.File_Name);
                }

            }
        }
        public TouchedDocuments CompliteTouchedDocumentsFileNameWithFile_Id(TouchedDocuments touchedDocuments)
        {
            toBeSignedDocuments             = new TouchedDocuments();
            toBeSignedDocuments.Proc_Id     = touchedDocuments.Proc_Id;
            toBeSignedDocuments.Documents   = new List<Document>();

            foreach (var document in touchedDocuments.Documents)
            {
                var toBeSignedDocument = new Document();

                toBeSignedDocument.File_Id = document.File_Id;
                toBeSignedDocument.File_Name = document.File_Id.ToString() + "_Prefix_" + document.File_Name;
                toBeSignedDocument.File_Content = document.File_Content;

                toBeSignedDocuments.Documents.Add(toBeSignedDocument);
            }
            return toBeSignedDocuments;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // működik
            //--------
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Internet Explorer\\IExplore.exe", "www.axis.hu");

            //ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "echo testing");

            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_111\\bin\\java.exe", "-Xms1280M -Xmx1280M -jar C:\\Program Files\\Noscan\\HALK4\\halk4.jar -cmdfile F:\\HALK\\HALK_Param_Arpad.xml");
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_111\\bin\\java.exe", "-jar C:\\Program Files\\Noscan\\HALK4\\halk4.jar -cmdfile F:\\HALK\\HALK_Param_Arpad.xml");
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_111\\bin\\java.exe", "-jar %~dpC:\\Program Files\\Noscan\\HALK4\\halk4.jar -cmdfile F:\\HALK\\HALK_Param_Arpad.xml");
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_111\\bin\\java.exe", "-jar %~dpC:\\Program Files\\Noscan\\HALK4\\halk4.jar");

            string longPathToJarFile = "C:\\Program Files\\Noscan\\HALK4";
            string sortPathToJarFile = GetShortPathName(longPathToJarFile);
            ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_131\\bin\\java.exe", "-jar " + sortPathToJarFile  + "\\halk4.jar -cmdfile E:\\HALK\\HALK_Param_Arpad.xml");
            
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Java\\jre1.8.0_111\\bin\\java.exe", "-jar C:\\Program Files\\Noscan\\HALK4\\halk4.jar");

            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Noscan\\HALK4\\halk4.jar", "-cmdfile F:\\HALK\\HALK_Param_Arpad.xml");
            //ProcessStartInfo startInfo = new ProcessStartInfo("C:\\Program Files\\Noscan\\HALK4\\halk4.jar");
            startInfo.UseShellExecute = false;
            // *** Redirect the output ***
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;

            Process process = Process.Start(startInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            int exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();

            //ProcessStandardOutput.Text = process.StandardOutput.ReadToEnd();
        }
        [DllImport("kernel32.dll", EntryPoint = "GetShortPathName", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetShortPathName( [MarshalAs(UnmanagedType.LPTStr)] string path,
                                                   [MarshalAs(UnmanagedType.LPTStr)] StringBuilder shortPath,
                                                                                     int shortPathLength);

        public static string GetShortPathName(string longPath)
        {
            StringBuilder shortPath = new StringBuilder(longPath.Length + 1);
            int len = GetShortPathName(longPath, shortPath, shortPath.Capacity);
            if (len == 0) throw new System.ComponentModel.Win32Exception();
            return shortPath.ToString();
        }

        private NameValueCollection GetQueryStringParameters()
        {
            NameValueCollection nameValueTable = new NameValueCollection();

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                System.Windows.MessageBox.Show("ApplicationDeployment.IsNetworkDeployed");
                if (ApplicationDeployment.CurrentDeployment.UpdateLocation != null)
                {
                    System.Windows.MessageBox.Show(ApplicationDeployment.CurrentDeployment.UpdateLocation.ToString());
                    string queryString = ApplicationDeployment.CurrentDeployment.UpdateLocation.Query;
                    if (!string.IsNullOrEmpty(queryString))
                    {
                        nameValueTable = HttpUtility.ParseQueryString(queryString);
                    }

                }
                if (ApplicationDeployment.CurrentDeployment.ActivationUri != null)
                {
                    System.Windows.MessageBox.Show(ApplicationDeployment.CurrentDeployment.ActivationUri.ToString());
                    string queryString = ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                    if (!string.IsNullOrEmpty(queryString))
                    {
                        nameValueTable = HttpUtility.ParseQueryString(queryString);
                    }

                }

            }

            return (nameValueTable);
        }

        public class Document
        {
            public Guid File_Id { get; set; }
            public string File_Name { get; set; }
            public byte[] File_Content { get; set; }
        }

        public class TouchedDocuments
        {
            public Guid Proc_Id { get; set; }
            public List<Document> Documents { get; set; }
        }

        public TouchedDocuments GetAlairandoDokumentumok(Guid proc_id)
        {
            TouchedDocuments retValue;

            retValue = new TouchedDocuments();
            retValue.Proc_Id = proc_id;
            List<Document> documents = new List<Document>();

            Document document = new Document();
            document.File_Id = Guid.NewGuid();
            document.File_Name = "vica utalás otp ltp.docx";

            documents.Add(document);

            retValue.Documents = documents;


            return retValue;
        }
    }
}
