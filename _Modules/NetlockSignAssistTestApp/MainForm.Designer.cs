﻿namespace TestApp1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Sign = new System.Windows.Forms.Button();
            this.textBox_Out = new System.Windows.Forms.TextBox();
            this.textBox_Server = new System.Windows.Forms.TextBox();
            this.textBox_In = new System.Windows.Forms.TextBox();
            this.button_SignError = new System.Windows.Forms.Button();
            this.button_Validate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Sign
            // 
            this.button_Sign.Location = new System.Drawing.Point(12, 23);
            this.button_Sign.Name = "button_Sign";
            this.button_Sign.Size = new System.Drawing.Size(75, 23);
            this.button_Sign.TabIndex = 0;
            this.button_Sign.Text = "Sign";
            this.button_Sign.UseVisualStyleBackColor = true;
            this.button_Sign.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_Out
            // 
            this.textBox_Out.Location = new System.Drawing.Point(12, 248);
            this.textBox_Out.Multiline = true;
            this.textBox_Out.Name = "textBox_Out";
            this.textBox_Out.Size = new System.Drawing.Size(776, 181);
            this.textBox_Out.TabIndex = 1;
            // 
            // textBox_Server
            // 
            this.textBox_Server.Location = new System.Drawing.Point(395, 23);
            this.textBox_Server.Name = "textBox_Server";
            this.textBox_Server.Size = new System.Drawing.Size(393, 20);
            this.textBox_Server.TabIndex = 2;
            // 
            // textBox_In
            // 
            this.textBox_In.Location = new System.Drawing.Point(12, 52);
            this.textBox_In.Multiline = true;
            this.textBox_In.Name = "textBox_In";
            this.textBox_In.Size = new System.Drawing.Size(776, 190);
            this.textBox_In.TabIndex = 3;
            this.textBox_In.Text = "NetLockSignAssist sign/validate test data\r\n12345678901234567890123456789012345678" +
    "90123456789012345678901234567890123456789012345678901234567890\r\nOK";
            // 
            // button_SignError
            // 
            this.button_SignError.Location = new System.Drawing.Point(93, 23);
            this.button_SignError.Name = "button_SignError";
            this.button_SignError.Size = new System.Drawing.Size(135, 23);
            this.button_SignError.TabIndex = 4;
            this.button_SignError.Text = "Sign error handling test";
            this.button_SignError.UseVisualStyleBackColor = true;
            this.button_SignError.Click += new System.EventHandler(this.button_SignError_Click);
            // 
            // button_Validate
            // 
            this.button_Validate.Location = new System.Drawing.Point(234, 23);
            this.button_Validate.Name = "button_Validate";
            this.button_Validate.Size = new System.Drawing.Size(75, 23);
            this.button_Validate.TabIndex = 5;
            this.button_Validate.Text = "Validate";
            this.button_Validate.UseVisualStyleBackColor = true;
            this.button_Validate.Click += new System.EventHandler(this.button_Validate_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_Validate);
            this.Controls.Add(this.button_SignError);
            this.Controls.Add(this.textBox_In);
            this.Controls.Add(this.textBox_Server);
            this.Controls.Add(this.textBox_Out);
            this.Controls.Add(this.button_Sign);
            this.Name = "MainForm";
            this.Text = "NetLock SignAssist Server TestApp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Sign;
        private System.Windows.Forms.TextBox textBox_Out;
        private System.Windows.Forms.TextBox textBox_Server;
        private System.Windows.Forms.TextBox textBox_In;
        private System.Windows.Forms.Button button_SignError;
        private System.Windows.Forms.Button button_Validate;
    }
}

