﻿using Contentum.eUtility;
using Contentum.eUtility.Netlock;
using Contentum.eUtility.NetLockSignAssist;
using System;
using System.Text;
using System.Windows.Forms;

namespace TestApp1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Sign(string fileName)
        {
            try
            {
                byte[] documentData;

                var netlockService = new NetLockSignAssistService();
                try
                {
                    var contentOriginal = Encoding.UTF8.GetBytes(textBox_In.Text);
                    documentData = netlockService.Sign(null, fileName, contentOriginal);
                }
                catch (Exception x)
                {
                    var errorMessage = x.Message;
                    Logger.Error("NETLOCKSignatureService hiba: " + errorMessage);
                    netlockService.ValidationReport.Dump();
                    throw new ResultException("NETLOCKSignatureService hiba: " + errorMessage);
                }

                textBox_Out.Text = documentData == null ? "null" : Convert.ToBase64String(documentData);
            }
            catch (Exception x)
            {
                textBox_Out.Text = x.Message;
            }
        }

        private void Validate(string fileName)
        {
            try
            {
                var validationReportJson = "";

                var netlockService = new NetLockSignAssistService();
                try
                {
                    var contentOriginal = Encoding.UTF8.GetBytes(textBox_In.Text);
                    validationReportJson = netlockService.Validate(fileName, contentOriginal);
                    textBox_Out.Text = validationReportJson == null ? "null" : validationReportJson + Environment.NewLine + "-----------------------";

                    var validationReport = NetLockHelper.DeserializeValidation(validationReportJson);
                    var signatureDataList = SignatureData.ParseNetLockSignAssistValidationReport(validationReport, null);
                    foreach (var signatureData in signatureDataList)
                    {
                        textBox_Out.Text += signatureData.ToHTML(null) + Environment.NewLine;
                    }
                }
                catch (Exception x)
                {
                    var errorMessage = x.Message;
                    Logger.Error("NETLOCKSignatureService hiba: " + errorMessage);
                    throw new ResultException("NETLOCKSignatureService hiba: " + errorMessage);
                }
            }
            catch (Exception x)
            {
                textBox_Out.Text = x.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           Sign("fileToSign.dat");
            
        }

        private void button_SignError_Click(object sender, EventArgs e)
        {
            Sign("error.handling.test");
        }

        private void button_Validate_Click(object sender, EventArgs e)
        {
            Validate("fileToValidate.dat");
        }
    }
}
