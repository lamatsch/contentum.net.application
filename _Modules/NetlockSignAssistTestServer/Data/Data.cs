﻿using IO.Swagger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetlockSignAssistTestServer.Data
{
    public class Data
    {
        public static Data DefaultData = new Data();

        protected List<KeyStoreDetail> _keyStores = new List<KeyStoreDetail>();
        public KeyStoreDetail GetKeyStore(string keyStoreName,
                                          string password)
        {
            var keyStore = _keyStores.FirstOrDefault(ks => ks.Name == keyStoreName);
            if (keyStore == null) throw new Exception($"Keystore {keyStoreName} not found.");
            if (!string.IsNullOrEmpty(password) && keyStore.Password != password) throw new Exception("Invalid password.");
            return keyStore;
        }
        public KeyStoreDetail ActiveKeyStore { get; private set; }
        public void ActivateKeyStore(KeyStoreDetail keyStore) {
            ActiveKeyStore = keyStore;
        }

        protected SignatureSession _signatureSession = null;

        public SignatureSession CreateSignatureSession(KeyStoreDetail keyStore) {
            _signatureSession = new SignatureSession(keyStore);
            return _signatureSession;
        }

        public void CheckSignatureSession(string sessionId)
        {
            if (string.IsNullOrEmpty(_signatureSession?.SessionId)) throw new Exception("Invalid session.");
        }

        public void ClearSignatureSession()
        {
            _signatureSession = null;
        }

        protected Data()
        {
            _keyStores.Add(new KeyStoreDetail ("TestKeyStore1", "pwd1", new[] { "KeyHolder1a", "KeyHolder1b" }));
            _keyStores.Add(new KeyStoreDetail("TestKeyStore2", "pwd2", new[] { "KeyHolder2a", "KeyHolder2b", "KeyHolder2c" }));
            _keyStores.Add(new KeyStoreDetail("TestKeyStore3", "pwd3", new[] { "KeyHolder3a" }));
        }
    }
}
