/*
 * SignAssist
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.14.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class SignAssistRemoteSignatureParameters : IEquatable<SignAssistRemoteSignatureParameters>
    { 
        /// <summary>
        /// Signature profile (form+level) to use. One of: XAdES_BASELINE_LTA, XAdES_BASELINE_LT, XAdES_BASELINE_T, XAdES_BASELINE_B, CAdES_BASELINE_LTA, CAdES_BASELINE_LT, CAdES_BASELINE_T, CAdES_BASELINE_B, PAdES_BASELINE_LTA, PAdES_BASELINE_LT, PAdES_BASELINE_T, PAdES_BASELINE_B 
        /// </summary>
        /// <value>Signature profile (form+level) to use. One of: XAdES_BASELINE_LTA, XAdES_BASELINE_LT, XAdES_BASELINE_T, XAdES_BASELINE_B, CAdES_BASELINE_LTA, CAdES_BASELINE_LT, CAdES_BASELINE_T, CAdES_BASELINE_B, PAdES_BASELINE_LTA, PAdES_BASELINE_LT, PAdES_BASELINE_T, PAdES_BASELINE_B </value>
        [DataMember(Name="signatureLevel")]
        public string SignatureLevel { get; set; }

        /// <summary>
        /// Packaging method of the signature. One of: ENVELOPED, ENVELOPING, DETACHED 
        /// </summary>
        /// <value>Packaging method of the signature. One of: ENVELOPED, ENVELOPING, DETACHED </value>
        [DataMember(Name="signaturePackaging")]
        public string SignaturePackaging { get; set; }

        /// <summary>
        /// Encryption algorithm plus digest algorithm to use. Specify as encAlgId_digAlgId, e.g. RSA_SHA1, RSA_SHA256, ECDSA_SHA1, ECDSA_SHA512. If not specified, RSA_SHA256 will be used. 
        /// </summary>
        /// <value>Encryption algorithm plus digest algorithm to use. Specify as encAlgId_digAlgId, e.g. RSA_SHA1, RSA_SHA256, ECDSA_SHA1, ECDSA_SHA512. If not specified, RSA_SHA256 will be used. </value>
        [DataMember(Name="signatureAlgorithm")]
        public string SignatureAlgorithm { get; set; }

        /// <summary>
        /// Asic container type. One of: ASIC_E, ASIC_S 
        /// </summary>
        /// <value>Asic container type. One of: ASIC_E, ASIC_S </value>
        [DataMember(Name="asicContainerType")]
        public string AsicContainerType { get; set; }

        /// <summary>
        /// Gets or Sets Pades
        /// </summary>
        [DataMember(Name="pades")]
        public RemotePadesSignatureParameters Pades { get; set; }

        /// <summary>
        /// Set the container type for the signature, such as \&quot;EAKTA\&quot; (\&quot;dosszie\&quot;). Currently \&quot;EAKTA\&quot; is the only supported type. signatureLevel must be a XAdES type, signaturePackaging must be \&quot;ENVELOPED\&quot;. 
        /// </summary>
        /// <value>Set the container type for the signature, such as \&quot;EAKTA\&quot; (\&quot;dosszie\&quot;). Currently \&quot;EAKTA\&quot; is the only supported type. signatureLevel must be a XAdES type, signaturePackaging must be \&quot;ENVELOPED\&quot;. </value>
        [DataMember(Name="containerType")]
        public string ContainerType { get; set; }

        /// <summary>
        /// Gets or Sets SigningCertificate
        /// </summary>
        [DataMember(Name="signingCertificate")]
        public RemoteCertificate SigningCertificate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class SignAssistRemoteSignatureParameters {\n");
            sb.Append("  SignatureLevel: ").Append(SignatureLevel).Append("\n");
            sb.Append("  SignaturePackaging: ").Append(SignaturePackaging).Append("\n");
            sb.Append("  SignatureAlgorithm: ").Append(SignatureAlgorithm).Append("\n");
            sb.Append("  AsicContainerType: ").Append(AsicContainerType).Append("\n");
            sb.Append("  Pades: ").Append(Pades).Append("\n");
            sb.Append("  ContainerType: ").Append(ContainerType).Append("\n");
            sb.Append("  SigningCertificate: ").Append(SigningCertificate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((SignAssistRemoteSignatureParameters)obj);
        }

        /// <summary>
        /// Returns true if SignAssistRemoteSignatureParameters instances are equal
        /// </summary>
        /// <param name="other">Instance of SignAssistRemoteSignatureParameters to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(SignAssistRemoteSignatureParameters other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    SignatureLevel == other.SignatureLevel ||
                    SignatureLevel != null &&
                    SignatureLevel.Equals(other.SignatureLevel)
                ) && 
                (
                    SignaturePackaging == other.SignaturePackaging ||
                    SignaturePackaging != null &&
                    SignaturePackaging.Equals(other.SignaturePackaging)
                ) && 
                (
                    SignatureAlgorithm == other.SignatureAlgorithm ||
                    SignatureAlgorithm != null &&
                    SignatureAlgorithm.Equals(other.SignatureAlgorithm)
                ) && 
                (
                    AsicContainerType == other.AsicContainerType ||
                    AsicContainerType != null &&
                    AsicContainerType.Equals(other.AsicContainerType)
                ) && 
                (
                    Pades == other.Pades ||
                    Pades != null &&
                    Pades.Equals(other.Pades)
                ) && 
                (
                    ContainerType == other.ContainerType ||
                    ContainerType != null &&
                    ContainerType.Equals(other.ContainerType)
                ) && 
                (
                    SigningCertificate == other.SigningCertificate ||
                    SigningCertificate != null &&
                    SigningCertificate.Equals(other.SigningCertificate)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (SignatureLevel != null)
                    hashCode = hashCode * 59 + SignatureLevel.GetHashCode();
                    if (SignaturePackaging != null)
                    hashCode = hashCode * 59 + SignaturePackaging.GetHashCode();
                    if (SignatureAlgorithm != null)
                    hashCode = hashCode * 59 + SignatureAlgorithm.GetHashCode();
                    if (AsicContainerType != null)
                    hashCode = hashCode * 59 + AsicContainerType.GetHashCode();
                    if (Pades != null)
                    hashCode = hashCode * 59 + Pades.GetHashCode();
                    if (ContainerType != null)
                    hashCode = hashCode * 59 + ContainerType.GetHashCode();
                    if (SigningCertificate != null)
                    hashCode = hashCode * 59 + SigningCertificate.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(SignAssistRemoteSignatureParameters left, SignAssistRemoteSignatureParameters right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SignAssistRemoteSignatureParameters left, SignAssistRemoteSignatureParameters right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
