@echo off
rem date format: d:MM-DD-YY
cd..
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to copy ALL dlls from ContentumFolder to remote drive \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T? (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

echo. ---------- eAdmin Bin ----------
xcopy .\eAdmin\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eAdminwebservice\bin\ /I 
xcopy .\eAdmin\Website\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eAdmin\bin\ /I


echo. ---------- eDocument Bin ----------
xcopy .\eDocument\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eDocumentWebService\bin\ /I 


echo. ---------- eMigration Bin ----------
xcopy .\eMigration\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eMigrationwebservice\bin\ /I 
xcopy .\eMigration\Website\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eMigration\bin\ /I 


echo. ---------- eRecord Bin ----------
xcopy .\eRecord\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eRecordwebservice\bin\  /I 
xcopy .\eRecord\Website\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eRecord\bin\ /I 


rem echo. ---------- eSharePoint Bin ----------
rem xcopy .\eSharePoint\Webservice\bin w:\ContentumWebSites\_PATCH_R2T\eSharePoint\webservice\bin /S /I /d:08-23-17


echo. ---------- eTemplateManager Bin ----------
xcopy .\eTemplateManager\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eTemplateManagerwebservice\bin\ /I 

echo. ---------- eIntegrator Bin ----------
xcopy .\eIntegrator\Webservice\bin\Contentum*.dll \\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\_PATCH_R2T\eIntegratorwebservice\bin\  /I 

pause
:END
endlocal