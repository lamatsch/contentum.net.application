@echo off
rem date format: d:MM-DD-YY
cd..
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to copy ALL dlls from ContentumFolder to remote AX-VFPHTST03:? (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

echo. ---------- eRecord Bin ----------
xcopy .\eRecord\Webservice\bin \\AX-VFPHTST03\C$\ContentumWebSites\_PATCH_R2T\eRecordwebservice\bin\Contentum*.dll /S /I /q /y
xcopy .\eRecord\Website\bin \\AX-VFPHTST03\C$\ContentumWebSites\_PATCH_R2T\eRecord\bin\Contentum*.dll /S /I /q /y

pause
:END
endlocal