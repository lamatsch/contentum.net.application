$OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding = New-Object System.Text.UTF8Encoding
cd ..
$FirstCommit = Read-Host -Prompt 'Input the older commit id'
$SecondCommit = Read-Host -Prompt 'Input the newer commit id'

git diff --name-only $FirstCommit $SecondCommit > .\tmp.txt

New-Item -ItemType Directory -Force -Path  "patch"

foreach($line in Get-Content .\tmp.txt) {
    $pattern = '/'
	$line = $line-replace $pattern, '\'
	Write-Host "patch\"$line
	$patchFolder = "patch\"
	$patchPath = $patchFolder+$line+'*'
	xcopy /y $line $patchPath
}


Compress-Archive -Path .\patch\* -CompressionLevel Fastest -Force -DestinationPath .\patch.zip
 

git diff --diff-filter=D --name-only $FirstCommit $SecondCommit > .\deleted.txt

If (!((Get-Content ".\deleted.txt") -eq $Null)) { 
	Write-Host "\Deleted files: " -ForegroundColor red
	foreach($line in Get-Content .\deleted.txt) {
		$pattern = '/'
		$line = $line-replace $pattern, '\'
		Write-Host $line		
	}
}
 
Remove-Item deleted.txt
Remove-Item tmp.txt
Remove-Item -LiteralPath ".\patch\" -Force -Recurse

