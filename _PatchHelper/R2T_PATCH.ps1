$OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding = New-Object System.Text.UTF8Encoding
cd ..

$CreatePatchSinceLastCommit = Read-Host -Prompt 'Create patch from last merge? (y=yes/q=quit)'
if($CreatePatchSinceLastCommit -eq  "q"){
	exit
}
$YoungerCommitID = git rev-parse HEAD~0
if($CreatePatchSinceLastCommit -eq "y"){
  $OlderCommitID = git rev-parse HEAD~1
  $LastMerge = git show $OlderCommitID  
  Write-Host "Creating patch since: " + $OlderCommitID
 
}else{
	$OlderCommitID = Read-Host -Prompt 'Enter the older commit ID'
	$YoungerCommitID = Read-Host -Prompt 'Enter the younger commit ID'
}

git diff --name-only $OlderCommitID $YoungerCommitID >  .\tmp.txt

New-Item -ItemType Directory -Force -Path  "patch-temp"
New-Item -ItemType Directory -Force -Path  "patch-zip"

foreach($line in Get-Content .\tmp.txt) {
    $pattern = '/'
	$line = $line-replace $pattern, '\'
	Write-Host "patch-temp\"$line
	$patchFolder = "patch-temp\"
	$patchPath = $patchFolder+$line+'*'
	xcopy /y $line $patchPath
}

#Atrendezzuk az R2T nek megfelelo mappaszerkezetbe
xcopy /y /s /i ".\patch-temp\eRecord\WebService\*" ".\patch-zip\eRecordWebService\"
xcopy /y /s /i ".\patch-temp\eRecord\WebSite\*" ".\patch-zip\eRecord\"

xcopy /y /s /i ".\patch-temp\eAdmin\WebService\*" ".\patch-zip\eAdminWebService\"
xcopy /y /s /i ".\patch-temp\eAdmin\WebSite\*" ".\patch-zip\eAdmin\"

xcopy /y /s /i ".\patch-temp\eDocument\WebService\*" ".\patch-zip\eDocumentWebService\"

xcopy /y /s /i ".\patch-temp\eIntegrator\WebService\*" ".\patch-zip\eIntegratorWebService\"

xcopy /y /s /i ".\patch-temp\eMigration\WebService\*" ".\patch-zip\eMigrationWebService\"
xcopy /y /s /i ".\patch-temp\eMigration\WebSite\*" ".\patch-zip\eMigration\"

git diff --diff-filter=D --name-only $OlderCommitID $YoungerCommitID > .\patch-zip\deleted.txt

If ((Test-Path ".\patch-zip\deleted.txt" -PathType leaf) -and !((Get-Content ".\patch-zip\deleted.txt") -eq $Null)) { 
	Write-Host "\Deleted files: " -ForegroundColor red
	foreach($line in Get-Content .\patch-zip\deleted.txt) {
		$pattern = '/'
		$line = $line-replace $pattern, '\'
		Write-Host $line		
	}
} 

$CopyBin = Read-Host -Prompt "Include DLL-s in the patch? (y=yes/n=no/q=quit)"
if($CopyBin -eq  "q"){
	Remove-Item tmp.txt
	Remove-Item -LiteralPath ".\patch-temp\" -Force -Recurse
	exit
}
if($CopyBin -eq  "y"){
	xcopy /y /s /i ".\eAdmin\Webservice\bin\Contentum*.dll" ".\patch-zip\eAdminwebservice\bin\" 
	xcopy /y /s /i ".\eAdmin\Website\bin\Contentum*.dll" ".\patch-zip\eAdmin\bin\" 

	xcopy /y /s /i ".\eDocument\Webservice\bin\Contentum*.dll" ".\patch-zip\eDocumentWebService\bin\"  

	xcopy /y /s /i ".\eMigration\Webservice\bin\Contentum*.dll" ".\patch-zip\eMigrationWebservice\bin\"  
	xcopy /y /s /i ".\eMigration\Website\bin\Contentum*.dll" ".\patch-zip\eMigration\bin\"  

	xcopy /y /s /i ".\eRecord\Webservice\bin\Contentum*.dll" ".\patch-zip\eRecordWebservice\bin\"   
	xcopy /y /s /i ".\eRecord\Website\bin\Contentum*.dll" ".\patch-zip\eRecord\bin\"  

	xcopy /y /s /i ".\eTemplateManager\Webservice\bin\Contentum*.dll" ".\patch-zip\eTemplateManagerWebservice\bin\" 

	xcopy /y /s /i ".\eIntegrator\Webservice\bin\Contentum*.dll" ".\patch-zip\eIntegratorWebservice\bin\"   
} 

Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include *.config -Recurse | Remove-Item
Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include *.pdb -Recurse | Remove-Item
Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include MasterPage.master.cs -Recurse | Remove-Item
Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include MasterPage.master -Recurse | Remove-Item
Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include BelepesiAdatok.ascx -Recurse | Remove-Item
Get-ChildItem .\patch-zip -ErrorAction SilentlyContinue -Include BelepesiAdatok.ascx.cs -Recurse | Remove-Item
Get-ChildItem .\patch-zip\eAdmin\images\hu\fejlec\ -ErrorAction SilentlyContinue -Include *.* -Recurse | Remove-Item
Get-ChildItem .\patch-zip\eRecord\images\hu\fejlec\ -ErrorAction SilentlyContinue -Include *.* -Recurse | Remove-Item
Get-ChildItem .\patch-zip\eIntegratorWebService\HALK_RunApp\ -ErrorAction SilentlyContinue -Include *.* -Recurse | Remove-Item

Compress-Archive -Path .\patch-zip\* -CompressionLevel Fastest -Force -DestinationPath .\patch.zip
Write-Host "patch.zip is created in the repo root!"


$InstallPatch = Read-Host -Prompt "Patch R2T?(y=yes_all/cs=csepel_only/b=bopmh_only/f=fph_only=q=quit)"
if($InstallPatch -eq  "q"){
	Remove-Item tmp.txt
	Remove-Item -LiteralPath ".\patch-temp\" -Force -Recurse
	Remove-Item -LiteralPath ".\patch-zip\" -Force -Recurse
	exit
}

if($InstallPatch -eq  "y"){
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\BOPMH_V3_R2T\"  	
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\FPH_V3_R2T\"  	
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\CSEPEL_R2T\"  	
}
if($InstallPatch -eq  "cs"){	
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\CSEPEL_R2T\"  	
}
if($InstallPatch -eq  "b"){	
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\BOPMH_V3_R2T\"  	
}
if($InstallPatch -eq  "f"){	
	xcopy /y /s /i ".\patch-zip\*" "\\AX-VFPHTST03.axis.hu\C$\ContentumWebSites\FPH_V3_R2T\"  	
}
Remove-Item tmp.txt
Remove-Item -LiteralPath ".\patch-temp\" -Force -Recurse
Remove-Item -LiteralPath ".\patch-zip\" -Force -Recurse
