
@echo off
powershell -ExecutionPolicy ByPass -command "& .\copy_bin.ps1"

pause

rem @echo off
rem date format: d:MM-DD-YY
set base_dir=.\..
set curr_date=%DATE:~6,2%-%DATE:~10,2%-%DATE:~2,2%
echo %curr_date%
echo %base_dir%
mkdir .\Patch
echo. ---------- eAdmin Bin ----------
xcopy %base_dir%\eAdmin\Webservice\bin .\Patch\_dll\eAdmin\webservice\bin /S /I /d:%curr_date%
xcopy %base_dir%\eAdmin\Website\bin .\Patch\_dll\eAdmin\website\bin /S /I /d:%curr_date%
echo. ---------- eDocument Bin ----------
xcopy %base_dir%\eDocument\Webservice\bin .\Patch\_dll\eDocument\webservice\bin /S /I /d:%curr_date%
echo. ---------- eMigration Bin ----------
xcopy %base_dir%\eMigration\Webservice\bin .\Patch\_dll\eMigration\webservice\bin /S /I /d:%curr_date%
xcopy %base_dir%\eMigration\Website\bin .\Patch\_dll\eMigration\website\bin /S /I /d:%curr_date%
echo. ---------- eRecord Bin ----------
xcopy %base_dir%\eRecord\Webservice\bin .\Patch\_dll\eRecord\webservice\bin /S /I /d:%curr_date%
xcopy %base_dir%\eRecord\Website\bin .\Patch\_dll\eRecord\website\bin /S /I /d:%curr_date%
echo. ---------- eSharePoint Bin ----------
xcopy %base_dir%\eSharePoint\Webservice\bin .\Patch\_dll\eSharePoint\webservice\bin /S /I /d:%curr_date%
echo. ---------- eTemplateManager Bin ----------
xcopy %base_dir%\eTemplateManager\Webservice\bin .\Patch\_dll\eTemplateManager\webservice\bin /S /I /d:%curr_date%
echo. ---------- eIntegrator Bin ----------
xcopy %base_dir%\eIntegrator\Webservice\bin .\Patch\_dll\eIntegrator\webservice\bin /S /I /d:%curr_date%
del /s *.pdb
