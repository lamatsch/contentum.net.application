# @echo off
# date format: d:MM-DD-YY


$CurrentDate= Get-Date -Format "MM-dd-yy"

Write-Host "Current Date: " $CurrentDate
Write-Host "BaseDir:" .\..

New-Item -ItemType directory -Path .\Patch
Write-Host  ---------- eAdmin Bin ----------
xcopy .\..\eAdmin\Webservice\bin .\Patch\eAdmin\webservice\bin /S /I /d:$CurrentDate
xcopy .\..\eAdmin\Website\bin .\Patch\eAdmin\website\bin /S /I /d:$CurrentDate
Write-Host  ---------- eDocument Bin ----------
xcopy .\..\eDocument\Webservice\bin .\Patch\eDocument\webservice\bin /S /I /d:$CurrentDate
Write-Host  ---------- eMigration Bin ----------
xcopy .\..\eMigration\Webservice\bin .\Patch\eMigration\webservice\bin /S /I /d:$CurrentDate
xcopy .\..\eMigration\Website\bin .\Patch\eMigration\website\bin /S /I /d:$CurrentDate
Write-Host  ---------- eRecord Bin ----------
xcopy .\..\eRecord\Webservice\bin .\Patch\eRecord\webservice\bin /S /I /d:$CurrentDate
xcopy .\..\eRecord\Website\bin .\Patch\eRecord\website\bin /S /I /d:$CurrentDate
Write-Host  ---------- eSharePoint Bin ----------
xcopy .\..\eSharePoint\Webservice\bin .\Patch\eSharePoint\webservice\bin /S /I /d:$CurrentDate
Write-Host  ---------- eTemplateManager Bin ----------
xcopy .\..\eTemplateManager\Webservice\bin .\Patch\eTemplateManager\webservice\bin /S /I /d:$CurrentDate
Write-Host  ---------- eIntegrator Bin ----------
xcopy .\..\eIntegrator\Webservice\bin .\Patch\eIntegrator\webservice\bin /S /I /d:$CurrentDate

Get-ChildItem -Path '.\Patch' *.pdb -Recurse | foreach { Remove-Item -Path $_.FullName }
