//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "AuthenticationServiceSoap", Namespace = "Contentum.eAdmin.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseLogin))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class AuthenticationService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {        
        /// <remarks/>
        public AuthenticationService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }

        #region Login

        private System.Threading.SendOrPostCallback LoginOperationCompleted;

        /// <remarks/>
        public event LoginCompletedEventHandler LoginCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/Login", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Login(ExecParam ExecParam, Login Record)
        {
            object[] results = this.Invoke("Login", new object[] {
                        ExecParam,
                        Record});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginLogin(ExecParam ExecParam, Login Record, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Login", new object[] {
                        ExecParam,
                        Record}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndLogin(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void LoginAsync(ExecParam ExecParam, Login Record)
        {
            this.LoginAsync(ExecParam, Record, null);
        }

        /// <remarks/>
        public void LoginAsync(ExecParam ExecParam, Login Record, object userState)
        {
            if ((this.LoginOperationCompleted == null))
            {
                this.LoginOperationCompleted = new System.Threading.SendOrPostCallback(this.OnLoginOperationCompleted);
            }
            this.InvokeAsync("Login", new object[] {
                        ExecParam,
                        Record}, this.LoginOperationCompleted, userState);
        }

        private void OnLoginOperationCompleted(object arg)
        {
            if ((this.LoginCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.LoginCompleted(this, new LoginCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        #endregion

        #region ChangePassword

        private System.Threading.SendOrPostCallback ChangePasswordOperationCompleted;

        /// <remarks/>
        public event ChangePasswordCompletedEventHandler ChangePasswordCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/ChangePassword", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result ChangePassword(ExecParam ExecParam, KRT_Felhasznalok Record)
        {
            object[] results = this.Invoke("ChangePassword", new object[] {
                        ExecParam,
                        Record});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginChangePassword(ExecParam ExecParam, KRT_Felhasznalok Record, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("ChangePassword", new object[] {
                        ExecParam,
                        Record}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndChangePassword(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void ChangePasswordAsync(ExecParam ExecParam, KRT_Felhasznalok Record)
        {
            this.ChangePasswordAsync(ExecParam, Record, null);
        }

        /// <remarks/>
        public void ChangePasswordAsync(ExecParam ExecParam, KRT_Felhasznalok Record, object userState)
        {
            if ((this.ChangePasswordOperationCompleted == null))
            {
                this.ChangePasswordOperationCompleted = new System.Threading.SendOrPostCallback(this.OnChangePasswordOperationCompleted);
            }
            this.InvokeAsync("ChangePassword", new object[] {
                        ExecParam,
                        Record}, this.ChangePasswordOperationCompleted, userState);
        }

        private void OnChangePasswordOperationCompleted(object arg)
        {
            if ((this.ChangePasswordCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ChangePasswordCompleted(this, new ChangePasswordCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        #endregion

        #region GetApplicationUrls

        private System.Threading.SendOrPostCallback GetApplicationUrlsOperationCompleted;

        /// <remarks/>
        public event GetApplicationUrlsCompletedEventHandler GetApplicationUrlsCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetApplicationUrls", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetApplicationUrls(ExecParam execParam)
        {
            object[] results = this.Invoke("GetApplicationUrls", new object[] {
                        execParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetApplicationUrls(ExecParam execParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetApplicationUrls", new object[] {
                        execParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetApplicationUrls(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetApplicationUrlsAsync(ExecParam execParam)
        {
            this.GetApplicationUrlsAsync(execParam, null);
        }

        /// <remarks/>
        public void GetApplicationUrlsAsync(ExecParam execParam, object userState)
        {
            if ((this.GetApplicationUrlsOperationCompleted == null))
            {
                this.GetApplicationUrlsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetApplicationUrlsOperationCompleted);
            }
            this.InvokeAsync("GetApplicationUrls", new object[] {
                        execParam}, this.GetApplicationUrlsOperationCompleted, userState);
        }

        private void OnGetApplicationUrlsOperationCompleted(object arg)
        {
            if ((this.GetApplicationUrlsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetApplicationUrlsCompleted(this, new GetApplicationUrlsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetApplicationUrlsCompletedEventHandler(object sender, GetApplicationUrlsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetApplicationUrlsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetApplicationUrlsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void LoginCompletedEventHandler(object sender, LoginCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class LoginCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal LoginCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public Result Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((Result)(this.results[0]));
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void ChangePasswordCompletedEventHandler(object sender, ChangePasswordCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ChangePasswordCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal ChangePasswordCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public Result Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((Result)(this.results[0]));
            }
        }
    }
}

