
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eAdmin.Service{

    public partial class ServiceFactory
    {
        
        public KRT_EsemenyekService GetKRT_EsemenyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_EsemenyekService _Service = new KRT_EsemenyekService(_BusinessServiceUrl + "KRT_EsemenyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public LoginLogsService GetLoginLogsService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                LoginLogsService _Service = new LoginLogsService(_BusinessServiceUrl + "LoginLogsService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public UIAccessLogsService GetUIAccessLogsService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                UIAccessLogsService _Service = new UIAccessLogsService(_BusinessServiceUrl + "UIAccessLogsService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public WSAccessLogsService GetWSAccessLogsService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                WSAccessLogsService _Service = new WSAccessLogsService(_BusinessServiceUrl + "WSAccessLogsService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_Tranz_ObjService GetKRT_Tranz_ObjService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Tranz_ObjService _Service = new KRT_Tranz_ObjService(_BusinessServiceUrl + "KRT_Tranz_ObjService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        
        public KRT_Log_LoginService GetKRT_Log_LoginService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Log_LoginService _Service = new KRT_Log_LoginService(_BusinessServiceUrl + "KRT_Log_LoginService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }

        public KRT_Log_AuditLogService GetKRT_Log_AuditLogService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Log_AuditLogService _Service = new KRT_Log_AuditLogService(_BusinessServiceUrl + "KRT_Log_AuditLogService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_Log_PageService GetKRT_Log_PageService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Log_PageService _Service = new KRT_Log_PageService(_BusinessServiceUrl + "KRT_Log_PageService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_Log_WebServiceService GetKRT_Log_WebServiceService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Log_WebServiceService _Service = new KRT_Log_WebServiceService(_BusinessServiceUrl + "KRT_Log_WebServiceService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_Log_StoredProcedureService GetKRT_Log_StoredProcedureService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Log_StoredProcedureService _Service = new KRT_Log_StoredProcedureService(_BusinessServiceUrl + "KRT_Log_StoredProcedureService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }    }
}