
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eAdmin.Service{

    public partial class ServiceFactory
    {
        
        public KRT_Szerepkor_FunkcioService GetKRT_Szerepkor_FunkcioService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Szerepkor_FunkcioService _Service = new KRT_Szerepkor_FunkcioService(_BusinessServiceUrl + "KRT_Szerepkor_FunkcioService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_NezetekService GetKRT_NezetekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_NezetekService _Service = new KRT_NezetekService(_BusinessServiceUrl + "KRT_NezetekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_MuveletekService GetKRT_MuveletekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_MuveletekService _Service = new KRT_MuveletekService(_BusinessServiceUrl + "KRT_MuveletekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_MenukService GetKRT_MenukService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_MenukService _Service = new KRT_MenukService(_BusinessServiceUrl + "KRT_MenukService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_ModulokService GetKRT_ModulokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ModulokService _Service = new KRT_ModulokService(_BusinessServiceUrl + "KRT_ModulokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_Modul_FunkcioService GetKRT_Modul_FunkcioService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Modul_FunkcioService _Service = new KRT_Modul_FunkcioService(_BusinessServiceUrl + "KRT_Modul_FunkcioService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_UIMezoObjektumErtekekService GetKRT_UIMezoObjektumErtekekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_UIMezoObjektumErtekekService _Service = new KRT_UIMezoObjektumErtekekService(_BusinessServiceUrl + "KRT_UIMezoObjektumErtekekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_AlkalmazasokService GetKRT_AlkalmazasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_AlkalmazasokService _Service = new KRT_AlkalmazasokService(_BusinessServiceUrl + "KRT_AlkalmazasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_TranzakciokService GetKRT_TranzakciokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_TranzakciokService _Service = new KRT_TranzakciokService(_BusinessServiceUrl + "KRT_TranzakciokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_FunkcioListaService GetKRT_FunkcioListaService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_FunkcioListaService _Service = new KRT_FunkcioListaService(_BusinessServiceUrl + "KRT_FunkcioListaService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_Erintett_TablakService GetKRT_Erintett_TablakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Erintett_TablakService _Service = new KRT_Erintett_TablakService(_BusinessServiceUrl + "KRT_Erintett_TablakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_TemplateManagerService GetKRT_TemplateManagerService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_TemplateManagerService _Service = new KRT_TemplateManagerService(_BusinessServiceUrl + "KRT_TemplateManagerService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }

        public KRT_Extra_NapokService GetKRT_Extra_NapokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Extra_NapokService _Service = new KRT_Extra_NapokService(_BusinessServiceUrl + "KRT_Extra_NapokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
    
    }
}