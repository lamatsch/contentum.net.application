
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eAdmin.Service{

    public partial class ServiceFactory
    {
        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        public KRT_FunkciokService GetKRT_FunkciokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_FunkciokService _Service = new KRT_FunkciokService(_BusinessServiceUrl + "KRT_FunkciokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_KodCsoportokService GetKRT_KodCsoportokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_KodCsoportokService _Service = new KRT_KodCsoportokService(_BusinessServiceUrl + "KRT_KodCsoportokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_KodTarakService GetKRT_KodTarakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_KodTarakService _Service = new KRT_KodTarakService(_BusinessServiceUrl + "KRT_KodTarakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_ObjTipusokService GetKRT_ObjTipusokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ObjTipusokService _Service = new KRT_ObjTipusokService(_BusinessServiceUrl + "KRT_ObjTipusokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_OrgokService GetKRT_OrgokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_OrgokService _Service = new KRT_OrgokService(_BusinessServiceUrl + "KRT_OrgokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_PartnerekService GetKRT_PartnerekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_PartnerekService _Service = new KRT_PartnerekService(_BusinessServiceUrl + "KRT_PartnerekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_VallalkozasokService GetKRT_VallalkozasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_VallalkozasokService _Service = new KRT_VallalkozasokService(_BusinessServiceUrl + "KRT_VallalkozasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_PartnerKapcsolatokService GetKRT_PartnerKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_PartnerKapcsolatokService _Service = new KRT_PartnerKapcsolatokService(_BusinessServiceUrl + "KRT_PartnerKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_PartnerCimekService GetKRT_PartnerCimekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_PartnerCimekService _Service = new KRT_PartnerCimekService(_BusinessServiceUrl + "KRT_PartnerCimekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_PartnerMinositesekService GetKRT_PartnerMinositesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_PartnerMinositesekService _Service = new KRT_PartnerMinositesekService(_BusinessServiceUrl + "KRT_PartnerMinositesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_TelepulesekService GetKRT_TelepulesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_TelepulesekService _Service = new KRT_TelepulesekService(_BusinessServiceUrl + "KRT_TelepulesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_ParameterekService GetKRT_ParameterekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ParameterekService _Service = new KRT_ParameterekService(_BusinessServiceUrl + "KRT_ParameterekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_SzemelyekService GetKRT_SzemelyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_SzemelyekService _Service = new KRT_SzemelyekService(_BusinessServiceUrl + "KRT_SzemelyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_FelhasznaloProfilokService GetKRT_FelhasznaloProfilokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_FelhasznaloProfilokService _Service = new KRT_FelhasznaloProfilokService(_BusinessServiceUrl + "KRT_FelhasznaloProfilokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_SzerepkorokService GetKRT_SzerepkorokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_SzerepkorokService _Service = new KRT_SzerepkorokService(_BusinessServiceUrl + "KRT_SzerepkorokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_Felhasznalo_SzerepkorService GetKRT_Felhasznalo_SzerepkorService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Felhasznalo_SzerepkorService _Service = new KRT_Felhasznalo_SzerepkorService(_BusinessServiceUrl + "KRT_Felhasznalo_SzerepkorService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_HelyettesitesekService GetKRT_HelyettesitesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_HelyettesitesekService _Service = new KRT_HelyettesitesekService(_BusinessServiceUrl + "KRT_HelyettesitesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_FelhasznalokService GetKRT_FelhasznalokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_FelhasznalokService _Service = new KRT_FelhasznalokService(_BusinessServiceUrl + "KRT_FelhasznalokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_CsoportTagokService GetKRT_CsoportTagokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_CsoportTagokService _Service = new KRT_CsoportTagokService(_BusinessServiceUrl + "KRT_CsoportTagokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_JogosultakService GetKRT_JogosultakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_JogosultakService _Service = new KRT_JogosultakService(_BusinessServiceUrl + "KRT_JogosultakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_CsoportokService GetKRT_CsoportokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_CsoportokService _Service = new KRT_CsoportokService(_BusinessServiceUrl + "KRT_CsoportokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_ACLService GetKRT_ACLService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ACLService _Service = new KRT_ACLService(_BusinessServiceUrl + "KRT_ACLService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_CimekService GetKRT_CimekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_CimekService _Service = new KRT_CimekService(_BusinessServiceUrl + "KRT_CimekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_OrszagokService GetKRT_OrszagokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_OrszagokService _Service = new KRT_OrszagokService(_BusinessServiceUrl + "KRT_OrszagokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_KozteruletekService GetKRT_KozteruletekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_KozteruletekService _Service = new KRT_KozteruletekService(_BusinessServiceUrl + "KRT_KozteruletekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_KozteruletTipusokService GetKRT_KozteruletTipusokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_KozteruletTipusokService _Service = new KRT_KozteruletTipusokService(_BusinessServiceUrl + "KRT_KozteruletTipusokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_HataridosFeladatokService GetKRT_HataridosFeladatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_HataridosFeladatokService _Service = new KRT_HataridosFeladatokService(_BusinessServiceUrl + "KRT_HataridosFeladatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_SoapMessageLogService GetKRT_SoapMessageLogService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_SoapMessageLogService _Service = new KRT_SoapMessageLogService(_BusinessServiceUrl + "KRT_SoapMessageLogService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_BankszamlaszamokService GetKRT_BankszamlaszamokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_BankszamlaszamokService _Service = new KRT_BankszamlaszamokService(_BusinessServiceUrl + "KRT_BankszamlaszamokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public KRT_Felhasznalok_Halozati_NyomtatoiService GetKRT_Felhasznalok_Halozati_NyomtatoiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Felhasznalok_Halozati_NyomtatoiService _Service = new KRT_Felhasznalok_Halozati_NyomtatoiService(_BusinessServiceUrl + "KRT_Felhasznalok_Halozati_NyomtatoiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_HalozatiNyomtatokService GetKRT_HalozatiNyomtatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_HalozatiNyomtatokService _Service = new KRT_HalozatiNyomtatokService(_BusinessServiceUrl + "KRT_HalozatiNyomtatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        
        public KRT_KodtarFuggosegService GetKRT_KodtarFuggosegService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_KodtarFuggosegService _Service = new KRT_KodtarFuggosegService(_BusinessServiceUrl + "KRT_KodtarFuggosegService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_ForditasokService GetKRT_ForditasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ForditasokService _Service = new KRT_ForditasokService(_BusinessServiceUrl + "KRT_ForditasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_AutoEUzenetSzabalyService GetINT_AutoEUzenetSzabalyService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_AutoEUzenetSzabalyService _Service = new INT_AutoEUzenetSzabalyService(_BusinessServiceUrl + "INT_AutoEUzenetSzabalyService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public INT_AutoEUzenetSzabalyNaploService GetINT_AutoEUzenetSzabalyNaploService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_AutoEUzenetSzabalyNaploService _Service = new INT_AutoEUzenetSzabalyNaploService(_BusinessServiceUrl + "INT_AutoEUzenetSzabalyNaploService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
    }
}