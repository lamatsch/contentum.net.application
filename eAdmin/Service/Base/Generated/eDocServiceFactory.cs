
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eAdmin.Service{

    public partial class ServiceFactory
    {
        
        public KRT_DokumentumokService GetKRT_DokumentumokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumokService _Service = new KRT_DokumentumokService(_BusinessServiceUrl + "KRT_DokumentumokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_DokumentumAlairasokService GetKRT_DokumentumAlairasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumAlairasokService _Service = new KRT_DokumentumAlairasokService(_BusinessServiceUrl + "KRT_DokumentumAlairasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_DokumentumCsatolasokService GetKRT_DokumentumCsatolasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumCsatolasokService _Service = new KRT_DokumentumCsatolasokService(_BusinessServiceUrl + "KRT_DokumentumCsatolasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public KRT_DokumentumKapcsolatokService GetKRT_DokumentumKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumKapcsolatokService _Service = new KRT_DokumentumKapcsolatokService(_BusinessServiceUrl + "KRT_DokumentumKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }
        
        public EDOC_IktatandokService GetEDOC_IktatandokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EDOC_IktatandokService _Service = new EDOC_IktatandokService(_BusinessServiceUrl + "EDOC_IktatandokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }    }
}