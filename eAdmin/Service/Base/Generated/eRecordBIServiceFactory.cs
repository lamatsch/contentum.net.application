
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eAdmin.Service
{

    public partial class ServiceFactory
    {
        public Fact_FeladatokService GetFact_FeladatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                Fact_FeladatokService _Service = new Fact_FeladatokService(_BusinessServiceUrl + "Fact_FeladatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public Dim_LejaratKategoriaService GetDim_LejaratKategoriaService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                Dim_LejaratKategoriaService _Service = new Dim_LejaratKategoriaService(_BusinessServiceUrl + "Dim_LejaratKategoriaService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public Dim_PrioritasService GetDim_PrioritasService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                Dim_PrioritasService _Service = new Dim_PrioritasService(_BusinessServiceUrl + "Dim_PrioritasService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public Dim_FeladatFelelosService GetDim_FeladatFelelosService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                Dim_FeladatFelelosService _Service = new Dim_FeladatFelelosService(_BusinessServiceUrl + "Dim_FeladatFelelosService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

    }
}