
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eAdmin.Service
{
    public partial class ServiceFactory
    {
        string _BusinessServiceType = "SOAP";
        string _BusinessServiceUrl = "";
        string _BusinessServiceAuthentication = "Windows";
        string _BusinessServiceUserDomain = "";
        string _BusinessServiceUserName = "";
        string _BusinessServicePassword = "";

        public string BusinessServiceType
        {
            get { return _BusinessServiceType; }
            set { _BusinessServiceType = value; }
        }

        public string BusinessServiceUrl
        {
            get { return _BusinessServiceUrl; }
            set { _BusinessServiceUrl = value; }
        }

        public string BusinessServiceAuthentication
        {
            get { return _BusinessServiceAuthentication; }
            set { _BusinessServiceAuthentication = value; }
        }

        public string BusinessServiceUserDomain
        {
            get { return _BusinessServiceUserDomain; }
            set { _BusinessServiceUserDomain = value; }
        }

        public string BusinessServiceUserName
        {
            get { return _BusinessServiceUserName; }
            set { _BusinessServiceUserName = value; }
        }

        public string BusinessServicePassword
        {
            get { return _BusinessServicePassword; }
            set { _BusinessServicePassword = value; }
        }

        public AuthenticationService GetAuthenticationService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                AuthenticationService _Service = new AuthenticationService(_BusinessServiceUrl + "AuthenticationService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public LockManagerService GetLockManagerService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                LockManagerService _Service = new LockManagerService(_BusinessServiceUrl + "LockManagerService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public RecordHistoryService GetRecordHistoryService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                RecordHistoryService _Service = new RecordHistoryService(_BusinessServiceUrl + "RecordHistoryService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EmailService GetEmailService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EmailService _Service = new EmailService(_BusinessServiceUrl + "EmailService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public LogService GetLogService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                LogService _Service = new LogService(_BusinessServiceUrl + "LogService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public RightsService getRightsService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                RightsService _Service = new RightsService(_BusinessServiceUrl + "RightsService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;

        }
        public EmailService GetBaseEmailService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EmailService _Service = new EmailService(_BusinessServiceUrl + "EmailService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraElosztoivekService GetEREC_IraElosztoivekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraElosztoivekService _Service = new EREC_IraElosztoivekService(_BusinessServiceUrl + "EREC_IraElosztoivekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraElosztoivTetelekService GetEREC_IraElosztoivTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraElosztoivTetelekService _Service = new EREC_IraElosztoivTetelekService(_BusinessServiceUrl + "EREC_IraElosztoivTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MessageService GetMessageService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MessageService _Service = new MessageService(_BusinessServiceUrl + "MessageService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public StoredProcedureService GetStoredProcedureService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                StoredProcedureService _Service = new StoredProcedureService(_BusinessServiceUrl + "StoredProcedureService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        #region BLG 2963
        public MaintenancePlanService GetMaintenancePlanService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MaintenancePlanService _Service = new MaintenancePlanService(_BusinessServiceUrl + "MaintenancePlanService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        #endregion

    }

}