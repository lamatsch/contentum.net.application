﻿

namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "KRT_Felhasznalok_Halozati_NyomtatoiServiceSoap", Namespace = "Contentum.eAdmin.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_Felhasznalok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_Felhasznalok_Halozati_Nyomtatoi))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_Felhasznalok_Halozati_NyomtatoiService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        /// <remarks/>
        public KRT_Felhasznalok_Halozati_NyomtatoiService(string ServiceUrl)
        {
            this.Url = ServiceUrl;//ServiceUrl;
        }

        #region GetByNyomtato --id --> 1vN

        private System.Threading.SendOrPostCallback GetByNyomtatoOperationCompleted;

        /// <remarks/>
        public event GetGetByNyomtatoCompletedEventHandler GetByNyomtatoCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetByNyomtato", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetByNyomtato(ExecParam execParam, string nyomtatoId)
        {
            object[] results = this.Invoke("GetByNyomtato", new object[] {
                        execParam,
                        nyomtatoId});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetByNyomtato(ExecParam execParam, string nyomtatoId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetByNyomtato", new object[] {
                        execParam,
                        nyomtatoId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetByNyomtato(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetGetByNyomtatoAsync(ExecParam execParam, string nyomtatoId)
        {
            this.GetByNyomtatoAsync(execParam, nyomtatoId, null);
        }

        /// <remarks/>
        public void GetByNyomtatoAsync(ExecParam execParam, string nyomtatoId, object userState)
        {
            if ((this.GetByNyomtatoOperationCompleted == null))
            {
                this.GetByNyomtatoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetByNyomtatoOperationCompleted);
            }
            this.InvokeAsync("GetByNyomtato", new object[] {
                        execParam,
                        nyomtatoId}, this.GetByNyomtatoOperationCompleted, userState);
        }

        private void OnGetByNyomtatoOperationCompleted(object arg)
        {
            if ((this.GetByNyomtatoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetByNyomtatoCompleted(this, new GetByNyomtatoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetGetByNyomtatoCompletedEventHandler(object sender, GetByNyomtatoCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetByNyomtatoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetByNyomtatoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region get All
        private System.Threading.SendOrPostCallback GetAllOperationCompleted;

        /// <remarks/>
        public event GetAllCompletedEventHandler GetAllCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAll", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAll(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch)
        {
            object[] results = this.Invoke("GetAll", new object[] {
                        ExecParam,
                        _KRT_Felhasznalok_Halozati_NyomtatoiSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAll(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAll", new object[] {
                        ExecParam,
                        _KRT_Felhasznalok_Halozati_NyomtatoiSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAll(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch)
        {
            this.GetAllAsync(ExecParam, _KRT_Felhasznalok_Halozati_NyomtatoiSearch, null);
        }

        /// <remarks/>
        public void GetAllAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch, object userState)
        {
            if ((this.GetAllOperationCompleted == null))
            {
                this.GetAllOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllOperationCompleted);
            }
            this.InvokeAsync("GetAll", new object[] {
                        ExecParam,
                        _KRT_Felhasznalok_Halozati_NyomtatoiSearch}, this.GetAllOperationCompleted, userState);
        }

        private void OnGetAllOperationCompleted(object arg)
        {
            if ((this.GetAllCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllCompleted(this, new GetAllCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllCompletedEventHandler(object sender, GetAllCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region Get
        private System.Threading.SendOrPostCallback GetOperationCompleted;

        /// <remarks/>
        public event GetCompletedEventHandler GetCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/Get", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Get(ExecParam ExecParam)
        {
            object[] results = this.Invoke("Get", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGet(Contentum.eBusinessDocuments.ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Get", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGet(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAsync(ExecParam ExecParam)
        {
            this.GetAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetOperationCompleted == null))
            {
                this.GetOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetOperationCompleted);
            }
            this.InvokeAsync("Get", new object[] {
                        ExecParam}, this.GetOperationCompleted, userState);
        }

        private void OnGetOperationCompleted(object arg)
        {
            if ((this.GetCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetCompleted(this, new GetCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetCompletedEventHandler(object sender, GetCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region insert
        private System.Threading.SendOrPostCallback InsertOperationCompleted;

        /// <remarks/>
        public event InsertCompletedEventHandler InsertCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/Insert", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Insert(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
        {
            object[] results = this.Invoke("Insert", new object[] {
                        ExecParam,
                        Record});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsert(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Insert", new object[] {
                        ExecParam,
                        Record}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndInsert(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void InsertAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
        {
            this.InsertAsync(ExecParam, Record, null);
        }

        /// <remarks/>
        public void InsertAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record, object userState)
        {
            if ((this.InsertOperationCompleted == null))
            {
                this.InsertOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertOperationCompleted);
            }
            this.InvokeAsync("Insert", new object[] {
                        ExecParam,
                        Record}, this.InsertOperationCompleted, userState);
        }

        private void OnInsertOperationCompleted(object arg)
        {
            if ((this.InsertCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertCompleted(this, new InsertCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void InsertCompletedEventHandler(object sender, InsertCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class InsertCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal InsertCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region update
        private System.Threading.SendOrPostCallback UpdateOperationCompleted;

        /// <remarks/>
        public event UpdateCompletedEventHandler UpdateCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/Update", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Update(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
        {
            object[] results = this.Invoke("Update", new object[] {
                        ExecParam,
                        Record});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdate(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Update", new object[] {
                        ExecParam,
                        Record}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndUpdate(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void UpdateAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
        {
            this.UpdateAsync(ExecParam, Record, null);
        }

        /// <remarks/>
        public void UpdateAsync(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record, object userState)
        {
            if ((this.UpdateOperationCompleted == null))
            {
                this.UpdateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateOperationCompleted);
            }
            this.InvokeAsync("Update", new object[] {
                        ExecParam,
                        Record}, this.UpdateOperationCompleted, userState);
        }

        private void OnUpdateOperationCompleted(object arg)
        {
            if ((this.UpdateCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateCompleted(this, new UpdateCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void UpdateCompletedEventHandler(object sender, UpdateCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class UpdateCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal UpdateCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region invalidate
        private System.Threading.SendOrPostCallback InvalidateOperationCompleted;

        /// <remarks/>
        public event InvalidateCompletedEventHandler InvalidateCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/Invalidate", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Invalidate(ExecParam ExecParam)
        {
            object[] results = this.Invoke("Invalidate", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInvalidate(ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Invalidate", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndInvalidate(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void InvalidateAsync(ExecParam ExecParam)
        {
            this.InvalidateAsync(ExecParam, null);
        }

        /// <remarks/>
        public void InvalidateAsync(ExecParam ExecParam, object userState)
        {
            if ((this.InvalidateOperationCompleted == null))
            {
                this.InvalidateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInvalidateOperationCompleted);
            }
            this.InvokeAsync("Invalidate", new object[] {
                        ExecParam}, this.InvalidateOperationCompleted, userState);
        }

        private void OnInvalidateOperationCompleted(object arg)
        {
            if ((this.InvalidateCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InvalidateCompleted(this, new InvalidateCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void InvalidateCompletedEventHandler(object sender, InvalidateCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class InvalidateCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal InvalidateCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion 

        //private System.Threading.SendOrPostCallback MultiInvalidateOperationCompleted;

        //private System.Threading.SendOrPostCallback DeleteOperationCompleted;
        
        ///// <remarks/>
        //public event MultiInvalidateCompletedEventHandler MultiInvalidateCompleted;

        ///// <remarks/>
        //public event DeleteCompletedEventHandler DeleteCompleted;

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        
        
        

    
    


    

    }
}
