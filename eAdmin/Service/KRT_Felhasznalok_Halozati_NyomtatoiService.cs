﻿

namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_Felhasznalok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_Felhasznalok_Halozati_NyomtatoiService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
    }
}
