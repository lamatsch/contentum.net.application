namespace Contentum.eAdmin.Service
{
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_KodtarFuggoseg))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_KodtarFuggosegService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        #region GET ALL WITH EXTENSION
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAllWithExtension", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtension(ExecParam ExecParam, KRT_KodtarFuggosegSearch _KRT_KodtarFuggosegSearch)
        {
            object[] results = this.Invoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_KodtarFuggosegSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtension(ExecParam ExecParam, KRT_KodtarFuggosegSearch _KRT_KodtarFuggosegSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_KodtarFuggosegSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtension(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, KRT_KodtarFuggosegSearch _KRT_KodtarFuggosegSearch)
        {
            this.GetAllWithExtensionAsync(ExecParam, _KRT_KodtarFuggosegSearch, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, KRT_KodtarFuggosegSearch _KRT_KodtarFuggosegSearch, object userState)
        {
            if ((this.GetAllWithExtensionOperationCompleted == null))
            {
                this.GetAllWithExtensionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_KodtarFuggosegSearch}, this.GetAllWithExtensionOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionCompleted(this, new GetAllWithExtensionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionCompletedEventHandler(object sender, GetAllWithExtensionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        private System.Threading.SendOrPostCallback GetAllWithExtensionOperationCompleted;
        /// <remarks/>
        public event GetAllWithExtensionCompletedEventHandler GetAllWithExtensionCompleted;
        #endregion
    }

}