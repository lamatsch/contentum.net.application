namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;


    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_ObjTipusok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_ObjTipusokService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        #region GetAllWithExtension
        private System.Threading.SendOrPostCallback GetAllWithExtensionOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionCompletedEventHandler GetAllWithExtensionCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAllWithExtension", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtension(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, string KodFilter)
        {
            object[] results = this.Invoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_ObjTipusokSearch,
                        KodFilter});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtension(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, string KodFilter, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_ObjTipusokSearch,
                        KodFilter}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtension(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, string KodFilter)
        {
            this.GetAllWithExtensionAsync(ExecParam, _KRT_ObjTipusokSearch, KodFilter, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, string KodFilter, object userState)
        {
            if ((this.GetAllWithExtensionOperationCompleted == null))
            {
                this.GetAllWithExtensionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtension", new object[] {
                        ExecParam,
                        _KRT_ObjTipusokSearch,
                        KodFilter}, this.GetAllWithExtensionOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionCompleted(this, new GetAllWithExtensionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionCompletedEventHandler(object sender, GetAllWithExtensionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion GetAllWithExtension

        #region GetAllByTableColumn
        private System.Threading.SendOrPostCallback GetAllByTableColumnOperationCompleted;

        /// <remarks/>
        public event GetAllByTableColumnCompletedEventHandler GetAllByTableColumnCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAllByTableColumn", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllByTableColumn(ExecParam ExecParam, string Table, string Column)
        {
            object[] results = this.Invoke("GetAllByTableColumn", new object[] {
                        ExecParam,
                        Table,
                        Column});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllByTableColumn(ExecParam ExecParam, string Table, string Column, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllByTableColumn", new object[] {
                        ExecParam,
                        Table,
                        Column}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllByTableColumn(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllByTableColumnAsync(ExecParam ExecParam, string Table, string Column)
        {
            this.GetAllByTableColumnAsync(ExecParam, Table, Column, null);
        }

        /// <remarks/>
        public void GetAllByTableColumnAsync(ExecParam ExecParam, string Table, string Column, object userState)
        {
            if ((this.GetAllByTableColumnOperationCompleted == null))
            {
                this.GetAllByTableColumnOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllByTableColumnOperationCompleted);
            }
            this.InvokeAsync("GetAllByTableColumn", new object[] {
                        ExecParam,
                        Table,
                        Column}, this.GetAllByTableColumnOperationCompleted, userState);
        }

        private void OnGetAllByTableColumnOperationCompleted(object arg)
        {
            if ((this.GetAllByTableColumnCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllByTableColumnCompleted(this, new GetAllByTableColumnCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllByTableColumnCompletedEventHandler(object sender, GetAllByTableColumnCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllByTableColumnCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllByTableColumnCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion GetAllByTableColumn

    }

}