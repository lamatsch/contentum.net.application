namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_TemplateManager))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_TemplateManagerService
    {
        private System.Threading.SendOrPostCallback GetAllWithFKOperationCompleted;

        /// <remarks/>
        public event GetAllWithFKCompletedEventHandler GetAllWithFKCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAllWithFK", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithFK(ExecParam ExecParam, KRT_TemplateManagerSearch _KRT_TemplateManagerSearch)
        {
            object[] results = this.Invoke("GetAllWithFK", new object[] {
                        ExecParam,
                        _KRT_TemplateManagerSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithFK(ExecParam ExecParam, KRT_TemplateManagerSearch _KRT_TemplateManagerSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithFK", new object[] {
                        ExecParam,
                        _KRT_TemplateManagerSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithFK(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithFKAsync(ExecParam ExecParam, KRT_TemplateManagerSearch _KRT_TemplateManagerSearch)
        {
            this.GetAllWithFKAsync(ExecParam, _KRT_TemplateManagerSearch, null);
        }

        /// <remarks/>
        public void GetAllWithFKAsync(ExecParam ExecParam, KRT_TemplateManagerSearch _KRT_TemplateManagerSearch, object userState)
        {
            if ((this.GetAllWithFKOperationCompleted == null))
            {
                this.GetAllWithFKOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithFKOperationCompleted);
            }
            this.InvokeAsync("GetAllWithFK", new object[] {
                        ExecParam,
                        _KRT_TemplateManagerSearch}, this.GetAllWithFKOperationCompleted, userState);
        }

        private void OnGetAllWithFKOperationCompleted(object arg)
        {
            if ((this.GetAllWithFKCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithFKCompleted(this, new GetAllWithFKCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithFKCompletedEventHandler(object sender, GetAllWithFKCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithFKCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithFKCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
    }
}
