﻿namespace Contentum.eAdmin.Service
{
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;
    using System;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "LogServiceSoap", Namespace = "Contentum.eAdmin.WebService")]
    public partial class MaintenancePlanService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback GetAllOperationCompleted;

        private System.Threading.SendOrPostCallback GetDbListOperationCompleted;

        private System.Threading.SendOrPostCallback BackupDbOperationCompleted;

        private System.Threading.SendOrPostCallback RestoreDbOperationCompleted;

        /// <remarks/>
        public MaintenancePlanService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }

        #region GetAll

        
        /// <remarks/>
        public event GetAllCompletedEventHandler GetAllCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetAll", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAll(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetAll", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAll(Contentum.eBusinessDocuments.ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAll", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAll(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllAsync(ExecParam ExecParam)
        {
            this.GetAllAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetAllAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetAllOperationCompleted == null))
            {
                this.GetAllOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllOperationCompleted);
            }
            this.InvokeAsync("GetAll", new object[] {
                        ExecParam}, this.GetAllOperationCompleted, userState);
        }


        private void OnGetAllOperationCompleted(object arg)
        {
            if ((this.GetAllCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllCompleted(this, new GetAllCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllCompletedEventHandler(object sender, GetAllCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion



        #region GetDbList


        /// <remarks/>
        public event GetDbListCompletedEventHandler GetDbListCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/GetDbList", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetDbList(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetDbList", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetDbList(Contentum.eBusinessDocuments.ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetDbList", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetDbList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetDbListAsync(ExecParam ExecParam)
        {
            this.GetDbListAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetDbListAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetDbListOperationCompleted == null))
            {
                this.GetDbListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetDbListOperationCompleted);
            }
            this.InvokeAsync("GetDbList", new object[] {
                        ExecParam}, this.GetDbListOperationCompleted, userState);
        }


        private void OnGetDbListOperationCompleted(object arg)
        {
            if ((this.GetDbListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetDbListCompleted(this, new GetDbListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetDbListCompletedEventHandler(object sender, GetDbListCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetDbListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetDbListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region BackupDb

        /// <remarks/>
        public event BackupDbCompletedEventHandler BackupDbCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/BackupDb", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result BackupDb(ExecParam ExecParam, String databaseName, String fileName)
        {
            object[] results = this.Invoke("BackupDb", new object[] {
                        ExecParam, databaseName, fileName});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginBackupDb(Contentum.eBusinessDocuments.ExecParam ExecParam, String databaseName, String fileName, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("BackupDb", new object[] {
                        ExecParam, databaseName, fileName}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndBackupDb(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void BackupDbAsync(ExecParam ExecParam, String databaseName, String fileName)
        {
            this.BackupDbAsync(ExecParam, databaseName, fileName, null);
        }

        /// <remarks/>
        public void BackupDbAsync(ExecParam ExecParam, String databaseName, String fileName, object userState)
        {
            if ((this.BackupDbOperationCompleted == null))
            {
                this.BackupDbOperationCompleted = new System.Threading.SendOrPostCallback(this.OnBackupDbOperationCompleted);
            }
            this.InvokeAsync("BackupDb", new object[] {
                        ExecParam, databaseName, fileName}, this.BackupDbOperationCompleted, userState);
        }


        private void OnBackupDbOperationCompleted(object arg)
        {
            if ((this.BackupDbCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.BackupDbCompleted(this, new BackupDbCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void BackupDbCompletedEventHandler(object sender, BackupDbCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class BackupDbCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal BackupDbCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region RestoreDb

        /// <remarks/>
        public event RestoreDbCompletedEventHandler RestoreDbCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/RestoreDb", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result RestoreDb(ExecParam ExecParam, String databaseName, String fileName)
        {
            object[] results = this.Invoke("RestoreDb", new object[] {
                        ExecParam, databaseName, fileName});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginRestoreDb(Contentum.eBusinessDocuments.ExecParam ExecParam, String databaseName, String fileName, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("RestoreDb", new object[] {
                        ExecParam, databaseName, fileName}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndRestoreDb(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void RestoreDbAsync(ExecParam ExecParam, String databaseName, String fileName)
        {
            this.RestoreDbAsync(ExecParam, databaseName, fileName, null);
        }

        /// <remarks/>
        public void RestoreDbAsync(ExecParam ExecParam, String databaseName, String fileName, object userState)
        {
            if ((this.RestoreDbOperationCompleted == null))
            {
                this.RestoreDbOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRestoreDbOperationCompleted);
            }
            this.InvokeAsync("RestoreDb", new object[] {
                        ExecParam, databaseName, fileName}, this.RestoreDbOperationCompleted, userState);
        }


        private void OnRestoreDbOperationCompleted(object arg)
        {
            if ((this.RestoreDbCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RestoreDbCompleted(this, new RestoreDbCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void RestoreDbCompletedEventHandler(object sender, RestoreDbCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class RestoreDbCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal RestoreDbCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

    }
}
