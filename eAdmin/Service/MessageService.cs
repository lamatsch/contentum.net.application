﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eAdmin.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Text;
    using System.Xml.Serialization;
    using Contentum.eBusinessDocuments;
    using System.Data;

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "MessageServiceSoap", Namespace = "Contentum.eAdmin.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Message))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class MessageService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        /// <remarks/>
        public MessageService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        #region SendNotification

        private System.Threading.SendOrPostCallback SendNotificationOperationCompleted;

        /// <remarks/>
        public event SendNotificationCompletedEventHandler SendNotificationCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/SendNotification", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool SendNotification(ExecParam execParam, Message message)
        {
            object[] results = this.Invoke("SendNotification", new object[] {
                        execParam,
                        message});
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSendNotification(ExecParam execParam, Message message, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SendNotification", new object[] {
                        execParam,
                        message}, callback, asyncState);
        }

        /// <remarks/>
        public bool EndSendNotification(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public void SendNotificationAsync(ExecParam execParam, Message message)
        {
            this.SendNotificationAsync(execParam, message, null);
        }

        /// <remarks/>
        public void SendNotificationAsync(ExecParam execParam, Message message, object userState)
        {
            if ((this.SendNotificationOperationCompleted == null))
            {
                this.SendNotificationOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSendNotificationOperationCompleted);
            }
            this.InvokeAsync("SendNotification", new object[] {
                        execParam,
                        message}, this.SendNotificationOperationCompleted, userState);
        }

        private void OnSendNotificationOperationCompleted(object arg)
        {
            if ((this.SendNotificationCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SendNotificationCompleted(this, new SendNotificationCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SendNotificationCompletedEventHandler(object sender, SendNotificationCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SendNotificationCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SendNotificationCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public bool Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((bool)(this.results[0]));
                }
            }
        }

        #endregion

        #region SendFeladatNotification

        private System.Threading.SendOrPostCallback SendFeladatNotificationMessageOperationCompleted;

        /// <remarks/>
        public event SendFeladatNotificationMessageCompletedEventHandler SendFeladatNotificationMessageCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/SendFeladatNotificationMessage", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool SendFeladatNotificationMessage(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            object[] results = this.Invoke("SendFeladatNotificationMessage", new object[] {
                        execParam,
                        erec_HataridosFeladatok});
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSendFeladatNotificationMessage(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SendFeladatNotificationMessage", new object[] {
                        execParam,
                        erec_HataridosFeladatok}, callback, asyncState);
        }

        /// <remarks/>
        public bool EndSendFeladatNotificationMessage(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public void SendFeladatNotificationMessageAsync(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            this.SendFeladatNotificationMessageAsync(execParam, erec_HataridosFeladatok, null);
        }

        /// <remarks/>
        public void SendFeladatNotificationMessageAsync(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, object userState)
        {
            if ((this.SendFeladatNotificationMessageOperationCompleted == null))
            {
                this.SendFeladatNotificationMessageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSendFeladatNotificationMessageOperationCompleted);
            }
            this.InvokeAsync("SendFeladatNotificationMessage", new object[] {
                        execParam,
                        erec_HataridosFeladatok}, this.SendFeladatNotificationMessageOperationCompleted, userState);
        }

        private void OnSendFeladatNotificationMessageOperationCompleted(object arg)
        {
            if ((this.SendFeladatNotificationMessageCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SendFeladatNotificationMessageCompleted(this, new SendFeladatNotificationMessageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SendFeladatNotificationMessageCompletedEventHandler(object sender, SendFeladatNotificationMessageCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SendFeladatNotificationMessageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SendFeladatNotificationMessageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public bool Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((bool)(this.results[0]));
                }
            }
        }

        #endregion

        #region SendHataridosErtesitesMessage

        private System.Threading.SendOrPostCallback SendHataridosErtesitesMessageOperationCompleted;

        /// <remarks/>
        public event SendHataridosErtesitesMessageCompletedEventHandler SendHataridosErtesitesMessageCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/SendHataridosErtesitesMessage", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool SendHataridosErtesitesMessage(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek)
        {
            object[] results = this.Invoke("SendHataridosErtesitesMessage", new object[] {
                        execParam,
                        erec_HataridosErtesitesek});
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSendHataridosErtesitesMessage(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SendHataridosErtesitesMessage", new object[] {
                        execParam,
                        erec_HataridosErtesitesek}, callback, asyncState);
        }

        /// <remarks/>
        public bool EndSendHataridosErtesitesMessage(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public void SendHataridosErtesitesMessageAsync(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek)
        {
            this.SendHataridosErtesitesMessageAsync(execParam, erec_HataridosErtesitesek, null);
        }

        /// <remarks/>
        public void SendHataridosErtesitesMessageAsync(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek, object userState)
        {
            if ((this.SendHataridosErtesitesMessageOperationCompleted == null))
            {
                this.SendHataridosErtesitesMessageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSendHataridosErtesitesMessageOperationCompleted);
            }
            this.InvokeAsync("SendHataridosErtesitesMessage", new object[] {
                        execParam,
                        erec_HataridosErtesitesek}, this.SendHataridosErtesitesMessageOperationCompleted, userState);
        }

        private void OnSendHataridosErtesitesMessageOperationCompleted(object arg)
        {
            if ((this.SendHataridosErtesitesMessageCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SendHataridosErtesitesMessageCompleted(this, new SendHataridosErtesitesMessageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SendHataridosErtesitesMessageCompletedEventHandler(object sender, SendHataridosErtesitesMessageCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SendHataridosErtesitesMessageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SendHataridosErtesitesMessageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public bool Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((bool)(this.results[0]));
                }
            }
        }

        #endregion

        #region SendHataridosErtesitesMessageTomeges

        private System.Threading.SendOrPostCallback SendHataridosErtesitesMessageTomegesOperationCompleted;

        /// <remarks/>
        public event SendHataridosErtesitesMessageTomegesCompletedEventHandler SendHataridosErtesitesMessageTomegesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eAdmin.WebService/SendHataridosErtesitesMessageTomeges", RequestNamespace = "Contentum.eAdmin.WebService", ResponseNamespace = "Contentum.eAdmin.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool SendHataridosErtesitesMessageTomeges(ExecParam execParam, EREC_HataridosErtesitesek[] erec_HataridosErtesitesekList)
        {
            object[] results = this.Invoke("SendHataridosErtesitesMessageTomeges", new object[] {
                        execParam,
                        erec_HataridosErtesitesekList});
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSendHataridosErtesitesMessageTomeges(ExecParam execParam, EREC_HataridosErtesitesek[] erec_HataridosErtesitesekList, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SendHataridosErtesitesMessageTomeges", new object[] {
                        execParam,
                        erec_HataridosErtesitesekList}, callback, asyncState);
        }

        /// <remarks/>
        public bool EndSendHataridosErtesitesMessageTomeges(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public void SendHataridosErtesitesMessageTomegesAsync(ExecParam execParam, EREC_HataridosErtesitesek[] erec_HataridosErtesitesekList)
        {
            this.SendHataridosErtesitesMessageTomegesAsync(execParam, erec_HataridosErtesitesekList, null);
        }

        /// <remarks/>
        public void SendHataridosErtesitesMessageTomegesAsync(ExecParam execParam, EREC_HataridosErtesitesek[] erec_HataridosErtesitesekList, object userState)
        {
            if ((this.SendHataridosErtesitesMessageTomegesOperationCompleted == null))
            {
                this.SendHataridosErtesitesMessageTomegesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSendHataridosErtesitesMessageTomegesOperationCompleted);
            }
            this.InvokeAsync("SendHataridosErtesitesMessageTomeges", new object[] {
                        execParam,
                        erec_HataridosErtesitesekList}, this.SendHataridosErtesitesMessageTomegesOperationCompleted, userState);
        }

        private void OnSendHataridosErtesitesMessageTomegesOperationCompleted(object arg)
        {
            if ((this.SendHataridosErtesitesMessageTomegesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SendHataridosErtesitesMessageTomegesCompleted(this, new SendHataridosErtesitesMessageTomegesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SendHataridosErtesitesMessageTomegesCompletedEventHandler(object sender, SendHataridosErtesitesMessageTomegesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SendHataridosErtesitesMessageTomegesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SendHataridosErtesitesMessageTomegesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public bool Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((bool)(this.results[0]));
                }
            }
        }

        #endregion
    }
}
