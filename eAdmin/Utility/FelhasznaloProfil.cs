using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contentum.eAdmin.BaseUtility
{
    public class FelhasznaloProfil: Contentum.eUtility.FelhasznaloProfil
    {
        public static bool OrgIsBOPMH(Page page)
        {
            return Constants.OrgKod.BOPMH.Equals(FelhasznaloProfil.OrgKod(page), StringComparison.InvariantCultureIgnoreCase);
        }
    }

}
