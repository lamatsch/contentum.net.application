using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;


/// <summary>
/// Summary description for AuthenticationService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AuthenticationService : System.Web.Services.WebService
{
    private AuthenticationServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public AuthenticationService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new AuthenticationServiceStoredProcedure(this.Application);
        sp = new AuthenticationServiceStoredProcedure(dataContext);
    }

    public AuthenticationService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new AuthenticationServiceStoredProcedure(dataContext);
    }

    /// <summary>
    /// Changes the password.
    /// </summary>
    /// <param name="ExecParam">The execute parameter.</param>
    /// <param name="Record">The record.</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentNullException">
    /// Record
    /// or
    /// ExecParam
    /// </exception>
    [WebMethod()]
    public Result ChangePassword(ExecParam ExecParam, KRT_Felhasznalok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (Record == null) throw new ArgumentNullException("Record");
            if (ExecParam == null) throw new ArgumentNullException("ExecParam");

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            ExecParam.Record_Id = Record.Id;
            ExecParam.Felhasznalo_Id = Record.Id;
            Contentum.eAdmin.Service.KRT_FelhasznalokService svcFelhasznalok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            Result res = svcFelhasznalok.UpdateFelhasznalo(ExecParam, Record);
            res.CheckError();

            result.Ds = null;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod]
    [WebMethod()]
    public Result Login(ExecParam ExecParam, Contentum.eBusinessDocuments.Login Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            //Jesz� SHA1 hash-elve
            Record.Jelszo = Crypt.GetSha1Hash(Record.Jelszo);

            result = sp.CheckLogin(ExecParam, Record);

            //Enged�lyezve van-e
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                //Jelsz� megfelel-e        
                System.Data.DataRow felhasznalo = result.Ds.Tables[0].Rows[0];
                if (felhasznalo["Tipus"].ToString().Equals("Basic"))
                {
                    Contentum.eAdmin.Service.KRT_FelhasznalokService svcUpdate = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                    if (felhasznalo["jelszo"].ToString() != Record.Jelszo)
                    {
                        ExecParam ep_getSysparam = ExecParam.Clone();
                        ep_getSysparam.Record_Id = "";
                        ep_getSysparam.Org_Id = felhasznalo["Org"].ToString();
                        ep_getSysparam.FelhasznaloSzervezet_Id = felhasznalo["Org"].ToString();
                        ep_getSysparam.Felhasznalo_Id = felhasznalo["Id"].ToString();

                        int maxwrongtry = int.Parse(Rendszerparameterek.Get(ep_getSysparam, "PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER"));
                        int wrongpasscnt = 0;
                        if (!int.TryParse(felhasznalo["WrongPassCnt"].ToString(), out wrongpasscnt))
                        {
                            wrongpasscnt = 0;
                        }

                        wrongpasscnt++;
                        KRT_Felhasznalok record = new KRT_Felhasznalok();
                        ExecParam updEp = ExecParam.Clone();
                        updEp.Record_Id = felhasznalo["Id"].ToString();
                        updEp.Felhasznalo_Id = felhasznalo["Id"].ToString();

                        record.Id = felhasznalo["Id"].ToString();
                        record.Updated.SetValueAll(false);
                        record.Base.Updated.SetValueAll(false);
                        record.Base.Ver = felhasznalo["Ver"].ToString();
                        record.Base.Updated.Ver = true;

                        if (wrongpasscnt == maxwrongtry)
                        {
                            record.Engedelyezett = Contentum.eUtility.Constants.Database.No;
                            record.Updated.Engedelyezett = true;

                            Contentum.eAdmin.Service.EmailService svcEmail = eAdminService.ServiceFactory.GetEmailService();
                            string syadminemail = Rendszerparameterek.Get(ep_getSysparam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);
                            string msg = string.Format("A {0} nev� felhaszn�l� a rendszerbe t�bbsz�ri sikertelen bejelentkez�si pr�b�lkoz�s miatt a bejelentkez�si enged�lye megvon�sra ker�lt.", felhasznalo["UserNev"].ToString());
                            string link = string.Empty;
                            try
                            {
                                link = string.Format("<br/> <a href='{0}'>Adminisztr�ci�</a>", UI.GetAppSetting("eAdminWebSiteUrl"));
                            }
                            catch (Exception) { }

                            bool resEmail = svcEmail.SendEmail(ep_getSysparam, syadminemail, new string[] { syadminemail }, "Felhaszn�l� felf�ggeszt�se", null, null, true, msg + Environment.NewLine + link);
                            if (!resEmail)
                                Logger.Error(string.Format("Az Email elk�ld�se nem siker�lt a {0} felhaszn�l� sz�m�ra, az {1} Email c�mr�l.", syadminemail, syadminemail));
                        }

                        record.WrongPassCnt = wrongpasscnt.ToString();
                        record.Updated.WrongPassCnt = true;

                        Result res = svcUpdate.UpdateFelhasznalo(updEp, record);
                        res.CheckError();

                        throw new ResultException(60000);
                    }
                    else
                    {
                        if (felhasznalo["wrongpasscnt"].ToString() != "0")
                        {
                            KRT_Felhasznalok record = new KRT_Felhasznalok();
                            ExecParam updEp = ExecParam.Clone();
                            updEp.Record_Id = felhasznalo["Id"].ToString();
                            updEp.Felhasznalo_Id = felhasznalo["Id"].ToString();

                            record.Updated.SetValueAll(false);
                            record.Base.Updated.SetValueAll(false);
                            record.Base.Ver = felhasznalo["Ver"].ToString();
                            record.Base.Updated.Ver = true;
                            record.WrongPassCnt = "0";
                            record.Updated.WrongPassCnt = true;

                            Result res = svcUpdate.UpdateFelhasznalo(updEp, record);
                            res.CheckError();
                        }
                    }

                    DateTime jelszoLejaratIdo = DateTime.MaxValue;
                    if (DateTime.TryParse(felhasznalo["jelszoLejaratIdo"].ToString(), out jelszoLejaratIdo))
                    {
                        if (DateTime.Now >= jelszoLejaratIdo)
                        {
                            //Lej�rt jelsz�                        
                            result.ErrorCode = "60006";
                        }
                    }
                }
                if (felhasznalo["Engedelyezett"].ToString() == Contentum.eUtility.Constants.Database.No)
                {
                    //Nincs enged�lyezve
                    throw new ResultException(60001);
                }
            }

            result.Ds = null;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    //[WebMethod()]
    //public Result LoginTest()
    //{
    //    Contentum.eBusinessDocuments.Login Record = new Login();
    //    Record.FelhasznaloNev = "AXIS\\harmat.krisztian";
    //    Record.Tipus = "Windows";
    //    //Record.Jelszo = "";
    //    return sp.CheckLogin(new ExecParam(),Record);
    //}
}

