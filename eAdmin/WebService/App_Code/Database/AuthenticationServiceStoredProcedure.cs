using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eUtility;

/// <summary>
/// Summary description for AuthenticationServiceStoredProcedure
/// </summary>

public class AuthenticationServiceStoredProcedure
{
    //private String ConnectionString;
    
    private DataContext dataContext;

    public AuthenticationServiceStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);
        //try
        //{
        //    //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //    throw e;
        //}
	}
    //private SqlConnection Connection;

    public Result CheckLogin(ExecParam ExecParam, Login Login)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_CheckLogin");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_CheckLogin]");

            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            //Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloNev", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@FelhasznaloNev"].Size = 100;
            SqlComm.Parameters["@FelhasznaloNev"].Value = Login.Typed.FelhasznaloNev;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Jelszo", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Jelszo"].Size = 100;
            SqlComm.Parameters["@Jelszo"].Value = Login.Typed.Jelszo;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tipus", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Tipus"].Size = 100;
            SqlComm.Parameters["@Tipus"].Value = Login.Typed.Tipus;


            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = SqlComm;

            try
            {
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    _ret.ErrorCode = "";
                    _ret.ErrorMessage = "";
                    _ret.Uid = ds.Tables[0].Rows[0]["Id"].ToString();
                    _ret.Ds = ds;
                }
                else
                {
                    _ret.ErrorCode = "[60000:AuthenticationServiceStoredProcedure:CheckLogin:UserPasswordPairNotFound]";
                    _ret.ErrorMessage = "[60000]";
                }
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
                throw e;
            }
            finally
            {
                ds.Dispose();
                da.Dispose();
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;         
                     
    }

}
