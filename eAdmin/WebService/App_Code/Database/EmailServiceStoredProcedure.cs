using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EmailServiceStoredProcedure
/// </summary>

public class EmailServiceStoredProcedure
{
	private DataContext dataContext;
	
	public EmailServiceStoredProcedure(DataContext _dataContext)
	{
		this.dataContext = _dataContext;
	}
	
    public Result GetAnswerEmail(ExecParam ExecParam, string kuldemeny_id, string ugyirat_id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_eMailServiceGetAnswerEmail");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_eMailServiceGetAnswerEmail]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KuldemenyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KuldemenyId"].Value = string.IsNullOrEmpty(kuldemeny_id) ? SqlGuid.Null : SqlGuid.Parse(kuldemeny_id);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = string.IsNullOrEmpty(ugyirat_id) ? SqlGuid.Null : SqlGuid.Parse(ugyirat_id);

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            //SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

}