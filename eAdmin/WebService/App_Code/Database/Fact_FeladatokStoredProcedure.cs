using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for Fact_FeladatokStoredProcedure
/// </summary>
public partial class Fact_FeladatokStoredProcedure
{
    public Result Ertesitesek(ExecParam execParam, FeladatokRiportParameterek feladatokRiportParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "eAdatpiac.Ertesitesek_SP");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[eAdatpiac].[Ertesitesek_SP]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            System.Reflection.PropertyInfo[] Properties = feladatokRiportParameterek.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo P in Properties)
            {
                if (P.GetValue(feladatokRiportParameterek, null) != null)
                {
                    Type SqlParamType = P.PropertyType;
                    string SqlParamName = FeladatokRiportParameterek.GetSQLParamName(P.Name);


                    if (!String.IsNullOrEmpty(SqlParamName))
                    {
                        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter(SqlParamName, P.PropertyType));
                        SqlComm.Parameters[SqlParamName].Value = P.GetValue(feladatokRiportParameterek, null);
                    }
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(execParam, _ret);
        return _ret;

    }


    //public Result FeladatFeldolgozo(ExecParam execParam, string Felhasznalo_Id)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "eAdatpiac.sp_feladatFeldolgozo");
    //    Result _ret = new Result();
    //    _ret.Ds = new DataSet();

    //    try
    //    {
    //        SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[eAdatpiac].[sp_feladatFeldolgozo]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@felhasznalo", System.Data.SqlDbType.VarChar, 100));
    //        SqlComm.Parameters["@felhasznalo"].Value = Felhasznalo_Id;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@szervezet", System.Data.SqlDbType.VarChar, 100));
    //        SqlComm.Parameters["@szervezet"].Value = execParam.FelhasznaloSzervezet_Id;

    //        SqlComm.ExecuteNonQuery();
    //    }
    //    catch (SqlException e)
    //    {
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(execParam, _ret);

    //    return _ret;
    //}


    public Result FeladatLetrehozo(ExecParam execParam, string Felhasznalo_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "eAdatpiac.sp_feladatLetrehozo");
        Result _ret = new Result();
        _ret.Ds = new DataSet();

        try
        {
            SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[eAdatpiac].[sp_feladatLetrehozo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@felhasznalo", System.Data.SqlDbType.VarChar, 100));
            SqlComm.Parameters["@felhasznalo"].Value = Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@szervezet", System.Data.SqlDbType.VarChar, 100));
            SqlComm.Parameters["@szervezet"].Value = execParam.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@indito_id", System.Data.SqlDbType.VarChar, 100));
            SqlComm.Parameters["@indito_id"].Value = execParam.LoginUser_Id; // az kapja az �zenetet, aki t�nylegesen megnyomta a gombot

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastUpdateTime", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@LastUpdateTime"].Direction = ParameterDirection.Output;

            SqlComm.ExecuteNonQuery();

            DateTime LastUpdateTime = DateTime.MinValue;

            if (SqlComm.Parameters["@LastUpdateTime"].Value != null)
            {
                LastUpdateTime = (DateTime)SqlComm.Parameters["@LastUpdateTime"].Value;
            }
            _ret.Record = LastUpdateTime;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);

        return _ret;
    }

    public Result StartFeladatJob(ExecParam execParam, string jobName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_start_job");     
        Result _ret = new Result();
        _ret.Ds = new DataSet();

        try
        {
            ConnectionStringSettings connStringSettings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.JobConnectionString"];
            string connectionString = connStringSettings.ConnectionString; //"Data Source=srvvirtual;Initial Catalog=msdb;Integrated Security=SSPI;";
            SqlConnection sqlConnection = Database.GetSqlConnection(connectionString);
            sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_start_job]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Connection = sqlConnection;

                //string jobName = "EDOKFeladatok";

                int jobResult;

                SqlParameter jobReturnValue = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
                jobReturnValue.Direction = ParameterDirection.ReturnValue;
                SqlComm.Parameters.Add(jobReturnValue);

                SqlComm.Parameters.Add(new SqlParameter("@job_name", SqlDbType.VarChar));
                SqlComm.Parameters["@job_name"].Value = jobName;

                SqlComm.ExecuteNonQuery();
                jobResult = (Int32)SqlComm.Parameters["@RETURN_VALUE"].Value;
                Logger.Info("StartFeladatJob: @RETURN_VALUE = " + jobResult.ToString(), execParam);
                sqlConnection.Close();

                switch (jobResult)
                {
                    case 0:
                        _ret.Record = "0";
                        break;
                    default:
                        _ret.ErrorCode = "[80010]";   // nem siker�lt elind�tani a jobot!
                        _ret.ErrorMessage = "[80010]";   // nem siker�lt elind�tani a jobot!
                        break;
                }

            }
            catch (SqlException e)
            {
                //_ret.ErrorCode = e.ErrorCode.ToString();
                //_ret.ErrorMessage = e.Message;
                Logger.Error("StartFeladatJob: ", e);
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);

        return _ret;
    }

}