using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_DokumentumokStoredProcedure
/// </summary>
public partial class KRT_DokumentumokStoredProcedure
{
    //private SqlConnection Connection;

    //private String ConnectionString;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_DokumentumokInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_DokumentumokUpdateParameters = null;

    public KRT_DokumentumokStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);

        //try
        //{
        //    //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //    throw e;
        //}
    }
    public Result Insert(String Method, ExecParam ExecParam, KRT_Dokumentumok Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Dokumentumok Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokInsert");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_DokumentumokInsert]");
                if (sp_KRT_DokumentumokInsertParameters == null)
                {
                    //sp_KRT_DokumentumokInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_DokumentumokInsert]");
                    //sp_KRT_DokumentumokInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_DokumentumokInsert]");
                    sp_KRT_DokumentumokInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_DokumentumokInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_DokumentumokInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_DokumentumokUpdate]");
                if (sp_KRT_DokumentumokUpdateParameters == null)
                {
                    //sp_KRT_DokumentumokUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_DokumentumokUpdate]");
                    //sp_KRT_DokumentumokUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_DokumentumokUpdate]");
                    sp_KRT_DokumentumokUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_DokumentumokUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_DokumentumokUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //  sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokGet");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            KRT_Dokumentumok _KRT_Dokumentumok = new KRT_Dokumentumok();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Dokumentumok, SqlComm);
            _ret.Record = _KRT_Dokumentumok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        // sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, KRT_DokumentumokSearch _KRT_DokumentumokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_DokumentumokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_DokumentumokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_DokumentumokSearch.OrderBy, _KRT_DokumentumokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokDelete");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokDelete]");

        try
        {
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokInvalidate");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}