﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using Contentum.eQuery;
using System.Data;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_HalozatiNyomtatokStoredProcedure
/// </summary>
public partial class KRT_HalozatiNyomtatokStoredProcedure
{
	 //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_HalozatiNyomtatokInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_HalozatiNyomtatokUpdateParameters = null;

    public KRT_HalozatiNyomtatokStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, KRT_HalozatiNyomtatok Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_HalozatiNyomtatok Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                    log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Halozati_NyomtatokInsert]");
                  if (sp_KRT_HalozatiNyomtatokInsertParameters == null)
                  {
                     //sp_KRT_SzemelyekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_SzemelyekInsert]");
                     //sp_KRT_SzemelyekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_SzemelyekInsert]");
                      sp_KRT_HalozatiNyomtatokInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Halozati_NyomtatokInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_HalozatiNyomtatokInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Halozati_NyomtatokUpdate]");
                  if (sp_KRT_HalozatiNyomtatokUpdateParameters == null)
                  {
                     //sp_KRT_SzemelyekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_SzemelyekUpdate]");
                     //sp_KRT_SzemelyekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_SzemelyekUpdate]");
                      sp_KRT_HalozatiNyomtatokUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Halozati_NyomtatokUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_HalozatiNyomtatokUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Halozati_NyomtatokGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            KRT_HalozatiNyomtatok _KRT_HalozatiNyomtatok = new KRT_HalozatiNyomtatok();
             Utility.LoadBusinessDocumentFromDataAdapter(_KRT_HalozatiNyomtatok, SqlComm);
             _ret.Record = _KRT_HalozatiNyomtatok;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, KRT_HalozatiNyomtatokSearch _KRT_HalozatiNyomtatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_HalozatiNyomtatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_HalozatiNyomtatokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Halozati_NyomtatokGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_HalozatiNyomtatokSearch.OrderBy, _KRT_HalozatiNyomtatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    //public Result Delete(ExecParam ExecParam)
    //{
    //    return Delete(ExecParam, DateTime.Now);
    //}

    //public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_SzemelyekDelete");
        
    //    Result _ret = new Result();
        
    //    //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
    //    //{
    //        //sqlConnection.Open();
            
    //        SqlCommand SqlComm = new SqlCommand("[sp_KRT_SzemelyekDelete]");

    //        try
    //        {
    //           SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //           //SqlComm.Connection = Connection;
    //           //SqlComm.Connection = sqlConnection;
    //           SqlComm.Connection = dataContext.Connection;
    //           SqlComm.Transaction = dataContext.Transaction;

    //           Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
    //           SqlComm.ExecuteNonQuery();

    //        }
    //        catch (SqlException e)
    //        {
    //           _ret.ErrorCode = e.ErrorCode.ToString();
    //           _ret.ErrorMessage = e.Message;
    //        }
            
    //        //sqlConnection.Close();
    //    //}
        
    //    log.SpEnd(ExecParam, _ret);
    //    return _ret;
    //}
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Halozati_NyomtatokInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}