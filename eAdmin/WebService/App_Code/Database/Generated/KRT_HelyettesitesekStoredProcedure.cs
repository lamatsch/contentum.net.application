using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_HelyettesitesekStoredProcedure
/// </summary>
public partial class KRT_HelyettesitesekStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_HelyettesitesekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_HelyettesitesekUpdateParameters = null;
    
    public KRT_HelyettesitesekStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, KRT_Helyettesitesek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Helyettesitesek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_HelyettesitesekInsert]");
                  if (sp_KRT_HelyettesitesekInsertParameters == null)
                  {
                     //sp_KRT_HelyettesitesekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_HelyettesitesekInsert]");
                     //sp_KRT_HelyettesitesekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_HelyettesitesekInsert]");
                     sp_KRT_HelyettesitesekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_HelyettesitesekInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_HelyettesitesekInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_HelyettesitesekUpdate]");
                  if (sp_KRT_HelyettesitesekUpdateParameters == null)
                  {
                     //sp_KRT_HelyettesitesekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_HelyettesitesekUpdate]");
                     //sp_KRT_HelyettesitesekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_HelyettesitesekUpdate]");
                     sp_KRT_HelyettesitesekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_HelyettesitesekUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_HelyettesitesekUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_HelyettesitesekGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_HelyettesitesekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             KRT_Helyettesitesek _KRT_Helyettesitesek = new KRT_Helyettesitesek();
             Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Helyettesitesek, SqlComm);
             _ret.Record = _KRT_Helyettesitesek;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, KRT_HelyettesitesekSearch _KRT_HelyettesitesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_KRT_HelyettesitesekSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _KRT_HelyettesitesekSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_HelyettesitesekGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_HelyettesitesekSearch.OrderBy, _KRT_HelyettesitesekSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_HelyettesitesekDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_KRT_HelyettesitesekInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}