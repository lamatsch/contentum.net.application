using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Log_PageStoredProcedure
/// </summary>
public partial class KRT_Log_PageStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_PageInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_PageUpdateParameters = null;
    
    public KRT_Log_PageStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_Page Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_Page Record, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_PageInsert]");
                  if (sp_KRT_Log_PageInsertParameters == null)
                  {
                     //sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_PageInsert]");
                     //sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_PageInsert]");
                     sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_PageInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_PageInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;

                  // TranzId-ba betesszük a Page_Id-t
                  Record.Base.Tranz_id = ExecParam.Page_Id;

                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_PageUpdate]");
                  if (sp_KRT_Log_PageUpdateParameters == null)
                  {
                     //sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_PageUpdate]");
                     //sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_PageUpdate]");
                     sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_PageUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_PageUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       //log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }

    public Result Get(ExecParam ExecParam)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_PageGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             KRT_Log_Page _KRT_Log_Page = new KRT_Log_Page();
             Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_Page, SqlComm);
             _ret.Record = _KRT_Log_Page;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, KRT_Log_PageSearch _KRT_Log_PageSearch)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_KRT_Log_PageSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _KRT_Log_PageSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_PageSearch.OrderBy, _KRT_Log_PageSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}