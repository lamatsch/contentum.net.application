using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Log_StoredProcedureStoredProcedure
/// </summary>
public partial class KRT_Log_StoredProcedureStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_StoredProcedureInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_StoredProcedureUpdateParameters = null;
    
    public KRT_Log_StoredProcedureStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}

    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_StoredProcedure Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_StoredProcedure Record, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;

                  // TranzId-ba betesszük a Page_Id-t
                  Record.Base.Tranz_id = ExecParam.Page_Id;

                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_StoredProcedureInsert]");
                  if (sp_KRT_Log_StoredProcedureInsertParameters == null)
                  {
                     //sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_StoredProcedureInsert]");
                     //sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_StoredProcedureInsert]");
                     sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_StoredProcedureInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_StoredProcedureInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_StoredProcedureUpdate]");
                  if (sp_KRT_Log_StoredProcedureUpdateParameters == null)
                  {
                     //sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_StoredProcedureUpdate]");
                     //sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_StoredProcedureUpdate]");
                     sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_StoredProcedureUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_StoredProcedureUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       //log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }

    public Result Get(ExecParam ExecParam)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_StoredProcedureGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             KRT_Log_StoredProcedure _KRT_Log_StoredProcedure = new KRT_Log_StoredProcedure();
             Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_StoredProcedure, SqlComm);
             _ret.Record = _KRT_Log_StoredProcedure;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, KRT_Log_StoredProcedureSearch _KRT_Log_StoredProcedureSearch)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_KRT_Log_StoredProcedureSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _KRT_Log_StoredProcedureSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_StoredProcedureSearch.OrderBy, _KRT_Log_StoredProcedureSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}