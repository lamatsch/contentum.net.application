using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Log_WebServiceStoredProcedure
/// </summary>
public partial class KRT_Log_WebServiceStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_WebServiceInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_Log_WebServiceUpdateParameters = null;
    
    public KRT_Log_WebServiceStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}

    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_WebService Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Log_WebService Record, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;

                  // TranzId-ba betesszük a Page_Id-t
                  Record.Base.Tranz_id = ExecParam.Page_Id;

                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_WebServiceInsert]");
                  if (sp_KRT_Log_WebServiceInsertParameters == null)
                  {
                     //sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_WebServiceInsert]");
                     //sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_WebServiceInsert]");
                     sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_WebServiceInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_WebServiceInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_Log_WebServiceUpdate]");
                  if (sp_KRT_Log_WebServiceUpdateParameters == null)
                  {
                     //sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_WebServiceUpdate]");
                     //sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_WebServiceUpdate]");
                     sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_WebServiceUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_WebServiceUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       //log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }

    public Result Get(ExecParam ExecParam)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_WebServiceGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             KRT_Log_WebService _KRT_Log_WebService = new KRT_Log_WebService();
             Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_WebService, SqlComm);
             _ret.Record = _KRT_Log_WebService;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, KRT_Log_WebServiceSearch _KRT_Log_WebServiceSearch)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_KRT_Log_WebServiceSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _KRT_Log_WebServiceSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_WebServiceSearch.OrderBy, _KRT_Log_WebServiceSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        //log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}