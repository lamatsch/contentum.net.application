using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_PartnerekStoredProcedure
/// </summary>
public partial class KRT_PartnerekStoredProcedure
{
    //private SqlConnection Connection;

    //private String ConnectionString;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KRT_PartnerekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KRT_PartnerekUpdateParameters = null;

    public KRT_PartnerekStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
        //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //throw e;
        //}
    }
    public Result Insert(String Method, ExecParam ExecParam, KRT_Partnerek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, KRT_Partnerek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekInsert");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_PartnerekInsert]");
                if (sp_KRT_PartnerekInsertParameters == null)
                {
                    //sp_KRT_PartnerekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_PartnerekInsert]");
                    //sp_KRT_PartnerekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_PartnerekInsert]");
                    sp_KRT_PartnerekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_PartnerekInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_PartnerekInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_PartnerekUpdate]");
                if (sp_KRT_PartnerekUpdateParameters == null)
                {
                    //sp_KRT_PartnerekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_PartnerekUpdate]");
                    //sp_KRT_PartnerekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_PartnerekUpdate]");
                    sp_KRT_PartnerekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_PartnerekUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_PartnerekUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGet");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            KRT_Partnerek _KRT_Partnerek = new KRT_Partnerek();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Partnerek, SqlComm);
            _ret.Record = _KRT_Partnerek;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_PartnerekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    // BLG_1714
    public Result FindByPartnerAndCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekFindByPartnerAndCim");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //cim query fel�p�t�se
            if (_KRT_CimekSearch != null)
            {
                Query queryCim = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_CimekSearch.ErvKezd.Clear();
                _KRT_CimekSearch.ErvVege.Clear();

                queryCim.BuildFromBusinessDocument(_KRT_CimekSearch);

                queryCim.Where += _KRT_CimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryCim.Where))
                {
                    if (!String.IsNullOrEmpty(queryPartner.Where))
                    {
                        query.Where = queryPartner.Where + " AND " + queryCim.Where;
                    }
                    else
                    {
                        query.Where = queryCim.Where;
                    }
                }
                else
                {
                    query.Where = queryPartner.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekFindByPartnerAndCim]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;


    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekDelete");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekDelete]");

        try
        {
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekInvalidate");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Consolidate_Update(ExecParam ExecParam, string RecordId, string ConsolidateRecordId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Consolidate_Update");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Consolidate_Update]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RecordId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@RecordId"].Value = new Guid(RecordId); 

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ConsolidatedRecordId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ConsolidatedRecordId"].Value = new Guid(ConsolidateRecordId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = new Guid(ExecParam.Felhasznalo_Id);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}