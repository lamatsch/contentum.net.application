using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_CimekStoredProcedure
/// </summary>

public partial class KRT_CimekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CimekGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_CimekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_CimekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CimekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_CimekSearch.OrderBy, _KRT_CimekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek, String CIM_TIPUS,
            KRT_PartnerCimekSearch _KRT_PartnerCimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CimekGetAllByPartner");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_PartnerCimekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_PartnerCimekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CimekGetAllByPartner]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Partner_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Partner_Id"].Value = _KRT_Partnerek.Typed.Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Cim_Tipus", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Cim_Tipus"].Size = 100;
            SqlComm.Parameters["@Cim_Tipus"].Value = CIM_TIPUS;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerCimekSearch.OrderBy, _KRT_PartnerCimekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllASPADOAddressId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_GetKRT_Partnerek_ASPADO_Azonosito(Id)");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("select Id, dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(Id) ASPADOId  from KRT_Cimek with (nolock)");
            SqlComm.CommandType = System.Data.CommandType.Text;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetASPADOAddressId(ExecParam ExecParam, long adoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_GetKRT_Partnerek_ASPADO_Cimek_Azonosito");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("select dbo.fn_GetKRT_Partnerek_ASPADO_Cimek_Azonosito(@adoId) as Id");
            SqlComm.CommandType = System.Data.CommandType.Text;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@adoId", SqlDbType.BigInt).Value = adoId;
            object result = SqlComm.ExecuteScalar();
            _ret.Record = result==null ? null : result.ToString();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}