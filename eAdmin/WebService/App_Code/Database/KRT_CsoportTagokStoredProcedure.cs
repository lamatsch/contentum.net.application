using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_CsoportTagokStoredProcedure
/// </summary>

public partial class KRT_CsoportTagokStoredProcedure
{
    public Result ForceUpdate(ExecParam ExecParam, KRT_CsoportTagok Record)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.

            log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokForceUpdate");
            Record.Base.Updated.ModositasIdo = true;
            Record.Base.Updated.Modosito_id = true;
            Record.Base.ModositasIdo = ExecutionTime.ToString();
            Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
            SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportTagokForceUpdate]");
            if (sp_KRT_CsoportTagokUpdateParameters == null)
            {
                //sp_KRT_CsoportTagokUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_CsoportTagokForceUpdate]");
                sp_KRT_CsoportTagokUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_CsoportTagokForceUpdate]");
            }

            Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_CsoportTagokUpdateParameters);

            Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;              
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result IsMember(ExecParam ExecParam, string CsoportId, string UserId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.

            log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokIsMember");

            SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportTagokIsMember]");



            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;              
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

            SqlComm.Parameters.Add(new SqlParameter("@UserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UserId"].Value = SqlGuid.Parse(UserId);

            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Record = ds.Tables[0].Rows[0].ItemArray[0].ToString() == "1" ? true : false;

                // CR#2293: a napl�z�sban megt�veszt�, mert nem val�di hib�t jelz� hiba�zenetek kik�sz�b�l�s�re
                // a bool rekordot adjuk tov�bb false eset�n is, �s nem gener�lunk hib�t
                //if (!(bool)_ret.Record)
                //    throw new Exception("[52001]");
            }
            catch (Exception e)
            {
                if (e.GetType() == Type.GetType("SqlException"))
                {
                    _ret.ErrorCode = (e as SqlException).ErrorCode.ToString();
                    _ret.ErrorMessage = (e as SqlException).Message;
                }
                else
                {
                    _ret.ErrorCode = _ret.ErrorMessage = e.Message;
                }
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result GetSzervezet(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokGetSzervezet");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportTagokGetSzervezet]");

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            _ret.Ds = ds;

            Contentum.eUtility.ResultError.CreateNoRowResultError(_ret);

            if (!_ret.IsError)
            {
                _ret.Uid = ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result GetAllWithExtension(ExecParam ExecParam, KRT_CsoportTagokSearch _KRT_CsoportTagokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_CsoportTagokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_CsoportTagokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportTagokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_CsoportTagokSearch.OrderBy, _KRT_CsoportTagokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result InvalidateWithoutRebuildCsoportTagokAll(ExecParam ExecParam)
    {
        return InvalidateWithoutRebuildCsoportTagokAll(ExecParam, DateTime.Now);
    }

    public Result InvalidateWithoutRebuildCsoportTagokAll(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokInvalidate");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportTagokInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.Parameters.Add("@RebuildCsoportTagokAll", SqlDbType.Int);
            SqlComm.Parameters["@RebuildCsoportTagokAll"].Value = 0;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result CsoporttagokAllBuildFromCsoportTagok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_CsoporttagokAllBuildFromCsoportTagok");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoporttagokAllBuildFromCsoportTagok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAll_ForTree(ExecParam execParam, string SelectedId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_CsoportTagokGetAll_ForTree");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportTagokGetAll_ForTree]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (!string.IsNullOrEmpty(SelectedId))
            {
                SqlComm.Parameters.Add(new SqlParameter("@SelectedId", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@SelectedId"].Value = SqlGuid.Parse(SelectedId);
            }

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(execParam.Felhasznalo_Id);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

}