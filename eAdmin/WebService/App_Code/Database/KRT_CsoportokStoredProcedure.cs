using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_CsoportokStoredProcedure
/// </summary>

public partial class KRT_CsoportokStoredProcedure
{
    public Result GetAllWithFK(ExecParam ExecParam, KRT_CsoportokSearch _KRT_CsoportokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetAllWithFK");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_CsoportokSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_CsoportokSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportokGetAllWithFK]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_CsoportokSearch.OrderBy, _KRT_CsoportokSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetAllBySzuloCsoport(ExecParam ExecParam, KRT_Csoportok szuloCsoport
        , KRT_CsoportTagokSearch _krt_csoporttagokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetAllBySzuloCsoport");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_krt_csoporttagokSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _krt_csoporttagokSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportokGetAllBySzuloCsoport]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SzuloCsoportId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@SzuloCsoportId"].Value = szuloCsoport.Typed.Id;


                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _krt_csoporttagokSearch.OrderBy, _krt_csoporttagokSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }





    public Result GetAllByFunkcio(ExecParam execParam, string funkcioKod, KRT_CsoportokSearch krt_CsoportokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_FelhasznaloCsoportokGetAllByFunkcio");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FunkcioKod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@FunkcioKod"].Value = funkcioKod;
            SqlComm.Parameters["@FunkcioKod"].Size = 100;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(krt_CsoportokSearch);
            query1.Where += krt_CsoportokSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, execParam, krt_CsoportokSearch.OrderBy, krt_CsoportokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(execParam, _ret);
        return _ret;

    }



    public Result GetByKod(ExecParam ExecParam, KRT_Csoportok krt_csoport)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetByKod");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportokGetByKod]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;                
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add("csoportkod", SqlDbType.NVarChar).Value = krt_csoport.Kod;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                //Id nem kell
                SqlComm.Parameters.RemoveAt(1);

                KRT_Csoportok _KRT_Csoportok = new KRT_Csoportok();
                Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Csoportok, SqlComm);
                _ret.Record = _KRT_Csoportok;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetFirstRightedLeader(ExecParam ExecParam, bool bIncludeSelfIfRightedLeader, string funkcioKod)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetFirstRightedLeader");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportokGetFirstRightedLeader]");

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

        SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = SqlGuid.Parse(ExecParam.FelhasznaloSzervezet_Id);

        if (!String.IsNullOrEmpty(ExecParam.Helyettesites_Id))
        {
            SqlComm.Parameters.Add(new SqlParameter("@Helyettesites_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Helyettesites_Id"].Value = SqlGuid.Parse(ExecParam.Helyettesites_Id);
        }

        if (!String.IsNullOrEmpty(funkcioKod))
        {
            SqlComm.Parameters.Add(new SqlParameter("@FunkcioKod", SqlDbType.NVarChar, 100));
            SqlComm.Parameters["@FunkcioKod"].Value = funkcioKod;
        }

        SqlComm.Parameters.Add(new SqlParameter("@IncludeSelfIfRightedLeader", SqlDbType.TinyInt));
        SqlComm.Parameters["@IncludeSelfIfRightedLeader"].Value = (bIncludeSelfIfRightedLeader == true ? 1 : 0);

        try
        {
            KRT_Csoportok _KRT_Csoportok = new KRT_Csoportok();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Csoportok, SqlComm);
            _ret.Record = _KRT_Csoportok;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;

    }

    public Result GetLeader(ExecParam ExecParam, string CsoportId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetLeader");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportokGetLeader]");

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        CsoportId = string.IsNullOrEmpty(CsoportId) ? ExecParam.Record_Id : CsoportId;
        SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

        try
        {
            KRT_Csoportok _KRT_Csoportok = new KRT_Csoportok();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Csoportok, SqlComm);
            _ret.Record = _KRT_Csoportok;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;

    }

    // CR3157 A csoport kiv�laszt� komponensben iktat�k�nyv v�laszt�sn�l az ide iktat�k �s a jogosultak unioja jelenjen meg. 
    public Result GetAllByIktatokonyvIktathat(ExecParam execParam, string IktatokonyvId, KRT_CsoportokSearch search)
    {
        var withCsoporttag = !String.IsNullOrEmpty(execParam.CsoportTag_Id);
        var spName = withCsoporttag ? "sp_KRT_CsoportokGetAllByIktatokonyvIktathatAndCsoporttag" : "sp_KRT_CsoportokGetAllByIktatokonyvIktathat";
        Logger.DebugStart();
        Logger.Debug(spName + " t�rolt elj�r�s elindul");
        Result _ret = new Result();
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, spName);

        SqlCommand SqlComm = new SqlCommand("[" + spName + "]");
        SqlComm.CommandType = CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@IraIktatokonyv_Id", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@IraIktatokonyv_Id"].Value = SqlGuid.Parse(IktatokonyvId);
        if (withCsoporttag)
        {
            SqlComm.Parameters.Add(new SqlParameter("@CsoporttagId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoporttagId"].Value = SqlGuid.Parse(execParam.CsoportTag_Id);
        }

        Query query = new Query();
        query.BuildFromBusinessDocument(search);
        query.Where += search.WhereByManual;
        Logger.Debug(String.Format("Param�terek: where: {0}, orderby: {1}, toprow: {2}", query.Where, search.OrderBy, search.TopRow));

        Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, search.OrderBy, search.TopRow);

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(String.Format(spName + " t�rolt elj�r�s hiba: {0},{1}", e.ErrorCode.ToString(), e.Message));
        }

        Logger.Debug(spName + " t�rolt elj�r�s v�ge");
        Logger.DebugEnd();
        log.SpEnd(execParam, _ret);
        return _ret;

    }


    public Result GetAllSubCsoport(ExecParam ExecParam, string CsoportId, bool KozvetlenulHozzarendelt, KRT_CsoportokSearch where)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokGetAllSubCsoport");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(where);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += where.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportokGetAllSubCsoport]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, where.OrderBy, where.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Where", SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Size = 4000;
            SqlComm.Parameters["@Where"].Value = query.Where;

            SqlComm.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            SqlComm.Parameters["@OrderBy"].Value = where.OrderBy;

            SqlComm.Parameters.Add(new SqlParameter("@TopRow", SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = where.TopRow;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);


            SqlComm.Parameters.Add(new SqlParameter("@kozvetlenulHozzarendelt", SqlDbType.TinyInt));
            SqlComm.Parameters["@kozvetlenulHozzarendelt"].Value = KozvetlenulHozzarendelt == true ? 1 : 0;

            SqlComm.Parameters.Add(new SqlParameter("@szervezetId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@szervezetId"].Value = SqlGuid.Parse(CsoportId);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Revalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportokRevalidate");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_CsoportokRevalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToRevalidateStoredProcedure(SqlComm, ExecParam);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
    public Result GetAllAtmenetiIrattar(ExecParam ExecParam, KRT_PartnerKapcsolatokSearch _KRT_PartnerKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_GetAllAtmenetiIrattar");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_GetAllAtmenetiIrattar]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;                
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_KRT_PartnerKapcsolatokSearch);
            query1.Where += _KRT_PartnerKapcsolatokSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Size = 4000;
            SqlComm.Parameters["@Where"].Value = query1.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            if (!String.IsNullOrEmpty(_KRT_PartnerKapcsolatokSearch.OrderBy))
                SqlComm.Parameters["@OrderBy"].Value = " " + _KRT_PartnerKapcsolatokSearch.OrderBy;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = _KRT_PartnerKapcsolatokSearch.TopRow.ToString();

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}