using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Extra_NapokStoredProcedure
/// </summary>
public partial class KRT_Extra_NapokStoredProcedure
{
    public Result KovetkezoMunkanap(ExecParam ExecParam, DateTime DatumTol, int Sorszam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Extra_NapokKovetkezoMunkanap");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Extra_NapokKovetkezoMunkanap]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@DatumTol", SqlDbType.DateTime).Value = DatumTol;
            SqlComm.Parameters.Add("@Sorszam", SqlDbType.Int).Value = Sorszam;

            _ret.Record = SqlComm.ExecuteScalar();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result MunkanapokSzama(ExecParam ExecParam, DateTime DatumTol, DateTime DatumIg)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_munkanapok_szama2");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("select dbo.fn_munkanapok_szama2(@DatumTol, @DatumIg)");
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@DatumTol", SqlDbType.DateTime).Value = DatumTol;
            SqlComm.Parameters.Add("@DatumIg", SqlDbType.DateTime).Value = DatumIg;

            _ret.Record = SqlComm.ExecuteScalar();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}