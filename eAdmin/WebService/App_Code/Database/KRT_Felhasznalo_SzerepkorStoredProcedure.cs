using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_Felhasznalo_SzerepkorStoredProcedure
/// </summary>

public partial class KRT_Felhasznalo_SzerepkorStoredProcedure
{
    public Result GetAllByCsoport(ExecParam ExecParam, KRT_Csoportok _KRT_Csoportok
       , KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorGetAllByCsoport");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorGetAllByCsoport]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Csoport_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Csoport_Id"].Value = _KRT_Csoportok.Typed.Id;

                Query query1 = new Query();
                query1.BuildFromBusinessDocument(_KRT_Felhasznalo_SzerepkorSearch);
                query1.Where += _KRT_Felhasznalo_SzerepkorSearch.WhereByManual;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, ExecParam, _KRT_Felhasznalo_SzerepkorSearch.OrderBy, _KRT_Felhasznalo_SzerepkorSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetAllWithExtension(ExecParam ExecParam, KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_Felhasznalo_SzerepkorSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_Felhasznalo_SzerepkorSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Felhasznalo_SzerepkorSearch.OrderBy, _KRT_Felhasznalo_SzerepkorSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    // megb�z�s id�tartam�nak m�dosul�sa eset�n a kapcsolt szerepk�r�k �rv�nyess�g�nek fel�lvizsg�lata
    // a megb�z�s azonos�t�j�t az ExecParam.Record_Id tartalmazza
    public Result UpdateByMegbizas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorUpdateByMegbizas");
        Result _ret = new Result();
        try
        {
            //sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorUpdateByMegbizas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    // A szerepkor hozz�rendel�s �rv�nyess�g�nek m�dosul�sakor a megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek
    // ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. Az eredeti felhaszn�l�-szerepk�r hozz�rendel�s azonos�t�j�t
    // az ExecParam.Record_Id tartalmazza.
    public Result UpdateAtruhazottBySzerepkor(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor");
        Result _ret = new Result();
        try
        {
            //sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    // A szerepkor hozz�rendel�s �rv�nytelen�t�sekor a megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek
    // ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. Az eredeti felhaszn�l�-szerepk�r hozz�rendel�s azonos�t�j�t
    // az ExecParam.Record_Id tartalmazza.
    public Result InvalidateAtruhazottBySzerepkor(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorInvalidateAtruhazottBySzerepkor");
        Result _ret = new Result();
        try
        {
            //sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorInvalidateAtruhazottBySzerepkor]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    // Az �tadott megb�z�i szerepk�r�k m�sol�sa a KRT_Felhasznalo_Szerepkor t�bl�ba a megb�z�t�l a megb�zotthoz,
    // a KRT_Helyettesitesek rekord alapj�n. A megb�z�si rekord azonos�t�j�t az ExecParam.Record_Id tartalmazza.
    public Result InsertToMegbizas_Tomeges(ExecParam ExecParam, string[] FelhasznaloSzerepkorIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges");
        Result _ret = new Result();
        try
        {
            //sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Megbizas_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Megbizas_Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add("@Ids", SqlDbType.NVarChar);
            SqlComm.Parameters["@Ids"].Value = "'" + String.Join("','", FelhasznaloSzerepkorIds) + "'";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}