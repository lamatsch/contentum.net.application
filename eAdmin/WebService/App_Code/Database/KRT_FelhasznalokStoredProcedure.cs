using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_FelhasznalokStoredProcedure
/// </summary>

public partial class KRT_FelhasznalokStoredProcedure
{
    public Result GetAllBySzerepkor(ExecParam ExecParam, KRT_Szerepkorok _KRT_Szerepkorok
        , KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FelhasznalokGetAllBySzerepkor");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznalokGetAllBySzerepkor]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szerepkor_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Szerepkor_Id"].Value = _KRT_Szerepkorok.Typed.Id;

                Query query1 = new Query();
                query1.BuildFromBusinessDocument(_KRT_Felhasznalo_SzerepkorSearch);
                query1.Where += _KRT_Felhasznalo_SzerepkorSearch.WhereByManual;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, ExecParam, _KRT_Felhasznalo_SzerepkorSearch.OrderBy, _KRT_Felhasznalo_SzerepkorSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }


    public Result GetLastLoginTime(ExecParam execParam, string felhasznaloId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetLastLoginTime");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_GetLastLoginTime]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloId"].Value = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(felhasznaloId, System.Data.SqlTypes.SqlGuid.Null);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

            string lastLoginTime = ds.Tables[0].Rows[0]["LastLoginTime"].ToString();

            _ret.Record = lastLoginTime;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result Revalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FelhasznalokRevalidate");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznalokRevalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToRevalidateStoredProcedure(SqlComm, ExecParam);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllUserNev(ExecParam ExecParam, KRT_FelhasznalokSearch _KRT_FelhasznalokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FelhasznalokGetAllUserNev");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznalokGetAllUserNev]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_KRT_FelhasznalokSearch);
            query1.Where += _KRT_FelhasznalokSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, ExecParam, _KRT_FelhasznalokSearch.OrderBy, _KRT_FelhasznalokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    // CR3328 Felhaszn�l� �rv�nytelen�t�s el�tt ellen�rz�s, hogy van-e hozz� ugyirat, irat v k�ldem�ny r�gz�tve (� a kezel�je) 
    public Result CheckKapcsoltUgyek(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FelhasznalokCheckKapcsoltUgyek");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznalokCheckKapcsoltUgyek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(ExecParam.Record_Id, System.Data.SqlTypes.SqlGuid.Null);
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;


        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}