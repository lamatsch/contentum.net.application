﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using System.Data;
using Contentum.eQuery;


/// <summary>
/// Summary description for KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure
/// </summary>
public partial class KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure
{
    public Result GetByNyomtato(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Felhasznalok_GetByNyomtato");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Felhasznalok_GetByNyomtato]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}