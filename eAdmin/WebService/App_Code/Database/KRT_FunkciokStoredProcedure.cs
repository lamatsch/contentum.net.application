using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_SzerepkorokStoredProcedure
/// </summary>

public partial class KRT_FunkciokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_FunkciokSearch _KRT_FunkciokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetAllWithExtension");

        Result _ret = new Result();


        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_FunkciokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where = Search.ConcatWhereExpressions(query.Where , _KRT_FunkciokSearch.WhereByManual);

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_FunkciokSearch.OrderBy, _KRT_FunkciokSearch.TopRow);
            if (_KRT_FunkciokSearch.Extended_KRT_ObjTipusokSearch != null)
                Utils.AddExtendedSearchObjectToGetAllStoredProcedure(SqlComm, "@WhereObjTipus", _KRT_FunkciokSearch.Extended_KRT_ObjTipusokSearch, _KRT_FunkciokSearch.Extended_KRT_ObjTipusokSearch.WhereByManual);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok, string Helyettesites_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetAllByFelhasznalo");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetAllByFelhasznalo]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Felhasznalo_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Felhasznalo_Id"].Value = _KRT_Felhasznalok.Typed.Id;

                if (!String.IsNullOrEmpty(Helyettesites_Id))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Helyettesites_Id", System.Data.SqlDbType.UniqueIdentifier));
                    SqlComm.Parameters["@Helyettesites_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Helyettesites_Id);
                }

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 200;
                SqlComm.Parameters["@OrderBy"].Value = "order by " + "KRT_Funkciok.Nev";

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TopRow"].Size = 5;
                SqlComm.Parameters["@TopRow"].Value = "0";

                /*            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                            SqlComm.Parameters["@OrderBy"].Size = 200;
                            SqlComm.Parameters["@OrderBy"].Value = "";

                            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
                            SqlComm.Parameters["@TopRow"].Size = 5;
                            SqlComm.Parameters["@TopRow"].Value = "1000";*/

                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    /// <summary>
    /// A csoporttags�g alapj�n lesz�ri a saj�t jog�, aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat. Azon szerepk�r hozz�rendel�seket is figyelembe veszi,
    /// ahol nincs megadva a csoporttags�g a felhaszn�l�-szerepk�r hozz�rendel�sben, de helyettes�t�s sem.
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a KRT_CsoportTagok rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt funkci�k (Id, Kod p�rokk�nt).</returns>
    public Result GetAllByCsoporttagSajatJogu(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetAllByCsoporttagSajatJogu");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetAllByCsoporttagSajatJogu]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CsoportTag_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportTag_Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            SqlComm.Parameters["@OrderBy"].Value = "order by " + "KRT_Funkciok.Nev";

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = "0";

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    /// <summary>
    /// A megb�z�s alapj�n lesz�ri az aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat.  A megb�z�snak aktu�lisan folyamatban kell lennie
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes helyettes�t�sre, csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a megb�z�st tartalmaz� KRT_Helyettesitesek rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt funkci�k (Id, Kod p�rokk�nt).</returns>
    public Result GetAllByMegbizas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetAllByMegbizas");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetAllByMegbizas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Helyettesites_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Helyettesites_Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            SqlComm.Parameters["@OrderBy"].Value = "order by " + "KRT_Funkciok.Nev";

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = "0";

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllBySzerepkor(ExecParam ExecParam, KRT_Szerepkorok  _KRT_Szerepkorok
        , KRT_Szerepkor_FunkcioSearch _KRT_Szerepkor_FunkcioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetAllBySzerepkor");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetAllBySzerepkor]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szerepkor_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Szerepkor_Id"].Value = _KRT_Szerepkorok.Typed.Id;

                Query query1 = new Query();
                query1.BuildFromBusinessDocument(_KRT_Szerepkor_FunkcioSearch);
                query1.Where += _KRT_Szerepkor_FunkcioSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where"].Size = 4000;
                SqlComm.Parameters["@Where"].Value = query1.Where;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 200;
                SqlComm.Parameters["@OrderBy"].Value = _KRT_Szerepkor_FunkcioSearch.OrderBy;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TopRow"].Size = 5;
                SqlComm.Parameters["@TopRow"].Value = _KRT_Szerepkor_FunkcioSearch.TopRow.ToString();

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }



    public Result GetByTableAndOperation(ExecParam ExecParam, String TablaKod, String MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FunkciokGetByTableAndOperation");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_FunkciokGetByTableAndOperation]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                //Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.UniqueIdentifier));
                //SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TablaKod", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TablaKod"].Size = 100;
                SqlComm.Parameters["@TablaKod"].Value = TablaKod;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MuveletKod", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@MuveletKod"].Size = 100;
                SqlComm.Parameters["@MuveletKod"].Value = MuveletKod;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                KRT_Funkciok _KRT_Funkciok = new KRT_Funkciok();
                Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Funkciok, SqlComm);
                _ret.Record = _KRT_Funkciok;
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

}