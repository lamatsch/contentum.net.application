﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for KRT_HalozatiNyomtatokStoredProcedure
/// </summary>
public partial class KRT_HalozatiNyomtatokStoredProcedure
{
    public Result GetByFelhasznalo(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Halozati_NyomtatokGetByFelhasznalo");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Halozati_NyomtatokGetByFelhasznalo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    //public Result Revalidate(ExecParam ExecParam)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_SzemelyekRevalidate");

    //    Result _ret = new Result();

    //    try
    //    {
    //        SqlCommand SqlComm = new SqlCommand("[sp_KRT_SzemelyekRevalidate]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        Utility.AddDefaultParameterToRevalidateStoredProcedure(SqlComm, ExecParam);

    //        SqlComm.ExecuteNonQuery();
    //    }
    //    catch (SqlException e)
    //    {
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, _ret);
    //    return _ret;
    //}
}