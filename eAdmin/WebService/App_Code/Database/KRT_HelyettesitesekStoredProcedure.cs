using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for KRT_HelyettesitesekStoredProcedure
/// </summary>

public partial class KRT_HelyettesitesekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_HelyettesitesekSearch _KRT_HelyettesitesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_HelyettesitesekGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_HelyettesitesekSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_HelyettesitesekSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_HelyettesitesekGetAllWithExtension]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_HelyettesitesekSearch.OrderBy, _KRT_HelyettesitesekSearch.TopRow);

                if (!String.IsNullOrEmpty(_KRT_HelyettesitesekSearch.ExtendedFilter.CsoportTag_Id))
                {
                    SqlComm.Parameters.Add(new SqlParameter("@CsoportTag_Id", SqlDbType.UniqueIdentifier));
                    SqlComm.Parameters["@CsoportTag_Id"].Value = SqlGuid.Parse(_KRT_HelyettesitesekSearch.ExtendedFilter.CsoportTag_Id);
                }

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

}
