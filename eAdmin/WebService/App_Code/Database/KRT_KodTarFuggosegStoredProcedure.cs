using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KRT_KodtarFuggosegStoredProcedure
/// </summary>

public partial class KRT_KodtarFuggosegStoredProcedure
{
    /// <summary>
    /// GetAllWithExtension
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_KRT_KodtarFuggosegSearch"></param>
    /// <returns></returns>
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_KodtarFuggosegSearch _KRT_KodtarFuggosegSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_KodtarFuggosegGetAllWithExtension");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_KodtarFuggosegSearch);
            if (_KRT_KodtarFuggosegSearch != null)
                query.Where += _KRT_KodtarFuggosegSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_KodtarFuggosegGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            if (_KRT_KodtarFuggosegSearch == null)
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;
            }

            if (_KRT_KodtarFuggosegSearch != null)
                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_KodtarFuggosegSearch.OrderBy, _KRT_KodtarFuggosegSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}