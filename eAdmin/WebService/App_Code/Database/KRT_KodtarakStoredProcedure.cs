using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_KodtarakStoredProcedure
/// </summary>

public partial class KRT_KodTarakStoredProcedure
{
    public Result GetAllByKodcsoportKod(ExecParam ExecParam, String KodcsoportKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_KodTarakGetAllByKodcsoportKod");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_KodTarakGetAllByKodcsoportKod]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KodcsoportKod", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@KodcsoportKod"].Size = 200;
                SqlComm.Parameters["@KodcsoportKod"].Value = KodcsoportKod;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;
    }

    public Result GetAllWithKodcsoport(ExecParam ExecParam, KRT_KodTarakSearch _KRT_KodTarakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_KodTarakGetAllWithKodcsoport");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_KodTarakSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_KodTarakSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("sp_KRT_KodTarakGetAllWithKodcsoport");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_KodTarakSearch.OrderBy, _KRT_KodTarakSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetByKodcsoportKod(ExecParam ExecParam, string kodcsoportKod, string kodtarKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_KodTarakGetByKodcsoportKod");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_KodTarakGetByKodcsoportKod]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //paraméterek
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KodcsoportKod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@KodcsoportKod"].Value = kodcsoportKod;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KodtarKod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@KodtarKod"].Value = kodtarKod;

            KRT_KodTarak _KRT_KodTarak = new KRT_KodTarak();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_KodTarak, SqlComm);
            _ret.Record = _KRT_KodTarak;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}