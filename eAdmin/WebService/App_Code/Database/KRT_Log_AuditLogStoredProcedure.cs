﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Log_AuditLogStoredProcedure
/// </summary>
public partial class KRT_Log_AuditLogStoredProcedure
{

    public Result AuditLogExcelExport(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_LogSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_LogSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_AuditLogExport]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 600;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_LogSearch.OrderBy, _KRT_LogSearch.TopRow);
            
            SqlComm.Parameters.Add("Where_Log_Login", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_Login;
            SqlComm.Parameters.Add("Where_Log_Page", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_Page;
            SqlComm.Parameters.Add("Where_Log_WebService", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_WebService;
            SqlComm.Parameters.Add("Where_Log_StoredProcedure", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_StoredProcedure;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        return _ret;

    }
}