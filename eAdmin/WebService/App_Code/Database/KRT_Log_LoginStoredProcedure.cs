using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_Log_LoginStoredProcedure
/// </summary>
public partial class KRT_Log_LoginStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
               Query query = new Query();

               query.Where = _KRT_LogSearch.Where_Log_Login;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_LoginGetAllWithExtension]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_LogSearch.KRT_Log_LoginSearch.OrderBy, _KRT_LogSearch.KRT_Log_LoginSearch.TopRow);

               SqlComm.Parameters.Add("Where_Log_Page", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_Page;
               SqlComm.Parameters.Add("Where_Log_WebService", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_WebService;
               SqlComm.Parameters.Add("Where_Log_StoredProcedure", SqlDbType.NVarChar).Value = _KRT_LogSearch.Where_Log_StoredProcedure;

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
        //    sqlConnection.Close();
        //}
        
        return _ret;

    }
}