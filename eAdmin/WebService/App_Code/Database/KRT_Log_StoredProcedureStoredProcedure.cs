using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_Log_StoredProcedureStoredProcedure
/// </summary>
public partial class KRT_Log_StoredProcedureStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_KRT_LogSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _KRT_LogSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureGetAllWithExtension]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_LogSearch.OrderBy, _KRT_LogSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
        //    sqlConnection.Close();
        //}
        
        return _ret;

    }
}