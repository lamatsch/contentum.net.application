using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_MenukStoredProcedure
/// </summary>
public partial class KRT_MenukStoredProcedure
{
    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_MenukGetAllByFelhasznalo");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_MenukGetAllByFelhasznalo]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Felhasznalo_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Felhasznalo_Id"].Value = _KRT_Felhasznalok.Typed.Id;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Alkalmazas_Kod", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Alkalmazas_Kod"].Size = 100;
                SqlComm.Parameters["@Alkalmazas_Kod"].Value = Alkalmazas_Kod;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;
    }

    /// <summary>
    /// A csoporttags�g alapj�n lesz�ri a saj�t jog�, aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat, �s a nekik megfelel� men�ket. Azon szerepk�r hozz�rendel�seket is figyelembe veszi,
    /// ahol nincs megadva a csoporttags�g a felhaszn�l�-szerepk�r hozz�rendel�sben, de helyettes�t�s sem.
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a KRT_CsoportTagok rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt men�k.</returns>
    public Result GetAllByCsoporttagSajatJogu(ExecParam ExecParam, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_MenukGetAllByCsoporttagSajatJogu");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_MenukGetAllByCsoporttagSajatJogu]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CsoportTag_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportTag_Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Alkalmazas_Kod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Alkalmazas_Kod"].Size = 100;
            SqlComm.Parameters["@Alkalmazas_Kod"].Value = Alkalmazas_Kod;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// A megb�z�s alapj�n lesz�ri az aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat �s a nekik megfelel� men�ket. A megb�z�snak aktu�lisan folyamatban kell lennie
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes helyettes�t�sre, csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a megb�z�st tartalmaz� KRT_Helyettesitesek rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt men�k.</returns>
    public Result GetAllByMegbizas(ExecParam ExecParam, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_MenukGetAllByMegbizas");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_MenukGetAllByMegbizas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Helyettesites_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Helyettesites_Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Alkalmazas_Kod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Alkalmazas_Kod"].Size = 100;
            SqlComm.Parameters["@Alkalmazas_Kod"].Value = Alkalmazas_Kod;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}
