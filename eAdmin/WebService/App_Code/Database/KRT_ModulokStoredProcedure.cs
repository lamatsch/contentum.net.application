using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_ModulokStoredProcedure
/// </summary>
public partial class KRT_ModulokStoredProcedure
{
    public Result GetAllWithFK(ExecParam ExecParam, KRT_ModulokSearch _KRT_ModulokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_ModulokGetAllWithFK");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_ModulokSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_ModulokSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_ModulokGetAllWithFK]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_ModulokSearch.OrderBy, _KRT_ModulokSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }
	
}
