using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KRT_NezetekStoredProcedure
/// </summary>
public partial class KRT_NezetekStoredProcedure
{
	public KRT_NezetekStoredProcedure()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Result GetAllWithFK(ExecParam ExecParam, KRT_NezetekSearch _KRT_NezetekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_NezetekGetAllWithFK");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_NezetekSearch, true);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_NezetekSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_NezetekGetAllWithFK]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_NezetekSearch.OrderBy, _KRT_NezetekSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetAllByMuvelet(ExecParam ExecParam, KRT_NezetekSearch _KRT_NezetekSearch, string MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_NezetekGetAllByMuvelet");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_NezetekSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_NezetekSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_NezetekGetAllByMuvelet]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MuveletKod", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@MuveletKod"].Size = 20;
                SqlComm.Parameters["@MuveletKod"].Value = MuveletKod;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_NezetekSearch.OrderBy, _KRT_NezetekSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

}
