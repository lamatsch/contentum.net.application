using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data.SqlTypes;
//using System.EnterpriseServices;

/// <summary>
/// Summary description for KRT_ObjTipusokStoredProcedure
/// </summary>

public partial class KRT_ObjTipusokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, String KodFilter)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_ObjTipusokGetAllWithExtension");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_ObjTipusokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_ObjTipusokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_ObjTipusokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_ObjTipusokSearch.OrderBy, _KRT_ObjTipusokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@KodFilter", SqlDbType.NVarChar, 100));
            SqlComm.Parameters["@KodFilter"].Value = KodFilter;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByTableColumn(ExecParam ExecParam, String Table, String Column)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_ObjTipusokGetAllByTableColumn");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_ObjTipusokGetAllByTableColumn]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Table", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@Table"].Value = Table;

            SqlComm.Parameters.Add("@Column", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@Column"].Value = Column;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAzonosito(ExecParam ExecParam, string ObjektumId, string ObjektumTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_ObjTipusokGetAzonosito");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_ObjTipusokGetAzonosito]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@ObjektumId", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(ObjektumId);

            SqlComm.Parameters.Add("@ObjektumTipus", SqlDbType.NVarChar, 100).Value = ObjektumTipus;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}