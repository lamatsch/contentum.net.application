using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;
//using System.EnterpriseServices;

/// <summary>
/// Summary description for KRT_OrgokStoredProcedure
/// </summary>

public partial class KRT_OrgokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_OrgokSearch _KRT_OrgokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_OrgokGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_OrgokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_OrgokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_OrgokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_OrgokSearch.OrderBy, _KRT_OrgokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    /// <summary>
    /// Uj ORG letrehozasakor a KRT_KodTarak es a KRT_Parameterek tabla feltoltese
    /// a rendszer vezerlesere hasznalt bejegyzesekkel, az uj ORG-hoz kotve.
    /// </summary>
    /// <param name="ExecParam">A Record_Id-ben tartalmaznia kell az �j Org Id-j�t!</param>
    /// <returns></returns>
    public Result CloneSystemControllersForNewOrg(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Orgok_CloneSystemControllersForNewOrg");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Orgok_CloneSystemControllersForNewOrg]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Org", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Org"].Value = SqlGuid.Parse(ExecParam.Record_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Tranz_id"].Value = SqlGuid.Parse(dataContext.Tranz_Id);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}