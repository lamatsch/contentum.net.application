using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_PartnerMinositesekStoredProcedure
/// </summary>

public partial class KRT_PartnerMinositesekStoredProcedure
{
    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek, KRT_PartnerMinositesekSearch _KRT_PartnerMinositesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerMinositesekGetAllByPartner");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_PartnerMinositesekSearch);

                query.Where += _KRT_PartnerMinositesekSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerMinositesekGetAllByPartner]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Partner_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Partner_Id"].Value = _KRT_Partnerek.Typed.Id;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerMinositesekSearch.OrderBy, _KRT_PartnerMinositesekSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }
}