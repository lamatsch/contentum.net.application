using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
//using System.EnterpriseServices;

/// <summary>
/// Summary description for KRT_PartnerekStoredProcedure
/// </summary>

public partial class KRT_PartnerekStoredProcedure
{
    public Result GetPartnerByEmail(ExecParam execparam,
                                    string email,
                                    KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execparam, "sp_KRT_PartnerekGetByEmail");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetByEmail]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar));
            SqlComm.Parameters["@Email"].Value = email;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(execparam, _ret);
        return _ret;
    }



    public Result GetByCsoportId(ExecParam ExecParam, string CsoportId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetByCsoportId");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_PartnerekGetByCsoportId]");

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@Csoport_Id", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Csoport_Id"].Value = SqlGuid.Parse(CsoportId);

        try
        {
            KRT_Partnerek _KRT_Partnerek = new KRT_Partnerek();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Partnerek, SqlComm);
            // ha nem t�lt�tte fel az objektumot, null-t adunk vissza (ha nem tal�lt partnert)
            if (String.IsNullOrEmpty(_KRT_Partnerek.Id))
            {
                _ret.Record = null;
            }
            else
            {
                _ret.Record = _KRT_Partnerek;
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek, KRT_PartnerKapcsolatokSearch _KRT_PartnerKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllByPartner");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllByPartner]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Partner_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Partner_Id"].Value = _KRT_Partnerek.Typed.Id;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_KRT_PartnerKapcsolatokSearch);
            query1.Where += _KRT_PartnerKapcsolatokSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Size = 4000;
            SqlComm.Parameters["@Where"].Value = query1.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            if (!String.IsNullOrEmpty(_KRT_PartnerKapcsolatokSearch.OrderBy))
                SqlComm.Parameters["@OrderBy"].Value = " " + _KRT_PartnerKapcsolatokSearch.OrderBy;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = _KRT_PartnerKapcsolatokSearch.TopRow.ToString();

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithFK(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllWithFK");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_PartnerekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllWithFK]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByCim(ExecParam ExecParam, KRT_Cimek _KRT_Cimek,
        KRT_PartnerCimekSearch _KRT_PartnerCimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllByCim");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllByCim]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Cim_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Cim_Id"].Value = _KRT_Cimek.Typed.Id;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_KRT_PartnerCimekSearch);
            query1.Where += _KRT_PartnerCimekSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, ExecParam, _KRT_PartnerCimekSearch.OrderBy, _KRT_PartnerCimekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithSzemelyek(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, bool withCim)
    {
        string spName;
        if (withCim)
        {
            spName = "[sp_KRT_PartnerekGetAllWithSzemelyAndCim]";
        }
        else
        {
            spName = "[sp_KRT_PartnerekGetAllWithSzemely]";
        }

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, spName);

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //szemely query fel�p�t�se
            Query querySzemely = new Query();
            querySzemely.BuildFromBusinessDocument(_KRT_PartnerekSearch.SzemelyekSearch);

            querySzemely.Where += _KRT_PartnerekSearch.SzemelyekSearch.WhereByManual;

            if (!String.IsNullOrEmpty(querySzemely.Where))
            {
                if (!String.IsNullOrEmpty(queryPartner.Where))
                {
                    query.Where = queryPartner.Where + " AND " + querySzemely.Where;
                }
                else
                {
                    query.Where = querySzemely.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }


            SqlCommand SqlComm = new SqlCommand(spName);
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithSzemelyek(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        return GetAllWithSzemelyek(ExecParam, _KRT_PartnerekSearch, false);
    }

    public Result GetAllWithVallalkozasok(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, bool withCim)
    {
        string spName;
        if (withCim)
        {
            spName = "[sp_KRT_PartnerekGetAllWithVallalkozasAndCim]";
        }
        else
        {
            spName = "[sp_KRT_PartnerekGetAllWithVallalkozas]";
        }

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //vallalkozasquery fel�p�t�se
            Query queryVallalkozas = new Query();
            queryVallalkozas.BuildFromBusinessDocument(_KRT_PartnerekSearch.VallalkozasokSearch);

            queryVallalkozas.Where += _KRT_PartnerekSearch.VallalkozasokSearch.WhereByManual;

            if (!String.IsNullOrEmpty(queryVallalkozas.Where))
            {
                if (!String.IsNullOrEmpty(queryPartner.Where))
                {
                    query.Where = queryPartner.Where + " AND " + queryVallalkozas.Where;
                }
                else
                {
                    query.Where = queryVallalkozas.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }

            SqlCommand SqlComm = new SqlCommand(spName);
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithVallalkozasok(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        return GetAllWithVallalkozasok(ExecParam, _KRT_PartnerekSearch, false);
    }

    public Result GetAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        return GetAllWithCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, String.Empty);

    }

    public Result GetAllWithCimForExport(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        return GetAllWithCimForExport(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, String.Empty);

    }
    
    public Result GetAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllWithCim");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //cim query fel�p�t�se
            if (_KRT_CimekSearch != null)
            {
                Query queryCim = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_CimekSearch.ErvKezd.Clear();
                _KRT_CimekSearch.ErvVege.Clear();

                queryCim.BuildFromBusinessDocument(_KRT_CimekSearch);

                queryCim.Where += _KRT_CimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryCim.Where))
                {
                    if (!String.IsNullOrEmpty(queryPartner.Where))
                    {
                        query.Where = queryPartner.Where + " AND " + queryCim.Where;
                    }
                    else
                    {
                        query.Where = queryCim.Where;
                    }
                }
                else
                {
                    query.Where = queryPartner.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllWithCim]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            if (!String.IsNullOrEmpty(WithAllCim))
            {
                SqlComm.Parameters.Add("@WithAllCim", SqlDbType.Char).Value = WithAllCim;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithCimForExport(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllWithCimForExport");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //cim query fel�p�t�se
            if (_KRT_CimekSearch != null)
            {
                Query queryCim = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_CimekSearch.ErvKezd.Clear();
                _KRT_CimekSearch.ErvVege.Clear();

                queryCim.BuildFromBusinessDocument(_KRT_CimekSearch);

                queryCim.Where += _KRT_CimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryCim.Where))
                {
                    if (!String.IsNullOrEmpty(queryPartner.Where))
                    {
                        query.Where = queryPartner.Where + " AND " + queryCim.Where;
                    }
                    else
                    {
                        query.Where = queryCim.Where;
                    }
                }
                else
                {
                    query.Where = queryPartner.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllWithCimForExport]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            if (!String.IsNullOrEmpty(WithAllCim))
            {
                SqlComm.Parameters.Add("@WithAllCim", SqlDbType.Char).Value = WithAllCim;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetASPADOId(ExecParam ExecParam, string partnerId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_GetKRT_Partnerek_ASPADO_Azonosito");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("select dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(@adoId) as Id");
            SqlComm.CommandType = System.Data.CommandType.Text;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (partnerId != null && !string.IsNullOrEmpty(partnerId))
            {
                SqlComm.Parameters.Add("@adoId", SqlDbType.UniqueIdentifier).Value = new Guid(partnerId.Trim());
                _ret.Record = SqlComm.ExecuteScalar().ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetASPADOPartnerId(ExecParam ExecParam, int adoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_GetKRT_Partnerek_ASPADO_Partner_Azonosito");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("select dbo.fn_GetKRT_Partnerek_ASPADO_Partner_Azonosito(@adoId) as Id");
            SqlComm.CommandType = System.Data.CommandType.Text;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@adoId", SqlDbType.Int).Value =adoId;
            object result = SqlComm.ExecuteScalar();
            _ret.Record = result == null ? null : result.ToString();            

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllASPADOPartnerId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "fn_GetKRT_Partnerek_ASPADO_Azonosito(id)");

        Result _ret = new Result();
  
        try
        {           
            SqlCommand SqlComm = new SqlCommand("select Id, dbo.fn_GetKRT_Partnerek_ASPADO_Azonosito(id) ASPADOId from KRT_Partnerek with (nolock)");
            SqlComm.CommandType = System.Data.CommandType.Text;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;            

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithCimandSzemelyekandVallalkozasok(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, KRT_SzemelyekSearch _KRT_SzemelyekSearch, KRT_VallalkozasokSearch _KRT_VallalkozasokSearch,KRT_PartnerCimekSearch _KRT_PartnerCimekSearch, string WithAllCim)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllWithVallalkozasAndSzemelyAndCim");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //cim query fel�p�t�se
            if (_KRT_CimekSearch != null)
            {
                Query queryCim = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_CimekSearch.ErvKezd.Clear();
                _KRT_CimekSearch.ErvVege.Clear();

                queryCim.BuildFromBusinessDocument(_KRT_CimekSearch);

                queryCim.Where += _KRT_CimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryCim.Where))
                {
                    if (!String.IsNullOrEmpty(queryPartner.Where))
                    {
                        query.Where = queryPartner.Where + " AND " + queryCim.Where;
                    }
                    else
                    {
                        query.Where = queryCim.Where;
                    }
                }
                else
                {
                    query.Where = queryPartner.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }


            if (_KRT_SzemelyekSearch != null)
            {
                Query querySzemelyek = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_SzemelyekSearch.ErvKezd.Clear();
                _KRT_SzemelyekSearch.ErvVege.Clear();

                querySzemelyek.BuildFromBusinessDocument(_KRT_SzemelyekSearch);

                querySzemelyek.Where += _KRT_SzemelyekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(querySzemelyek.Where))
                {
                    if (!String.IsNullOrEmpty(query.Where))
                    {
                        query.Where = query.Where + " AND " + querySzemelyek.Where;
                    }
                    else
                    {
                        query.Where = querySzemelyek.Where;
                    }
                }
            }

            if (_KRT_VallalkozasokSearch != null)
            {
                Query queryVallalkozasok = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_VallalkozasokSearch.ErvKezd.Clear();
                _KRT_VallalkozasokSearch.ErvVege.Clear();

                queryVallalkozasok.BuildFromBusinessDocument(_KRT_VallalkozasokSearch);

                queryVallalkozasok.Where += _KRT_VallalkozasokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryVallalkozasok.Where))
                {
                    if (!String.IsNullOrEmpty(query.Where))
                    {
                        query.Where = query.Where + " AND " + queryVallalkozasok.Where;
                    }
                    else
                    {
                        query.Where = queryVallalkozasok.Where;
                    }
                }
            }

            if (_KRT_PartnerCimekSearch != null)
            {
                Query queryPartnerCimek = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_VallalkozasokSearch.ErvKezd.Clear();
                _KRT_VallalkozasokSearch.ErvVege.Clear();

                queryPartnerCimek.BuildFromBusinessDocument(_KRT_PartnerCimekSearch);

                queryPartnerCimek.Where += _KRT_PartnerCimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryPartnerCimek.Where))
                {
                    if (!String.IsNullOrEmpty(query.Where))
                    {
                        query.Where = query.Where + " AND " + queryPartnerCimek.Where;
                    }
                    else
                    {
                        query.Where = queryPartnerCimek.Where;
                    }
                }
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllWithVallalkozasAndSzemelyAndCim]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            if (!String.IsNullOrEmpty(WithAllCim))
            {
                SqlComm.Parameters.Add("@WithAllCim", SqlDbType.Char).Value = WithAllCim;
            }

            if (!String.IsNullOrEmpty(_KRT_PartnerekSearch.Id.Value))
            {
                SqlComm.Parameters.Add("@partnerId", SqlDbType.NVarChar).Value = _KRT_PartnerekSearch.Id.Value;
                _KRT_PartnerekSearch.Id.Value = null;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetReallyAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim, string WithKuldemeny, string cimTipus, string WithKapcsoltPartner)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetReallyAllWithCim");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            //partner query fel�p�t�se
            Query queryPartner = new Query();
            queryPartner.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            queryPartner.Where += _KRT_PartnerekSearch.WhereByManual;

            //cim query fel�p�t�se
            if (_KRT_CimekSearch != null)
            {
                Query queryCim = new Query();
                //c�mek �rv�nyess�g�nek figyelmen k�v�l hagy�sa
                _KRT_CimekSearch.ErvKezd.Clear();
                _KRT_CimekSearch.ErvVege.Clear();

                queryCim.BuildFromBusinessDocument(_KRT_CimekSearch);

                queryCim.Where += _KRT_CimekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(queryCim.Where))
                {
                    if (!String.IsNullOrEmpty(queryPartner.Where))
                    {
                        query.Where = queryPartner.Where + " AND " + queryCim.Where;
                    }
                    else
                    {
                        query.Where = queryPartner.Where;
                    }
                }
                else
                {
                    query.Where = queryPartner.Where;
                }
            }
            else
            {
                query.Where = queryPartner.Where;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetReallyAllWithCim]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            if (!String.IsNullOrEmpty(WithAllCim))
            {
                SqlComm.Parameters.Add("@WithAllCim", SqlDbType.Char).Value = WithAllCim;
            }

            if (!String.IsNullOrEmpty(WithKuldemeny))
            {
                SqlComm.Parameters.Add("@WithKuldemeny", SqlDbType.Char).Value = WithKuldemeny;
            }

            if (cimTipus != null)
            {
                List<string> cimTipusParamValue = new List<string>();
                foreach (string item in cimTipus.Split(','))
                {
                    cimTipusParamValue.Add(String.Format("'{0}'", item));

                }
                SqlComm.Parameters.Add("@CimTipus", SqlDbType.NVarChar).Value = String.Join(",", cimTipusParamValue.ToArray());
            }

            // BUG_5197 (nekrisz)
            if (!String.IsNullOrEmpty(WithKapcsoltPartner))
            {
                SqlComm.Parameters.Add("@WithKapcsoltPartner", SqlDbType.Char).Value = WithKapcsoltPartner;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllForSzamla(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, string Cim_Id, string Bankszamlaszam_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetAllForSzamla");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_PartnerekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_PartnerekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetAllForSzamla]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_PartnerekSearch.OrderBy, _KRT_PartnerekSearch.TopRow);

            if (!String.IsNullOrEmpty(Cim_Id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Cim_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Cim_Id"].Value = SqlGuid.Parse(Cim_Id);
            }

            if (!String.IsNullOrEmpty(Bankszamlaszam_Id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Bankszamlaszam_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Bankszamlaszam_Id"].Value = SqlGuid.Parse(Bankszamlaszam_Id);
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Revalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekRevalidate");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekRevalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToRevalidateStoredProcedure(SqlComm, ExecParam);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    //public Result Test(string ExecParam)
    //{
    //    //SqlTransaction tran = null;
    //    Result _ret = new Result();
    //    try
    //    {
    //        SqlCommand SqlComm = new SqlCommand("insert into KRT_Partnerek (Id, Nev, LetrehozasIdo, MinositesKezdDat, MinositesVegeDat) values (newid(), 'testtran1', getdate(), getdate(), getdate())");
    //        SqlComm.Connection = Connection;
    //        SqlComm.ExecuteNonQuery();

    //        //tran = Connection.BeginTransaction();
    //        if (ExecParam == "1")
    //        {
    //            SqlCommand SqlComm3 = new SqlCommand("insert into KRT_Partnerek (Id, Nev, LetrehozasIdo, MinositesKezdDat, MinositesVegeDat) values (newid(), 'testtran1withommit', getdate(), getdate(), getdate())");
    //            SqlComm3.Connection = Connection;
    //            SqlTransaction tran = Connection.BeginTransaction();
    //            SqlComm3.Transaction = tran;
    //            SqlComm3.ExecuteNonQuery();
    //            tran.Commit();

    //        }
    //        if (ExecParam == "2")
    //        {
    //            SqlCommand SqlComm2 = new SqlCommand("insert into KRT_Partnerek (Id, Nev, LetrehozasIdo, MinositesKezdDat) values (newid(), 'testtran2', getdate(), getdate())");
    //            SqlComm2.Connection = Connection;
    //            //SqlComm.Transaction = tran;
    //            SqlComm2.ExecuteNonQuery();
    //        }
    //        //tran.Commit();
    //        //Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

    //        /* Ide kell meg a datum parameter */

    //    }
    //    catch (SqlException e)
    //    {
    //        //tran.Rollback();
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }
    //    return _ret;
    //}

    public Result GetTermeszetesAzonositok(ExecParam execParam, String nev, String emailCim)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_PartnerekGetTermeszetesAzonositok");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetTermeszetesAzonositok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Nev", SqlDbType.VarChar));
            SqlComm.Parameters[0].Direction = ParameterDirection.Input;
            SqlComm.Parameters[0].Value = nev;

            SqlComm.Parameters.Add(new SqlParameter("@EmailCim", SqlDbType.VarChar));
            SqlComm.Parameters[1].Direction = ParameterDirection.Input;
            SqlComm.Parameters[1].Value = emailCim;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(SqlComm);
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }


    /// <summary>
    /// Szervezet partner keres�se - kapcsoattartoval �sszef�zve
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="topRow"></param>
    /// <param name="partnerLike"></param>
    /// <returns></returns>
    public Result GetSzervezetekWithKapcsolattarto(ExecParam ExecParam, int topRow, string partnerLike)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_PartnerekGetSzervezetWithKapcsolattarto");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_PartnerekGetSzervezetWithKapcsolattarto]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@TopRow", SqlDbType.Int).Value = topRow;
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = new Guid(ExecParam.Felhasznalo_Id);
            SqlComm.Parameters.Add("@Like", SqlDbType.NVarChar, -1).Value = partnerLike;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}