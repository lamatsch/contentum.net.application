using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Data.SqlTypes;
//using System.EnterpriseServices;

/// <summary>
/// Summary description for KRT_CsoportokStoredProcedure
/// </summary>

public partial class KRT_Szerepkor_FunkcioStoredProcedure
{
    /// <summary>
    /// Egy adott szerepk�rh�z minden m�g hozz� nem rendelt funkci� felv�tele.
    /// �j ORG l�trehoz�sakor minden funkci� hozz�rendel�se a l�trehozott �j adminisztr�ci�s szerepk�rh�z.
    /// </summary>
    /// <param name="ExecParam">A Record_Id-ben tartalmaznia kell az �j Org Id-j�t!</param>
    /// <returns></returns>
    public Result AddAllFunctionsToSzerepkor(ExecParam ExecParam, string Szerepkor_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Szerepkor_FunkcioAddAllFunctionsToSzerepkor");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Szerepkor_FunkcioAddAllFunctionsToSzerepkor]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Szerepkor_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Szerepkor_Id"].Value = SqlGuid.Parse(Szerepkor_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Tranz_id"].Value = SqlGuid.Parse(dataContext.Tranz_Id);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}