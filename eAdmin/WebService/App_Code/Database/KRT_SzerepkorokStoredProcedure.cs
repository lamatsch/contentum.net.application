using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_SzerepkorokStoredProcedure
/// </summary>

public partial class KRT_SzerepkorokStoredProcedure
{
    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok
        ,KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_SzerepkorokGetAllByFelhasznalo");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_SzerepkorokGetAllByFelhasznalo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            /*Meg implemetalni kell a eQuery-ben*/
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Felhasznalo_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = _KRT_Felhasznalok.Typed.Id;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_KRT_Felhasznalo_SzerepkorSearch);
            query1.Where += _KRT_Felhasznalo_SzerepkorSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query1, ExecParam, _KRT_Felhasznalo_SzerepkorSearch.OrderBy, _KRT_Felhasznalo_SzerepkorSearch.TopRow);

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            //SqlComm.Parameters["@Where"].Size = 4000;
            //SqlComm.Parameters["@Where"].Value = query1.Where;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            //SqlComm.Parameters["@OrderBy"].Size = 200;
            //SqlComm.Parameters["@OrderBy"].Value = "order by " + _KRT_Felhasznalo_SzerepkorSearch.OrderBy;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            //SqlComm.Parameters["@TopRow"].Size = 5;
            //SqlComm.Parameters["@TopRow"].Value = "0";

            /*            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                        SqlComm.Parameters["@OrderBy"].Size = 200;
                        SqlComm.Parameters["@OrderBy"].Value = "";

                        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
                        SqlComm.Parameters["@TopRow"].Size = 5;
                        SqlComm.Parameters["@TopRow"].Value = "1000";*/

            //SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            //SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;            

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetAllByFunkcio(ExecParam ExecParam, KRT_Funkciok _KRT_Funkciok
        , KRT_Szerepkor_FunkcioSearch _KRT_Szerepkor_FunkcioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_SzerepkorokGetAllByFunkcio");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_SzerepkorokGetAllByFunkcio]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Funkcio_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Funkcio_Id"].Value = _KRT_Funkciok.Typed.Id;

                Query query1 = new Query();
                query1.BuildFromBusinessDocument(_KRT_Szerepkor_FunkcioSearch);
                query1.Where += _KRT_Szerepkor_FunkcioSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where"].Size = 4000;
                SqlComm.Parameters["@Where"].Value = query1.Where;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 200;
                SqlComm.Parameters["@OrderBy"].Value = _KRT_Szerepkor_FunkcioSearch.OrderBy;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TopRow"].Size = 5;
                SqlComm.Parameters["@TopRow"].Value = _KRT_Szerepkor_FunkcioSearch.TopRow.ToString();

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }
}