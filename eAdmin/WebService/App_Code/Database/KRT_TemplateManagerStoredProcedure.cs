using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KRT_TemplateManagerStoredProcedure
/// </summary>
public partial class KRT_TemplateManagerStoredProcedure
{
    public Result GetAllWithFK(ExecParam ExecParam, KRT_TemplateManagerSearch _KRT_TemplateManagerSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_TemplateManagerGetAllWithFK");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_TemplateManagerSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_TemplateManagerSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_TemplateManagerGetAllWithFK]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_TemplateManagerSearch.OrderBy, _KRT_TemplateManagerSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam,_ret);
        return _ret;

    }
}
