using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for KRT_UIMezoObjektumErtekekStoredProcedure
/// </summary>
public partial class KRT_UIMezoObjektumErtekekStoredProcedure
{
    #region CR3194 - Kisherceg megosztás
    public Result GetAll(ExecParam ExecParam, KRT_UIMezoObjektumErtekekSearch _KRT_UIMezoObjektumErtekekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_UIMezoObjektumErtekekGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {

           
            //_KRT_UIMezoObjektumErtekekSearch.WhereByManual = " or (KRT_UIMezoObjektumErtekek.Org_Id = '" + ExecParam.Org_Id + "' and KRT_UIMezoObjektumErtekek.Publikus='1'" +
            //    "and KRT_UIMezoObjektumErtekek.ErvKezd <= getdate() and KRT_UIMezoObjektumErtekek.ErvVege >= getdate()" +
            //    " and KRT_UIMezoObjektumErtekek.TemplateTipusNev = '" + _KRT_UIMezoObjektumErtekekSearch.TemplateTipusNev.Value + "')";
            
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_UIMezoObjektumErtekekSearch);
            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_UIMezoObjektumErtekekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_UIMezoObjektumErtekekGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_UIMezoObjektumErtekekSearch.OrderBy, _KRT_UIMezoObjektumErtekekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
    #endregion
    // CR3194 - Kisherceg megosztás
    internal static Dictionary<String, Utility.Parameter> sp_KRT_UIMezoObjektumErtekekMegosztasWithSzemelyOrSzervezetParameters = null;

    public Result sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet(ExecParam ExecParam, KRT_UIMezoObjektumErtekek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet");

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Id);
            if (!String.IsNullOrEmpty(Record.Felhasznalo_Id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@FelhasznaloId"].Value = new System.Data.SqlTypes.SqlGuid(Record.Felhasznalo_Id);
            }
            if (!String.IsNullOrEmpty(Record.Publikus))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Org_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Org_Id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Org_Id);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Publikus", System.Data.SqlDbType.Char));
                SqlComm.Parameters["@Publikus"].Value = Record.Publikus;
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Szervezet_Id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Szervezet_Id);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Modosito_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Modosito_id"].Value = new System.Data.SqlTypes.SqlGuid(ExecParam.Felhasznalo_Id);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ModositasIdo", System.Data.SqlDbType.DateTime));
                SqlComm.Parameters["@ModositasIdo"].Value = ExecutionTime.ToString();
            }
            if (!String.IsNullOrEmpty(Record.Base.Stat_id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Stat_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Stat_id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Base.Stat_id);
            }
            if (!String.IsNullOrEmpty(Record.ErvKezd))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ErvKezd", System.Data.SqlDbType.DateTime));
                SqlComm.Parameters["@ErvKezd"].Value = Record.ErvKezd;
            }
            if (!String.IsNullOrEmpty(Record.ErvVege))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ErvVege", System.Data.SqlDbType.DateTime));
                SqlComm.Parameters["@ErvVege"].Value = Record.ErvVege;
            }
            if (!String.IsNullOrEmpty(Record.Base.Letrehozo_id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Letrehozo_id"].Value = new System.Data.SqlTypes.SqlGuid(ExecParam.Felhasznalo_Id);
            }
            if (!String.IsNullOrEmpty(Record.Base.LetrehozasIdo))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozasIdo", System.Data.SqlDbType.DateTime));
                SqlComm.Parameters["@LetrehozasIdo"].Value = ExecutionTime.ToString();
            }
            if (!String.IsNullOrEmpty(Record.Base.Tranz_id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tranz_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Tranz_id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Base.Tranz_id);
            }
            if (!String.IsNullOrEmpty(Record.Base.UIAccessLog_id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UIAccessLog_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@UIAccessLog_id"].Value = new System.Data.SqlTypes.SqlGuid(Record.Base.UIAccessLog_id);
            }

            Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

            //foreach (SqlParameter item in SqlComm.Parameters)
            //{
            //    try { System.Data.SqlTypes.SqlGuid.Parse(item.SqlValue.ToString()); }
            //    catch(Exception e)
            //    {
            //        var test = e.InnerException;
            //        try { System.Data.SqlTypes.SqlGuid.Parse(item.Value.ToString()); }
            //        catch (Exception ex)
            //        {
            //            test = ex.InnerException;
            //        }
            //    }
            //}

            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}