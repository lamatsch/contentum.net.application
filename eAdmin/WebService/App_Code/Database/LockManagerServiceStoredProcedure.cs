using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eUtility;

/// <summary>
/// Summary description for LockManagerServiceStoredProcedure
/// </summary>
public class LockManagerServiceStoredProcedure
{
    //private String ConnectionString;
    //private SqlConnection Connection;
    
    private DataContext dataContext;

    public LockManagerServiceStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);

        //try
        //{
        //    //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //    throw e;
        //}
	}


    public Result LockRecord(ExecParam ExecParam, string TableName)
    {
        return LockRecord(ExecParam, TableName, DateTime.Now);
    }

    public Result LockRecord(ExecParam ExecParam, string TableName, DateTime executionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_LockRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_LockRecord]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TableName"].Size = 200;
            SqlComm.Parameters["@TableName"].Value = TableName;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutionTime", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@ExecutionTime"].Value = executionTime;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

            
        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;
    }


    public Result UnlockRecord(ExecParam ExecParam, string TableName)
    {
        return UnlockRecord(ExecParam, TableName, DateTime.Now);
    }

    public Result UnlockRecord(ExecParam ExecParam, string TableName, DateTime executionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_UnlockRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_UnlockRecord]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TableName"].Size = 200;
            SqlComm.Parameters["@TableName"].Value = TableName;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutionTime", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@ExecutionTime"].Value = executionTime;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

            
        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;
    }


    public Result ForceLockRecord(ExecParam ExecParam, string TableName)
    {
        return ForceLockRecord(ExecParam, TableName, DateTime.Now);
    }

    public Result ForceLockRecord(ExecParam ExecParam, string TableName, DateTime executionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_ForceLockRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_ForceLockRecord]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TableName"].Size = 200;
            SqlComm.Parameters["@TableName"].Value = TableName;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutionTime", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@ExecutionTime"].Value = executionTime;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastZarolo_id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@LastZarolo_id"].Direction = ParameterDirection.Output;

            SqlComm.ExecuteNonQuery();

            _ret.Uid = SqlComm.Parameters["@LastZarolo_id"].Value.ToString(); 
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
            
        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;
    }


    public Result ForceUnlockRecord(ExecParam ExecParam, string TableName)
    {
        return ForceUnlockRecord(ExecParam, TableName, DateTime.Now);
    }

    public Result ForceUnlockRecord(ExecParam ExecParam, string TableName, DateTime executionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_ForceUnlockRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_ForceUnlockRecord]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TableName"].Size = 200;
                SqlComm.Parameters["@TableName"].Value = TableName;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Id"].Value = ExecParam.Typed.Record_Id;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutionTime", System.Data.SqlDbType.DateTime));
                SqlComm.Parameters["@ExecutionTime"].Value = executionTime;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastZarolo_id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@LastZarolo_id"].Direction = ParameterDirection.Output;

                SqlComm.ExecuteNonQuery();

                _ret.Uid = SqlComm.Parameters["@LastZarolo_id"].Value.ToString();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;
    }

}
