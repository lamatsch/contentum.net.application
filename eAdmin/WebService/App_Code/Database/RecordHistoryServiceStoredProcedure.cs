using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using System.Xml;
using Contentum.eUtility;

/// <summary>
/// Summary description for RecordHistoryServiceStoredProcedure
/// </summary>
public class RecordHistoryServiceStoredProcedure
{
    //private SqlConnection Connection;
    //private String ConnectionString;

    private DataContext dataContext;

    public RecordHistoryServiceStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);
        //try
        //{
        //    Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //    throw e;
        //}
	}

    public Result GetAllByRecordOLD(ExecParam ExecParam, String TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_HistoryGetAllByRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_HistoryGetAllByRecord]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@TableName"].Size = 100;
                SqlComm.Parameters["@TableName"].Value = TableName;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Record_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Record_Id"].Value = ExecParam.Typed.Record_Id;

                //DataSet ds = new DataSet();
                //string xmlstring = (string)SqlComm.ExecuteScalar();

                //if (!String.IsNullOrEmpty(xmlstring))
                //{
                //    XmlDocument doc = new XmlDocument();
                //    doc.LoadXml(xmlstring);
                //    byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(doc.OuterXml);
                //    System.IO.MemoryStream ms = new System.IO.MemoryStream(buf);
                //    ds.ReadXml(ms, XmlReadMode.InferSchema);
                //    ms.Close();
                //    _ret.Ds = ds;
                //}
                //else
                //{
                //    _ret.Ds = null;
                //}


                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;


            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result GetAllByRecord(ExecParam ExecParam, String TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_" + TableName + "HistoryGetRecord");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_" + TableName + "HistoryGetRecord]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TableName", System.Data.SqlDbType.NVarChar));
                //SqlComm.Parameters["@TableName"].Size = 100;
                //SqlComm.Parameters["@TableName"].Value = TableName;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RecordId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@RecordId"].Value = ExecParam.Typed.Record_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;


            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
