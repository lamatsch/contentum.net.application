using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using System.Xml;
using Contentum.eUtility;

/// <summary>
/// Summary description for RightsServiceStoredProcedure
/// </summary>
public class RightsServiceStoredProcedure
{
    //private SqlConnection Connection;

    //private String ConnectionString;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_RightsInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_RightsUpdateParameters = null;

    public RightsServiceStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);

        //try
        //{
        //    //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //    throw e;
        //}
	}

    public Result AddScopeIdToJogtargy(ExecParam execParam, string JogtargyId, string JogtargyTablaNev, string JogtargySzuloId, char CsoportOroklesMod, string CsoportId,char OroklottJogszint)
    {
        #region "Init"
        Result _ret = new Result();
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.

        //Record.Base.Updated.LetrehozasIdo = true;
        //Record.Base.Updated.Letrehozo_id = true;
        //Record.Base.LetrehozasIdo = ExecutionTime.ToString();
        //Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
        #endregion

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_AddScopeIdToJogtargy");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();


            SqlCommand SqlComm = new SqlCommand("[sp_AddScopeIdToJogtargy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

            SqlComm.Parameters.Add(new SqlParameter("@JogtargyTablaNev", System.Data.SqlDbType.VarChar));
            SqlComm.Parameters["@JogtargyTablaNev"].Size = 200;
            SqlComm.Parameters["@JogtargyTablaNev"].Value = JogtargyTablaNev;

            if (!string.IsNullOrEmpty(JogtargySzuloId))
            {
                SqlComm.Parameters.Add(new SqlParameter("@JogtargySzuloId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@JogtargySzuloId"].Value = SqlGuid.Parse(JogtargySzuloId);
            }
            else
            {
                SqlComm.Parameters.Add(new SqlParameter("@JogtargySzuloId", null));
            }

            SqlComm.Parameters.Add(new SqlParameter("@CsoportOroklesMod", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@CsoportOroklesMod"].Size = 1;
            SqlComm.Parameters["@CsoportOroklesMod"].Value = CsoportOroklesMod;

            if (!string.IsNullOrEmpty(CsoportId))
            {
                SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);
            }
            else
            {
                SqlComm.Parameters.Add(new SqlParameter("@CsoportId", null));
            }

            if (!string.IsNullOrEmpty(OroklottJogszint.ToString()))
            {
                SqlComm.Parameters.Add(new SqlParameter("@OroklottJogszint", SqlDbType.Char));
                SqlComm.Parameters["@OroklottJogszint"].Size = 1;
                SqlComm.Parameters["@OroklottJogszint"].Value = OroklottJogszint;
            }

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(execParam.Felhasznalo_Id);


            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            //}
            //sqlConnection.Close();
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result AddCsoportToJogtargy(ExecParam execParam, string JogtargyId, string CsoportId,char Kezi, char Jogszint, char Tipus)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_AddCsoportToJogtargy");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_AddCsoportToJogtargy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);
            SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);
            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(execParam.Felhasznalo_Id);
            
            SqlComm.Parameters.Add(new SqlParameter("@Kezi", SqlDbType.Char));
            SqlComm.Parameters["@Kezi"].Size = 1;
            SqlComm.Parameters["@Kezi"].Value = Kezi;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            SqlComm.Parameters.Add(new SqlParameter("@Tipus", SqlDbType.Char));
            SqlComm.Parameters["@Tipus"].Size = 1;
            SqlComm.Parameters["@Tipus"].Value = Tipus;


            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
        //    sqlConnection.Close();
        //}        

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result ChangeJogtargyRightInherit(ExecParam execParam, string JogtargyId, string SzuloJogtargyId, char UjOroklesTipus, char OroklesMasolas)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_ChangeJogtargyRightInherit");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_ChangeJogtargyRightInherit]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

            SqlComm.Parameters.Add(new SqlParameter("@SzuloJogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@SzuloJogtargyId"].Value = SqlGuid.Parse(SzuloJogtargyId);

            SqlComm.Parameters.Add(new SqlParameter("@UjOroklesTipus", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@UjOroklesTipus"].Size = 1;
            SqlComm.Parameters["@UjOroklesTipus"].Value = UjOroklesTipus;

            SqlComm.Parameters.Add(new SqlParameter("@OroklesMasolas", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@OroklesMasolas"].Size = 1;
            SqlComm.Parameters["@OroklesMasolas"].Value = OroklesMasolas;


            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
        //        sqlConnection.Close();
        //}        

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetRightsByJogtargyIdAndCsoportId(ExecParam execParam, string JogtargyId, string CsoportId, char Jogszint)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetRightsByJogtargyIdAndCsoportId");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_GetRightsByJogtargyIdAndCsoportId]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

            SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint",SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            try
            {
                _ret.Record = SqlComm.ExecuteScalar();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
        //    sqlConnection.Close();
        //}
        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetRightsByJogtargyIdAndCsoportId(ExecParam execParam, string JogtargyId, string CsoportId, char Jogszint, char Tipus)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetRightsByJogtargyIdAndCsoportId");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_GetRightsByJogtargyIdAndCsoportId]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;


        SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

        SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

        SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
        SqlComm.Parameters["@Jogszint"].Size = 1;
        SqlComm.Parameters["@Jogszint"].Value = Jogszint;

        SqlComm.Parameters.Add(new SqlParameter("@Tipus", SqlDbType.Char));
        SqlComm.Parameters["@Tipus"].Size = 1;
        SqlComm.Parameters["@Tipus"].Value = Tipus;


        try
        {
            _ret.Record = SqlComm.ExecuteScalar();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        //    sqlConnection.Close();
        //}
        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result RemoveCsoportFromJogtargy(ExecParam execParam, string JogtargyId, string CsoportId, char Kezi, char Tipus)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_RemoveCsoportFromJogtargy");
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_RemoveCsoportFromJogtargy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

            SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

            SqlComm.Parameters.Add(new SqlParameter("@Kezi", SqlDbType.Char));
            SqlComm.Parameters["@Kezi"].Size = 1;
            SqlComm.Parameters["@Kezi"].Value = Kezi;

            SqlComm.Parameters.Add(new SqlParameter("@Tipus", SqlDbType.Char));
            SqlComm.Parameters["@Tipus"].Size = 1;
            SqlComm.Parameters["@Tipus"].Value = Tipus;

            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            //}
            //sqlConnection.Close();
        }
        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAllRightedCsoportByJogtargy(ExecParam execParam, string JogtargyId, RowRightsCsoportSearch where)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetAllRightedCsoportByJogtargy");
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_GetAllRightedCsoportByJogtargy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

            Query query = new Query();
            query.BuildFromBusinessDocument(where);
            query.Where += where.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, where.OrderBy, where.TopRow);

            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            //}
            //sqlConnection.Close();
        }
        
        log.SpEnd(execParam, _ret);
        return _ret;

    }

    public Result RemoveCsoportFromJogtargyById(ExecParam execParam)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_RemoveCsoportFromJogtargyById");
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            SqlCommand SqlComm = new SqlCommand("[sp_RemoveCsoportFromJogtargyById]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = SqlGuid.Parse(execParam.Record_Id.ToString());

            try
            {
                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            //}
            //sqlConnection.Close();
        }
        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result IsUserMember(ExecParam ExecParam, string CsoportId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_CsoportTagokIsMember");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_CsoportTagokIsMember]");


        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

        SqlComm.Parameters.Add(new SqlParameter("@UserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@UserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Record = ds.Tables[0].Rows[0].ItemArray[0].ToString() == "1" ? true : false;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result GetAllRightedSubCsoportByJogtargy(ExecParam execParam, string JogtargyId, KRT_CsoportokSearch search)
    {
        Logger.DebugStart();
        Logger.Debug("GetAllRightedSubCsoportByJogtargy t�rolt elj�r�s elindul");
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetAllRightedSubCsoportByJogtargy");

        SqlCommand SqlComm = new SqlCommand("[sp_GetAllRightedSubCsoportByJogtargy]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;


        SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);

        Query query = new Query();
        query.BuildFromBusinessDocument(search);
        query.Where += search.WhereByManual;

        Logger.Debug(String.Format("Param�terek: where: {0}, orderby: {1}, toprow: {2}",query.Where,search.OrderBy,search.TopRow));

        Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, search.OrderBy, search.TopRow);

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(String.Format("GetAllRightedSubCsoportByJogtargy t�rolt elj�r�s hiba: {0},{1}",e.ErrorCode.ToString(),e.Message));
        }

        Logger.Debug("GetAllRightedSubCsoportByJogtargy t�rolt elj�r�s v�ge");
        Logger.DebugEnd();
        log.SpEnd(execParam, _ret);
        return _ret;

    }

    public Result AddCsoportToJogtargyTomeges(ExecParam execParam, string[] jogtargyIds, string[] csoportIds)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_AddCsoportToJogtargyTomeges");

        SqlCommand SqlComm = new SqlCommand("[sp_AddCsoportToJogtargyTomeges]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@JogtargyIds", SqlDbType.NVarChar));
        SqlComm.Parameters["@JogtargyIds"].Value = String.Join(",", jogtargyIds);
        SqlComm.Parameters.Add(new SqlParameter("@CsoportIds", SqlDbType.NVarChar));
        SqlComm.Parameters["@CsoportIds"].Value = String.Join(",", csoportIds);
        SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(execParam.Felhasznalo_Id);


        try
        {
            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }   

        log.SpEnd(execParam, _ret);
        return _ret;
    }

}
