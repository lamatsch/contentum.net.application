using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eAdmin.Service;
using System.Web.Script.Services;


[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraElosztoivekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraElosztoivekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraElosztoivekSearch _EREC_IraElosztoivekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraElosztoivekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetElosztoivekListForAutoComplete(string prefixText, int count, string contextKey)
    {
        if (string.IsNullOrEmpty(prefixText))
            return null;

        string[] parameters = contextKey.Split(';');

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = parameters[0];

        EREC_IraElosztoivekSearch search = new EREC_IraElosztoivekSearch();
        search.OrderBy = "EREC_IraElosztoivek.NEV ASC, EREC_IraElosztoivek.Id";
        
        search.NEV.Value = prefixText + "%";
        search.NEV.Operator = Contentum.eQuery.Query.Operators.like;

        search.TopRow = count;


        Result res = GetAllWithExtension(execParam, search);

        if (!string.IsNullOrEmpty(res.ErrorCode))
            return null;

        return ElosztoivekDataSourceToStringPairArray(res);
    }

    private string[] ElosztoivekDataSourceToStringPairArray(Result res)
    {
        string elosztoivForras = "E";
        string forrasDelimeter = ":   ";

        if (res.Ds != null)
        {
            string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
            string value = "";
            string text = "";
            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                value = res.Ds.Tables[0].Rows[i]["Id"].ToString();

                // eloszt��v "forr�s" hozz�ad�s
                text = elosztoivForras + forrasDelimeter;

                text += res.Ds.Tables[0].Rows[i]["NEV"].ToString();

                // kommentezve (a formon a fajt�t a c�m mez�be sz�rja be
                //text += Contentum.eUtility.Constants.AutoComplete.delimeter + res.Ds.Tables[0].Rows[i]["Fajta_Nev"].ToString();

                string item = Utility.CreateAutoCompleteItem(text, value);
                StringArray.SetValue(item, i);
            }
            return StringArray;
        }
        else
        {
            return null;
        }
    }

}