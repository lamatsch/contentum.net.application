using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.Net.Mail;
using EmailTemplateParser;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eUtility;
using System.IO;

using log4net;
using log4net.Config;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Data;

/// <summary>
/// Summary description for EmailService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class EmailService : System.Web.Services.WebService
{

    private DataContext dataContext;
    private static readonly ILog ilog = LogManager.GetLogger(typeof(Logger));

    public EmailService()
    {
        dataContext = new DataContext(this.Application);

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public EmailService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    private bool SendTemplatedEmail(ExecParam ExecParam, String MessageFromAddress, String[] MessageToAddress, Parser emailparser)
    {
        return SendTemplatedEmail(ExecParam, MessageFromAddress, MessageToAddress, "Registration", emailparser, null);
    }

    private bool SendTemplatedEmail(ExecParam ExecParam, String MessageFromAddress, String[] MessageToAddress, String Subject, Parser emailparser)
    {
        return SendTemplatedEmail(ExecParam, MessageFromAddress, MessageToAddress, Subject, emailparser, null);
    }


    private bool SendTemplatedEmail(ExecParam ExecParam, String MessageFromAddress, String[] MessageToAddress, String Subject, Parser emailparser, string[] MessageCCAddresses)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        try
        {
            if (String.IsNullOrEmpty(MessageFromAddress))
            {
                throw new ResultException("MessageFromAddress is null or empty");
            }
            MailMessage message = new MailMessage();
            message.From = new MailAddress(MessageFromAddress);
            foreach (String toaddress in MessageToAddress)
            {
                if (!String.IsNullOrEmpty(toaddress))
                    message.To.Add(new MailAddress(toaddress));
            }
            message.Subject = Subject;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = emailparser.Parse();
            message.IsBodyHtml = true;

            if (MessageCCAddresses != null)
            {
                foreach (string ccAddress in MessageCCAddresses)
                {
                    if (!String.IsNullOrEmpty(ccAddress))
                    {
                        message.CC.Add(ccAddress);
                    }
                }
            }

            SmtpClient client = GetSmtpClient(ExecParam);
            client.Send(message);
        }
        catch (Exception ex)
        {
            Result res = ResultException.GetResultFromException(ex);
            log.WsEnd(ExecParam, res);
            return false;
        }

        log.WsEnd(ExecParam);
        return true;
    }

    private Result SendEmailFromeMailBoritek(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritekok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result res = new Result();

        try
        {
            if (String.IsNullOrEmpty(erec_eMailBoritekok.Felado))
            {
                res.ErrorCode = "MessageFromAddress is null or empty";
                res.ErrorMessage = "MessageFromAddress is null or empty";
                log.WsEnd(ExecParam, res);
                return res;
            }

            if (String.IsNullOrEmpty(erec_eMailBoritekok.Cimzett))
            {
                res.ErrorCode = "MessageToAddress is null or empty";
                res.ErrorMessage = "MessageToAddress is null or empty";
                log.WsEnd(ExecParam, res);
                return res;
            }
            MailMessage message = new MailMessage();
            message.From = new MailAddress(erec_eMailBoritekok.Felado);
            message.To.Add(new MailAddress(erec_eMailBoritekok.Cimzett));

            message.Subject = erec_eMailBoritekok.Targy;
            message.BodyEncoding = System.Text.Encoding.UTF8;

            message.Body = erec_eMailBoritekok.Uzenet;
            message.IsBodyHtml = true;

            SmtpClient client = GetSmtpClient(ExecParam);
            client.Send(message);
        }
        catch (Exception ex)
        {
            res.ErrorCode = ex.Message;
            res.ErrorMessage = ex.Message;
            Exception e = ex;

            while (e.InnerException != null)
            {
                e = e.InnerException;
                res.ErrorMessage += "; " + e.Message;
            }

            log.WsEnd(ExecParam, res);
            return res;
        }

        res.Record = erec_eMailBoritekok;
        log.WsEnd(ExecParam, res);
        return res;
    }
    /*    [WebMethod]
        public bool Test()
        {
            ExecParam[] ExecParams = new ExecParam[2];
            ExecParams[0] = new ExecParam();
            ExecParams[0].Felhasznalo_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";
            ExecParams[0].Record_Id = "A29BECFA-B48E-42BA-9F07-69BC74752734";
            ExecParams[1] = new ExecParam();
            ExecParams[1].Felhasznalo_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";
            ExecParams[1].Record_Id = "C47A7D78-B797-4D96-8D91-55499C6669D6";

            KRT_Felhasznalok[] KRT_Felhasznalok = new KRT_Felhasznalok[1];
            KRT_FelhasznalokService KRT_FelhasznalokService = new KRT_FelhasznalokService();

            ExecParam ExecParam = new ExecParam();
            // teszt email sajat magunknak: ide egy for ciklus kell az osszes cimzett eloszedesehez
            ExecParam.Felhasznalo_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";
            ExecParam.Record_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";
            Result _res = KRT_FelhasznalokService.Get(ExecParam);

            KRT_Felhasznalok[0] = (KRT_Felhasznalok)_res.Record;

            return SendObjectsLinkToEmail(ExecParams, KRT_Felhasznalok, "KRT_Szerepkorok");
        }*/

    [WebMethod]
    public bool SendObjectsLinkToEmail(ExecParam[] ExecParams, KRT_Felhasznalok[] KRT_Felhasznalok, String Message, String TableName, String QueryString)
    {
        KRT_FelhasznalokService service = new KRT_FelhasznalokService();

        // hupsz: ide valami superuser kene, aki minden rekorodot tud olvasni!!!
        ExecParam ExecParam_KRT_Felhasznalok = new ExecParam();
        ExecParam_KRT_Felhasznalok.Felhasznalo_Id = ExecParams[0].Felhasznalo_Id;
        ExecParam_KRT_Felhasznalok.Record_Id = ExecParams[0].Felhasznalo_Id;

        Result _res = service.Get(ExecParam_KRT_Felhasznalok);
        KRT_Felhasznalok Executer_KRT_Felhasznalok = (_res.Record as KRT_Felhasznalok);

        // Felado email cime:
        String MessageFrom = Executer_KRT_Felhasznalok.EMail;

        string EmailTemplateName = "SendObjectsLink";
        Parser emailparser = null;
        try
        {
            emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm"));
        }
        catch
        {
            return false;
        }

        try
        {
            Hashtable templateVars = new Hashtable();

            templateVars.Add("FelhasznaloNev", Executer_KRT_Felhasznalok.Nev);

            // convert normal line breaks to HTML line breaks
            Message = Message.Replace("\r\n", "<br />").Replace("\r", "<br />").Replace("\n", "<br />");

            templateVars.Add("Megjegyzes", Message);

            string link = "";

            String RecordsId = "";
            for (int x = 0; x < ExecParams.Length; x++)
            {
                RecordsId += ExecParams[x].Record_Id + ";";
            }
            RecordsId = RecordsId.Substring(0, RecordsId.Length - 1);

            if (!String.IsNullOrEmpty(QueryString))
            {
                if (QueryString.StartsWith("?") || QueryString.StartsWith("&"))
                {
                    QueryString = QueryString.Substring(1);
                }
                if (!QueryString.EndsWith("&"))
                {
                    QueryString += "&";
                }
            }


            List<string> KRT_in_eRecord_Tables = new List<string>(new string[] { "KRT_BarkodSavok", "KRT_Barkodok", "KRT_Mappak", "KRT_Dokumentumok" });

            string _prefix = TableName.Substring(0, 3);
            switch (_prefix)
            {
                case "KRT":
                    if (KRT_in_eRecord_Tables.Contains(TableName))
                    {
                        goto case "ERE";
                    }
                    else
                    {
                        link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eAdminWebSiteUrl") + TableName.Substring(TableName.IndexOf('_') + 1) + "List.aspx?" + QueryString + "Id=" + RecordsId + "'>Itt tekintheti meg...</a>";
                    }
                    break;
                case "ERE":
                case "HKP":
                    if (TableName == "EREC_HataridosFeladatok")
                    {
                        link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eRecordWebSiteUrl") + "Feladatok.aspx?" + QueryString + QueryStringVars.Id + "=" + RecordsId + "&" + QueryStringVars.SelectedTab + "=TabPanelFeldataim'>Itt tekintheti meg...</a>";
                    }
                    else
                    {
                        link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eRecordWebSiteUrl") + TableName.Substring(TableName.IndexOf('_') + 1) + "List.aspx?" + QueryString + "Id=" + RecordsId + "'>Itt tekintheti meg...</a>";
                    }
                    break;
                case "MIG":
                    link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eMigrationWebSiteUrl") + TableName.Substring(TableName.IndexOf('_') + 1) + "List.aspx?" + QueryString + "Id=" + RecordsId + "'>Itt tekintheti meg...</a>";
                    break;
            }
            templateVars.Add("Link", link);

            emailparser.Variables = templateVars;

            String[] FelhasznalokEmail = new String[KRT_Felhasznalok.Length];
            Contentum.eAdmin.Service.RightsService service_jog = eAdminService.ServiceFactory.getRightsService();


            #region Email c�mek el�k�sz�t�se �tad�sra
            for (int i = 0; i < KRT_Felhasznalok.Length; i++)
            {
                if (!String.IsNullOrEmpty(KRT_Felhasznalok[i].EMail))
                {
                    FelhasznalokEmail[i] = KRT_Felhasznalok[i].EMail;
                    #region Jogosults�g hozz�rendel�se az adott c�mzetthez a kijel�lt iratokra.

                    for (int j = 0; j < ExecParams.Length; j++)
                    {
                        Result result = service_jog.AddCsoportToJogtargyWithJogszint(ExecParam_KRT_Felhasznalok, ExecParams[j].Record_Id.ToString(), KRT_Felhasznalok[i].Id.ToString(), 'I', 'O', '0');
                        if (result.IsError)
                            Logger.Error("SendObjectsLinkToEmail", ExecParam_KRT_Felhasznalok, result);
                    }
                    #endregion

                }
            }
            #endregion

            #region azonositok lekerese
            Logger.Debug("Azonositok lekerese start");
            KRT_ObjTipusokService svc = new KRT_ObjTipusokService();
            Result res = svc.GetAzonositoTomeges(ExecParams, TableName);
            if (res.IsError)
            {
                throw new ResultException(res);
            }
            StringBuilder sb = new StringBuilder(res.Ds.Tables[0].Rows.Count);
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string azonosito = row["Azonosito"].ToString();
                if (!String.IsNullOrEmpty(azonosito))
                {
                    if (sb.Length > 0)
                        sb.Append("; ");
                    sb.Append(azonosito);
                }
            }
            Logger.Debug("Azonositok lekerese end");
            #endregion

            string objektumTipusNev = ObjektumTipusok.GetDisplayableNameFromObjectType(TableName);
            string azonostio = sb.ToString();
            if (!String.IsNullOrEmpty(objektumTipusNev))
                azonostio = objektumTipusNev + ": " + azonostio;

            templateVars.Add("Azonosito", azonostio);

            SendTemplatedEmail(ExecParams[0], MessageFrom, FelhasznalokEmail, "[Contentum] Hivatkoz�s k�ld�s: " + azonostio, emailparser);
        }
        catch
        {
            return false;
        }
        return true;
    }

    [WebMethod]
    public bool BreakLockNotifyEmail(ExecParam ExecParam, String NotifiedId, String TableName, String QueryString, String RecordsId)
    {
        KRT_FelhasznalokService service = new KRT_FelhasznalokService();
        // hupsz: ide valami superuser kene, aki minden rekorodot tud olvasni!!!
        ExecParam.Record_Id = ExecParam.Felhasznalo_Id;

        Result _res = service.Get(ExecParam);
        KRT_Felhasznalok Executer_KRT_Felhasznalok = (_res.Record as KRT_Felhasznalok);

        // Felado email cime:
        string MessageFrom = Executer_KRT_Felhasznalok.EMail;

        ExecParam.Record_Id = NotifiedId;
        _res = service.Get(ExecParam);
        KRT_Felhasznalok Notified_KRT_Felhasznalok = (_res.Record as KRT_Felhasznalok);

        string EmailTemplateName = "BreakLock";
        //string ss = Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm");
        Parser emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm"));

        Hashtable templateVars = new Hashtable();
        templateVars.Add("UserFrom", Executer_KRT_Felhasznalok.Nev);
        templateVars.Add("NotifiedUser", Notified_KRT_Felhasznalok.Nev);

        string link = String.Empty;
        /*switch (TableName)
        { 
            case "KRT_Szerepkorok":
                link = "<a href='http://localhost:100/eAdmin/SzerepkorokList.aspx?Id=" + RecordsId +"'>Felt�rt recordok</a>";
                break;
        }*/
        if (!String.IsNullOrEmpty(QueryString))
        {
            if (QueryString.StartsWith("?") || QueryString.StartsWith("&"))
            {
                QueryString = QueryString.Substring(1);
            }
            if (!QueryString.EndsWith("&"))
            {
                QueryString += "&";
            }
        }

        string _prefix = TableName.Substring(0, 3);
        switch (_prefix)
        {
            case "KRT":
                // eAdmin-ban kell ezeket megnyitni, kiv�ve: KRT_Dokumentumok
                if (TableName == "KRT_Dokumentumok")
                {
                    goto case "ERE";
                }
                else
                {
                    link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eAdminWebSiteUrl") + TableName.Substring(TableName.IndexOf('_') + 1) + "List.aspx?" + QueryString + "Id=" + RecordsId + "'>Itt tekintheti meg...</a>";
                }
                break;
            case "ERE":
                link = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eRecordWebSiteUrl") + TableName.Substring(TableName.IndexOf('_') + 1) + "List.aspx?" + QueryString + "Id=" + RecordsId + "'>Itt tekintheti meg...</a>";
                break;
        }

        templateVars.Add("Link", link);

        emailparser.Variables = templateVars;

        String[] FelhasznalokEmail = new String[1];
        if (!String.IsNullOrEmpty(Notified_KRT_Felhasznalok.EMail))
        {
            FelhasznalokEmail[0] = Notified_KRT_Felhasznalok.EMail;
        }

        string emailSubject = "[Contentum] �rtes�t�s z�rol�s felt�r�s�r�l...";

        // itt meg a Notified_KRT_Felhasznalok.Email-t bekell tenni
        return SendTemplatedEmail(ExecParam, MessageFrom, FelhasznalokEmail, emailSubject, emailparser);
    }

    [WebMethod]
    public Result SendAnswerDeclinedEmail(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritekok, String FullErkeztetoSzam, String MegtagadasIndoka)
    {
        log4net.Config.XmlConfigurator.Configure();
        ilog.Debug("SendEmailDeclinedEmail(execParam, erec_eMailBoritekok, '" + FullErkeztetoSzam.ToString() + "')");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        string Subject = "";
        Result result = new Result();

        EREC_eMailBoritekok erec_eMailBoritekokOut = new EREC_eMailBoritekok();

        KRT_FelhasznalokService service = new KRT_FelhasznalokService();
        ExecParam.Record_Id = ExecParam.Felhasznalo_Id;

        Result _res = service.Get(ExecParam);
        KRT_Felhasznalok Executer_KRT_Felhasznalok = (_res.Record as KRT_Felhasznalok);

        // Uj felado email cime:
        erec_eMailBoritekokOut.Felado = Executer_KRT_Felhasznalok.EMail;

        // Uj cimzett az eredeti felado:
        erec_eMailBoritekokOut.Cimzett = erec_eMailBoritekok.Felado;

        string EmailTemplateName = "DeclinedEmail";
        //string ss = Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm");
        Parser emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm"));

        Hashtable templateVars = new Hashtable();
        templateVars.Add("FelhasznaloNev", Executer_KRT_Felhasznalok.Nev);
        templateVars.Add("FelhasznaloEmail", Executer_KRT_Felhasznalok.EMail);
        if (erec_eMailBoritekok != null)
        {
            templateVars.Add("Felado", erec_eMailBoritekok.Felado);
            templateVars.Add("FeladasDatuma", erec_eMailBoritekok.FeladasDatuma);
            templateVars.Add("ErkezesDatuma", erec_eMailBoritekok.ErkezesDatuma);
            templateVars.Add("Targy", erec_eMailBoritekok.Targy);
            templateVars.Add("MegtagadasIndoka", MegtagadasIndoka);
            templateVars.Add("Cimzett", erec_eMailBoritekok.Cimzett);

            if (!String.IsNullOrEmpty(erec_eMailBoritekok.Targy))
            {
                Subject += "Re: " + erec_eMailBoritekok.Targy + " - Iktat�s megtagadva";
            }
            else
            {
                Subject = "Az �n k�ldem�nye - Iktat�s megtagadva";
            }

        }
        else
        {

            result.ErrorCode = "eMailBoritek object cannot be found.";
            result.ErrorMessage = "eMailBoritek object cannot be found.";
            log.WsEnd(ExecParam, result);
            return result;
        }

        templateVars.Add("FullErkeztetoSzam", FullErkeztetoSzam ?? "");

        //templateVars.Add("Uzenet", erec_eMailBoritekok.Uzenet);

        emailparser.Variables = templateVars;

        erec_eMailBoritekokOut.Uzenet = emailparser.Parse();

        erec_eMailBoritekokOut.Targy = Subject;

        result = SendEmailFromeMailBoritek(ExecParam, erec_eMailBoritekokOut);
        log.WsEnd(ExecParam, result);
        return result;

    }


    [WebMethod]
    public bool SendErrorInMail(ExecParam ExecParam, string NotifiedAddress, string Url, string ErrorCode, string ErrorMessage, string LogReference, string Description)
    {
        KRT_FelhasznalokService service = new KRT_FelhasznalokService();
        ExecParam.Record_Id = ExecParam.Felhasznalo_Id;

        Result res = service.Get(ExecParam);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            return false;
        }

        KRT_Felhasznalok felhasznalo = (res.Record as KRT_Felhasznalok);

        // Felad� e-mail c�me:
        String MessageFrom = felhasznalo.EMail;

        //A hib�t �szlel� felhaszn�l� neve
        String UserName = felhasznalo.UserNev;
        string Name = felhasznalo.Nev;

        string EmailTemplateName = "ErrorMessage";
        Parser emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm"));

        //templ�t v�ltoz�inak felt�lt�se
        Hashtable templateVars = new Hashtable();

        if (!String.IsNullOrEmpty(LogReference))
        {
            LogReference = "<a href='" + Contentum.eUtility.UI.GetAppSetting("eAdminWebSiteUrl") + "AuditLogList.aspx?" + Contentum.eUtility.Constants.PageStartDate + "=" + LogReference + "'>Itt tekinthet� meg...</a>";
        }
        else
        {
            LogReference = "A hib�hoz nem tartozik logbejegyz�s.";
        }

        templateVars.Add("Name", Name);
        templateVars.Add("UserName", UserName);
        templateVars.Add("Url", Url);
        templateVars.Add("ErrorCode", ErrorCode);
        templateVars.Add("ErrorMessage", ErrorMessage);
        templateVars.Add("LogReference", LogReference);
        templateVars.Add("Description", Description);

        emailparser.Variables = templateVars;

        //C�l e-mail c�m
        String[] MessageToAddress = { NotifiedAddress };

        string subject = "Hibabejelent�s";

        return SendTemplatedEmail(ExecParam, MessageFrom, MessageToAddress, subject, emailparser);
    }

    [WebMethod]
    public bool SendDocReady(ExecParam ExecParam, string NotifiedAddress, string Url)
    {

        // CR3197 felad� emailc�me be volt "dr�tozva"
        // String MessageFrom = "noreply@budapest.hu";
        String MessageFrom = Rendszerparameterek.Get(ExecParam, "RENDSZERFELUGYELET_EMAIL_CIM");


        string EmailTemplateName = "DocReady";
        Parser emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + EmailTemplateName + ".htm"));

        //templ�t v�ltoz�inak felt�lt�se
        Hashtable templateVars = new Hashtable();

        templateVars.Add("Url", Url);

        emailparser.Variables = templateVars;

        //C�l e-mail c�m
        String[] MessageToAddress = { NotifiedAddress };

        string subject = "Dokumentum elk�sz�lt";

        return SendTemplatedEmail(ExecParam, MessageFrom, MessageToAddress, subject, emailparser);
    }

    [WebMethod]
    public bool SendAnswerEmail(ExecParam execParam, string kuldemeny_id, string ugyirat_id)
    {
        log4net.Config.XmlConfigurator.Configure();
        ilog.Debug("SendAnswerEmail(execParam, '" + kuldemeny_id + "', '" + ugyirat_id + "')");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool resultSuccess = false;

        try
        {
            EmailServiceStoredProcedure sp = new EmailServiceStoredProcedure(this.dataContext);
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAnswerEmail(execParam, kuldemeny_id, ugyirat_id);
            ilog.Debug("sp.GetAnswerEmail(execParam, '" + kuldemeny_id + "', '" + ugyirat_id + "')");
            if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count != 1)
            {
                ilog.Debug("nincs adat");
                throw new ResultException("nincs adat");
            }
            ilog.Debug("van adat");
            string[] to = new string[1];
            to[0] = result.Ds.Tables[0].Rows[0]["ToAddress"].ToString();
            string[] cc = new string[0];
            string[] bcc = new string[0];

            resultSuccess = SendEmail(execParam,
                "graf.laszlo@axis.hu",
                to,
                result.Ds.Tables[0].Rows[0]["Subject"].ToString(),
                cc,
                bcc,
                true,
                result.Ds.Tables[0].Rows[0]["Body"].ToString()
            );
        }
        catch (Exception ex)
        {
            ilog.Error("Hiba: " + ex.Message);
            Result res = new Result();
            res.ErrorCode = ex.Message;
            Exception e = ex;
            while (e.InnerException != null)
            {
                e = e.InnerException;
                res.ErrorMessage += e.Message + ";";
            }
            result = ResultException.GetResultFromException(e);
            //log.WsEnd(execParam, res);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return resultSuccess;
    }

    [WebMethod]
    public bool SendEmail(ExecParam execParam,
                          string from,
                          string[] to,
                          string subject,
                          string[] cc,
                          string[] bcc,
                          bool ishtml,
                          string body)
    {
        log4net.Config.XmlConfigurator.Configure();
        bool rv = false;
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        try
        {
            if (String.IsNullOrEmpty(from))
            {
                Result res = new Result();
                res.ErrorCode = "MessageFromAddress is null or empty";
                res.ErrorMessage = "MessageFromAddress is null or empty";
                log.WsEnd(execParam, res);
                return false;
            }

            SmtpClient client = GetSmtpClient(execParam);
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            foreach (String toaddress in to)
            {
                if (!String.IsNullOrEmpty(toaddress))
                    message.To.Add(new MailAddress(toaddress));
            }
            message.Subject = subject;
            if (cc != null)
            {
                foreach (String ccaddress in cc)
                {
                    if (!String.IsNullOrEmpty(ccaddress))
                        message.CC.Add(new MailAddress(ccaddress));
                }
            }
            if (bcc != null)
            {
                foreach (String bccaddress in bcc)
                {
                    if (!String.IsNullOrEmpty(bccaddress))
                        message.Bcc.Add(new MailAddress(bccaddress));
                }
            }
            message.IsBodyHtml = ishtml;
            message.BodyEncoding = System.Text.Encoding.UTF8;

            message.Body = body;//"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd\">\n<html>\n\t<head>\n\t\t<title></title>\n\t\t<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-2\">\n\t</head>\n\t<body>\n\t\t<b>Pr�ba</b> <font color=\"red\">����</font> <img src=\"http://localhost:100/eRecord/images/hu/fejlec/eadmin_top_middle.jpg\">\n\t</body>\n</html>\n";
            client.Send(message);
            rv = true;
        }
        catch (Exception ex)
        {
            Result res = new Result();
            res.ErrorCode = ex.Message;
            Exception e = ex;
            while (e.InnerException != null)
            {
                e = e.InnerException;
                res.ErrorMessage += e.Message + ";";
            }
            log.WsEnd(execParam, res);
            rv = false;
        }
        log.WsEnd(execParam);
        return rv;
    }

    private Parser GetTemplateParser(string templateName)
    {
        Parser emailparser = null;
        try
        {
            emailparser = new Parser(Server.MapPath("~/EmailTemplate/" + templateName + ".htm"));
        }
        catch
        {
            return null;
        }

        return emailparser;
    }

    public const string EmailSubjectPrefix = "[Contentum]";

    public static string GetFormattedEmailSubject(string subject)
    {
        return String.Format("{0} {1}", EmailSubjectPrefix, subject);
    }

    public enum FeladatadNotificationType
    {
        Uj,
        Lezart
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public bool SendFeladatNotificationEmail(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        bool _ret = true;

        try
        {
            Logger.Debug("Bemeneti parameterek ellenorzes");
            FeladatadNotificationType notificationType = FeladatadNotificationType.Uj;
            Logger.Debug("Feladat Allapot:" + erec_HataridosFeladatok.Allapot);
            switch (erec_HataridosFeladatok.Allapot)
            {
                case KodTarak.FELADAT_ALLAPOT.Uj:
                    Logger.Debug("Uj");
                    notificationType = FeladatadNotificationType.Uj;
                    break;
                case KodTarak.FELADAT_ALLAPOT.Lezart:
                    Logger.Debug("Lezart");
                    notificationType = FeladatadNotificationType.Lezart;
                    break;
                default:
                    Logger.Debug("Nem kell ertesites");
                    return true;
            }
            Arguments args = new Arguments();
            Logger.Debug("execParam.Felhasznalo_Id: " + execParam.Felhasznalo_Id ?? "NULL");
            args.Add(new Argument("K�ld� azonos�t�", execParam.Felhasznalo_Id, ArgumentTypes.Guid));
            if (notificationType == FeladatadNotificationType.Uj)
            {
                Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_Felelos: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos ?? "NULL");
                args.Add(new Argument("C�mzett azonos�t�", erec_HataridosFeladatok.Felhasznalo_Id_Felelos, ArgumentTypes.Guid));
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_kiado: " + erec_HataridosFeladatok.Felhasznalo_Id_Kiado ?? "NULL");
                args.Add(new Argument("C�mzett azonos�t�", erec_HataridosFeladatok.Felhasznalo_Id_Kiado, ArgumentTypes.Guid));
            }
            args.ValidateArguments();

            Logger.Debug("Felhasznalok lekerese start: " + execParam.Record_Id);
            KRT_FelhasznalokService svcFelhasznalok = new KRT_FelhasznalokService(this.dataContext);
            ExecParam xpmFelhasznalok = execParam.Clone();
            KRT_FelhasznalokSearch schFelhasznalok = new KRT_FelhasznalokSearch();
            List<string> listFelhasznalok = new List<string>();
            listFelhasznalok.Add(execParam.Felhasznalo_Id);
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Felhasznalo_Id_Felelos))
            {
                listFelhasznalok.Add(erec_HataridosFeladatok.Felhasznalo_Id_Felelos);
            }
            else
            {
            }
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Felhasznalo_Id_Kiado))
            {
                listFelhasznalok.Add(erec_HataridosFeladatok.Felhasznalo_Id_Kiado);
            }
            else
            {
                Logger.Warn("String.IsNullOrEmpty(erec_HataridosFeladatok.Felhasznalo_Id_kiado)");
            }
            schFelhasznalok.Id.Value = Search.GetSqlInnerString(listFelhasznalok.ToArray());
            schFelhasznalok.Id.Operator = Query.Operators.inner;
            Result resFelhasznalok = svcFelhasznalok.GetAll(xpmFelhasznalok, schFelhasznalok);
            if (resFelhasznalok.IsError)
            {
                Logger.Error("Felhasznalok lekerese hiba", xpmFelhasznalok, resFelhasznalok);
                throw new ResultException(resFelhasznalok);
            }
            Logger.Debug("Felhasznalok lekerese vege");
            string mailAddressSender = String.Empty;
            string mailAddressReceiver = String.Empty;
            string felhasznaloFelelosNev = String.Empty;
            string felhasznaloKiadoNev = String.Empty;
            foreach (DataRow row in resFelhasznalok.Ds.Tables[0].Rows)
            {
                if (String.Compare(row["Id"].ToString(), execParam.Felhasznalo_Id, true) == 0)
                {
                    mailAddressSender = row["EMail"].ToString();
                    Logger.Debug("mailAddressSender: " + mailAddressSender ?? "NULL");
                }
                if (String.Compare(row["Id"].ToString(), erec_HataridosFeladatok.Felhasznalo_Id_Felelos, true) == 0)
                {
                    if (notificationType == FeladatadNotificationType.Uj)
                    {
                        mailAddressReceiver = row["EMail"].ToString();
                        Logger.Debug("mailAddressReceiver: " + mailAddressReceiver ?? "NULL");
                    }
                    felhasznaloFelelosNev = row["Nev"].ToString();
                    Logger.Debug("felhasznaloFelelosNev: " + felhasznaloFelelosNev ?? "NULL");
                }
                if (String.Compare(row["Id"].ToString(), erec_HataridosFeladatok.Felhasznalo_Id_Kiado, true) == 0)
                {
                    if (notificationType == FeladatadNotificationType.Lezart)
                    {
                        mailAddressReceiver = row["EMail"].ToString();
                        Logger.Debug("mailAddressReceiver: " + mailAddressReceiver ?? "NULL");
                    }
                    felhasznaloKiadoNev = row["Nev"].ToString();
                    Logger.Debug("felhasznaloKiadoNev: " + felhasznaloKiadoNev ?? "NULL");
                }
            }

            if (String.IsNullOrEmpty(mailAddressSender))
            {
                Logger.Error("String.IsNullOrEmpty(mailAddressSender)");
                throw new ResultException("String.IsNullOrEmpty(mailAddressSender)");
            }

            if (String.IsNullOrEmpty(mailAddressReceiver))
            {
                Logger.Error("String.IsNullOrEmpty(mailAddressReceiver)");
                throw new ResultException("String.IsNullOrEmpty(mailAddressReceiver)");
            }

            Parser emailParser = null;

            if (notificationType == FeladatadNotificationType.Uj)
            {
                emailParser = this.GetTemplateParser("UjFeladatNotificationTemplate");
            }

            if (notificationType == FeladatadNotificationType.Lezart)
            {
                emailParser = this.GetTemplateParser("LezartFeladatNotificationTemplate");
            }

            if (emailParser == null)
            {
                Logger.Error("emailParser = null");
                throw new ResultException("emailParser = null");
            }

            Logger.Debug("Template valtozok hozzadasa start");
            Hashtable templateVars = new Hashtable();
            if (notificationType == FeladatadNotificationType.Uj)
            {
                templateVars.Add("Cimzett", felhasznaloFelelosNev);
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                templateVars.Add("Cimzett", felhasznaloKiadoNev);
            }
            string tipusNev = String.Empty;
            string funkcioKod = String.Empty;
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Funkcio_Id_Inditando))
            {
                Logger.Debug("Funkcio_Id_Inditando lekerese start");
                KRT_FunkciokService funkciokService = new KRT_FunkciokService(this.dataContext);
                ExecParam xpmFunkcio = execParam.Clone();
                xpmFunkcio.Record_Id = erec_HataridosFeladatok.Funkcio_Id_Inditando;
                Result resFunkcio = funkciokService.Get(xpmFunkcio);
                if (res.IsError)
                {
                    Logger.Error("Funkcio_Id_Inditando lekerese hiba", xpmFunkcio, resFunkcio);
                }
                else
                {
                    funkcioKod = ((KRT_Funkciok)resFunkcio.Record).Kod;
                    tipusNev = ((KRT_Funkciok)resFunkcio.Record).Nev;
                }
                Logger.Debug("Funkcio_Id_Inditando lekerese end");

            }
            else
            {
                tipusNev = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.FELADAT_ALTIPUS.kcsNev, erec_HataridosFeladatok.Altipus, execParam, HttpContext.Current.Cache);
            }
            templateVars.Add("FeladatTipus", tipusNev);
            templateVars.Add("FeladatLeiras", erec_HataridosFeladatok.Leiras);
            templateVars.Add("FeladatHatarido", erec_HataridosFeladatok.Typed.IntezkHatarido.IsNull ? String.Empty : erec_HataridosFeladatok.Typed.IntezkHatarido.Value.ToShortDateString());
            templateVars.Add("FeladatKiadas", erec_HataridosFeladatok.Base.Typed.LetrehozasIdo.Value.ToShortDateString());
            templateVars.Add("FeladatKiado", felhasznaloKiadoNev);
            templateVars.Add("FeladatFelelos", felhasznaloFelelosNev);
            string selectedTab = String.Empty;
            if (notificationType == FeladatadNotificationType.Uj)
            {
                selectedTab = "TabPanelFeldataim";
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                selectedTab = "TabPanelKiadott";
            }
            string FeladataimUrl = Contentum.eUtility.UI.GetAppSetting("eRecordWebSiteUrl") + "Feladatok.aspx?" + QueryStringVars.SelectedTab + "=" + selectedTab
                + "&" + QueryStringVars.Id + "=" + erec_HataridosFeladatok.Id;
            templateVars.Add("FeladataimUrl", FeladataimUrl);

            ResolveFeladatAzonostio(this.dataContext, execParam, erec_HataridosFeladatok);

            string azonosito = erec_HataridosFeladatok.Azonosito;

            if (!String.IsNullOrEmpty(azonosito))
            {
                templateVars.Add("Azonosito", azonosito);
                string azonositoUrl = GetObjektumUrl(erec_HataridosFeladatok, funkcioKod);
                if (!String.IsNullOrEmpty(azonositoUrl))
                {
                    templateVars.Add("AzonositoUrl", azonositoUrl);
                }
            }

            templateVars.Add("FeladatMegoldas", erec_HataridosFeladatok.Megoldas);
            bool isFeladatKezelesDisabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.FELADAT_KEZELES_DISABLED, false);
            templateVars.Add("IsFeladatKezelesEnabled", !isFeladatKezelesDisabled);

            emailParser.Variables = templateVars;
            Logger.Debug("Template valtozok hozzadasa end");

            Logger.Debug("SendTemplatedEmail start");
            #region feladat azonosito lekerese
            Logger.Debug("Feladat azonosito lekerese start");
            KRT_ObjTipusokService svc = new KRT_ObjTipusokService(this.dataContext);
            ExecParam xpm = execParam.Clone();
            xpm.Record_Id = erec_HataridosFeladatok.Id;
            Result resFeladatAzonosito = svc.GetAzonosito(xpm, erec_HataridosFeladatok.Id, Contentum.eUtility.Constants.TableNames.EREC_HataridosFeladatok);
            if (resFeladatAzonosito.IsError)
            {
                throw new ResultException(resFeladatAzonosito);
            }
            string feladatAzonosito = resFeladatAzonosito.Ds.Tables[0].Rows[0]["Azonosito"].ToString();
            Logger.Debug("Feladat lekerese end");
            #endregion
            _ret = SendTemplatedEmail(execParam, mailAddressSender, new string[] { mailAddressReceiver }, GetFormattedEmailSubject("Feladat �rtes�t�s") + ": " + feladatAzonosito, emailParser);
            if (!_ret)
            {
                Logger.Error("SendTemplatedEmail hiba");
                throw new ResultException("SendTemplatedEmail hiba");
            }
            Logger.Debug("SendTemplatedEmail end");
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendFeladatNotificationEmail hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }


    public static string GetHataridosErtesitesUrl(EREC_HataridosErtesitesek erec_HataridosErtesitesek, string funkcioKod)
    {
        string url = String.Empty;

        if (!String.IsNullOrEmpty(erec_HataridosErtesitesek.Obj_type))
        {
            url = ObjektumTipusok.GetFullListUrl(erec_HataridosErtesitesek.Obj_type, funkcioKod);
            if (!String.IsNullOrEmpty(url))
            {
                if (!String.IsNullOrEmpty(erec_HataridosErtesitesek.ObjektumSzukites))
                {
                    if (url.Contains("?"))
                        url += "&";
                    else
                        url += "?";

                    string base64string = Convert.ToBase64String(Encoding.UTF8.GetBytes(erec_HataridosErtesitesek.ObjektumSzukites));
                    url += QueryStringVars.WhereByManual + "=" + HttpContext.Current.Server.UrlEncode(base64string);
                }

            }
        }

        return url;
    }

    public static string GetObjektumUrl(EREC_HataridosFeladatok erec_HataridosFeladatok, string funcioKod)
    {
        string url = String.Empty;

        if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Obj_type) && !String.IsNullOrEmpty(erec_HataridosFeladatok.Obj_Id))
        {
            url = ObjektumTipusok.GetFullListUrl(erec_HataridosFeladatok.Obj_Id, erec_HataridosFeladatok.Obj_type, funcioKod);
        }

        return url;
    }

    public static void ResolveFeladatAzonostio(DataContext dataContext, ExecParam xpm, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        if (String.IsNullOrEmpty(erec_HataridosFeladatok.Azonosito) && !String.IsNullOrEmpty(erec_HataridosFeladatok.Obj_Id) && !String.IsNullOrEmpty(erec_HataridosFeladatok.Obj_type))
        {
            KRT_ObjTipusokService svcObjTipusok = new KRT_ObjTipusokService(dataContext);
            ExecParam xpmAzonosito = xpm.Clone();
            xpmAzonosito.Record_Id = erec_HataridosFeladatok.Obj_Id;
            Result resAzonosito = svcObjTipusok.GetAzonosito(xpmAzonosito, erec_HataridosFeladatok.Obj_Id, erec_HataridosFeladatok.Obj_type);
            if (resAzonosito.IsError)
            {
                throw new ResultException(resAzonosito);
            }
            if (resAzonosito.Ds.Tables[0].Rows.Count > 0)
            {
                erec_HataridosFeladatok.Azonosito = resAzonosito.Ds.Tables[0].Rows[0]["Azonosito"].ToString();
            }
        }
    }

    public enum ErtesitesType
    {
        HataridosErtesites,
        Esemenyertesites
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosErtesitesek))]
    public bool SendHataridosErtesitesEmailTomeges(ExecParam execParam, List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        bool _ret = true;

        foreach (EREC_HataridosErtesitesek erec_HataridosErtesitesek in erec_HataridosErtesitesekList)
        {
            _ret = this.SendHataridosErtesitesEmail(execParam, erec_HataridosErtesitesek);
        }

        log.WsEnd(execParam);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosErtesitesek))]
    public bool SendHataridosErtesitesEmail(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        bool _ret = true;

        try
        {
            #region job fix

            //job nem ad �t felhaszn�l� id-t, ez�rt a rendszerparam�tert nem tudjuk lek�rni
            if (String.IsNullOrEmpty(execParam.Felhasznalo_Id) && !String.IsNullOrEmpty(erec_HataridosErtesitesek.Felhasznalo_Id_Felelos))
            {
                execParam.Felhasznalo_Id = erec_HataridosErtesitesek.Felhasznalo_Id_Felelos;
            }

            #endregion

            ErtesitesType ertesitesType = (erec_HataridosErtesitesek.Funkcio_Futtatando == "EsemenyErtesites") ? ErtesitesType.Esemenyertesites : ErtesitesType.HataridosErtesites;

            EREC_HataridosFeladatok erec_HataridosFeladatok = (EREC_HataridosFeladatok)erec_HataridosErtesitesek;
            Logger.Debug("Bemeneti parameterek ellenorzes");
            Arguments args = new Arguments();
            Logger.Debug("execParam.Felhasznalo_Id: " + execParam.Felhasznalo_Id ?? "NULL");
            Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_Felelos: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos ?? "NULL");
            args.Add(new Argument("C�mzett azonos�t�", erec_HataridosFeladatok.Felhasznalo_Id_Felelos, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("Kuldo lekerese start");
            string mailAddressSender = Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);
            Logger.Debug("mailAddressSender: " + mailAddressSender ?? "NULL");
            if (String.IsNullOrEmpty(mailAddressSender))
            {
                Logger.Error("String.IsNullOrEmpty(mailAddressSender)");
                throw new ResultException("String.IsNullOrEmpty(mailAddressSender)");
            }
            Logger.Debug("Kuldo lekerese end");

            Logger.Debug("Felhasznalo_Id_Felelos lekerese start: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos);
            KRT_FelhasznalokService svcFelhasznalok = new KRT_FelhasznalokService(this.dataContext);
            ExecParam xpmFelhasznalo = execParam.Clone();
            xpmFelhasznalo.Record_Id = erec_HataridosFeladatok.Felhasznalo_Id_Felelos;
            Result resFelhasznalo = svcFelhasznalok.Get(xpmFelhasznalo);
            if (resFelhasznalo.IsError)
            {
                Logger.Error("Felhasznalo lekerese hiba", xpmFelhasznalo, resFelhasznalo);
                throw new ResultException(resFelhasznalo);
            }
            Logger.Debug("Felhasznalo lekerese vege");
            KRT_Felhasznalok FelelosFelhasznalo = (KRT_Felhasznalok)resFelhasznalo.Record;

            #region 323
            try
            {
                MessageService msgSvc = new MessageService();
                if (!msgSvc.FeladatErtesitesKuldheto(execParam, erec_HataridosFeladatok.FeladatDefinicio_Id, erec_HataridosFeladatok.Felhasznalo_Id_Felelos))
                {
                    Logger.Debug(string.Format("FeladatErtesites alapjan nem kell ertesites email. FELADATDEF:{0} FELADATFELELOS:{1}", erec_HataridosFeladatok.FeladatDefinicio_Id, erec_HataridosFeladatok.Felhasznalo_Id_Felelos));
                    return true;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("SendHataridosErtesitesEmail.FeladatErtesitesKuldheto hiba:", exc);
            }

            #endregion 323

            string mailAddressReceiver = FelelosFelhasznalo.EMail;
            Logger.Debug("mailAddressReceiver: " + mailAddressReceiver ?? "NULL");
            string felhasznaloFelelosNev = FelelosFelhasznalo.Nev;
            Logger.Debug("felhasznaloFelelosNev: " + felhasznaloFelelosNev ?? "NULL");

            if (String.IsNullOrEmpty(mailAddressReceiver))
            {
                Logger.Error("String.IsNullOrEmpty(mailAddressReceiver)");
                throw new ResultException("String.IsNullOrEmpty(mailAddressReceiver)");
            }

            Parser emailParser = null;

            if (ertesitesType == ErtesitesType.HataridosErtesites)
            {
                emailParser = this.GetTemplateParser("HataridosErtesitesTemplate");
            }

            if (ertesitesType == ErtesitesType.Esemenyertesites)
            {
                emailParser = this.GetTemplateParser("EsemenyErtesitesTemplate");
            }

            if (emailParser == null)
            {
                Logger.Error("emailParser = null");
                throw new ResultException("emailParser = null");
            }

            Logger.Debug("Template valtozok hozzadasa start");
            Hashtable templateVars = new Hashtable();
            templateVars.Add("Cimzett", felhasznaloFelelosNev);
            string esemeny_Kivalto_Nev = String.Empty;
            string esemeny_Kivalto_Kod = String.Empty;

            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Esemeny_Id_Kivalto))
            {
                Logger.Debug("Esemeny_Id_Kivalto lekerese start");
                KRT_FunkciokService funkciokService = new KRT_FunkciokService(this.dataContext);
                ExecParam xpmFunkcio = execParam.Clone();
                xpmFunkcio.Record_Id = erec_HataridosFeladatok.Esemeny_Id_Kivalto;
                Result resFunkcio = funkciokService.Get(xpmFunkcio);
                if (resFunkcio.IsError)
                {
                    Logger.Error("Esemeny_Id_Kivalto lekerese hiba", xpmFunkcio, resFunkcio);
                }
                else
                {
                    esemeny_Kivalto_Nev = ((KRT_Funkciok)resFunkcio.Record).Nev;
                    esemeny_Kivalto_Kod = ((KRT_Funkciok)resFunkcio.Record).Kod;
                }
                Logger.Debug("Esemeny_Id_Kivalto lekerese end");

            }

            if (ertesitesType == ErtesitesType.HataridosErtesites)
            {
                templateVars.Add("Esemeny_Kivalto_Nev", esemeny_Kivalto_Nev);
            }

            templateVars.Add("Leiras", erec_HataridosFeladatok.Leiras);
            if (ertesitesType == ErtesitesType.HataridosErtesites)
            {
                string hatarido = String.Empty;
                if (!String.IsNullOrEmpty(erec_HataridosFeladatok.IntezkHatarido))
                {
                    hatarido = "Hat�rid�: " + erec_HataridosFeladatok.IntezkHatarido;
                }
                templateVars.Add("Hatarido", hatarido);
            }

            if (ertesitesType == ErtesitesType.HataridosErtesites)
            {
                string url = GetHataridosErtesitesUrl(erec_HataridosErtesitesek, esemeny_Kivalto_Kod);

                if (String.IsNullOrEmpty(url))
                    url = "#";
                templateVars.Add("Url", url);
            }

            if (ertesitesType == ErtesitesType.Esemenyertesites)
            {
                string url = String.Empty;
                string azonosito = String.Empty;

                if (!String.IsNullOrEmpty(erec_HataridosErtesitesek.ObjektumSzukites))
                {
                    azonosito = erec_HataridosErtesitesek.ObjektumSzukites;
                    url = GetObjektumUrl(erec_HataridosErtesitesek, esemeny_Kivalto_Kod);
                }

                if (String.IsNullOrEmpty(url))
                    url = "#";

                templateVars.Add("Azonosito", azonosito);
                templateVars.Add("Url", url);

                bool isTargy = !String.IsNullOrEmpty(erec_HataridosErtesitesek.Targy);
                templateVars.Add("IsTargy", isTargy);
                templateVars.Add("Targy", erec_HataridosErtesitesek.Targy ?? String.Empty);

                bool isUgyintezo = !String.IsNullOrEmpty(erec_HataridosErtesitesek.UgyintezoNev);
                templateVars.Add("IsUgyintezo", isUgyintezo);
                templateVars.Add("UgyintezoNev", erec_HataridosErtesitesek.UgyintezoNev ?? String.Empty);

            }

            emailParser.Variables = templateVars;
            Logger.Debug("Template valtozok hozzadasa end");

            Logger.Debug("SendTemplatedEmail start");
            string title = "Hat�rid�s �rtes�t�s";
            if (ertesitesType == ErtesitesType.Esemenyertesites)
            {
                title = "Esem�ny �rtes�t�s";
            }
            _ret = SendTemplatedEmail(execParam, mailAddressSender, new string[] { mailAddressReceiver }, GetFormattedEmailSubject(title), emailParser);
            if (!_ret)
            {
                Logger.Error("SendTemplatedEmail hiba");
                throw new ResultException("SendTemplatedEmail hiba");
            }
            Logger.Debug("SendTemplatedEmail end");
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendHataridosErtesitesEmail hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekok))]
    public bool SendEmailErkeztetve(ExecParam execParam, EREC_eMailBoritekok erec_eMailBoritekok, string erkeztetoszam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result res = new Result();
        bool _ret = true;

        try
        {
            //string mailAddressSender = "ugyfelszolgalat@budapest.hu";
            string mailAddressSender = Rendszerparameterek.Get(execParam, "UGYFELSZOLGALAT_EMAIL_CIM");

            if (String.IsNullOrEmpty(mailAddressSender))
            {
                throw new ResultException("UGYFELSZOLGALAT_EMAIL_CIM rendszerparam�ter �res!");
            }

            string mailAddressReceiver = erec_eMailBoritekok.Felado;

            if (String.IsNullOrEmpty(mailAddressReceiver))
            {
                throw new ResultException("erec_eMailBoritekok.Felado �res!");
            }

            string subject = String.Format("RE: {0} - {1}", erec_eMailBoritekok.Targy, erkeztetoszam);

            Parser emailParser = this.GetTemplateParser("EmailErkeztetesTemplate");

            Hashtable templateVars = new Hashtable();
            templateVars.Add("erkeztetoszam", erkeztetoszam);

            emailParser.Variables = templateVars;

            _ret = SendTemplatedEmail(execParam, mailAddressSender, new string[] { mailAddressReceiver }, subject, emailParser, new string[] { mailAddressSender });
            if (!_ret)
            {
                Logger.Error("SendTemplatedEmail hiba");
                throw new ResultException("SendTemplatedEmail hiba");
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendEmailErkeztes hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }

    //LZS - BUG_6386
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekok))]
    public bool SendEmailToKozpontiIrattarosok(ExecParam execParam, EREC_eMailBoritekok erec_eMailBoritekok, string ugyiratszam, string ITSZ, string ugyintezo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result res = new Result();
        bool _ret = true;

        try
        {
            //string mailAddressSender = "ugyfelszolgalat@budapest.hu";
            //string mailAddressSender = Rendszerparameterek.Get(execParam, "UGYFELSZOLGALAT_EMAIL_CIM");

            //if (String.IsNullOrEmpty(mailAddressSender))
            //{
            //    throw new ResultException("UGYFELSZOLGALAT_EMAIL_CIM rendszerparam�ter �res!");
            //}

            string mailAddressReceiver = erec_eMailBoritekok.Cimzett;

            if (String.IsNullOrEmpty(mailAddressReceiver))
            {
                throw new ResultException("erec_eMailBoritekok.Cimzett �res!");
            }

            string subject = String.Format("{0}", erec_eMailBoritekok.Targy);

            Parser emailParser = this.GetTemplateParser("KolcsonzesTemplate");

            Hashtable templateVars = new Hashtable();
            templateVars.Add("Ugyiratszam", ugyiratszam);
            templateVars.Add("ITSZ", ITSZ);
            templateVars.Add("Ugyintezo", ugyintezo);


            emailParser.Variables = templateVars;

            _ret = SendTemplatedEmail(execParam, erec_eMailBoritekok.Felado, mailAddressReceiver.Split(','), subject, emailParser);
            if (!_ret)
            {
                Logger.Error("SendTemplatedEmail hiba");
                throw new ResultException("SendTemplatedEmail hiba");
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendEmailErkeztes hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatok))]
    public bool SendEmailCimzettNemVetteAt(ExecParam execParam, HKP_DokumentumAdatok hkp_DokumentumAdatok, string azonosito)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result res = new Result();
        bool _ret = SendEmailCimzettNemVetteAt(execParam, hkp_DokumentumAdatok.Base.Letrehozo_id, hkp_DokumentumAdatok.IratPeldany_Id, azonosito);

        log.WsEnd(execParam, res);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public bool SendEmaileBeadvanyCimzettNemVetteAt(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, string azonosito)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result res = new Result();
        bool _ret = SendEmailCimzettNemVetteAt(execParam, erec_eBeadvanyok.Base.Letrehozo_id, erec_eBeadvanyok.IratPeldany_Id, azonosito);

        log.WsEnd(execParam, res);
        return _ret;
    }

    bool SendEmailCimzettNemVetteAt(ExecParam execParam, string cimzettFelhasznalo_Id, string iratPeldany_Id, string iratPeldany_Azonosito)
    {
        Result res = new Result();
        bool _ret = true;

        try
        {
            string mailAddressSender = Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);

            if (String.IsNullOrEmpty(mailAddressSender))
            {
                throw new ResultException("RENDSZERFELUGYELET_EMAIL_CIM rendszerparam�ter �res!");
            }

            Logger.Debug("Felhasznalo_Id lekerese start: " + cimzettFelhasznalo_Id);
            KRT_FelhasznalokService svcFelhasznalok = new KRT_FelhasznalokService(this.dataContext);
            ExecParam xpmFelhasznalo = execParam.Clone();
            xpmFelhasznalo.Record_Id = cimzettFelhasznalo_Id;
            Result resFelhasznalo = svcFelhasznalok.Get(xpmFelhasznalo);
            if (resFelhasznalo.IsError)
            {
                Logger.Error("Felhasznalo lekerese hiba", xpmFelhasznalo, resFelhasznalo);
                throw new ResultException(resFelhasznalo);
            }
            Logger.Debug("Felhasznalo lekerese vege");
            KRT_Felhasznalok expedialoFelhasznalo = (KRT_Felhasznalok)resFelhasznalo.Record;
            string mailAddressReceiver = expedialoFelhasznalo.EMail;
            Logger.Debug("mailAddressReceiver: " + mailAddressReceiver ?? "NULL");

            if (String.IsNullOrEmpty(mailAddressReceiver))
            {
                Logger.Error("String.IsNullOrEmpty(mailAddressReceiver)");
                throw new ResultException("A felhaszn�l� e-mail c�m�t nem siker�lt lek�rni!");
            }

            string subject = GetFormattedEmailSubject(String.Format("�t nem vett hivatali kapus �zenet - {0}", iratPeldany_Azonosito));

            string url = ObjektumTipusok.GetFullListUrl(iratPeldany_Id, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok, String.Empty);

            string urlText = String.Format("{0}", iratPeldany_Azonosito);

            Parser emailParser = this.GetTemplateParser("CimzettNemVetteAtTemplate");

            Hashtable templateVars = new Hashtable();
            templateVars.Add("Cimzett", expedialoFelhasznalo.Nev);
            templateVars.Add("Url", url);
            templateVars.Add("UrlText", urlText);

            emailParser.Variables = templateVars;

            _ret = SendTemplatedEmail(execParam, mailAddressSender, new string[] { mailAddressReceiver }, subject, emailParser);
            if (!_ret)
            {
                Logger.Error("SendTemplatedEmail hiba");
                throw new ResultException("SendTemplatedEmail hiba");
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendEmailCimzettNemVetteAt hiba", execParam, res);
            _ret = false;
        }

        return _ret;
    }

    SmtpClient GetSmtpClient(ExecParam execParam)
    {
        SmtpClient client = new SmtpClient();
        execParam.Record_Id = String.Empty;
        client.Host = Rendszerparameterek.Get(execParam, Rendszerparameterek.EMAIL_SERVER_ADDRESS);

        int port = Rendszerparameterek.GetInt(execParam, "EMAIL_SERVER_PORT", 0);

        if (port > 0)
        {
            client.Port = port;
        }

        string userName = Rendszerparameterek.Get(execParam, "EMAIL_SERVER_USERNAME");
        string password = Rendszerparameterek.Get(execParam, "EMAIL_SERVER_PASSWORD");

        if (!String.IsNullOrEmpty(userName) && !String.IsNullOrEmpty(password))
        {
            client.Credentials = new System.Net.NetworkCredential(userName, password);
        }

        bool useSSL = Rendszerparameterek.GetBoolean(execParam, "EMAIL_SERVER_USESSL", false);

        if (useSSL)
        {
            client.EnableSsl = useSSL;
        }

        return client;
    }
}

