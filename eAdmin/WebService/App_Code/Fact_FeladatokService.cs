using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
///    A(z) Fact_Feladatok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class Fact_FeladatokService : System.Web.Services.WebService
{

    /// <summary>
    /// Ertesitesek(ExecParam ExecParam, Fact_FeladatokSearch _Fact_FeladatokSearch)
    /// A Fact_Feladatok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�terek tartalmazz�k. 
    /// </summary>   
    /// <param name="execParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="feladatokRiportParameterek">Bemen� param�ter, a riport gener�l�shoz sz�ks�ges param�tereket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A Fact_Feladatok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�terek tartalmazz�k. Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[System.Xml.Serialization.XmlInclude(typeof(Fact_FeladatokSearch))]
    [System.Xml.Serialization.XmlInclude(typeof(FeladatokRiportParameterek))]
    public Result Ertesitesek(ExecParam execParam, FeladatokRiportParameterek feladatokRiportParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        
         Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Ertesitesek(execParam, feladatokRiportParameterek);
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(execParam, result);
        return result;
    }

    //class DTSEventListener : Microsoft.SqlServer.Dts.Runtime.DefaultEvents
    //{
    //    public override bool OnError(Microsoft.SqlServer.Dts.Runtime.DtsObject source, int errorCode, string subComponent,
    //      string description, string helpFile, int helpContext, string idofInterfaceWithError)
    //    {
    //        // Add application-specific diagnostics here.
    //        //Console.WriteLine("Error in {0}/{1} : {2}", source, subComponent, description);
    //        Logger.Error(String.Format("Error in {0}/{1} : {2}", source, subComponent, description));
    //        return false;
    //    }
    //}

    /// <summary>
    /// FeladatLetrehozo(ExecParam ExecParam, string Felhasznalo_Id)
    /// Az adatpiac friss�t�se megadott felhaszn�l�ra, szervezetre.
    /// </summary>   
    /// <param name="execParam">Az ExecParam bemen� param�ter tartalmazza a szervezetre vonatkoz� inform�ci�t. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="Felhasznalo_Id">Azon felhaszn�l� azonos�t�ja, melyre a friss�t�st v�grehajtjuk (elt�rhet a v�grehajt� felhaszn�l�t�l, de azonos szervezetbe kell tartoznia vele)</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adatpiac friss�t�se megadott felhaszn�l�ra, szervezetre. A Felhasznalo_Id bemen� param�ter azon felhaszn�l� azonos�t�ja, melyre a friss�t�st v�grehajtjuk (elt�rhet a v�grehajt� felhaszn�l�t�l, de azonos szervezetbe kell tartoznia vele). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    public Result FeladatLetrehozo(ExecParam execParam, string Felhasznalo_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        /* 1. t�pus -  package ind�t�s (jogosults�gi gondok miatt nem m�k�dik)
        Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
        Microsoft.SqlServer.Dts.Runtime.Package package = null;
        Microsoft.SqlServer.Dts.Runtime.DTSExecResult pkgResults;

        DTSEventListener dtsEventListener = new DTSEventListener();

        try
        {
            // TODO: param�terez�s (web.config)
            string packagePath = "\\MSDB\\UserFeladatok";
            string serverName = "srvvirtual";
            string connectionString = "Data Source=srvvirtual;Initial Catalog=FeladatDev;Provider=SQLNCLI.1;Integrated Security=SSPI;Auto Translate=False;";

            if (app.ExistsOnDtsServer(packagePath, serverName))
            {
                package = app.LoadFromDtsServer(packagePath, serverName, null);
                Microsoft.SqlServer.Dts.Runtime.Variables vars = package.Variables;
                vars["conn_edok"].Value = connectionString;
                vars["logicalDate"].Value = System.DateTime.Now.ToString("yyyy.MM.dd");
                vars["logID"].Value = 7;
                vars["Szervezet"].Value = execParam.FelhasznaloSzervezet_Id;
                vars["Felhasznalo"].Value = Felhasznalo_Id;
                vars["Osszesre"].Value = false;
                pkgResults = (Microsoft.SqlServer.Dts.Runtime.DTSExecResult)package.Execute(null, null, dtsEventListener, null, null);
                

                result.Record = pkgResults.ToString();
            }
            else
            {
                throw new Exception("Invalid package name or location: " + packagePath + " (server: " + serverName + ")");
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            if (package != null)
            {
                package.Dispose();
                package = null;
            }
        }
        */

        /* 2. t�pus - bejegyz�s friss�t�si ig�nyre + job megl�k�s */
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        // megjegyezz�k a friss�t�s d�tum�t, �s hiba eset�n is visszaadjuk
        // (hib�nak sz�m�t az is, ha el��rt id�n bel�l pr�b�l �jra friss�teni)
        DateTime LastUpdateTime = DateTime.MinValue;
        bool isFeladatLetrehozoError = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // 1. l�p�s: bejegyz�s l�trehoz�sa az eAdatpiac.FeladatIgeny t�bl�ban
            Result result_feladatletrehozo = sp.FeladatLetrehozo(execParam, Felhasznalo_Id);

            if (result_feladatletrehozo.Record != null)
            {
                LastUpdateTime = (DateTime)result_feladatletrehozo.Record;
            }

            if (result_feladatletrehozo.IsError)
            {
                isFeladatLetrehozoError = true; // ha az �j ig�ny bejegyz�se nem siker�lt, lehet, hogy visszakaptuk a kor�bbi bejegyz�s id�pontj�t
                throw new ResultException(result_feladatletrehozo);
            }

            // 2. l�p�s: job elind�t�sa
            string jobName = Rendszerparameterek.Get(execParam, Rendszerparameterek.FELADATOSSZESITO_JOB_ADATFRISSITES);
            if (String.IsNullOrEmpty(jobName))
            {
                //A feladat �sszes�t� adatfriss�t�s�t v�gz� job neve, illetve az azt tartalmaz� rendszerparam�ter nem tal�lhat�!
                throw new ResultException(52793);
            }

            Logger.Info("StartFeladatJob: " + jobName);
            Result result_job = sp.StartFeladatJob(execParam, jobName);

            if (result_job.IsError)
            {
                // nem g�rgetj�k vissza...
                //(ha nem siker�l a job ind�t�sa, majd egy m�sik jobfut�s, vagy az id�z�tett v�grehajt�s elint�zi)
                //throw new ResultException(result_job);
                Logger.Warn("Feladat Job ind�t�sa sikertelen!", execParam, result_job);
            }

            // ha a job is elindult, akkor �j bejegyz�s sz�letett (nem g�rgett�k vissza), ennek id�pontj�t visszaadjuk
            result = result_feladatletrehozo; // Record tartalmazza az utols� Update id�pontj�t
            result.Record = LastUpdateTime; // biztons�g kedv��rt vissza�rjuk, nem lehet null
            Logger.Info(String.Format("FeladatLetrehozo: Sikeres bejegyz�s. Utols� friss�t�si id�: {0} ", LastUpdateTime.ToString()), execParam);

            #region Esem�nynapl�z�s
            // TODO
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "Fact_Feladatok", "Update").Record;

            //    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);

            if (isFeladatLetrehozoError)
            {
                result.Record = LastUpdateTime; // vissza�rjuk a kor�bbi friss�t�s id�pontj�t, hogy a fel�leten pontosabb inform�ci�nk legyen
                Logger.Info(String.Format("FeladatLetrehozo: Sikertelen bejegyz�s. Utols� friss�t�si id�: {0} "
                                            , (LastUpdateTime==DateTime.MinValue ? "null" : LastUpdateTime.ToString())), execParam);
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }

       
}