﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Net;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for Configuration
/// </summary>
namespace Fph.Ter.Messanger
{
    public static class Configuration
    {
        private class MessageSection
        {
            private string _DefaultSkin = null;

            public string DefaultSkin
            {
                get { return _DefaultSkin; }
                set { _DefaultSkin = value; }
            }
            
            private int _DefaultDelaySeconds = 0;

            public int DefaultDelaySeconds
            {
                get { return _DefaultDelaySeconds; }
                set { _DefaultDelaySeconds = value; }
            }

            public MessageSection(NameValueCollection config)
            {
                if (config["DefaultSkin"] != null)
                {
                    this._DefaultSkin = config["DefaultSkin"].ToString();
                    Logger.Debug("DefaultSkin: " + this.DefaultSkin);
                }

                if (config["DefaultDelaySeconds"] != null)
                {
                    if (Int32.TryParse(config["DefaultDelaySeconds"].ToString(), out this._DefaultDelaySeconds))
                    {
                        Logger.Debug("DefaultDelaySeconds: " + this.DefaultDelaySeconds);
                    }
                    else
                    {
                        Logger.Error("Int32.TryParse = false");
                    }
                }
            }
        }

        private class WebServiceSection
        {
            private string _MessageServiceUrl = null;

            public string MessageServiceUrl
            {
                get { return _MessageServiceUrl; }
                set { _MessageServiceUrl = value; }
            }

            public WebServiceSection(NameValueCollection config)
            {
                if (config["MessageServiceUrl"] != null)
                {
                    this._MessageServiceUrl = config["MessageServiceUrl"].ToString();
                    Logger.Debug("MessageServiceUrl: " + this.MessageServiceUrl);
                }
            }
        }

        public static void ConfigureMessageService(WebServiceProxy.MessageService service)
        {
            Logger.Debug("ConfigureMessageService start");
            NameValueCollection config = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("Fph.Ter.Messanger/WebService");

            if (config != null)
            {
                WebServiceSection section = new WebServiceSection(config);
                if (section.MessageServiceUrl != null)
                {
                    service.Url = section.MessageServiceUrl;
                }
                else
                {
                    throw new ResultException("MessageServiceUrl config not found");
                }
            }
            else
            {
                throw new ResultException("Fph.Ter.Messanger/WebService configsection not found");
            }

            service.Credentials = CredentialCache.DefaultCredentials;
            Logger.Debug("ConfigureMessageService end");
        }

        public static void ConfigureDefaultMessage(Message message)
        {
            Logger.Debug("ConfigureMessage start");
            NameValueCollection config = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("Fph.Ter.Messanger/Message");

            if (config != null)
            {
                MessageSection section = new MessageSection(config);

                if (String.IsNullOrEmpty(message.SkinName))
                    message.SkinName = section.DefaultSkin;
                if (message.DelaySeconds < 0)
                    message.DelaySeconds = section.DefaultDelaySeconds;
            }
            else
            {
                Logger.Warn("Fph.Ter.Messanger/Message configsection not found");
            }

            Logger.Debug("ConfigureMessage end");
        }
    }
}
