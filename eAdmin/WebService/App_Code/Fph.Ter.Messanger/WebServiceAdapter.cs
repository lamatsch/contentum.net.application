using System;
using System.Data;
using System.Net;
using System.Xml;
using System.Collections.Specialized;



namespace Fph.Ter.Messanger
{
    /// <summary>
    /// A webszerviz-proxy f�l�tti adapter-r�teg, ez h�vja a webszervizeket.
    /// </summary>
    internal static class WebServiceAdapter
    {       
        public static WebServiceProxy.MessageService GetMessageService()
        {
            WebServiceProxy.MessageService service = new WebServiceProxy.MessageService();
            Configuration.ConfigureMessageService(service);

            return service;
        }
    }
}
