﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eUtility;
using System.Web.Services;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_Felhasznalok_Halozati_NyomtatoiService
/// </summary>
public partial class KRT_Felhasznalok_Halozati_NyomtatoiService : System.Web.Services.WebService
{
    private KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure sp = null;
    
    private DataContext dataContext;
    
    public KRT_Felhasznalok_Halozati_NyomtatoiService()
    {
        dataContext = new DataContext(this.Application);
        
        //sp = new KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure(this.Application);
        sp = new KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure(dataContext);
    }

    public KRT_Felhasznalok_Halozati_NyomtatoiService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new KRT_Felhasznalok_Halozati_NyomtatoiStoredProcedure(dataContext);
    }   
    /// <summary>
    /// Get(ExecParam ExecParam)  
    /// A KRT_Felhasznalok_Halozati_Nyomtatoi rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemenő paraméter további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok_Halozati_Nyomtatoi))]
    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        
            result = sp.Get(ExecParam);
            
        }
        catch (Exception e)
        {            
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
            
        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// GetAll(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch)
    /// A(z) KRT_Felhasznalok_Halozati_Nyomtatoi táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szűrési feltételeket paraméter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam.Record_Id nem használt, további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).</param>
    /// <param name="_KRT_Felhasznalok_Halozati_NyomtatoiSearch">Bemenő paraméter, a keresési feltételeket tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szűrési feltételeket paraméter tartalmazza (*Search). Az ExecParam.Record_Id nem használt, a további adatok a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok_Halozati_NyomtatoiSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_Felhasznalok_Halozati_NyomtatoiSearch _KRT_Felhasznalok_Halozati_NyomtatoiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_Felhasznalok_Halozati_NyomtatoiSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Insert(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
    /// Egy rekord felvétele a KRT_Felhasznalok_Halozati_Nyomtatoi táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok_Halozati_Nyomtatoi))]
    public Result Insert(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.Insert(Constants.Insert, ExecParam, Record);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            
            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
               KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

               KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Felhasznalok_Halozati_Nyomtatoi", "New").Record;

               Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion
                        
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);            
        }
        catch (Exception e)
        {            
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Update(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
    /// Az KRT_Felhasznalok_Halozati_Nyomtatoi tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <param name="Record">A módosított adatokat a Record paraméter tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok_Halozati_Nyomtatoi))]
    public Result Update(ExecParam ExecParam, KRT_Felhasznalok_Halozati_Nyomtatoi Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            
            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
               KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

               KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok_Halozati_Nyomtatoi", "Modify").Record;

               Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);                        
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az KRT_Felhasznalok_Halozati_Nyomtatoi tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). Az ExecParam. adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok_Halozati_Nyomtatoi))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
         Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            
            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
               KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

               KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok_Halozati_Nyomtatoi", "Invalidate").Record;

               Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(ExecParam, result);
        return result;
    }
}