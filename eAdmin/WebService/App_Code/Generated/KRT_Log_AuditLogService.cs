﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for KRT_Log_AuditLogService
/// </summary>
public partial class KRT_Log_AuditLogService : System.Web.Services.WebService
{
    private KRT_Log_AuditLogStoredProcedure sp = null;

    private DataContext dataContext;

    public KRT_Log_AuditLogService()
    {
        dataContext = new DataContext(this.Application);
        sp = new KRT_Log_AuditLogStoredProcedure(dataContext);
    }

    public KRT_Log_AuditLogService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new KRT_Log_AuditLogStoredProcedure(dataContext);
    }
}