using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
///    A(z) KRT_Log_StoredProcedure t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
public partial class KRT_Log_StoredProcedureService : System.Web.Services.WebService
{
    private KRT_Log_StoredProcedureStoredProcedure sp = null;
    
    private DataContext dataContext;
    
    public KRT_Log_StoredProcedureService()
    {
        dataContext = new DataContext(this.Application);
        
        //sp = new KRT_Log_StoredProcedureStoredProcedure(this.Application);
        sp = new KRT_Log_StoredProcedureStoredProcedure(dataContext);
    }   
    
    public KRT_Log_StoredProcedureService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new KRT_Log_StoredProcedureStoredProcedure(dataContext);
    }   
    /// <summary>
    /// Get(ExecParam ExecParam)  
    /// A KRT_Log_StoredProcedure rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Log_StoredProcedure))]
    public Result Get(ExecParam ExecParam)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        
            result = sp.Get(ExecParam);
            
        }
        catch (Exception e)
        {            
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
            
        //log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// GetAll(ExecParam ExecParam, KRT_Log_StoredProcedureSearch _KRT_Log_StoredProcedureSearch)
    /// A(z) KRT_Log_StoredProcedure t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_KRT_Log_StoredProcedureSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Log_StoredProcedureSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_Log_StoredProcedureSearch _KRT_Log_StoredProcedureSearch)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
         Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_Log_StoredProcedureSearch);
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        //log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Insert(ExecParam ExecParam, KRT_Log_StoredProcedure Record)
    /// Egy rekord felv�tele a KRT_Log_StoredProcedure t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Log_StoredProcedure))]
    public Result Insert(ExecParam ExecParam, KRT_Log_StoredProcedure Record)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.Insert(Constants.Insert, ExecParam, Record);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
                        
                        
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);            
        }
        catch (Exception e)
        {            
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        //log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Update(ExecParam ExecParam, KRT_Log_StoredProcedure Record)
    /// Az KRT_Log_StoredProcedure t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Log_StoredProcedure))]
    public Result Update(ExecParam ExecParam, KRT_Log_StoredProcedure Record)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
           
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);                        
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        //log.WsEnd(ExecParam, result);
        return result;
    }
     /// <summary>
    /// Delete(ExecParam ExecParam)
    /// A(z) KRT_Log_StoredProcedure t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>     
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Log_StoredProcedure))]
    public Result Delete(ExecParam ExecParam)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
                       
          
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);            
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        //log.WsEnd(ExecParam, result);
        return result;
    }   
        
}