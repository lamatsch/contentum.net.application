using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Web.Script.Services;

/// <summary>
///    A(z) KRT_Bankszamlaszamok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_BankszamlaszamokService : System.Web.Services.WebService
{
    /// <summary>
    /// GetAllWithExtension(ExecParam ExecParam, KRT_BankszamlaszamokSearch _KRT_BankszamlaszamokSearch)
    /// A(z) KRT_Bankszamlaszamok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_KRT_BankszamlaszamokSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_BankszamlaszamokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_BankszamlaszamokSearch _KRT_BankszamlaszamokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_BankszamlaszamokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBankszamlaszamokList(string prefixText, int count, string contextKey)
    {
        string[] parameters = contextKey.Split(';');

        // userId �s loginId k�telez�
        if (parameters.Length < 2) return null;

        String userId = parameters[0];
        String loginId = parameters[1];

        string partnerId = null;
        if (parameters.Length > 2)
        {
            partnerId = parameters[2];
        }

        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;
        execparam.LoginUser_Id = loginId;

        KRT_BankszamlaszamokSearch search = new KRT_BankszamlaszamokSearch();
        search.OrderBy = "KRT_Bankszamlaszamok.Bankszamlaszam";

        if (!String.IsNullOrEmpty(prefixText))
        {
            search.Bankszamlaszam.Value = prefixText + '%';
            search.Bankszamlaszam.Operator = Query.Operators.like;

            if (!string.IsNullOrEmpty(partnerId))
            {
                search.Partner_Id.Value = partnerId;
                search.Partner_Id.Operator = Query.Operators.equals;
            }
            search.TopRow = count;
            Result res = this.GetAll(execparam, search);

            if (!res.IsError)
            {
                char[] trimChar = new char[] {'-'};

                foreach (System.Data.DataRow row in res.Ds.Tables[0].Rows)
                {
                    row["Bankszamlaszam"] = System.Text.RegularExpressions.Regex.Replace(row["Bankszamlaszam"].ToString(), @"^([0-9]{8})([0-9]{8})([0-9]{8})?$", @"$1-$2-$3").TrimEnd(trimChar);
                }

                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Bankszamlaszam");
            }
        }

        return null;

    }
       
}