using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_CimekService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Cimek))]
    public Result InsertWithFKResolution(ExecParam ExecParam, KRT_Cimek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string orszagID = String.Empty;
            string telepulesID = String.Empty;
            string kozteruletID = String.Empty;
            string kozteruletTipusID = String.Empty;

            if (!String.IsNullOrEmpty(Record.OrszagNev))
            {
                KRT_OrszagokService serviceOrszagok = new KRT_OrszagokService(this.dataContext);
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Nev.Value = Record.OrszagNev;
                searchOrszagok.Nev.Operator = Query.Operators.equals;
                result = serviceOrszagok.GetAll(ExecParam, searchOrszagok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(orszagID))
                {
                    Record.Orszag_Id = orszagID;
                    Record.Updated.Orszag_Id = Record.Updated.OrszagNev;
                }
            }

            if (!String.IsNullOrEmpty(Record.TelepulesNev))
            {
                KRT_TelepulesekService serviceTelepulesek = new KRT_TelepulesekService(this.dataContext);
                KRT_TelepulesekSearch searchTelepulesek = new KRT_TelepulesekSearch();
                searchTelepulesek.Nev.Value = Record.TelepulesNev;
                searchTelepulesek.Nev.Operator = Query.Operators.equals;
                if (!String.IsNullOrEmpty(Record.IRSZ))
                {
                    searchTelepulesek.IRSZ.Value = Record.IRSZ;
                    searchTelepulesek.IRSZ.Operator = Query.Operators.equals;
                }
                result = serviceTelepulesek.GetAll(ExecParam, searchTelepulesek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        telepulesID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(telepulesID))
                {
                    Record.Telepules_Id = telepulesID;
                    Record.Updated.Telepules_Id = Record.Updated.TelepulesNev;
                }
            }

            if (!String.IsNullOrEmpty(Record.KozteruletNev))
            {
                KRT_KozteruletekService serviceKozteruletek = new KRT_KozteruletekService(this.dataContext);
                KRT_KozteruletekSearch searchKozteruletek = new KRT_KozteruletekSearch();
                searchKozteruletek.Nev.Value = Record.KozteruletNev;
                searchKozteruletek.Nev.Operator = Query.Operators.equals;
                result = serviceKozteruletek.GetAll(ExecParam, searchKozteruletek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        kozteruletID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(kozteruletID))
                {
                    Record.Kozterulet_Id = kozteruletID;
                    Record.Updated.Kozterulet_Id = Record.Updated.KozteruletNev;
                }
            }

            if (!String.IsNullOrEmpty(Record.KozteruletTipusNev))
            {
                KRT_KozteruletTipusokService serviceKozteruletTipusok = new KRT_KozteruletTipusokService(this.dataContext);
                KRT_KozteruletTipusokSearch searchKozteruletTipusok = new KRT_KozteruletTipusokSearch();
                searchKozteruletTipusok.Nev.Value = Record.KozteruletTipusNev;
                searchKozteruletTipusok.Nev.Operator = Query.Operators.equals;
                result = serviceKozteruletTipusok.GetAll(ExecParam, searchKozteruletTipusok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        kozteruletTipusID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(kozteruletTipusID))
                {
                    Record.KozteruletTipus_Id = kozteruletTipusID;
                    Record.Updated.KozteruletTipus_Id = Record.Updated.KozteruletTipusNev;
                }
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Cimek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Cimek))]
    public Result UpdateWithFKResolution(ExecParam ExecParam, KRT_Cimek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string orszagID = String.Empty;
            string telepulesID = String.Empty;
            string kozteruletID = String.Empty;
            string kozteruletTipusID = String.Empty;

            if (!String.IsNullOrEmpty(Record.OrszagNev))
            {
                KRT_OrszagokService serviceOrszagok = new KRT_OrszagokService(this.dataContext);
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Nev.Value = Record.OrszagNev;
                searchOrszagok.Nev.Operator = Query.Operators.equals;
                result = serviceOrszagok.GetAll(ExecParam, searchOrszagok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                Record.Orszag_Id = orszagID;
                Record.Updated.Orszag_Id = Record.Updated.OrszagNev;
            }

            if (!String.IsNullOrEmpty(Record.TelepulesNev))
            {
                KRT_TelepulesekService serviceTelepulesek = new KRT_TelepulesekService(this.dataContext);
                KRT_TelepulesekSearch searchTelepulesek = new KRT_TelepulesekSearch();
                searchTelepulesek.Nev.Value = Record.TelepulesNev;
                searchTelepulesek.Nev.Operator = Query.Operators.equals;
                searchTelepulesek.IRSZ.Value = Record.IRSZ;
                searchTelepulesek.IRSZ.Operator = Query.Operators.equals;
                result = serviceTelepulesek.GetAll(ExecParam, searchTelepulesek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        telepulesID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                Record.Telepules_Id = telepulesID;
                Record.Updated.Telepules_Id = Record.Updated.TelepulesNev;
            }

            if (!String.IsNullOrEmpty(Record.KozteruletNev))
            {
                KRT_KozteruletekService serviceKozteruletek = new KRT_KozteruletekService(this.dataContext);
                KRT_KozteruletekSearch searchKozteruletek = new KRT_KozteruletekSearch();
                searchKozteruletek.Nev.Value = Record.KozteruletNev;
                searchKozteruletek.Nev.Operator = Query.Operators.equals;
                result = serviceKozteruletek.GetAll(ExecParam, searchKozteruletek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        kozteruletID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                Record.Kozterulet_Id = kozteruletID;
                Record.Updated.Kozterulet_Id = Record.Updated.KozteruletNev;
            }

            if (!String.IsNullOrEmpty(Record.KozteruletTipusNev))
            {
                KRT_KozteruletTipusokService serviceKozteruletTipusok = new KRT_KozteruletTipusokService(this.dataContext);
                KRT_KozteruletTipusokSearch searchKozteruletTipusok = new KRT_KozteruletTipusokSearch();
                searchKozteruletTipusok.Nev.Value = Record.KozteruletTipusNev;
                searchKozteruletTipusok.Nev.Operator = Query.Operators.equals;
                result = serviceKozteruletTipusok.GetAll(ExecParam, searchKozteruletTipusok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        kozteruletTipusID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                Record.KozteruletTipus_Id = kozteruletTipusID;
                Record.Updated.KozteruletTipus_Id = Record.Updated.KozteruletTipusNev;
            }

            result = this.Update(ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Cimek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CimekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithExtension(ExecParam, _KRT_CimekSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CimekSearch))]
    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek, String CIM_TIPUS, KRT_PartnerCimekSearch _KRT_PartnerCimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());	
		
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByPartner(ExecParam, _KRT_Partnerek, CIM_TIPUS, _KRT_PartnerCimekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllASPADOAddressId(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllASPADOAddressId(execParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetASPADOAddressId(ExecParam execParam, long adoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetASPADOAddressId(execParam, adoId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}