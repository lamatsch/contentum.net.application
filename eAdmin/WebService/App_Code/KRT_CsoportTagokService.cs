using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_CsoportTagokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �thozott fv.-ek

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportTagok))]
    public Result Insert(ExecParam ExecParam, KRT_CsoportTagok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            String csoportId = Record.Csoport_Id;
            String csoportIdJogalany = Record.Csoport_Id_Jogalany;

            KRT_CsoportokService krt_csoportokservice = new KRT_CsoportokService(this.dataContext);

            ExecParam execparam_csoport = ExecParam.Clone();
            execparam_csoport.Record_Id = csoportId;
            Result result_csoport = krt_csoportokservice.Get(execparam_csoport);
            if (!String.IsNullOrEmpty(result_csoport.ErrorCode))
            {
                // hiba
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_csoport);
                //return result_csoport;

                throw new ResultException(result_csoport);
            }

            KRT_Csoportok csoport = (KRT_Csoportok)result_csoport.Record;

            ExecParam execparam_csoportJogalany = ExecParam.Clone();
            execparam_csoportJogalany.Record_Id = csoportIdJogalany;
            Result result_csoportJogalany = krt_csoportokservice.Get(execparam_csoportJogalany);
            if (!String.IsNullOrEmpty(result_csoportJogalany.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_csoportJogalany);
                //return result_csoportJogalany;

                throw new ResultException(result_csoportJogalany);
            }

            KRT_Csoportok csoportJogalany = (KRT_Csoportok)result_csoportJogalany.Record;

            // A l�trehozand� csoporttags�g vizsg�lata:

            if (csoport.Tipus == KodTarak.CSOPORTTIPUS.Dolgozo)
            {   // Egyszem�lyes csoport alatt nem lehet m�sik csoport

                //Result result = ResultError.CreateNewResultWithErrorCode(51301);
                //log.WsEnd(ExecParam, result);
                //return result;

                throw new ResultException(51301);
            }
            else if (csoport.Tipus == KodTarak.CSOPORTTIPUS.Testulet_Bizottsag && csoportJogalany.Tipus == KodTarak.CSOPORTTIPUS.Szervezet)
            {   // Bizotts�g al� nem mehet szervezet

                //Result result = ResultError.CreateNewResultWithErrorCode(51302);
                //log.WsEnd(ExecParam, result);
                //return result;

                throw new ResultException(51302);
            }
            else
            {

                 result = sp.Insert(Constants.Insert, ExecParam, Record);

                 if (!String.IsNullOrEmpty(result.ErrorCode))
                 {
                     throw new ResultException(result);
                 }

            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_CsoportTagok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion
           
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportTagok))]
    public Result InvalidateWithoutRebuildCsoportTagokAll(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InvalidateWithoutRebuildCsoportTagokAll(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_CsoportTagok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region ERTESITES
            KRT_FelhasznalokService svcFelh = new KRT_FelhasznalokService(this.dataContext);
            svcFelh.CallKapcsoltUgyekErtesitesKuldes(ExecParam, this.dataContext, KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloCsoportTagsagTorles);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportTagok))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = null;
                if (i==ExecParams.Length - 1)
                    result_invalidate = Invalidate(ExecParams[i]);
                else
                    result_invalidate = InvalidateWithoutRebuildCsoportTagokAll(ExecParams[i]);

                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = "";

                for (int i = 0; i < ExecParams.Length; i++)
                {
                    Ids += "'" + ExecParams[i].Record_Id + "',";
                }

                Ids = Ids.TrimEnd(',');

                Result eventLogResult = eventLogService.InsertTomeges(ExecParams[0], Ids, "KRT_CsoportTagok", "Invalidate");
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }


    #endregion


    public Result ForceUpdate(ExecParam ExecParam, KRT_CsoportTagok Record)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.ForceUpdate(ExecParam, Record);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Csoporttagok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    public Result InsertOrNotInsert(ExecParam ExecParam, KRT_CsoportTagok Record)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            ExecParam execParamCsoport = ExecParam.Clone();
            execParamCsoport.Record_Id = Record.Id;

            if (IsCsoportKapcsolatExist(execParamCsoport, Record))
            {
                result = this.ForceUpdate(execParamCsoport, Record);
                
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }
            else
            {               
                result = this.Insert(ExecParam, Record);
                
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);   
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }


    public bool IsCsoportKapcsolatExist(ExecParam execParam, KRT_CsoportTagok Record)
    {
        if (String.IsNullOrEmpty(execParam.Record_Id))
            return false;


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = this.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_CsoportTagok krt_csoportTag = (KRT_CsoportTagok)result.Record;
                Record.Base.Ver = krt_csoportTag.Base.Ver;

                // RETURN
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                return true;
            }
            else
            {
                if (result.ErrorMessage == "[50101]")
                {
                    // RETURN
                    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                    return false;
                }
                else
                    throw new ResultException(result);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return false;
    }

    [WebMethod]
    public Result IsMember(ExecParam ExecParam, string CsoportId, string UserId)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.IsMember(ExecParam, CsoportId, UserId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);   
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }


    [WebMethod]
    public Result GetSzervezet(ExecParam ExecParam)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.GetSzervezet(ExecParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);   
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }

    /// <summary>
    /// GetAllWithExtension(ExecParam ExecParam, KRT_CsoportTagokSearch _KRT_CsoportTagokSearch)
    /// A(z) KRT_CsoportTagok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_KRT_CsoportTagokSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportTagokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_CsoportTagokSearch _KRT_CsoportTagokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_CsoportTagokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public void TryInvalidate(ExecParam execParam)
    {
        Result result = this.Invalidate(execParam);
        if (result.IsError)
        {
            //ha nem l�tezik a record vagy m�r invalid�lva van nem dobunk hib�t
            if (result.ErrorMessage == "[50602]" || result.ErrorMessage == "[50603]")
            {
            }
            else
            {
                throw new ResultException(result);
            }
        }
    }

    public Result CsoporttagokAllBuildFromCsoportTagok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.CsoporttagokAllBuildFromCsoportTagok(execParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result GetAll_ForTree(ExecParam execParam, string SelectedId)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.GetAll_ForTree(execParam, SelectedId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az KRT_CsoportTagok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportTagok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_CsoportTagok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region ERTESITES
            KRT_FelhasznalokService svcFelh = new KRT_FelhasznalokService(this.dataContext);
            svcFelh.CallKapcsoltUgyekErtesitesKuldes(ExecParam, this.dataContext, KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloCsoportTagsagTorles);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}