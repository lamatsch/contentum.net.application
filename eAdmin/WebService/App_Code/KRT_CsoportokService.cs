using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;
using System.Web.Script.Services;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_CsoportokService : System.Web.Services.WebService
{

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportokSearch))]
    public Result GetAllWithFK(ExecParam ExecParam, KRT_CsoportokSearch _KRT_CsoportokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithFK(ExecParam, _KRT_CsoportokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_CsoportokSearch, new KRT_CsoportokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Csoportok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_CsoportokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_CsoportokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_CsoportokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportokSearch))]
    public Result GetAllBySzuloCsoport(ExecParam ExecParam,
        KRT_Csoportok szuloCsoport, KRT_CsoportTagokSearch _krt_csoporttagokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllBySzuloCsoport(ExecParam, szuloCsoport, _krt_csoporttagokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Visszaadja azokat a felhaszn�l�csoportokat, akik al��rhatnak. (Funkci�jogosults�g alapj�n)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="funkcioKod"></param>
    /// <param name="krt_CsoportokSearch"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAll_Alairok(ExecParam execParam, KRT_CsoportokSearch krt_CsoportokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (krt_CsoportokSearch == null)
            {
                krt_CsoportokSearch = new KRT_CsoportokSearch();
            }

            // "IraIratAlairoNew" funkci�k�dra van a sz�r�s
            result = GetAllByFunkcio(execParam, "IraIratAlairoNew", krt_CsoportokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetAllByFunkcio(ExecParam execParam, string funkcioKod, KRT_CsoportokSearch krt_CsoportokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByFunkcio(execParam, funkcioKod, krt_CsoportokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Csoportok))]
    public Result Update(ExecParam ExecParam, KRT_Csoportok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            KRT_CsoportokService service = new KRT_CsoportokService(this.dataContext);
            Result res = service.Get(ExecParam);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            KRT_Csoportok csoport = (KRT_Csoportok)res.Record;
            if (csoport.Tipus == 1.ToString())
            {
                //ContextUtil.SetComplete();
                res.ErrorCode = "[50404:csoportok.update:system_group]";
                res.ErrorMessage = "[50404]";
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            if (csoport.Tipus == Contentum.eUtility.KodTarak.CSOPORTTIPUS.Dolgozo)
            {
                //ContextUtil.SetComplete();
                res.ErrorCode = "[50404:csoportok.update:dolgozo_type]";
                res.ErrorMessage = "[50404]";
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }


            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Csoportok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Felt�tel n�lk�li update, nem vizsg�lja a csoport t�pus�t(rendszer, dolgoz�)
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    public Result ForceUpdate(ExecParam ExecParam, KRT_Csoportok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Csoportok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Csoportok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            KRT_CsoportokService service = new KRT_CsoportokService(this.dataContext);
            Result res = service.Get(ExecParam);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            KRT_Csoportok csoport = (KRT_Csoportok)res.Record;
            if (csoport.System == 1.ToString())
            {
                //ContextUtil.SetComplete();
                res.ErrorCode = "[50404:csoportok.invalidate:system_group]";
                res.ErrorMessage = "[50404]";
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            if (csoport.Tipus == Contentum.eUtility.KodTarak.CSOPORTTIPUS.Dolgozo)
            {
                //ContextUtil.SetComplete();
                res.ErrorCode = "[50404:csoportok.invalidate:dolgozo_type]";
                res.ErrorMessage = "[50404]";
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Csoportok))]
    public Result ForceInvalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    public void TryInvalidate(ExecParam execParam)
    {
        Result result = this.ForceInvalidate(execParam);
        if (result.IsError)
        {
            //ha nem l�tezik a record vagy m�r invalid�lva van nem dobunk hib�t
            if (result.ErrorMessage == "[50602]" || result.ErrorMessage == "[50603]")
            {
                Logger.Warn("Csoportok TryInvalidate figyelmeztetes", execParam, result);
            }
            else
            {
                Logger.Error("Csoportok TryInvalidate hiba", execParam, result);
                throw new ResultException(result);
            }
        }
    }

    [WebMethod(Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Csoportok))]
    public Result GetByKod(ExecParam ExecParam, KRT_Csoportok krt_Csoport)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetByKod(ExecParam, krt_Csoport);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// A megadott partnerhez tartoz� KRT_Csoportok objektumot adja vissza
    /// Ha nincs ilyen csoport, result.Record null lesz
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partnerId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetByPartnerId(ExecParam execParam, string partnerId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            /// Ha a megadott partner bels� szervezet, akkor ugyanilyen id-val van elvileg a csoportja is
            /// Ha bels� szem�ly, akkor a KRT_Felhasznalok.PartnerId alapj�n kell lek�rni a felhaszn�l�Id-t, ami megegyezik a csoportId-val
            /// Ha nem bels� a partner, nincs hozz� csoport
            /// 

            KRT_PartnerekService service_Partnerek = new KRT_PartnerekService(this.dataContext);

            ExecParam execParam_partnerGet = execParam.Clone();
            execParam_partnerGet.Record_Id = partnerId;

            Result result_partnerGet = service_Partnerek.Get(execParam_partnerGet);
            if (result_partnerGet.IsError)
            {
                // hiba:
                throw new ResultException(result_partnerGet);
            }

            KRT_Partnerek krt_partnerek = (KRT_Partnerek)result_partnerGet.Record;

            if (krt_partnerek.Belso != "1")
            {
                // Nem bels� partner --> �res Result-ot adunk vissza:
                result = new Result();
            }
            else
            {
                if (krt_partnerek.Tipus == KodTarak.Partner_Tipus.Szemely)
                {
                    #region Szem�ly

                    /// Felhaszn�l�k t�bl�b�l kell lek�rdez�s: (PartnerId mez�re sz�r�s)
                    /// Ha van tal�lat, felhaszn�l�id == CsoportId
                    /// 
                    KRT_FelhasznalokService service_felhasznalok = new KRT_FelhasznalokService(this.dataContext);

                    KRT_FelhasznalokSearch search_felhasznalok = new KRT_FelhasznalokSearch();

                    search_felhasznalok.Partner_id.Value = partnerId;
                    search_felhasznalok.Partner_id.Operator = Query.Operators.equals;

                    Result result_felhasznalokGetAll = service_felhasznalok.GetAll(execParam.Clone(), search_felhasznalok);
                    if (result_felhasznalokGetAll.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_felhasznalokGetAll);
                    }

                    // ha nincs tal�lat, vagy t�bb is van --> nem hat�rozhat� meg a csoportId
                    if (result_felhasznalokGetAll.Ds.Tables[0].Rows.Count != 1)
                    {
                        result = new Result();
                    }
                    else
                    {
                        string felhasznaloId = result_felhasznalokGetAll.Ds.Tables[0].Rows[0]["Id"].ToString();

                        // csoportId == felhasznaloId
                        ExecParam execParam_csoportGet = execParam.Clone();
                        execParam_csoportGet.Record_Id = felhasznaloId;

                        Result result_csoportGet = this.Get(execParam_csoportGet);
                        if (result_csoportGet.IsError)
                        {
                            // hiba:
                            throw new ResultException(result_csoportGet);
                        }

                        result = result_csoportGet;
                    }

                    #endregion
                }
                else
                {
                    #region Szervezet

                    /// Szervezet, elvileg ugyanilyen Id-j� csoport tartozik hozz�
                    /// (Get, de ha nincs ilyen rekord, nem dobunk vissza hib�t, csak egy �res Result-tal t�r�nk vissza)

                    ExecParam execParam_csoportGet = execParam.Clone();
                    execParam_csoportGet.Record_Id = partnerId;

                    Result result_csoportGet = this.Get(execParam_csoportGet);
                    /// ha nincs hiba, visszaadjuk a tal�latot, egy�bk�nt pedig egy �res Result-ot:
                    if (!result_csoportGet.IsError && result_csoportGet.Record != null)
                    {
                        // ha van tal�lat:
                        result = result_csoportGet;
                    }
                    else
                    {
                        result = new Result();
                    }

                    #endregion
                }
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    [WebMethod()]
    public Result GetAllIrattar(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            string felhasznaloSzervezete = ExecParam.FelhasznaloSzervezet_Id;
            if (String.IsNullOrEmpty(felhasznaloSzervezete))
            {
                //Nincs a felhaszn�l� szervezethez rendelve
                throw new ResultException(53302);
            }

            Logger.Debug("Felhaszn�l� szervezete: " + felhasznaloSzervezete);

            KRT_PartnerekService srvcPartnerek = new KRT_PartnerekService();
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = felhasznaloSzervezete;
          
            KRT_PartnerKapcsolatokSearch srchPartnerKapcsolat = new KRT_PartnerKapcsolatokSearch();
            srchPartnerKapcsolat.Tipus.Value = Contentum.eUtility.KodTarak.PartnerKapcsolatTipus.irattara;
            srchPartnerKapcsolat.Tipus.Operator = Query.Operators.equals;

            result = srvcPartnerek.GetAllByPartner(ExecParam, partner, srchPartnerKapcsolat);

            Logger.Debug("Eredm�ny: " + result.Ds.Tables[0].Rows.Count);

            //id meghack-el�se
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    row["Id"] = row["Partner_id_kapcsolt"];
                }
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Visszaadja azon �tmeneti iratt�rak azonos�t�it, melyekb�l a felhaszn�l� megfelle� jogosults�g mellett kiadhat,
    /// illetve amelyekbe �gyiratot �tvehet
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAllKezelhetoIrattar(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //string felhasznaloSzervezete = ExecParam.FelhasznaloSzervezet_Id;
            //if (String.IsNullOrEmpty(felhasznaloSzervezete))
            //{
            //    //Nincs a felhaszn�l� szervezethez rendelve
            //    throw new ResultException(53302);
            //}

            //Logger.Debug("Felhaszn�l� szervezete: " + felhasznaloSzervezete);

            Result result_irattarak = GetAllIrattar(ExecParam);

            if (result_irattarak.IsError)
            {
                throw new ResultException(result_irattarak);
            }

            //iratt�ral bel�pve kezelheti az iratt�rat
            bool addSajatSzervezet = true;
            foreach (DataRow row in result_irattarak.Ds.Tables[0].Rows)
            {
                if (row["Id"].ToString().Equals(ExecParam.FelhasznaloSzervezet_Id, StringComparison.CurrentCultureIgnoreCase))
                {
                    addSajatSzervezet = false;
                }
            }

            if (addSajatSzervezet)
            {
                DataRow newRow = result_irattarak.Ds.Tables[0].NewRow();
                newRow["Id"] = ExecParam.FelhasznaloSzervezet_Id;
                result_irattarak.Ds.Tables[0].Rows.Add(newRow);
            }


            switch(Rendszerparameterek.Get(ExecParam, Rendszerparameterek.ATMENETI_IRATTAR_KEZELESJOG_TIPUS))
            {

                case "0":
                    goto default;
                case "1":
                    {
                        DataView dv = new DataView(result_irattarak.Ds.Tables[0]);
                        dv.RowFilter = String.Format("Id='{0}'", ExecParam.FelhasznaloSzervezet_Id);
                        result.Ds = new DataSet();
                        result.Ds.Tables.Add(dv.ToTable(true, new string[] { "Id" }));
                    }
                    break;
                default:
                    result = result_irattarak;
                    break;
        }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
    [WebMethod()]
    public Result GetAllAtmenetiIrattar(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            KRT_PartnerKapcsolatokSearch srchPartnerKapcsolat = new KRT_PartnerKapcsolatokSearch();
            srchPartnerKapcsolat.Tipus.Value = Contentum.eUtility.KodTarak.PartnerKapcsolatTipus.irattara;
            srchPartnerKapcsolat.Tipus.Operator = Query.Operators.equals;

            result = sp.GetAllAtmenetiIrattar(ExecParam, srchPartnerKapcsolat);
            Logger.Debug("Eredm�ny: " + result.Ds.Tables[0].Rows.Count);

            //id meghack-el�se
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    row["Id"] = row["Partner_id_kapcsolt"];
                }
            }      
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// a user felettes szervezete al� tartoz� �sszes csoportot hozza, bele�rtve mag�t is.
    /// kifejezetten a szign�lhat� csoportok list�ja miatt k�sz�lt
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Csoport_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAllSubCsoport(ExecParam ExecParam, string Csoport_Id, bool KozvetlenulHozzarendelt, KRT_CsoportokSearch where)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllSubCsoport(ExecParam,Csoport_Id,KozvetlenulHozzarendelt,where);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Az aktu�lis felhaszn�l� adott szervezet�t tekintve f�l�rendelt vezet�i k�z�l
    /// visszaadja aszervezeti hierarchi�ban azon els� vezet�t (KRT_Csoportok rekord),
    /// aki a megadott funkci�joggal rendelkezik.
    /// Ha a @IncludeSelfIfRightedLeader �rt�ke 1, akkor a megfelel� joggal rendelkez� vezet� felhaszn�l�ra �nmag�t adja vissza.
    /// Ha @IncludeSelfIfRightedLeader �rt�ke 0 (vagy m�s), akkor a megfelel� joggal rendelkez� vezet� felhaszn�l�ra
    /// a f�l�rendelt szefrvezeti hierarchia els� jogosult vezet�j�t adja vissza (pl. ha �nmag�nak nem hagyhat j�v�)
    /// Ha nincs tal�lat, null �rt�k� Record-ot ad vissza.
    /// Pl. j�v�hagy�si hierarchia eset�n haszn�land�.
    /// </summary>
    /// <param name="ExecParam">A felhhaszn�l� �s aktu�lis szervezet�nek azonos�t�it tartalmazza.</param>
    /// <param name="bIncludeSelfIfRightedLeader">Megmondja, hogy ha az adott felhaszn�l� vezet�
    /// �s rendelkezik az adott joggal, akkor �t, vagy a f�l�rendelt vezet�j�t adjuk-e vissza (pl. j�bv�hagyhat-e mag�nak)
    /// </param>
    /// <param name="funkcioKod">Az a funkci�k�d, amivel a vezet�nek rendelkeznie kell</param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetFirstRightedLeader(ExecParam ExecParam, bool bIncludeSelfIfRightedLeader, string funkcioKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetFirstRightedLeader(ExecParam, bIncludeSelfIfRightedLeader, funkcioKod);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod]
    public Result GetLeader(ExecParam ExecParam, string CsoportId)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetLeader(ExecParam, CsoportId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }

    // CR3157 A csoport kiv�laszt� komponensben iktat�k�nyv v�laszt�sn�l az ide iktat�k �s a jogosultak unioja jelenjen meg. 
    [WebMethod]
    public Result GetAllByIktatokonyvIktathat(ExecParam execParam, string IktatokonyvId, KRT_CsoportokSearch search)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByIktatokonyvIktathat(execParam, IktatokonyvId, search);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return result;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetCsoportokList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("GetCsoportokList webservice elindul");
        Logger.Debug(String.Format("Param�terek: prefixText {0}, count: {1}, contextKey {2}",prefixText,count, contextKey));

        Csoportok.CsoportFilterObject filterObject = new Csoportok.CsoportFilterObject(contextKey);
        String userId = filterObject.FelhasznaloId;

        Logger.Debug("Felhaszn�l� id: " + userId);

        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;
        
        KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();
        csoportokSearch.OrderBy = " KRT_Csoportok.Nev ASC, KRT_Csoportok.Id";
        csoportokSearch.Nev.Value = prefixText + '%';
        csoportokSearch.Nev.Operator = Query.Operators.like;
        csoportokSearch.TopRow = count;

        Logger.Debug("Csoport t�pus: " + filterObject.Tipus.ToString());

        if (filterObject.Tipus == Csoportok.CsoportFilterObject.CsoprtTipus.Disabled)
        {
            Logger.DebugEnd("Lek�rdez�s kikapcsolva, visszat�r�s null �rt�kkel");
            return null;
        }

        switch (filterObject.Tipus)
        {
            case Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo:
            case Csoportok.CsoportFilterObject.CsoprtTipus.OsszesDolgozo:
                csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
                csoportokSearch.Tipus.Operator = Query.Operators.equals;
                break;
            case Csoportok.CsoportFilterObject.CsoprtTipus.Szervezet:
                csoportokSearch.Tipus.Value = "'" + KodTarak.CSOPORTTIPUS.Szervezet + "','" + KodTarak.CSOPORTTIPUS.Projekt
                    + "','" + KodTarak.CSOPORTTIPUS.HivatalVezetes + "'";
                csoportokSearch.Tipus.Operator = Query.Operators.inner;
                //csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Szervezet;
                //csoportokSearch.Tipus.Operator = Query.Operators.equals;
                break;
            case Csoportok.CsoportFilterObject.CsoprtTipus.Bizottsag:
                csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Testulet_Bizottsag;
                csoportokSearch.Tipus.Operator = Query.Operators.equals;
                break;
        }

        //Sz�r�s iktat�k�nyv hozz�f�r�si jogosults�g alapj�n
        if (!String.IsNullOrEmpty(filterObject.IktatokonyvId.Trim()) && String.IsNullOrEmpty(filterObject.SzervezetId))
        {
            Logger.Debug("Sz�r�s iktat�k�ny alapj�n: " + filterObject.IktatokonyvId);
            // CR3157 A csoport kiv�laszt� komponensben iktat�k�nyv v�laszt�sn�l az ide iktat�k �s a jogosultak unioja jelenjen meg. 
            //RightsService rightsService = new RightsService();
            //Result res = rightsService.GetAllRightedSubCsoportByJogtargy(execparam,filterObject.IktatokonyvId,csoportokSearch);
            Result res = this.GetAllByIktatokonyvIktathat(execparam, filterObject.IktatokonyvId, csoportokSearch);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Debug("GetCsoportokList webservice iktat�k�nyv sz�r�ssel v�ge");
                Logger.DebugEnd();
                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
            }
        }
        else if (filterObject.Tipus != Csoportok.CsoportFilterObject.CsoprtTipus.OsszesDolgozo && (filterObject.Tipus == Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi || !String.IsNullOrEmpty(filterObject.SzervezetId)))
        {
            // Saj�t szervezet dolgoz�ira sz�r�s:
            Logger.Debug("Sz�r�s a szervezet dolgoz�ira: " + filterObject.SzervezetId);

            if (filterObject.Tipus == Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi)
            {
                csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
                csoportokSearch.Tipus.Operator = Query.Operators.equals;
            }

            Result res = this.GetAllSubCsoport(execparam, filterObject.SzervezetId, false, csoportokSearch);
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Debug("GetCsoportokList webservice szervezetre sz�r�ssel v�ge");
                Logger.DebugEnd();
                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
            }
        }
        else if (filterObject.Tipus == Csoportok.CsoportFilterObject.CsoprtTipus.SajatCsoportDolgozoi)
        {
            // Saj�t szervezet dolgoz�ira sz�r�s:
            Logger.Debug("Sz�r�s a csoport dolgoz�ira: " + filterObject.SzervezetId);

            csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
            csoportokSearch.Tipus.Operator = Query.Operators.equals;

            Result res = this.GetAllSubCsoport(execparam, filterObject.SzervezetId, true, csoportokSearch);
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Debug("GetCsoportokList webservice csoportra sz�r�ssel v�ge");
                Logger.DebugEnd();
                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
            }
        }        
        else
        {
            Logger.Debug("Csoportok service haszn�lata");

            if (!String.IsNullOrEmpty(prefixText))
            {
                KRT_CsoportokService csoportkService = new KRT_CsoportokService();
                Result res = csoportkService.GetAll(execparam, csoportokSearch);

                if (String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Debug("GetCsoportokList webservice v�ge");
                    Logger.DebugEnd();
                    return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
                }
            }
        }

        Logger.Debug("GetCsoportokList webservice v�ge null visszat�r�si �rt�kkel");
        Logger.DebugEnd();
        return null;

    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFelhasznaloCsoportokList(string prefixText, int count, string contextKey)
    {
        string[] parameters = contextKey.Split(';');
        String userId = parameters[0];


        KRT_CsoportokService csoportkService = new KRT_CsoportokService();
        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;

        KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();
        csoportokSearch.OrderBy = " KRT_Csoportok.Nev " + Utility.customCollation + " ASC, KRT_Csoportok.Id";

        if (!String.IsNullOrEmpty(prefixText))
        {
            csoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
            csoportokSearch.Tipus.Operator = Query.Operators.equals;
            csoportokSearch.Nev.Value = prefixText + '%';
            csoportokSearch.Nev.Operator = Query.Operators.like;
            csoportokSearch.TopRow = count;
            Result res = csoportkService.GetAll(execparam, csoportokSearch);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
            }
        }

        return null;

    }

    [WebMethod()]
    public Result Revalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Revalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Csoportok", "Revalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}