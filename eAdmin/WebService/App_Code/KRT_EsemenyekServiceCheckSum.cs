﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.Convert;
using Contentum.eUtility.eCryptography;
using Contentum.eUtility.Models;
using System;
using System.Data;
using System.Web.Services;

public partial class KRT_EsemenyekService
{
    [WebMethod(Description =
        "A bemeneti paraméter a maximális elemszám, amit ha meghívunk, " +
        "akkor azokat az esemény bejegyzéseket, " +
        "amelyek még nem kerültek korábban hitelesítése - hitelesít. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result SetIntegrityCheckSum(ExecParam ExecParam, int itemsNumber)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        if (itemsNumber > 0)
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

                KRT_EsemenyekService service = new KRT_EsemenyekService(this.dataContext);
                KRT_EsemenyekSearch src = new KRT_EsemenyekSearch();
                src.TopRow = itemsNumber;
                src.CheckSum.Value = string.Empty;
                src.CheckSum.Operator = Query.Operators.isnull;                

                Result resultFind = service.GetAll(ExecParam, src);

                if (string.IsNullOrEmpty(resultFind.ErrorCode) && resultFind.Ds.Tables[0].Rows.Count > 0)
                {
                    Model_KRT_Esemenyek_CheckSum model = null;
                    string modelAsXml = null;
                    ExecParam execParamUpdate = null;
                    Result resultUpdate = null;
                    KRT_Esemenyek _KRT_Esemenyek;

                    foreach (DataRow row in resultFind.Ds.Tables[0].Rows)
                    {
                        _KRT_Esemenyek = new KRT_Esemenyek();
                        Utility.LoadBusinessDocumentFromDataRow(_KRT_Esemenyek, row);

                        model = ModelConverters.Convert(_KRT_Esemenyek);
                        modelAsXml = XmlFunction.ObjectToXml(model, false);

                        #region SIGN
                        string outSignedValue = ComputeSignature(ExecParam, modelAsXml);
                        #endregion

                        #region UPDATE CHECKSUM FIELD
                        _KRT_Esemenyek.CheckSum = outSignedValue;
                        _KRT_Esemenyek.Updated.CheckSum = true;

                        execParamUpdate = ExecParam.Clone();
                        execParamUpdate.Record_Id = _KRT_Esemenyek.Id;
                        resultUpdate = service.Update(execParamUpdate, _KRT_Esemenyek);

                        if (!string.IsNullOrEmpty(resultUpdate.ErrorCode))
                            throw new ResultException(resultUpdate);

                        #endregion
                    }
                }

                // COMMIT
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
            catch (Exception e)
            {
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                result = ResultException.GetResultFromException(e);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description =
       "A bemeneti paraméter a maximális elemszám, amit ha meghívunk, " +
       "akkor azokat az esemény bejegyzéseket, " +
       "amelyek még nem kerültek korábban hitelesítése - hitelesít. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result SetIntegrityCheckSumSimple(string felhasznaloId, string FelhasznaloSzervezetId, int itemsNumber)
    {
        Logger.Debug("KRT_EsemenyekService.SetIntegrityCheckSumSimple START");
        if (string.IsNullOrEmpty(felhasznaloId) || string.IsNullOrEmpty(FelhasznaloSzervezetId))
        {
            Logger.Debug("KRT_EsemenyekService.SetIntegrityCheckSumSimple Hiányzó paraméterek !");
            return new Result();
        }
        ExecParam ExecParam = new ExecParam();
        ExecParam.Felhasznalo_Id = felhasznaloId;
        ExecParam.FelhasznaloSzervezet_Id = FelhasznaloSzervezetId;
        Result result = SetIntegrityCheckSum(ExecParam, itemsNumber);

        Logger.Debug("KRT_EsemenyekService.SetIntegrityCheckSumSimple STOP");
        return result;
    }

    [WebMethod(Description =
      "A bemeneti paraméterben megadott esemény bejegyzést hitelesíti. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result SetItemIntegrityCheckSum(ExecParam ExecParam, string itemId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            #region FIND ITEM

            ExecParam execParamGet = ExecParam.Clone();

            if (!string.IsNullOrEmpty(itemId))
                execParamGet.Record_Id = itemId;
            else if (!string.IsNullOrEmpty(ExecParam.Record_Id))
                execParamGet.Record_Id = ExecParam.Record_Id;
            else
                throw new ArgumentException("Input parameter error !");

            KRT_EsemenyekService service = new KRT_EsemenyekService(this.dataContext);
            Result resultFind = service.Get(execParamGet);

            if (!string.IsNullOrEmpty(resultFind.ErrorCode))
                throw new ResultException(resultFind);

            KRT_Esemenyek _KRT_Esemenyek = (KRT_Esemenyek)resultFind.Record;

            #endregion

            #region ALREADY SIGNED ?
            if (!string.IsNullOrEmpty(_KRT_Esemenyek.CheckSum))
            {
                Logger.Debug("(65121) A Hash képzés már korábban megtörtént !" + itemId);
                throw new ResultException(65121.ToString(), "A Hash képzés már korábban megtörtént !");
                //throw new ResultException(65121, new ErrorDetails("A Hash képzés már korábban megtörtént !", "KRT_Esemenyek", itemId, string.Empty, string.Empty));
            }
            #endregion

            Model_KRT_Esemenyek_CheckSum model = ModelConverters.Convert(_KRT_Esemenyek);
            string modelAsXml = XmlFunction.ObjectToXml(model, false);

            #region SIGNATURE
            XmlSignatureDetached xmlSignature = LoadXmlSignatureClass(ExecParam);

            string outSignedValue = null;
            string signedXmlResult = xmlSignature.Sign(modelAsXml, out outSignedValue);
            #endregion

            #region UPDATE CHECKSUM FIELD

            _KRT_Esemenyek.CheckSum = outSignedValue;
            _KRT_Esemenyek.Updated.CheckSum = true;

            ExecParam execParamUpdate = ExecParam.Clone();
            execParamUpdate.Record_Id = _KRT_Esemenyek.Id;
            Result resultUpdate = service.Update(execParamUpdate, _KRT_Esemenyek);

            if (!string.IsNullOrEmpty(resultUpdate.ErrorCode))
                throw new ResultException(resultUpdate);
            #endregion

            result.Record = true;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description =
   "A bemeneti paraméterben megadott esemény bejegyzésre ellenőrzi a hitelességet. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result CheckItemIntegrityCheckSum(ExecParam ExecParam, string itemId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        result.Record = false;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);
            #region FIND ITEM

            ExecParam execParamGet = ExecParam.Clone();

            if (!string.IsNullOrEmpty(itemId))
                execParamGet.Record_Id = itemId;
            else if (!string.IsNullOrEmpty(ExecParam.Record_Id))
                execParamGet.Record_Id = ExecParam.Record_Id;
            else
                throw new ArgumentException("Input parameter error !");

            KRT_EsemenyekService service = new KRT_EsemenyekService(this.dataContext);
            Result resultFind = service.Get(execParamGet);

            if (!string.IsNullOrEmpty(resultFind.ErrorCode))
                throw new ResultException(resultFind);

            KRT_Esemenyek _KRT_Esemenyek = (KRT_Esemenyek)resultFind.Record;

            #endregion

            #region CHECK: STORED HASH
            if (string.IsNullOrEmpty(_KRT_Esemenyek.CheckSum))
            {
                Logger.Debug("(65120) Az ellenőrzés, a hash hiánya miatt nem elvégezhető !" + itemId);
                throw new ResultException(65120.ToString(), "Az ellenőrzés, a hash hiánya miatt nem elvégezhető !");
                //throw new ResultException(65120, new ErrorDetails("Az ellenőrzés, a hash hiánya miatt nem elvégezhető !", "KRT_Esemenyek", itemId, string.Empty, string.Empty));
            }
            #endregion

            #region XML FROM MODEL
            Model_KRT_Esemenyek_CheckSum model = ModelConverters.Convert(_KRT_Esemenyek);
            string modelAsXml = XmlFunction.ObjectToXml(model, false);
            #endregion

            #region SIGN AGAIN
            string outSignedValue = ComputeSignature(ExecParam, modelAsXml);
            #endregion

            #region CHECK STORED SIGN VALUE AND ON THE FLY SIGNED VALUE
            result.Record = string.Equals(_KRT_Esemenyek.CheckSum, outSignedValue);
            result.Uid = itemId;
            #endregion

            if (result != null && result.Record != null && !(bool)result.Record)
            {
                Logger.Debug("(65122) A rekord  megváltozott !" + itemId);
                throw new ResultException(65122.ToString(), "A rekord  megváltozott !");
                //throw new ResultException(65122, new ErrorDetails("A rekord  megváltozott !", "KRT_Esemenyek", itemId, string.Empty, string.Empty));
            }

            //Logger.Debug(string.Format("STORED HASH:{0}, ON THE FLY HASH: {1}", _KRT_Esemenyek.CheckSum, outSignedValue));

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region HELPER
    /// <summary>
    /// ComputeSignature
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="modelAsXml"></param>
    /// <returns></returns>
    private static string ComputeSignature(ExecParam ExecParam, string modelAsXml)
    {
        XmlSignatureDetached xmlSignature = LoadXmlSignatureClass(ExecParam);
        string outSignedValue = null;
        string signedXmlResult = xmlSignature.Sign(modelAsXml, out outSignedValue);
        return outSignedValue;
    }

    /// <summary>
    /// LoadXmlSignatureClass
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    private static XmlSignatureDetached LoadXmlSignatureClass(ExecParam ExecParam)
    {
        string privateKeyPath = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.SIGN_SAJAT_PRIVAT_KULCS_PATH);
        string privateKeyPasswordFilePath = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.SIGN_SAJAT_PRIVAT_KULCS_PASSWORD);
        string password = string.Empty;

        if (!string.IsNullOrEmpty(privateKeyPasswordFilePath) && System.IO.File.Exists(privateKeyPasswordFilePath))
            password = System.IO.File.ReadAllText(privateKeyPasswordFilePath);
        XmlSignatureDetached xmlSignature = new XmlSignatureDetached(privateKeyPath, password);

        return xmlSignature;
    }
    #endregion
}