using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
//using System.EnterpriseServices;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_Felhasznalo_SzerepkorService : System.Web.Services.WebService
{
    [WebMethod()]
    public Result GetAllByCsoport(ExecParam ExecParam, KRT_Csoportok _KRT_Csoport
        , KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByCsoport(ExecParam, _KRT_Csoport, _KRT_Felhasznalo_SzerepkorSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetAllWithExtension(ExecParam ExecParam, KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    /// A(z) KRT_Felhasznalo_Szerepkor t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_KRT_Felhasznalo_SzerepkorSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalo_SzerepkorSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_Felhasznalo_SzerepkorSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #region Megbizas
    /// <summary>
    /// InsertSzerepkorokToMegbizas_Tomeges(ExecParam ExecParam, string[] FelhasznaloSzerepkorIds)
    /// Az �tadott megb�z�i szerepk�r�k m�sol�sa a KRT_Felhasznalo_Szerepkor t�bl�ba a megb�z�t�l a megb�zotthoz, a KRT_Helyettesitesek rekord alapj�n. A megb�z�si rekord azonos�t�j�t az ExecParam.Record_Id tartalmazza.
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az �tadott megb�z�i szerepk�r�k m�sol�sa a KRT_Felhasznalo_Szerepkor t�bl�ba a megb�z�t�l a megb�zotthoz, a KRT_Helyettesitesek rekord alapj�n. A megb�z�si rekord azonos�t�j�t az ExecParam.Record_Id tartalmazza.")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result InsertSzerepkorokToMegbizas_Tomeges(ExecParam ExecParam, string[] FelhasznaloSzerepkorIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (FelhasznaloSzerepkorIds == null || FelhasznaloSzerepkorIds.Length == 0)
            {
                throw new ResultException("A megb�z�sban nem szerepelnek �tadand� szerepk�r�k.");
            }

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException("Nincs megadva a megb�z�st tartalmaz� rekord azonos�t�ja.");
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InsertToMegbizas_Tomeges(ExecParam, FelhasznaloSzerepkorIds);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count != FelhasznaloSzerepkorIds.Length)
            {
                throw new ResultException("Az �truh�zand� szerepk�r�k sz�ma elt�r a t�nylegesen �tadhat� szerepk�r�k�t�l. Az �truh�z�s nem siker�lt!");
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Felhasznalo_Szerepkor", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// UpdateByMegbizas(ExecParam ExecParam)
    /// A megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. A megb�z�s azonos�t�j�t az ExecParam.Record_Id tartalmazza.
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. A megb�z�s azonos�t�j�t az ExecParam.Record_Id tartalmazza.")]
    public Result UpdateByMegbizas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
 
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.UpdateByMegbizas(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables.Count > 0)
            {
                result.Ds.Tables[0].TableName = "UPDATE";
            }
            if (result.Ds.Tables.Count > 1)
            {
                result.Ds.Tables[0].TableName = "INVALIDATE";
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Felhasznalo_Szerepkor", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// UpdateWithMegbizasControl(ExecParam ExecParam, KRT_Felhasznalo_Szerepkor Record)
    /// A szerepkor hozz�rendel�s �rv�nyess�g�nek m�dosul�sakor a megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. Az eredeti felhaszn�l�-szerepk�r hozz�rendel�s azonos�t�j�t az ExecParam.Record_Id tartalmazza.
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A szerepkor hozz�rendel�s �rv�nyess�g�nek m�dosul�sakor a megb�z�s alapj�n �tadott szerepk�r�k �rv�nyess�g�nek ellen�rz�se �s sz�ks�g eset�n m�dos�t�sa. Az eredeti felhaszn�l�-szerepk�r hozz�rendel�s azonos�t�j�t az ExecParam.Record_Id tartalmazza.")]
    public Result UpdateWithMegbizasControl(ExecParam ExecParam, KRT_Felhasznalo_Szerepkor Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            bool bErvChanged = false;
            KRT_Felhasznalo_Szerepkor krt_Felhasznalo_Szerepkor_Regi = null;
            #region r�gi rekord
            // r�gi rekord lek�r�se, ha sz�ks�ges, �s annak ellen�rz�se, az �rv�nyess�g eleje-v�ge m�dosult-e
            if (Record.Updated.ErvKezd || Record.Updated.ErvVege)
            {
                Logger.Info("R�gi felhaszn�l�-szerepk�r hozz�rendel�s rekord lek�r�se START");
                Result result_get = sp.Get(ExecParam);
                if (!String.IsNullOrEmpty(result_get.ErrorCode))
                {
                    throw new ResultException(result_get);
                }
                Logger.Info("R�gi felhaszn�l�-szerepk�r hozz�rendel�s rekord lek�r�se sikeres volt.");

                krt_Felhasznalo_Szerepkor_Regi = (KRT_Felhasznalo_Szerepkor)result_get.Record;

                if (String.IsNullOrEmpty(krt_Felhasznalo_Szerepkor_Regi.Helyettesites_Id))
                {
                    // egyel�re feltessz�k, hogy mindig azonos form�tum� d�tumot kapunk
                    bErvChanged |= (krt_Felhasznalo_Szerepkor_Regi.ErvKezd != Record.ErvKezd);
                    bErvChanged |= (krt_Felhasznalo_Szerepkor_Regi.ErvVege != Record.ErvVege);
                }

            }
            #endregion r�gi rekord

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (bErvChanged)
            {
                Logger.Info("Megb�z�sban �tadott szerepk�r rekordok �rv�nyess�g�nek ellen�rz�se START");
                Result result_fszUpdate = sp.UpdateAtruhazottBySzerepkor(ExecParam);

                if (!String.IsNullOrEmpty(result_fszUpdate.ErrorCode))
                {
                    throw new ResultException(result_fszUpdate);
                }

                Logger.Info("Megb�z�sban �tadott szerepk�r rekordok �rv�nyess�g�nek ellen�rz�se END");
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Felhasznalo_Szerepkor", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// InvalidateWithMegbizasControl(ExecParam ExecParam)
    /// Az KRT_Felhasznalo_Szerepkor t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s).
    /// A megb�z�s sor�n �truh�zott, m�g �rv�nyes jogokat is �rv�nytelen�ti.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A megb�z�s sor�n �truh�zott, m�g �rv�nyes jogokat is �rv�nytelen�ti. Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalo_Szerepkor))]
    public Result InvalidateWithMegbizasControl(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            KRT_Felhasznalo_Szerepkor krt_Felhasznalo_Szerepkor_Regi = null;
            bool bSajatjogu = true;
            #region r�gi rekord
            // r�gi rekord lek�r�se
            Logger.Info("R�gi felhaszn�l�-szerepk�r hozz�rendel�s rekord lek�r�se START");
            Result result_get = sp.Get(ExecParam);
            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }
            Logger.Info("R�gi felhaszn�l�-szerepk�r hozz�rendel�s rekord lek�r�se sikeres volt.");

            krt_Felhasznalo_Szerepkor_Regi = (KRT_Felhasznalo_Szerepkor)result_get.Record;

            if (!String.IsNullOrEmpty(krt_Felhasznalo_Szerepkor_Regi.Helyettesites_Id))
            {
                bSajatjogu = false;
            }

            #endregion r�gi rekord

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (bSajatjogu)
            {
                Logger.Info("Megb�z�sban �tadott szerepk�r rekordok �rv�nytelen�t�se START");
                Result result_fszInvalidate = sp.InvalidateAtruhazottBySzerepkor(ExecParam);

                if (!String.IsNullOrEmpty(result_fszInvalidate.ErrorCode))
                {
                    throw new ResultException(result_fszInvalidate);
                }

                Logger.Info("Megb�z�sban �tadott szerepk�r rekordok �rv�nytelen�t�se END");
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalo_Szerepkor", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidateWithMegbizasControl(ExecParam ExecParams)
    /// A KRT_Felhasznalo_Szerepkor t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza.
    /// A megb�z�s sor�n �truh�zott, m�g �rv�nyes jogokat is �rv�nytelen�ti.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A megb�z�s sor�n �truh�zott, m�g �rv�nyes jogokat is �rv�nytelen�ti. A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalo_Szerepkor))]
    public Result MultiInvalidateWithMegbizasControl(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = InvalidateWithMegbizasControl(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    #endregion Megbizas
}