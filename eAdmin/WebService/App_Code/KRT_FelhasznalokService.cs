﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;
using System.Web.Script.Services;
using System.Collections;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Data;

using log4net;
using log4net.Config;
using Contentum.eRecord.Service;
using System.Text;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public partial class KRT_FelhasznalokService : System.Web.Services.WebService
{
    public const string Header = "$Header: KRT_FelhasznalokService.cs, 44, 2017. 05. 30. 11:23:34, Németh Krisztina$";
    public const string Version = "$Revision: 44$";

    private static readonly ILog ilog = LogManager.GetLogger(typeof(Logger));

    private static string[] FelhasznalokList = null;

    private bool needPartnerCheck = true;

    private static void FelhasznalokListReset()
    {
        FelhasznalokList = null;
    }

    #region Generáltból átvett manuális WebService-ek

    /// <summary>
    /// Felhasznalo Insert, automatikus csoportgenerálással
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result Insert(ExecParam ExecParam, KRT_Felhasznalok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            //A hozzárendelt partner ellenõrzése
            if (needPartnerCheck)
            {
                result = this.CheckPartner(ExecParam, Record);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    //ContextUtil.SetAbort();
                    //result.Ds = null;
                    //result.Record = null;
                    //log.WsEnd(ExecParam, result);
                    //return result;

                    throw new ResultException(result);
                }
            }

            //Jelszó SHA1 hash-ének tárolása
            Record.Jelszo = Crypt.GetSha1Hash(Record.Jelszo);

            Result result_insert = sp.Insert(Constants.Insert, ExecParam, Record);
            if (!String.IsNullOrEmpty(result_insert.ErrorCode))
            {
                ////RollBack:
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_insert);
                //return result_insert;

                throw new ResultException(result_insert);
            }
            Record.Id = result_insert.Uid;

            //A felhasználóval azonos id-jú egyszemélyes csoport létrehozása
            KRT_CsoportokService krt_csoportokService = new KRT_CsoportokService(this.dataContext);
            KRT_Csoportok krt_csoportok = new KRT_Csoportok();
            krt_csoportok.Updated.SetValueAll(true);

            krt_csoportok.Id = result_insert.Uid;

            krt_csoportok.Org = Record.Org; // Org átvétele, fõleg új Org létrehozásakor fontos!

            krt_csoportok.Base.Tranz_id = Record.Base.Tranz_id; // ha a felhasználóra jegyeztük a tranz_id-t, akkor itt is átvesszük
            krt_csoportok.Base.Updated.Tranz_id = true;
            krt_csoportok.Nev = Contentum.eUtility.Felhasznalok.GetCsoportNev(ExecParam.Clone(), Record);
            krt_csoportok.Tipus = KodTarak.CSOPORTTIPUS.Dolgozo;
            krt_csoportok.ErtesitesEmail = Record.EMail;
            krt_csoportok.System = "1";
            krt_csoportok.Jogalany = "1";

            krt_csoportok.ErvKezd = Record.ErvKezd;
            krt_csoportok.Updated.ErvKezd = true;

            krt_csoportok.ErvVege = Record.ErvVege;
            krt_csoportok.Updated.ErvVege = true;

            Result result_csoport = krt_csoportokService.Insert(ExecParam, krt_csoportok);
            if (!String.IsNullOrEmpty(result_csoport.ErrorCode))
            {
                Logger.Debug("Csoport insert hiba");
                ////RollBack:
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_csoport);
                //return result_csoport;

                throw new ResultException(result_csoport);
            }

            Logger.Debug("Egyszemélyes csoport beszúrva");

            //A felhasználó hozzáadása a Mindenki csoporthoz

            // TODO: (HA a következõ mûvelet elõtt még folyamatban van a tranzakció
            // a mûvelet nem tud végrehajtódni, mert õ nincs benne a tranzakcióban --> timeout)
            // emiatt elõtte lezárjuk a tranzakciót
            // TODO: A GetMindenki() fv.-t nem eUtility-bõl kellene hívni, hanem itt megvalósítani


            Logger.Debug("Kapcsolt partner id:" + Record.Partner_id);

            //A felhasználóhoz tartozó partner kapcsolatainak leképzése a csoportkapcsolatokra
            if (!String.IsNullOrEmpty(Record.Partner_id))
            {
                KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
                foreach (KRT_CsoportTagok csoportTag in this.GetCsoportTagok(ExecParam, Record))
                {
                    result = service.InsertOrNotInsert(ExecParam, csoportTag);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {

                        //ContextUtil.SetAbort();
                        //log.WsEnd(ExecParam, result);
                        //return result;

                        throw new ResultException(result);
                    }
                }
            }

            #region FPH_Lekerdezo szerepkorhoz hozzáadás
            Logger.Debug("Default szerepkor hozzaadasa start");

            if (String.IsNullOrEmpty(Record.Org) || Record.Org.Equals(ExecParam.Org_Id, StringComparison.OrdinalIgnoreCase))
            {
                ExecParam xpmSzerepkor = ExecParam.Clone();

                KRT_SzerepkorokService svcSzerepkorok = new KRT_SzerepkorokService(this.dataContext);
                string defaultSzerepkorID = svcSzerepkorok.GetDefaultSzerepkorID(xpmSzerepkor);
                if (defaultSzerepkorID != null)
                {
                    Logger.Debug("Felhasznalo_Szerepkor insert start");
                    KRT_Felhasznalo_SzerepkorService svcFelhasznaloSzerepkorok = new KRT_Felhasznalo_SzerepkorService(this.dataContext);
                    KRT_Felhasznalo_Szerepkor felhaszanloSzerepkor = new KRT_Felhasznalo_Szerepkor();
                    felhaszanloSzerepkor.Felhasznalo_Id = Record.Id;
                    Logger.Debug("Felhasznalo_Id: " + felhaszanloSzerepkor.Felhasznalo_Id);
                    felhaszanloSzerepkor.Updated.Felhasznalo_Id = true;
                    felhaszanloSzerepkor.Szerepkor_Id = defaultSzerepkorID;
                    Logger.Debug("Szerepkor_Id: " + felhaszanloSzerepkor.Szerepkor_Id);
                    felhaszanloSzerepkor.Updated.Szerepkor_Id = true;
                    Result resFelhasznaloSzerepkor = svcFelhasznaloSzerepkorok.Insert(xpmSzerepkor, felhaszanloSzerepkor);
                    if (resFelhasznaloSzerepkor.IsError)
                    {
                        Logger.Error("Felhasznalo_Szerepkor insert hiba", xpmSzerepkor, resFelhasznaloSzerepkor);
                        throw new ResultException(resFelhasznaloSzerepkor);
                    }
                    Logger.Debug("Felhasznalo_Szerepkor insert end");
                }
            }
            else
            {
                Logger.Debug("Az új felhasználó nem az aktuális Org tagja, ezért nem rendelünk hozzá default szerepkört.");
            }
            Logger.Debug("Default szerepkor hozzadasa end");
            #endregion


            result = result_insert;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Felhasznalok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            //#region Mindenki csoport
            //dataContext.Transaction.Commit();

            //KRT_Csoportok csoportMinenki = Contentum.eUtility.Csoportok.GetMindenki(ExecParam);

            //Logger.Debug("Mindenki csoport: " + csoportMinenki.Nev + ";" + csoportMinenki.Id);

            //this.AddCsoport(ExecParam, csoportMinenki.Id, result_csoport.Uid);

            //Logger.Debug("Mindenki csoporthoz hozzáadva");
            //#endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// A felhasználóhoz tartozó egyszemélyes csoport update
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result Update(ExecParam ExecParam, KRT_Felhasznalok Record)
    {
        Logger.DebugStart();
        Logger.Debug("Felhasználó update elindul");
        Logger.Debug(String.Format("Execparam felhasználó id: {0},Record id: {1},Record Név: {2}", ExecParam.Felhasznalo_Id, Record.Id, Record.Nev));
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            //A hozzárendelt partner ellenõrzése
            if (needPartnerCheck)
            {
                Logger.Debug("Partner ellenõrzés");
                result = this.CheckPartner(ExecParam, Record);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    //ContextUtil.SetAbort();
                    //result.Ds = null;
                    //result.Record = null;
                    //log.WsEnd(ExecParam, result);
                    //return result;
                    Logger.Error(String.Format("Partner ellenõrzés hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                    throw new ResultException(result);
                }
            }

            //A felhasználóval azonos id-jú egyszemélyes csoport update
            Logger.Debug("A felhasználóhoz tartozó egyszemélyes csoport update elindul");
            Logger.Debug("A felhasználó verziója: " + Record.Base.Ver);
            ExecParam xpmCsoport = ExecParam.Clone();
            KRT_CsoportokService krt_csoportokService = new KRT_CsoportokService(this.dataContext);
            Logger.Debug("A felhasználóhoz tartozó egyszemélyes csoport lekérése elindul");
            Result resCsoportGet = krt_csoportokService.Get(xpmCsoport);
            if (!String.IsNullOrEmpty(resCsoportGet.ErrorCode))
            {
                Logger.Error(String.Format("A felhasználóhoz nem sikerült lekérni az egyszemélyes csoportot: {0},{1}", resCsoportGet.ErrorCode, resCsoportGet.ErrorMessage));
                throw new ResultException(resCsoportGet);
            }
            Logger.Debug("A felhasználóhoz tartozó egyszemélyes csoport lekérése vége");
            KRT_Csoportok krt_csoportok = (KRT_Csoportok)resCsoportGet.Record;

            if (krt_csoportok == null)
            {
                Logger.Error("A csoportok record null");
                throw new ResultException("A csoportok record null");
            }

            krt_csoportok.Updated.SetValueAll(false);

            krt_csoportok.Nev = Contentum.eUtility.Felhasznalok.GetCsoportNev(ExecParam.Clone(), Record);
            krt_csoportok.Updated.Nev = Record.Updated.Nev;
            krt_csoportok.ErtesitesEmail = Record.EMail;
            krt_csoportok.Updated.ErtesitesEmail = Record.Updated.EMail;
            krt_csoportok.ErvKezd = Record.ErvKezd;
            krt_csoportok.Updated.ErvKezd = Record.Updated.ErvKezd;
            krt_csoportok.ErvVege = Record.ErvVege;
            krt_csoportok.Updated.ErvVege = Record.Updated.ErvVege;

            Result result_csoport = krt_csoportokService.ForceUpdate(xpmCsoport, krt_csoportok);
            if (!String.IsNullOrEmpty(result_csoport.ErrorCode))
            {
                ////RollBack:
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_csoport);
                //return result_csoport;
                Logger.Error(String.Format("A felhasználóhoz tartozó egyszemélyes csoport update hiba: {0},{1}", result_csoport.ErrorCode, result_csoport.ErrorMessage));
                throw new ResultException(result_csoport);
            }

            Logger.Debug("A felhasználóhoz tartozó egyszemélyes csoport update vége");


            #region partner update
            //A felhasználóhoz tartozó személy partner update
            Logger.Debug("A felhasználóhoz tartozó személy update elindul");
            Logger.Debug("A partner id-ja: " + Record.Partner_id);

            KRT_PartnerekService krt_partnerekService = new KRT_PartnerekService(this.dataContext);

            Logger.Debug("A felhasználóhoz tartozó partner lekérése");

            KRT_PartnerSzemelyek krtPartnerSzemely = null;

            try
            {
                krtPartnerSzemely = krt_partnerekService.GetSzemelyByFelhasznalo(ExecParam, Record);
            }
            catch (ResultException ex)
            {
                Logger.Warn(String.Format("A felhasználóhoz tartozó partner lekérése sikertelen: {0}, {1}", ex.ErrorCode, ex.ErrorMessage));
            }

            if (krtPartnerSzemely != null)
            {
                Logger.Debug("A felhasználóhoz tartozik személy");
                Logger.Debug("A személy id-ja: " + krtPartnerSzemely.Id);
                ExecParam xpmPartner = ExecParam.Clone();
                xpmPartner.Record_Id = Record.Partner_id;
                krtPartnerSzemely.Updated.SetValueAll(false);
                krtPartnerSzemely.Szemelyek.Updated.SetValueAll(false);
                KRT_SzemelyekService.SetNevFromString(krtPartnerSzemely.Szemelyek, Record.Nev);
                krtPartnerSzemely.Szemelyek.Updated.UjTitulis = Record.Updated.Nev;
                krtPartnerSzemely.Szemelyek.Updated.UjCsaladiNev = Record.Updated.Nev;
                krtPartnerSzemely.Szemelyek.Updated.UjUtonev = Record.Updated.Nev;
                krtPartnerSzemely.Updated.Nev = Record.Updated.Nev;
                Result resPartner = krt_partnerekService.Update(xpmPartner, krtPartnerSzemely);

                if (!String.IsNullOrEmpty(resPartner.ErrorCode) && resPartner.ErrorMessage != "[50403]")
                {
                    Logger.Error(String.Format("A felhasználóhoz tartozó személy update hiba: {0}, {1}", resPartner.ErrorCode, resPartner.ErrorMessage));
                    throw new ResultException(resPartner);
                }

            }
            Logger.Debug("A felhasználóhoz tartozó személy update vége");

            #endregion


            //jelszó tárolása SHA1 hash értékként
            Record.Jelszo = Crypt.GetSha1Hash(Record.Jelszo);

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Felhasználó update hiba: {0}, {1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("Felhasználó update vége");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// A felhasználó rekord update 
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result UpdateFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok Record)
    {
        Logger.DebugStart();
        Logger.Debug("Felhasználó update elindul");
        Logger.Debug(String.Format("Execparam felhasználó id: {0},Record id: {1},Record Név: {2}", ExecParam.Felhasznalo_Id, Record.Id, Record.Nev));
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Felhasználó update hiba: {0}, {1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("Felhasználó update vége");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// A felhasználóhoz tartozó egyszemélyes csoport törlése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Logger.DebugStart();
        Logger.Debug("A felhasználó érvénytelenítése elindul");
        Logger.Debug("Az érvénytelenítendõ record id-ja: " + ExecParam.Record_Id);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // CR3328 Felhasználó érvénytelenítés elött ellenörzés, hogy van-e hozzá ugyirat, irat v küldemény rögzítve (õ a kezelõje) 
            Result resultCheck = this.CheckKapcsoltUgyek(ExecParam);
            if (String.IsNullOrEmpty(resultCheck.ErrorCode))
            {
                if (resultCheck.Ds.Tables[0].Rows.Count > 0)
                {
                    int ugyiratCount;
                    int iratCount;
                    int kuldemenyCount;
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Ugyiratok_count"].ToString(), out ugyiratCount);
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Iratok_count"].ToString(), out iratCount);
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Kuldemenyek_count"].ToString(), out kuldemenyCount);
                    if ((ugyiratCount + iratCount + kuldemenyCount) > 0)
                    {
                        resultCheck.ErrorMessage = resultCheck.Ds.Tables[0].Rows[0]["nev"].ToString() + " nem törölhetõ mert rendelkezik a rendszerben Ügyirattal (" + ugyiratCount.ToString() + " db), Irattal (" + iratCount.ToString() + " db) és/vagy Küldeménnyel (" + kuldemenyCount.ToString() + " db) !";
                        resultCheck.ErrorCode = "50001";
                        throw new ResultException(resultCheck);

                    }

                }

            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("A felhasználóhoz tartozó csoport érvénytelenítése elindul");
            KRT_CsoportokService csService = new KRT_CsoportokService(this.dataContext);
            csService.TryInvalidate(ExecParam);
            Logger.Debug("A felhasználóhoz tartozó csoport érvénytelenítése vége");

            #region partner érvénytelenítés
            //A felhasználóhoz tartozó személy partner érvénytelenítése
            Logger.Debug("A felhasználóhoz tartozó személy érvénytelenítése elindul");
            Logger.Debug("A felhasználóhoz tartozó személy meghatározása elindul");
            Result resGet = this.Get(ExecParam);
            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error(String.Format("A felhasználó lekérése sikertelen: {0},{1}", resGet.ErrorCode, resGet.ErrorMessage));
                throw new ResultException(resGet);
            }
            Logger.Debug("A felhasználóhoz tartozó személy meghatározása vége");
            KRT_Felhasznalok felhasznalo = (KRT_Felhasznalok)resGet.Record;
            if (String.IsNullOrEmpty(felhasznalo.Partner_id))
            {
                Logger.Warn("A felhasználóhoz nem tartozik partner");
            }
            else
            {

                Logger.Debug("A személy id-ja: " + felhasznalo.Partner_id);

                KRT_PartnerekService krt_partnerekService = new KRT_PartnerekService(this.dataContext);

                ExecParam xpmPartner = ExecParam.Clone();
                xpmPartner.Record_Id = felhasznalo.Partner_id;

                Result resPartner = krt_partnerekService.Invalidate(xpmPartner);

                //ha érvénytelen a record nem dobunk hibát
                if (!String.IsNullOrEmpty(resPartner.ErrorCode) && resPartner.ErrorMessage != "[50603]")
                {
                    Logger.Error(String.Format("A felhasználóhoz tartozó személy érvénytelenítés hiba: {0}, {1}", resPartner.ErrorCode, resPartner.ErrorMessage));
                    throw new ResultException(resPartner);
                }
            }

            Logger.Debug("A felhasználóhoz tartozó személy érvénytelenítés vége");

            #endregion


            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region ERTESITES
            KRT_FelhasznalokService svcFelh = new KRT_FelhasznalokService(this.dataContext);
            svcFelh.CallKapcsoltUgyekErtesitesKuldes(ExecParam, this.dataContext, KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloTorles);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("A felhasználó érvénytelenítése hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("A felhasználó érvénytelenítése vége");
        Logger.DebugEnd();
        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    [WebMethod()]
    public Result GetAllBySzerepkor(ExecParam ExecParam, KRT_Szerepkorok _KRT_Szerepkorok
        , KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch
        , KRT_FelhasznalokSearch _KRT_FelhasznalokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllBySzerepkor(ExecParam, _KRT_Szerepkorok
               , _KRT_Felhasznalo_SzerepkorSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetLastLoginTime(ExecParam execParam, string felhasznaloId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetLastLoginTime(execParam, felhasznaloId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    //public Result Test()
    //{
    //    ExecParam ExecParam = new ExecParam();
    //    return sp.Get(ExecParam);
    //    //ExecParam.Felhasznalo_Id = Guid.NewGuid().ToString();
    //    //KRT_FelhasznalokSearch KRT_FelhasznalokSearch = new KRT_FelhasznalokSearch();
    //    //KRT_FelhasznalokSearch.Partner_id.Value = "2ca3ca4e-6878-4e68-b244-425f65981271";
    //    //KRT_FelhasznalokSearch.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
    //    //return sp.GetAll(ExecParam, KRT_FelhasznalokSearch);
    //}

    /// <summary>
    /// Beszúr egy új felhasználót és létrehoz neki egy Partnert is
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="krt_felhasznalok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result InsertAndCreateNewPartner(ExecParam execParam, KRT_Felhasznalok krt_felhasznalok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("ExecParam felhasználó id:" + execParam.Felhasznalo_Id);
            Logger.Debug("Felhasználó neve: " + krt_felhasznalok.Nev);

            if (String.IsNullOrEmpty(krt_felhasznalok.Org))
            {
                krt_felhasznalok.Org = execParam.Org_Id;
            }


            KRT_PartnerekService krt_partnerekService = new KRT_PartnerekService(this.dataContext);
            KRT_PartnerSzemelyek krt_partnerek = new KRT_PartnerSzemelyek();

            krt_partnerek.Tipus = Contentum.eUtility.KodTarak.Partner_Tipus.Szemely;
            KRT_SzemelyekService.SetNevFromString(krt_partnerek.Szemelyek, krt_felhasznalok.Nev);
            //krt_partnerek.MaxMinosites = krt_felhasznalok.MaxMinosites;
            krt_partnerek.Base.Ver = "1";
            krt_partnerek.Belso = "1";

            krt_partnerek.ErvKezd = krt_felhasznalok.ErvKezd;
            krt_partnerek.Updated.ErvKezd = true;

            krt_partnerek.ErvVege = krt_felhasznalok.ErvVege;
            krt_partnerek.Updated.ErvVege = true;

            krt_partnerek.Org = krt_felhasznalok.Org; // Org átvétele, fõleg új Org létrehozásakor fontos!

            krt_partnerek.Base.Tranz_id = krt_felhasznalok.Base.Tranz_id; // ha a felhasználóra jegyeztük a tranz_id-t, akkor itt is átvesszük
            krt_partnerek.Base.Updated.Tranz_id = true;

            Result result_newpartner = krt_partnerekService.Insert(execParam, krt_partnerek);
            if (!String.IsNullOrEmpty(result_newpartner.ErrorCode) || String.IsNullOrEmpty(result_newpartner.Uid))
            {
                Logger.Debug("Partner insert hiba");
                ////RollBack:
                //ContextUtil.SetAbort();
                //log.WsEnd(execParam, result_newpartner);
                //return result_newpartner;

                throw new ResultException(result_newpartner);
            }
            else
            {
                Logger.Debug("Partner insert siker");
                this.needPartnerCheck = false;
                string generatedPartnerId = result_newpartner.Uid;
                krt_felhasznalok.Partner_id = generatedPartnerId;
                krt_felhasznalok.Updated.Partner_id = true;

                Result result_krt_felh = Insert(execParam, krt_felhasznalok);
                if (!String.IsNullOrEmpty(result_krt_felh.ErrorCode))
                {
                    ////RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam, result_krt_felh);
                    //return result_krt_felh;

                    throw new ResultException(result_krt_felh);
                }
                else
                {
                    result = result_krt_felh;
                }

                #region Eseménynaplózás
                if (isTransactionBeginHere)
                {
                    // partner
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, result_newpartner.Uid, "KRT_Partnerek", "PartnerNew").Record;

                    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);

                    // felhasználó
                    eventLogService = new KRT_EsemenyekService(this.dataContext);

                    eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, result_krt_felh.Uid, "KRT_Felhasznalok", "New").Record;

                    eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
                }
                #endregion

            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }



        log.WsEnd(execParam, result);
        return result;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFelhasznalokList(string prefixText, int count, string contextKey)
    {

        String userId = "";

        if (contextKey.IndexOf(";") > -1)
        {
            userId = contextKey.Substring(0, contextKey.IndexOf(";"));
        }
        else
        {
            userId = contextKey;
        }

        if (FelhasznalokList == null)
        {
            String[] Columns = new String[2];
            Columns[0] = "Nev";
            Columns[1] = "Email";

            KRT_FelhasznalokService Felhasznalokservice = new KRT_FelhasznalokService();
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            KRT_FelhasznalokSearch Felhasznaloksearch = new KRT_FelhasznalokSearch();

            Felhasznaloksearch.OrderBy = "Nev ASC";

            Result _res = Felhasznalokservice.GetAll(execparam, Felhasznaloksearch);
            if (String.IsNullOrEmpty(_res.ErrorCode))
            {
                FelhasznalokList = Utility.DataSourceColumnToStringArray(_res, Columns);
            }
            else
            {
                return null;
            }
        }

        return Utility.SearchInStringArrayByPrefix(FelhasznalokList, prefixText, count);

    }

    //[WebMethod()]
    //public string GetFelhasznalokEmailList(string keyword, bool usePaging, int pageIndex, int pageSize)
    //{
    //    ENTech.WebControls.AutoSuggestMenuItem menuItem;

    //    KRT_FelhasznalokService Felhasznalokservice = new KRT_FelhasznalokService();
    //    ExecParam execparam = new ExecParam();
    //    execparam.Felhasznalo_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";

    //    KRT_FelhasznalokSearch Felhasznaloksearch = new KRT_FelhasznalokSearch();

    //    // kene meg egy email szures is, ha nincsen ne hozza a listaba...
    //    Felhasznaloksearch.Nev.Operator = Contentum.eQuery.Query.Operators.like;
    //    Felhasznaloksearch.Nev.Value = keyword + "%";
    //    Felhasznaloksearch.OrderBy = "Nev ASC";

    //    Result _res = Felhasznalokservice.GetAll(execparam, Felhasznaloksearch);

    //    System.Collections.Generic.List<ENTech.WebControls.AutoSuggestMenuItem> menuItems = new System.Collections.Generic.List<ENTech.WebControls.AutoSuggestMenuItem>();

    //    for (int i = 0; i < _res.Ds.Tables[0].Rows.Count; i++)
    //    {
    //        menuItem = new ENTech.WebControls.AutoSuggestMenuItem();
    //        menuItem.Label = _res.Ds.Tables[0].Rows[i]["Nev"].ToString() + "  |  " + _res.Ds.Tables[0].Rows[i]["Email"].ToString();
    //        menuItem.Value = _res.Ds.Tables[0].Rows[i]["Id"].ToString();

    //        menuItems.Add(menuItem);
    //    }

    //    return ENTech.WebControls.AutoSuggestMenu.ConvertMenuItemsToJSON(menuItems, -1);
    //}

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result MultiDeletePartnerID(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            KRT_FelhasznalokService service = new KRT_FelhasznalokService(this.dataContext);
            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result resultFelhasznalo = service.Get(ExecParams[i]);
                if (!String.IsNullOrEmpty(resultFelhasznalo.ErrorCode))
                {
                    ////RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, resultFelhasznalo);
                    //return resultFelhasznalo;

                    throw new ResultException(resultFelhasznalo);
                }

                KRT_Felhasznalok record = (KRT_Felhasznalok)resultFelhasznalo.Record;

                //csoportkapcsolatok invalidálása
                if (!String.IsNullOrEmpty(record.Partner_id))
                {
                    foreach (KRT_CsoportTagok krt_csoportTag in this.GetCsoportTagok(ExecParams[i], record))
                    {
                        ExecParam execParamInvalidate = ExecParams[i].Clone();
                        execParamInvalidate.Record_Id = krt_csoportTag.Id;
                        KRT_CsoportTagokService serviceInvalidate = new KRT_CsoportTagokService(this.dataContext);
                        Result result_invalidate = serviceInvalidate.Invalidate(execParamInvalidate);
                        if (!String.IsNullOrEmpty(result_invalidate.ErrorCode) && result_invalidate.ErrorMessage != "[50603]")
                        {
                            //ContextUtil.SetAbort();
                            //log.WsEnd(execParamInvalidate, result_invalidate);
                            //return result_invalidate;

                            throw new ResultException(result_invalidate);
                        }
                    }
                }

                record.Typed.Partner_id = System.Data.SqlTypes.SqlGuid.Null;
                result = Update(ExecParams[i], record);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    //RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, result);
                    //return result;

                    throw new ResultException(result);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerSzemelyek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerVallalkozasok))]
    public Result AddPartner(ExecParam ExecParam, KRT_Felhasznalok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = this.CheckPartner(ExecParam, Record);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //result.Ds = null;
                //result.Record = null;
                //log.WsEnd(ExecParam, result);
                //return result;

                throw new ResultException(result);
            }

            Result result_add = sp.Insert(Constants.Update, ExecParam, Record);
            if (!String.IsNullOrEmpty(result_add.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_add);
                //return result_add;

                throw new ResultException(result_add);
            }

            //A felhasználó csoportjának hozzáadása a kapcsolt partnerekhez
            if (!String.IsNullOrEmpty(Record.Partner_id))
            {
                KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
                foreach (KRT_CsoportTagok csoportTag in this.GetCsoportTagok(ExecParam, Record))
                {
                    result = service.InsertOrNotInsert(ExecParam, csoportTag);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {

                        //ContextUtil.SetAbort();
                        //log.WsEnd(ExecParam, result);
                        //return result;

                        throw new ResultException(result);
                    }
                }
            }

            result = result_add;

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public void AddCsoport(ExecParam execParam, string idFelettes, string idJogalany)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            KRT_CsoportTagok csoportTagok = new KRT_CsoportTagok();
            csoportTagok.Updated.SetValueAll(false);
            csoportTagok.Csoport_Id = idFelettes;
            csoportTagok.Updated.Csoport_Id = true;
            csoportTagok.Csoport_Id_Jogalany = idJogalany;
            csoportTagok.Updated.Csoport_Id_Jogalany = true;
            csoportTagok.System = Contentum.eUtility.Constants.Database.Yes;
            csoportTagok.Updated.System = true;
            KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
            result = service.Insert(execParam, csoportTagok);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

    }

    public IEnumerable<KRT_CsoportTagok> GetCsoportTagok(ExecParam execParam, KRT_Felhasznalok Record)
    {
        KRT_PartnerKapcsolatokService service = new KRT_PartnerKapcsolatokService();
        KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
        search.Partner_id_kapcsolt.Value = Record.Partner_id;
        search.Partner_id_kapcsolt.Operator = Query.Operators.equals;

        Result res = service.GetAll(execParam, search);
        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                KRT_CsoportTagok krt_Csoportagok = new KRT_CsoportTagok();
                krt_Csoportagok.Updated.SetValueAll(false);
                krt_Csoportagok.Id = row["Id"].ToString();
                krt_Csoportagok.Updated.Id = true;
                ExecParam execParamCsoport = execParam.Clone();
                execParamCsoport.Record_Id = row["Partner_id"].ToString();
                KRT_Csoportok csoport = service.GetCsoport(execParamCsoport);
                if (csoport != null)
                {
                    krt_Csoportagok.Csoport_Id = csoport.Id;
                    krt_Csoportagok.Updated.Csoport_Id = true;
                    krt_Csoportagok.Csoport_Id_Jogalany = Record.Id;
                    krt_Csoportagok.Updated.Csoport_Id_Jogalany = true;
                    krt_Csoportagok.System = Contentum.eUtility.Constants.Database.Yes;
                    krt_Csoportagok.Updated.System = true;
                    krt_Csoportagok.ErvKezd = row["ErvKezd"].ToString();
                    krt_Csoportagok.Updated.ErvKezd = true;
                    krt_Csoportagok.ErvVege = row["ErvVege"].ToString();
                    krt_Csoportagok.Updated.ErvVege = true;
                    krt_Csoportagok.Base = Record.Base;
                    krt_Csoportagok.Base.Updated = Record.Base.Updated;
                    yield return krt_Csoportagok;
                }
            }
        }
        else
        {
            throw new ResultException(res);
        }


    }

    public Result CheckPartner(ExecParam execParam, KRT_Felhasznalok Record)
    {
        Result result = new Result();
        //Nincs hozzárendelt partner, nem kell ellenõrizni
        if (String.IsNullOrEmpty(Record.Partner_id))
        {
            return result;
        }

        KRT_PartnerekService service = new KRT_PartnerekService();
        ExecParam execParamPartner = execParam.Clone();
        execParamPartner.Record_Id = Record.Partner_id;
        result = service.Get(execParamPartner);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return result;
        }

        KRT_Partnerek partner = (KRT_Partnerek)result.Record;

        //partner nem található
        if (partner == null)
        {
            result.ErrorCode = "[53100]";
            result.ErrorMessage = "[53100]";
            return result;
        }
        //partner nem személy
        if (partner.Tipus != Contentum.eUtility.KodTarak.Partner_Tipus.Szemely)
        {
            result.ErrorCode = result.ErrorMessage = "[53103]";
            return result;
        }
        //parnter nem belsõ
        if (partner.Belso != Contentum.eUtility.Constants.Database.Yes)
        {
            result.ErrorCode = result.ErrorMessage = "[53101]";
            return result;
        }

        KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
        search.Partner_id.Value = Record.Partner_id;
        search.Partner_id.Operator = Query.Operators.equals;
        result = this.GetAll(execParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return result;
        }

        //A partnerhez már tartozik felhasználó
        if (result.Ds.Tables[0].Rows.Count > 0)
        {
            if (String.IsNullOrEmpty(execParam.Record_Id) || result.Ds.Tables[0].Rows[0]["Id"].ToString() != execParam.Record_Id)
            {
                result.ErrorCode = result.ErrorMessage = "[53104]";
                return result;
            }
        }

        return result;

    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFelhasznaloList(string prefixText, int count, string contextKey)
    {
        string[] parameters = contextKey.Split(';');
        String userId = parameters[0];

        string filterType = String.Empty;

        if (parameters.Length > 1)
        {
            filterType = parameters[1];
        }


        KRT_FelhasznalokService csoportkService = new KRT_FelhasznalokService();
        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;

        KRT_FelhasznalokSearch FelhasznalokSearch = new KRT_FelhasznalokSearch();

        FelhasznalokSearch.OrderBy = " KRT_Felhasznalok.Nev " + Utility.customCollation + " ASC, KRT_Felhasznalok.Id";

        if (!String.IsNullOrEmpty(prefixText))
        {
            FelhasznalokSearch.Nev.Value = prefixText + '%';
            FelhasznalokSearch.Nev.Operator = Query.Operators.like;
            FelhasznalokSearch.TopRow = count;
            Result res = csoportkService.GetAll(execparam, FelhasznalokSearch);

            if (filterType == Contentum.eUtility.Constants.FilterType.Felhasznalok.WithEmail)
            {
                if (!res.IsError)
                {
                    foreach (DataRow row in res.Ds.Tables[0].Rows)
                    {
                        row["Nev"] = Contentum.eUtility.Felhasznalok.GetFelhasznaloNevWithEmail(row);
                    }
                }
            }

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                return Utility.DataSourceColumnToStringPairArray(res, "Id", "Nev");
            }
        }

        return null;

    }

    [WebMethod(Description = "Felhasználó keresése email cím alapján.")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result GetByEmail(ExecParam ExecParam, string p_email)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        log4net.Config.XmlConfigurator.Configure();
        ilog.Debug("p_email: " + p_email);

        KRT_FelhasznalokSearch FelhasznalokSearch = new KRT_FelhasznalokSearch();
        FelhasznalokSearch.EMail.Value = p_email;
        FelhasznalokSearch.EMail.Operator = Contentum.eQuery.Query.Operators.like;

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetAll(ExecParam, FelhasznalokSearch);
            if (result.Ds.Tables[0].Rows.Count == 0)
            {
                throw new ResultException(55885);//Ehhez az email címhez nem található felhasználó!
            }
            KRT_Felhasznalok fh = new KRT_Felhasznalok();
            Utility.LoadBusinessDocumentFromDataRow(fh, result.Ds.Tables[0].Rows[0]);

            result.Record = fh;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);

        return result;
    }

    [WebMethod()]
    public Result Revalidate(ExecParam ExecParam)
    {
        Logger.Debug("A felhasználó revalidalasa elindul");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Revalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            Logger.Debug("A felhasználóhoz tartozó csoport revalidalasa elindul");
            KRT_CsoportokService service = new KRT_CsoportokService(this.dataContext);
            Result res = service.Revalidate(ExecParam);
            if (res.IsError)
            {
                //ha már érvényes a csoport nem dobunk hibát
                if (res.ErrorMessage != "[50653]")
                {
                    Logger.Error("A felhasználóhoz tartozó csoport revalidalasa hiba", ExecParam, res);
                    throw new ResultException(res);
                }
            }
            Logger.Debug("A felhasználóhoz tartozó csoport revalidalasa vége");

            #region partner érvénytelenítés
            //A felhasználóhoz tartozó személy partner érvénytelenítése
            Logger.Debug("A felhasználóhoz tartozó személy revalidalasa elindul");
            Logger.Debug("A felhasználó lekérése elindul");
            Result resGet = this.Get(ExecParam);
            if (resGet.IsError)
            {
                Logger.Error("A felhasználó lekérése hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }
            Logger.Debug("A felhasználó lekérése vége");
            KRT_Felhasznalok felhasznalo = (KRT_Felhasznalok)resGet.Record;
            if (String.IsNullOrEmpty(felhasznalo.Partner_id))
            {
                Logger.Warn("A felhasználóhoz nem tartozik partner");
            }
            else
            {

                Logger.Debug("A személy id-ja: " + felhasznalo.Partner_id);

                KRT_PartnerekService krt_partnerekService = new KRT_PartnerekService(this.dataContext);

                ExecParam xpmPartner = ExecParam.Clone();
                xpmPartner.Record_Id = felhasznalo.Partner_id;

                Result resPartner = krt_partnerekService.Revalidate(xpmPartner);

                //ha érvénytelen a record nem dobunk hibát
                if (resPartner.IsError)
                {
                    //ha már érvényes a partner nem dobunk hibát
                    if (resPartner.ErrorMessage != "[50653]")
                    {
                        Logger.Error("A felhasználóhoz tartozó személy revalidalasa hiba", xpmPartner, resPartner);
                        throw new ResultException(resPartner);
                    }
                }
            }

            Logger.Debug("A felhasználóhoz tartozó személy revalidalasa vége");

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Felhasznalok", "Revalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("A felhasználó revalidalasa hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("A felhasználó revalidalasa vége");
        return result;
    }

    [WebMethod(Description = "Lekéri adott feltétel mellett (pl. Org-tól függõen vagy éppen függetlenül) a felhasználó neveket."
        + " Használható pl. felhasználó név egyediség elõzetes ellenõrzéséhez.")]
    public Result GetAllUserNev(ExecParam ExecParam, KRT_FelhasznalokSearch _KRT_FelhasznalokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllUserNev(ExecParam, _KRT_FelhasznalokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #region KAPCSOLT UGYEK
    // CR3328 Felhasználó érvénytelenítés elött ellenörzés, hogy van-e hozzá ugyirat, irat v küldemény rögzítve (õ a kezelõje) 
    // ExecParam.Record_Id az ellenõrizendõ felhasználó Id-ja
    [WebMethod()]
    public Result CheckKapcsoltUgyek(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.CheckKapcsoltUgyek(execParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    [WebMethod(Description = "felhasználóhoz kapcsolt ügyek listája. Execparam.Id-ban kell megadni a felhasznalo id-t.")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalo_SzerepkorSearch))]
    public Result GetAllKapcsoltUgyek(ExecParam execParam)
    {
        return GetAllKapcsoltUgyekInternal(execParam, this.dataContext);
    }

    public Result GetAllKapcsoltUgyekInternal(ExecParam execParam, DataContext context)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        #region VARIABLES
        Result result = new Result();
        result.Ds = new DataSet();

        Result resultFELH = new Result();
        Result resultUGY = new Result();
        Result resultIRAT = new Result();
        Result resultKULD = new Result();
        Result resultJOGOS = new Result();
        Result resultCSOP = new Result();

        KRT_FelhasznalokService serviceFELH;

        EREC_UgyUgyiratokService serviceUGY;
        EREC_UgyUgyiratokSearch srcUgy;

        EREC_IraIratokService serviceIRAT;
        EREC_IraIratokSearch srcIRAT;

        EREC_KuldKuldemenyekService serviceKULD;
        EREC_KuldKuldemenyekSearch srcKULD;

        KRT_JogosultakService serviceJOGOS;
        KRT_JogosultakSearch srcJOGOS;

        KRT_CsoportokService serviceCSOP;

        DataTable resultDataTable = KapcsoltUgyek_CreateDataTable();
        string nev = string.Empty;
        #endregion

        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region FIND NEV

            #region FELHASZANLO
            serviceFELH = new KRT_FelhasznalokService(context);
            resultFELH = serviceFELH.Get(execParam);
            if (string.IsNullOrEmpty(resultFELH.ErrorCode))
            {
                KRT_Felhasznalok felhasznalok = resultFELH.Record as KRT_Felhasznalok;
                nev = felhasznalok.Nev + " [FELHASZNÁLÓ]";
            }
            else
            {
                #region CSOPORT
                serviceCSOP = new KRT_CsoportokService(context);
                resultCSOP = serviceCSOP.Get(execParam);
                if (string.IsNullOrEmpty(resultCSOP.ErrorCode))
                {
                    KRT_Csoportok csoport = resultCSOP.Record as KRT_Csoportok;
                    nev = csoport.Nev + " [CSOPORT]";
                }
                #endregion
            }

            #endregion

            #endregion

            #region [EREC_UgyUgyiratok]
            serviceUGY = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            srcUgy = new EREC_UgyUgyiratokSearch();
            srcUgy.Csoport_Id_Felelos.Value = execParam.Record_Id;
            srcUgy.Csoport_Id_Felelos.Operator = Query.Operators.equals;


            if (serviceUGY == null)
            {
                Logger.Debug("GetAllKapcsoltUgyekInternal.ERROR=ServiceFactory.GetEREC_UgyUgyiratokService.Init problem");
            }
            else
            {
                resultUGY = serviceUGY.GetAll(execParam, srcUgy);
                if (!String.IsNullOrEmpty(resultUGY.ErrorCode))
                    return resultUGY;

                if (resultUGY != null && resultUGY.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultUGY.Ds.Tables[0].Rows)
                    {
                        KapcsoltUgyek_AddToDataTable(ref resultDataTable, row["Id"].ToString(), row["Azonosito"].ToString(), "Ugyiratok", nev, null);
                    }
                }
            }
            #endregion

            #region [EREC_IraIratok]
            serviceIRAT = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            srcIRAT = new EREC_IraIratokSearch();
            srcIRAT.Csoport_Id_Felelos.Value = execParam.Record_Id;
            srcIRAT.Csoport_Id_Felelos.Operator = Query.Operators.equals;

            if (serviceIRAT == null)
            {
                Logger.Debug("GetAllKapcsoltUgyekInternal.ERROR=ServiceFactory.GetEREC_IraIratokService.Init problem");
            }
            else
            {
                resultIRAT = serviceIRAT.GetAll(execParam, srcIRAT);
                if (!String.IsNullOrEmpty(resultIRAT.ErrorCode))
                    return resultIRAT;

                if (resultIRAT != null && resultIRAT.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultIRAT.Ds.Tables[0].Rows)
                    {
                        KapcsoltUgyek_AddToDataTable(ref resultDataTable, row["Id"].ToString(), row["Azonosito"].ToString(), "Iratok", nev, null);
                    }
                }
            }
            #endregion

            #region [EREC_KuldKuldemenyek]
            serviceKULD = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            srcKULD = new EREC_KuldKuldemenyekSearch();
            srcKULD.Csoport_Id_Felelos.Value = execParam.Record_Id;
            srcKULD.Csoport_Id_Felelos.Operator = Query.Operators.equals;

            if (serviceKULD == null)
            {
                Logger.Debug("GetAllKapcsoltUgyekInternal.ERROR=ServiceFactory.GetEREC_KuldKuldemenyekService.Init problem");
            }
            else
            {
                resultKULD = serviceKULD.GetAll(execParam, srcKULD);
                if (!String.IsNullOrEmpty(resultKULD.ErrorCode))
                    return resultKULD;

                if (resultKULD != null && resultKULD.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultKULD.Ds.Tables[0].Rows)
                    {
                        KapcsoltUgyek_AddToDataTable(ref resultDataTable, row["Id"].ToString(), row["Azonosito"].ToString(), "Kuldemenyek", nev, null);
                    }
                }
            }
            #endregion

            #region [KRT_Jogosultak]
            serviceJOGOS = new KRT_JogosultakService(context);
            srcJOGOS = new KRT_JogosultakSearch();
            srcJOGOS.Csoport_Id_Jogalany.Value = execParam.Record_Id;
            srcJOGOS.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            resultJOGOS = serviceJOGOS.GetAll(execParam, srcJOGOS);
            if (string.IsNullOrEmpty(resultJOGOS.ErrorCode) && resultJOGOS != null && resultJOGOS.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in resultJOGOS.Ds.Tables[0].Rows)
                {
                    #region UGYIRAT
                    if (serviceUGY != null)
                    {
                        ExecParam exexcParamJOGUGY = execParam.Clone();
                        exexcParamJOGUGY.Record_Id = row["Obj_Id"].ToString();
                        resultUGY = serviceUGY.Get(exexcParamJOGUGY);

                        if (string.IsNullOrEmpty(resultUGY.ErrorCode) && resultUGY != null && resultUGY.Record != null && resultUGY.Record is EREC_UgyUgyiratok)
                        {
                            EREC_UgyUgyiratok ugyJOG = resultUGY.Record as EREC_UgyUgyiratok;
                            KapcsoltUgyek_AddToDataTable(ref resultDataTable, ugyJOG.Id, ugyJOG.Azonosito, "Jogosultak.Ugyirat", nev, null);
                        }
                    }
                    #endregion

                    #region IRAT
                    if (serviceIRAT != null)
                    {
                        ExecParam exexcParamJOGIRAT = execParam.Clone();
                        exexcParamJOGIRAT.Record_Id = row["Obj_Id"].ToString();
                        resultIRAT = serviceIRAT.Get(exexcParamJOGIRAT);

                        if (string.IsNullOrEmpty(resultIRAT.ErrorCode) && resultIRAT != null && resultIRAT.Record != null && resultIRAT.Record is EREC_IraIratok)
                        {
                            EREC_IraIratok iratJOG = resultIRAT.Record as EREC_IraIratok;
                            KapcsoltUgyek_AddToDataTable(ref resultDataTable, iratJOG.Id, iratJOG.Azonosito, "Jogosultak.Irat", nev, null);
                        }
                    }
                    #endregion

                    #region KULDEMENY
                    if (serviceKULD != null)
                    {
                        ExecParam exexcParamJOGKULD = execParam.Clone();
                        exexcParamJOGKULD.Record_Id = row["Obj_Id"].ToString();
                        resultKULD = serviceKULD.Get(exexcParamJOGKULD);
                        if (string.IsNullOrEmpty(resultKULD.ErrorCode) && resultKULD != null && resultKULD.Record != null && resultKULD.Record is EREC_KuldKuldemenyek)
                        {
                            EREC_KuldKuldemenyek kuldJOG = resultKULD.Record as EREC_KuldKuldemenyek;
                            KapcsoltUgyek_AddToDataTable(ref resultDataTable, kuldJOG.Id, kuldJOG.Azonosito, "Jogosultak.Kuldemenyek", nev, null);
                        }
                    }
                    #endregion
                }
            }

            #endregion

            result.Ds.Tables.Add(resultDataTable);
        }
        catch (Exception e)
        {
            Logger.Debug("GetAllKapcsoltUgyekInternal.ERROR=" + e.Message + e.StackTrace);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// KapcsoltUgyekErtesitesKuldes
    /// </summary>
    /// <param name="execParam">Execparam.Id-ban kell megadni a felhasznalo id-t.</param>
    /// <param name="ertesitesTipus">Kodtarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS</param>
    /// <returns></returns>
    [WebMethod(Description = "E-mail értesítés a kapcsolt ügyek felülvizsgálatáról. Execparam.Id-ban kell megadni a felhasznalo id-t.")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalo_SzerepkorSearch))]
    public Result KapcsoltUgyekErtesitesKuldes(ExecParam execParam, string ertesitesTipus)
    {
        return KapcsoltUgyekErtesitesKuldesInternal(execParam, this.dataContext, ertesitesTipus);
    }

    public Result KapcsoltUgyekErtesitesKuldesInternal(ExecParam execParam, DataContext context, string ertesitesTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        bool isConnectionOpenHere = false;
        Result result = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            Result resultKapcsoltUgyekLista = GetAllKapcsoltUgyekInternal(execParam, context);
            if (!String.IsNullOrEmpty(resultKapcsoltUgyekLista.ErrorCode))
                return resultKapcsoltUgyekLista;

            if (resultKapcsoltUgyekLista.Ds.Tables[0] == null || resultKapcsoltUgyekLista.Ds.Tables[0].Rows.Count < 1)
            {
                Logger.Debug(string.Format("Nincsenek kapcsolt ugyek. Felhasznalo={0}", execParam.Record_Id));
                return result;
            }

            string sablon = null;
            string targy = null;
            if (ertesitesTipus == KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloTorles)
            {
                Result resultSablon = GetKrtParameterItem(execParam.Clone(), Rendszerparameterek.FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON);
                sablon = GetKrtParameterNoteValue(resultSablon);
                targy = Rendszerparameterek.Get(execParam, Rendszerparameterek.FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY);
            }
            else if (ertesitesTipus == KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloCsoportTagsagTorles)
            {
                Result resultSablon = GetKrtParameterItem(execParam.Clone(), Rendszerparameterek.FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON);
                sablon = GetKrtParameterNoteValue(resultSablon);
                targy = Rendszerparameterek.Get(execParam, Rendszerparameterek.FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY);
            }
            else
            {
                throw new ArgumentException("Helytelen bejövő paraméter: Értesítés típus");
            }

            string sablonTartalom = GetKapcsoltUgyekErtesitesSablonTartalom(resultKapcsoltUgyekLista.Ds.Tables[0]);
            string levelTorzs = string.Format(sablon, sablonTartalom);

            #region PARAMETEREK
            string alkalmazasgazdaEmailCim = Rendszerparameterek.Get(execParam, Rendszerparameterek.ALKALMAZASGAZDA_EMAIL_CIM);
            string messageFrom = Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);
            string emailAlkalmazasGazda = Rendszerparameterek.Get(execParam, Rendszerparameterek.ALKALMAZASGAZDA_EMAIL_CIM);
            #endregion

            #region CIMZETTEK
            List<string> cimzettek = new List<string>();
            if (!string.IsNullOrEmpty(alkalmazasgazdaEmailCim))
                cimzettek.Add(alkalmazasgazdaEmailCim);

            #region FELHASZNALO ADATOK
            KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
            KRT_FelhasznalokService serviceFELH = new KRT_FelhasznalokService(this.dataContext);
            Result resultFELH = serviceFELH.Get(execParam);
            if (string.IsNullOrEmpty(resultFELH.ErrorCode))
            {
                felhasznalo = resultFELH.Record as KRT_Felhasznalok;
                cimzettek.Add(felhasznalo.EMail);
            }
            #endregion

            #endregion

            #region MAIL
            EmailService emailService = new EmailService();
            bool ret = emailService.SendEmail(execParam, messageFrom, cimzettek.ToArray(), targy, null, null, true, levelTorzs);

            if (!ret)
                throw new Exception("Sikertelen email küldés !");
            #endregion

        }
        catch (Exception e)
        {
            Logger.Debug("KapcsoltUgyekErtesitesKuldesInternal.ERROR=" + e.Message + e.StackTrace);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// CallKapcsoltUgyekErtesitesKuldes
    /// </summary>
    /// <param name="execParam">ExecParam.RecordId = KRT_CsoportTagok.Id, Krt_Felhasznalok.Id</param>
    /// <param name="ertesitesTipus">KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS</param>
    public void CallKapcsoltUgyekErtesitesKuldes(ExecParam execParam, DataContext context, string ertesitesTipus)
    {
        string felhasznaloId = null;
        if (ertesitesTipus == KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloTorles)
        {
            felhasznaloId = execParam.Record_Id;
        }
        else if (ertesitesTipus == KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloCsoportTagsagTorles)
        {
            KRT_CsoportTagokService svc = new KRT_CsoportTagokService(context);
            Result result = svc.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("CallKapcsoltUgyekErtesitesKuldes", execParam, result);
                return;
            }

            if (!(result.Record is KRT_CsoportTagok))
            {
                Logger.Error("CallKapcsoltUgyekErtesitesKuldes", execParam, result);
                return;
            }
            KRT_CsoportTagok item = result.Record as KRT_CsoportTagok;
            felhasznaloId = item.Csoport_Id_Jogalany;
        }
        else
        {
            throw new ArgumentException("Helytelen bejövő paraméter: Értesítés típus");
        }

        KRT_FelhasznalokService svcFelh = new KRT_FelhasznalokService(context);
        ExecParam execParamErtesites = execParam.Clone();
        execParamErtesites.Record_Id = felhasznaloId;
        svcFelh.KapcsoltUgyekErtesitesKuldesInternal(execParamErtesites, context, KodTarak.KAPCSOLT_UGYEK_ERTESITES_TIPUS.FelhasznaloCsoportTagsagTorles);
    }

    #region HELPER
    /// <summary>
    /// KapcsoltUgyek_CreateDataTable
    /// </summary>
    /// <returns></returns>
    private DataTable KapcsoltUgyek_CreateDataTable()
    {
        DataTable dt = new DataTable();

        #region COLUMNS
        DataColumn column1 = new DataColumn();
        column1.DataType = System.Type.GetType("System.Guid");
        column1.ColumnName = "Id";
        dt.Columns.Add(column1);

        DataColumn column2 = new DataColumn();
        column2.DataType = System.Type.GetType("System.String");
        column2.ColumnName = "Azonosito";
        dt.Columns.Add(column2);

        DataColumn column3 = new DataColumn();
        column3.DataType = System.Type.GetType("System.String");
        column3.ColumnName = "Tipus";
        dt.Columns.Add(column3);

        DataColumn column3b = new DataColumn();
        column3b.DataType = System.Type.GetType("System.String");
        column3b.ColumnName = "FelhasznaloNev";
        dt.Columns.Add(column3b);

        DataColumn column4 = new DataColumn();
        column4.DataType = System.Type.GetType("System.String");
        column4.ColumnName = "Megjegyzes";
        dt.Columns.Add(column4);

        #endregion
        return dt;
    }
    /// <summary>
    /// KapcsoltUgyek_AddToDataTable
    /// </summary>
    /// <param name="dataTable"></param>
    /// <param name="id"></param>
    /// <param name="azonosito"></param>
    /// <param name="tipus"></param>
    /// <param name="felhasznaloNev"></param>
    /// <param name="megjegyzes"></param>
    private void KapcsoltUgyek_AddToDataTable(ref DataTable dataTable, string id, string azonosito, string tipus, string felhasznaloNev, string megjegyzes)
    {
        DataRow dr = dataTable.NewRow();

        dr["Id"] = id;
        dr["Azonosito"] = azonosito;
        dr["Tipus"] = tipus;
        dr["FelhasznaloNev"] = felhasznaloNev;
        dr["Megjegyzes"] = megjegyzes;
        dataTable.Rows.Add(dr);
    }
    /// <summary>
    /// GetKapcsoltUgyekErtesitesSablonTartalom
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    private string GetKapcsoltUgyekErtesitesSablonTartalom(DataTable dt)
    {
        if (dt == null || dt.Rows.Count < 1)
            return string.Empty;

        StringBuilder sb = new StringBuilder();
        foreach (DataRow row in dt.Rows)
        {
            sb.Append("<tr style='border - bottom:1pt solid black;'>");
            //sb.Append("<td style=\"border: 1px solid black; padding-left: 5px; padding-right: 5px;\">");
            //sb.Append(row["Id"].ToString());
            //sb.Append("</td>");

            sb.Append("<td style=\"border: 1px solid black; padding-left: 5px; padding-right: 5px;\">");
            sb.Append(row["FelhasznaloNev"].ToString());
            sb.Append("</td>");

            sb.Append("<td style=\"border: 1px solid black; padding-left: 5px; padding-right: 5px;\">");
            sb.Append(row["Tipus"].ToString());
            sb.Append("</td>");

            sb.Append("<td style=\"border: 1px solid black; padding-left: 5px; padding-right: 5px;\">");
            sb.Append(row["Azonosito"].ToString());
            sb.Append("</td>");

            sb.Append("</tr>");
            sb.Append(Environment.NewLine);
        }
        return sb.ToString();
    }

    private Result GetKrtParameterItem(ExecParam execParam, string kodNev)
    {
        KRT_ParameterekService service = new KRT_ParameterekService();
        KRT_ParameterekSearch src = new KRT_ParameterekSearch();
        src.Nev.Value = kodNev;
        src.Nev.Operator = Query.Operators.equals;
        src.TopRow = 1;

        Result result = service.GetAll(execParam, src);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("GetKrtParameterItem", execParam, result);
            return null;
        }
        return result;
    }
    /// <summary>
    /// GetKrtParameterNoteValue
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    private string GetKrtParameterNoteValue(Result result)
    {
        if (result == null || !string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
            throw new ResultException(result);

        return result.Ds.Tables[0].Rows[0]["Note"].ToString();
    }
    #endregion HELPER

    #endregion KAPCSOLT UGYEK
}