﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_Felhasznalok_Halozati_NyomtatoiService : System.Web.Services.WebService
{

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result GetByNyomtato(ExecParam execParam, string nyomtatoId)
    {
        execParam.Record_Id = nyomtatoId;
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetByNyomtato(execParam);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}