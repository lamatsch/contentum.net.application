using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Xml;
using System.IO;
using System.Data;
using System.Web.Script.Services;
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_FunkciokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvett manu�lis fv.-ek



    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Funkciok))]
    public Result Update(ExecParam ExecParam, KRT_Funkciok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // m�dos�that�s�g ellen�rz�se:
            ExecParam execParam_Get = ExecParam.Clone();
            Result result_Get = Get(execParam_Get);
            if (!String.IsNullOrEmpty(result_Get.ErrorCode))
            {
                // hiba
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_Get);
                //return result_Get;

                throw new ResultException(result_Get);
            }
            else
            {
                KRT_Funkciok krt_funkciok = (KRT_Funkciok)result_Get.Record;
                if (krt_funkciok.Modosithato != "1")
                {
                    // nem m�dos�that�
                    //ContextUtil.SetAbort();
                    //Result result = ResultError.CreateNewResultWithErrorCode(50404);
                    //log.WsEnd(ExecParam, result);
                    //return result;

                    throw new ResultException(50404);
                }
                else
                {
                    result = sp.Insert(Constants.Update, ExecParam, Record);

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        throw new ResultException(result);
                    }
                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Funkciok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Funkciok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // m�dos�that�s�g ellen�rz�se:
            ExecParam execParam_Get = ExecParam.Clone();
            Result result_Get = Get(execParam_Get);
            if (!String.IsNullOrEmpty(result_Get.ErrorCode))
            {
                // hiba
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_Get);
                //return result_Get;

                throw new ResultException(result_Get);
            }
            else
            {
                KRT_Funkciok krt_funkciok = (KRT_Funkciok)result_Get.Record;
                if (krt_funkciok.Modosithato != "1")
                {
                    // nem m�dos�that�
                    //ContextUtil.SetAbort();
                    //Result result = ResultError.CreateNewResultWithErrorCode(50404);
                    //log.WsEnd(ExecParam, result);
                    //return result;

                    throw new ResultException(50404);
                }
                else
                {

                    result = sp.Invalidate(ExecParam);

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        throw new ResultException(result);
                    }

                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Funkciok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Funkciok))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // m�dos�that�s�g ellen�rz�se:
            ExecParam execParam_Get = ExecParam.Clone();
            Result result_Get = Get(execParam_Get);
            if (!String.IsNullOrEmpty(result_Get.ErrorCode))
            {
                // hiba
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_Get);
                //return result_Get;

                throw new ResultException(result_Get);
            }
            else
            {
                KRT_Funkciok krt_funkciok = (KRT_Funkciok)result_Get.Record;
                if (krt_funkciok.Modosithato != "1")
                {
                    // nem m�dos�that�
                    //ContextUtil.SetAbort();
                    //Result result = ResultError.CreateNewResultWithErrorCode(50404);
                    //log.WsEnd(ExecParam, result);
                    //return result;

                    throw new ResultException(50404);
                }
                else
                {
                    result = sp.Delete(ExecParam);

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        throw new ResultException(result);
                    }

                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_FunkciokSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_FunkciokSearch _KRT_FunkciokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_FunkciokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_FunkciokSearch, new KRT_FunkciokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Funkciok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_FunkciokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_FunkciokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_FunkciokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [WebMethod(Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_FunkciokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_FunkciokSearch _KRT_FunkciokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_FunkciokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_FunkciokSearch, new KRT_FunkciokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Funkciok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_FunkciokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_FunkciokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_FunkciokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Felhasznalok.BaseTyped)), System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Funkciok.BaseTyped))]
    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok, string Helyettesites_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, Helyettesites_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// A csoporttags�g alapj�n lesz�ri a saj�t jog�, aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat. Azon szerepk�r hozz�rendel�seket is figyelembe veszi,
    /// ahol nincs megadva a csoporttags�g a felhaszn�l�-szerepk�r hozz�rendel�sben, de helyettes�t�s sem.
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a KRT_CsoportTagok rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt funkci�k (Id, Kod p�rokk�nt).</returns>
    [WebMethod()]
    public Result GetAllByCsoporttagSajatJogu(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByCsoporttagSajatJogu(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// A megb�z�s alapj�n lesz�ri az aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat.  A megb�z�snak aktu�lisan folyamatban kell lennie
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes helyettes�t�sre, csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a megb�z�st tartalmaz� KRT_Helyettesitesek rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt funkci�k (Id, Kod p�rokk�nt).</returns>
    [WebMethod()]
    public Result GetAllByMegbizas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByMegbizas(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Felhasznalok.BaseTyped)), System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Funkciok.BaseTyped))]
    public Result GetAllBySzerepkor(ExecParam ExecParam, KRT_Szerepkorok _KRT_Szerepkorok
        , KRT_Szerepkor_FunkcioSearch _KRT_Szerepkor_FunkcioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllBySzerepkor(ExecParam, _KRT_Szerepkorok, _KRT_Szerepkor_FunkcioSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetByTableAndOperation(ExecParam ExecParam, String TablaKod, String MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetByTableAndOperation(ExecParam, TablaKod, MuveletKod);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [System.Web.Services.WebMethod]
    public string[] GetFunkciokList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("GetFunkciokList webservice elindul");
        Logger.Debug(String.Format("Param�terek: prefixText {0}, count: {1}, contextKey {2}", prefixText, count, contextKey));

        //Csoportok.CsoportFilterObject filterObject = new Csoportok.CsoportFilterObject(contextKey);
        string[] arrayContextKey = contextKey.Split(';');
        String userId = arrayContextKey[0];

        Logger.Debug("Felhaszn�l� id: " + userId);

        bool FeladatMode = false;
        bool FeladatDefinicioMode = false;

        if (arrayContextKey.Length > 1)
        {
            if (arrayContextKey[1] == "FeladatMode")
            {
                Logger.Debug("FeladatMode = true");
                FeladatMode = true;
            }
            else if (arrayContextKey[1] == "FeladatDefinicioMode")
            {
                Logger.Debug("FeladatDefinicioMode = true");
                FeladatDefinicioMode = true;
            }
        }

        string ObjektumTipusFilter = String.Empty;

        if (arrayContextKey.Length > 2)
        {
            ObjektumTipusFilter = arrayContextKey[2];
            Logger.Debug("ObjektumTipusFilter: " + ObjektumTipusFilter);
        }

        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;

        KRT_FunkciokSearch funkciokSearch;
        if (!String.IsNullOrEmpty(ObjektumTipusFilter))
        {
            funkciokSearch = new KRT_FunkciokSearch(true);
            funkciokSearch.Extended_KRT_ObjTipusokSearch.Kod.Value = ObjektumTipusFilter;
            funkciokSearch.Extended_KRT_ObjTipusokSearch.Kod.Operator = Query.Operators.isnullorequals;
        }
        else
        {
            funkciokSearch = new KRT_FunkciokSearch();
        }
        funkciokSearch.OrderBy = " KRT_Funkciok.Nev ASC";
        funkciokSearch.Nev.Value = prefixText + '%';
        funkciokSearch.Nev.Operator = Query.Operators.like;
        funkciokSearch.TopRow = count;

        if (FeladatMode)
        {
            funkciokSearch.KeziFeladatJelzo.Value = Contentum.eUtility.Constants.Database.Yes;
            funkciokSearch.KeziFeladatJelzo.Operator = Query.Operators.equals;
        }
        else if (FeladatDefinicioMode)
        {
            funkciokSearch.FeladatJelzo.Value = Contentum.eUtility.Constants.Database.Yes;
            funkciokSearch.FeladatJelzo.Operator = Query.Operators.equals;
        }

        Result res = this.GetAllWithExtension(execparam, funkciokSearch);

        if (!res.IsError)
        {
            Logger.Debug("GetFunkciokList webservice v�ge");
            Logger.DebugEnd();
            return FunkciokDataSourceToStringPairArray(res);
        }
        else
        {
            Logger.Error("GetFunkciokList webservice hiba", execparam, res);
        }

        Logger.Debug("GetFunkciokList webservice v�ge null visszat�r�si �rt�kkel");
        Logger.DebugEnd();
        return null;

    }

    private static string[] FunkciokDataSourceToStringPairArray(Result result)
    {
        const string delimeter = ";";
        if (result.Ds != null)
        {
            string[] StringArray = new string[result.Ds.Tables[0].Rows.Count];
            string value = "";
            string text = "";
            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                value = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                value += delimeter + result.Ds.Tables[0].Rows[i]["ObjTipus_Id_AdatElem_Kod"].ToString();
                text = result.Ds.Tables[0].Rows[i]["Nev"].ToString();
                string item = Utility.CreateAutoCompleteItem(text, value);
                StringArray.SetValue(item, i);
            }
            return StringArray;
        }
        else
        {
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    public bool HasFunctionRight(ExecParam execParam, string function)
    {
        Logger.DebugStart("HasFunctionRight - Start");
        Logger.Debug("Felhasznalo_Id: " + (execParam.Felhasznalo_Id ?? "NULL") + " LoginUser_Id: " + (execParam.LoginUser_Id ?? "NULL"));
        Logger.Debug("CsoportTag_Id: " + (execParam.CsoportTag_Id ?? "NULL") + " FelhasznaloSzervezet_Id: " + (execParam.FelhasznaloSzervezet_Id ?? "NULL"));

        bool HasFunctionRight = false;
        bool isMegbizas = false;
        // ha hiba volt a lek�r�s sor�n, akkor a v�g�n false-t adunk vissza
        if (!String.IsNullOrEmpty(execParam.Helyettesites_Id))
        {
            Logger.Debug("Helyettesites_Id: " + execParam.Helyettesites_Id);
            KRT_HelyettesitesekService service_helyettesitesek = new KRT_HelyettesitesekService(this.dataContext);
            ExecParam execParam_helyettesitesek = execParam.Clone();
            execParam_helyettesitesek.Record_Id = execParam.Helyettesites_Id;
            Result result_helyettesitesGet = service_helyettesitesek.Get(execParam_helyettesitesek);

            if (String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode))
            {
                KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result_helyettesitesGet.Record;
                if (krt_Helyettesitesek != null)
                {
                    if (krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                    {
                        isMegbizas = true;
                        Logger.Debug("isMegbizas = true");
                    }
                }
                else
                {
                    // hiba
                    Logger.Debug("Error: krt_Helyettesitesek == null");
                    Logger.DebugEnd("HasFunctionRight - End");
                    return false;
                }
            }
            else
            {
                // hiba
                Logger.Debug("Error: !String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode)");
                Logger.DebugEnd("HasFunctionRight - End");
                return false;
            }
        }

        // Mindig a Felhasznalo -ra k�rj�k le a funkci�jogosults�gokat
        // (Helyettes�t�sn�l is a Felhaszn�l� lesz a helyettes�tett) - kiv�ve megb�z�s
        Result _ret;

        if (isMegbizas)
        {
            execParam.Record_Id = execParam.Helyettesites_Id;
            Logger.Debug("HasFunctionRight: GetAllByMegbizas", execParam);
            _ret = this.GetAllByMegbizas(execParam);
        }
        else
        {
            // szervezet �s felhaszn�l� adott, de sz�ks�g van a csoporttags�gra
            execParam.Record_Id = execParam.CsoportTag_Id;
            Logger.Debug("HasFunctionRight: GetAllByCsoporttagSajatJogu", execParam);
            _ret = this.GetAllByCsoporttagSajatJogu(execParam);
        }

        if (_ret.Ds != null)
        {
            DataRow[] rows_funkcio = _ret.Ds.Tables[0].Select("Kod='" + function + "'");
            if (rows_funkcio.Length > 0)
            {
                HasFunctionRight = true;
            }
        }
        else
        {
            // hiba
            Logger.Debug("Error: _ret.Ds == null");
            Logger.DebugEnd("HasFunctionRight - End");
            return false;
        }

        Logger.Debug("HasFunctionRight = " + HasFunctionRight.ToString());
        Logger.DebugEnd("HasFunctionRight - End");
        return HasFunctionRight;
    }
}