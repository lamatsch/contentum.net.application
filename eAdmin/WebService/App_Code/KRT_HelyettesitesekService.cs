using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_HelyettesitesekService : System.Web.Services.WebService
{
    [WebMethod()]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Helyettesitesek.BaseTyped)), System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Helyettesitesek.BaseTyped))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_HelyettesitesekSearch _KRT_HelyettesitesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_HelyettesitesekSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_HelyettesitesekSearch, new KRT_HelyettesitesekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Helyettesitesek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_HelyettesitesekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_HelyettesitesekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_HelyettesitesekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region Megbizas
    /// <summary>
    /// UpdateMegbizas(ExecParam ExecParam, KRT_Helyettesitesek Record)
    /// Az KRT_Helyettesitesek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. Ha a megb�z�s kezdete vagy v�ge m�dosult, akkor a kapcsol�d� �truh�zott szerepk�r�k �rv�nyess�g�nek fel�lvizsg�lata, m�dos�t�sa.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa.  Ha a megb�z�s kezdete vagy v�ge m�dosult, akkor a kapcsol�d� �truh�zott szerepk�r�k �rv�nyess�g�nek fel�lvizsg�lata, m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result UpdateMegbizas(ExecParam ExecParam, KRT_Helyettesitesek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Megjegyz�s: a fel�leten nem nem engedj�k meg, hogy a megb�z�sban
            // r�sztvev� szem�lyek v�ltozzanak, ez�rt itt azt nem kezelj�k

            bool bErvChanged = false;
            KRT_Helyettesitesek krt_Helyettesitesek_Regi = null;
            #region r�gi rekord
            // r�gi rekord lek�r�se, ha sz�ks�ges, �s annak ellen�rz�se, hogy a megb�z�s eleje, v�ge m�dosult-e
            // TODO: �rv�nyess�g figyelembe v�tele is?
            if (Record.Updated.HelyettesitesKezd || Record.Updated.HelyettesitesVege)
            {
                Logger.Info("R�gi megb�z�s rekord lek�r�se START");
                Result result_get = sp.Get(ExecParam);
                if (!String.IsNullOrEmpty(result_get.ErrorCode))
                {
                    throw new ResultException(result_get);
                }
                Logger.Info("R�gi megb�z�s rekord lek�r�se sikeres volt.");

                krt_Helyettesitesek_Regi = (KRT_Helyettesitesek)result_get.Record;

                // egyel�re feltessz�k, hogy mindig azonos form�tum� d�tumot kapunk
                bErvChanged |= (krt_Helyettesitesek_Regi.HelyettesitesKezd != Record.HelyettesitesKezd);
                bErvChanged |= (krt_Helyettesitesek_Regi.HelyettesitesVege != Record.HelyettesitesVege);

            }
            #endregion r�gi rekord

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (bErvChanged)
            {
                Logger.Info("Kapcsolt szerepk�r rekordok �rv�nyess�g�nek ellen�rz�se START");
                KRT_Felhasznalo_SzerepkorService service_fsz = new KRT_Felhasznalo_SzerepkorService(this.dataContext);
                Result result_fszUpdate = service_fsz.UpdateByMegbizas(ExecParam);

                if (!String.IsNullOrEmpty(result_fszUpdate.ErrorCode))
                {
                    throw new ResultException(result_fszUpdate);
                }

                Logger.Info("Kapcsolt szerepk�r rekordok �rv�nyess�g�nek ellen�rz�se END");
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Helyettesitesek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// InvalidateMegbizas(ExecParam ExecParam)
    /// Az KRT_Helyettesitesek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A megb�z�shoz kapcsol�d� �truh�zott szerepk�r�k �rv�nytelen�t�se. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A megb�z�shoz kapcsol�d� �truh�zott szerepk�r�k �rv�nytelen�t�se. Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result InvalidateMegbizas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region r�gi rekord
            // r�gi rekord lek�r�se,�s annak ellen�rz�se, hogy megkezd�d�tt-e m�r
            bool bAlreadyStarted = false;
            Logger.Info("R�gi megb�z�s rekord lek�r�se START");
            Result result_get = sp.Get(ExecParam);
            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }
            Logger.Info("R�gi megb�z�s rekord lek�r�se sikeres volt.");

            KRT_Helyettesitesek krt_Helyettesitesek_Regi = (KRT_Helyettesitesek)result_get.Record;

            System.Data.SqlTypes.SqlDateTime Date = new System.Data.SqlTypes.SqlDateTime(DateTime.Now);

            if (krt_Helyettesitesek_Regi.Typed.HelyettesitesKezd < Date)
            {
                bAlreadyStarted = true;
            }
            #endregion r�gi rekord

            if (bAlreadyStarted == false)
            {

                #region Szerepk�r�k
                Logger.Info("Kapcsolt szerepk�r rekordok lek�r�se START");
                KRT_Felhasznalo_SzerepkorService service_fsz = new KRT_Felhasznalo_SzerepkorService(this.dataContext);
                KRT_Felhasznalo_SzerepkorSearch search_fsz = new KRT_Felhasznalo_SzerepkorSearch();

                search_fsz.Helyettesites_Id.Value = ExecParam.Record_Id;
                search_fsz.Helyettesites_Id.Operator = Query.Operators.equals;

                search_fsz.ErvVege.Value = Query.SQLFunction.getdate;
                search_fsz.ErvVege.Operator = Query.Operators.greater;

                search_fsz.ErvKezd.Value = "";
                search_fsz.ErvKezd.Operator = "";

                Result result_fszGetAll = service_fsz.GetAll(ExecParam, search_fsz);

                if (!String.IsNullOrEmpty(result_fszGetAll.ErrorCode))
                {
                    throw new ResultException(result_fszGetAll);
                }
                Logger.Info("Kapcsolt szerepk�r rekordok lek�r�se END");

                int cnt = result_fszGetAll.Ds.Tables[0].Rows.Count;
                if (cnt == 0)
                {
                    Logger.Info("Kapcsolt szerepk�r rekord nem tal�lhat�.");
                }
                else
                {
                    Logger.Info("Kapcsolt szerepk�r rekordok �rv�nytelen�t�se START");

                    ExecParam[] execParams_fsz = new ExecParam[cnt];

                    for (int i = 0; i < cnt; i++)
                    {
                        execParams_fsz[i] = ExecParam.Clone();
                        execParams_fsz[i].Record_Id = result_fszGetAll.Ds.Tables[0].Rows[i]["Id"].ToString();
                    }

                    Result result_fszMultiInvalidate = service_fsz.MultiInvalidate(execParams_fsz);

                    if (!String.IsNullOrEmpty(result_fszMultiInvalidate.ErrorCode))
                    {
                        throw new ResultException(result_fszMultiInvalidate);
                    }
                    Logger.Info("Kapcsolt szerepk�r rekordok �rv�nytelen�t�se END");
                }
                #endregion

                Logger.Info("Megb�z�s �rv�nytelen�t�se");
                result = sp.Invalidate(ExecParam);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                #region Esem�nynapl�z�s
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Helyettesitesek", "Invalidate").Record;

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion
            }
            else
            {
                Logger.Info("Megb�z�s lez�r�sa az �rv�nyess�g meg�rz�se mellett");
                krt_Helyettesitesek_Regi.HelyettesitesVege = Date.ToSqlString().ToString();
                // csak be�ll�tjuk a v�g�t mostanra
                result = UpdateMegbizas(ExecParam, krt_Helyettesitesek_Regi);
 
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }


                #region Esem�nynapl�z�s
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Helyettesitesek", "Modify").Record;

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidateMegbizas(ExecParam ExecParams)
    /// A KRT_Helyettesitesek t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza.  A megb�z�shoz kapcsol�d� �truh�zott szerepk�r�k �rv�nytelen�t�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza.  A megb�z�shoz kapcsol�d� �truh�zott szerepk�r�k �rv�nytelen�t�se. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result MultiInvalidateMegbizas(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = InvalidateMegbizas(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    #endregion Megbizas

    #region Helyettesites
        /// <summary>
    /// InvalidateHelyettesites(ExecParam ExecParam)
    /// Az KRT_Helyettesitesek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s), vagy lez�r�sa (HelyettesitesVege be�ll�t�sa). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s), vagy lez�r�sa (HelyettesitesVege be�ll�t�sa). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result InvalidateHelyettesites(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region r�gi rekord
            // r�gi rekord lek�r�se,�s annak ellen�rz�se, hogy megkezd�d�tt-e m�r
            bool bAlreadyStarted = false;
            Logger.Info("R�gi helyettes�t�s rekord lek�r�se START");
            Result result_get = sp.Get(ExecParam);
            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }
            Logger.Info("R�gi helyettes�t�s rekord lek�r�se sikeres volt.");

            KRT_Helyettesitesek krt_Helyettesitesek_Regi = (KRT_Helyettesitesek)result_get.Record;

            System.Data.SqlTypes.SqlDateTime Date = new System.Data.SqlTypes.SqlDateTime(DateTime.Now);

            if (krt_Helyettesitesek_Regi.Typed.HelyettesitesKezd < Date)
            {
                bAlreadyStarted = true;
            }
            #endregion r�gi rekord

            if (bAlreadyStarted == false)
            {
                Logger.Info("helyettes�t�s �rv�nytelen�t�se");
                result = sp.Invalidate(ExecParam);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                #region Esem�nynapl�z�s
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Helyettesitesek", "Invalidate").Record;

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion
            }
            else
            {
                Logger.Info("helyettes�t�s lez�r�sa az �rv�nyess�g meg�rz�se mellett");
                krt_Helyettesitesek_Regi.HelyettesitesVege = Date.ToSqlString().ToString();
                // csak be�ll�tjuk a v�g�t mostanra
                result = sp.Insert(Constants.Update, ExecParam, krt_Helyettesitesek_Regi);
 
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }


                #region Esem�nynapl�z�s
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Helyettesitesek", "Modify").Record;

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidateHelyettesites(ExecParam ExecParams)
    /// A KRT_Helyettesitesek t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s), vagy lez�r�sa (HelyettesitesVege be�ll�t�sa). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s), vagy lez�r�sa (HelyettesitesVege be�ll�t�sa). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Helyettesitesek))]
    public Result MultiInvalidateHelyettesites(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = InvalidateHelyettesites(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = "";

                for (int i = 0; i < ExecParams.Length; i++)
                {
                    Ids += "'" + ExecParams[i].Record_Id + "',";
                }

                Ids = Ids.TrimEnd(',');

                Result eventLogResult = eventLogService.InsertTomeges(ExecParams[0], Ids, "KRT_Helyettesitesek", "Invalidate");
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    #endregion Helyettesites

}
