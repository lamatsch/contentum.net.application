using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.Web.Script.Services;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public partial class KRT_KozteruletekService : System.Web.Services.WebService
{
    private static string[] KozteruletekList = null;

    private static void KozteruletekListReset()
    {
        KozteruletekList = null;
    }

    #region Insert, Invalidate, Delete átvéve a Generated -ből

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Kozteruletek))]
    public Result Insert(ExecParam ExecParam, KRT_Kozteruletek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Insert(Constants.Insert, ExecParam, Record);
             

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            KozteruletekListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Kozteruletek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Kozteruletek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Invalidate(ExecParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            
            KozteruletekListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Kozteruletek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Kozteruletek))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            KozteruletekListReset();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetKozteruletekList(string prefixText, int count, string contextKey)
    {
        String userId = contextKey.Substring(0, contextKey.IndexOf(";"));

        if (KozteruletekList == null)
        {
            KRT_KozteruletekService Kozteruletekservice = new KRT_KozteruletekService();
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            KRT_KozteruletekSearch Kozteruleteksearch = new KRT_KozteruletekSearch();
            Kozteruleteksearch.OrderBy = "Nev " + Utility.customCollation + " ASC";

            Result _res = Kozteruletekservice.GetAll(execparam, Kozteruleteksearch);
            if (String.IsNullOrEmpty(_res.ErrorCode))
            {
                KozteruletekList = Utility.DataSourceColumnToStringArray(_res, "Nev");
            }
            else
            {
                return null;
            }
        }

        return Utility.SearchInStringArrayByPrefix(KozteruletekList, prefixText, count,true);

    }
}