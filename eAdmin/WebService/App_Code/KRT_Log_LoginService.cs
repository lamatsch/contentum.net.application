using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_Log_LoginService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_LogSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_LogSearch);

            #region Eseménynaplózás
            if (!result.IsError)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Parameterek", "AuditLogList").Record;

                if (eventLogRecord != null)
                {
                    string where = String.Empty;
                    where = Search.ConcatWhereExpressions(_KRT_LogSearch.Where_Log_Login, _KRT_LogSearch.Where_Log_Page);
                    where = Search.ConcatWhereExpressions(where, _KRT_LogSearch.Where_Log_WebService);
                    where = Search.ConcatWhereExpressions(where, _KRT_LogSearch.Where_Log_StoredProcedure);
                    eventLogRecord.KeresesiFeltetel = where;
                    if (result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }
                    else
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[0].Rows.Count.ToString();
                    }


                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
}