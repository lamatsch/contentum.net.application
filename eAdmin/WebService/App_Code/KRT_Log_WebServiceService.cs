using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_Log_WebServiceService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_LogSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_LogSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_LogSearch))]
    public Result GetServices(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetServices(ExecParam, _KRT_LogSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_LogSearch))]
    public Result GetMethodsByService(ExecParam ExecParam, KRT_LogSearch _KRT_LogSearch)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetMethodsByService(ExecParam, _KRT_LogSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
}