using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_MenukService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvettek (GetAll)

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_MenukSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_MenukSearch _KRT_MenukSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_MenukSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_MenukSearch, new KRT_MenukSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Menuk", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_MenukSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_MenukSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_MenukSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Menuk))]
    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, Alkalmazas_Kod);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// A csoporttags�g alapj�n lesz�ri a saj�t jog�, aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat, �s a nekik megfelel� men�ket. Azon szerepk�r hozz�rendel�seket is figyelembe veszi,
    /// ahol nincs megadva a csoporttags�g a felhaszn�l�-szerepk�r hozz�rendel�sben, de helyettes�t�s sem.
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a KRT_CsoportTagok rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt men�k.</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Menuk))]
    public Result GetAllByCsoporttagSajatJogu(ExecParam ExecParam, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByCsoporttagSajatJogu(ExecParam, Alkalmazas_Kod);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// A megb�z�s alapj�n lesz�ri az aktu�lisan �rv�nyes szerepk�r�ket,
    /// �s azok alapj�n az enged�lyezett funkci�kat �s a nekik megfelel� men�ket. A megb�z�snak aktu�lisan folyamatban kell lennie
    /// Csak l�tez� felhaszn�l�ra, �rv�nyes helyettes�t�sre, csoporttags�gra, futtat�si id�pontban �rv�nyes hozz�rendel�sre.
    /// </summary>
    /// <param name="ExecParam">A Record_Id (k�telez�!) a megb�z�st tartalmaz� KRT_Helyettesitesek rekord azonos�t�j�t tartalmazza, a t�bbi inform�ci� a napl�z�shoz sz�ks�ges.</param>
    /// <returns>Hiba, vagy a hozz�rendelt men�k.</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Menuk))]
    public Result GetAllByMegbizas(ExecParam ExecParam, String Alkalmazas_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByMegbizas(ExecParam, Alkalmazas_Kod);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod()]
    //[System.Xml.Serialization.XmlInclude(typeof(KRT_Menuk))]
    //public Result Teszt()
    //{
    //    ExecParam execParam = new ExecParam();
    //    execParam.Felhasznalo_Id = "F088E89E-7917-4D0B-B94F-F4DAC84F0F95";
    //    execParam.Record_Id = "aa1421b9-0498-4ca3-b075-60f699cb1b7c";
    //    Result result = sp.Get(execParam);
    //    return null;
    //}
    
}