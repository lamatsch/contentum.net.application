using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_ModulokService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_ModulokSearch))]
    public Result GetAllWithFK(ExecParam ExecParam, KRT_ModulokSearch _KRT_ModulokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithFK(ExecParam, _KRT_ModulokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_ModulokSearch, new KRT_ModulokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Modulok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_ModulokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_ModulokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_ModulokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    //public Result Test()
    //{
    //    KRT_ModulokService service = new KRT_ModulokService();
    //    Contentum.eBusinessDocuments.ExecParam ExecParam = new Contentum.eBusinessDocuments.ExecParam();
    //    ExecParam.Felhasznalo_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";

    //    Contentum.eQuery.BusinessDocuments.KRT_ModulokSearch moduloksearch = new Contentum.eQuery.BusinessDocuments.KRT_ModulokSearch();

    //    moduloksearch.Kod.Value = "PartnerekForm.aspx2";
    //    moduloksearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

    //    Result _res = service.GetAll(ExecParam, moduloksearch);

    //    return _res;
    //}
}
