using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_NezetekService : System.Web.Services.WebService
{
    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    //public Result Test()
    //{
    //    KRT_NezetekService service = new KRT_NezetekService();
    //    Contentum.eBusinessDocuments.ExecParam ExecParam = new Contentum.eBusinessDocuments.ExecParam();
    //    ExecParam.Felhasznalo_Id = "2303F57B-C9EB-4F3D-82E4-A2CC47510F7A";

    //    Contentum.eQuery.BusinessDocuments.KRT_NezetekSearch moduloksearch = new Contentum.eQuery.BusinessDocuments.KRT_NezetekSearch();

    //    moduloksearch.Form_Id.Value = "147943a3-f8aa-479a-bae5-81f3262c9e01";
    //    moduloksearch.Form_Id.Operator = Contentum.eQuery.Query.Operators.equals;

    //    Result _res = service.GetAll(ExecParam, moduloksearch);

    //    return _res;
    //}

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_NezetekSearch))]
    public Result GetAllWithFK(ExecParam ExecParam, KRT_NezetekSearch _KRT_NezetekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithFK(ExecParam, _KRT_NezetekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_NezetekSearch))]
    public Result GetAllByMuvelet(ExecParam ExecParam, KRT_NezetekSearch _KRT_NezetekSearch, string MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            if (String.IsNullOrEmpty(MuveletKod))
            {
                MuveletKod = "NotMuveletSet";
            }
            result = sp.GetAllByMuvelet(ExecParam, _KRT_NezetekSearch, MuveletKod);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}
