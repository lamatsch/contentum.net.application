using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Text;
using System.Data;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_ObjTipusokService : System.Web.Services.WebService
{

    /// <summary>
    /// GetAllWithExtension(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, String KodFilter)
    /// A(z) KRT_ObjTipusok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search).
    /// A KodFilter az egy bizonyos t�pus� objektumokra (pl. DBTable) val�keres�st teszi lehet�v�. 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_KRT_ObjTipusokSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <param name="KodFilter">Bemen� param�ter, az ObjTipus_Id_Tipus-ra vonatkoz� sz�r�felt�telt (a Tipus Kod-j�t) tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_ObjTipusokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_ObjTipusokSearch _KRT_ObjTipusokSearch, String KodFilter)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_ObjTipusokSearch, KodFilter);
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetAllByTableColumn(ExecParam ExecParam, String Table, String Column)
    /// A(z) KRT_ObjTipusok t�bl�ra vonatkoz� oszlop objektum keres�se, az oszlop �s az azt tartalmaz� t�bla neve alapj�n.
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="Table">Bemen� param�ter, az oszlopot tartalmaz� t�bla neve. </param>
    /// <param name="Column">Bemen� param�ter, a keresett oszlop neve. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A(z) KRT_ObjTipusok t�bl�ra vonatkoz� oszlop objektum keres�se, az oszlop �s az azt tartalmaz� t�bla neve alapj�n. Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    public Result GetAllByTableColumn(ExecParam ExecParam, String Table, String Column)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByTableColumn(ExecParam, Table, Column);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAzonosito(ExecParam ExecParam, string ObjektumId, string ObjektumTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("Objektum azonos�t�", ObjektumId, ArgumentTypes.Guid));
            args.Add(new Argument("Objektum t�pus", ObjektumTipus, ArgumentTypes.String));
            args.ValidateArguments();

            result = sp.GetAzonosito(ExecParam, ObjektumId, ObjektumTipus);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAzonositoTomeges(ExecParam[] ExecParams, string ObjektumTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result.Ds = new DataSet();
            DataTable table = new DataTable();
            table.Columns.Add("Azonosito");
            result.Ds.Tables.Add(table);

            foreach (ExecParam xpm in ExecParams)
            {
                Result res = GetAzonosito(xpm, xpm.Record_Id, ObjektumTipus);
                if (res.IsError)
                {
                    throw new ResultException(res);
                }
                DataRow row = res.Ds.Tables[0].Rows[0];
                result.Ds.Tables[0].ImportRow(row);
            }

            return result;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }


}