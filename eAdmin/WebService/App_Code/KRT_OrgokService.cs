using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_OrgokService : System.Web.Services.WebService
{
    [WebMethod()]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Orgok.BaseTyped)), System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Orgok.BaseTyped))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_OrgokSearch _KRT_OrgokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_OrgokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_OrgokSearch, new KRT_OrgokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Orgok", "OrgokList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_OrgokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_OrgokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_OrgokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// CreateNewOrg(ExecParam ExecParam, KRT_Orgok Record, KRT_Felhasznalok krt_Felhasznalok, KRT_Csoportok krt_Csoportok, string SzerepkorNev)
    /// Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza.
    /// Meg kell adni tov�bb� egy �j adminisztr�tor felhaszn�l� �s �j szervezetcsoportj�nak adatait, �s a felhaszn�l� szerepk�r�nek nev�t.
    /// A szerepk�rh�z minden funkci� hozz�rendel�dik.
    /// <param name="Record">Az �j ORG adatai.</param>
    /// <param name="krt_Felhasznalok">A l�trehozand� adminisztr�to felhaszn�l� adatai.</param>
    /// <param name="krt_Csoportok">Az �j adminisztr�to felhaszn�l� l�trehozand� szervezet�nek adatai.</param>
    /// <param name="SzerepkorNev">Az �j felhaszn�l�hoz rendelend�, minden funkci�t tartalmaz� szerepk�r megnevez�se.</param>
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza."
    + " Meg kell adni tov�bb� egy �j adminisztr�tor felhaszn�l� �s �j szervezetcsoportj�nak adatait, �s a felhaszn�l� szerepk�r�nek nev�t."
    + " A szerepk�rh�z minden funkci� hozz�rendel�dik.")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Orgok))]
    public Result CreateNewOrg(ExecParam ExecParam, KRT_Orgok Record, KRT_Felhasznalok krt_Felhasznalok, KRT_Csoportok krt_Csoportok, string SzerepkorNev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ellen�rz�s: A felhaszn�l�i n�vnek (UserNev) egyedinek kell lennie
            KRT_FelhasznalokService service_Felhasznalo = new KRT_FelhasznalokService(this.dataContext);
            string Felhasznalo_UserNev = krt_Felhasznalok.UserNev;

            KRT_FelhasznalokSearch search_Felhasznalo = new KRT_FelhasznalokSearch();
            search_Felhasznalo.UserNev.Value = Felhasznalo_UserNev;
            search_Felhasznalo.UserNev.Operator = Query.Operators.equals;

            Result result_UserNevGetAll = service_Felhasznalo.GetAllUserNev(ExecParam, search_Felhasznalo);
            if (result_UserNevGetAll.IsError)
            {
                throw new ResultException(result_UserNevGetAll);
            }

            if (result_UserNevGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                throw new ResultException(53611);
                // M�r l�tezik ilyen felhaszn�l� n�vvel felhaszn�l�. A felhaszn�l� n�vnek egyedinek kell lenni az eg�sz alkalmaz�sra, szervezett�l (ORG-t�l) f�ggetlen�l.
            }

            // Tranz_Id besz�r�sa minden itt l�trehozott objektumba
            Record.Base.Tranz_id = this.dataContext.Tranz_Id;
            Record.Base.Updated.Tranz_id = true;

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // �jonnan l�trej�tt Org azonos�t�ja
            string Org_Id = result.Uid;

            #region felhasznalo
            krt_Felhasznalok.Org = Org_Id;
            krt_Felhasznalok.Updated.Org = true;

            krt_Felhasznalok.Base.Tranz_id = this.dataContext.Tranz_Id;
            krt_Felhasznalok.Base.Updated.Tranz_id = true;

            // m�r kor�bban l�trehozzuk az ellen�rz�shez
            //KRT_FelhasznalokService service_Felhasznalo = new KRT_FelhasznalokService(this.dataContext);

            // Insert, partner gener�l�ssal:
            Result result_Felhasznalo = service_Felhasznalo.InsertAndCreateNewPartner(ExecParam, krt_Felhasznalok);

            if (result_Felhasznalo.IsError)
            {
                throw new ResultException(result_Felhasznalo);
            }

            string Felhasznalo_Id = result_Felhasznalo.Uid;

            #endregion felhasznalo

            #region csoport
            krt_Csoportok.Org = Org_Id;
            krt_Csoportok.Updated.Org = true;

            krt_Csoportok.Base.Tranz_id = this.dataContext.Tranz_Id;
            krt_Csoportok.Base.Updated.Tranz_id = true;

            KRT_CsoportokService service_Csoport = new KRT_CsoportokService(this.dataContext);

            Result result_Csoport = service_Csoport.Insert(ExecParam, krt_Csoportok);

            if (result_Csoport.IsError)
            {
                throw new ResultException(result_Csoport);
            }

            string Csoport_Id = result_Csoport.Uid;

            #endregion csoport

            #region felhasznalo - csoport osszerendeles

            KRT_CsoportTagok krt_CsoportTagok = new KRT_CsoportTagok();
            krt_CsoportTagok.Updated.SetValueAll(false);
            krt_CsoportTagok.Base.Updated.SetValueAll(false);

            krt_CsoportTagok.Csoport_Id = Csoport_Id;
            krt_CsoportTagok.Updated.Csoport_Id = true;

            krt_CsoportTagok.Csoport_Id_Jogalany = Felhasznalo_Id;
            krt_CsoportTagok.Updated.Csoport_Id_Jogalany = true;

            krt_CsoportTagok.Tipus = KodTarak.CsoprttagsagTipus.dolgozo;
            krt_CsoportTagok.Updated.Tipus = true;

            krt_CsoportTagok.Base.Tranz_id = this.dataContext.Tranz_Id;
            krt_CsoportTagok.Base.Updated.Tranz_id = true;

            KRT_CsoportTagokService service_CsoportTagok = new KRT_CsoportTagokService(this.dataContext);

            Result result_CsoportTagok = service_CsoportTagok.Insert(ExecParam, krt_CsoportTagok);

            if (result_CsoportTagok.IsError)
            {
                throw new ResultException(result_CsoportTagok);
            }

            string CsoportTag_Id = result_CsoportTagok.Uid;

            #endregion felhasznalo - csoport osszerendeles

            #region szerepkor
            KRT_Szerepkorok krt_Szerepkorok = new KRT_Szerepkorok();
            krt_Szerepkorok.Updated.SetValueAll(false);
            krt_Szerepkorok.Base.Updated.SetValueAll(false);

            krt_Szerepkorok.Nev = SzerepkorNev;
            krt_Szerepkorok.Updated.Nev = true;

            krt_Szerepkorok.Org = Org_Id;
            krt_Szerepkorok.Updated.Org = true;

            krt_Szerepkorok.Base.Tranz_id = this.dataContext.Tranz_Id;
            krt_Szerepkorok.Base.Updated.Tranz_id = true;

            KRT_SzerepkorokService service_Szerepkorok = new KRT_SzerepkorokService(this.dataContext);
            Result result_Szerepkorok = service_Szerepkorok.Insert(ExecParam, krt_Szerepkorok);

            if (result_Szerepkorok.IsError)
            {
                throw new ResultException(result_Szerepkorok);
            }

            string Szerepkor_Id = result_Szerepkorok.Uid;

            #endregion szerepkor

            #region szerepkor - funkcio hozzarendeles

            KRT_Szerepkor_FunkcioService service_Szerepkor_Funkcio = new KRT_Szerepkor_FunkcioService(this.dataContext);
            Result result_Szerepkor_Funkcio = service_Szerepkor_Funkcio.AddAllFunctionsToSzerepkor(ExecParam, Szerepkor_Id);

            if (result_Szerepkor_Funkcio.IsError)
            {
                throw new ResultException(result_Szerepkor_Funkcio);
            }
            
            #endregion szerepkor - funkcio hozzarendeles

            #region felhasznalo - szerepkor hozzarendeles

            KRT_Felhasznalo_Szerepkor krt_Felhasznalo_Szerepkor = new KRT_Felhasznalo_Szerepkor();
            krt_Felhasznalo_Szerepkor.Updated.SetValueAll(false);
            krt_Felhasznalo_Szerepkor.Base.Updated.SetValueAll(false);

            krt_Felhasznalo_Szerepkor.Felhasznalo_Id = Felhasznalo_Id;
            krt_Felhasznalo_Szerepkor.Updated.Felhasznalo_Id = true;

            krt_Felhasznalo_Szerepkor.Szerepkor_Id = Szerepkor_Id;
            krt_Felhasznalo_Szerepkor.Updated.Szerepkor_Id = true;

            krt_Felhasznalo_Szerepkor.Base.Tranz_id = this.dataContext.Tranz_Id;
            krt_Felhasznalo_Szerepkor.Base.Updated.Tranz_id = true;

            KRT_Felhasznalo_SzerepkorService service_Felhasznalo_Szerepkor = new KRT_Felhasznalo_SzerepkorService(this.dataContext);
            Result result_Felhasznalo_Szerepkor = service_Felhasznalo_Szerepkor.Insert(ExecParam, krt_Felhasznalo_Szerepkor);

            if (result_Felhasznalo_Szerepkor.IsError)
            {
                throw new ResultException(result_Felhasznalo_Szerepkor);
            }

            #endregion felhasznalo - szerepkor hozzarendeles

            #region rendszer vezerlok klonozasa

            ExecParam ExecParam_Clone = ExecParam.Clone();
            ExecParam_Clone.Record_Id = Org_Id;

            Result result_Clone = sp.CloneSystemControllersForNewOrg(ExecParam_Clone);

            if (result_Clone.IsError)
            {
                throw new ResultException(result_Clone);
            }

            #endregion rendszer vezerlok klonozasa

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Orgok", "OrgNew").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}