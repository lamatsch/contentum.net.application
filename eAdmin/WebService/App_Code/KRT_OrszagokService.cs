using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.Web.Script.Services;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery;
using System.Data;
using System.Data.SqlTypes;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public partial class KRT_OrszagokService : System.Web.Services.WebService
{
    private static string[] OrszagokList = null;

    private static void OrszagokListReset()
    {
        OrszagokList = null;
    }

    #region Insert, Invalidate, Delete átvéve a Generated -ből, Update

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Orszagok))]
    public Result Insert(ExecParam ExecParam, KRT_Orszagok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            OrszagokListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Orszagok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Orszagok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Invalidate(ExecParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            OrszagokListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Orszagok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Orszagok))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Delete(ExecParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            OrszagokListReset();


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Orszagok))]
    public Result Update(ExecParam ExecParam, KRT_Orszagok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string recordID = ExecParam.Record_Id;
            string modifiedOrszagNev = Record.Nev;
            Result res = null;

            KRT_CimekService service = new KRT_CimekService(this.dataContext);
            KRT_CimekSearch search = new KRT_CimekSearch();

            search.Orszag_Id.Value = recordID;
            search.Orszag_Id.Operator = Query.Operators.equals;

            ExecParam exec = ExecParam.Clone();
            exec.Record_Id = String.Empty;

            res = service.GetAll(exec, search);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                DataSet ds = res.Ds;
                string orszagNev = null;
                KRT_Cimek cimRecord = new KRT_Cimek();
                Result resCim = null;

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    orszagNev = row["OrszagNev"].ToString();
                    if (!String.IsNullOrEmpty(orszagNev) && orszagNev != modifiedOrszagNev)
                    {
                        cimRecord.Updated.SetValueAll(false);
                        cimRecord.Base.Updated.SetValueAll(false);
                        cimRecord.OrszagNev = modifiedOrszagNev;
                        cimRecord.Updated.OrszagNev = true;
                        cimRecord.Base.Ver = row["Ver"].ToString();
                        cimRecord.Base.Updated.Ver = true;

                        exec.Record_Id = row["Id"].ToString();

                        resCim = service.Update(exec, cimRecord);

                        if (!String.IsNullOrEmpty(resCim.ErrorCode))
                        {
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, resCim);
                            //return resCim;

                            throw new ResultException(resCim);
                        }
                    }
                }
            }
            else
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            res = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Orszagok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetOrszagokList(string prefixText, int count, string contextKey)
    {
        String userId = contextKey.Substring(0, contextKey.IndexOf(";"));

        if (OrszagokList == null)
        {
            KRT_OrszagokService orszagokservice = new KRT_OrszagokService();
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            KRT_OrszagokSearch orszagoksearch = new KRT_OrszagokSearch();
            orszagoksearch.OrderBy = "Nev " + Utility.customCollation + " ASC";

            Result _res = orszagokservice.GetAll(execparam, orszagoksearch);
            if (String.IsNullOrEmpty(_res.ErrorCode))
            {
                OrszagokList = Utility.DataSourceColumnToStringArray(_res, "Nev");
            }
            else
            {
                return null;
            }
        }

        return Utility.SearchInStringArrayByPrefix(OrszagokList, prefixText, count,true);

    }
}