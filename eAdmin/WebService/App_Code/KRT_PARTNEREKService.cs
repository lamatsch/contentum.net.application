﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Contentum.eRecord.Service;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_PartnerekService : System.Web.Services.WebService
{
    #region SegédOsztály

    [Serializable()]
    public class PartnerCimek
    {
        /// <summary>
        /// Az ügyfél postai irányítószáma.
        /// </summary>
        public string Iranyitoszam { get; set; }
        /// <summary>
        /// Az ügyfél települése.
        /// </summary>
        public string Telepules { get; set; }
        /// <summary>
        /// Az ügyfél címe település nélkül.
        /// </summary>
        public string Cim { get; set; }
        /// <summary>
        /// Az ügyfél telefonszáma.
        /// </summary>
        public string Telefon { get; set; }
        /// <summary>
        /// Az ügyfél mobiltelefonszáma.
        /// </summary>
        public string Mobil { get; set; }
        /// <summary>
        /// Az ügyfél email címe.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Az ügyfél telefaxszáma.
        /// </summary>
        public string Fax { get; set; }
    }

    #endregion

    [WebMethod(Description = "Ezzel a metódussal keressük ki az email cím alapján a levél érkeztetõjének az adatait.")]
    public Result GetPartnerByEmail(ExecParam execparam,
                                    string email,
                                    KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execparam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            result = sp.GetPartnerByEmail(
                execparam,
                email,
                _KRT_PartnerekSearch
            );
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execparam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek
        , KRT_PartnerKapcsolatokSearch _KRT_PartnerKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByPartner(ExecParam, _KRT_Partnerek
               , _KRT_PartnerKapcsolatokSearch);

            #region Eseménynaplózás
            if (!Search.IsIdentical(_KRT_PartnerKapcsolatokSearch, new KRT_PartnerKapcsolatokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_PartnerKapcsolatok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_PartnerKapcsolatokSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_KRT_PartnerKapcsolatokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_PartnerKapcsolatokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllWithFK(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithFK(ExecParam, _KRT_PartnerekSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllByCim(ExecParam ExecParam, KRT_Cimek _KRT_Cimek,
        KRT_PartnerCimekSearch _KRT_PartnerCimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByCim(ExecParam, _KRT_Cimek, _KRT_PartnerCimekSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllWithCimForExport(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithCimForExport(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetASPADOId(ExecParam execParam, string partnerId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetASPADOId(execParam, partnerId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllASPADOPartnerId(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllASPADOPartnerId(execParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetASPADOPartnerId(ExecParam execParam, int adoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetASPADOPartnerId(execParam, adoId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllWithCimAndSzemelyekandVallakozasok(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, KRT_SzemelyekSearch _KRT_SzemelyekSearch, KRT_VallalkozasokSearch _KRT_VallalkozasokSearch, KRT_PartnerCimekSearch _KRT_PartnerCimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithCimandSzemelyekandVallalkozasok(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, _KRT_SzemelyekSearch, _KRT_VallalkozasokSearch, _KRT_PartnerCimekSearch, "");

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //AutoComplete-hez, kell-e az összes cím, vagy csak egy bizonyos típusú
    public Result GetAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, WithAllCim);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CimekSearch))]
    public Result GetReallyAllWithCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim, string WithKuldemeny)
    {
        return GetReallyAllWithCimExt(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, WithAllCim, WithKuldemeny, null, "0");
    }

    public Result GetReallyAllWithCimExt(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch, string WithAllCim, string WithKuldemeny, string cimTipus, string WithKapcsoltPartner)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // BUG_5197
            //result = sp.GetReallyAllWithCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, WithAllCim, WithKuldemeny);
            result = sp.GetReallyAllWithCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch, WithAllCim, WithKuldemeny, cimTipus, WithKapcsoltPartner);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    /// <summary>
    /// A megadott csoporthoz tartozó KRT_Partnerek obj-ot adja vissza, ha talál
    /// Ha a csoport egy felhasználó, akkor a felhasznalo.PartnerId által megadott partnert adjuk vissza, ha pedig szervezet, akkor
    /// ellenõrizzük, hogy van-e ilyen id-jú partner, és akkor azt adjuk vissza
    /// </summary>    
    [WebMethod()]
    public Result GetByCsoportId(ExecParam execParam, string csoportId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetByCsoportId(execParam, csoportId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


 
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Vallalkozasok))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Szemelyek))]
    public Result Consolidate(ExecParam ExecParam, KRT_Partnerek Record, List<string> partnerCimIds,
  List<string> partnerKapcsolatIds, KRT_Szemelyek krt_szemely, KRT_Vallalkozasok krt_vallalkozas, KRT_Partnerek ConsolidatedRecord)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = null;
            //Konszolidáció menete
            //1. id-k átírása a táblákban

            //Ahol a konszolidáltpartner partner id-ja szerepelt (ConsolidatedRecord.Id), 
            //oda most be kell írni a megmaradó partner id-ját (Record.Id).

            //Módosítandó Partner_Id-k:            
            //...........................
            //EREC_eBeadvanyok.Partner_Id
            //EREC_eMailBoritekCimei.Partner_Id
            //EREC_IraElosztoivTetelek.Partner_Id
            //EREC_IraIratok.Partner_Id_VisszafizetesCimzet
            //EREC_IraJegyzekek.Partner_Id_LeveltariAtvevo
            //EREC_KuldBekuldok.Partner_Id_Bekuldo
            //EREC_KuldKuldemenyek.Partner_Id_Bekuldo
            //EREC_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt
            //EREC_PldIratPeldanyok.Partner_Id_Cimzett
            //EREC_Szamlak.Partner_Id_Szallito
            //EREC_UgyUgyiratok.Partner_Id_Ugyindito

            //KRT_PartnerCimek.Partner_Id
            //KRT_PartnerKapcsolatok.Partner_Id
            //KRT_Bankszamlaszamok.Partner_Id
            //KRT_Bankszamlaszamok.Partner_Id_Bank
            //KRT_Partner_Dokumentumok.Partner_id
            //KRT_PartnerMinositesek.Partner_id
            //...........................

            //Szükséges service-ek:
            //KRT_PartnerekService:
            //this.Update

            //KRT_SzemelyekService
            KRT_SzemelyekService szemelyekService = new KRT_SzemelyekService(this.dataContext);
            //KRT_VallalkozasokService
            KRT_VallalkozasokService vallalkozasokService = new KRT_VallalkozasokService(this.dataContext);
            //KRT_PartnerCimekService
            KRT_PartnerCimekService partnerCimekService = new KRT_PartnerCimekService(this.dataContext);
            //KRT_PartnerKapcsolatokService:
            KRT_PartnerKapcsolatokService pkService = new KRT_PartnerKapcsolatokService(this.dataContext);

            //Szükséges id-k lecserélése:
            UpdateConsolidatedIds(ExecParam, Record.Id, ConsolidatedRecord.Id);

            #region KRT_Partnerek
            //KRT_Partnerek
            if (Record != null)
            {
                ExecParam ExecParamPartner = ExecParam.Clone();
                ExecParamPartner.Record_Id = Record.Id;

                KRT_Partnerek local_krt_partner = this.Get(ExecParamPartner).Record as KRT_Partnerek;

                //2020.02.07
                Record = local_krt_partner;

                Record.Id = local_krt_partner.Id;
                Record.Base.Ver = local_krt_partner.Base.Ver;
                Record.Base.Updated.Ver = true;
                Record.Base.LetrehozasIdo = local_krt_partner.Base.LetrehozasIdo;
                Record.Base.Updated.LetrehozasIdo = true;


                if (!string.IsNullOrEmpty(ConsolidatedRecord.Nev))
                {
                    Record.Nev = ConsolidatedRecord.Nev;
                    Record.Updated.Nev = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.Orszag_Id))
                {
                    Record.Orszag_Id = ConsolidatedRecord.Orszag_Id;
                    Record.Updated.Orszag_Id = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.Belso))
                {
                    Record.Belso = ConsolidatedRecord.Belso;
                    Record.Updated.Belso = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.Allapot))
                {
                    Record.Allapot = ConsolidatedRecord.Allapot;
                    Record.Updated.Allapot = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.KulsoAzonositok))
                {
                    Record.KulsoAzonositok = ConsolidatedRecord.KulsoAzonositok;
                    Record.Updated.KulsoAzonositok = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.ErvKezd))
                {
                    Record.ErvKezd = ConsolidatedRecord.ErvKezd;
                    Record.Updated.ErvKezd = true;
                }

                if (!string.IsNullOrEmpty(ConsolidatedRecord.ErvVege))
                {
                    Record.ErvVege = ConsolidatedRecord.ErvVege;
                    Record.Updated.ErvVege = true;
                }

                Result resUpdate = this.Update(ExecParamPartner, Record);

                if (resUpdate.IsError)
                {
                    //HIBA
                    Logger.Error(String.Format("KRT_PARTNEREKService.Consolidate -> Hiba a partner módosítása során: Id: {0}", Record.Id));
                    log.WsEnd(ExecParamPartner, resUpdate);
                }
                else
                {
                    Logger.Info(String.Format("KRT_PARTNEREKService.Consolidate -> Partner módosítása rendben: Id: {0}", Record.Id));
                }
            }
            #endregion

            #region KRT_Szemelyek
            //KRT_Szemelyek.Partner_Id
            if (krt_szemely != null)
            {
                ExecParam ExecParamSzemely = ExecParam.Clone();
                ExecParamSzemely.Record_Id = krt_szemely.Id;

                KRT_Szemelyek local_krt_szemely = szemelyekService.Get(ExecParamSzemely).Record as KRT_Szemelyek;
                krt_szemely.Id = local_krt_szemely.Id;
                krt_szemely.Base.Ver = local_krt_szemely.Base.Ver;
                krt_szemely.Base.Updated.Ver = true;
                krt_szemely.Base.LetrehozasIdo = local_krt_szemely.Base.LetrehozasIdo;
                krt_szemely.Base.Updated.LetrehozasIdo = true;
                krt_szemely.Partner_Id = Record.Id;
                krt_szemely.Updated.Partner_Id = true;

                Result resUpdate = szemelyekService.Update(ExecParamSzemely, krt_szemely);

                if (resUpdate.IsError)
                {
                    //HIBA
                    Logger.Error(String.Format("KRT_PARTNEREKService.Consolidate -> Hiba a személy módosítása során: Id: {0}, Partner_Id: {1}", krt_szemely.Id, krt_szemely.Partner_Id));
                    log.WsEnd(ExecParamSzemely, resUpdate);
                }
                else
                {
                    Logger.Info(String.Format("KRT_PARTNEREKService.Consolidate -> Személy módosítása rendben: Id: {0}, Partner_Id: {1}", krt_szemely.Id, krt_szemely.Partner_Id));
                }
            }
            #endregion

            #region KRT_Vallalkozasok
            //KRT_Vallalkozasok.Partner_Id

            if (krt_vallalkozas != null)
            {
                ExecParam ExecParamVallalkozas = ExecParam.Clone();
                ExecParamVallalkozas.Record_Id = krt_vallalkozas.Id;
                KRT_Vallalkozasok local_krt_vallalkozas = vallalkozasokService.Get(ExecParamVallalkozas).Record as KRT_Vallalkozasok;
                krt_vallalkozas.Id = local_krt_vallalkozas.Id;
                krt_vallalkozas.Base.Ver = local_krt_vallalkozas.Base.Ver;
                krt_vallalkozas.Base.Updated.Ver = true;
                krt_vallalkozas.Partner_Id = Record.Id;
                krt_vallalkozas.Updated.Partner_Id = true;
                krt_vallalkozas.Base.LetrehozasIdo = local_krt_vallalkozas.Base.LetrehozasIdo;
                krt_vallalkozas.Base.Updated.LetrehozasIdo = true;

                Result resUpdate = vallalkozasokService.Update(ExecParamVallalkozas, krt_vallalkozas);

                if (resUpdate.IsError)
                {
                    //HIBA
                    Logger.Error(String.Format("KRT_PARTNEREKService.Consolidate -> Hiba a szervezet (vállalkozás) módosítása során: Id: {0}, Partner_Id: {1}", krt_vallalkozas.Id, krt_vallalkozas.Partner_Id));
                    log.WsEnd(ExecParamVallalkozas, resUpdate);
                }
                else
                {
                    Logger.Info(String.Format("KRT_PARTNEREKService.Consolidate -> Szervezet (vállalkozás) módosítása rendben: Id: {0}, Partner_Id: {1}", krt_vallalkozas.Id, krt_vallalkozas.Partner_Id));
                }
            }
            #endregion

            #region KRT_PartnerCimek
            //KRT_PartnerCimek.Partner_Id
            ExecParam ExecParamCimek = ExecParam.Clone();

            foreach (var cimId in partnerCimIds)
            {
                ExecParamCimek.Record_Id = cimId;
                KRT_PartnerCimek partnerCim = partnerCimekService.Get(ExecParamCimek).Record as KRT_PartnerCimek;

                partnerCim.Updated.SetValueAll(false);

                partnerCim.Partner_id = Record.Id;
                partnerCim.Updated.Partner_id = true;
                partnerCim.Base.Updated.Ver = true;

                Result resUpdate = partnerCimekService.Update(ExecParamCimek, partnerCim);

                if (resUpdate.IsError)
                {
                    //HIBA
                    Logger.Error(String.Format("KRT_PARTNEREKService.Consolidate -> Hiba a partnercím módosítása során: Id: {0}, Partner_Id: {1}", cimId, partnerCim.Partner_id));
                    log.WsEnd(ExecParamCimek, resUpdate);
                }
                else
                {
                    Logger.Info(String.Format("KRT_PARTNEREKService.Consolidate -> Partnercím módosítása rendben: Id: {0}, Partner_Id: {1}", cimId, partnerCim.Partner_id));
                }
            }
            #endregion

            #region KRT_PartnerKapcsolatok
            //KRT_PartnerCimek.Partner_Id
            ExecParam ExecParamKapcsolatok = ExecParam.Clone();

            foreach (var kapcsolatId in partnerKapcsolatIds)
            {
                ExecParamKapcsolatok.Record_Id = kapcsolatId;
                KRT_PartnerKapcsolatok partnerKapcsolat = pkService.Get(ExecParamKapcsolatok).Record as KRT_PartnerKapcsolatok;



                partnerKapcsolat.Updated.SetValueAll(false);
                //Alárendelt kapcsolatnál fordítva van a partnerId és a partnerId_kapcsolt.
                //Ezt kell kezelni:
                if (partnerKapcsolat.Partner_id_kapcsolt == ConsolidatedRecord.Id)
                {
                    partnerKapcsolat.Partner_id_kapcsolt = Record.Id;
                    partnerKapcsolat.Updated.Partner_id_kapcsolt = true;
                }
                else
                {
                    partnerKapcsolat.Partner_id = Record.Id;
                    partnerKapcsolat.Updated.Partner_id = true;
                }

                partnerKapcsolat.Base.Updated.Ver = true;
                Result resUpdate = pkService.Update(ExecParamKapcsolatok, partnerKapcsolat);

                if (resUpdate.IsError)
                {
                    //HIBA
                    Logger.Error(String.Format("KRT_PARTNEREKService.Consolidate -> Hiba a partnerkapcsolat módosítása során: Id: {0}, Partner_Id: {1}", kapcsolatId, partnerKapcsolat.Partner_id));
                    log.WsEnd(ExecParamKapcsolatok, resUpdate);
                }
                else
                {
                    Logger.Info(String.Format("KRT_PARTNEREKService.Consolidate -> Partnerkapcsolat módosítása rendben: Id: {0}, Partner_Id: {1}", kapcsolatId, partnerKapcsolat.Partner_id));
                }
            }
            #endregion


            //2. konszolidált kapcsolat felvétele
            KRT_PartnerKapcsolatok krt_partnerKapcsolatok = new KRT_PartnerKapcsolatok();

            krt_partnerKapcsolatok.Partner_id = Record.Id;
            krt_partnerKapcsolatok.Partner_id_kapcsolt = ConsolidatedRecord.Id;
            krt_partnerKapcsolatok.ErvKezd = Record.ErvKezd;
            krt_partnerKapcsolatok.ErvVege = Record.ErvVege;
            krt_partnerKapcsolatok.Tipus = Contentum.eUtility.KodTarak.PartnerKapcsolatTipus.konszolidalt;

            res = pkService.Insert(ExecParam, krt_partnerKapcsolatok);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            //3. a konszolidált partner invalidálása
            ExecParam execInvalidate = ExecParam.Clone();
            execInvalidate.Record_Id = ConsolidatedRecord.Id;

            res = this.InvalidateForConsolidate(execInvalidate);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, Record.Id, "KRT_Partnerek", "PartnerKonszolidacioNew").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "A konszolidáció során a partnerhez tartozó rekordok megfelelő mezőinek módosítása. Consolidated.PartnerId -> Record.PartnerId")]
    public Result UpdateConsolidatedIds(ExecParam ExecParam, string RecordId, string ConsolidatedRecordId)
    {
        Logger.Debug("UpdateConsolidatedIds start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Consolidate_Update(ExecParam, RecordId, ConsolidatedRecordId);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Vallalkozasok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    // BLG_1714
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CimekSearch))]
    public Result FindByPartnerAndCim(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, KRT_CimekSearch _KRT_CimekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.FindByPartnerAndCim(ExecParam, _KRT_PartnerekSearch, _KRT_CimekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //Generált függvények felülírása
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerSzemelyek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerVallalkozasok))]
    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            Result res = new Result();
            res = sp.Get(ExecParam);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            KRT_Partnerek krt_partnerek = (KRT_Partnerek)res.Record;

            switch (krt_partnerek.Tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_SzemelyekStoredProcedure spSzemelyek = new KRT_SzemelyekStoredProcedure(this.dataContext);
                        res = spSzemelyek.GetByPartner(ExecParam);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, res);
                            //return res;

                            throw new ResultException(res);
                        }
                        KRT_Szemelyek krt_szemelyek = new KRT_Szemelyek();
                        if (res.Record != null)
                        {
                            krt_szemelyek = (KRT_Szemelyek)res.Record;
                        }
                        KRT_PartnerSzemelyek krt_partnerSzemelyek = new KRT_PartnerSzemelyek(krt_partnerek, krt_szemelyek);
                        res.Record = krt_partnerSzemelyek;
                        break;
                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        KRT_VallalkozasokStoredProcedure spVallalkozasok = new KRT_VallalkozasokStoredProcedure(this.dataContext);
                        res = spVallalkozasok.GetByPartner(ExecParam);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, res);
                            //return res;

                            throw new ResultException(res);
                        }
                        KRT_Vallalkozasok krt_vallalkozasok = new KRT_Vallalkozasok();
                        if (res.Record != null)
                        {
                            krt_vallalkozasok = (KRT_Vallalkozasok)res.Record;
                        }
                        KRT_PartnerVallalkozasok krt_partnerVallalkozasok = new KRT_PartnerVallalkozasok(krt_partnerek, krt_vallalkozasok);
                        res.Record = krt_partnerVallalkozasok;
                        break;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }

            result = res;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;

    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerSzemelyek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerVallalkozasok))]
    public Result Insert(ExecParam ExecParam, KRT_Partnerek Record)
    {
        Logger.Debug("ExecParam: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("Partner neve: " + Record.Nev);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = new Result();

            //személyek esetén az insert elõtt ki kell tölteni a nevet
            if (Record.Tipus != KodTarak.Partner_Tipus.Szemely)
            {
                //LZS - BLG_1915
                if (string.IsNullOrEmpty(Record.Allapot))
                {
                    Record.Allapot = "JOVAHAGYANDO";
                }
                //partner insert
                res = sp.Insert(Constants.Insert, ExecParam, Record);

                //PartnerekListReset();

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, res);
                    //return res;

                    throw new ResultException(res);
                }

                //partnerrel azonos id-jú csoport insert-je, ha belsõ
                if (Record.Tipus != KodTarak.Partner_Tipus.Alkalmazas && Record.Belso == Contentum.eUtility.Constants.Database.Yes)
                {
                    KRT_CsoportokService svcCsoport = new KRT_CsoportokService(this.dataContext);
                    ExecParam xpmCsoport = ExecParam.Clone();
                    KRT_Csoportok csoport = new KRT_Csoportok();
                    csoport.Id = res.Uid;
                    csoport.Nev = Record.Nev;
                    csoport.Tipus = MapPartnerTipusToCsoportTipus(Record.Tipus);
                    csoport.System = "1";
                    csoport.ErvKezd = Record.ErvKezd;
                    csoport.ErvVege = Record.ErvVege;
                    csoport.JogosultsagOroklesMod = "1";

                    Result resCsoport = svcCsoport.Insert(xpmCsoport, csoport);

                    if (!String.IsNullOrEmpty(resCsoport.ErrorCode))
                    {
                        throw new ResultException(resCsoport);
                    }
                }
            }

            Result resTemp = new Result();

            switch (Record.Tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_Szemelyek szRecord = null;
                        try
                        {
                            KRT_PartnerSzemelyek partnerSzemely = (KRT_PartnerSzemelyek)Record;
                            szRecord = partnerSzemely.Szemelyek;
                            if (partnerSzemely.UpdatePartnerNevBySzemelyAdatok)
                            {
                                //megadott név üsszefûzése és beírása a partnerek táblába                            
                                Record.Nev = KRT_SzemelyekService.GetNevFromStrings(szRecord);
                            }
                        }
                        catch (InvalidCastException)
                        {
                        }
                        //partner insert
                        res = sp.Insert(Constants.Insert, ExecParam, Record);
                        res.CheckError();
                        //PartnerekListReset();

                        if (szRecord != null)
                        {
                            szRecord.Partner_Id = res.Uid;
                            szRecord.Updated.Partner_Id = true;
                            szRecord.Base.Tranz_id = Record.Base.Tranz_id; // ha jegyeztük a TRanz_id-t, akkor itt is átvesszük

                            KRT_SzemelyekService service = new KRT_SzemelyekService(this.dataContext);
                            resTemp = service.InsertWithFKResolution(ExecParam, szRecord);
                            resTemp.CheckError();
                        }
                        break;
                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        try
                        {
                            KRT_Vallalkozasok vRecord = ((KRT_PartnerVallalkozasok)Record).Vallalkozasok;
                            vRecord.Partner_Id = res.Uid;
                            vRecord.Updated.Partner_Id = true;
                            KRT_VallalkozasokService service = new KRT_VallalkozasokService(this.dataContext);
                            resTemp = service.Insert(ExecParam, vRecord);
                            resTemp.CheckError();
                        }
                        catch (InvalidCastException)
                        {
                        }
                        break;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Partnerek", "PartnerNew").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Partner felvétel címmel, illetve partnerkapcsolatok létrehozása (kapcsolattartók) címeikkel együtt
    /// (NMHH/SZURWebService felõl használva)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partner"></param>
    /// <param name="partnerCimek"></param>
    /// <param name="partnerKapcsolatTipus">A kapcsolat típusa a megadott partnerekkel</param>
    /// <param name="kapcsoltPartnerek">A partnerek, akikkel kapcsolatban van (létrehozandó új partnerek)</param>
    /// <param name="kapcsoltPartnerCimek">A kapcsolt partnerekhez tartozó címadatok (abban a sorrendben, ahogy a kapcsoltPartnerek-ben vannak)</param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(PartnerCimek))]
    public Result InsertWithCimekEsKapcsolatok(ExecParam execParam, KRT_Partnerek partner, PartnerCimek partnerCimek
                        , string partnerKapcsolatTipus, KRT_Partnerek[] kapcsoltPartnerek, PartnerCimek[] kapcsoltPartnerCimek)
    {
        /// Lépések:
        /// - KRT_Partnerek Insert
        /// - PartnerCimek-bõl az egyes címtipusokhoz külön-külön KRT_Cimek és KRT_PartnerCimek létrehozása
        /// - Kapcsolattartó partnerek létrehozása, kapcsolatok létrehozása (KRT_PartnerKapcsolatok)
        /// - Kapcsolattartó partnerekhez címek létrehozása
        /// 

        Logger.Debug("ExecParam: " + execParam.Felhasznalo_Id);
        Logger.Debug("Partner neve: " + partner.Nev);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellenõrzés

            // kapcsoltPartnerek és kapcsoltPartnerCimek elemszámának egyeznie kell:
            int kapcsoltPartnerElemszam = (kapcsoltPartnerek != null ? kapcsoltPartnerek.Length : 0);
            int kapcsoltPartnerCimekElemszam = (kapcsoltPartnerCimek != null ? kapcsoltPartnerCimek.Length : 0);
            if (kapcsoltPartnerElemszam != kapcsoltPartnerCimekElemszam)
            {
                throw new ResultException("Hibás paraméterek: 'kapcsoltPartnerek' és 'kapcsoltPartnerCimek' tömbök elemszáma eltér egymástól!");
            }

            #endregion

            // KRT_Partnerek Insert:
            result = this.Insert(execParam, partner);
            result.CheckError();

            string ujPartnerId = result.Uid;

            // PartnerCímek felvétele:
            this.CreatePartnerCimek(execParam, ujPartnerId, partnerCimek);

            #region Partnerkapcsolatok felvétele

            if (kapcsoltPartnerek != null)
            {
                for (int i = 0; i < kapcsoltPartnerek.Length; i++)
                {
                    KRT_Partnerek kapcsoltPartner = kapcsoltPartnerek[i];
                    PartnerCimek cimek = kapcsoltPartnerCimek[i];

                    Logger.Debug("Kapcsolt partner létrehozása: " + kapcsoltPartner.Nev);

                    // Partner létrehozása:
                    var resultKapcsoltPartnerInsert = this.Insert(execParam, kapcsoltPartner);
                    if (resultKapcsoltPartnerInsert.IsError)
                    {
                        throw new ResultException(resultKapcsoltPartnerInsert);
                    }
                    string ujKapcsoltPartnerId = resultKapcsoltPartnerInsert.Uid;

                    // Partnerkapcsolat létrehozása:
                    KRT_PartnerKapcsolatok partnerKapcsolat = new KRT_PartnerKapcsolatok();

                    partnerKapcsolat.Partner_id = ujPartnerId;
                    partnerKapcsolat.Partner_id_kapcsolt = ujKapcsoltPartnerId;

                    partnerKapcsolat.Tipus = partnerKapcsolatTipus;

                    var resultPartnerKapcs = new KRT_PartnerKapcsolatokService(this.dataContext).Insert(execParam, partnerKapcsolat);

                    if (resultPartnerKapcs.IsError)
                    {
                        throw new ResultException(resultPartnerKapcs);
                    }

                    // Kapcsolt partnerhez címek létrehozása:
                    this.CreatePartnerCimek(execParam, ujKapcsoltPartnerId, cimek);
                }
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Partner és címadatainak módosítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partner"></param>
    /// <param name="partnerCimek"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(PartnerCimek))]
    public Result UpdateWithCimek(ExecParam execParam, KRT_Partnerek partner, PartnerCimek partnerCimek)
    {

        Logger.Debug("ExecParam: " + execParam.Felhasznalo_Id);
        Logger.Debug("Partner neve: " + partner.Nev);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string partnerId = execParam.Record_Id;

            // Partner UPDATE:
            result = this.Update(execParam, partner);
            result.CheckError();

            // Címek módosítása:
            this.CreateOrModifyPartnerCimek(execParam, partnerId, partnerCimek);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Partnerhez címek felvétele
    /// </summary>
    /// <param name="partnerId"></param>
    /// <param name="partnerCimek"></param>
    private void CreatePartnerCimek(ExecParam execParam, string partnerId, PartnerCimek partnerCimek)
    {
        if (!String.IsNullOrEmpty(partnerCimek.Cim))
        {
            this.CreatePartnerPostaiCim(execParam, partnerId, partnerCimek);
        }

        // Egyéb címek felvétele:
        if (!String.IsNullOrEmpty(partnerCimek.Telefon))
        {
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Telefon, partnerCimek.Telefon);
        }
        if (!String.IsNullOrEmpty(partnerCimek.Mobil))
        {
            // TODO: kell ennek külön címtípus?
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Telefon, partnerCimek.Mobil);
        }
        if (!String.IsNullOrEmpty(partnerCimek.Email))
        {
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Email, partnerCimek.Email);
        }
        if (!String.IsNullOrEmpty(partnerCimek.Fax))
        {
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Fax, partnerCimek.Fax);
        }
    }

    /// <summary>
    /// Partner címeinek módosítása/felvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partnerId"></param>
    /// <param name="partnerCimek"></param>
    private void CreateOrModifyPartnerCimek(ExecParam execParam, string partnerId, PartnerCimek partnerCimek)
    {
        #region Megadott Partner címeinek lekérése

        var resultCimekGetAll = new KRT_CimekService(this.dataContext).GetAllByPartner(execParam
                , new KRT_Partnerek() { Id = partnerId }, String.Empty, new KRT_PartnerCimekSearch());
        resultCimekGetAll.CheckError();

        #endregion

        DataRow rowPostaiCim = null;
        DataRow rowTelefon = null;
        // TODO: kell majd mobil címtípus is
        // Addig is Telefon típusként próbáljuk kezelni mindkettõt
        //DataRow rowMobil = null;
        DataRow rowTelefon2 = null;
        DataRow rowEmail = null;
        DataRow rowFax = null;

        // A lekért partnercímekbõl kigyûjtjük a különbözõ típusú címeket:
        foreach (DataRow row in resultCimekGetAll.Ds.Tables[0].Rows)
        {
            string cimTipus = row["Tipus"].ToString();

            switch (cimTipus)
            {
                case KodTarak.Cim_Tipus.Postai:
                    rowPostaiCim = row;
                    break;
                case KodTarak.Cim_Tipus.Telefon:
                    if (rowTelefon == null)
                    {
                        rowTelefon = row;
                    }
                    else
                    {
                        // TODO: ez már nem kell, ha lesz külön "Mobil" típus:
                        rowTelefon2 = row;
                    }
                    break;
                // TODO:
                //case KodTarak.Cim_Tipus.Mobil:
                //    rowMobil = row;
                //    break;
                case KodTarak.Cim_Tipus.Email:
                    rowEmail = row;
                    break;
                case KodTarak.Cim_Tipus.Fax:
                    rowFax = row;
                    break;
            }
        }

        #region Postai cím

        // Ha nem volt még postai cím, és most van, akkor létrehozzuk
        // Ha már volt postai címe, akkor UPDATE:
        if (rowPostaiCim != null)
        {
            #region Postai cím UPDATE

            Guid cimId = (Guid)rowPostaiCim["Cim_Id"];

            KRT_Cimek cimUpdateObj = this.GetCimObjById(cimId, execParam);

            cimUpdateObj.Updated.SetValueAll(false);
            cimUpdateObj.Base.Updated.SetValueAll(false);

            // Módosítandó értékek beállítása:
            cimUpdateObj.IRSZ = partnerCimek.Iranyitoszam;
            cimUpdateObj.Updated.IRSZ = true;

            cimUpdateObj.TelepulesNev = partnerCimek.Telepules;
            cimUpdateObj.Updated.TelepulesNev = true;

            // Összefûzött cím:
            cimUpdateObj.Nev = String.Format("{0} {1}, {2}", partnerCimek.Iranyitoszam, partnerCimek.Telepules, partnerCimek.Cim);
            cimUpdateObj.Updated.Nev = true;

            // TODO: A KozteruletNev-be tesszük a Cim mezõt (ami tartalmazza a közterület típust ("utca") és házszámot is)
            cimUpdateObj.KozteruletNev = partnerCimek.Cim;
            cimUpdateObj.Updated.KozteruletNev = true;

            ExecParam execParamCimUpdate = execParam.Clone();
            execParamCimUpdate.Record_Id = cimId.ToString();

            var resultCimUpdate = new KRT_CimekService(this.dataContext).UpdateWithFKResolution(execParamCimUpdate, cimUpdateObj);
            if (resultCimUpdate.IsError)
            {
                throw new ResultException(resultCimUpdate);
            }
            #endregion
        }
        else if (!String.IsNullOrEmpty(partnerCimek.Cim))
        {
            this.CreatePartnerPostaiCim(execParam, partnerId, partnerCimek);
        }

        #endregion

        // Egyéb címek felvétel/módosítás:

        // Telefon:
        if (rowTelefon != null)
        {
            Guid cimId = (Guid)rowTelefon["Cim_Id"];
            // UPDATE:
            this.UpdatePartnerEgyebCim(execParam, cimId, partnerCimek.Telefon);
        }
        else if (!String.IsNullOrEmpty(partnerCimek.Telefon))
        {
            // INSERT:
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Telefon, partnerCimek.Telefon);
        }

        // TODO: Mobil:
        if (rowTelefon2 != null)
        {
            Guid cimId = (Guid)rowTelefon2["Cim_Id"];
            // UPDATE:
            this.UpdatePartnerEgyebCim(execParam, cimId, partnerCimek.Mobil);
        }
        else if (!String.IsNullOrEmpty(partnerCimek.Mobil))
        {
            // TODO: kell ennek külön címtípus?
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Telefon, partnerCimek.Mobil);
        }

        // Email:
        if (rowEmail != null)
        {
            Guid cimId = (Guid)rowEmail["Cim_Id"];
            // UPDATE:
            this.UpdatePartnerEgyebCim(execParam, cimId, partnerCimek.Email);
        }
        else if (!String.IsNullOrEmpty(partnerCimek.Email))
        {
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Email, partnerCimek.Email);
        }

        // Fax:
        if (rowFax != null)
        {
            Guid cimId = (Guid)rowFax["Cim_Id"];
            // UPDATE:
            this.UpdatePartnerEgyebCim(execParam, cimId, partnerCimek.Fax);
        }
        else if (!String.IsNullOrEmpty(partnerCimek.Fax))
        {
            this.CreatePartnerEgyebCim(execParam, partnerId, KodTarak.Cim_Tipus.Fax, partnerCimek.Fax);
        }
    }

    /// <summary>
    /// KRT_Cimek objektum lekérése Id alapján
    /// </summary>
    /// <param name="cimId"></param>
    /// <param name="execParam"></param>
    /// <returns></returns>
    private KRT_Cimek GetCimObjById(Guid cimId, ExecParam execParam)
    {
        var execParamGet = execParam.Clone();
        execParamGet.Record_Id = cimId.ToString();

        var result = new KRT_CimekService(this.dataContext).Get(execParamGet);
        result.CheckError();

        return result.Record as KRT_Cimek;
    }

    /// <summary>
    /// A partner cím adatokból a postai cím létrehozása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partnerId"></param>
    /// <param name="partnerCimek"></param>
    private void CreatePartnerPostaiCim(ExecParam execParam, string partnerId, PartnerCimek partnerCimek)
    {
        KRT_Cimek cim = new KRT_Cimek();
        cim.IRSZ = partnerCimek.Iranyitoszam;
        cim.TelepulesNev = partnerCimek.Telepules;
        // Összefûzött cím:
        cim.Nev = String.Format("{0} {1}, {2}", partnerCimek.Iranyitoszam, partnerCimek.Telepules, partnerCimek.Cim);
        cim.Tipus = KodTarak.Cim_Tipus.Postai;
        // TODO: A KozteruletNev-be tesszük a Cim mezõt (ami tartalmazza a közterület típust ("utca") és házszámot is)
        cim.KozteruletNev = partnerCimek.Cim;

        // TODO: ez mindenhol "K"-ra van állítva...
        cim.Kategoria = "K";

        var resultCimInsert = new KRT_CimekService(this.dataContext).InsertWithFKResolution(execParam, cim);
        resultCimInsert.CheckError();
        string ujCimId = resultCimInsert.Uid;

        // KRT_PartnerCimek felvétel:
        KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
        partnerCim.Cim_Id = ujCimId;
        partnerCim.Partner_id = partnerId;

        var resultPartnerCimInsert = new KRT_PartnerCimekService(this.dataContext).Insert(execParam, partnerCim);
        resultPartnerCimInsert.CheckError();
    }

    /// <summary>
    /// Partnerhez egyéb cím felvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partnerId"></param>
    /// <param name="egyebCimTipus"></param>
    /// <param name="egyebCimErtek"></param>
    private void CreatePartnerEgyebCim(ExecParam execParam, string partnerId, string egyebCimTipus, string egyebCimErtek)
    {
        KRT_Cimek cim = new KRT_Cimek();

        cim.CimTobbi = egyebCimErtek;
        cim.Tipus = egyebCimTipus;

        // TODO: ez mindenhol "K"-ra van állítva...
        cim.Kategoria = "K";

        var resultCimInsert = new KRT_CimekService(this.dataContext).InsertWithFKResolution(execParam, cim);
        resultCimInsert.CheckError();
        string ujCimId = resultCimInsert.Uid;

        // KRT_PartnerCimek felvétel:
        KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
        partnerCim.Cim_Id = ujCimId;
        partnerCim.Partner_id = partnerId;

        var resultPartnerCimInsert = new KRT_PartnerCimekService(this.dataContext).Insert(execParam, partnerCim);
        resultPartnerCimInsert.CheckError();
    }

    /// <summary>
    /// "Egyéb" típusú cím értékének módosítása ("CimTobbi" mezõ)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="cimId"></param>
    /// <param name="egyebCimErtek"></param>
    private void UpdatePartnerEgyebCim(ExecParam execParam, Guid cimId, string egyebCimErtek)
    {
        KRT_Cimek cimUpdateObj = this.GetCimObjById(cimId, execParam);

        cimUpdateObj.Updated.SetValueAll(false);
        cimUpdateObj.Base.Updated.SetValueAll(false);

        // Módosítandó érték beállítása:
        cimUpdateObj.CimTobbi = egyebCimErtek;
        cimUpdateObj.Updated.CimTobbi = true;

        ExecParam execParamCimUpdate = execParam.Clone();
        execParamCimUpdate.Record_Id = cimId.ToString();

        var resultCimUpdate = new KRT_CimekService(this.dataContext).Update(execParamCimUpdate, cimUpdateObj);
        resultCimUpdate.CheckError();
    }

    private string MapPartnerTipusToCsoportTipus(string partnerTipus)
    {
        string csoportTipus = KodTarak.CSOPORTTIPUS.Szervezet;
        switch (partnerTipus)
        {
            case KodTarak.Partner_Tipus.Szervezet:
                csoportTipus = KodTarak.CSOPORTTIPUS.Szervezet;
                break;
            case KodTarak.Partner_Tipus.Projekt:
                csoportTipus = KodTarak.CSOPORTTIPUS.Projekt;
                break;
            case KodTarak.Partner_Tipus.Bizottsag:
                csoportTipus = KodTarak.CSOPORTTIPUS.Testulet_Bizottsag;
                break;
            case KodTarak.Partner_Tipus.HivatalVezetes:
                csoportTipus = KodTarak.CSOPORTTIPUS.HivatalVezetes;
                break;
            default:
                csoportTipus = KodTarak.CSOPORTTIPUS.Szervezet;
                break;
        }

        return csoportTipus;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerSzemelyek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerVallalkozasok))]
    public Result Update(ExecParam ExecParam, KRT_Partnerek Record)
    {
        Logger.DebugStart();
        Logger.Debug("Partner Update Webservice elindul");
        Logger.Debug("ExecParam: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("Partner neve: " + Record.Nev);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = new Result();

            //személyek esetén az update elõtt ki kell tölteni a nevet
            if (Record.Tipus != KodTarak.Partner_Tipus.Szemely)
            {
                //partner update
                res = sp.Insert(Constants.Update, ExecParam, Record);
                res.CheckError();
                //PartnerekListReset();

                //partnerrel azonos id-jú csoport update-je, ha belsõ
                if (Record.Tipus != KodTarak.Partner_Tipus.Alkalmazas && Record.Belso == Contentum.eUtility.Constants.Database.Yes)
                {
                    Logger.Debug("A partnerhez tartozó csoport update elindul");
                    Logger.Debug("A partner verziója: " + Record.Base.Ver);

                    KRT_CsoportokService svcCsoport = new KRT_CsoportokService(this.dataContext);
                    ExecParam xpmCsoport = ExecParam.Clone();
                    Logger.Debug("A partnerhez tartozó csoport lekérése elindul");
                    Result resCsoportGet = svcCsoport.Get(xpmCsoport);
                    if (!String.IsNullOrEmpty(resCsoportGet.ErrorCode))
                    {
                        if (resCsoportGet.ErrorMessage == "[50101]") // a rekord nem található
                        {
                            Logger.Debug("partnerhez tartozó csoport nem található - új csoport létrehozása");

                            #region csoport insert
                            KRT_Csoportok csoport = new KRT_Csoportok();
                            csoport.Id = xpmCsoport.Record_Id;
                            csoport.Nev = Record.Nev;
                            csoport.Tipus = MapPartnerTipusToCsoportTipus(Record.Tipus);
                            csoport.System = "1";
                            csoport.ErvKezd = Record.ErvKezd;
                            csoport.ErvVege = Record.ErvVege;

                            Result resCsoportInsert = svcCsoport.Insert(xpmCsoport, csoport);

                            if (resCsoportInsert.IsError)
                            {
                                Logger.Error(String.Format("A partnerhez nem sikerült létrehozni a csoportot: {0},{1}", resCsoportInsert.ErrorCode, resCsoportInsert.ErrorMessage));
                                throw new ResultException(resCsoportInsert);
                            }
                            #endregion csoport insert

                            #region Partnerkapcsolatok leképzése

                            #region partnerkapcsolatok lekérése
                            ExecParam execParam_partnerkapcsolatokGetAll = ExecParam.Clone();

                            KRT_PartnerKapcsolatokService service_partnerkapcsolatok = new KRT_PartnerKapcsolatokService(this.dataContext);
                            KRT_PartnerKapcsolatokSearch search_partnerkapcsolatok = new KRT_PartnerKapcsolatokSearch();

                            Result res_partnerkapcsolatokGetAll = service_partnerkapcsolatok.GetAllByPartner(execParam_partnerkapcsolatokGetAll, search_partnerkapcsolatok);

                            if (res_partnerkapcsolatokGetAll.IsError)
                            {
                                Logger.Error(String.Format("A partnerhez nem sikerült lekérni a kapcsolatokat: {0},{1}", res_partnerkapcsolatokGetAll.ErrorCode, res_partnerkapcsolatokGetAll.ErrorMessage));
                                throw new ResultException(res_partnerkapcsolatokGetAll);
                            }

                            #endregion partnerkapcsolatok lekérése


                            // partnerkapcsolatoknak megfelelõ csoporttagságok lekérése - elõkészítés
                            List<string> partnerkapcsolatokIds = new List<string>();

                            foreach (DataRow row in res_partnerkapcsolatokGetAll.Ds.Tables[0].Rows)
                            {
                                string Id = row["Id"].ToString();
                                if (!String.IsNullOrEmpty(Id) && !partnerkapcsolatokIds.Contains(Id))
                                {
                                    partnerkapcsolatokIds.Add(Id);
                                }
                            }

                            if (partnerkapcsolatokIds.Count > 0)
                            {
                                #region partnerkapcsolatoknak megfelelõ csoporttagságok lekérése

                                KRT_CsoportTagokService service_csoporttagok = new KRT_CsoportTagokService(this.dataContext);
                                KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();
                                ExecParam execParam_csoporttagokGetAll = ExecParam.Clone();

                                search_csoporttagok.Id.Value = Search.GetSqlInnerString(partnerkapcsolatokIds.ToArray());
                                search_csoporttagok.Id.Operator = Query.Operators.inner;

                                Result res_csoporttagokGetAll = service_csoporttagok.GetAll(execParam_csoporttagokGetAll, search_csoporttagok);

                                if (res_csoporttagokGetAll.IsError)
                                {
                                    Logger.Error(String.Format("A partnerhez nem sikerült lekérni a kapcsolatokat: {0},{1}", res_csoporttagokGetAll.ErrorCode, res_csoporttagokGetAll.ErrorMessage));
                                    throw new ResultException(res_partnerkapcsolatokGetAll);
                                }

                                #endregion partnerkapcsolatoknak megfelelõ csoporttagságok lekérése

                                #region nem létezõ csoporttagságok a partnerkapcsolatokhoz

                                if (res_partnerkapcsolatokGetAll.Ds.Tables[0].Rows.Count > res_csoporttagokGetAll.Ds.Tables[0].Rows.Count)
                                {
                                    foreach (DataRow row in res_partnerkapcsolatokGetAll.Ds.Tables[0].Rows)
                                    {
                                        string Id = row["Id"].ToString();

                                        DataRow[] row_cst = res_csoporttagokGetAll.Ds.Tables[0].Select(String.Format("Id='{0}'", Id));
                                        if (row_cst == null || row_cst.Length == 0)
                                        {
                                            Logger.Debug(String.Format("Csoporttagság létrehozása a partnerkapcsolathoz: {0}", Id));
                                            Logger.Debug("Partnerkapcsolatnak megfelelõ csoporttagok leképzése");

                                            KRT_PartnerKapcsolatok krt_PartnerKapcsolatok = new KRT_PartnerKapcsolatok();
                                            Utility.LoadBusinessDocumentFromDataRow(krt_PartnerKapcsolatok, row);

                                            KRT_CsoportTagok krt_csoporttagok = service_partnerkapcsolatok.GetCsoportTagsag(ExecParam.Clone(), krt_PartnerKapcsolatok);
                                            if (krt_csoporttagok != null)
                                            {
                                                Logger.Debug("Partnerkapcsolatnak megfelelõ csoporttagok insert");
                                                KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
                                                //A partnerkapcsolat id-jával azonos id-jú csoportkapcsolat
                                                krt_csoporttagok.Id = Id;
                                                krt_csoporttagok.Updated.Id = true;

                                                ExecParam execParam_cs_insert = ExecParam.Clone();

                                                Result result_cst_insert = service.Insert(execParam_cs_insert, krt_csoporttagok);
                                                if (!String.IsNullOrEmpty(result_cst_insert.ErrorCode))
                                                {
                                                    //ContextUtil.SetAbort();
                                                    //log.WsEnd(ExecParam, result_cst_insert);
                                                    //return result_cst_insert;

                                                    throw new ResultException(result_cst_insert);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion nem létezõ csoporttagságok a partnerkapcsolatokhoz

                            #endregion Partnerkapcsolatok leképzése
                        }
                        else
                        {
                            Logger.Error(String.Format("A partnerhez nem sikerült lekérni a csoportot: {0},{1}", resCsoportGet.ErrorCode, resCsoportGet.ErrorMessage));
                            throw new ResultException(resCsoportGet);
                        }
                    }
                    else
                    {
                        Logger.Debug("partnerhez tartozó csoport lekérése lekérése vége");
                        KRT_Csoportok krt_csoportok = (KRT_Csoportok)resCsoportGet.Record;

                        if (krt_csoportok == null)
                        {
                            Logger.Error("A csoportok record null");
                            throw new ResultException("A csoportok record null");
                        }

                        krt_csoportok.Updated.SetValueAll(false);
                        krt_csoportok.Nev = Record.Nev;
                        krt_csoportok.Updated.Nev = Record.Updated.Nev;
                        krt_csoportok.Tipus = MapPartnerTipusToCsoportTipus(Record.Tipus);
                        krt_csoportok.Updated.Tipus = Record.Updated.Tipus;
                        krt_csoportok.ErvKezd = Record.ErvKezd;
                        krt_csoportok.Updated.ErvKezd = Record.Updated.ErvKezd;
                        krt_csoportok.ErvVege = Record.ErvVege;
                        krt_csoportok.Updated.ErvVege = Record.Updated.ErvVege;

                        Result resCsoport = svcCsoport.ForceUpdate(xpmCsoport, krt_csoportok);

                        if (!String.IsNullOrEmpty(resCsoport.ErrorCode))
                        {
                            Logger.Error(String.Format("A partnerhez tartozó csoport update hiba: {0}, {1}", resCsoport.ErrorCode, resCsoport.ErrorMessage));
                            throw new ResultException(resCsoport);
                        }

                        Logger.Debug("A partnerhez tartozó csoport update vége");
                    }
                }
            }

            Result resTemp = new Result();

            switch (Record.Tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_Szemelyek szRecord = null;
                        try
                        {
                            szRecord = ((KRT_PartnerSzemelyek)Record).Szemelyek;
                            KRT_PartnerSzemelyek partnerSzemely = (KRT_PartnerSzemelyek)Record;
                            szRecord = partnerSzemely.Szemelyek;
                            if (partnerSzemely.UpdatePartnerNevBySzemelyAdatok)
                            {
                                //megadott név üsszefûzése és beírása a partnerek táblába
                                Record.Nev = KRT_SzemelyekService.GetNevFromStrings(szRecord);
                            }
                        }
                        catch (InvalidCastException e)
                        {
                            Logger.Error(String.Format("Személyre kasztolás hiba: {0}", e.Message));
                        }
                        //partner update
                        res = sp.Insert(Constants.Update, ExecParam, Record);
                        res.CheckError();
                        //PartnerekListReset();

                        if (szRecord != null)
                        {
                            Logger.Debug("Személy update elindul");
                            KRT_SzemelyekService service = new KRT_SzemelyekService(this.dataContext);
                            resTemp = service.UpdateByPartner(ExecParam, szRecord);
                            if (!String.IsNullOrEmpty(resTemp.ErrorCode))
                            {
                                //ContextUtil.SetAbort();
                                //log.WsEnd(ExecParam, resTemp);
                                //return resTemp;
                                Logger.Error(String.Format("Személy update hiba: {0}, {1}", resTemp.ErrorCode, resTemp.ErrorMessage));
                                throw new ResultException(resTemp);
                            }
                            Logger.Debug("Személy update vége");
                        }
                        break;
                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        try
                        {
                            Logger.Debug("Szervezet update elindul");
                            KRT_Vallalkozasok vRecord = ((KRT_PartnerVallalkozasok)Record).Vallalkozasok;
                            KRT_VallalkozasokService service = new KRT_VallalkozasokService(this.dataContext);
                            resTemp = service.UpdateByPartner(ExecParam, vRecord);
                            if (!String.IsNullOrEmpty(resTemp.ErrorCode))
                            {
                                //ContextUtil.SetAbort();
                                //log.WsEnd(ExecParam, resTemp);
                                //return resTemp;
                                Logger.Error(String.Format("Szervezet update hiba: {0}, {1}", resTemp.ErrorCode, resTemp.ErrorMessage));
                                throw new ResultException(resTemp);
                            }
                            Logger.Debug("Szervezet update vége");
                        }
                        catch (InvalidCastException e)
                        {
                            Logger.Error(String.Format("Szervezetre kasztolás hiba: {0}", e.Message));
                        }
                        break;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Partner update hiba: {0}, {1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Partner Update Webservice vége");
        Logger.DebugEnd();
        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = new Result();
            Logger.Debug(String.Format("ParnterID: {0}", ExecParam.Record_Id));
            Logger.Debug("Partner lekerese start");
            Result resGet = this.Get(ExecParam);

            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error("Partner lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            KRT_Partnerek partner = (KRT_Partnerek)resGet.Record;
            Logger.Debug("Partner lekerese end");

            Logger.Debug("Partner Invalidalasa start");
            result = sp.Invalidate(ExecParam);
            //PartnerekListReset();
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("Partner Invalidalas hiba", ExecParam, result);
                throw new ResultException(result);
            }
            Logger.Debug("Partner Invalidalas end");

            Logger.Debug("Partnerhez tartozo szemely vagy vallalkozas Invalidalasa");
            ExecParam execTemp = ExecParam.Clone();
            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Debug("A partner szemely tipusu");
                //személyek táblában a partnerhez tartozó bejegyzés invalidálása
                KRT_SzemelyekService szService = new KRT_SzemelyekService(this.dataContext);
                Logger.Debug("Szemely lekerese start");
                res = szService.GetByPartner(execTemp);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Szemely lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Szemely lekerese end");
                if (res.Record != null)
                {
                    execTemp.Record_Id = ((KRT_Szemelyek)res.Record).Id;
                    if (!string.IsNullOrEmpty(execTemp.Record_Id))
                    {
                        Logger.Debug(String.Format("Szemely ({0}) Invalidate start", execTemp.Record_Id));
                        res = szService.Invalidate(execTemp);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            Logger.Error("Szemely Invalidate hiba", execTemp, res);
                            throw new ResultException(res);
                        }
                        Logger.Debug("Szemely Invalidate end");
                    }
                }
            }
            else if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
            {
                Logger.Debug("A partner szervezet tipusu");
                //vállalkozások táblában a partnerhez tartozó bejegyzés invalidálása
                KRT_VallalkozasokService vService = new KRT_VallalkozasokService(this.dataContext);
                Logger.Debug("Vallalkozas lekerese start");
                res = vService.GetByPartner(execTemp);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Vallalkozas lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Vallalkozas lekerese end");
                if (res.Record != null)
                {
                    execTemp.Record_Id = ((KRT_Vallalkozasok)res.Record).Id;
                    if (!string.IsNullOrEmpty(execTemp.Record_Id))
                    {
                        Logger.Debug(String.Format("Vallalkozas ({0}) Invalidate start", execTemp.Record_Id));
                        res = vService.Invalidate(execTemp);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            Logger.Error("Vallalkozas Invalidate hiba", execTemp, res);
                            throw new ResultException(res);
                        }
                        Logger.Debug("Vallalkozas Invalidate end");
                    }
                }
            }

            //A partnerhez tartozó kapcsolatok invalidálása
            //KRT_PartnerKapcsolatokService servicePartnerkapcsolatok = new KRT_PartnerKapcsolatokService(this.dataContext);
            //res = servicePartnerkapcsolatok.GetAllByPartner(ExecParam, new KRT_PartnerKapcsolatokSearch());

            //if (!String.IsNullOrEmpty(res.ErrorCode))
            //{
            //    throw new ResultException(res);
            //}

            //if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow rowPartnerKapcsolat in res.Ds.Tables[0].Rows)
            //    {
            //        ExecParam execPkInvalidate = ExecParam.Clone();
            //        execPkInvalidate.Record_Id = rowPartnerKapcsolat["Id"].ToString();
            //        //A konszolidációs kapcsolatokat nem invalidáljuk
            //        if (rowPartnerKapcsolat["Tipus"].ToString() != Contentum.eUtility.KodTarak.PartnerKapcsolatTipus.konszolidalt)
            //        {
            //            res = servicePartnerkapcsolatok.Invalidate(execPkInvalidate);
            //            if (!String.IsNullOrEmpty(res.ErrorCode))
            //            {
            //                throw new ResultException(res);
            //            }
            //        }
            //    }
            //}

            //A partnerhez tartotó csoport invalidlása
            if (partner.Belso == Contentum.eUtility.Constants.Database.Yes && partner.Tipus != KodTarak.Partner_Tipus.Alkalmazas && partner.Tipus != KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Debug(String.Format("Csoport ({0}) TryInvalidate hiba", ExecParam.Record_Id));
                KRT_CsoportokService svcCsoportok = new KRT_CsoportokService(this.dataContext);
                svcCsoportok.TryInvalidate(ExecParam);
                Logger.Debug("Csoport TryInvalidate end");
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    public Result InvalidateForConsolidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = new Result();
            Logger.Debug(String.Format("ParnterID: {0}", ExecParam.Record_Id));
            Logger.Debug("Partner lekerese start");
            Result resGet = this.Get(ExecParam);

            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error("Partner lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            KRT_Partnerek partner = (KRT_Partnerek)resGet.Record;
            Logger.Debug("Partner lekerese end");

            Logger.Debug("Partner Invalidalasa start: "+ ExecParam.Record_Id);
            result = sp.Invalidate(ExecParam);
            //PartnerekListReset();
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("Partner Invalidalas hiba", ExecParam, result);
                throw new ResultException(result);
            }
            Logger.Debug("Partner Invalidalas end");


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            if (_KRT_PartnerekSearch.Tipus.Operator == Query.Operators.equals)
            {
                switch (_KRT_PartnerekSearch.Tipus.Value)
                {
                    case KodTarak.Partner_Tipus.Szemely:
                        {
                            result = sp.GetAllWithSzemelyek(ExecParam, _KRT_PartnerekSearch);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                throw new ResultException(result);
                            }
                            break;
                        }
                    case KodTarak.Partner_Tipus.Szervezet:
                        {
                            result = sp.GetAllWithVallalkozasok(ExecParam, _KRT_PartnerekSearch);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                throw new ResultException(result);
                            }
                            break;
                        }
                    case KodTarak.Partner_Tipus.Alkalmazas:
                        result = sp.GetAllWithFK(ExecParam, _KRT_PartnerekSearch);
                        break;
                    default:
                        result = sp.GetAllWithFK(ExecParam, _KRT_PartnerekSearch);
                        break;
                }
            }
            else
            {
                result = sp.GetAllWithFK(ExecParam, _KRT_PartnerekSearch);
            }


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllWithExtnesions(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            if (_KRT_PartnerekSearch.Tipus.Operator == Query.Operators.equals)
            {
                switch (_KRT_PartnerekSearch.Tipus.Value)
                {
                    case KodTarak.Partner_Tipus.Szemely:
                        {
                            result = sp.GetAllWithSzemelyek(ExecParam, _KRT_PartnerekSearch, true);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                throw new ResultException(result);
                            }
                            break;
                        }
                    case KodTarak.Partner_Tipus.Szervezet:
                        {
                            result = sp.GetAllWithVallalkozasok(ExecParam, _KRT_PartnerekSearch, true);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                throw new ResultException(result);
                            }
                            break;
                        }
                    case KodTarak.Partner_Tipus.Alkalmazas:
                        break;
                    default:
                        break;
                }
            }

            result = sp.GetAllWithCim(ExecParam, _KRT_PartnerekSearch, new KRT_CimekSearch());

            #region Eseménynaplózás
            if (!Search.IsIdentical(_KRT_PartnerekSearch, new KRT_PartnerekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Partnerek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_PartnerekSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_KRT_PartnerekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_PartnerekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetAllForSzamla(ExecParam ExecParam, KRT_PartnerekSearch KRT_PartnerekSearch)
    /// A(z) KRT_PartnerekSearch táblára vonatkozó keresés eredményhalmazának elkérése, a PirEdokInterface által igényelt adatokra specializálva. A táblára vonatkozó szûrési feltételeket paraméter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemenõ paraméter. Az ExecParam.Record_Id nem használt, további adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).</param>
    /// <param name="_KRT_PartnerekSearch">Bemenõ paraméter, a keresési feltételeket tartalmazza. </param>
    /// <param name="Cim_Id">Bemenõ paraméter, a partner (postai) címének azonosítója a keresés szûkítéséhez</param>
    /// <param name="Bankszamlaszam_Id">Bemenõ paraméter, a partner bankszámlaszámának azonossítója a keresés szûkítéséhez</param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott táblára vonatkozó keresés eredményhalmazának elkérése, a PirEdokInterface által igényelt adatokra specializálva. A táblára vonatkozó szûrési feltételeket paraméter tartalmazza (*Search). Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetAllForSzamla(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch, string Cim_Id, string Bankszamlaszam_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllForSzamla(ExecParam, _KRT_PartnerekSearch, Cim_Id, Bankszamlaszam_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    //private static string[] PartnerekList = null;

    //private static void PartnerekListReset()
    //{
    //    PartnerekList = null;
    //}

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPartnerekList(string prefixText, int count, string contextKey)
    {
        // BUG_8419
        const string WithSeparatorsOption = "WithSeparators";
        bool withSeparators = !String.IsNullOrEmpty(contextKey) && contextKey.Contains(WithSeparatorsOption);
        if (withSeparators)
        {
            contextKey = contextKey.Replace(WithSeparatorsOption, "");
        }

        //
        string[] parameters = contextKey.Split(';');
        String userId = parameters[0];
        string withAllCim = Contentum.eUtility.Constants.Database.Yes;

        string withKuldemeny = Contentum.eUtility.Constants.Database.Yes;
        string _type = KodTarak.Partner_Tipusok_Filter.Mind.Name;
        if (parameters.Length > 1)
        {
            if (!String.IsNullOrEmpty(parameters[1]))
                withAllCim = parameters[1];

            if (withAllCim == "1" && withSeparators) { withAllCim = "2"; } // BUG_8419: get results with separators

            if (parameters.Length > 2)
            {
                if (!string.IsNullOrEmpty(parameters[2]))
                    withKuldemeny = parameters[2];
            }
            if (parameters.Length > 5)
            {
                if (!string.IsNullOrEmpty(parameters[4]))
                {
                    _type = parameters[4];
                }
            }
        }

        string cimTipus = null;

        if (System.Text.RegularExpressions.Regex.Match(contextKey, "CimTipus=([^;]*)").Success)
            cimTipus = System.Text.RegularExpressions.Regex.Match(contextKey, "CimTipus=([^;]*)").Groups[1].Value;

        KRT_PartnerekService partnerekservice = new KRT_PartnerekService();
        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;

        KRT_PartnerekSearch partnereksearch = new KRT_PartnerekSearch();
        partnereksearch.OrderBy = " Nev ASC, Id";

        if (!string.IsNullOrEmpty(prefixText))
        {
            partnereksearch.Nev.Like(prefixText + '%');
            partnereksearch.TopRow = count;


            SetPartnerSearchFilterByType(ref partnereksearch, _type);
            Result res = new Result();
            // BUG_5197 (nekrisz)
            //if (_type == KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Name)
            //{
            //    res = GetSzervezetekWithKapcsolattarto(execparam, partnereksearch);
            //}
            //else
            //{
            String withKapcsoltPartner = "0";
            if ((_type == KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Name) || (_type == KodTarak.Partner_Tipusok_Filter.Mind.Name))
            {
                withKapcsoltPartner = "1";
            }

            //Result res = partnerekservice.GetAllWithCim(execparam, partnereksearch, new KRT_CimekSearch(),withAllCim);
            res = partnerekservice.GetReallyAllWithCimExt(execparam, partnereksearch, new KRT_CimekSearch(), withAllCim, withKuldemeny, cimTipus, withKapcsoltPartner);
            // }
            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                return PartnerekDataSourceToStringPairArray(res);
            }
        }

        return null;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <param name="contextKey">
    /// <userid>;<parentpartnerid>
    /// </param>
    /// <returns></returns>
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMinositoPartnerekList(string prefixText, int count, string contextKey)
    {
        string[] mainParameters = contextKey.Split(';');
        if (mainParameters.Count() < 2)
            return null;

        string userId = mainParameters[0];
        string szulopartner = mainParameters[1];

        // BUG_13213
        //string withAllCim = Contentum.eUtility.Constants.Database.Yes;
        //string withKuldemeny = Contentum.eUtility.Constants.Database.Yes;		

        KRT_PartnerekService partnerekservice = new KRT_PartnerekService();
        ExecParam execparam = new ExecParam();
        execparam.Felhasznalo_Id = userId;

        KRT_PartnerekSearch partnereksearch = new KRT_PartnerekSearch();

        #region MINOSITO

        KRT_Partnerek partner = new KRT_Partnerek();
        partner.Id = szulopartner;

        KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
        search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Minosito;
        search.Tipus.Operator = Query.Operators.equals;

        Result resultPartnerKapcs = GetAllByPartner(execparam, partner, search);

        List<string> kapcsoltPartnerekLista = GetKapcsoltPartnerIds(resultPartnerKapcs);
        if (kapcsoltPartnerekLista == null || kapcsoltPartnerekLista.Count < 1)
            return null;
        SetPartnerSearchIds(ref partnereksearch, kapcsoltPartnerekLista);
        #endregion

        partnereksearch.OrderBy = " Nev ASC, Id";

        if (!string.IsNullOrEmpty(prefixText))
        {
            partnereksearch.Nev.Value = prefixText + '%';
            partnereksearch.Nev.Operator = Query.Operators.like;
            partnereksearch.TopRow = count;
              // BUG_13213
            //Result res = partnerekservice.GetReallyAllWithCim(execparam, partnereksearch, new KRT_CimekSearch(), withAllCim, withKuldemeny);
            Result res = partnerekservice.GetAll(execparam, partnereksearch);

            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                return PartnerekDataSourceToWebMethodsClass(res);
            }
        }
        else
        {
            partnereksearch.TopRow = count;
             // BUG_13213
            //Result res = partnerekservice.GetReallyAllWithCim(execparam, partnereksearch, new KRT_CimekSearch(), withAllCim, withKuldemeny);
            Result res = partnerekservice.GetAll(execparam, partnereksearch);

            if (string.IsNullOrEmpty(res.ErrorCode))
            {
                return PartnerekDataSourceToWebMethodsClass(res);
            }
        }

        return null;
    }
    /// <summary>
    /// Get Kapcsolt Partner Ids
    /// </summary>
    /// <param name="resultPartnerKapcs"></param>
    /// <returns></returns>
    private List<string> GetKapcsoltPartnerIds(Result resultPartnerKapcs)
    {
        List<string> kapcsoltPartnerekLista = new List<string>();
        if (resultPartnerKapcs.Ds != null && resultPartnerKapcs.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in resultPartnerKapcs.Ds.Tables[0].Rows)
            {
                string kapcsoltPartner = row["Partner_id_kapcsolt"].ToString();
                kapcsoltPartnerekLista.Add(kapcsoltPartner);
            }
            return kapcsoltPartnerekLista;
        }
        return null;
    }

    /// <summary>
    /// Partner keresõ obj szûrése id-kra
    /// </summary>
    /// <param name="searchPartner"></param>
    /// <param name="kapcsoltPartnerekLista"></param>
    private void SetPartnerSearchIds(ref KRT_PartnerekSearch searchPartner, List<string> kapcsoltPartnerekLista)
    {
        if (kapcsoltPartnerekLista != null && kapcsoltPartnerekLista.Count > 0)
        {
            string inQuery = null;
            foreach (var item in kapcsoltPartnerekLista)
            {
                if (!string.IsNullOrEmpty(inQuery))
                    inQuery += ", ";
                inQuery += "'" + item + "'";
            }
            searchPartner.Id.Value = inQuery;
            searchPartner.Id.Operator = Query.Operators.inner;
        }
    }

    private static string[] PartnerekDataSourceToStringPairArray(Result res)
    {
        bool needForras = true;
        string forrasDelimeter = ":   ";
        if (res.Ds != null)
        {
            string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
            string value = "";
            string text = "";
            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                value = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                value += ";" + res.Ds.Tables[0].Rows[i]["CimId"].ToString();
                if (needForras)
                {
                    if (!String.IsNullOrEmpty(res.Ds.Tables[0].Rows[i]["Forras"].ToString()))
                    {
                        text = res.Ds.Tables[0].Rows[i]["Forras"].ToString() + forrasDelimeter;
                    }
                    else
                    {
                        text = String.Empty;
                    }
                }
                else
                {
                    text = String.Empty;
                }

                text += res.Ds.Tables[0].Rows[i]["Nev"].ToString();
                if (!String.IsNullOrEmpty(res.Ds.Tables[0].Rows[i]["CimNev"].ToString()))
                    text += Contentum.eUtility.Constants.AutoComplete.delimeter + res.Ds.Tables[0].Rows[i]["CimNev"].ToString();

                string item = Utility.CreateAutoCompleteItem(text, value);
                StringArray.SetValue(item, i);
            }
            return StringArray;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Return webmethod class from dataset
    /// </summary>
    /// <param name="res"></param>
    /// <returns></returns>
    private static string[] PartnerekDataSourceToWebMethodsClass(Result res)
    {
        List<string> result = new List<string>();
        bool needForras = true;
        string forrasDelimeter = ":   ";
        if (res.Ds != null)
        {
            string id = "";
            string nev = "";

            // BUG_13213
            //string cimid = "";
            //string cimnev = "";

            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                id = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                 // BUG_13213
                //cimid = res.Ds.Tables[0].Rows[i]["CimId"].ToString();
				if (needForras)
                {
                    if (!String.IsNullOrEmpty(res.Ds.Tables[0].Rows[i]["Forras"].ToString()))
                    {
                        nev = res.Ds.Tables[0].Rows[i]["Forras"].ToString() + forrasDelimeter;
                    }
                    else
                    {
                        nev = String.Empty;
                    }
                }
                else
                {
                    nev = String.Empty;
                }

                nev += res.Ds.Tables[0].Rows[i]["Nev"].ToString();
                     // BUG_13213
                //if (!String.IsNullOrEmpty(res.Ds.Tables[0].Rows[i]["CimNev"].ToString()))
                //    cimnev = res.Ds.Tables[0].Rows[i]["CimNev"].ToString();
                //result.Add(string.Format("{0}|{1}|{2}|{3}", id, nev, cimid, cimnev));
                result.Add(string.Format("{0}|{1}", id, nev));     
            }
            return result.ToArray();
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Visszaadja a felhasználóhoz tartozó személy típusú partnert
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="?"></param>
    /// <returns></returns>
    public KRT_PartnerSzemelyek GetSzemelyByFelhasznalo(ExecParam execParam, KRT_Felhasznalok felhasznalo)
    {
        if (String.IsNullOrEmpty(felhasznalo.Partner_id.Trim()))
        {
            throw new ResultException("A felhasználóhoz nem tartozik partner");
        }

        ExecParam xpmPartner = execParam.Clone();
        xpmPartner.Record_Id = felhasznalo.Partner_id;
        Result resPartner = this.Get(xpmPartner);

        if (!String.IsNullOrEmpty(resPartner.ErrorCode))
        {
            throw new ResultException(resPartner);
        }

        KRT_PartnerSzemelyek szemely = null;

        if (resPartner.Record == null)
        {
            throw new ResultException("A lekért record null");
        }

        try
        {
            szemely = (KRT_PartnerSzemelyek)resPartner.Record;
        }
        catch (InvalidCastException e)
        {
            throw new ResultException("A partner személlyé kasztolása sikertelen: " + e.Message);
        }

        return szemely;
    }


    [System.Web.Services.WebMethod]
    public Result GetEmailPartnerAdatok(ExecParam execParam, string felado, string cimzett)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Partner adatok lekérése email feladó/címzett alapján - Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result.Ds = new DataSet();

            if (!String.IsNullOrEmpty(felado))
            {
                #region Partner megkeresése felado alapján

                Logger.Debug("Feladó: " + felado, execParam);

                KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
                KRT_CimekSearch cimekSearch = new KRT_CimekSearch();

                // ha a felado egy e-mail cím, a partner címeként keresünk rá, egyébként a partnerek nevében
                // 2008.07.15: ez így timeout-ol: vagy index kell a CimTobbi mezõre, vagy valahogy máshogy kellene lekérni...
                if (felado.Contains("@"))
                {
                    // VAGY név VAGY CimTobbi:
                    cimekSearch.Nev.Value = felado;
                    cimekSearch.Nev.Operator = Query.Operators.equals;
                    //cimekSearch.Nev.Group = "42";
                    //cimekSearch.Nev.GroupOperator = Query.Operators.or;

                    //cimekSearch.CimTobbi.Value = felado;
                    //cimekSearch.CimTobbi.Operator = Query.Operators.equals;
                    //cimekSearch.CimTobbi.Group = "42";
                    //cimekSearch.CimTobbi.GroupOperator = Query.Operators.or;
                }
                else
                {
                    partnerekSearch.Nev.Value = "*" + felado;
                    partnerekSearch.Nev.Operator = Query.Operators.like;
                }

                // hátha túl sok adat jönne:
                cimekSearch.TopRow = 100;
                partnerekSearch.TopRow = 100;

                ExecParam execParam_feladoGet = execParam.Clone();

                Result result_feladoGet = this.GetAllWithCim(execParam_feladoGet, partnerekSearch, cimekSearch);
                if (!String.IsNullOrEmpty(result_feladoGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_feladoGet);
                }

                DataTable table_felado = result_feladoGet.Ds.Tables[0].Copy();
                table_felado.TableName = "Felado";

                result.Ds.Tables.Add(table_felado);

                Logger.Debug("Találatok száma: " + table_felado.Rows.Count, execParam);

                /// BUG#6879: 
                /// Ha az emailcímet nem sikerült megtalálni a partnercímek között, akkor még rákeresünk a KRT_Felhasznalok.EMail mezõjére, 
                /// hátha belsõ emailcímrõl van szó
                /// 
                if (table_felado.Rows.Count == 0
                    && felado.Contains("@"))
                {
                    #region KRT_Felhasznalok.EMail mezõben keresés

                    KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                    felhasznalokSearch.EMail.Value = felado;
                    felhasznalokSearch.EMail.Operator = Query.Operators.equals;

                    Result result_felhGetAll = new KRT_FelhasznalokService(this.dataContext).GetAll(execParam.Clone(), felhasznalokSearch);
                    result_felhGetAll.CheckError();

                    Guid? partnerIdFelado = null;
                    if (result_felhGetAll.Ds.Tables[0].Rows.Count > 0)
                    {
                        partnerIdFelado = result_felhGetAll.Ds.Tables[0].Rows[0]["Partner_id"] as Guid?;
                    }
                    #endregion

                    if (partnerIdFelado != null)
                    {
                        #region Partner lekérése (KRT_Felhasznalok.Partner_id)

                        partnerekSearch = new KRT_PartnerekSearch();
                        partnerekSearch.Id.Value = partnerIdFelado.ToString();
                        partnerekSearch.Id.Operator = Query.Operators.equals;

                        var resultFlhPartner = this.GetAllWithCim(execParam.Clone(), partnerekSearch, null);
                        resultFlhPartner.CheckError();

                        if (resultFlhPartner.Ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in resultFlhPartner.Ds.Tables[0].Rows)
                            {
                                table_felado.ImportRow(row);
                            }
                        }

                        #endregion
                    }
                }

                if (table_felado.Rows.Count > 0)
                {
                    #region Ha talált a feladóhoz partnert, és az belsõ, szervezetének lekérése

                    DataRow row = table_felado.Rows[0];
                    string belso = row["Belso"].ToString();
                    if (belso == "1")
                    {
                        KRT_Partnerek partnerFelado = new KRT_Partnerek();
                        partnerFelado.Id = row["Id"].ToString();

                        KRT_PartnerKapcsolatokSearch partnerKapcsolatokSearch = new KRT_PartnerKapcsolatokSearch();
                        ExecParam execParam_feladoSzervezetGet = execParam.Clone();

                        Result result_feladoSzervezetGet = this.GetAllByPartner(execParam_feladoSzervezetGet, partnerFelado, partnerKapcsolatokSearch);
                        if (!String.IsNullOrEmpty(result_feladoSzervezetGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_feladoSzervezetGet);
                        }

                        DataTable table_feladoSzervezet = result_feladoSzervezetGet.Ds.Tables[0].Copy();
                        table_feladoSzervezet.TableName = "FeladoSzervezet";

                        result.Ds.Tables.Add(table_feladoSzervezet);
                    }

                    #endregion
                }

                #endregion
            }

            if (!String.IsNullOrEmpty(cimzett))
            {
                #region Cimzett (felhasználó/csoport) megkeresése a cimzett string alapján

                Logger.Debug("Cimzett: " + cimzett, execParam);

                KRT_FelhasznalokService service_felhasznalok = new KRT_FelhasznalokService(this.dataContext);

                KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                felhasznalokSearch.EMail.Value = cimzett;
                felhasznalokSearch.EMail.Operator = Query.Operators.equals;

                ExecParam execParam_felhGetAll = execParam.Clone();

                Result result_felhGetAll = service_felhasznalok.GetAll(execParam_felhGetAll, felhasznalokSearch);
                if (!String.IsNullOrEmpty(result_felhGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_felhGetAll);
                }

                DataTable table_cimzett = result_felhGetAll.Ds.Tables[0].Copy();
                table_cimzett.TableName = "Cimzett";

                result.Ds.Tables.Add(table_cimzett);

                if (table_cimzett.Rows.Count > 0)
                {
                    #region Címzett szervezetének lekérése

                    DataRow row = table_cimzett.Rows[0];
                    string csoportId = row["Id"].ToString();

                    KRT_CsoportTagokService service_csoporttagok = new KRT_CsoportTagokService(this.dataContext);
                    KRT_CsoportTagokSearch csoporttagokSearch = new KRT_CsoportTagokSearch();

                    csoporttagokSearch.Csoport_Id_Jogalany.Value = csoportId;
                    csoporttagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                    Result result_csoporttagokGetAll = service_csoporttagok.GetAll(execParam, csoporttagokSearch);
                    if (!String.IsNullOrEmpty(result_csoporttagokGetAll.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_csoporttagokGetAll);
                    }

                    if (result_csoporttagokGetAll.Ds.Tables[0].Rows.Count > 0)
                    {
                        // van találat:
                        DataRow row_csoporttagok = result_csoporttagokGetAll.Ds.Tables[0].Rows[0];
                        string szervezetId = row_csoporttagok["Csoport_Id"].ToString();

                        KRT_CsoportokService service_csoportok = new KRT_CsoportokService(this.dataContext);

                        KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();

                        csoportokSearch.Id.Value = szervezetId;
                        csoportokSearch.Id.Operator = Query.Operators.equals;

                        Result result_cimzettSzervezet = service_csoportok.GetAll(execParam, csoportokSearch);
                        if (!String.IsNullOrEmpty(result_cimzettSzervezet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_cimzettSzervezet);
                        }

                        DataTable table_cimzettSzervezet = result_cimzettSzervezet.Ds.Tables[0].Copy();
                        table_cimzettSzervezet.TableName = "CimzettSzervezet";

                        result.Ds.Tables.Add(table_cimzettSzervezet);
                    }

                    #endregion
                }


                #endregion
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Revalidate(ExecParam ExecParam)
    {
        Logger.Debug("Partner revalidalasa start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = new Result();
            Logger.Debug(String.Format("ParnterID: {0}", ExecParam.Record_Id));
            Logger.Debug("Partner lekerese start");
            Result resGet = this.Get(ExecParam);

            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error("Partner lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            KRT_Partnerek partner = (KRT_Partnerek)resGet.Record;
            Logger.Debug("Partner lekerese end");

            result = sp.Revalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("Partner revalidalasa sp hiba", ExecParam, result);
                throw new ResultException(result);
            }

            Logger.Debug("Partnerhez tartozo szemely vagy vallalkozas Revalidalasa");
            ExecParam execTemp = ExecParam.Clone();
            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Debug("A partner szemely tipusu");
                //személyek táblában a partnerhez tartozó bejegyzés revalidálása
                KRT_SzemelyekService szService = new KRT_SzemelyekService(this.dataContext);
                Logger.Debug("Szemely lekerese start");
                // BUG_12079
                //res = szService.GetByPartner(execTemp);
                KRT_SzemelyekSearch szemelyekSearch = new KRT_SzemelyekSearch();
                szemelyekSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
                szemelyekSearch.ErvKezd.AndGroup("100");

                szemelyekSearch.ErvVege.LessOrEqual(Query.SQLFunction.getdate);
                szemelyekSearch.ErvVege.AndGroup("100");

                szemelyekSearch.Partner_Id.Value = ExecParam.Record_Id;
                szemelyekSearch.Partner_Id.Operator = Query.Operators.equals;
                
                res = szService.GetAll(execTemp, szemelyekSearch);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Szemely lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Szemely lekerese end");
                // BUG_12079
                //if (res.Record != null)
                //{
                //  execTemp.Record_Id = ((KRT_Szemelyek)res.Record).Id;
                if (res.Ds.Tables[0].Rows.Count >0)
                {                   
                    execTemp.Record_Id = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                    if (!string.IsNullOrEmpty(execTemp.Record_Id))
                    {
                        Logger.Debug(String.Format("Szemely ({0}) Revalidate start", execTemp.Record_Id));
                        res = szService.Revalidate(execTemp);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            Logger.Error("Szemely Revalidate hiba", execTemp, res);
                            throw new ResultException(res);
                        }
                        Logger.Debug("Szemely Revalidate end");
                    }
                }
            }
            else if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
            {
                Logger.Debug("A partner szervezet tipusu");
                //vállalkozások táblában a partnerhez tartozó bejegyzés revalidalasa
                KRT_VallalkozasokService vService = new KRT_VallalkozasokService(this.dataContext);
                Logger.Debug("Vallalkozas lekerese start");
                // BUG_12079
                //res = vService.GetByPartner(execTemp);
                // BUG_12079
                KRT_VallalkozasokSearch vallalkozasokSearch = new KRT_VallalkozasokSearch();
                vallalkozasokSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
                vallalkozasokSearch.ErvKezd.AndGroup("100");

                vallalkozasokSearch.ErvVege.LessOrEqual(Query.SQLFunction.getdate);
                vallalkozasokSearch.ErvVege.AndGroup("100");

                vallalkozasokSearch.Partner_Id.Value = ExecParam.Record_Id;
                vallalkozasokSearch.Partner_Id.Operator = Query.Operators.equals;
                //res = szService.GetByPartner(execTemp);
                res = vService.GetAll(execTemp, vallalkozasokSearch);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Vallalkozas lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Vallalkozas lekerese end");
                // BUG_12079
                //if (res.Record != null)
                //{
                //    execTemp.Record_Id = ((KRT_Vallalkozasok)res.Record).Id;
                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    //execTemp.Record_Id = ((KRT_Szemelyek)res.Record).Id;
                    execTemp.Record_Id = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                    if (!string.IsNullOrEmpty(execTemp.Record_Id))
                    {
                        Logger.Debug(String.Format("Vallalkozas ({0}) Revalidate start", execTemp.Record_Id));
                        res = vService.Revalidate(execTemp);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            Logger.Error("Vallalkozas Revalidate hiba", execTemp, res);
                            throw new ResultException(res);
                        }
                        Logger.Debug("Vallalkozas Revalidate end");
                    }
                }
            }

            //A partnerhez tartotó csoport revalidalasa
            if (partner.Belso == Contentum.eUtility.Constants.Database.Yes && partner.Tipus != KodTarak.Partner_Tipus.Alkalmazas && partner.Tipus != KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Debug("Csoport Revalidate start");
                KRT_CsoportokService svcCsoportok = new KRT_CsoportokService(this.dataContext);
                Result resCsoportok = svcCsoportok.Revalidate(ExecParam);
                if (resCsoportok.IsError)
                {
                    //ha a csoport már érévnyes nem dobunk hibát
                    if (resCsoportok.ErrorMessage != "[50653]")
                    {
                        Logger.Error("Csoport Revalidate hiba", ExecParam, resCsoportok);
                    }
                }
                Logger.Debug("Csoport Revalidate end");
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "Revalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("Partner revalidalasa hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("Partner revalidalasa end");
        return result;
    }

    /// <summary>
    /// A partnerhez tartozó adószám lekérése a partner típusától (személy, vállalkozás) függõen.
    /// </summary>
    /// <param name="ExecParam">A Record_Id mezõben kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.</param>
    /// <returns>A Result.Record mezõben az adószámot tartalmazó objektumot (KRT_Szemelyek vagy KRT_Vallakozasok) tartalmazza, ha a partner személy vagy vállalkozás, egyébként null értéket.</returns>
    [WebMethod(Description = "A partnerhez tartozó adószám lekérése a partner típusától (személy, vállalkozás) függõen. Az ExecParam Record_Id mezõjében kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza. Visszatéréskor a Result.Record mezõben az adószámot tartalmazó objektumot (KRT_Szemelyek vagy KRT_Vallakozasok) tartalmazza, ha a partner személy vagy vállalkozás, egyébként null értéket.")]
    private Result GetSzemelyVallakozasRecordByPartner(ExecParam ExecParam)
    {
        Logger.Debug("GetSzemelyVallakozasRecordByPartner start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException(50660); //"Nincs megadva a partner azonosítója!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = new Result();
            Logger.Debug(String.Format("Partner_Id: {0}", ExecParam.Record_Id));
            Logger.Debug("Partner lekerese start");
            Result resGet = this.Get(ExecParam);

            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error("Partner lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            KRT_Partnerek partner = (KRT_Partnerek)resGet.Record;
            Logger.Debug("Partner lekerese end");

            Logger.Debug("Partnerhez tartozo szemely vagy vallalkozas lekerese");
            ExecParam execTemp = ExecParam.Clone();
            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Debug("A partner szemely tipusu");
                //személyek táblában a partnerhez tartozó bejegyzés visszaolvasása
                KRT_SzemelyekService szService = new KRT_SzemelyekService(this.dataContext);
                Logger.Debug("Szemely lekerese start");
                res = szService.GetByPartner(execTemp);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Szemely lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Szemely lekerese end");
                if (res.Record != null)
                {
                    result.Record = res.Record;
                }
            }
            else if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
            {
                Logger.Debug("A partner szervezet tipusu");
                //vállalkozások táblában a partnerhez tartozó bejegyzés visszaolvasása
                KRT_VallalkozasokService vService = new KRT_VallalkozasokService(this.dataContext);
                Logger.Debug("Vallalkozas lekerese start");
                res = vService.GetByPartner(execTemp);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error("Vallalkozas lekerese hiba", execTemp, res);
                    throw new ResultException(res);
                }
                Logger.Debug("Vallalkozas lekerese end");
                if (res.Record != null)
                {
                    result.Record = res.Record;
                }
            }
            else
            {
                result.Record = null;
                Logger.Debug("A partner nem szemely vagy vallalkozas, visszateres NULL ertekkel.");
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("GetSzemelyVallakozasRecordByPartner hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("GetSzemelyVallakozasRecordByPartner end");
        return result;
    }

    /// <summary>
    /// A partnerhez tartozó adószám lekérése a partner típusától (személy, vállalkozás) függõen.
    /// </summary>
    /// <param name="ExecParam">A Record_Id mezõben kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.</param>
    /// <returns>A Result.Record mezõben az adószámot, mint nem-null stringet tartalmazza, ha a partner személy vagy vállalkozás, egyébként null értéket.</returns>
    [WebMethod(Description = "A partnerhez tartozó adószám lekérése a partner típusától (személy, vállalkozás) függõen. Az ExecParam Record_Id mezõjében kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza. Visszatéréskor a Result.Record mezõben az adószámot, mint nem-null stringet tartalmazza, ha a partner személy vagy vállalkozás, egyébként pedig null értéket.")]
    [System.Xml.Serialization.XmlInclude(typeof(String[]))]
    public Result GetAdoszamByPartner(ExecParam ExecParam)
    {
        Logger.Debug("GetAdoszamByPartner start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException(50660); //"Nincs megadva a partner azonosítója!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = this.GetSzemelyVallakozasRecordByPartner(ExecParam);

            if (res.IsError)
            {
                throw new ResultException(res);
            }

            if (res.Record == null)
            {
                result.Record = null;
            }
            else if (res.Record is KRT_Vallalkozasok)
            {
                KRT_Vallalkozasok krt_Vallalkozasok = (KRT_Vallalkozasok)res.Record;

                result.Record = new string[] { krt_Vallalkozasok.Adoszam, String.IsNullOrEmpty(krt_Vallalkozasok.KulfoldiAdoszamJelolo) ? "0" : krt_Vallalkozasok.KulfoldiAdoszamJelolo };
            }
            else if (res.Record is KRT_Szemelyek)
            {
                KRT_Szemelyek krt_Szemelyek = (KRT_Szemelyek)res.Record;

                result.Record = new string[] { krt_Szemelyek.Adoszam, String.IsNullOrEmpty(krt_Szemelyek.KulfoldiAdoszamJelolo) ? "0" : krt_Szemelyek.KulfoldiAdoszamJelolo };
            }
            else
            {
                result.Record = null;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "ModifyAdoszam").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("GetAdoszamByPartner hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("GetAdoszamByPartner end");
        return result;
    }

    /// <summary>
    /// A partnerhez tartozó adószám mentése a partner típusától (személy, vállalkozás) függõen a megfelelõ rekordba.
    /// </summary>
    /// <param name="ExecParam">A Record_Id mezõben kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.</param>
    /// <param name="Adoszam">Adószám</param>
    /// <param name="IsKulfoldiAdoszam">Értéke true, ha külföldi az adószám</param>
    /// <param name="EnableSzemelyVallakozasInsert">Ha értéke true, a  személy vagy vállakozás típusú parner hiányzó hozzárendelt rekordja beszúrásra kerül.</param>
    /// <returns></returns>
    [WebMethod(Description = "A partnerhez tartozó adószám mentése a partner típusától (személy, vállalkozás) függõen a megfelelõ rekordba. Az ExecParam Record_Id mezõjében kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.")]
    public Result UpdateAdoszam(ExecParam ExecParam, string Adoszam, bool IsKulfoldiAdoszam, bool EnableSzemelyVallakozasInsert)
    {
        Logger.Debug("UpdateAdoszam start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException(50660); //"Nincs megadva a partner azonosítója!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = new Result();
            Logger.Debug(String.Format("Partner_Id: {0}", ExecParam.Record_Id));

            res = this.GetSzemelyVallakozasRecordByPartner(ExecParam);

            if (res.IsError)
            {
                Logger.Error("Partnerhez tartozo szemely vagy vallakozas lekerese hiba", ExecParam, res);
                throw new ResultException(res);
            }

            if (res.Record == null)
            {
                // TODO: Hibauzenet
                Logger.Error("Partnerhez tartozo szemely vagy vallakozas lekerese hiba", ExecParam, res);
                throw new ResultException(53160); //"A partnerhez tartozó személy vagy vállalkozás nem található!"
            }
            else if (res.Record is KRT_Vallalkozasok)
            {
                KRT_Vallalkozasok krt_Vallalkozasok = (KRT_Vallalkozasok)res.Record;

                if (String.IsNullOrEmpty(krt_Vallalkozasok.Id))
                {
                    //Rekord lekérése az adatbázisból sikertelen
                    if (EnableSzemelyVallakozasInsert)
                    {
                        KRT_VallalkozasokService service_vallalkozas = new KRT_VallalkozasokService(this.dataContext);

                        ExecParam execParam_insert = ExecParam.Clone();

                        // insert
                        krt_Vallalkozasok.Partner_Id = ExecParam.Record_Id;
                        krt_Vallalkozasok.Updated.Partner_Id = true;

                        krt_Vallalkozasok.Adoszam = Adoszam;
                        krt_Vallalkozasok.Updated.Adoszam = true;

                        krt_Vallalkozasok.KulfoldiAdoszamJelolo = (IsKulfoldiAdoszam ? "1" : "0");
                        krt_Vallalkozasok.Updated.KulfoldiAdoszamJelolo = true;

                        Result result_insert = service_vallalkozas.Insert(execParam_insert, krt_Vallalkozasok);

                        if (result_insert.IsError)
                        {
                            throw new ResultException(result_insert);
                        }
                    }
                    else
                    {
                        Logger.Error("Partnerhez tartozo szemely vagy vallakozas lekerese hiba", ExecParam, res);
                        throw new ResultException(53160); //"A partnerhez tartozó személy vagy vállalkozás nem található!"
                    }
                }
                else if (Adoszam != krt_Vallalkozasok.Adoszam || (IsKulfoldiAdoszam && krt_Vallalkozasok.KulfoldiAdoszamJelolo != "1") || (!IsKulfoldiAdoszam && krt_Vallalkozasok.KulfoldiAdoszamJelolo == "1"))
                {
                    KRT_VallalkozasokService service_vallalkozas = new KRT_VallalkozasokService(this.dataContext);

                    // update
                    krt_Vallalkozasok.Updated.SetValueAll(false);
                    krt_Vallalkozasok.Base.Updated.SetValueAll(false);

                    ExecParam execParam_update = ExecParam.Clone();
                    execParam_update.Record_Id = krt_Vallalkozasok.Id;

                    krt_Vallalkozasok.Adoszam = Adoszam;
                    krt_Vallalkozasok.Updated.Adoszam = true;

                    krt_Vallalkozasok.KulfoldiAdoszamJelolo = (IsKulfoldiAdoszam ? "1" : "0");
                    krt_Vallalkozasok.Updated.KulfoldiAdoszamJelolo = true;

                    krt_Vallalkozasok.Base.Updated.Ver = true;

                    Result result_update = service_vallalkozas.Update(execParam_update, krt_Vallalkozasok);

                    if (result_update.IsError)
                    {
                        throw new ResultException(result_update);
                    }
                }
            }
            else if (res.Record is KRT_Szemelyek)
            {
                KRT_Szemelyek krt_Szemelyek = (KRT_Szemelyek)res.Record;

                if (String.IsNullOrEmpty(krt_Szemelyek.Id))
                {
                    //Rekord lekérése az adatbázisból sikertelen
                    if (EnableSzemelyVallakozasInsert)
                    {
                        KRT_SzemelyekService service_szemely = new KRT_SzemelyekService(this.dataContext);

                        ExecParam execParam_insert = ExecParam.Clone();

                        // insert
                        krt_Szemelyek.Partner_Id = ExecParam.Record_Id;
                        krt_Szemelyek.Updated.Partner_Id = true;

                        krt_Szemelyek.Adoszam = Adoszam;
                        krt_Szemelyek.Updated.Adoszam = true;

                        krt_Szemelyek.KulfoldiAdoszamJelolo = (IsKulfoldiAdoszam ? "1" : "0");
                        krt_Szemelyek.Updated.KulfoldiAdoszamJelolo = true;

                        Result result_insert = service_szemely.Insert(execParam_insert, krt_Szemelyek);

                        if (result_insert.IsError)
                        {
                            throw new ResultException(result_insert);
                        }
                    }
                    else
                    {
                        Logger.Error("Partnerhez tartozo szemely vagy vallakozas lekerese hiba", ExecParam, res);
                        throw new ResultException(53160); //"A partnerhez tartozó személy vagy vállalkozás nem található!"
                    }
                }
                else if (Adoszam != krt_Szemelyek.Adoszam || (IsKulfoldiAdoszam && krt_Szemelyek.KulfoldiAdoszamJelolo != "1") || (!IsKulfoldiAdoszam && krt_Szemelyek.KulfoldiAdoszamJelolo == "1"))
                {
                    // update
                    krt_Szemelyek.Updated.SetValueAll(false);
                    krt_Szemelyek.Base.Updated.SetValueAll(false);

                    KRT_SzemelyekService service_szemely = new KRT_SzemelyekService(this.dataContext);

                    ExecParam execParam_update = ExecParam.Clone();
                    execParam_update.Record_Id = krt_Szemelyek.Id;

                    krt_Szemelyek.Adoszam = Adoszam;
                    krt_Szemelyek.Updated.Adoszam = true;

                    krt_Szemelyek.KulfoldiAdoszamJelolo = (IsKulfoldiAdoszam ? "1" : "0");
                    krt_Szemelyek.Updated.KulfoldiAdoszamJelolo = true;

                    krt_Szemelyek.Base.Updated.Ver = true;

                    Result result_update = service_szemely.Update(execParam_update, krt_Szemelyek);

                    if (result_update.IsError)
                    {
                        throw new ResultException(result_update);
                    }
                }
            }
            else
            {
                result.Record = null;
                Logger.Debug("A partner nem szemely vagy vallalkozas, visszateres NULL ertekkel.");
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Partnerek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("UpdateAdoszam hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("UpdateAdoszam end");
        return result;
    }

    /// <summary>
    /// A kapott azonosítójú cím kötése a partnerhez, ha a kötés még nem létezett.
    /// </summary>
    /// <param name="ExecParam">A Record_Id mezõben kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.</param>
    /// <param name="Cim_Id">A partnerhez kapcsolandó cím azonosítója.</param>
    /// <returns></returns>
    [WebMethod(Description = "A kapott azonosítójú cím kötése a partnerhez, ha a kötés még nem létezett. A Record_Id mezõben kell átadni a partner azonosítóját, a többi mezõ a naplózáshoz szükséges adatokat tartalmazza.")]
    public Result BindCim(ExecParam ExecParam, string Cim_Id)
    {
        Logger.Debug("BindCim start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException(50660); //"Nincs megadva a partner azonosítója!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = new Result();

            KRT_PartnerCimekService service = new KRT_PartnerCimekService(this.dataContext);

            ExecParam execParam_getAll = ExecParam.Clone();

            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.Cim_Id.Value = Cim_Id;
            search.Cim_Id.Operator = Query.Operators.equals;

            search.Partner_id.Value = ExecParam.Record_Id;
            search.Partner_id.Operator = Query.Operators.equals;

            search.TopRow = 1;

            Result result_getAll = service.GetAll(execParam_getAll, search);

            if (result_getAll.IsError)
            {
                throw new ResultException(result_getAll);
            }

            if (result_getAll.Ds.Tables[0].Rows.Count == 0)
            {
                ExecParam execParam_insert = ExecParam.Clone();
                KRT_PartnerCimek krt_PartnerCimek = new KRT_PartnerCimek();

                krt_PartnerCimek.Updated.SetValueAll(false);
                krt_PartnerCimek.Base.Updated.SetValueAll(false);

                krt_PartnerCimek.Cim_Id = Cim_Id;
                krt_PartnerCimek.Updated.Cim_Id = true;

                krt_PartnerCimek.Partner_id = ExecParam.Record_Id;
                krt_PartnerCimek.Updated.Partner_id = true;

                Result result_insert = service.Insert(execParam_insert, krt_PartnerCimek);

                if (result_insert.IsError)
                {
                    throw new ResultException(result_insert);
                }

                result = result_insert;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_PartnerCimek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("BindCim hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("BindCim end");
        return result;
    }


    /// <summary>
    /// Ügyfélkapus viszontazonosításhoz a partner természetes azonosítóinak lekérése.
    /// 
    /// Elvileg a név és email cím kombónak egy partnert kell visszaadnia.
    /// 
    /// Mukod imigyen:
    /// 
    /// </summary>
    /// <param name="execParam">Execparam.</param>
    /// <param name="nev">Partner neve.</param>
    /// <param name="emailCim">Partner email címe.</param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetPartnerTermeszeresAzonositok(ExecParam execParam, String nev, String emailCim)
    {
        Result result = new Result();

        try
        {

            result = GetPartnerTermeszeresAzonositok(execParam, nev, emailCim);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a sp_KRT_PartnerekGetTermeszetesAzonositok tarolt eljaras soran. A visszaadott hibakod: {0}\nDetail: {1}\nMessage: {2}"
                    , result.ErrorCode
                    , result.ErrorDetail
                    , result.ErrorMessage));
            }

        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba GetPartnerTermeszeresAzonositok futasi hiba: {0}\nSource: {1}\nStackTrace: {2}"
                , ex.Message
                , ex.Source
                , ex.StackTrace));
        }

        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerekSearch))]
    public Result GetSzervezetekWithKapcsolattarto(ExecParam ExecParam, KRT_PartnerekSearch _KRT_PartnerekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            System.Text.StringBuilder sb = new System.Text.StringBuilder(_KRT_PartnerekSearch.Nev.Value);
            sb.Replace('*', '%');
            result = sp.GetSzervezetekWithKapcsolattarto(ExecParam, _KRT_PartnerekSearch.TopRow, sb.ToString());
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="partnereksearch"></param>
    /// <param name="type"></param>
    private void SetPartnerSearchFilterByType(ref KRT_PartnerekSearch partnereksearch, string type)
    {
        switch (type)
        {
            case "Szervezet":
                partnereksearch.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                partnereksearch.Tipus.Operator = Query.Operators.equals;
                break;
            case "Szemely":
                partnereksearch.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
                partnereksearch.Tipus.Operator = Query.Operators.equals;
                break;
            case Contentum.eUtility.Constants.FilterType.Partnerek.BelsoSzemely:
                partnereksearch.Belso.Value = Contentum.eUtility.Constants.Database.Yes;
                partnereksearch.Belso.Operator = Query.Operators.equals;
                goto case "Szemely";
            case Contentum.eUtility.Constants.FilterType.Partnerek.SzervezetKapcsolattartoja:
                partnereksearch.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                partnereksearch.Tipus.Operator = Query.Operators.equals;
                break;
            case Contentum.eUtility.Constants.FilterType.Partnerek.All:
            default:
                break;
        }
    }

    [WebMethod()]
    public Result GetKRID(ExecParam execParam, string partnerId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            ExecParam execParamGet = execParam.Clone();
            execParamGet.Record_Id = partnerId;

            Result resultGet = this.Get(execParamGet);

            if (resultGet.IsError)
            {
                //A partner lekérdezése sikertelen!
                throw new ResultException(50661);
            }

            KRT_Partnerek partner = resultGet.Record as KRT_Partnerek;

            if (partner.Tipus != KodTarak.Partner_Tipus.Szervezet)
            {
                //A partner nem szervezet!
                throw new ResultException(50662);
            }

            KRT_VallalkozasokService vallalkozasokService = new KRT_VallalkozasokService(this.dataContext);

            KRT_VallalkozasokSearch search = new KRT_VallalkozasokSearch();
            search.Partner_Id.Value = partnerId;
            search.Partner_Id.Operator = Query.Operators.equals;

            Result resultVallalkozas = vallalkozasokService.GetAll(execParam, search);

            if (resultVallalkozas.IsError || resultVallalkozas.Ds.Tables[0].Rows.Count == 0)
            {
                //Az adószám lekérdezése sikertelen!
                throw new ResultException(50663);
            }
            string adoszam = resultVallalkozas.Ds.Tables[0].Rows[0]["Adoszam"].ToString();

            if (String.IsNullOrEmpty(adoszam))
            {
                //Az adószám nincs megadva!
                throw new ResultException(50664);
            }

            // BUG_14830 magyar adószám esetén ellenőrizzük a hosszát
            if ((resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"] == null || String.IsNullOrEmpty(resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"].ToString()) || resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"].ToString() == "0")
                && adoszam.Length < 8)
            {
                //Az adószám túl rövid!
                throw new ResultException(50665);
            }

            if (resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"] == null || String.IsNullOrEmpty(resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"].ToString()) || resultVallalkozas.Ds.Tables[0].Rows[0]["KulfoldiAdoszamJelolo"].ToString() == "0")
                // BUG_14830 magyar adószám esetén levágjuk a végét
                result.Uid = adoszam.Substring(0, 8);
            else
                result.Uid = adoszam;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}