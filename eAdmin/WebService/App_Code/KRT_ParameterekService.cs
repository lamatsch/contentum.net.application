using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_ParameterekService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvettek (GetAll)

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_ParameterekSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_ParameterekSearch _KRT_ParameterekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_ParameterekSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_ParameterekSearch, new KRT_ParameterekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Parameterek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_ParameterekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_ParameterekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_ParameterekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Parameterek))]
    public Result Update(ExecParam ExecParam, KRT_Parameterek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            KRT_ParameterekService service = new KRT_ParameterekService(this.dataContext);
            Result res = service.Get(ExecParam);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            KRT_Parameterek parameter = (KRT_Parameterek)res.Record;

            if (parameter.Karbantarthato == "N" || parameter.Karbantarthato == "n" || parameter.Karbantarthato == 0.ToString())
            {
                //ContextUtil.SetComplete();
                res.ErrorCode = "[50404:rendszerparameterek_update:notKarbantarthato]";
                res.ErrorMessage = "[50404]";
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Parameterek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}