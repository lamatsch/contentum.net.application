using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Data;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_PartnerKapcsolatokService : System.Web.Services.WebService
{
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerKapcsolatok))]
    public Result Insert(ExecParam ExecParam, KRT_PartnerKapcsolatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            CheckPartnerKapcsolat(ExecParam, Record);

            Result result_insert = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result_insert.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_insert);
                //return result_insert;

                throw new ResultException(result_insert);
            }

            Result result_cst_insert = new Result();

            try
            {
                Logger.Debug("Partnerkapcsolatnak megfelel� csoporttagok lek�pz�se");
                KRT_CsoportTagok krt_csoporttagok = this.GetCsoportTagsag(ExecParam, Record);
                if (krt_csoporttagok != null)
                {
                    Logger.Debug("Partnerkapcsolatnak megfelel� csoporttagok insert");
                    KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
                    //A partnerkapcsolat id-j�val azonos id-j� csoportkapcsolat
                    krt_csoporttagok.Id = result_insert.Uid;
                    krt_csoporttagok.Updated.Id = true;



                    result_cst_insert = service.Insert(ExecParam, krt_csoporttagok);
                    if (!String.IsNullOrEmpty(result_cst_insert.ErrorCode))
                    {
                        //ContextUtil.SetAbort();
                        //log.WsEnd(ExecParam, result_cst_insert);
                        //return result_cst_insert;

                        throw new ResultException(result_cst_insert);
                    }
                }
            }
            catch (ResultException e)
            {
                //ContextUtil.SetAbort();
                //result_cst_insert = e.GetResult();
                //log.WsEnd(ExecParam, result_cst_insert);
                //return result_cst_insert;

                throw e;
            }

            result = result_insert;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_PartnerKapcsolatok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    private void CheckPartnerKapcsolat(ExecParam ExecParam, KRT_PartnerKapcsolatok Record)
    {
        Logger.Debug("CheckPartnerKapcsolat start");

        Arguments args = new Arguments();
        args.Add(new Argument("Partner azonos�t�", Record.Partner_id, ArgumentTypes.Guid));
        args.Add(new Argument("Kapcsolt partner azonos�t�", Record.Partner_id_kapcsolt, ArgumentTypes.Guid));
        args.ValidateArguments();

        KRT_PartnerekService svcPartner = new KRT_PartnerekService(this.dataContext);
        Logger.Debug("Partner lekerese: " + Record.Partner_id);
        ExecParam xpmPartner = ExecParam.Clone();
        xpmPartner.Record_Id = Record.Partner_id;
        Result resPartner = svcPartner.Get(xpmPartner);
        if (resPartner.IsError)
        {
            Logger.Error("Partner lekerese hiba", xpmPartner, resPartner);
            throw new ResultException(resPartner);
        }

        KRT_Partnerek partner = (KRT_Partnerek)resPartner.Record;

        Logger.Debug("Kapcsolt partner lekerese: " + Record.Partner_id_kapcsolt);
        ExecParam xpmPartnerKapcsolt = ExecParam.Clone();
        xpmPartnerKapcsolt.Record_Id = Record.Partner_id_kapcsolt;
        Result resPartnerKapcsolt = svcPartner.Get(xpmPartnerKapcsolt);
        if (resPartnerKapcsolt.IsError)
        {
            Logger.Error("Kapcsolt partner lekerese hiba", xpmPartnerKapcsolt, resPartnerKapcsolt);
            throw new ResultException(resPartnerKapcsolt);
        }

        KRT_Partnerek partnerKapcsolt = (KRT_Partnerek)resPartnerKapcsolt.Record;

        if (ExecParam.FunkcioKod != "PartnerKonszolidacioNew" && Record.Tipus != Contentum.eUtility.KodTarak.PartnerKapcsolatTipus.konszolidalt)
        {
            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely && partnerKapcsolt.Tipus == KodTarak.Partner_Tipus.Szemely)
            {
                Logger.Error("Ket szemely kozott nem hozhato letre kapcsolat!");
                throw new ResultException(53150);
            }
        }

        if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet || partner.Tipus == KodTarak.Partner_Tipus.HivatalVezetes)
        {
            switch (partnerKapcsolt.Tipus)
            {
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        if (Record.Tipus == KodTarak.PartnerKapcsolatTipus.felhasznaloSzervezete || Record.Tipus == KodTarak.PartnerKapcsolatTipus.vezetoje)
                        {
                            Logger.Debug("Ez a tipusu kapcsolat csak szemely es szervezet kozott hozhato letre!");
                            throw new ResultException(53151);
                        }
                        if (Record.Tipus == KodTarak.PartnerKapcsolatTipus.felhasznaloSzervezete || Record.Tipus == KodTarak.PartnerKapcsolatTipus.SzervezetAsszisztense)
                        {
                            Logger.Debug("Ez a tipusu kapcsolat csak szemely es szervezet kozott hozhato letre!");
                            throw new ResultException(53151);
                        }
                        //al�rendelt partner eset�n csere, �s kapcsolatt�pus invert�l�s
                        if (Record.Tipus == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere)
                        {
                            Logger.Debug("KodTarak.PartnerKapcsolatTipus.alarendeltPartnere -> kapcsolat csere");
                            string tempId = Record.Partner_id;
                            Record.Partner_id = Record.Partner_id_kapcsolt;
                            Record.Partner_id_kapcsolt = tempId;
                            Record.Tipus = KodTarak.PartnerKapcsolatTipus.felettesSzervezete;
                        }
                    }
                    break;
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        if (Record.Tipus == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere || Record.Tipus == KodTarak.PartnerKapcsolatTipus.felettesSzervezete)
                        {
                            Logger.Debug("Ez a tipusu kapcsolat csak ket szervezet kozott hozhato letre!");
                            throw new ResultException(53152);
                        }
                        //szemely  sosem kapcsolt, csere
                        Logger.Debug("Kapcsolat csere: szemely kapcsolt");
                        string tempId = Record.Partner_id;
                        Record.Partner_id = Record.Partner_id_kapcsolt;
                        Record.Partner_id_kapcsolt = tempId;
                    }
                    break;
            }
        }

        if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
        {
            switch (partnerKapcsolt.Tipus)
            {
                case KodTarak.Partner_Tipus.Szervezet:
                    if (Record.Tipus == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere || Record.Tipus == KodTarak.PartnerKapcsolatTipus.felettesSzervezete)
                    {
                        Logger.Debug("Ez a tipusu kapcsolat csak ket szervezet kozott hozhato letre!");
                        throw new ResultException(53152);
                    }
                    break;
            }
        }

        Logger.Debug("CheckPartnerKapcsolat end");
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerKapcsolatok))]
    public Result Update(ExecParam ExecParam, KRT_PartnerKapcsolatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            CheckPartnerKapcsolat(ExecParam, Record);

            Result result_update = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result_update.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_update);
                //return result_update;

                throw new ResultException(result_update);
            }

            result = result_update;

            Result result_cst_update = new Result();

            try
            {
                KRT_CsoportTagok krt_csoporttagok = this.GetCsoportTagsag(ExecParam, Record);
                if (krt_csoporttagok != null)
                {
                    KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
                    result_cst_update = service.Update(ExecParam, krt_csoporttagok);
                    if (!String.IsNullOrEmpty(result_cst_update.ErrorCode))
                    {
                        //ContextUtil.SetAbort();
                        //log.WsEnd(ExecParam, result_cst_update);
                        //return result_cst_update;

                        throw new ResultException(result_cst_update);
                    }
                }
            }
            catch (ResultException e)
            {
                //result_cst_update = e.GetResult();
                //log.WsEnd(ExecParam, result_cst_update);
                //return result_cst_update;

                throw e;
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_PartnerKapcsolatok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerKapcsolatok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Logger.InfoStart();
        Logger.Info("Partnerkapcsolat invaidate elkezd�d�tt");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("isConnectionOpenHere ut�n, sp.Invalidate(ExecParam) el�tt");
            Result result_invalidate = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result_invalidate);
                //return result_invalidate;
                Logger.Error(String.Format("Partnerkapcsolat invalidate hiba: {0}", result_invalidate.ErrorCode));
                throw new ResultException(result_invalidate);
            }

            result = result_invalidate;

            Logger.Debug("KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext) el�tt");
            KRT_CsoportTagokService service = new KRT_CsoportTagokService(this.dataContext);
            Logger.Debug("KRT_CsoportTagokService TryInvalidate start");
            service.TryInvalidate(ExecParam);
            Logger.Debug("KRT_CsoportTagokService TryInvalidate end");

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_PartnerKapcsolatok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("Partnerkapcsolat invalidate hiba", ExecParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Info("Partnerkapcsolat invalidate v�ge");
        Logger.InfoEnd();
        return result;
    }

    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A KRT_PartnerKapcsolatok t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerKapcsolatok))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                ExecParam xpm = ExecParams[i];
                Result result_invalidate = sp.Invalidate(xpm);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    Logger.Error("KRT_PartnerKapcsolatok Invalidate hiba: " + xpm.Record_Id, xpm, result_invalidate);
                    throw new ResultException(result_invalidate);
                }
                KRT_CsoportTagokService serviceCsoportTagok = new KRT_CsoportTagokService(this.dataContext);
                Result resultCsoportTagok_invalidate = serviceCsoportTagok.InvalidateWithoutRebuildCsoportTagokAll(xpm);

                if (resultCsoportTagok_invalidate.IsError)
                {
                    if (resultCsoportTagok_invalidate.ErrorMessage != "[50602]" && resultCsoportTagok_invalidate.ErrorMessage != "[50603]")
                    {
                        Logger.Error("KRT_CsoportTagokService Invalidate hiba: " + xpm.Record_Id, xpm, resultCsoportTagok_invalidate);
                        throw new ResultException(resultCsoportTagok_invalidate);
                    }
                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = "";

                for (int i = 0; i < ExecParams.Length; i++)
                {
                    Ids += "'" + ExecParams[i].Record_Id + "',";
                }

                Ids = Ids.TrimEnd(',');

                Result eventLogResult = eventLogService.InsertTomeges(ExecParams[0], Ids, "KRT_PartnerKapcsolatok", "Invalidate");
            }
            #endregion


            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A partnerhez tartoz� kapcsolatokat adja vissza f�ggetlen�l, hogy melyik oldalon �ll a partner. A partner ID-t az ExecParam.Record_Id")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerKapcsolatokSearch))]
    public Result GetAllByPartner(ExecParam ExecParam, KRT_PartnerKapcsolatokSearch _KRT_PartnerKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                throw new ResultException("PartnerKapcsolatok.GetAllByPartner:Nincs megadva ExecParam.Record_Id");
            }

            //Keres�si felt�tel be�ll�t�sa
            string partnerID = ExecParam.Record_Id;
            _KRT_PartnerKapcsolatokSearch.Partner_id.Value = partnerID;
            _KRT_PartnerKapcsolatokSearch.Partner_id.Operator = Query.Operators.equals;
            _KRT_PartnerKapcsolatokSearch.Partner_id.Group = "111";
            _KRT_PartnerKapcsolatokSearch.Partner_id.GroupOperator = Query.Operators.or;
            _KRT_PartnerKapcsolatokSearch.Partner_id_kapcsolt.Value = partnerID;
            _KRT_PartnerKapcsolatokSearch.Partner_id_kapcsolt.Operator = Query.Operators.equals;
            _KRT_PartnerKapcsolatokSearch.Partner_id_kapcsolt.Group = "111";
            _KRT_PartnerKapcsolatokSearch.Partner_id_kapcsolt.GroupOperator = Query.Operators.or;


            result = sp.GetAll(ExecParam, _KRT_PartnerKapcsolatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public KRT_Partnerek GetPartner(ExecParam execParam)
    {
        Result res = new Result();

        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            KRT_PartnerekService service = new KRT_PartnerekService(this.dataContext);
            res = service.Get(execParam);
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                KRT_Partnerek partner = (KRT_Partnerek)res.Record;
                if (partner != null)
                {
                    // RETURN
                    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                    return partner;
                }
                else
                {
                    res.ErrorCode = "[53100]Partner not found";
                    res.ErrorMessage = "[53100]Partner not found";
                }
            }

        }
        catch (Exception e)
        {
            res = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        throw new ResultException(res);

    }

    public KRT_Csoportok GetCsoport(ExecParam execParam)
    {
        Result res = new Result();

        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            KRT_Partnerek partner = this.GetPartner(execParam);

            if (partner.Belso == Contentum.eUtility.Constants.Database.Yes)
            {
                #region szem�ly

                if (partner.Tipus == Contentum.eUtility.KodTarak.Partner_Tipus.Szemely)
                {
                    Logger.Debug("Szem�ly t�pus� partnerhez csoport meghat�roz�sa");
                    KRT_FelhasznalokService service = new KRT_FelhasznalokService(this.dataContext);
                    KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
                    search.Partner_id.Value = partner.Id;
                    search.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
                    res = service.GetAll(execParam, search);
                    if (String.IsNullOrEmpty(res.ErrorCode))
                    {
                        if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
                        {
                            if (res.Ds.Tables[0].Rows.Count == 1)
                            {
                                execParam.Record_Id = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                            }
                            else
                            {
                                throw new ResultException("[53301]More than one felhasznalo");
                            }
                        }
                        else
                        {
                            //partnerhez nem tartozik felhasznal�, nem hiba, ekkor nem kell a csoportkapcsolatot l�trehozni

                            // RETURN
                            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                            return null;
                        }
                    }
                    else
                    {
                        throw new ResultException(res);
                    }
                }

                #endregion

                if (partner.Tipus == Contentum.eUtility.KodTarak.Partner_Tipus.Szervezet || partner.Tipus == KodTarak.Partner_Tipus.Bizottsag
                    || partner.Tipus == KodTarak.Partner_Tipus.Projekt || partner.Tipus == KodTarak.Partner_Tipus.HivatalVezetes || partner.Tipus == Contentum.eUtility.KodTarak.Partner_Tipus.Szemely)
                {
                    Logger.Debug("Partnerhez csoport lek�r�se");
                    KRT_CsoportokService service = new KRT_CsoportokService(this.dataContext);
                    res = service.Get(execParam);
                    if (String.IsNullOrEmpty(res.ErrorCode))
                    {
                        KRT_Csoportok csoport = (KRT_Csoportok)res.Record;
                        if (csoport != null)
                        {
                            // RETURN
                            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                            return csoport;
                        }
                        else
                        {
                            Logger.Debug(String.Format("A partnerhez ({0}) nem tal�lhat� csoport", execParam.Record_Id));
                            res.ErrorCode = "[53200]Csoport not found";
                            res.ErrorMessage = "[53200]Csoport not found";
                        }
                    }
                }
                else
                {
                    Logger.Debug(String.Format("Ehhez a t�pus� ({0}) partnerhez nincs csoport", partner.Tipus));
                    res.ErrorCode = "[53102]Partner not szemely or szervezet";
                    res.ErrorMessage = "[53102]Partner not szemely or szervezet";
                }

            }
            else
            {
                //A partner nem bels�, nem hiba, ekkor nem kell csoporkapcsolatot l�trehozni

                // RETURN
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                return null;
            }

        }
        catch (Exception e)
        {
            res = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        throw new ResultException(res);
    }

    public KRT_CsoportTagok GetCsoportTagsag(ExecParam execParam, KRT_PartnerKapcsolatok krt_ParnterKapcsolatok)
    {
        if (!(krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere
            || krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.felettesSzervezete
            || krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.felhasznaloSzervezete
            || krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.vezetoje
            || krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.SzervezetAsszisztense
            ))
        {
            Logger.Debug(String.Format("Ehhez t�pus� ({0}) a partnerkapcsolathoz nem kell csoporttags�g", krt_ParnterKapcsolatok.Tipus));
            return null;
        }

        Logger.Debug(String.Format("Els� partnerhez ({0}) csoport lek�r�se", krt_ParnterKapcsolatok.Partner_id_kapcsolt));
        ExecParam execParamCsoport = execParam.Clone();
        ExecParam execParamJogalany = execParam.Clone();
        //2-es t�pus eset�n nincs ford�t�s
        if (krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.felettesSzervezete)
        {
            Logger.Debug(String.Format("KodTarak.PartnerKapcsolatTipus.felettesSzervezete({0}) csoport_id = partner_id ({1})�s csoport_id_jogalany = partner_id_kapcsolt ({2})", KodTarak.PartnerKapcsolatTipus.felettesSzervezete, krt_ParnterKapcsolatok.Partner_id, krt_ParnterKapcsolatok.Partner_id_kapcsolt));
            execParamCsoport.Record_Id = krt_ParnterKapcsolatok.Partner_id;
            execParamJogalany.Record_Id = krt_ParnterKapcsolatok.Partner_id_kapcsolt;
        }
        else //eredeti (megford�t�s)
        {
            Logger.Debug(String.Format("Nem KodTarak.PartnerKapcsolatTipus.felettesSzervezete({0}) csoport_id = partner_id_kapcsolt ({2})�s csoport_id_jogalany = partner_id ({1})", KodTarak.PartnerKapcsolatTipus.felettesSzervezete, krt_ParnterKapcsolatok.Partner_id, krt_ParnterKapcsolatok.Partner_id_kapcsolt));
            execParamCsoport.Record_Id = krt_ParnterKapcsolatok.Partner_id_kapcsolt;
            execParamJogalany.Record_Id = krt_ParnterKapcsolatok.Partner_id;
        }
        KRT_Csoportok csoport = this.GetCsoport(execParamCsoport);
        if (csoport == null) return null;

        Logger.Debug(String.Format("M�sodik partnerhez ({0}) csoport lek�r�se", krt_ParnterKapcsolatok.Partner_id));
        KRT_Csoportok jogalany = this.GetCsoport(execParamJogalany);
        if (jogalany == null) return null;

        KRT_CsoportTagok krt_Csoportagok = new KRT_CsoportTagok();
        krt_Csoportagok.Updated.SetValueAll(false);
        krt_Csoportagok.Csoport_Id = csoport.Id;
        krt_Csoportagok.Updated.Csoport_Id = true;
        krt_Csoportagok.Csoport_Id_Jogalany = jogalany.Id;
        krt_Csoportagok.Updated.Csoport_Id_Jogalany = true;
        krt_Csoportagok.System = Contentum.eUtility.Constants.Database.Yes;
        krt_Csoportagok.Updated.System = true;
        krt_Csoportagok.ErvKezd = krt_ParnterKapcsolatok.ErvKezd;
        krt_Csoportagok.Updated.ErvKezd = true;
        krt_Csoportagok.ErvVege = krt_ParnterKapcsolatok.ErvVege;
        krt_Csoportagok.Updated.ErvVege = true;
        krt_Csoportagok.Base = krt_ParnterKapcsolatok.Base;
        krt_Csoportagok.Base.Updated = krt_ParnterKapcsolatok.Base.Updated;
        //kapcsolat t�pus�nak lek�pz�se
        KRT_PartnerekService partnerService = new KRT_PartnerekService(this.dataContext);
        ExecParam partnerExecParam = execParam.Clone();
        partnerExecParam.Record_Id = krt_ParnterKapcsolatok.Partner_id;

        Result partnerResult = partnerService.Get(partnerExecParam);
        if (!string.IsNullOrEmpty(partnerResult.ErrorCode))
            throw new ResultException(partnerResult);

        if ((partnerResult.Record as KRT_Partnerek).Tipus == KodTarak.Partner_Tipus.Szemely)
        {
            if (krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.vezetoje)
            {
                krt_Csoportagok.Tipus = KodTarak.CsoprttagsagTipus.vezeto;
            }
            else if (krt_ParnterKapcsolatok.Tipus == KodTarak.PartnerKapcsolatTipus.SzervezetAsszisztense)
            {
                krt_Csoportagok.Tipus = KodTarak.CsoprttagsagTipus.SzervezetAsszisztense;
            }
            else
            {
                krt_Csoportagok.Tipus = KodTarak.CsoprttagsagTipus.dolgozo;
            }
        }
        else krt_Csoportagok.Tipus = KodTarak.CsoprttagsagTipus.alszervezet;
        krt_Csoportagok.Updated.Tipus = true;



        return krt_Csoportagok;
    }
}