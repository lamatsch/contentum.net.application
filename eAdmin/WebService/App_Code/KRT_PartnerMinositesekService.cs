using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_PartnerMinositesekService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Partnerek))]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_PartnerMinositesekSearch))]
    public Result GetAllByPartner(ExecParam ExecParam, KRT_Partnerek _KRT_Partnerek, KRT_PartnerMinositesekSearch _KRT_PartnerMinositesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByPartner(ExecParam, _KRT_Partnerek, _KRT_PartnerMinositesekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}