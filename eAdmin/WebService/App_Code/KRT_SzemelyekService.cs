using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_SzemelyekService : System.Web.Services.WebService
{
    #region publikus seg�df�ggv�nyek

    //nev sz�tszed�se: vezet�k, kereszt
    public static void SetNevFromString(KRT_Szemelyek szemely, string nev)
    {
        if (szemely == null)
        {
            return;
        }
        string[] nevek = nev.Split(' ');
        string csaladiNev = String.Empty;
        string utoNev = String.Empty;
        string titulus = String.Empty;
        if (nevek.Length == 2)
        {
            csaladiNev = nevek[0];
            utoNev = nevek[1];
        }
        if (nevek.Length > 2)
        {
            for (int i = 0; i < nevek.Length; i++)
            {
                if (nevek[i].Contains("."))
                {
                    titulus = nevek[i];
                }
                else
                {
                    if (String.IsNullOrEmpty(csaladiNev))
                    {
                        csaladiNev = nevek[i];
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(utoNev))
                        {
                            utoNev += " ";
                        }
                        utoNev += nevek[i];
                    }
                }
            }
        }

        if (nevek.Length < 2)
        {
            csaladiNev = nev;
        }

        if (!String.IsNullOrEmpty(csaladiNev))
        {
            szemely.UjCsaladiNev = csaladiNev;
            szemely.Updated.UjCsaladiNev = true;
        }

        if (!String.IsNullOrEmpty(utoNev))
        {
            szemely.UjUtonev = utoNev;
            szemely.Updated.UjUtonev = true;
        }

        if (!String.IsNullOrEmpty(titulus))
        {
            szemely.UjTitulis = titulus;
            szemely.Updated.UjTitulis = true;
        }
    }

    //nev �sszef�z�se
    public static string GetNevFromStrings(KRT_Szemelyek szemely)
    {
        if (szemely == null)
        {
            return string.Empty;
        }

        szemely.UjCsaladiNev = szemely.UjCsaladiNev.Trim();
        szemely.UjUtonev = szemely.UjUtonev.Trim();
        szemely.UjTovabbiUtonev = szemely.UjTovabbiUtonev.Trim();
        szemely.UjTitulis = szemely.UjTitulis.Trim();

        System.Text.StringBuilder nev = new System.Text.StringBuilder();

        if (!string.IsNullOrEmpty(szemely.UjCsaladiNev))
        {
            nev.Append(szemely.UjCsaladiNev);
        }

        if (!string.IsNullOrEmpty(nev.ToString()))
        {
            if (!string.IsNullOrEmpty(szemely.UjUtonev))
            {
                nev.Append(" ");
                nev.Append(szemely.UjUtonev);
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(szemely.UjUtonev))
            {
                nev.Append(" ");
                nev.Append(szemely.UjUtonev);
            }
        }

        if (!string.IsNullOrEmpty(szemely.UjTovabbiUtonev))
        {
            nev.Append(" ");
            nev.Append(szemely.UjTovabbiUtonev);
        }

        if (!string.IsNullOrEmpty(szemely.UjTitulis))
        {
            nev.Append(" ");
            nev.Append(szemely.UjTitulis);
        }

        return nev.ToString();
    }

    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Szemelyek))]
    public Result GetByPartner(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetByPartner(ExecParam);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Szemelyek))]
    public Result UpdateByPartner(ExecParam ExecParam, KRT_Szemelyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result res = new Result();
            res = GetByPartner(ExecParam);

            //l�tezik partnerhez bejegyz�s a szem�lyek t�bl�ban
            if (res.Record != null && ((KRT_Szemelyek)res.Record).Id != String.Empty)
            {
                ExecParam.Record_Id = ((KRT_Szemelyek)res.Record).Id;
                //verzi� figyelmen k�v�l hagy�sa
                Record.Base.Ver = ((KRT_Szemelyek)res.Record).Base.Ver;
                res = UpdateWithFKResolution(ExecParam, Record);
            }
            //ha nem l�tezik insert
            else
            {
                Record.Partner_Id = ExecParam.Record_Id;
                res = InsertWithFKResolution(ExecParam, Record);
            }

            result = res;

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Szemelyek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Szemelyek))]
    public Result InsertWithFKResolution(ExecParam ExecParam, KRT_Szemelyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string orszagID = String.Empty;
            string telepulesID = String.Empty;

            if (!String.IsNullOrEmpty(Record.SzuletesiOrszag))
            {
                KRT_OrszagokService serviceOrszagok = new KRT_OrszagokService(this.dataContext);
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Nev.Value = Record.SzuletesiOrszag;
                searchOrszagok.Nev.Operator = Query.Operators.equals;
                result = serviceOrszagok.GetAll(ExecParam, searchOrszagok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(orszagID))
                {
                    Record.SzuletesiOrszagId = orszagID;
                    Record.Updated.SzuletesiOrszagId = Record.Updated.SzuletesiOrszag;
                }
            }

            if (!String.IsNullOrEmpty(Record.SzuletesiHely))
            {
                KRT_TelepulesekService serviceTelepulesek = new KRT_TelepulesekService(this.dataContext);
                KRT_TelepulesekSearch searchTelepulesek = new KRT_TelepulesekSearch();
                searchTelepulesek.Nev.Value = Record.SzuletesiHely;
                searchTelepulesek.Nev.Operator = Query.Operators.equals;
                result = serviceTelepulesek.GetAll(ExecParam, searchTelepulesek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        telepulesID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(telepulesID))
                {
                    Record.SzuletesiHely_id = telepulesID;
                    Record.Updated.SzuletesiHely_id = Record.Updated.SzuletesiHely;
                }
            }

            result = Insert(ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Szemelyek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Szemelyek))]
    public Result UpdateWithFKResolution(ExecParam ExecParam, KRT_Szemelyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string orszagID = String.Empty;
            string telepulesID = String.Empty;

            if (!String.IsNullOrEmpty(Record.SzuletesiOrszag))
            {
                KRT_OrszagokService serviceOrszagok = new KRT_OrszagokService(this.dataContext);
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Nev.Value = Record.SzuletesiOrszag;
                searchOrszagok.Nev.Operator = Query.Operators.equals;
                result = serviceOrszagok.GetAll(ExecParam, searchOrszagok);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }

                Record.SzuletesiOrszagId = orszagID;
                Record.Updated.SzuletesiOrszagId = Record.Updated.SzuletesiOrszag;
            }

            if (!String.IsNullOrEmpty(Record.SzuletesiHely))
            {
                KRT_TelepulesekService serviceTelepulesek = new KRT_TelepulesekService(this.dataContext);
                KRT_TelepulesekSearch searchTelepulesek = new KRT_TelepulesekSearch();
                searchTelepulesek.Nev.Value = Record.SzuletesiHely;
                searchTelepulesek.Nev.Operator = Query.Operators.equals;
                result = serviceTelepulesek.GetAll(ExecParam, searchTelepulesek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        telepulesID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }

                Record.SzuletesiHely_id = telepulesID;
                Record.Updated.SzuletesiHely_id = Record.Updated.SzuletesiHely;
            }

            result = Update(ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Szemelyek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result Revalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Revalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Szemelyek", "Revalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}