using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_SzerepkorokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvettek (GetAll)

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_SzerepkorokSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_SzerepkorokSearch _KRT_SzerepkorokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_SzerepkorokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_SzerepkorokSearch, new KRT_SzerepkorokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_Szerepkorok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_SzerepkorokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_SzerepkorokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_SzerepkorokSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result GetAllByFelhasznalo(ExecParam ExecParam, KRT_Felhasznalok _KRT_Felhasznalok
        , KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, _KRT_Felhasznalo_SzerepkorSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Felhasznalok))]
    public Result GetAllByFunkcio(ExecParam ExecParam, KRT_Funkciok _KRT_Funkciok
        , KRT_Szerepkor_FunkcioSearch _KRT_Szerepkor_FunkcioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllByFunkcio(ExecParam, _KRT_Funkciok, _KRT_Szerepkor_FunkcioSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private static Dictionary<string,string> defaultSzerepkorCache = new Dictionary<string,string>();

    public string GetDefaultSzerepkorID(ExecParam execParam)
    {
        Logger.Debug("GetDefaultSzerepkor start");
        ResultError.CheckOrg(execParam.Org_Id);

        if(defaultSzerepkorCache.ContainsKey(execParam.Org_Id))
        {
            string defaultSzerpekorId = defaultSzerepkorCache[execParam.Org_Id];
            Logger.Debug("DefaultSzerepkorID:" + defaultSzerpekorId ?? "NULL");
            return defaultSzerpekorId;
        }

        string defaultSzerepkorNev = Rendszerparameterek.Get(execParam, "DEFAULT_SZEREPKOR");
        if (!String.IsNullOrEmpty(defaultSzerepkorNev))
        {
            Logger.Debug("Default szerepkor: " + defaultSzerepkorNev);
            KRT_SzerepkorokSearch schSzerepkor = new KRT_SzerepkorokSearch();
            schSzerepkor.Nev.Value = defaultSzerepkorNev;
            schSzerepkor.Nev.Operator = Query.Operators.equals;
            Result res = this.GetAll(execParam, schSzerepkor);
            if (res.IsError)
            {
                Logger.Error("Szerepkor lekerese hiba", execParam, res);
                throw new ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error("Default szerepkor nem talalhato", execParam);
                throw new ResultException(53601);
            }
            string defaultSzerepkorID = res.Ds.Tables[0].Rows[0]["Id"].ToString();
            Logger.Debug("DefaultSzerepkorID:" + defaultSzerepkorID);
            defaultSzerepkorCache[execParam.Org_Id] = defaultSzerepkorID;
            return defaultSzerepkorID;
           
        }
        else
        {
            Logger.Debug("Nincs megadva default szerepkor");
            return null;
        }
    }
}