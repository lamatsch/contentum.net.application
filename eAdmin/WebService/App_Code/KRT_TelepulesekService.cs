using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.Web.Script.Services;
using Contentum.eQuery.BusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery;
using System.Data;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public partial class KRT_TelepulesekService : System.Web.Services.WebService
{
    #region Insert, Invalidate, Delete átvéve a Generated -ből, Update

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Telepulesek))]
    public Result Insert(ExecParam ExecParam, KRT_Telepulesek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            TelepulesekListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Telepulesek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Telepulesek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            TelepulesekListReset();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Telepulesek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Telepulesek))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.Delete(ExecParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            TelepulesekListReset();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Telepulesek))]
    public Result Update(ExecParam ExecParam, KRT_Telepulesek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string recordID = ExecParam.Record_Id;
            string modifiedTelepulesNev = Record.Nev;
            string modifiedIranyitoszam = Record.IRSZ;
            Result res = null;

            KRT_CimekService service = new KRT_CimekService(this.dataContext);
            KRT_CimekSearch search = new KRT_CimekSearch();

            search.Telepules_Id.Value = recordID;
            search.Telepules_Id.Operator = Query.Operators.equals;

            ExecParam exec = ExecParam.Clone();
            exec.Record_Id = String.Empty;

            res = service.GetAll(exec, search);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                DataSet ds = res.Ds;
                string telepulesNev = null;
                string iranyitoszam = null;
                KRT_Cimek cimRecord = new KRT_Cimek();
                Result resCim = null;

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    telepulesNev = row["TelepulesNev"].ToString();
                    iranyitoszam = row["IRSZ"].ToString();
                    if ((!String.IsNullOrEmpty(telepulesNev) && telepulesNev != modifiedTelepulesNev) || (!String.IsNullOrEmpty(iranyitoszam) && iranyitoszam != modifiedIranyitoszam))
                    {
                        cimRecord.Updated.SetValueAll(false);
                        cimRecord.Base.Updated.SetValueAll(false);
                        if (!String.IsNullOrEmpty(telepulesNev) && telepulesNev != modifiedTelepulesNev)
                        {
                            cimRecord.TelepulesNev = modifiedTelepulesNev;
                            cimRecord.Updated.TelepulesNev = true;
                        }
                        if (!String.IsNullOrEmpty(iranyitoszam) && iranyitoszam != modifiedIranyitoszam)
                        {
                            cimRecord.IRSZ = modifiedIranyitoszam;
                            cimRecord.Updated.IRSZ = true;
                        }
                        cimRecord.Base.Ver = row["Ver"].ToString();
                        cimRecord.Base.Updated.Ver = true;

                        exec.Record_Id = row["Id"].ToString();

                        resCim = service.Update(exec, cimRecord);

                        if (!String.IsNullOrEmpty(resCim.ErrorCode))
                        {
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, resCim);
                            //return resCim;

                            throw new ResultException(resCim);
                        }
                    }
                }
            }
            else
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, result);
                //return result;

                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Telepulesek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_TelepulesekSearch))]
    public Result GetAllWithOrszag(ExecParam ExecParam, KRT_TelepulesekSearch _KRT_TelepulesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithOrszag(ExecParam, _KRT_TelepulesekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_TelepulesekSearch))]
    public Result GetAllDistinctNames(ExecParam ExecParam, KRT_TelepulesekSearch _KRT_TelepulesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllDistinctNames(ExecParam, _KRT_TelepulesekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_TelepulesekSearch))]
    public Result GetAllDistinctIrsz(ExecParam ExecParam, KRT_TelepulesekSearch _KRT_TelepulesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllDistinctIrsz(ExecParam, _KRT_TelepulesekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    private static string[] TelepulesekList = null;    
    private static string Orszag = "";
    private static string Iranyitoszam = String.Empty;

    private static void TelepulesekListReset()
    {
        TelepulesekList = null;
        Orszag = "";
        Iranyitoszam = String.Empty;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTelepulesekList(string prefixText, int count, string contextKey)
    {
        string[] contexts = contextKey.Split(';');
        String userId = contexts[0];

        string contextOrszag = String.Empty;
        string contextIrsz = string.Empty;
        if (contexts.Length > 1)
        {
            contextOrszag = contexts[1];
            if (contexts.Length > 2)
            {
                contextIrsz = contexts[2];
            }
        }

        if (TelepulesekList == null || Orszag != contextOrszag || Iranyitoszam != contextIrsz)
        {
            Orszag = contextOrszag;
            Iranyitoszam = contextIrsz;
            KRT_TelepulesekService telepulesekservice = new KRT_TelepulesekService();
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            KRT_TelepulesekSearch telepuleseksearch = new KRT_TelepulesekSearch();
            telepuleseksearch.OrderBy = "Nev " + Utility.customCollation + " ASC";
            if (!String.IsNullOrEmpty(Orszag))
            {
                KRT_OrszagokService orszagokservice = new KRT_OrszagokService();
                KRT_OrszagokSearch orszagoksearch = new KRT_OrszagokSearch();
                orszagoksearch.Nev.Value = Orszag;
                orszagoksearch.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                Result orszagok_res = orszagokservice.GetAll(execparam, orszagoksearch);

                if (String.IsNullOrEmpty(orszagok_res.ErrorCode))
                {
                    if (orszagok_res.Ds.Tables[0].Rows.Count > 0)
                    {
                        telepuleseksearch.Orszag_Id.Value = orszagok_res.Ds.Tables[0].Rows[0]["Id"].ToString();
                        telepuleseksearch.Orszag_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                    }
                    else
                    {
                        TelepulesekList = new string[0];
                        return TelepulesekList;
                    }
                }
                else
                {
                    return null;
                }
            }

            if (!String.IsNullOrEmpty(Iranyitoszam))
            {
                telepuleseksearch.IRSZ.Value = Iranyitoszam;
                telepuleseksearch.IRSZ.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result _res = telepulesekservice.GetAllDistinctNames(execparam, telepuleseksearch);
            if (String.IsNullOrEmpty(_res.ErrorCode))
            {
                TelepulesekList = Utility.DataSourceColumnToStringArray(_res, "Nev");
            }
            else
            {
                return null;
            }
        }

        return Utility.SearchInStringArrayByPrefix(TelepulesekList, prefixText, count, true);

    }

    private static string[] IranyitoszamokList = null;
    private static string TelepulesNev = "";

    private static void IranyitoszamokListReset()
    {
        IranyitoszamokList = null;
        TelepulesNev = "";
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetIranyitoszamokList(string prefixText, int count, string contextKey)
    {
        String userId = contextKey.Substring(0, contextKey.IndexOf(";"));
        contextKey = contextKey.Substring(contextKey.IndexOf(";") + 1);

        if (IranyitoszamokList == null || TelepulesNev != contextKey)
        {
            TelepulesNev = contextKey;
            KRT_TelepulesekService telepulesekservice = new KRT_TelepulesekService();
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            KRT_TelepulesekSearch telepuleseksearch = new KRT_TelepulesekSearch();
            telepuleseksearch.OrderBy = "IRSZ ASC";
            if (!String.IsNullOrEmpty(TelepulesNev))
            {
                telepuleseksearch.Nev.Value = TelepulesNev;
                telepuleseksearch.Nev.Operator = Query.Operators.equals;
            }
            Result _res = telepulesekservice.GetAllDistinctIrsz(execparam, telepuleseksearch);
            if (String.IsNullOrEmpty(_res.ErrorCode))
            {
                IranyitoszamokList = Utility.DataSourceColumnToStringArray(_res, "IRSZ");
            }
            else
            {
                return null;
            }
        }

        return Utility.SearchInStringArrayByPrefix(IranyitoszamokList, prefixText, count);

    }
}