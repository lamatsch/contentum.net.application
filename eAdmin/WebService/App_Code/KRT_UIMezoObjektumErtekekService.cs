using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
//using System.EnterpriseServices;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_UIMezoObjektumErtekekService : System.Web.Services.WebService
{
    [WebMethod()]
    public Result Test()
    {
        //    ExecParam execParam = new ExecParam();
        //    KRT_UIMezoObjektumErtekek obj = new KRT_UIMezoObjektumErtekek();
        //    obj.Nev = "TestFromWS";
        //    obj.TemplateTipusNev = "KRT_PartnerekSearch";

        //    return sp.Insert("Insert",execParam,obj);
        return null;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_UIMezoObjektumErtekek))]
    public Result MegosztasWithSzemelyOrSzervezet(ExecParam execParam, KRT_UIMezoObjektumErtekek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet(execParam, Record, DateTime.Now);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}