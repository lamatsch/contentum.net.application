using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;

/// <summary>
/// Summary description for LockManagerService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class LockManagerService : System.Web.Services.WebService
{
    private LockManagerServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public LockManagerService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new LockManagerServiceStoredProcedure(this.Application);        
        sp = new LockManagerServiceStoredProcedure(this.dataContext);
    }

    public LockManagerService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new LockManagerServiceStoredProcedure(this.dataContext);
    }

    [WebMethod()]
    public Result LockRecord(ExecParam ExecParam, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.LockRecord(ExecParam, TableName);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, TableName, "Lock").Record;

            eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result UnlockRecord(ExecParam ExecParam, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.UnlockRecord(ExecParam, TableName);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result ForceLockRecord(ExecParam ExecParam, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.ForceLockRecord(ExecParam, TableName);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

            // E-mail k�ld�s tranzakci�n k�v�l menjen, ne k�sleltesse a Commitot!
            // TODO: aszinkron kellene megh�vni
            if (String.IsNullOrEmpty(result.ErrorCode) && !String.IsNullOrEmpty(result.Uid) && result.Uid != ExecParam.Felhasznalo_Id)
            {
                // fel lett t�rve m�svalakinek a z�rol�sa --> e-mail k�ld�s
                /// ...

                EmailService emailservice = new EmailService();
                emailservice.BreakLockNotifyEmail(ExecParam, result.Uid, TableName, "", ExecParam.Record_Id);
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result ForceUnlockRecord(ExecParam ExecParam, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.ForceUnlockRecord(ExecParam, TableName);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

            // E-mail k�ld�s tranzakci�n k�v�l menjen, ne k�sleltesse a Commitot!
            // TODO: aszinkron kellene megh�vni
            if (String.IsNullOrEmpty(result.ErrorCode) && !String.IsNullOrEmpty(result.Uid) && result.Uid != ExecParam.Felhasznalo_Id)
            {
                // fel lett t�rve m�svalakinek a z�rol�sa --> e-mail k�ld�s
                /// ...
                EmailService emailservice = new EmailService();
                emailservice.BreakLockNotifyEmail(ExecParam, result.Uid, TableName, "", ExecParam.Record_Id);
            }                        
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result MultiLockRecord(ExecParam[] ExecParams, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_lock = sp.LockRecord(ExecParams[i], TableName);
                if (!String.IsNullOrEmpty(result_lock.ErrorCode))
                {
                    //RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, result_lock);
                    //return result_lock;

                    throw new ResultException(result_lock);
                }

                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParams[i], dataContext.Tranz_Id, ExecParams[i].Record_Id, TableName, "Lock").Record;

                Result eventLogResult = eventLogService.Insert(ExecParams[i], eventLogRecord);
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams);
        return result;
    }


    [WebMethod()]
    public Result MultiUnlockRecord(ExecParam[] ExecParams, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_unlock = sp.UnlockRecord(ExecParams[i], TableName);
                if (!String.IsNullOrEmpty(result_unlock.ErrorCode))
                {
                    ////RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, result_unlock);
                    //return result_unlock;

                    throw new ResultException(result_unlock);
                }

                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParams[i], dataContext.Tranz_Id, ExecParams[i].Record_Id, TableName, "UnLock").Record;

                Result eventLogResult = eventLogService.Insert(ExecParams[i], eventLogRecord);
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParams);
        return result;
    }

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    //public Result Test_ForceMultiLockRecord()
    //{
    //    ExecParam[] ExecParams = new ExecParam[2];
    //    ExecParams[0] = new ExecParam();
    //    ExecParams[1] = new ExecParam();
    //    ExecParams[0].Record_Id = "711C464C-AEAF-43C4-BCFC-13381F75A6DB";
    //    ExecParams[0].Felhasznalo_Id = "F088E89E-7917-4D0B-B94F-F4DAC84F0F95";
    //    ExecParams[1].Record_Id = "A0266555-47D5-49A8-92DA-906D984F7261";
    //    ExecParams[1].Felhasznalo_Id = "F088E89E-7917-4D0B-B94F-F4DAC84F0F95";

    //    return ForceMultiLockRecord(ExecParams, "KRT_Szerepkorok");
    //}

    private static void ForceMultiSendEmail(ExecParam ExecParam, Dictionary<String, List<String>> breakedLockingUsersList, String TableName)
    {
        //ExecParam ExecParam = ExecParams[0];
        string RecordsId = "";

        EmailService emailservice = new EmailService();
        foreach (KeyValuePair<String, List<String>> felhasznaloIdandRecordId in breakedLockingUsersList)
        {
            // e-mail k�ld�s
            // ...
            List<String> List = felhasznaloIdandRecordId.Value;
            foreach (String Record in List)
            {
                RecordsId += Record + ";";
            }
            RecordsId = RecordsId.Substring(0, RecordsId.Length - 1);
            emailservice.BreakLockNotifyEmail(ExecParam, felhasznaloIdandRecordId.Key, TableName, "", RecordsId);

        }
    }

    [WebMethod()]
    public Result ForceMultiLockRecord(ExecParam[] ExecParams, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            /// a felt�rt lockok usereinek list�ja
            Dictionary<String, List<String>> breakedLockingUsersList = new Dictionary<string, List<string>>();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_forceLock = sp.ForceLockRecord(ExecParams[i], TableName);
                if (!String.IsNullOrEmpty(result_forceLock.ErrorCode))
                {
                    ////RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, result_forceLock);
                    //return result_forceLock;

                    throw new ResultException(result_forceLock);
                }
                else if (!String.IsNullOrEmpty(result_forceLock.Uid) && result_forceLock.Uid != ExecParams[i].Felhasznalo_Id)
                {
                    if (breakedLockingUsersList.ContainsKey(result_forceLock.Uid))
                    {
                        //breakedLockingUsersList.Add(result.Uid);
                        breakedLockingUsersList[result_forceLock.Uid].Add(ExecParams[i].Record_Id);
                    }
                    else
                    {
                        List<string> List = new List<string>();
                        List.Add(ExecParams[i].Record_Id);
                        breakedLockingUsersList.Add(result_forceLock.Uid, List);
                    }
                }

                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParams[i], dataContext.Tranz_Id, ExecParams[i].Record_Id, TableName, "Lock").Record;

                Result eventLogResult = eventLogService.Insert(ExecParams[i], eventLogRecord);
                #endregion

            }

            // e-mailek k�ld�se a felt�rt lockok usereinek:        

            ForceMultiSendEmail(ExecParams[0], breakedLockingUsersList, TableName);


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams);
        return result;
    }


    [WebMethod()]
    public Result ForceMultiUnlockRecord(ExecParam[] ExecParams, string TableName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            /// a felt�rt lockok usereinek list�ja
            Dictionary<String, List<String>> breakedLockingUsersList = new Dictionary<string, List<string>>();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_forceUnlock = sp.ForceUnlockRecord(ExecParams[i], TableName);
                if (!String.IsNullOrEmpty(result_forceUnlock.ErrorCode))
                {
                    ////RollBack:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParams, result_forceUnlock);
                    //return result_forceUnlock;

                    throw new ResultException(result_forceUnlock);
                }
                else if (!String.IsNullOrEmpty(result_forceUnlock.Uid) && result_forceUnlock.Uid != ExecParams[i].Felhasznalo_Id)
                {
                    if (breakedLockingUsersList.ContainsKey(result_forceUnlock.Uid))
                    {
                        //breakedLockingUsersList.Add(result.Uid);
                        breakedLockingUsersList[result_forceUnlock.Uid].Add(ExecParams[i].Record_Id);
                    }
                    else
                    {
                        List<string> List = new List<string>();
                        List.Add(ExecParams[i].Record_Id);
                        breakedLockingUsersList.Add(result_forceUnlock.Uid, List);
                    }
                }

                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParams[i], dataContext.Tranz_Id, ExecParams[i].Record_Id, TableName, "UnLock").Record;

                Result eventLogResult = eventLogService.Insert(ExecParams[i], eventLogRecord);
                #endregion

            }

            ForceMultiSendEmail(ExecParams[0], breakedLockingUsersList, TableName);


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams);
        return result;
    }



}

