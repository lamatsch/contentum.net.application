using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;


/// <summary>
/// Summary description for LogService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class LogService : System.Web.Services.WebService 
{

    public LogService()
    {
    }

    [System.Xml.Serialization.XmlInclude(typeof(LogData))]
    [WebMethod()]
    public Result WriteOutLog(ExecParam execParam, LogData log)
    {
        Result res = new Result();

        if (log.LogIn != null)
        {
            KRT_Log_LoginService service = new KRT_Log_LoginService();
            res = service.Insert(execParam, log.LogIn);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                return res;
            }
        }

        if (log.Page != null)
        {
            KRT_Log_PageService service = new KRT_Log_PageService();
            res = service.Insert(execParam, log.Page);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                return res;
            }
        }

        if (log.WebServices.Count > 0)
        {
            KRT_Log_WebServiceService service = new KRT_Log_WebServiceService();

            for (int i = 0; i < log.WebServices.Count; i++)
            {
                res = service.Insert(execParam, log.WebServices[i]);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    //ContextUtil.SetAbort();
                    return res;
                }
            }
        }

        if (log.StoredProcedure != null)
        {
            KRT_Log_StoredProcedureService service = new KRT_Log_StoredProcedureService();
            res = service.Insert(execParam, log.StoredProcedure);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                return res;
            }
        }

        //ContextUtil.SetComplete();
        return res;
    }
    
}

