﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Web.Services;

/// <summary>
/// Summary description for MaintenancePlanService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MaintenancePlanService : System.Web.Services.WebService {

    private DataContext dataContext;

    public MaintenancePlanService () {
        dataContext = new DataContext(this.Application);
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "MainTenancePlan lelérdezése. ")]
    public Result GetAll(ExecParam ExecParam)
    {
        //dataContext = new DataContext(this.Application);

        //TESZT
        //ConnectionStringSettings conn = new ConnectionStringSettings();
        //conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];

        //String connStr = conn.ConnectionString;

        bool isConnectionOpenHere = false;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "GetAll");

        Result _ret = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            String sqlStr = "SELECT mp.id, mp.name AS[MTXPlanName], msp.subplan_name AS[SubPlanName], mpl.start_time AS[JobStart], mpl.end_time AS[JobEnd], mpl.succeeded AS[JobSucceeded] " +
                                " FROM msdb.dbo.sysmaintplan_plans mp " +
                                " INNER JOIN msdb.dbo.sysmaintplan_subplans msp ON mp.id = msp.plan_id " +
                                " INNER JOIN msdb.dbo.sysmaintplan_log mpl ON msp.subplan_id = mpl.subplan_id " +
                                    " AND mpl.task_detail_id = " +
                                       " (SELECT TOP 1 ld.task_detail_id " +
                                       " FROM msdb.dbo.sysmaintplan_logdetail ld " +
                                       " WHERE ld.command LIKE('%[' + db_name() + ']%') " +
                                       " ORDER BY ld.start_time DESC)";
            SqlCommand SqlComm = new SqlCommand(sqlStr);
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //_ret.Record = SqlComm.ExecuteScalar();

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "DB lista lelérdezése. ")]
    public Result GetDbList(ExecParam ExecParam)
    {
        bool isConnectionOpenHere = false;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "GetDbList");

        Result _ret = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            String sqlStr = "SELECT database_id,name FROM master.sys.databases where name not in ('master','tempdb','model','msdb') order by 2";
            SqlCommand SqlComm = new SqlCommand(sqlStr);
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //_ret.Record = SqlComm.ExecuteScalar();

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "DB backup. ")]
    public Result BackupDb(ExecParam ExecParam,String databaseName, String fileName)
    {
        bool isConnectionOpenHere = false;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "BackupDb");

        Result _ret = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            String sqlStr = String.Format("BACKUP DATABASE [{0}] TO DISK='{1}' ", databaseName, fileName);
            SqlCommand SqlComm = new SqlCommand(sqlStr);
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            _ret.Uid = SqlComm.ExecuteNonQuery().ToString();

            //DataSet ds = new DataSet();
            //SqlDataAdapter adapter = new SqlDataAdapter();
            //adapter.SelectCommand = SqlComm;
            //adapter.Fill(ds);

            //_ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "DB restore. ")]
    public Result RestoreDb(ExecParam ExecParam, String databaseName, String fileName)
    {
        bool isConnectionOpenHere = false;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "RestoreDb");

        Result _ret = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //String sqlStr = String.Format("RESTORE DATABASE [{0}] FROM DISK='{1}' WITH REPLACE ", databaseName, fileName);
            String sqlStr = String.Format("RESTORE FILELISTONLY FROM DISK = '{0}'", fileName);
            SqlCommand SqlComm = new SqlCommand(sqlStr);
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //_ret.Uid = SqlComm.exe

            //DataSet ds = new DataSet();
            //SqlDataAdapter adapter = new SqlDataAdapter();
            //adapter.SelectCommand = SqlComm;
            //adapter.Fill(ds);

            //_ret.Ds = ds;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            String logicalNameToMDF = String.Empty;
            String logicalNameToLDF = String.Empty;
            foreach (DataRow drDatabase in ds.Tables[0].Rows)
            {
                String rowType = drDatabase["Type"].ToString();
                if ("L".Equals(rowType, StringComparison.CurrentCultureIgnoreCase))
                {
                    logicalNameToLDF = drDatabase["LogicalName"].ToString();
                }
                else
                {
                    logicalNameToMDF = drDatabase["LogicalName"].ToString();
                }
            }
            if(!String.IsNullOrEmpty(logicalNameToMDF) && !String.IsNullOrEmpty(logicalNameToLDF))
            {
                sqlStr = String.Format("RESTORE DATABASE [{0}] FROM DISK='{1}' WITH MOVE '{2}' TO 'c:\\Temp\\{0}.mdf', MOVE '{3}' TO 'c:\\Temp\\{0}_log.ldf'", databaseName, fileName, logicalNameToMDF, logicalNameToLDF);
            }
            else
            {
                sqlStr = String.Format("RESTORE DATABASE [{0}] FROM DISK='{1}'", databaseName, fileName);
            }

            SqlCommand SqlComm2 = new SqlCommand(sqlStr);
            SqlComm2.CommandType = System.Data.CommandType.Text;

            SqlComm2.Connection = dataContext.Connection;
            SqlComm2.Transaction = dataContext.Transaction;

            _ret.Uid = SqlComm2.ExecuteNonQuery().ToString();
            
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

}
