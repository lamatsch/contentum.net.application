﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using System.Collections.Generic;
using EmailTemplateParser;
using System.IO;

/// <summary>
/// Summary description for MessageService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class MessageService : System.Web.Services.WebService
{
    private DataContext dataContext;

    public MessageService()
    {
        dataContext = new DataContext(this.Application);
    }

    public MessageService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(Message))]
    public bool SendNotification(ExecParam execParam, Message message)
    {
        Logger.Debug("SendNotification start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        bool _ret = true;

        try
        {
            Fph.Ter.Messanger.Configuration.ConfigureDefaultMessage(message);
            message.ValidateProperties();
            foreach (string address in message.AddressLogins)
            {
                Logger.Debug("addressLogin: " + address);
            }
            Fph.Ter.Messanger.WebServiceProxy.MessageService messageService = Fph.Ter.Messanger.WebServiceAdapter.GetMessageService();
            Guid messageID = messageService.SendNotification(message.AddressLogins.ToArray(), message.Body, message.Title,
                message.SkinName, message.BodyLinkUrl, message.TitleLinkUrl, message.DelaySeconds);

            _ret = (messageID != Guid.Empty);
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendNotification hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        Logger.Debug("SendNotification end");
        return _ret;
    }

    private Parser GetTemplateParser(string templateName)
    {
        Parser emailparser = null;
        try
        {
            emailparser = new Parser(Server.MapPath("~/MessageTemplate/" + templateName + ".txt"));
        }
        catch
        {
            return null;
        }

        return emailparser;
    }

    public const string FeladatNotificationMessageTitle = "Feladat értesítés";
    public const string HataridosErtesitesMessageTitle = "Határidős értesítés";

    public static string GetUserNameWithoutDomain(string userNev)
    {
        return Path.GetFileName(userNev);
    }

    public const int defaultMaxtLength = 27;

    public static string GetLimitedString(string text, int maxLength)
    {
        if (maxLength < 0)
        {
            return text;
        }

        //if (maxLength> defaultMaxtLength && text.Length > defaultMaxtLength)
        //{
        //    text = text.Substring(0, defaultMaxtLength) + " " + text.Substring(defaultMaxtLength, text.Length - defaultMaxtLength);
        //}
        if (text.Length > maxLength)
        {
            text = text.Substring(0, maxLength - 3) + "...";
        }

        return text;
    }

    public static string GetLimitedString(string text)
    {
        return GetLimitedString(text, defaultMaxtLength);
    }

    public enum FeladatadNotificationType
    {
        Uj,
        Lezart
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public bool SendFeladatNotificationMessage(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("execParam.Felhasznalo_Id: " + execParam.Felhasznalo_Id ?? "NULL");
        Result res = new Result();
        bool _ret = true;

        try
        {
            Logger.Debug("Bemeneti parameterek ellenorzes");
            FeladatadNotificationType notificationType = FeladatadNotificationType.Uj;
            Logger.Debug("Feladat Allapot:" + erec_HataridosFeladatok.Allapot);
            switch (erec_HataridosFeladatok.Allapot)
            {
                case KodTarak.FELADAT_ALLAPOT.Uj:
                    Logger.Debug("Uj");
                    notificationType = FeladatadNotificationType.Uj;
                    break;
                case KodTarak.FELADAT_ALLAPOT.Lezart:
                    Logger.Debug("Lezart");
                    notificationType = FeladatadNotificationType.Lezart;
                    break;
                default:
                    Logger.Debug("Nem kell ertesites");
                    return true;
            }
            Arguments args = new Arguments();
            if (notificationType == FeladatadNotificationType.Uj)
            {
                Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_Felelos: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos ?? "NULL");
                args.Add(new Argument("Címzett azonosító", erec_HataridosFeladatok.Felhasznalo_Id_Felelos, ArgumentTypes.Guid));
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_kiado: " + erec_HataridosFeladatok.Felhasznalo_Id_Kiado ?? "NULL");
                args.Add(new Argument("Címzett azonosító", erec_HataridosFeladatok.Felhasznalo_Id_Kiado, ArgumentTypes.Guid));
            }
            args.ValidateArguments();

            Logger.Debug("Felhasznalok lekerese start: " + execParam.Record_Id);
            KRT_FelhasznalokService svcFelhasznalok = new KRT_FelhasznalokService(this.dataContext);
            ExecParam xpmFelhasznalok = execParam.Clone();
            KRT_FelhasznalokSearch schFelhasznalok = new KRT_FelhasznalokSearch();
            List<string> listFelhasznalok = new List<string>();
            listFelhasznalok.Add(erec_HataridosFeladatok.Felhasznalo_Id_Felelos);
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Felhasznalo_Id_Kiado))
            {
                listFelhasznalok.Add(erec_HataridosFeladatok.Felhasznalo_Id_Kiado);
            }
            else
            {
                Logger.Warn("String.IsNullOrEmpty(erec_HataridosFeladatok.Felhasznalo_Id_kiado)");
            }

            #region 323
            List<string> listErtesitendoFelhasznalok = new List<string>();
            foreach (string felhasznalo in listFelhasznalok)
            {
                if (!FeladatErtesitesKuldheto(execParam, erec_HataridosFeladatok.FeladatDefinicio_Id, felhasznalo))
                {
                    Logger.Debug(string.Format("FeladatErtesites alapjan nem kell ertesites. FELADATDEF:{0} FELADATFELELOS:{1}", erec_HataridosFeladatok.FeladatDefinicio_Id, felhasznalo));
                }
                else
                {
                    listErtesitendoFelhasznalok.Add(felhasznalo);
                }
            }
            listFelhasznalok = listErtesitendoFelhasznalok;
            if (listFelhasznalok.Count < 1)
            {
                Logger.Debug("Feladat értesites nem szükséges, nincsenek értesítendők !");
                return true;
            }
            #endregion 323

            schFelhasznalok.Id.Value = Search.GetSqlInnerString(listFelhasznalok.ToArray());
            schFelhasznalok.Id.Operator = Query.Operators.inner;
            Result resFelhasznalok = svcFelhasznalok.GetAll(xpmFelhasznalok, schFelhasznalok);
            if (resFelhasznalok.IsError)
            {
                Logger.Error("Felhasznalok lekerese hiba", xpmFelhasznalok, resFelhasznalok);
                throw new ResultException(resFelhasznalok);
            }
            Logger.Debug("Felhasznalok lekerese vege");
            string messageAddress = String.Empty;
            string felhasznaloFelelosNev = String.Empty;
            string felhasznaloKiadoNev = String.Empty;
            foreach (DataRow row in resFelhasznalok.Ds.Tables[0].Rows)
            {
                if (String.Compare(row["Id"].ToString(), erec_HataridosFeladatok.Felhasznalo_Id_Felelos, true) == 0)
                {
                    if (notificationType == FeladatadNotificationType.Uj)
                    {
                        messageAddress = row["UserNev"].ToString();
                        Logger.Debug("messageAddress: " + messageAddress ?? "NULL");
                    }
                    felhasznaloFelelosNev = row["Nev"].ToString();
                    Logger.Debug("felhasznaloFelelosNev: " + felhasznaloFelelosNev ?? "NULL");
                }
                if (String.Compare(row["Id"].ToString(), erec_HataridosFeladatok.Felhasznalo_Id_Kiado, true) == 0)
                {
                    if (notificationType == FeladatadNotificationType.Lezart)
                    {
                        messageAddress = row["UserNev"].ToString();
                        Logger.Debug("messageAddress: " + messageAddress ?? "NULL");
                    }
                    felhasznaloKiadoNev = row["Nev"].ToString();
                    Logger.Debug("felhasznaloKiadoNev: " + felhasznaloKiadoNev ?? "NULL");
                }
            }

            if (String.IsNullOrEmpty(messageAddress))
            {
                Logger.Error("String.IsNullOrEmpty(messageAddress)");
                throw new ResultException("String.IsNullOrEmpty(messageAddress)");
            }

            Parser emailParser = null;

            if (notificationType == FeladatadNotificationType.Uj)
            {
                emailParser = this.GetTemplateParser("UjFeladatNotificationTemplate");
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                emailParser = this.GetTemplateParser("LezartFeladatNotificationTemplate");
            }

            if (emailParser == null)
            {
                Logger.Error("emailParser = null");
                throw new ResultException("emailParser = null");
            }

            Logger.Debug("Template valtozok hozzadasa start");
            Hashtable templateVars = new Hashtable();
            if (notificationType == FeladatadNotificationType.Uj)
            {
                templateVars.Add("Cimzett", GetLimitedString(felhasznaloFelelosNev));
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                templateVars.Add("Cimzett", GetLimitedString(felhasznaloKiadoNev));
            }
            string tipusNev = String.Empty;
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Funkcio_Id_Inditando))
            {
                Logger.Debug("Funkcio_Id_Inditando lekerese start");
                KRT_FunkciokService funkciokService = new KRT_FunkciokService(this.dataContext);
                ExecParam xpmFunkcio = execParam.Clone();
                xpmFunkcio.Record_Id = erec_HataridosFeladatok.Funkcio_Id_Inditando;
                Result resFunkcio = funkciokService.Get(xpmFunkcio);
                if (res.IsError)
                {
                    Logger.Error("Funkcio_Id_Inditando lekerese hiba", xpmFunkcio, resFunkcio);
                }
                else
                {
                    tipusNev = ((KRT_Funkciok)resFunkcio.Record).Nev;
                }
                Logger.Debug("Funkcio_Id_Inditando lekerese end");

            }
            else
            {
                KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.FELADAT_ALTIPUS.kcsNev, erec_HataridosFeladatok.Altipus, execParam, HttpContext.Current.Cache);
            }
            templateVars.Add("FeladatTipus", GetLimitedString(tipusNev));
            templateVars.Add("FeladatLeiras", GetLimitedString(erec_HataridosFeladatok.Leiras, 49));
            templateVars.Add("FeladatHatarido", erec_HataridosFeladatok.Typed.IntezkHatarido.IsNull ? String.Empty : erec_HataridosFeladatok.Typed.IntezkHatarido.Value.ToShortDateString());
            templateVars.Add("FeladatKiadas", erec_HataridosFeladatok.Base.Typed.LetrehozasIdo.Value.ToShortDateString());
            templateVars.Add("FeladatKiado", felhasznaloKiadoNev);
            templateVars.Add("FeladatFelelos", felhasznaloFelelosNev);
            string selectedTab = String.Empty;
            if (notificationType == FeladatadNotificationType.Uj)
            {
                selectedTab = "TabPanelFeldataim";
            }
            if (notificationType == FeladatadNotificationType.Lezart)
            {
                selectedTab = "TabPanelKiadott";
            }
            string FeladataimUrl = Contentum.eUtility.UI.GetAppSetting("eRecordWebSiteUrl") + "Feladatok.aspx?" + QueryStringVars.SelectedTab + "=" + selectedTab
            + "&" + QueryStringVars.Id + "=" + erec_HataridosFeladatok.Id;
            templateVars.Add("FeladataimUrl", FeladataimUrl);
            EmailService.ResolveFeladatAzonostio(this.dataContext, execParam, erec_HataridosFeladatok);
            string azonosito = erec_HataridosFeladatok.Azonosito;
            templateVars.Add("Azonosito", azonosito);
            emailParser.Variables = templateVars;
            Logger.Debug("Template valtozok hozzadasa end");

            Message message = new Message();
            message.AddressLogins.Add(GetUserNameWithoutDomain(messageAddress));
            message.Title = FeladatNotificationMessageTitle;
            bool isFeladatDisabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.FELADAT_KEZELES_DISABLED, false);
            if (!isFeladatDisabled)
                message.TitleLinkUrl = FeladataimUrl;
            message.Body = emailParser.Parse();
            //message.BodyLinkUrl = FeladataimUrl;

            Logger.Debug("SendNotification start");
            _ret = SendNotification(execParam, message);
            if (!_ret)
            {
                Logger.Error("SendNotification hiba");
                throw new ResultException("SendNotification hiba");
            }
            Logger.Debug("SendNotification end");
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendFeladatNotificationMessage hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosErtesitesek))]
    public bool SendHataridosErtesitesMessageTomeges(ExecParam execParam, List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        bool _ret = true;

        foreach (EREC_HataridosErtesitesek erec_HataridosErtesitesek in erec_HataridosErtesitesekList)
        {
            _ret = this.SendHataridosErtesitesMessage(execParam, erec_HataridosErtesitesek);
        }

        log.WsEnd(execParam);
        return _ret;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosErtesitesek))]
    public bool SendHataridosErtesitesMessage(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        bool _ret = true;

        try
        {
            #region job fix

            //job nem ad át felhasználó id-t, ezért a rendszerparamétert nem tudjuk lekérni
            if (String.IsNullOrEmpty(execParam.Felhasznalo_Id) && !String.IsNullOrEmpty(erec_HataridosErtesitesek.Felhasznalo_Id_Felelos))
            {
                execParam.Felhasznalo_Id = erec_HataridosErtesitesek.Felhasznalo_Id_Felelos;
            }

            #endregion

            EREC_HataridosFeladatok erec_HataridosFeladatok = (EREC_HataridosFeladatok)erec_HataridosErtesitesek;
            Logger.Debug("Bemeneti parameterek ellenorzes");
            Arguments args = new Arguments();
            Logger.Debug("execParam.Felhasznalo_Id: " + execParam.Felhasznalo_Id ?? "NULL");
            Logger.Debug("erec_HataridosFeladatok.Felhasznalo_Id_Felelos: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos ?? "NULL");
            args.Add(new Argument("Címzett azonosító", erec_HataridosFeladatok.Felhasznalo_Id_Felelos, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("Felhasznalo_Id_Felelos lekerese start: " + erec_HataridosFeladatok.Felhasznalo_Id_Felelos);
            KRT_FelhasznalokService svcFelhasznalok = new KRT_FelhasznalokService(this.dataContext);
            ExecParam xpmFelhasznalo = execParam.Clone();
            xpmFelhasznalo.Record_Id = erec_HataridosFeladatok.Felhasznalo_Id_Felelos;
            Result resFelhasznalo = svcFelhasznalok.Get(xpmFelhasznalo);
            if (resFelhasznalo.IsError)
            {
                Logger.Error("Felhasznalo lekerese hiba", xpmFelhasznalo, resFelhasznalo);
                throw new ResultException(resFelhasznalo);
            }
            Logger.Debug("Felhasznalo lekerese vege");
            KRT_Felhasznalok FelelosFelhasznalo = (KRT_Felhasznalok)resFelhasznalo.Record;

            #region 323
            if (!FeladatErtesitesKuldheto(execParam, erec_HataridosFeladatok.FeladatDefinicio_Id, erec_HataridosFeladatok.Felhasznalo_Id_Felelos))
            {
                Logger.Debug(string.Format("FeladatErtesites alapjan nem kell ertesites. FELADATDEF:{0} FELADATFELELOS:{1}", erec_HataridosFeladatok.FeladatDefinicio_Id, erec_HataridosFeladatok.Felhasznalo_Id_Felelos));
                return true;
            }
            #endregion 323

            string messageAddress = FelelosFelhasznalo.UserNev;
            Logger.Debug("messageAddress: " + messageAddress ?? "NULL");
            string felhasznaloFelelosNev = FelelosFelhasznalo.Nev;
            Logger.Debug("felhasznaloFelelosNev: " + felhasznaloFelelosNev ?? "NULL");

            if (String.IsNullOrEmpty(messageAddress))
            {
                Logger.Error("String.IsNullOrEmpty(messageAddress)");
                throw new ResultException("String.IsNullOrEmpty(messageAddress)");
            }

            Parser emailParser = null;

            emailParser = this.GetTemplateParser("HataridosErtesitesTemplate");

            if (emailParser == null)
            {
                Logger.Error("emailParser = null");
                throw new ResultException("emailParser = null");
            }

            Logger.Debug("Template valtozok hozzadasa start");
            Hashtable templateVars = new Hashtable();
            templateVars.Add("Cimzett", felhasznaloFelelosNev);
            string esemeny_Kivalto_Nev = String.Empty;
            string esemeny_Kivalto_Kod = String.Empty;
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.Esemeny_Id_Kivalto))
            {
                Logger.Debug("Esemeny_Id_Kivalto lekerese start");
                KRT_FunkciokService funkciokService = new KRT_FunkciokService(this.dataContext);
                ExecParam xpmFunkcio = execParam.Clone();
                xpmFunkcio.Record_Id = erec_HataridosFeladatok.Esemeny_Id_Kivalto;
                Result resFunkcio = funkciokService.Get(xpmFunkcio);
                if (resFunkcio.IsError)
                {
                    Logger.Error("Esemeny_Id_Kivalto lekerese hiba", xpmFunkcio, resFunkcio);
                }
                else
                {
                    esemeny_Kivalto_Nev = ((KRT_Funkciok)resFunkcio.Record).Nev;
                    esemeny_Kivalto_Kod = ((KRT_Funkciok)resFunkcio.Record).Kod;
                }
                Logger.Debug("Esemeny_Id_Kivalto lekerese end");

            }
            templateVars.Add("Esemeny_Kivalto_Nev", esemeny_Kivalto_Nev);
            templateVars.Add("Leiras", erec_HataridosFeladatok.Leiras);
            string hatarido = String.Empty;
            if (!String.IsNullOrEmpty(erec_HataridosFeladatok.IntezkHatarido))
            {
                hatarido = "\nHatáridő: " + erec_HataridosFeladatok.IntezkHatarido;
            }
            templateVars.Add("Hatarido", hatarido);
            emailParser.Variables = templateVars;
            Logger.Debug("Template valtozok hozzadasa end");

            Message message = new Message();
            message.AddressLogins.Add(GetUserNameWithoutDomain(messageAddress));
            message.Title = HataridosErtesitesMessageTitle;
            message.Body = emailParser.Parse();

            string url = EmailService.GetHataridosErtesitesUrl(erec_HataridosErtesitesek, esemeny_Kivalto_Kod);

            if (!String.IsNullOrEmpty(url))
            {
                //message.TitleLinkUrl = url;
                message.BodyLinkUrl = url;
            }

            Logger.Debug("SendNotification start");
            _ret = SendNotification(execParam, message);
            if (!_ret)
            {
                Logger.Error("SendNotification hiba");
                throw new ResultException("SendNotification hiba");
            }
            Logger.Debug("SendNotification end");
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SendFeladatNotificationEmail hiba", execParam, res);
            _ret = false;
        }

        log.WsEnd(execParam, res);
        return _ret;
    }
    /// <summary>
    /// EREC_FeladatErtesites alapján eldönti, hogy kell-e értesítés.
    /// Ha nincs tíltva akkor mehet az értesítés.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="feladatDefinicioId"></param>
    /// <param name="feladatFelhasznaloId"></param>
    /// <returns></returns>
    public bool FeladatErtesitesKuldheto(ExecParam execParam, string feladatDefinicioId, string feladatFelhasznaloId)
    {
        Contentum.eRecord.Service.EREC_FeladatErtesitesService serviceFER = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
        ExecParam xpm = execParam.Clone();
        EREC_FeladatErtesitesSearch src = new EREC_FeladatErtesitesSearch();

        src.FeladatDefinicio_id.Value = feladatDefinicioId;
        src.FeladatDefinicio_id.Operator = Query.Operators.equals;

        src.Felhasznalo_Id.Value = feladatFelhasznaloId;
        src.Felhasznalo_Id.Operator = Query.Operators.equals;

        src.Ertesites.Value = "0";
        src.Ertesites.Operator = Query.Operators.equals;

        Result result = serviceFER.GetAll(xpm, src);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            return true; //? DEFAULT
        }

        if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
            return false;

        return true;
    }
}

