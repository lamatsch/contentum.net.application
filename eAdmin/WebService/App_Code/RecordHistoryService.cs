using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;


/// <summary>
/// Summary description for RecordHistoryService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class RecordHistoryService : System.Web.Services.WebService
{
    private RecordHistoryServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public RecordHistoryService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new RecordHistoryServiceStoredProcedure(this.Application);
        sp = new RecordHistoryServiceStoredProcedure(this.dataContext);
    }

    public RecordHistoryService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new RecordHistoryServiceStoredProcedure(this.dataContext);
    }


    [WebMethod()]
    public Result GetAllByRecord(ExecParam ExecParam, string TableName, System.Data.SqlTypes.SqlGuid Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByRecord(ExecParam, TableName);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            KRT_Esemenyek eventLogRecord = null;

            if (TableName == "EREC_KuldKuldemenyek")
            {
                //LZS BUG_7058
                eventLogRecord = (KRT_Esemenyek)eventLogService.InsertByFunkcioKod(ExecParam, ExecParam.Page_Id, ExecParam.Record_Id, TableName, "KuldemenyViewHistory", null, null).Record;
            }
            else
            {
                eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, TableName, "ViewHistory").Record;
            }

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    //[WebMethod()]
    //public Result Test()
    //{
    //    ExecParam ExecParam = new ExecParam();
    //    ExecParam.Record_Id = "0C63DE14-8060-4A16-8A22-333C6E7A9882";

    //    return sp.GetAllByRecord(ExecParam, "KRT_Szerepkorok");
    //}

    //[WebMethod()]
    //public Result TransactionTest_WithSetAbort()
    //{
    //    ExecParam ExecParam = new ExecParam();
    //    ExecParam.Felhasznalo_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";
    //    ExecParam.Record_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";     

    //    KRT_Felhasznalok f = new KRT_Felhasznalok();
    //    f.Nev = "Harmat Tran2";
    //    f.Updated.SetValueAll(false);
    //    f.Updated.Nev = true;
    //    f.Base.Ver = "3";
    //    f.Base.LetrehozasIdo = "2007-06-19 13:36:51.217";

    //    KRT_FelhasznalokService s = new KRT_FelhasznalokService();
    //    s.Update(ExecParam, f);

    //    return null;            
    //}

    //[WebMethod()]
    //public Result TransactionTest_WithException()
    //{
    //    //ContextUtil.EnableCommit();
    //    ExecParam ExecParam = new ExecParam();
    //    ExecParam.Felhasznalo_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";
    //    ExecParam.Record_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";

    //    KRT_Felhasznalok f = new KRT_Felhasznalok();
    //    f.Nev = "Harmat Tran2";
    //    f.Updated.SetValueAll(false);
    //    f.Updated.Nev = true;
    //    f.Base.Ver = "3";
    //    f.Base.LetrehozasIdo = "2007-06-19 13:36:51.217";


    //    KRT_FelhasznalokService s = new KRT_FelhasznalokService();
    //    s.Update(ExecParam, f);

    //    ExecParam.Record_Id = "1B6763A5-A8EA-4A9F-A44D-1D1F9A60E0D7";
    //    s.Delete(ExecParam);

    //    ExecParam = new ExecParam();
    //    ExecParam.Record_Id = "9E714610-5E6F-4338-B410-FA25CD14A715";
    //    ExecParam.Felhasznalo_Id = "D8DC057E-A7D2-4FDE-A5AB-880574B069E8";
    //    KRT_Csoportok cs = new KRT_Csoportok();
    //    cs.Nev = "TRAN";
    //    cs.Updated.SetValueAll(false);
    //    cs.Updated.Nev = true;
    //    cs.Base.Ver = "2";
    //    cs.Base.LetrehozasIdo = "2007-06-19 13:36:51.217";


    //    KRT_CsoportokService c = new KRT_CsoportokService();
    //    c.Update(ExecParam, cs);

    //    try
    //    {
    //        int xx = 0;

    //        int x = 1 / xx;
    //    }
    //    catch
    //    {
    //        ContextUtil.SetAbort();
    //    }

    //    //ContextUtil.SetAbort();

    //    return null;
    //}  

}

