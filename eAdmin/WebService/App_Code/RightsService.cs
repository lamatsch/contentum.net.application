using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
//using System.EnterpriseServices;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using System.Data;
using Contentum.eDocument.Service;

/// <summary>
/// Summary description for JogosultsagService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class RightsService : System.Web.Services.WebService
{
    private RightsServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public RightsService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new RightsServiceStoredProcedure(this.Application);
        sp = new RightsServiceStoredProcedure(dataContext);

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public RightsService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new RightsServiceStoredProcedure(dataContext);
    }

    [WebMethod]
    public Result AddCsoportToJogtargy(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi)
    {

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, 'I', '0');

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            SetUCMJogosultsag(execParam, JogtargyId);
        }

        return result;
    }

    [WebMethod]
    public Result AddCsoportToJogtargyWithJogszint(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi,
        char Jogszint,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, Jogszint, Tipus);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            SetUCMJogosultsag(execParam, JogtargyId);
        }

        return result;
    }

    [WebMethod]
    public Result AddScopeIdToJogtargy(ExecParam execParam,
        string JogtargyId,
        string JogtargyTablaNev,
        string JogtargySzuloId,
        char CsoportOroklesMod,
        string CsoportId)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddScopeIdToJogtargy(execParam, JogtargyId, JogtargyTablaNev, JogtargySzuloId, CsoportOroklesMod, CsoportId, 'I');


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result AddScopeIdToJogtargyWithJogszint(ExecParam execParam,
        string JogtargyId,
        string JogtargyTablaNev,
        string JogtargySzuloId,
        char CsoportOroklesMod,
        string CsoportId, char OroklottJogszint)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddScopeIdToJogtargy(execParam, JogtargyId, JogtargyTablaNev, JogtargySzuloId, CsoportOroklesMod, CsoportId, OroklottJogszint);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    //[WebMethod]
    //public Result ChangeJogtargyRightInherit(ExecParam execParam,
    //    string JogtargyId,
    //    string SzuloJogtargyId,
    //    char UjOroklesTipus,
    //    char OroklesMasolas)
    //{
    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


    //        result = sp.ChangeJogtargyRightInherit(execParam, JogtargyId, SzuloJogtargyId, UjOroklesTipus, OroklesMasolas);


    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    [WebMethod]
    public Result GetRightsByJogtargyIdAndCsoportId(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Jogszint)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetRightsByJogtargyIdAndCsoportId(execParam, JogtargyId, CsoportId, Jogszint);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod]
    public Result GetRightsByJogtargyIdAndCsoportIdAndTipus(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Jogszint,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetRightsByJogtargyIdAndCsoportId(execParam, JogtargyId, CsoportId, Jogszint,Tipus);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargy(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargy(execParam, JogtargyId, CsoportId, Kezi, '0');


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            RemoveUCMJogosultsag(execParam, JogtargyId, CsoportId);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargyByTipus(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        KRT_Jogosultak jogosult = null;

        Result res = GetJogosult(execParam);

        if (!res.IsError && res.Record != null)
        {
            jogosult = res.Record as KRT_Jogosultak;
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargy(execParam, JogtargyId, CsoportId, Kezi, Tipus);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            RemoveUCMJogosultsag(execParam, JogtargyId, CsoportId);
        }

        return result;
    }


    [WebMethod]
    public Result GetAllRightedCsoportByJogtargy(ExecParam exeParam, string JogtargyId, RowRightsCsoportSearch where)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllRightedCsoportByJogtargy(exeParam, JogtargyId, where);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargyById(ExecParam execParam)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        KRT_Jogosultak jogosult = null;

        Result res = GetJogosult(execParam);

        if (!res.IsError && res.Record != null)
        {
            jogosult = res.Record as KRT_Jogosultak;
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargyById(execParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            if (jogosult != null)
            {
                RemoveUCMJogosultsag(execParam, jogosult.Obj_Id, jogosult.Csoport_Id_Jogalany);
            }
        }

        return result;
    }


    [WebMethod()]
    public Result MultiRemoveCsoportFromJogtargyById(ExecParam[] execParams)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            for (int i = 0; i < execParams.Length; i++)
            {
                result = RemoveCsoportFromJogtargyById(execParams[i]);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    public Result IsUserMember(ExecParam execParam, string csoportId)
    {
        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.IsUserMember(execParam,csoportId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportokSearch))]
    public Result GetAllRightedSubCsoportByJogtargy(ExecParam exeParam, string JogtargyId, KRT_CsoportokSearch search)
    {
        Logger.DebugStart();
        Logger.Debug("GetAllRightedSubCsoportByJogtargy webservice elindul");
        Logger.Debug(String.Format("Param�terek: felhaszn�l� id: {0}, jogt�rgy id: {1}",exeParam.Felhasznalo_Id,JogtargyId));
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllRightedSubCsoportByJogtargy(exeParam, JogtargyId, search);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("GetAllRightedSubCsoportByJogtargy webservice hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("GetAllRightedSubCsoportByJogtargy webservice v�ge");
        Logger.DebugEnd();
        return result;
    }

    [WebMethod()]
    public Result SetCsoportokToJogtargy(ExecParam execParam, string JogtargyId, List<string> CsoportIds, char Kezi)
    {
        Logger.Debug("SetCsoportokToJogtargy start");

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("jelenleg hozz�rendelt csoportok lek�r�se");

            Result resultGetAll = GetAllRightedCsoportByJogtargy(execParam, JogtargyId, new RowRightsCsoportSearch());

            if (!String.IsNullOrEmpty(resultGetAll.ErrorCode))
            {
                throw new ResultException(resultGetAll);
            }

            List<string> CurrentCsoportIds = new List<string>();

            foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
            {
                string csoportId = row["Csoport_Id_Jogalany"].ToString();
                CurrentCsoportIds.Add(csoportId);
            }

            List<string> AddCsoportIds = new List<string>();
            List<string> RemoveCsoportIds = new List<string>();

            foreach (string csoportId in CsoportIds)
            {
                if (!CurrentCsoportIds.Contains(csoportId))
                {
                    AddCsoportIds.Add(csoportId);
                }
            }

            foreach (string csoportId in CurrentCsoportIds)
            {
                if (!CsoportIds.Contains(csoportId))
                {
                    RemoveCsoportIds.Add(csoportId);
                }
            }

            Logger.Debug("�j csoportok hozz�rendel�se");

            foreach (string csoportId in AddCsoportIds)
            {
                Result resultAdd = AddCsoportToJogtargy(execParam, JogtargyId, csoportId, Kezi);

                if (!String.IsNullOrEmpty(resultAdd.ErrorCode))
                {
                    throw new ResultException(resultAdd);
                }
            }

            result.Record = AddCsoportIds;

            Logger.Debug("Megsz�ntetett csoportok hozz�rendel�s�nek t�rl�se");

            List<string> RemoveJogosultIds = new List<string>();

            foreach (string csoportId in RemoveCsoportIds)
            {
                foreach(DataRow row in resultGetAll.Ds.Tables[0].Rows)
                {
                    string jogosultId = row["Id"].ToString();
                    string csoportIdJogalany = row["Csoport_Id_Jogalany"].ToString();

                    if (csoportIdJogalany == csoportId)
                    {
                        RemoveJogosultIds.Add(jogosultId);
                    }
                }
            }

            foreach (string jogosultId in RemoveJogosultIds)
            {
                ExecParam execParamRemove = execParam.Clone();
                execParamRemove.Record_Id = jogosultId;

                Result resultRemove = RemoveCsoportFromJogtargyById(execParamRemove);

                if (!String.IsNullOrEmpty(resultRemove.ErrorCode))
                {
                    throw new ResultException(resultRemove);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("SetCsoportokToJogtargy vege");
        return result;
    }

    [WebMethod]
    public Result AddCsoportToJogtargyTomeges(ExecParam execParam, string[] jogtargyIds, string[] csoportIds, string obj_type)
    {

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargyTomeges(execParam, jogtargyIds, csoportIds);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!result.IsError)
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentService ucmService = eDocumentService.ServiceFactory.GetUCMDocumentService();
                System.Threading.Thread t = new System.Threading.Thread(delegate ()
                {
                    ucmService.SetJogosultakTomeges(execParam, jogtargyIds, obj_type);
                });
                t.Start();
            }
        }

        return result;
    }

    #region ucm

    private Result SetUCMJogosultsag(ExecParam execParam, string jogtargyId)
    {
        Logger.Info(String.Format("RightsService.SetUCMJogosultsag: {0}", jogtargyId));
        Result result = new Result();
        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentService ucmService = eDocumentService.ServiceFactory.GetUCMDocumentService();
                result = ucmService.SetJogosultak(execParam, jogtargyId);
            }
        }
        catch (Exception ex)
        {
            Logger.Error("RightsService.SetUCMJogosultsag hiba", ex);
            result = ResultException.GetResultFromException(ex);
        }

        return result;
    }

    private Result GetJogosult(ExecParam execParam)
    {
        Logger.Info(String.Format("RightsService.GetJogosult: {0}", execParam.Record_Id));
        Result result = new Result();

        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                KRT_JogosultakService jogosultakService = new KRT_JogosultakService();
                ExecParam execParamJogosultak = execParam.Clone();
                Result jogosultakResult = jogosultakService.Get(execParam);

                if (jogosultakResult.IsError)
                    throw new ResultException(jogosultakResult);

                return jogosultakResult;
            }
        }
        catch (Exception ex)
        {
            Logger.Error("RightsService.GetJogosult hiba", ex);
            result = ResultException.GetResultFromException(ex);
        }

        return result;
    }

    private Result RemoveUCMJogosultsag(ExecParam execParam, string jogtargyId, string csoportId)
    {
        Logger.Info(String.Format("RightsService.RemoveUCMJogosultsag: {0}, {1}", jogtargyId, csoportId));
        Result result = new Result();
        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentService ucmService = eDocumentService.ServiceFactory.GetUCMDocumentService();
                result = ucmService.RemoveJogosultak(execParam, jogtargyId, csoportId);
            }
        }
        catch (Exception ex)
        {
            Logger.Error("RightsService.RemoveUCMJogosultsag hiba", ex);
            result = ResultException.GetResultFromException(ex);
        }

        return result;
    }

    #endregion
}

