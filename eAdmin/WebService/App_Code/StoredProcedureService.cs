﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for StoredProcedureService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class StoredProcedureService : System.Web.Services.WebService
{

    private DataContext dataContext;

    public StoredProcedureService()
    {
        dataContext = new DataContext(this.Application);
    }

    [WebMethod]
    public Result GetAll(ExecParam ExecParam, StoredProcedureSearch _StoredProcedureSearch)
    {
        bool isConnectionOpenHere = false;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "GetAll");

        Result _ret = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Query query = new Query();
            query.BuildFromBusinessDocument(_StoredProcedureSearch);

            var topRow = _StoredProcedureSearch.TopRow > 0 ? " TOP " + _StoredProcedureSearch.TopRow : "";
            String sqlStr = "select" + topRow + " object_id as Id, name from sys.procedures";
            if (!string.IsNullOrEmpty(query.Where))
                sqlStr += " where " + query.Where;
            if (!string.IsNullOrEmpty(_StoredProcedureSearch.OrderBy))
                sqlStr += " order by " + _StoredProcedureSearch.OrderBy;
            SqlCommand SqlComm = new SqlCommand(sqlStr);
            SqlComm.CommandType = System.Data.CommandType.Text;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}
