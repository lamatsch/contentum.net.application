﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<%@ Register Assembly="Contentum.eUIControls" Namespace="Contentum.eUIControls" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <asp:Label runat="server" ID="LabelMsg"> </asp:Label>
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <asp:TextBox ID="TextBox_EsemenySetChecksum" runat="server" Width="400px"></asp:TextBox>
            <br />
            <asp:Button ID="ButtonKrt_EsemenySetChecksum" runat="server" Text="SET KRT_ESEMENY_CHECKSUM" OnClick="ButtonKrt_SET_EsemenyChecksum_Click" />
            <br />
            <br />
            <br />
            <asp:TextBox ID="TextBox_EsemenyCheckChecksum" runat="server" Width="400px"></asp:TextBox>
            <br />
            <asp:Button ID="ButtonKrt_EsemenyCheckChecksum" runat="server" Text="CHECK KRT_ESEMENY_CHECKSUM" OnClick="ButtonKrt_CHECK_EsemenyChecksum_Click" />
            <br />
            <asp:Button ID="Button1" runat="server" Text="SET COUNTED KRT_ESEMENY_CHECKSUM" OnClick="ButtonKrt_SET_CNT_EsemenyChecksum_Click" />
            <br />
        </div>

    </form>
</body>
</html>
