﻿using Contentum.eBusinessDocuments;
using System;

public partial class Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // throw new AccessViolationException();
    }
    protected void ButtonKrt_SET_EsemenyChecksum_Click(object sender, EventArgs e)
    {
        LabelMsg.Text = string.Empty;
        KRT_EsemenyekService svc = new KRT_EsemenyekService();
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        Result result = svc.SetItemIntegrityCheckSum(execParam, TextBox_EsemenySetChecksum.Text);
        LabelMsg.Text = result.ErrorCode + result.ErrorMessage;
        /* "F169CE56-E442-E811-80C9-00155D020DD3"*/
    }
    protected void ButtonKrt_CHECK_EsemenyChecksum_Click(object sender, EventArgs e)
    {
        LabelMsg.Text = string.Empty;
        KRT_EsemenyekService svc = new KRT_EsemenyekService();
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        Result resultcrc = svc.CheckItemIntegrityCheckSum(execParam, TextBox_EsemenyCheckChecksum.Text);/*"E256B179-1A45-E711-A948-000D3A2773CF"*/

        LabelMsg.Text = resultcrc.ErrorCode
            + resultcrc.ErrorMessage
            + (resultcrc.ErrorDetail == null ? string.Empty : resultcrc.ErrorDetail.Message)
            + ((resultcrc.Record == null) ? string.Empty : ((bool)resultcrc.Record).ToString());
    }

    protected void ButtonKrt_SET_CNT_EsemenyChecksum_Click(object sender, EventArgs e)
    {
        LabelMsg.Text = string.Empty;
        KRT_EsemenyekService svc = new KRT_EsemenyekService();
        Result resultcrc = svc.SetIntegrityCheckSumSimple("54E861A5-36ED-44CA-BAA7-C287D125B309", "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6", 10);
        LabelMsg.Text = resultcrc.ErrorCode
            + resultcrc.ErrorMessage
            + (resultcrc.ErrorDetail == null ? string.Empty : resultcrc.ErrorDetail.Message)
            + ((resultcrc.Record == null) ? string.Empty : ((bool)resultcrc.Record).ToString());
    }
}