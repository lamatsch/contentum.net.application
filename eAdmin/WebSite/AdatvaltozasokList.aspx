<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="AdatvaltozasokList.aspx.cs" Inherits="AdatvaltozasokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
        
    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeAdatvaltozasok" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelAdatvaltozasok" runat="server" OnLoad="updatePanelAdatvaltozasok_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeAdatvaltozasok" runat="server" TargetControlID="panelAdatvaltozasok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeAdatvaltozasok" CollapseControlID="btnCpeAdatvaltozasok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeAdatvaltozasok"
                            ExpandedSize="0" ExpandedText="Adatv�ltoz�sok list�ja" CollapsedText="K�dt�rak list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelAdatvaltozasok" runat="server" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewAdatvaltozasok" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewAdatvaltozasok_RowCommand" 
                                     OnPreRender="gridViewAdatvaltozasok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                     DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewAdatvaltozasok_RowDataBound" OnSorting="gridViewAdatvaltozasok_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
                                            <asp:BoundField DataField="ObjTipus_Id_Tabla" HeaderText="T�bla neve" SortExpression="ObjTipus_Id_Tabla">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObjTipus_Id_Oszlop" HeaderText="Oszlop" SortExpression="ObjTipus_Id_Oszlop">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RegiErtek" HeaderText="R�gi �rt�k" SortExpression="RegiErtek">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UjErtek" HeaderText="�j �rt�k" SortExpression="UjErtek">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Letrehozo_id" HeaderText="V�grehajt�" SortExpression="Letrehozo_id">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="V�grehajt�si id�" SortExpression="LetrehozasIdo">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <HeaderStyle  CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkModosithato" runat="server">M�dos�that�</asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbModosithato" runat="server" Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Sorrend" HeaderText="Megjelen�t�si sorrend" SortExpression="Sorrend">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="160px" />
                                            </asp:BoundField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                       <PagerSettings Visible="False" />
                                     </asp:GridView>
                                   </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>
         </tr>
       </table>  
</asp:Content>

