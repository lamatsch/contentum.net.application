using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Contentum.eQuery;

public partial class AdatvaltozasokList : System.Web.UI.Page
{

    UI ui = new UI();

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AdatvaltozasokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_AdatvaltozasokSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.AdatvaltozasokListHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("AdatvaltozasokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAdatvaltozasok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("AdatvaltozasokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAdatvaltozasok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewAdatvaltozasok.ClientID, "check", "cbModosithato");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewAdatvaltozasok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewAdatvaltozasok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewAdatvaltozasok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewAdatvaltozasok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAdatvaltozasok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewAdatvaltozasok;


        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewAdatvaltozasok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_AdatvaltozasokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            AdatvaltozasokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewAdatvaltozasok);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void AdatvaltozasokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewAdatvaltozasok", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewAdatvaltozasok", ViewState);

        AdatvaltozasokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void AdatvaltozasokGridViewBind(String SortExpression, SortDirection SortDirection)
    {


        //KRT_AdatvaltozasokService service = eAdminService.ServiceFactory.GetKRT_AdatvaltozasokService();
        //ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //KRT_AdatvaltozasokSearch search = (KRT_AdatvaltozasokSearch)Search.GetSearchObject(Page, new KRT_AdatvaltozasokSearch());

        //search.OrderBy = Search.GetOrderBy("gridViewAdatvaltozasok", ViewState, SortExpression, SortDirection);

        //Result res = service.GetAllWithKodcsoport(ExecParam, search);

        //UI.GridViewFill(gridViewAdatvaltozasok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewAdatvaltozasok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }

        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewAdatvaltozasok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewAdatvaltozasok.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewAdatvaltozasok.PageIndex = ListHeader1.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewAdatvaltozasok.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeAdatvaltozasok);
            AdatvaltozasokGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeAdatvaltozasok);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewAdatvaltozasok);

        //oldalsz�mok be�ll�t�sa
        ListHeader1.PageCount = gridViewAdatvaltozasok.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewAdatvaltozasok);

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        AdatvaltozasokGridViewBind();
    }


    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewAdatvaltozasok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewAdatvaltozasok, selectedRowNumber, "check");

            string id = gridViewAdatvaltozasok.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewAdatvaltozasok_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewAdatvaltozasok);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("AdatvaltozasokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAdatvaltozasok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("AdatvaltozasokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAdatvaltozasok.ClientID);
            string tableName = "Krt_Adatvaltozasok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelAdatvaltozasok.ClientID);
        }
    }


    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelAdatvaltozasok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    AdatvaltozasokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewAdatvaltozasok));
                    ////gridViewAdatvaltozasok_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewAdatvaltozasok));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodtar();
            AdatvaltozasokGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedKodtar()
    {

        if (FunctionRights.GetFunkcioJog(Page, "KodtarInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewAdatvaltozasok, EErrorPanel1, ErrorUpdatePanel);

            //KRT_AdatvaltozasokService service = eAdminService.ServiceFactory.GetKRT_AdatvaltozasokService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewAdatvaltozasok);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }

            //Result result = service.MultiInvalidate(execParams.ToArray());
            //if (!String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            //    ErrorUpdatePanel.Update();
            //}

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }


    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKodtarRecords();
                AdatvaltozasokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKodtarRecords();
                AdatvaltozasokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedAdatvaltozasok();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedKodtarRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewAdatvaltozasok, "KRT_Adatvaltozasok"
            , "KodtarLock", "KodtarForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedKodtarRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewAdatvaltozasok, "KRT_Adatvaltozasok"
            , "KodtarLock", "KodtarForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewAdatvaltozasok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedAdatvaltozasok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewAdatvaltozasok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Adatvaltozasok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewAdatvaltozasok_Sorting(object sender, GridViewSortEventArgs e)
    {
        AdatvaltozasokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewAdatvaltozasok", ViewState, e.SortExpression));
    }

    #endregion
}
