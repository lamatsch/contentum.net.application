<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="AdatvaltozasokSearch.aspx.cs" Inherits="AdatvaltozasokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc7" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,AdatvaltozasokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="T�bla neve:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc10:KodtarakDropDownList ID="KodtarakDropDownList1" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Oszlop neve:"></asp:Label></td>
                                    <td class="mrUrlapCaption">
                                        <uc10:KodtarakDropDownList ID="KodtarakDropDownList2" runat="server" />
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKod" runat="server" Text="R�gi �rt�k:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        &nbsp;<asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                    <td>
                                        <asp:Label ID="labelRovidNev" runat="server" Text="�j �rt�k:"></asp:Label></td>
                                    <td class="mrUrlapCaption">
                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelModosithato" runat="server" Text="V�grehajt�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox1" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="V�grehajt�s ideje:"></asp:Label></td>
                                    <td class="mrUrlapCaption">
                                        &nbsp;<uc7:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                            runat="server" ValidateDateFormat="true" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                     <td colspan="5">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

