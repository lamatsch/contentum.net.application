using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

// A teljes k�d kidolgozand�! 
public partial class AdatvaltozasokSearch : System.Web.UI.Page
{

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        static public bool Contain(string value)
        {
            if ((value == Yes) || (value == No) || (value == NotSet))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private Type _type = typeof(KRT_AdatvaltozasokSearch);


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_AdatvaltozasokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_AdatvaltozasokSearch)Search.GetSearchObject(Page, new KRT_AdatvaltozasokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_AdatvaltozasokSearch _KRT_AdatvaltozasokSearch = (KRT_AdatvaltozasokSearch)searchObject;

        if (_KRT_AdatvaltozasokSearch != null)
        {
            //KodcsoportokTextBox1.Id_HiddenField = _KRT_AdatvaltozasokSearch.KodCsoport_Id.Value;
            //if (KodcsoportokTextBox1.Id_HiddenField != "")
            //{
            //    KodcsoportokTextBox1.SetTextBoxById(null);
            //}
            //else
            //{
            //    KodcsoportokTextBox1.Text = "";
            //}
            //textNev.Text = _KRT_AdatvaltozasokSearch.Nev.Value;
            //textKod.Text = _KRT_AdatvaltozasokSearch.Kod.Value;
            //textRovidNev.Text = _KRT_AdatvaltozasokSearch.RovidNev.Value;
            //if (BoolString.Contain(_KRT_AdatvaltozasokSearch.Modosithato.Value))
            //{
            //    ddownModosithato.SelectedValue = _KRT_AdatvaltozasokSearch.Modosithato.Value;
            //}
            //else
            //{
            //    ddownModosithato.SelectedValue = BoolString.NotSet;
            //}
            //Ervenyesseg_SearchFormComponent1.SetDefault(
            //   _KRT_AdatvaltozasokSearch.ErvKezd, _KRT_AdatvaltozasokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_AdatvaltozasokSearch SetSearchObjectFromComponents()
    {
        KRT_AdatvaltozasokSearch _KRT_AdatvaltozasokSearch = (KRT_AdatvaltozasokSearch)SearchHeader1.TemplateObject;

        if (_KRT_AdatvaltozasokSearch == null)
        {
            _KRT_AdatvaltozasokSearch = new KRT_AdatvaltozasokSearch();
        }

        //if (KodcsoportokTextBox1.Id_HiddenField != "")
        //{
        //    _KRT_AdatvaltozasokSearch.KodCsoport_Id.Value = KodcsoportokTextBox1.Id_HiddenField;
        //    _KRT_AdatvaltozasokSearch.KodCsoport_Id.Operator = Query.Operators.equals;
        //}

        //_KRT_AdatvaltozasokSearch.Nev.Value = textNev.Text;
        //_KRT_AdatvaltozasokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        //_KRT_AdatvaltozasokSearch.Kod.Value = textKod.Text;
        //_KRT_AdatvaltozasokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(textKod.Text);
        //_KRT_AdatvaltozasokSearch.RovidNev.Value = textRovidNev.Text;
        //_KRT_AdatvaltozasokSearch.RovidNev.Operator = Search.GetOperatorByLikeCharater(textRovidNev.Text);
        //_KRT_AdatvaltozasokSearch.Modosithato.Value = ddownModosithato.SelectedValue;
        //if (ddownModosithato.SelectedValue != BoolString.NotSet)
        //{
        //    _KRT_AdatvaltozasokSearch.Modosithato.Operator = Query.Operators.equals;
        //}
        //else
        //{
        //    _KRT_AdatvaltozasokSearch.Modosithato.Operator = "";
        //}

        //Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
        //   _KRT_AdatvaltozasokSearch.ErvKezd, _KRT_AdatvaltozasokSearch.ErvVege);

        return _KRT_AdatvaltozasokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_AdatvaltozasokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_AdatvaltozasokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_AdatvaltozasokSearch();
    }
}
