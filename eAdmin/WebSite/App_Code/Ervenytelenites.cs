﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Contentum.eAdmin.Utility
{
    public class Ervenytelenites
    {
        /// <summary>
        /// VanFelhasznalohozKapcsoltUgy
        /// </summary>
        /// <param name="felhasznaloId"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool VanFelhasznalohozKapcsoltUgy(Page page, Contentum.eUIControls.eErrorPanel errorPanel, string felhasznaloId, out string msg)
        {
            msg = string.Empty;

            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            KRT_CsoportTagokSearch src = new KRT_CsoportTagokSearch();
            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            execParam.Record_Id = felhasznaloId;
            Result resultCheck = service.CheckKapcsoltUgyek(execParam);

            if (!resultCheck.IsError)
            {
                if (resultCheck.GetCount > 0)
                {
                    int ugyiratCount;
                    //int iratCount;
                    int iratPeldanyCount;
                    int kuldemenyCount;
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Ugyiratok_count"].ToString(), out ugyiratCount);
                    //Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Iratok_count"].ToString(), out iratCount);
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Iratpeldanyok_count"].ToString(), out iratPeldanyCount);
                    Int32.TryParse(resultCheck.Ds.Tables[0].Rows[0]["Kuldemenyek_count"].ToString(), out kuldemenyCount);
                    if ((ugyiratCount + iratPeldanyCount + kuldemenyCount) > 0)
                    {
                        var ugyiratok = ugyiratCount > 0 ? ugyiratCount + " ügyirattal" : "";
                        var iratpeldanyok = iratPeldanyCount > 0 ? iratPeldanyCount + " iratpéldánnyal" : "";
                        var kuldemenyek = kuldemenyCount > 0 ? kuldemenyCount + " küldeménnyel" : "";
                        var s = String.Join(", ", new string[] { ugyiratok, iratpeldanyok, kuldemenyek }.Where(x => !String.IsNullOrEmpty(x)).ToArray());

                        msg = string.Format("Rendelkezik a rendszerben {0}!", s);
                        return true;
                    }
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, resultCheck);
                return false;
            }
            msg = "Nem rendelkezik a rendszerben ügyirattal, iratpéldánnyal, küldeménnyel!";
            return false;
        }
    }
}