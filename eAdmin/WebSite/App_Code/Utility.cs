using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

/* Figyelem!!!!!
 * 
 * 1. Ebbe a fajlba legyszi a "kulso" fuggvenyek es egyebeket teljes namespace utvonallal irjatok be:
 * 
 *              Contentum.eBusinessDocuments.KRT_Felhasznalok , stb...

 * 2. A resource-oke itt igy hasznaljatok:
 * 
 *              Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem")
 
 * 
 * * 3. Az ASPX lapokon a Control tulajdonsagaihoz mehet igy a resource:
 * 
 *              ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"
*/

namespace Contentum.eAdmin.Utility
{
    public class eTemplateManagerService : BaseUtility.eTemplateManagerService
    { 
    }
    public class XmlFunction : BaseUtility.XmlFunction
    {
    }

    public class Search : BaseUtility.Search
    {
        public static class DefaultSearchObjects
        {
            public static KRT_LogSearch CreateSearchObjWithDefFilter_KRT_LogSearch()
            {
                KRT_LogSearch search = new KRT_LogSearch();

                // Mai napon beloggolt userekre sz�r�nk:
                search.KRT_Log_LoginSearch.Date.GreaterOrEqual(DateTime.Today.ToString());

                return search;
            }
            
            /// <summary>
            /// Esem�nyek lista default sz�r�je (mai napra sz�r)
            /// </summary>
            /// <param name="page"></param>
            /// <returns></returns>
            public static KRT_EsemenyekSearch CreateSearchObjWithDefFilter_Esemenyek()
            {
                KRT_EsemenyekSearch searchObject = new KRT_EsemenyekSearch(true);

                // default �rt�kek be�ll�t�sa:

                // BUG#7286: Timeout probl�ma miatt, alapb�l a mai napra sz�r�nk:
                DateTime dt = DateTime.Today;
                searchObject.LetrehozasIdo.GreaterOrEqual(dt.ToString());

                searchObject.ReadableWhere = String.Format("Esem�ny id�pontja >= {0}", dt.ToShortDateString());

                return searchObject;
            }
        }

        public static KRT_HelyettesitesekSearch CreateSearchObjWithDefFilter_Helyettesiteek()
        {

            KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();
            krt_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Helyettesites;
            krt_HelyettesitesekSearch.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;
            krt_HelyettesitesekSearch.HelyettesitesVege.Value = DateTime.Now.ToString();
            krt_HelyettesitesekSearch.HelyettesitesVege.Operator = Contentum.eQuery.Query.Operators.greater;

            return krt_HelyettesitesekSearch;
        }

    }

    public class eAdminService : BaseUtility.eAdminService
    {
    }

    public class UI : BaseUtility.UI
    {
        public static void SetFocus(Control control)
        {
            if (control != null)
            {
                control.Page.ClientScript.RegisterStartupScript(control.Page.GetType(), "setfocus", "var focusedcontrol = document.getElementById('" + control.ClientID + "'); focusedcontrol.focus();", true);
            }
        }
    }

    public class UIMezoObjektumErtekek : BaseUtility.UIMezoObjektumErtekek
    {
    }

    public class Log : BaseUtility.Log
    {
        public Log(Page page):base(page)
        {
        }
    }

    public class Authentication : Contentum.eAdmin.BaseUtility.Authentication
    {
    }

    public class FelhasznaloProfil : BaseUtility.FelhasznaloProfil
    {
    }

    public class FunctionRights : BaseUtility.FunctionRights
    {
    }

    public class LockManager : BaseUtility.LockManager
    {
    }

    public class JavaScripts : BaseUtility.JavaScripts
    {
    }

    public class DefaultDates : BaseUtility.DefaultDates
    {
    }

    public class CommandName : BaseUtility.CommandName
    {
    }

    public class EventArgumentConst : BaseUtility.EventArgumentConst
    {
    }

    public class Constants : BaseUtility.Constants
    {
    }

    /// <summary>
    /// URL-ekben el�fordul� param�ternevek
    /// </summary>
    public partial class QueryStringVars : BaseUtility.QueryStringVars
    {
        public const string EmailBoritekokId = "EmailBoritekokId";
    }

    public class Defaults : BaseUtility.Defaults
    {
    }

    public class ResultError : BaseUtility.ResultError
    {
    }

    public class Notify : BaseUtility.Notify
    {
    }

    public class KodTarak : BaseUtility.KodTarak
    {

        public static void RefreshKodcsoportCacheByKodcsoportId_eRecordWebSite(Page page, String krt_KodCsoportok_Id)
        {
            if (String.IsNullOrEmpty(krt_KodCsoportok_Id)) { return; }

            // Id alapj�n k�dcsoport k�dj�nak lek�r�se:
            KRT_KodCsoportokService service_kcs = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            execParam.Record_Id = krt_KodCsoportok_Id;

            Result result = service_kcs.Get(execParam);
            if (!result.IsError)
            {
                KRT_KodCsoportok kcs = (KRT_KodCsoportok) result.Record;
                RefreshKodcsoportCache_eRecordWebSite(page, kcs.Kod);
            }
        }

        /// <summary>
        /// WebService h�v�s az eRecord website fel�, ami ott majd friss�ti a lecache-elt k�dcsoportot
        /// </summary>
        /// <param name="kodcsoport_Kod"></param>
        public static void RefreshKodcsoportCache_eRecordWebSite(Page page, String kodcsoport_Kod)
        {
            if (String.IsNullOrEmpty(kodcsoport_Kod)) { return; }

            try
            {
                string eRecordWebSite_WrappedServiceUrl = UI.GetAppSetting("eRecordWebSiteUrl") + "WrappedWebService/Wrapped";

                eUtility.BusinessService bs = new Contentum.eUtility.BusinessService();
                bs.Type = "SOAP";
                bs.Url = eRecordWebSite_WrappedServiceUrl;
                bs.Authentication = "Windows";
                Contentum.eAdmin.Service.ServiceFactory sf = eUtility.eAdminService.GetServiceFactory(bs);

                KRT_KodTarakService service = sf.GetKRT_KodTarakService();

                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result =
                    service.KodCsoportChangedNotification(execParam, kodcsoport_Kod);

                //service.KodCsoportChangedNotificationAsync(execParam, kodcsoport_Kod);

                if (result.IsError)
                {
                    // Hiba, de nem dobjuk tov�bb
                }
            }
            catch
            {
                // ha esetleg nem �rhet� el a webservice, nem dobunk hib�t miatta, mehet tov�bb
            }
        }
    }

    public class UgyiratFunkcioGombsor
    {
        public const string Uj_ugyirat = "�j �gyirat";
        public const string Modositas = "M�dos�t�s";
        public const string Szignalas = "Szign�l�s";
        public const string Szignalas_vissza = "Szign�l�s vissza";
        public const string Szkontro_inditas = "Szkontr� ind�t�s";
        public const string Szkontrobol_vissza = "Szkontr�b�l vissza";
        public const string Felfuggesztes = "Felf�ggeszt�s";
        public const string Felfuggesztesbol_vissza = "Felf�ggeszt�sb�l vissza";
        public const string Tovabbitas = "Tov�bb�t�s";
        public const string Sztornozas = "Sztorn�z�s";
        public const string Ugyintezes_lezarasa = "�gyint�z�s lez�r�sa";
        public const string Ugyintezes_lezarasa_vissza = "�gyint�z�s lez�r�sa vissza";
        public const string Ugyirat_lezarasa = "�gyirat lez�r�sa";
        public const string Ugyirat_lezarasa_vissza = "�gyirat lez�r�sa vissza";
    }

    public interface ISelectableUserComponent : BaseUtility.ISelectableUserComponent
    {
    }

    public class PageView : BaseUtility.PageView
    {
        public PageView(Page ParentPage, StateBag ViewState) : base(ParentPage, ViewState)
        {
        }
    }

    public class CustomTextBox : Contentum.eUIControls.CustomTextBox
    {
    }

    public class Rendszerparameterek : Contentum.eAdmin.BaseUtility.Rendszerparameterek
    {
    }

    public class ExtraNapok
    {
        public static class Jelzo
        {
            private static ListItem itemMunkaszunet = new ListItem("Munkasz�neti nap", Contentum.eUtility.KodTarak.EXTRANAPOK.MunkaszunetiNap);
            private static ListItem itemMunakanap = new ListItem("Rendk�v�li munkanap", Contentum.eUtility.KodTarak.EXTRANAPOK.RendkivuliMunkanap);
            private static ListItem itemEmpty = new ListItem("Nincs kiv�lasztva", Contentum.eUtility.KodTarak.EXTRANAPOK.Empty);

            public static string GetTextFromValue(string value)
            {
                if (value == itemMunakanap.Value)
                    return itemMunakanap.Text;

                if (value == itemMunkaszunet.Value)
                    return itemMunkaszunet.Text;

                return Contentum.eUtility.KodTarak.EXTRANAPOK.Ervenytelen;
            }

            public static void FillListControl(ListControl listControl)
            {
                FillListControl(listControl, false);
            }

            public static void FillListControl(ListControl listControl, bool AddEmptyItem)
            {
                listControl.Items.Clear();
                if (AddEmptyItem)
                    listControl.Items.Add(itemEmpty);
                listControl.Items.Add(itemMunkaszunet);
                listControl.Items.Add(itemMunakanap);
            }
        }
    }

    public class ForditasokManager : Contentum.eUtility.ForditasokManager
    {
        public static new void ClearCache()
        {
            Contentum.eUtility.ForditasokManager.ClearCache();
            CleareRecordCache();
        }

        static void CleareRecordCache()
        {
            string url = UI.GetAppSetting("eRecordWebSiteUrl") + "RefreshCache.aspx";
            url += "?" + QueryStringVars.Command + "=RefreshForditasokCache";

            using (System.Net.WebClient wc = new System.Net.WebClient())
            {
                wc.UseDefaultCredentials = true;
                string response = wc.DownloadString(new Uri(url));
            }
        }
    }
}
