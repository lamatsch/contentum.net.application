<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AuditLogList.aspx.cs" Inherits="AuditLogList" Title="Untitled Page" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc1:ListHeader ID="ListHeader1" runat="server" />
    <uc2:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <div style="text-align:left">
    <asp:UpdatePanel runat="server" ID="UpdatePanelMain" OnLoad="UpdatePanelMain_Load">
    <ContentTemplate>
        <asp:TreeView ID="TreeView1" runat="server" ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" PopulateNodesFromClient="true">
        </asp:TreeView>
        <asp:GridView ID="treeAsGridView" runat="server" Visible="false"></asp:GridView>
    </ContentTemplate>   
    </asp:UpdatePanel>
    </div> 
</asp:Content>

