using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using System.IO;
using System.Text;
using System.Xml.Serialization;

public partial class AuditLogList : Contentum.eUtility.UI.PageBase
{
    private string PageStartDate = String.Empty;
    int columnNumber = 0;

    private const int maxLength = 1000;

    private bool isFilteredByPageStartDate
    {
        get
        {
            if (String.IsNullOrEmpty(PageStartDate))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = "Audit logok list�ja";
        ListHeader1.SearchObjectType = typeof(KRT_LogSearch);

        ListHeader1.RefreshOnClientClick = Page.ClientScript.GetPostBackEventReference(UpdatePanelMain, EventArgumentConst.refreshMasterList);

        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;

        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.SearchOnClientClick = ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("AuditLogSearch.aspx", ""
            , 900, Defaults.PopupHeight, UpdatePanelMain.ClientID, EventArgumentConst.refreshMasterList, false);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);

        PageStartDate = Request.QueryString.Get(Constants.PageStartDate);

        if (!IsPostBack)
        {
            PopulateLoginLogs();
        }
    }

    private const string errorBegin = "<span style=\"color:red \">";
    private const string errorEnd = "</span>";
    private const string warningBegin = "<span style=\"color:yellow \">";
    private const string warningEnd = "</span>";

    private string getBegin(DataRow row)
    {
        log4net.Core.Level level;
        if (Log.TryParseStringAsLevel(row["Level"].ToString(), out level))
        {
            return getBegin(level);
        }

        return String.Empty;
    }

    private string getEnd(DataRow row)
    {
        StringBuilder sb = new StringBuilder();
        string end = String.Empty;
        log4net.Core.Level level;
        if (Log.TryParseStringAsLevel(row["Level"].ToString(), out level))
        {
            if (level.CompareTo(log4net.Core.Level.Warn) >= 0)
            {
                sb.Append("; Hiba�zenet: ");
                sb.Append(row["HibaUzenet"].ToString());
                sb.Append("; Hibak�d:");
                sb.Append(row["HibaKod"].ToString());
                end = Server.HtmlEncode(sb.ToString());
                if (end.Length > maxLength)
                {
                    end = end.Substring(0, maxLength);
                }
            }

            end += getEnd(level);
        }
        return end;
    }

    private string getBegin(log4net.Core.Level level)
    {
        if (level.CompareTo(log4net.Core.Level.Error) >=0)
        {
            return errorBegin;
        }
        if (level.CompareTo(log4net.Core.Level.Warn) >= 0)
        {
            return warningBegin;
        }

        return String.Empty;
    }

    private string getEnd(log4net.Core.Level level)
    {
        if (level.CompareTo(log4net.Core.Level.Error) >= 0)
        {
            return errorEnd;
        }
        if (level.CompareTo(log4net.Core.Level.Warn) >= 0)
        {
            return warningEnd;
        }

        return String.Empty;
    }

    public string GetLogText(DataRow row)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(getBegin(row));
        sb.Append(row["Felhasznalo_Nev"].ToString());
        string MegbizasText = "";
        if (!String.IsNullOrEmpty(row["Helyettesites_Id"].ToString()))
        {
            if (!String.IsNullOrEmpty(row["HelyettesitesMod_Nev"].ToString()))
            {
                MegbizasText = " -> " + row["HelyettesitesMod_Nev"].ToString();
            }
        }

        if (!String.IsNullOrEmpty(row["Helyettesito_Nev"].ToString()))
        {
            sb.Append(" [");
            sb.Append(row["Helyettesito_Nev"].ToString());
            sb.Append(MegbizasText);
            sb.Append("]");
        }
        sb.Append(GetFullTimeString(row));
        sb.Append(getEnd(row));
        return sb.ToString();
    }

    public string GetWebServiceText(DataRow row)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(getBegin(row));
        sb.Append("[");
        sb.Append(row["Machine"].ToString());
        sb.Append("], ");
        sb.Append(row["Url"].ToString());
        sb.Append(" , ");
        sb.Append(row["Name"].ToString());
        sb.Append("/");
        sb.Append(row["Method"].ToString());
        sb.Append(GetTimeString(row));
        sb.Append(getEnd(row));
        return sb.ToString();
    }

    public string GetShortUrl(string url)
    {
        UriBuilder uri = new UriBuilder(url);
        return uri.Path;
    }

    public string GetPageText(DataRow row)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(getBegin(row));
        sb.Append(this.GetShortUrl(row["Url"].ToString()));
        sb.Append(row["QueryString"].ToString());
        sb.Append(GetTimeString(row));
        sb.Append(getEnd(row));
        //Text = Server.HtmlEncode(Text);
        return sb.ToString();
    }

    public string GetStoredProcedureText(DataRow row)
    {
        string Text = getBegin(row);
        Text += row["Name"].ToString();
        Text += GetTimeString(row);
        Text += getEnd(row);
        return Text;
    }

    public string GetTimeString(DataRow row)
    {
        string Text = String.Empty;
        if (row["StartDate"].ToString() != String.Empty)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" (");
            sb.Append(GetTime(row["StartDate"]));
            sb.Append(" - ");
            sb.Append(GetTime(row["Date"]));
            sb.Append(" = ");
            sb.Append(GetTimeSpan(row["StartDate"],row["Date"]));
            Text = sb.ToString();
        }
        else
        {
            Text = " (" + GetTime(row["Date"]);
        }
        Text += ")";
        return Text;
    }

    public string GetFullTimeString(DataRow row)
    {
        string Text = String.Empty;
        if (row["StartDate"].ToString() != String.Empty)
            Text = " (" + GetFullTime(row["StartDate"]) + " - " + GetFullTime(row["Date"]);
        else
        {
            Text = " (" + GetFullTime(row["Date"]);
        }
        Text += ")";
        return Text;
    }

    public string GetTime(object date)
    {
        string time = String.Empty;
        try
        {
            DateTime dt = (DateTime)date;
            time = dt.ToString("HH:mm:ss.fff");
        }
        catch
        {
        }

        return time;
    }

    public string GetFullTime(object date)
    {
        string time = String.Empty;
        try
        {
            DateTime dt = (DateTime)date;
            time = dt.ToString("yyyy.MM.dd HH:mm:ss.fff");
        }
        catch
        {
        }

        return time;
    }

    public const int limitSpanMilliseconds = 3000;

    public string GetTimeSpan(object startDate, object endDate)
    {
        string timeSpan = String.Empty;
        bool isAbovelLimit = false;
        try
        {
            DateTime dtStart = (DateTime)startDate;
            DateTime dtEnd = (DateTime)endDate;
            TimeSpan tsElapsed = dtEnd.Subtract(dtStart);
            if (tsElapsed.TotalMilliseconds > limitSpanMilliseconds)
                isAbovelLimit = true;
            timeSpan = tsElapsed.TotalMilliseconds.ToString() + " ms";
        }
        catch
        {
        }

        if (isAbovelLimit)
        {
            timeSpan = "<b>" + timeSpan + "</b>";
        }

        return timeSpan;
    }

    //private bool _showStart = false;
    protected bool ShowStart
    {
        get
        {
            KRT_LogSearch search = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
            return search.ShowStart;
        }
    }

    private string GetPageTranzId(TreeNode treeNode)
    {
        string[] values = treeNode.ValuePath.Split('/');
        if (values.Length > 1)
            return values[1];

        return String.Empty;
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        switch (e.Node.Depth)
        {
            case 0:
                PopulatePageLogs(e.Node);
                break;
            case 1:
                PopulateWebServiceLogs(e.Node);
                break;
            case 2:
                PopulateWebServiceOrStoredProcedureLogs(e.Node);
                break;
            default:
                PopulateWebServiceOrStoredProcedureLogs(e.Node);
                break;
        }
    }

    private void PopulateLoginLogs()
    {
        KRT_Log_LoginService service = eAdminService.ServiceFactory.GetKRT_Log_LoginService();

        KRT_LogSearch search = null;

        // Default sz�r�s kell, ha �res a session:
        if (!Search.IsSearchObjectInSession(Page, typeof(KRT_LogSearch)) && !isFilteredByPageStartDate)
        {
            search = (KRT_LogSearch)Search.DefaultSearchObjects.CreateSearchObjWithDefFilter_KRT_LogSearch();

            // sessionbe ment�s
            Search.SetSearchObject(Page, search);
        }

        search = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());

        search.KRT_Log_LoginSearch.OrderBy = " Date Desc";
        search.KRT_Log_LoginSearch.TopRow = UI.GetTopRow(Page);

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        if (isFilteredByPageStartDate)
        {
            //search.WhereByManual = " AND Id in (select LoginId from KRT_Log_Page where Id = '" + PageId + "' )";
            search.KRT_Log_PageSearch.StartDate.Value = PageStartDate;
            search.KRT_Log_PageSearch.StartDate.Operator = Query.Operators.equals;
        }


        Result res = service.GetAllWithExtension(execParam, search);

        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    TreeNode newNode = new TreeNode();

                    newNode.Value = row["Tranz_id"].ToString();
                    newNode.Text = this.GetLogText(row);

                    newNode.PopulateOnDemand = true;
                    newNode.SelectAction = TreeNodeSelectAction.Expand;
                    newNode.Expanded = false;

                    if (isFilteredByPageStartDate)
                    {
                        newNode.Expand();
                    }

                    TreeView1.Nodes.Add(newNode);

                }
            }
        }

    }

    private void PopulatePageLogs(TreeNode treeNode)
    {
        KRT_Log_PageService service = eAdminService.ServiceFactory.GetKRT_Log_PageService();
        KRT_Log_PageSearch search = (KRT_Log_PageSearch)Search.GetSearchObject(Page, new KRT_Log_PageSearch());
        KRT_LogSearch search_all = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
        if (!String.IsNullOrEmpty(search_all.Where_Log_Page))
        {
            search.WhereByManual = " AND " + search_all.Where_Log_Page;
        }
        search.Login_Tranz_id.Value = treeNode.Value;
        search.Login_Tranz_id.Operator = Query.Operators.equals;
        search.OrderBy = " Date ASC";
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (isFilteredByPageStartDate)
        {
            search.StartDate.Value = PageStartDate;
            search.StartDate.Operator = Query.Operators.equals;
        }

        if (!ShowStart)
        {
            search.Status.Value = Log.Status.Elindult;
            search.Status.Operator = Query.Operators.notequals;
            //search.StartDate.Operator = Query.Operators.notnull;
            //search.OrderBy = " StartDate ASC";
        }

        Result res = service.GetAll(execParam, search);

        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    TreeNode newNode = new TreeNode();

                    newNode.Value = row["Tranz_id"].ToString();
                    newNode.Text = GetPageText(row);

                    newNode.PopulateOnDemand = true;
                    newNode.SelectAction = TreeNodeSelectAction.Expand;
                    newNode.Expanded = false;

                    if (isFilteredByPageStartDate)
                    {
                        newNode.Expand();
                    }

                    treeNode.ChildNodes.Add(newNode);

                }
            }
        }
    }

    private void PopulateWebServiceLogs(TreeNode treeNode)
    {

        KRT_Log_WebServiceService service = eAdminService.ServiceFactory.GetKRT_Log_WebServiceService();
        KRT_Log_WebServiceSearch search = (KRT_Log_WebServiceSearch)Search.GetSearchObject(Page, new KRT_Log_WebServiceSearch());
        KRT_LogSearch search_all = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
        if (!String.IsNullOrEmpty(search_all.Where_Log_WebService))
        {
            search.WhereByManual = " AND " + search_all.Where_Log_WebService;
        }
        search.Tranz_id.Value = treeNode.Value;
        search.Tranz_id.Operator = Query.Operators.equals;
        search.ParentWS_StartDate.Operator = Query.Operators.isnull;
        search.OrderBy = " Date ASC";
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        if (!ShowStart)
        {
            search.Status.Value = Log.Status.Elindult;
            search.Status.Operator = Query.Operators.notequals;
            //search.StartDate.Operator = Query.Operators.notnull;
            //search.OrderBy = " StartDate ASC";
        }

        Result res = service.GetAll(execParam, search);

        if (String.IsNullOrEmpty(res.ErrorCode))
        {
            if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    TreeNode newNode = new TreeNode();

                    if (!String.IsNullOrEmpty(row["StartDate"].ToString()))
                        newNode.Value = GetFullTime(row["StartDate"]);
                    else
                        newNode.Value = GetFullTime(row["Date"]);
                    newNode.Text = GetWebServiceText(row);

                    newNode.PopulateOnDemand = true;
                    newNode.SelectAction = TreeNodeSelectAction.Expand;
                    newNode.Expanded = false;

                    //if (isFilteredByPageId)
                    //{
                    //    newNode.Expand();
                    //}

                    treeNode.ChildNodes.Add(newNode);

                }
            }
        }
    }

    private void PopulateWebServiceOrStoredProcedureLogs(TreeNode treeNode)
    {
        //�sszef�s�lt datatable
        DataTable table = new DataTable();
        DataColumn colId = new DataColumn("Date", typeof(DateTime));
        table.Columns.Add(colId);
        DataColumn colText = new DataColumn("Text", typeof(String));
        table.Columns.Add(colText);
        DataColumn colStartDate = new DataColumn("StartDate", typeof(DateTime));
        table.Columns.Add(colStartDate);
        DataColumn colType = new DataColumn("Type", typeof(int));
        table.Columns.Add(colType);
        DataColumn colLevel = new DataColumn("Level", typeof(String));
        table.Columns.Add(colLevel);
        //webservicek lek�r�se
        KRT_Log_WebServiceService serviceWebService = eAdminService.ServiceFactory.GetKRT_Log_WebServiceService();
        KRT_Log_WebServiceSearch searchWebService = (KRT_Log_WebServiceSearch)Search.GetSearchObject(Page, new KRT_Log_WebServiceSearch());
        searchWebService.ParentWS_StartDate.Value = treeNode.Value;
        searchWebService.ParentWS_StartDate.Operator = Query.Operators.equals;
        searchWebService.OrderBy = " Date ASC";
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string PageTranzId = this.GetPageTranzId(treeNode);

        if (!String.IsNullOrEmpty(PageTranzId))
        {
            searchWebService.Tranz_id.Value = PageTranzId;
            searchWebService.Tranz_id.Operator = Query.Operators.equals;
        }

        if (!ShowStart)
        {
            searchWebService.Status.Value = Log.Status.Elindult;
            searchWebService.Status.Operator = Query.Operators.notequals;
            //searchWebService.StartDate.Operator = Query.Operators.notnull;
            //searchWebService.OrderBy = " StartDate ASC";
        }

        Result resWebService = serviceWebService.GetAll(execParam, searchWebService);

        if (String.IsNullOrEmpty(resWebService.ErrorCode))
        {
            if (resWebService.Ds != null && resWebService.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in resWebService.Ds.Tables[0].Rows)
                {
                    DataRow newRow = table.NewRow();

                    newRow["Date"] = row["Date"];
                    newRow["Text"] = GetWebServiceText(row); ;
                    newRow["StartDate"] = row["StartDate"];
                    newRow["Level"] = row["Level"];
                    newRow["Type"] = 0;

                    table.Rows.Add(newRow);
                }
            }
        }

        //t�rolt elj�r�s lek�rdez�s
        KRT_Log_StoredProcedureService serviceSp = eAdminService.ServiceFactory.GetKRT_Log_StoredProcedureService();
        KRT_Log_StoredProcedureSearch searchSp = (KRT_Log_StoredProcedureSearch)Search.GetSearchObject(Page, new KRT_Log_StoredProcedureSearch());
        KRT_LogSearch search_all = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
        if (!String.IsNullOrEmpty(search_all.Where_Log_StoredProcedure))
        {
            searchSp.WhereByManual = " AND " + search_all.Where_Log_StoredProcedure;
        }
        searchSp.WS_StartDate.Value = treeNode.Value;
        searchSp.WS_StartDate.Operator = Query.Operators.equals;
        searchSp.OrderBy = " Date ASC";

        if (!String.IsNullOrEmpty(PageTranzId))
        {
            searchSp.Tranz_id.Value = PageTranzId;
            searchSp.Tranz_id.Operator = Query.Operators.equals;
        }

        if (!ShowStart)
        {
            searchSp.Status.Value = Log.Status.Elindult;
            searchSp.Status.Operator = Query.Operators.notequals;
            //searchSp.StartDate.Operator = Query.Operators.notnull;
            //searchSp.OrderBy = " StartDate ASC";
        }

        Result resSp = serviceSp.GetAll(execParam, searchSp);

        if (String.IsNullOrEmpty(resSp.ErrorCode))
        {
            if (resSp.Ds != null && resSp.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in resSp.Ds.Tables[0].Rows)
                {
                    DataRow newRow = table.NewRow();

                    newRow["Date"] = row["Date"];
                    newRow["Text"] = GetStoredProcedureText(row);
                    newRow["StartDate"] = row["StartDate"];
                    newRow["Level"] = row["Level"];
                    newRow["Type"] = 1;

                    table.Rows.Add(newRow);
                }
            }
        }

        //rendet�s �s hozz�ad�s a f�hoz
        if (table.Rows.Count > 0)
        {
            table.DefaultView.Sort = "Date ASC";

            foreach (DataRowView row in table.DefaultView)
            {
                TreeNode newNode = new TreeNode();
                if (!String.IsNullOrEmpty(row["StartDate"].ToString()))
                    newNode.Value = GetTime(row["StartDate"]);
                else
                    newNode.Value = GetTime(row["Date"]);
                newNode.Text = row["Text"].ToString();


                if (Int32.Parse(row["Type"].ToString()) == 0)
                {
                    newNode.PopulateOnDemand = true;
                    newNode.SelectAction = TreeNodeSelectAction.Expand;
                    newNode.Expanded = false;
                }
                else
                {
                    newNode.SelectAction = TreeNodeSelectAction.None;
                    newNode.Expanded = false;
                }

                treeNode.ChildNodes.Add(newNode);



            }
        }
    }

    protected void UpdatePanelMain_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    TreeView1.Nodes.Clear();
                    PopulateLoginLogs();
                    break;
            }
        }
    }


    #region excel export

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //LZS - Excel Export (Grid)
        //ListGrid export�l�sa Excel  file-ba, a sz�ks�ges oszlopsz�less�geket param�terk�nt �tadva.
        if (e.CommandName == CommandName.ExcelExport)
        {
            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }
            
            treeAsGridView.DataSource = populateTableForExcelExport();
            treeAsGridView.DataBind();

            ex_Export.SaveGridView_ToExcel(exParam, treeAsGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, 1.1d, browser);
        }
        
    }
    private DataTable populateTableForExcelExport()
    {
        KRT_Log_AuditLogService service = eAdminService.ServiceFactory.GetKRT_Log_AuditLogService();
        KRT_LogSearch search = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());

        if (!ShowStart)
        {
            search.KRT_Log_PageSearch.Status.Value = Log.Status.Elindult;
            search.KRT_Log_PageSearch.Status.Operator = Query.Operators.isnullornotequals;
            search.KRT_Log_StoredProcedureSearch.Status.Value = Log.Status.Elindult;
            search.KRT_Log_StoredProcedureSearch.Status.Operator = Query.Operators.isnullornotequals;
            search.KRT_Log_WebServiceSearch.Status.Value = Log.Status.Elindult;
            search.KRT_Log_WebServiceSearch.Status.Operator = Query.Operators.isnullornotequals;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result res = service.AuditLogExcelExport(execParam, search);
        DataTable auditLogTable = new DataTable();

        if (!res.IsError)
        {
            if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                auditLogTable.Columns.Add(new DataColumn("Bejelentkez�s"));
                auditLogTable.Columns.Add(new DataColumn("Oldal"));
                auditLogTable.Columns.Add(new DataColumn("Szolg�ltat�s"));
                auditLogTable.Columns.Add(new DataColumn("T�rolt elj�r�s"));

                DataRow tempRow = res.Ds.Tables[0].NewRow();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    DataRow auditLogRow = auditLogTable.NewRow();
                    
                    String loginCol = GetLogText(row);
                    auditLogRow["Bejelentkez�s"] = CutStringIntoSubstrings(loginCol, 254);

                    // Mivel az osszefesult tablaszerkezetben tobb ugyanolyan nevu oszlop volt, automatikusan szamozva lettek,
                    // a stringbuilderes fuggvenyek viszont szamozas nelkulit hasznalnak
                    tempRow.ItemArray = row.ItemArray;

                    if (!String.IsNullOrEmpty(tempRow["Name"].ToString()))
                    {
                        tempRow["Level"] = row["Level1"].ToString();
                        tempRow["StartDate"] = row["StartDate1"];
                        tempRow["Date"] = row["Date1"];
                        tempRow["HibaKod"] = row["HibaKod1"].ToString();
                        tempRow["HibaUzenet"] = row["HibaUzenet1"].ToString();
                        String pageCol = GetPageText(tempRow);
                        auditLogRow["Oldal"] = CutStringIntoSubstrings(pageCol, 254);
                    }

                    if (!String.IsNullOrEmpty(tempRow["Name1"].ToString()))
                    {
                        tempRow["Level"] = row["Level2"].ToString();
                        tempRow["StartDate"] = row["StartDate2"];
                        tempRow["Date"] = row["Date2"];
                        tempRow["HibaKod"] = row["HibaKod2"].ToString();
                        tempRow["HibaUzenet"] = row["HibaUzenet2"].ToString();
                        tempRow["Name"] = row["Name1"].ToString();
                        tempRow["Url"] = row["Url1"].ToString();
                        String serviceCol = GetWebServiceText(tempRow);
                        auditLogRow["Szolg�ltat�s"] = CutStringIntoSubstrings(serviceCol, 254);
                    }

                    if (!String.IsNullOrEmpty(tempRow["Name2"].ToString()))
                    {
                        tempRow["Level"] = row["Level3"].ToString();
                        tempRow["StartDate"] = row["StartDate3"];
                        tempRow["Date"] = row["Date3"];
                        tempRow["HibaKod"] = row["HibaKod3"].ToString();
                        tempRow["HibaUzenet"] = row["HibaUzenet3"].ToString();
                        tempRow["Name"] = row["Name2"].ToString();
                        String storedCol = GetStoredProcedureText(tempRow);
                        auditLogRow["T�rolt elj�r�s"] = CutStringIntoSubstrings(storedCol, 254);
                    }

                    auditLogTable.Rows.Add(auditLogRow);
                }
            }
        }
        else
        {
            return null;
        }
        return auditLogTable;
    }

    private string CutStringIntoSubstrings(string nodeText, int chunkSize)
    {
        //Az Excel meghal, ha 255 karakternel szelesebb egy oszlop, ezert a hosszu szovegekbe sortoreseket kell rakni
        string formattedString = "";
        while (nodeText.Length > 0)
        {
            if(nodeText.Length - 1 > chunkSize)
            {
                formattedString += nodeText.Substring(0, chunkSize);
                formattedString += Environment.NewLine;
                nodeText = nodeText.Substring(chunkSize + 1);
            }
            else
            {
                formattedString += nodeText;
                nodeText = "";
            }
        }
        return formattedString;
    }

    #endregion
}
