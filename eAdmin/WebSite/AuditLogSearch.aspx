﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AuditLogSearch.aspx.cs" Inherits="AuditLogSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum"
TagPrefix="uc5" %>     
<%@ Register Src="Component/TaroltEljarasTextBox.ascx" TagName="TaroltEljarasTextBox" TagPrefix="uc6" %> 
<%@ Register Src="Component/WebSzolgaltatasTextBox.ascx" TagName="WebSzolgaltatasTextBox" TagPrefix="uc7" %> 
<%@ Register Src="Component/WebMetodusTextBox.ascx" TagName="WebMetodusTextBox" TagPrefix="uc8" %> 
<%@ Register Src="Component/WebOldalTextBox.ascx" TagName="WebOldalTextBox" TagPrefix="uc9" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoportokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapSor">
                                    <td>
                                        <div class="LogTitle">Login adatok</div>
                                        <asp:Panel runat="server" ID="panelLogin" CssClass="LogPanel">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelFelhasznalo" runat="server" Text="Felhasználó:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc4:FelhasznaloTextBox ID="FelhasznaloTextBox" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true"></uc4:FelhasznaloTextBox>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelHelyettesito" runat="server" Text="Helyettesitõ:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo" style="padding-right:30px">
                                                        <uc4:FelhasznaloTextBox ID="FelhasznaloTextBoxHelyettesito" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true"></uc4:FelhasznaloTextBox>
                                                   </td>

                                               </tr>
                                               <tr class="urlapSor">
                                                   <td class="mrUrlapMezo" colspan="2" style="white-space:nowrap;width:100%;padding-left:30px">
                                                   </td>     
                                                   <td class="mrUrlapCaption_short">
                                                       
                                                   </td>
                                                   <td class="mrUrlapCaption" style="padding-right:30px">
                                                      <asp:CheckBox runat="server" ID="cbLoginError" Text="Csak hibásak" />
                                                   </td>
                                               </tr>
                                             </tbody>
                                        </table>  
                                        </asp:Panel>
                                    </td>   
                                </tr>
                                <tr class="urlapSor">
                                    <td>
                                        <div class="LogTitle">Oldal adatok</div>
                                        <asp:Panel runat="server" ID="panelPage" CssClass="LogPanel">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelOldalNeve" runat="server" Text="Oldal neve:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc9:WebOldalTextBox ID="WebOldalTextBox" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true" ></uc9:WebOldalTextBox>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelQueryString" runat="server" Text="QueryString:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo" style="padding-right:30px">
                                                        <asp:TextBox ID="TextBoxQueryString" runat="server" CssClass="mrUrlapInputSearch" ></asp:TextBox>
                                                   </td>
                                               </tr>
                                               <tr class="urlapSor">
                                                   <td class="mrUrlapMezo" colspan="2" style="white-space:nowrap;width:100%;padding-left:30px">
                                                   </td>     
                                                   <td class="mrUrlapCaption_short">
                                                       
                                                   </td>
                                                   <td class="mrUrlapCaption" style="padding-right:30px">
                                                      <asp:CheckBox runat="server" ID="cbPageError" Text="Csak hibásak" />
                                                   </td>
                                               </tr>
                                             </tbody>
                                        </table>  
                                        </asp:Panel>
                                    </td>   
                                </tr>
                                <tr class="urlapSor">
                                    <td>
                                        <div class="LogTitle">Webszolgáltatás</div>
                                        <asp:Panel runat="server" ID="panelWebService" CssClass="LogPanel">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelWsName" runat="server" Text="Szolgáltatás neve:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc7:WebSzolgaltatasTextBox ID="WebSzolgaltatasTextBox" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true" ></uc7:WebSzolgaltatasTextBox>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelWsMethod" runat="server" Text="Metódus neve:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo" style="padding-right:30px">
                                                        <uc8:WebMetodusTextBox ID="WebMetodusTextBox" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true" ></uc8:WebMetodusTextBox>
                                                   </td>
                                               </tr>
                                               <tr class="urlapSor">
                                                   <td class="mrUrlapMezo" colspan="2" style="white-space:nowrap;width:100%;padding-left:30px">
                                                   </td>     
                                                   <td class="mrUrlapCaption_short">
                                                       
                                                   </td>
                                                   <td class="mrUrlapCaption" style="padding-right:30px">
                                                      <asp:CheckBox runat="server" ID="cbWsError" Text="Csak hibásak" />
                                                   </td>
                                               </tr>
                                             </tbody>
                                        </table>  
                                        </asp:Panel>
                                    </td>   
                                </tr>
                                <tr class="urlapSor">
                                    <td>
                                        <div class="LogTitle">Tárolt eljárás</div>
                                        <asp:Panel runat="server" ID="panelStoredProcedure" CssClass="LogPanel">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelSpName" runat="server" Text="Tárolt eljárás neve:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo">
                                                      <uc6:TaroltEljarasTextBox ID="TaroltEljarasTextBox" runat="server" CssClass="mrUrlapInputSearch" SearchMode="true"></uc6:TaroltEljarasTextBox>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                   </td>
                                                   <td class="mrUrlapCaption" style="padding-right:30px">
                                                      <asp:CheckBox runat="server" ID="cbSpError" Text="Csak hibásak" />
                                                   </td>
                                               </tr>
                                             </tbody>
                                        </table>  
                                        </asp:Panel>
                                    </td>   
                                </tr>
                                <tr class="urlapSor">
                                    <td>
                                        <div class="LogTitle">Általános</div>
                                        <asp:Panel runat="server" ID="panelAll" CssClass="LogPanel">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                                <tr class="urlapSor">
                                                   <td class="mrUrlapMezo" colspan="4" style="white-space:nowrap;padding-left:30px">
                                                       <div style="display:inline">
                                                            <asp:Label ID="label1" runat="server" Text="Időpont:" CssClass="mrUrlapCaption"></asp:Label>
                                                            <uc5:DatumIntervallum ID="StartDate" runat="server" Validate="false" ValidateDateFormat="true"></uc5:DatumIntervallum>
                                                       </div>
                                                   </td>     
                                               </tr>
                                               <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelShowStart" runat="server" Text="Indulás mutatása:"></asp:Label>
                                                   </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:CheckBox ID="cbShowStart" runat="server" ToolTip="Az oldalhívások lefutásakor keletkező bejegyzések mellett a hívások indításakor keletkezőket is listázza"/>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                   </td>
                                                    <td class="mrUrlapMezo" style="padding-right:30px">
                                                    </td>       
                                               </tr>
                                             </tbody>
                                        </table>  
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </tbody>
                      </table>          
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
         </tbody>
     </table>               

</asp:Content>

