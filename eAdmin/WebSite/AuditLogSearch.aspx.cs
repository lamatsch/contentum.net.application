using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class AuditLogSearch : System.Web.UI.Page
{
    private Type _type = typeof(KRT_LogSearch);


    private bool GetHiba(Field HibaKod, Field HibaUzenet)
    {
        if (HibaKod.Operator == Query.Operators.notnull || HibaUzenet.Operator == Query.Operators.notnull)
        {
            return true;
        }

        return false;
    }

    private void SetHiba(Field HibaKod, Field HibaUzenet, bool searchHiba)
    {
        if (searchHiba)
        {
            HibaKod.Value = String.Empty;
            HibaKod.Operator = Query.Operators.notnull;
            HibaKod.Group = "16";
            HibaKod.GroupOperator = Query.Operators.or;

            HibaUzenet.Value = String.Empty;
            HibaUzenet.Operator = Query.Operators.notnull;
            HibaUzenet.Group = "16";
            HibaUzenet.GroupOperator = Query.Operators.or;
        }

        else
        {
            HibaKod = new Field();
            HibaUzenet = new Field();
        }
    }



    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = "Audit log keresés";

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        WebSzolgaltatasTextBox.CloneFieldId = WebMetodusTextBox.HiddenFieldId;

        if (!IsPostBack)
        {
            KRT_LogSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_LogSearch KRT_LogSearch = null;
        if (searchObject != null) KRT_LogSearch = (KRT_LogSearch)searchObject;

        if (KRT_LogSearch != null)
        {
            LoadComponentsFromLogin(KRT_LogSearch.KRT_Log_LoginSearch);
            LoadComponentsFromPage(KRT_LogSearch.KRT_Log_PageSearch);
            LoadComponentsFromWebService(KRT_LogSearch.KRT_Log_WebServiceSearch);
            LoadComponentsFromStoredProcedure(KRT_LogSearch.KRT_Log_StoredProcedureSearch);
            cbShowStart.Checked = KRT_LogSearch.ShowStart;
        }
    }

    private void LoadComponentsFromLogin(KRT_Log_LoginSearch loginSearch)
    {
        if (loginSearch != null)
        {
            FelhasznaloTextBox.Id_HiddenField = loginSearch.Felhasznalo_Id.Value;
            FelhasznaloTextBox.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            FelhasznaloTextBoxHelyettesito.Id_HiddenField = loginSearch.Helyettesito_Id.Value;
            FelhasznaloTextBoxHelyettesito.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            StartDate.SetComponentFromSearchObjectFields(loginSearch.Date);

            cbLoginError.Checked = this.GetHiba(loginSearch.HibaKod, loginSearch.HibaUzenet);
        }
    }

    private void LoadComponentsFromPage(KRT_Log_PageSearch pageSearch)
    {
        if (pageSearch != null)
        {
            WebOldalTextBox.TextBox.Text = pageSearch.Name.Value;
            TextBoxQueryString.Text = pageSearch.QueryString.Value;

            cbPageError.Checked = this.GetHiba(pageSearch.HibaKod, pageSearch.HibaUzenet);
        }
    }

    private void LoadComponentsFromWebService(KRT_Log_WebServiceSearch WsSearch)
    {
        if (WsSearch != null)
        {
            WebSzolgaltatasTextBox.TextBox.Text = WsSearch.Name.Value;
            WebMetodusTextBox.TextBox.Text = WsSearch.Method.Value;

            cbWsError.Checked = this.GetHiba(WsSearch.HibaKod, WsSearch.HibaUzenet);
        }
    }

    private void LoadComponentsFromStoredProcedure(KRT_Log_StoredProcedureSearch SpSearch)
    {
        if (SpSearch != null)
        {
            TaroltEljarasTextBox.TextBox.Text = SpSearch.Name.Value;

            cbSpError.Checked = this.GetHiba(SpSearch.HibaKod, SpSearch.HibaUzenet);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_LogSearch SetSearchObjectFromComponents()
    {
        KRT_LogSearch KRT_LogSearch = (KRT_LogSearch)SearchHeader1.TemplateObject;
        if (KRT_LogSearch == null)
        {
            KRT_LogSearch = new KRT_LogSearch();
        }

        KRT_LogSearch.KRT_Log_LoginSearch = SetSearchObjectFromLogin();
        KRT_LogSearch.KRT_Log_PageSearch = SetSearchObjectFromPage();
        KRT_LogSearch.KRT_Log_WebServiceSearch = SetSearchObjectFromWebService();
        KRT_LogSearch.KRT_Log_StoredProcedureSearch = SetSearchObjectFromStoredProcedure();
        KRT_LogSearch.ShowStart = cbShowStart.Checked;

        return KRT_LogSearch;
    }

    KRT_Log_LoginSearch SetSearchObjectFromLogin()
    {
        KRT_Log_LoginSearch loginSearch = new KRT_Log_LoginSearch();

        if (!String.IsNullOrEmpty(FelhasznaloTextBox.Id_HiddenField))
        {
            loginSearch.Felhasznalo_Id.Value = FelhasznaloTextBox.Id_HiddenField;
            loginSearch.Felhasznalo_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(FelhasznaloTextBoxHelyettesito.Id_HiddenField))
        {
            loginSearch.Helyettesito_Id.Value = FelhasznaloTextBoxHelyettesito.Id_HiddenField;
            loginSearch.Helyettesito_Id.Operator = Query.Operators.equals;
        }

        StartDate.SetSearchObjectFields(loginSearch.Date);

        this.SetHiba(loginSearch.HibaKod, loginSearch.HibaUzenet, cbLoginError.Checked);

        return loginSearch;
    }

    KRT_Log_PageSearch SetSearchObjectFromPage()
    {
        KRT_Log_PageSearch pageSearch = new KRT_Log_PageSearch();

        pageSearch.Name.Value = WebOldalTextBox.TextBox.Text;
        pageSearch.Name.Operator = Search.GetOperatorByLikeCharater(WebOldalTextBox.TextBox.Text);

        pageSearch.QueryString.Value = TextBoxQueryString.Text;
        pageSearch.QueryString.Operator = Search.GetOperatorByLikeCharater(TextBoxQueryString.Text);

        this.SetHiba(pageSearch.HibaKod, pageSearch.HibaUzenet, cbPageError.Checked);

        return pageSearch;
    }

    KRT_Log_WebServiceSearch SetSearchObjectFromWebService()
    {
        KRT_Log_WebServiceSearch WsSearch = new KRT_Log_WebServiceSearch();

        WsSearch.Name.Value = WebSzolgaltatasTextBox.TextBox.Text;
        WsSearch.Name.Operator = Search.GetOperatorByLikeCharater(WebSzolgaltatasTextBox.TextBox.Text);

        WsSearch.Method.Value = WebMetodusTextBox.TextBox.Text;
        WsSearch.Method.Operator = Search.GetOperatorByLikeCharater(WebMetodusTextBox.TextBox.Text);

        this.SetHiba(WsSearch.HibaKod, WsSearch.HibaUzenet, cbWsError.Checked);

        return WsSearch;
    }

    KRT_Log_StoredProcedureSearch SetSearchObjectFromStoredProcedure()
    {
        KRT_Log_StoredProcedureSearch SpSearch = new KRT_Log_StoredProcedureSearch();

        SpSearch.Name.Value = TaroltEljarasTextBox.TextBox.Text;
        SpSearch.Name.Operator = Search.GetOperatorByLikeCharater(TaroltEljarasTextBox.TextBox.Text);

        this.SetHiba(SpSearch.HibaKod, SpSearch.HibaUzenet, cbSpError.Checked);

        return SpSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_LogSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()) && Search.IsIdentical(searchObject.KRT_Log_LoginSearch, new KRT_Log_LoginSearch())
                && Search.IsIdentical(searchObject.KRT_Log_PageSearch, new KRT_Log_PageSearch()) && Search.IsIdentical(searchObject.KRT_Log_WebServiceSearch, new KRT_Log_WebServiceSearch())
                && Search.IsIdentical(searchObject.KRT_Log_StoredProcedureSearch, new KRT_Log_StoredProcedureSearch()) &&
                GetDefaultSearchObject().ShowStart == searchObject.ShowStart)
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            //TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_LogSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_LogSearch();
    }
}
