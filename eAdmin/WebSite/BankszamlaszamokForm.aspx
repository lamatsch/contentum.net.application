<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="BankszamlaszamokForm.aspx.cs" Inherits="BankszamlaszamokForm" Title="Bankszámlaszámok" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc3" %>
<%@ Register Src="Component/BankszamlaszamTextBox.ascx" TagName="BankszamlaszamTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,BankszamlaszamokFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelPartnerStar" runat="server" CssClass="ReqStar" Text="*" />
                                        <asp:Label ID="labelPartner" runat="server" Text="Partner:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:EditablePartnerTextBox ID="PartnerTextBox_Partner" runat="server" Validate="false"
                                            LabelRequiredIndicatorID="labelPartnerStar" WithAllCimCheckBoxVisible="false"
                                            WithKuldemeny="false" CssClass="mrUrlapInputSzeles" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelBankszamlaszamStar" runat="server" CssClass="ReqStar" Text="*" />
                                        <asp:Label ID="labelBankszamlaszam" runat="server" Text="Bankszámlaszám:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:BankszamlaszamTextBox ID="BankszamlaszamTextBox1" runat="server" Validate="false"
                                            LabelRequiredIndicatorID="labelBankszamlaszamStar" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelBankStar" runat="server" CssClass="ReqStar" Text="*" />
                                        <asp:Label ID="labelBank" runat="server" Text="Bank:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:EditablePartnerTextBox ID="PartnerTextBox_Bank" runat="server" Validate="false"
                                            LabelRequiredIndicatorID="labelBankStar" WithAllCimCheckBoxVisible="false" CssClass="mrUrlapInputSzeles" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelErvenyessegStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" Enabled="false" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                    <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
            </tr>
        </tbody>
    </table>
</asp:Content>
