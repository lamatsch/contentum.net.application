<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="BankszamlaszamokSearch.aspx.cs" Inherits="BankszamlaszamokSearch" %>

<%@ Register Src="Component/MuveletTextBox.ascx" TagName="MuveletTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc4" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc7" %>
<%@ Register Src="Component/BankszamlaszamTextBox.ascx" TagName="BankszamlaszamTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,BankszamlaszamokSearchHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartner" runat="server" Text="Partner:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:EditablePartnerTextBox ID="PartnerTextBox_Partner" runat="server" Validate="false"
                                    SearchMode="true" WithAllCimCheckBoxVisible="false" CssClass="mrUrlapInputSzeles" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBankszamlaszam" runat="server" Text="Bankszámlaszám:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:BankszamlaszamTextBox ID="BankszamlaszamTextBox1" runat="server" Validate="false"
                                    SearchMode="true" Mode="Search" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBank" runat="server" Text="Bank:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:EditablePartnerTextBox ID="PartnerTextBox_Bank" runat="server" Validate="false"
                                    SearchMode="true" WithAllCimCheckBoxVisible="false" CssClass="mrUrlapInputSzeles" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2">
                                <uc4:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc7:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
