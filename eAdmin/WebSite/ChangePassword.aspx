<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><asp:Literal  runat="server" ID="LoginTitle" Text="<%$Resources:Form,LoginPageTitle%>"></asp:Literal></title>
    <base target="_self" />
</head>
<body>
    <script language="JavaScript" type="text/javascript">

    function swapByName(obj, newImageName) {
    var myobj = document.getElementById(obj); 
    if (myobj) 
    {
        var fileNameStartIndex = myobj.src.lastIndexOf("/");
        var newUrl = myobj.src.substring(0,fileNameStartIndex+1)+newImageName;
        
        myobj.src = newUrl;    
    }
    }
    </script>
    
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="ScriptManager1" runat="server" 
        EnableScriptGlobalization="True"
        EnableScriptLocalization="true" >
        </asp:ScriptManager>
        <center>
            
        <table style="WIDTH: 400px; HEIGHT: 400px">            
            <tbody>
                
                <tr>                    
                    <td valign=middle align=center>
                        <h3 id="PasswordExpired" runat="server" style="color:red">A jelszava lej�rt ez�rt meg kell v�ltoztatnia!</h3>
                        <eUI:eFormPanel id="EFormPanel1" runat="server">
                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                        <ContentTemplate>
                        <table id="table_baseLogin" runat="server">
                            <tbody>                                
                                <tr>
                                    <td style="TEXT-ALIGN: right">
                                        <asp:Label id="OldPassword" runat="server" Text="R�gi jelsz�:"></asp:Label></td>
                                    <td style="TEXT-ALIGN: left">
                                        <asp:TextBox id="OldPasswordText" runat="server" Enabled="true" TextMode="Password" style="width:250px"></asp:TextBox>
                                    <span style="display:block">  <asp:RequiredFieldValidator style="display:block;" ControlToValidate="PasswordConfirmText" runat ="server" ID="rfvOldPassword" ErrorMessage="A r�gi jelsz� mez� nincs kit�ltve"  Display="Dynamic"/></span>
                                    </td>

                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="TEXT-ALIGN: right">
                                        <asp:Label id="PasswordLabel" runat="server" Text="Jelsz�:"></asp:Label></td>
                                    <td style="TEXT-ALIGN: left">
                                        <asp:TextBox id="Password" runat="server" Enabled="true" TextMode="Password" style="width:250px"></asp:TextBox>
                                  <span style="display:block;"> <asp:RegularExpressionValidator ID="regexpPassword" runat="server"   style="display:block;"
                                    ErrorMessage="A jelsz� nem el�g er�s." 
                                    ControlToValidate="Password"     
                                    ValidationExpression= "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.+[!@#$%^&*]).{8,}"  Display="Dynamic"/></span>  
                                    <span style="display:block">  <asp:RequiredFieldValidator style="display:block;" ControlToValidate="Password" runat ="server" ID="rfvPassword" ErrorMessage="A jelsz� mez� nincs kit�ltve"  Display="Dynamic"/></span>
                                        </td>
                                    <td>
                                        <asp:ImageButton ID="InfoImage" runat="server"  title= "Er�s jelsz� krit�riumai:" ImageUrl="~/images/hu/egyeb/info_icon.png"  Visible="true" OnClientClick="return false;" Text= "Hello World" />
                                    </ItemTemplate>
                                </td>                           
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="TEXT-ALIGN: right">
                                        <asp:Label id="PasswordConfirm" runat="server" Text="Jelsz� meger�s�t�s:"></asp:Label></td>
                                    <td style="TEXT-ALIGN: left">
                                        <asp:TextBox id="PasswordConfirmText" runat="server" Enabled="true" TextMode="Password" style="width:250px"></asp:TextBox>
                                    <span style="display:block">  <asp:RequiredFieldValidator style="display:block;" ControlToValidate="PasswordConfirmText" runat ="server" ID="rfv" ErrorMessage="A jelsz� mez� nincs kit�ltve"  Display="Dynamic"/></span>
                                     <asp:CompareValidator id="cvComparePasswords" 
                                    runat="server"
                                    ControlToCompare="Password"
                                    ControlToValidate="PasswordConfirmText"
                                    ErrorMessage="A jelsz� �s a jelsz� meger�s�t�se mez�k k�l�nb�znek."
                                    Display="Dynamic" />
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>                         
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        <table width="100%">
                            <tbody>
                                <tr >
                                    <td style="TEXT-ALIGN: right" >
                                        <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="~/images/hu/ovalgomb/mentes.gif" onmouseover="swapByName(this.id,'mentes2.gif')" onmouseout="swapByName(this.id,'mentes.gif')"
                                        OnClick="SaveButton_Click"
                                        AlternateText="Jelsz� modos�t�s"/>
                                     </td>
                                </tr>
                            </tbody>
                        </table>
                        </eUI:eFormPanel> 
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </div>
    </form>
</body>
</html>
