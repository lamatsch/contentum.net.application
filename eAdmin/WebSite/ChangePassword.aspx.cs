using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

public partial class ChangePassword : Contentum.eUtility.UI.PageBase
{
    private string PASSWORD_POLICY_REGEXP = "";
    private string PASSWORD_POLICY = "";
    private string PASSWORD_LENGTH = "";
    private void RegisterJavaScripts()
    {        
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "password_hndlr", js_AddHandler, true);
    }
    private string Uid { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        string fromlogin = Request.QueryString.Get("fromlogin");        
        InfoImage.ToolTip = "Az er�s jelsz� krit�riumai:" + Environment.NewLine + "� Tartalmaznia kell legal�bb egy nagybet�t" + Environment.NewLine + "� Tartalmaznia kell legal�bb egy kisbet�t" + Environment.NewLine + "� Tartalmaznia kell legal�bb egy sz�mot " + Environment.NewLine + "� Tartalmaznia kell legal�bb egy speci�lis karaktert" + Environment.NewLine + "� Nem tartalmazhatja a felhaszn�l� nevet.";
        Uid = Request.QueryString.Get(Constants.LoginUserId);
        Response.CacheControl = "no-cache";

        if (fromlogin != "1")
        {
            PasswordExpired.Visible = false;
        }
        
        ExecParam ep = UI.SetExecParamDefault(Page);
        // Ha nincs meg a szervezet
        if (string.IsNullOrEmpty(ep.FelhasznaloSzervezet_Id)) {
        KRT_FelhasznalokService svcFelh = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ep.Record_Id = Uid;
        Result result = svcFelh.Get(ep);
        var krt_felhasznalok = result.Record as KRT_Felhasznalok;
        ep.Record_Id = "";        
        ep.FelhasznaloSzervezet_Id = krt_felhasznalok.Org;
        ep.Felhasznalo_Id = Uid;
        }

        PASSWORD_POLICY_REGEXP = Rendszerparameterek.Get(ep, "PASSWORD_POLICY_REGEXP");
        PASSWORD_POLICY = Rendszerparameterek.Get(ep, "PASSWORD_POLICY");
        PASSWORD_LENGTH = Rendszerparameterek.Get(ep, "PASSWORD_POLICY_PASS_LENGTH");
        if (!string.IsNullOrEmpty(PASSWORD_POLICY_REGEXP) && !string.IsNullOrEmpty("PASSWORD_POLICY_PASS_LENGTH"))
        {
            PASSWORD_POLICY_REGEXP = PASSWORD_POLICY_REGEXP.Replace("PASSLENGTH", PASSWORD_LENGTH);
        }

        if (PASSWORD_POLICY.Equals("STRICT")) { 
        regexpPassword.ValidationExpression = PASSWORD_POLICY_REGEXP;
        }
        else
        {
            regexpPassword.ValidationExpression = "";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterJavaScripts();        
    }    
    

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        if (PASSWORD_POLICY.Equals("STRICT"))
        {
            System.Text.RegularExpressions.Regex regexp = new System.Text.RegularExpressions.Regex(PASSWORD_POLICY_REGEXP);        
            if (!regexp.IsMatch(Password.Text))
            {
                  UI.RedirectToErrorPageByErrorMsg(Page, "A jelsz� nem felel meg az er�s jelsz� krit�riumoknak.");
            }
        }
        else if(Password.Text != PasswordConfirmText.Text)
        {
            UI.RedirectToErrorPageByErrorMsg(Page, "A jelsz� �s a jelsz� meger�s�t�se nem egyezik.");
        }
        if (string.IsNullOrEmpty(UI.GetSession(Page,"LoginUserId")))
        { 
        Session[Constants.LoginUserId] = Uid;
        }
        Session[Constants.LoginType] = "Basic";
        Session[Constants.OldPassword] = OldPasswordText.Text;
        Session[Constants.Password] = Password.Text;
        Session[Constants.PasswordConfirm] = PasswordConfirmText.Text;
        
        string remoteAddr = Page.Request.ServerVariables["remote_addr"];
        try
        {
            Session[Constants.Munkaallomas] = System.Net.Dns.GetHostEntry(remoteAddr).HostName;
        }
        catch
        {
            Session[Constants.Munkaallomas] = remoteAddr;
        }

        Authentication.ChangePassword(Page);
        
    }
    
}
