<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CimekForm.aspx.cs" Inherits="PartnercimekForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %> 
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc7" %>    
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc7" %>  
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnercimekFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <%--BLG_288--%>
    <script type="text/javascript">
        function Validate_PostafiokSzam_IsEmpty(questionTxt)
        {
            var ret = true;
            var tipus = document.getElementById('ctl00_ContentPlaceHolder1_KodtarakDropDownListTipus_Kodtarak_DropDownList').value;
            if (document.getElementById('ctl00_ContentPlaceHolder1_RequiredNumberBoxPostafiok_TextBox1') != null)
            {
                var hazszam = document.getElementById('ctl00_ContentPlaceHolder1_RequiredNumberBoxPostafiok_TextBox1').value;

                if ((tipus == '02') && (hazszam == '')) {
                    ret = (confirm(questionTxt));
                }
            }
            return ret;
        }

        // BUG_1347
        function Validate_Hrsz(alertTxt) {
            var ret = true;
            var tipus = document.getElementById('ctl00_ContentPlaceHolder1_KodtarakDropDownListTipus_Kodtarak_DropDownList').value;
            if (document.getElementById('ctl00_ContentPlaceHolder1_textHelyrajziSzam') != null) {
                var hrsz = document.getElementById('ctl00_ContentPlaceHolder1_textHelyrajziSzam').value;
                var koztertipus = document.getElementById('ctl00_ContentPlaceHolder1_KozteruletTipusTextBox1_KozteruletTipusNev_TextBox').value;
                var kozter = document.getElementById('ctl00_ContentPlaceHolder1_KozteruletTextBox1_KozteruletNev_TextBox').value;
                if ((tipus == '01') && (hrsz == '') && ((koztertipus == '') || (kozter == ''))) {
                    alert(alertTxt);
                    ret = false;
                }
            }
            return ret;
        }
    </script>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                    <tr class="urlapSor">
                                    <td colspan="2" style="text-align:center">
                                    <div>
                                        <span class="mrUrlapCaption" style="bottom:4px;position:relative">
                                            <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label>
                                        </span>
                                        <span class="mrUrlapMezo">
                                            <uc12:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server" />
                                         </span>
                                    </div>    
                                    </td>    
                                </tr>
                            </tbody>
                        </table>
                        <hr />
                        <asp:Panel ID="PostaiCim_Panel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                                <td class="mrUrlapCaption">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapMezo">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td>
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapCaption">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapMezo">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:OrszagTextBox ID="OrszagTextBox1" runat="server" CssClass="mrUrlapInputSearch" Validate="true"/>                                        
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label11" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelTelepules" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc7:TelepulesTextBox ID="TelepulesTextBox1" runat="server" CssClass="mrUrlapInputSearch"/>                                        
                                        </td>
                                        
                                    </tr>                                      
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIranyitoszamStar" runat="server" CssClass="ReqStar" Text="*" />
                                            <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc13:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" CssClass="mrUrlapInputSearch" Number="false" Validate="false" LabelRequiredIndicatorID="labelIranyitoszamStar" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <%--BUG_1347--%>
                                           <%-- <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%>
                                            <asp:Label ID="labelKozterulet" runat="server" Text="K�zter�let neve:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc9:KozteruletTextBox ID="KozteruletTextBox1" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />                                        
                                        </td>
                                    </tr>                                                                                                                                            
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <%--BUG_1347--%>
                                            <%--<asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%>
                                            <asp:Label ID="labelKozteruletTipus" runat="server" Text="K�zter�let tipusa:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" CssClass="mrUrlapInputSearch" Validate="false"/>                                        
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelHazszam" runat="server" Text="H�zsz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc8:RequiredNumberBox ID="RequiredNumberBoxHsz" runat="server" Validate="false" FilterType="NumbersWithDot"/>
                                            <span style="position:relative;top:-3px;padding-left:9px;padding-right:9px;">&#8212;</span>
                                            <uc8:RequiredNumberBox ID="RequiredNumberBoxHszIg" runat="server" Validate="false" FilterType="NumbersWithDot"/>
                                        </td>
                                    </tr>                                    
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelEpulet" runat="server" Text="�p�let:" ></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textEpulet" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelLepcsohaz" runat="server" Text="L�pcs�h�z:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textLepcsohaz" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelEmelet" runat="server" Text="Emelet:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textEmelet" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAjto" runat="server" Text="Ajt�:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAjto" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAjtoBetujel" runat="server" Text="Ajt� bet�jele:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAjtoBetujel" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                        </td>
                                        <td class="mrUrlapMezo">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                        <hr />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelMindketOldal" runat="server" Text="Mindk�t oldal:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rbListMindketOldal" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputSearchRadioButtonList">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelHelyrajziSzam" runat="server" Text="Helyrajzi sz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textHelyrajziSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelEgyeb" runat="server" Text="Egy�b adatok:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo" colspan="4">
                                            <asp:TextBox ID="textEgyeb" runat="server" CssClass="mrUrlapInputFull" Rows="3" Height="30"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <!-- Egy�b c�mek -->
                        <hr id="EgyebCimek_hr" runat="server" visible="false" />
                            <asp:Panel ID="PostafiokPanel" runat="server" Visible="false">
                            <asp:Panel ID="PostafiokEgybenPanel" runat="server" Visible="false">
                                <table cellspacing="0" cellpadding="0" style="padding-right:155px;">
                                    <tbody>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="labelPostafiokCime" runat="server" Text="Postafi�k c�me:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <uc5:RequiredTextBox ID="RequiredTextBoxPostafiokEgyben" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Validate="false"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="PostafiokDividedPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" style="padding-right:130px;">
                                    <tbody>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                <asp:Label ID="labelPostafiokIrsz" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <uc13:IranyitoszamTextBox ID="IranyitoszamTextBoxPostafiok" runat="server" CssClass="mrUrlapInput" Number="false"/>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label7" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                <asp:Label ID="labelTelepulesPostafiok" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <uc7:TelepulesTextBox ID="TelepulesTextBoxPostafiok" runat="server" CssClass="mrUrlapInput"/>                                        
                                            </td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <%--BLG_288--%>
                                                <%--  <asp:Label ID="Label8" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%>
                                                <asp:Label ID="labelPostafiokSzama" runat="server" Text="Postafi�k sz�ma:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                 <%--BLG_288--%>
                                               <%--<uc8:RequiredNumberBox ID="RequiredNumberBoxPostafiok" runat="server" Validate="true" FilterType="NumbersWithSlashDot"/>--%>
                                               <uc8:RequiredNumberBox ID="RequiredNumberBoxPostafiok" runat="server" Validate="false" FilterType="NumbersWithSlashDot"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="EgyebCimek_Panel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr class="urlapSor">
                                        <td colspan="2" style="text-align:center;">
                                        <div id="divEgyeb" runat="server">
                                            <span class="mrUrlapCaption" style="bottom:4px;position:relative">
                                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                <asp:Label ID="labelEgyebCim" runat="server" Text="C�m:"></asp:Label>
                                            </span>
                                            <span class="mrUrlapMezo">
                                                <uc5:RequiredTextBox ID="textEgyebCim" runat="server" CssClass="mrUrlapInput" />
                                            </span>
                                        </div>    
                                        </td>    
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                        <br />
                        <table cellspacing="0" cellpadding="0" style="padding-right:85px;">
                            <tbody>
                                <tr class="urlapSor">
                                    <td colspan="2" style="text-align:center">
                                    <div>
                                        <span class="mrUrlapCaption" style="bottom:4px;position:relative">
                                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                        </span>
                                        <span class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                        </span>
                                    </div>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
            </eUI:eFormPanel>
             <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
             <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
           </tr>
        </tbody>
     </table>
</asp:Content>

