using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Text;

public partial class PartnercimekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private string filterType = null;

    private const string kodcsoportCim = "CIM_TIPUS";
    private readonly string jsModifyAlert = "alert('" + Resources.Form.CimekModifyWarning +"');";

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");
        public static void FillRadioList(RadioButtonList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillRadioList(RadioButtonList list)
        {
            FillRadioList(list, No.Value);
        }
        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            if (selectedValue == Yes.Value || selectedValue == No.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(KRT_Cimek krt_cimek)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (krt_cimek.Tipus)
            {
                case KodTarak.Cim_Tipus.Postai:
                    cim.Add(krt_cimek.OrszagNev, delimeter);
                    cim.Add(krt_cimek.IRSZ, delimeter);
                    cim.Add(krt_cimek.TelepulesNev, delimeter);
                   // BLG_1347
                    //cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                    //cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                    if (String.IsNullOrEmpty(krt_cimek.KozteruletNev) && !String.IsNullOrEmpty(krt_cimek.HRSZ))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(krt_cimek.HRSZ, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                        cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                    string hazszam = krt_cimek.Hazszam;
                    string hazszamIg = krt_cimek.Hazszamig;
                    string hazszamBetujel = krt_cimek.HazszamBetujel;
                    if (!String.IsNullOrEmpty(hazszamIg))
                        hazszam += "-" + hazszamIg;
                    if (!String.IsNullOrEmpty(hazszamBetujel))
                        hazszam += "/" + hazszamBetujel;
                    cim.Add(hazszam, delimeter);
                    string lepcsohaz = krt_cimek.Lepcsohaz;
                    if (!String.IsNullOrEmpty(lepcsohaz))
                        lepcsohaz += " l�pcs�h�z";
                    cim.Add(lepcsohaz, delimeter);
                    string szint = krt_cimek.Szint;
                    if (!String.IsNullOrEmpty(szint))
                        szint += ". emelet";
                    cim.Add(szint, delimeter);
                    string ajto = krt_cimek.Ajto;
                    if (!String.IsNullOrEmpty(ajto))
                    {
                        string ajtoBetujel = krt_cimek.AjtoBetujel;
                        if (!String.IsNullOrEmpty(ajtoBetujel))
                            ajto += "/" + ajtoBetujel;
                        ajto += " ajt�";
                    }
                    cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Postafiok:
                    cim.Add(krt_cimek.IRSZ, delimeterSpace);
                    cim.Add(krt_cimek.TelepulesNev, delimeter);
                    // BLG_288
                    if (!String.IsNullOrEmpty(krt_cimek.Hazszam))
                    {
                        cim.Add("Pf.:", delimeterSpace);
                        cim.Add(krt_cimek.Hazszam, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = krt_cimek.CimTobbi;
                    cim.Add(Cim, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception)
        {
            return "";
        }

        return text.ToString();
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        KodtarakDropDownListTipus.ReadOnly = true;
        OrszagTextBox1.ReadOnly = true;
        TelepulesTextBox1.ReadOnly = true;
        IranyitoszamTextBox1.ReadOnly = true;
        KozteruletTextBox1.ReadOnly = true;
        KozteruletTipusTextBox1.ReadOnly = true;
        RequiredNumberBoxHsz.ReadOnly = true;
        textEpulet.ReadOnly = true;
        RequiredNumberBoxHszIg.ReadOnly = true;
        rbListMindketOldal.Enabled = false;
        textEmelet.ReadOnly = true;
        textLepcsohaz.ReadOnly = true;
        textAjto.ReadOnly = true;
        textAjtoBetujel.ReadOnly = true;
        textHelyrajziSzam.ReadOnly = true;
        textEgyeb.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
        textEgyebCim.ReadOnly = true;
        IranyitoszamTextBoxPostafiok.ReadOnly = true;
        TelepulesTextBoxPostafiok.ReadOnly = true;
        RequiredNumberBoxPostafiok.ReadOnly = true;

        labelTipus.CssClass = "mrUrlapInputWaterMarked";
        labelOrszag.CssClass = "mrUrlapInputWaterMarked";
        labelTelepules.CssClass = "mrUrlapInputWaterMarked";
        labelIranyitoszam.CssClass = "mrUrlapInputWaterMarked";
        labelKozterulet.CssClass = "mrUrlapInputWaterMarked";
        labelKozteruletTipus.CssClass = "mrUrlapInputWaterMarked";
        labelHazszam.CssClass = "mrUrlapInputWaterMarked";
        labelEpulet.CssClass = "mrUrlapInputWaterMarked";
        labelMindketOldal.CssClass = "mrUrlapInputWaterMarked";
        labelEmelet.CssClass = "mrUrlapInputWaterMarked";
        labelLepcsohaz.CssClass = "mrUrlapInputWaterMarked";
        labelAjto.CssClass = "mrUrlapInputWaterMarked";
        labelAjtoBetujel.CssClass = "mrUrlapInputWaterMarked";
        labelHelyrajziSzam.CssClass = "mrUrlapInputWaterMarked";
        labelEgyeb.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelEgyebCim.CssClass = "mrUrlapInputWaterMarked";
        labelPostafiokIrsz.CssClass = "mrUrlapInputWaterMarked";
        labelTelepulesPostafiok.CssClass = "mrUrlapInputWaterMarked";
        labelPostafiokSzama.CssClass = "mrUrlapInputWaterMarked";

    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        KodtarakDropDownListTipus.ReadOnly = true;

        labelTipus.CssClass = "mrUrlapInputWaterMarked";

    }

    private int GetPartnerekCountByCim(KRT_Cimek krt_Cimek)
    {
        if (Command == CommandName.Modify)
        {
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            search.Cim_Id.Value = krt_Cimek.Id;
            search.Cim_Id.Operator = Query.Operators.equals;
            Result result = service.GetAll(execParam, search);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                return result.Ds.Tables[0].Rows.Count;
            }

        }
        return 0;
    }

    /// <summary>
    /// A leg�rd�l� list�ban kiv�lasztott �rt�knek megfelel� panel aktiv�l�sa
    /// </summary>
    private void SetActivePanel()
    {
        IranyitoszamTextBox1.Validate = false;

        if (Command == CommandName.DesignView)
        {
            PostaiCim_Panel.Visible = true;
            EgyebCimek_Panel.Visible = true;
            EgyebCimek_hr.Visible = true;
            labelEgyebCim.Text = "C�m/Sz�m:";
            HtmlControl divEgyeb = (HtmlControl)EgyebCimek_Panel.FindControl("divEgyeb");
            divEgyeb.Style.Add(HtmlTextWriterStyle.MarginLeft, "-25px");
            BoolString.FillRadioList(rbListMindketOldal);
            rbListMindketOldal.SelectedItem.Selected = false;
        }
        else
        {
            switch (KodtarakDropDownListTipus.SelectedValue)
            {
                case KodTarak.Cim_Tipus.Postai:
                    PostaiCim_Panel.Visible = true;
                    PostafiokPanel.Visible = false;
                    EgyebCimek_Panel.Visible = false;
                    IranyitoszamTextBox1.Validate = true; // itt k�telez� az ir�ny�t�sz�m
                    break;

                case KodTarak.Cim_Tipus.Postafiok:
                    PostaiCim_Panel.Visible = false;
                    PostafiokPanel.Visible = true;
                    EgyebCimek_Panel.Visible = false;
                    break;

                case KodTarak.Cim_Tipus.Email:
                case KodTarak.Cim_Tipus.Web:
                    {
                        labelEgyebCim.Text = "C�m:";
                        HtmlControl divEgyeb = (HtmlControl)EgyebCimek_Panel.FindControl("divEgyeb");
                        divEgyeb.Style.Add(HtmlTextWriterStyle.MarginLeft, "8px");
                        goto case KodTarak.Cim_Tipus.Egyeb;
                    }

                case KodTarak.Cim_Tipus.Telefon:
                case KodTarak.Cim_Tipus.Fax:
                    {
                        labelEgyebCim.Text = "Sz�m:";
                        HtmlControl divEgyeb = (HtmlControl)EgyebCimek_Panel.FindControl("divEgyeb");
                        divEgyeb.Style.Add(HtmlTextWriterStyle.MarginLeft, "-1px");
                        goto case KodTarak.Cim_Tipus.Egyeb;
                    }


                case KodTarak.Cim_Tipus.Egyeb:
                    PostaiCim_Panel.Visible = false;
                    PostafiokPanel.Visible = false;
                    EgyebCimek_Panel.Visible = true;
                    break;

                default:
                    labelEgyebCim.Text = "C�m";
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }
    }

    private void RemoveDropDownListItems(DropDownList ddlist, string filter, bool equals)
    {
        ListItemCollection removableItmes = new ListItemCollection();
        foreach (ListItem item in ddlist.Items)
        {
            if (equals)
            {
                if (item.Value == filter)
                {
                    removableItmes.Add(item);
                }
            }
            else
            {
                if (item.Value != filter)
                {
                    removableItmes.Add(item);
                }
            }
        }
        if (removableItmes.Count > 0)
        {
            foreach (ListItem item in removableItmes)
            {
                ddlist.Items.Remove(item);
            }
        }
    }

    private void RemoveDropDownListItems(bool equals)
    {
        RemoveDropDownListItems(KodtarakDropDownListTipus.DropDownList, KodTarak.Cim_Tipus.Postai, equals);
    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Cim" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Cimek krt_Cimek = (KRT_Cimek)result.Record;
                    LoadComponentsFromBusinessObject(krt_Cimek);
                    if (!IsPostBack)
                    {
                        int partnerekCount = GetPartnerekCountByCim(krt_Cimek);
                        if (partnerekCount > 0)
                        {
                            string script = jsModifyAlert.Replace("{partnerekCount}", partnerekCount.ToString());
                            ClientScript.RegisterStartupScript(Page.GetType(), "modifyAlert", script, true);
                        }
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

        }
        else if (Command == CommandName.New)
        {
            //drop down list�k felt�lt�s �rt�kekkel
            string cimTipus = (string)(Request.QueryString[QueryStringVars.CimTipusKod] ?? KodTarak.Cim_Tipus.Default);
            KodtarakDropDownListTipus.FillAndSetSelectedValue(kodcsoportCim, cimTipus, FormHeader1.ErrorPanel);
            BoolString.FillRadioList(rbListMindketOldal);

            //a lovlist kiv�lasztott eleme alapj�n a form felt�lt�se
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (!String.IsNullOrEmpty(id))
            {
                KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Cimek krt_Cimek = (KRT_Cimek)result.Record;
                    LoadComponentsFromBusinessObject(krt_Cimek);
                    KodtarakDropDownListTipus.FillAndSetSelectedValue(kodcsoportCim, krt_Cimek.Tipus, FormHeader1.ErrorPanel);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
            else
            {
                // BUG_8613
                OrszagTextBox1.SetDefault();
            }

            //sz�r�s
            filterType = Request.QueryString.Get(QueryStringVars.Filter);
            if (filterType != null)
            {
                switch (filterType)
                {
                    case KodTarak.Cim_Tipus.Postai:
                        FormHeader1.HeaderTitle = Resources.Form.PartnercimekFormHeaderTitle_Filtered_Postai;
                        KodtarakDropDownListTipus.SetSelectedValue(KodTarak.Cim_Tipus.Postai);
                        RemoveDropDownListItems(false);
                        break;
                    case KodTarak.Cim_Tipus.Egyeb:
                        FormHeader1.HeaderTitle = Resources.Form.PartnercimekFormHeaderTitle_Filtered_Egyeb;
                        KodtarakDropDownListTipus.SetSelectedValue(KodTarak.Cim_Tipus.EgyebDefault);
                        RemoveDropDownListItems(true);
                        break;
                    default:
                        goto case KodTarak.Cim_Tipus.Egyeb;
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {

            SetModifyControls();
        }

        SetActivePanel();

        //registerJavascripts();

        //Ir�ny�t�sz�m textbox id-j�nak �tad�sa a telep�l�s lovlist�nak
        TelepulesTextBox1.IrszTextBoxClientID = IranyitoszamTextBox1.TextBox.ClientID;
        //Ir�ny�t�sz�m textbox id-j�nak �tad�sa a telep�l�s lovlist�nak
        TelepulesTextBoxPostafiok.IrszTextBoxClientID = IranyitoszamTextBoxPostafiok.TextBox.ClientID;
        
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        // BLG_288
        if (!IsPostBack)
        {  
            string jsConfirm = "if (!Validate_PostafiokSzam_IsEmpty('" + Contentum.eUtility.Resources.Question.GetString("UIPostafiokSzamIsEmpty") + "')) { return false;} " +
                               "if (!Validate_Hrsz('" + Contentum.eUtility.Resources.Error.GetString("UIHrszCimError") + "')){return false;}";
            FormFooter1.ImageButton_Save.OnClientClick = jsConfirm;
        }
        FormFooter1.ButtonsClick += new
          System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        KodtarakDropDownListTipus.DropDownList.AutoPostBack = true;
        KodtarakDropDownListTipus.DropDownList.SelectedIndexChanged += new EventHandler(KodtarakDropDownListTipus_SelectedIndexChanged);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        registerJavascripts();
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Cimek krt_Cimek)
    {
        switch (krt_Cimek.Tipus)
        {
            case KodTarak.Cim_Tipus.Postai:
                KodtarakDropDownListTipus.FillWithOneValue(kodcsoportCim, krt_Cimek.Tipus, FormHeader1.ErrorPanel);
                // BUG_8613
                //OrszagTextBox1.Text = krt_Cimek.OrszagNev;
                OrszagTextBox1.Id_HiddenField = krt_Cimek.Orszag_Id;
                OrszagTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

                TelepulesTextBox1.Text = krt_Cimek.TelepulesNev;
                IranyitoszamTextBox1.Text = krt_Cimek.IRSZ;
                KozteruletTextBox1.Text = krt_Cimek.KozteruletNev;
                KozteruletTipusTextBox1.Text = krt_Cimek.KozteruletTipusNev;
                RequiredNumberBoxHsz.Text = krt_Cimek.Hazszam;
                textEpulet.Text = krt_Cimek.HazszamBetujel;
                BoolString.FillRadioList(rbListMindketOldal,krt_Cimek.MindketOldal);
                textEmelet.Text = krt_Cimek.Szint;
                textLepcsohaz.Text = krt_Cimek.Lepcsohaz;
                textAjto.Text = krt_Cimek.Ajto;
                textAjtoBetujel.Text = krt_Cimek.AjtoBetujel;
                textHelyrajziSzam.Text = krt_Cimek.HRSZ;
                textEgyeb.Text = krt_Cimek.Tobbi;
                RequiredNumberBoxHszIg.Text = krt_Cimek.Hazszamig;
                break;

            case KodTarak.Cim_Tipus.Postafiok:
                KodtarakDropDownListTipus.FillWithOneValue(kodcsoportCim, krt_Cimek.Tipus, FormHeader1.ErrorPanel);
                IranyitoszamTextBoxPostafiok.Text = krt_Cimek.IRSZ;
                TelepulesTextBoxPostafiok.Text = krt_Cimek.TelepulesNev;
                RequiredNumberBoxPostafiok.Text = krt_Cimek.Hazszam;
                RequiredTextBoxPostafiokEgyben.Text = krt_Cimek.CimTobbi;
                SetPostaFiokView(krt_Cimek);
                break;

            case KodTarak.Cim_Tipus.Egyeb:
                KodtarakDropDownListTipus.FillWithOneValue(kodcsoportCim, krt_Cimek.Tipus, FormHeader1.ErrorPanel);
                textEgyebCim.Text = krt_Cimek.CimTobbi;
                break;

            default:
                goto case KodTarak.Cim_Tipus.Egyeb;
        }

        ErvenyessegCalendarControl1.ErvKezd = krt_Cimek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Cimek.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_Cimek.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_Cimek.Base);

    }

    private void SetPostaFiokView(KRT_Cimek krt_Cimek)
    {
        if (!String.IsNullOrEmpty(krt_Cimek.IRSZ) &&
            !String.IsNullOrEmpty(krt_Cimek.TelepulesNev) &&
            !String.IsNullOrEmpty(krt_Cimek.Hazszam))
        {
            PostafiokDividedPanel.Visible = true;
            PostafiokEgybenPanel.Visible = false;
        }
        else
        {
            if (Command == CommandName.View)
            {
                PostafiokDividedPanel.Visible = false;
                PostafiokEgybenPanel.Visible = true;
            }
            else
            {
                PostafiokDividedPanel.Visible = true;
                PostafiokEgybenPanel.Visible = true;
            }
        }
    }

    // form --> business object
    private KRT_Cimek GetBusinessObjectFromComponents()
    {
        KRT_Cimek krt_Cimek = new KRT_Cimek();
        krt_Cimek.Updated.SetValueAll(false);
        krt_Cimek.Base.Updated.SetValueAll(false);

        switch (KodtarakDropDownListTipus.SelectedValue)
        {
            case KodTarak.Cim_Tipus.Postai:
                krt_Cimek.Tipus = KodtarakDropDownListTipus.SelectedValue;
                krt_Cimek.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListTipus);
                // BUG_8613
                krt_Cimek.Orszag_Id = OrszagTextBox1.Id_HiddenField;
                krt_Cimek.Updated.Orszag_Id = pageView.GetUpdatedByView(OrszagTextBox1);

                krt_Cimek.OrszagNev = OrszagTextBox1.Text;
                krt_Cimek.Updated.OrszagNev = pageView.GetUpdatedByView(OrszagTextBox1);
                krt_Cimek.TelepulesNev = TelepulesTextBox1.Text;
                krt_Cimek.Updated.TelepulesNev = pageView.GetUpdatedByView(TelepulesTextBox1);
                krt_Cimek.IRSZ = IranyitoszamTextBox1.Text;
                krt_Cimek.Updated.IRSZ = pageView.GetUpdatedByView(IranyitoszamTextBox1);
                krt_Cimek.KozteruletNev = KozteruletTextBox1.Text;
                krt_Cimek.Updated.KozteruletNev = pageView.GetUpdatedByView(KozteruletTextBox1);
                krt_Cimek.KozteruletTipusNev = KozteruletTipusTextBox1.Text;
                krt_Cimek.Updated.KozteruletTipusNev = pageView.GetUpdatedByView(KozteruletTipusTextBox1);
                krt_Cimek.Hazszam = RequiredNumberBoxHsz.Text;
                krt_Cimek.Updated.Hazszam = pageView.GetUpdatedByView(RequiredNumberBoxHsz);
                krt_Cimek.HazszamBetujel = textEpulet.Text;
                krt_Cimek.Updated.HazszamBetujel = pageView.GetUpdatedByView(textEpulet);
                krt_Cimek.MindketOldal = rbListMindketOldal.SelectedValue;
                krt_Cimek.Updated.MindketOldal = pageView.GetUpdatedByView(rbListMindketOldal);
                krt_Cimek.Szint = textEmelet.Text;
                krt_Cimek.Updated.Szint = pageView.GetUpdatedByView(textEmelet);
                krt_Cimek.Lepcsohaz = textLepcsohaz.Text;
                krt_Cimek.Updated.Lepcsohaz = pageView.GetUpdatedByView(textLepcsohaz);
                krt_Cimek.Ajto = textAjto.Text;
                krt_Cimek.Updated.Ajto = pageView.GetUpdatedByView(textAjto);
                krt_Cimek.AjtoBetujel = textAjtoBetujel.Text;
                krt_Cimek.Updated.AjtoBetujel = pageView.GetUpdatedByView(textAjtoBetujel);
                krt_Cimek.HRSZ = textHelyrajziSzam.Text;
                krt_Cimek.Updated.HRSZ = pageView.GetUpdatedByView(textHelyrajziSzam);
                krt_Cimek.Tobbi = textEgyeb.Text;
                krt_Cimek.Updated.Tobbi = pageView.GetUpdatedByView(textEgyeb);
                krt_Cimek.Hazszamig = RequiredNumberBoxHszIg.Text;
                krt_Cimek.Updated.Hazszamig = pageView.GetUpdatedByView(RequiredNumberBoxHszIg);
                break;

            case KodTarak.Cim_Tipus.Postafiok:
                krt_Cimek.Tipus = KodtarakDropDownListTipus.SelectedValue;
                krt_Cimek.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListTipus);
                krt_Cimek.IRSZ = IranyitoszamTextBoxPostafiok.Text;
                krt_Cimek.Updated.IRSZ = pageView.GetUpdatedByView(IranyitoszamTextBoxPostafiok);
                krt_Cimek.TelepulesNev = TelepulesTextBoxPostafiok.Text;
                krt_Cimek.Updated.TelepulesNev = pageView.GetUpdatedByView(IranyitoszamTextBoxPostafiok);
                krt_Cimek.Hazszam = RequiredNumberBoxPostafiok.Text;
                krt_Cimek.Updated.Hazszam = pageView.GetUpdatedByView(IranyitoszamTextBoxPostafiok);
                krt_Cimek.CimTobbi = GetAppendedCim(krt_Cimek);
                krt_Cimek.Updated.CimTobbi = true;
                break;

            case KodTarak.Cim_Tipus.Egyeb:
                krt_Cimek.Tipus = KodtarakDropDownListTipus.SelectedValue;
                krt_Cimek.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListTipus);
                krt_Cimek.CimTobbi = textEgyebCim.Text;
                krt_Cimek.Updated.CimTobbi = pageView.GetUpdatedByView(textEgyebCim);
                break;

            default:
                goto case KodTarak.Cim_Tipus.Egyeb;
        }

        //krt_Cimek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_Cimek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_Cimek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_Cimek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Cimek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //Kateg�ria be�ll�t�sa konkr�tra, jelenleg bedr�tozva
        krt_Cimek.Kategoria = "K";
        krt_Cimek.Updated.Kategoria = true;

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Cimek.Base.Ver = FormHeader1.Record_Ver;
        krt_Cimek.Base.Updated.Ver = true;

        return krt_Cimek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(KodtarakDropDownListTipus);
            compSelector.Add_ComponentOnClick(OrszagTextBox1);
            compSelector.Add_ComponentOnClick(TelepulesTextBox1);
            compSelector.Add_ComponentOnClick(IranyitoszamTextBox1);
            compSelector.Add_ComponentOnClick(KozteruletTextBox1);
            compSelector.Add_ComponentOnClick(KozteruletTipusTextBox1);
            compSelector.Add_ComponentOnClick(RequiredNumberBoxHsz);
            compSelector.Add_ComponentOnClick(textEmelet);
            compSelector.Add_ComponentOnClick(textLepcsohaz);
            compSelector.Add_ComponentOnClick(textAjto);
            compSelector.Add_ComponentOnClick(textAjtoBetujel);
            compSelector.Add_ComponentOnClick(textHelyrajziSzam);
            compSelector.Add_ComponentOnClick(textEgyeb);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textEgyebCim);
            compSelector.Add_ComponentOnClick(RequiredNumberBoxHszIg);
            compSelector.Add_ComponentOnClick(textEpulet);
            compSelector.Add_ComponentOnClick(rbListMindketOldal);

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Cim" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
                            KRT_Cimek krt_Cimek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            //CR3321 teszt
                            Result result = service.InsertWithFKResolution(execParam, krt_Cimek);
                            //Result result = new Result();
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                                //a h�v� oldal akt�v tab-j�nak be�ll�t�sa
                                string tabContainer = Request.QueryString.Get("TabContainer");
                                if (String.IsNullOrEmpty(tabContainer))
                                {
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, GetAppendedCim(krt_Cimek), true, tryFireChangeEvent))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                }
                                else
                                {
                                    /*int tabIndex = -1;
                                    switch (KodtarakDropDownListTipus.SelectedValue)
                                    {
                                        case KodTarak.Cim_Tipus.Postai:
                                            tabIndex = 0;
                                            break;
                                        case KodTarak.Cim_Tipus.Egyeb:
                                            tabIndex = 1;
                                            break;
                                        default:
                                            goto case KodTarak.Cim_Tipus.Egyeb;
                                    }*/
                                    // CR3321 Partner r�gz�t�s csak keres�s ut�n
                                    //JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, GetAppendedCim(krt_Cimek), true, tryFireChangeEvent))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    //JavaScripts.RegisterSelectedRecordIdToParent(Page,"2C374873-52D0-E611-90F7-0050569A6FBA");
                                    //JavaScripts.RegisterCloseWindowClientScript(Page, tabIndex);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
                                KRT_Cimek krt_Cimek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.UpdateWithFKResolution(execParam, krt_Cimek);

                                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, recordId, GetAppendedCim(krt_Cimek), true, tryFireChangeEvent))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }


                                    //JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    //JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }


    protected void KodtarakDropDownListTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetActivePanel();
    }

    protected void registerJavascripts()
    {
        //AutoComplete-ek k�zti f�gg�s�gek be�ll�t�sa, postback eset�n contextkey �jra be�ll�t�sa
        //JavaScripts.SetAutoCompleteContextKey(Page, OrszagTextBox1.TextBox,TelepulesTextBox1.TextBox, TelepulesTextBox1.AutoCompleteExtenderClientID);
        //JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //JavaScripts.SetAutoCompleteContextKey(Page, IranyitoszamTextBoxPostafiok.TextBox, TelepulesTextBoxPostafiok.TextBox, TelepulesTextBoxPostafiok.AutoCompleteExtenderClientID);
        //JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBoxPostafiok.TextBox, IranyitoszamTextBoxPostafiok.TextBox, IranyitoszamTextBoxPostafiok.AutoCompleteExtenderClientID);
        //az els� ir�ny�t�sz�m automatikus be�r�sa
        //JavaScripts.SetTextWithFirstResult(TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //A popup list�k zIndex-�nek be�ll�t�sa 1000-re
        JavaScripts.SetCompletionListszIndex(Page);

        if (KodtarakDropDownListTipus.SelectedValue == KodTarak.Cim_Tipus.Postai)
        {
            ScriptManager1.Scripts.Add(new ScriptReference("~/JavaScripts/Cim.js"));
            string js = @"Sys.Application.add_init(function() {
                     Sys.Component.create(Utility.CimManager, {'IranyitoszamAutoCompleteClientId':'" + IranyitoszamTextBox1.AutoCompleteExtenderClientID 
                     + "','IranyitoszamTextBoxClientId':'" + IranyitoszamTextBox1.TextBox.ClientID
                     + "','TelepulesAutoCompleteClientId':'" + TelepulesTextBox1.AutoCompleteExtenderClientID
                     + "','TelepulesTextBoxClientId':'" + TelepulesTextBox1.TextBox.ClientID
                     + "','OrszagTextBoxClientId':'" + OrszagTextBox1.TextBox.ClientID + @"'}
                     , null, null, 
                     $get('" + IranyitoszamTextBox1.TextBox.ClientID + @"'));
                     });";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
        }

        if (KodtarakDropDownListTipus.SelectedValue == KodTarak.Cim_Tipus.Postafiok)
        {
            ScriptManager1.Scripts.Add(new ScriptReference("~/JavaScripts/Cim.js"));
            string js = @"Sys.Application.add_init(function() {
                     Sys.Component.create(Utility.CimManager, {'IranyitoszamAutoCompleteClientId':'" + IranyitoszamTextBoxPostafiok.AutoCompleteExtenderClientID
                     + "','IranyitoszamTextBoxClientId':'" + IranyitoszamTextBoxPostafiok.TextBox.ClientID
                     + "','TelepulesAutoCompleteClientId':'" + TelepulesTextBoxPostafiok.AutoCompleteExtenderClientID
                     + "','TelepulesTextBoxClientId':'" + TelepulesTextBoxPostafiok.TextBox.ClientID + @"'}
                     , null, null, 
                     $get('" + IranyitoszamTextBoxPostafiok.TextBox.ClientID + @"'));
                     });";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
        }
    }


}
