<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CimekLovList.aspx.cs" Inherits="CimekLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc7" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        function pageLoad() {
            var accordion = $find('<%=Accordion1.ClientID%>_AccordionExtender');
        accordion.add_selectedIndexChanged(SelectedIndexChanged);
        var btnSearch = $get('<%=ButtonSearch.ClientID%>');
        if (accordion.get_SelectedIndex() == 2) {
            btnSearch.style.display = "none";
        }
        else {
            btnSearch.style.display = "block";
        }
    }
    function SelectedIndexChanged(sender, e) {
        var btnSearch = $get('<%=ButtonSearch.ClientID%>');
        if (e._selectedIndex == 2) {
            btnSearch.style.display = "none";
        }
        else {
            btnSearch.style.display = "block";
        }
    }
    </script>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,CimekLovListHeaderTitle%>" />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <%--// CR3321  Partner r�gz�t�s csak keres�s ut�n--%>
            <asp:TextBox ID="CimTextBox" runat="server" EnableViewState="true" OnTextChanged="CimTextBox_ValueChanged" style="display:none"  />
            <asp:HiddenField ID="CimHiddenField" runat="server" />
            <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                <ContentTemplate>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                        <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                            <td>
                                <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div style="position: relative; width: 99%">
                                    <ajaxToolkit:Accordion ID="Accordion1" runat="server" HeaderCssClass="AccordionHeader" HeaderSelectedCssClass="AccordionHeaderSelected"
                                        FramesPerSecond="50" TransitionDuration="200">
                                        <Panes>
                                            <ajaxToolkit:AccordionPane runat="server" ID="paneFullText">
                                                <Header>Teljes keres�s</Header>
                                                <Content>
                                                    <eUI:eFormPanel ID="EFormPanelFullText" runat="server">
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                    <td class="mrUrlapMezo">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <asp:Label ID="labelKifejezes" runat="server" Text="Kifejez�s:"></asp:Label></td>
                                                                    <td class="mrUrlapMezo">
                                                                        <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </eUI:eFormPanel>
                                                </Content>
                                            </ajaxToolkit:AccordionPane>
                                            <ajaxToolkit:AccordionPane runat="server" ID="paneFast">
                                                <Header>Gyors keres�s</Header>
                                                <Content>
                                                    <eUI:eFormPanel ID="EFormPanelDetail" runat="server">
                                                        <ajaxToolkit:TabContainer ID="TabContainerDetail" runat="server" Width="100%">
                                                            <!-- Postai c�mek -->
                                                            <ajaxToolkit:TabPanel ID="TabPanelPostai" runat="server" TabIndex="0">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="labelPostaiHeader" runat="server" Text="Postai c�mek"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tbody>
                                                                            <tr class="urlapNyitoSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapSpacer">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapCaption">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                            </tr>
                                                                            <tr class="urlapSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelTelepules" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <uc4:TelepulesTextBox ID="TelepulesTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                                </td>
                                                                                <td></td>
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <uc5:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="urlapSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelKozterulet" runat="server" Text="K�zter�let neve:"></asp:Label></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <uc6:KozteruletTextBox ID="KozteruletTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                                </td>
                                                                                <td></td>
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelKozteruletTipus" runat="server" Text="K�zter�let tipusa:"></asp:Label></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <uc7:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <!-- Egy�b c�mek -->
                                                            <ajaxToolkit:TabPanel ID="TabPanelEgyeb" runat="server" TabIndex="1">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="labelEgyebHeader" runat="server" Text="Egy�b c�mek"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tbody>
                                                                            <tr class="urlapNyitoSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapSpacer">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapCaption">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                            </tr>
                                                                            <tr class="urlapSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <uc8:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                                                                </td>
                                                                                <td></td>
                                                                                <td class="mrUrlapCaption"></td>
                                                                                <td class="mrUrlapMezo"></td>
                                                                            </tr>
                                                                            <tr class="urlapSor">
                                                                                <td class="mrUrlapCaption">
                                                                                    <asp:Label ID="labelEgyebCim" runat="server" Text="Tartalom:"></asp:Label>
                                                                                </td>
                                                                                <td class="mrUrlapMezo">
                                                                                    <asp:TextBox ID="textEgyebCim" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                                </td>
                                                                                <td></td>
                                                                                <td class="mrUrlapCaption"></td>
                                                                                <td class="mrUrlapMezo"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                        </ajaxToolkit:TabContainer>
                                                    </eUI:eFormPanel>
                                                </Content>
                                            </ajaxToolkit:AccordionPane>
                                            <ajaxToolkit:AccordionPane runat="server" ID="paneDetail">
                                                <Header>R�szletes keres�s</Header>
                                                <Content>
                                                    <eUI:eFormPanel ID="EFormPanel2" runat="server">

                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                    <td class="mrUrlapMezo">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapMezo">
                                                                        <asp:ImageButton ID="ButtonAdvancedSearch" runat="server"
                                                                            ImageUrl="images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                                            onmouseover="swapByName(this.id,'reszleteskereses_trap2.jpg')"
                                                                            onmouseout="swapByName(this.id,'reszleteskereses_trap1.jpg')"
                                                                            AlternateText="R�szletes keres�s" Visible="True"
                                                                            CausesValidation="false" />
                                                                    </td>
                                                                    <td class="mrUrlapCaption">
                                                                        <asp:Label ID="labelSearchIndicator" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </eUI:eFormPanel>
                                                </Content>
                                            </ajaxToolkit:AccordionPane>
                                        </Panes>
                                    </ajaxToolkit:Accordion>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap.jpg" AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                <div class="listaFulFelsoCsikKicsi" style="width: 99%">
                                    <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top;">
                                <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="99%"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top; height: 35px;">
                                <table style="width: 99%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="text-align: left; width: 33%">
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"
                                                onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')" />
                                        </td>
                                        <td style="text-align: left; width: 33%">
                                            <%--CR3321 Partner r�gz�t�s csak keres�s ut�n--%>
                                           <%-- Lejjebb rakva (RegisterCloseWindowClientScript nem j�l m�k�d�tt)--%>
                                            <%--  <asp:ImageButton ID="ButtonNewOnSelected" runat="server" 
                                                ImageUrl="~/images/hu/ovalgomb/uj_klonozassal.png" 
                                                AlternateText="�j l�trehoz�sa kl�noz�ssal" 
                                                onmouseover="swapByName(this.id,'uj_klonozassal2.png')" onmouseout="swapByName(this.id,'uj_klonozassal.png')"/>--%>
                                        </td>
                                        <td style="text-align: right; width: 33%">
                                            <%--     <asp:ImageButton ID="ButtonNew" runat="server" 
                                                ImageUrl="~/images/hu/ovalgomb/uj_letrehozasa.png" 
                                                AlternateText="�j l�trehoz�sa" 
                                                onmouseover="swapByName(this.id,'uj_letrehozasa2.png')" onmouseout="swapByName(this.id,'uj_letrehozasa.png')"/>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </eUI:eFormPanel>
        <br />
        <%--CR3321 Partner r�gz�t�s csak keres�s ut�n--%>
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="ButtonNewOnSelected" runat="server"
                        ImageUrl="~/images/hu/ovalgomb/uj_klonozassal.png"
                        AlternateText="�j l�trehoz�sa kl�noz�ssal"
                        onmouseover="swapByName(this.id,'uj_klonozassal2.png')" onmouseout="swapByName(this.id,'uj_klonozassal.png')" />

                    <asp:ImageButton ID="ButtonNew" runat="server"
                        ImageUrl="~/images/hu/ovalgomb/uj_letrehozasa.png"
                        AlternateText="�j l�trehoz�sa"
                        onmouseover="swapByName(this.id,'uj_letrehozasa2.png')" onmouseout="swapByName(this.id,'uj_letrehozasa.png')" />

                </td>
                <td>
                    <uc2:LovListFooter ID="LovListFooter1" runat="server" />
                </td>
            </tr>
 
        </table>


    </div>
</asp:Content>

