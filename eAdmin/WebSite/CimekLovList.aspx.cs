/* $Header: CimekLovList.aspx.cs, 24, 2017.06.09. 11:43:09, Németh Krisztina$ 
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Text;
using System.Collections.Generic;
using Contentum.eUIControls;

public partial class CimekLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    private Type _type = typeof(KRT_CimekSearch);

    private const string kodcsoportCim = "CIM_TIPUS";

    private string filterType = null;

    public const int tabIndexPostai = 0;
    public const int tabIndexEgyeb = 1;

    public const int paneIndexFull = 0;
    public const int paneIndexFast = 1;
    public const int paneIndexDetail = 2;

    private const string searchIndicatorText = "Keres�s v�grehajtva!";
    private const string noSerchResult = "A keres�snek nincs eredm�nye";

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection:IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text,delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(DataRow row)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (row["Tipus"].ToString())
            {
                case KodTarak.Cim_Tipus.Postai:
                    string orszag = row["OrszagNev"].ToString();
                    cim.Add(orszag,delimeter);
                    string iranyitoszam = row["IRSZ"].ToString();
                    cim.Add(iranyitoszam,delimeter);
                    string telepules = row["TelepulesNev"].ToString();
                    cim.Add(telepules,delimeter);
                    // BLG_1347
                    //cim.Add(kozterulet, delimeterSpace);
                    string kozterulet = row["KozteruletNev"].ToString();
                    string hrsz = row["HRSZ"].ToString();                   
                    if (String.IsNullOrEmpty(kozterulet) && !String.IsNullOrEmpty(hrsz))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(hrsz, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(kozterulet, delimeterSpace);
                        string kozteruletTipus = row["KozteruletTipusNev"].ToString();
                        cim.Add(kozteruletTipus, delimeterSpace);
                        string hazszam = row["Hazszam"].ToString();
                        string hazszamIg = row["Hazszamig"].ToString();
                        string hazszamBetujel = row["HazszamBetujel"].ToString();
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = row["Lepcsohaz"].ToString();
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = row["Szint"].ToString();
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = row["Ajto"].ToString();
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = row["AjtoBetujel"].ToString();
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = row["CimTobbi"].ToString();
                    cim.Add(Cim,delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if(text.Length >= lastDelimeter.Length)
            text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            throw e;
        }

        return text.ToString();
    }

    private void SetDefaultValues()
    {
        TelepulesTextBox1.Text = "";
        IranyitoszamTextBox1.Text = "";
        KozteruletTextBox1.Text = "";
        KozteruletTipusTextBox1.Text = "";

        KodtarakDropDownListTipus.SetSelectedValue("");
        textEgyebCim.Text = "";

        TabContainerDetail.ActiveTabIndex = tabIndexPostai;
        Accordion1.SelectedIndex = paneIndexFull;
    }

    private void SetNoSearchResultText()
    {
        if (ListBoxSearchResult.Items.Count == 0)
        {
            ListBoxSearchResult.Items.Add(new ListItem(noSerchResult, ""));
        }
        else
        {
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CimekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //Accordion panel vez�rl�inek inicializ�l�sa, enn�lk�l nem hajland� rendesen m�k�dni, de hogy m�rt az rejt�ly
        ControlCollection controls = paneFullText.Controls;
        controls = paneDetail.Controls;
        controls = paneFast.Controls;

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        KodtarakDropDownListTipus.DropDownList.AutoPostBack = true;
        KodtarakDropDownListTipus.DropDownList.SelectedIndexChanged += new EventHandler(ButtonSearch_Click);

        registerJavascript();


        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        string query = "&" + QueryStringVars.HiddenFieldId + "=" + CimHiddenField.ClientID
                  + "&" + QueryStringVars.TextBoxId + "=" + CimTextBox.ClientID;


        if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.TryFireChangeEvent)))
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        string jsdoPostback = "function NewCimCallback(){__doPostBack('" + CimTextBox.ClientID + @"','');}";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NewCimCallback", jsdoPostback, true);

        //sz�r�s
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType != null)
        {
            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx",
                                                QueryStringVars.Command + "=" + CommandName.New +
                                                "&" + QueryStringVars.Filter + "=" + filterType + query,
                                                Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID,
                                                EventArgumentConst.refresh, TabContainerDetail.ClientID);

            //ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType
            //, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
          + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType + query + "&" +
          QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);
            //ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            //+ JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType + "&" +
            //QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("CimekSearch.aspx", QueryStringVars.Filter + "=" + filterType, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch, true);

            switch (filterType)
            {
                case KodTarak.Cim_Tipus.Postai:
                    LovListHeader1.HeaderTitle = Resources.LovList.CimekLovListHeaderTitle_Filtered_Postai;
                    TabContainerDetail.Tabs[tabIndexEgyeb].Visible = false;
                    labelEgyebHeader.Visible = false;
                    break;
                case KodTarak.Cim_Tipus.Egyeb:
                    LovListHeader1.HeaderTitle = Resources.LovList.CimekLovListHeaderTitle_Filtered_Egyeb;
                    TabContainerDetail.Tabs[tabIndexPostai].Visible = false;
                    labelPostaiHeader.Visible = false;
                    paneDetail.Visible = false;
                    break;
                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }
        else
        {
            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            //ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            //, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx",
                                                QueryStringVars.Command + "=" + CommandName.New +
                                                query,
                                                Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID,
                                                EventArgumentConst.refresh, TabContainerDetail.ClientID);

            //ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            //    + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" +
            //    QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);
            ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
               + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + query + "&" +
               QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("CimekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch, true);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
        + JavaScripts.SetOnClientClick("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);


        if (!IsPostBack)
        {
            KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportCim, LovListHeader1.ErrorPanel);
            ListItem itemPostai = KodtarakDropDownListTipus.DropDownList.Items.FindByValue(KodTarak.Cim_Tipus.Postai);
            KodtarakDropDownListTipus.DropDownList.Items.Remove(itemPostai);
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        labelSearchIndicator.Text = "";
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
        KRT_CimekSearch search = null;

        //a keres� formon eszk�zlt r�szletes keres�s eredm�nye
        if (searchObjectFromSession == true)
        {
            search = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());
            TextBoxSearch.Text = "";
        }
        else
        {
            search = new KRT_CimekSearch();
            switch (Accordion1.SelectedIndex)
            {
                case paneIndexFull:
                    search.Tipus.Value = "";
                    search.Tipus.Operator = "";
                    search.TelepulesNev.Value = SearchKey;
                    search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.TelepulesNev.Group = "1";
                    search.TelepulesNev.GroupOperator = Query.Operators.or;
                    search.OrszagNev.Value = SearchKey;
                    search.IRSZ.Value = SearchKey;
                    search.IRSZ.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.IRSZ.Group = "1";
                    search.IRSZ.GroupOperator = Query.Operators.or;
                    search.OrszagNev.Value = SearchKey;
                    search.OrszagNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.OrszagNev.Group = "1";
                    search.OrszagNev.GroupOperator = Query.Operators.or;
                    search.KozteruletNev.Value = SearchKey;
                    search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.KozteruletNev.Group = "1";
                    search.KozteruletNev.GroupOperator = Query.Operators.or;
                    search.KozteruletTipusNev.Value = SearchKey;
                    search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.KozteruletTipusNev.Group = "1";
                    search.KozteruletTipusNev.GroupOperator = Query.Operators.or;
                    search.Hazszam.Value = SearchKey;
                    search.Hazszam.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.Hazszam.Group = "1";
                    search.Hazszam.GroupOperator = Query.Operators.or;
                    search.CimTobbi.Value = SearchKey;
                    search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.CimTobbi.Group = "1";
                    search.CimTobbi.GroupOperator = Query.Operators.or;
                    break;

                //r�szletes keres�s
                case paneIndexFast:
                    int deltaTabIndex = 0;
                    if (filterType != null)
                    {
                        if (filterType != KodTarak.Cim_Tipus.Postai)
                            deltaTabIndex = 1;

                    }
                    switch (TabContainerDetail.ActiveTabIndex + deltaTabIndex)
                    {
                        case tabIndexPostai:
                            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                            search.Tipus.Operator = Query.Operators.equals;
                            if (true)
                            {
                                if (!String.IsNullOrEmpty(TelepulesTextBox1.Text))
                                {
                                    search.TelepulesNev.Value = TelepulesTextBox1.Text;
                                    search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(IranyitoszamTextBox1.Text))
                                {
                                    search.IRSZ.Value = IranyitoszamTextBox1.Text;
                                    search.IRSZ.Operator = Search.GetOperatorByLikeCharater(IranyitoszamTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(KozteruletTextBox1.Text))
                                {
                                    search.KozteruletNev.Value = KozteruletTextBox1.Text;
                                    search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(KozteruletTipusTextBox1.Text))
                                {
                                    search.KozteruletTipusNev.Value = KozteruletTipusTextBox1.Text;
                                    search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTipusTextBox1.Text);
                                }
                            }
                            break;
                        case tabIndexEgyeb:
                            if (String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                            {
                                search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                                search.Tipus.Operator = Query.Operators.notequals;
                            }
                            else
                            {
                                search.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                                search.Tipus.Operator = Query.Operators.equals;
                            }
                            if (!String.IsNullOrEmpty(textEgyebCim.Text))
                            {
                                search.CimTobbi.Value = textEgyebCim.Text;
                                search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(textEgyebCim.Text);
                            }
                            break;
                    }
                    break;
            }
        }

        // ha kell, sz�r�nk a t�pusra, ak�r fel�lv�gva a r�szletes keres�sn�l megadott t�pust is
        if (filterType != null)
        {
            switch (filterType)
            {
                case KodTarak.Cim_Tipus.Postai:
                    search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                    search.Tipus.Operator = Query.Operators.equals;
                    break;
                case KodTarak.Cim_Tipus.Egyeb:
                    if (search.Tipus.Value == "" || search.Tipus.Value == KodTarak.Cim_Tipus.Postai)
                    {
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.notequals;
                    }
                    break;
                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        string defaultSortExpression = "TelepulesNev, KozteruletNev";

        if (String.IsNullOrEmpty(search.Tipus.Value))
        {
            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
            search.Tipus.Operator = Query.Operators.equals;
            //search.OrderBy = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev,Hazszam";
            search.OrderBy = defaultSortExpression;
            Result result = service.GetAll(execParam, search);
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, true);
            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
            search.Tipus.Operator = Query.Operators.notequals;
            search.OrderBy = "KodTarakCimTipus.Nev, CimTobbi";//"TipusNev, CimTobbi";
            result = service.GetAllWithExtension(execParam, search);
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, false);

            trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        }
        else
        {
            Result result = new Result();
            if (search.Tipus.Value == KodTarak.Cim_Tipus.Postai && search.Tipus.Operator == Query.Operators.equals)
            {
                //search.OrderBy = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev,Hazszam";
                search.OrderBy = defaultSortExpression;
                result = service.GetAllWithExtension(execParam, search);
            }
            else
            {
                search.OrderBy = "KodTarakCimTipus.Nev, CimTobbi";//"TipusNev, CimTobbi";
                result = service.GetAllWithExtension(execParam, search);
            }
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, true);

            trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        }

        SetNoSearchResultText();
    }

    public void ListBoxFill(ListBox listBox, Contentum.eBusinessDocuments.Result result, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel, bool clear)
    {
        //if (listBox == null || result.Ds == null) return;
        if (listBox == null ) return;

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }
        else
        {
            try
            {
                if (clear)
                    listBox.Items.Clear();

                DataSet ds = result.Ds;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string id = ((Guid)row["Id"]).ToString();
                    string text = GetAppendedCim(row);
                    listBox.Items.Add(new ListItem(text, id));
                }
            }
            catch (Exception)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"),"");
            }
        }
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        //SetDefaultValues();
                        labelSearchIndicator.Text = "";
                        FillListBoxSearchResult("", true);
                        labelSearchIndicator.Text = "Keres�s v�grehajtva.";
                    }
                    break;
                case EventArgumentConst.refresh:
                    if (!disable_refreshLovList)
                    {
                        FillListBoxSearchResult(TextBoxSearch.Text, false);
                    }
                    break;
            }
        }
    }
    protected void TabContainerDetail_ActiveTabChanged(object sender, EventArgs e)
    {
        //FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void registerJavascript()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //autocomplete-ek f�gg�s�geinek be�ll�t�sa, postback ut�n contextkey �jra be�ll�t�sa
        JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox,IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);
        
        //ir�ny�t�sz�m kit�lt�se
        //JavaScripts.SetTextWithFirstResult(TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //accordion postback ut�n helyes header st�lus megjelen�t�s
        JavaScripts.FixAccordionPane(Page, Accordion1.ClientID);

        //textbox-ok eset�n enter lenyom�s�ra keres�s, f�kusz meg�rz�se, kurzor a sz�veg v�g�re �ll�t�sa
        string jsOnKeyDown = "function CancelPopulating(sender,e){sender.remove_populating(CancelPopulating);e.set_cancel(true);}\n" +
            "function SetFocus(sender,e){sender.remove_endRequest(SetFocus);window.setTimeout(function() {var text = $get(sender.lastFocus); text.focus();\n" +
            "text.value = text.value;if(sender.autoComplete)$find(sender.autoComplete).add_populating(CancelPopulating);},1);};\n" +
            "function OnKeyDown(sender,ev) {var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;\n" +
            "if(AjaxControlToolkit.AutoCompleteBehavior) var autoComplete = Sys.UI.Behavior.getBehaviorsByType(sender,AjaxControlToolkit.AutoCompleteBehavior)[0];\n" +
            "if(k === Sys.UI.Key.enter && (!autoComplete || !autoComplete._popupBehavior.get_visible()))\n" +
            "{var prm = Sys.WebForms.PageRequestManager.getInstance();prm.lastFocus = sender.id;if(autoComplete) prm.autoComplete = autoComplete.get_id();\n" +
            "prm.add_endRequest(SetFocus);\n" +
            "" + ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}}";

        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "OnKeyDown", jsOnKeyDown, true);

        TextBoxSearch.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        textEgyebCim.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        TelepulesTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        IranyitoszamTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        KozteruletTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        KozteruletTipusTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");

    }


    // CR3321 Partner r�gz�t�s csak keres�s ut�n
    protected void CimTextBox_ValueChanged(object sender, EventArgs e)
    {
        string selectedId = CimHiddenField.Value;
        string selectedText = CimTextBox.Text;
        
        bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

        JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);
        JavaScripts.RegisterCloseWindowClientScript(Page, false);
        disable_refreshLovList = true;
        
    }
}
