<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CimekSearch.aspx.cs" Inherits="PartnercimekSearch" Title="Untitled Page" %>


<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc5" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc6" %> 
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc7" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc10" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,PartnercimekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <ajaxToolkit:Accordion ID="Accordion1" runat="server" HeaderCssClass="AccordionHeader" HeaderSelectedCssClass="AccordionHeaderSelected"
                    FramesPerSecond="50" TransitionDuration="200">
                    <Panes>
                    <ajaxToolkit:AccordionPane runat="server" ID="paneFullText">
                    <Header>Teljes keres�s</Header>
                    <Content>
                    <eUI:eFormPanel ID="EFormPanelFullText" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                               <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKifejezes" runat="server" Text="Kifejez�s:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textKifejezes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                   </Content> 
                   </ajaxToolkit:AccordionPane> 
                    <ajaxToolkit:AccordionPane runat="server" ID="paneDetail">
                   <Header>R�szletes keres�s</Header>
                   <Content>
                       <eUI:eFormPanel ID="EFormPanelDetail" runat="server">
                                   <ajaxToolkit:TabContainer ID="TabContainerDetail" runat="server" 
                                     Width="100%">
                                     <!-- Postai c�mek -->
                                        <ajaxToolkit:TabPanel ID="TabPanelPostai" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                        <asp:Label ID="labelPostaiHeader" runat="server" Text="Postai c�mek"></asp:Label>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                            <tr class="urlapNyitoSor">
                                                <td class="mrUrlapCaption">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapMezo">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapSpacer">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapCaption">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapMezo">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelOrszag" runat="server" Text="Orsz�g:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc6:OrszagTextBox ID="OrszagTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch"/>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelTelepules" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc7:TelepulesTextBox ID="TelepulesTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch"/>                                        
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc8:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch" Number="false"/>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelKozterulet" runat="server" Text="K�zter�let neve:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc9:KozteruletTextBox ID="KozteruletTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch"/>
                                                </td>    
                                            </tr>                                    
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelKozteruletTipus" runat="server" Text="K�zter�let tipusa:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc10:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearch"/>                                        
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelMindketOldal" runat="server" Text="Mindk�t oldal:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:DropDownList ID="ddownMinketOldal" runat="server" CssClass="mrUrlapInputSearchComboBox">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                           <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelHazszam" runat="server" Text="H�zsz�m:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc4:RequiredNumberBox ID="RequiredNumberBoxHsz" runat="server" Validate="false"/>
                                                    <span style="position:relative;top:-3px;padding-left:9px;padding-right:9px">&#8212;</span>
                                                    <uc4:RequiredNumberBox ID="RequiredNumberBoxHszIg" runat="server" Validate="false"/>
                                                </td>                                               
                                                 <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelHazszamBetujel" runat="server" Text="H�zsz�m bet�jele:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textHazszamBetujel" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelLepcsohaz" runat="server" Text="L�pcs�h�z:"></asp:Label>&nbsp;</td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textLepcsohaz" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelEmelet" runat="server" Text="Emelet:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textEmelet" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelAjto" runat="server" Text="Ajt�:"></asp:Label>&nbsp;</td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textAjto" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelAjtoBetujel" runat="server" Text="Ajt� bet�jele:"></asp:Label>&nbsp;</td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textAjtoBetujel" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelHelyrajziSzam" runat="server" Text="Helyrajzi sz�m:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textHelyrajziSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelEgyeb" runat="server" Text="Egy�b adatok:"></asp:Label>&nbsp;</td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textEgyeb" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                            </tr> 
                                            </tbody>
                                            </table>
                                        </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <!-- Egy�b c�mek -->
                                        <ajaxToolkit:TabPanel ID="TabPanelEgyeb" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                        <asp:Label ID="labelEgyebHeader" runat="server" Text="Egy�b c�mek"></asp:Label>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                              <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <uc11:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="mrUrlapCaption">
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                    </td>
                                               </tr>
                                               <tr class="urlapSor">
                                                    <td class="mrUrlapCaption">
                                                        <asp:Label ID="labelEgyebCimek" runat="server" Text="Tartalom:"></asp:Label>                                                      
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                        <asp:TextBox ID="textEgyebCimek" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="mrUrlapCaption">
                                                    </td>
                                                    <td class="mrUrlapMezo">
                                                    </td>
                                               </tr>
                                            </tbody>
                                            </table>    
                                        </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer> 
                        </eUI:eFormPanel>
                        <!-- K�z�s r�sz -->
                        <eUI:eFormPanel ID="EFormPanelCommon" runat="server"> 
                        <div style="margin-left:-85px;">
                        <uc5:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                        </uc5:Ervenyesseg_SearchFormComponent>
                        </div> 
                        <div style="text-align:left;margin-left:-10px;padding-top:10px;padding-bottom:5px">    
                        <uc3:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                           runat="server">
                        </uc3:TalalatokSzama_SearchFormComponent>
                        </div>
                        </eUI:eFormPanel> 
                    </Content> 
                   </ajaxToolkit:AccordionPane>
                   </Panes>    
                   </ajaxToolkit:Accordion>
                 <div style="height:10px"></div>
                 <uc2:SearchFooter ID="SearchFooter1" runat="server" />              
              </td>
            </tr>
          </tbody>
        </table>                 
</asp:Content>

