<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdoszamTextBox.ascx.cs"
    Inherits="Component_AdoszamTextBox" %>
<%@Reference Control="EditablePartnerTextBox.ascx" %>
<div class="DisableWrap">
    <asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
    <asp:CheckBox ID="cbKulfoldi" runat="server" Enabled="true" Visible="false" Text="K�lf�ldi" ToolTip="K�lf�ldi (EU) ad�sz�m" Checked="false" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="HiddenField_PartnerId" runat="server" />
    <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="TextBox1"
        Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="Validator1">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
<%--    <asp:CustomValidator ID="CustomValidator_Format" runat="server" ErrorMessage="Az ad�sz�m form�tuma nem megfelel�!"
        SetFocusOnError="true" ClientValidationFunction="ValidateAdoszamFormat" EnableClientScript="true"
        ControlToValidate="TextBox1" Display="None"
        onservervalidate="CustomValidator_Format_ServerValidate"/>--%>
    <asp:CustomValidator ID="CustomValidator_Format" runat="server" ErrorMessage="Az ad�sz�m form�tuma nem megfelel�!"
        SetFocusOnError="true" EnableClientScript="true"
        ControlToValidate="TextBox1" Display="None"/>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Format" runat="server"
        TargetControlID="CustomValidator_Format">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>
