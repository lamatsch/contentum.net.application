using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public partial class Component_AdoszamTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    private bool isEightCharsFormatEnabled = true;

    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool ValidateFormat
    {
        set { CustomValidator_Format.Enabled = value; }
        get { return CustomValidator_Format.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox1.ValidationGroup = value;
        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public string Text
    {
        set
        {
            TextBox1.Text = value;
            HiddenField1.Value = value;
        }
        get { return TextBox1.Text; }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            if (!value)
            {
                cbKulfoldi.Enabled = false;
            }
        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TextBox1.ReadOnly = value;
            if (value)
            {
                cbKulfoldi.Enabled = false;
            }

        }
        get { return TextBox1.ReadOnly; }
    }

    public bool KulfoldiAdoszamCheckBoxVisible
    {
        get { return cbKulfoldi.Visible; }
        set { cbKulfoldi.Visible = value; }
    }

    public bool KulfoldiAdoszamCheckBoxTextVisible
    {
        get { return (cbKulfoldi.LabelAttributes.CssStyle["display"] == "none" ? false : true); }
        set { cbKulfoldi.LabelAttributes.CssStyle["display"] = (value ? "" : "none"); }
    }

    Component_EditablePartnerTextBox _PartnerTextBox = null;
    public Component_EditablePartnerTextBox PartnerTextBox
    {
        set {_PartnerTextBox = value; }
        get { return _PartnerTextBox; }
    }

    public string Partner_Id
    {
        set {
            if (this._PartnerTextBox != null)
                this._PartnerTextBox.Id_HiddenField = value;
        }
        get {
            if (this._PartnerTextBox != null)
                return this._PartnerTextBox.Id_HiddenField;
            return null;
        }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly= !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
            }
        }
    }


    public string CssClass
    {
        get
        {
            return TextBox1.CssClass;
        }
        set
        {
            TextBox1.CssClass = value;
        }
    }

    public bool IsKulfoldiAdoszam
    {
        get { return cbKulfoldi.Checked; }
    }

    public string KulfoldiAdoszamJelolo
    {
        get { return cbKulfoldi.Checked ? "1" : "0"; }
        set { cbKulfoldi.Checked = (value == "1" ? true : false);
            // TODO: hiddenfield?
        }
    }

    public bool IsAdoszamChanged
    {
        get
        {
            string newValue = null;

            if (!String.IsNullOrEmpty(TextBox1.Text))
            {
                newValue = String.Join(";", new string[] { TextBox1.Text, (cbKulfoldi.Checked ? "1" : "0") });
            }
            else
            {
                newValue = "";
            }
            return newValue != HiddenField1.Value;
        }
    }

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    #endregion

    public void SetTextBoxByAdoszam(string adoszam, string kulfoldiAdoszamJelolo)
    {
        Text = adoszam;
        cbKulfoldi.Checked = (kulfoldiAdoszamJelolo == "1" ? true : false);

        HiddenField1.Value = String.Join(";", new string[] {adoszam, kulfoldiAdoszamJelolo});
    }

    /// <summary>
    /// Ha meg volt adva partner Id, lek�ri az ahhoz tartoz� ad�sz�mot
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetTextBoxByPartnerId(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        // ha hiba volt, �res lesz
        Text = HiddenField1.Value = "";
        cbKulfoldi.Checked = false;

        if (!String.IsNullOrEmpty(Partner_Id))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Partner_Id;

            Result result = service.GetAdoszamByPartner(execParam);

            if (result.IsError)
            {
                if (errorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                }
            }
            else
            {
                if (result.Record != null)
                {
                    string[] items = (string[])result.Record;
                    Text = items[0];
                    HiddenField1.Value = String.Join(";", items);

                    if (items.Length > 0)
                    {
                        cbKulfoldi.Checked = (items[1] == "1" ? true : false);
                    }
                }
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ////ASP.NET 2.0 bug work around
        //TextBox.Attributes.Add("readonly", "readonly");

        // work around: k�nyszer�tett ugr�s az ad�sz�m v�g�re (m�sk�pp az elej�re ugrik...)
        TextBox.Attributes["onfocus"] = @"
// IE Support
if (document.selection) { 
        // to get cursor position, get empty selection range
       var oSel = document.selection.createRange();

       // move selection start and end to desired position
       oSel.moveStart ('character', this.value.length);
       oSel.moveEnd ('character', this.value.length);
       oSel.select ();
}
";
    }

    // jelenleg nem valid�lunk szerver oldalon
    protected void CustomValidator_Format_ServerValidate(object source, ServerValidateEventArgs arguments)
    {
        if (cbKulfoldi.Checked)
        {
            arguments.IsValid = true;
            return;
        }

        string adoszam = arguments.Value;

        if (String.IsNullOrEmpty(adoszam.Trim()))
        {
            arguments.IsValid = false;
            return;
        }

        string adoszamPattern = @"^(\d{8})(-[1-5]-\d{2})$";
        if (this.isEightCharsFormatEnabled) {
            adoszamPattern = @"^(\d{8})(-[1-5]-\d{2}|)$";
        }

        Regex checkRegex = new Regex(adoszamPattern);
        MatchCollection matches = checkRegex.Matches(adoszam);
        if (matches.Count == 0)
        {
            //rossz formatum
            arguments.IsValid = false;
            return;
        }
        string adoszam1 = matches[0].Groups[1].Value;

        string vektor = Rendszerparameterek.Get(Page, Rendszerparameterek.ADOSZAM_ELLENORZO_VEKTOR);

        int adoszamLength = vektor.Length + 1;

        if (!String.IsNullOrEmpty(vektor))
        {
            int nVektor = -1;
            if (!Int32.TryParse(vektor, out nVektor))
            {
                //rossz vektor
                arguments.IsValid = false;
                return;
            }

            int nAdoszam = -1;
            if (!Int32.TryParse(adoszam1, out nAdoszam))
            {
                //rossz adoszam
                arguments.IsValid = false;
                return;
            }

            int length = vektor.Length;
            if (adoszam1.Length != adoszamLength) {
                //hibas adoszam hossz
                arguments.IsValid = false;
                return;
            }


            if (!Char.IsDigit(adoszam[length]))
            {
                // hibas adoszam formatum
                //return Utility.AdoszamError.badAdoszamFormat;
                arguments.IsValid = false;
                return;
            }

            int checkSum = Int32.Parse(adoszam.Substring(length));

            int c = 0;

            for (int i = 0; i < length; i++) {
                int a = -1;
                if (!Int32.TryParse(adoszam.Substring(i, 1), out a))
                {
                    // hibas adoszam formatum
                    //return Utility.AdoszamError.badAdoszamFormat;
                    arguments.IsValid = false;
                    return;
                }
                int b = -1;
                if (!Int32.TryParse(vektor.Substring(i, 1), out b))
                {
                    // hibas vektor formatum
                    //return Utility.AdoszamError.badVektorFormat;
                    arguments.IsValid = false;
                    return;
                }

                c += a * b;
            }

            //Ad�sz�m ellen�rz� sz�m (8. jegy) k�pz�se: jegy(8) = 10 - MOD10 (SUM (jegy(i)*vekt(i))) 
            // (i=1 - 7)  vekt=9731973
            c = (10 - (c % 10)) % 10;

            if (checkSum != c) {
                // hibas ellenorzo osszeg
                //return Utility.AdoszamError.chekSumError;
                arguments.IsValid = false;
                return;
            }
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
 
        // Lekell tiltani a ClientValidatort
        Validator1.Enabled = false;
        CustomValidator_Format.Enabled = false;

        return componentList;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);

            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    c.Visible = Validate;
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.AdoszamBehavior", this.TextBox.ClientID);
        string CheckVektor = Rendszerparameterek.Get(Page, Rendszerparameterek.ADOSZAM_ELLENORZO_VEKTOR);
        if (!String.IsNullOrEmpty(CheckVektor))
            descriptor.AddProperty("CheckVektor", CheckVektor);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("ChecksumValidatorId", CustomValidator_Format.ClientID);
        descriptor.AddProperty("ChecksumValidatorCalloutId", ValidatorCalloutExtender_Format.ClientID);
        descriptor.AddProperty("AdoszamHiddenFieldId", HiddenField.ClientID);
        descriptor.AddProperty("IsEightCharsFormatEnabled", isEightCharsFormatEnabled);
        descriptor.AddProperty("KulfoldiAdoszamCheckBoxId", cbKulfoldi.ClientID);

        descriptor.AddProperty("UserId", FelhasznaloProfil.FelhasznaloId(Page));
        descriptor.AddProperty("LoginId", FelhasznaloProfil.LoginUserId(Page));

        if (this._PartnerTextBox != null)
        {
            descriptor.AddProperty("PartnerBehaviorId", _PartnerTextBox.BehaviorID);
        }

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AdoszamBehavior.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
