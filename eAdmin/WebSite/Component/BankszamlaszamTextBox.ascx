<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BankszamlaszamTextBox.ascx.cs"
    Inherits="Component_BankszamlaszamTextBox" %>
    
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@Reference Control="EditablePartnerTextBox.ascx" %>
<div class="DisableWrap">
    <asp:TextBox ID="TextBox1" runat="server" MaxLength="8" CssClass="mrUrlapInputNumber8Digits" Enabled="true" />
    <asp:Label ID="lbKotojel1" runat="server" Text="-" />
    <asp:TextBox ID="TextBox2" runat="server" MaxLength="8" CssClass="mrUrlapInputNumber8Digits" Enabled="true" />
    <asp:Label ID="lbKotojel2" runat="server" Text="-" />
    <asp:TextBox ID="TextBox3" runat="server" MaxLength="8" CssClass="mrUrlapInputNumber8Digits" Enabled="true" />
    <ajaxToolkit:FilteredTextBoxExtender ID="fteNumber1" runat="server" TargetControlID="TextBox1" FilterType="Custom" ValidChars="0123456789" />
    <ajaxToolkit:FilteredTextBoxExtender ID="fteNumber2" runat="server" TargetControlID="TextBox2" FilterType="Custom" ValidChars="0123456789" />
    <ajaxToolkit:FilteredTextBoxExtender ID="fteNumber3" runat="server" TargetControlID="TextBox3" FilterType="Custom" ValidChars="0123456789" />

<asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')" 
 CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" />
 <asp:ImageButton TabIndex = "-1" ID="NewImageButton" runat="server" CssClass="mrUrlapInputImageButton" ImageUrl="~/images/hu/lov/hozzaad.gif" onmouseover="swapByName(this.id,'hozzaad_keret.gif')" onmouseout="swapByName(this.id,'hozzaad.gif')"
 AlternateText="�j" />
<asp:ImageButton TabIndex = "-1" 
    ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton" ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" />
<asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alap�llapot"/>
    
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="HiddenField_PartnerId" runat="server" />
    <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="TextBox1"
        Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="Validator1">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>

    <asp:CustomValidator ID="CustomValidator_Format" runat="server" ErrorMessage="A banksz�mlasz�m form�tuma nem megfelel�!"
        SetFocusOnError="true" EnableClientScript="true"
        ControlToValidate="TextBox3" Display="None" ValidateEmptyText="true" />
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Format" runat="server"
        TargetControlID="CustomValidator_Format">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    
   <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" MinimumPrefixLength="2" 
      TargetControlID="TextBox1" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
      ServicePath="http://localhost:100/eAdminWebService/KRT_BankszamlaszamokService.asmx" ServiceMethod="GetBankszamlaszamokList"
      CompletionListItemCssClass="GridViewRowStyle" CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle"
      ></ajaxToolkit:AutoCompleteExtender>
      
      <eUI:WorldJumpExtender ID="WorldJumpExtender1" runat="server" TargetControlID="TextBox1" AutoCompleteExtenderId="AutoCompleteExtender1" />
    
</div>
