﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Web.Script.Serialization;

public partial class Component_BizottsagTextBox : System.Web.UI.UserControl, IScriptControl
{
    #region public properties

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string Text
    {
        set { CsoportMegnevezes.Text = value; }
        get { return CsoportMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return CsoportMegnevezes; }
    }

    public string CssClass
    {
        get { return CsoportMegnevezes.CssClass; }
        set { CsoportMegnevezes.CssClass = value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    private string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public bool Enabled
    {
        set
        {
            CsoportMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return CsoportMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            CsoportMegnevezes.ReadOnly = value;
            LovImageButton.Visible = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;


        }
        get { return CsoportMegnevezes.ReadOnly; }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    private bool _tryFireChangeEvent = true;

    public bool TryFireChangeEvent
    {
        get { return _tryFireChangeEvent; }
        set { _tryFireChangeEvent = value; }
    }

    private bool _IsCustomTextEnabled = true;

    public bool IsCustomTextEnabled
    {
        get { return _IsCustomTextEnabled; }
        set { _IsCustomTextEnabled = value; }
    } 

    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    private string queryString_Lov = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();
        filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Bizottsag;

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
            WorldJumpEnabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            filterObject.FelhasznaloId = felh_Id;

            AutoCompleteExtender1.ContextKey = filterObject.JsSerialize();

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        queryString_Lov = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + CsoportMegnevezes.ClientID;

        if (_tryFireChangeEvent)
        {
            queryString_Lov += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        queryString_Lov += "&" + filterObject.QsSerialize();


        OnClick_Lov = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", queryString_Lov
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        if (ajaxEnabled)
        {
            //Manuális beírás esetén az id törlése
            string jsClearHiddenField = "var clear =true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
                + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

            TextBox.Attributes.Add("onchange", jsClearHiddenField);
        }
    }

    public void SetCsoportTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            // Csoport nevének lekérése a Cache-ből:

            String csoportNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Id_HiddenField, Page);

            if (csoportNev == null)
            {
                // hiba:
                errorPanel.Header = Resources.Error.ErrorLabel;
                errorPanel.Body = Resources.Error.ErrorText_CsoportTextBoxFill;
                errorPanel.Visible = true;
            }
            else
            {
                Text = csoportNev;
            }
        }
        else
        {
            Text = "";
        }
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CsoportAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("CustomTextEnabled", IsCustomTextEnabled);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}