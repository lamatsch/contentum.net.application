using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using System.Web.Script.Serialization;
using System.Text;
using System.Collections.Generic;

public partial class Component_CalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, IScriptControl
{
    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public AjaxControlToolkit.CalendarPosition PopupPosition
    {
        get { return CalendarExtender1.PopupPosition; }
        set { CalendarExtender1.PopupPosition = value; }
    }

    public bool bOpenDirectionTop
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
        }
    }

    public bool bOpenDirectionLeft
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
            }
        }
    }

    public bool TimeVisible
    {
        get
        {
            object o = ViewState["TimeVisible"];
            if (o != null)
                return (bool)o;
            return false;
        }
        set
        {
            ViewState["TimeVisible"] = value;
            TimePanel.Visible = value;
        }
    }

    private bool IsTimeEmpty
    {
        get
        {
            if (String.IsNullOrEmpty(HourTextBox.Text.ToString().Trim()))
            {
                return true;
            }
            else
            {
                if (String.IsNullOrEmpty(MinuteTextBox.Text.ToString().Trim()))
                {
                    MinuteTextBox.Text = "00";
                }
                return false;
            }
        }
    }

    public void SetDeafultTime()
    {
        if (TimeVisible && IsTimeEmpty)
        {
            HourTextBox.Text = "08";
            MinuteTextBox.Text = "00";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //SetDeafultTime();
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        if (TimeVisible)
        {
            //JavaScripts.RegisterCalendarTimeScript(Page, HourTextBox.ClientID, MinuteTextBox.ClientID, ArrowUpImageButton.ClientID, ArrowDownImageButton.ClientID);

            string vfHour = "ValidateHour_" + this.ID;
            string jsHour = @"function " + vfHour + @"(source, arguments)
                    {
                       var textBox = $get('" + HourTextBox.ClientID + @"');
                       if(textBox)
                       {
                          var text = textBox.value.trim();
                          if(text.length > 0)
                          {
                             var number = parseInt(text);
                             if(isNaN(number))
                             {
                                arguments.IsValid = false;
                             }
                             else
                             {
                                if(number > 24|| number < 0) arguments.IsValid = false;
                                else arguments.IsValid = true;
                             }
                          }
                          else
                          {
                             arguments.IsValid = true;
                          }
                       }
                       else
                       {
                          arguments.IsValid = true;
                       } 
                    }";

            ScriptManager.RegisterStartupScript(this, this.GetType(), vfHour, jsHour, true);
            HourCustomValidator.ClientValidationFunction = vfHour;
            HourCustomValidator.Enabled = true;

            string vfMinute = "ValidateMinute_" + this.ID;
            string jsMinute = @"function " + vfMinute + @"(source, arguments)
                    {
                       var textBox = $get('" + MinuteTextBox.ClientID + @"');
                       if(textBox)
                       {
                          var text = textBox.value.trim();
                          if(text.length > 0)
                          {
                             var number = parseInt(text);
                             if(isNaN(number))
                             {
                                arguments.IsValid = false;
                             }
                             else
                             {
                                if(number > 59 || number < 0) arguments.IsValid = false;
                                else arguments.IsValid = true;
                             }
                          }
                          else
                          {
                             arguments.IsValid = true;
                          }
                       }
                       else
                       {
                          arguments.IsValid = true;
                       }     
                    }";

            ScriptManager.RegisterStartupScript(this, this.GetType(), vfMinute, jsMinute, true);
            MinuteCustomValidator.ClientValidationFunction = vfMinute;
            MinuteCustomValidator.Enabled = true;
        }
        else
        {
            HourCustomValidator.Enabled = false;
            MinuteCustomValidator.Enabled = false;
        }

    }

    protected void DateFormatValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string inputValue = args.Value;
        DateTime inputDate;
        if (!DateTime.TryParse(inputValue, out inputDate))
        {
            args.IsValid = false;
        }
    }

    public void SetToday()
    {
        DateTime now = DateTime.Now;
        //LZS - BUG_6373
        DateTextBox.Text = now.ToString("yyyy.MM.dd."); 

        if (TimeVisible)
        {
            HourTextBox.Text = now.Hour.ToString("D2");
            MinuteTextBox.Text = now.Minute.ToString("D2");
        }
    }

    public void SetTodayAndTime()
    {
        if (!TimeVisible)
        {
            //LZS - BUG_6373
            DateTextBox.Text = DateTime.Now.ToString("yyyy.MM.dd.");
        }
        else
        {
            DateTime now = DateTime.Now;
            //LZS - BUG_6373
            DateTextBox.Text = DateTime.Now.ToString("yyyy.MM.dd.");
            HourTextBox.Text = now.Hour.ToString("D2");
            MinuteTextBox.Text = now.Minute.ToString("D2");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Enabled || ReadOnly)
        {
            DateFormatValidator.Enabled = false;
            DateTimeCompareValidator1.Enabled = false;
            RequiredFieldValidator1.Enabled = false;
        }

        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }

    #region public properties

    /*    public bool ReadOnly
        {
            get { return DateTextBox.ReadOnly; }
            set 
            { 
                DateTextBox.ReadOnly = value;
                if (value == true)
                {
                    // readonly
                    CalendarImageButton1.Enabled = false;
                    //DateTextBox.CssClass = "mrUrlapCalendarDis";
                }
                else
                {
                    CalendarImageButton1.Enabled = true;
                    //DateTextBox.CssClass = "mrUrlapCalendar";
                }
            }
        }*/

    public bool ReadOnly
    {
        get { return DateTextBox.ReadOnly; }
        set
        {
            DateTextBox.ReadOnly = value;
            HourTextBox.ReadOnly = value;
            MinuteTextBox.ReadOnly = value;
            ArrowUpImageButton.Enabled = !value;
            ArrowDownImageButton.Enabled = !value;
            HourCustomValidator.Enabled = !value;
            MinuteCustomValidator.Enabled = !value;
            CalendarImageButton1.Enabled = !value;
            //Validate = !value;
            if (value)
            {
                UI.AddCssClass(CalendarImageButton1, "disabledCalendarImage");
                UI.AddCssClass(ArrowUpImageButton, "disabledCalendarImage");
                UI.AddCssClass(ArrowDownImageButton, "disabledCalendarImage");
            }
            else
            {
                UI.RemoveCssClass(CalendarImageButton1, "disabledCalendarImage");
                UI.RemoveCssClass(ArrowUpImageButton, "disabledCalendarImage");
                UI.RemoveCssClass(ArrowDownImageButton, "disabledCalendarImage");
            }
        }
    }

    Boolean _Enabled = true;

    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;

            DateTextBox.Enabled = _Enabled;
            HourTextBox.Enabled = _Enabled;
            MinuteTextBox.Enabled = _Enabled;
            ArrowUpImageButton.Enabled = _Enabled;
            ArrowDownImageButton.Enabled = _Enabled;
            HourCustomValidator.Enabled = _Enabled;
            MinuteCustomValidator.Enabled = _Enabled;
            CalendarImageButton1.Enabled = _Enabled;
            //Validate = _Enabled;
            if (!value)
            {
                CalendarImageButton1.CssClass = "disabledCalendarImage";
                ArrowDownImageButton.CssClass += " disabledCalendarImage";
                ArrowUpImageButton.CssClass += " disabledCalendarImage";
            }
        }
    }

    public DateTime SelectedDate
    {
        get
        {
            DateTime dt = DateTime.MinValue;
            if (!String.IsNullOrEmpty(DateTextBox.Text))
            {
                string time = DateTextBox.Text;
                if (TimeVisible && !IsTimeEmpty)
                {
                    time += " " + HourTextBox.Text + ":" + MinuteTextBox.Text;
                }
                DateTime.TryParse(time, out dt);
            }
            return dt;
        }
        set
        {
            if (value != DateTime.MinValue)
            {
                DateTextBox.Text = value.ToString("yyyy.MM.dd.");

                if (TimeVisible)
                {
                    HourTextBox.Text = value.Hour.ToString();
                    MinuteTextBox.Text = value.Minute.ToString();
                }
                else
                {
                    HourTextBox.Text = "";
                    MinuteTextBox.Text = "";
                }
            }
            else
            {
                DateTextBox.Text = "";
                HourTextBox.Text = "";
                MinuteTextBox.Text = "";
            }
        }
    }

    public String SelectedDateText
    {
        get
        {
            if (TimeVisible && !IsTimeEmpty)
            {
                return this.SelectedDate.ToString();
            }
            else
            {
                if (SelectedDate != DateTime.MinValue)
                {
                    return this.SelectedDate.ToString("yyyy.MM.dd.");
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }


    private string initHourValueWhenEmpty;

    public string InitHourValueWhenEmpty
    {
        get { return initHourValueWhenEmpty; }
        set { initHourValueWhenEmpty = value; }
    }

    private string initMinuteValueWhenEmpty;

    public string InitMinuteValueWhenEmpty
    {
        get { return initMinuteValueWhenEmpty; }
        set { initMinuteValueWhenEmpty = value; }
    }

    public string Text
    {
        get
        {
            if (!String.IsNullOrEmpty(DateTextBox.Text))
            {
                try
                {
                    string time = DateTextBox.Text;
                    if (TimeVisible)
                    {
                        if (!IsTimeEmpty)
                        {
                            time += " " + HourTextBox.Text + ":" + MinuteTextBox.Text;
                        }
                        else if (!string.IsNullOrEmpty(InitHourValueWhenEmpty))
                        {
                            time += " " + InitHourValueWhenEmpty + ":" + InitMinuteValueWhenEmpty ?? "00";
                        }
                    }
                    DateTime dt = DateTime.Parse(time);
                    // KA: Ha az �ra:perc is enged�lyezett, akkor ne �rjuk fel�l a mai nap 00:00 id�pontot DateTime.Now-ra:
                    if (dt == DateTime.Today && !this.TimeVisible)
                        return DateTime.Now.ToString();
                    else
                    {
                        if (TimeVisible)
                            return dt.ToString();
                        else
                            return dt.ToString("yyyy.MM.dd.");

                    }
                }
                catch (FormatException)
                {
                    return DateTextBox.Text;
                }
            }
            else return "";
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    DateTextBox.Text = dt.ToString("yyyy.MM.dd.");

                    if (TimeVisible)
                    {
                        HourTextBox.Text = dt.Hour.ToString("D2");
                        MinuteTextBox.Text = dt.Minute.ToString("D2");
                    }
                }
                catch (FormatException)
                {
                    DateTextBox.Text = value;
                }
            }
            else
            {
                DateTextBox.Text = String.Empty;
                HourTextBox.Text = String.Empty;
                MinuteTextBox.Text = String.Empty;
            }
        }
    }

    public ImageButton CalendarImage
    {
        get { return CalendarImageButton1; }
    }

    public ImageButton ArrowUpImage
    {
        get { return ArrowUpImageButton; }
    }

    public ImageButton ArrowDownImage
    {
        get { return ArrowDownImageButton; }
    }

    public TextBox TextBox
    {
        get { return DateTextBox; }
    }

    public TextBox GetHourTextBox
    {
        get { return this.HourTextBox; }
    }

    public TextBox GetMinuteTextBox
    {
        get { return this.MinuteTextBox; }
    }

    public bool Validate
    {
        set { RequiredFieldValidator1.Enabled = value; }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return RequiredFieldValidator1;
        }
    }

    public string ValidationGroup
    {
        set
        {
            RequiredFieldValidator1.ValidationGroup = value;
            DateFormatValidator.ValidationGroup = value;
            DateTimeCompareValidator1.ValidationGroup = value;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                DateTextBox.CssClass += " ViewReadOnlyWebControl";
                CalendarImageButton1.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                DateTextBox.CssClass += " ViewDisabledWebControl";
                CalendarImageButton1.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    public void EnableCompareValidation()
    {
        DateTimeCompareValidator1.Enabled = true;
    }

    #endregion public properties

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(DateTextBox);
        componentList.Add(CalendarImageButton1);
        componentList.Add(HourTextBox);
        componentList.Add(MinuteTextBox);
        componentList.Add(ArrowUpImageButton);
        componentList.Add(ArrowDownImageButton);

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        DateFormatValidator.Enabled = false;
        DateTimeCompareValidator1.Enabled = false;
        MinuteCustomValidator.Enabled = false;

        CalendarExtender1.Enabled = false;
        CalendarImageButton1.OnClientClick = "";

        return componentList;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;
    private bool ajaxEnabled = true;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled && TimeVisible)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (!ajaxEnabled || !TimeVisible)
            return null;

        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.Time", this.DateTextBox.ClientID);
        descriptor.AddProperty("HourTextBoxClientId", this.HourTextBox.ClientID);
        descriptor.AddProperty("MinuteTextBoxClientId", this.MinuteTextBox.ClientID);
        descriptor.AddProperty("ArrowUpClientId", this.ArrowUpImageButton.ClientID);
        descriptor.AddProperty("ArrowDownClientId", this.ArrowDownImageButton.ClientID);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        List<ScriptReference> listOfScripRefernces = new List<ScriptReference>();

        if (TimeVisible)
        {
            ScriptReference referenceTime = new ScriptReference();
            referenceTime.Path = "~/JavaScripts/Time.js";
            listOfScripRefernces.Add(referenceTime);
        }

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Calendar.js";
        listOfScripRefernces.Add(reference);

        return listOfScripRefernces.ToArray();
    }

    #endregion

}
