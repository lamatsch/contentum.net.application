using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Text;

public partial class Component_CimekTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(KRT_Cimek krt_cimek)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (krt_cimek.Tipus)
            {
                case KodTarak.Cim_Tipus.Postai:
                    cim.Add(krt_cimek.OrszagNev, delimeter);
                    cim.Add(krt_cimek.IRSZ, delimeter);
                    cim.Add(krt_cimek.TelepulesNev, delimeter);
                    // BLG_1347
                    //cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                    //cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                    if (String.IsNullOrEmpty(krt_cimek.Hazszam) && !String.IsNullOrEmpty(krt_cimek.HRSZ))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(krt_cimek.HRSZ, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                        cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                        string hazszam = krt_cimek.Hazszam;
                        string hazszamIg = krt_cimek.Hazszamig;
                        string hazszamBetujel = krt_cimek.HazszamBetujel;
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = krt_cimek.Lepcsohaz;
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = krt_cimek.Szint;
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = krt_cimek.Ajto;
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = krt_cimek.AjtoBetujel;
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = krt_cimek.CimTobbi;
                    cim.Add(Cim, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception)
        {
            return "";
        }

        return text.ToString();
    }

    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox1.ValidationGroup = value;
        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }


    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { TextBox1.Text = value; }
        get { return TextBox1.Text; }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TextBox1.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return TextBox1.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly= !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetCimekTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Cimek krt_cimek = (KRT_Cimek)result.Record;
                if (krt_cimek == null)
                {
                    Text = ""; // ha nem talal cim recordot
                }
                else
                {
                    Text = GetAppendedCim(krt_cimek);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, CsoportMegnevezes.ClientID);   

        OnClick_Lov = JavaScripts.SetOnClientClick("CimekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID
                    , 900, 650, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("CimekForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "CimekForm.aspx", "", HiddenField1.ClientID);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
