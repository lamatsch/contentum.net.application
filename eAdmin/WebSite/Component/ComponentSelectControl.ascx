<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComponentSelectControl.ascx.cs" Inherits="ComponentSelectControl" %>
<%@ Register Src="MuveletTextBox.ascx" TagName="MuveletTextBox" TagPrefix="uc5" %>
<%@ Register Src="RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc1" %>
<%@ Register Src="CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc2" %>
<%@ Register Src="CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="FormFooter.ascx" TagName="FromFooter" TagPrefix="uc0" %>


<asp:Panel ID="MainPanel" runat="server">
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" BorderColor="Red" runat="server" Visible="true">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:HiddenField ID="HiddenField_Ver" runat="server" />
    <asp:HiddenField ID="HiddenField_NezetekId" runat="server" />
    <asp:HiddenField ID="HiddenField_FormId" runat="server" />
    <asp:HiddenField ID="HiddenField_readOnlyComponents" runat="server" />
    <asp:HiddenField ID="HiddenField_disabledComponents" runat="server" />
    <asp:HiddenField ID="HiddenField_invisibleComponents" runat="server" />
    <asp:HiddenField ID="HiddenField_userComponents" runat="server" />
    <eUI:eFormPanel ID="EFormPanel1" runat="server" Width="100%" Enabled="False" Visible="False">
        <table>
            <tr>
                <td style="text-align: left; vertical-align: top; width: 0px;">
                    <asp:ImageButton runat="server" ID="ComponentSelectControlCPEButton" ImageUrl="~/images/hu/Grid/minus.gif"
                        OnClientClick="return false;" />
                </td>
                <td>
                    <table style="width: 90%;">
                        <tr>
                            <td style="vertical-align: bottom; text-align: left;">
                                <asp:ImageButton runat="server" ImageUrl="~/images/hu/trapezgomb/uj_trap.jpg" ID="newButton"
                                    OnClick="newButton_Click" CommandName="New" AlternateText="�j" onmouseover="swapByName(this.id,'uj_trap2.jpg')"
                                    onmouseout="swapByName(this.id,'uj_trap.jpg')" CausesValidation="false" />
                                <asp:ImageButton runat="server" ImageUrl="../images/hu/trapezgomb/ervenytelenites_trap.jpg"
                                    ID="invalidateButton" OnClick="invalidateButton_Click" AlternateText="�rv�nytelen�t�s"
                                    onmouseover="swapByName(this.id,'ervenytelenites_trap2.jpg')" onmouseout="swapByName(this.id,'ervenytelenites_trap.jpg')" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ajaxToolkit:CollapsiblePanelExtender ID="NezetekCPE" runat="server" TargetControlID="Panel1"
                                    CollapsedSize="0" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                    AutoExpand="false" ExpandedSize="100" ScrollContents="true">
                                </ajaxToolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="Panel1" runat="server" BackColor="White">
                                    <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                <asp:GridView ID="NezetekGridView" runat="server" OnRowCommand="NezetekGridView_RowCommand"
                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="NezetekGridView_PreRender"
                                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="NezetekGridView_Sorting"
                                                    OnRowDataBound="NezetekGridView_RowDataBound">
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                    <Columns>
                                                        <asp:TemplateField Visible="false">
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../../~/images/hu/egyeb/kispipa.jpg"
                                                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                &nbsp;&nbsp;
                                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../~/images/hu/egyeb/kisiksz.jpg"
                                                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                            SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField>
                                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                            SortExpression="Nev" HeaderStyle-Width="200px" />
                                                        <asp:BoundField DataField="Muvelet_Nev" HeaderText="M�velet" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                            SortExpression="Muvelet_Nev" HeaderStyle-Width="200px" />
                                                        <asp:BoundField DataField="Prioritas" HeaderText="Prior�t�s" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                            SortExpression="Prioritas" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 90%;">
                        <tr>
                            <td colspan="2" style="height: 20px; text-align: left; vertical-align: top; border-bottom: solid 1px Gray;">
                                <asp:RadioButtonList ID="RadioButtonList_componentRights" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1">Eredeti&nbsp;�llapot</asp:ListItem>
                                    <asp:ListItem Value="2">Csak&nbsp;olvashat�</asp:ListItem>
                                    <asp:ListItem Value="3">Letiltott</asp:ListItem>
                                    <asp:ListItem Value="4">Nem&nbsp;l�that�</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="height: 26px; text-align: right; vertical-align: middle;">
                                <asp:Button ID="saveButton" runat="server" Text="L�trehoz�s" OnClick="saveButton_Click"
                                    CommandName="Modify" ValidationGroup="ComponentSelectControl" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px; text-align: left;">
                                <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="newView_Label" runat="server" Text="N�v:" Visible="True"></asp:Label>
                                <br />
                                <uc1:RequiredTextBox ID="NevTextBox" runat="server" ValidationGroup="ComponentSelectControl" />
                            </td>
                            <td style="height: 20px; width: 120px; text-align: left;">
                                <div class="DisableWrap">
                                    <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Text="Prior�t�s:" Visible="True"></asp:Label>
                                    <br />
                                    <uc4:RequiredNumberBox ID="PrioritasTextBox" runat="server" ValidationGroup="ComponentSelectControl" />
                                </div>
                            </td>
                            <td rowspan="2" style="height: 20px; text-align: right;">
                                <div class="DisableWrap" style="text-align: left;">
                                    <asp:Label ID="Label8" runat="server" Text="Le�r�s:" Visible="True"></asp:Label><br />
                                    <asp:TextBox ID="LeirasTextBox" runat="server" Text="" Width="170px" Rows="3" Height="80px"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px; text-align: left;">
                                <asp:Label ID="Label2" runat="server" Text="M�velet:" Visible="True"></asp:Label>
                                <br />
                                <uc5:MuveletTextBox ID="MuveletTextBox1" runat="server" />
                            </td>
                            <td style="height: 20px; text-align: left; vertical-align: top;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </eUI:eFormPanel>
</asp:Panel>
<ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
    VerticalSide="Bottom" VerticalOffset="0" HorizontalSide="Left" HorizontalOffset="0"
    ScrollEffectDuration=".1" Enabled="False" TargetControlID="MainPanel">
</ajaxToolkit:AlwaysVisibleControlExtender>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
    TargetControlID="EFormPanel1" CollapsedSize="20" Collapsed="False" ExpandControlID="ComponentSelectControlCPEButton"
    CollapseControlID="ComponentSelectControlCPEButton" ExpandDirection="Horizontal"
    AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="~/images/hu/Grid/plus.gif"
    ExpandedImage="~/images/hu/Grid/minus.gif" ImageControlID="ComponentSelectControlCPEButton">
</ajaxToolkit:CollapsiblePanelExtender>
