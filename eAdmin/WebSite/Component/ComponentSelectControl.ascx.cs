using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;

public partial class ComponentSelectControl : System.Web.UI.UserControl, IScriptControl, Contentum.eUtility.IComponentSelectControl
{

    UI ui = new UI(); 
    
    private const string viewStateKeyNameToDictionary = "ClientId_UniqueId_Pairs";
    private Dictionary<String, String> ClientId_UniqueId_Pairs = new Dictionary<string, string>();
    private List<string> ClickableComponentsClientIds = new List<string>();

    public static ComponentSelectControl GetCurrent(Page page)
    {
        if (page == null)
        {
            throw new ArgumentNullException("page");
        }

        return page.Items[typeof(ComponentSelectControl)] as ComponentSelectControl;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!DesignMode)
        {
            ComponentSelectControl existingInstance = ComponentSelectControl.GetCurrent(Page);

            if (existingInstance != null)
            {
                //???
            }
            else
            {

                Page.Items[typeof(ComponentSelectControl)] = this;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EErrorPanel1.Visible = false;

        RadioButtonList_componentRights.SelectedIndex = 1;

        EFormPanel1.Enabled = enabled;
        EFormPanel1.Visible = enabled;
        AlwaysVisibleControlExtender1.Enabled = enabled;
        
        // csak ha enged�lyezve van a komponens:
        if (enabled )
        {
            //registerJavascriptCodes(
            //    Page,
            //    (HtmlGenericControl)Page.Master.FindControl("masterBody"));
            

            if (IsPostBack)
            {
                ClientId_UniqueId_Pairs = GetClientIdUniqueIdFromViewState();
                //List<string> asfd = DisabledComponentsUniqueId_List;
            }
        }

        if (!IsPostBack) NezetekGridViewBind();

        invalidateButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        
        NevTextBox.TextBox.Width = 150;
        MuveletTextBox1.TextBox.Width = 150;
        PrioritasTextBox.TextBox.Width = 30;

        MuveletTextBox1.Validate = false;

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        SetClientIdUniqueIdToViewState();
    }

    // leteszi a kliensId-uniqueId �sszerendel�st a ViewState-be
    private void SetClientIdUniqueIdToViewState()
    {
        string str_dictionary = "";
        bool isFirstItem = true;
        foreach (KeyValuePair<string, string> kvp in ClientId_UniqueId_Pairs)
        {
            string clientId = kvp.Key;
            string uniqueId = kvp.Value;
            if (!isFirstItem)
            {
                str_dictionary += ";";
            }
            else
            {
                isFirstItem = false;
            }
            str_dictionary += clientId += "-" + uniqueId;
        }

        ViewState[viewStateKeyNameToDictionary] = str_dictionary;
    }

    private Dictionary<String, String> GetClientIdUniqueIdFromViewState()
    {
        Dictionary<String, String> clientIdUniqueIdDict = new Dictionary<string, string>();
        string str_dictionary = (String)ViewState[viewStateKeyNameToDictionary];
        if (!string.IsNullOrEmpty(str_dictionary))
        {
            string[] pairs = str_dictionary.Split(';');
            for (int i = 0; i < pairs.Length; i++)
            {
                string pair = pairs[i];
                string[] items = pair.Split('-');
                if (items.Length != 2)
                {
                    return null;
                }
                else
                {
                    string clientId = items[0].Trim();
                    string uniqueId = items[1].Trim();
                    clientIdUniqueIdDict.Add(clientId, uniqueId);
                }
            }
        }
        return clientIdUniqueIdDict;
    }

    private List<String> GetUniqueIdListFromClientIdStrings(string clientIds)
    {
        List<String> uniqueIdList = new List<string>();
        if (!String.IsNullOrEmpty(clientIds.Trim()))
        {
            string[] clientIdsArray = clientIds.Split(',');
            for (int i = 0; i < clientIdsArray.Length; i++)
            {
                string clientId = clientIdsArray[i];
                string uniqueId;
                if (ClientId_UniqueId_Pairs.TryGetValue(clientId, out uniqueId))
                {
                    uniqueIdList.Add(uniqueId);
                }
            }
        }
        return uniqueIdList;
    }

    private List<String> GetClientIdListFromUniqueIdStrings(string uniqueIds)
    {
        List<String> clientIdList = new List<string>();
        if (!String.IsNullOrEmpty(uniqueIds.Trim()))
        {
            string[] uniqueIdsArray = uniqueIds.Split(',');
            foreach (string uniqueId in uniqueIdsArray)
            {
                if (!String.IsNullOrEmpty(uniqueId.Trim()))
                {
                    string clientId = uniqueId.Replace("$", "_");
                    clientIdList.Add(clientId);
                }
            }
        }
        return clientIdList;
    }

    #region public properties

    private bool enabled = false;        
    public bool Enabled
    {
        get { return enabled; }
        set 
        {
            enabled = value;
            EFormPanel1.Enabled = enabled;
            EFormPanel1.Visible = enabled;
            AlwaysVisibleControlExtender1.Enabled = enabled;
        }
    }


    /// <summary>
    /// disabled st�tusz� komponensek id-jainak list�ja
    /// </summary>
    public string DisabledComponents_str
    {
        get { return HiddenField_disabledComponents.Value; }
        set { HiddenField_disabledComponents.Value = value; }
    }
    /// <summary>
    /// disabled st�tusz� komponensek uniqueId-jainak list�ja
    /// </summary>
    public List<string> DisabledComponentsUniqueId_List
    {
        get { return GetUniqueIdListFromClientIdStrings(HiddenField_disabledComponents.Value); }
    }

    /// <summary>
    /// readonly st�tusz� komponensek id-jainak list�ja
    /// </summary>
    public string ReadonlyComponents_str
    {
        get { return HiddenField_readOnlyComponents.Value; }
        set { HiddenField_readOnlyComponents.Value = value; }
    }

    /// <summary>
    /// readonly st�tusz� komponensek uniqueId-jainak list�ja
    /// </summary>
    public List<string> ReadonlyComponentsUniqueId_List
    {
        get { return GetUniqueIdListFromClientIdStrings(HiddenField_readOnlyComponents.Value); }
    }

    /// <summary>
    /// (get) HiddenField_readOnlyComponents komponens kliens oldali id-ja
    /// </summary>
    public string Id_HiddenField_ReadOnly
    {
        get { return HiddenField_readOnlyComponents.ClientID; }
    }

    /// <summary>
    /// (get) HiddenField_disabledComponents komponens kliens oldali id-ja
    /// </summary>
    public string Id_HiddenField_Disabled
    {
        get { return HiddenField_disabledComponents.ClientID; }
    }

    /// <summary>
    /// (get) combo_componentRights komponens kliens oldali id-ja
    /// </summary>
    public string Id_ComponentRights
    {
        //get { return combo_componentRights.ClientID; }
        get { return RadioButtonList_componentRights.ClientID; }
    }

    /// <summary>
    /// Komponensek kliensoldali id-jainak list�ja, amelyekre be lett 
    /// �ll�tva az onclick() attr., vagyis amelyek hozz�f�rhet�s�ge
    /// v�ltoztathat� a n�zetben
    /// </summary>
    private List<string> list_SelectableComponents = new List<string>();

    public List<string> List_SelectableComponents
    {
        get { return list_SelectableComponents; }
    }

    public string InvisibleComponents_str
    {
        get { return HiddenField_invisibleComponents.Value; }
        set { HiddenField_invisibleComponents.Value = value; }
    }

    public List<string> InvisibleComponentsUniqueId_List
    {
        get { return GetUniqueIdListFromClientIdStrings(HiddenField_invisibleComponents.Value); }
    }


    #endregion

    public void Add_ComponentOnClick(Contentum.eUtility.ISelectableUserComponent userComponent)
    {
        List<WebControl> webControlList = userComponent.GetComponentList();
        string hf_userComponents = userComponent.ClientID;
        bool isFirstItem = true;

        foreach (WebControl webControl in webControlList)
        {
            ClickableComponentsClientIds.Add(GetWebControlClientID(webControl));
            if (isFirstItem)
            {
                hf_userComponents += ":";
                isFirstItem = false;
            }
            else
            {
                hf_userComponents += ",";
            }
            hf_userComponents += GetWebControlClientID(webControl);
        }
        if (!String.IsNullOrEmpty(HiddenField_userComponents.Value))
        {
            HiddenField_userComponents.Value += ";" + hf_userComponents;
        }
        else
        {
            HiddenField_userComponents.Value += hf_userComponents;
        }

        Add_ComponentOnClick(userComponent as Control, userComponent.ClientID, userComponent.UniqueID);
    }

    /// <summary>
    /// onclick() attr. hozz�ad�sa az adott webkomponenshez
    /// </summary>
    /// <param name="component"></param>
    public void Add_ComponentOnClick(System.Web.UI.WebControls.WebControl webControl)
    {
        Add_ComponentOnClick(webControl, GetWebControlClientID(webControl), webControl.UniqueID);
    }

    private string GetWebControlClientID(WebControl webControl)
    {
        if (webControl is Contentum.eUIControls.eDropDownList)
        {
            return (webControl as Contentum.eUIControls.eDropDownList).ReadOnlyClientID;
        }
        return webControl.ClientID;
    }

    public void Add_ComponentOnClick(HtmlControl htmlControl)
    {
        Add_ComponentOnClick(htmlControl, htmlControl.ClientID, htmlControl.UniqueID);

        if (htmlControl is HtmlTableRow)
        {
            UI.AddCssClass(htmlControl,"Component_TableRow");
        }

        if (htmlControl is HtmlTable)
        {
            UI.AddCssClass(htmlControl, "Component_Table");
        }
    }

    private void Add_ComponentOnClick(Control control, string clientID, string uniqueID)
    {
        control.Visible = true;

        if (control is Contentum.eUtility.ISelectableUserComponent)
        {
        }
        else
        {
            ClickableComponentsClientIds.Add(clientID);
        }

        list_SelectableComponents.Add(clientID);

        ClientId_UniqueId_Pairs.Add(clientID, uniqueID);
    }

    public void Add_ComponentOnClick(Contentum.eUtility.ISelectableUserComponentContainer componentContainer)
    {
        List<Control> componentList = componentContainer.GetComponentList();
        (componentContainer as Control).Visible = true;
        Add_ComponentOnClick(componentList);
    }

    public void Add_ComponentChildsOnClick(Control component)
    {
        Add_ComponentChildsOnClick(component, null);
    }

    public void Add_ComponentChildsOnClick(Control component, List<Control> excludedComponents)
    {
        if (component != null && component.Controls != null)
        {
            List<Control> componentList = new List<Control>(PageView.GetSelectableChildComponents(component.Controls, excludedComponents));
            Add_ComponentOnClick(componentList);
        }
    }

    public void Add_ComponentOnClick(List<Control> componentList)
    {
        if (componentList != null)
        {
            foreach (Control component in componentList)
            {
                if (component is Contentum.eUtility.ISelectableUserComponentContainer)
                {
                    Add_ComponentOnClick(component as Contentum.eUtility.ISelectableUserComponentContainer);
                    continue;
                }
                if (component is Contentum.eUtility.ISelectableUserComponent)
                {
                    Add_ComponentOnClick(component as Contentum.eUtility.ISelectableUserComponent);
                    continue;
                }
                if (component is WebControl)
                {
                    Add_ComponentOnClick(component as WebControl);
                    continue;
                }

                if (component is HtmlControl)
                {
                    Add_ComponentOnClick(component as HtmlControl);
                    continue;
                }
            }
        }
    }


    protected void saveButton_Click(object sender, EventArgs e)
    {
        /*if (SaveButtonClick != null)
        {
            CommandEventArgs args = new CommandEventArgs("SaveButtonClick", sender);
            SaveButtonClick(sender, args);
        }*/

        if (String.IsNullOrEmpty(HiddenField_FormId.Value) 
            || String.IsNullOrEmpty(NevTextBox.Text) 
            || String.IsNullOrEmpty(PrioritasTextBox.Text))
        {
            return;
        }

        KRT_Nezetek nezetek = new KRT_Nezetek();
        nezetek.Updated.SetValueAll(false);
        nezetek.Base.Updated.SetValueAll(false);

        nezetek.Form_Id = HiddenField_FormId.Value;
        nezetek.Updated.Form_Id = true;
        nezetek.Nev = NevTextBox.Text;
        nezetek.Updated.Nev = true;
        if (String.IsNullOrEmpty(MuveletTextBox1.Id_HiddenField))
        {
            nezetek.Muvelet_Id = "<null>";
        }
        else
        {
            nezetek.Muvelet_Id = MuveletTextBox1.Id_HiddenField;
        }
        nezetek.Updated.Muvelet_Id = true;
        nezetek.Prioritas = PrioritasTextBox.Text;
        nezetek.Updated.Prioritas = true;

        nezetek.Leiras = LeirasTextBox.Text;
        nezetek.Updated.Leiras = true;

        nezetek.ReadOnlyControls = String.Join(",",ReadonlyComponentsUniqueId_List.ToArray());
        nezetek.Updated.ReadOnlyControls = true;
        nezetek.DisableControls = String.Join(",",DisabledComponentsUniqueId_List.ToArray());
        nezetek.Updated.DisableControls = true;
        nezetek.InvisibleControls = String.Join(",", InvisibleComponentsUniqueId_List.ToArray());
        nezetek.Updated.InvisibleControls = true;

        KRT_NezetekService nezetekservice = eAdminService.ServiceFactory.GetKRT_NezetekService();

        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

        Result _res = new Result();

        if (String.IsNullOrEmpty(HiddenField_NezetekId.Value))
        {
            // Uj rekord felvetel
            _res = nezetekservice.Insert(execparam, nezetek);
        }
        else
        { 
            // Modositas
            nezetek.Base.Ver = HiddenField_Ver.Value;
            nezetek.Base.Updated.Ver = true;
            
            execparam.Record_Id = HiddenField_NezetekId.Value;
            _res = nezetekservice.Update(execparam, nezetek);
        }

        if (!String.IsNullOrEmpty(_res.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, _res);
        }
        else
        {
            PageView.NezetekCache.RemoveNezetek(Page);
            NezetekGridViewBind();
            ClearComponents();
        }

    }

    protected void NezetekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {

    }
    protected void NezetekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void NezetekGridView_PreRender(object sender, EventArgs e)
    {

    }
    protected void NezetekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            string id = (sender as GridView).DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString();            

            KRT_NezetekService nezetekservice = eAdminService.ServiceFactory.GetKRT_NezetekService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            
            ExecParam.Record_Id = id;

            // Form:

            Result _res = nezetekservice.Get(ExecParam);

            if (!String.IsNullOrEmpty(_res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, _res); 
                return;   
            }

            KRT_Nezetek nezetek = (KRT_Nezetek)_res.Record;

            HiddenField_NezetekId.Value = nezetek.Id;
            NevTextBox.Text = nezetek.Nev;
            MuveletTextBox1.Id_HiddenField = nezetek.Muvelet_Id;
            MuveletTextBox1.SetMuveletTextBoxById(EErrorPanel1);
            ReadonlyComponents_str = String.Join(",",GetClientIdListFromUniqueIdStrings(nezetek.ReadOnlyControls).ToArray());
            DisabledComponents_str = String.Join(",",GetClientIdListFromUniqueIdStrings(nezetek.DisableControls).ToArray());
            InvisibleComponents_str = String.Join(",", GetClientIdListFromUniqueIdStrings(nezetek.InvisibleControls).ToArray());
            PrioritasTextBox.Text = nezetek.Prioritas;
            LeirasTextBox.Text = nezetek.Leiras;
            HiddenField_Ver.Value = nezetek.Base.Ver;

            saveButton.Text = Resources.Buttons.Save;

            invalidateButton.OnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_Delete + " \\n \\n" + Resources.Question.UIisDelete + "\\n')) {  } else { return false; }"; 
            
        }
    }

    #region Master List

    protected void NezetekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("NezetekGridView", ViewState, "Prioritas");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("NezetekGridView", ViewState);

        NezetekGridViewBind(sortExpression, sortDirection);
    }

    protected void NezetekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(NezetekGridView);

        KRT_NezetekService nezetekservice = eAdminService.ServiceFactory.GetKRT_NezetekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        
        KRT_NezetekSearch nezeteksearch = (KRT_NezetekSearch)Search.GetSearchObject(Page, new KRT_NezetekSearch());
        nezeteksearch.OrderBy = Search.GetOrderBy("NezetekGridView", ViewState, SortExpression, SortDirection);

        nezeteksearch.TopRow = 0;

        // Form:
        KRT_ModulokSearch moduloksearch = new KRT_ModulokSearch();
        moduloksearch.Kod.Value = Request.Path.Substring(Request.RawUrl.LastIndexOf('/')+1);
        moduloksearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;
        
        KRT_ModulokService modulokservice = eAdminService.ServiceFactory.GetKRT_ModulokService();
        Result modulok_res = modulokservice.GetAll(ExecParam, moduloksearch);

        if (modulok_res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, modulok_res);
            return;
        }
        if (modulok_res.Ds.Tables[0].Rows.Count <= 0)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIPageNotModul);
            return;
        }

        String modulok_id = modulok_res.Ds.Tables[0].Rows[0]["Id"].ToString();

        nezeteksearch.Form_Id.Value = modulok_id;
        nezeteksearch.Form_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        HiddenField_FormId.Value = modulok_id;

        Result res = nezetekservice.GetAllWithFK(ExecParam, nezeteksearch);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            return;
        }

        ui.GridViewFill(NezetekGridView, res, "0", EErrorPanel1, ErrorUpdatePanel);
    }

    #endregion

    protected void newButton_Click(object sender, EventArgs e)
    {
        ClearComponents();
    }

    private void ClearComponents()
    {
        HiddenField_NezetekId.Value = "";
        NevTextBox.Text = "";
        MuveletTextBox1.Clear();
        PrioritasTextBox.Text = "";
        LeirasTextBox.Text = "";

        HiddenField_Ver.Value = "";
        ReadonlyComponents_str = "";
        DisabledComponents_str = "";
        ReadonlyComponents_str = String.Empty;
        saveButton.Text = Resources.Buttons.Create;        
    }
    protected void invalidateButton_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(HiddenField_NezetekId.Value)) return;

        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

        execparam.Record_Id = HiddenField_NezetekId.Value;

        KRT_NezetekService nezetekservice = eAdminService.ServiceFactory.GetKRT_NezetekService();
        Result nezetek_res = nezetekservice.Invalidate(execparam);
        if (!String.IsNullOrEmpty(nezetek_res.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, nezetek_res);
            return;
        }
        else
        {
            PageView.NezetekCache.RemoveNezetek(Page);
            NezetekGridViewBind();
            ClearComponents();
        }        
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {

        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }
        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (!enabled) return null;

        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.ComponentSelect", this.EFormPanel1.ClientID);
        descriptor.AddProperty("HiddenField_readOnlyComponents_id", Id_HiddenField_ReadOnly);
        descriptor.AddProperty("HiddenField_disabledComponents_id", Id_HiddenField_Disabled);
        descriptor.AddProperty("HiddenField_invisibleComponents_id", HiddenField_invisibleComponents.ClientID);
        descriptor.AddProperty("HiddenField_userComponents_id", HiddenField_userComponents.ClientID);
        descriptor.AddProperty("Id_componentRights", Id_ComponentRights);
        descriptor.AddProperty("ClickableComponentsClientIds", ClickableComponentsClientIds);
        descriptor.AddProperty("SaveButtonClientId", saveButton.ClientID);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!enabled) return null;

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/ComponentSelect.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
