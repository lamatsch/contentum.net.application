using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Web.Script.Serialization;

public partial class Component_CsoportTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    

    #region public properties


    private string filter;

    public string Filter
    {
        get { return filter; }
        set { filter = value; }
    }

    private bool szervezetCsoport;

    public bool SzervezetCsoport
    {
        get { return szervezetCsoport; }
        set { if (value == true) { dolgozoCsoport = false; } szervezetCsoport = value; }
    }

    private bool dolgozoCsoport;

    public bool DolgozoCsoport
    {
        get { return dolgozoCsoport; }
        set { if (value == true) { szervezetCsoport = false; }  dolgozoCsoport = value; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }


    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            CsoportMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { CsoportMegnevezes.Text = value; }
        get { return CsoportMegnevezes.Text; }    
    }

    public TextBox TextBox
    {
        get { return CsoportMegnevezes; }
    }

    public string BehaviorID
    {
        get { return TextBox.ClientID + "_Behavior"; }
    }

    public string FilterBehaviorID
    {
        get { return TextBox.ClientID + "_FilterBehavior"; }
    }

    public string CssClass
    {
        get { return CsoportMegnevezes.CssClass;  }
        set { CsoportMegnevezes.CssClass = value;  }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    #region Szign�l�s t�pushoz k�t�d� viselked�s
    // ha szign�l�si t�pust�l f�gg�en v�ltozik a sz�r�s, ezt a containert
    // deaktiv�ljuk vagy t�ntetj�k el, ig�ny - azaz a a hideIfDisabled be�ll�t�sa - szerint
    private string containerId = String.Empty;
    public string ContainerId
    {
        get { return this.containerId; }
        set { this.containerId = value; }
    }

    private bool hideIfDisabled = false;
    public bool HideIfDisabled
    {
        get { return this.hideIfDisabled; }
        set { this.hideIfDisabled = value; }
    }
    #endregion Szign�l�s t�pushoz k�t�d� viselked�s

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool Enabled
    {
        set
        {
            CsoportMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return CsoportMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            CsoportMegnevezes.ReadOnly = value;
            LovImageButton.Visible = !value;
            NewImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;
            //if (value == true)
            //{
            //    LovImageButton.CssClass = "disabledLovListItem";
            //    NewImageButton.CssClass = "disabledLovListItem";
            //    ResetImageButton.CssClass = "disabledLovListItem";
            //}
            //else
            //{
            //    LovImageButton.CssClass = "mrUrlapInputImageButton";
            //    NewImageButton.CssClass = "mrUrlapInputImageButton";
            //    ResetImageButton.CssClass = "mrUrlapInputImageButton";
            //}


        }
        get { return CsoportMegnevezes.ReadOnly; }
    }


    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                CsoportMegnevezes.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                CsoportMegnevezes.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    string szervezetId = String.Empty;
    public void FilterBySzervezetId(string p_szervezetId)
    {
        if (String.IsNullOrEmpty(p_szervezetId))
        {
            szervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        }
        else
        {
            szervezetId = p_szervezetId;
        }
    }

    private bool _tryFireChangeEvent = true;

    public bool TryFireChangeEvent
    {
        get { return _tryFireChangeEvent; }
        set { _tryFireChangeEvent = value; }
    }

    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    private string queryString_Lov = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        if (!ReadOnly)
        {
            ResetImageButton.Visible = !Validate && !ViewMode;
        }

        Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();

        if (szervezetCsoport == true)
        {
            filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Szervezet;
        }
        else if (dolgozoCsoport == true)
        {
            filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo;
        }

        if (!String.IsNullOrEmpty(szervezetId))
        {
            filterObject.SzervezetId = szervezetId;
        }

        if (!String.IsNullOrEmpty(filter))
        {
            if (filter == Constants.FilterType.AllAtmenetiIrattar)
            {
                filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.AllAtmenetiIrattar;
            } else  filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Irattar;
            NewImageButton.Visible = false;
        }

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
            WorldJumpEnabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            filterObject.FelhasznaloId = felh_Id;

            AutoCompleteExtender1.ContextKey = filterObject.JsSerialize();

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        queryString_Lov = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + CsoportMegnevezes.ClientID;

        if (_tryFireChangeEvent)
        {
            queryString_Lov += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        queryString_Lov += "&" + filterObject.QsSerialize();


        OnClick_Lov = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", queryString_Lov
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("CsoportokForm.aspx"
           , CommandName.Command + "=" + CommandName.New
           + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + CsoportMegnevezes.ClientID
           , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "CsoportokForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';return false";

        if (ajaxEnabled)
        {
            //Manu�lis be�r�s eset�n az id t�rl�se
            string jsClearHiddenField = "var clear =true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
                + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

            TextBox.Attributes.Add("onchange", jsClearHiddenField);
        }
    }

    public void SetCsoportTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            // Csoport nev�nek lek�r�se a DB-b�l:
            String csoportNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportFromDB(Id_HiddenField, UI.SetExecParamDefault(Page)).Nev;

            if (csoportNev == null)
            {
                // hiba:
                errorPanel.Header = Resources.Error.ErrorLabel;
                errorPanel.Body = Resources.Error.ErrorText_CsoportTextBoxFill;
                errorPanel.Visible = true;
            }
            else
            {
                Text = csoportNev;
            }
        }
        else
        {
            Text = "";
        }
    }

    #region Filter

    private bool filterEnabled = false;
    private DropDownList filterDropDownList = null;

    public void SetFilterByIktatokonyv(DropDownList iktatokonyList)
    {
        if (iktatokonyList != null)
        {
            filterEnabled = true;
            filterDropDownList = iktatokonyList;
        }
    }

    private HiddenField filterHiddenField = null;

    public void SetFilterByIktatokonyv(HiddenField hf_iktatoKonyvId)
    {
        if (hf_iktatoKonyvId != null)
        {
            filterEnabled = true;
            filterHiddenField = hf_iktatoKonyvId;
        }
    }

    private DropDownList filterSzignalasDropDownList = null;

    public void SetFilterBySzignalasTipus(DropDownList szignalasList)
    {
        if (szignalasList != null)
        {
            filterEnabled = true;
            filterSzignalasDropDownList = szignalasList;
        }
    }

    #endregion

    public void Page_PreRender(object sender, EventArgs e)
    {
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(CsoportMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);

                if (filterEnabled)
                {
                    //query string dinamikus �ll�t�sa
                    Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();
                    filterObject.IktatokonyvId = "' + $find('" + this.FilterBehaviorID + "').get_iktatokonyvId() + '";
                    filterObject.SzignalasTipus = "' + $find('" + this.FilterBehaviorID  + "').get_szignalasTipus() + '";

                    queryString_Lov += "&" + filterObject.QsSerialize();

                    OnClick_Lov = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", queryString_Lov
                            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
                }
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CsoportAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("HiddenValue", "");

        if (filterEnabled)
        {
            ScriptControlDescriptor descriptorFilter = new ScriptControlDescriptor("Utility.CsoportTextBoxFilterBehavior", this.TextBox.ClientID);
            descriptorFilter.AddProperty("HiddenFieldClientId", HiddenField.ClientID);
            descriptorFilter.AddProperty("id", this.FilterBehaviorID);

            if (filterDropDownList != null)
            {
                descriptorFilter.AddProperty("ErkeztetoDropDownListClientId", this.filterDropDownList.ClientID);
            }
            if (filterHiddenField != null)
            {
                descriptorFilter.AddProperty("IktatoHiddenfieldClientId", this.filterHiddenField.ClientID);
            }

            if (filterSzignalasDropDownList != null)
            {
                descriptorFilter.AddProperty("SzignalasDropDownListClientId", this.filterSzignalasDropDownList.ClientID);
                if (!String.IsNullOrEmpty(this.containerId))
                {
                    descriptorFilter.AddProperty("SzervezetTextBoxContainerClientId", this.containerId);
                    descriptorFilter.AddProperty("HideSzervezetTextBoxContainerIfDisabled", this.hideIfDisabled);
                }
            }

            return new ScriptDescriptor[] { descriptor, descriptorFilter };
        }

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js";

        if (filterEnabled)
        {
            ScriptReference referenceFilter = new ScriptReference();
            referenceFilter.Path = "~/JavaScripts/CsoportTextBoxFilter.js";

            return new ScriptReference[] { reference, referenceFilter };
        }

        return new ScriptReference[] { reference };
    }

    #endregion
}
