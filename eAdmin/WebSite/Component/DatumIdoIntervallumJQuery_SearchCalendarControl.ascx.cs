﻿using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

/// <summary>
/// Datum intervallum komponens JQuery funkcionalitással
/// </summary>
public partial class Component_DatumIdoIntervallumJQuery_SearchCalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string DatumKezdValue
    {
        get
        {
            return this.DatumKezd.Text;
        }
        set
        {
            this.DatumKezd.Text = value;
        }
    }

    public string DatumVegeValue
    {
        get
        {
            return this.DatumVege.Text;
        }
        set
        {
            this.DatumVege.Text = value;
        }
    }

    #region ISelectableUserComponent method implementations

    Boolean _Enabled = true;
    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            DatumKezd.Enabled = _Enabled;
            DatumVege.Enabled = _Enabled;
        }
    }

    Boolean _Readonly = false;
    public Boolean Readonly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            DatumKezd.ReadOnly = _Readonly;
            DatumVege.ReadOnly = _Readonly;
        }
    }
    #endregion

    #region VIEW

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Readonly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                DatumKezd.CssClass += " ViewReadOnlyWebControl";
                DatumVege.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                DatumKezd.CssClass += " ViewDisabledWebControl";
                DatumVege.CssClass = "ViewDisabledWebControl";
            }
        }
    }
    #endregion

    #region ISearchComponent Members

    /// <summary>
    /// Datum kezdete és Datum vége beállítása a megadott mezőknek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        if (!String.IsNullOrEmpty(DatumKezdValue) || !String.IsNullOrEmpty(DatumVegeValue))
        {
            if (String.IsNullOrEmpty(DatumKezdValue))
            {
                SearchField.Value = DatumKezdValue;
                SearchField.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(DatumVegeValue))
            {
                SearchField.Value = DatumKezdValue;
                SearchField.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                SearchField.Value = DatumKezdValue;
                SearchField.ValueTo = DatumVegeValue;
                SearchField.Operator = Query.Operators.between;
            }
        }
    }

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;
        }
        return text;
    }

    public List<WebControl> GetComponentList()
    {
        throw new NotImplementedException();
    }

    #endregion
    #region VALIDATE
    public bool IsValid()
    {
        if (string.IsNullOrEmpty(DatumKezd.Text) && string.IsNullOrEmpty(DatumVege.Text))
            return true;
        else if (!string.IsNullOrEmpty(DatumKezd.Text) && !string.IsNullOrEmpty(DatumVege.Text))
        {
            DateTime dtS;
            if (!DateTime.TryParse(DatumKezd.Text, out dtS))
                return false;

            DateTime dtV;
            if (!DateTime.TryParse(DatumVege.Text, out dtV))
                return false;

            if (dtV < dtS)
                return false;
        }
        else if (!string.IsNullOrEmpty(DatumKezd.Text))
        {
            DateTime dt;
            if (!DateTime.TryParse(DatumKezd.Text, out dt))
                return false;
        }
        else
        {
            DateTime dt;
            if (!DateTime.TryParse(DatumVege.Text, out dt))
                return false;
        }
        return true;
    }
    #endregion
}
