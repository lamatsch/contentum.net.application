using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// D�tumkiv�laszt� keres�si felt�telek megad�s�hoz
/// </summary>
public partial class Component_DatumIntervallum_SearchCalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{

    /// <summary>
    /// Handles the Init event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    public bool AutoPostback = false;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Enabled)
        {
            datumKezd_masol_ImageButton.OnClientClick = "JavaScript: document.getElementById('" + DatumVege_TextBox.ClientID + "').value = " +
                " document.getElementById('" + DatumKezd_TextBox.ClientID + "').value; " +
                "return false;";
        }

        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        if (AutoPostback)
        {
            datumKezd_TextBox.AutoPostBack = true;
            datumVege_TextBox.AutoPostBack = true;
            datumVege_TextBox.TextChanged += new EventHandler(datumVege_TextBox_TextChanged);
            datumKezd_TextBox.TextChanged += new EventHandler(datumKezd_TextBox_TextChanged);
        }
    }


    public event EventHandler EvChanged;

    #region private and public properties for EvChanged
    private int _LastEvKezd
    {
        set { ViewState["EvKezd"] = value; }
        get
        {
            if (ViewState["EvKezd"] != null)
            {
                string vsErvKezd = ViewState["EvKezd"].ToString();
                int nEvKezd;
                if (Int32.TryParse(vsErvKezd, out nEvKezd))
                {
                    return nEvKezd;
                }
            }

            return -1;
        }
    }

    private int _LastEvVege
    {
        set { ViewState["EvVege"] = value; }
        get
        {
            if (ViewState["EvVege"] != null)
            {
                string vsErvVege = ViewState["EvVege"].ToString();
                int nEvVege;
                if (Int32.TryParse(vsErvVege, out nEvVege))
                {
                    return nEvVege;
                }
            }

            return -1;
        }
    }

    public int EvKezd
    {
        get
        {
            DateTime dtKezd;
            if (DateTime.TryParse(datumKezd_TextBox.Text, out dtKezd))
            {
                return dtKezd.Year;
            }
            return -1;
        }
    }

    public int EvVege
    {
        get
        {
            DateTime dtVege;
            if (DateTime.TryParse(datumVege_TextBox.Text, out dtVege))
            {
                return dtVege.Year;
            }
            return -1;
        }
    }

    public bool IsValid_EvKezd
    {
        get { return EvKezd > -1; }
    }

    public bool IsValid_EvVege
    {
        get { return EvVege > -1; }
    }
    #endregion private and public properties for EvChanged

    public event EventHandler DatumChanged;

    void datumKezd_TextBox_TextChanged(object sender, EventArgs e)
    {
        if (DatumChanged != null)
        {
            DatumChanged(sender, e);
        }

        if (EvChanged != null)
        {
            if (_LastEvKezd != this.EvKezd)
            {
                EvChanged(sender, e);
            }
        }

        _LastEvKezd = this.EvKezd;
    }

    void datumVege_TextBox_TextChanged(object sender, EventArgs e)
    {
        if (DatumChanged != null)
        {
            DatumChanged(sender, e);
        }

        if (EvChanged != null)
        {
            if (_LastEvVege != this.EvVege)
            {
                EvChanged(sender, e);
            }
        }

        _LastEvVege = this.EvVege;
        
    }

    /// <summary>
    /// Datum kezdete �s Datum v�ge be�ll�t�sa a megadott mez�knek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {

        if (AktualisEv_CheckBox.Checked == true)
        {
            DateTime evEleje = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime evVege = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            SearchField.Value = evEleje.ToString();
            SearchField.ValueTo = evVege.ToString();
            SearchField.Operator = Query.Operators.between;

        }
        else    /// Aktu�lis �v checkBox nincs bekattintva: 
        {
            if (!String.IsNullOrEmpty(DatumKezd) || !String.IsNullOrEmpty(DatumVege))
            {
                if (String.IsNullOrEmpty(DatumKezd))
                {
                    SearchField.Value = DatumVege;
                    SearchField.Operator = Query.Operators.lessorequal;
                }
                else if (String.IsNullOrEmpty(DatumVege))
                {
                    SearchField.Value = DatumKezd;
                    SearchField.Operator = Query.Operators.greaterorequal;
                }
                else
                {
                    SearchField.Value = DatumKezd;
                    SearchField.ValueTo = DatumVege;
                    SearchField.Operator = Query.Operators.between;
                }
            }
        }
    }

    /// <summary>
    /// TextBoxok be�ll�t�sa a keres�si objektum mez�je alapj�n
    /// </summary>
    /// <param name="SearchField"></param>
    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        if (SearchField.Operator == Query.Operators.lessorequal)
        {
            DatumVege = SearchField.Value;
        }
        else if (SearchField.Operator == Query.Operators.greaterorequal)
        {
            DatumKezd = SearchField.Value;
        }
        else
        {
            DatumKezd = SearchField.Value;
            DatumVege = SearchField.ValueTo;
        }
    }

    #region public properties

    /// <summary>
    /// Gets or sets the erv kezd.
    /// </summary>
    /// <value>The erv kezd.</value>
    public string DatumKezd
    {
        get
        {
            if (!String.IsNullOrEmpty(datumKezd_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(datumKezd_TextBox.Text);
                    return dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    return datumKezd_TextBox.Text;
                }
            }
            else return "";
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    datumKezd_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    datumKezd_TextBox.Text = value;
                }
            }
            else datumKezd_TextBox.Text = "";
        }
    }

    /// <summary>
    /// Gets or sets the erv vege.
    /// </summary>
    /// <value>The erv vege.</value>
    public string DatumVege
    {
        get
        {
            if (!String.IsNullOrEmpty(datumVege_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(datumVege_TextBox.Text);
                    dt = dt.AddHours(23 - dt.Hour);
                    dt = dt.AddMinutes(59 - dt.Minute);
                    dt = dt.AddSeconds(59 - dt.Second);
                    dt = dt.AddMilliseconds(999 - dt.Millisecond);
                    return dt.ToString();
                }
                catch (FormatException)
                {
                    return datumVege_TextBox.Text;
                }
            }
            else return "";
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    datumVege_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    datumVege_TextBox.Text = value;
                }
            }
            else datumVege_TextBox.Text = "";
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv kezd].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv kezd]; otherwise, <c>false</c>.</value>
    public bool Enable_DatumKezd
    {
        set
        {
            datumKezd_TextBox.Enabled = value;
            datumKezd_ImageButton.Enabled = value;
            DatumKezd_RequiredFieldValidator.Enabled = value;
            CalendarExtender1.Enabled = value;
            if (!value)
            {
                datumKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_DatumKezd
    {
        set
        {
            datumKezd_TextBox.ReadOnly = value;
            datumKezd_ImageButton.Enabled = !value;
            DatumKezd_RequiredFieldValidator.Enabled = !value;
            CalendarExtender1.Enabled = !value;
            if (value)
            {
                datumKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public string GetJsSetReadonly(bool isReadonly)
    {
        string js = "var readOnly = " + isReadonly.ToString().ToLower() +@";
                     var txtKezd = $get('" + datumKezd_TextBox.ClientID + @"');
                     if(txtKezd) txtKezd.readOnly = readOnly;
                     var txtVege = $get('" + datumVege_TextBox.ClientID + @"');
                     if(txtVege) txtVege.readOnly = readOnly;
                     var imgKezd = $get('" + datumKezd_ImageButton.ClientID + @"');
                     if(imgKezd) {imgKezd.disabled = readOnly; 
                     if(readOnly) imgKezd.className = 'disabledCalendarImage'; else imgKezd.className = '';}
                     var imgVege = $get('" + datumVege_ImageButton.ClientID + @"');
                     if(imgVege) {imgVege.disabled = readOnly; 
                     if(readOnly) imgVege.className = 'disabledCalendarImage'; else imgVege.className = '';}
                     var imgCopy = $get('" + ImageButton_Copy.ClientID + @"');
                     if(imgCopy) {imgCopy.disabled = readOnly; 
                     if(readOnly) imgCopy.className = 'disabledCalendarImage'; else imgCopy.className = '';};";

        return js;
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_DatumVege
    {
        set
        {
            datumVege_TextBox.Enabled = value;
            datumVege_ImageButton.Enabled = value;
            DatumVege_RequiredFieldValidator.Enabled = value;
            ImageButton_Copy.Enabled = value;
            CalendarExtender2.Enabled = value;
            if (!value)
            {
                ImageButton_Copy.CssClass = "disabledCalendarImage";
                datumVege_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_DatumVege
    {
        set
        {
            datumVege_TextBox.ReadOnly = value;
            datumVege_ImageButton.Enabled = !value;
            DatumVege_RequiredFieldValidator.Enabled = !value;
            ImageButton_Copy.Enabled = !value;
            CalendarExtender2.Enabled = !value;
            if (value)
            {
                ImageButton_Copy.CssClass = "disabledCalendarImage";
                datumVege_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }


    /// <summary>
    /// Gets the erv kezd_ text box.
    /// </summary>
    /// <value>The erv kezd_ text box.</value>
    public TextBox DatumKezd_TextBox
    {
        get { return datumKezd_TextBox; }
    }

    /// <summary>
    /// Gets the erv vege_ text box.
    /// </summary>
    /// <value>The erv vege_ text box.</value>
    public TextBox DatumVege_TextBox
    {
        get { return datumVege_TextBox; }
    }

    /// <summary>
    /// Gets the erv kezd_ image button.
    /// </summary>
    /// <value>The erv kezd_ image button.</value>
    public ImageButton DatumKezd_ImageButton
    {
        get { return datumKezd_ImageButton; }
    }

    public ImageButton ImageButton_Copy
    {
        get { return datumKezd_masol_ImageButton; }
    }

    /// <summary>
    /// Gets the erv vege_ image button.
    /// </summary>
    /// <value>The erv vege_ image button.</value>
    public ImageButton DatumVege_ImageButton
    {
        get { return datumVege_ImageButton; }
    }

    /// <summary>
    /// Sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is validate.
    /// </summary>
    /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
    public bool Validate
    {
        set
        {
            DatumKezd_RequiredFieldValidator.Enabled = value;
            DatumKezd_RequiredFieldValidator.Visible = value;
            DatumVege_RequiredFieldValidator.Enabled = value;
            DatumVege_RequiredFieldValidator.Visible = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether this CompareValidator_DateFormat_Kezd and CompareValidator_DateFormat_Vege will validate.
    /// </summary>
    /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
    public bool ValidateDateFormat
    {
        set
        {
            CustomValidator_DateFormat_Kezd.Enabled = value;
            CustomValidator_DateFormat_Kezd.Visible = value;
            CustomValidator_DateFormat_Vege.Enabled = value;
            CustomValidator_DateFormat_Vege.Visible = value;
        }
    }

    private Boolean _setDefaultDatumKezd;
    /// <summary>
    /// Kit�ltse-e a d�tum kezdete mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv kezd]; otherwise, <c>false</c>.</value>
    public bool SetDefaultDatumKezd
    {
        set
        {
            _setDefaultDatumKezd = value;
        }

    }

    private Boolean _setDefaultDatumVege;
    /// <summary>
    /// Kit�ltse-e a d�tum v�ge mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv vege]; otherwise, <c>false</c>.</value>
    public bool SetDefaultDatumVege
    {
        set
        {
            _setDefaultDatumVege = value;
        }

    }

    /// <summary>
    /// Sets the default.
    /// </summary>
    public void SetDefault()
    {
        if (_setDefaultDatumKezd)
        {
            datumKezd_TextBox.Text = DefaultDates.Today;
        }
        if (_setDefaultDatumVege)
        {
            datumVege_TextBox.Text = DefaultDates.EndDate;
        }
        CompareValidator1.Enabled = true;
        CompareValidator1.Visible = true;
    }

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_DatumKezd = _Enabled;
            Enable_DatumVege = _Enabled;
        }
    }

    Boolean _Readonly = false;

    public Boolean Readonly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            Readonly_DatumKezd = _Readonly;
            Readonly_DatumVege = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Readonly = !_ViewEnabled;

            if (!_ViewEnabled)
            {
                datumKezd_TextBox.CssClass += " ViewReadOnlyWebControl";
                datumKezd_ImageButton.CssClass = "ViewReadOnlyWebControl";

                datumVege_TextBox.CssClass += " ViewReadOnlyWebControl";
                datumVege_ImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;

            if (!_ViewVisible)
            {
                datumKezd_TextBox.CssClass += " ViewDisabledWebControl";
                datumKezd_ImageButton.CssClass = "ViewDisabledWebControl";

                datumVege_TextBox.CssClass += " ViewDisabledWebControl";
                datumVege_ImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #region AktualisEv

    public bool AktualisEv_Enable
    {
        get { return AktualisEv_ChBox.Enabled; }
        set { AktualisEv_ChBox.Enabled = value; }
    }

    public bool AktualisEv_Visible
    {
        get { return AktualisEv_ChBox.Visible; }
        set { AktualisEv_ChBox.Visible = value; }
    }

    public CheckBox AktualisEv_CheckBox
    {
        get { return AktualisEv_ChBox; }
    }


    #endregion


    public void Clear()
    {
        DatumKezd_TextBox.Text = String.Empty;
        DatumVege_TextBox.Text = String.Empty;
    }

    public string GetJsClear()
    {
        string js = "var txtKezd = $get('" + datumKezd_TextBox.ClientID + @"');
                     if(txtKezd) {txtKezd.prevValue = txtKezd.value;txtKezd.value='';}
                     var txtVege = $get('" + datumVege_TextBox.ClientID + @"');
                     if(txtVege) {txtVege.prevValue = txtVege.value;txtVege.value='';};";

        return js;
    }

    public string GetJsRestore()
    {
        string js = "var txtKezd = $get('" + datumKezd_TextBox.ClientID + @"');
                     if(txtKezd && txtKezd.prevValue) txtKezd.value=txtKezd.prevValue;
                     var txtVege = $get('" + datumVege_TextBox.ClientID + @"');
                     if(txtVege && txtVege.prevValue) txtVege.value=txtVege.prevValue;";

        return js;
    }

    public bool IsEmpty
    {
        get
        {
            return String.IsNullOrEmpty(DatumKezd) && String.IsNullOrEmpty(DatumVege);
        }
    }

    public AjaxControlToolkit.CalendarPosition CalendarPosition
    {
        set
        {
            CalendarExtender1.PopupPosition = value;
            CalendarExtender2.PopupPosition = value;
        }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    /// <summary>
    /// Gets the component list.
    /// </summary>
    /// <returns></returns>
    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(DatumKezd_TextBox);
        componentList.Add(DatumKezd_ImageButton);
        componentList.Add(DatumVege_TextBox);
        componentList.Add(DatumVege_ImageButton);

        // Lekell tiltani a ClientValidator
        DatumKezd_RequiredFieldValidator.Enabled = false;
        DatumVege_RequiredFieldValidator.Enabled = false;
        CompareValidator1.Enabled = false;
        CustomValidator_DateFormat_Kezd.Enabled = false;
        CustomValidator_DateFormat_Vege.Enabled = false;

        DatumKezd_ImageButton.OnClientClick = "";
        CalendarExtender1.Enabled = false;
        DatumVege_ImageButton.OnClientClick = "";
        CalendarExtender2.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;

        }

        return text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;
    private bool ajaxEnabled = true;

    protected override void OnPreRender(EventArgs e)
    {
        if (!IsPostBack)
        {
            _LastEvKezd = this.EvKezd;
            _LastEvVege = this.EvVege;
        }


        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        List<ScriptReference> listOfScripRefernces = new List<ScriptReference>();

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Calendar.js";
        listOfScripRefernces.Add(reference);

        return listOfScripRefernces.ToArray();
    }

    #endregion
}
