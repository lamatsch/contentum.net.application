using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_DdlIntervallum_SearchFormControl : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    /// <summary>
    /// Handles the Init event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //SetDefault();
        }
    }

    public void Clear()
    {
        DdlTol_DropDownList.Items.Clear();
        DdlIg_DropDownList.Items.Clear();
    }

    public void BindData()
    {
        BindData(false);
    }

    public void BindData(ListItemCollection listItems)
    {
        BindData(listItems, false);
    }

    public void BindData(bool addEmptyItem)
    {
        DdlTol_DropDownList.Items.Clear();
        DdlIg_DropDownList.Items.Clear();
        if (this.DataSource != null)
        {
            DdlTol_DropDownList.DataBind();
            DdlIg_DropDownList.DataBind();
        }

        if (addEmptyItem)
        {
            AddEmptyItem();
        }
    }

    public void BindData(ListItemCollection listItems, bool addEmptyItem)
    {
        DdlTol_DropDownList.Items.Clear();
        DdlIg_DropDownList.Items.Clear();
        if (listItems != null)
        {
            DdlTol_DropDownList.DataSource = listItems;
            DdlIg_DropDownList.DataSource = listItems;
            DdlTol_DropDownList.DataBind();
            DdlIg_DropDownList.DataBind();
        }

        if (addEmptyItem)
        {
            AddEmptyItem();
        }
    }

    public void AddEmptyItem()
    {
        DdlTol_DropDownList.Items.Insert(0, new ListItem(EmptyItemText, ""));
        DdlIg_DropDownList.Items.Insert(0, new ListItem(EmptyItemText, ""));
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Enabled)
        {
            string jsCopy = "var ddlIg = $get('" + DdlIg_DropDownList.ClientID + "');var ddlTol = $get('" + DdlTol_DropDownList.ClientID + "');"
                          + "if(ddlTol && ddlIg) ddlIg.selectedIndex = ddlTol.selectedIndex;";

            DdlTol_masol_ImageButton.OnClientClick = jsCopy + "return false;";

            if (autoCopy)
            {
                DdlTol_DropDownList.Attributes.Add("onkeyup", jsCopy);
                DdlTol_DropDownList.Attributes.Add("onchange", jsCopy);
            }
        }

        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }



    /// <summary>
    /// DdlTol �s DdlIg be�ll�t�sa a megadott keres�si mez�nek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        if (!String.IsNullOrEmpty(DdlTol_DropDownList.SelectedValue) || !String.IsNullOrEmpty(DdlIg_DropDownList.SelectedValue))
        {
            if (String.IsNullOrEmpty(DdlTol_DropDownList.SelectedValue))
            {
                SearchField.Value = DdlIg_DropDownList.SelectedValue;
                SearchField.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(DdlIg_DropDownList.SelectedValue))
            {
                SearchField.Value = DdlTol_DropDownList.SelectedValue;
                SearchField.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                SearchField.Value = DdlTol_DropDownList.SelectedValue;
                SearchField.ValueTo = DdlIg_DropDownList.SelectedValue;
                SearchField.Operator = Query.Operators.between;
            }
        }
    }

    /// <summary>
    /// DropDownListok be�ll�t�sa a keres�si objektum mez�je alapj�n
    /// </summary>
    /// <param name="SearchField"></param>
    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        if (SearchField.Operator == Query.Operators.lessorequal)
        {
            DdlIg_DropDownList.SelectedValue = SearchField.Value;
        }
        else if (SearchField.Operator == Query.Operators.greaterorequal)
        {
            DdlTol_DropDownList.SelectedValue = SearchField.Value;
        }
        else
        {
            DdlTol_DropDownList.SelectedValue = SearchField.Value;
            DdlIg_DropDownList.SelectedValue = SearchField.ValueTo;
        }
    }

    /// <summary>
    /// true, ha mindk�t DropDownList.SelectedValue �res, egy�bk�nt false
    /// </summary>
    /// <returns></returns>
    public bool BothDropDownListsAreEmpty()
    {
        if (String.IsNullOrEmpty(DdlTol_DropDownList.SelectedValue) && String.IsNullOrEmpty(DdlIg_DropDownList.SelectedValue))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    #region public properties

    private bool autoCopy = false;

    public bool AutoCopy
    {
        get { return autoCopy; }
        set { autoCopy = value; }
    }

    public object DataSource
    {
        get { return DdlTol_DropDownList.DataSource; }
        set 
        {
            DdlTol_DropDownList.DataSource = value;
            DdlIg_DropDownList.DataSource = value;
        }
    }

    public string DataMember
    {
        get { return DdlTol_DropDownList.DataMember; }
        set
        {
            DdlTol_DropDownList.DataMember = value;
            DdlIg_DropDownList.DataMember = value;
        }
    }

    public string DataTextField
    {
        get { return DdlTol_DropDownList.DataTextField; }
        set
        {
            DdlTol_DropDownList.DataTextField = value;
            DdlIg_DropDownList.DataTextField = value;
        }
    }

    public string DataValueField
    {
        get { return DdlTol_DropDownList.DataValueField; }
        set
        {
            DdlTol_DropDownList.DataValueField = value;
            DdlIg_DropDownList.DataValueField = value;
        }
    }

    private string _EmptyItemText = Resources.Form.EmptyListItem;

    public string EmptyItemText
    {
        get { return _EmptyItemText; }
        set { _EmptyItemText = (value ?? ""); }
    }

    /// <summary>
    /// Gets or sets the intervall begin.
    /// </summary>
    /// <value>The intervall begin.</value>
    public string DdlTol
    {
        get
        {
            return DdlTol_DropDownList.SelectedValue;
        }
        set
        {
            DdlTol_DropDownList.SelectedValue = value;
        }
    }

    /// <summary>
    /// Gets or sets the intervall end.
    /// </summary>
    /// <value>The intervall end.</value>
    public string DdlIg
    {
        get
        {
            return DdlIg_DropDownList.SelectedValue;
        }
        set
        {
            DdlIg_DropDownList.SelectedValue = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ intervall begin].
    /// </summary>
    /// <value><c>true</c> if [enable_ intervall begin]; otherwise, <c>false</c>.</value>
    public bool Enable_DdlTol
    {
        set
        {
            DdlTol_DropDownList.Enabled = value;
        }
    }

    public bool Readonly_DdlTol
    {
        set
        {
            //DdlTol_DropDownList.ReadOnly = value;
            DdlIg_DropDownList.Enabled = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_DdlIg
    {
        set
        {
            DdlIg_DropDownList.Enabled = value;
            DdlTol_masol_ImageButton.Enabled = value;
            if (!value)
            {
                DdlTol_masol_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_DdlIg
    {
        set
        {
            DdlIg_DropDownList.Enabled = value;
            DdlTol_masol_ImageButton.Enabled = !value;
            if (value)
            {
                DdlTol_masol_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }


    /// <summary>
    /// Gets the intervall begin_ dropdown list.
    /// </summary>
    /// <value>The intervall begin_ dropdown list.</value>
    public DropDownList DdlTol_DropDownList
    {
        get { return DdlTolDropDownList; }
    }

    /// <summary>
    /// Gets the erv vege_ dropdown list.
    /// </summary>
    /// <value>The erv vege_ dropdown list.</value>
    public DropDownList DdlIg_DropDownList
    {
        get { return DdlIgDropDownList; }
    }

    public ImageButton ImageButton_Masol
    {
        get { return DdlTol_masol_ImageButton; }
    }

    public string CssClass
    {
        get { return DdlTol_DropDownList.CssClass; }
        set {
            DdlTol_DropDownList.CssClass = value;
            DdlIg_DropDownList.CssClass = value;
        }
    }
      

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_DdlTol = _Enabled;
            Enable_DdlIg = _Enabled;
        }
    }

    Boolean _Readonly = false;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean ReadOnly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            Readonly_DdlTol = _Readonly;
            Readonly_DdlIg = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;

            if (!_ViewEnabled)
            {
                Enabled = _ViewEnabled;
                DdlTol_DropDownList.CssClass = "ViewReadOnlyWebControl";

                DdlIg_DropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;

            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DdlTol_DropDownList.CssClass = "ViewDisabledWebControl";

                DdlIg_DropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    //#region ISelectableUserComponent method implementations

    ///// <summary>
    ///// Gets the component list.
    ///// </summary>
    ///// <returns></returns>
    //public System.Collections.Generic.List<WebControl> GetComponentList()
    //{
    //    System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
    //    componentList.Add(FoddlTol_DropDownList);
    //    componentList.Add(FoddlIg_DropDownList);

    //    return componentList;
    //}

    //#endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;

        }

        return text;
    }

    #endregion
}
