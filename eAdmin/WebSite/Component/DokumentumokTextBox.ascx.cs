using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class Component_DokumentumokTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set
        {
            Validator1.Enabled = value;
        }
        get { return Validator1.Enabled; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { textDokumentum.Text = value; }
        get { return textDokumentum.Text; }
    }

    public TextBox TextBox
    {
        get { return textDokumentum; }
    }

    public bool Enabled
    {
        set
        {
            textDokumentum.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return textDokumentum.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            textDokumentum.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return textDokumentum.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { hiddenDokumentumID.Value = value; }
        get { return hiddenDokumentumID.Value; }
    }

    public HiddenField HiddenField
    {
        get { return hiddenDokumentumID; }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {

        OnClick_Lov = JavaScripts.SetOnClientClick("DokumentumokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + TextBox.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        //OnClick_New = JavaScripts.SetOnClientClick("DokumentumokForm.aspx"
        //    , CommandName.Command + "=" + CommandName.New
        //    + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + TextBox.ClientID
        //    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "DokumentuokForm.aspx", "", HiddenField.ClientID);

        OnClick_New = @"var id = '96d81134-b276-41ee-bd3b-adb59c5d89a1';var newId = '';for(var i = 0; i<id.length; i++) 
                      {var c= id.charAt(i);if(c != '-'){var random = Math.round(9*Math.random()); c = random;} newId +=c;};
                      $get('" + TextBox.ClientID + "').value = newId;$get('" + HiddenField.ClientID + "').value = newId;return false;";

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField.ClientID + "').value = '';return false;";

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            //KRT_DokumentumokService service = eAdminService.ServiceFactory.GetKRT_DokumentumokService();
            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //execParam.Record_Id = Id_HiddenField;

            //Result result = service.Get(execParam);
            //if (String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    if (result.Record != null)
            //    {
            //        KRT_Dokumentumok KRT_Dokumentumok = (KRT_Dokumentumok)result.Record;
            //        Text = KRT_Dokumentumok.Nev;
            //    }
            //    else
            //        Text = "";
            //}
            //else
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            //}

            Text = Id_HiddenField;
        }
        else
        {
            Text = "";
        }
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(textDokumentum);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";


        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    Boolean _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                textDokumentum.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                textDokumentum.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
