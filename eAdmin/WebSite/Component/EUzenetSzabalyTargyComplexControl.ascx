﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="EUzenetSzabalyTargyComplexControl.ascx.cs"
    Inherits="EUzenetSzabalyTargyComplexControl"
    Description="EUzenetSzabaly tárgy összeállító kontrol" %>

<%@ Register Src="EUzenetSzabalyTargyUserControl.ascx" TagName="EUzenetSzabalyTargyUserControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="ErrorUpdatePanel_EUZTC" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel_EUZTC" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel_EUZTCS" runat="server">
    <ContentTemplate>
        <u>
            <label>Alapértelmezett</label></u>
        <br />
        &nbsp;&nbsp;
        <label for="DropDownList_Default_Type">Típus</label>
        <asp:DropDownList ID="DropDownList_Default_Type" runat="server" ToolTip="Típus" AutoPostBack="True" Width="65px" OnSelectedIndexChanged="DropDownList_Default_Type_SelectedIndexChanged"></asp:DropDownList>

        &nbsp;&nbsp;
        <label for="div_Default_Ertek">Érték</label>
        <span id="div_Default_Ertek" title="Érték">
            <asp:TextBox ID="TextBox_Default_Value_TXT" runat="server" placeholder="Szöveg" Width="180px"></asp:TextBox>
            <asp:TextBox ID="TextBox_Default_Value_XML" runat="server" placeholder="XML XPath" Width="180px"></asp:TextBox>
            <asp:DropDownList ID="DropDownList_Default_Value_DB" runat="server" Width="184px"></asp:DropDownList>
        </span>
        <br />
        <br />
        <u>
            <label>Összeállítása</label>
        </u>
        <asp:Panel runat="server" ID="Panel_FROM_DATA">
            <div>
                <asp:HiddenField runat="server" ID="HiddenField_EUZT_UtolsoSorszam" Value="1" />
                <asp:ImageButton runat="server" ID="ImageButton_EUZT_Hozzaad"
                    ImageUrl="~/images/hu/muvgomb/ugyirat_letrehozasa.jpg" Height="25px"
                    OnClick="ImageButton_EUZT_Hozzaad_Click" CausesValidation="false" />
                <br />
                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_01" runat="server" SorSzam="1" Visible="true" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_02" runat="server" SorSzam="2" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_03" runat="server" SorSzam="3" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_04" runat="server" SorSzam="4" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_05" runat="server" SorSzam="5" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_06" runat="server" SorSzam="6" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_07" runat="server" SorSzam="7" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_08" runat="server" SorSzam="8" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_09" runat="server" SorSzam="9" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_10" runat="server" SorSzam="10" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_11" runat="server" SorSzam="11" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_12" runat="server" SorSzam="12" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_13" runat="server" SorSzam="13" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_14" runat="server" SorSzam="14" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_15" runat="server" SorSzam="15" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_16" runat="server" SorSzam="16" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_17" runat="server" SorSzam="17" Visible="false" />

                <uc1:EUzenetSzabalyTargyUserControl ID="EUzenetSzabalyTargyUserControl_18" runat="server" SorSzam="18" Visible="false" />
            </div>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
