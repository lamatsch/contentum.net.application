﻿using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility.EKF.Models;
using Contentum.eUtility.EKF;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EUzenetSzabalyTargyComplexControl : ContentumUserControlBaseClass
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Init();

        //////InitEvents();
    }
    /// <summary>
    /// Initialize
    /// </summary>
    private void Init()
    {
        InitDropDownListControls();
        TypeChanged();
    }
    #region EVENTS
    protected void ImageButton_EUZT_Hozzaad_Click(object sender, ImageClickEventArgs e)
    {
        VisibleNextControl();
    }
    #endregion

    /// <summary>
    /// VisibleNextControl
    /// </summary>
    private void VisibleNextControl()
    {
        int SorSzam = 2;
        if (int.TryParse(HiddenField_EUZT_UtolsoSorszam.Value, out SorSzam))
        {
            ++SorSzam;
        }
        else
        {
            SorSzam = 2;
        }

        if (!EUzenetSzabalyTargyUserControl_02.Visible)
            EUzenetSzabalyTargyUserControl_02.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_03.Visible)
            EUzenetSzabalyTargyUserControl_03.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_04.Visible)
            EUzenetSzabalyTargyUserControl_04.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_05.Visible)
            EUzenetSzabalyTargyUserControl_05.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_06.Visible)
            EUzenetSzabalyTargyUserControl_06.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_07.Visible)
            EUzenetSzabalyTargyUserControl_07.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_08.Visible)
            EUzenetSzabalyTargyUserControl_08.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_09.Visible)
            EUzenetSzabalyTargyUserControl_09.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_10.Visible)
            EUzenetSzabalyTargyUserControl_10.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_11.Visible)
            EUzenetSzabalyTargyUserControl_11.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_12.Visible)
            EUzenetSzabalyTargyUserControl_12.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_13.Visible)
            EUzenetSzabalyTargyUserControl_13.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_14.Visible)
            EUzenetSzabalyTargyUserControl_14.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_15.Visible)
            EUzenetSzabalyTargyUserControl_15.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_16.Visible)
            EUzenetSzabalyTargyUserControl_16.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_17.Visible)
            EUzenetSzabalyTargyUserControl_17.Visible = true;
        else if (!EUzenetSzabalyTargyUserControl_18.Visible)
            EUzenetSzabalyTargyUserControl_18.Visible = true;

        HiddenField_EUZT_UtolsoSorszam.Value = SorSzam.ToString();
    }

    #region COMPLEX
    /// <summary>
    /// GetAllModels
    /// </summary>
    /// <returns></returns>
    private List<EKF_ComplexTargyModel> GetAllModels()
    {
        List<EKF_ComplexTargyModel> result = new List<EKF_ComplexTargyModel>();
        EKF_ComplexTargyModel tmp = null;

        bool isValid = IsValidComplex();
        if (!isValid)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel_EUZTC, Resources.Error.MessageError, Resources.Error.ErrorParameters);
            ErrorUpdatePanel_EUZTC.Update();
            throw new Exception(Resources.Error.ErrorParameters);
        }

        if (EUzenetSzabalyTargyUserControl_01.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_01.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_02.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_02.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_03.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_03.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_04.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_04.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_05.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_05.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_06.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_06.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_07.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_07.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_08.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_08.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_09.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_09.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_10.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_10.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_11.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_11.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_12.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_12.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_13.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_13.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_14.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_14.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_15.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_15.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_16.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_16.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_17.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_17.GetBO();
            result.Add(tmp);
        }
        if (EUzenetSzabalyTargyUserControl_18.Visible)
        {
            tmp = EUzenetSzabalyTargyUserControl_18.GetBO();
            result.Add(tmp);
        }
        return result;
    }
    /// <summary>
    /// SetAllModels
    /// </summary>
    /// <param name="models"></param>
    private void SetAllModels(List<EKF_ComplexTargyModel> models)
    {
        if (models == null || models.Count < 1)
            return;

        int index = 1;
        for (int i = 0; i < models.Count; i++)
        {
            if (models[i].IsValid())
            {
                switch (index)
                {
                    case 1:
                        EUzenetSzabalyTargyUserControl_01.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_01.Visible = true;
                        break;
                    case 2:
                        EUzenetSzabalyTargyUserControl_02.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_02.Visible = true;
                        break;
                    case 3:
                        EUzenetSzabalyTargyUserControl_03.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_03.Visible = true;
                        break;
                    case 4:
                        EUzenetSzabalyTargyUserControl_04.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_04.Visible = true;
                        break;
                    case 5:
                        EUzenetSzabalyTargyUserControl_05.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_05.Visible = true;
                        break;
                    case 6:
                        EUzenetSzabalyTargyUserControl_06.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_06.Visible = true;
                        break;
                    case 7:
                        EUzenetSzabalyTargyUserControl_07.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_07.Visible = true;
                        break;
                    case 8:
                        EUzenetSzabalyTargyUserControl_08.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_08.Visible = true;
                        break;
                    case 9:
                        EUzenetSzabalyTargyUserControl_09.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_09.Visible = true;
                        break;
                    case 10:
                        EUzenetSzabalyTargyUserControl_10.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_10.Visible = true;
                        break;
                    case 11:
                        EUzenetSzabalyTargyUserControl_11.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_11.Visible = true;
                        break;
                    case 12:
                        EUzenetSzabalyTargyUserControl_12.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_12.Visible = true;
                        break;
                    case 13:
                        EUzenetSzabalyTargyUserControl_13.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_13.Visible = true;
                        break;
                    case 14:
                        EUzenetSzabalyTargyUserControl_14.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_14.Visible = true;
                        break;
                    case 15:
                        EUzenetSzabalyTargyUserControl_15.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_15.Visible = true;
                        break;
                    case 16:
                        EUzenetSzabalyTargyUserControl_16.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_16.Visible = true;
                        break;
                    case 17:
                        EUzenetSzabalyTargyUserControl_17.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_17.Visible = true;
                        break;
                    case 18:
                        EUzenetSzabalyTargyUserControl_18.SetBO(models[i]);
                        EUzenetSzabalyTargyUserControl_18.Visible = true;
                        break;
                    default:
                        break;
                }
                ++index;
            }
        }
    }
    /// <summary>
    /// ResetComplex
    /// </summary>
    private void ResetComplex()
    {
        EUzenetSzabalyTargyUserControl_01.ResetData();

        EUzenetSzabalyTargyUserControl_02.ResetData();
        EUzenetSzabalyTargyUserControl_02.Visible = false;

        EUzenetSzabalyTargyUserControl_03.ResetData();
        EUzenetSzabalyTargyUserControl_03.Visible = false;

        EUzenetSzabalyTargyUserControl_04.ResetData();
        EUzenetSzabalyTargyUserControl_04.Visible = false;

        EUzenetSzabalyTargyUserControl_05.ResetData();
        EUzenetSzabalyTargyUserControl_05.Visible = false;

        EUzenetSzabalyTargyUserControl_06.ResetData();
        EUzenetSzabalyTargyUserControl_06.Visible = false;

        EUzenetSzabalyTargyUserControl_07.ResetData();
        EUzenetSzabalyTargyUserControl_07.Visible = false;

        EUzenetSzabalyTargyUserControl_08.ResetData();
        EUzenetSzabalyTargyUserControl_08.Visible = false;

        EUzenetSzabalyTargyUserControl_09.ResetData();
        EUzenetSzabalyTargyUserControl_09.Visible = false;

        EUzenetSzabalyTargyUserControl_10.ResetData();
        EUzenetSzabalyTargyUserControl_10.Visible = false;

        EUzenetSzabalyTargyUserControl_11.ResetData();
        EUzenetSzabalyTargyUserControl_11.Visible = false;

        EUzenetSzabalyTargyUserControl_12.ResetData();
        EUzenetSzabalyTargyUserControl_12.Visible = false;

        EUzenetSzabalyTargyUserControl_13.ResetData();
        EUzenetSzabalyTargyUserControl_13.Visible = false;

        EUzenetSzabalyTargyUserControl_14.ResetData();
        EUzenetSzabalyTargyUserControl_14.Visible = false;

        EUzenetSzabalyTargyUserControl_15.ResetData();
        EUzenetSzabalyTargyUserControl_15.Visible = false;

        EUzenetSzabalyTargyUserControl_16.ResetData();
        EUzenetSzabalyTargyUserControl_16.Visible = false;

        EUzenetSzabalyTargyUserControl_17.ResetData();
        EUzenetSzabalyTargyUserControl_17.Visible = false;

        EUzenetSzabalyTargyUserControl_18.ResetData();
        EUzenetSzabalyTargyUserControl_18.Visible = false;
    }
    /// <summary>
    /// IsValidComplex
    /// </summary>
    /// <returns></returns>
    private bool IsValidComplex()
    {
        if (
               (EUzenetSzabalyTargyUserControl_01.Visible && !EUzenetSzabalyTargyUserControl_01.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_02.Visible && !EUzenetSzabalyTargyUserControl_02.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_03.Visible && !EUzenetSzabalyTargyUserControl_03.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_04.Visible && !EUzenetSzabalyTargyUserControl_04.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_05.Visible && !EUzenetSzabalyTargyUserControl_05.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_06.Visible && !EUzenetSzabalyTargyUserControl_06.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_07.Visible && !EUzenetSzabalyTargyUserControl_07.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_08.Visible && !EUzenetSzabalyTargyUserControl_08.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_09.Visible && !EUzenetSzabalyTargyUserControl_09.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_10.Visible && !EUzenetSzabalyTargyUserControl_10.IsValid())
               ||
               (EUzenetSzabalyTargyUserControl_11.Visible && !EUzenetSzabalyTargyUserControl_11.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_12.Visible && !EUzenetSzabalyTargyUserControl_12.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_13.Visible && !EUzenetSzabalyTargyUserControl_13.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_14.Visible && !EUzenetSzabalyTargyUserControl_14.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_15.Visible && !EUzenetSzabalyTargyUserControl_15.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_16.Visible && !EUzenetSzabalyTargyUserControl_16.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_17.Visible && !EUzenetSzabalyTargyUserControl_17.IsValid())
                ||
               (EUzenetSzabalyTargyUserControl_18.Visible && !EUzenetSzabalyTargyUserControl_18.IsValid())
           )
            return false;

        return true;
    }
    /// <summary>
    /// GetComplexTargyFormula
    /// </summary>
    /// <returns></returns>
    private string GetComplexTargyFormula()
    {
        List<EKF_ComplexTargyModel> models = GetAllModels();
        return Contentum.eUtility.JSONUtilityFunctions.SerializeEKFTargyModelObj(models);
    }
    /// <summary>
    /// SetComplexTargyFormula
    /// </summary>
    /// <param name="targyFormula"></param>
    private void SetComplexTargyFormula(string targyFormula)
    {
        List<EKF_ComplexTargyModel> models = Contentum.eUtility.JSONUtilityFunctions.DeSerializeEKFTargyModelObj(targyFormula);
        SetAllModels(models);
    }
    #endregion

    #region DEFAULT
    private bool IsValidDefault()
    {
        if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
        {
            if (string.IsNullOrEmpty(DropDownList_Default_Value_DB.SelectedValue))
                return false;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
        {
            if (string.IsNullOrEmpty(TextBox_Default_Value_TXT.Text))
                return false;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
        {
            if (string.IsNullOrEmpty(TextBox_Default_Value_XML.Text))
                return false;
        }
        else
            return false;

        return true;
    }
    /// <summary>
    /// GetDefaultModel
    /// </summary>
    /// <returns></returns>
    private EKF_DefaultTargyModel GetDefaultModel()
    {
        EKF_DefaultTargyModel item = new EKF_DefaultTargyModel();

        bool isValid = IsValidDefault();
        if (!isValid)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel_EUZTC, Resources.Error.MessageError, Resources.Error.ErrorParameters);
            ErrorUpdatePanel_EUZTC.Update();
            throw new Exception(Resources.Error.ErrorParameters);
        }

        item.Type = DropDownList_Default_Type.SelectedValue;
        if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
        {
            item.Value_DB = DropDownList_Default_Value_DB.SelectedValue;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
        {
            item.Value_TXT = TextBox_Default_Value_TXT.Text;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
        {
            item.Value_DB = TextBox_Default_Value_XML.Text;
        }
        return item;
    }
    /// <summary>
    /// SetDefaultModel
    /// </summary>
    /// <param name="models"></param>
    private void SetDefaultModel(EKF_DefaultTargyModel model)
    {
        if (model == null)
            return;

        if (model.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
        {
            DropDownList_Default_Value_DB.SelectedValue = model.Value_DB;
        }
        else if (model.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
        {
            TextBox_Default_Value_TXT.Text = model.Value_TXT;
        }
        else if (model.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
        {
            TextBox_Default_Value_XML.Text = model.Value_DB;
        }
        DropDownList_Default_Type.SelectedValue = model.Type;
    }
    /// <summary>
    /// ResetDefault
    /// </summary>
    private void ResetDefault()
    {
        DropDownList_Default_Value_DB.SelectedValue = null;
        TextBox_Default_Value_TXT.Text = string.Empty;
        TextBox_Default_Value_XML.Text = string.Empty;
    }
    private string GetDefaultTargyFormula()
    {
        EKF_DefaultTargyModel model = GetDefaultModel();
        string targyDefaultFormula = Contentum.eUtility.JSONUtilityFunctions.SerializeEKFDefaultTargyModelObj(model);
        return targyDefaultFormula;
    }
    /// <summary>
    /// SetComplexTargyFormula
    /// </summary>
    /// <param name="targyFormula"></param>
    private void SetDefaultTargyFormula(string targyDefaultFormula)
    {
        EKF_DefaultTargyModel model = Contentum.eUtility.JSONUtilityFunctions.DeSerializeEKFDefaultTargyModelObj(targyDefaultFormula);
        SetDefaultModel(model);
    }
    #endregion

    #region INTERFACES
    /// <summary>
    /// IsValid()
    /// </summary>
    /// <returns></returns>
    public bool IsValid()
    {
        bool isValidDef = IsValidDefault();
        bool isValidComp = IsValidComplex();

        if (isValidDef == false && isValidComp == false)
            return false;
        else if (isValidDef == false && isValidComp == true)
            return false;

        return true;
    }
    /// <summary>
    /// GetTargyFormula
    /// </summary>
    /// <param name="targyForrasTipus"></param>
    /// <param name="targyFormula"></param>
    /// <returns></returns>
    public bool GetTargyFormula(out string targyForrasTipus, out string targyFormula, out string targyDefaultFormula)
    {
        targyForrasTipus = string.Empty;
        targyFormula = string.Empty;
        targyDefaultFormula = string.Empty;

        if (!IsValid())
            throw new Exception(Resources.Error.ErrorMessage_EKF_TargyFormula_Problem);

        targyForrasTipus = EKFConstants.EnumTargyForrasTipus.FROM_DATA.ToString();
        targyFormula = GetComplexTargyFormula();

        targyDefaultFormula = GetDefaultTargyFormula();
        return true;
    }
    /// <summary>
    /// SetTargyFormula
    /// </summary>
    /// <param name="targyForrasTipus"></param>
    /// <param name="targyFormula"></param>
    public void SetTargyFormula(string targyForrasTipus, string targyFormula, string targyDefaultFormula)
    {
        InitDropDownListControls();

        SetComplexTargyFormula(targyFormula);
        SetDefaultTargyFormula(targyDefaultFormula);
    }
    #endregion

    protected void DropDownList_Default_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }
    /// <summary>
    /// Tipus valtas eseten, a beviteli mezok lathatosaganak beallitasa.
    /// </summary>
    private void TypeChanged()
    {
        if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
        {
            DropDownList_Default_Value_DB.Visible = true;
            TextBox_Default_Value_TXT.Visible = false;
            TextBox_Default_Value_XML.Visible = false;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
        {
            DropDownList_Default_Value_DB.Visible = false;
            TextBox_Default_Value_TXT.Visible = true;
            TextBox_Default_Value_XML.Visible = false;
        }
        else if (DropDownList_Default_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
        {
            DropDownList_Default_Value_DB.Visible = false;
            TextBox_Default_Value_TXT.Visible = false;
            TextBox_Default_Value_XML.Visible = true;
        }
        else
            throw new NotImplementedException("Kódtár elem ismeretlen !");
    }

    #region FILL DEFAULT
    /// <summary>
    /// InitDropDownListControls
    /// </summary>
    private void InitDropDownListControls()
    {
        if (DropDownList_Default_Type.Items.Count < 1)
            FillControlType();

        if (DropDownList_Default_Value_DB.Items.Count < 1)
            FillControlDB();
    }

    /// <summary>
    /// Fill type control
    /// </summary>
    private void FillControlType()
    {
        DropDownList_Default_Type.Items.Clear();
        Contentum.eBusinessDocuments.ExecParam execParam = UI.SetExecParamDefault(Page, new Contentum.eBusinessDocuments.ExecParam());
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.KODCSOPORT, execParam, HttpContext.Current.Cache, null);
        for (int i = 0; i < kodtarList.Count; i++)
        {
            DropDownList_Default_Type.Items.Add(new ListItem() { Text = kodtarList[i].Nev, Value = kodtarList[i].Kod });
        }
    }
    /// <summary>
    /// Fill DB Value control
    /// </summary>
    private void FillControlDB()
    {
        System.Reflection.PropertyInfo[] myPropertyInfo = typeof(EREC_eBeadvanyokSearch).GetProperties();
        foreach (System.Reflection.PropertyInfo name in myPropertyInfo)
        {
            string item = name.ToString().Replace("Contentum.eQuery.BusinessDocuments.Field ", "");
            if (item.Contains("WhereByManual") ||
                item.Contains("OrderBy") ||
                item.Contains("TopRow") ||
                item.Contains("ErvKezd") ||
                item.Contains("ErvVege"))
                continue;

            DropDownList_Default_Value_DB.Items.Add(new ListItem(item, item));
        }
    }
    #endregion
}