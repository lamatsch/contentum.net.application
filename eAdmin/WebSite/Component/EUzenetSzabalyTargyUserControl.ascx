﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="EUzenetSzabalyTargyUserControl.ascx.cs"
    Inherits="EUzenetSzabalyTargyUserControl"
    Description="EUzenetSzabaly tárgy mező" %>

<asp:UpdatePanel ID="ErrorUpdatePanel_EUZT" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel_EUZT" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="UpdatePanel_EUzenetSzabalyTargyUserControl" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="HiddenField_ID" runat="server" Value="" />
        <asp:HiddenField ID="HiddenField_SorSzam" runat="server" Value="" />

        <label for="TextBox_Prefix">Előtag</label>
        <asp:TextBox ID="TextBox_Prefix" runat="server" MaxLength="5" placeholder="Előtag" name="TextBox_Prefix" ToolTip="Előtag" Width="65px"></asp:TextBox>
        
        &nbsp;&nbsp;
        <label for="DropDownList_Type">Típus</label>
        <asp:DropDownList ID="DropDownList_Type" runat="server" ToolTip="Típus" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_Type_SelectedIndexChanged" Width="65px"></asp:DropDownList>
        
        &nbsp;&nbsp;
        <label for="div_Ertek">Érték</label>
        <span id="div_Ertek" title="Érték">
            <asp:TextBox ID="TextBox_Value_TXT" runat="server" Visible="false" placeholder="Szöveg" Width="180px"></asp:TextBox>
            <asp:TextBox ID="TextBox_Value_XML" runat="server" Visible="false" placeholder="XML XPath" Width="180px"></asp:TextBox>
            <asp:DropDownList ID="DropDownList_Value_DB" runat="server" Visible="false" Width="184px"></asp:DropDownList>
        </span>
        
        &nbsp;&nbsp;
        <label for="TextBox_PostFix">Utótag</label>
        <asp:TextBox ID="TextBox_PostFix" runat="server" MaxLength="5" placeholder="Utótag" name="TextBox_PostFix" ToolTip="Utótag" Width="65px"></asp:TextBox>
        
        &nbsp;&nbsp;
        <asp:ImageButton runat="server" ID="ImageButtonClose" ImageUrl="~/images/hu/ikon/delete-icon.png" Height="16px" CausesValidation="false" OnClick="ImageButtonClose_Click" />
    </ContentTemplate>
</asp:UpdatePanel>






















