﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EUzenetSzabalyTargyUserControl : ContentumUserControlBaseClass
{
    [Bindable(true)]
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    public string SorSzam { get; set; }

    [Bindable(true)]
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    public override bool Visible
    {
        get { return base.Visible; }
        set { base.Visible = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitDropDownListControls();

        TypeChanged();
    }
    /// <summary>
    /// InitDropDownListControls
    /// </summary>
    private void InitDropDownListControls()
    {
        if (DropDownList_Type.Items.Count < 1)
            FillControlType();

        if (DropDownList_Value_DB.Items.Count < 1)
            FillControlDB();
    }

    /// <summary>
    /// Fill type control
    /// </summary>
    private void FillControlType()
    {
        DropDownList_Type.Items.Clear();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.KODCSOPORT, execParam, HttpContext.Current.Cache, null);
        for (int i = 0; i < kodtarList.Count; i++)
        {
            DropDownList_Type.Items.Add(new ListItem() { Text = kodtarList[i].Nev, Value = kodtarList[i].Kod });
        }
    }
    /// <summary>
    /// Fill DB Value control
    /// </summary>
    private void FillControlDB()
    {
        System.Reflection.PropertyInfo[] myPropertyInfo = typeof(EREC_eBeadvanyokSearch).GetProperties();
        foreach (System.Reflection.PropertyInfo name in myPropertyInfo)
        {
            string item = name.ToString().Replace("Contentum.eQuery.BusinessDocuments.Field ", "");
            if (item.Contains("WhereByManual") ||
                item.Contains("OrderBy") ||
                item.Contains("TopRow") ||
                item.Contains("ErvKezd") ||
                item.Contains("ErvVege"))
                continue;

            DropDownList_Value_DB.Items.Add(new ListItem(item, item));
        }
    }
    /// <summary>
    /// Tipus valtas eseten, a beviteli mezok lathatosaganak beallitasa.
    /// </summary>
    private void TypeChanged()
    {
        if (DropDownList_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
        {
            DropDownList_Value_DB.Visible = true;
            TextBox_Value_TXT.Visible = false;
            TextBox_Value_XML.Visible = false;
        }
        else if (DropDownList_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
        {
            DropDownList_Value_DB.Visible = false;
            TextBox_Value_TXT.Visible = true;
            TextBox_Value_XML.Visible = false;
        }
        else if (DropDownList_Type.SelectedValue == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
        {
            DropDownList_Value_DB.Visible = false;
            TextBox_Value_TXT.Visible = false;
            TextBox_Value_XML.Visible = true;
        }
        else
            throw new NotImplementedException("Kódtár elem ismeretlen !");
    }

    #region EVENTS
    protected void DropDownList_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        TypeChanged();
    }
    protected void ImageButtonClose_Click(object sender, ImageClickEventArgs e)
    {
        if (OnClose != null)
            OnClose(SorSzam, this);

        ResetData();
        if (SorSzam != null && SorSzam == "1")
            Visible = true;
        else
            Visible = false;
    }
    #endregion

    #region EXTERNAL EVENTS
    public delegate void Close(string sorszam, EUzenetSzabalyTargyUserControl self);
    public event Close OnClose;
    #endregion

    #region INTERFACE
    /// <summary>
    /// Beallitja a kontrolokat a model alapjan
    /// </summary>
    /// <param name="model"></param>
    public void SetBO(EKF_ComplexTargyModel model)
    {
        ResetData();
        InitDropDownListControls();

        TextBox_Prefix.Text = model.Prefix;
        TextBox_PostFix.Text = model.Postfix;
        TextBox_Value_TXT.Text = model.Value_TXT;
        TextBox_Value_XML.Text = model.Value_XML;

        DropDownList_Type.SelectedValue = model.Type;

        DropDownList_Value_DB.SelectedValue = model.Value_DB;

        HiddenField_ID.Value = model.Id.ToString();
        HiddenField_SorSzam.Value = model.SorSzam.ToString();

        TypeChanged();
    }
    /// <summary>
    /// Visszaadja a modelt a kontrolok alapjan
    /// </summary>
    /// <returns></returns>
    public EKF_ComplexTargyModel GetBO()
    {
        EKF_ComplexTargyModel model = new EKF_ComplexTargyModel();

        model.Prefix = TextBox_Prefix.Text;
        model.Postfix = TextBox_PostFix.Text;
        model.Value_TXT = TextBox_Value_TXT.Text;
        model.Value_XML = TextBox_Value_XML.Text;
        model.Type = DropDownList_Type.SelectedValue;
        model.Value_DB = DropDownList_Value_DB.SelectedValue;
        model.Id = HiddenField_ID.Value;

        byte tmpSorSzam = 254;
        if (byte.TryParse(HiddenField_SorSzam.Value, out tmpSorSzam))
            model.SorSzam = tmpSorSzam;

        return model;
    }

    /// <summary>
    /// IsValid
    /// </summary>
    /// <returns></returns>
    public bool IsValid()
    {
        EKF_ComplexTargyModel model = GetBO();
        if (model == null)
            return false;

        return model.IsValid();
    }

    /// <summary>
    /// Clear controls
    /// </summary>
    public void ResetData()
    {
        TextBox_Prefix.Text = string.Empty;
        TextBox_PostFix.Text = string.Empty;
        TextBox_Value_TXT.Text = string.Empty;
        TextBox_Value_XML.Text = string.Empty;

        DropDownList_Type.SelectedValue = null;
        DropDownList_Value_DB.SelectedValue = null;
    }

    /// <summary>
    /// SetVisibility
    /// </summary>
    /// <param name="visible"></param>
    public void SetVisibility(bool visible)
    {
        Visible = visible;
    }
    /// <summary>
    /// GetVisibility
    /// </summary>
    /// <param name="visible"></param>
    /// <returns></returns>
    public bool GetVisibility(bool visible)
    {
        return Visible;
    }
    #endregion

}