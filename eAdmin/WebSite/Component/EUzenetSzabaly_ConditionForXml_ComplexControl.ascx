﻿<%@ Control Language="C#" AutoEventWireup="true" 
    CodeFile="EUzenetSzabaly_ConditionForXml_ComplexControl.ascx.cs" 
    Inherits="Component_EUzenetSzabaly_ConditionForXml_ComplexControl" 
    Description="Összetett feltételek a csatolmány tartalmában" %>

<%@ Register src="EUzenetSzabaly_ConditionForXml_UserControl.ascx" tagname="EUzenetSzabaly_ConditionForXml_UserControl" tagprefix="ucc" %>

<asp:UpdatePanel ID="ErrorUpdatePanel_EUZCC" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel_EUZCC" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel_EUZCCS" runat="server">
    <ContentTemplate>

        <asp:Panel runat="server" ID="Panel_FROM_DATA">
            <div>
                <asp:HiddenField runat="server" ID="HiddenField_EUZCC_UtolsoSorszam" Value="1" />
                
                <asp:ImageButton runat="server" ID="ImageButton_EUZCC_Hozzaad"
                    ImageUrl="~/images/hu/muvgomb/ugyirat_letrehozasa.jpg" Height="25px"
                    OnClick="ImageButton_EUZCC_Hozzaad_Click" CausesValidation="false" />
                <br />

                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_01" runat="server" SorSzam="1" Visible="true" />
               
                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_02" runat="server" SorSzam="2" Visible="false" />
                
                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_03" runat="server" SorSzam="3" Visible="false" />
                
                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_04" runat="server" SorSzam="4" Visible="false" />
                
                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_05" runat="server" SorSzam="5" Visible="false" />
                
                <ucc:EUzenetSzabaly_ConditionForXml_UserControl ID="EUzenetSzabaly_ConditionForXml_UserControl_06" runat="server" SorSzam="6" Visible="false" />

            </div>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>

