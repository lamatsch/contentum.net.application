﻿using Contentum.eAdmin.BaseUtility;
using Contentum.eUtility.EKF;
using System;
using System.Collections.Generic;
using System.Web.UI;

public partial class Component_EUzenetSzabaly_ConditionForXml_ComplexControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region EVENTS
    protected void ImageButton_EUZCC_Hozzaad_Click(object sender, ImageClickEventArgs e)
    {
        VisibleNextControl();
    }
    #endregion

    /// <summary>
    /// VisibleNextControl
    /// </summary>
    private void VisibleNextControl()
    {
        int SorSzam = 2;
        if (int.TryParse(HiddenField_EUZCC_UtolsoSorszam.Value, out SorSzam))
        {
            ++SorSzam;
        }
        else
        {
            SorSzam = 2;
        }

        if (!EUzenetSzabaly_ConditionForXml_UserControl_02.Visible)
            EUzenetSzabaly_ConditionForXml_UserControl_02.Visible = true;
        else if (!EUzenetSzabaly_ConditionForXml_UserControl_03.Visible)
            EUzenetSzabaly_ConditionForXml_UserControl_03.Visible = true;
        else if (!EUzenetSzabaly_ConditionForXml_UserControl_04.Visible)
            EUzenetSzabaly_ConditionForXml_UserControl_04.Visible = true;
        else if (!EUzenetSzabaly_ConditionForXml_UserControl_05.Visible)
            EUzenetSzabaly_ConditionForXml_UserControl_05.Visible = true;
        else if (!EUzenetSzabaly_ConditionForXml_UserControl_06.Visible)
            EUzenetSzabaly_ConditionForXml_UserControl_06.Visible = true;

        HiddenField_EUZCC_UtolsoSorszam.Value = SorSzam.ToString();
    }

    #region COMPLEX
    /// <summary>
    /// GetAllModels
    /// </summary>
    /// <returns></returns>
    private List<EKFConditionForXML> GetAllModels()
    {
        List<EKFConditionForXML> result = new List<EKFConditionForXML>();
        EKFConditionForXML tmp = null;

        //if (!IsValidComplex())
        //{
        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel_EUZCC, Resources.Error.MessageError, Resources.Error.ErrorParameters);
        //ErrorUpdatePanel_EUZCC.Update();
        //throw new Exception(Resources.Error.ErrorParameters);
        //    return null;
        //}

        if (EUzenetSzabaly_ConditionForXml_UserControl_01.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_01.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }
        if (EUzenetSzabaly_ConditionForXml_UserControl_02.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_02.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }
        if (EUzenetSzabaly_ConditionForXml_UserControl_03.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_03.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }
        if (EUzenetSzabaly_ConditionForXml_UserControl_04.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_04.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }
        if (EUzenetSzabaly_ConditionForXml_UserControl_05.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_05.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }
        if (EUzenetSzabaly_ConditionForXml_UserControl_06.Visible)
        {
            tmp = EUzenetSzabaly_ConditionForXml_UserControl_06.GetBO();
            if (tmp != null && tmp.IsValid())
                result.Add(tmp);
        }

        return result;
    }
    /// <summary>
    /// SetAllModels
    /// </summary>
    /// <param name="models"></param>
    private void SetAllModels(List<EKFConditionForXML> models)
    {
        if (models == null || models.Count < 1)
            return;

        int index = 1;
        for (int i = 0; i < models.Count; i++)
        {
            if (models[i].IsValid())
            {
                switch (index)
                {
                    case 1:
                        EUzenetSzabaly_ConditionForXml_UserControl_01.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_01.Visible = true;
                        break;
                    case 2:
                        EUzenetSzabaly_ConditionForXml_UserControl_02.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_02.Visible = true;
                        break;
                    case 3:
                        EUzenetSzabaly_ConditionForXml_UserControl_03.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_03.Visible = true;
                        break;
                    case 4:
                        EUzenetSzabaly_ConditionForXml_UserControl_04.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_04.Visible = true;
                        break;
                    case 5:
                        EUzenetSzabaly_ConditionForXml_UserControl_05.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_05.Visible = true;
                        break;
                    case 6:
                        EUzenetSzabaly_ConditionForXml_UserControl_06.SetBO(models[i]);
                        EUzenetSzabaly_ConditionForXml_UserControl_06.Visible = true;
                        break;

                    default:
                        break;
                }
                ++index;
            }
        }
    }
    /// <summary>
    /// ResetComplex
    /// </summary>
    private void ResetComplex()
    {
        EUzenetSzabaly_ConditionForXml_UserControl_01.ResetData();

        EUzenetSzabaly_ConditionForXml_UserControl_02.ResetData();
        EUzenetSzabaly_ConditionForXml_UserControl_02.Visible = false;

        EUzenetSzabaly_ConditionForXml_UserControl_03.ResetData();
        EUzenetSzabaly_ConditionForXml_UserControl_03.Visible = false;

        EUzenetSzabaly_ConditionForXml_UserControl_04.ResetData();
        EUzenetSzabaly_ConditionForXml_UserControl_04.Visible = false;

        EUzenetSzabaly_ConditionForXml_UserControl_05.ResetData();
        EUzenetSzabaly_ConditionForXml_UserControl_05.Visible = false;

        EUzenetSzabaly_ConditionForXml_UserControl_06.ResetData();
        EUzenetSzabaly_ConditionForXml_UserControl_06.Visible = false;


    }
    /// <summary>
    /// IsValidComplex
    /// </summary>
    /// <returns></returns>
    private bool IsValidComplex()
    {
        if (
               (EUzenetSzabaly_ConditionForXml_UserControl_01.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_01.IsValid())
               ||
               (EUzenetSzabaly_ConditionForXml_UserControl_02.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_02.IsValid())
               ||
               (EUzenetSzabaly_ConditionForXml_UserControl_03.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_03.IsValid())
               ||
               (EUzenetSzabaly_ConditionForXml_UserControl_04.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_04.IsValid())
               ||
               (EUzenetSzabaly_ConditionForXml_UserControl_05.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_05.IsValid())
               ||
               (EUzenetSzabaly_ConditionForXml_UserControl_06.Visible && !EUzenetSzabaly_ConditionForXml_UserControl_06.IsValid())

           )
            return false;

        return true;
    }


    /// <summary>
    /// GetComplexTargyFormula
    /// </summary>
    /// <returns></returns>
    private List<EKFConditionForXML> GetComplexFormula()
    {
        List<EKFConditionForXML> models = GetAllModels();
        return models;
    }
    /// <summary>
    /// SetComplexTargyFormula
    /// </summary>
    /// <param name="targyFormula"></param>
    private void SetComplexFormula(List<EKFConditionForXML> items)
    {
        SetAllModels(items);
    }
    #endregion

    #region INTERFACES
    /// <summary>
    /// HasConditions
    /// </summary>
    /// <returns></returns>
    public bool HasConditions()
    {
        List<EKFConditionForXML> models = GetAllModels();
        if (models == null || models.Count < 1)
            return false;

        return true;
    }
    /// <summary>
    /// IsValid()
    /// </summary>
    /// <returns></returns>
    public bool IsValid()
    {
        bool isValidComp = IsValidComplex();
        return isValidComp;
    }
    /// <summary>
    /// GetTargyFormula
    /// </summary>
    /// <param name="targyForrasTipus"></param>
    /// <param name="targyFormula"></param>
    /// <returns></returns>
    public bool GetBO(out List<EKFConditionForXML> items)
    {
        items = new List<EKFConditionForXML>();

        Check();

        items = GetComplexFormula();
        return true;
    }
    /// <summary>
    /// Check
    /// </summary>
    public void Check()
    {
        if (!IsValid())
            throw new Exception(Resources.Error.ErrorMessage_EKF_ConditionInXMLFormula_Problem);
    }
    /// <summary>
    /// SetTargyFormula
    /// </summary>
    /// <param name="targyForrasTipus"></param>
    /// <param name="targyFormula"></param>
    public void SetBO(List<EKFConditionForXML> items)
    {
        SetComplexFormula(items);
    }
    #endregion
}