﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="EUzenetSzabaly_ConditionForXml_UserControl.ascx.cs"
    Inherits="Component_EUzenetSzabaly_ConditionForXml_UserControl"
    Description="Feltétel a csatolmány tartalmában" %>

<asp:UpdatePanel ID="ErrorUpdatePanel_EUZCA" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel_EUZCA" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="UpdatePanel_EUzenetSzabalyConditionInAttachmentUserControl" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:HiddenField ID="HiddenField_ID" runat="server" Value="" />
        <asp:HiddenField ID="HiddenField_SorSzam" runat="server" Value="" />

        <label for="TextBox_FileName">Fájlnév (.xml)</label>
        <asp:TextBox ID="TextBox_FileName" runat="server" MaxLength="254" placeholder="Fájlnév (.xml)" name="TextBox_FileName" ToolTip="Fájlnév (.xml)" Width="180px"></asp:TextBox>

        &nbsp;&nbsp;
        <label for="div_XPath">Figyelt XPath</label>
        <span id="div_XPath" title="Xml XPath">
            <asp:TextBox ID="TextBox_Xml_XPath" runat="server" placeholder="Figyelt Xml XPath" Width="180px" MaxLength="254"></asp:TextBox>
        </span>

        &nbsp;&nbsp;
        <label for="DropDownList_Operator">Operátor</label>
        <asp:DropDownList ID="DropDownList_Operator" runat="server" ToolTip="Típus" AutoPostBack="True" Width="180px"></asp:DropDownList>

        &nbsp;&nbsp;
        <label for="div_Value">Érték</label>
        <span id="div_Value" title="Érték">
            <asp:TextBox ID="TextBox_Value" runat="server" placeholder="Érték" Width="180px" MaxLength="254"></asp:TextBox>
        </span>

          &nbsp;&nbsp;
        <asp:ImageButton runat="server" ID="ImageButtonClose" ImageUrl="~/images/hu/ikon/delete-icon.png" Height="16px" CausesValidation="false" OnClick="ImageButtonClose_Click" />
    </ContentTemplate>
</asp:UpdatePanel>
