﻿using Contentum.eUtility.EKF;
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_EUzenetSzabaly_ConditionForXml_UserControl : System.Web.UI.UserControl
{
    [Bindable(true)]
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    public string SorSzam { get; set; }

    [Bindable(true)]
    [Browsable(true)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    public override bool Visible
    {
        get { return base.Visible; }
        set { base.Visible = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        InitOperator();

    }
    private void InitOperator()
    {
        if (DropDownList_Operator.Items.Count < 1)
        {
            foreach (EnumEKFConditionOperatorForXML name in Enum.GetValues(typeof(EnumEKFConditionOperatorForXML)))
            {
                DropDownList_Operator.Items.Add(new ListItem(name.ToString(), name.ToString()));
            }
        }
    }

    #region EVENTS
    protected void ImageButtonClose_Click(object sender, ImageClickEventArgs e)
    {
        if (OnClose != null)
            OnClose(SorSzam, this);

        ResetData();
        if (SorSzam != null && SorSzam == "1")
            Visible = true;
        else
            Visible = false;
    }
    #endregion

    #region EXTERNAL EVENTS
    public delegate void Close(string sorszam, Component_EUzenetSzabaly_ConditionForXml_UserControl self);
    public event Close OnClose;
    #endregion

    #region INTERFACE
    /// <summary>
    /// Beallitja a kontrolokat a model alapjan
    /// </summary>
    /// <param name="model"></param>
    public void SetBO(EKFConditionForXML model)
    {
        ResetData();
        InitOperator();

        TextBox_FileName.Text = model.FileNameProperty;
        TextBox_Xml_XPath.Text = model.XPathProperty;
        TextBox_Value.Text = model.ValueProperty;

        DropDownList_Operator.SelectedValue = model.OperatorProperty.ToString();

        HiddenField_ID.Value = model.Id.ToString();
        HiddenField_SorSzam.Value = model.SorSzam.ToString();
    }
    /// <summary>
    /// Visszaadja a modelt a kontrolok alapjan
    /// </summary>
    /// <returns></returns>
    public EKFConditionForXML GetBO()
    {
        EKFConditionForXML model = new EKFConditionForXML();

        model.FileNameProperty = TextBox_FileName.Text;
        model.XPathProperty = TextBox_Xml_XPath.Text;
        model.ValueProperty = TextBox_Value.Text;

        model.OperatorProperty = (EnumEKFConditionOperatorForXML)Enum.Parse(typeof(EnumEKFConditionOperatorForXML), DropDownList_Operator.SelectedValue);
        model.Id = HiddenField_ID.Value;

        byte tmpSorSzam = 254;
        if (byte.TryParse(HiddenField_SorSzam.Value, out tmpSorSzam))
            model.SorSzam = tmpSorSzam;

        return model.IsValid() ? model : null;
    }

    /// <summary>
    /// IsValid
    /// </summary>
    /// <returns></returns>
    public bool IsValid()
    {
        EKFConditionForXML model = GetBO();
        if (model == null)
            return false;

        return model.IsValid();
    }

    /// <summary>
    /// Clear controls
    /// </summary>
    public void ResetData()
    {
        TextBox_FileName.Text = string.Empty;
        TextBox_Xml_XPath.Text = string.Empty;
        TextBox_Value.Text = string.Empty;

        DropDownList_Operator.SelectedValue = null;
    }

    /// <summary>
    /// SetVisibility
    /// </summary>
    /// <param name="visible"></param>
    public void SetVisibility(bool visible)
    {
        Visible = visible;
    }
    /// <summary>
    /// GetVisibility
    /// </summary>
    /// <param name="visible"></param>
    /// <returns></returns>
    public bool GetVisibility(bool visible)
    {
        return Visible;
    }
    #endregion

}