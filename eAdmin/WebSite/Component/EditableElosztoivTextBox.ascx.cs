using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Specialized;

public partial class Component_EditableElosztoivTextBox : System.Web.UI.UserControl, ISelectableUserComponent, IScriptControl
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            ElosztoivMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public bool Enabled
    {
        set
        {
            ElosztoivMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return ElosztoivMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            ElosztoivMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }


        }
        get { return ElosztoivMegnevezes.ReadOnly; }
    }

    public string Text
    {
        set { ElosztoivMegnevezes.Text = value; }
        get { return ElosztoivMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return ElosztoivMegnevezes; }
    }

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }
    
    public HiddenField Control_HiddenField
    {
        get { return HiddenField1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                ElosztoivMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }


    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                ElosztoivMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                ElosztoivMegnevezes.ReadOnly = value;
                AutoCompleteExtender1.Enabled = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public string CssClass
    {
        get
        {
            return TextBox.CssClass;
        }
        set
        {
            TextBox.CssClass = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';return false";

        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax_eRecord.asmx";

        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        //JavaScripts.RegisterAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID, CimTextBoxClientId, CimHiddenFieldClientId, null);

        Contentum.eUtility.Configuration.Ajax.ConfigurePartnerAutoComplete(AutoCompleteExtender1);

        string query = "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + ElosztoivMegnevezes.ClientID;

        OnClick_Lov = JavaScripts.SetOnClientClick("ElosztoivLovList.aspx",
               query, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("ElosztoivekForm.aspx"
                , CommandName.Command + "=" + CommandName.New
                + query, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "ElosztoivekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);


        //Manu�lis be�r�s eset�n az id t�rl�se
        string jsClearHiddenField = "$get('" + HiddenField1.ClientID + "').value = '';";

        TextBox.Attributes.Add("onchange", jsClearHiddenField);
    }

    public void SetElosztoivTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            EREC_IraElosztoivekService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraElosztoivek EREC_IraElosztoivek = (EREC_IraElosztoivek)result.Record;
                Text = EREC_IraElosztoivek.NEV;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(ElosztoivMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.PartnerAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("Delimeter", Constants.AutoComplete.delimeter);
        descriptor.AddProperty("HiddenValue", "");

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
