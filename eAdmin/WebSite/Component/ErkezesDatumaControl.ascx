<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErkezesDatumaControl.ascx.cs" Inherits="Component_ErkezesDatumaControl" %>
<asp:TextBox ID="erkKezd_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="erkKezd_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png" OnClientClick="return false;" />&nbsp;
<asp:ImageButton TabIndex = "-1" ID="masol_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/ffwd.jpg" AlternateText="M�sol"  />
<asp:TextBox ID="erkVege_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="erkVege_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png" OnClientClick="return false;" />
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="erkKezd_ImageButton" TargetControlID="erkKezd_TextBox">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="erkVege_ImageButton" TargetControlID="erkVege_TextBox">
</ajaxToolkit:CalendarExtender>
<asp:RequiredFieldValidator ID="ErkKezd_RequiredFieldValidator" runat="server" SetFocusOnError="true" ControlToValidate="erkKezd_TextBox" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>">
</asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="ErkKezd_RequiredFieldValidator">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<asp:RequiredFieldValidator ID="ErkVege_RequiredFieldValidator" runat="server" SetFocusOnError="true" ControlToValidate="erkVege_TextBox" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>">
</asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="ErkVege_RequiredFieldValidator">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<asp:CompareValidator ID="CompareValidator1" runat="server" SetFocusOnError="true" ControlToCompare="erkKezd_TextBox" ControlToValidate="erkVege_TextBox" Display="None" ErrorMessage="<%$Resources:Form,Erk_kezd_vege_CompareValidatorMessage%>" Operator="GreaterThanEqual" Type="Date" Visible="False">
</asp:CompareValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="CompareValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
