<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErrorModalPopup.ascx.cs" Inherits="Component_ErrorModalPopup" %>
<script type="text/javascript">
     function emp_onContinue()  
     {  
       $find('<%=mdeException.ClientID %>').hide();
       return false;  
     }
     
     function emp_onRefresh()  
     {  
       window.location.reload();
     }
     
     function emp_toggleErrorDetails()  
     {  
      var divDetials = $get('divExceptionDetails');  
      var text = '';  
    
      if (divDetials.style.display == 'none')  
      {  
          divDetials.style.display = '';  
          text = 'R�szletek elrejt�se';  
      }  
      else  
      {  
          divDetials.style.display = 'none';  
          text = 'R�szletek megtekint�se';  
      }  
    
      emp_setText($get('lnkDetails'), text);  
     }
    
    function emp_setText(element, text)  
    {  
      if (typeof element.innerText != 'undefined')  
      {  
          element.innerText = text;  
      }  
      else if (typeof element.textContent != 'undefined')  
      {  
          element.textContent = text;  
      }  
    }
</script>    
<asp:Panel ID="pnlException" runat="server" CssClass="emp_Panel" style="display: none">
    <div style="padding: 8px">
        <h2 id="header" class="emp_HeaderWrapper">
            <asp:Label ID="labelHeader" runat="server" Text="Hiba" CssClass="emp_Header"></asp:Label>
        </h2>
        <div id="errorText" class="emp_ErrorText">
            <asp:Label ID="labelError" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <a id="lnkDetails" href="javascript:void(0)" style="text-decoration:underline" onclick="return emp_toggleErrorDetails()">R�szletek
            megtekint�se</a>
        <br />
        <div id="divExceptionDetails" class="emp_Details" style="display: none">
            <asp:Label ID="labelExceptionDetails" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <asp:Button CssClass="emp_OkButton" ID="btnContinue" runat="server" CausesValidation="false" 
            Text="Bez�r"/>  
        <asp:Button CssClass="emp_RefreshButton" style="display:none" ID="btnRefresh" runat="server" CausesValidation="false" 
            Text="Friss�t"/>       
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdeException" runat="server" TargetControlID="pnlException"
    PopupControlID="pnlException" OkControlID="btnContinue" BackgroundCssClass="emp_modalBackground"
    OnOkScript="emp_onContinue()" CancelControlID="btnRefresh" OnCancelScript="emp_onRefresh()"/>
