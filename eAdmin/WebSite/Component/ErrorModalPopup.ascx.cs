using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Component_ErrorModalPopup : System.Web.UI.UserControl
{
    #region public properties

    public AjaxControlToolkit.ModalPopupExtender ModulPopupExtender
    {
        get
        {
            return this.mdeException;
        }
    }

    public Label Header
    {
        get
        {
            return this.labelHeader;
        }
    }

    public string HeaderText
    {
        get
        {
            return this.labelHeader.Text;
        }
        set
        {
            this.labelHeader.Text = value;
        }
    }

    public Label Error
    {
        get
        {
            return this.labelError;
        }
    }

    public string ErrorText
    {
        get
        {
            return this.labelError.Text;
        }
        set
        {
            this.labelError.Text = value;
        }
    }

    public Label Details
    {
        get
        {
            return this.labelExceptionDetails;
        }
    }

    public string DetailText
    {
        get
        {
            return this.labelExceptionDetails.Text;
        }
        set
        {
            this.labelExceptionDetails.Text = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
