<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Ervenyesseg_SearchFormComponent.ascx.cs" Inherits="Component_Ervenyesseg_SearchFormComponent" %>


<%@ Register Src="~/Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc3" %>
<asp:Panel ID="Panel1" runat="server">
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="mrUrlapCaption">
            <asp:Label ID="Ervenyesseg_Label" runat="server" Text="Érvényesség:"></asp:Label></td>
        <td class="mrUrlapMezo">
            <table cellspacing="0" width="<%= Width.ToString() %>">
                <tr class="urlapSor">
                    <td style="width: 33%; padding-left: 0px;">
                        <asp:RadioButton ID="Ervenyes_RadioButton" runat="server" GroupName="Ervenyesseg"
                            Text="Érvényesek" /></td>
                    <td style="width: 33%">
                        <asp:RadioButton ID="Ervenytelen_RadioButton" runat="server" GroupName="Ervenyesseg"
                            Text="Érvénytelenek" /></td>
                    <td style="width: 33%">
                        <asp:RadioButton ID="ErvenyessegMindegy_RadioButton" runat="server" GroupName="Ervenyesseg"
                            Text="Összes" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="trErvenyessegTartomany" runat="server">
        <td class="mrUrlapCaption">
            <asp:Label ID="ErvenyessegTartomany_Label" runat="server" Text="Érvényesek ekkor:"></asp:Label></td>
        <td class="mrUrlapMezo">
            <asp:CheckBox ID="ervenyessegTartomany_CheckBox" runat="server" />
            <uc3:ErvenyessegCalendarControl id="ErvenyessegCalendarControl1" runat="server" SearchMode="true" ValidateDateFormat="true" />
        </td>
    </tr>
</table>
</asp:Panel>
