using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Collections.Generic;

public partial class Component_FelhasznaloCsoportTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl, UI.ILovListTextBox
{
    #region public properties

    private bool _RefreshCallingWindow = false;

    public bool RefreshCallingWindow
    {
        set { _RefreshCallingWindow = value; }
        get { return _RefreshCallingWindow; }
    }

    private bool _TryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _TryFireChangeEvent; }
        set { _TryFireChangeEvent = value; }
    }
   
    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool LabelRequiredIndicatorVisible
    {
        get
        {
            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    return c.Visible;
                }
            }

            return false;
        }
        set
        {
            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    c.Visible = value;
                }
            }
        }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { CsoportMegnevezes.Text = value; }
        get { return CsoportMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return CsoportMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public bool Enabled
    {
        set
        {
            CsoportMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return CsoportMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            CsoportMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;
            if (Validate)
            {
                Validator1.Enabled = !value;
            }
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.Visible = !value;
                cbAllDolgozo.Visible = false;
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.Visible = value;
            }

        }
        get { return CsoportMegnevezes.ReadOnly; }
    }



    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                CsoportMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                CsoportMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    public AjaxControlToolkit.AutoCompleteExtender AutoCompleteExtender
    {
        get
        {
            return AutoCompleteExtender1;
        }
    }

    private bool csakSajatSzervezetDolgozoi = false;
    // Legk�s�bb Page_Load()-ban �ll�tand�
    public bool CsakSajatSzervezetDolgozoi
    {
        get { return csakSajatSzervezetDolgozoi; }
        set { csakSajatSzervezetDolgozoi = value; if (value == true) { csakSajatCsoportDolgozoi = false; } }
    }

    private bool csakSajatCsoportDolgozoi = false;
    // Legk�s�bb Page_Load()-ban �ll�tand�
    public bool CsakSajatCsoportDolgozoi
    {
        get { return csakSajatCsoportDolgozoi; }
        set { csakSajatCsoportDolgozoi = value; if (value == true) { csakSajatSzervezetDolgozoi = false; } }
    }

    private bool sajatSzervezetDolgozoiVagyOsszes = false;
    public bool SajatSzervezetDolgozoiVagyOsszes
    {
        get
        {
            return sajatSzervezetDolgozoiVagyOsszes;
        }
        set
        {
            sajatSzervezetDolgozoiVagyOsszes = value;
            csakSajatSzervezetDolgozoi = value;
            if (!ReadOnly)
                cbAllDolgozo.Visible = value;
            if (value == true)
            {
                filterEnabled = true;
            }
        }
    }

    string szervezetId = String.Empty;
    public void FilterBySzervezetId(string p_szervezetId)
    {
        if (String.IsNullOrEmpty(p_szervezetId))
        {
            szervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        }
        else
        {
            szervezetId = p_szervezetId;
        }
    }
    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }

    private bool customTextEnabled = false;

    public bool CustomTextEnabled
    {
        get { return customTextEnabled; }
        set { customTextEnabled = value; }
    }

    #endregion

    private string queryString_Lov = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        LabelRequiredIndicatorVisible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode && !ReadOnly;

        Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();
        filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo;

        if (CsakSajatSzervezetDolgozoi == true)
        {
            filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi;
            filterObject.SzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        }
        else if (CsakSajatCsoportDolgozoi == true)
        {
            filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatCsoportDolgozoi;
            filterObject.SzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        }
        else if (!String.IsNullOrEmpty(szervezetId))
        {
            filterObject.SzervezetId = szervezetId;
        }

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
            WorldJumpEnabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            filterObject.FelhasznaloId = felh_Id;

            AutoCompleteExtender1.ContextKey = filterObject.JsSerialize();

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        queryString_Lov = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + CsoportMegnevezes.ClientID;

        queryString_Lov += "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0");
        if (TryFireChangeEvent)
        {
            queryString_Lov += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }
        // felhaszn�l�Csoport kell, megadjuk az URL-ben:       
        queryString_Lov += "&" + filterObject.QsSerialize();
        OnClick_Lov = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", queryString_Lov
                    , Defaults.PopupWidth, Defaults.PopupHeight, this.TextBox.ClientID, "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "CsoportokForm.aspx", "", HiddenField1.ClientID);

        OnClick_Reset = "var tbox=$get('" + TextBox.ClientID + "');tbox.value = '';$get('" +
            HiddenField1.ClientID + "').value = '';" +
            "$common.tryFireEvent(tbox, 'change');" +
            "return false;";

        if (ajaxEnabled)
        {
            if (!customTextEnabled)
            {
                //Manu�lis be�r�s eset�n az id t�rl�se
                string jsClearHiddenField = "var clear =true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
                    + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

                TextBox.Attributes.Add("onchange", jsClearHiddenField);
            }
        }
    }

    public void SetCsoportTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            // Csoport nev�nek lek�r�se a DB-b�l:
            String csoportNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportFromDB(Id_HiddenField, UI.SetExecParamDefault(Page)).Nev;

            if (csoportNev == null)
            {
                // hiba:
                errorPanel.Header = Resources.Error.ErrorLabel;
                errorPanel.Body = Resources.Error.ErrorText_CsoportTextBoxFill;
                errorPanel.Visible = true;
            }
            else
            {
                Text = csoportNev;
            }
        }
        else
        {
            Text = "";
        }
    }

    private Pair[] GetCsoportokList()
    {
        Ajax ajax = new Ajax();
        string[] jsRet = ajax.GetCsoportokList(String.Empty, 0, AutoCompleteExtender1.ContextKey);
        if(jsRet != null)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Pair[] ret = new Pair[jsRet.Length];
            for (int i = 0; i < jsRet.Length; i++)
            {
                Pair pair = js.Deserialize<Pair>(jsRet[i]);
                ret[i] = pair;
            }
            return ret;
        }
        return null;
    }

    public string[] GetCsoportokIdList()
    {
        Pair[] csoportPairList = GetCsoportokList();
        if (csoportPairList != null)
        {
            List<string> csoportIdList = new List<string>(csoportPairList.Length);
            foreach (Pair csoportPair in csoportPairList)
            {
                if (csoportPair.Second != null)
                    csoportIdList.Add(csoportPair.Second.ToString());
            }

            return csoportIdList.ToArray();
        }
        return null;
    }

    #region Filter

    private bool filterEnabled = false;
    private DropDownList filterDropDownList = null;
    private HiddenField filterHiddenField = null;

    public void SetFilterByIktatokonyv(DropDownList iktatokonyList)
    {
        if (iktatokonyList != null)
        {
            filterEnabled = true;
            filterDropDownList = iktatokonyList;
        }
    }
    
    public void SetFilterByIktatokonyv(HiddenField hf_iktatoKonyvId)
    {
        if (hf_iktatoKonyvId != null)
        {
            filterEnabled = true;
            filterHiddenField = hf_iktatoKonyvId;
        }
    }

    private Component_CsoportTextBox szervezetCsoportTextBox;
    public void SetFilterBySzervezet(Component_CsoportTextBox p_szervezetCsoportTextBox)
    {
        if (p_szervezetCsoportTextBox != null)
        {
            filterEnabled = true;
            szervezetCsoportTextBox = p_szervezetCsoportTextBox;
        }
    }

    #endregion

    public void Page_PreRender(object sender, EventArgs e)
    {
        LabelRequiredIndicatorVisible = Validate;

        if (filterEnabled)
        {
            //add script reference
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm == null)
            {
                throw new Exception("ScriptManager not found.");
            }
            if (!sm.IsInAsyncPostBack)
            {
                ScriptReference sr = new ScriptReference();
                sr.Path = "~/JavaScripts/CsoportTextBoxFilter.js";
                if (!sm.Scripts.Contains(sr))
                    sm.Scripts.Add(sr);
            }
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(CsoportMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;



        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);

                if (filterEnabled)
                {
                    Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();
                    if (filterDropDownList != null || filterHiddenField != null)
                    {
                        filterObject.IktatokonyvId = "' + $find('" + this.TextBox.ClientID + "_FilterBehavior').get_iktatokonyvId() + '";
                    }
                    if (szervezetCsoportTextBox != null)
                    {
                        filterObject.SzervezetId = "' + $find('" + this.TextBox.ClientID + "_FilterBehavior').get_szervezetId() + '";
                    }
                    queryString_Lov += "&" + filterObject.QsSerialize();
                    if (SajatSzervezetDolgozoiVagyOsszes)
                    {
                        //query string dinamikus �ll�t�sa
                        //Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();
                        string Tipus = "' + $find('" + this.TextBox.ClientID + "_FilterBehavior').get_tipus() + '";

                        int index = queryString_Lov.LastIndexOf("FilterByTipus=");
                        if (index > 0)
                        {
                            index += "FilterByTipus=".Length;
                            queryString_Lov = queryString_Lov.Substring(0, index) + Tipus;
                        }
                        else
                        {
                            queryString_Lov += "&FilterByTipus=" + Tipus;
                        }
                    }

                    OnClick_Lov = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", queryString_Lov
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
                }
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CsoportAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("CustomTextEnabled", customTextEnabled);
        if (filterEnabled)
        {
            ScriptControlDescriptor descriptorFilter = new ScriptControlDescriptor("Utility.CsoportTextBoxFilterBehavior", this.TextBox.ClientID);
            descriptorFilter.AddProperty("HiddenFieldClientId", HiddenField.ClientID);
            descriptorFilter.AddProperty("id", this.TextBox.ClientID + "_FilterBehavior");
            if (cbAllDolgozo.Visible)
            {
                descriptorFilter.AddProperty("CheckBoxAllDolgozoClientId", this.cbAllDolgozo.ClientID);
            }
            if (filterDropDownList != null)
            {
                descriptorFilter.AddProperty("ErkeztetoDropDownListClientId", this.filterDropDownList.ClientID);
            }
            if (filterHiddenField != null)
            {
                descriptorFilter.AddProperty("IktatoHiddenfieldClientId", this.filterHiddenField.ClientID);
            }
            if (szervezetCsoportTextBox != null)
            {
                descriptorFilter.AddProperty("SzervezetTextBoxClientId", this.szervezetCsoportTextBox.TextBox.ClientID);
                descriptorFilter.AddProperty("SzervezetHiddenFieldClientId", this.szervezetCsoportTextBox.HiddenField.ClientID);
            }


            return new ScriptDescriptor[] { descriptor, descriptorFilter };
        }

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js";

        if (filterEnabled)
        {
            ScriptReference referenceFilter = new ScriptReference();
            referenceFilter.Path = "~/JavaScripts/CsoportTextBoxFilter.js";

            return new ScriptReference[] { reference, referenceFilter };
        }

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ILovListTextBox Members


    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetCsoportTextBoxById(errorPanel);
    }

    #endregion
}
