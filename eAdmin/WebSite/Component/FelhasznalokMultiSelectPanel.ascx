<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FelhasznalokMultiSelectPanel.ascx.cs"
    Inherits="Component_FelhasznalokMultiSelectPanel" %>
<%@ Register Src="FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc4" %>
<%@ Register Src="CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc1" %>
<asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always" RenderMode="Block">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td colspan="3" style="text-align: left; vertical-align: top;">
                    <asp:RadioButtonList ID="rblFelhasznaloCsoport" runat="server" RepeatDirection="Horizontal"
                        Visible="false" AutoPostBack="false">
                        <asp:ListItem Selected="true" Text="Felhaszn�l�" Value="f" />
                        <asp:ListItem Text="Szervezet tagjai" Value="c" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr style="text-align: left;">
                <td colspan="2" class="mrUrlapCaption">
                    <table>
                        <tr runat="server" id="tr_felhasznaloselector">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFelhasznalo" runat="server" Text="Felhaszn�l�:" />
                            </td>
                            <td class="mrUrlapMezo" style="font-weight:normal;">
                                <uc4:FelhasznaloTextBox ID="TextBoxSearch" runat="server" Validate="false" CustomTextEnabled="false"
                                    WithEmail="true" />
                            </td>
                        </tr>
                        <tr runat="server" id="tr_csoportselector" style="display: none;">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelCsoport" runat="server" Text="Szervezet:" />
                            </td>
                            <td class="mrUrlapMezo" style="font-weight:normal;">
                                <uc1:CsoportTextBox ID="TextBoxSearchCsoport" runat="server" Validate="false" SzervezetCsoport="true"
                                    AjaxEnabled="true" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 100%">
                    <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="../images/hu/trapezgomb/hozzaad_trap.jpg"
                        onmouseover="swapByName(this.id,'hozzaad_trap2.jpg')" onmouseout="swapByName(this.id,'hozzaad_trap.jpg')"
                        AlternateText="Hozz�ad" OnClick="ButtonSearch_Click" Style="padding-right: 5px;" />
                    <asp:ImageButton ID="ButtonRemove" runat="server" ImageUrl="../images/hu/trapezgomb/torles_trap.jpg"
                        onmouseover="swapByName(this.id,'torles_trap2.jpg')" onmouseout="swapByName(this.id,'torles_trap.jpg')"
                        AlternateText="T�r�l" OnClick="ButtonRemove_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                    <div class="listaFulFelsoCsikKicsi">
                        <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; vertical-align: top;">
                    <%-- <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="5" Width="100%" />--%>
                    <select id="ListBoxSearchResult" runat="server" size="5" style="width: 100%;" multiple="true" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
