using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_FormFooter : System.Web.UI.UserControl
{
    private string _Command = "";
    
    public bool SaveEnabled
    {
        get { return ImageSave.Enabled; }
        set { ImageSave.Enabled = value; }
    }

    public ImageButton ImageButton_Save
    {
        get { return ImageSave; }        
    }
    public ImageButton ImageButton_SaveAndClose
    {
        get { return ImageSaveAndClose; }
    }

    public ImageButton ImageButton_SaveAndNew
    {
        get { return ImageSaveAndNew; }
    }
    public ImageButton ImageButton_Close
    {
        get { return ImageClose; }
    }
    public ImageButton ImageButton_Cancel
    {
        get { return ImageCancel; }
    }
    public ImageButton ImageButton_Back
    {
        get { return ImageBack; }
    }
    public ImageButton ImageButton_Accept
    {
        get { return ImageAccept; }
    }
    public ImageButton ImageButton_Decline
    {
        get { return ImageDecline; }
    }
    public ImageButton ImageButton_ResultList_ExportExcel
    {
        get { return ImageResultList_ExportExcel; }
    }

    public string Command
    {
        get { return _Command; }
        set { _Command = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Command == "")
        {
            Command = Request.QueryString.Get("Command");
        }
        setButtonsVisibleMode(Command);

        //ne lehessen t�bbsz�r kattintani szkript
        ImageButton_Save.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_Save);
        ImageButton_SaveAndNew.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_SaveAndNew);

        //UI.SwapImageToDisabled(ImageSave);

        //if (!ImageSave.Enabled)
        //{
        //    int pont = ImageSave.ImageUrl.IndexOf('.');
        //    ImageSave.ImageUrl = ImageSave.ImageUrl.Substring(0, pont) + "2" + ImageSave.ImageUrl.Substring(pont);
        //}

    }
    
    public event CommandEventHandler ButtonsClick;
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    private void setButtonsVisibleMode(string mode)
    {
        switch (mode)
        {
            case CommandName.New:
                ImageSave.Visible = true;
                ImageClose.Visible = false;
                ImageCancel.Visible = true;
                ImageBack.Visible = false;
                break;
            case CommandName.View:
                ImageSave.Visible = false;
                ImageClose.Visible = true;
                ImageCancel.Visible = false;
                ImageBack.Visible = false;
                break;
            case CommandName.Modify:
                ImageSave.Visible = true;
                ImageClose.Visible = false;
                ImageCancel.Visible = true;
                ImageBack.Visible = false;
                break;
            case CommandName.KolcsonzesJovahagyas:
                ImageAccept.Visible = true;
                ImageDecline.Visible = true;
                ImageClose.Visible = true;
                break;
            case CommandName.Invalidate:
                ImageSave.Visible = true;
                ImageClose.Visible = false;
                ImageCancel.Visible = true;
                ImageBack.Visible = false;
                break;
            default:
                ImageSave.Visible = false;
                ImageClose.Visible = true;
                ImageCancel.Visible = false;
                ImageBack.Visible = false;
                break;
        }
    }


    public string ValidationGroup
    {
        set
        {
            ImageSave.ValidationGroup = value;
        }
        get
        {
            return ImageSave.ValidationGroup;
        }
    }
}
