using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Collections.Generic;

public partial class Component_FormPart_CreatedModified : System.Web.UI.UserControl, Contentum.eUtility.ISelectableUserComponentContainer
{
    private string Command = "";

    private bool _addToNezet = true;

    public bool AddToNezet
    {
        get { return _addToNezet; }
        set { _addToNezet = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get("Command");
        setVisibleMode(Command);

        if (Command == CommandName.DesignView && AddToNezet)
        {
            ComponentSelectControl componentSelectControl = ComponentSelectControl.GetCurrent(Page);
            if (componentSelectControl != null)
            {
                componentSelectControl.Add_ComponentOnClick(this);
            }
        }
    }

    private void setVisibleMode(string mode)
    {
        switch (mode)
        {
            case CommandName.New:
                EFormPanel1.Visible = false;
                break;
            case CommandName.View:
                EFormPanel1.Visible = true;
                break;
            case CommandName.Modify:
                EFormPanel1.Visible = true;
                break;
            default:
                EFormPanel1.Visible = true;
                break;
        }
    }

#region public properties

    public string Letrehozo
    {
        get { return Letrehozo_TextBox.Text; }
        set { Letrehozo_TextBox.Text = value; }
    }

    public string LetrehozasIdo
    {
        get { return Letrehozas_ido_TextBox.Text; }
        set { Letrehozas_ido_TextBox.Text = value; }
    }

    public string Modosito
    {
        get { return Modosito_TextBox.Text; }
        set { Modosito_TextBox.Text = value; }
    }

    public string ModositasIdo
    {
        get { return Modositas_ido_TextBox.Text; }
        set { Modositas_ido_TextBox.Text = value; }
    }
	
#endregion public properties

    public void SetComponentValues(Contentum.eBusinessDocuments.BaseDocument baseDocument)
    {
        Letrehozo_TextBox.Text = eAdminService.GetFelhasznaloNevById(baseDocument.Letrehozo_id, Page);
        Letrehozas_ido_TextBox.Text = baseDocument.LetrehozasIdo;
        
        Modosito_TextBox.Text = eAdminService.GetFelhasznaloNevById(baseDocument.Modosito_id, Page);
        Modositas_ido_TextBox.Text = baseDocument.ModositasIdo;

        //Result _ret = new Result();
        //KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        //ExecParam execparam = new ExecParam();
        //execparam.Record_Id = baseDocument.Letrehozo_id;
        //_ret = service.Get(execparam);
        //if (_ret.Record != null)
        //{
        //    Letrehozo_TextBox.Text = (_ret.Record as KRT_Felhasznalok).Nev;
        //}
        //execparam.Record_Id = baseDocument.Modosito_id;
        //_ret = service.Get(execparam);
        //if (_ret.Record != null)
        //{
        //    Modosito_TextBox.Text = (_ret.Record as KRT_Felhasznalok).Nev;
        //}

    }



    #region ISelectableUserComponentContainer Members

    public System.Collections.Generic.List<Control> GetComponentList()
    {
        List<Control> componentList = new List<Control>();
        componentList.Add(EFormPanel1);
        componentList.AddRange(PageView.GetSelectableChildComponents(EFormPanel1.Controls));
        return componentList;
    }

    #endregion
}
