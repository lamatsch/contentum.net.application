﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FuggoKodtarakDropDownList.ascx.cs" Inherits="Component_FuggoKodtarakDropDownList" %>

<asp:DropDownList ID="Kodtarak_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" />
<eUI:CustomCascadingDropDown ID="Kodtarak_CascadingDropDown" runat="server" TargetControlID="Kodtarak_DropDownList"
    UseContextKey="true" ServicePath="~/WrappedWebService/Ajax.asmx"
    ServiceMethod="GetFuggokodtarak" EmptyText="<%$ Resources:Form, EmptyListItem %>"
    EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<%$ Resources:Form, EmptyListItem %>"
    PromptValue="" ContextKey="">
</eUI:CustomCascadingDropDown>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Kodtarak_DropDownList"
    Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>