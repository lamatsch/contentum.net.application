﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_FuggoKodtarakDropDownList : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region cascading properties

    public string KodcsoportKod
    {
        get
        {
            return Kodtarak_CascadingDropDown.Category;
        }

        set
        {
            Kodtarak_CascadingDropDown.Category = value;
        }
    }

    private string _ParentControlId;

    public string ParentControlId
    {
        get
        {
            return _ParentControlId;
        }
        set
        {
            _ParentControlId = value;
        }
    }

    private string ContextKey
    {
        get
        {
            return Kodtarak_CascadingDropDown.ContextKey;
        }
        set
        {
            Kodtarak_CascadingDropDown.ContextKey = value;
        }
    }

    public DropDownList DropDownList
    {
        get
        {
            return Kodtarak_DropDownList;
        }
    }

    #endregion

    public string SelectedValue
    {
        get { return Kodtarak_CascadingDropDown.SelectedValue; }
        set { Kodtarak_CascadingDropDown.SelectedValue = value; }
    }

    public string Text
    {
        get
        {
            if (Kodtarak_DropDownList.SelectedItem != null)
            {
                return Kodtarak_DropDownList.SelectedItem.Text;
            }
            else
            {
                return "";
            }
        }
    }

    public bool ReadOnly
    {
        get { return Kodtarak_DropDownList.ReadOnly; }
        set { Kodtarak_DropDownList.ReadOnly = value; }
    }

    public bool Enabled
    {
        get { return Kodtarak_DropDownList.Enabled; }
        set { Kodtarak_DropDownList.Enabled = value; }
    }

    public string CssClass
    {
        get { return Kodtarak_DropDownList.CssClass; }
        set { Kodtarak_DropDownList.CssClass = value; }
    }

    public bool Validate
    {
        get { return RequiredFieldValidator1.Enabled; }
        set { RequiredFieldValidator1.Enabled = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!String.IsNullOrEmpty(this.ParentControlId))
        {
            Component_FuggoKodtarakDropDownList parentControl = this.NamingContainer.FindControl(this.ParentControlId) as Component_FuggoKodtarakDropDownList;

            if (parentControl != null)
            {
                this.ContextKey = parentControl.KodcsoportKod;
                Kodtarak_CascadingDropDown.ParentControlID = parentControl.ID + "$" + parentControl.DropDownList.ID;
            }
        }
        base.OnPreRender(e);
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(Kodtarak_DropDownList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                DropDownList.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DropDownList.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}