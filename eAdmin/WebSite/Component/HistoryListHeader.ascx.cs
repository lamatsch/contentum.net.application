using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_HistoryListHeader : System.Web.UI.UserControl
{
    

    private static int _PageIndex = 0;
    private static int _PageCount = 0;

    private static string _PageName = "";

    public string PageName
    {
        get { return _PageName; }
        set
        {
            _PageName = value;
            Page.Title = value;
        }
    }

    public int PageIndex
    {
        get { return _PageIndex; }
        set { _PageIndex = value; }
    }
    public int PageCount
    {
        get { return _PageCount; }
        set { _PageCount = value; }
    }

    public String PagerLabel
    {
        get { return Pager.Text; }
        set { Pager.Text = value; }
    }

    public String HeaderLabel
    {
        get { return Header.Text; }
        set
        {
            Header.Text = value;
            PageName = value;
        }
    }

    #region Search
    public bool SearchEnabled
    {
        get { return ImageSearch.Enabled; }
        set 
        { 
            ImageSearch.Enabled = value;
            if (value == false)
            {
                ImageSearch.CssClass = "disableditem";
            }
        }
    }

    public String SearchOnClientClick
    {
        get { return ImageSearch.OnClientClick; }
        set { ImageSearch.OnClientClick = value; }
    }
    #endregion

    #region View
    public bool ViewEnabled
    {
        get { return ImageView.Enabled; }
        set { 
            ImageView.Enabled = value;
            if (value == false)
            {
                ImageView.CssClass = "disableditem";
            }
        }
    }

    public String ViewOnClientClick
    {
        get { return ImageView.OnClientClick; }
        set { ImageView.OnClientClick = value; }
    }
    #endregion


    #region Export
    public bool ExportEnabled
    {
        get { return ImageExport.Enabled; }
        set
        {
            ImageExport.Enabled = value;
            if (value == false)
            {
                ImageExport.CssClass = "disableditem";
            }
        }
    }
    public bool ExportVisible
    {
        get { return ImageExport.Visible; }
        set { ImageExport.Visible = value; }
    }
    public string ExportOnClientClick
    {
        get { return ImageExport.OnClientClick; }
        set { ImageExport.OnClientClick = value; }
    }
    public string ExportToolTip
    {
        get { return ImageExport.ToolTip; }
        set { ImageExport.ToolTip = value; }
    }
    #endregion


    #region Print
    public bool PrintEnabled
    {
        get { return ImagePrint.Enabled; }
        set
        {
            ImagePrint.Enabled = value;
            if (value == false)
            {
                ImagePrint.CssClass = "disableditem";
            }
        }
    }
    public bool PrintVisible
    {
        get { return ImagePrint.Visible; }
        set { ImagePrint.Visible = value; }
    }
    public string PrintOnClientClick
    {
        get { return ImagePrint.OnClientClick; }
        set { ImagePrint.OnClientClick = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

    }


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case "First":
                PageIndex = 0;
                break;
            case "Prev":
                if (PageIndex - 1 >= 0)
                {
                    PageIndex = PageIndex - 1;
                }
                break;
            case "Next":
                if (PageIndex + 1 < PageCount)
                {
                    PageIndex = PageIndex + 1;
                }
                break;
            case "Last":
                PageIndex = PageCount - 1;
                break;
        }

    }

    public event CommandEventHandler LeftFunkcionButtonsClick;

    public virtual void Left_FunkcionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (LeftFunkcionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            LeftFunkcionButtonsClick(this, args);
        }
    }

}
