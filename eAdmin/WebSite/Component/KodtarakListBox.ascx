<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KodtarakListBox.ascx.cs" Inherits="Component_KodtarakListBox" %>

<div runat="server" id = "listboxDiv" style="overflow-y: scroll; overflow-x:auto; POSITION: relative; HEIGHT: 80px;" >
<asp:ListBox ID="KodtarakListBox" runat="server" SelectionMode="Multiple"></asp:ListBox>
 </div>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="KodtarakListBox"
    Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
