﻿using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

public partial class Component_KodtarakListBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    //   private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

    //LZS - BUG_11346
    private string KodCsoport = string.Empty;
    //public enum ListModeEnum
    //{
    //    DropDownList,
    //    RadioButtonList
    //}

    //public ListModeEnum ListMode
    //{
    //    get
    //    {
    //        object o = ViewState["ListMode"];
    //        if (o != null)
    //            return (ListModeEnum)o;

    //        return ListModeEnum.DropDownList;
    //    }
    //    set
    //    {
    //        switch (value)
    //        {
    //            case ListModeEnum.DropDownList:
    //                Kodtarak_DropDownList.Visible = true;
    //                Kodtarak_RadioButtonList.Visible = false;
    //                break;
    //            case ListModeEnum.RadioButtonList:
    //                Kodtarak_DropDownList.Visible = false;
    //                Kodtarak_RadioButtonList.Visible = true;
    //                break;
    //            default:
    //                goto case ListModeEnum.DropDownList;
    //        }
    //        ViewState["ListMode"] = value;
    //    }
    //}

    //private ListControl _KodtarakList
    //{
    //    get
    //    {
    //       return KodtarakListBox;
    //    }
    //}

    public ListControl KodtarakList
    {
        get
        {
            return KodtarakListBox;
        }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        KodtarakListBox.SelectedIndexChanged += new EventHandler(KodtarakListBox_SelectedIndexChanged);

    }

    public void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }

    }

    void KodtarakListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (_SelectedIndexChanged != null)
        {
            _SelectedIndexChanged(sender, e);
        }
    }

    private event EventHandler _SelectedIndexChanged;

    public event EventHandler SelectedIndexChanged
    {
        add
        {
            _SelectedIndexChanged += value;
        }
        remove
        {
            _SelectedIndexChanged -= value;
        }
    }
    private Char _Separator = '|';
    public char Separator
    {
        get
        {
            return _Separator;
        }
        set
        {
            _Separator = value;
        }
    }
    public bool AutoPostBack
    {
        get
        {
            return KodtarakListBox.AutoPostBack;
        }
        set
        {
            KodtarakListBox.AutoPostBack = true;
        }
    }

    public CssStyleCollection Style
    {
        get
        {
            return KodtarakListBox.Style;
        }
    }

    //public eDropDownList DropDownList
    //{
    //    get { return Kodtarak_DropDownList; }
    //}

    //public RadioButtonList RadioButtonList
    //{
    //    get { return Kodtarak_RadioButtonList; }
    //}

    public Dictionary<String, String> SelectedItems
    {
        get
        {

            Dictionary<String, String> selectedItems = new Dictionary<string, string>();
            foreach (ListItem item in KodtarakListBox.Items)
            {
                if (item.Selected)
                {
                    selectedItems.Add(item.Value, item.Text);

                }

            }
            return selectedItems;
        }
        set { }
    }

    private string _selectedValues = string.Empty;
    public String SelectedValue
    {
        get
        {
            String selectedValues = String.Empty;
            if (Ismetlodo)
            {
                foreach (ListItem item in KodtarakListBox.Items)
                {
                    if (item.Selected)
                    {
                        selectedValues += item.Value + Separator;

                    }

                }
                if (!String.IsNullOrEmpty(selectedValues))
                    selectedValues = selectedValues.Remove(selectedValues.Length - 1, 1);
            }
            else
                selectedValues = KodtarakListBox.SelectedValue;
            return selectedValues;
        }
        set
        {
            String[] values = value.Split(Separator);
            //     if (Ismetlodo)
            {
                foreach (string aktValue in values)
                {
                    if (!String.IsNullOrEmpty(aktValue))
                    {
                        ListItem selectedListItem = KodtarakListBox.Items.FindByValue(aktValue);
                        if (selectedListItem != null)
                        {
                            selectedListItem.Selected = true;
                        }
                        else
                        {
                            //LZS - BUG_11346
                            DeletedKodTarErtek_Handling(this.KodCsoport, aktValue, true);
                            _selectedValues = value;
                        }
                    }
                }
            }
            //else
            //    KodtarakListBox.SelectedValue = values[0];
        }
    }
    //public Dictionary<String, bool> ItemsStatus ()
    //{
    //        Dictionary<String, bool> items = new Dictionary<string, bool>();
    //        foreach (ListItem item in KodtarakListBox.Items)
    //        {
    //            items.Add(item.Value, item.Selected);

    //        }
    //        return items;
    //}
    public String Text
    {
        get
        {
            if (KodtarakListBox.SelectedItem != null)
            {
                return KodtarakListBox.SelectedItem.Text;
            }
            else
            {
                return "";
            }
        }
    }

    public int Rows
    {
        get { return KodtarakListBox.Rows; }
        set { if (value > 0) KodtarakListBox.Rows = value; }
    }

    public bool Enabled
    {
        get { return KodtarakListBox.Enabled; }
        set { KodtarakListBox.Enabled = value; }
    }

    public bool Ismetlodo
    {
        get { return (KodtarakListBox.SelectionMode == ListSelectionMode.Multiple); }
        set { KodtarakListBox.SelectionMode = value ? ListSelectionMode.Multiple : ListSelectionMode.Single; }
    }
    //private bool _ReadOnly = false;
    //public bool ReadOnly
    //{
    //    get
    //    {
    //        return KodtarakListBox.Enabled;

    //    }
    //    set
    //    {
    //        int rowsCount = 0;
    //        foreach (ListItem item in KodtarakListBox.Items)
    //        {
    //            if (value)
    //            {
    //                item.Enabled = item.Selected;
    //                if (item.Enabled)
    //                {
    //                   // item.Attributes.Add("class", "Component_ReadOnly");
    //                    rowsCount++;
    //                }
    //            }
    //            else
    //                item.Enabled = true;
    //        }
    //        if (rowsCount > 0)
    //            Rows = rowsCount;
    //        KodtarakListBox.Enabled = !value;
    //    }
    //}
    //private bool _ReadOnly = false;
    //public bool ReadOnly
    //{
    //    get
    //    {
    //        return !KodtarakListBox.Enabled;

    //    }
    //    set
    //    {
    //        KodtarakListBox.Enabled = !value;
    //        //if (value)

    //        //{
    //        //    KodtarakListBox.Attributes.Add("disabled", "true");
    //        //} else
    //        //    KodtarakListBox.Attributes.Remove("disabled");
    //        //listboxDiv.Disabled = value;
    //        //KodtarakListBox.Enabled = true;
    //    }
    //}
    public bool ReadOnly
    {
        get
        {
            return !KodtarakListBox.Enabled;

        }
        set
        {
            KodtarakListBox.Enabled = !value;
        }
    }

    public Unit Width
    {
        get { return KodtarakListBox.Width; }
        set
        {
            KodtarakListBox.Width = value;
            listboxDiv.Attributes.Add("width", KodtarakListBox.Width.ToString());
        }
    }

    public string CssClass
    {
        get { return KodtarakListBox.CssClass; }
        set { KodtarakListBox.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            KodtarakListBox.ValidationGroup = value;
        }
        get { return KodtarakListBox.ValidationGroup; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    private DateTime? _Ervenyesseg;
    public DateTime? Ervenyesseg
    {
        get { return _Ervenyesseg; }
        set { _Ervenyesseg = value; }
    }

    private string _TextFormat;

    public string TextFormat
    {
        get { return _TextFormat; }
        set { _TextFormat = value; }
    }

    private string GetKodtarText(Contentum.eUtility.KodTar_Cache.KodTarElem kodTarElem)
    {
        //default
        if (String.IsNullOrEmpty(TextFormat))
            return kodTarElem.Nev;

        string text = TextFormat;
        //név
        text = text.Replace("#Nev", kodTarElem.Nev);
        //kód
        text = text.Replace("#Kod", kodTarElem.Kod);
        //rövid név
        text = text.Replace("#RovidNev", kodTarElem.RovidNev);

        return text;
    }

    #region Nincs funkciójog filter
    /// <summary>
    /// Feltölti a DropDownList-et a megadott kódcsoporthoz tartozó (érvényes) kódtárértékekkel
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="errorPanel"></param>
    public void FillListBox(String kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillListBox(kodcsoport, null, errorPanel);
    }

    public void FillListBox(String kodcsoport, List<string> filterList, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //LZS - BUG_11346
        this.KodCsoport = kodcsoport;

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

        if (kodtarakList == null)
        {
            errorPanel.Header = Resources.Error.ErrorLabel;
            errorPanel.Body = Resources.Error.ErrorText_KodtarDropDownFill; // TODOO
            errorPanel.Visible = true;
        }
        else
        {
            //  string oldselectedvalues = SelectedValue;

            //        oldselectedvalues.CopyTo(selectedValues, selectedValues.Length);     
            KodtarakListBox.Items.Clear();

            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
            {
                String kodtarKod = kte.Kod; // Key a kódtár kód
                String kodtarNev = kte.Nev; // Value a kódtárérték neve
                string kodtarText = GetKodtarText(kte);

                if (!IsInGlobalFilter(kodcsoport, kodtarKod))
                {
                    // ha van megadva szûrés lista, és nincs benne, nem adjuk hozzá:
                    if (filterList != null && filterList.Contains(kodtarKod) == false)
                    {
                        // nem adjuk hozzá
                    }
                    else
                    {
                        KodtarakListBox.Items.Add(new ListItem(kodtarText, kodtarKod));
                    }
                }
            }
            // SelectedValue = oldselectedvalues;
        }
        Rows = KodtarakListBox.Items.Count;
        //KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport);
        //if (String.IsNullOrEmpty(result.ErrorCode))
        //{
        //    _KodtarakList.Items.Clear();

        //    foreach (DataRow row in result.Ds.Tables[0].Rows)
        //    {
        //        String kodtarNev = row["Nev"].ToString();
        //        String kodtarKod = row["Kod"].ToString();
        //        _KodtarakList.Items.Add(new ListItem(kodtarNev, kodtarKod));
        //    }
        //    if (addEmptyItem)
        //    {
        //        _KodtarakList.Items.Insert(0, emptyListItem);
        //    }
        //}
        //else
        //{
        //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
        //}
    }

    private bool IsInGlobalFilter(string kodcsoport, string kodtarKod)
    {
        if (kodcsoport == "KEZELESI_FELJEGYZESEK_TIPUSA" && kodtarKod == KodTarak.KEZELESI_FELJEGYZESEK_TIPUSA.EloadoiMunkanaploMegjegyzes)
        {
            return true;
        }

        if (kodcsoport == KodTarak.FELADAT_ALTIPUS.kcsNev && kodtarKod == KodTarak.FELADAT_ALTIPUS.EloadoiMunkanaploMegjegyzes)
        {
            return true;
        }

        return false;
    }


    public void FillWithOneValue(string kodcsoport, string kodtar_ertek, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        KodtarakListBox.Items.Clear();

        //LZS - BUG_11346
        this.KodCsoport = kodcsoport;

        // Kódtárérték Cache-bõl:
        if (!String.IsNullOrEmpty(kodtar_ertek))
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
             Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

            if (kodtarakList == null)
            {
                errorPanel.Header = Resources.Error.ErrorLabel;
                errorPanel.Body = Resources.Error.ErrorText_KodtarDropDownFill;
                errorPanel.Visible = true;
            }
            else
            {
                //_KodtarakList.Items.Clear();
                Contentum.eUtility.KodTar_Cache.KodTarElem kodtarElem = null;
                foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
                {
                    if (kte.Kod == kodtar_ertek)
                    {
                        kodtarElem = kte;
                        break;
                    }
                }

                if (kodtarElem != null)
                {
                    KodtarakListBox.Items.Add(new ListItem(GetKodtarText(kodtarElem), kodtar_ertek));
                }
                else
                {
                    //LZS - BUG_11346
                    DeletedKodTarErtek_Handling(kodcsoport, kodtar_ertek, true);
                }

            }


            //KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport);

            //if (String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    _KodtarakList.Items.Clear();
            //    if (result.Ds.Tables[0].Rows.Count > 0)
            //    {
            //        bool isEqualKodtar = false;
            //        foreach (DataRow row in result.Ds.Tables[0].Rows)
            //        {
            //            String kodtarKod = row["Kod"].ToString();
            //            if (kodtarKod == kodtar)
            //            {
            //                String kodtarNev = row["Nev"].ToString();
            //                _KodtarakList.Items.Add(new ListItem(kodtarNev, kodtarKod));
            //                isEqualKodtar = true;
            //                break;
            //            }
            //        }
            //        if (!isEqualKodtar)
            //        {
            //            _KodtarakList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
            //        }
            //    }
            //    else
            //    {
            //        _KodtarakList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
            //    }
            //}
            //else
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            //}
        }

    }

    /// <summary>
    /// Feltölti a listát, és beállítja az értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    /// 
    public void FillAndSetSelectedValues(String kodcsoport, String[] selectedValues, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValues(kodcsoport, selectedValues, null, errorPanel);
    }

    public void FillAndSetSelectedValues(String kodcsoport, String selectedValuesStr, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        string[] selectedValues = selectedValuesStr.Split(Separator);
        FillAndSetSelectedValues(kodcsoport, selectedValues, null, errorPanel);
    }

    public void FillAndSetSelectedValues(String kodcsoport, String[] selectedValues, List<string> filterList, Contentum.eUIControls.eErrorPanel errorPanel)
    {

        FillListBox(kodcsoport, filterList, errorPanel);
        if (Ismetlodo)
        {
            foreach (string selectedValue in selectedValues)
            {
                ListItem selectedListItem = KodtarakListBox.Items.FindByValue(selectedValue);
                if (selectedListItem != null)
                {
                    selectedListItem.Selected = true;
                }
                else
                {
                    //LZS - BUG_11346
                    DeletedKodTarErtek_Handling(kodcsoport, selectedValue, true);
                }
            }
        }
        else
        {
            SelectedValue = selectedValues[0];
        }

    }

    //public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillAndSetSelectedValue(kodcsoport, selectedValue, false, errorPanel);
    //}

    //public void FillAndSetEmptyValue(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FillAndSetSelectedValue(kodcsoport, emptyListItem.Value, true, errorPanel);
    //}

    #endregion Nincs funkciójog filter

    #region Funkciójog filter

    #region Utils
    private List<string> GetFunkcioKodokByKodCsoport(string kodcsoport)
    {
        if (String.IsNullOrEmpty(kodcsoport)) return null;

        //if (ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] == null)
        //{
        KRT_FunkciokSearch search = new KRT_FunkciokSearch();
        search.Kod.Value = String.Format("KT_{0}_*_*abled", kodcsoport);  // "KT_<kódcsoport kód>_<kódtár kód>_Enabled", "KT_<kódcsoport kód>_<kódtár kód>_Disabled";
        search.Kod.Operator = Query.Operators.like;
        List<string> lstFunkcioKodok = FunctionRights.GetAllFunkcioKod(Page, search);

        //    ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] = lstFunkcioKodok;
        //}

        //return ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] as List<string>;
        return lstFunkcioKodok;
    }

    // Ha létezik egy adott kódtár értékhez definiált funkciójog, megengedõ vagy tiltó,
    // akkor amennyiben a felhasználónak nincs explicit megengedõjoga
    //  illetve van explicit tiltójoga, töröljük az elemet.
    // Ha nem létezik a  kódtár értékhez megadott névkonvenciót követõ funkciójog, akkor az elem látható marad.
    private void FilterListItemsByFunkcioJog(ListItemCollection listItems, string kodcsoport)
    {
        if (String.IsNullOrEmpty(kodcsoport)) return;
        if (listItems == null || listItems.Count == 0) return;

        List<string> lstFunkciok = GetFunkcioKodokByKodCsoport(kodcsoport);

        if (lstFunkciok != null)
        {
            for (int i = listItems.Count - 1; i >= 0; i--)
            {
                ListItem item = listItems[i];
                if (!String.IsNullOrEmpty(item.Value))
                {
                    string enabledFunkcio = String.Format("KT_{0}_{1}_Enabled", kodcsoport, item.Value);
                    string disabledFunkcio = String.Format("KT_{0}_{1}_Disabled", kodcsoport, item.Value);

                    if ((lstFunkciok.Contains(enabledFunkcio) && !FunctionRights.GetFunkcioJog(Page, enabledFunkcio))
                        || (lstFunkciok.Contains(disabledFunkcio) && FunctionRights.GetFunkcioJog(Page, disabledFunkcio)))
                    {
                        if (item.Selected == false)
                        {
                            listItems.Remove(item);
                        }
                        else
                        {
                            // ki van választva, csak megjelöljük
                            item.Text = String.Format("{0} {1}", item.Text, deletedValue);
                        }
                    }
                }
            }
        }
    }
    #endregion Utils

    /// <summary>
    /// Feltölti a DropDownList-et a megadott kódcsoporthoz tartozó (érvényes) kódtárértékekkel
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="errorPanel"></param>
    public void FillListBox(String kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillListBox(kodcsoport, null, errorPanel, bFilterByFunkcioJog);
    }

    public void FillListBox(String kodcsoport, List<string> filterList, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillListBox(kodcsoport, filterList, errorPanel);
        if (bFilterByFunkcioJog)
        {
            FilterListItemsByFunkcioJog(KodtarakListBox.Items, kodcsoport);
        }
    }


    public void FillWithOneValue(string kodcsoport, string kodtar_ertek, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillWithOneValue(kodcsoport, kodtar_ertek, errorPanel);
        if (bFilterByFunkcioJog)
        {
            FilterListItemsByFunkcioJog(KodtarakListBox.Items, kodcsoport);
        }
    }

    /// <summary>
    /// Feltölti a listát, és beállítja az értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    /// 
    public void FillAndSetSelectedValues(String kodcsoport, String[] selectedValues, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillAndSetSelectedValues(kodcsoport, selectedValues, null, errorPanel, bFilterByFunkcioJog);
    }

    public void FillAndSetSelectedValues(String kodcsoport, String[] selectedValues, List<string> filterList, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillListBox(kodcsoport, filterList, errorPanel, bFilterByFunkcioJog);
        foreach (string selectedValue in selectedValues)
        {
            ListItem selectedListItem = KodtarakListBox.Items.FindByValue(selectedValue);
            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
            else
            {
                //LZS - BUG_11346
                DeletedKodTarErtek_Handling(kodcsoport, selectedValue, true);
            }
        }
    }

    //public void FillAndSetSelectedValues(String kodcsoport, String[] selectedValues, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    //{
    //    FillAndSetSelectedValue(kodcsoport, selectedValue, false, errorPanel, bFilterByFunkcioJog);
    //}

    //public void FillAndSetEmptyValue(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    //{
    //    FillAndSetSelectedValue(kodcsoport, emptyListItem.Value, true, errorPanel, bFilterByFunkcioJog);
    //}

    #endregion Funkciójog filter

    /// <summary>
    /// Beállítja a már feltöltött lista értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValues(String[] selectedValues)
    {
        foreach (string selectedValue in selectedValues)
        {
            if (!String.IsNullOrEmpty(selectedValue))
            {
                ListItem selectedListItem = KodtarakListBox.Items.FindByValue(selectedValue);
                if (selectedListItem != null)
                {
                    selectedListItem.Selected = true;
                }
                else
                {
                    //LZS - BUG_11346
                    DeletedKodTarErtek_Handling(this.KodCsoport, selectedValue, true);
                }
            }
        }
    }

    private void DeletedKodTarErtek_Handling(string kodcsoport, string selectedValue, bool selected)
    {
        ListItem deletedItem = null;

        //LZS - BUG_11346
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakErvenytelenList =
       Contentum.eUtility.KodTar_Cache.GetErvenytelenKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

        var querykodtarakErvenytelenList = (from item in kodtarakErvenytelenList
                                            where item.Kod == selectedValue
                                            orderby item.ErvVege descending
                                            select item);

        //LZS - BUG_11346 - Amennyiben nincs érvényes, de van érvénytelen, és csak egy ilyen van az adott kóddal, akkor az jelenjen meg:
        //LZS - BUG_11346 - Amennyiben több is van az adott kóddal, akkor az jelenjen meg, amelyiknél az érvényességi idő vége a legkésőbbi:
        if (querykodtarakErvenytelenList != null && querykodtarakErvenytelenList.Count() > 0)
        {
            var queryResult = querykodtarakErvenytelenList.FirstOrDefault();
            KodtarakListBox.Items.Insert(0, new ListItem(queryResult.Nev, queryResult.Kod));
        }
        else//LZS - BUG_11346 - Amennyiben egyáltalán nincs az adott kódtár érték a kódcsoportban, akkor a kód érték + [Nem létező kódérték] szöveg jelenjen meg:
        {
            string itemText_Deleted = selectedValue + " " + deletedValue;
            // ha üres elem volt a selectedValue, akkor nem [Nem létező kódérték] szöveget írunk ki, hanem egy üres elemet adunk hozzá
            if (String.IsNullOrEmpty(selectedValue))
            {
                itemText_Deleted = String.Empty;
            }
            KodtarakListBox.Items.Insert(0, new ListItem(itemText_Deleted, selectedValue));
        }

        deletedItem.Selected = selected;
        KodtarakListBox.Items.Insert(0, deletedItem);
    }


    public void Clear()
    {
        KodtarakListBox.Items.Clear();
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(KodtarakList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                KodtarakList.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                KodtarakList.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        if (KodtarakListBox.SelectedIndex > -1)
        {
            foreach (ListItem item in KodtarakListBox.Items)
            {
                if (item.Selected)
                    text = item.Value + ",";

            }
            if (!String.IsNullOrEmpty(text))
                text = text.Remove(text.Length - 1, 1);
        }

        return text;
    }

    #endregion
}
