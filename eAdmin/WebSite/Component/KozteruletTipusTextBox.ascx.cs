using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_KozteruletTipusTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    public bool WatermarkEnabled
    {
        get { return TextBoxWatermarkExtender1.Enabled; }
        set { TextBoxWatermarkExtender1.Enabled = value; }
    }

    public bool ImageVisible
    {
        get { return KozteruletTipusImageButton.Visible; }
        set { KozteruletTipusImageButton.Visible = value; }
    }

    public string Text
    {
        set { KozteruletTipusNev_TextBox.Text = value; }
        get { return KozteruletTipusNev_TextBox.Text; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public string OnClick
    {
        set { KozteruletTipusImageButton.OnClientClick = value; }
        get { return KozteruletTipusImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return KozteruletTipusNev_TextBox; }
    }

    public bool Enabled
    {
        set
        {
            KozteruletTipusNev_TextBox.Enabled = value;
            KozteruletTipusImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (!value)
            {
                KozteruletTipusImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                KozteruletTipusImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return KozteruletTipusNev_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            KozteruletTipusNev_TextBox.ReadOnly = value;
            KozteruletTipusImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (value)
            {
                KozteruletTipusImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                KozteruletTipusImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return KozteruletTipusNev_TextBox.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public bool AutoCompleteEnabled
    {
        set { AutoCompleteExtender1.Enabled = value; }
        get { return AutoCompleteExtender1.Enabled; }
    }

    public string CssClass
    {
        get { return KozteruletTipusNev_TextBox.CssClass; }
        set 
        { 
            KozteruletTipusNev_TextBox.CssClass = value;
            TextBoxWatermarkExtender1.WatermarkCssClass = value + " " + TextBoxWatermarkExtender1.WatermarkCssClass;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                KozteruletTipusNev_TextBox.CssClass += " ViewReadOnlyWebControl";
                KozteruletTipusImageButton.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                KozteruletTipusNev_TextBox.CssClass += " ViewDisabledWebControl";
                KozteruletTipusImageButton.CssClass += " ViewDisabledWebControl";
            }
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("KozteruletTipusTipusokLovList.aspx","", Defaults.PopupWidth, Defaults.PopupHeight,KozteruletTipusImageButton.ClientID);

        //AutoCompleteExtender1.ServicePath = UI.GetAppSetting("eAdminBusinessServiceUrl") + "KRT_KozteruletTipusokService.asmx";
        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

        // �tt�ve a Page_Loadba, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        //String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

        OnClick = JavaScripts.SetOnClientClick("KozteruletTipusokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + KozteruletTipusNev_TextBox.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // autentik�ci� ut�n, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(KozteruletTipusNev_TextBox);
        componentList.Add(KozteruletTipusImageButton);

        KozteruletTipusImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
