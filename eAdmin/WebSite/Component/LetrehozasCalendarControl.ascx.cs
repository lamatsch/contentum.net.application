﻿
using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class Component_LetrehozasCalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region public properties

    public AjaxControlToolkit.CalendarPosition PopupPosition
    {
        get { return CalendarExtender1.PopupPosition; }
        set { CalendarExtender1.PopupPosition = value; CalendarExtender2.PopupPosition = value; }
    }

    public bool bOpenDirectionTop
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
        }
    }

    public bool bOpenDirectionLeft
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
            }
        }
    }


    
    // Ha searchMode true, akkor az letrehozasVege mező nem rejti el a default letrehozasVege értéket,
    // illetve kiolvasásnál az üres letrehozasVege nem adja vissza alapból a default letrehozasVege értéket (4700.12.31)
    private bool searchMode = true;
    public bool SearchMode 
    {
        get { return searchMode; }
        set { searchMode = value; }
    }

    public string letrehozasKezd
    {
        get 
        {
            if (!String.IsNullOrEmpty(letrehozasKezd_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(letrehozasKezd_TextBox.Text);
                    if (dt == DateTime.Today)
                        //return DateTime.Now.ToString();
                        return DateTime.Today.ToString();  // mai nap 0:0 -ra állítjuk
                    else
                        return dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    return letrehozasKezd_TextBox.Text;
                }
            }
            else return "";            
        }
        set 
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    letrehozasKezd_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    letrehozasKezd_TextBox.Text = value;
                }
            }
            else letrehozasKezd_TextBox.Text = "";
        }
    }    

    public string letrehozasVege
    {
        get
        {
            if (!String.IsNullOrEmpty(letrehozasVege_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(letrehozasVege_TextBox.Text);
                    dt = dt.AddHours(23 - dt.Hour);
                    dt = dt.AddMinutes(59 - dt.Minute);
                    dt = dt.AddSeconds(59 - dt.Second);
                    dt = dt.AddMilliseconds(999 - dt.Millisecond);
                    return dt.ToString();
                }
                catch (FormatException)
                {
                    return letrehozasVege_TextBox.Text;
                }
            }
            else
            {
                if (!searchMode)
                {
                    // Ha az létrehozás vége mező üres, akkor a default létrehozás vége dátumot vesszük (adatbázisban ez a 4700.12.31)
                    return new DateTime(4700, 12, 31).ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    // ha a dátum a default létrehozás vége, üresen hagyjuk a textboxot
                    // többféle default létrehozás vége volt használatban: 4700.12.31, 4700.01.01, 2099.12.31, 9999.12.31
                    if (!searchMode &&
                            (dt.Year == 4700
                            || dt.Year == 2099 && dt.Month == 12 && dt.Day == 31
                            || dt.Year == 9999)
                       )
                    {
                        letrehozasVege_TextBox.Text = "";
                    }
                    else
                    {
                        letrehozasVege_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                    }
                }
                catch (FormatException)
                {
                    letrehozasVege_TextBox.Text = value;
                }
            }
            else letrehozasVege_TextBox.Text = "";
        }
    }
    
    
    public bool ReadOnly_LetrehozasKezd
    {
        set
        {
            letrehozasKezd_TextBox.ReadOnly = value;
            letrehozasKezd_ImageButton.Enabled = !value;
            //letrehozasKezd_RequiredFieldValidator.Enabled = !value;
            CalendarExtender1.Enabled = !value;
            if (value)
            {
                letrehozasKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                letrehozasKezd_ImageButton.CssClass = "";
            }
        }
    }

    public bool ReadOnly_LetrehozasVege
    {
        set
        {
            letrehozasVege_TextBox.ReadOnly = value;
            letrehozasVege_ImageButton.Enabled = !value;
            //LetrehozasVege_RequiredFieldValidator.Enabled = !value;
            CalendarExtender2.Enabled = !value;
            if (value)
            {
                letrehozasVege_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                letrehozasVege_ImageButton.CssClass = "";
            }
        }
    }

    public bool Enable_LetrehozasKezd
    {
        set
        {
            letrehozasKezd_TextBox.Enabled = value;
            letrehozasKezd_ImageButton.Enabled = value;
            //letrehozasKezd_RequiredFieldValidator.Enabled = value;
            CalendarExtender1.Enabled = value;
            if (!value)
            {
                letrehozasKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                letrehozasKezd_ImageButton.CssClass = "";
            }
        }
    }

    public bool Enable_LetrehozasVege
    {
        set
        {
            letrehozasVege_TextBox.Enabled = value;
            letrehozasVege_ImageButton.Enabled = value;
            //LetrehozasVege_RequiredFieldValidator.Enabled = value;
            CalendarExtender2.Enabled = value;
            if (!value)
            {
                letrehozasVege_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                letrehozasVege_ImageButton.CssClass = "";
            }
        }
    }


    public TextBox LetrehozasKezd_TextBox
    {
        get { return letrehozasKezd_TextBox; }        
    }

    public TextBox LetrehozasVege_TextBox
    {
        get { return letrehozasVege_TextBox; }
    }

    public ImageButton LetrehozasKezd_ImageButton
    {
        get { return letrehozasKezd_ImageButton; }
    }

    public ImageButton LetrehozasVege_ImageButton
    {
        get { return letrehozasVege_ImageButton; }
    }

    //public bool Validate
    //{
    //    set
    //    {
    //        letrehozasKezd_RequiredFieldValidator.Enabled = value;
    //        letrehozasKezd_RequiredFieldValidator.Visible = value;
    //        //LetrehozasVege_RequiredFieldValidator.Enabled = value;
    //        //LetrehozasVege_RequiredFieldValidator.Visible = value;
    //    }
    //}

    /// <summary>
    /// Sets a value indicating whether this CustomValidator_DateFormat_Kezd and CustomValidator_DateFormat_Vege will validate.
    /// </summary>
    /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
    public bool ValidateDateFormat
    {
        set
        {
            CustomValidator_DateFormat_Kezd.Enabled = value;
            CustomValidator_DateFormat_Kezd.Visible = value;
            CustomValidator_DateFormat_Vege.Enabled = value;
            CustomValidator_DateFormat_Vege.Visible = value;
        }
    }

    public void SetDefault()
    {
        
    }

    Boolean _Enabled = true;

    public Boolean Enabled
    {
        get { return _Enabled; }
        set { 
            _Enabled = value;
            Enable_LetrehozasKezd = _Enabled;
            Enable_LetrehozasVege = _Enabled;
        }
    }

    Boolean _Readonly = false;

    public Boolean ReadOnly
    {
        get { return _Readonly; }
        set
        {
            _Readonly= value;
            ReadOnly_LetrehozasKezd = _Readonly;
            ReadOnly_LetrehozasVege = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            
            if (!_ViewEnabled)
            {
                letrehozasKezd_TextBox.CssClass += " ViewReadOnlyWebControl";
                letrehozasKezd_ImageButton.CssClass = "ViewReadOnlyWebControl";

                letrehozasVege_TextBox.CssClass += " ViewReadOnlyWebControl";
                letrehozasVege_ImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            
            if (!_ViewVisible)
            {
                letrehozasKezd_TextBox.CssClass += " ViewDisabledWebControl";
                letrehozasKezd_ImageButton.CssClass = "ViewDisabledWebControl";

                letrehozasVege_TextBox.CssClass += " ViewDisabledWebControl";
                letrehozasVege_ImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(letrehozasKezd_TextBox);
        componentList.Add(letrehozasKezd_ImageButton);
        componentList.Add(letrehozasVege_TextBox);
        componentList.Add(letrehozasVege_ImageButton);

        // Lekell tiltani a ClientValidator
        //letrehozasKezd_RequiredFieldValidator.Enabled = false;
        //LetrehozasVege_RequiredFieldValidator.Enabled = false;
        CompareValidator1.Enabled = false;
        CustomValidator_DateFormat_Kezd.Enabled = false;
        CustomValidator_DateFormat_Vege.Enabled = false;
        
        CalendarExtender1.Enabled = false;
        LetrehozasKezd_ImageButton.OnClientClick = "";
        CalendarExtender2.Enabled = false;
        LetrehozasVege_ImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        if (!String.IsNullOrEmpty(letrehozasKezd) && !String.IsNullOrEmpty(letrehozasVege))
        {
            text = letrehozasKezd + Query.OperatorNames[Query.Operators.between] + letrehozasVege;
        }
        else
        {
            if (!String.IsNullOrEmpty(letrehozasKezd))
            {
                text = Query.OperatorNames[Query.Operators.greaterorequal] + letrehozasKezd;
            }

            if (!String.IsNullOrEmpty(letrehozasVege))
            {
                text = Query.OperatorNames[Query.Operators.lessorequal] + letrehozasVege;
            }
        }

        return text;
    }

    #endregion
    #region IScriptControl Members

    private ScriptManager sm;
    private bool ajaxEnabled = true;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        List<ScriptReference> listOfScripRefernces = new List<ScriptReference>();

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Calendar.js";
        listOfScripRefernces.Add(reference);

        return listOfScripRefernces.ToArray();
    }

    #endregion

}
