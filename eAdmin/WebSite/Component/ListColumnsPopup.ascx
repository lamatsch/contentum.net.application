﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListColumnsPopup.ascx.cs" Inherits="Component_ListColumnsPopup" %>

<asp:Panel ID="pnlListColumn" runat="server" CssClass="listSortPanel" Style="display: none;">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <%--<div style="padding: 8px">--%>
        <h2 id="header" class="emp_HeaderWrapper" style="cursor: move;">
            <asp:Label ID="labelHeader" runat="server" Text="Oszlopok láthatóságának beállítása"
                CssClass="emp_Header"></asp:Label>
        </h2>
        <table>
            <tr>
                <td>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatLayout="Table" RepeatColumns="3"
                         CssClass="listColumnsPanel_CheckBoxList">                    
                    </asp:CheckBoxList>
                </td>
            </tr>            
            <tr>
            <td>
                <a id="lnkDetails" href="javascript:void(0)" style="text-decoration:underline" onclick="return showOrHideDetailsColumnsPanel()">
                    Mentés/visszaállítás...</a>
                <br /><br />
                <div id="divDetailsColumnsPanel" style="display: none">
                    <table style="width: 100%">
                        <tr style="text-align: center;">                                                   
                            <td>
                                <asp:Button ID="SaveTemplateButton" runat="server" Text="Megjelenítés mentése" OnClick="SaveButton_Click" />
                            </td>
                        </tr>
                        <tr style="text-align: center;">
                            <td>
                            <asp:Button ID="DeleteTemplateButton" runat="server" Text="Eredeti megjelenítés visszaállítása" OnClick="DeleteButton_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            </tr>
        </table>
        <br />
        
        <asp:Button ID="btnRefresh" runat="server" CausesValidation="false" Text="Frissít" 
                OnClientClick="cancelListColumnsPopup(); doPostBack_Refresh();" OnClick="btnRefresh_Click" />
        <asp:Button ID="btnContinue" runat="server" CausesValidation="false"
            Text="Bezár" />
        <%--</div>--%>
    </eUI:eFormPanel>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdeListColumnsPopup" runat="server"
    PopupControlID="pnlListColumn" OkControlID="btnContinue" BackgroundCssClass="emp_modalBackground" Y="150"
    OnOkScript="hideListColumnsPopup()" OnCancelScript="hideListColumnsPopup()" PopupDragHandleControlID="pnlListColumn" />