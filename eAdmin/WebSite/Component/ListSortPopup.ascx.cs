﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using Contentum.eAdmin.BaseUtility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Text;
using System.Xml;
using Contentum.eUIControls;

public partial class Component_ListSortPopup : System.Web.UI.UserControl
{
    private bool _isInitialized = false;

    public bool IsInitialized
    {
        get { return _isInitialized; }
    }

    private GridView _attachedGridView = null;

    public GridView AttachedGridView
    {
        get { return _attachedGridView; }
        set 
        { 
            _attachedGridView = value;
            _isInitialized = true;
        }
    }

    private Contentum.eUIControls.TreeGridView _attachedTreeGridView = null;

    public Contentum.eUIControls.TreeGridView AttachedTreeGridView
    {
        get { return _attachedTreeGridView; }
        set
        {
            _attachedTreeGridView = value;
            _isInitialized = true;
        }
    }

    private Type searchObjectType;
    public Type SearchObjectType
    {
        get { return searchObjectType; }
        set { searchObjectType = value; }
    }

    private String customSearchObjectSessionName = "";
    public String CustomSearchObjectSessionName
    {
        get { return customSearchObjectSessionName; }
        set { customSearchObjectSessionName = value; }
    }

    public string TargetControlID
    {
        get
        {
            return mdeListSortPopup.TargetControlID;
        }
        set
        {
            mdeListSortPopup.TargetControlID = value;
        }
    }

    private List<Contentum.eUIControls.eDropDownList> lstSortExpressionDropDowns = new List<Contentum.eUIControls.eDropDownList>();
    private List<Contentum.eUIControls.eDropDownList> lstSortDirectionDropDowns = new List<Contentum.eUIControls.eDropDownList>();

    public event EventHandler DefaultOrderBySaved;

    protected void Page_Init(object sender, EventArgs e)
    {
        // SortExpressions
        lstSortExpressionDropDowns.Add(SortExpression_DropDown_1);
        lstSortExpressionDropDowns.Add(SortExpression_DropDown_2);
        lstSortExpressionDropDowns.Add(SortExpression_DropDown_3);
        lstSortExpressionDropDowns.Add(SortExpression_DropDown_4);
        lstSortExpressionDropDowns.Add(SortExpression_DropDown_5);

        // SortDirections
        lstSortDirectionDropDowns.Add(SortDirection_DropDown_1);
        lstSortDirectionDropDowns.Add(SortDirection_DropDown_2);
        lstSortDirectionDropDowns.Add(SortDirection_DropDown_3);
        lstSortDirectionDropDowns.Add(SortDirection_DropDown_4);
        lstSortDirectionDropDowns.Add(SortDirection_DropDown_5);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetDropDownOnChangeEvent();
        //if (!IsPostBack)
            RegisterJavascripts();

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "disableFields", "disableFieldsStartCascading();", true);
    }

    private void RegisterJavascripts()
    {
//        string js = @"function showListSortPopup()
//        {                
//            $find('" + mdeListSortPopup.ClientID + @"').show();        
//            
//             var ddl_expr1 = $get('" + SortExpression_DropDown_1.ClientID + @"');
//                if (ddl_expr1 && ddl_expr1.onchange)
//                {
//                    ddl_expr1.onchange();                  
//                }
//                
//            return false;
//        }    
//    
//       function hideListSortPopup()  
//       {  
//           $find('" + mdeListSortPopup.ClientID + @"').hide();
//           return false;  
//       }
//       function cancelListSortPopup()
//       {
//        var closeButton = $get('" + btnContinue.ClientID + @"'); 
//        if (closeButton) {closeButton.click();}
//       }
//     
//       function doPostBack_Sort()
//       {
//        __doPostBack('" + btnSort.UniqueID + @"','');
//       }";

        string js = @"
               function disableFieldsStartCascading()
               {
                     var ddl_expr1 = $get('" + lstSortExpressionDropDowns[0].ClientID + @"');
                        if (ddl_expr1 && ddl_expr1.onchange)
                        {
                            ddl_expr1.onchange();                  
                        }
               }

               function showListSortPopup()
                {
                    $find('" + mdeListSortPopup.ClientID + @"').show();        
                    
                    disableFieldsStartCascading();
                        
                    return false;
                }    
            
               function hideListSortPopup()  
               {  
                   $find('" + mdeListSortPopup.ClientID + @"').hide();
                   return false;  
               }
               function cancelListSortPopup()
               {
                var closeButton = $get('" + btnContinue.ClientID + @"'); 
                if (closeButton) {closeButton.click();}
               }
             
               function doPostBack_Sort()
               {
                __doPostBack('" + btnSort.UniqueID + @"','');
               }
                     
               function showOrHideDetailsSortPanel()
               {
                    var divDetailsPanel = $get('divDetailsSortPanel');
                    var text = '';  
                  
                    if (divDetailsPanel.style.display == 'none')
                    {  
                        divDetailsPanel.style.display = '';
                    }  
                    else  
                    {  
                        divDetailsPanel.style.display = 'none';
                    }         
               }";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ListSortPopupScript", js, true);
    }

    private void SetDropDownOnChangeEvent()
    {
//        // kliensoldali események:
//        SortExpression_DropDown_1.Attributes["onchange"] = @"
//            var ddl_dir1 = $get('" + SortDirection_DropDown_1.ClientID + @"');
//            var ddl_expr2 = $get('" + SortExpression_DropDown_2.ClientID + @"');
//
//            if (ddl_dir1 && ddl_expr2 && ddl_expr2.onchange)
//            {
//                if (this.options[this.selectedIndex].value == '')
//                {                
//                    ddl_dir1.disabled = true;
//                    ddl_expr2.selectedIndex = 0;
//                    ddl_expr2.disabled = true;
//                    ddl_expr2.onchange();                  
//                }
//                else 
//                {   
//                    ddl_dir1.disabled = false;                    
//                    ddl_expr2.disabled = false;            
//                    ddl_expr2.onchange();
//                }
//            }";

//        SortExpression_DropDown_2.Attributes["onchange"] = @"
//            var ddl_dir2 = $get('" + SortDirection_DropDown_2.ClientID + @"');
//            var ddl_expr3 = $get('" + SortExpression_DropDown_3.ClientID + @"');
// 
//            if (ddl_dir2 && ddl_expr3 && ddl_expr3.onchange)
//            {
//                if (this.options[this.selectedIndex].value == '')
//                {                
//                    ddl_dir2.disabled = true;
//                    ddl_expr3.selectedIndex = 0;
//                    ddl_expr3.disabled = true;
//                    ddl_expr3.onchange();                  
//                }
//                else 
//                {   
//                    ddl_dir2.disabled = false;                    
//                    ddl_expr3.disabled = false;
//                    ddl_expr3.onchange();                    
//                }
//            }";

//        SortExpression_DropDown_3.Attributes["onchange"] = @"
//            var ddl_dir3 = $get('" + SortDirection_DropDown_3.ClientID + @"');
//
//            if (ddl_dir3)
//            {
//                if (this.options[this.selectedIndex].value == '')
//                {                
//                    ddl_dir3.disabled = true;
//                }
//                else 
//                {   
//                    ddl_dir3.disabled = false;                                        
//                }
//            }";

        // kliensoldali események:
        int lastDropDownNo = lstSortExpressionDropDowns.Count - 1;
        for (int i = 0; i < lastDropDownNo; i++)
        {
            Contentum.eUIControls.eDropDownList dropdownSortExpression = lstSortExpressionDropDowns[i];
            Contentum.eUIControls.eDropDownList dropdownSortDirection = lstSortDirectionDropDowns[i];
            Contentum.eUIControls.eDropDownList dropdownSortExpression_Next = lstSortExpressionDropDowns[i+1];

            dropdownSortExpression.Attributes["onchange"] = @"
            var ddl_dir1 = $get('" + dropdownSortDirection.ClientID + @"');
            var ddl_expr2 = $get('" + dropdownSortExpression_Next.ClientID + @"');

            if (ddl_dir1 && ddl_expr2 && ddl_expr2.onchange)
            {
                if (this.options[this.selectedIndex].value == '')
                {                
                    ddl_dir1.disabled = true;
                    ddl_expr2.selectedIndex = 0;
                    ddl_expr2.disabled = true;
                    ddl_expr2.onchange();                  
                }
                else 
                {   
                    ddl_dir1.disabled = false;                    
                    ddl_expr2.disabled = false;            
                    ddl_expr2.onchange();
                }
            }";
        }

        // az utolsó elemnél már nem kell a rákövetkezőt letiltani
        lstSortExpressionDropDowns[lastDropDownNo].Attributes["onchange"] = @"
            var ddl_dir1 = $get('" + lstSortDirectionDropDowns[lastDropDownNo].ClientID + @"');

            if (ddl_dir1)
            {
                if (this.options[this.selectedIndex].value == '')
                {                
                    ddl_dir1.disabled = true;
                }
                else 
                {   
                    ddl_dir1.disabled = false;                                        
                }
            }";
    }

    public void FillSortExpressionDropDown()
    {
        if (_attachedGridView != null) FillSortExpressionDropDown(_attachedGridView.Columns);
        if (_attachedTreeGridView != null) FillSortExpressionDropDown(_attachedTreeGridView.Columns);
    }

    public void FillSortExpressionDropDown(DataControlFieldCollection Columns)
    {
        if (Columns == null) {return;}

        ListItemCollection listItemCollection = new ListItemCollection();

        foreach(DataControlField dataControlField in Columns)
        {
            string headerText = dataControlField.HeaderText;
            string sortExpression = dataControlField.SortExpression;

            listItemCollection.Add(new ListItem(headerText, sortExpression));
        }

        FillSortExpressionDropDown(listItemCollection);
    }

    public void FillSortExpressionDropDown(ListItemCollection sortItemCollection)
    {
        if (sortItemCollection == null) { return; }

        //SortExpression_DropDown_1.Items.Clear();
        //SortExpression_DropDown_1.Items.Add("");
        //SortExpression_DropDown_2.Items.Clear();
        //SortExpression_DropDown_2.Items.Add("");
        //SortExpression_DropDown_3.Items.Clear();
        //SortExpression_DropDown_3.Items.Add("");

        foreach (Contentum.eUIControls.eDropDownList dropdown in lstSortExpressionDropDowns)
        {
            dropdown.Items.Clear();
            dropdown.Items.Add("");
        }

        foreach (ListItem sortItem in sortItemCollection)
        {

            if (!String.IsNullOrEmpty(sortItem.Text) && !String.IsNullOrEmpty(sortItem.Value))
            {
                //SortExpression_DropDown_1.Items.Add(new ListItem(sortItem.Text, sortItem.Value));
                //SortExpression_DropDown_2.Items.Add(new ListItem(sortItem.Text, sortItem.Value));
                //SortExpression_DropDown_3.Items.Add(new ListItem(sortItem.Text, sortItem.Value));

                foreach (Contentum.eUIControls.eDropDownList dropdown in lstSortExpressionDropDowns)
                {
                    dropdown.Items.Add(new ListItem(sortItem.Text, sortItem.Value));
                }
            }
        }
        // BUG_8359
        // Volt-e már rekord erre a templateTípusra?
        string templateTipusNev = String.Empty;
        if (searchObjectType != null)
            templateTipusNev = GetOrderByTemplateTipusNev(searchObjectType, customSearchObjectSessionName);

        Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy> columnsOrderByDict;

        if (Page.Session[Constants.SessionNames.DefaultOrderByDictionary] == null)
        {
            #region Elmentett rendezettség adatok betöltése

            KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

            search.Felhasznalo_Id.Value = FelhasznaloProfil.FelhasznaloId(Page);
            search.Felhasznalo_Id.Operator = Query.Operators.equals;

            search.Alapertelmezett.Value = Constants.Database.Yes;
            search.Alapertelmezett.Operator = Query.Operators.equals;

            search.TemplateTipusNev.Value = templateTipusNev;
            search.TemplateTipusNev.Operator = Query.Operators.equals;

            ExecParam execParam = UI.SetExecParamDefault(Page);

            Result result = service.GetAll(execParam, search);
            if (result.IsError)
            {
                // hiba:
                Logger.Error("Hiba a default KRT_UIMezoObjektumErtekek értékek lekérésekor (OrderBy értékekhez)", execParam, result);
                return;
            }

            columnsOrderByDict = Contentum.eUtility.UIMezoObjektumErtekek.CreateOrderByDictionary(result.Ds);

            #endregion
        }
        else
        {
            columnsOrderByDict = (Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy>)Page.Session[Constants.SessionNames.DefaultOrderByDictionary];
        }

        if (columnsOrderByDict != null)
        {
            if (columnsOrderByDict.ContainsKey(templateTipusNev))
            {
                Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy gridViewOrderBy = columnsOrderByDict[templateTipusNev];
                string sortExpr = Contentum.eUtility.Search.FullSortExpression;
                bool empty = true;

                foreach (KeyValuePair<int, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy.ColumnProperties> kvp in gridViewOrderBy.Values)
                {
                    int index = kvp.Key;
                    Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy.ColumnProperties columnProperties = kvp.Value;

                     lstSortExpressionDropDowns[index].SelectedValue = columnProperties.sortExpression;
                     lstSortDirectionDropDowns[index].SelectedValue = columnProperties.sortDirection;

                    string columnName = columnProperties.columnName;
                    string sortExpression = columnProperties.sortExpression;
                    string sortDirection = columnProperties.sortDirection;

                    if (!empty) sortExpr += ", ";
                    sortExpr += String.Format(sortExpression, sortDirection) + " " + sortDirection;
                    empty = false;

                }
                this.sortExpression = sortExpr;
            }

        }

        //if (!String.IsNullOrEmpty(this.sortExpression))
        //{
        //    SetSortExpression(this.sortExpression);
        //}

    }

    protected void btnSort_Click(object sender, EventArgs e)
    {

        if (AttachedGridView != null)
        {
            // A Sort() fv.-ben a rendezés iránya itt mindegy, a SortExpression-be rakjuk bele azt is (mert lehet több oszlop, több irány is)
            AttachedGridView.Sort(GetSortExpression(), SortDirection.Ascending);
        }
        else if (AttachedTreeGridView != null)
        {
            // A Sort() fv.-ben a rendezés iránya itt mindegy, a SortExpression-be rakjuk bele azt is (mert lehet több oszlop, több irány is)
            AttachedTreeGridView.Sort(GetSortExpression(), SortDirection.Ascending);
        }

        RaiseSortButtonClick(this, e);
    

    }

    public string GetSortExpression()
    {
        // [FullSortExpression] -nel kezdjük, hogy meg tudjuk különböztetni az eddigi rendezési módszertől (hogy ne dobjunk a végére még egy ASC-et vagy DESC-et)
        string expr = Contentum.eUtility.Search.FullSortExpression;
        bool empty = true;

        //if (SortExpression_DropDown_1 != null
        //    && SortExpression_DropDown_1.Enabled == true
        //    && !String.IsNullOrEmpty(SortExpression_DropDown_1.SelectedValue))
        //{
        //    expr += SortExpression_DropDown_1.SelectedValue + " " + SortDirection_DropDown_1.SelectedValue;
        //    empty = false;
        //}

        //if (SortExpression_DropDown_2 != null
        //    && SortExpression_DropDown_2.Enabled == true
        //    && !String.IsNullOrEmpty(SortExpression_DropDown_2.SelectedValue))
        //{
        //    if (!empty) expr += ", ";
        //    expr += SortExpression_DropDown_2.SelectedValue + " " + SortDirection_DropDown_2.SelectedValue;
        //    empty = false;
        //}
        
        //if (SortExpression_DropDown_3 != null
        //    && SortExpression_DropDown_3.Enabled == true
        //    && !String.IsNullOrEmpty(SortExpression_DropDown_3.SelectedValue))
        //{
        //    if (!empty) expr += ", ";
        //    expr += SortExpression_DropDown_3.SelectedValue + " " + SortDirection_DropDown_3.SelectedValue;
        //    empty = false;
        //}

        // BUG_8359
        for (int i = 0; i < lstSortExpressionDropDowns.Count; i++)
        {
            Contentum.eUIControls.eDropDownList dropdownSortExpression = lstSortExpressionDropDowns[i];
            Contentum.eUIControls.eDropDownList dropdownSortDirection = lstSortDirectionDropDowns[i];
            if (dropdownSortExpression != null
                && dropdownSortExpression.Enabled == true
                && !String.IsNullOrEmpty(dropdownSortExpression.SelectedValue))
            {
                if (!empty) expr += ", ";
                expr += String.Format(dropdownSortExpression.SelectedValue, dropdownSortDirection.SelectedValue) + " " + dropdownSortDirection.SelectedValue;
                empty = false;
            }
        }

        return expr;
    }

    private string sortExpression;

    public void SetSortExpression(string sortExpression)
    {
        this.sortExpression = sortExpression;
        if (sortExpression.StartsWith(Contentum.eUtility.Search.FullSortExpression))
        {
            // [FullSortExpression] string levágása:
            sortExpression = sortExpression.Remove(0, Contentum.eUtility.Search.FullSortExpression.Length);
        }

        string[] sortParts = sortExpression.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);

        //SortExpression_DropDown_1.SelectedIndex = -1;
        //SortExpression_DropDown_2.SelectedIndex = -1;
        //SortExpression_DropDown_3.SelectedIndex = -1;

        foreach (Contentum.eUIControls.eDropDownList dropdown in lstSortExpressionDropDowns)
        {
            dropdown.SelectedIndex = -1;
        }

        for (int i = 0; i < sortParts.Length; i++)
        {
            string sortPart = sortParts[i];
            string[] sortSubParts = sortPart.Split(' ');
            string sortColumn = sortSubParts[0];
            string sortDirection = "ASC";
            if (sortSubParts.Length > 1)
            {
                sortDirection = sortSubParts[1];
            }

            //if (i == 0)
            //{
            //    try
            //    {
            //        SortExpression_DropDown_1.SelectedValue = sortColumn;
            //        SortDirection_DropDown_1.SelectedValue = sortDirection;
            //        continue;
            //    }
            //    catch(Exception ex)
            //    {
            //    }
            //}
            //if (i == 1)
            //{
            //    try
            //    {
            //        SortExpression_DropDown_2.SelectedValue = sortColumn;
            //        SortDirection_DropDown_2.SelectedValue = sortDirection;
            //        continue;
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}
            //if (i == 2)
            //{
            //    try
            //    {
            //        SortExpression_DropDown_3.SelectedValue = sortColumn;
            //        SortDirection_DropDown_3.SelectedValue = sortDirection;
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}

            if (i < lstSortExpressionDropDowns.Count)
            {
                try
                {
                    lstSortExpressionDropDowns[i].SelectedValue = sortColumn;

                    if (i < lstSortDirectionDropDowns.Count)
                    {
                        lstSortDirectionDropDowns[i].SelectedValue = sortDirection;
                    }
                    continue;
                }
                catch (Exception ex)
                {
                }
            }
        }
    }

    protected event EventHandler _sortButtonClick;

    public event EventHandler SortButtonClick
    {
        add
        {
            _sortButtonClick += value;
        }
        remove
        {
            _sortButtonClick -= value;
        }
    }

    protected void RaiseSortButtonClick(object seneder, EventArgs e)
    {
        if (_sortButtonClick != null)
        {
            _sortButtonClick(seneder, e);
        }
    }

    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        if (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return;
        }

        // Volt-e már rekord erre a templateTípusra?
        string templateTipusNev = GetOrderByTemplateTipusNev(searchObjectType, customSearchObjectSessionName);


        #region Keresés:
        Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

        KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

        search.Felhasznalo_Id.Value = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        search.Felhasznalo_Id.Operator = Query.Operators.equals;

        search.TemplateTipusNev.Value = templateTipusNev;
        search.TemplateTipusNev.Operator = Query.Operators.equals;

        search.Alapertelmezett.Value = Contentum.eUtility.Constants.Database.Yes;
        search.Alapertelmezett.Operator = Query.Operators.equals;

        ExecParam execParam = Contentum.eUtility.UI.SetExecParamDefault(Page);

        Result result_Search = service.GetAll(execParam, search);
        if (result_Search.IsError)
        {
            // hiba
            Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            return;
        }

        #endregion

        if (result_Search.Ds.Tables[0].Rows.Count > 0)
        {
            #region Invalidate

            ExecParam[] execParamArray = new ExecParam[result_Search.Ds.Tables[0].Rows.Count];

            for (int i = 0; i < result_Search.Ds.Tables[0].Rows.Count; i++)
            {
                execParamArray[i] = Contentum.eUtility.UI.SetExecParamDefault(Page);
                execParamArray[i].Record_Id = result_Search.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            Result result_invalidate = service.MultiInvalidate(execParamArray);
            if (result_invalidate.IsError)
            {
                // hiba
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.MultiInvalidate ", execParam, result_invalidate);
                return;
            }

            #endregion
        }


        #region Sessionben lévő Dictionary-ből is törlés
        if (Page.Session[Constants.SessionNames.DefaultOrderByDictionary] != null)
        {
            Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy> columnsOrderByDict =
                (Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy>)Page.Session[Constants.SessionNames.DefaultOrderByDictionary];

            if (columnsOrderByDict.ContainsKey(templateTipusNev))
            {
                columnsOrderByDict.Remove(templateTipusNev);
            }
        }
       
        #endregion

        Response.Redirect(Request.Url.OriginalString, true);
       
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        if (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return;
        }

        // Insert vagy Update kell
        // Volt-e már rekord erre a templateTípusra?

        string templateTipusNev = GetOrderByTemplateTipusNev(searchObjectType, customSearchObjectSessionName);

        #region Keresés:
        Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

        KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

        search.Felhasznalo_Id.Value = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        search.Felhasznalo_Id.Operator = Query.Operators.equals;

        search.TemplateTipusNev.Value = templateTipusNev;
        search.TemplateTipusNev.Operator = Query.Operators.equals;

        search.Alapertelmezett.Value = Contentum.eUtility.Constants.Database.Yes;
        search.Alapertelmezett.Operator = Query.Operators.equals;

        ExecParam execParam = Contentum.eUtility.UI.SetExecParamDefault(Page);

        Result result_Search = service.GetAll(execParam, search);
        if (result_Search.IsError)
        {
            // hiba
            Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            return;
        }

        #endregion

        string templateId = String.Empty;
        string templateVer = String.Empty;

        if (result_Search.Ds.Tables[0].Rows.Count > 1)
        {
            // ha valamiért több van, akkor az elsőt beállítjuk update-re, a többit érvénytelenítjük:
            templateId = result_Search.Ds.Tables[0].Rows[0]["Id"].ToString();
            templateVer = result_Search.Ds.Tables[0].Rows[0]["Ver"].ToString();

            #region Invalidate
            ExecParam[] execParamArray = new ExecParam[result_Search.Ds.Tables[0].Rows.Count - 1];
            // a 0.-at nem töröljük, mert azt majd update-eljük
            for (int i = 1; i < result_Search.Ds.Tables[0].Rows.Count; i++)
            {
                execParamArray[i - 1] = Contentum.eUtility.UI.SetExecParamDefault(Page);
                execParamArray[i - 1].Record_Id = result_Search.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            Result result_invalidate = service.MultiInvalidate(execParamArray);
            if (result_invalidate.IsError)
            {
                // hiba
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.MultiInvalidate ", execParam, result_invalidate);
                return;
            }
            #endregion
        }
        else if (result_Search.Ds.Tables[0].Rows.Count == 1)
        {
            templateId = result_Search.Ds.Tables[0].Rows[0]["Id"].ToString();
            templateVer = result_Search.Ds.Tables[0].Rows[0]["Ver"].ToString();
        }

        KRT_UIMezoObjektumErtekek krt_UIMezoObjektumErtekek = CreateBusinessObject();

        if (!String.IsNullOrEmpty(templateId))
        {
            #region Update

            // elmentett verziót be kell állítani az Update-hez:
            krt_UIMezoObjektumErtekek.Base.Ver = templateVer;
            krt_UIMezoObjektumErtekek.Base.Updated.Ver = true;

            ExecParam execParam_update = Contentum.eUtility.UI.SetExecParamDefault(Page);
            execParam_update.Record_Id = templateId;

            Result result_update = service.Update(execParam_update, krt_UIMezoObjektumErtekek);
            if (result_update.IsError)
            {
                // hiba:
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.Update", execParam_update, result_update);
                return;
            }
            #endregion
        }
        else
        {
            #region Insert

            ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);

            Result result_Insert = service.Insert(execParam_Insert, krt_UIMezoObjektumErtekek);
            if (result_Insert.IsError)
            {
                // hiba:
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.Insert", execParam_Insert, result_Insert);
                return;
            }

            templateId = result_Insert.Uid;

            #endregion
        }

        #region Sessionben lévő Dictionary-ben update
        if (Page.Session[Constants.SessionNames.DefaultOrderByDictionary] != null)
        {
            Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy> columnsOrderByDict =
                (Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy>)Page.Session[Constants.SessionNames.DefaultOrderByDictionary];

            Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy columnsOrderByObj = new Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy(templateId, krt_UIMezoObjektumErtekek.TemplateXML);

            columnsOrderByDict[templateTipusNev] = columnsOrderByObj;
        }
        #endregion

        // Mintha a frissítés gombot is megnyomtuk volna:
        btnSort_Click(sender, e);

        // esemény kiváltása:
        if (DefaultOrderBySaved != null)
        {
            DefaultOrderBySaved(sender, e);
        }
    }

    private string GetOrderByTemplateTipusNev(Type searchObjectType, string customSearchObjectSessionName)
    {
        if (!string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return Contentum.eUtility.UIMezoObjektumErtekek.OrderByTag + customSearchObjectSessionName;
        }
        else
        {
            return Contentum.eUtility.UIMezoObjektumErtekek.OrderByTag + searchObjectType.Name;
        }
    }

    private KRT_UIMezoObjektumErtekek CreateBusinessObject()
    {
        KRT_UIMezoObjektumErtekek krt_UIMezoObjektumErtekek = new KRT_UIMezoObjektumErtekek();
        krt_UIMezoObjektumErtekek.Updated.SetValueAll(false);
        krt_UIMezoObjektumErtekek.Base.Updated.SetValueAll(false);

        krt_UIMezoObjektumErtekek.Felhasznalo_Id = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        krt_UIMezoObjektumErtekek.Updated.Felhasznalo_Id = true;

        krt_UIMezoObjektumErtekek.TemplateTipusNev = GetOrderByTemplateTipusNev(searchObjectType, customSearchObjectSessionName);
        krt_UIMezoObjektumErtekek.Updated.TemplateTipusNev = true;

        // Névnek beállítjuk a templateTipusNev-et, úgyis csak egy lesz egy fajtából
        krt_UIMezoObjektumErtekek.Nev = krt_UIMezoObjektumErtekek.TemplateTipusNev;
        krt_UIMezoObjektumErtekek.Updated.Nev = true;

        if (AttachedGridView != null)
            krt_UIMezoObjektumErtekek.TemplateXML = GetXmlStringFromComponents();
        else if (AttachedTreeGridView != null)
            krt_UIMezoObjektumErtekek.TemplateXML = GetXmlStringFromComponents();
        krt_UIMezoObjektumErtekek.Updated.TemplateXML = true;

        // Alapértelmezettnek kell beállítani:
        krt_UIMezoObjektumErtekek.Alapertelmezett = Contentum.eUtility.Constants.Database.Yes;
        krt_UIMezoObjektumErtekek.Updated.Alapertelmezett = true;

        krt_UIMezoObjektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
        krt_UIMezoObjektumErtekek.Updated.UtolsoHasznIdo = true;

        return krt_UIMezoObjektumErtekek;
    }

    private string GetXmlStringFromComponents()
    {
        StringBuilder sb_xml = new StringBuilder(@"<OrderBy><Columns>");

        // BUG_8359
        for (int i = 0; i < lstSortExpressionDropDowns.Count ; i++)
        {
            Contentum.eUIControls.eDropDownList dropdownSortExpression = lstSortExpressionDropDowns[i];
            Contentum.eUIControls.eDropDownList dropdownSortDirection = lstSortDirectionDropDowns[i];
            if (dropdownSortExpression != null
                && dropdownSortExpression.Enabled == true
                && !String.IsNullOrEmpty(dropdownSortExpression.SelectedValue))
            {

                string name = String.Empty;
                string sortExpression = String.Empty;
                string sortDirection = String.Empty;

                try
                {

                    name = dropdownSortExpression.SelectedItem.Text;
                    sortExpression = dropdownSortExpression.SelectedValue;
                    sortDirection = dropdownSortDirection.SelectedValue;

                }
                catch (Exception e)
                {
                    Logger.Error("Hiba: ListSortPopup.GetXmlStringFromComponents", e);
                }

                sb_xml.Append(String.Format("<Column index=\"{0}\" name=\"{1}\" sortExpression=\"{2}\" sortDirection=\"{3}\"/>",
                    i, name, sortExpression, sortDirection));
            }
        }
        sb_xml.Append("</Columns></OrderBy>");

        return sb_xml.ToString();
        
    }

    // Default rendezettség beállítása
    // Csak ha már meg van adva a SearchObjectType
    public bool SetDefaultOrderByIfExist()
    {
        bool existsDefault = false;
        // BUG_8359
        //GridView gridView = _attachedGridView;
        //if (gridView == null)
        //{
        //    if (_attachedTreeGridView != null)
        //    {
        //        gridView = new GridView();
        //        gridView.ID = _attachedTreeGridView.ID;
        //    }
        //}
        //if (gridView != null && searchObjectType != null)
        //{
        if (_attachedGridView != null && searchObjectType != null) 
            existsDefault = SetDefaultOrderByIfExist(Page, _attachedGridView, searchObjectType, customSearchObjectSessionName);
        if (_attachedTreeGridView != null && searchObjectType != null)
            existsDefault = SetDefaultOrderByIfExist(Page, _attachedTreeGridView, searchObjectType, customSearchObjectSessionName);

        //}
        return existsDefault;
    }


   

    /// <summary>
    /// Ha van elmentett rendezettség az adott listára, beállítja a rendezettséget
    /// A visszaadott érték ilyenkor true, egyéb esetben false
    /// </summary>
    /// <param name="page"></param>
    /// <param name="attachedGridView"></param>
    /// <param name="searchObjectType"></param>
    /// <param name="customSearchObjectSessionName"></param>
    /// <returns></returns>
    public static bool SetDefaultOrderByIfExist(Page page, Object attachedGridView, Type searchObjectType, string customSearchObjectSessionName)
    {
        if (attachedGridView == null || page == null || (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName)))
        {
            return false;
        }
        GridView _attachedGridView = null ;
        TreeGridView _attachedTreeGridView = null ;
        if (attachedGridView.GetType()== typeof(GridView)) _attachedGridView = (GridView)attachedGridView;
        if (attachedGridView.GetType() == typeof(TreeGridView)) _attachedTreeGridView = (TreeGridView)attachedGridView;

        string templateTipusNev;
        if (!string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            templateTipusNev = Contentum.eUtility.UIMezoObjektumErtekek.OrderByTag + customSearchObjectSessionName;
        }
        else
        {
            templateTipusNev = Contentum.eUtility.UIMezoObjektumErtekek.OrderByTag + searchObjectType.Name;
        }

        Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy> columnsOrderByDict;

        if (page.Session[Constants.SessionNames.DefaultOrderByDictionary] == null)
        {
            #region Elmentett rendezettség adatok betöltése

            KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

            search.Felhasznalo_Id.Value = FelhasznaloProfil.FelhasznaloId(page);
            search.Felhasznalo_Id.Operator = Query.Operators.equals;

            search.Alapertelmezett.Value = Constants.Database.Yes;
            search.Alapertelmezett.Operator = Query.Operators.equals;

            ExecParam execParam = UI.SetExecParamDefault(page);

            Result result = service.GetAll(execParam, search);
            if (result.IsError)
            {
                // hiba:
                Logger.Error("Hiba a default KRT_UIMezoObjektumErtekek értékek lekérésekor (OrderBy értékekhez)", execParam, result);
                return false;
            }

            columnsOrderByDict = Contentum.eUtility.UIMezoObjektumErtekek.CreateOrderByDictionary(result.Ds);

            #endregion
        }
        else
        {
            columnsOrderByDict = (Dictionary<string, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy>)page.Session[Constants.SessionNames.DefaultOrderByDictionary];
        }

        if (columnsOrderByDict != null)
        {
            if (columnsOrderByDict.ContainsKey(templateTipusNev))
            {
                if (_attachedGridView != null) SetGridViewOrderBy(_attachedGridView, columnsOrderByDict[templateTipusNev]);
                
                if (_attachedTreeGridView != null) SetGridViewOrderBy(_attachedTreeGridView, columnsOrderByDict[templateTipusNev]);
                

                //SetGridViewOrderBy(attachedGridView, columnsOrderByDict[templateTipusNev]);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    

    private static void SetGridViewOrderBy(Object attachedGridView, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy gridViewOrderBy)
    {
        if (attachedGridView == null || gridViewOrderBy == null)
        {
            return;
        }
        GridView _attachedGridView = null;
        TreeGridView _attachedTreeGridView = null;
        if (attachedGridView.GetType() == typeof(GridView)) _attachedGridView = (GridView)attachedGridView;
        if (attachedGridView.GetType() == typeof(TreeGridView)) _attachedTreeGridView = (TreeGridView)attachedGridView;


        string sortExpr = Contentum.eUtility.Search.FullSortExpression;
        bool empty = true;
        foreach (KeyValuePair<int, Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy.ColumnProperties> kvp in gridViewOrderBy.Values)
        {
            int index = kvp.Key;
            Contentum.eUtility.UIMezoObjektumErtekek.GridViewOrderBy.ColumnProperties columnProperties = kvp.Value;

            string columnName = columnProperties.columnName;
            string sortExpression = columnProperties.sortExpression;
            string sortDirection = columnProperties.sortDirection;

            if (!empty) sortExpr += ", ";
            sortExpr += String.Format(sortExpression, sortDirection) + " " + sortDirection;
            empty = false;
        }
        if (_attachedGridView != null)
        {
            // A Sort() fv.-ben a rendezés iránya itt mindegy, a SortExpression-be rakjuk bele azt is (mert lehet több oszlop, több irány is)
            _attachedGridView.Sort(sortExpr, SortDirection.Ascending);
        }
        if (_attachedTreeGridView != null)
        {
            _attachedTreeGridView.DefaultSortExpression = sortExpr;
            _attachedTreeGridView.DefaultSortDirection = SortDirection.Ascending;
            
        }
    }

 

}
