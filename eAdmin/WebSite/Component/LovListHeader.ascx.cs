using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;

public partial class Component_LovListHeader : System.Web.UI.UserControl
{
    public string HeaderTitle
    {
        set 
        { 
            HeaderLabel.Text = value;
            Page.Title = value;
        }
        get { return HeaderLabel.Text; }

    }

    public string FilterTitle
    {
        set { FilterLabel.Text = value; }
        get { return FilterLabel.Text; }
    }

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return EErrorPanel1; }
        set { EErrorPanel1 = value; }
    }

    public UpdatePanel ErrorUpdatePanel
    {
        get { return errorUpdatePanel; }
    }

    public ASP.component_customupdateprogress_ascx UpdateProgress
    {
        get
        {
            return CustomUpdateProgress1;
        }
    }

    #region FormTemplateLoader

    public bool FormTemplateLoader1_Visibility
    {
        get { return FormTemplateLoader1.Visible; }
        set { FormTemplateLoader1.Visible = value; }
    }

    public object TemplateObject
    {
        get { return FormTemplateLoader1.SearchObject; }
        set { FormTemplateLoader1.SearchObject = value; }
    }

    public Type TemplateObjectType
    {
        get { return FormTemplateLoader1.SearchObjectType; }
        set { FormTemplateLoader1.SearchObjectType = value; }
    }

    public string CurrentTemplateName
    {
        get { return FormTemplateLoader1.CurrentTemplateName; }
        set { FormTemplateLoader1.CurrentTemplateName = value; }
    }

    public string CurrentTemplateId
    {
        get { return FormTemplateLoader1.CurrentTemplateId; }
        set { FormTemplateLoader1.CurrentTemplateId = value; }
    }

    public string CustomTemplateTipusNev
    {
        get { return FormTemplateLoader1.CustomTemplateTipusNev; }
        set { FormTemplateLoader1.CustomTemplateTipusNev = value; }
    }

    /// <summary>
    /// �j template l�trehoz�sa
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void NewTemplate(object newSearchObject)
    {
        FormTemplateLoader1.NewTemplate(newSearchObject);
    }

    /// <summary>
    /// Aktu�lis template elment�se
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void SaveCurrentTemplate(object newSearchObject)
    {
        FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    }

    public void TemplateReset()
    {
        FormTemplateLoader1.TemplateReset();
    }

    public event CommandEventHandler ButtonsClick;

    protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (ButtonsClick != null)
        {
            ButtonsClick(this, e);
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
        FormTemplateLoader1.ErrorPanel = ErrorPanel;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {     
        

    }
}
