using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_MenuControl : System.Web.UI.UserControl
{
    /// <summary>
    /// Men�oszlopok magass�ga
    /// </summary>
    private const int menuHeight = 300;

    /// <summary>
    /// F�men�k sz�less�ge
    /// </summary>
    private const int mainMenuWidth = 118;
    private const int menuWidth = 150;

    public static Component_MenuControl AddMenuControl(Panel panel)
    {
        Page page = panel.Page;
        Component_MenuControl menuControl =
            (Component_MenuControl) page.LoadControl("Component/MenuControl.ascx");
        panel.Controls.Add(menuControl);

        return menuControl;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // ha session-ben benne van a menu, kiolvassuk onnan
        // ha nincs, adatb�zisb�l lek�rj�k, �s letessz�k sessionbe
        if (Page.Session[Constants.SessionNames.UserMenuList] != null)
        {
            mainMenus = (List<Contentum.eUtility.UI.Menu.MainMenuStruct>)Page.Session[Constants.SessionNames.UserMenuList];
        }
        else
        {
            buildMenuLists();
            Page.Session[Constants.SessionNames.UserMenuList] = mainMenus;
        }

        registerJavascriptCodes();
        createMainMenuTable(true);
    }



    /// <summary>
    /// F�men�k list�ja: Dictionaryk list�ja, ahol a Dictionaryk 
    /// f�men�_n�v - image_url p�rokban vannak
    /// </summary>
    private List<Contentum.eUtility.UI.Menu.MainMenuStruct> mainMenus;




    /// <summary>
    /// List<MainMenuStruct> mainMenus �ssze�ll�t�sa
    /// </summary>
    private void buildMenuLists()
    {
        mainMenus = new List<Contentum.eUtility.UI.Menu.MainMenuStruct>();
        
        // fomenu_id -> MainMenuStruct p�rok
        Dictionary<string,Contentum.eUtility.UI.Menu.MainMenuStruct> dict_MainMenuStruct = new Dictionary<string,Contentum.eUtility.UI.Menu.MainMenuStruct>();
        /// menu_id - SubMenuStruct p�rok
        Dictionary<string, Contentum.eUtility.UI.Menu.SubMenuStruct> dict_SubMenuStruct = new Dictionary<string, Contentum.eUtility.UI.Menu.SubMenuStruct>();
        // menu_id -> szuloMenu_id p�rok
        Dictionary<string,string> dict_Szulok = new Dictionary<string,string>();

        KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
        bool isMegbizas = false;

        if (FunctionRights.FelhasznaloProfilIsCached(Page))
        {
            Contentum.eUtility.FelhasznaloProfil _FelhasznaloProfil =
            (Contentum.eUtility.FelhasznaloProfil)Page.Session[Constants.FelhasznaloProfil];

            ////if (_FelhasznaloProfil != null && _FelhasznaloProfil.Helyettesitett != null
            ////    && _FelhasznaloProfil.Helyettesitett.Id != null)
            ////{
            ////    felhasznalo.Id = _FelhasznaloProfil.Helyettesitett.Id;
            ////}
            ////else
            ////{
            ////    felhasznalo.Id = FelhasznaloProfil.FelhasznaloId(Page);
            ////}

            //if (_FelhasznaloProfil != null)
            //{
            //    felhasznalo.Id = FelhasznaloProfil.FelhasznaloId(Page);
            //}


            if (_FelhasznaloProfil != null)
            {
                if (_FelhasznaloProfil.Helyettesites.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                {
                    isMegbizas = true;
                    if (!String.IsNullOrEmpty(_FelhasznaloProfil.Helyettesites.Id))
                    {
                        execParam.Record_Id = _FelhasznaloProfil.Helyettesites.Id;
                    }
                    else
                    {
                        // Elvileg m�r a Login sor�n kisz�rj�k, itt csak a biztons�g kedv��rt:
                        // Men� l�trehoz�si hiba: A megb�z�s nem azonos�that�!
                        string errorMessage = ResultError.GetErrorMessageByErrorCode(61002);
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorMessage);
                        return;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(_FelhasznaloProfil.FelhasznaloCsoportTagsag.Id))
                    {
                        // csoporttags�g
                        execParam.Record_Id = _FelhasznaloProfil.FelhasznaloCsoportTagsag.Id;
                    }
                    else
                    {
                         //Elvileg m�r a Login sor�n kisz�rj�k, itt csak a biztons�g kedv��rt:
                         //Men� l�trehoz�si hiba: A felhaszn�l� szervezete nem azonos�that�!
                        string errorMessage = ResultError.GetErrorMessageByErrorCode(61001);
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorMessage);
                        return;
                    }
                }
            }

        }
                

        string alkalmazas_kod = UI.GetAppSetting("ApplicationName");
        //Result result = service.GetAllByFelhasznalo(execParam, felhasznalo, alkalmazas_kod);
        Result result;
        if (isMegbizas)
        {
            result = service.GetAllByMegbizas(execParam, alkalmazas_kod);
        }
        else
        {
            result = service.GetAllByCsoporttagSajatJogu(execParam, alkalmazas_kod);
        }

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        }
        else
        {
            // fel�p�tj�k a dictionary-ket
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                String menuId = row["KRT_Menuk_Id"].ToString();
                String menuSzuloId = row["KRT_Menuk_Menu_Id_Szulo"].ToString();

                String menuNev = row["KRT_Menuk_Nev"].ToString();         

                if (String.IsNullOrEmpty(menuSzuloId))
                {
                    // f�men�
                    String imageURL = row["KRT_Menuk_ImageURL"].ToString();

                    Contentum.eUtility.UI.Menu.MainMenuStruct newMainMenuStruct = new Contentum.eUtility.UI.Menu.MainMenuStruct();                    
                    newMainMenuStruct.id = menuId;
                    newMainMenuStruct.name = menuNev;
                    newMainMenuStruct.imageUrl = imageURL;

                    dict_MainMenuStruct.Add(menuId,newMainMenuStruct);
                    mainMenus.Add(newMainMenuStruct);
                }
                else if (!String.IsNullOrEmpty(menuId) && UtilityMenu.IsMenuVisible(Page, menuId))
                {
                    // almen�
                    String pageName = row["KRT_Modulok_Kod"].ToString();
                    String parameter = row["KRT_Menuk_Parameter"].ToString();
                    String url = "";
                    if (String.IsNullOrEmpty(parameter))
                    {
                        url = pageName;
                    }
                    else
                    {
                        url = pageName + "?" + parameter;
                    }

                    Contentum.eUtility.UI.Menu.SubMenuStruct newSubMenuStruct = new Contentum.eUtility.UI.Menu.SubMenuStruct();
                    newSubMenuStruct.id = menuId;
                    newSubMenuStruct.name = menuNev;
                    newSubMenuStruct.url = url;

                    dict_SubMenuStruct.Add(menuId, newSubMenuStruct);
                    dict_Szulok.Add(menuId, menuSzuloId);
                }
            }

            // v�gigmegy�nk a menuId - szuloMenuId p�rokon, �s fel�p�tj�k a hierarchi�t
            foreach (KeyValuePair<string, string> kvp in dict_Szulok)
            {
                string menuId = kvp.Key;
                string szuloMenuId = kvp.Value;
                Contentum.eUtility.UI.Menu.SubMenuStruct subMenu = null;

                if (dict_SubMenuStruct.ContainsKey(menuId))
                {
                    subMenu = dict_SubMenuStruct[menuId];

                    if (dict_MainMenuStruct.ContainsKey(szuloMenuId))
                    {
                        Contentum.eUtility.UI.Menu.MainMenuStruct mainMenu = dict_MainMenuStruct[szuloMenuId];
                        mainMenu.AddSubMenu(subMenu);
                    }
                    else if (dict_SubMenuStruct.ContainsKey(szuloMenuId))
                    {
                        Contentum.eUtility.UI.Menu.SubMenuStruct szuloSubMenu = dict_SubMenuStruct[szuloMenuId];
                        szuloSubMenu.AddSubMenu(subMenu);
                    }
                    else
                    {
                        // hiba
                    }
                }
                else
                {
                    //hiba
                }
            }
        }       
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="enableJavascript">ha false: javascript nem ad�dik hozz�,
    ///    leny�l� men�k nem lesznek</param>
    private void createMainMenuTable(bool enableJavascript)
    {
        HtmlTableRow headerRow = MainMenuTable.Rows[0];
        HtmlTableRow mainMenus_row = MainMenuTable.Rows[1];
        HtmlTableRow subMenus_row = MainMenuTable.Rows[2];

        // headerRow
        for (int i = 0; i < mainMenus.Count; i++)
        {
            HtmlTableCell newCell = new HtmlTableCell();
            headerRow.Cells.Add(newCell);
            newCell.Attributes["style"] = "width: " + mainMenuWidth.ToString()
                + "px; height: 0px;";
            newCell.InnerHtml =
                "<img src='images/hu/menuelemek/menu_fill.jpg' alt='' />";
            // nekrisz
            newCell.Attributes["id"] = mainMenus[i].id.Replace("-", "");
        }

        // mainMenus_row
        mainMenus_row.Attributes["align"] = "center";
        mainMenus_row.Attributes["style"] = "height:120px;";

        int mainMenuIndex = 0;
        foreach (Contentum.eUtility.UI.Menu.MainMenuStruct aMainMenu in mainMenus)
        {
            mainMenuIndex++;

            HtmlTableCell newCell = new HtmlTableCell();
            mainMenus_row.Cells.Add(newCell);
            // nekrisz
            //string m1mnX = "m1mn" + mainMenuIndex.ToString();
            //string m1tlmX = "m1tlm" + mainMenuIndex.ToString();
            string m1mnX = "m1mn" + aMainMenu.id.Replace("-", "");
            string m1tlmX = "m1tlm" + aMainMenu.id.Replace("-", "");
            newCell.Attributes["id"] = m1tlmX;

            if (enableJavascript)
            {
                newCell.Attributes["onmouseover"] =
                    "exM(m1,'" + m1mnX + "',this.id,event)";
                newCell.Attributes["onmouseout"] =
                    "chgBg(m1,this.id,0);coM(m1,'" + m1mnX + "')";
            }

            newCell.Attributes["style"] =
                "background-color:#edf4fa; " +
                " background-image: url('images/hu/menuelemek/menu_hatter.jpg'); " +
                " width: " + mainMenuWidth.ToString() + "px; height: 98px; vertical-align: top;" +
                " background-repeat:no-repeat; ";

            if (!String.IsNullOrEmpty(aMainMenu.imageUrl))
            {
                newCell.InnerHtml =
                    @"<a href='javascript:void(0);'>
                    <img src='" + aMainMenu.imageUrl
                          + @"' alt='' style='padding-top: 20px;'/>
                    <br/><font color='#476d91' size='2'>
                    <b>" + aMainMenu.name
                        + @"</b>
                  </font></a>";
            }
            else
            {
                newCell.InnerHtml = "";
            }
        }

        // subMenus_row
        for (int i = 0; i < mainMenus.Count; i++)
        {
            HtmlTableCell newCell = new HtmlTableCell();
            subMenus_row.Cells.Add(newCell);
            newCell.Attributes["style"] =
                @"BACKGROUND-IMAGE: url(images/hu/menuelemek/nyitooldalgradiens.jpg); 
                 HEIGHT: " + menuHeight.ToString() + "px;width: " + mainMenuWidth.ToString()
                 + "px; text-align: left;vertical-align: top;";
            // nekrisz
            newCell.Attributes["id"] = mainMenus[i].id.Replace("-", "");
        }

    }



    /// <summary>
    /// Javascript a men�megjelen�t�shez
    /// </summary>
    private void registerJavascriptCodes()
    {
        // menu_dom.js regisztr�l�sa
        string script_menu_dom =
            @"<script language='JavaScript1.2' 
                      src='JavaScripts/menu_dom.js'></script>";
        if (!Page.ClientScript.IsClientScriptBlockRegistered("menu_dom"))
        {
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(),
                "menu_dom", script_menu_dom);
        }

        // Menu.js mint�j�ra a script �ssze�ll�t�sa:
        string script_menu =
            @"<script language=javascript>
            // JScript File
            //script generated by SiteXpert (www.xtreeme.com)
            //Copyright(C) 1998-2003 Xtreeme GmbH
            var ver='3.0.2D'
            var m1=new Object
            m1.name='m1'
            m1.fnm='menu_m1'
            if(!window.lastm||window.lastm<1)lastm=1
            m1.v17=null
            m1.v17Timeout=''
            var maxZ=1000
            m1.v18
            m1.targetFrame
            var docLoaded=false
            m1.bIncBorder=true
            m1.v29=null
            m1.v29Str=''
            m1.v55=50
            m1.scrollStep=10
            m1.fadingSteps=8
            m1.itemOverDelay=0
            m1.transTLO=0
            m1.fixSB=0
            m1.v21='.'
            m1.maxlev=2
            m1.v22=0
            m1.sepH=10
            m1.bHlNL=1
            m1.showA=1
            m1.bVarWidth=0
            m1.bShowDel=0
            m1.scrDel=0
            m1.v23=150
            m1.levelOffset=20
            m1.bord=1
            m1.vertSpace=5
            m1.sep=1
            m1.v19=false
            m1.bkv=0
            m1.rev=0
            m1.shs=0
            m1.xOff=0
            m1.yOff=0
            m1.v20=false
            m1.cntFrame=''
            m1.menuFrame=''
            m1.v24=''
            m1.mout=true
            m1.iconSize=8
            m1.closeDelay=1000
            m1.tlmOrigBg='#B2CFEB'
            m1.tlmOrigCol='Black'
            m1.v25=false
            m1.v52=false
            m1.v60=75
            m1.v11=false
            m1.v10=0
            m1.ppLeftPad=5
            m1.v54=0
            m1.v01=2
            m1.tlmHlBg='#003366'
            m1.tlmHlCol='White'
            m1.borderCol='white'
            m1.menuHorizontal=true
            m1.scrollHeight=6
            m1.attr=new Array('11px',false,false,'Black','#B2CFEB','White','Arial,Helvetica','#003366','Black','#B2CFEB')
            ";

        int mainMenuIndex = 0;
        foreach (Contentum.eUtility.UI.Menu.MainMenuStruct aMainMenu in mainMenus)
        {
            mainMenuIndex++;
            if (aMainMenu.subMenus != null)
            {
                // nekrisz
                //script_menu =
                //    appendSubMenuCode(aMainMenu.subMenus
                //        , script_menu, "m1mn", mainMenuIndex);
                script_menu =
                    appendSubMenuCode(aMainMenu.subMenus
                        , script_menu, "m1mn", aMainMenu.id.Replace("-",""));
            }
        }

        script_menu +=
            @"
            absPath=''
            if(m1.v19&&!m1.v20){
            if(window.location.href.lastIndexOf('\\')>window.location.href.lastIndexOf('/')) {sepCh = '\\' ;} else {sepCh = '/' ;}
            absPath=window.location.href.substring(0,window.location.href.lastIndexOf(sepCh)+1)}
            m1.v61=0
            m1.v02=m1.v23
            </script>";


        if (!Page.ClientScript.IsClientScriptBlockRegistered("menu_js"))
        {
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(),
                "menu_js", script_menu);
        }

        if (!Page.ClientScript.IsClientScriptBlockRegistered("menu_js_css"))
        {
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(),
                "menu_js_css"
                , "<style type='text/css'>\n.m1CL0,.m1CL0:link{text-decoration:none;width:100%;color:Black; }\n.m1CL0:visited{color:Black}\n.m1CL0:hover{text-decoration:underline}\n.m1mit{padding-left:15px;padding-right:15px;color:Black; font-family:Arial,Helvetica; font-size:11px; }\n"
                + "</style>");
        }


    }

    /// <summary>
    /// Javascript k�d hozz�ad�s az adott almen�lista megjelen�t�s�hez
    /// </summary>
    /// <param name="subMenuList">Men�pontok list�ja</param>
    /// <param name="jsCode">Javascript k�d, amihez hozz� kell f�zni az itt gener�lt k�dot</param>
    /// <param name="parent_m1mnX"></param>
    /// <param name="listIndex"></param>
    /// <returns>Az �sszef�z�tt Javascript k�d</returns>
    private string appendSubMenuCode(List<Contentum.eUtility.UI.Menu.SubMenuStruct> subMenuList, string jsCode
                    , string parent_m1mnX, string listId)
        //int listIndex)
    {
        string m1mnX = parent_m1mnX + listId.ToString();
        //string m1mnX = parent_m1mnX + listIndex;

        jsCode +=
            @"
                document." + m1mnX + @"prop=';w:" + menuWidth.ToString() + @"' 
             ";

        jsCode += m1mnX + @"=new Array(
             ";

        int index = 0;
        foreach (Contentum.eUtility.UI.Menu.SubMenuStruct aSubMenu in subMenuList)
        {
            index++;

            string url = String.IsNullOrEmpty(aSubMenu.url)
                ? "#" : aSubMenu.url;

            bool hasSubmenus = false;
            if (aSubMenu.subMenus != null && aSubMenu.subMenus.Count > 0)
            {
                hasSubmenus = true;
            }

            if (index > 1) { jsCode += ","; }

            jsCode += "'" + aSubMenu.name + "','" + url + "',";
            if (hasSubmenus == false)
            {
                jsCode += "0,";
            }
            else
            {
                jsCode += "1,";
            }
            // nekrisz
            jsCode += @"'" + aSubMenu.id.Replace("-", "") + "',''";

        }

        jsCode += ") ";

        // v�gign�zz�k �jra, hogy volt-e valamelyiknek almen�je:
        index = 0;
        foreach (Contentum.eUtility.UI.Menu.SubMenuStruct aSubMenu in subMenuList)
        {
            index++;
            if (aSubMenu.subMenus != null && aSubMenu.subMenus.Count > 0)
            {
                // nekrisz
                //jsCode = appendSubMenuCode(aSubMenu.subMenus, jsCode, m1mnX + "_", index);
                jsCode = appendSubMenuCode(aSubMenu.subMenus, jsCode, m1mnX + "_", aSubMenu.id.Replace("-", ""));
            }
        }

        return jsCode;
    }


#region AdminMenu

    //private void addAdminMenuPanel()
    //{
    //    EFormPanel1.Enabled = true;
    //    EFormPanel1.Visible = true;
    //    // adminPanel-sz�less�g �ll�t�s:
    //    int panelWidth = mainMenus.Count * menuWidth;
    //    if (panelWidth < 650) { panelWidth = 650; }
    //    EFormPanel1.Width = panelWidth;
    //    //AlwaysVisibleControlExtender1.Enabled = true;
    //}


    ///// <summary>
    ///// Men�pontok megjelen�t�se admin m�dban: �sszes men�pont kibontva egym�s alatt, checkbox-szal ell�tva
    ///// </summary>
    ///// <param name="createCheckBoxes">ha true, l�trehozza a checkboxokat
    ///// checked �llapottal; ha false, csak megjelen�ti azokat
    ///// </param>
    //private void addMenusInAdminMode(bool createCheckBoxes)
    //{
    //    HtmlTableRow subMenus_row = MainMenuTable.Rows[2];
    //    int cellIndex = 0;
    //    foreach (MainMenuStruct aMainMenu in mainMenus)
    //    {
    //        HtmlTableCell cell = subMenus_row.Cells[cellIndex];
    //        HtmlTable newTable = new HtmlTable();
    //        newTable.Attributes["style"] =
    //            "padding: 0px;	border: 0px;";
    //        cell.Controls.Add(newTable);

    //        // men�pontok ki�r�sa:
    //        if (aMainMenu.subMenus != null
    //            && aMainMenu.subMenus.Count > 0)
    //        {
    //            addSubMenuRowsWithCheckBox(
    //                aMainMenu.subMenus, newTable, 0, createCheckBoxes, null);
    //        }

    //        cellIndex++;
    //    }
    //}

    ///// <summary>
    ///// A megadott men�pontok megjelen�t�se a megadott html table-ben
    ///// </summary>
    ///// <param name="subMenuList"></param>
    ///// <param name="containerTable"></param>
    ///// <param name="indent_px">Beh�z�s pixelben</param>
    ///// <param name="createCheckBoxes">ha true, l�trehozza a checkboxokat
    ///// checked �llapottal; ha false, csak megjelen�ti azokat</param>
    //private void addSubMenuRowsWithCheckBox(
    //    List<SubMenuStruct> subMenuList,
    //    HtmlTable containerTable,
    //    int indent_px,
    //    bool createCheckBoxes,
    //    CheckBox parentCheckBox)
    //{
    //    foreach (SubMenuStruct aSubMenu in subMenuList)
    //    {
    //        HtmlTableRow newRow = new HtmlTableRow();
    //        containerTable.Controls.Add(newRow);
    //        HtmlTableCell newCell = new HtmlTableCell();
    //        newRow.Controls.Add(newCell);

    //        CheckBox checkBox = null;
    //        if (createCheckBoxes == true)
    //        {
    //            checkBox = new CheckBox();
    //            checkBox.ID = "checkBox_" + newCell.ClientID + "_" + aSubMenu.name;
    //            checkBox.Checked = true;
    //            aSubMenu.checkBox = checkBox;
    //        }
    //        else
    //        {
    //            checkBox = aSubMenu.checkBox;
    //        }


    //        Label label_subMenu = new Label();
    //        label_subMenu.Text = aSubMenu.name;

    //        if (aSubMenu.checkBox != null)
    //        {
    //            newCell.Controls.Add(aSubMenu.checkBox);
    //            checkBox.Attributes["style"] =
    //                "padding-left: " + indent_px.ToString() + "px;";

    //            //if (parentCheckBox != null)
    //            //{
    //            //    // javascript fv. h�v�s onClick esem�nyre:
    //            //    // (ha checked-be ker�l, sz�l�t is checked-re �ll�tja)
    //            //    checkBox.Attributes["onchange"] = "check('" + checkBox.ClientID +
    //            //        "',['" + parentCheckBox.ClientID + "']);";
    //            //}

    //            label_subMenu.AssociatedControlID = checkBox.ID;
    //        }


    //        HyperLink link = new HyperLink();
    //        link.Text = aSubMenu.name;

    //        // relat�v sourceDirectory �t�ll�t�sa:

    //        string newRelativeSrcDir =
    //            link.AppRelativeTemplateSourceDirectory;
    //        string subDir = "/Component";
    //        // subDir lev�g�sa a v�g�r�l:
    //        if (newRelativeSrcDir.EndsWith(subDir))
    //        {
    //            newRelativeSrcDir =
    //                newRelativeSrcDir.Remove(newRelativeSrcDir.Length - subDir.Length - 1);
    //        }
    //        // �j relat�v sourceDirectory:
    //        link.AppRelativeTemplateSourceDirectory = newRelativeSrcDir;

    //        //

    //        link.NavigateUrl = aSubMenu.url;

    //        newCell.Controls.Add(link);

    //        if (aSubMenu.subMenus != null
    //            && aSubMenu.subMenus.Count > 0)
    //        {
    //            addSubMenuRowsWithCheckBox(
    //                aSubMenu.subMenus, containerTable,
    //                indent_px + 12, createCheckBoxes, aSubMenu.checkBox);
    //        }

    //    }
    //}

    ///// <summary>
    ///// onClick = check(...), vagy onClick = uncheck(...) hozz�ad�sa a CheckBoxokhoz
    ///// </summary>
    //private void addOnClickToCheckBoxes()
    //{
    //    /// checkboxokhoz tartoz� alcheckboxok
    //    Dictionary<CheckBox, List<CheckBox>> checkBoxDescendants = new Dictionary<CheckBox, List<CheckBox>>();
    //    /// checkboxokhoz tartoz� felettes checkboxok
    //    Dictionary<CheckBox, List<CheckBox>> checkBoxParents = new Dictionary<CheckBox, List<CheckBox>>();

    //    foreach (MainMenuStruct aMainMenu in mainMenus)
    //    {
    //        if (aMainMenu.subMenus != null && aMainMenu.subMenus.Count > 0)
    //        {
    //            processSubMenu_checkBoxStructure(null, aMainMenu.subMenus,
    //                checkBoxDescendants, checkBoxParents);
    //        }
    //    }

    //    // checkBoxParents feldolgoz�sa:
    //    foreach (KeyValuePair<CheckBox, List<CheckBox>> kvPair_parents
    //                in checkBoxParents)
    //    {
    //        CheckBox checkBox = kvPair_parents.Key;
    //        List<CheckBox> parentsList = kvPair_parents.Value;
    //        if (parentsList.Count > 0)
    //        {
    //            string parentCheckBoxIds = "'";
    //            bool isFirstItem = true;
    //            foreach (CheckBox parentCheckBox in parentsList)
    //            {
    //                if (!isFirstItem) { parentCheckBoxIds += "','"; }
    //                parentCheckBoxIds += parentCheckBox.ClientID;
    //                isFirstItem = false;
    //            }
    //            parentCheckBoxIds += "'";
    //            checkBox.Attributes["onClick"] += "check(this.id,[" +
    //                parentCheckBoxIds + "]);";
    //        }
    //    }

    //    // checkBoxDescendants feldolgoz�sa:
    //    foreach (KeyValuePair<CheckBox, List<CheckBox>> kvPair_descendants
    //                in checkBoxDescendants)
    //    {
    //        CheckBox checkBox = kvPair_descendants.Key;
    //        List<CheckBox> descendantsList = kvPair_descendants.Value;
    //        if (descendantsList.Count > 0)
    //        {
    //            string descCheckBoxIds = "'";
    //            bool isFirstItem = true;
    //            foreach (CheckBox descCheckBox in descendantsList)
    //            {
    //                if (!isFirstItem) { descCheckBoxIds += "','"; }
    //                descCheckBoxIds += descCheckBox.ClientID;
    //                isFirstItem = false;
    //            }
    //            descCheckBoxIds += "'";
    //            checkBox.Attributes["onClick"] += "uncheck(this.id,[" +
    //                descCheckBoxIds + "]);";
    //        }
    //    }


    //}

    //private void processSubMenu_checkBoxStructure(
    //    SubMenuStruct parentMenu,
    //    List<SubMenuStruct> subMenuList,
    //    Dictionary<CheckBox, List<CheckBox>> checkBoxDescendants,
    //    Dictionary<CheckBox, List<CheckBox>> checkBoxParents)
    //{
    //    if (parentMenu != null && parentMenu.checkBox != null)
    //    {
    //        // als�bb szint� men�k

    //        // subMenu lista hozz�v�tele a sz�l�kh�z: (checkBoxDescendants b�v�t�s)
    //        List<CheckBox> descendants = new List<CheckBox>();
    //        foreach (SubMenuStruct aSubMenu in subMenuList)
    //        {
    //            if (aSubMenu.checkBox != null)
    //            {
    //                descendants.Add(aSubMenu.checkBox);
    //            }
    //        }
    //        checkBoxDescendants.Add(
    //            parentMenu.checkBox, new List<CheckBox>(descendants));
    //        // �j bejegyz�s, ha m�g nincs:
    //        if (!checkBoxParents.ContainsKey(parentMenu.checkBox))
    //        {
    //            checkBoxParents.Add(parentMenu.checkBox, new List<CheckBox>());
    //        }

    //        // parentMenu �sszes sz�l�j�hez is hozz�adjuk:
    //        foreach (CheckBox parentCheckBox in checkBoxParents[parentMenu.checkBox])
    //        {
    //            if (checkBoxDescendants.ContainsKey(parentCheckBox))
    //            {
    //                List<CheckBox> descendantsOfParent =
    //                    checkBoxDescendants[parentCheckBox];
    //                // ehhez hozz�adjuk az �j lista tartalm�t:
    //                descendantsOfParent.AddRange(descendants);
    //            }
    //            else
    //            {
    //                checkBoxDescendants.Add(
    //                    parentCheckBox, new List<CheckBox>(descendants));
    //            }
    //        }


    //        // �j checkBoxParents elemek a subMenuList tagjaira:

    //        // parentMenu sz�l�list�ja:
    //        List<CheckBox> parentListOfParentMenu = null;
    //        if (checkBoxParents.ContainsKey(parentMenu.checkBox))
    //        {
    //            parentListOfParentMenu = checkBoxParents[parentMenu.checkBox];
    //        }

    //        foreach (SubMenuStruct aSubMenu in subMenuList)
    //        {
    //            if (aSubMenu.checkBox != null)
    //            {
    //                if (parentListOfParentMenu != null)
    //                {
    //                    List<CheckBox> newParentCheckBoxList =
    //                        new List<CheckBox>(parentListOfParentMenu);
    //                    newParentCheckBoxList.Add(parentMenu.checkBox);
    //                    checkBoxParents.Add(aSubMenu.checkBox, newParentCheckBoxList);
    //                }
    //            }
    //        }
    //    }
    //    // almen�k tov�bbi feldolgoz�sa:
    //    foreach (SubMenuStruct aSubMenu in subMenuList)
    //    {
    //        if (aSubMenu.subMenus != null && aSubMenu.subMenus.Count > 0
    //            && aSubMenu.checkBox != null)
    //        {
    //            processSubMenu_checkBoxStructure(aSubMenu, aSubMenu.subMenus,
    //                checkBoxDescendants, checkBoxParents);
    //        }
    //    }
    //}

    ///// <summary>
    ///// CheckBoxes.js regisztr�l�sa
    ///// </summary>
    //private void registerJavascript_CheckBoxes()
    //{
    //    if (!Page.ClientScript.IsClientScriptBlockRegistered("js_checkBoxes"))
    //    {
    //        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "js_checkBoxes",
    //            "<script language=javascript src='JavaScripts/CheckBoxes.js'></script>");
    //    }

    //}


    #endregion

    #region treeView_menu
    ///// <summary>
    ///// Csak Admin m�dban haszn�latos
    ///// </summary>
    //private Dictionary<string, TreeView> treeViewsDict;

    //private void addMenusInAdminMode_treeView()
    //{

    //    HtmlTableRow subMenus_row = MainMenuTable.Rows[2];
    //    treeViewsDict = new Dictionary<string, TreeView>();

    //    int cellIndex = 0;
    //    foreach (MainMenuStruct aMainMenu in mainMenus)
    //    {
    //        if (aMainMenu.subMenus != null &&
    //            aMainMenu.subMenus.Count > 0)
    //        {
    //            TreeView treeView = new TreeView();
    //            treeViewsDict.Add(aMainMenu.name, treeView);
    //            treeView.Width = Unit.Pixel(menuWidth.ToString());

    //            addSubmenusToTreenodecollection(aMainMenu.subMenus, treeView.Nodes);

    //            treeView.ExpandAll();
    //            treeView.ShowExpandCollapse = true;
    //            treeView.ShowLines = true;

    //            // cell�hoz adjuk:
    //            subMenus_row.Cells[cellIndex].Controls.Add(treeView);
    //        }
    //        cellIndex++;
    //    }
    //}
    //private void addSubmenusToTreenodecollection(
    //    List<SubMenuStruct> subMenuList, TreeNodeCollection treeNodeCollection)
    //{
    //    foreach (SubMenuStruct aSubMenu in subMenuList)
    //    {
    //        TreeNode newTreeNode = new TreeNode(aSubMenu.name);
    //        newTreeNode.Checked = true;
    //        newTreeNode.ShowCheckBox = true;
    //        newTreeNode.NavigateUrl = aSubMenu.url;
    //        if (aSubMenu.id != null)
    //        {
    //            newTreeNode.Value = aSubMenu.id.ToString();
    //        }
    //        else
    //        {
    //            newTreeNode.Value = "";
    //        }

    //        treeNodeCollection.Add(newTreeNode);
    //        if (aSubMenu.subMenus != null && aSubMenu.subMenus.Count > 0)
    //        {
    //            addSubmenusToTreenodecollection(
    //                aSubMenu.subMenus, newTreeNode.ChildNodes);
    //        }
    //    }
    //}
    #endregion
}
