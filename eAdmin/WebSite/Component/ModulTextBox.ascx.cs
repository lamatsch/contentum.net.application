using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_ModulTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public RequiredFieldValidator Validator
    {
        get { return Validator1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    //public string OnClick_View
    //{
    //    set { ViewImageButton.OnClientClick = value; }
    //    get { return ViewImageButton.OnClientClick; }
    //}

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { ModulNev.Text = value; }
        get { return ModulNev.Text; }
    }

    public TextBox TextBox
    {
        get { return ModulNev; }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public bool Enabled
    {
        set
        {
            ModulNev.Enabled = value;
            LovImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return ModulNev.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            ModulNev.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return ModulNev.ReadOnly; }
    }


    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    public void SetModulTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Modulok krt_modulok = (KRT_Modulok)result.Record;
                Text = krt_modulok.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("ModulokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + ModulNev.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';return false";

        //OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
        //        "MuveletekForm.aspx", "", HiddenField1.ClientID);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    #region ISelectableUserComponent Members

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;                  
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;  
                TextBox.CssClass += " ViewReadOnlyWebControl";
                ImageButton_Lov.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;            
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                TextBox.CssClass += " ViewDisabledWebControl";
                ImageButton_Lov.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(TextBox);
        componentList.Add(ImageButton_Lov);
        componentList.Add(ResetImageButton);

        ImageButton_Lov.OnClientClick = "";
        ResetImageButton.OnClientClick = "";
        
        // Le kell tiltani a ClientValidatort
        Validate = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
