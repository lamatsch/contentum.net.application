﻿using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Contentum.eUtility;
using System.Data;


// TODO: lapozás paging nélkül
// Rendezés (hogyan értelmezhető több gridview esetén?)
public partial class Component_MultiGridViewPager : System.Web.UI.UserControl
{
    //private static int _PageIndex = 0;
    //private static int _PageCount = 0;

    private const string CssClass_item_default = "";

    public string Width
    {
        get { return MainTable.Width; }
        set { MainTable.Width = value; }
    }

    public string Title
    {
        get { return labelTitle.Text; }
        set { labelTitle.Text = value; }
    }

    public string TitleCssClass
    {
        get { return td_title.Attributes["class"]; }
        set { td_title.Attributes["class"] = value; }
    }

    #region Pager Public Property
    public int PageIndex
    {
        get
        {
            try
            {
                return Int32.Parse(PageIndex_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }
        set
        {
            PageIndex_HiddenField.Value = value.ToString();
        }
    }
    public int PageCount
    {
        get
        {
            try
            {
                return Int32.Parse(PageCount_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 1;
            }
        }
        set
        {
            PageCount_HiddenField.Value = value.ToString();
        }
    }

    public void RefreshPagerLabel()
    {
        // max. oldalszám:
        int pageSize = Int32.Parse(RowCount);
        if (pageSize == 0)
        {
            PageCount = 1;
        }
        else
        {
            PageCount = RecordNumber / pageSize;
            if (RecordNumber % pageSize > 0) PageCount++;
        }

        PagerLabel = (PageIndex + 1).ToString() + "/" + PageCount;
    }


    #region SelectedRecord properties

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(selectedRecordId.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedRecordId.Value;
            }
        }
        set
        {
            selectedRecordId.Value = value;
            RaiseSelectedRecordChanged();
        }
    }


    public string SelectedGridViewUniqueID
    {
        get
        {
            if (String.IsNullOrEmpty(selectedGridViewUniqueID.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedGridViewUniqueID.Value;
            }
        }
        set
        {
            selectedGridViewUniqueID.Value = value;
            RaiseSelectedGridViewChanged();
        }
    }
    

    public event EventHandler SelectedRecordChanged;

    private void RaiseSelectedRecordChanged()
    {
        if (SelectedRecordChanged != null)
        {
            SelectedRecordChanged(this, EventArgs.Empty);
        }
    }

    public event EventHandler SelectedGridViewChanged;

    private void RaiseSelectedGridViewChanged()
    {
        if (SelectedGridViewChanged != null)
        {
            SelectedGridViewChanged(this, EventArgs.Empty);
        }
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
        SelectedGridViewUniqueID = String.Empty;
    }

    private List<GridView> attachedGridViews = null;

    public List<GridView> AttachedGridViews
    {
        get { return attachedGridViews; }
        set
        {
            if (attachedGridViews != null)
            {
                foreach (GridView attachedGridView in attachedGridViews)
                {
                    attachedGridView.SelectedIndexChanged -= attachedGridView_SelectedIndexChanged;
                }
            }
            attachedGridViews = value;
            foreach (GridView attachedGridView in attachedGridViews)
            {
                attachedGridView.SelectedIndexChanged += new EventHandler(attachedGridView_SelectedIndexChanged);
            }
        }
    }

    public List<GridView> AddAttachedGridViewToPager(GridView attachedGridView)
    {
        List<GridView> attachedGridViewsNew = AttachedGridViews;
        if (attachedGridViewsNew == null)
        {
            attachedGridViewsNew = new List<GridView>();
        }
        if (!attachedGridViewsNew.Contains(attachedGridView))
        {
            attachedGridViewsNew.Add(attachedGridView);
        }
        AttachedGridViews = attachedGridViewsNew;

        return AttachedGridViews;
    }

    public List<GridView> RemoveAttachedGridViewFromPager(GridView attachedGridView)
    {
        if (attachedGridViews != null && attachedGridViews.Contains(attachedGridView))
        {
            AttachedGridViews.Remove(attachedGridView);
        }

        return AttachedGridViews;
    }

    protected void attachedGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        // TODO: a többi törlése, most az utolsó kerül be
        GridView gv = (GridView)sender;

        // TODO: kell ez majd valamire?
        string prevSelectedGridViewUniqueID = SelectedGridViewUniqueID;
        GridView prevSelectedGridView = null;
        if (!String.IsNullOrEmpty(SelectedGridViewUniqueID))
        {
            prevSelectedGridView = (GridView)Page.FindControl(SelectedGridViewUniqueID);
        }

        foreach (GridView attachedGridView in attachedGridViews)
        {
            if (attachedGridView.Equals(gv))
            {
                if (attachedGridView.SelectedValue != null)
                {
                    SelectedRecordId = attachedGridView.SelectedValue.ToString();
                    SelectedGridViewUniqueID = attachedGridView.UniqueID;
                }
                else
                {
                    SelectedRecordId = String.Empty;
                    SelectedGridViewUniqueID = String.Empty;
                }
            }
            else
            {
                attachedGridView.SelectedIndex = -1;
            }
        }
    }

    public void FillAttachedGridViews(Contentum.eBusinessDocuments.Result result, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        FillAttachedGridViews(result, errorPanel, parentErrorUpdatePanel, true);
    }

    public void FillAttachedGridViews(Contentum.eBusinessDocuments.Result result, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel, bool showHeaderForEmptyGridView)
    {
        if (attachedGridViews == null || attachedGridViews.Count == 0) return;

        string RowCount = this.RowCount; //String.Empty;
        string selectedRecordId = this.SelectedRecordId; //String.Empty;


        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }
        else
        {

            int rowCount = -1;
            if (!String.IsNullOrEmpty(RowCount) && RowCount != "0")
            {
                rowCount = Int32.Parse(RowCount);
            }
            else
            {
                rowCount = 10; //ds.Tables[0].Rows.Count; // alapertek, mondjuk rendszer parameterekbol veve!!!
            }

            DataSet ds = result.Ds;
            //bool isEmpty = true;
            bool isSingleRow = false; // true, ha csak egy találat van

            int allRowsCount = 0;

            bool isPaging = ds.Tables.Contains("paging");

            GridView singleRowGridView = null;
            int singleRowTableIndex = -1;

            if (!isPaging)
            {
                int pageIndexGlobal = this.PageIndex;
                int startRecordGlobal = pageIndexGlobal * rowCount;
                int afterEndRecordGlobal = startRecordGlobal + rowCount;

                int startRecord = startRecordGlobal;
                int afterEndRecord = afterEndRecordGlobal;

                int cntTableGridViewPairs = Math.Min(attachedGridViews.Count, ds.Tables.Count);

                for (int i = 0; i < cntTableGridViewPairs; i++)
                {
                    GridView ParentGridView = attachedGridViews[i];
                    DataTable dt = ds.Tables[i];
                    DataTable dtPaging = null;

                    ParentGridView.PageSize = rowCount;

                    int currentRowsCount = dt.Rows.Count;

                    // belül vagyunk a táblán
                    if (startRecord >= 0 && afterEndRecord >= 0)
                    {
                        if (startRecord < currentRowsCount)
                        {
                            if (startRecord == 0 && dt.Rows.Count == rowCount)
                            {
                                dtPaging = dt.Copy();
                            }
                            else
                            {
                                dtPaging = dt.Clone();
                                int maxRow = Math.Min(afterEndRecord, dt.Rows.Count);
                                for (int r = startRecord; r < maxRow; r++)
                                {
                                    dtPaging.ImportRow(dt.Rows[r]);
                                }
                            }
                            startRecord = 0;
                            afterEndRecord = rowCount - (currentRowsCount - startRecord);
                        }
                        else if (currentRowsCount == 0)
                        {
                            dtPaging = dt.Copy();
                        }
                        else
                        {
                            startRecord -= currentRowsCount;
                            afterEndRecord -= currentRowsCount;
                        }
                    }


                    ParentGridView.DataSource = dtPaging;

                    allRowsCount += dt.Rows.Count;
                    if (allRowsCount == 1 && dt.Rows.Count == 1)
                    {
                        singleRowGridView = ParentGridView;
                        singleRowTableIndex = i;
                    }
                    else if (allRowsCount > 1)
                    {
                        singleRowGridView = null;
                        singleRowTableIndex = -1;
                    }
                }
            } // if (!isPaging)
            else
            {
                // találatok száma:
                int recordNumberFromPaging = Int32.Parse(ds.Tables["paging"].Rows[0]["RecordNumber"].ToString());
                this.RecordNumber = recordNumberFromPaging;
                allRowsCount = recordNumberFromPaging;

                // oldalszám:
                int pageIndex = Int32.Parse(ds.Tables["paging"].Rows[0]["PageNumber"].ToString());
                PageIndex = pageIndex;

                int cntTableGridViewPairs = Math.Min(attachedGridViews.Count, ds.Tables.Count - 1);
                // a lapozási táblát kihagyjuk
                for (int i = 0; i < cntTableGridViewPairs; i++)
                {
                    GridView ParentGridView = attachedGridViews[i];

                    DataTable dtPaging = ds.Tables[i+1];

                    ParentGridView.DataSource = dtPaging;

                    if (allRowsCount == 1 && dtPaging.Rows.Count == 1)
                    {
                        singleRowGridView = ParentGridView;
                        singleRowTableIndex = i;
                    }
                    else if (allRowsCount > 1)
                    {
                        singleRowGridView = null;
                        singleRowTableIndex = -1;
                    }

                    //a hiddenfield-ben beállított id-hoz tartozó sor kiválasztása
                    //ha nincs beállítva id, a kiválasztás törlése
                    if (!String.IsNullOrEmpty(selectedRecordId) && selectedRecordId != Guid.Empty.ToString())
                    {
                        string id = selectedRecordId;
                        int selectedIndex = -1;

                        string keyName = null;
                        if (ParentGridView.DataKeyNames != null && ParentGridView.DataKeyNames.Length == 1)
                        {
                            keyName = ParentGridView.DataKeyNames[0];
                        }
                        else
                        {
                            keyName = "Id";
                        }

                        if (dtPaging.TableName != "paging")
                        {
                            for (int r = 0; r < dtPaging.Rows.Count; r++)
                            {
                                if (dtPaging.Rows[r][keyName].ToString() == id)
                                {
                                    selectedIndex = r;
                                    break;
                                }
                            }
                        }
                        if (selectedIndex > -1)
                        {
                            // TODO: az aktuális táblára/gridviewra
                            rowCount = Int32.Parse(RowCount);

                            if (rowCount > 0)
                                selectedIndex = selectedIndex % rowCount;

                            ParentGridView.SelectedIndex = selectedIndex;
                            SelectedGridViewUniqueID = ParentGridView.UniqueID;
                        }
                        else
                        {
                            ParentGridView.SelectedIndex = -1;
                            SelectedRecordId = String.Empty;
                            SelectedGridViewUniqueID = String.Empty;
                        }
                    }
                    else
                    {
                        ParentGridView.SelectedIndex = -1;
                    }

                }

            }

            if (allRowsCount > 0)
            {
                //isEmpty = false;
                if (allRowsCount == 1)
                {
                    isSingleRow = true;
                }
            }

           if (showHeaderForEmptyGridView)
           {
                //foreach (DataTable dt in ds.Tables)
                foreach (GridView ParentGridView in attachedGridViews)
                {
                    DataTable dt = ParentGridView.DataSource as DataTable;
                    if (dt != null && dt.Rows.Count == 0)
                    {
                        DataRow x = dt.NewRow();
                        dt.Rows.Add(x);
                    }
                }
            }


            ////if (!isPaging)
            ////{

            //    //a hiddenfield-ben beállított id-hoz tartozó sor kiválasztása
            //    //ha nincs beállítva id a kiválasztás törlése
            //    if (!String.IsNullOrEmpty(selectedRecordId) && selectedRecordId != Guid.Empty.ToString())
            //    {
            //        string id = selectedRecordId;
            //        int selectedIndex = -1;
            //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //        {
            //            if (ds.Tables[0].Rows[i]["Id"].ToString() == id)
            //            {
            //                selectedIndex = i;
            //                break;
            //            }
            //        }
            //        if (selectedIndex > -1)
            //        {
            //            int rowCount = ParentGridView.PageSize;
            //            int pageNumber = 0;
            //            if (rowCount > 0)
            //            {
            //                pageNumber = selectedIndex / rowCount;
            //                selectedIndex = selectedIndex % rowCount;
            //            }
            //            ParentGridView.PageIndex = pageNumber;
            //            ParentGridView.SelectedIndex = selectedIndex;
            //            if (piPageIndex != null)
            //            {
            //                piPageIndex.SetValue(MultiGridViewPager, pageNumber, null);
            //            }
            //            else
            //            {
            //                System.Diagnostics.Debug.WriteLine("PageIndex property not found.");
            //            }
            //        }
            //        else
            //        {
            //            ParentGridView.SelectedIndex = -1;
            //            piSelectedRecorID.SetValue(MultiGridViewPager, String.Empty, null);
            //        }
            //    }
            //    else
            //    {
            //        ParentGridView.SelectedIndex = -1;
            //    }


            this.RecordNumber = allRowsCount;
            //}

            foreach (GridView ParentGridView in attachedGridViews)
            {
                ParentGridView.DataBind();

                if (showHeaderForEmptyGridView && ParentGridView.Rows.Count == 1)
                {
                    ParentGridView.Rows[0].Visible = false;
                }

                if (ParentGridView.SelectedIndex > -1 && ParentGridView.SelectedIndex < ParentGridView.Rows.Count)
                {
                    UI.SetGridViewCheckBoxesToSelectedRow(ParentGridView, ParentGridView.SelectedIndex, "check");
                }
            }
        }
    }

    //public string AttachedGridViewId
    //{
    //    get
    //    {
    //        if (attachedGridView != null)
    //            return attachedGridView.ID;
    //        else
    //            return String.Empty;
    //    }
    //}

    public void SetScrollable(AjaxControlToolkit.CollapsiblePanelExtender CPE)
    {
        if (CPE == null) return;

        CPE.ScrollContents = this.Scrollable;
        if (Scrollable)
        {
            int height = Rendszerparameterek.GetInt(CPE.Page, Rendszerparameterek.SCROLL_PANEL_HEIGHT);
            if (height <= 0)
            {
                height = 200;
            }
            CPE.ExpandedSize = height; // grid magassaga, ha scrollable
        }
        else
        {
            CPE.ExpandedSize = 0; // grid magassaga, ha nem scrollable
        }
    }

    #endregion

    public String PagerLabel
    {
        get { return Pager.Text; }
        set { Pager.Text = value; }
    }

    public bool Scrollable
    {
        get { return ScrollableCheckBox.Checked; }
        //set { _PageIndex = value; }
    }

    //public string RowCount
    //{
    //    get { return RowCountTextBox.Text; }
    //    set { RowCountTextBox.Text = value; }
    //}

    public string RowCount
    {
        get
        {
            string strRowCount = RowCountTextBox.Text;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            strRowCount = this.GetRowCountFromSession();
            if (!String.IsNullOrEmpty(strRowCount))
            {
                RowCountTextBox.Text = strRowCount;
                return strRowCount;
            }

            RowCountTextBox.Text = Constants.MasterRowCountNumber.ToString();
            return RowCountTextBox.Text;
        }
        set { RowCountTextBox.Text = value; }
        //set { _PageIndex = value; }
    }

    private string GetRowCountFromSession()
    {
        if (Session[Constants.DetailRowCount] != null)
        {
            string strRowCount = Session[Constants.DetailRowCount].ToString(); ;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            Session[Constants.DetailRowCount] = Constants.MasterRowCountNumber;
            return Constants.MasterRowCountNumber.ToString();

        }
        else
        {
            return String.Empty;
        }
    }

    public int RecordNumber
    {
        get
        {
            string text = labelRecordNumber.Text;
            int number = 0;
            if (text.StartsWith("(") && text.EndsWith(")"))
            {
                Int32.TryParse(text.Substring(1, text.Length - 2), out number);
            }
            return number;
        }
        set { labelRecordNumber.Text = String.Format("({0})", value); }
    }


    #endregion

    #region SetImageButtonEnabledProperty
    private void SetImageButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        imageButton.Enabled = enabledValue;
    }

    private bool IsDisabledButtonToSetHidden()
    {
        return Contentum.eUtility.Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS);
    }
    #endregion SetImageButtonEnabledProperty


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //lapozás esetén kiválasztás törlése
        switch ((sender as ImageButton).CommandName.ToString())
        {
            case "First":
                if (PageIndex != 0)
                {
                    PageIndex = 0;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Prev":
                if (PageIndex - 1 >= 0)
                {
                    PageIndex = PageIndex - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Next":
                if (PageIndex + 1 < PageCount)
                {
                    PageIndex = PageIndex + 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Last":
                if (PageIndex != PageCount - 1)
                {
                    PageIndex = PageCount - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "IndexChanged":
                if (PagingButtonsClick != null)
                {
                    PagingButtonsClick(this, new EventArgs());
                }
                break;
        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        bool isDisabledIconToSetToHidden = IsDisabledButtonToSetHidden();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session[Constants.DetailRowCount] == null)
        {
            Session[Constants.DetailRowCount] = RowCountTextBox.Text;
        }

        if (!IsPostBack)
        {
            if (Page.Session[Constants.DetailGridViewScrollable] != null)
                ScrollableCheckBox.Checked = (bool)Page.Session[Constants.DetailGridViewScrollable];

            if (Session[Constants.DetailRowCount] != null)
            {
                RowCountTextBox.Text = Session[Constants.DetailRowCount].ToString();
            }
        }

        //rendezés esetén selectedRecordId törlése
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (eventArgument.Split('$')[0].ToLower() == "sort")
            {
                if (Request.Params["__EVENTTARGET"] != null)
                {
                    string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                    string[] list = eventTarget.Split('$');
                    // TODO: (jelenleg nincs értelmezve a rendezés a több gridview esetére)
                    //if (AttachedGridView != null && list[list.Length - 1] == AttachedGridViewId)
                    //{
                    //    ForgetSelectedRecord();
                    //}
                }
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Ha törölve lett a gridView a SelectedRecordId mezőt is törölni kell
        bool isThereAnyGridViewRow = false;
        if (AttachedGridViews != null)
        {
            foreach (GridView attachedGridView in AttachedGridViews)
            {
                if (attachedGridView.Rows.Count > 0)
                    isThereAnyGridViewRow = true;
            }
        }
        if (!isThereAnyGridViewRow)
        {
            ForgetSelectedRecord();
        }
    }

    public event CommandEventHandler ButtonsClick;

    public event EventHandler PagingButtonsClick;

    public virtual void Buttons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    protected void ScrollableCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        Page.Session[Constants.DetailGridViewScrollable] = (sender as CheckBox).Checked;
    }

    public event EventHandler RowCount_Changed;

    protected void RowCountTextBox_TextChanged(object sender, EventArgs e)
    {
        Session[Constants.DetailRowCount] = RowCount;
        if (RowCount_Changed != null)
        {
            RowCount_Changed(sender, e);
        }
    }
}

