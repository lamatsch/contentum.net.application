using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_MuveletTextBox : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    //public string OnClick_View
    //{
    //    set { ViewImageButton.OnClientClick = value; }
    //    get { return ViewImageButton.OnClientClick; }
    //}

    public string Text
    {
        set { MuveletNev.Text = value; }
        get { return MuveletNev.Text; }
    }

    public string Kod
    {
        get
        {
            return HiddenFieldKod.Value;
        }
        set
        {
            HiddenFieldKod.Value = value;
        }
    }

    public TextBox TextBox
    {
        get { return MuveletNev; }
    }

    public bool Enabled
    {
        set
        {
            MuveletNev.Enabled = value;
            LovImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return MuveletNev.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            MuveletNev.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return MuveletNev.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                MuveletNev.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                MuveletNev.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    public void SetMuveletTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_MuveletekService service = eAdminService.ServiceFactory.GetKRT_MuveletekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Muveletek krt_muveletek = (KRT_Muveletek)result.Record;
                Text = krt_muveletek.Nev;
                Kod = krt_muveletek.Kod;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
            Kod = String.Empty;
        }
    }

    public void Clear()
    {
        Id_HiddenField = String.Empty;
        Text = String.Empty;
        Kod = String.Empty;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {       
        OnClick_Lov = JavaScripts.SetOnClientClick("MuveletekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + MuveletNev.ClientID +
           "&" + "HiddenFieldKodId=" + HiddenFieldKod.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';$get('" + HiddenFieldKod.ClientID + "').value = '';return false";

        //OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
        //        "MuveletekForm.aspx", "", HiddenField1.ClientID);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(MuveletNev);
        componentList.Add(LovImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
