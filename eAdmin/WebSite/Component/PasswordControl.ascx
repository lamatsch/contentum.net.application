﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordControl.ascx.cs" Inherits="Component_PasswordControl" %>

<%@ Register Src="RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<tr class="urlapSor" id="trPasswordJelszo" runat="server">
    <td class="mrUrlapCaption">
    <asp:Panel runat="server" ID="PanelLabelsJelszo">
        <asp:Label ID="LabelReq1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
        <asp:Label ID="labelJelszo" runat="server" Text="Jelszó:"></asp:Label>
    </asp:Panel>    
    </td>
    <td class="mrUrlapMezo">
        <uc3:RequiredTextBox ID="requiredTextBoxJelszo" runat="server" TextBoxMode="Password" LabelRequiredIndicatorID="LabelReq1"/>
    </td>
</tr>
<tr class="urlapSor" id="trPasswordJelszoMegerosites" runat="server">
    <td class="mrUrlapCaption">
    <asp:Panel runat="server" ID="PanelLabelsJelszoMegerosites">
        <asp:Label ID="LabelReq2" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
        <asp:Label ID="labelJelszoMegerosites" runat="server" Text="Jelszó megerősítése:"></asp:Label>
    </asp:Panel>      
    </td>
    <td class="mrUrlapMezo">
        <uc3:RequiredTextBox ID="requiredTextBoxJelszoMegerositese" runat="server" TextBoxMode="Password" LabelRequiredIndicatorID="LabelReq2"/>
    </td>
</tr>
<asp:CompareValidator ID="CompareValidatorPassword" runat="server" 
SetFocusOnError="true" ControlToCompare="requiredTextBoxJelszoMegerositese$TextBox1" ControlToValidate="requiredTextBoxJelszo$TextBox1"  
ErrorMessage="CompareValidator" Operator="Equal" Type="String" Display="None" EnableClientScript="false"></asp:CompareValidator>