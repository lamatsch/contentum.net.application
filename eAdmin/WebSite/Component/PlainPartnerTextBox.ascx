﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlainPartnerTextBox.ascx.cs" Inherits="Component_PlainPartnerTextBox" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<div class="DisableWrap">
    <asp:TextBox ID="PartnerMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="PartnerMegnevezes"
        Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
        <Animations>
                <OnShow>
                <Sequence>
                    <HideAction Visible="true" />
                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                </Sequence>    
                </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" MinimumPrefixLength="2"
        TargetControlID="PartnerMegnevezes" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true"
        ServicePath="http://localhost:100/eAdminWebService/KRT_PartnerekService.asmx" ServiceMethod="GetPlainPartnerekList"
        CompletionListItemCssClass="GridViewRowStyle" CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle"
        CompletionListCssClass="AutoCompleteExtenderCompletionList">
    </ajaxToolkit:AutoCompleteExtender>

    <eUI:WorldJumpExtender ID="WorldJumpExtender1" runat="server" TargetControlID="PartnerMegnevezes" AutoCompleteExtenderId="AutoCompleteExtender1" />
</div>
