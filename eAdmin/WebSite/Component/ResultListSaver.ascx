﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResultListSaver.ascx.cs" Inherits="Component_ResultListSaver" %>

<table border="0" cellpadding="0" cellspacing="0"><tr><td>
<div runat="server" id="Div_ResultListPanel" style="width: 0px; height: 0px; top: 800px; left: 0px; position:fixed; display: none;">
<eUI:eFormPanel ID="EEEEFormPanel1" runat="server" Width="600px" Enabled="true" Visible="true">    
      <table>
        <tr>
            <td style="height: 26px; text-align: right;">
                <asp:Label ID="Label20" CssClass="formLoaderCaption" runat="server" Text="Találati lista neve:"></asp:Label></td>
            <td colspan="1" style="padding-right:10px;text-align:left">
                <asp:TextBox ID="NewResultListTextBox" runat="server" CssClass="formLoaderInput"></asp:TextBox></td>
            <td colspan="1" style="text-align:left">
                <asp:Button ID="NewResultListButton" runat="server" Text="Mentés" CausesValidation="false" OnClick="Button_Click" />
                </td>
        </tr>
    </table>
<asp:Button ID="Button5" runat="server" Text="Bezár" CausesValidation="false"/>
    <asp:HiddenField ID="Ver_KRT_UIMezoObjektumErtekek_HiddenField" runat="server" />
</eUI:eFormPanel>
</div>
</td></tr>
<tr><td>
<asp:Panel runat="server" ID="ResultListPanel" >
<table border="0" cellpadding="0" cellspacing="0">
<tr><td style="text-align: left;padding-right: 10px; ">
<asp:ImageButton TabIndex = "-1" ID="ResultListImageButton" runat="server" 
    ImageUrl="~/images/hu/egyeb/resultlist_ikon.png" onmouseover="swapByName(this.id,'resultlist_ikon_keret.png')" onmouseout="swapByName(this.id,'resultlist_ikon.png')" 
    AlternateText="Találati lista mentése" CausesValidation="false" />&nbsp;
    </td>
    </tr></table>
    </asp:Panel>
</td></tr></table>

<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender5" runat="server" 
        PopupControlID="EEEEFormPanel1" 
        TargetControlID="ResultListImageButton"  
        CancelControlID="Button5"          
        PopupDragHandleControlID="EEEEFormPanel1" 
        BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>

