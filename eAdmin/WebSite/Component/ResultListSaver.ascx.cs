﻿using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Data;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_ResultListSaver : System.Web.UI.UserControl//, IScriptControl
{
    #region Settings

    private const string defaultTemplateBackgroundColor = "yellow";
    private const string defaultTemplateStyle = "background:" + defaultTemplateBackgroundColor;
    private const string servicePath = @"WrappedWebService/Ajax.asmx";

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        NewResultListButton.CommandName = Contentum.eAdmin.Utility.CommandName.NewResultList;

        ResultListImageButton.OnClientClick = "var divObj = document.getElementById('" + Div_ResultListPanel.ClientID + "'); "
            + " divObj.style.top='0px'; divObj.style.display='';";

        NewResultListButton.Attributes["onclick"] = "if (document.getElementById('" + NewResultListTextBox.ClientID + "').value=='') "
            + " { alert('" + Resources.Error.UITextBoxIsEmpty + "'); return false; }";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    private object searchObject;

    public object SearchObject
    {
        get { return searchObject; }
        set { searchObject = value; }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel = null;

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return errorPanel; }
        set { errorPanel = value; }
    }

    public void NewResultList(object newSearchObject, ExecParam execParam)
    {
        SqlConnection temp = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ConnectionString);
        searchObject = newSearchObject;
        Result_List resultObject = new Result_List(temp);
        resultObject.Save(searchObject, DateTime.Now, execParam, System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName, NewResultListTextBox.Text);
        if (resultObject.ErrorFlag)
        {
            Response.Write("<script> alert('" + resultObject.ClassMsg + "');</script>");
        }
    }

    public event CommandEventHandler ButtonsClick;

    protected void Button_Click(object sender, EventArgs e)
    {
        if (ButtonsClick != null)
        {
            string commandName = (sender as Button).CommandName;

            CommandEventArgs args = null;
            if (commandName == Contentum.eAdmin.Utility.CommandName.NewResultList)
            {
                string newResultList = NewResultListTextBox.Text;
                //NewResultListTextBox.Text = "";
                args = new CommandEventArgs(commandName, newResultList);
            }

            ButtonsClick(this, args);
        }
    }

    /*#region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.TemplateLoaderBehavior", this.EEEEFormPanel1.ClientID);
        descriptor.AddProperty("TemplateListId", "");
        descriptor.AddProperty("SetDefaultButtonId", "");
        descriptor.AddProperty("RemoveDefaultButtonId", "");
        descriptor.AddProperty("DefaultTemplateBackgroundColor", defaultTemplateBackgroundColor);
        descriptor.AddProperty("UpdateAlapertelmezettMethod", "");
        descriptor.AddProperty("ServicePath", servicePath);
        descriptor.AddProperty("FelhasznaloId", Contentum.eAdmin.Utility.FelhasznaloProfil.FelhasznaloId(Page));
        descriptor.AddProperty("DefaultTemplateHiddenFieldId", "");
        descriptor.AddProperty("CurrentTemplateHiddenFieldId", "");
        descriptor.AddProperty("CurrentVersionHiddenFieldId", Ver_KRT_UIMezoObjektumErtekek_HiddenField.ClientID);
        descriptor.AddProperty("LabelDefaultId", "");
        descriptor.AddProperty("LinkButtonsPanelId", "");

        return new ScriptDescriptor[] { descriptor };
    }

    public IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/TemplateLoader.js";

        return new ScriptReference[] { reference };
    }

    #endregion*/
}
