<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchHeader.ascx.cs" Inherits="Component_SearchHeader" %>
<%@ Register Src="FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc1" %>
<%-- bernat.laszlo added: Tal�lati lista ment�se--%>
<%@ Register Src="ResultListSaver.ascx" TagName="ResultListSaver" TagPrefix="resultList"%>
<%-- bernat.laszlo eddig--%>
<%@ Register Src="HelpLink.ascx" TagName="HelpLink" TagPrefix="help" %>   
    <table style="width: 100%; height: 70px; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat: repeat-x;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 85px; vertical-align: top;">
            <img id="Img1" runat="server" src="../images/hu/fejlec/kepernyo_ikon.jpg" alt=" " />
            </td>
            <td style="vertical-align: top; text-align: left;">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <tr>
                        <td style="height: 46px; text-align: left; vertical-align: middle;">
                            <asp:Label CssClass="FormHeaderText" ID="Title_Label" runat="server"></asp:Label>   
                        </td>
                        <td align="right" style="height: 46px; vertical-align: bottom; position: static; text-align: right;">
                         <%-- Spacer--%>
                        </td>
                        <td style="vertical-align: top; text-align:right; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat:repeat-x; width: 128px; height: 46px;">
                       </td>
                        <td align="right" valign="middle" style="padding-left:1px">
                            <help:HelpLink ID="linkHelp" runat="server" />
                        </td>                                                    
                    </tr>
                    <tr>
                        <td style="height: 3px;" colspan="3">
                        <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <%-- bernat.laszlo added: Tal�lati lista ment�se--%>
                                <resultList:ResultListSaver id="ResultListSaver1" runat="server" Visible = "true" />
                                 <%-- bernat.laszlo eddig--%>  
                            </td>
                            <td>
                                 <uc1:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="true"></uc1:FormTemplateLoader> 
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td colspan="4">
        <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false" Width="90%" EnableViewState="false">
        </eUI:eErrorPanel>
        </td>
        </tr>
    </table>
