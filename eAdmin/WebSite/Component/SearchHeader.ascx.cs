using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;

public partial class Component_SearchHeader : System.Web.UI.UserControl
{

    public string HeaderTitle
    {
        set 
        { 
            Title_Label.Text = value;
            Page.Title = value;
        }
        get { return Title_Label.Text; }
    }

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return EErrorPanel1; }
    }

    public object TemplateObject
    {
        get { return FormTemplateLoader1.SearchObject; }
        set { FormTemplateLoader1.SearchObject = value; }
    }

    public Type TemplateObjectType
    {
        get { return FormTemplateLoader1.SearchObjectType; }
        set { FormTemplateLoader1.SearchObjectType = value; }
    }

    public string CustomTemplateTipusNev
    {
        get { return FormTemplateLoader1.CustomTemplateTipusNev; }
        set { FormTemplateLoader1.CustomTemplateTipusNev = value; }
    }

    public string CurrentTemplateName
    {
        get { return FormTemplateLoader1.CurrentTemplateName; }
        set { FormTemplateLoader1.CurrentTemplateName = value; }
    }

    public string CurrentTemplateId
    {
        get { return FormTemplateLoader1.CurrentTemplateId; }
        set { FormTemplateLoader1.CurrentTemplateId = value; }
    }

    /// <summary>
    /// �j template l�trehoz�sa
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void NewTemplate(object newSearchObject)
    {
        FormTemplateLoader1.NewTemplate(newSearchObject);
    }

    public void NewResultList(object newSearchObject, ExecParam execParam)
    {
        ResultListSaver1.NewResultList(newSearchObject, execParam);
    }

    /// <summary>
    /// Aktu�lis template elment�se
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void SaveCurrentTemplate(object newSearchObject)
    {
        FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    }

    public void TemplateReset()
    {
        FormTemplateLoader1.TemplateReset();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
        FormTemplateLoader1.ErrorPanel = ErrorPanel;
        ResultListSaver1.ButtonsClick += new CommandEventHandler(ResultListSaver1_ButtonsClick);
        ResultListSaver1.ErrorPanel = ErrorPanel;
    }

    void ResultListSaver1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (ButtonsClick != null)
        {
            ButtonsClick(this, e);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public event CommandEventHandler ButtonsClick;

    protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (ButtonsClick != null)
        {            
            ButtonsClick(this, e);
        }
    }



}
