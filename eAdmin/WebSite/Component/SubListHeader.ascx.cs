using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_SubListHeader : System.Web.UI.UserControl
{
    //private static int _PageIndex = 0;
    //private static int _PageCount = 0;

    private const string CssClass_item_default = "";

    public enum ValidityType { Valid, Invalid, All };

    #region Pager Public Property
    public int PageIndex
    {
        get
        {
            try
            {
                return Int32.Parse(PageIndex_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }
        set
        {
            PageIndex_HiddenField.Value = value.ToString();
        }
    }
    public int PageCount
    {
        get
        {
            try
            {
                return Int32.Parse(PageCount_HiddenField.Value);
            }
            catch (FormatException)
            {
                return 1;
            }
        }
        set
        {
            PageCount_HiddenField.Value = value.ToString();
        }
    }

    public void RefreshPagerLabel()
    {
        // max. oldalsz�m:
        int pageSize = Int32.Parse(RowCount);
        if (pageSize == 0)
        {
            PageCount = 1;
        }
        else
        {
            PageCount = RecordNumber / pageSize;
            if (RecordNumber % pageSize > 0) PageCount++;
        }

        PagerLabel = (PageIndex + 1).ToString() + "/" + PageCount;
    }


    #region SelectedRecord properties

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(selectedRecordId.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedRecordId.Value;
            }
        }
        set
        {
            selectedRecordId.Value = value;
        }
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
    }

    private GridView attachedGridView = null;

    public GridView AttachedGridView
    {
        get { return attachedGridView; }
        set
        {
            if (attachedGridView != null)
            {
                attachedGridView.SelectedIndexChanged -= attachedGridView_SelectedIndexChanged;
            }
            attachedGridView = value;
            attachedGridView.SelectedIndexChanged += new EventHandler(attachedGridView_SelectedIndexChanged);
        }
    }

    protected void attachedGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (attachedGridView.SelectedValue != null)
            SelectedRecordId = attachedGridView.SelectedValue.ToString();
    }

    public string AttachedGridViewId
    {
        get
        {
            if (attachedGridView != null)
                return attachedGridView.ID;
            else
                return String.Empty;
        }
    }

    #endregion

    public String PagerLabel
    {
        get { return Pager.Text; }
        set { Pager.Text = value; }
    }

    public bool Scrollable
    {
        get { return ScrollableCheckBox.Checked; }
        //set { _PageIndex = value; }
    }

    //public string RowCount
    //{
    //    get { return RowCountTextBox.Text; }
    //    set { RowCountTextBox.Text = value; }
    //}

    public string RowCount
    {
        get
        {
            string strRowCount = RowCountTextBox.Text;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            strRowCount = this.GetRowCountFromSession();
            if (!String.IsNullOrEmpty(strRowCount))
            {
                RowCountTextBox.Text = strRowCount;
                return strRowCount;
            }

            RowCountTextBox.Text = Constants.MasterRowCountNumber.ToString();
            return RowCountTextBox.Text;
        }
        set { RowCountTextBox.Text = value; }
        //set { _PageIndex = value; }
    }

    private string GetRowCountFromSession()
    {
        if (Session[Constants.DetailRowCount] != null)
        {
            string strRowCount = Session[Constants.DetailRowCount].ToString(); ;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            Session[Constants.DetailRowCount] = Constants.MasterRowCountNumber;
            return Constants.MasterRowCountNumber.ToString();

        }
        else
        {
            return String.Empty;
        }
    }

    public int RecordNumber
    {
        get
        {
            string text = labelRecordNumber.Text;
            int number = 0;
            if (text.StartsWith("(") && text.EndsWith(")"))
            {
                Int32.TryParse(text.Substring(1, text.Length - 2), out number);
            }
            return number;
        }
        set { labelRecordNumber.Text = String.Format("({0})", value); }
    }

    public bool ValidFilterVisible
    {
        get { return ValidFilter.Visible; }
        set { ValidFilter.Visible = value; }
    }

    public ValidityType ValidFilterSetting
    {
        get
        {
            switch (ValidFilter.SelectedIndex)
            {
                case 0:
                    return ValidityType.Valid;
                case 1:
                    return ValidityType.Invalid;
                case 2:
                    return ValidityType.All;
                default:
                    return ValidityType.All;
            }
        }

    }

    public bool RightButtonsVisible
    {
        get
        {
            return tdRightButtons.Visible;
        }
        set
        {
            tdRightButtons.Visible = value;
        }
    }

    #endregion

    #region SetImageButtonEnabledProperty
    private void SetImageButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        imageButton.Enabled = enabledValue;
    }

    private bool IsDisabledButtonToSetHidden()
    {
        return Contentum.eUtility.Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS);
    }
    #endregion SetImageButtonEnabledProperty

    #region New Button Public Properties
    public String NewOnClientClick
    {
        get { return ImageNew.OnClientClick; }
        set { ImageNew.OnClientClick = value; }
    }
    public bool NewEnabled
    {
        get { return ImageNew.Enabled; }
        set { SetImageButtonEnabledProperty(ImageNew, value); }
    }
    public bool NewVisible
    {
        get { return ImageNew.Visible; }
        set { ImageNew.Visible = value; }
    }
    #endregion

    #region View Button Public Properties
    public String ViewOnClientClick
    {
        get { return ImageView.OnClientClick; }
        set { ImageView.OnClientClick = value; }
    }
    public bool ViewEnabled
    {
        get { return ImageView.Enabled; }
        set { SetImageButtonEnabledProperty(ImageView, value); }
    }
    public bool ViewVisible
    {
        get { return ImageView.Visible; }
        set { ImageView.Visible = value; }
    }
    #endregion

    #region Modify Button Public Properties
    public String ModifyOnClientClick
    {
        get { return ImageModify.OnClientClick; }
        set { ImageModify.OnClientClick = value; }
    }
    public bool ModifyEnabled
    {
        get { return ImageModify.Enabled; }
        set { SetImageButtonEnabledProperty(ImageModify, value); }
    }
    public bool ModifyVisible
    {
        get { return ImageModify.Visible; }
        set { ImageModify.Visible = value; }
    }
    #endregion

    #region Invalidate Button Public Properties
    public String InvalidateOnClientClick
    {
        get { return ImageInvalidate.OnClientClick; }
        set { ImageInvalidate.OnClientClick = value; }
    }
    public bool InvalidateEnabled
    {
        get { return ImageInvalidate.Enabled; }
        set { SetImageButtonEnabledProperty(ImageInvalidate, value); }
    }
    public bool InvalidateVisible
    {
        get { return ImageInvalidate.Visible; }
        set { ImageInvalidate.Visible = value; }
    }
    #endregion

    #region Delete Button Public Properties
    //LZS - BLG_13452
    public String DeleteOnClientClick
    {
        get { return ImageDelete.OnClientClick; }
        set { ImageDelete.OnClientClick = value; }
    }
    public bool DeleteEnabled
    {
        get { return ImageDelete.Enabled; }
        set { SetImageButtonEnabledProperty(ImageDelete, value); }
    }
    public bool DeleteVisible
    {
        get { return ImageDelete.Visible; }
        set { ImageDelete.Visible = value; }
    }
    #endregion

    #region Map Button Public Properties
    public String MapOnClientClick
    {
        get { return ImageMap.OnClientClick; }
        set { ImageMap.OnClientClick = value; }
    }
    public bool MapEnabled
    {
        get { return ImageMap.Enabled; }
        set { SetImageButtonEnabledProperty(ImageMap, value); }
    }
    public bool MapVisible
    {
        get { return ImageMap.Visible; }
        set { ImageMap.Visible = value; }
    }
    #endregion

    //#region Irattarozasra Button Public Properties
    //public String IrattarozasraOnClientClick
    //{
    //    get { return ImageIrattarozasra.OnClientClick; }
    //    set { ImageIrattarozasra.OnClientClick = value; }
    //}
    //public bool IrattarozasraEnabled
    //{
    //    get { return ImageIrattarozasra.Enabled; }
    //    set
    //    {
    //        ImageIrattarozasra.Enabled = value;
    //        if (value == false)
    //        {
    //            ImageIrattarozasra.CssClass = "disableditem";
    //        }
    //        else
    //        {
    //            ImageIrattarozasra.CssClass = "";
    //        }
    //    }
    //}
    //public bool IrattarozasraVisible
    //{
    //    get { return ImageIrattarozasra.Visible; }
    //    set { ImageIrattarozasra.Visible = value; }
    //}
    //#endregion

    //#region KiadasOsztalyra Button Public Properties
    //public String KiadasOsztalyraOnClientClick
    //{
    //    get { return ImageKiadasOsztalyra.OnClientClick; }
    //    set { ImageKiadasOsztalyra.OnClientClick = value; }
    //}
    //public bool KiadasOsztalyraEnabled
    //{
    //    get { return ImageKiadasOsztalyra.Enabled; }
    //    set
    //    {
    //        ImageKiadasOsztalyra.Enabled = value;
    //        if (value == false)
    //        {
    //            ImageKiadasOsztalyra.CssClass = "disableditem";
    //        }
    //        else
    //        {
    //            ImageKiadasOsztalyra.CssClass = "";
    //        }
    //    }
    //}
    //public bool KiadasOsztalyraVisible
    //{
    //    get { return ImageKiadasOsztalyra.Visible; }
    //    set { ImageKiadasOsztalyra.Visible = value; }
    //}
    //#endregion

    //#region SkontrobaHelyez Button Public Properties
    //public String SkontrobaHelyezOnClientClick
    //{
    //    get { return ImageSkontrobaHelyez.OnClientClick; }
    //    set { ImageSkontrobaHelyez.OnClientClick = value; }
    //}
    //public bool SkontrobaHelyezEnabled
    //{
    //    get { return ImageSkontrobaHelyez.Enabled; }
    //    set
    //    {
    //        ImageSkontrobaHelyez.Enabled = value;
    //        if (value == false)
    //        {
    //            ImageSkontrobaHelyez.CssClass = "disableditem";
    //        }
    //        else
    //        {
    //            ImageSkontrobaHelyez.CssClass = "";
    //        }
    //    }
    //}
    //public bool SkontrobaHelyezVisible
    //{
    //    get { return ImageSkontrobaHelyez.Visible; }
    //    set { ImageSkontrobaHelyez.Visible = value; }
    //}
    //#endregion

    //#region SkontrobolKivetel Button Public Properties
    //public String SkontrobolKivetelOnClientClick
    //{
    //    get { return ImageSkontrobolKivetel.OnClientClick; }
    //    set { ImageSkontrobolKivetel.OnClientClick = value; }
    //}
    //public bool SkontrobolKivetelEnabled
    //{
    //    get { return ImageSkontrobolKivetel.Enabled; }
    //    set
    //    {
    //        ImageSkontrobolKivetel.Enabled = value;
    //        if (value == false)
    //        {
    //            ImageSkontrobolKivetel.CssClass = "disableditem";
    //        }
    //        else
    //        {
    //            ImageSkontrobolKivetel.CssClass = "";
    //        }
    //    }
    //}
    //public bool SkontrobolKivetelVisible
    //{
    //    get { return ImageSkontrobolKivetel.Visible; }
    //    set { ImageSkontrobolKivetel.Visible = value; }
    //}
    //#endregion


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //lapoz�s eset�n kiv�laszt�s t�rl�se
        switch ((sender as ImageButton).CommandName.ToString())
        {
            case "First":
                if (PageIndex != 0)
                {
                    PageIndex = 0;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Prev":
                if (PageIndex - 1 >= 0)
                {
                    PageIndex = PageIndex - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Next":
                if (PageIndex + 1 < PageCount)
                {
                    PageIndex = PageIndex + 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Last":
                if (PageIndex != PageCount - 1)
                {
                    PageIndex = PageCount - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "IndexChanged":
                if (PagingButtonsClick != null)
                {
                    PagingButtonsClick(this, new EventArgs());
                }
                break;
        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        bool isDisabledIconToSetToHidden = IsDisabledButtonToSetHidden();
        ImageView.HideIfDisabled = isDisabledIconToSetToHidden;
        ImageModify.HideIfDisabled = isDisabledIconToSetToHidden;
        ImageNew.HideIfDisabled = isDisabledIconToSetToHidden;
        ImageInvalidate.HideIfDisabled = isDisabledIconToSetToHidden;
        ImageMap.HideIfDisabled = isDisabledIconToSetToHidden;

        //ImageView.CssClassItemDefault = CssClass_item_default;
        //ImageModify.CssClassItemDefault = CssClass_item_default;
        //ImageNew.CssClassItemDefault = CssClass_item_default;
        //ImageInvalidate.CssClassItemDefault = CssClass_item_default;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //UI.SwapImageToDisabled(ImageNew);
        //UI.SwapImageToDisabled(ImageView);
        //UI.SwapImageToDisabled(ImageModify);
        //UI.SwapImageToDisabled(ImageInvalidate);

        if (Session[Constants.DetailRowCount] == null)
        {
            Session[Constants.DetailRowCount] = RowCountTextBox.Text;
        }

        if (!IsPostBack)
        {
            if (Page.Session[Constants.DetailGridViewScrollable] != null)
                ScrollableCheckBox.Checked = (bool)Page.Session[Constants.DetailGridViewScrollable];

            if (Session[Constants.DetailRowCount] != null)
            {
                RowCountTextBox.Text = Session[Constants.DetailRowCount].ToString();
            }
        }

        //rendez�s eset�n selectedRecordId t�rl�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (eventArgument.Split('$')[0].ToLower() == "sort")
            {
                if (Request.Params["__EVENTTARGET"] != null)
                {
                    string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                    string[] list = eventTarget.Split('$');
                    if (AttachedGridView != null && list[list.Length - 1] == AttachedGridViewId)
                    {
                        ForgetSelectedRecord();
                    }
                }
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Ha t�r�lve lett a gridView a SelectedRecordId mez�t is t�r�lni kell
        if (attachedGridView != null && attachedGridView.Rows.Count == 0)
            ForgetSelectedRecord();
    }

    public event CommandEventHandler ButtonsClick;

    public event EventHandler PagingButtonsClick;

    public virtual void Buttons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    protected void ScrollableCheckBox_CheckedChanged(object sender, EventArgs e)
    {

    }

    public event EventHandler RowCount_Changed;

    protected void RowCountTextBox_TextChanged(object sender, EventArgs e)
    {
        Session[Constants.DetailRowCount] = RowCount;
        if (RowCount_Changed != null)
        {
            RowCount_Changed(sender, e);
        }
    }

    public event EventHandler ErvenyessegFilter_Changed;

    protected void ValidFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ErvenyessegFilter_Changed != null)
        {
            //filter v�ltoz�sa eset�s a selectedRecordId t�rl�se
            ForgetSelectedRecord();
            ErvenyessegFilter_Changed(sender, e);
        }
    }

    /// <summary>
    /// A megadott keres�si objektum �rv�nyess�gi mez�inek be�ll�t�sa
    /// </summary>
    /// <param name="ErvKezd"></param>
    /// <param name="ErvVege"></param>
    public void SetErvenyessegFields(Field ervKezd, Field ervVege)
    {
        if (ValidFilter.SelectedIndex == 0)
        {
            // �rv�nyes
            ervKezd.Value = Query.SQLFunction.getdate;
            ervKezd.Operator = Query.Operators.lessorequal;
            ervKezd.Group = "100";
            ervKezd.GroupOperator = Query.Operators.and;

            ervVege.Value = Query.SQLFunction.getdate;
            ervVege.Operator = Query.Operators.greaterorequal;
            ervVege.Group = "100";
            ervVege.GroupOperator = Query.Operators.and;
        }
        else if (ValidFilter.SelectedIndex == 1)
        {
            // �rv�nytelen
            ervKezd.Value = Query.SQLFunction.getdate;
            ervKezd.Operator = Query.Operators.greaterorequal;
            ervKezd.Group = "100";
            ervKezd.GroupOperator = Query.Operators.or;

            ervVege.Value = Query.SQLFunction.getdate;
            ervVege.Operator = Query.Operators.lessorequal;
            ervVege.Group = "100";
            ervVege.GroupOperator = Query.Operators.or;
        }
        else if (ValidFilter.SelectedIndex == 2)
        {
            // �sszes
            ervKezd.Clear();

            ervVege.Clear();
        }
    }
}
