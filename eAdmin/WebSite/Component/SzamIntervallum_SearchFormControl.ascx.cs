using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_SzamIntervallum_SearchFormControl : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    private bool autoCopy = true;

    public bool AutoCopy
    {
        get { return autoCopy; }
        set { autoCopy = value; }
    }


    private bool moreValueEnabled = true;

    public bool MoreValueEnabled
    {
        get { return moreValueEnabled; }
        set { moreValueEnabled = value; }
    }

    private const string valueSeparator = "+";


    /// <summary>
    /// Handles the Init event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //SetDefault();
        }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Enabled)
        {
            string jsCopy = " var szamIg = $get('" + SzamIg_TextBox.ClientID + "');var szamTol = $get('" + SzamTol_TextBox.ClientID + "'); var copyImage = $get('" + SzamTol_masol_ImageButton.ClientID + "');"
                              + "if(szamTol && szamIg) { if(szamTol.value.indexOf('" + valueSeparator + @"') > -1)
                              { szamIg.value = ''; szamIg.readOnly = true;Sys.UI.DomElement.addCssClass(szamIg,'ReadOnlyWebControl'); if(copyImage) {copyImage.disabled = true;Sys.UI.DomElement.addCssClass(copyImage,'disableditem');}} 
                              else {  szamIg.readOnly = false;Sys.UI.DomElement.removeCssClass(szamIg,'ReadOnlyWebControl');szamIg.value = szamTol.value; if(copyImage) {copyImage.disabled = false;Sys.UI.DomElement.removeCssClass(copyImage,'disableditem');}}};";

            SzamTol_masol_ImageButton.Attributes["onclick"] = jsCopy + "return false;";

            if (autoCopy)
            {
                SzamTol_TextBox.Attributes["onkeyup"] += ";" + jsCopy;
            }
        }

        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (MoreValueEnabled)
        {
            if (FilteredTextBoxExtender1.FilterType == AjaxControlToolkit.FilterTypes.Numbers)
            {
                FilteredTextBoxExtender1.FilterType = AjaxControlToolkit.FilterTypes.Numbers | AjaxControlToolkit.FilterTypes.Custom;
                FilteredTextBoxExtender1.ValidChars = valueSeparator;
            }
            else
            {
                FilteredTextBoxExtender1.ValidChars += valueSeparator;
            }

            if (IsMoreValue)
            {
                Readonly_SzamIg = true;
            }
            else
            {
                Readonly_SzamIg = false;
            }
        }
    }

    private bool IsMoreValue
    {
        get
        {
            string[] values = SzamTol.Split(new String[] { valueSeparator }, StringSplitOptions.RemoveEmptyEntries);
            SzamTol = String.Join(valueSeparator, values);
            return SzamTol.Contains(valueSeparator);
        }
    }

    private string GetSlqInnerString()
    {
        string[] values = SzamTol.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, ".").Split(new String[] { valueSeparator }, StringSplitOptions.RemoveEmptyEntries);
        string sqlString = String.Join(",",values);
        return sqlString;
    }

    private string GetTextFromSqlInnerString(Field SearchField)
    {
        return SearchField.Value.Replace(",", valueSeparator).Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
    }

    /// <summary>
    /// SzamTol �s SzamIg be�ll�t�sa a megadott keres�si mez�nek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        if (!String.IsNullOrEmpty(SzamTol) || !String.IsNullOrEmpty(SzamIg))
        {
            if (MoreValueEnabled && IsMoreValue)
            {
                SearchField.Value = GetSlqInnerString();
                SearchField.Operator = Query.Operators.inner;
            }
            else
            {
                if (String.IsNullOrEmpty(SzamTol))
                {
                    SearchField.Value = SzamIg;
                    SearchField.Operator = Query.Operators.lessorequal;
                }
                else if (String.IsNullOrEmpty(SzamIg))
                {
                    SearchField.Value = SzamTol;
                    SearchField.Operator = Query.Operators.greaterorequal;
                }
                else
                {
                    SearchField.Value = SzamTol;
                    SearchField.ValueTo = SzamIg;
                    SearchField.Operator = Query.Operators.between;
                }
            }
        }
    }

    /// <summary>
    /// TextBoxok be�ll�t�sa a keres�si objektum mez�je alapj�n
    /// </summary>
    /// <param name="SearchField"></param>
    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        if (SearchField.Operator == Query.Operators.inner)
        {
            SzamTol = GetTextFromSqlInnerString(SearchField);
            SzamIg = String.Empty;
        }
        else if (SearchField.Operator == Query.Operators.lessorequal)
        {
            SzamIg = SearchField.Value;
        }
        else if (SearchField.Operator == Query.Operators.greaterorequal)
        {
            SzamTol = SearchField.Value;
        }
        else
        {
            SzamTol = SearchField.Value;
            SzamIg = SearchField.ValueTo;
        }
    }

    /// <summary>
    /// true, ha mindk�t TextBox �res, egy�bk�nt false
    /// </summary>
    /// <returns></returns>
    public bool BothTextBoxesAreEmpty()
    {
        if (String.IsNullOrEmpty(SzamTol) && String.IsNullOrEmpty(SzamIg))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    #region public properties


    public bool EnableNegativeNumbers
    {
        get
        {
            if (ViewState["EnableNegativeNumbers"] != null)
            {
                return (bool)ViewState["EnableNegativeNumbers"];
            }
            else
            {
                return false;
            }
        }

        set
        {
            ViewState["EnableNegativeNumbers"] = value;
            this.SetFilteredTextBox(this.FilterType, value);
        }
    }
    public enum NumberTextBoxFilterTypes
    {
        Numbers,
        NumbersWithDot,
        NumbersWithDecimalSeparator
    }
    public NumberTextBoxFilterTypes FilterType
    {
        get
        {
            if (ViewState["NumberTextBoxFilterType"] != null)
            {
                return (NumberTextBoxFilterTypes)ViewState["NumberTextBoxFilterType"];
            }
            else
            {
                return NumberTextBoxFilterTypes.Numbers;
            }
        }

        set
        {
            ViewState["NumberTextBoxFilterType"] = value;
            this.SetFilteredTextBox(value, this.EnableNegativeNumbers);
        }

    }

    private void SetFilteredTextBox(NumberTextBoxFilterTypes ntbtype, bool enableNegativeNumbers)
    {
        string validCharsSign = EnableNegativeNumbers ? "-" : "";
        string regexpSign = EnableNegativeNumbers ? "-?" : "";
        FilteredTextBoxExtender1.FilterType = AjaxControlToolkit.FilterTypes.Numbers | AjaxControlToolkit.FilterTypes.Custom;
        FilteredTextBoxExtender1.ValidChars = validCharsSign;
        FilteredTextBoxExtender2.FilterType = AjaxControlToolkit.FilterTypes.Numbers | AjaxControlToolkit.FilterTypes.Custom;
        FilteredTextBoxExtender2.ValidChars = validCharsSign;
        switch (ntbtype)
        {
            case NumberTextBoxFilterTypes.Numbers:
                break;
            case NumberTextBoxFilterTypes.NumbersWithDot:
                FilteredTextBoxExtender1.ValidChars += ".";
                FilteredTextBoxExtender2.ValidChars += ".";
                break;
            case NumberTextBoxFilterTypes.NumbersWithDecimalSeparator:
                FilteredTextBoxExtender1.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                FilteredTextBoxExtender2.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                break;
        }
    }

    /// <summary>
    /// Gets or sets the erv kezd.
    /// </summary>
    /// <value>The erv kezd.</value>
    public string SzamTol
    {
        get
        {
            return SzamTol_TextBox.Text;
        }
        set
        {
            SzamTol_TextBox.Text = value;
        }
    }

    /// <summary>
    /// Gets or sets the erv vege.
    /// </summary>
    /// <value>The erv vege.</value>
    public string SzamIg
    {
        get
        {
            return SzamIg_TextBox.Text;
        }
        set
        {
            SzamIg_TextBox.Text = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv kezd].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv kezd]; otherwise, <c>false</c>.</value>
    public bool Enable_SzamTol
    {
        set
        {
            SzamTol_TextBox.Enabled = value;
            FilteredTextBoxExtender1.Enabled = value;
        }
    }

    public bool Readonly_SzamTol
    {
        set
        {
            SzamTol_TextBox.ReadOnly = value;
            FilteredTextBoxExtender1.Enabled = !value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_SzamIg
    {
        set
        {
            SzamIg_TextBox.Enabled = value;
            FilteredTextBoxExtender2.Enabled = value;
            SzamTol_masol_ImageButton.Enabled = value;
            if (!value)
            {
                SzamTol_masol_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_SzamIg
    {
        set
        {
            if (value)
            {
                SzamIg_TextBox.Attributes["readonly"] = "readonly";
                UI.AddCssClass(SzamIg_TextBox, "ReadOnlyWebControl");
            }
            else
            {
                SzamIg_TextBox.Attributes.Remove("readonly");
                UI.RemoveCssClass(SzamIg_TextBox, "ReadOnlyWebControl");
            }
            FilteredTextBoxExtender2.Enabled = !value;
            SzamTol_masol_ImageButton.Enabled = !value;
        }
    }


    /// <summary>
    /// Gets the erv kezd_ text box.
    /// </summary>
    /// <value>The erv kezd_ text box.</value>
    public TextBox SzamTol_TextBox
    {
        get { return SzamTolTextBox; }
    }

    /// <summary>
    /// Gets the erv vege_ text box.
    /// </summary>
    /// <value>The erv vege_ text box.</value>
    public TextBox SzamIg_TextBox
    {
        get { return SzamIgTextBox; }
    }

    public ImageButton ImageButton_Masol
    {
        get { return SzamTol_masol_ImageButton; }
    }
      

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_SzamTol = _Enabled;
            Enable_SzamIg = _Enabled;
        }
    }

    Boolean _Readonly = false;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean ReadOnly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            Readonly_SzamTol = _Readonly;
            Readonly_SzamIg = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;

            if (!_ViewEnabled)
            {
                Enabled = _ViewEnabled;
                SzamTol_TextBox.CssClass = "ViewReadOnlyWebControl";

                SzamIg_TextBox.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;

            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                SzamTol_TextBox.CssClass = "ViewDisabledWebControl";

                SzamIg_TextBox.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    //#region ISelectableUserComponent method implementations

    ///// <summary>
    ///// Gets the component list.
    ///// </summary>
    ///// <returns></returns>
    //public System.Collections.Generic.List<WebControl> GetComponentList()
    //{
    //    System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
    //    componentList.Add(FoszamTol_TextBox);
    //    componentList.Add(FoszamIg_TextBox);

    //    return componentList;
    //}

    //#endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;
            case Query.Operators.inner:
                text = GetTextFromSqlInnerString(field);
                break;

        }

        return text;
    }

    #endregion
}
