using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_TargySzavakTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, IScriptControl, Contentum.eUtility.ISearchComponent
{

    #region events
    public event EventHandler TextChanged;

    public virtual void TextBox_TextChanged(object sender, EventArgs e)
    {
        if (TextChanged != null)
        {
            TextChanged(this, e);
        }
    }
    #endregion events

    #region public properties

    private bool _RefreshCallingWindow = true;

    public bool RefreshCallingWindow
    {
        set { _RefreshCallingWindow = value; }
        get { return _RefreshCallingWindow; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            TargySzavakMegnevezes.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return TargySzavakMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TargySzavakMegnevezes.ReadOnly = value;
            AutoCompleteExtender1.Enabled = !value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "";
                NewImageButton.CssClass = "";
                ResetImageButton.CssClass = "";
            }
        }
        get { return TargySzavakMegnevezes.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { TargySzavakMegnevezes.Text = value; }
        get { return TargySzavakMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return TargySzavakMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string CssClass
    {
        get { return TargySzavakMegnevezes.CssClass; }
        set { TargySzavakMegnevezes.CssClass = value; }
    }

    public String Tipus
    {
        get { return Tipus_HiddenField.Value; }
    }

    public String RegExp
    {
        get { return RegExp_HiddenField.Value; }
    }

    public String AlapertelmezettErtek
    {
        get { return AlapertelmezettErtek_HiddenField.Value; }
    }

    public String ToolTip
    {
        get { return ToolTip_HiddenField.Value; }
    }

    public String ControlTypeSource
    {
        get { return ControlTypeSource_HiddenField.Value; }
    }

    public String ControlTypeDataSource
    {
        get { return ControlTypeDataSource_HiddenField.Value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                TargySzavakMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TargySzavakMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            //if (value == true)
            //{
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                //ViewImageButton.Visible = value;
                ViewImageButton.Visible = true;
                ResetImageButton.Visible = !value;
            //}
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_Reset
    {
        get { return ResetImageButton; }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public EREC_TargySzavak SetTargySzavakTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        EREC_TargySzavak erec_TargySzavak = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_TargySzavak = (EREC_TargySzavak)result.Record;
                Text = erec_TargySzavak.TargySzavak;

                Tipus_HiddenField.Value = erec_TargySzavak.Tipus;
                RegExp_HiddenField.Value = erec_TargySzavak.RegExp;
                AlapertelmezettErtek_HiddenField.Value = erec_TargySzavak.AlapertelmezettErtek;
                ToolTip_HiddenField.Value = erec_TargySzavak.ToolTip;
                ControlTypeSource_HiddenField.Value = erec_TargySzavak.ControlTypeSource;
                ControlTypeDataSource_HiddenField.Value = erec_TargySzavak.ControlTypeDataSource;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }

        return erec_TargySzavak; // hiba eset�n �rt�ke null
    }

    #endregion

    #region AJAX
    private ScriptManager sm;
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }

    private bool customValueEnabled = true;

    public bool CustomValueEnabled
    {
        get { return customValueEnabled; }
        set { customValueEnabled = value; }
    }


    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //String asdf = TargySzavakMegnevezes.Text;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        //ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.Visible = !ViewMode;

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax_eRecord.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);

            AutoCompleteExtender1.ContextKey = felh_Id;

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        if (ajaxEnabled)
        {
            //Manu�lis be�r�s eset�n az id �s t�pus t�rl�se
            string jsClearHiddenField = "$get('" + HiddenField1.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + Tipus_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + RegExp_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + AlapertelmezettErtek_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + ToolTip_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + ControlTypeSource_HiddenField.ClientID + @"').value = '';";
            jsClearHiddenField += "$get('" + ControlTypeDataSource_HiddenField.ClientID + @"').value = '';";

            TextBox.Attributes.Add("onchange", jsClearHiddenField);
        }

        TextBox.TextChanged += this.TextChanged;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("TargySzavakLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + TargySzavakMegnevezes.ClientID
            + "&" + QueryStringVars.TipusHiddenFieldId + "=" + Tipus_HiddenField.ClientID
            + "&" + QueryStringVars.RegExpHiddenFieldId + "=" + RegExp_HiddenField.ClientID
            + "&" + QueryStringVars.AlapertelmezettErtekHiddenFieldId + "=" + AlapertelmezettErtek_HiddenField.ClientID
            + "&" + QueryStringVars.ToolTipHiddenFieldId + "=" + ToolTip_HiddenField.ClientID
            + "&" + QueryStringVars.ControlTypeSourceHiddenFieldId + "=" + ControlTypeSource_HiddenField.ClientID
            + "&" + QueryStringVars.ControlTypeDataSourceHiddenFieldId + "=" + ControlTypeDataSource_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0")
            , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakMegnevezes.ClientID, "", false);

        OnClick_New = JavaScripts.SetOnClientClick("TargySzavakForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + TargySzavakMegnevezes.ClientID
            + "&" + QueryStringVars.TipusHiddenFieldId + "=" + Tipus_HiddenField.ClientID
            + "&" + QueryStringVars.RegExpHiddenFieldId + "=" + RegExp_HiddenField.ClientID
            + "&" + QueryStringVars.AlapertelmezettErtekHiddenFieldId + "=" + AlapertelmezettErtek_HiddenField.ClientID
            + "&" + QueryStringVars.ToolTipHiddenFieldId + "=" + ToolTip_HiddenField.ClientID
            + "&" + QueryStringVars.ControlTypeSourceHiddenFieldId + "=" + ControlTypeSource_HiddenField.ClientID
            + "&" + QueryStringVars.ControlTypeDataSourceHiddenFieldId + "=" + ControlTypeDataSource_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0")
            , Defaults.PopupWidth, Defaults.PopupHeight, TargySzavakMegnevezes.ClientID, "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "TargySzavakForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "var tbox = $get('" + TextBox.ClientID + "');tbox.value = '';$get('" +
            HiddenField1.ClientID + "').value = '';" +
            "$get('" + Tipus_HiddenField.ClientID + "').value = '';" +
            "$get('" + RegExp_HiddenField.ClientID + "').value = '';" +
            "$get('" + AlapertelmezettErtek_HiddenField.ClientID + "').value = '';" +
            "$get('" + ToolTip_HiddenField.ClientID + "').value = '';" +
            "$get('" + ControlTypeSource_HiddenField.ClientID + "').value = '';" +
            "$get('" + ControlTypeDataSource_HiddenField.ClientID + "').value = '';" +
            "$common.tryFireEvent(tbox, 'change');" +
            "return false;";

        #region Javascript Componens l�trehoz�sa

        if (ajaxEnabled)
        {
            //add script reference
            //ScriptManager.RegisterClientScriptInclude(Page, Page.GetType(), "TargySzavak.js", "JavaScripts/TargySzavak.js");

            //create javascript component
            //StringBuilder initBuilder = new StringBuilder();
            //initBuilder.AppendLine("Sys.Application.add_init(function() {");
            //initBuilder.Append("    ");

            //string js = "";
            //js = "$create(Utility.TargySzavakBehavior,{\"AutoCompleteExtenderId\":\"" + this.AutoCompleteExtender1.ClientID + "\","
            //     + "\"id\":\"" + this.TextBox.ClientID + "_TargySzavakBehavior\", \"HiddenFieldId\":\"" + this.HiddenField.ClientID + "\","
            //     + "\"TipusHiddenFieldId\":\"" + this.Tipus_HiddenField.ClientID +"\"},null,null,$get('" + this.TextBox.ClientID + "'));";

            //initBuilder.AppendLine(js);
            //initBuilder.AppendLine("});");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), this.TextBox.ClientID + "_TargySzavakBehaviorInit", initBuilder.ToString(), true);
        }

        #endregion

    }

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TargySzavakMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion


    #region IScriptControl Members

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.TargySzavakBehavior", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldId", this.HiddenField1.ClientID);
        descriptor.AddProperty("TipusHiddenFieldId", this.Tipus_HiddenField.ClientID);
        descriptor.AddProperty("RegExpHiddenFieldId", this.RegExp_HiddenField.ClientID);
        descriptor.AddProperty("AlapertelmezettErtekHiddenFieldId", this.AlapertelmezettErtek_HiddenField.ClientID);
        descriptor.AddProperty("ToolTipHiddenFieldId", this.ToolTip_HiddenField.ClientID);
        descriptor.AddProperty("ControlTypeSourceHiddenFieldId", this.ControlTypeSource_HiddenField.ClientID);
        descriptor.AddProperty("ControlTypeDataSourceHiddenFieldId", this.ControlTypeDataSource_HiddenField.ClientID);
        descriptor.AddProperty("CustomValueEnabled", this.CustomValueEnabled);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/TargySzavak.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
