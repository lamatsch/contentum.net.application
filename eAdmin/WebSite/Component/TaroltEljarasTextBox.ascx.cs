﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_TaroltEljarasTextBox : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    protected void Page_Load(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("TaroltEljarasLovList.aspx",
                     QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                     + "&" + QueryStringVars.TextBoxId + "=" + TaroltEljarasMegnevezes.ClientID,
                     Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return TaroltEljarasMegnevezes; }
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.TextBox.Text;
    }

    #endregion

}