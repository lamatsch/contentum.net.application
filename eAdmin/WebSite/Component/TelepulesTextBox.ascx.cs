using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class Component_TelepulesTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    public bool WatermarkEnabled
    {
        get { return TextBoxWatermarkExtender1.Enabled; }
        set { TextBoxWatermarkExtender1.Enabled = value; }
    }

    public bool ImageVisible
    {
        get { return TelepulesImageButton.Visible; }
        set { TelepulesImageButton.Visible = value; }
    }

    public string Text
    {
        set { TelepulesNev_TextBox.Text = value; }
        get { return TelepulesNev_TextBox.Text; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public string OnClick
    {
        set { TelepulesImageButton.OnClientClick = value; }
        get { return TelepulesImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return TelepulesNev_TextBox; }
    }
        
    public bool Enabled
    {
        set 
        { 
            TelepulesNev_TextBox.Enabled = value;
            TelepulesImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = !value;
            if (!value)
            {
                TelepulesImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                TelepulesImageButton.CssClass = "mrUrlapInputImageButton";
            }
            
        }
        get { return TelepulesNev_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TelepulesNev_TextBox.ReadOnly = value;
            TelepulesImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (value)
            {
                TelepulesImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                TelepulesImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return TelepulesNev_TextBox.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public bool AutoCompleteEnabled
    {
        set { AutoCompleteExtender1.Enabled = value; }
        get { return AutoCompleteExtender1.Enabled; }
    }

    public String AutoCompleteExtenderClientID
    {
        get { return AutoCompleteExtender1.ClientID; }
    }

    public string CssClass
    {
        get { return TelepulesNev_TextBox.CssClass; }
        set
        {
            TelepulesNev_TextBox.CssClass = value;
            TextBoxWatermarkExtender1.WatermarkCssClass = value + " " + TextBoxWatermarkExtender1.WatermarkCssClass;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TelepulesNev_TextBox.CssClass += " ViewReadOnlyWebControl";
                TelepulesImageButton.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TelepulesNev_TextBox.CssClass += " ViewDisabledWebControl";
                TelepulesImageButton.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    private string irszTextBoxClientID = "";

    public string IrszTextBoxClientID
    {
        get { return irszTextBoxClientID; }
        set { irszTextBoxClientID = value; }
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("TelepulesekLovList.aspx","", Defaults.PopupWidth, Defaults.PopupHeight,TelepulesImageButton.ClientID);

        //AutoCompleteExtender1.ServicePath = UI.GetAppSetting("eAdminBusinessServiceUrl") + "KRT_TelepulesekService.asmx";
        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

        // �tt�ve a Page_Loadba, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        //String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        // autentik�ci� ut�n, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        if (!String.IsNullOrEmpty(IrszTextBoxClientID))
        {
            OnClick = JavaScripts.SetOnClientClick("TelepulesekLovList.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + TelepulesNev_TextBox.ClientID
               + "&" + QueryStringVars.IrszTextBoxClientId + "=" + IrszTextBoxClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }
        else
        {
            OnClick = JavaScripts.SetOnClientClick("TelepulesekLovList.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + TelepulesNev_TextBox.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }


    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Telepulesek KRT_Telepulesek = (KRT_Telepulesek)result.Record;
                Text = KRT_Telepulesek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TelepulesNev_TextBox);
        componentList.Add(TelepulesImageButton);

        TelepulesImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
