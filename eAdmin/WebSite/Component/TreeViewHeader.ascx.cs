using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;

public partial class Component_TreeViewHeader : System.Web.UI.UserControl
{
    #region JavaScripts
        private void RegisterPrintControlJavaScript(string cssPath)
    {
        if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "PrintTreeViewScript"))
        {
            string js = "";
            js = @"function PrintContent(ctrl_Id, title, footer)
{
    var DocumentContainer = $get(ctrl_Id);
    if (DocumentContainer)
    {
        var WindowObject = window.open('', 'Nyomtat�s','width=800,height=600,top=100,left=150,location=no,menubar=no,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<html><head>');";
            if (!String.IsNullOrEmpty(cssPath))
            {
                js += @"
        WindowObject.document.writeln('<link href=""" + cssPath + @""" type=""text/css"" rel=""stylesheet"" />');";
            }
            js += @"
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln('<h1 class=""PrintHeader"">' + title + '</h1>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('<p class=""PrintFooter"">' + footer + '</p>');
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
}
";

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "PrintTreeViewScript", js, true);
        }
    }
    #endregion

    #region SelectedRecord properties

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(selectedRecordId.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedRecordId.Value;
            }
        }
        set
        {
            selectedRecordId.Value = value;
        }
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
    }

    private TreeView attachedTreeView = null;

    public TreeView AttachedTreeView
    {
        get { return attachedTreeView; }
        set 
        {
            if (attachedTreeView != null)
            {
                attachedTreeView.SelectedNodeChanged -= attachedTreeView_SelectedNodeChanged;
            }
            attachedTreeView = value;
            attachedTreeView.SelectedNodeChanged += new EventHandler(attachedTreeView_SelectedNodeChanged);
        }
    }

    protected void attachedTreeView_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (attachedTreeView.SelectedValue != null)
            SelectedRecordId = attachedTreeView.SelectedValue.ToString();
        else
            SelectedRecordId = String.Empty;
    }

    public string AttachedTreeViewId
    {
        get 
        {
            if (attachedTreeView != null)
                return attachedTreeView.ID;
            else
                return String.Empty;
        }
    }

    #endregion

    public String HeaderNodeButtonsLabel
    {
        get { return HeaderNodeButtons.Text; }
        set
        {
            HeaderNodeButtons.Text = value;
        }
    }

    public String HeaderChildButtonsLabel
    {
        get { return HeaderChildButtons.Text; }
        set
        {
            HeaderChildButtons.Text = value;
        }
    }

    public String HeaderCustomNodeButtonsLabel
    {
        get { return HeaderCustomNodeButtons.Text; }
        set
        {
            HeaderCustomNodeButtons.Text = value;
        }
    }


    public String HeaderLabel
    {
        get { return Header.Text; }
        set
        {
            Header.Text = value;
            CollapsiblePanelExtender2.CollapsedText = value;
        }
    }

    private Type searchObjectType;
    /// <summary>
    /// A list�ra vonatkoz� keres�si objektum t�pusa
    /// </summary>
    public Type SearchObjectType
    {
        get { return searchObjectType; }
        set 
        { 
            searchObjectType = value;
            SetDefaultSearchTemplate();

        }
    }

    private String customSearchObjectSessionName = "";
    /// <summary>
    /// Ha be van �ll�tva, ezt veszi figyelembe a keres�si objektum vizsg�latakor, nem pedig a SearchObjectType -ot
    /// </summary>
    public String CustomSearchObjectSessionName
    {
        get { return customSearchObjectSessionName; }
        set { customSearchObjectSessionName = value;}
    }

    private bool SetDefaultSearchTemplate()
    {
        return this.SetDefaultSearchTemplate(false);
    }

    private bool SetDefaultSearchTemplate(bool sessionOverWrite)
    {
        if (searchObjectType == null)
            return false;

        string sessionName = String.Empty;
        bool IsSearchObjectInSession = false;
        bool IsCustomSession = false;

        if (!String.IsNullOrEmpty(customSearchObjectSessionName))
        {
            sessionName = customSearchObjectSessionName;
            IsSearchObjectInSession = Search.IsSearchObjectInSession_CustomSessionName(Page, sessionName);
            IsCustomSession = true;
        }
        else if (searchObjectType != null)
        {
            sessionName = searchObjectType.Name;
            IsSearchObjectInSession = Search.IsSearchObjectInSession(Page, searchObjectType);
            IsCustomSession = false;
        }

        if (!String.IsNullOrEmpty(sessionName) && (!IsSearchObjectInSession || sessionOverWrite))
        {
            object defaultSearchObject = Contentum.eUtility.UIMezoObjektumErtekek.GetDeafultSearchObject(Page, sessionName, searchObjectType);

            if (defaultSearchObject != null)
            {
                if (IsCustomSession)
                {
                    Search.SetSearchObject_CustomSessionName(Page, defaultSearchObject, sessionName);
                }
                else
                {
                    Search.SetSearchObject(Page, defaultSearchObject);
                }

                return true;
            }
        }

        return false;
    }

    #region Buttons

    #region Search
    public bool SearchEnabled
    {
        get { return ImageSearch.Enabled; }
        set
        {
            ImageSearch.Enabled = value;
            if (value == false)
            {
                ImageSearch.CssClass = "disableditem";
            }
            else
            {
                ImageSearch.CssClass = "";
            }
        }
    }

    public bool SearchVisible
    {
        get { return ImageSearch.Visible; }
        set { ImageSearch.Visible = value; }
    }

    public String SearchOnClientClick
    {
        get { return ImageSearch.OnClientClick; }
        set { ImageSearch.OnClientClick = value; }
    }
    #endregion

    #region New
    public bool NewEnabled
    {
        get { return ImageNew.Enabled; }
        set
        {
            ImageNew.Enabled = value;
            if (value == false)
            {
                ImageNew.CssClass = "disableditem";
            }
            else
            {
                ImageNew.CssClass = "";
            }
        }
    }

    public bool NewVisible
    {
        get { return ImageNew.Visible; }
        set { ImageNew.Visible = value; }
    }

    public String NewOnClientClick
    {
        get { return ImageNew.OnClientClick; }
        set { ImageNew.OnClientClick = value; }
    }
    
    public string NewToolTip
    {
        get { return ImageNew.ToolTip; }
        set { ImageNew.ToolTip = value; }
    }
    #endregion
     
    #region UgyiratokList CR3206 - Iratt�ri strukt�ra
    public bool UgyiratokListEnabled
    {
        get { return ImageUgyiratokList.Enabled; }
        set
        {
            ImageUgyiratokList.Enabled = value;
            if (value == false)
            {
                ImageUgyiratokList.CssClass = "disableditem";
            }
            else
            {
                ImageUgyiratokList.CssClass = "";
            }
        }
    }

    public bool UgyiratokListVisible
    {
        get { return ImageUgyiratokList.Visible; }
        set { ImageUgyiratokList.Visible = value; }
    }

    public String UgyiratokListOnClientClick
    {
        get { return ImageUgyiratokList.OnClientClick; }
        set { ImageUgyiratokList.OnClientClick = value; }
    }

    public string UgyiratokListToolTip
    {
        get { return ImageUgyiratokList.ToolTip; }
        set { ImageUgyiratokList.ToolTip = value; }
    }
    #endregion


    #region View
    public ImageButton ImageButtonView
    {
        get
        {
            return ImageView;
        }
    }
    public bool ViewEnabled
    {
        get { return ImageView.Enabled; }
        set
        {
            ImageView.Enabled = value;
            if (value == false)
            {
                ImageView.CssClass = "disableditem";
            }
            else
            {
                ImageView.CssClass = "";
            }
        }
    }

    public bool ViewVisible
    {
        get { return ImageView.Visible; }
        set { ImageView.Visible = value; }
    }

    public String ViewOnClientClick
    {
        get { return ImageView.OnClientClick; }
        set { ImageView.OnClientClick = value; }
    }
    public string ViewToolTip
    {
        get { return ImageView.ToolTip; }
        set { ImageView.ToolTip = value; }
    }
    #endregion

    #region Modify
    public bool ModifyEnabled
    {
        get { return ImageModify.Enabled; }
        set
        {
            ImageModify.Enabled = value;
            if (value == false)
            {
                ImageModify.CssClass = "disableditem";
            }
            else
            {
                ImageModify.CssClass = "";
            }
        }
    }

    public bool ModifyVisible
    {
        get { return ImageModify.Visible; }
        set { ImageModify.Visible = value; }
    }

    public String ModifyOnClientClick
    {
        get { return ImageModify.OnClientClick; }
        set { ImageModify.OnClientClick = value; }
    }
    public string ModifyToolTip
    {
        get { return ImageModify.ToolTip; }
        set { ImageModify.ToolTip = value; }
    }
    #endregion

    #region Invalidate
    public bool InvalidateEnabled
    {
        get { return ImageInvalidate.Enabled; }
        set
        {
            ImageInvalidate.Enabled = value;
            if (value == false)
            {
                ImageInvalidate.CssClass = "disableditem";
            }
            else
            {
                ImageInvalidate.CssClass = "";
            }
        }
    }

    public bool InvalidateVisible
    {
        get { return ImageInvalidate.Visible; }
        set { ImageInvalidate.Visible = value; }
    }

    public String InvalidateOnClientClick
    {
        get { return ImageInvalidate.OnClientClick; }
        set { ImageInvalidate.OnClientClick = value; }
    }
    #endregion

    #region Refresh
    public bool RefreshEnabled
    {
        get { return ImageRefresh.Enabled; }
        set
        {
            ImageRefresh.Enabled = value;
            if (value == false)
            {
                ImageRefresh.CssClass = "disableditem";
            }
            else
            {
                ImageRefresh.CssClass = "";
            }
        }

    }
    public bool RefreshVisible
    {
        get { return ImageRefresh.Visible; }
        set { ImageRefresh.Visible = value; }
    }

    public String RefreshOnClientClick
    {
        get { return ImageRefresh.OnClientClick; }
        set { ImageRefresh.OnClientClick = value; }
    }
    public ImageButton RefreshImageButton
    {
        get { return ImageRefresh; }
    }
    #endregion


    #region DefaultFilter
    public bool DefaultFilterEnabled
    {
        get { return ImageDefaultFilter.Enabled; }
        set
        {
            ImageDefaultFilter.Enabled = value;
            if (value == false)
            {
                ImageDefaultFilter.CssClass = "disableditem";
            }
            else
            {
                ImageDefaultFilter.CssClass = "";
            }
        }
    }
    public bool DefaultFilterVisible
    {
        get { return ImageDefaultFilter.Visible; }
        set { ImageDefaultFilter.Visible = value; }
    }

    public String DefaultFilterOnClientClick
    {
        get { return ImageDefaultFilter.OnClientClick; }
        set { ImageDefaultFilter.OnClientClick = value; }
    }
    public ImageButton DefaultFilterImageButton
    {
        get { return ImageDefaultFilter; }
    }
    #endregion

    #region ExpandNode
    public ImageButton ImageButtonExpandNode
    {
        get
        {
            return ImageExpandNode;
        }
    }
    public bool ExpandNodeEnabled
    {
        get { return ImageExpandNode.Enabled; }
        set
        {
            ImageExpandNode.Enabled = value;
            if (value == false)
            {
                ImageExpandNode.CssClass = "disableditem";
            }
            else
            {
                ImageExpandNode.CssClass = "";
            }
        }
    }

    public bool ExpandNodeVisible
    {
        get { return ImageExpandNode.Visible; }
        set { ImageExpandNode.Visible = value; }
    }

    public String ExpandNodeOnClientClick
    {
        get { return ImageExpandNode.OnClientClick; }
        set { ImageExpandNode.OnClientClick = value; }
    }
    public string ExpandNodeToolTip
    {
        get { return ImageExpandNode.ToolTip; }
        set { ImageExpandNode.ToolTip = value; }
    }
    #endregion


    #region SelectNewChild
    public CheckBox CheckBoxSelectNewChild
    {
        get
        {
            return cbSelectNewChild;
        }
    }

    public bool SelectNewChild
    {
        get
        {
            return cbSelectNewChild.Checked;
        }
    }

    public bool SelectNewChildEnabled
    {
        get { return CheckBoxSelectNewChild.Enabled; }
        set
        {
            CheckBoxSelectNewChild.Enabled = value;
            if (value == false)
            {
                cbSelectNewChild.CssClass = "disableditem";
            }
            else
            {
                cbSelectNewChild.CssClass = "";
            }
        }
    }

    public bool SelectNewChildVisible
    {
        get { return cbSelectNewChild.Visible; }
        set { cbSelectNewChild.Visible = value; }
    }

    //public String SelectNewChildOnClientClick
    //{
    //    get { return cbSelectNewChild.OnClientClick; }
    //    set { cbSelectNewChild.OnClientClick = value; }
    //}

    public string SelectNewChildToolTip
    {
        get { return cbSelectNewChild.ToolTip; }
        set { cbSelectNewChild.ToolTip = value; }
    }
    #endregion

    #region NewCustomNode
    public bool NewCustomNodeEnabled
    {
        get { return ImageNewCustomNode.Enabled; }
        set
        {
            ImageNewCustomNode.Enabled = value;
            if (value == false)
            {
                ImageNewCustomNode.CssClass = "disableditem";
            }
            else
            {
                ImageNewCustomNode.CssClass = "";
            }
        }
    }

    public bool NewCustomNodeVisible
    {
        get { return ImageNewCustomNode.Visible; }
        set { ImageNewCustomNode.Visible = value; }
    }

    public String NewCustomNodeOnClientClick
    {
        get { return ImageNewCustomNode.OnClientClick; }
        set { ImageNewCustomNode.OnClientClick = value; }
    }

    public string NewCustomNodeToolTip
    {
        get { return ImageNewCustomNode.ToolTip; }
        set { ImageNewCustomNode.ToolTip = value; }
    }
    #endregion

    #region PrintTree
    public ImageButton ImageButtonPrintTree
    {
        get
        {
            return ImageButton_TreeViewPrint;
        }
    }
    public bool PrintTreeEnabled
    {
        get { return ImageButton_TreeViewPrint.Enabled; }
        set
        {
            ImageButton_TreeViewPrint.Enabled = value;
            if (value == false)
            {
                ImageButton_TreeViewPrint.CssClass = "disableditem";
            }
            else
            {
                ImageButton_TreeViewPrint.CssClass = "";
            }
        }
    }

    public bool PrintTreeVisible
    {
        get { return ImageButton_TreeViewPrint.Visible; }
        set { ImageButton_TreeViewPrint.Visible = value; }
    }

    public String PrintTreeOnClientClick
    {
        get { return ImageButton_TreeViewPrint.OnClientClick; }
        set { ImageButton_TreeViewPrint.OnClientClick = value; }
    }
    public string PrintTreeToolTip
    {
        get { return ImageButton_TreeViewPrint.ToolTip; }
        set { ImageButton_TreeViewPrint.ToolTip = value; }
    }
    #endregion

    public void SetNodeFunctionButtonsVisible(bool value)
    {
        NodeFunctionButtonsPanel.Visible = value;
    }

    public void SetChildFunctionButtonsVisible(bool value)
    {
        ChildFunctionButtonsPanel.Visible = value;
    }

    public void SetFilterFunctionButtonsVisible(bool value)
    {
        FilterFunctionButtonsPanel.Visible = value;
    }

    public void SetCustomNodeFunctionButtonsVisible(bool value)
    {
        CustomNodeFunctionButtonsPanel.Visible = value;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        // DefaultFilter ikon be�ll�t�sa:
        DefaultFilterOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(DefaultFilterImageButton.ClientID);

        string cssPath = Request.ApplicationPath + "/App_Themes/" + Page.Theme + "/StyleSheet.css";
        RegisterPrintControlJavaScript(cssPath);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (eventArgument == EventArgumentConst.refreshMasterList)
            {

                // Default filter gomb lekezel�se:
                string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                if (eventTarget == DefaultFilterImageButton.ClientID)
                {
                    // Default keres�si objektum be�ll�t�sa a sessionbe (vagy session ki�r�t�se)
                    SetDefaultFilter();
                }

            }
            else //rendez�s eset�n selectedRecordId t�rl�se
                if (eventArgument.Split('$')[0].ToLower() == "sort")
                {
                    if (Request.Params["__EVENTTARGET"] != null)
                    {
                        string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                        string[] list = eventTarget.Split('$');
                        if (AttachedTreeView != null && list[list.Length - 1] == AttachedTreeViewId)
                        {
                            ForgetSelectedRecord();
                        }
                    }
                }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {        
        if (searchObjectType == null && String.IsNullOrEmpty(customSearchObjectSessionName))
        {
            IsFilteredLabel.Text = "(???)";
        }
        else
        {
            bool isFiltered = false;
            BaseSearchObject searchObject = null;
            if (!String.IsNullOrEmpty(customSearchObjectSessionName))
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, customSearchObjectSessionName))
                {
                    isFiltered = true;
                    try
                    {
                        searchObject = (BaseSearchObject)Search.GetSearchObject_CustomSessionName(Page, new Object(), customSearchObjectSessionName);
                    }
                    catch (InvalidCastException ex)
                    {
                        Logger.Error("Keres�si objektum kasztol�si hiba: " + ex.ToString());
                    }
                }
                else isFiltered = false;
            }
            else
            {
                if (Search.IsSearchObjectInSession(Page, searchObjectType))
                {
                    isFiltered = true;
                    try
                    {
                        searchObject = (BaseSearchObject)Search.GetSearchObject_CustomSessionName(Page, new Object(), searchObjectType.Name);
                    }
                    catch (InvalidCastException ex)
                    {
                        Logger.Error("Keres�si objektum kasztol�si hiba: " + ex.ToString());
                    }
                }
                else isFiltered = false;
            }

            if (isFiltered)
            {
                if (searchObject != null && !String.IsNullOrEmpty(searchObject.ReadableWhere))
                {
                    IsFilteredLabel.Text = "(" + Resources.List.UI_IsFiltered + ")" + GetReadableWhereFormattedStyle(searchObject.ReadableWhere);
                }
                else
                {
                    IsFilteredLabel.Text = "(" + Resources.List.UI_IsFiltered + ")";
                }
            }
            else
            {
                IsFilteredLabel.Text = "";
            }
        }

        if (attachedTreeView != null)
        {
            string PrintTreeTitle = Header.Text + " " + IsFilteredLabel.Text;
            string felhasznaloNev = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            string PrintTreeFooter = String.Format(Resources.Form.UI_TreeView_PrintFooter, felhasznaloNev, System.DateTime.Now.ToShortDateString());
            ImageButton_TreeViewPrint.OnClientClick = "PrintContent('" + attachedTreeView.ClientID + @"','" + PrintTreeTitle + @"','" + PrintTreeFooter + @"');return false;";
        }

    }

    private const int maxLength = 70;

    private string GetReadableWhereFormattedStyle(string text)
    {
        text = text.TrimEnd(Search.whereDelimeter.ToCharArray());
        int length = text.Length;
        string cutText = text;

        if (length > maxLength)
        {
            cutText = text.Substring(0, maxLength) + "...";
            string[] darabok = text.Split(new string[1]{Search.whereDelimeter} , StringSplitOptions.RemoveEmptyEntries);
            text = String.Empty;
            for(int i = 0; i<darabok.Length; i++)
            {
                if (i % 2 == 1)
                {
                    darabok[i] = "<span class=\"sp_AlternateText\">" + darabok[i] + "</span>";
                }

                text += darabok[i] + Search.whereDelimeter;
            }
            text = text.TrimEnd(Search.whereDelimeter.ToCharArray());
            spSearchPanel.SearchText = text;
            spSearchPanel.PopupControlExtender.TargetControlID = Header.ID;
            spSearchPanel.PopupControlExtender.OffsetY = 15;

            string jsShowPopup = "showSearchPanel();return false;";
            string jsClearShowPopup = "clearShowSearchPanel();return false";

            return "<br/><span onmouseover=\"" + jsShowPopup + "\" onmouseout=\"" + jsClearShowPopup + "\" class=\"searchLabel\">" + cutText + "</span>";
        }
        else
        {
            return "<br/><span class=\"searchLabel\">" + cutText + "</span>";
        }
    }


    // Default keres�si objektum be�ll�t�sa a sessionbe (vagy session ki�r�t�se)
    private void SetDefaultFilter()
    {
        //Default template-ek list�j�nak t�rl�se
        Session.Remove(Constants.SessionNames.DefaultTemplatesList);
        //Default Template bet�lt�se
        bool IsDefaultTemplate = SetDefaultSearchTemplate(true);
        // SearchObjectType �s CustomSearchObjectSessionName alapj�n el kell d�nteni, hogy milyen list�r�l is van sz�
        if (!IsDefaultTemplate)
        {
            if (!String.IsNullOrEmpty(CustomSearchObjectSessionName))
            {
                switch (CustomSearchObjectSessionName)
                {
                    default:
                        {
                            // Default esetben ki�r�tj�k az adott sessionv�ltoz�t:
                            Contentum.eUtility.Search.RemoveSearchObjectFromSession_CustomSessionName(Page, CustomSearchObjectSessionName);
                            break;
                        }
                }
            }
            else if (SearchObjectType != null)
            {

                switch (SearchObjectType.Name)
                {
                    //case (Constants.SearchObjectTypes.EREC_KuldKuldemenyekSearch):
                    //    {
                    //        // obj. lek�r�se default sz�r�ssel:
                    //        EREC_KuldKuldemenyekSearch defaultSearch = (EREC_KuldKuldemenyekSearch)
                    //            Contentum.eUtility.Search.CreateSearchObjectWithDefaultFilter(SearchObjectType, Page);

                    //        // sessionbe ment�s:
                    //        Contentum.eUtility.Search.SetSearchObject(Page, defaultSearch);
                    //        break;
                    //    }
                    default:
                        {
                            // Default esetben ki�r�tj�k az adott sessionv�ltoz�t:
                            Contentum.eUtility.Search.RemoveSearchObjectFromSession(Page, SearchObjectType);
                        }
                        break;
                }
            }
        }
    }


    public event CommandEventHandler NodeFunctionButtonsClick;

    public virtual void Node_FunctionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (NodeFunctionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            NodeFunctionButtonsClick(this, args);
        }
    }

    public event CommandEventHandler ChildFunctionButtonsClick;

    public virtual void Child_FunctionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ChildFunctionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ChildFunctionButtonsClick(this, args);
        }
    }

    public event CommandEventHandler FilterFunctionButtonsClick;

    public virtual void Filter_FunctionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (FilterFunctionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            FilterFunctionButtonsClick(this, args);
        }
    }

    public event CommandEventHandler CustomNodeFunctionButtonsClick;

    public virtual void CustomNode_FunctionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (CustomNodeFunctionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            CustomNodeFunctionButtonsClick(this, args);
        }
    }

}

