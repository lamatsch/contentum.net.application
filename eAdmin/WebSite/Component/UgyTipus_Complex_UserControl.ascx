﻿<%@ Control Language="C#" AutoEventWireup="true" 
    CodeFile="UgyTipus_Complex_UserControl.ascx.cs"
    Inherits="Component_UgyTipus_Complex_UserControl" %>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktddl" %>
<%@ Register Src="~/Component/IrattariTetelszamDropDownList.ascx" TagName="IrattariTetelszamDropDownList" TagPrefix="uc18" %>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>

<div style="width: 100%;" runat="server" id="tableUgyFajta">
    <%--    
        <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:" Font-Bold="true"></asp:Label>
    <br />
    --%>
    <ktddl:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" CssClass="" Width="98%" AutoPostBack="true" />
</div>

<asp:UpdatePanel ID="UpdatePanel_UgyTipus" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="width: 100%;" runat="server" id="tableUgyTipus">

            <asp:Label ID="Label_agazatiJel" runat="server" Text="Ágazati jel:"></asp:Label>
            <br />
            <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%"
                AutoPostBack="true"
                OnSelectedIndexChanged="AgazatiJelek_DropDownList_SelectedIndexChanged" />
            <br />
            <asp:Label ID="labelIrattariTetelszam" runat="server" Text="Irattári tételszám:"></asp:Label>
            <br />
            <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%" AutoPostBack="true" OnSelectedIndexChanged="Ugykor_DropDownList_SelectedIndexChanged">
            </asp:DropDownList>
            <uc18:IrattariTetelszamDropDownList ID="IrattariTetelszam_DropDownList" runat="server" Visible="False"></uc18:IrattariTetelszamDropDownList>
            <br />
            <asp:Label ID="Label27" runat="server" Text="Ügytípus:"></asp:Label>
            <br />
            <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" AutoPostBack="true" Width="97%">
            </asp:DropDownList>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel_UgyTipus" runat="server" DisplayAfter="1">
    <ProgressTemplate>
        <div class="divWaiting">
            <div style="position: relative; top: 50%;">
                <img src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" alt="Feldolgozás folyamatban..." style="position: relative; top: 50%;" />
                <asp:Label ID="Label_progress" runat="server" Text="Feldolgozás folyamatban..."></asp:Label>
            </div>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

<%-- 
    <table style="width: 100%;" runat="server" id="tableUgyFajta">
    <tr runat="server" class="urlapSor_kicsi">
        <td class="mrUrlapCaption_short">
            <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:"></asp:Label>
        </td>
        <td class="mrUrlapMezo">
            <ktddl:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" CssClass="" Width="98%" />
        </td>
    </tr>
</table>

<asp:UpdatePanel ID="UpdatePanel_UgyTipus" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table style="width: 100%;" runat="server" id="tableUgyTipus">
            <tr>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label_agazatiJel" runat="server" Text="Ágazati jel:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%"
                        AutoPostBack="true"
                        OnSelectedIndexChanged="AgazatiJelek_DropDownList_SelectedIndexChanged" />

                </td>
            </tr>
            <tr>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="labelIrattariTetelszam" runat="server" Text="Irattári tételszám:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" Width="97%" AutoPostBack="true" OnSelectedIndexChanged="Ugykor_DropDownList_SelectedIndexChanged">
                    </asp:DropDownList>
                    <uc18:IrattariTetelszamDropDownList ID="IrattariTetelszam_DropDownList" runat="server" Visible="False"></uc18:IrattariTetelszamDropDownList>
                </td>
            </tr>
            <tr>
                <td class="mrUrlapCaption_short">
                    <asp:Label ID="Label27" runat="server" Text="Ügytípus:"></asp:Label>
                </td>
                <td class="mrUrlapMezo">
                    <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" AutoPostBack="true" Width="97%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
--%>