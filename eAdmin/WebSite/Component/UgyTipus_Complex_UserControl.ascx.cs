﻿using Contentum.eBusinessDocuments; 
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class Component_UgyTipus_Complex_UserControl : System.Web.UI.UserControl
{
    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
    private bool UgyTipusMetaDefbol;
    protected void Page_Load(object sender, EventArgs e)
    {
        UgyTipusMetaDefbol = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true);
        if (!Page.IsPostBack)
        {
            Init();
        }
    }


    private void Init()
    {
        if (UgyTipusMetaDefbol)
        {
            tableUgyFajta.Visible = false;
            tableUgyTipus.Visible = true;

            FillAgazatiJelek();
            FillUgykor();
            FillUgyTipus();
        }
        else
        {
            tableUgyFajta.Visible = true;
            tableUgyTipus.Visible = false;

            if (UGY_FAJTAJA_KodtarakDropDownList.DropDownList.Items.Count < 1)
                UGY_FAJTAJA_KodtarakDropDownList.FillDropDownList(kcs_UGY_FAJTAJA, true, EErrorPanel1);
        }
    }

    public void GetBO(out string ugyFajtaId, out string agazatiJelId, out string ugykorId, out string ugyTipusId)
    {
        UgyTipusMetaDefbol = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true);

        ugyFajtaId = null;
        agazatiJelId = null;
        ugykorId = null;
        ugyTipusId = null;
        if (UgyTipusMetaDefbol)
        {
            agazatiJelId = AgazatiJelek_DropDownList.SelectedValue;
            ugykorId = Ugykor_DropDownList.SelectedValue;
            ugyTipusId = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
        }
        else
        {
            ugyFajtaId = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
        }
    }

    public void SetBO(string ugyFajtaId, string agazatiJelId, string ugykorId, string ugyTipusId)
    {
        if (UgyTipusMetaDefbol)
        {
            FillAgazatiJelek();
            if (!string.IsNullOrEmpty(agazatiJelId))
                AgazatiJelek_DropDownList.SelectedValue = agazatiJelId;

            FillUgykor();
            if (!string.IsNullOrEmpty(ugykorId))
                Ugykor_DropDownList.SelectedValue = ugykorId;

            FillUgyTipus();
            if (!string.IsNullOrEmpty(ugyTipusId))
                UgyUgyirat_Ugytipus_DropDownList.SelectedValue = ugyTipusId;
        }
        else
        {
            UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, ugyFajtaId, EErrorPanel1);
        }
    }

    public string GetUgy_Fajtaja()
    {
        return UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
    }
    public void Set_Ugy_Fajtaja(string value)
    {
        UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, value, EErrorPanel1);
    }

    private void FillAgazatiJelek()
    {
        AgazatiJelek_DropDownList.Items.Clear();
        List<ListItem> items = LoadAgazatiJelek();
        if (items != null && items.Count > 0)
            AgazatiJelek_DropDownList.Items.AddRange(items.ToArray());
    }
    private List<ListItem> LoadAgazatiJelek()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_AgazatiJelekService service_agazatiJelek = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();

        EREC_AgazatiJelekSearch search_agJel = new EREC_AgazatiJelekSearch();
        search_agJel.OrderBy = "Kod";

        Result result_agjelGetAll = service_agazatiJelek.GetAllWithIktathat(execParam, search_agJel);
        if (!string.IsNullOrEmpty(result_agjelGetAll.ErrorCode) || result_agjelGetAll.Ds.Tables[0].Rows.Count < 1)
        {
            return null;
        }
        else
        {
            List<ListItem> result = new List<ListItem>();
            foreach (DataRow row in result_agjelGetAll.Ds.Tables[0].Rows)
            {
                string Id = row["Id"].ToString();
                string Nev = row["Nev"].ToString();
                string Kod = row["Kod"].ToString();

                result.Add(new ListItem(Nev, Id));
            }
            return result;
        }
    }

    private void FillUgykor()
    {
        Ugykor_DropDownList.Items.Clear();
        string agazatiJelid = AgazatiJelek_DropDownList.SelectedValue;
        if (string.IsNullOrEmpty(agazatiJelid))
            return;

        List<ListItem> items = LoadUgykor(agazatiJelid);
        if (items != null && items.Count > 0)
            Ugykor_DropDownList.Items.AddRange(items.ToArray());
    }
    private List<ListItem> LoadUgykor(string agazatiJelId)
    {
        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Session);

        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

        search.AgazatiJel_Id.Value = agazatiJelId;
        search.AgazatiJel_Id.Operator = Query.Operators.equals;

        search.OrderBy = "IrattariTetelszam";

        Logger.DebugStart("service.GetAllWithIktathat");

        Result result = service.GetAllWithIktathat(execParam, search);

        Logger.DebugEnd("service.GetAllWithIktathat");

        if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
        {
            return null;
        }

        List<ListItem> resultItems = new List<ListItem>();
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string Id = row["Id"].ToString();
            string Ittsz = row["IrattariTetelszam"].ToString();

            string Nev = row["Nev"].ToString();
            resultItems.Add(new ListItem(Nev, Id));
        }
        return resultItems;
    }

    private void FillUgyTipus()
    {
        UgyUgyirat_Ugytipus_DropDownList.Items.Clear();
        string ugykor = Ugykor_DropDownList.SelectedValue;
        if (string.IsNullOrEmpty(ugykor))
            return;

        List<ListItem> items = LoadUgyTipus(ugykor);
        if (items != null && items.Count > 0)
            UgyUgyirat_Ugytipus_DropDownList.Items.AddRange(items.ToArray());
    }
    private List<ListItem> LoadUgyTipus(string ugykorId)
    {
        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Session);

        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

        search.Ugykor_Id.Value = ugykorId;
        search.Ugykor_Id.Operator = Query.Operators.equals;

        search.Ugytipus.Value = "";
        search.Ugytipus.Operator = Query.Operators.notnull;

        search.EljarasiSzakasz.Operator = Query.Operators.isnull;
        search.Irattipus.Operator = Query.Operators.isnull;

        search.OrderBy = "UgytipusNev";

        Result result = service.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        List<ListItem> resultItems = new List<ListItem>();
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string iratMetaDef_Id = row["Id"].ToString();
            string Ugytipus = row["Ugytipus"].ToString();
            string UgytipusNev = row["UgytipusNev"].ToString();

            resultItems.Add(new ListItem(UgytipusNev, Ugytipus));
        }
        return resultItems;
    }

    protected void AgazatiJelek_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillUgykor();
        FillUgyTipus();
    }

    protected void Ugykor_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillUgyTipus();
    }
}