using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;

public partial class Component_FunkcioGombsor : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Gomb megnyomásakor funkció kezelése.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        switch ((sender as ImageButton).CommandName)
        {
            case "Uj_ugyirat":
                break;
            case "Modositas":
                break;
            case "Szignalas":
                break;
            case "Szignalas_vissza":
                break;
            case "Szkontro_inditas":
                break;
            case "Szkontrobol_vissza":
                break;
            case "Felfuggesztes":
                break;
            case "Felfuggesztesbol_vissza":
                break;
            case "Tovabbitas":
                break;
            case "Sztornozas":
                break;
            case "Ugyintezes_lezarasa":
                break;
            case "Ugyintezes_lezarasa_vissza":
                break;
            case "ugyirat_lezarasa":
                break;
            case "Ugyirat_lezarasa_vissza":
                break;

        }
    }
    /* UgyiratFunkcioGombsor
        Uj_ugyirat Modositas Szignalas Szignalas_vissza Szkontro_inditas
        Szkontrobol_vissza Felfuggesztes Felfuggesztesbol_vissza 
        Tovabbitas Sztornozas Ugyintezes_lezarasa Ugyintezes_lezarasa_vissza 
        Ugyirat_lezarasa Ugyirat_lezarasa_vissza 
    
     * "Uj_ugyirat", "Modositas", "Szignalas", "Szignalas_vissza", "Szkontro_inditas",
        "Szkontrobol_vissza", "Felfuggesztes", "Felfuggesztesbol_vissza", "Tovabbitas",
        "Sztornozas", "Ugyintezes_lezarasa", "Ugyintezes_lezarasa_vissza", "Ugyirat_lezarasa",
        "Ugyirat_lezarasa_vissza";
    */

    /// <summary>
    /// Buttonses the visible.
    /// </summary>
    /// <param name="value"><c>true</c> látszanak a gombok.</param>
    private void ButtonsVisible(Boolean value)
    {
        Uj_ugyirat.Visible = value;
        Modositas.Visible = value;
        Szignalas.Visible = value;
        Szignalas_vissza.Visible = value;
        Szkontro_inditas.Visible = value;
        Szkontrobol_vissza.Visible = value;
        Felfuggesztes.Visible = value;
        Felfuggesztesbol_vissza.Visible = value;
        Tovabbitas.Visible = value;
        Sztornozas.Visible = value;
        Ugyintezes_lezarasa.Visible = value;
        Ugyintezes_lezarasa_vissza.Visible = value;
        Ugyirat_lezarasa.Visible = value;
        Ugyirat_lezarasa_vissza.Visible = value;
    }

    /// <summary>
    /// Gombok engedélyezése
    /// A gombok látszanak de "halványak"
    /// </summary>
    /// <param name="value"><c>true</c> engedélyezettek a gombok</param>
    private void ButtonsEnable(Boolean value)
    {
        Uj_ugyirat.Enabled = value;
        Uj_ugyirat.CssClass = (value ? "highlightit" : "disableditem");
        Modositas.Enabled = value;
        Modositas.CssClass = (value ? "highlightit" : "disableditem");
        Szignalas.Enabled = value;
        Szignalas.CssClass = (value ? "highlightit" : "disableditem");
        Szignalas_vissza.Enabled = value;
        Szignalas_vissza.CssClass = (value ? "highlightit" : "disableditem");
        Szkontro_inditas.Enabled = value;
        Szkontro_inditas.CssClass = (value ? "highlightit" : "disableditem");
        Szkontrobol_vissza.Enabled = value;
        Szkontrobol_vissza.CssClass = (value ? "highlightit" : "disableditem");
        Felfuggesztes.Enabled = value;
        Felfuggesztes.CssClass = (value ? "highlightit" : "disableditem");
        Felfuggesztesbol_vissza.Enabled = value;
        Felfuggesztesbol_vissza.CssClass = (value ? "highlightit" : "disableditem");
        Tovabbitas.Enabled = value;
        Tovabbitas.CssClass = (value ? "highlightit" : "disableditem");
        Sztornozas.Enabled = value;
        Sztornozas.CssClass = (value ? "highlightit" : "disableditem");
        Ugyintezes_lezarasa.Enabled = value;
        Ugyintezes_lezarasa.CssClass = (value ? "highlightit" : "disableditem");
        Ugyintezes_lezarasa_vissza.Enabled = value;
        Ugyintezes_lezarasa_vissza.CssClass = (value ? "highlightit" : "disableditem");
        Ugyirat_lezarasa.Enabled = value;
        Ugyirat_lezarasa.CssClass = (value ? "highlightit" : "disableditem");
        Ugyirat_lezarasa_vissza.Enabled = value;
        Ugyirat_lezarasa_vissza.CssClass = (value ? "highlightit" : "disableditem");
    }

    public void HideButtons()
    {
        this.ButtonsVisible(false);
    }

    public void ShowButtons()
    {
        this.ButtonsVisible(true);
    }

    public void DisableButtons()
    {
        this.ButtonsEnable(false);
    }

    public void EnableButtons()
    {
        this.ButtonsEnable(true);
    }

    #region Uj_ugyirat
    public bool Uj_ugyiratEnabled
    {
        get { return Uj_ugyirat.Enabled; }
        set
        {
            Uj_ugyirat.Enabled = value;
            Uj_ugyirat.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Uj_ugyiratVisible
    {
        get { return Uj_ugyirat.Visible; }
        set
        {
            Uj_ugyirat.Visible = value;
        }
    }
    public String Uj_ugyiratOnClientClick
    {
        get { return Uj_ugyirat.OnClientClick; }
        set { Uj_ugyirat.OnClientClick = value; }
    }
    #endregion

    #region Modositas
    public bool ModositasEnabled
    {
        get { return Modositas.Enabled; }
        set
        {
            Modositas.Enabled = value;
            Modositas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool ModositasVisible
    {
        get { return Modositas.Visible; }
        set
        {
            Modositas.Visible = value;
        }
    }
    public String ModositasOnClientClick
    {
        get { return Modositas.OnClientClick; }
        set { Modositas.OnClientClick = value; }
    }
    #endregion

    #region Szignalas
    public bool SzignalasEnabled
    {
        get { return Szignalas.Enabled; }
        set
        {
            Szignalas.Enabled = value;
            Szignalas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool SzignalasVisible
    {
        get { return Szignalas.Visible; }
        set
        {
            Szignalas.Visible = value;
        }
    }
    public String SzignalasOnClientClick
    {
        get { return Szignalas.OnClientClick; }
        set { SzignalasOnClientClick = value; }
    }
    #endregion

    #region Szignalas_vissza
    public bool Szignalas_visszaEnabled
    {
        get { return Szignalas_vissza.Enabled; }
        set
        {
            Szignalas_vissza.Enabled = value;
            Szignalas_vissza.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Szignalas_visszaVisible
    {
        get { return Szignalas_vissza.Visible; }
        set
        {
            Szignalas_vissza.Visible = value;
        }
    }
    public String Szignalas_visszaOnClientClick
    {
        get { return Szignalas_vissza.OnClientClick; }
        set { Szignalas_visszaOnClientClick = value; }
    }
    #endregion

    #region Szkontro_inditas
    public bool Szkontro_inditasEnabled
    {
        get { return Szkontro_inditas.Enabled; }
        set
        {
            Szkontro_inditas.Enabled = value;
            Szkontro_inditas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Szkontro_inditasVisible
    {
        get { return Szkontro_inditas.Visible; }
        set
        {
            Szkontro_inditas.Visible = value;
        }
    }
    public String Szkontro_inditasOnClientClick
    {
        get { return Szkontro_inditas.OnClientClick; }
        set { Szkontro_inditasOnClientClick = value; }
    }
    #endregion

    #region Szkontrobol_vissza
    public bool Szkontrobol_visszaEnabled
    {
        get { return Szkontrobol_vissza.Enabled; }
        set
        {
            Szkontrobol_vissza.Enabled = value;
            Szkontrobol_vissza.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Szkontrobol_visszaVisible
    {
        get { return Szkontrobol_vissza.Visible; }
        set
        {
            Szkontrobol_vissza.Visible = value;
        }
    }
    public String Szkontrobol_visszaOnClientClick
    {
        get { return Szkontrobol_vissza.OnClientClick; }
        set { Szkontrobol_visszaOnClientClick = value; }
    }
    #endregion

    #region Felfuggesztes
    public bool FelfuggesztesEnabled
    {
        get { return Felfuggesztes.Enabled; }
        set
        {
            Felfuggesztes.Enabled = value;
            Felfuggesztes.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool FelfuggesztesVisible
    {
        get { return Felfuggesztes.Visible; }
        set
        {
            Felfuggesztes.Visible = value;
        }
    }
    public String FelfuggesztesOnClientClick
    {
        get { return Felfuggesztes.OnClientClick; }
        set { FelfuggesztesOnClientClick = value; }
    }
    #endregion

    #region Felfuggesztesbol_vissza
    public bool Felfuggesztesbol_visszaEnabled
    {
        get { return Felfuggesztesbol_vissza.Enabled; }
        set
        {
            Felfuggesztesbol_vissza.Enabled = value;
            Felfuggesztesbol_vissza.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Felfuggesztesbol_visszaVisible
    {
        get { return Felfuggesztesbol_vissza.Visible; }
        set
        {
            Felfuggesztesbol_vissza.Visible = value;
        }
    }
    public String Felfuggesztesbol_visszaOnClientClick
    {
        get { return Felfuggesztesbol_vissza.OnClientClick; }
        set { Felfuggesztesbol_visszaOnClientClick = value; }
    }
    #endregion

    #region Tovabbitas
    public bool TovabbitasEnabled
    {
        get { return Tovabbitas.Enabled; }
        set
        {
            Tovabbitas.Enabled = value;
            Tovabbitas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool TovabbitasVisible
    {
        get { return Tovabbitas.Visible; }
        set
        {
            Tovabbitas.Visible = value;
        }
    }
    public String TovabbitasOnClientClick
    {
        get { return Tovabbitas.OnClientClick; }
        set { TovabbitasOnClientClick = value; }
    }
    #endregion

    #region Sztornozas
    public bool SztornozasEnabled
    {
        get { return Sztornozas.Enabled; }
        set
        {
            Sztornozas.Enabled = value;
            Sztornozas.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool SztornozasVisible
    {
        get { return Sztornozas.Visible; }
        set
        {
            Sztornozas.Visible = value;
        }
    }
    public String SztornozasOnClientClick
    {
        get { return Sztornozas.OnClientClick; }
        set { SztornozasOnClientClick = value; }
    }
    #endregion

    #region Ugyintezes_lezarasa
    public bool Ugyintezes_lezarasaEnabled
    {
        get { return Ugyintezes_lezarasa.Enabled; }
        set
        {
            Ugyintezes_lezarasa.Enabled = value;
            Ugyintezes_lezarasa.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Ugyintezes_lezarasaVisible
    {
        get { return Ugyintezes_lezarasa.Visible; }
        set
        {
            Ugyintezes_lezarasa.Visible = value;
        }
    }
    public String Ugyintezes_lezarasaOnClientClick
    {
        get { return Ugyintezes_lezarasa.OnClientClick; }
        set { Ugyintezes_lezarasaOnClientClick = value; }
    }
    #endregion

    #region Ugyintezes_lezarasa_vissza
    public bool Ugyintezes_lezarasa_visszaEnabled
    {
        get { return Ugyintezes_lezarasa_vissza.Enabled; }
        set
        {
            Ugyintezes_lezarasa_vissza.Enabled = value;
            Ugyintezes_lezarasa_vissza.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Ugyintezes_lezarasa_visszaVisible
    {
        get { return Ugyintezes_lezarasa_vissza.Visible; }
        set
        {
            Ugyintezes_lezarasa_vissza.Visible = value;
        }
    }
    public String Ugyintezes_lezarasa_visszaOnClientClick
    {
        get { return Ugyintezes_lezarasa_vissza.OnClientClick; }
        set { Ugyintezes_lezarasa_visszaOnClientClick = value; }
    }
    #endregion

    #region Ugyirat_lezarasa
    public bool Ugyirat_lezarasaEnabled
    {
        get { return Ugyirat_lezarasa.Enabled; }
        set
        {
            Ugyirat_lezarasa.Enabled = value;
            Ugyirat_lezarasa.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Ugyirat_lezarasaVisible
    {
        get { return Ugyirat_lezarasa.Visible; }
        set
        {
            Ugyirat_lezarasa.Visible = value;
        }
    }
    public String Ugyirat_lezarasaOnClientClick
    {
        get { return Ugyirat_lezarasa.OnClientClick; }
        set { Ugyirat_lezarasaOnClientClick = value; }
    }
    #endregion

    #region Ugyirat_lezarasa_vissza
    public bool Ugyirat_lezarasa_visszaEnabled
    {
        get { return Ugyirat_lezarasa_vissza.Enabled; }
        set
        {
            Ugyirat_lezarasa_vissza.Enabled = value;
            Ugyirat_lezarasa_vissza.CssClass = (value ? "highlightit" : "disableditem");
        }
    }
    public bool Ugyirat_lezarasa_visszaVisible
    {
        get { return Ugyirat_lezarasa_vissza.Visible; }
        set
        {
            Ugyirat_lezarasa_vissza.Visible = value;
        }
    }
    public String Ugyirat_lezarasa_visszaOnClientClick
    {
        get { return Ugyirat_lezarasa_vissza.OnClientClick; }
        set { Ugyirat_lezarasa_visszaOnClientClick = value; }
    }
    #endregion


}
