<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UploadControl.ascx.cs" Inherits="Component_UploadControl" %>
<div class="DisableWrap">
<asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="LovImageButton" 
                 runat="server" 
                 ImageUrl="~/images/hu/lov/kivalaszt1.gif" 
                 onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" 
                 onmouseout="swapByName(this.id,'kivalaszt1.gif')"
                 CssClass="mrUrlapInputImageButton" 
                 AlternateText="Kiválaszt" />
<asp:HiddenField ID="HiddenField1" runat="server" />
</div>

<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>">
</asp:RequiredFieldValidator>
<%--
TODO: nem megy updatepanelen belul!!
<ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>--%>
