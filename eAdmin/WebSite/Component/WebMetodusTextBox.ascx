﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebMetodusTextBox.ascx.cs" Inherits="Component_WebMetodusTextBox" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<div class="DisableWrap">
    <asp:TextBox ID="WebMetodusMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
        CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />
    <asp:HiddenField ID="HiddenField2" runat="server" />

</div>