﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_WebMetodusTextBox : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var getValueById = "var szolgaltatas = document.getElementById('" + HiddenField2.ClientID + "') == undefined ? '' : document.getElementById('" + HiddenField2.ClientID + "').value; ";
        string clickEvent = JavaScripts.SetOnClientClick("WebMetodusLovList.aspx",
                     QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                     + "&" + QueryStringVars.TextBoxId + "=" + WebMetodusMegnevezes.ClientID
                     + "&WebSzervizId='+szolgaltatas+'",
                     Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        OnClick_Lov = getValueById+clickEvent;
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string HiddenFieldId
    {
        get { return HiddenField2.ClientID; }
    }

    public TextBox TextBox
    {
        get { return WebMetodusMegnevezes; }
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.TextBox.Text;
    }

    #endregion

}