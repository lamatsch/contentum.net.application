﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebOldalTextBox.ascx.cs" Inherits="Component_WebOldalTextBox" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<div class="DisableWrap">
    <asp:TextBox ID="WebOldalMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
        CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />

</div>