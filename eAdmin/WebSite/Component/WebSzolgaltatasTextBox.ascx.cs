﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_WebSzolgaltatasTextBox : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    protected void Page_Load(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("WebSzolgaltatasLovList.aspx",
                     QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                     + "&" + QueryStringVars.TextBoxId + "=" + WebSzolgaltatasMegnevezes.ClientID
                     + (string.IsNullOrEmpty(_CloneFieldId) ? "" : "&" + QueryStringVars.CloneFieldId + "=" + _CloneFieldId),
                     Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return WebSzolgaltatasMegnevezes; }
    }

    private string _CloneFieldId;
    public string CloneFieldId
    {
        get { return _CloneFieldId; }
        set { _CloneFieldId = value; }
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.TextBox.Text;
    }

    #endregion

}