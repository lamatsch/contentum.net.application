﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="CsoportTagok_Invalidate_Form.aspx.cs"
    Inherits="CsoportTagok_Invalidate_Form"
    Title="Felhasználó csoporttagság érvénytelenítés" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager_Felhasznalo_Csoport_Invalidate" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader_Felhasznalo_Csoport_Invalidate" runat="server" HeaderTitle="<%$Resources:Form,Felhasznalo_Szerepkor_FormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <div class="alert">
                        <%--<span></span>--%>
                        <strong>Figyelmeztetés !</strong>
                        <br />
                        Hozzáférési jogosultságát felül kell vizsgálni !
                        <br />
                        A felhasználóhoz rendelt tételek elérhetőek az elszámoltatási jegyzőkönyvben.
                        <br />
                        E-mail értesítés kerül kiküldésre az Alkalmazásgazdának és az érintett felhasználónak.
                        <br />
                        <br />
                    </div>
                    <asp:UpdatePanel ID="Felhasznalo_Csoport_Invalidate_UpdatePanel" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="CsoportTagokInvalidateGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                PagerSettings-Visible="false" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <%--                                     <HeaderTemplate>
                                            <asp:ImageButton ID="SelectingRowsImageButton"
                                                runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" Checked='<%# Eval("Checked") %>' AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage"
                                        ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>--%>
                                    <asp:BoundField DataField="Csoport_Jogalany_Nev" HeaderText="Felhasználó" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Csoport_Jogalany_Nev" HeaderStyle-Width="200px" />
                                    <asp:BoundField DataField="Csoport_Nev" HeaderText="Csoport" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Csoport_Nev" HeaderStyle-Width="150px" />
                                    <asp:BoundField DataField="Msg" HeaderText="Ellenőrzések" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Msg" />

                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>

                <uc2:FormFooter ID="FormFooter_Felhasznalo_Csoport_Invalidate" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
