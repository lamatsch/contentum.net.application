using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

public partial class CsoportTagok_Invalidate_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Mode = "";
    private PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        Command = Request.QueryString.Get(CommandName.Command);
        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        FormHeader_Felhasznalo_Csoport_Invalidate.HeaderTitle = Resources.Form.CsoporttagokFormHeaderTitle;

        FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + CommandName.Invalidate);

        if (Mode == CommandName.Invalidate && Command == CommandName.Invalidate)
        {
            string ids = Request.QueryString.Get(QueryStringVars.Id);
            if (string.IsNullOrEmpty(ids))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel);
                return;
            }

            Result result = LoadCsoportTagok(ids.Split(','));
            StringBuilder warnMsg = new StringBuilder();
            if (result != null && result.GetCount > 0)
            {
                DataTable dt = CreateDataTable();
                string msg;
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    if (Ervenytelenites.VanFelhasznalohozKapcsoltUgy(Page, FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel, row["Csoport_Id_Jogalany"].ToString(), out msg))
                    {
                        AddToDataTable(ref dt, row, msg);
                    }
                    else
                    {
                        AddToDataTable(ref dt, row, msg ?? string.Empty);
                    }
                    msg = null;
                }
                Result res = new Result();
                res.Ds = new DataSet();
                res.Ds.Tables.Add(dt);
                UI.GridViewFill(CsoportTagokInvalidateGridView, res, FormHeader_Felhasznalo_Csoport_Invalidate, FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel, Felhasznalo_Csoport_Invalidate_UpdatePanel);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter_Felhasznalo_Csoport_Invalidate.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(Footer_InvalidateButtonsClick);
        FormHeader_Felhasznalo_Csoport_Invalidate.ModeText = Resources.Form.FormHeaderInvalidate;
    }

    private void Footer_InvalidateButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Modify || e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, "CsoportTagokInvalidate"))
            {
                string[] deletableItemsList = UI.GetCheckedCheckBoxIdsInGridView(CsoportTagokInvalidateGridView);

                if (deletableItemsList == null || deletableItemsList.Length < 1)
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel);
                    return;
                }

                KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                ExecParam[] execParams = new ExecParam[deletableItemsList.Length];
                for (int i = 0; i < deletableItemsList.Length; i++)
                {
                    execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                    execParams[i].Record_Id = deletableItemsList[i];
                }
                Result result = service.MultiInvalidate(execParams);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel, result);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    #region SERVICE
    /// <summary>
    /// LoadCsoportTagok
    /// </summary>
    /// <param name="ids"></param>
    private Result LoadCsoportTagok(IEnumerable<string> ids)
    {
        KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch src = new KRT_CsoportTagokSearch();
        src.Id.In(ids);

        if (string.IsNullOrEmpty(src.Id.Value))
            return new Result();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAllWithExtension(execParam, src);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader_Felhasznalo_Csoport_Invalidate.ErrorPanel, result);
            return null;
        }
        return result;
    }
    #endregion

    private DataTable CreateDataTable()
    {
        DataTable dt = new DataTable();

        #region COLUMNS
        DataColumn column1 = new DataColumn();
        column1.DataType = System.Type.GetType("System.Guid");
        column1.ColumnName = "Id";
        dt.Columns.Add(column1);

        DataColumn column2 = new DataColumn();
        column2.DataType = System.Type.GetType("System.Guid");
        column2.ColumnName = "Csoport_Id";
        dt.Columns.Add(column2);

        DataColumn column3 = new DataColumn();
        column3.DataType = System.Type.GetType("System.Guid");
        column3.ColumnName = "Csoport_Id_Jogalany";
        dt.Columns.Add(column3);

        DataColumn column3b = new DataColumn();
        column3b.DataType = System.Type.GetType("System.String");
        column3b.ColumnName = "Csoport_Nev";
        dt.Columns.Add(column3b);

        DataColumn column4 = new DataColumn();
        column4.DataType = System.Type.GetType("System.String");
        column4.ColumnName = "Csoport_Jogalany_Nev";
        dt.Columns.Add(column4);

        DataColumn column6 = new DataColumn();
        column6.DataType = System.Type.GetType("System.Boolean");
        column6.ColumnName = "Checked";
        dt.Columns.Add(column6);

        DataColumn column5 = new DataColumn();
        column5.DataType = System.Type.GetType("System.String");
        column5.ColumnName = "Msg";
        dt.Columns.Add(column5);
        #endregion
        return dt;
    }
    private void AddToDataTable(ref DataTable dataTable, DataRow row, string msg)
    {
        DataRow dr = dataTable.NewRow();

        dr["Id"] = row["Id"]; ;
        dr["Csoport_Id"] = row["Csoport_Id"]; ;
        dr["Csoport_Id_Jogalany"] = row["Csoport_Id_Jogalany"]; ;
        dr["Csoport_Nev"] = row["Csoport_Nev"]; ;
        dr["Csoport_Jogalany_Nev"] = row["Csoport_Jogalany_Nev"];
        dr["Checked"] = "true";
        if (!string.IsNullOrEmpty(msg))
            dr["Msg"] = msg;

        dataTable.Rows.Add(dr);
    }
}
