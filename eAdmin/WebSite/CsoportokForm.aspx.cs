using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class CsoportokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcsKod_CSOPORTTIPUS = "CSOPORTTIPUS";

    //tipus dropdown lista felt�lt�s, dolgoz� kiszed�se
    private void FillDropDrownList()
    {
        Tipus_KodtarakDropDownList.FillDropDownList(kcsKod_CSOPORTTIPUS, FormHeader1.ErrorPanel);
        foreach (ListItem item in Tipus_KodtarakDropDownList.DropDownList.Items)
        {
            if (item.Value == KodTarak.CSOPORTTIPUS.Dolgozo)
            {
                Tipus_KodtarakDropDownList.DropDownList.Items.Remove(item);
                break;
            }
        }
    }

    private void FillAndSetSelectedValue(string selectedValue)
    {
        Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_CSOPORTTIPUS
            , selectedValue, FormHeader1.ErrorPanel);
        //if (selectedValue != KodTarak.CSOPORTTIPUS.Dolgozo)
        //{
        //    foreach (ListItem item in Tipus_KodtarakDropDownList.DropDownList.Items)
        //    {
        //        if (item.Value == KodTarak.CSOPORTTIPUS.Dolgozo)
        //        {
        //            Tipus_KodtarakDropDownList.DropDownList.Items.Remove(item);
        //            break;
        //        }
        //    }
        //}
        //else
        //{
        //    Tipus_KodtarakDropDownList.Enabled = false;
        //}

        //Tipus_KodtarakDropDownList.Enabled = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                System_tr.Visible = false;
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Csoport" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Csoportok krt_csoportok = (KRT_Csoportok)result.Record;
                    LoadComponentsFromBusinessObject(krt_csoportok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                FillDropDrownList();
            }
        }
        // BLG_619
        tr_CsoportKod.Visible = (Tipus_KodtarakDropDownList.SelectedValue == KodTarak.CSOPORTTIPUS.Szervezet);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.CsoportokFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        // BLG_619
        Tipus_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        Tipus_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(Tipus_KodtarakDropDownList_SelectedIndexChanged);


        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // BLG_619
    private void Tipus_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        tr_CsoportKod.Visible = (Tipus_KodtarakDropDownList.SelectedValue == KodTarak.CSOPORTTIPUS.Szervezet);
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Csoportok krt_csoportok)
    {
        Nev.Text = krt_csoportok.Nev;

        FillAndSetSelectedValue(krt_csoportok.Tipus);
        // BLG_619
        CsoportKod_TextBox.Text = krt_csoportok.Kod;

        ErtesitesEmail_TextBox.Text = krt_csoportok.ErtesitesEmail;

        //Jogalany.Checked = krt_csoportok.Jogalany == "1" ? true : false;
        //Jogalany.Enabled = false;

        System_CheckBox.Checked = krt_csoportok.System == "1" ? true : false;
        System_CheckBox.Enabled = false;

        CheckBoxOroklesMod.Enabled = krt_csoportok.Tipus == "1" ? false : true;
        CheckBoxOroklesMod.Checked = krt_csoportok.JogosultsagOroklesMod == "1" ? true : false;

        ErvenyessegCalendarControl1.ErvKezd = krt_csoportok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_csoportok.ErvVege;

        FormHeader1.Record_Ver = krt_csoportok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_csoportok.Base);

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            Tipus_KodtarakDropDownList.ReadOnly = true;
        }

        if (Command == CommandName.View)
        {
            // readonly-ra, vagy disabled-re �ll�tunk minden olyan komponenst,
            // amit k�l�n 'View' m�dban kell �ll�tani
            Nev.ReadOnly = true;
            // BLG_619
            CsoportKod_TextBox.ReadOnly = true;

            ErtesitesEmail_TextBox.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
            CheckBoxOroklesMod.Enabled = false;
        }
    }

    // form --> business object
    private KRT_Csoportok GetBusinessObjectFromComponents()
    {
        KRT_Csoportok krt_csoportok = new KRT_Csoportok();
        krt_csoportok.Updated.SetValueAll(false);
        krt_csoportok.Base.Updated.SetValueAll(false);

        krt_csoportok.Nev = Nev.Text;
        krt_csoportok.Updated.Nev = pageView.GetUpdatedByView(Nev);

        krt_csoportok.Tipus = Tipus_KodtarakDropDownList.SelectedValue;
        krt_csoportok.Updated.Tipus = pageView.GetUpdatedByView(Tipus_KodtarakDropDownList);

        // BLG_619
        krt_csoportok.Kod = CsoportKod_TextBox.Text;
        krt_csoportok.Updated.Kod = pageView.GetUpdatedByView(CsoportKod_TextBox);

        krt_csoportok.ErtesitesEmail = ErtesitesEmail_TextBox.Text;
        krt_csoportok.Updated.ErtesitesEmail = pageView.GetUpdatedByView(ErtesitesEmail_TextBox);

        krt_csoportok.JogosultsagOroklesMod = CheckBoxOroklesMod.Checked ? "1" : "0";
        krt_csoportok.Updated.JogosultsagOroklesMod = pageView.GetUpdatedByView(CheckBoxOroklesMod);

        //krt_csoportok.Jogalany = Jogalany.Checked ? "1" : "0";
        //krt_csoportok.Updated.Jogalany = pageView.GetUpdatedByView(Jogalany);

        if (Command == CommandName.New)
        {
            krt_csoportok.System = "0";
            krt_csoportok.Updated.System = pageView.GetUpdatedByView(System_CheckBox);
        }        

        //krt_csoportok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_csoportok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_csoportok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_csoportok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_csoportok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_csoportok.Base.Ver = FormHeader1.Record_Ver;
        krt_csoportok.Base.Updated.Ver = true;

        return krt_csoportok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(Tipus_KodtarakDropDownList);
            // BLG_619
            compSelector.Add_ComponentOnClick(CsoportKod_TextBox);

            compSelector.Add_ComponentOnClick(ErtesitesEmail_TextBox);
            //compSelector.Add_ComponentOnClick(Jogalany);
            compSelector.Add_ComponentOnClick(System_CheckBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }
    
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Csoport" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                            KRT_Csoportok krt_csoportok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_csoportok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                                KRT_Csoportok krt_csoportok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_csoportok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
