﻿using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class CsoportokList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";
    readonly string jsNincsKivalasztva = "alert('" + Resources.List.UI_NoSelectedItem + "');return false;";


    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }

    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        bool modosithato = false;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            CheckBox cbSystem = (CheckBox)selectedRow.FindControl("cbSystem");
            if (cbSystem != null)
            {
                modosithato = !cbSystem.Checked;
            }
        }
        return modosithato;
    }

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CsoportokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        CsoporttagokSubListHeader.RowCount_Changed += new EventHandler(CsoporttagokSubListHeader_RowCount_Changed);
        FelhasznaloSzerepkorSubListHeader.RowCount_Changed += new EventHandler(FelhasznaloSzerepkorSubListHeader_RowCount_Changed);

        CsoporttagokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsoporttagokSubListHeader_ErvenyessegFilter_Changed);
        FelhasznaloSzerepkorSubListHeader.ErvenyessegFilter_Changed += new EventHandler(FelhasznaloSzerepkorSubListHeader_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.CsoportokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_CsoportokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.CsoportHierarchiaVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("CsoportokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("CsoportokForm.aspx", QueryStringVars.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(CsoportokGridView.ClientID, "check", "cbSystem", false);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(CsoportokGridView.ClientID);

        ListHeader1.CsoportHierarchiaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(CsoportokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(CsoportokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(CsoportokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, "", true);


        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = CsoportokGridView;

        CsoporttagokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoporttagokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //BLG_2219
        //CsoporttagokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //CsoporttagokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsoporttagokGridView.ClientID);
        CsoporttagokSubListHeader.ButtonsClick += new CommandEventHandler(CsoporttagokSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        CsoporttagokSubListHeader.AttachedGridView = CsoporttagokGridView;

        FelhasznaloSzerepkorSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznaloSzerepkorSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznaloSzerepkorSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //FelhasznaloSzerepkorSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FelhasznaloSzerepkorGridView.ClientID);
        FelhasznaloSzerepkorSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(FelhasznaloSzerepkorGridView.ClientID, "check", "cbMegbizasbanKapott", false);
        FelhasznaloSzerepkorSubListHeader.ButtonsClick += new CommandEventHandler(FelhasznaloSzerepkorSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        FelhasznaloSzerepkorSubListHeader.AttachedGridView = FelhasznaloSzerepkorGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoporttagokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznaloSzerepkorGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_CsoportokSearch());

        if (!IsPostBack) CsoportokGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        #region BLG_2219
        //A Csoporttagok fülön inaktiváltam az ’Új’ és a ’Megtekintés’ gombokat.Csak a ’Módosítás’ maradt meg. 
        CsoporttagokSubListHeader.NewEnabled = CsoporttagokSubListHeader.NewVisible = false;
        CsoporttagokSubListHeader.ViewEnabled = CsoporttagokSubListHeader.ViewVisible = false;
        #endregion


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Csoport" + CommandName.Lock);

        CsoporttagokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "CsoportTagokList");
        FelhasznaloSzerepkorPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorList");

        CsoporttagokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + CommandName.New);
        CsoporttagokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + CommandName.View);
        CsoporttagokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + CommandName.Modify);
        CsoporttagokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + CommandName.Invalidate);

        FelhasznaloSzerepkorSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New);
        FelhasznaloSzerepkorSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.View);
        FelhasznaloSzerepkorSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        FelhasznaloSzerepkorSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate);


        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(CsoportokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

    }



    #endregion

    #region Master List

    protected void CsoportokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsoportokGridView", ViewState, "KRT_Csoportok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoportokGridView", ViewState);

        CsoportokGridViewBind(sortExpression, sortDirection);
    }

    protected void CsoportokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_CsoportokSearch search = (KRT_CsoportokSearch)Search.GetSearchObject(Page, new KRT_CsoportokSearch());
        search.OrderBy = Search.GetOrderBy("CsoportokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithFK(ExecParam, search);

        UI.GridViewFill(CsoportokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void CsoportokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbSystem", "System");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }


    protected void CsoportokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, CsoportokCPE);
        ListHeader1.RefreshPagerLabel();

        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        CsoportokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, CsoportokCPE);
        CsoportokGridViewBind();
    }

    protected void CsoportokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsoportokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            CsoportokGridView_SelectRowCommand(id);
        }
    }

    private void CsoportokGridView_SelectRowCommand(string csoportId)
    {
        string id = csoportId;
        if (!String.IsNullOrEmpty(id))
        {
            ActiveTabRefresh(TabContainer1, id);

            //RefreshOnClientClicksByMasterListSelectedRow(id);

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modosíthatóság ellenõrzése
            bool modosithato = getModosithatosag(CsoportokGridView);
            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("CsoportokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("CsoportokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);

            string tableName = "Krt_Csoportok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, CsoportokUpdatePanel.ClientID);

            #region BLG_2219
            //LZS
            //A MasterGrid kiválasztáskor lefutó függvényben módosítottam a partnerhierarchia fülén lévő  ’Módosítás’ gomb JS hívását, és kiszedtem az ’Új’ függvényét.

            CsoporttagokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
                , "Command=" + CommandName.Modify + "&" + QueryStringVars.SzuloCsoportId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoporttagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            //CsoporttagokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
            //    , "Command=" + CommandName.New + "&" + QueryStringVars.SzuloCsoportId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, CsoporttagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            #endregion

            FelhasznaloSzerepkorSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_szerepkor_Form.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.CsoportId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznaloSzerepkorGridView.ClientID, EventArgumentConst.refreshDetailList);

            ListHeader1.CsoportHierarchiaOnClientClick = JavaScripts.SetOnClientClick("CsoportHierarchiaPrintForm.aspx"
                , "PrintMode=1&" + QueryStringVars.Id + "=" + id
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, CsoportokUpdatePanel.ClientID);
        }
    }

    protected void CsoportokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    CsoportokGridViewBind();
                    CsoportokGridView_SelectRowCommand(UI.GetGridViewSelectedRecordId(CsoportokGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedCsoportok();
            CsoportokGridViewBind();

            //BUG_7078
            //Refresh mater list
            JavaScripts.SetOnClientClickRefreshMasterList(CsoportokGridView.ClientID);
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedCsoportRecords();
                CsoportokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedCsoportRecords();
                CsoportokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedCsoportok();
                break;
        }
    }

    private void LockSelectedCsoportRecords()
    {
        LockManager.LockSelectedGridViewRecords(CsoportokGridView, "KRT_Csoportok"
            , "CsoportLock", "CsoportForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedCsoportRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(CsoportokGridView, "KRT_Csoportok"
            , "CsoportLock", "CsoportForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Törli a CsoportokGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedCsoportok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "CsoportInvalidate"))
        {
            List<string[]> deletableItemsList = ui.GetGridViewSelectedRowsWithIndex(CsoportokGridView, EErrorPanel1, ErrorUpdatePanel);

            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(CsoportokGridView, Int32.Parse(deletableItemsList[i][1]));
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i][0];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a CsoportokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedCsoportok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(CsoportokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Csoportok");
        }
    }

    protected void CsoportokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsoportokGridViewBind(e.SortExpression, UI.GetSortToGridView("CsoportokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }


    #endregion

    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(CsoportokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (CsoportokGridView.SelectedIndex == -1)
        {
            return;
        }
        string csoportId = UI.GetGridViewSelectedRecordId(CsoportokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, csoportId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string csoportId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsoporttagokGridViewBind(csoportId);
                CsoporttagokPanel.Visible = true;
                break;
            case 1:
                FelhasznaloSzerepkorGridViewBind(csoportId);
                FelhasznaloSzerepkorPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(CsoporttagokGridView);
                break;
            case 1:
                ui.GridViewClear(FelhasznaloSzerepkorGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsoporttagokSubListHeader.RowCount = RowCount;
        FelhasznaloSzerepkorSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(CsoporttagokTabPanel))
        {
            CsoporttagokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoporttagokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(FelhasznaloSzerepkorTabPanel))
        {
            FelhasznaloSzerepkorGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(FelhasznaloSzerepkorGridView));
        }
    }

    #endregion

    #region Csoporttagok Detail

    private void CsoporttagokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedCsoporttagok();
            CallInvalidateButtonCommand();
            CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
    }

    private void CsoporttagokGridViewBind(string csoportId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsoporttagokGridView", ViewState, "KRT_Csoportok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoporttagokGridView", ViewState);

        CsoporttagokGridViewBind(csoportId, sortExpression, sortDirection);
    }

    private void CsoporttagokGridViewBind(string csoportId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(csoportId))
        {
            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Csoportok szuloCsoport = new KRT_Csoportok();
            szuloCsoport.Id = csoportId;

            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
            search.OrderBy = Search.GetOrderBy("CsoporttagokGridView", ViewState, SortExpression, SortDirection);

            CsoporttagokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllBySzuloCsoport(ExecParam, szuloCsoport, search);

            UI.GridViewFill(CsoporttagokGridView, res, CsoporttagokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

            ui.SetClientScriptToGridViewSelectDeSelectButton(CsoporttagokGridView);
        }
        else
        {
            ui.GridViewClear(CsoporttagokGridView);
        }
    }

    private void CsoporttagokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    }

    protected void CsoporttagokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(CsoporttagokTabPanel))
                    //{
                    //    CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
                    //}
                    ActiveTabRefreshDetailList(CsoporttagokTabPanel);
                    break;
            }
        }
    }

    [Obsolete]
    /// <summary>
    /// Törli a CsoporttagokGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedCsoporttagok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "CsoportTagokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsoporttagokGridView, EErrorPanel1, ErrorUpdatePanel);

            KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void CsoporttagokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsoporttagokGridView, selectedRowNumber, "check");
        }
    }

    private void CsoporttagokGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //LZS
            //A DetailGrid módosításakor lefutó függvényből kivettem az ’Új’ és a ’Módosítás’ gomb JS hívását, mivel az új működés során a hívás nem függ majd a kiválasztott detail sortól, hanem mindenképpen az új Listbox-os módosító ablak fog feljönni.

            //CsoporttagokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
            //   , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
            //   , Defaults.PopupWidth, Defaults.PopupHeight, CsoporttagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //CsoporttagokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, CsoporttagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void CsoporttagokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsoporttagokGridView.PageIndex;

        CsoporttagokGridView.PageIndex = CsoporttagokSubListHeader.PageIndex;

        if (prev_PageIndex != CsoporttagokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, CsoporttagokCPE);
            CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(CsoporttagokSubListHeader.Scrollable, CsoporttagokCPE);
        }

        CsoporttagokSubListHeader.PageCount = CsoporttagokGridView.PageCount;
        CsoporttagokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsoporttagokGridView);
    }

    void CsoporttagokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsoporttagokSubListHeader.RowCount);
        CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    }

    protected void CsoporttagokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsoporttagokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView)
            , e.SortExpression, UI.GetSortToGridView("CsoporttagokGridView", ViewState, e.SortExpression));
    }

    #endregion



    #region FelhasznaloSzerepkor Detail

    private void FelhasznaloSzerepkorSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_FelhasznaloSzerepkor();
            FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
    }

    protected void FelhasznaloSzerepkorGridViewBind(string csoportId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FelhasznaloSzerepkorGridView", ViewState, "Felhasznalo_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FelhasznaloSzerepkorGridView", ViewState);

        FelhasznaloSzerepkorGridViewBind(csoportId, sortExpression, sortDirection);
    }

    protected void FelhasznaloSzerepkorGridViewBind(string csoportId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(csoportId))
        {
            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_Csoportok csoport = new KRT_Csoportok();
            csoport.Id = csoportId;

            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();

            search.OrderBy = Search.GetOrderBy("FelhasznaloSzerepkorGridView", ViewState, SortExpression, SortDirection);

            FelhasznaloSzerepkorSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByCsoport(execParam, csoport, search);

            UI.GridViewFill(FelhasznaloSzerepkorGridView, res, FelhasznaloSzerepkorSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznaloSzerepkorGridView);
        }
        else
        {
            ui.GridViewClear(FelhasznaloSzerepkorGridView);
        }
    }

    private void FelhasznaloSzerepkorSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    }


    protected void FelhasznaloSzerepkorUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(FelhasznaloSzerepkorTabPanel))
                    //{
                    //    FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
                    //}
                    ActiveTabRefreshDetailList(FelhasznaloSzerepkorTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// Törli a FelhasznaloSzerepkorGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelected_FelhasznaloSzerepkor()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FelhasznaloSzerepkorGridView, EErrorPanel1, ErrorUpdatePanel);

            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = !UI.isGridViewCheckBoxChecked(FelhasznaloSzerepkorGridView, "cbMegbizasbanKapott", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            //Result result = service.MultiInvalidate(execParams.ToArray());
            Result result = service.MultiInvalidateWithMegbizasControl(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void FelhasznaloSzerepkorGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FelhasznaloSzerepkorGridView, selectedRowNumber, "check");
        }
    }

    private void FelhasznaloSzerepkorGridView_RefreshOnClientClicks(string felhasznalo_szerepkor_id)
    {
        string id = felhasznalo_szerepkor_id;
        if (!String.IsNullOrEmpty(id))
        {
            FelhasznaloSzerepkorSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznaloSzerepkorUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //FelhasznaloSzerepkorSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznaloSzerepkorUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            bool modosithato = !UI.isGridViewCheckBoxChecked(FelhasznaloSzerepkorGridView, "cbMegbizasbanKapott", id);

            if (modosithato)
            {
                FelhasznaloSzerepkorSubListHeader.ModifyEnabled = true;
                FelhasznaloSzerepkorSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznaloSzerepkorUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                FelhasznaloSzerepkorSubListHeader.ModifyEnabled = false;// nem engedjük meg
            }
        }
    }

    protected void FelhasznaloSzerepkorGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FelhasznaloSzerepkorGridView.PageIndex;

        FelhasznaloSzerepkorGridView.PageIndex = FelhasznaloSzerepkorSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != FelhasznaloSzerepkorGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, FelhasznaloSzerepkorCPE);
            FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(FelhasznaloSzerepkorSubListHeader.Scrollable, FelhasznaloSzerepkorCPE);
        }
        FelhasznaloSzerepkorSubListHeader.PageCount = FelhasznaloSzerepkorGridView.PageCount;
        FelhasznaloSzerepkorSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(FelhasznaloSzerepkorGridView);
    }

    void FelhasznaloSzerepkorSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(FelhasznaloSzerepkorSubListHeader.RowCount);
        FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    }

    protected void FelhasznaloSzerepkorGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FelhasznaloSzerepkorGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView)
            , e.SortExpression, UI.GetSortToGridView("FelhasznaloSzerepkorGridView", ViewState, e.SortExpression));
    }

    #endregion

    private void CallInvalidateButtonCommand()
    {
        List<string> selectedItems = ui.GetGridViewSelectedRows(CsoporttagokGridView, EErrorPanel1, ErrorUpdatePanel);
        if (selectedItems == null || selectedItems.Count < 1)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedCsoporttagokNincsKivalasztva", jsNincsKivalasztva, true);
            return;
        }
        Dictionary<string, string> unique = new Dictionary<string, string>();
        foreach (string item in selectedItems)
        {
            if (!unique.ContainsKey(item))
                unique.Add(item, item);
        }
        System.Text.StringBuilder stringSelectedCsoporttag = new System.Text.StringBuilder();

        string SelectedCsoportRecordId = UI.GetGridViewSelectedRecordId(CsoportokGridView);

        foreach (string s in unique.Values)
        {
            stringSelectedCsoporttag.Append(s + ",");
        }
        stringSelectedCsoporttag.Remove(stringSelectedCsoporttag.Length - 1, 1);
        string queryString = string.Format("{0}={1}&{2}={3}&{4}={5}&{6}={7}", CommandName.Command, CommandName.Invalidate, QueryStringVars.Mode, CommandName.Invalidate, QueryStringVars.Id, stringSelectedCsoporttag.ToString(), "Csop", SelectedCsoportRecordId);

        string js = JavaScripts.SetOnClientClickWithTimeout("CsoportTagok_Invalidate_Form.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, CsoporttagokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CsoportTagok_Invalidate_Form", js, true);
    }
}
