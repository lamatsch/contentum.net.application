<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CsoporttagokForm.aspx.cs" Inherits="CsoporttagokForm" Title="Untitled Page" %>


<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="Csoport:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="Csoport_CsoportTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tagRow" runat="server" visible="true">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label6" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Tag" runat="server" Text="Tag:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="Csoport_Jogalany_CsoportTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label10" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label9" runat="server" Text="T�pus" />
                            </td>
                            <td class="mrUrlapMezo" style="width: 200px">
                                <uc9:KodtarakDropDownList ID="KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel runat="server" ID="ListUpdatePanel" Visible="false">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 10px; margin-bottom: 10px;">
                                            <tr>
                                                <td style="width: 50%; padding: 0px 10px;">
                                                    <div style="text-align: left; padding-bottom: 2px;">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="width: auto">
                                                                    <span style="font-weight: bold; text-decoration: underline;">Csoportok:</span>
                                                                </td>
                                                                <td style="width: 100%; padding-left: 5px; padding-right: 8px">
                                                                    <asp:TextBox ID="TextBoxSearch" runat="server" Width="100%" CausesValidation="false" OnTextChanged="TextBoxSearch_TextChanged" AutoPostBack="true" />
                                                                </td>
                                                                <td style="width: auto">
                                                                    <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/egyeb/kereses.gif" CausesValidation="false"></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                </td>
                                                <td></td>
                                                <td style="width: 50%; padding: 0px 10px;">
                                                    <div style="text-align: left; padding-bottom: 2px;">
                                                        <span style="font-weight: bold; text-decoration: underline;">Tagok:</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%; padding: 0px 10px;">
                                                    <asp:ListBox ID="CsoportokList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" ID="AddButton" Text=">" Width="50px" Style="margin-bottom: 5px;" ToolTip="Hozz�ad" CausesValidation="false" OnClick="AddButton_Click" />
                                                    <asp:Button runat="server" ID="RemoveButton" Text="<" Width="50px" Style="margin-top: 5px;" ToolTip="T�r�l" CausesValidation="false" OnClick="RemoveButton_Click" />
                                                </td>
                                                <td style="width: 50%; padding: 0px 10px;">
                                                    <asp:ListBox ID="TagokList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                                <td colspan="3" style="padding: 10px;">
                                                    <asp:Label ID="label2" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

