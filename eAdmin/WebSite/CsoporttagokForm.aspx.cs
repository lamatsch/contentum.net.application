﻿using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CsoporttagokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.CsoporttagokFormHeaderTitle;

        pageView = new PageView(Page, ViewState);


        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CsoportTagok" + Command);
                break;
        }

        //BLG_2219
        //Modify esetet kiszedtem, módosítás esetén nem fut bele a kód ebbe az ágba.
        if (Command == CommandName.View /*|| Command == CommandName.Modify*/)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_CsoportTagok krt_csoporttagok = (KRT_CsoportTagok)result.Record;
                    LoadComponentsFromBusinessObject(krt_csoporttagok);

                    if (Command == CommandName.View)
                    {
                        KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS", krt_csoporttagok.Tipus, FormHeader1.ErrorPanel);
                        KodtarakDropDownList.Enabled = false;
                    }
                    else
                    {
                        KodtarakDropDownList.FillDropDownList("CSOPORTTAGSAG_TIPUS", false, FormHeader1.ErrorPanel);
                        KodtarakDropDownList.Enabled = false;
                        KodtarakDropDownList.DropDownList.Items.FindByValue(krt_csoporttagok.Tipus).Selected = true;
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string szuloCsoportId = Request.QueryString.Get(QueryStringVars.SzuloCsoportId);
            if (!String.IsNullOrEmpty(szuloCsoportId))
            {
                Csoport_CsoportTextBox.Id_HiddenField = szuloCsoportId;
                Csoport_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                Csoport_CsoportTextBox.Enabled = false;

                #region nem mûködõ kódtár érték leválogatás csoport típus szerint

                KodtarakDropDownList.FillDropDownList("CSOPORTTAGSAG_TIPUS", false, FormHeader1.ErrorPanel);
                //if (!string.IsNullOrEmpty(Csoport_Jogalany_CsoportTextBox.Id_HiddenField))
                //{
                //KRT_CsoportTagokService csTservice = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                //KRT_CsoportokService csService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                //execParam.Record_Id = Csoport_Jogalany_CsoportTextBox.Id_HiddenField;

                //if ((csService.Get(execParam).Record as KRT_Csoportok).Tipus == "0")
                //    KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS", "1", FormHeader1.ErrorPanel);
                //else
                //{
                //    if (!(bool)csTservice.HasLeader(execParam, szuloCsoportId).Record)
                //        KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS", "3", FormHeader1.ErrorPanel);
                //    KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS", "2", FormHeader1.ErrorPanel);
                //    //KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS","AlVezetõ",FormHeader1.ErrorPanel);
                //}
                //}
                #endregion

            }
        }

        if (Command == CommandName.Modify)
        {
            #region BLG_2219
            //LZS
            //QueryString - ből kiszedjük a SzuloCsoportId értékét. Kitöltjük a CSoportCsoportTextBox controlt a SzuloCsoportId alapján, majd feltöltjük a listbox controlokat tartalommal.
            //Ezekhez a FillCsoportokList() és a FillTagokList(szuloCsoportId) függvényeket kell használni.
            //Végül pedig láthatóvá kell tenni az egészet tartalmazó ListUpdatePanel - t, és elrejtjük az itt nem szükséges tagRow - t.
            string szuloCsoportId = Request.QueryString.Get(QueryStringVars.SzuloCsoportId);
            KodtarakDropDownList.FillDropDownList("CSOPORTTAGSAG_TIPUS", false, FormHeader1.ErrorPanel);

            if (!IsPostBack)
            {
                Csoport_CsoportTextBox.Id_HiddenField = szuloCsoportId;
                Csoport_CsoportTextBox.SetCsoportTextBoxById(new Contentum.eUIControls.eErrorPanel());
                Csoport_CsoportTextBox.Enabled = false;
                FillCsoportokList();
                FillTagokList(szuloCsoportId);
                ListUpdatePanel.Visible = true;
                tagRow.Visible = false;
            }
            #endregion
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_CsoportTagok krt_csoporttagok)
    {
        Csoport_CsoportTextBox.Id_HiddenField = krt_csoporttagok.Csoport_Id;
        Csoport_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Csoport_CsoportTextBox.Enabled = false;

        Csoport_Jogalany_CsoportTextBox.Id_HiddenField = krt_csoporttagok.Csoport_Id_Jogalany;
        Csoport_Jogalany_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Csoport_Jogalany_CsoportTextBox.Enabled = false;

        ErvenyessegCalendarControl1.ErvKezd = krt_csoporttagok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_csoporttagok.ErvVege;

        FormHeader1.Record_Ver = krt_csoporttagok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_csoporttagok.Base);

        if (Command == CommandName.View)
        {
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private KRT_CsoportTagok GetBusinessObjectFromComponents()
    {
        KRT_CsoportTagok krt_csoporttagok = new KRT_CsoportTagok();
        krt_csoporttagok.Updated.SetValueAll(false);
        krt_csoporttagok.Base.Updated.SetValueAll(false);

        krt_csoporttagok.Csoport_Id = Csoport_CsoportTextBox.Id_HiddenField;
        krt_csoporttagok.Updated.Csoport_Id = pageView.GetUpdatedByView(Csoport_CsoportTextBox);

        krt_csoporttagok.Csoport_Id_Jogalany = Csoport_Jogalany_CsoportTextBox.Id_HiddenField;
        krt_csoporttagok.Updated.Csoport_Id_Jogalany = pageView.GetUpdatedByView(Csoport_Jogalany_CsoportTextBox);

        krt_csoporttagok.Tipus = KodtarakDropDownList.SelectedValue;
        krt_csoporttagok.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownList);

        //krt_csoporttagok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_csoporttagok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_csoporttagok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_csoporttagok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_csoporttagok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_csoporttagok.Base.Ver = FormHeader1.Record_Ver;
        krt_csoporttagok.Base.Updated.Ver = true;

        return krt_csoporttagok;
    }


    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Csoport_CsoportTextBox);
            compSelector.Add_ComponentOnClick(Csoport_Jogalany_CsoportTextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "CsoportTagok" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                            KRT_CsoportTagok krt_csoporttagok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_csoporttagok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.SzuloCsoportId);

                            #region BLG_2219
                            //LZS
                            //Módosítás esetén lekérjük a csoporttagok eredeti állapotát a GetCsoportTagok(string szuloCsoportId) függvénnyel.
                            //Egy KRT_CsoportTagok objektumot feltöltünk a felületen lévő controlokból, majd pedig a listboxban kiválasztott elemeket egy foreach ciklusban hozzáadogatjuk,
                            //és egyesével insertáljuk az adatbázisba akkor, ha még nem szerepelt benne az adott id - jú rekord.
                            //Hiba esetén kiíratjuk azt a képernyő tetejére a DisplayErrorMessage() függvénnyel.
                            //Végül egy törlés rész is lefut, ahol az eredeti állapothoz képest törölt elemeket töröljük az adatbázisból is.
                            //Hiba esetén itt is kiíratjuk azt a képernyő tetejére a DisplayErrorMessage() függvénnyel.
                            KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                            List<KRT_CsoportTagok> csoportTagokBase = GetCsoportTagok(recordId);

                            KRT_CsoportTagok krt_csoportTagok = new KRT_CsoportTagok();
                            krt_csoportTagok = GetBusinessObjectFromComponents();
                            krt_csoportTagok.Csoport_Id = recordId;
                            krt_csoportTagok.Updated.Csoport_Id = true;

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = recordId;

                            Result result = null;
                            foreach (ListItem item in TagokList.Items)
                            {
                                if (item != null)
                                {
                                    krt_csoportTagok.Csoport_Id_Jogalany = item.Value;
                                    krt_csoportTagok.Updated.ErvKezd = krt_csoportTagok.Updated.ErvVege = krt_csoportTagok.Updated.Csoport_Id_Jogalany = true;
                                    //LZS - Ha tartalmazza az eredeti partnerkapcsolatok collection, akkor nem változott (maradt), és nincs vele teendő.
                                    //LZS - Ha nem, akkor beszúrjuk.
                                    var matches = csoportTagokBase.Where(p => p.Csoport_Id == krt_csoportTagok.Csoport_Id && p.Id == krt_csoportTagok.Csoport_Id_Jogalany);

                                    if (matches.Count() == 0)
                                    {
                                        result = service.Insert(execParam, krt_csoportTagok);
                                        if (result.IsError)
                                        {
                                            ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                                            return;
                                        }
                                    }
                                }
                            }

                            //Törlés, ha szükséges
                            string[] baseCsoportTagok;
                            string[] currentCsoportTagok = new string[TagokList.Items.Count];

                            //Kigyűjtjük az Id-kat az eredeti állapotot tároló listából a baseKapcsolatok tömbbe.
                            baseCsoportTagok = csoportTagokBase.Select(x => x.Id).ToArray<string>();

                            //Kigyűjtjük az Id-kat a jelenlegi állapotot tároló listából a currentKapcsolatok tömbbe.
                            for (int i = 0; i < TagokList.Items.Count; i++)
                            {
                                currentCsoportTagok[i] = TagokList.Items[i].Value;
                            }

                            //Képezzük a két tömb különségét. Ez lesz a törlendő id-k tömbje. [idForDelete]
                            var xpForInvalidate = baseCsoportTagok.Except(currentCsoportTagok).Select(id => new ExecParam() { Record_Id = id }).ToArray();
                            if (xpForInvalidate.Length > 0)
                            {
                                result = service.MultiInvalidate(xpForInvalidate);
                                if (result.IsError)
                                {
                                    ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                                    return;
                                }
                            }

                            //
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                            #endregion

                            //}
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    #region BLG_2219
    /// <summary>
    /// Feltölti az összes csoporttal a képernyő bal oldalán lévő CsoportokList listbox-ot név szerint rendezve.
    /// </summary>
    void FillCsoportokList()
    {
        CsoportokList.Items.Clear();

        KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);
        KRT_CsoportokSearch search = (KRT_CsoportokSearch)Search.GetSearchObject(Page, new KRT_CsoportokSearch());
        search.OrderBy = "Nev";
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);
        UI.ListBoxFill(CsoportokList, result, "Nev", FormHeader1.ErrorPanel, null);
        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    /// <summary>
    /// Feltölti a kiválasztott csoporthoz tartozó csoporttagokat a képernyő jobb oldalán lévő TagokList listbox-ot név szerint rendezve.
    /// </summary>
    /// <param name="partnerId"></param>
    void FillTagokList(string szuloCsoportId)
    {
        TagokList.Items.Clear();

        if (!String.IsNullOrEmpty(szuloCsoportId))
        {
            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Csoportok szuloCsoport = new KRT_Csoportok();
            szuloCsoport.Id = szuloCsoportId;
            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
            search.OrderBy = "Csoport_Nev";

            Result result = service.GetAllBySzuloCsoport(ExecParam, szuloCsoport, search);
            UI.ListBoxFill(TagokList, result, "Csoport_Nev", FormHeader1.ErrorPanel, null);
        }
    }

    /// <summary>
    /// A csoporttagok eredeti állapotát adja vissza adatbázisból, és eltárolja egy KRT_CsoportTagok listában.
    /// </summary>
    /// <param name="szuloCsoportId"></param>
    /// <returns></returns>
    private List<KRT_CsoportTagok> GetCsoportTagok(string szuloCsoportId)
    {
        List<KRT_CsoportTagok> csoportTagokBase = new List<KRT_CsoportTagok>();

        if (!String.IsNullOrEmpty(szuloCsoportId))
        {
            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Csoportok csoportok = new KRT_Csoportok();
            csoportok.Id = szuloCsoportId;

            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
            search.OrderBy = "Csoport_Nev";

            Result result = service.GetAllBySzuloCsoport(ExecParam, csoportok, search);

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                KRT_CsoportTagok csoportTag = new KRT_CsoportTagok();
                csoportTag.Id = row["Id"].ToString();
                csoportTag.ErvKezd = row["ErvKezd"].ToString();
                csoportTag.ErvVege = row["ErvVege"].ToString();
                csoportTag.Csoport_Id_Jogalany = row["Csoport_Id_Jogalany"].ToString();
                csoportTag.Csoport_Id = row["Csoport_Id"].ToString();
                csoportTag.Tipus = row["Tipus"].ToString();

                csoportTagokBase.Add(csoportTag);
            }
        }

        return csoportTagokBase;
    }

    /// <summary>
    /// Csoportok listából a csoportkapcsolatok listához ad elemeket.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(CsoportokList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = TagokList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                TagokList.Items.Add(item);
            }
        }
    }

    /// <summary>
    /// Csoporttagok listából a vesz el elemeket.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(TagokList);
        foreach (ListItem item in selectedItems)
        {
            TagokList.Items.Remove(item);
        }
    }

    /// <summary>
    /// Az adott listboxban kijelölt elemeket adja vissza.
    /// </summary>
    /// <param name="listBox"></param>
    /// <returns></returns>
    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    /// <summary>
    /// Szűr a csoportok között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokList();
    }

    /// <summary>
    /// Szűr a csoportok között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokList();
    }
    #endregion

}
