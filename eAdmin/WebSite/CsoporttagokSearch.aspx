<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CsoporttagokSearch.aspx.cs" Inherits="CsoporttagokSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc9" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc6" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc5" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc8" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>


<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc4:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoporttagokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Csoport:" CssClass="mrUrlapCaption"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:CsoportTextBox Id="Csoport_CsoportTextBox" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Felhasznalo_Label" runat="server" Text="Felhaszn�l�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:FelhasznaloTextBox ID="FelhasznaloTextBox1" runat="server" SearchMode="false" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Tags�g t�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="Tipus" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                                <br />
                                </td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="Jogalany" runat="server" Text="Jogalany" />&nbsp;
                                <asp:CheckBox ID="Kiszolgalhato" runat="server" Text="Csak a rendszer kezelheti" />&nbsp;&nbsp;
                                &nbsp;&nbsp;</td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2" style="height: 30px" >
                                <uc8:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" />
                                &nbsp;&nbsp;</td>
                        </tr>
                        <tr class="urlapSor">
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc9:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc5:SearchFooter ID="SearchFooter1" runat="server" />
                    &nbsp; &nbsp;
                </td>
            </tr>
        </table>
</asp:Content>

