using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class CsoporttagokSearch : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            setDefaultValues();
        }
    }

    private void setDefaultValues()
    {
        Csoport_CsoportTextBox.Text = "";
        FelhasznaloTextBox1.Text = "";
        Tipus.SelectedIndex = 0;
        Jogalany.Checked = true;
        Kiszolgalhato.Checked = true;

        Ervenyesseg_SearchFormComponent1.SetDefault();
//        TalalatokSzama_SearchFormComponent1.SetDefault();
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            setDefaultValues();
        }
    }
}
