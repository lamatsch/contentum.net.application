﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiagnosticTest.aspx.cs" Inherits="DiagnosticTest" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="cache-control" content="no-store" />
    <title></title>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/Ajax.asmx" />
        </Services>
        </asp:ScriptManager>
        <uc1:FormHeader ID="FormHeader1" runat="server" FullManualHeaderTitle="Diagnosztikai tesztoldal" />
        <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" width="90%">
                    <tr>
                        <td>
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eAdminWebSite" runat="server" Text="eAdmin website elérés" Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eAdminWebSite_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eAdminWebService" runat="server" Text="eAdmin webservice elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eAdminWebService_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eRecordWebSite" runat="server" Text="eRecord website elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eRecordWebSite_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eRecordWebService" runat="server" Text="eRecord webservice elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eRecordWebService_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eDocumentWebService" runat="server" Text="eDocument webservice elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eDocumentWebService_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eMigrationWebSite" runat="server" Text="eMigration website elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eMigrationWebSite_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_eMigrationWebService" runat="server" Text="eMigration webservice elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_eMigrationWebService_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_TemplateManager" runat="server" Text="Template Manager elérés"
                                                Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_TemplateManager_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_SqlServer" runat="server" Text="SQL Server elérés" Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_SqlServer_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaptionBal_indent_fixWidth">
                                            <asp:CheckBox ID="cb_Ajax" runat="server" Text="Ajax elérés" Checked="true" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="result_Ajax_Label" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                            <uc2:FormFooter ID="FormFooter1" runat="server" Command="Modify" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
