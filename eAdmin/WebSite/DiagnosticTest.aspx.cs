﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Net;
using System.Threading;
using System.IO;
using Contentum.eQuery.BusinessDocuments;

public partial class DiagnosticTest : System.Web.UI.Page
{
    private const string key_eAdminWebSiteUrl = "eAdminWebSiteUrl";
    private const string key_eAdminBusinessServiceUrl = "eAdminBusinessServiceUrl";
    private const string key_eRecordWebSiteUrl = "eRecordWebSiteUrl";
    private const string key_eRecordBusinessServiceUrl = "eRecordBusinessServiceUrl";
    private const string key_eMigrationWebSiteUrl = "eMigrationWebSiteUrl";
    private const string key_eMigrationBusinessServiceUrl = "eMigrationBusinessServiceUrl";
    private const string key_eDocumentBusinessServiceUrl = "eDocumentBusinessServiceUrl";
    private const string key_eTemplateManagerBusinessServiceUrl = "eTemplateManagerBusinessServiceUrl";
    private const string key_SqlServer = "SqlServer";
    private const string key_Ajax = "Ajax";

    protected void Page_Init(object sender, EventArgs e)
    {
        Response.CacheControl = "no-cache";


        //AjaxLogging.js referencia hozzáadása
        Contentum.eUtility.JavaScripts.RegisterAjaxLoggingScript(Page);
        //Common.js referencia hozzáadása
        Contentum.eUtility.JavaScripts.RegisterCommonScripts(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            ClearResults();

            // Url-ek összegyűjtése a webRequest-ek indításához:
            Dictionary<string, string> appUrls = GetAppUrls();

            // függőben lévő kérések listájának feltöltése (még a webRequest-ek indítása előtt kell)
            foreach (KeyValuePair<string, string> kvp in appUrls)
            {
                pendingRequestsList.Add(kvp.Key);
            }

            // Sql server-es kérést is hozzáadjuk a listához:
            if (cb_SqlServer.Checked)
            {
                pendingRequestsList.Add(key_SqlServer);
            }

            // webRequest-ek indítása:
            foreach (KeyValuePair<string, string> kvp in appUrls)
            {
                SendHttpRequests(kvp.Key, kvp.Value);
            }

            if (cb_SqlServer.Checked)
            {
                // Sql Server teszt:
                BeginSqlServerTest();
            }

            // ha nincs semmi bepipálva, ne kelljen kivárni, míg letelik a megadott idő
            if (appUrls.Count > 0 || cb_SqlServer.Checked)
            {
                // várakozás, míg meg nem jön az összes eredmény, vagy le nem telik a megadott idő (msec)
                allDone.WaitOne(60000, false);
            }

            if (cb_Ajax.Checked)
            {
                // Ajaxos tesztelő javascript beregisztrálása:
                RegisterAjaxTestJs();
            }
        }
    }
    

    private Dictionary<string, string> GetAppUrls()
    {
        Dictionary<string, string> appUrls = new Dictionary<string, string>();

        if (cb_eAdminWebSite.Checked)
        {
        string eAdminWebsiteUrl = Request.Url.AbsoluteUri.Remove(Request.Url.AbsoluteUri.LastIndexOf("/"));
        eAdminWebsiteUrl = NormalizeAppUrl(eAdminWebsiteUrl) + "Login.aspx";
        appUrls.Add(key_eAdminWebSiteUrl, eAdminWebsiteUrl);
        }

        if (cb_eAdminWebService.Checked)
        {
        string eAdminWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(key_eAdminBusinessServiceUrl);
        eAdminWsUrl = NormalizeAppUrl(eAdminWsUrl) + "KRT_FelhasznalokService.asmx";
        appUrls.Add(key_eAdminBusinessServiceUrl, eAdminWsUrl);
        }

        if (cb_eRecordWebSite.Checked)
        {
        string eRecordWebsiteUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(key_eRecordWebSiteUrl);
        eRecordWebsiteUrl = NormalizeAppUrl(eRecordWebsiteUrl) + "Login.aspx";
        appUrls.Add(key_eRecordWebSiteUrl, eRecordWebsiteUrl);
        }

        if (cb_eRecordWebService.Checked)
        {
        string eRecordWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(key_eRecordBusinessServiceUrl);
        eRecordWsUrl = NormalizeAppUrl(eRecordWsUrl) + "EREC_UgyUgyiratokService.asmx";
        appUrls.Add(key_eRecordBusinessServiceUrl, eRecordWsUrl);
        }

        if (cb_TemplateManager.Checked)
        {
            string templateManagerWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(key_eTemplateManagerBusinessServiceUrl);
            templateManagerWsUrl = NormalizeAppUrl(templateManagerWsUrl) + "TemplateManagerService.asmx";
            appUrls.Add(key_eTemplateManagerBusinessServiceUrl, templateManagerWsUrl);
        }

        // eMigration, eDocument Url-eket az eRecordból kell lekérdezni:
        if (cb_eMigrationWebSite.Checked || cb_eMigrationWebService.Checked || cb_eDocumentWebService.Checked)
        {
            //AuthenticationService service_authentication = eAdminService.ServiceFactory
                            
            try
            {
                string eRecordWebSite_WrappedServiceUrl = UI.GetAppSetting(key_eRecordWebSiteUrl) + "WrappedWebService/Wrapped";

                Contentum.eUtility.BusinessService bs = new Contentum.eUtility.BusinessService();
                bs.Type = "SOAP";
                bs.Url = eRecordWebSite_WrappedServiceUrl;
                bs.Authentication = "Windows";
                Contentum.eAdmin.Service.ServiceFactory sf = Contentum.eUtility.eAdminService.GetServiceFactory(bs);

                AuthenticationService service_authentication = sf.GetAuthenticationService();

                ExecParam execParam = new ExecParam(); //UI.SetExecParamDefault(Page);

                Result result =
                    service_authentication.GetApplicationUrls(execParam);

                if (result.IsError)
                {
                    // hiba:
                    throw new Exception("Url-ek lekérdezése eRecord-ból sikertelen");
                }

                // Ilyen formában jön a válasz:
                // "[appKey],[appUrl];[appKey],[appUrl];...
                string responseString = result.Record.ToString();
                string[] responseStringParts = responseString.Split(';');
                foreach (string appUrl in responseStringParts)
                {
                    string[] appUrlParts = appUrl.Split(',');
                    if (appUrlParts.Length > 1)
                    {
                        string appKey = appUrlParts[0];
                        string url = appUrlParts[1];

                        switch (appKey)
                        {
                            case key_eMigrationWebSiteUrl:
                                if (cb_eMigrationWebSite.Checked)
                                {
                                    string eMigrationWebsiteUrl = NormalizeAppUrl(url) + "Login.aspx";
                                    appUrls.Add(appKey, eMigrationWebsiteUrl);
                                }
                                break;
                            case key_eMigrationBusinessServiceUrl:
                                if (cb_eMigrationWebService.Checked)
                                {
                                    string eMigrationWsUrl = NormalizeAppUrl(url) + "MIG_FoszamService.asmx";
                                    appUrls.Add(appKey, eMigrationWsUrl);
                                }
                                break;
                            case key_eDocumentBusinessServiceUrl:
                                if (cb_eDocumentWebService.Checked)
                                {
                                    string eDocumentWsUrl = NormalizeAppUrl(url) + "DocumentService.asmx";
                                    appUrls.Add(appKey, eDocumentWsUrl);
                                }
                                break;
                        }
                    }
                }
            }
            catch
            {
                string errorMsg_urlLekerdezesSikertelen = "Az alkalmazás URL-ének lekérdezése sikertelen.";

                result_eMigrationWebService_Label.Text = errorMsg_urlLekerdezesSikertelen;
                result_eMigrationWebSite_Label.Text = errorMsg_urlLekerdezesSikertelen;
                result_eDocumentWebService_Label.Text = errorMsg_urlLekerdezesSikertelen;
            }
        }       
        
        return appUrls;
    }

    /// <summary>
    /// Rak egy / jelet az url végére, ha nincs
    /// </summary>    
    private string NormalizeAppUrl(string url)
    {
        if (String.IsNullOrEmpty(url)) { return String.Empty; }

        if (!url.EndsWith("/"))
        {
            return url + "/";
        }
        else
        {
            return url;
        }
    }
    

    private ManualResetEvent allDone = new ManualResetEvent(false);
    private List<string> pendingRequestsList = new List<string>();

    public class RequestState
    {
        // This class stores the state of the request.     
        public WebRequest request;
        public WebResponse response;
        public string key;
        public string url;
        public DateTime requestStartTime;
        public DateTime requestEndTime;

        public RequestState(string key)
        {            
            request = null;
            this.key = key;
        }
    }

    private void SendHttpRequests(string key, string url)
    {
        WebRequest webRequest;

        webRequest = WebRequest.Create(url);
        webRequest.Credentials = CredentialCache.DefaultCredentials;
        webRequest.PreAuthenticate = true;        

        RequestState requestState = new RequestState(key);
        requestState.request = webRequest;
        requestState.url = url;
        requestState.requestStartTime = DateTime.Now;

        IAsyncResult asyncResult = (IAsyncResult) webRequest.BeginGetResponse(new AsyncCallback(this.HttpRequest_Callback), requestState);                        
    }

    public void HttpRequest_Callback(IAsyncResult asyncResult)
    {           
        RequestState requestState = (RequestState)asyncResult.AsyncState;
        WebRequest webRequest = requestState.request;
        requestState.requestEndTime = DateTime.Now;

        // kérés kivétele a listából:
        if (pendingRequestsList.Contains(requestState.key))
        {
            pendingRequestsList.Remove(requestState.key);
        }
                
        try
        {
            requestState.response = webRequest.EndGetResponse(asyncResult);
        }
        catch (WebException e)
        {
            SetResultMessage(requestState.key, e.Message + "<br/>(URL: '" + requestState.url + "')");
            return;
        }

        HttpWebResponse webResponse = (HttpWebResponse)requestState.response;

        //if (webResponse.StatusCode == HttpStatusCode.OK)
        //{
        //}
        
        SetResultMessage(requestState.key, webResponse.StatusDescription);
        // le kell zárni a webResponse-t:
        webResponse.Close();

        // ha már nincs több függőben lévő kérés:
        if (pendingRequestsList.Count == 0)
        {
            allDone.Set();
        }
    }

    
    private void BeginSqlServerTest()
    {
        KRT_ParameterekService service_parameterek = eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam execParam = new ExecParam(); //UI.SetExecParamDefault(Page);

        KRT_ParameterekSearch search_param = new KRT_ParameterekSearch();

        IAsyncResult asyncResult = service_parameterek.BeginGetAll(execParam, search_param, new AsyncCallback(this.SqlServerTest_Callback), service_parameterek);         
    }

    public void SqlServerTest_Callback(IAsyncResult asyncResult)
    {
        // kérés kivétele a listából:
        if (pendingRequestsList.Contains(key_SqlServer))
        {
            pendingRequestsList.Remove(key_SqlServer);
        }

        KRT_ParameterekService service_parameterek = (KRT_ParameterekService)asyncResult.AsyncState;

        Result result = service_parameterek.EndGetAll(asyncResult);

        if (result.IsError)
        {
            string errorMessage = ResultError.GetErrorMessage(ResultError.GetErrorNumberFromResult(result));
            result_SqlServer_Label.Text = errorMessage;
        }
        else
        {
            result_SqlServer_Label.Text = "OK";
        }

        service_parameterek.Dispose();

        // ha már nincs több függőben lévő kérés:
        if (pendingRequestsList.Count == 0)
        {
            allDone.Set();
        }
    }

    
    private void RegisterAjaxTestJs()
    {        

        string js = @" function AjaxTesztStart()
                    {   
                        $get('" + result_Ajax_Label.ClientID + @"').firstChild.nodeValue = '[Teszt folyamatban...]';
                        Ajax.AjaxTeszt(OnWsCallBack_AjaxTeszt, OnWsError_AjaxTeszt);
                    }

                    function OnWsCallBack_AjaxTeszt(result)
                    {
                        var resultMessage = '';
                        if (result == 1) 
                        { resultMessage = 'OK'; }
                        else
                        { resultMessage = 'Hiba: ' + result; }

                        $get('" + result_Ajax_Label.ClientID + @"').firstChild.nodeValue = resultMessage;
                    }

                    function OnWsError_AjaxTeszt(result)
                    {
                        // hiba:                        
                        $get('" + result_Ajax_Label.ClientID + @"').firstChild.nodeValue = 'Hiba: ' + result.get_message();
                    }   
                    
                    AjaxTesztStart();
                    
                    ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ajaxTesztJs", js, true);
    }
    
    private void SetResultMessage(string key, string message)
    {
        switch (key)
        {
            case key_eAdminWebSiteUrl:
                result_eAdminWebSite_Label.Text = message;
                break;
            case key_eAdminBusinessServiceUrl:
                result_eAdminWebService_Label.Text = message;
                break;
            case key_eRecordWebSiteUrl:
                result_eRecordWebSite_Label.Text = message;
                break;
            case key_eRecordBusinessServiceUrl:
                result_eRecordWebService_Label.Text = message;
                break;
            case key_eDocumentBusinessServiceUrl:
                result_eDocumentWebService_Label.Text = message;
                break;
            case key_eMigrationWebSiteUrl:
                result_eMigrationWebSite_Label.Text = message;
                break;
            case key_eMigrationBusinessServiceUrl:
                result_eMigrationWebService_Label.Text = message;
                break;
            case key_eTemplateManagerBusinessServiceUrl:
                result_TemplateManager_Label.Text = message;
                break;
            case key_SqlServer:
                result_SqlServer_Label.Text = message;
                break;
            case key_Ajax:
                result_Ajax_Label.Text = message;
                break;
        }
    }


    private const string emptyResultString = "??";

    private void ClearResults()
    {
        result_Ajax_Label.Text = emptyResultString;
        result_eAdminWebService_Label.Text = emptyResultString;
        result_eAdminWebSite_Label.Text = emptyResultString;
        result_eDocumentWebService_Label.Text = emptyResultString;
        result_eMigrationWebService_Label.Text = emptyResultString;
        result_eMigrationWebSite_Label.Text = emptyResultString;
        result_eRecordWebService_Label.Text = emptyResultString;
        result_eRecordWebSite_Label.Text = emptyResultString;
        result_SqlServer_Label.Text = emptyResultString;
        result_TemplateManager_Label.Text = emptyResultString;
    }


}
