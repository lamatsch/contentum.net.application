﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EUzenetSzabalyForm.aspx.cs" Inherits="EUzenetSzabalyForm" Title="EUzenetSzabaly"
    ValidateRequest="false" %>

<%@ Register Src="~/Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="~/Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="~/Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="~/Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList" TagPrefix="kddl" %>
<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="ucFCS" %>
<%@ Register Src="~/Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="ucCSOPTB" %>
<%@ Register Src="~/Component/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox" TagPrefix="tsztb" %>
<%@ Register Src="~/Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>

<%@ Register Src="~/Component/EUzenetSzabalyTargyComplexControl.ascx" TagPrefix="EUZT" TagName="EUzenetSzabalyTargyComplexControl" %>

<%@ Register Src="Component/EUzenetSzabaly_ConditionForXml_ComplexControl.ascx" TagName="EUzenetSzabaly_ConditionForXml_ComplexControl" TagPrefix="uc7" %>
<%@ Register Src="~/Component/EUzenetSzabaly_ConditionForXml_ComplexControl.ascx" TagPrefix="EUZT" TagName="EUzenetSzabaly_ConditionForXml_ComplexControl" %>
<%@ Register Src="~/Component/UgyTipus_Complex_UserControl.ascx" TagPrefix="EUZT" TagName="UgyTipus_Complex_UserControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <style>
        html {
            height: 100%;
        }

        .divWaiting {
            position: absolute;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            /*min-height: 99% !important;*/
            height: 99%;
            width: 100%;
            padding-top: 20%;
            box-sizing: border-box;
        }
    </style>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KodcsoportokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="FormUpdatePanel" runat="server" DisplayAfter="1">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <div style="position: relative; top: 50%;">
                            <img style="position: relative; top: 50%;" src="images/hu/egyeb/activity_indicator.gif" alt="" />
                            <asp:Label ID="Label_progress" runat="server" Text="Feldolgozás folyamatban..."></asp:Label>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="FormUpdatePanel" UpdateMode="Always">
                <ContentTemplate>
                    <asp:HiddenField runat="server" ID="HiddenField_Id_InsertedItem" />
                    <asp:Panel runat="server" ID="PanelMain" HorizontalAlign="Left">
                        <%--        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                        <h1>E-Beadvány szabályok</h1>
                        <asp:Panel runat="server" ID="Panel1">
                            <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                <table>
                                    <tr>
                                        <td align="left">SZABÁLY FÕ ADATAI
     
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server">
                            <div style="">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Kinek a nevében" Width="180px"></asp:Label>
                                                        &nbsp;<small><span style="color: red" title="Kitöltése kötelező !">*</span></small> </td>
                                                    <td>
                                                        <ucFCS:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxRuleUserId" runat="server" Validate="true" TryFireChangeEvent="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Csoport nevében" Width="180px"></asp:Label>
                                                        &nbsp;<small><span style="color: red" title="Kitöltése kötelező !">*</span></small> </td>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownListRuleUserRoleId" runat="server" Width="300px">
                                                        </asp:DropDownList>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr id="trInterfeszAdatlaptipus" runat="server" visible="true">
                                                    <td>
                                                        <asp:Label ID="lblInterfeszAdatlaptipus" runat="server" Text="<%$Forditas:lblInterfeszAdatlaptipus|Interfész adatlaptípus:%>" Width="180px"></asp:Label>
                                                        &nbsp;<small><span style="color: red" title="Kitöltése kötelező !">*</span></small> </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxRuleName" runat="server" Text="" Width="300px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxRuleName" Display="Dynamic" ErrorMessage="RequiredFieldValidatorName" ToolTip="Interfész adatlaptípus megadása kötelezõ">Interfész adatlaptípus megadása kötelezõ</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Leírás" Width="180px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxRuleDetail" runat="server" Text="" Width="300px"></asp:TextBox>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownListRuleType" runat="server" Width="300px">
                                                        </asp:DropDownList>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Leállító?" ToolTip="Szabály végrehajtása megállítja a soron következő szabály végrehajtását" Width="180px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="CheckBoxRuleIsStop" runat="server" Width="300px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" Text="Sorrend" ToolTip="Szabály végrehajtási sorrendje" Width="180px"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxSorrend" runat="server" MaxLength="4" Text="" Width="300px"></asp:TextBox>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </asp:Panel>

                        <ajaxToolkit:TabContainer ID="TabContainerMAIN" runat="server" AutoPostBack="true" OnActiveTabChanged="TabContainerMAIN_ActiveTabChanged" Width="100%">

                            <ajaxToolkit:TabPanel ID="TabPanelCondition" runat="server">
                                <HeaderTemplate>
                                    <asp:Label ID="labelPartnerHeader" runat="server" Text="Feltételek" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>

                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelFeltetelShowHide">
                                        <div style="font-weight: bold; background-color: darkolivegreen; color: white; margin: 5px;">
                                            <table>
                                                <tr>
                                                    <td align="left" style="padding: 3px">FELTÉTELEK - Adatokban
     
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel5" runat="server">
                                        <div style="margin-left: 20px;">
                                            <table>
                                                <td valign="top">
                                                    <u><b>1. feltétel</b></u><br />

                                                    <asp:Label runat="server" Text="Figyelt mezõ" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionField_1" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionOperator_1" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Érték" Width="180px"></asp:Label>
                                                    <asp:TextBox ID="TextBoxConditionValue_1" runat="server" Width="300px"></asp:TextBox>
                                                    <br />
                                                    <div style="margin-top: 3px;"></div>

                                                    <u><b>2. feltétel</b></u><br />

                                                    <asp:Label runat="server" Text="Figyelt mezõ" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionField_2" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionOperator_2" runat="server" Width="300px"></asp:DropDownList>

                                                    <br />

                                                    <asp:Label runat="server" Text="Érték" Width="180px"></asp:Label>

                                                    <asp:TextBox ID="TextBoxConditionValue_2" runat="server" Width="300px"></asp:TextBox>
                                                    <br />
                                                    <div style="margin-top: 3px;"></div>

                                                    <u><b>3. feltétel</b></u><br />

                                                    <asp:Label runat="server" Text="Figyelt mezõ" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionField_3" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionOperator_3" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Érték" Width="180px"></asp:Label>
                                                    <asp:TextBox ID="TextBoxConditionValue_3" runat="server" Width="300px"></asp:TextBox>
                                                    <br />
                                                </td>
                                                <td valign="top">

                                                    <u><b>4. feltétel</b></u><br />

                                                    <asp:Label runat="server" Text="Figyelt mezõ" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionField_4" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionOperator_4" runat="server" Width="300px"></asp:DropDownList>

                                                    <br />

                                                    <asp:Label runat="server" Text="Érték" Width="180px"></asp:Label>
                                                    <asp:TextBox ID="TextBoxConditionValue_4" runat="server" Width="300px"></asp:TextBox>
                                                    <br />
                                                    <div style="margin-top: 3px;"></div>

                                                    <u><b>5. feltétel</b></u><br />
                                                    <asp:Label runat="server" Text="Figyelt mezõ" Width="180px"></asp:Label>
                                                    <asp:DropDownList ID="DropDownListConditionField_5" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Típus" Width="180px"></asp:Label>

                                                    <asp:DropDownList ID="DropDownListConditionOperator_5" runat="server" Width="300px"></asp:DropDownList>
                                                    <br />

                                                    <asp:Label runat="server" Text="Érték" Width="180px"></asp:Label>
                                                    <asp:TextBox ID="TextBoxConditionValue_5" runat="server" Width="300px"></asp:TextBox>
                                                </td>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <br />
                                    <asp:Panel runat="server">
                                        <div style="font-weight: bold; background-color: darkolivegreen; color: white; margin: 5px;">
                                            <table>
                                                <tr>
                                                    <td align="left" style="padding: 3px">FELTÉTELEK - Üzenet csatolmány adat alapján
     
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server">
                                        <div style="margin-left: 20px;">
                                            <uc7:EUzenetSzabaly_ConditionForXml_ComplexControl ID="EUzenetSzabaly_ConditionForXml_ComplexControl_A" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelOperation" runat="server">
                                <HeaderTemplate>
                                    <asp:Label ID="label3" runat="server" Text="Fő műveletek" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <asp:Panel runat="server" ID="Panel3">
                                        <div style="font-weight: bold; background-color: darkolivegreen; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">FÕ MÛVELET
     
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <div style="margin-left: 15px;">
                                        <asp:Panel ID="Panel4" runat="server">
                                            <div style="margin: 20px;">
                                                <asp:Label runat="server" Text="Mûvelet típusa" Width="180px"></asp:Label>
                                                <asp:DropDownList ID="DropDownListOperationType" runat="server" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="DropDownListOperationType_SelectedIndexChanged">
                                                    <asp:ListItem Text="Érkeztet" Value="ERKEZTET" Selected="true" />
                                                    <asp:ListItem Text="Iktat" Value="IKTAT" />
                                                    <%--<asp:ListItem Text="Irattároz" Value="IRATTAROZ" />--%>
                                                    <asp:ListItem Text="Iktatás alszámra" Value="IKTATAS_FOSZAMMAL" />
                                                    <asp:ListItem Text="Nincs művelet" Value="NINCS" />
                                                </asp:DropDownList><br />
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="PanelOperationErkeztet">
                                            <asp:Panel runat="server" ID="PanelOperationErkeztetShowHideMain">
                                                <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                                    <table>
                                                        <tr>
                                                            <td align="left">&nbsp;&nbsp;ÉRKEZTETÉS
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="PanelOperationErkeztetShowHideSub" runat="server">
                                                <div style="margin: 20px;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="Felelõs" Width="180px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <ucFCS:FelhasznaloCsoportTextBox ID="FelhasznaloCsoport_ERK_ErkeztetesFelelos" runat="server" Validate="false" width="300px" />
                                                                <%--<asp:DropDownList ID="DropDownListOpErkeztetesFelelos" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="Címzett" Width="180px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <%--<asp:DropDownList ID="DropDownListOpErkeztetesCimzett" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoport_ERK_ErkeztetesCimzett" Validate="false" Width="300px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="ÉrkeztetõHely:" Width="180px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <%--<asp:TextBox runat="server" ID="TextBoxIktatasErkeztetoKonyv" Width="300px" />--%>
                                                                <asp:DropDownList ID="DropDownListOpErkeztetesErkeztetoHely" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="ErkeztetoKonyv" Width="180px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="DropDownListOpErkeztetesErkeztetokonyv" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="Kézbesítés módja" Width="180px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <kddl:KodTarakDropDownList ID="DropDownListOpErkeztetesKezbesitesModja" runat="server" Width="300px" CssClass=""></kddl:KodTarakDropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>

                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="PanelOperationIktat">
                                            <asp:Panel runat="server" ID="PanelOperationIktatasShowHideMain">
                                                <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                                    <table>
                                                        <tr>
                                                            <td align="left">&nbsp;&nbsp;IKTATÁS
     
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="PanelOperationIktatasShowHideSub" runat="server">
                                                <div style="margin: 20px;">
                                                    <table>
                                                        <tr>
                                                            <td valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="ÉrkeztetõHely:" Width="180px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:TextBox runat="server" ID="TextBoxIktatasErkeztetoKonyv" Width="300px" />--%>
                                                                            <asp:DropDownList ID="DropDownListIktatasErkeztetoKonyvHely" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Érkeztetõkönyv" Width="180px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <asp:DropDownList ID="DropDownListOpIktatasErkeztetoKonyv" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Iktatóhely" Width="180px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:TextBox runat="server" ID="TextBoxIktatasIktatoKonyv" Width="300px" />--%>
                                                                            <asp:DropDownList ID="DropDownListIktatasIktatoKonyvHely" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="IktatoKonyv" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <asp:DropDownList ID="DropDownListOpIktatasIktatoKonyv" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Irat típusa" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <asp:DropDownList ID="DropDownListOpIktatasIratTipus" runat="server" Width="300px"></asp:DropDownList>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="SzakrendszerKod" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <asp:TextBox ID="TextBoxOpIktatasSzakrendszerKod" runat="server" Text="" Width="300px"></asp:TextBox>

                                                                        </td>
                                                                    </tr>
                                                                    <%-- <tr>
                                                            <td style="width: 180px">
                                                                <asp:Label runat="server" Text="Tárgy" Width="180px"></asp:Label>

                                                            </td>
                                                            <td style="width: 300px">
                                                                <asp:TextBox ID="TextBoxOpIktatasTargy" runat="server" Text="" Width="300px" Rows="2" TextMode="MultiLine"></asp:TextBox>

                                                            </td>
                                                        </tr>--%>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Ügyirat ügyintézési ideje:" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <kddl:KodTarakDropDownList ID="KodtarakDropDownList_OP_IK_IntezesiIdo" runat="server" CssClass="" />
                                                                            <kddl:KodTarakDropDownList ID="KodtarakDropDownList_OP_IK_IntezesiIdoegyseg" runat="server" CssClass="" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" visible="false">
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Irat ügyintézõ" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:DropDownList ID="DropDownListOpIktatasIratUgyintezo" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                            <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_IKT_IratUgyintezo" Validate="false" Width="300px" />

                                                                        </td>
                                                                    </tr>


                                                                </table>

                                                            </td>
                                                            <td valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Ügyirat ügyintézõ" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:DropDownList ID="DropDownListOpIktatasUgyiratUgyintezo" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                            <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo" Validate="false" Width="300px" />
                                                                            <asp:Label runat="server" ID="LabelWarning_IKT_UgyiratUgyintezo" Font-Size="Small" ForeColor="Orange" Visible="false" Text="Mező letíltva szignálás miatt !" />
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Irattári tétel" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <asp:DropDownList ID="DropDownListOpIktatasIrattariTetel" runat="server" Width="300px"></asp:DropDownList>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Küldemény Felelõs csoport" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:DropDownList ID="DropDownListOpIktatas_KuldemenyFelelos" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                            <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_IKT_KuldemenyFelelos" Validate="false" Width="300px" />

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Küldemény címzett csoport" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <%--<asp:DropDownList ID="DropDownListOpIktatas_KuldemenyCimzett" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                            <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_IKT_KuldemenyCimzett" Validate="false" Width="300px" />

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 180px">
                                                                            <asp:Label runat="server" Text="Ügyfajta / Ügytípus" Width="180px"></asp:Label>

                                                                        </td>
                                                                        <td style="width: 300px">
                                                                            <EUZT:UgyTipus_Complex_UserControl runat="server" ID="UgyTipus_Complex_UserControl_IKT" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                        </tr>

                                                    </table>

                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="PanelOperationIktatasFoSzammal">
                                            <asp:Panel runat="server" ID="Panel9">
                                                <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                                    <table>
                                                        <tr>
                                                            <td align="left">&nbsp;&nbsp;IKTATÁS ALSZÁMRA
     
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="PanelOperationIktatasFoSzammalShowHideSub" runat="server">
                                                <div style="margin: 20px;">
                                                    <table>
                                                        <td valign="top">
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Ügyirat iktatószám:" Width="180px"></asp:Label><br />
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <asp:TextBox runat="server" ID="TextBox_OP_IK_FS_FoSzam" Width="300px" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Érkeztetõkönyv" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <asp:DropDownList ID="DropDownList_OP_IK_FS_ErkeztetoKonyv" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Irat típusa" Width="180px"></asp:Label>

                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <asp:DropDownList ID="DropDownList_OP_IK_FS_IratTipus" runat="server" Width="300px"></asp:DropDownList>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="SzakrendszerKod" Width="180px"></asp:Label>

                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <asp:TextBox ID="TextBox_OP_IK_FS_Szakrendszerkod" runat="server" Text="" Width="300px"></asp:TextBox>

                                                                    </td>
                                                                </tr>

                                                                <tr runat="server" visible="false">
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Irat ügyintézõ" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_OP_IK_FS_IratUgyintezo" Validate="false" Width="300px" />
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <table>

                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Ügyirat ügyintézõ" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo" Validate="false" Width="300px" />
                                                                        <asp:Label runat="server" ID="LabelWarning_OP_IK_FS_UgyiratUgyintezo" Font-Size="Small" ForeColor="Red" Visible="false" Text="Mező letíltva szignálás miatt !" />
                                                                    </td>
                                                                </tr>

                                                                <%--  <tr>
                                                            <td style="width: 180px">
                                                                <asp:Label runat="server" Text="Ügy fajtája" Width="180px"></asp:Label>

                                                            </td>
                                                            <td style="width: 300px">
                                                                <asp:DropDownList ID="DropDownList_OP_IK_FS_UgyFajta" runat="server" Width="300px"></asp:DropDownList>

                                                            </td>
                                                        </tr>--%>
                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Irattári tétel" Width="180px"></asp:Label>

                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <asp:DropDownList ID="DropDownList_OP_IK_FS_IrattariTetel" runat="server" Width="300px"></asp:DropDownList>

                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Küldemény Felelõs csoport" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyFelelos" Validate="false" Width="300px" />
                                                                        <%--<asp:DropDownList ID="DropDownList_OP_IK_FS_KuldemenyFelelos" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Küldemény címzett csoport" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyCimzett" Validate="false" Width="300px" />
                                                                        <%--<asp:DropDownList ID="DropDownList_OP_IK_FS_KuldemenyCimzett" runat="server" Width="300px" AutoPostBack="true"></asp:DropDownList>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 180px">
                                                                        <asp:Label runat="server" Text="Ügyfajta / Ügytípus" Width="180px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 300px">
                                                                        <EUZT:UgyTipus_Complex_UserControl runat="server" ID="UgyTipus_Complex_UserContro_IKT_FSZ" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </table>

                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </div>





                                </ContentTemplate>




                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanel_Targy" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Fő művelet: Tárgy" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel_Header_Targy">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">&nbsp;&nbsp; TÁRGY
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblWarningTargy" runat="server" Visible="false" Text="A megadott fő művelet esetén nem értelmezett művelet" Style="padding: 5px; color: red; background-color: darkgray;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="Panel_Content_Targy">
                                        <table width="100%">
                                            <tr>
                                                <td class="mrUrlapCaption" style="width: 180px">
                                                    <asp:Label runat="server" Text="Tárgy szerkesztő:"></asp:Label>&nbsp;
                                                </td>
                                                <td>
                                                    <EUZT:EUzenetSzabalyTargyComplexControl runat="server" ID="EUzenetSzabalyTargyComplexControlInstance" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>




                                </ContentTemplate>




                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelPostOperation001" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Szignál" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelPOSzignalas">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Szignalas" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; SZIGNÁLÁS
                                                    <td>
                                                        <asp:Label ID="lblWarningSzignalas" runat="server" Visible="false" Text="A megadott fő művelet esetén nem értelmezett művelet" Style="padding: 5px; color: red; background-color: darkgray;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="PanelSzignalasBody" runat="server">
                                        <div style="margin: 20px;">
                                            <div style="max-width: 1200px; overflow-x: auto; white-space: nowrap;">



                                                <table>
                                                    <tr id="trSzignalasSzervezet" runat="server">
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Szervezet" Width="180px"></asp:Label>
                                                        </td>
                                                        <td style="width: 300px">
                                                            <ucCSOPTB:CsoportTextBox ID="Szervezet_UgySzignalas_CsoportTextBox" SzervezetCsoport="true" runat="server"
                                                                Validate="false" TryFireChangeEvent="true" />
                                                            <asp:Label runat="server" Font-Size="Small" ForeColor="Orange" Visible="true"
                                                                Text="Csak kitöltött szervezet esetén - szervezetre szignálás történik !" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Ügyintézõ" Width="180px"></asp:Label>
                                                        </td>
                                                        <td style="width: 300px">
                                                            <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo" Validate="false" Width="300px" />

                                                            <asp:Label runat="server" Font-Size="Small" ForeColor="Orange" Visible="true"
                                                                Text="Kitöltött ügyintéző esetén - Ügyintézőre szignálás történik !" />

                                                            <%--  <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo" Validate="false" Width="300px"
                                                                SajatSzervezetDolgozoiVagyOsszes="true" CsakSajatCsoportDolgozoi="false" CsakSajatSzervezetDolgozoi="false" />--%>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Feladat típus"></asp:Label>
                                                        </td>
                                                        <td style="width: 300px">
                                                            <asp:DropDownList runat="server" ID="DropDownList_PO_SZIGNALAS_Feladat_Tipus" Width="380px">
                                                                <asp:ListItem Text="Megjegyzes" Value="M"></asp:ListItem>
                                                                <asp:ListItem Text="Feladat" Value="F"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Kezelési utasítás" Width="180px"></asp:Label>
                                                        </td>
                                                        <td style="width: 300px">
                                                            <asp:TextBox ID="TextBox_PO_SZIGNALAS_KezelesiUtasitas" runat="server" Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Feladat szöveges kifejtese" Width="180px"></asp:Label>
                                                        </td>
                                                        <td style="width: 300px">
                                                            <asp:TextBox ID="TextBox_PO_SZIGNALAS_FeladatSzovegesen" runat="server" Width="300px"></asp:TextBox>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelPostOperation002" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Elintéz" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel11">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Elintez" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; ELINTÉZETTÉ NYILVÁNÍTÁS / ELEKTRONIKUS IRATTÁRBA TÉTEL
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblWarningElintezette" runat="server" Visible="false" Text="A megadott fő művelet esetén nem értelmezett művelet" Style="padding: 5px; color: red; background-color: darkgray;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel_ElintezetteNyilvanitasBody" runat="server">
                                        <div style="margin: 20px;">
                                            <table>
                                                <tr>
                                                    <td style="width: 180px">
                                                        <asp:Label runat="server" Text="Irattári tételszám" Width="180px"></asp:Label>
                                                    </td>
                                                    <td style="width: 300px">
                                                        <asp:TextBox ID="TextBox_PO_ELINTEZ_IrattariTetelszam" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 180px">
                                                        <asp:Label runat="server" Text="FT1" Width="180px"></asp:Label>
                                                    </td>
                                                    <td style="width: 300px">
                                                        <asp:DropDownList runat="server" ID="DropDownList_PO_ELINTEZ_FT1" Width="380px" OnSelectedIndexChanged="DropDownList_PO_ELINTEZ_FT1_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 180px">
                                                        <asp:Label runat="server" Text="FT2" Width="180px"></asp:Label>
                                                    </td>
                                                    <td style="width: 300px">

                                                        <asp:DropDownList runat="server" ID="DropDownList_PO_ELINTEZ_FT2" Width="380px" OnSelectedIndexChanged="DropDownList_PO_ELINTEZ_FT2_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 180px">
                                                        <asp:Label runat="server" Text="FT3" Width="180px"></asp:Label>
                                                    </td>
                                                    <td style="width: 300px">

                                                        <asp:DropDownList runat="server" ID="DropDownList_PO_ELINTEZ_FT3" Width="380px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelPostOperation003" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Értesít" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelErtesites">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Ertesites" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; ÉRTESÍTÉS
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="PanelPOErtesitesBody" runat="server">
                                        <div style="margin: 20px;">
                                            <div style="max-width: 1200px; overflow-x: auto; white-space: nowrap;">
                                                <table>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Email címzett(ek)" Width="180px"></asp:Label><br />
                                                            <small>Címzett elválasztó karakterek:  ',' ';' '|'</small>
                                                        </td>
                                                        <td style="width: 98%">
                                                            <asp:TextBox ID="TextBox_PO_ErtesitesCimzettek" runat="server" Width="98%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Email tárgya" Width="180px"></asp:Label>

                                                        </td>
                                                        <td style="width: 98%">
                                                            <asp:TextBox ID="TextBox_PO_Ertesites_EmailTargy" runat="server" Width="98%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 180px">
                                                            <asp:Label runat="server" Text="Email szövege" Width="180px"></asp:Label>
                                                        </td>
                                                        <td style="width: 98%">
                                                            <asp:TextBox ID="TextBox_PO_Ertesites_EmailSzoveg" runat="server" Width="98%" Rows="10" TextMode="MultiLine"></asp:TextBox>

                                                        </td>
                                                    </tr>

                                                </table>
                                                <br />
                                                Használható csere változók :
                                                <ul>
                                                    <li>$KR_HIVATKOZASISZAM$        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány KR_HivatkozasiSzam</li>
                                                    <li>$KR_ERKEZTETESISZAM$        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány KR_ErkeztetesiSzam</li>
                                                    <li>$KULDORENDSZER$             &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány KuldoRendszer</li>
                                                    <li>$PARTNERNEV$                &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány PartnerNev</li>
                                                    <li>$PARTNEREMAIL$              &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány PartnerEmail</li>
                                                    <li>$PR_ERKEZTETESISZAM$        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; eBeadvány PR_ErkeztetesiSzam</li>
                                                    <li>$IRAT_ID$                   &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; Irat egyedi azonosítója</li>
                                                    <li>$KULDEMENY_ID$              &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; Küldemény egyedi azonosítója</li>
                                                    <li>$AZONOSITO$                 &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; Irat/Küldemény azonosítója</li>
                                                    <li>$TARGY$                     &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; Irat/Küldemény tárgya</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                </ContentTemplate>




                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelPostOperation004" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Jogosultak" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelJogosultakHead">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Jogosultak" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; JOGOSULTAK
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblWarningJogosultak" runat="server" Visible="false" Text="A megadott fő művelet esetén nem értelmezett művelet" Style="padding: 5px; color: red; background-color: darkgray;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="PanelJogosultakBody" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td class="mrUrlapCaption" style="width: 180px">
                                                    <asp:Label ID="Label1" runat="server" Text="Kiválasztott jogosultak:"></asp:Label>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:ListBox runat="server" ID="ListBoxJogosultak" Rows="5" Width="100%"></asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption" style="width: 180px">
                                                    <asp:Label ID="Label_Csoport_Jogosult" runat="server" Text="Jogosult jelöltek:"></asp:Label>&nbsp;
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <ucCSOPTB:CsoportTextBox ID="Csoport_Id_JogosultTextBox" runat="server" Validate="false" />

                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption" style="width: 180px"></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:ImageButton TabIndex="1" ID="ImageSaveJogosultak" runat="server"
                                                        ImageUrl="~/images/hu/ovalgomb/hozzaadas.jpg" CausesValidation="False"
                                                        onmouseover="swapByName(this.id,'hozzaadas2.jpg')" onmouseout="swapByName(this.id,'hozzaadas.jpg')"
                                                        CommandName="Save" OnClick="ImageSaveJogosultak_Click" />
                                                    <asp:ImageButton TabIndex="1" ID="ImageButtonDeleteJogosultak" runat="server"
                                                        ImageUrl="~/images/hu/ovalgomb/torles.jpg" CausesValidation="False"
                                                        onmouseover="swapByName(this.id,'torles2.jpg')" onmouseout="swapByName(this.id,'torles.jpg')"
                                                        CommandName="Delete" OnClick="ImageDeleteJogosultak_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelPostOperation005" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Tárgyszavak" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelTargyszavakHead">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Targyszavak" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; TÁRGYSZAVAK
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblWarningTargyszavak" runat="server" Visible="false" Text="A megadott fő művelet esetén nem értelmezett művelet" Style="padding: 5px; color: red; background-color: darkgray;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="PaneltargyszavakBody" runat="server">
                                        <asp:GridView ID="GridViewTargyszavak"
                                            runat="server"
                                            CellPadding="0" BorderWidth="1px"
                                            AllowPaging="True" AllowSorting="True" OnRowDataBound="GridViewTargyszavak_RowDataBound"
                                            DataKeyNames="Id" AutoGenerateColumns="False" Width="98%" EnableModelValidation="True">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>

                                                <asp:BoundField DataField="Targyszo" HeaderText="Tárgyszó" SortExpression="Targyszo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>

                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="headerGridErtek" runat="server" CommandName="Sort" CommandArgument="Ertek">Érték</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <eUI:DynamicValueControl ID="TextBoxErtek" CssClass="mrUrlapInput" runat="server"
                                                            Value='<%# Eval("Ertek") %>'
                                                            ToolTip='<%# Eval("Ertek") %>'
                                                            Enabled="true"
                                                            DefaultControlTypeSource='<%# DefaultControlTypeSource_TextBoxErtek %>'
                                                            ControlTypeSource='<%# Convert.IsDBNull(Eval("ControlTypeSource")) ? DefaultControlTypeSource_TextBoxErtek : Eval("ControlTypeSource") %>'
                                                            Ismetlodo="False" ReadOnly="true" SearchMode="False" Validate="False" />
                                                        <asp:Label ID="labelGridControlTypeDataSource" runat="server" Text='<%# Eval("ControlTypeDataSource") %>' Visible="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="headerGridSorszam" runat="server" CommandName="Sort" CommandArgument="Sorszam">Sorszám</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBoxGridSorszam" runat="server" Text='<%# Eval("Sorszam") %>' Enabled="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="False">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="headerGridForras" runat="server" CommandName="Sort" CommandArgument="Forras">Forrás</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelGridForras" runat="server" Text='<%# Eval("Forras") %>' Enabled="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField Visible="False">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="headerGridRegExp" runat="server" CommandName="Sort" CommandArgument="RegExp">RegExp</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelGridRegExp" runat="server" Text='<%# Eval("RegExp") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="headerGridNote" runat="server" CommandName="Sort" CommandArgument="Note">Megjegyzés</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBoxGridNote" CssClass="GridViewTextBox" runat="server"
                                                            Text='<%# Eval("Note") %>' Enabled="False" />

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:Label ID="headerGridTipus" runat="server" Text="Típus" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelGridTipus" runat="server" Text='<%# Eval("Tipus") %>' Enabled="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:ImageButton TabIndex="1" ID="ImageButtonObjektumTargySzoTorles" runat="server"
                                                            ImageUrl="~/images/hu/egyeb/kisiksz.jpg" CausesValidation="false" CommandArgument='<%# Eval("Targyszo_Id") %>'
                                                            OnClick="ImageButtonObjektumTargySzoTorles_Click" CommandName="deleteit" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelGridRecordId" runat="server" Text='<%# Eval("Id") %>' />
                                                        <asp:Label ID="labelGridTargyszo_Id" runat="server" Text='<%# Eval("Targyszo_Id") %>' />
                                                        <asp:Label ID="labelGridObj_Metaadatai_Id" runat="server" Text='<%# Eval("Obj_Metaadatai_Id") %>' />
                                                        <asp:Label ID="labelGridTargyszo" runat="server" Text='<%# Eval("Targyszo") %>' />
                                                        <asp:Label ID="labelGridErvVege" runat="server" Text='<%# Eval("ErvVege") %>' />
                                                        <asp:Label ID="labelGridAlapertelmezettErtek" runat="server" Text='<%# Eval("AlapertelmezettErtek") %>' />

                                                        <asp:Label ID="labelGridToolTip" runat="server" Text='<%# Eval("ToolTip") %>' />


                                                        <asp:Label ID="labelGridErtek" runat="server" Text='<%# Eval("Ertek") %>' />
                                                        <asp:Label ID="labelGridErtekText" runat="server" Text='<%# Eval("Ertek") %>' />
                                                        <asp:Label ID="labelGridSorszam" runat="server" Text='<%# Eval("Sorszam") %>' />
                                                        <asp:Label ID="labelGridNote" runat="server" Text='<%# Eval("Note") %>' />

                                                        <asp:Label ID="labelGridOpcionalis" runat="server" Text='<%# Eval("Opcionalis") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>

                                        <table cellspacing="0" cellpadding="0" width="100%" align="center">

                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelForrasCaption" runat="server" Text="Forrás:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:RadioButton ID="rbSzabvanyos" runat="server" GroupName="rbgForras"
                                                        Checked="True" Text="Szabványos tárgyszó" Enabled="False"></asp:RadioButton>

                                                    <asp:RadioButton ID="rbEgyedi" runat="server" GroupName="rbgForras" Text="Egyedi tárgyszó" Enabled="False"></asp:RadioButton>

                                                    <asp:Label ID="labelForras" runat="server"></asp:Label>

                                                    <asp:HiddenField ID="Forras_HiddenField" runat="server" />
                                                    <asp:HiddenField ID="ObjMetaAdataiId_HiddenField" runat="server" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelRequiredTextBoxStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                    <asp:Label ID="labelTargySzavak" runat="server" Text="Tárgyszavak:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo" align="left" visible="false">
                                                    <tsztb:TargySzavakTextBox ID="TargySzavakTextBoxSzabvanyos" runat="server" Validate="false" OnTextChanged="TargySzavakTextBox_TextChanged" />

                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelErtekStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                    <asp:Label ID="labelErtek" runat="server" Text="Érték:" />
                                                </td>
                                                <td class="mrUrlapMezo">

                                                    <eUI:DynamicValueControl ID="TextBoxErtek" CssClass="mrUrlapInput" runat="server"
                                                        DefaultControlTypeSource='<%# DefaultControlTypeSource_TextBoxErtek %>'
                                                        Ismetlodo="False" ReadOnly="False" SearchMode="False" Validate="False" ViewMode="False" />
                                                    <asp:HiddenField ID="Regex_HiddenField" runat="server" />
                                                    <asp:Label ID="labelRegex" runat="server" Visible="False" />

                                                    <%--ControlTypeSource="System.Web.UI.WebControls.TextBox, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"--%> 
                                                </td>
                                            </tr>

                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelSorszam" runat="server" Text="Sorszám:"></asp:Label>
                                                </td>
                                                <td class="">
                                                    <asp:TextBox ID="TextBoxSorszam" runat="server" CssClass="NumberTextBoxShort" cols="40"></asp:TextBox>
                                                    <br />
                                                    <br />
                                                    <ajaxToolkit:NumericUpDownExtender ID="nupeSorszam" runat="server"
                                                        Minimum="0" Maximum="100" TargetControlID="TextBoxSorszam" Width="100" Enabled="True" RefValues="" ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" TargetButtonDownID="" TargetButtonUpID="" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                    <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelNote" runat="server" Text="Megjegyzés:"></asp:Label></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption"></td>
                                                <td class="mrUrlapMezo">
                                                    <asp:ImageButton TabIndex="1" ID="ImageButtonAddTargySzo" runat="server"
                                                        ImageUrl="~/images/hu/ovalgomb/hozzaadas.jpg" CausesValidation="False"
                                                        onmouseover="swapByName(this.id,'hozzaadas2.jpg')" onmouseout="swapByName(this.id,'hozzaadas.jpg')"
                                                        CommandName="Save" OnClick="ImageSaveTargySzoAdd_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>




                                </ContentTemplate>




                            </ajaxToolkit:TabPanel>



                            <ajaxToolkit:TabPanel ID="TabPanelPostOperationAutoValasz" runat="server" TabIndex="0" Visible="true">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Auto válasz" Style="padding: 5px"></asp:Label>




                                </HeaderTemplate>




                                <ContentTemplate>

                                    <asp:Panel runat="server" ID="PanelUtoMuveletekutoValasz" Style="margin-left: 15px;">
                                        <asp:Panel runat="server" ID="Panel13">
                                            <asp:Panel runat="server" ID="PanelPOAutoValasz">
                                                <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                                    <table>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:CheckBox runat="server" ID="CheckBox_ENABLE_AutoValasz" AutoPostBack="true" />
                                                            </td>
                                                            <td align="left">AUTO VÁLASZ  </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="PanelPOAutoValaszContent" runat="server">
                                                <div style="margin: 20px;">
                                                    <div style="max-width: 1200px; overflow-x: auto; white-space: nowrap;">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 180px">
                                                                    <asp:Label runat="server" Text="Riport_Szerver_Url" Width="180px"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:TextBox ID="TextBox_PO_AUTOVALASZ_RiportSzerverUrl" runat="server" Width="300px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 180px">
                                                                    <asp:Label runat="server" Text="Riport sablon" Width="180px"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:TextBox ID="TextBox_PO_AUTOVALASZ_RiportSablon" runat="server" Width="300px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 180px">
                                                                    <asp:Label runat="server" Text="Elektronikus Alairas Kell ?" Width="180px"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:CheckBox ID="CheckBox_PO_AUTOVALASZ_EAlairasKell" runat="server"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 180px">
                                                                    <asp:Label runat="server" Text="Iktatni Kell ?" Width="180px"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:CheckBox ID="CheckBox_PO_AUTOVALASZ_IktatniKell" runat="server"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 180px">
                                                                    <asp:Label runat="server" Text="Irattarozni Kell ?" Width="180px"></asp:Label>
                                                                </td>
                                                                <td style="width: 300px">
                                                                    <asp:CheckBox ID="CheckBox_PO_AUTOVALASZ_IrattarozniKell" runat="server"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabPanelCelRendszer" runat="server">
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Utó művelet: Cél rendszer" Style="padding: 5px"></asp:Label>
                                </HeaderTemplate>

                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="PanelCelRendszer">
                                        <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="CheckBox_ENABLE_CelRendszer" AutoPostBack="true" />
                                                    </td>
                                                    <td align="left">&nbsp;&nbsp; CÉL RENDSZER
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="Panel_PO_CNT_CelRendszer">
                                        <table width="100%">
                                            <tr>
                                                <td class="mrUrlapCaption" style="width: 180px">
                                                    <asp:Label runat="server" Text="Cél rendszer értéke:"></asp:Label>&nbsp;
                                                </td>
                                                <td>
                                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownListCelRendszer" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>




                                </ContentTemplate>




                            </ajaxToolkit:TabPanel>
                            <%--LZS - BLG_11345--%>
                            <%--BUG: https://stackoverflow.com/questions/21482411/ajax-control-toolkit-tabpanel-visible-false-causes-other-tab-panels-to-show-mix--%>
                            <%--Workaround: A hide-olt tabot a végére kell tenni, akkor nem lesz elcsúszás.--%>
                             <ajaxToolkit:TabPanel ID="TabPanelPostOperation006" runat="server" Visible="false">
                                 <HeaderTemplate>
                                     <asp:Label ID="label4" runat="server" Text="Utó művelet: Átadás" Style="padding: 5px"></asp:Label>




                                 </HeaderTemplate>




                                 <ContentTemplate>
                                     <asp:Panel runat="server" ID="PanelUtoMuveletekContent" Style="margin-left: 15px;">

                                         <asp:Panel runat="server" ID="PanelPOAtadas" Visible="false">
                                             <div style="font-weight: bold; background-color: darkslategrey; color: white; margin: 3px;">
                                                 <table>
                                                     <tr>
                                                         <td align="left">
                                                             <asp:CheckBox runat="server" ID="CheckBox_ENABLE_Atadas" AutoPostBack="true" />
                                                         </td>
                                                         <td align="left">&nbsp;&nbsp; ÁTADÁS
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </asp:Panel>
                                         <asp:Panel ID="PanelPOAtadasBody" runat="server">
                                             <div style="margin: 20px;">
                                                 <div style="max-width: 1200px; overflow-x: auto; white-space: nowrap;">
                                                     <table>
                                                         <tr>
                                                             <td style="width: 180px">
                                                                 <asp:Label runat="server" Text="Ügyintézõ" Width="180px"></asp:Label>
                                                             </td>
                                                             <td style="width: 300px">
                                                                 <ucFCS:FelhasznaloCsoportTextBox runat="server" ID="PO_ATADAS_FelhasznaloCsoportTextBox" Validate="false" Width="300px" />
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="width: 180px">
                                                                 <asp:Label runat="server" Text="Feladat típus"></asp:Label>
                                                             </td>
                                                             <td style="width: 300px">
                                                                 <asp:DropDownList runat="server" ID="DropDownList_PO_Atadas_Feladat_Tipus" Width="380px">
                                                                     <asp:ListItem Text="Megjegyzes" Value="M"></asp:ListItem>
                                                                     <asp:ListItem Text="Feladat" Value="F"></asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="width: 180px">
                                                                 <asp:Label runat="server" Text="Kezelési utasítás" Width="180px"></asp:Label>
                                                             </td>
                                                             <td style="width: 300px">
                                                                 <asp:TextBox ID="TextBox_PO_ATADAS_KezelesiUtasitas" runat="server" Width="300px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="width: 180px">
                                                                 <asp:Label runat="server" Text="Feladat szöveges kifejtese" Width="180px"></asp:Label>
                                                             </td>
                                                             <td style="width: 300px">
                                                                 <asp:TextBox ID="TextBox_PO_ATADAS_FeladatSzovegesKifejtese" runat="server" Width="300px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="width: 180px">
                                                                 <asp:Label runat="server" Text="Irattári tétel" Width="180px"></asp:Label>
                                                             </td>
                                                             <td style="width: 300px">
                                                                 <asp:TextBox ID="TextBox_PO_ATADAS_IrattariTetel" runat="server" Width="300px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                     </table>
                                                 </div>
                                             </div>
                                         </asp:Panel>

                                     </asp:Panel>




                                 </ContentTemplate>




                             </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </eUI:eFormPanel>
        <%--<uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>--%>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
        <br />
    </div>

</asp:Content>

