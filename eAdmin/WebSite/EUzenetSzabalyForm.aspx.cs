﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using Contentum.eUtility.EKF.Helper;
using Contentum.eUtility.EKF.Models;
using Contentum.eUtility.EKF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EUzenetSzabalyForm : Contentum.eUtility.UI.PageBase
{
    #region PARAMETERS
    #region MIX
    UI ui = new UI();
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private const string Nincs = "--Nem kitöltött--";
    private const string DDL_Default_Value = "--- Kérem válasszon ---";
    #endregion
    public enum FoMuveletTipus
    {
        ERKEZTET,
        IKTAT,
        IKTATAS_FOSZAMMAL,
        NINCS
    }

    #region CONSTANTS
    private const string ConstKrtTableName = "INT_AutoEUzenetSzabaly";
    private const string ConstPipeLine = "|";
    private const char ConstPipeLineChar = '|';
    private const string ConstDefaultOrderBy = "Nev ASC";

    protected const string DefaultControlTypeSource_TextBoxErtek = KodTarak.CONTROLTYPE_SOURCE.CustomTextBox;
    private string ViewStateKeyTargySzavakObjektumLista = "VIEWSTATE_KEY_TARGYSZAVAK_OBJEKTUM_LISTA";
    private const string kcs_UGYIRAT_INTEZESI_IDO = "UGYIRAT_INTEZESI_IDO";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";
    #endregion

    #region UTILITY
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }

    #endregion
    #endregion

    #region PAGE EVENTS
    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = true;
    }
    /// <summary>
    /// Az oldal Init eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        FelhasznaloCsoportTextBoxRuleUserId.TextBox.AutoPostBack = true;
        try
        {
            Command = Request.QueryString.Get(Contentum.eUtility.CommandName.Command);
            FormHeader1.HeaderTitle = Resources.List.EUzenetSzabalyListHeader;
            pageView = new Contentum.eUtility.PageView(Page, ViewState);
            FormFooter1.ImageButton_Close.OnClientClick = string.Empty;// "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";

            switch (Command)
            {
                case Contentum.eUtility.CommandName.DesignView:
                    Contentum.eUtility.FunctionRights.GetFunkcioJogRedirectErrorPage(Page, Contentum.eUtility.CommandName.DesignView);
                    compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                    FormFooter1.Controls.Add(compSelector);
                    break;
                default:
                    if (Command != null)
                        Contentum.eUtility.FunctionRights.GetFunkcioJogRedirectErrorPage(Page, ConstKrtTableName + Command);
                    else
                        Contentum.eUtility.FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INT_AutoEUzenetSzabalyKezeles");
                    break;
            }
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, exc.Message, "Hiba");
            Logger.Error("EUzenetSzabalyForm.Page_Init", exc);
        }

    }

    /// <summary>
    /// Oldal Load eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                InitControls();
                LoadValues();
            }
            else
            {
                SetFelhasznaloSzervezet();
            }
            ShowHideOperationPanels();
            SetIktatoKonvyEnabledStatus();
            SetErkeztetoKonvyEnabledStatus();
            FormFooter1.ButtonsClick += new System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

            SetVisibility();

        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, exc.Message, "Hiba");
            Logger.Error("EUzenetSzabalyForm.Page_Load", exc);
        }

        SetSignalControls();

        SetUtoMuveletekEnabledDisabled();
        SetUtoMuveletekClearIfUnChecked();

        SzignalasEsetenUgyiratUgyintezoTorlese();
        
    }

    private void SetSignalControls()
    {
        Szervezet_UgySzignalas_CsoportTextBox.TextBox.AutoPostBack = true;

        if (!string.IsNullOrEmpty(Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField))
            FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.FilterBySzervezetId(Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField);
    }

    /// <summary>
    /// SetUtoMuveletekEnabledDisabled
    /// </summary>
    private void SetUtoMuveletekEnabledDisabled()
    {
        if (CheckBox_ENABLE_Atadas.Checked)
            PanelPOAtadasBody.Enabled = true;
        else
            PanelPOAtadasBody.Enabled = false;

        if (CheckBox_ENABLE_Elintez.Checked)
            Panel_ElintezetteNyilvanitasBody.Enabled = true;
        else
            Panel_ElintezetteNyilvanitasBody.Enabled = false;

        if (CheckBox_ENABLE_Ertesites.Checked)
            PanelPOErtesitesBody.Enabled = true;
        else
            PanelPOErtesitesBody.Enabled = false;

        if (CheckBox_ENABLE_Jogosultak.Checked)
            PanelJogosultakBody.Enabled = true;
        else
            PanelJogosultakBody.Enabled = false;

        if (CheckBox_ENABLE_Szignalas.Checked)
            PanelSzignalasBody.Enabled = true;
        else
            PanelSzignalasBody.Enabled = false;

        if (CheckBox_ENABLE_Targyszavak.Checked)
            PaneltargyszavakBody.Enabled = true;
        else
            PaneltargyszavakBody.Enabled = false;

        if (CheckBox_ENABLE_AutoValasz.Checked)
            PanelPOAutoValaszContent.Enabled = true;
        else
            PanelPOAutoValaszContent.Enabled = false;

        if (CheckBox_ENABLE_CelRendszer.Checked)
            Panel_PO_CNT_CelRendszer.Enabled = true;
        else
            Panel_PO_CNT_CelRendszer.Enabled = false;
    }
    private void SetUtoMuveletekClearIfUnChecked()
    {
        if (Page.IsPostBack)
        {
            if (!CheckBox_ENABLE_Atadas.Checked)
            {
                PO_ATADAS_FelhasznaloCsoportTextBox.Id_HiddenField = string.Empty;
                PO_ATADAS_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                TextBox_PO_ATADAS_KezelesiUtasitas.Text = string.Empty;
                TextBox_PO_ATADAS_FeladatSzovegesKifejtese.Text = string.Empty;
                TextBox_PO_ATADAS_IrattariTetel.Text = string.Empty;
            }

            if (!CheckBox_ENABLE_Elintez.Checked)
            {
                TextBox_PO_ELINTEZ_IrattariTetelszam.Text = string.Empty;
                DropDownList_PO_ELINTEZ_FT1.SelectedValue = null;
                DropDownList_PO_ELINTEZ_FT2.SelectedValue = null;
                DropDownList_PO_ELINTEZ_FT3.SelectedValue = null;
            }

            if (!CheckBox_ENABLE_Ertesites.Checked)
            {
                TextBox_PO_ErtesitesCimzettek.Text = string.Empty;
                TextBox_PO_Ertesites_EmailTargy.Text = string.Empty;
                TextBox_PO_Ertesites_EmailSzoveg.Text = string.Empty;
            }

            if (!CheckBox_ENABLE_Jogosultak.Checked)
            {
                ListBoxJogosultak.Items.Clear();
                Csoport_Id_JogosultTextBox.Id_HiddenField = string.Empty;
                Csoport_Id_JogosultTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }

            if (!CheckBox_ENABLE_Szignalas.Checked)
            {
                FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.Id_HiddenField = string.Empty;
                FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField = string.Empty;
                Szervezet_UgySzignalas_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                TextBox_PO_SZIGNALAS_KezelesiUtasitas.Text = string.Empty;
                TextBox_PO_SZIGNALAS_FeladatSzovegesen.Text = string.Empty;
            }

            if (!CheckBox_ENABLE_Targyszavak.Checked)
            {
                GridViewTargyszavak.DataSource = null;
                GridViewTargyszavak.DataBind();

                ViewState[ViewStateKeyTargySzavakObjektumLista] = null;
            }

            if (!CheckBox_ENABLE_AutoValasz.Checked)
            {
                TextBox_PO_AUTOVALASZ_RiportSablon.Text = string.Empty;
                TextBox_PO_AUTOVALASZ_RiportSzerverUrl.Text = string.Empty;
                //DropDownList_PO_AUTOVALASZ_ExpedialasModja.SelectedValue = string.Empty;
            }
            if (!CheckBox_ENABLE_CelRendszer.Checked)
            {
                KodtarakDropDownListCelRendszer.SelectedValue = string.Empty;
            }
        }
    }
    private void SzignalasEsetenUgyiratUgyintezoTorlese()
    {
        if (CheckBox_ENABLE_Szignalas.Checked)
        {
            FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Id_HiddenField = null;
            FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Enabled = false;

            FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.Id_HiddenField = null;
            FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.Enabled = false;

            LabelWarning_IKT_UgyiratUgyintezo.Visible = true;
            LabelWarning_OP_IK_FS_UgyiratUgyintezo.Visible = true;
        }
        else
        {
            FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Enabled = true;
            FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.Enabled = true;

            LabelWarning_IKT_UgyiratUgyintezo.Visible = false;
            LabelWarning_OP_IK_FS_UgyiratUgyintezo.Visible = false;
        }
    }
    /// <summary>
    /// Mentés gomb folyamat indítása
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        ResultError.ResetErrorPanel(FormHeader1.ErrorPanel);
        SaveButtonProcedure(e);
    }
    #endregion

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            FormFooter1.SaveEnabled = false;
        }
    }

    /// <summary>
    /// Start save procedure
    /// </summary>
    /// <param name="e"></param>
    private void SaveButtonProcedure(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + Command))
            {
                if (e.CommandName == CommandName.Save)
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                InsertProcedure(false);
                                break;
                            }

                        case CommandName.Modify:
                            {
                                UpdateProcedure(false);
                                //String recordId = Request.Querystring.Get(QueryStringVars.Id);
                                //JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                break;
                            }
                    }
                }
                else if (e.CommandName == CommandName.SaveAndClose)
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                if (InsertProcedure(true))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                break;
                            }
                        case CommandName.Modify:
                            {
                                if (UpdateProcedure(true))
                                {
                                    string recordId = Request.QueryString.Get(QueryStringVars.Id);
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                break;
                            }
                    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
        else if (e.CommandName == "Close")
        {
            string recordId = Request.QueryString.Get(QueryStringVars.Id);
            JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    #region CRUD PROCEDURES
    /// <summary>
    /// Execute update rpocedure
    /// </summary>
    private bool UpdateProcedure(bool requireClose)
    {
        string recordId = GetId();
        if (string.IsNullOrEmpty(recordId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }
        else
        {
            Result result = Update(requireClose);
            if (string.IsNullOrEmpty(result.ErrorCode))
                return true;
            else
                return false;
        }
    }
    /// <summary>
    /// Execute insert procedure
    /// </summary>
    private bool InsertProcedure(bool requireClose)
    {
        Result result = Insert(requireClose);
        if (string.IsNullOrEmpty(result.ErrorCode))
            return true;
        else
            return false;
    }
    #endregion

    #region MAIN PROCEDURE
    /// <summary>
    /// Generate Rule
    /// </summary>
    /// <returns></returns>
    private EKFRuleClass GenerateUIRule()
    {
        #region GENERATE

        EKFRuleClass rule = new EKFRuleClass();
        string idOfRule = GetId();
        if (Command == CommandName.New)
        {
            if (string.IsNullOrEmpty(idOfRule))
                rule.IdProperty = Guid.NewGuid();
            else
                rule.IdProperty = new Guid(idOfRule);
        }
        else if (Command == CommandName.Modify)
        {
            if (string.IsNullOrEmpty(idOfRule))
                throw new Exception("Nem található az azonosító !");

            rule.IdProperty = new Guid(idOfRule);
        }
        int sorrend = 0;
        if (int.TryParse(TextBoxSorrend.Text, out sorrend))
            rule.OrderProperty = sorrend;
        else
            rule.OrderProperty = 0;

        //LZS - BLG_11345
        if (Panel_Content_Targy.Enabled)
        {
            GenerateTargyFormula(rule);
        }
        GenerateMainData(rule);
        GenerateFeltetelek(ref rule);
        GenerateMuveletek(ref rule);
        GenerateUtoMuveletek(ref rule);
        return rule;

        #endregion
    }
    /// <summary>
    /// GenerateTargyFormula
    /// </summary>
    /// <param name="rule"></param>
    private void GenerateTargyFormula(EKFRuleClass rule)
    {
        #region TARGY
        string targyForrasTipus;
        string targyFormula;
        string targyDefaultFormula;
        EUzenetSzabalyTargyComplexControlInstance.GetTargyFormula(out targyForrasTipus, out targyFormula, out targyDefaultFormula);
        rule.TargyForrasTipus = targyForrasTipus;
        rule.TargyFormula = targyFormula;
        rule.TargyAlapertelmezettFormula = targyDefaultFormula;
        #endregion
    }
    /// <summary>
    /// GenerateMainData
    /// </summary>
    /// <param name="rule"></param>
    private void GenerateMainData(EKFRuleClass rule)
    {
        EKFRuleParameters parametersProperty = new EKFRuleParameters();
        parametersProperty.DescriptionProperty = TextBoxRuleDetail.Text ?? "NA";
        parametersProperty.NameProperty = string.IsNullOrEmpty(TextBoxRuleName.Text) ? "NA" : TextBoxRuleName.Text;
        parametersProperty.StopRuleProperty = CheckBoxRuleIsStop.Checked;

        var type = (EnumEKFRuleType)Enum.Parse(typeof(EnumEKFRuleType), DropDownListRuleType.SelectedValue);
        parametersProperty.TypeProperty = type;
        parametersProperty.UserGroupProperty = new Guid(DropDownListRuleUserRoleId.SelectedValue);
        //parametersProperty.UserIdProperty = new Guid(DropDownListRuleUserId.SelectedValue);
        parametersProperty.UserIdProperty = new Guid(FelhasznaloCsoportTextBoxRuleUserId.Id_HiddenField);
        rule.ParametersProperty = parametersProperty;
    }

    private void GenerateFeltetelek(ref EKFRuleClass rule)
    {
        #region CONDITIONS

        Generate_Conditions_ForDB(ref rule);
        Generate_Conditions_ForXML(ref rule);

        if ((rule.Conditions == null || rule.Conditions.Count < 1) && (rule.ConditionsForXML == null || rule.ConditionsForXML.Count < 1))
        {
            throw new Exception("Hiányzó feltétel(ek) !");
        }

        #endregion
    }

    private void Generate_Conditions_ForDB(ref EKFRuleClass rule)
    {
        #region 1
        if (!string.IsNullOrEmpty(DropDownListConditionField_1.SelectedValue))
        {
            if (DropDownListConditionOperator_1.SelectedValue == EnumEKFConditionOperator.NULL_VAGY_URES.ToString())
            {
                EKFCondition condition_1 = new EKFCondition();
                condition_1.FieldProperty = DropDownListConditionField_1.SelectedValue;

                var ConditionOperator_1 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_1.SelectedValue);
                condition_1.OperatorProperty = ConditionOperator_1;

                rule.Conditions.Add(condition_1);
            }
            else if (!string.IsNullOrEmpty(TextBoxConditionValue_1.Text))
            {
                EKFCondition condition_1 = new EKFCondition();
                condition_1.FieldProperty = DropDownListConditionField_1.SelectedValue;

                var ConditionOperator_1 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_1.SelectedValue);
                condition_1.OperatorProperty = ConditionOperator_1;

                condition_1.ConditionProperty = TextBoxConditionValue_1.Text;
                rule.Conditions.Add(condition_1);
            }
        }
        #endregion 1

        #region 2
        if (!string.IsNullOrEmpty(DropDownListConditionField_2.SelectedValue))
        {
            if (DropDownListConditionOperator_2.SelectedValue == EnumEKFConditionOperator.NULL_VAGY_URES.ToString())
            {
                EKFCondition condition_2 = new EKFCondition();
                condition_2.FieldProperty = DropDownListConditionField_2.SelectedValue;

                var ConditionOperator_2 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_2.SelectedValue);
                condition_2.OperatorProperty = ConditionOperator_2;

                rule.Conditions.Add(condition_2);
            }
            else if (!string.IsNullOrEmpty(TextBoxConditionValue_2.Text))
            {
                EKFCondition condition_2 = new EKFCondition();
                condition_2.FieldProperty = DropDownListConditionField_2.SelectedValue;

                var ConditionOperator_2 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_2.SelectedValue);
                condition_2.OperatorProperty = ConditionOperator_2;

                condition_2.ConditionProperty = TextBoxConditionValue_2.Text;
                rule.Conditions.Add(condition_2);
            }
        }
        #endregion 2

        #region 3
        if (!string.IsNullOrEmpty(DropDownListConditionField_3.SelectedValue))
        {
            if (DropDownListConditionOperator_3.SelectedValue == EnumEKFConditionOperator.NULL_VAGY_URES.ToString())
            {
                EKFCondition condition_3 = new EKFCondition();
                condition_3.FieldProperty = DropDownListConditionField_3.SelectedValue;

                var ConditionOperator_3 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_3.SelectedValue);
                condition_3.OperatorProperty = ConditionOperator_3;

                rule.Conditions.Add(condition_3);
            }
            else if (!string.IsNullOrEmpty(TextBoxConditionValue_3.Text))
            {
                EKFCondition condition_3 = new EKFCondition();
                condition_3.FieldProperty = DropDownListConditionField_3.SelectedValue;

                var ConditionOperator_3 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_3.SelectedValue);
                condition_3.OperatorProperty = ConditionOperator_3;

                condition_3.ConditionProperty = TextBoxConditionValue_3.Text;
                rule.Conditions.Add(condition_3);
            }
        }
        #endregion 3

        #region 4
        if (!string.IsNullOrEmpty(DropDownListConditionField_4.SelectedValue))
        {
            if (DropDownListConditionOperator_4.SelectedValue == EnumEKFConditionOperator.NULL_VAGY_URES.ToString())
            {
                EKFCondition condition_4 = new EKFCondition();
                condition_4.FieldProperty = DropDownListConditionField_4.SelectedValue;

                var ConditionOperator_4 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_4.SelectedValue);
                condition_4.OperatorProperty = ConditionOperator_4;

                rule.Conditions.Add(condition_4);
            }
            else if (!string.IsNullOrEmpty(TextBoxConditionValue_4.Text))
            {
                EKFCondition condition_4 = new EKFCondition();
                condition_4.FieldProperty = DropDownListConditionField_4.SelectedValue;

                var ConditionOperator_4 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_4.SelectedValue);
                condition_4.OperatorProperty = ConditionOperator_4;

                condition_4.ConditionProperty = TextBoxConditionValue_4.Text;
                rule.Conditions.Add(condition_4);
            }
        }
        #endregion 4

        #region 5
        if (!string.IsNullOrEmpty(DropDownListConditionField_5.SelectedValue))
        {
            if (DropDownListConditionOperator_5.SelectedValue == EnumEKFConditionOperator.NULL_VAGY_URES.ToString())
            {
                EKFCondition condition_5 = new EKFCondition();
                condition_5.FieldProperty = DropDownListConditionField_5.SelectedValue;

                var ConditionOperator_5 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_5.SelectedValue);
                condition_5.OperatorProperty = ConditionOperator_5;

                rule.Conditions.Add(condition_5);
            }
            else if (!string.IsNullOrEmpty(TextBoxConditionValue_5.Text))
            {
                EKFCondition condition_5 = new EKFCondition();
                condition_5.FieldProperty = DropDownListConditionField_5.SelectedValue;

                var ConditionOperator_5 = (EnumEKFConditionOperator)Enum.Parse(typeof(EnumEKFConditionOperator), DropDownListConditionOperator_5.SelectedValue);
                condition_5.OperatorProperty = ConditionOperator_5;

                condition_5.ConditionProperty = TextBoxConditionValue_5.Text;
                rule.Conditions.Add(condition_5);
            }
        }
        #endregion 5
    }
    /// <summary>
    /// GenerateXMLRules
    /// </summary>
    /// <param name="rule"></param>
    private void Generate_Conditions_ForXML(ref EKFRuleClass rule)
    {
        if (EUzenetSzabaly_ConditionForXml_ComplexControl_A.HasConditions())
        {
            EUzenetSzabaly_ConditionForXml_ComplexControl_A.Check();

            List<EKFConditionForXML> conditionItems = null;
            EUzenetSzabaly_ConditionForXml_ComplexControl_A.GetBO(out conditionItems);
            if (conditionItems != null && conditionItems.Count > 0)
                rule.ConditionsForXML = conditionItems;
        }
    }
    private void GenerateMuveletek(ref EKFRuleClass rule)
    {
        #region MUVELETEK
        if (DropDownListOperationType.SelectedValue == FoMuveletTipus.ERKEZTET.ToString())
        {
            if (string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetokonyv.SelectedValue) && string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetoHely.SelectedValue))
            {
                throw new Exception("Érkeztetõkönyv vagy érkeztetőhely megadása kötelezõ !");
            }

            EKFMainOperationErkeztetes operationA = new EKFMainOperationErkeztetes();
            operationA.InitParameters();
            operationA.AddParameterValue(Enum_ParameterNames_Main_Erkeztetes.Felelos.ToString(), FelhasznaloCsoport_ERK_ErkeztetesFelelos.Id_HiddenField);

            operationA.AddParameterValue(Enum_ParameterNames_Main_Erkeztetes.Cimzett.ToString(), FelhasznaloCsoport_ERK_ErkeztetesCimzett.Id_HiddenField);
            operationA.AddParameterValue(Enum_ParameterNames_Main_Erkeztetes.ErkeztetoKonyv.ToString(), DropDownListOpErkeztetesErkeztetokonyv.SelectedValue);
            operationA.AddParameterValue(Enum_ParameterNames_Main_Erkeztetes.ErkeztetoHely.ToString(), DropDownListOpErkeztetesErkeztetoHely.SelectedValue);
            operationA.AddParameterValue(Enum_ParameterNames_Main_Erkeztetes.KezbesitesModja.ToString(), DropDownListOpErkeztetesKezbesitesModja.SelectedValue);

            rule.Operation = operationA;
        }
        else if (DropDownListOperationType.SelectedValue == FoMuveletTipus.IKTAT.ToString())
        {
            if (string.IsNullOrEmpty(DropDownListOpIktatasIktatoKonyv.SelectedValue) && string.IsNullOrEmpty(DropDownListIktatasIktatoKonyvHely.SelectedValue))
            {
                throw new Exception("Iktatókönyv megadása kötelezõ !");
            }
            if (string.IsNullOrEmpty(DropDownListOpIktatasErkeztetoKonyv.SelectedValue) && string.IsNullOrEmpty(DropDownListIktatasErkeztetoKonyvHely.SelectedValue))
            {
                throw new Exception("Érkeztetõkönyv vagy érkeztetőhely megadása kötelezõ !");
            }

            EKFMainOperationIktatas operationB = new EKFMainOperationIktatas();
            operationB.InitParameters();
            //operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Iktatas_Tipus.ToString(), TextBoxOpIktatasTipus.Text);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Iktatokonyv.ToString(), DropDownListOpIktatasIktatoKonyv.SelectedValue);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyv.ToString(), DropDownListOpIktatasErkeztetoKonyv.SelectedValue);

            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.IktatoKonyvHely.ToString(), DropDownListIktatasIktatoKonyvHely.SelectedValue);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyvHely.ToString(), DropDownListIktatasErkeztetoKonyvHely.SelectedValue);

            //operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Iratot_Olvashatjak.ToString(), "");
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.IratTipus.ToString(), DropDownListOpIktatasIratTipus.SelectedValue);
            //operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Irat_Cime.ToString(), "");
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Irat_Ugyintezo.ToString(), FelhasznaloCsoportTextBox_IKT_IratUgyintezo.Id_HiddenField);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.SzakrendszerKod.ToString(), TextBoxOpIktatasSzakrendszerKod.Text);

            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Ugyirat_Ugyintezo.ToString(), FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Id_HiddenField);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.IrattariTelel.ToString(), DropDownListOpIktatasIrattariTetel.SelectedValue);

            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Erkeztetes_Felelos.ToString(), FelhasznaloCsoportTextBox_IKT_KuldemenyFelelos.Id_HiddenField);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.Erkeztetes_Cimzett.ToString(), FelhasznaloCsoportTextBox_IKT_KuldemenyCimzett.Id_HiddenField);

            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString(), KodtarakDropDownList_OP_IK_IntezesiIdo.SelectedValue);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString(), KodtarakDropDownList_OP_IK_IntezesiIdoegyseg.SelectedValue);

            #region UGYFAJTA/TIPUS
            string ugyFajtaId, agazatiJelId, ugykorId, ugyTipusId;
            UgyTipus_Complex_UserControl_IKT.GetBO(out ugyFajtaId, out agazatiJelId, out ugykorId, out ugyTipusId);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_AgazatiJel.ToString(), agazatiJelId);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString(), ugykorId);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString(), ugyTipusId);
            operationB.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString(), ugyFajtaId);
            #endregion

            rule.Operation = operationB;
        }
        else if (DropDownListOperationType.SelectedValue == FoMuveletTipus.IKTATAS_FOSZAMMAL.ToString())
        {
            if (string.IsNullOrEmpty(DropDownList_OP_IK_FS_ErkeztetoKonyv.SelectedValue) && string.IsNullOrEmpty(DropDownList_OP_IK_FS_ErkeztetoKonyv.SelectedValue))
            {
                throw new Exception("Érkeztetõkönyv megadása kötelezõ !");
            }

            EKFOperationIktatasFoSzammal operationC = new EKFOperationIktatasFoSzammal();
            operationC.InitParameters();
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.ErkeztetoKonyv.ToString(), DropDownList_OP_IK_FS_ErkeztetoKonyv.SelectedValue);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.IratTipus.ToString(), DropDownList_OP_IK_FS_IratTipus.SelectedValue);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.Irat_Ugyintezo.ToString(), FelhasznaloCsoportTextBox_OP_IK_FS_IratUgyintezo.Id_HiddenField);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.SzakrendszerKod.ToString(), TextBox_OP_IK_FS_Szakrendszerkod.Text);

            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.Ugyirat_Ugyintezo.ToString(), FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.Id_HiddenField);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.IrattariTelel.ToString(), DropDownList_OP_IK_FS_IrattariTetel.SelectedValue);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.FoSzam.ToString(), TextBox_OP_IK_FS_FoSzam.Text);

            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Felelos.ToString(), FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyFelelos.Id_HiddenField);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Cimzett.ToString(), FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyCimzett.Id_HiddenField);

            #region UGYFAJTA/TIPUS
            string ugyFajtaId, agazatiJelId, ugykorId, ugyTipusId;
            UgyTipus_Complex_UserContro_IKT_FSZ.GetBO(out ugyFajtaId, out agazatiJelId, out ugykorId, out ugyTipusId);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_AgazatiJel.ToString(), agazatiJelId);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString(), ugykorId);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString(), ugyTipusId);
            operationC.AddParameterValue(Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString(), ugyFajtaId);
            #endregion

            rule.Operation = operationC;
        }

        #endregion
    }

    private void GenerateUtoMuveletek(ref EKFRuleClass rule)
    {
        #region UTOMUVELETEK

        #region TARGYSZO
        if (CheckBox_ENABLE_Targyszavak.Checked)
        {
            if (ViewState[ViewStateKeyTargySzavakObjektumLista] != null)
            {
                string json = ViewState[ViewStateKeyTargySzavakObjektumLista].ToString();
                if (!string.IsNullOrEmpty(json))
                {
                    List<EKF_ObjTargyszavakModel> listOriginal = JSONUtilityFunctions.DeSerializeEKFTargyszavakList(json);
                    if (listOriginal != null && listOriginal.Count > 0)
                    {
                        EKFPostOperationTargyszavak operationTARGY = new EKFPostOperationTargyszavak();
                        operationTARGY.InitParameters();
                        operationTARGY.AddParameterValue(Enum_ParameterNames_Post_Targyszavak.TargySzoObjektumLista.ToString(), json);
                        operationTARGY.AddParameterValue(Enum_ParameterNames_Post_Targyszavak.TargySzoObjektumTipusKod.ToString(), EnumTargySzoObjektumTipusKod.EREC_IraIratok.ToString());
                        rule.PostOperations.Add(operationTARGY);
                    }
                }
            }
        }
        #endregion

        #region ERTESITES
        if (CheckBox_ENABLE_Ertesites.Checked)
        {
            if (!string.IsNullOrEmpty(TextBox_PO_ErtesitesCimzettek.Text))
            {
                EKFPostOperationErtesites operationPOE = new EKFPostOperationErtesites();
                operationPOE.InitParameters();
                operationPOE.AddParameterValue(Enum_ParameterNames_Post_Ertesites.Email_Cimzettek.ToString(), TextBox_PO_ErtesitesCimzettek.Text);
                operationPOE.AddParameterValue(Enum_ParameterNames_Post_Ertesites.Email_Targy.ToString(), TextBox_PO_Ertesites_EmailTargy.Text);
                operationPOE.AddParameterValue(Enum_ParameterNames_Post_Ertesites.Email_Szovege.ToString(), TextBox_PO_Ertesites_EmailSzoveg.Text);
                rule.PostOperations.Add(operationPOE);
            }
        }
        #endregion

        #region ATADAS
        if (CheckBox_ENABLE_Atadas.Checked)
        {
            if (!string.IsNullOrEmpty(PO_ATADAS_FelhasznaloCsoportTextBox.Id_HiddenField))
            {
                EKFPostOperationAtadas operationPOA = new EKFPostOperationAtadas();
                operationPOA.InitParameters();
                operationPOA.AddParameterValue(Enum_ParameterNames_Post_Atadas.Szervezet_Ugyintezo.ToString(), PO_ATADAS_FelhasznaloCsoportTextBox.Id_HiddenField);
                operationPOA.AddParameterValue(Enum_ParameterNames_Post_Atadas.Feladat_Megjegyzes_Tipus.ToString(), DropDownList_PO_Atadas_Feladat_Tipus.SelectedValue);
                operationPOA.AddParameterValue(Enum_ParameterNames_Post_Atadas.Kezelesi_Utasitas.ToString(), TextBox_PO_ATADAS_KezelesiUtasitas.Text);
                operationPOA.AddParameterValue(Enum_ParameterNames_Post_Atadas.Feladat_Szoveges_Kifejtese.ToString(), TextBox_PO_ATADAS_FeladatSzovegesKifejtese.Text);
                operationPOA.AddParameterValue(Enum_ParameterNames_Post_Atadas.IrattariTetel.ToString(), TextBox_PO_ATADAS_IrattariTetel.Text);
                rule.PostOperations.Add(operationPOA);
            }
        }
        #endregion

        #region SZIGNAL
        if (CheckBox_ENABLE_Szignalas.Checked)
        {
            if (!string.IsNullOrEmpty(FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.Id_HiddenField) ||
                !string.IsNullOrEmpty(Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField))
            {
                EKFPostOperationSzignalas operationPOAV = new EKFPostOperationSzignalas();
                operationPOAV.InitParameters();
                operationPOAV.AddParameterValue(Enum_ParameterNames_Post_Szignalas.Feladat_Megjegyzes_Tipus.ToString(), DropDownList_PO_SZIGNALAS_Feladat_Tipus.SelectedValue);
                operationPOAV.AddParameterValue(Enum_ParameterNames_Post_Szignalas.Feladat_Szoveges_Kifejtese.ToString(), TextBox_PO_SZIGNALAS_FeladatSzovegesen.Text);
                operationPOAV.AddParameterValue(Enum_ParameterNames_Post_Szignalas.Kezelesi_Utasitas.ToString(), TextBox_PO_SZIGNALAS_KezelesiUtasitas.Text.ToString());
                operationPOAV.AddParameterValue(Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString(), FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.Id_HiddenField.ToString());
                operationPOAV.AddParameterValue(Enum_ParameterNames_Post_Szignalas.Szervezet.ToString(), Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField.ToString());

                rule.PostOperations.Add(operationPOAV);
            }
        }
        #endregion

        #region ELINTEZ
        if (CheckBox_ENABLE_Elintez.Checked)
        {
            if (!string.IsNullOrEmpty(TextBox_PO_ELINTEZ_IrattariTetelszam.Text))
            {
                EKFPostOperationElintezetteNyilvanitas operationPOENY = new EKFPostOperationElintezetteNyilvanitas();
                operationPOENY.InitParameters();
                operationPOENY.AddParameterValue(Enum_ParameterNames_Post_ElintezetteNyilvanitas.Irattari_Tetelszam.ToString(), TextBox_PO_ELINTEZ_IrattariTetelszam.Text);
                operationPOENY.AddParameterValue(Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT1.ToString(), DropDownList_PO_ELINTEZ_FT1.SelectedValue);
                operationPOENY.AddParameterValue(Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT2.ToString(), DropDownList_PO_ELINTEZ_FT2.SelectedValue);
                operationPOENY.AddParameterValue(Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT3.ToString(), DropDownList_PO_ELINTEZ_FT3.SelectedValue);
                rule.PostOperations.Add(operationPOENY);
            }
        }
        #endregion

        #region SAVE JOGOSULTAK
        if (CheckBox_ENABLE_Jogosultak.Checked)
        {
            if (ListBoxJogosultak.Items.Count > 0)
            {
                EKFPostOperationJogosultak operationJOGO = new EKFPostOperationJogosultak();
                operationJOGO.InitParameters();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < ListBoxJogosultak.Items.Count; i++)
                {
                    if (i == 0)
                        sb.Append(string.Format("{0}#{1}", ListBoxJogosultak.Items[i].Text, ListBoxJogosultak.Items[i].Value));
                    else
                        sb.Append(string.Format("{0}{1}#{2}", ConstPipeLine, ListBoxJogosultak.Items[i].Text, ListBoxJogosultak.Items[i].Value));
                }
                operationJOGO.AddParameterValue(Enum_ParameterNames_Post_Jogosultak.Jogosultak.ToString(), sb.ToString());

                rule.PostOperations.Add(operationJOGO);
            }
        }
        #endregion

        #region SAVE AUTO VALASZ
        if (CheckBox_ENABLE_AutoValasz.Checked)
        {
            EKFPostOperationAutoValasz operationPOAV = new EKFPostOperationAutoValasz();
            operationPOAV.InitParameters();

            operationPOAV.AddParameterValue(Enum_ParameterNames_Post_AutoValasz.Irattarozni_Kell.ToString(),
              CheckBox_PO_AUTOVALASZ_IrattarozniKell.Checked == true ? bool.TrueString : bool.FalseString);

            operationPOAV.AddParameterValue(Enum_ParameterNames_Post_AutoValasz.Iktatni_Kell.ToString(),
              CheckBox_PO_AUTOVALASZ_IktatniKell.Checked == true ? bool.TrueString : bool.FalseString);

            operationPOAV.AddParameterValue(Enum_ParameterNames_Post_AutoValasz.Elektronikus_Alairas_Kell.ToString(),
              CheckBox_PO_AUTOVALASZ_EAlairasKell.Checked == true ? bool.TrueString : bool.FalseString);

            operationPOAV.AddParameterValue(Enum_ParameterNames_Post_AutoValasz.Riport_Szerver_Url.ToString(),
              TextBox_PO_AUTOVALASZ_RiportSzerverUrl.Text);

            operationPOAV.AddParameterValue(Enum_ParameterNames_Post_AutoValasz.Valasz_Uzenet_Riport_Sablon.ToString(),
              TextBox_PO_AUTOVALASZ_RiportSablon.Text);

            rule.PostOperations.Add(operationPOAV);
        }
        #endregion

        #region SAVE CEL RENDSZER
        if (CheckBox_ENABLE_CelRendszer.Checked)
        {
            if (!string.IsNullOrEmpty(KodtarakDropDownListCelRendszer.SelectedValue))
            {
                EKFPostOperationCelRendszer operationCELREND = new EKFPostOperationCelRendszer();
                operationCELREND.InitParameters();

                operationCELREND.AddParameterValue(Enum_ParameterNames_Post_CelRendszer.CelRendszerErtek.ToString(),
                    KodtarakDropDownListCelRendszer.SelectedValue);
                rule.PostOperations.Add(operationCELREND);
            }
        }
        #endregion
        #endregion
    }

    /// <summary>
    /// Save rule
    /// </summary>
    /// <returns></returns>
    private Result SaveUIRule()
    {
        Result result = new Result();
        EKFRuleClass rule = null;
        try
        {
            rule = GenerateUIRule();
        }
        catch (Exception exc)
        {
            result.ErrorMessage = exc.Message;
            result.ErrorCode = "R001";
            return result;
        }
        if (rule == null)
            return result;

        bool existRule = false;
        Result resultGet = null;
        try
        {
            resultGet = GetAutoEUzenetSzabaly(rule.IdProperty.ToString());
            if (!string.IsNullOrEmpty(resultGet.ErrorCode))
                throw new Exception(resultGet.ErrorCode + " " + resultGet.ErrorMessage);

            existRule = true;
        }
        catch (Exception exc)
        {
            Logger.Debug(exc.Message);
        }

        if (existRule)
        {
            INT_AutoEUzenetSzabaly original = (INT_AutoEUzenetSzabaly)resultGet.Record;
            result = UpdateAutoEUzenetSzabaly(original, rule);
        }
        else
        {
            result = AddAutoEUzenetSzabaly(rule);
        }
        return result;
    }

    #endregion

    #region HELPER
    private TableCell GetDefaultTableCell(string text, bool isBold)
    {
        TableCell cell = new TableCell();
        //cell.Attributes.Add("style", "BORDER-TOP: ridge;BORDER - BOTTOM: ridge; BORDER - LEFT: ridge");
        cell.Attributes.Add("style", "BORDER: ridge;");
        if (!string.IsNullOrEmpty(text))
            cell.Text = text;
        cell.Font.Bold = isBold;
        return cell;
    }

    /// <summary>
    /// Láthatóságok beállítása
    /// </summary>
    private void SetVisibility()
    {
        if (Command == null)
        {
            EFormPanel1.Enabled = false;
            return;
        }

        switch (Command)
        {
            case CommandName.View:
                EFormPanel1.Enabled = false;
                break;
            case CommandName.New:
                EFormPanel1.Enabled = true;
                FormFooter1.ImageButton_Save.Visible = true;
                FormFooter1.ImageButton_SaveAndClose.Visible = true;
                FormFooter1.ImageButton_Close.Visible = true;
                break;
            case CommandName.Modify:
                EFormPanel1.Enabled = true;
                FormFooter1.ImageButton_Save.Visible = true;
                FormFooter1.ImageButton_SaveAndClose.Visible = true;
                FormFooter1.ImageButton_Close.Visible = true;
                break;
            default:
                break;
        }
    }
    private void SetErkeztetoKonvyEnabledStatus()
    {
        if (string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetoHely.SelectedValue) && string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetokonyv.SelectedValue))
        {
            DropDownListOpErkeztetesErkeztetoHely.Enabled = true;
            DropDownListOpErkeztetesErkeztetokonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetoHely.SelectedValue) && !string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetokonyv.SelectedValue))
        {
            DropDownListOpErkeztetesErkeztetoHely.Enabled = true;
            DropDownListOpErkeztetesErkeztetokonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetoHely.SelectedValue))
        {
            DropDownListOpErkeztetesErkeztetoHely.Enabled = true;
            DropDownListOpErkeztetesErkeztetokonyv.Enabled = false;
        }
        else if (!string.IsNullOrEmpty(DropDownListOpErkeztetesErkeztetokonyv.SelectedValue))
        {
            DropDownListOpErkeztetesErkeztetoHely.Enabled = false;
            DropDownListOpErkeztetesErkeztetokonyv.Enabled = true;
        }
    }
    private void SetIktatoKonvyEnabledStatus()
    {
        if (string.IsNullOrEmpty(DropDownListIktatasErkeztetoKonyvHely.SelectedValue) && string.IsNullOrEmpty(DropDownListOpIktatasErkeztetoKonyv.SelectedValue))
        {
            DropDownListIktatasErkeztetoKonyvHely.Enabled = true;
            DropDownListOpIktatasErkeztetoKonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListIktatasErkeztetoKonyvHely.SelectedValue) && !string.IsNullOrEmpty(DropDownListOpIktatasErkeztetoKonyv.SelectedValue))
        {
            DropDownListIktatasErkeztetoKonyvHely.Enabled = true;
            DropDownListOpIktatasErkeztetoKonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListIktatasErkeztetoKonyvHely.SelectedValue))
        {
            DropDownListIktatasErkeztetoKonyvHely.Enabled = true;
            DropDownListOpIktatasErkeztetoKonyv.Enabled = false;
        }
        else if (!string.IsNullOrEmpty(DropDownListOpIktatasErkeztetoKonyv.SelectedValue))
        {
            DropDownListIktatasErkeztetoKonyvHely.Enabled = false;
            DropDownListOpIktatasErkeztetoKonyv.Enabled = true;
        }


        if (string.IsNullOrEmpty(DropDownListIktatasIktatoKonyvHely.SelectedValue) && string.IsNullOrEmpty(DropDownListOpIktatasIktatoKonyv.SelectedValue))
        {
            DropDownListIktatasIktatoKonyvHely.Enabled = true;
            DropDownListOpIktatasIktatoKonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListIktatasIktatoKonyvHely.SelectedValue) && !string.IsNullOrEmpty(DropDownListOpIktatasIktatoKonyv.SelectedValue))
        {
            DropDownListIktatasIktatoKonyvHely.Enabled = true;
            DropDownListOpIktatasIktatoKonyv.Enabled = true;
        }
        else if (!string.IsNullOrEmpty(DropDownListIktatasIktatoKonyvHely.SelectedValue))
        {
            DropDownListIktatasIktatoKonyvHely.Enabled = true;
            DropDownListOpIktatasIktatoKonyv.Enabled = false;
        }
        else if (!string.IsNullOrEmpty(DropDownListOpIktatasIktatoKonyv.SelectedValue))
        {
            DropDownListIktatasIktatoKonyvHely.Enabled = false;
            DropDownListOpIktatasIktatoKonyv.Enabled = true;
        }
    }
    private void InitControls()
    {
        //DropDownListRuleUserId.Items.Clear();
        DropDownListRuleUserRoleId.Items.Clear();
        DropDownListRuleType.Items.Clear();

        DropDownListConditionOperator_1.Items.Clear();
        DropDownListConditionField_1.Items.Clear();
        DropDownListConditionOperator_2.Items.Clear();
        DropDownListConditionField_2.Items.Clear();
        DropDownListConditionOperator_3.Items.Clear();
        DropDownListConditionField_3.Items.Clear();
        DropDownListConditionOperator_4.Items.Clear();
        DropDownListConditionField_4.Items.Clear();
        DropDownListConditionOperator_5.Items.Clear();
        DropDownListConditionField_5.Items.Clear();

        FelhasznaloCsoport_ERK_ErkeztetesCimzett.Id_HiddenField = null;
        FelhasznaloCsoport_ERK_ErkeztetesFelelos.Id_HiddenField = null;

        FelhasznaloCsoportTextBox_IKT_IratUgyintezo.Id_HiddenField = null;
        FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Id_HiddenField = null;
        //DropDownListOpIktatasIratUgyintezo.Items.Clear();
        //DropDownListOpIktatasUgyiratUgyintezo.Items.Clear();
        DropDownListOpIktatasIratTipus.Items.Clear();

        //DropDownListSelectedEBeadvany.Items.Clear();

        DropDownListOpErkeztetesErkeztetokonyv.Items.Clear();
        DropDownListOpErkeztetesErkeztetoHely.Items.Clear();

        DropDownListOpIktatasErkeztetoKonyv.Items.Clear();
        DropDownListIktatasErkeztetoKonyvHely.Items.Clear();
        DropDownListIktatasIktatoKonyvHely.Items.Clear();

        foreach (EnumEKFRuleType name in Enum.GetValues(typeof(EnumEKFRuleType)))
        {
            DropDownListRuleType.Items.Add(new ListItem(name.ToString(), name.ToString()));
        }

        foreach (EnumEKFConditionOperator name in Enum.GetValues(typeof(EnumEKFConditionOperator)))
        {
            DropDownListConditionOperator_1.Items.Add(new ListItem(name.ToString(), name.ToString()));
            DropDownListConditionOperator_2.Items.Add(new ListItem(name.ToString(), name.ToString()));
            DropDownListConditionOperator_3.Items.Add(new ListItem(name.ToString(), name.ToString()));
            DropDownListConditionOperator_4.Items.Add(new ListItem(name.ToString(), name.ToString()));
            DropDownListConditionOperator_5.Items.Add(new ListItem(name.ToString(), name.ToString()));
        }

        System.Reflection.PropertyInfo[] myPropertyInfo = typeof(EREC_eBeadvanyokSearch).GetProperties();
        foreach (System.Reflection.PropertyInfo name in myPropertyInfo)
        {
            string item = name.ToString().Replace("Contentum.eQuery.BusinessDocuments.Field ", "");
            if (item.Contains("WhereByManual") ||
                item.Contains("OrderBy") ||
                item.Contains("TopRow") ||
                item.Contains("ErvKezd") ||
                item.Contains("ErvVege"))
                continue;

            DropDownListConditionField_1.Items.Add(new ListItem(item, item));
            DropDownListConditionField_2.Items.Add(new ListItem(item, item));
            DropDownListConditionField_3.Items.Add(new ListItem(item, item));
            DropDownListConditionField_4.Items.Add(new ListItem(item, item));
            DropDownListConditionField_5.Items.Add(new ListItem(item, item));
        }

        DropDownListConditionField_1.Items.Insert(0, new ListItem(DDL_Default_Value, string.Empty));
        DropDownListConditionField_2.Items.Insert(0, new ListItem(DDL_Default_Value, string.Empty));
        DropDownListConditionField_3.Items.Insert(0, new ListItem(DDL_Default_Value, string.Empty));
        DropDownListConditionField_4.Items.Insert(0, new ListItem(DDL_Default_Value, string.Empty));
        DropDownListConditionField_5.Items.Insert(0, new ListItem(DDL_Default_Value, string.Empty));

        DropDownListOpErkeztetesKezbesitesModja.FillDropDownList(KodTarak.KULD_KEZB_MODJA.KCS,
               true, FormHeader1.ErrorPanel);

        try
        {
            Result res = GetIktatoErkeztetoKonyvek(Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto);

            DropDownListOpErkeztetesErkeztetokonyv.Items.Add(new ListItem(Nincs, string.Empty));
            DropDownListOpErkeztetesErkeztetoHely.Items.Add(new ListItem(Nincs, string.Empty));

            DropDownListOpIktatasErkeztetoKonyv.Items.Add(new ListItem(Nincs, string.Empty));
            DropDownListIktatasErkeztetoKonyvHely.Items.Add(new ListItem(Nincs, string.Empty));

            Dictionary<string, string> erkeztetoHelyDistinct = new Dictionary<string, string>();
            foreach (DataRow item in res.Ds.Tables[0].Rows)
            {

                DropDownListOpErkeztetesErkeztetokonyv.Items.Add(
                    new ListItem(
                      GetIktatokonyvText(item),
                       item["Id"].ToString()));

                DropDownListOpIktatasErkeztetoKonyv.Items.Add(
                    new ListItem(
                     GetIktatokonyvText(item),
                       item["Id"].ToString()));

                if (!string.IsNullOrEmpty(item["MegkulJelzes"].ToString()))
                {
                    if (!erkeztetoHelyDistinct.ContainsKey(item["MegkulJelzes"].ToString()))
                    {
                        DropDownListIktatasErkeztetoKonyvHely.Items.Add(
                              new ListItem(item["MegkulJelzes"].ToString(), item["MegkulJelzes"].ToString()));

                        DropDownListOpErkeztetesErkeztetoHely.Items.Add(
                               new ListItem(item["MegkulJelzes"].ToString(), item["MegkulJelzes"].ToString()));

                        erkeztetoHelyDistinct.Add(item["MegkulJelzes"].ToString(), item["MegkulJelzes"].ToString());
                    }
                }
                DropDownList_OP_IK_FS_ErkeztetoKonyv.Items.Add(
                  new ListItem(
                     GetIktatokonyvText(item),
                     item["Id"].ToString()));
            }
        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
            return;
        }

        #region IKTATAS

        Dictionary<string, string> iratTipusok = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(Contentum.eUtility.KodTarak.IRATTIPUS.KodcsoportKod, Page);
        foreach (KeyValuePair<string, string> item in iratTipusok)
        {
            DropDownListOpIktatasIratTipus.Items.Add(new ListItem(item.Value, item.Key));
            DropDownList_OP_IK_FS_IratTipus.Items.Add(new ListItem(item.Value, item.Key));
        }

        try
        {
            Result res = GetIktatoErkeztetoKonyvek(Contentum.eUtility.Constants.IktatoErkezteto.Iktato);
            DropDownListOpIktatasIktatoKonyv.Items.Add(new ListItem(Nincs, string.Empty));
            DropDownListIktatasIktatoKonyvHely.Items.Add(new ListItem(Nincs, string.Empty));
            Dictionary<string, string> iktatoHelyDistinct = new Dictionary<string, string>();
            foreach (DataRow item in res.Ds.Tables[0].Rows)
            {
                DropDownListOpIktatasIktatoKonyv.Items.Add(
                    new ListItem(
                       GetIktatokonyvText(item),
                       item["Id"].ToString()));

                if (!string.IsNullOrEmpty(item["Iktatohely"].ToString()))
                {
                    if (!iktatoHelyDistinct.ContainsKey(item["Iktatohely"].ToString()))
                    {
                        DropDownListIktatasIktatoKonyvHely.Items.Add(
                          new ListItem(item["Iktatohely"].ToString(), item["Iktatohely"].ToString()));

                        iktatoHelyDistinct.Add(item["Iktatohely"].ToString(), item["Iktatohely"].ToString());
                    }
                }
            }
        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
            return;
        }
        try
        {
            Result res = GetIrattariTetelek();
            //DropDownListOpIktatasIrattariTetel.Items.Add(new ListItem(Nincs, string.Empty));
            DropDownListOpIktatasIrattariTetel.Items.Insert(0, new ListItem(Nincs, string.Empty));
            foreach (DataRow item in res.Ds.Tables[0].Rows)
            {
                DropDownListOpIktatasIrattariTetel.Items.Add(
                    new ListItem(
                        string.Format("{0} {1} {2}", item["IrattariTetelszam"].ToString(), item["Nev"].ToString(), item["IrattariJel"].ToString()),
                       item["Id"].ToString()));

                DropDownList_OP_IK_FS_IrattariTetel.Items.Add(
                    new ListItem(
                        string.Format("{0} {1} {2}", item["IrattariTetelszam"].ToString(), item["Nev"].ToString(), item["IrattariJel"].ToString()),
                       item["Id"].ToString()));
            }

        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
            return;
        }
        #endregion

        //DropDownList_PO_AUTOVALASZ_ExpedialasModja.FillDropDownList(KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod,
        //       true, FormHeader1.ErrorPanel);

        //#region INTEZESI IDO
        List<string> filterList = new List<string>();
        filterList.Add(KodTarak.IDOEGYSEG.Nap);
        filterList.Add(KodTarak.IDOEGYSEG.Munkanap);

        KodtarakDropDownList_OP_IK_IntezesiIdo.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, FormHeader1.ErrorPanel);
        KodtarakDropDownList_OP_IK_IntezesiIdoegyseg.FillDropDownList(kcs_IDOEGYSEG, filterList, false, FormHeader1.ErrorPanel);

        //KodtarakDropDownList_OP_IK_FS_IntezesiIdo.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, FormHeader1.ErrorPanel);
        //KodtarakDropDownList_OP_IK_FS_IntezesiIdoegyseg.FillDropDownList(kcs_IDOEGYSEG, filterList, false, FormHeader1.ErrorPanel);
        //#endregion

        LoadAndSetFT1();
        LoadAndSetFT2();
        LoadAndSetFT3();

        KodtarakDropDownListCelRendszer.FillDropDownList(KodTarak.eBeadvany_CelRendszer.KODCSOPORT, true, FormHeader1.ErrorPanel);


        lblInterfeszAdatlaptipus.Text = ForditasExpressionBuilder.GetForditas("lblInterfeszAdatlaptipus", "Interfész adatlaptípus:", this.AppRelativeVirtualPath);
    }



    private void SetFelhasznaloSzervezet()
    {
        try
        {
            DropDownListRuleUserRoleId.Items.Clear();
            if (!string.IsNullOrEmpty(FelhasznaloCsoportTextBoxRuleUserId.Id_HiddenField))
            {
                Result res = GetCsoportTagsagokByFelhasznalo(FelhasznaloCsoportTextBoxRuleUserId.Id_HiddenField);
                ListItem li;
                foreach (DataRow item in res.Ds.Tables[0].Rows)
                {
                    li = new ListItem(item["Csoport_Nev"].ToString(), item["Csoport_Id"].ToString());
                    if (!DropDownListRuleUserRoleId.Items.Contains(li))
                        DropDownListRuleUserRoleId.Items.Add(li);
                }
            }
        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
        }
    }

    //LZS - BLG_11345
    public void SetControlsForNINCS_Muvelet()
    {
        SetControlsEnableBase(false);
    }

    public void SetControlsForERKEZTET_Muvelet()
    {
        //Warning üzenetek láthatóságának beállítása:
        lblWarningElintezette.Visible = true;

        lblWarningTargyszavak.Visible =
        lblWarningTargy.Visible =
        lblWarningSzignalas.Visible =
        lblWarningJogosultak.Visible = false;

        //Checkboxok beállítása:
        CheckBox_ENABLE_Elintez.Enabled = false;

        CheckBox_ENABLE_Targyszavak.Enabled =
        CheckBox_ENABLE_Szignalas.Enabled =
        CheckBox_ENABLE_Jogosultak.Enabled =
        Panel_Content_Targy.Enabled = true;

        //Muvelet Tab többi mezőjének letiltása:
        PanelOperationErkeztetShowHideSub.Enabled =
        PanelOperationIktatasShowHideSub.Enabled =
        PanelOperationIktatasFoSzammalShowHideSub.Enabled = true;
    }

    public void SetControlsForIKTAT_IKTATAS_FOSZAMMAL_Muvelet()
    {
        SetControlsEnableBase(true);
    }

    private void SetControlsEnableBase(bool enable)
    {
        //Warning üzenetek láthatóságának beállítása:
        lblWarningTargyszavak.Visible =
        lblWarningTargy.Visible =
        lblWarningElintezette.Visible =
        lblWarningSzignalas.Visible =
        lblWarningJogosultak.Visible = !enable;

        //Checkboxok beállítása:
        CheckBox_ENABLE_Targyszavak.Enabled =
        CheckBox_ENABLE_Elintez.Enabled =
        CheckBox_ENABLE_Szignalas.Enabled =
        CheckBox_ENABLE_Jogosultak.Enabled =
        Panel_Content_Targy.Enabled = enable;

        //Muvelet Tab többi mezőjének letiltása:
        PanelOperationErkeztetShowHideSub.Enabled =
        PanelOperationIktatasShowHideSub.Enabled =
        PanelOperationIktatasFoSzammalShowHideSub.Enabled = enable;

    }

    public string GetIktatokonyvText(DataRow row)
    {
        string txtDelimeter = "/";
        //string valueDelimeter = "|";
        string nevBeginDelimeter = " (";
        string nevEndDelimeter = ")";

        string iktatohely = row["Iktatohely"].ToString();
        string megkulonboztetoJelzes = row["MegkulJelzes"].ToString();
        string nev = row["Nev"].ToString();
        string ev = row["Ev"].ToString();

        string text = iktatohely;
        if (!string.IsNullOrEmpty(megkulonboztetoJelzes))
        {
            if (!string.IsNullOrEmpty(text))
                text += txtDelimeter;

            text += megkulonboztetoJelzes;
        }

        if (!string.IsNullOrEmpty(nev))
        {
            text += nevBeginDelimeter + nev + nevEndDelimeter + " [" + ev + "]";
        }

        return text;
    }

    private void JavascriptMessageBox(Exception exc)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
      "Hiba",
      "alert('EK= " + exc.Message + "');",
      true);
        return;
    }
    /// <summary>
    /// return id of item
    /// </summary>
    /// <returns></returns>
    public string GetId()
    {
        if (!string.IsNullOrEmpty(Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Id)))
            return Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Id);
        else if (!string.IsNullOrEmpty(HiddenField_Id_InsertedItem.Value))
            return HiddenField_Id_InsertedItem.Value;
        else
            return string.Empty;
    }
    private void LoadValues()
    {
        Result result;
        try
        {
            string recordId = GetId();

            result = GetAutoEUzenetSzabaly(recordId);
            INT_AutoEUzenetSzabaly szabaly = (INT_AutoEUzenetSzabaly)result.Record;

            EKFRuleClass rule = EKFManager.EKFDeSerialize(szabaly.Szabaly);
            TextBoxRuleDetail.Text = rule.ParametersProperty.DescriptionProperty;
            TextBoxRuleName.Text = rule.ParametersProperty.NameProperty;
            CheckBoxRuleIsStop.Checked = rule.ParametersProperty.StopRuleProperty.HasValue ? rule.ParametersProperty.StopRuleProperty.Value : false;
            TextBoxSorrend.Text = szabaly.Sorrend;

            #region TARGY
            EUzenetSzabalyTargyComplexControlInstance.SetTargyFormula(rule.TargyForrasTipus, rule.TargyFormula, rule.TargyAlapertelmezettFormula);
            #endregion

            DropDownListRuleType.SelectedValue = rule.ParametersProperty.TypeProperty.ToString();

            //DropDownListRuleUserId.SelectedValue = rule.ParametersProperty.UserIdProperty.ToString();

            FelhasznaloCsoportTextBoxRuleUserId.Id_HiddenField = rule.ParametersProperty.UserIdProperty.ToString();
            FelhasznaloCsoportTextBoxRuleUserId.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            SetFelhasznaloSzervezet();
            DropDownListRuleUserRoleId.SelectedValue = rule.ParametersProperty.UserGroupProperty.ToString();

            #region CONDITIONS
            if (rule.Conditions != null && rule.Conditions.Count > 0)
            {
                for (int i = 0; i < rule.Conditions.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            if (!string.IsNullOrEmpty(rule.Conditions[0].FieldProperty))
                            {
                                DropDownListConditionField_1.SelectedValue = rule.Conditions[0].FieldProperty;
                                DropDownListConditionOperator_1.SelectedValue = rule.Conditions[0].OperatorProperty.ToString();
                                TextBoxConditionValue_1.Text = rule.Conditions[0].ConditionProperty;
                            }
                            break;
                        case 1:
                            if (!string.IsNullOrEmpty(rule.Conditions[1].FieldProperty))
                            {
                                DropDownListConditionField_2.SelectedValue = rule.Conditions[1].FieldProperty;
                                DropDownListConditionOperator_2.SelectedValue = rule.Conditions[1].OperatorProperty.ToString();
                                TextBoxConditionValue_2.Text = rule.Conditions[1].ConditionProperty;
                            }
                            break;
                        case 2:
                            if (!string.IsNullOrEmpty(rule.Conditions[2].FieldProperty))
                            {
                                DropDownListConditionField_3.SelectedValue = rule.Conditions[2].FieldProperty;
                                DropDownListConditionOperator_3.SelectedValue = rule.Conditions[2].OperatorProperty.ToString();
                                TextBoxConditionValue_3.Text = rule.Conditions[2].ConditionProperty;
                            }
                            break;
                        case 3:
                            if (!string.IsNullOrEmpty(rule.Conditions[3].FieldProperty))
                            {
                                DropDownListConditionField_4.SelectedValue = rule.Conditions[3].FieldProperty;
                                DropDownListConditionOperator_4.SelectedValue = rule.Conditions[3].OperatorProperty.ToString();
                                TextBoxConditionValue_4.Text = rule.Conditions[3].ConditionProperty;
                            }
                            break;
                        case 4:
                            if (!string.IsNullOrEmpty(rule.Conditions[4].FieldProperty))
                            {
                                DropDownListConditionField_5.SelectedValue = rule.Conditions[4].FieldProperty;
                                DropDownListConditionOperator_5.SelectedValue = rule.Conditions[4].OperatorProperty.ToString();
                                TextBoxConditionValue_5.Text = rule.Conditions[4].ConditionProperty;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            if (rule.ConditionsForXML != null && rule.ConditionsForXML.Count > 0)
            {
                EUzenetSzabaly_ConditionForXml_ComplexControl_A.SetBO(rule.ConditionsForXML);
            }
            #endregion

            //LZS - BLG_11345
            if (rule.Operation == null)
            {
                DropDownListOperationType.SelectedValue = FoMuveletTipus.NINCS.ToString();
            }
            else if (rule.Operation.TypeFullName.Contains(new EKFMainOperationErkeztetes().GetType().FullName))
            {
                DropDownListOperationType.SelectedValue = FoMuveletTipus.ERKEZTET.ToString();

                FelhasznaloCsoport_ERK_ErkeztetesFelelos.Id_HiddenField =
                    EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.Felelos.ToString());
                FelhasznaloCsoport_ERK_ErkeztetesFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                FelhasznaloCsoport_ERK_ErkeztetesCimzett.Id_HiddenField =
                    EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.Cimzett.ToString());
                FelhasznaloCsoport_ERK_ErkeztetesCimzett.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                DropDownListOpErkeztetesErkeztetokonyv.SelectedValue =
                    EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.ErkeztetoKonyv.ToString());

                DropDownListOpErkeztetesErkeztetoHely.SelectedValue =
                 EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.ErkeztetoHely.ToString());

                DropDownListOpErkeztetesKezbesitesModja.SelectedValue =
                    EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.KezbesitesModja.ToString());
            }
            else if (rule.Operation.TypeFullName.Contains(new EKFMainOperationIktatas().GetType().FullName))
            {
                DropDownListOperationType.SelectedValue = FoMuveletTipus.IKTAT.ToString();

                //TextBoxOpIktatasTipus.Text
                //    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Iktatas_Tipus.ToString());
                DropDownListOpIktatasIktatoKonyv.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Iktatokonyv.ToString());
                DropDownListOpIktatasErkeztetoKonyv.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyv.ToString());

                DropDownListIktatasErkeztetoKonyvHely.SelectedValue
                          = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyvHely.ToString());
                DropDownListIktatasIktatoKonyvHely.SelectedValue
                          = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.IktatoKonyvHely.ToString());

                DropDownListOpIktatasIratTipus.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.IratTipus.ToString());

                FelhasznaloCsoportTextBox_IKT_IratUgyintezo.Id_HiddenField
                //DropDownListOpIktatasIratUgyintezo.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Irat_Ugyintezo.ToString());
                FelhasznaloCsoportTextBox_IKT_IratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                TextBoxOpIktatasSzakrendszerKod.Text
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.SzakrendszerKod.ToString());

                #region ugytipus/ugyfajta
                string ugyfajta = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString());
                string agazatijel = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_AgazatiJel.ToString());
                string ugykor = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString());
                string ugytipus = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString());

                UgyTipus_Complex_UserControl_IKT.SetBO(ugyfajta, agazatijel, ugykor, ugytipus);
                #endregion

                FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.Id_HiddenField
                   //DropDownListOpIktatasUgyiratUgyintezo.SelectedValue
                   = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Ugyirat_Ugyintezo.ToString());
                FelhasznaloCsoportTextBox_IKT_UgyiratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                DropDownListOpIktatasIrattariTetel.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.IrattariTelel.ToString());

                FelhasznaloCsoportTextBox_IKT_KuldemenyFelelos.Id_HiddenField
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Erkeztetes_Felelos.ToString());
                FelhasznaloCsoportTextBox_IKT_KuldemenyFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                FelhasznaloCsoportTextBox_IKT_KuldemenyCimzett.Id_HiddenField
                     = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.Erkeztetes_Cimzett.ToString());
                FelhasznaloCsoportTextBox_IKT_KuldemenyCimzett.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                KodtarakDropDownList_OP_IK_IntezesiIdo.SelectedValue = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString());
                KodtarakDropDownList_OP_IK_IntezesiIdoegyseg.SelectedValue = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString());

            }
            else if (rule.Operation.TypeFullName.Contains(new EKFOperationIktatasFoSzammal().GetType().FullName))
            {
                DropDownListOperationType.SelectedValue = FoMuveletTipus.IKTATAS_FOSZAMMAL.ToString();
                DropDownList_OP_IK_FS_ErkeztetoKonyv.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.ErkeztetoKonyv.ToString());
                DropDownList_OP_IK_FS_IratTipus.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.IratTipus.ToString());

                FelhasznaloCsoportTextBox_OP_IK_FS_IratUgyintezo.Id_HiddenField
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Irat_Ugyintezo.ToString());
                FelhasznaloCsoportTextBox_OP_IK_FS_IratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                TextBox_OP_IK_FS_Szakrendszerkod.Text
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.SzakrendszerKod.ToString()); ;

                #region ugytipus/ugyfajta
                string ugyfajta = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyFajta.ToString());
                string agazatijel = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_AgazatiJel.ToString());
                string ugykor = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_Ugykor.ToString());
                string ugytipus = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString());

                UgyTipus_Complex_UserContro_IKT_FSZ.SetBO(ugyfajta, agazatijel, ugykor, ugytipus);
                #endregion

                FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.Id_HiddenField
                   = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Ugyirat_Ugyintezo.ToString());
                FelhasznaloCsoportTextBox_OP_IK_FS_UgyiratUgyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                DropDownList_OP_IK_FS_IrattariTetel.SelectedValue
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.IrattariTelel.ToString());

                TextBox_OP_IK_FS_FoSzam.Text
                    = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.FoSzam.ToString());

                FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyFelelos.Id_HiddenField
                      = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Felelos.ToString());
                FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyCimzett.Id_HiddenField
                     = EKFManager.GetEKFOperationParameterValue(rule.Operation.ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Cimzett.ToString());
                FelhasznaloCsoportTextBox_OP_IK_FS_KuldemenyCimzett.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            }

            #region POST OPERATIONS
            if (rule.PostOperations != null && rule.PostOperations.Count > 0)
            {
                for (int i = 0; i < rule.PostOperations.Count; i++)
                {
                    if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationErtesites().GetType().FullName))
                    {
                        CheckBox_ENABLE_Ertesites.Checked = true;

                        TextBox_PO_ErtesitesCimzettek.Text = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Cimzettek.ToString());
                        TextBox_PO_Ertesites_EmailTargy.Text = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Targy.ToString());
                        TextBox_PO_Ertesites_EmailSzoveg.Text = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Szovege.ToString());
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationSzignalas().GetType().FullName))
                    {
                        CheckBox_ENABLE_Szignalas.Checked = true;

                        FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.Id_HiddenField
                         = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString());
                        FelhasznaloCsoportTextBox_PO_SZIGNALAS_Ugyintezo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                        Szervezet_UgySzignalas_CsoportTextBox.Id_HiddenField
                         = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet.ToString());
                        Szervezet_UgySzignalas_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                        DropDownList_PO_SZIGNALAS_Feladat_Tipus.SelectedValue
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Megjegyzes_Tipus.ToString());
                        TextBox_PO_SZIGNALAS_KezelesiUtasitas.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Szignalas.Kezelesi_Utasitas.ToString());
                        TextBox_PO_SZIGNALAS_FeladatSzovegesen.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Szoveges_Kifejtese.ToString());
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationAtadas().GetType().FullName))
                    {
                        CheckBox_ENABLE_Atadas.Checked = true;

                        PO_ATADAS_FelhasznaloCsoportTextBox.Id_HiddenField
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Atadas.Szervezet_Ugyintezo.ToString());
                        PO_ATADAS_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                        DropDownList_PO_Atadas_Feladat_Tipus.SelectedValue
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Atadas.Feladat_Megjegyzes_Tipus.ToString());
                        TextBox_PO_ATADAS_KezelesiUtasitas.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Atadas.Kezelesi_Utasitas.ToString());
                        TextBox_PO_ATADAS_FeladatSzovegesKifejtese.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Atadas.Feladat_Szoveges_Kifejtese.ToString());
                        TextBox_PO_ATADAS_IrattariTetel.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Atadas.IrattariTetel.ToString());
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationAutoValasz().GetType().FullName))
                    {
                        CheckBox_ENABLE_AutoValasz.Checked = true;

                        CheckBox_PO_AUTOVALASZ_IrattarozniKell.Checked
                              = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Irattarozni_Kell.ToString()) == bool.TrueString ? true : false;
                        CheckBox_PO_AUTOVALASZ_IktatniKell.Checked
                              = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Iktatni_Kell.ToString()) == bool.TrueString ? true : false;
                        CheckBox_PO_AUTOVALASZ_EAlairasKell.Checked
                              = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Elektronikus_Alairas_Kell.ToString()) == bool.TrueString ? true : false;
                        TextBox_PO_AUTOVALASZ_RiportSzerverUrl.Text
                               = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Riport_Szerver_Url.ToString());
                        TextBox_PO_AUTOVALASZ_RiportSablon.Text
                               = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Valasz_Uzenet_Riport_Sablon.ToString());
                    }

                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationElintezetteNyilvanitas().GetType().FullName))
                    {
                        CheckBox_ENABLE_Elintez.Checked = true;

                        TextBox_PO_ELINTEZ_IrattariTetelszam.Text
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.Irattari_Tetelszam.ToString());
                        DropDownList_PO_ELINTEZ_FT1.SelectedValue
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT1.ToString());
                        DropDownList_PO_ELINTEZ_FT2.SelectedValue
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT2.ToString());
                        DropDownList_PO_ELINTEZ_FT3.SelectedValue
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT3.ToString());
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationJogosultak().GetType().FullName))
                    {
                        CheckBox_ENABLE_Jogosultak.Checked = true;

                        string jogosultakList
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Jogosultak.Jogosultak.ToString());
                        string[] paramA = jogosultakList.Split(new char[] { ConstPipeLineChar });
                        if (paramA != null)
                        {
                            foreach (var itemA in paramA)
                            {
                                string[] paramB = itemA.Split('#');
                                if (paramB.Length == 2)
                                {
                                    ListBoxJogosultak.Items.Add(new ListItem(paramB[0], paramB[1]));
                                }
                            }
                        }
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationTargyszavak().GetType().FullName))
                    {
                        CheckBox_ENABLE_Targyszavak.Checked = true;

                        string targyszavakList
                            = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_Targyszavak.TargySzoObjektumLista.ToString());

                        List<EKF_ObjTargyszavakModel> listOriginal = JSONUtilityFunctions.DeSerializeEKFTargyszavakList(targyszavakList);
                        GridViewTargyszavak.DataSource = listOriginal;
                        GridViewTargyszavak.DataBind();

                        ViewState[ViewStateKeyTargySzavakObjektumLista] = targyszavakList;
                    }
                    else if (rule.PostOperations[i].TypeFullName.Contains(new EKFPostOperationCelRendszer().GetType().FullName))
                    {
                        CheckBox_ENABLE_CelRendszer.Checked = true;
                        KodtarakDropDownListCelRendszer.SelectedValue
                               = EKFManager.GetEKFOperationParameterValue(rule.PostOperations[i].ParameterProperty, Enum_ParameterNames_Post_CelRendszer.CelRendszerErtek.ToString());
                    }
                }
                #endregion

                ShowHideOperationPanels();
            }
        }
        catch (Exception exc)
        {
            Logger.Error("LoadValues", exc);
            return;
        }
    }
    protected void DropDownListOperationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideOperationPanels();
    }

    private void ShowHideOperationPanels()
    {
        if (DropDownListOperationType.SelectedValue == FoMuveletTipus.ERKEZTET.ToString())
        {
            PanelOperationErkeztet.Visible = true;
            PanelOperationIktat.Visible = false;
            PanelOperationIktatasFoSzammal.Visible = false;

            //LZS - BLG_11345
            //Beállítjuk a controlokat:
            SetControlsForERKEZTET_Muvelet();
        }
        else if (DropDownListOperationType.SelectedValue == FoMuveletTipus.IKTAT.ToString())
        {
            PanelOperationErkeztet.Visible = false;
            PanelOperationIktat.Visible = true;
            PanelOperationIktatasFoSzammal.Visible = false;

            //LZS - BLG_11345
            //Beállítjuk a controlokat:
            SetControlsForIKTAT_IKTATAS_FOSZAMMAL_Muvelet();
        }
        else if (DropDownListOperationType.SelectedValue == FoMuveletTipus.IKTATAS_FOSZAMMAL.ToString())
        {
            PanelOperationIktatasFoSzammal.Visible = true;
            PanelOperationErkeztet.Visible = false;
            PanelOperationIktat.Visible = false;

            //LZS - BLG_11345
            //Beállítjuk a controlokat:
            SetControlsForIKTAT_IKTATAS_FOSZAMMAL_Muvelet();
        }//LZS - BLG_11345
        else if (DropDownListOperationType.SelectedValue == FoMuveletTipus.NINCS.ToString())
        {
            //Beállítjuk a controlokat:
            SetControlsForNINCS_Muvelet();
        }
    }
    #endregion

    #region SERVICES
    /// <summary>
    /// Létrehozás
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    private Result Insert(bool requireClose)
    {
        Result result = SaveUIRule();
        if (result == null)
            return null;

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            HiddenField_Id_InsertedItem.Value = result.Uid;
            if (requireClose)
            {
                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, string.Empty))
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
                else
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        return result;
    }
    /// <summary>
    /// Módosítás
    /// </summary>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    private Result Update(bool requireClose)
    {
        Result result = SaveUIRule();
        if (result == null)
            return null;
        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            if (requireClose)
            {
                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, string.Empty))
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
                else
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        return result;
    }

    private Result GetIktatoErkeztetoKonyvek(string iktatoErkezteto)
    {
        Contentum.eRecord.Service.EREC_IraIktatoKonyvekService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
        search.IktatoErkezteto.Value = iktatoErkezteto;
        search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

        //search.LezarasDatuma.Value = "";
        //search.LezarasDatuma.Operator = Contentum.eQuery.Query.Operators.isnull;

        search.OrderBy = "Iktatohely";

        Result res = service.GetAll(ExecParam, search);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new Contentum.eUtility.ResultException(res);

        return res;
    }
    private Result GetPartnerek()
    {
        Contentum.eAdmin.Service.KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_PartnerekSearch searchPa = new KRT_PartnerekSearch();
        KRT_CimekSearch searchCi = new KRT_CimekSearch();
        searchCi.OrderBy = " Nev ASC";
        Result res = service.GetAllWithCim(ExecParam, searchPa, searchCi);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new Contentum.eUtility.ResultException(res);

        return res;
    }
    private Result GetFelhasznalok()
    {
        Contentum.eAdmin.Service.KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_FelhasznalokSearch searchPa = new KRT_FelhasznalokSearch();
        searchPa.OrderBy = " Nev ASC";

        Result res = service.GetAll(ExecParam, searchPa);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new Contentum.eUtility.ResultException(res);

        return res;
    }
    private Result GetCsoportTagsagokByFelhasznalo(string felhasznaloOd)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

        search_csoportok.Csoport_Id_Jogalany.Value = felhasznaloOd;
        search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        search_csoportok.OrderBy = "Csoport_Nev ASC";

        Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);
        if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
            throw new Contentum.eUtility.ResultException(result_csoportok);

        return result_csoportok;
    }
    private Result GetCsoportok()
    {
        Contentum.eAdmin.Service.KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_CsoportokSearch searchPa = new KRT_CsoportokSearch();
        searchPa.OrderBy = " Nev ASC";

        Result res = service.GetAll(ExecParam, searchPa);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new Contentum.eUtility.ResultException(res);

        return res;
    }
    private Result GetAutoEUzenetSzabalyok()
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        INT_AutoEUzenetSzabalyService ekfService = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        INT_AutoEUzenetSzabalySearch search = new INT_AutoEUzenetSzabalySearch();
        search.OrderBy = "Sorrend ASC";
        Result resultGetRules = ekfService.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(resultGetRules.ErrorCode))
            throw new Contentum.eUtility.ResultException(resultGetRules);
        return resultGetRules;
    }

    private Result GetAutoEUzenetSzabaly(string id)
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        INT_AutoEUzenetSzabalyService ekfService = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        execParam.Record_Id = id;
        Result resultGetRules = ekfService.Get(execParam);

        if (!string.IsNullOrEmpty(resultGetRules.ErrorCode))
            throw new Contentum.eUtility.ResultException(resultGetRules);
        return resultGetRules;
    }
    private Result GetEBeadvanyok()
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eRecord.Service.EREC_eBeadvanyokService eBeadvService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
        search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
        Result eBeadvanyResult = eBeadvService.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
            throw new Contentum.eUtility.ResultException(eBeadvanyResult);
        return eBeadvanyResult;
    }

    private Result GetIrattariTetelek()
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eRecord.Service.EREC_IraIrattariTetelekService svc = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
        Result result = svc.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result.ErrorCode))
            throw new Contentum.eUtility.ResultException(result);
        return result;
    }

    private Result AddAutoEUzenetSzabaly(EKFRuleClass rule)
    {
        INT_AutoEUzenetSzabaly szabaly = new INT_AutoEUzenetSzabaly();
        szabaly.Id = rule.IdProperty.ToString();
        szabaly.Szabaly = EKFManager.EKFSerialize(rule);
        szabaly.Leallito = rule.ParametersProperty.StopRuleProperty.Value.ToString();
        szabaly.Leiras = rule.ParametersProperty.DescriptionProperty;
        szabaly.Nev = rule.ParametersProperty.NameProperty ?? "NA";
        szabaly.Tipus = rule.ParametersProperty.TypeProperty.ToString();
        szabaly.Sorrend = rule.OrderProperty.ToString();
        szabaly.Base.Note = rule.ToString();

        INT_AutoEUzenetSzabalyService ekfInstance = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result resultInsert = ekfInstance.Insert(execParam, szabaly);
        if (!string.IsNullOrEmpty(resultInsert.ErrorCode))
            throw new Exception(resultInsert.ErrorCode + " " + resultInsert.ErrorMessage);

        return resultInsert;
    }
    private Result UpdateAutoEUzenetSzabaly(INT_AutoEUzenetSzabaly szabaly, EKFRuleClass rule)
    {
        szabaly.Szabaly = EKFManager.EKFSerialize(rule);
        szabaly.Leallito = rule.ParametersProperty.StopRuleProperty.Value.ToString();
        szabaly.Leiras = rule.ParametersProperty.DescriptionProperty;
        szabaly.Nev = rule.ParametersProperty.NameProperty ?? "NA";
        szabaly.Tipus = rule.ParametersProperty.TypeProperty.ToString();
        szabaly.Sorrend = rule.OrderProperty.ToString();
        szabaly.Base.Note = rule.ToString();

        INT_AutoEUzenetSzabalyService ekfInstance = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = szabaly.Id;
        Result resultUpdate = ekfInstance.Update(execParam, szabaly);
        if (!string.IsNullOrEmpty(resultUpdate.ErrorCode))
            throw new Exception(resultUpdate.ErrorCode + " " + resultUpdate.ErrorMessage);

        resultUpdate.Uid = execParam.Record_Id;

        return resultUpdate;
    }
    #endregion

    #region FT TARGYSZAVAK UGYSTATISZTIKA
    private void LoadAndSetFT1()
    {
        #region T1
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodTarakService svc_kodtarT1 = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        KRT_KodTarakSearch src_kodtarakT1 = new KRT_KodTarakSearch();
        src_kodtarakT1.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T1' ");

        Result resT1 = svc_kodtarT1.GetAllWithKodcsoport(execParam, src_kodtarakT1);

        if (resT1.IsError || resT1.Ds == null)
        {
            throw new ResultException(resT1);
        }
        DropDownList_PO_ELINTEZ_FT1.Items.Clear();
        foreach (DataRow row in resT1.Ds.Tables[0].Rows)
        {
            DropDownList_PO_ELINTEZ_FT1.Items.Add(new ListItem(row["Nev"].ToString(), row["Kod"].ToString()));
        }
        #endregion T1
    }
    private void LoadAndSetFT2()
    {
        #region T2
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodTarakService svc_kodtarT2 = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        KRT_KodTarakSearch src_kodtarakT2 = new KRT_KodTarakSearch();
        src_kodtarakT2.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T2' ");

        Result resT2 = svc_kodtarT2.GetAllWithKodcsoport(execParam, src_kodtarakT2);

        if (resT2.IsError || resT2.Ds == null)
        {
            throw new ResultException(resT2);
        }
        DropDownList_PO_ELINTEZ_FT2.Items.Clear();
        foreach (DataRow row in resT2.Ds.Tables[0].Rows)
        {
            DropDownList_PO_ELINTEZ_FT2.Items.Add(new ListItem(row["Nev"].ToString(), row["Kod"].ToString()));
        }
        #endregion T2
    }
    private void LoadAndSetFT3()
    {
        #region T3
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodTarakService svc_kodtarT3 = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        KRT_KodTarakSearch src_kodtarakT3 = new KRT_KodTarakSearch();
        src_kodtarakT3.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T3' ");

        Result resT3 = svc_kodtarT3.GetAllWithKodcsoport(execParam, src_kodtarakT3);

        if (resT3.IsError || resT3.Ds == null)
        {
            throw new ResultException(resT3);
        }
        DropDownList_PO_ELINTEZ_FT3.Items.Clear();
        foreach (DataRow row in resT3.Ds.Tables[0].Rows)
        {
            DropDownList_PO_ELINTEZ_FT3.Items.Add(new ListItem(row["Nev"].ToString(), row["Kod"].ToString()));
        }
        #endregion T3
    }

    private string GetKodtarNevFromResult(Result result, string kod)
    {
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            if (row["Id"].ToString() == kod)
                return row["Nev"].ToString();
        }
        return null;
    }

    #endregion

    #region EVENTS
    protected void DropDownList_PO_ELINTEZ_FT1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void DropDownList_PO_ELINTEZ_FT2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ImageSaveJogosultak_Click(object sender, ImageClickEventArgs e)
    {
        if (!string.IsNullOrEmpty(Csoport_Id_JogosultTextBox.Id_HiddenField))
        {
            ListItem li = new ListItem(Csoport_Id_JogosultTextBox.Text, Csoport_Id_JogosultTextBox.Id_HiddenField);
            if (!ListBoxJogosultak.Items.Contains(li))
                ListBoxJogosultak.Items.Add(li);
        }
    }

    protected void ImageDeleteJogosultak_Click(object sender, ImageClickEventArgs e)
    {
        if (!string.IsNullOrEmpty(ListBoxJogosultak.SelectedValue))
            ListBoxJogosultak.Items.Remove(ListBoxJogosultak.SelectedItem);
    }
    #endregion

    #region TARGYSZAVAK


    protected void ImageSaveTargySzoAdd_Click(object sender, ImageClickEventArgs e)
    {
        List<EKF_ObjTargyszavakModel> result = AddNewTargySzoModel();
        GridViewTargyszavak.DataSource = result;

        GridViewTargyszavak.DataBind();
    }


    /// <summary>
    /// DeleteSelectedTargySzo
    /// </summary>
    /// <param name="id"></param>
    private void DeleteSelectedTargySzo(string id)
    {
        if (ViewState[ViewStateKeyTargySzavakObjektumLista] != null)
        {
            object obj = ViewState[ViewStateKeyTargySzavakObjektumLista];

            List<EKF_ObjTargyszavakModel> listOriginal = JSONUtilityFunctions.DeSerializeEKFTargyszavakList(obj.ToString());
            List<EKF_ObjTargyszavakModel> listResult = new List<EKF_ObjTargyszavakModel>();
            foreach (EKF_ObjTargyszavakModel item in listOriginal)
            {
                if (item.Targyszo_Id != id)
                    listResult.Add(item);
            }

            string json = JSONUtilityFunctions.SerializeEKFTargyszavakList(listResult);
            ViewState[ViewStateKeyTargySzavakObjektumLista] = json;
            GridViewTargyszavak.DataSource = listResult;
        }

        GridViewTargyszavak.DataBind();
    }
    /// <summary>
    /// Add new targySzo to list and viewstate 
    /// </summary>
    /// <returns></returns>
    private List<EKF_ObjTargyszavakModel> AddNewTargySzoModel()
    {
        List<EKF_ObjTargyszavakModel> list = new List<EKF_ObjTargyszavakModel>();
        EREC_ObjektumTargyszavai objektumTargySzavai = GetUjTargySzoBOFromComponents();

        if (objektumTargySzavai != null)
        {
            EKF_ObjTargyszavakModel model = Converter_EKF.Convert(objektumTargySzavai);
            EREC_TargySzavak targySzo = GetTargySzoById(model.Targyszo_Id);
            if (targySzo != null)
                Converter_EKF.Add(ref model, targySzo);

            if (ViewState[ViewStateKeyTargySzavakObjektumLista] == null)
            {
                list = new List<EKF_ObjTargyszavakModel>();
                list.Add(model);
                string json = JSONUtilityFunctions.SerializeEKFTargyszavakList(list);
                ViewState.Add(ViewStateKeyTargySzavakObjektumLista, json);
            }
            else
            {
                object obj = ViewState[ViewStateKeyTargySzavakObjektumLista];
                list = JSONUtilityFunctions.DeSerializeEKFTargyszavakList(obj.ToString());
                list.Add(model);
                string json = JSONUtilityFunctions.SerializeEKFTargyszavakList(list);
                ViewState[ViewStateKeyTargySzavakObjektumLista] = json;
            }
        }
        return list;
    }

    /// <summary>
    /// GetUjTargySzoBOFromComponents
    /// </summary>
    /// <returns></returns>
    private EREC_ObjektumTargyszavai GetUjTargySzoBOFromComponents()
    {
        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

        if (rbSzabvanyos.Checked == true)
        {
            erec_ObjektumTargyszavai.Targyszo_Id = TargySzavakTextBoxSzabvanyos.Id_HiddenField;
            erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

            erec_ObjektumTargyszavai.Targyszo = TargySzavakTextBoxSzabvanyos.Text;
            erec_ObjektumTargyszavai.Updated.Targyszo = true;

            erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Listas; // Szervezeti
            erec_ObjektumTargyszavai.Updated.Forras = true;
        }
        else
            if (rbEgyedi.Checked == true)
        {
            erec_ObjektumTargyszavai.Targyszo_Id = "";
            erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

            erec_ObjektumTargyszavai.Targyszo = TargySzavakTextBoxSzabvanyos.Text;
            erec_ObjektumTargyszavai.Updated.Targyszo = true;

            erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Kezi; // Kézi
            erec_ObjektumTargyszavai.Updated.Forras = true;
        }
        string value = GetDynamicValueControlErtek(TextBoxErtek);
        if (!string.IsNullOrEmpty(TextBoxErtek.Value))
            erec_ObjektumTargyszavai.Ertek = TextBoxErtek.Value; //TextBoxErtek.Text;

        erec_ObjektumTargyszavai.Updated.Ertek = pageView.GetUpdatedByView(TextBoxErtek);

        erec_ObjektumTargyszavai.Sorszam = TextBoxSorszam.Text;
        erec_ObjektumTargyszavai.Updated.Sorszam = pageView.GetUpdatedByView(TextBoxSorszam);

        erec_ObjektumTargyszavai.ObjTip_Id = "";
        erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

        erec_ObjektumTargyszavai.Base.Note = textNote.Text;
        erec_ObjektumTargyszavai.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        return erec_ObjektumTargyszavai;
    }
    /// <summary>
    /// GetTargySzoById
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private EREC_TargySzavak GetTargySzoById(string id)
    {
        EREC_TargySzavakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        Result result = service.Get(execParam);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            Logger.Error("EUzenetSzabalyForm.GetTargySzoById", execParam, result);
            return null;
        }

        return result.Record as EREC_TargySzavak;
    }

    protected void ImageButtonObjektumTargySzoTorles_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgBtn = sender as CustomFunctionImageButton;
        if (imgBtn != null && !string.IsNullOrEmpty(imgBtn.CommandArgument))
            DeleteSelectedTargySzo(imgBtn.CommandArgument);
    }
    protected void TargySzavakTextBox_TextChanged(object sender, EventArgs e)
    {
        if (TargySzavakTextBoxSzabvanyos.Visible)
        {
            if (string.IsNullOrEmpty(TargySzavakTextBoxSzabvanyos.Id_HiddenField))
            {
                rbEgyedi.Checked = true;
                rbSzabvanyos.Checked = false;
                labelErtekStar.Visible = false;
                //TextBoxErtek.Text = string.Empty;
                TextBoxErtek.ControlTypeSource = DefaultControlTypeSource_TextBoxErtek;
                TextBoxErtek.Value = string.Empty;
                SetControlByControlSourceType(TextBoxErtek, null, FormHeader1.ErrorPanel);
                SetTextBoxReadOnly(TextBoxErtek, true, "mrUrlapInput");

                //RegExValidator.Enabled = false;
                labelRegex.Text = "";
                TextBoxErtek.ToolTip = "";
            }
            else
            {
                rbEgyedi.Checked = false;
                rbSzabvanyos.Checked = true;
                SetTextBoxReadOnly(TextBoxErtek, (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? true : false), "mrUrlapInput");
                labelRegex.Text = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? "" : TargySzavakTextBoxSzabvanyos.RegExp);
                TextBoxErtek.Value = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? "" : TargySzavakTextBoxSzabvanyos.AlapertelmezettErtek);
                TextBoxErtek.ControlTypeSource = (TargySzavakTextBoxSzabvanyos.Tipus == "0" ? DefaultControlTypeSource_TextBoxErtek : TargySzavakTextBoxSzabvanyos.ControlTypeSource);
                SetControlByControlSourceType(TextBoxErtek, TargySzavakTextBoxSzabvanyos.ControlTypeDataSource, FormHeader1.ErrorPanel);
                labelErtekStar.Visible = (TargySzavakTextBoxSzabvanyos.Tipus == "1" ? true : false);
                if (TargySzavakTextBoxSzabvanyos.Tipus == "0")
                {
                    TextBoxErtek.ToolTip = "";
                }
                else
                {
                    TextBoxErtek.ToolTip = GetErtekToolTip(TargySzavakTextBoxSzabvanyos.ToolTip, TargySzavakTextBoxSzabvanyos.RegExp);
                }
            }
        }

    }
    private void SetControlByControlSourceType(Contentum.eUIControls.DynamicValueControl dvc, string ControlTypeDataSource, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (dvc != null)
        {
            if (!string.IsNullOrEmpty(dvc.ControlTypeName))
            {
                if (dvc.ControlTypeName.Equals("component_kodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        if (string.IsNullOrEmpty(dvc.Value))
                        {
                            dvc.Control.GetType().GetMethod("FillAndSetEmptyValue"
                                , new Type[] { typeof(string), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, errorPanel });
                        }
                        else
                        {
                            dvc.Control.GetType().GetMethod("FillAndSetSelectedValue"
                                , new Type[] { typeof(String), typeof(String), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, dvc.Value, errorPanel });
                        }
                    }
                }

                if (dvc.ControlTypeName.Equals("component_fuggokodtarakdropdownlist_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        dvc.Control.GetType().GetProperty("KodcsoportKod").SetValue(dvc.Control, ControlTypeDataSource, null);

                    }

                    //if (hf_Targyszo_ParentTargyszo != null && !string.IsNullOrEmpty(hf_Targyszo_ParentTargyszo.Value))
                    //{
                    //    DynamicValueControl parentTargyszo = this.FindControlByTargyszo(hf_Targyszo_ParentTargyszo.Value);
                    //    if (parentTargyszo != null)
                    //    {
                    //        dvc.Control.GetType().GetProperty("ParentControlId").SetValue(dvc.Control, parentTargyszo.Control.UniqueID, null);
                    //    }
                    //}
                }
                // BLG_608
                if (dvc.ControlTypeName.Equals("component_kodtaraklistbox_ascx", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(ControlTypeDataSource))
                    {
                        dvc.Control.GetType().GetProperty("Ismetlodo").SetValue(dvc.Control, dvc.Ismetlodo, null);
                        if (string.IsNullOrEmpty(dvc.Value))
                        {
                            dvc.Control.GetType().GetMethod("FillListBox", new Type[] { typeof(string), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control, new object[] { ControlTypeDataSource, errorPanel });
                        }
                        else
                        {
                            // string[] selectedValues= dvc.Value.Split('|');
                            dvc.Control.GetType().GetMethod("FillAndSetSelectedValues"
                                , new Type[] { typeof(String), typeof(String), typeof(Contentum.eUIControls.eErrorPanel) }).Invoke(dvc.Control
                                , new object[] { ControlTypeDataSource, dvc.Value, errorPanel });
                        }
                    }
                }
            }

            if (dvc.Control is UI.ILovListTextBox)
            {
                dvc.Control.GetType().GetMethod("SetTextBoxById"
                    , new Type[] { typeof(Contentum.eUIControls.eErrorPanel), typeof(UpdatePanel) }).Invoke(dvc.Control
                        , new object[] { errorPanel, null });
            }
        }
    }
    protected void SetTextBoxReadOnly(TextBox tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        //tbox.Enabled = !isReadOnly;
        if (!isReadOnly)
        {
            // visszaállítjuk a stílust
            tbox.CssClass = normalCssClass;
        }

    }
    protected void SetTextBoxReadOnly(Contentum.eUIControls.DynamicValueControl tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        //tbox.Enabled = !isReadOnly;
        if (!isReadOnly)
        {
            // visszaállítjuk a stílust
            tbox.CssClass = normalCssClass;

            // balsõ textbox stílusállítás
            TextBox tb = GetTextBoxFromDVC(tbox);
            if (tb != null)
            {
                SetTextBoxReadOnly(tb, isReadOnly, normalCssClass);
            }
        }

    }

    protected String GetErtekToolTip(string ToolTip, string RegExp)
    {
        String strToolTip = null;
        if (!string.IsNullOrEmpty(ToolTip))
        {
            strToolTip = ToolTip;
            if (!string.IsNullOrEmpty(RegExp))
            {
                strToolTip += "\nRegExp: " + RegExp;
            }
        }
        else if (!string.IsNullOrEmpty(RegExp))
        {
            strToolTip += "RegExp: " + RegExp;
        }
        return strToolTip;

    }
    protected TextBox GetTextBoxFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        TextBox tb = null;
        if (dvc != null)
        {
            if (dvc.Control != null && dvc.Control is TextBox)
            {
                tb = (TextBox)dvc.Control;
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megnézzük, hogy van-e textbox a vezérlõk között, az elsõt kivesszük
                        tb = (TextBox)c;
                        break;
                    }
                }
            }
        }

        return tb;
    }
    /// <summary>
    /// GridViewTargyszavak_RowDataBound
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridViewTargyszavak_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Contentum.eUIControls.DynamicValueControl dvc = (Contentum.eUIControls.DynamicValueControl)e.Row.FindControl("TextBoxErtek");
            if (dvc != null)
            {
                string controlTypeDataSource = getControlTypeDataSource(e.Row);
                if (!string.IsNullOrEmpty(controlTypeDataSource))
                    SetControlByControlSourceType(dvc, controlTypeDataSource, FormHeader1.ErrorPanel);
            }

        }
    }
    /// <summary>
    /// getControlTypeDataSource
    /// </summary>
    /// <param name="gridViewRow"></param>
    /// <returns></returns>
    protected string getControlTypeDataSource(GridViewRow gridViewRow)
    {
        string controlTypeDataSource = "";
        if (gridViewRow != null)
        {
            Label lb = (Label)gridViewRow.FindControl("labelGridControlTypeDataSource");
            if (lb != null)
            {
                controlTypeDataSource = lb.Text;
            }
        }
        return controlTypeDataSource;
    }

    /// <summary>
    /// GetDynamicValueControlErtek
    /// </summary>
    /// <param name="dvc"></param>
    /// <returns></returns>
    private string GetDynamicValueControlErtek(Contentum.eUIControls.DynamicValueControl dvc)
    {
        Control valueControlErtek = null;
        WebControl tbErtek = null;
        if (dvc != null && dvc.Visible == true)
        {
            //ertekTextBox = GetTextBoxFromDVC(dvc);
            valueControlErtek = GetValueControlFromDVC(dvc);
            tbErtek = GetWebControlFromDVC(dvc);
        }

        if (valueControlErtek != null && tbErtek != null && tbErtek.Visible == true)
        {
            if (valueControlErtek is TextBox)
            {
                // megnézzük, hogy van-e textbox a vezérlõk között, az elsõt kivesszük
                TextBox tb = (TextBox)valueControlErtek;
                return tb.Text;
            }
            else if (valueControlErtek is DropDownList)
            {
                // megnézzük, hogy van-e dropdownlist a vezérlõk között, az elsõt kivesszük
                DropDownList ddl = (DropDownList)valueControlErtek;
                return ddl.SelectedValue;
            }
            // BLG_608
            else if (valueControlErtek is ListBox)
            {
                // megnézzük, hogy van-e listbox a vezérlõk között, az elsõt kivesszük
                ListBox lb = (ListBox)valueControlErtek;
                return lb.SelectedValue;
            }
        }
        return null;
    }
    protected WebControl GetWebControlFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        WebControl wc = null;
        if (dvc != null)
        {
            if (dvc.Control != null && dvc.Control is TextBox)
            {
                wc = (TextBox)dvc.Control;
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megnézzük, hogy van-e textbox a vezérlõk között, az elsõt kivesszük
                        wc = (TextBox)c;
                        break;
                    }
                    else if (c is DropDownList)
                    {
                        // megnézzük, hogy van-e dropdownlist a vezérlõk között, az elsõt kivesszük
                        wc = (DropDownList)c;
                        break;
                    }
                    // BLG_608
                    else if (c is ListBox)
                    {
                        // megnézzük, hogy van-e listbox a vezérlõk között, az elsõt kivesszük
                        wc = (ListBox)c;
                        break;
                    }
                }
            }
        }

        return wc;
    }

    protected Control GetValueControlFromDVC(Contentum.eUIControls.DynamicValueControl dvc)
    {
        Control tb = null;
        if (dvc != null && dvc.Control != null)
        {
            if (dvc.Control is TextBox)
            {
                tb = (TextBox)dvc.Control;
            }
            else if (dvc.Control is UI.ILovListTextBox)
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is HiddenField)
                    {
                        // megnézzük, hogy van-e hiddenfield a vezérlõk között, az elsõt kivesszük
                        tb = (HiddenField)c;
                        break;
                    }
                }
            }
            else if (dvc.Control.HasControls())
            {
                foreach (Control c in dvc.Control.Controls)
                {
                    if (c is TextBox)
                    {
                        // megnézzük, hogy van-e textbox a vezérlõk között, az elsõt kivesszük
                        tb = (TextBox)c;
                        break;
                    }
                    else if (c is DropDownList)
                    {
                        // megnézzük, hogy van-e dropdownlist a vezérlõk között, az elsõt kivesszük
                        tb = (DropDownList)c;
                        break;
                    }
                    // BLG_608
                    else if (c is ListBox)
                    {
                        // megnézzük, hogy van-e listbox a vezérlõk között, az elsõt kivesszük
                        tb = (ListBox)c;
                        break;
                    }
                }
            }
        }

        return tb;
    }

    #endregion

    #region TARGY SZERKESZTO

    #endregion

    //LZS - BL_11345
    protected void TabContainerMAIN_ActiveTabChanged(object sender, EventArgs e)
    {
        
    }
}
