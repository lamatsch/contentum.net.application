﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EUzenetSzabalyList.aspx.cs" Inherits="EUzenetSzabalyList" Title="EUzenetSzabalyList" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <!--Hiba megjelenítése-->
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <!--Fő lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeEUzenetSzabaly" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="updatePanelEUzenetSzabaly" runat="server" OnLoad="updatePanelEUzenetSzabaly_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeEUzenetSzabaly" runat="server" TargetControlID="panelEUzenetSzabaly"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeEUzenetSzabaly" CollapseControlID="btnCpeEUzenetSzabaly"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeEUzenetSzabaly"
                            ExpandedSize="0" ExpandedText="Lista" CollapsedText="Lista">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelEUzenetSzabaly" runat="server" Width="100%">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="gridViewEUzenetSzabaly" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewEUzenetSzabaly_RowCommand"
                                            OnPreRender="gridViewEUzenetSzabaly_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0"
                                            DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewEUzenetSzabaly_RowDataBound" OnSorting="gridViewEUzenetSzabaly_Sorting" AllowPaging="true">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage"
                                                    ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Nev" HeaderText="Nev" SortExpression="Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Leiras" HeaderText="Leírás" SortExpression="Leiras">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="250px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--   <asp:BoundField DataField="Szabaly" HeaderText="Szabály" SortExpression="Szabaly">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:BoundField DataField="Sorrend" HeaderText="Sorrend" SortExpression="Sorrend">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>

                                                <asp:TemplateField HeaderText="Leállító?">
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBoxLeallito" runat="server" Enabled="false" Checked='<%# Eval("Leallito").ToString() == "T" ? true:false %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Note" HeaderText="Σ" SortExpression="Note">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <br />

</asp:Content>
