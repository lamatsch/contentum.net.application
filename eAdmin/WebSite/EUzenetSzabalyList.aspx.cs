using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class EUzenetSzabalyList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    #region CONSTANTS
    private const string ConstQueryDefaultOrder = "INT_AutoEUzenetSzabaly.Sorrend";
    private const string ConstUiGridName = "gridViewEUzenetSzabaly";
    private const string ConstKrtTableName = "INT_AutoEUzenetSzabaly";
    //private const string ConsQueryStringParamName = "FuggoKtId";
    private string ConstSessionKeyEUzenetSzabalyInsertAndOpenModify = "ConstSessionKeyEUzenetSzabalyInsertAndOpenModify";
    #endregion

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";

        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == BoolString.Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INT_AutoEUzenetSzabalyList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.HistoryVisible = false;
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(INT_AutoEUzenetSzabalySearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.EUzenetSzabalyListHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("EUzenetSzabalySearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEUzenetSzabaly.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("EUzenetSzabalyForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth_1280, Defaults.PopupHeight_750, updatePanelEUzenetSzabaly.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewEUzenetSzabaly.ClientID, "check", "cbModosithato");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewEUzenetSzabaly.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEUzenetSzabaly.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewEUzenetSzabaly.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEUzenetSzabaly.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEUzenetSzabaly.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewEUzenetSzabaly;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEUzenetSzabaly);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", GetDefaultSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            EUzenetSzabalyGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        if (Session[ConstSessionKeyEUzenetSzabalyInsertAndOpenModify] != null)
        {
            string script = "OpenNewWindow();";
            script += "var isIE = false || !!document.documentMode; if(isIE) { window.location.reload();}";
            script += "function OpenNewWindow() {";
            script += JavaScripts.SetOnClientClick_DisplayUpdateProgress(
                "EUzenetSzabalyForm.aspx"
              , CommandName.Command + "=" + CommandName.Modify
              + "&" + QueryStringVars.Id + "=" + Session[ConstSessionKeyEUzenetSzabalyInsertAndOpenModify].ToString()
              , Defaults.PopupWidth_1280, Defaults.PopupHeight_750
              , updatePanelEUzenetSzabaly.ClientID, EventArgumentConst.refreshMasterList,
                CustomUpdateProgress1.UpdateProgress.ClientID);
            script += "}";

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "EUzenetSzabalyFormUjraNyit", script, true);
            Session.Remove(ConstSessionKeyEUzenetSzabalyInsertAndOpenModify);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEUzenetSzabaly);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
   
            }
        }
    }
    #endregion Base Page

    #region Master List
    private INT_AutoEUzenetSzabalySearch GetDefaultSearch()
    {
        INT_AutoEUzenetSzabalySearch search = new INT_AutoEUzenetSzabalySearch();
        if (!Page.IsPostBack)
        {
            //search.Leallito.Value = BoolString.Yes;
            //search.Leallito.Operator = Query.Operators.equals;
        }
        return search;
    }
    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void EUzenetSzabalyGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState(ConstUiGridName, ViewState, ConstQueryDefaultOrder);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState(ConstUiGridName, ViewState);

        EUzenetSzabalyGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void EUzenetSzabalyGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        INT_AutoEUzenetSzabalyService service = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        INT_AutoEUzenetSzabalySearch search = (INT_AutoEUzenetSzabalySearch)Search.GetSearchObject(Page, GetDefaultSearch());

        search.OrderBy = Search.GetOrderBy(ConstUiGridName, ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(gridViewEUzenetSzabaly, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewEUzenetSzabaly_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewEUzenetSzabaly_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEUzenetSzabaly);
        ListHeader1.RefreshPagerLabel();

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEUzenetSzabaly);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EUzenetSzabalyGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEUzenetSzabaly);
        EUzenetSzabalyGridViewBind();
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewEUzenetSzabaly);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("EUzenetSzabalyForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth_1280, Defaults.PopupHeight_750, updatePanelEUzenetSzabaly.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("EUzenetSzabalyForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth_1280, Defaults.PopupHeight_750, updatePanelEUzenetSzabaly.ClientID);
            string tableName = ConstKrtTableName;
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.PopupWidth_1280, Defaults.PopupHeight_750, updatePanelEUzenetSzabaly.ClientID);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelEUzenetSzabaly_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    EUzenetSzabalyGridViewBind();
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodcsoport();
            EUzenetSzabalyGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedKodcsoport()
    {

        if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + "Invalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewEUzenetSzabaly, EErrorPanel1, ErrorUpdatePanel);

            INT_AutoEUzenetSzabalyService service = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewEUzenetSzabaly);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKodcsoportRecords();
                EUzenetSzabalyGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKodcsoportRecords();
                EUzenetSzabalyGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedEUzenetSzabaly();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedKodcsoportRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewEUzenetSzabaly, ConstKrtTableName
            , ConstKrtTableName + "Lock", ConstKrtTableName + "ForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedKodcsoportRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewEUzenetSzabaly, ConstKrtTableName
            , ConstKrtTableName + "Lock", ConstKrtTableName + "ForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEUzenetSzabaly -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEUzenetSzabaly()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEUzenetSzabaly, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, ConstKrtTableName);
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewEUzenetSzabaly_Sorting(object sender, GridViewSortEventArgs e)
    {
        EUzenetSzabalyGridViewBind(e.SortExpression, UI.GetSortToGridView(ConstUiGridName, ViewState, e.SortExpression));
    }

    #endregion

    #region Detail Tab

    /// <summary>
    /// TabContainer ActiveTabChanged esem�nykezel�je
    /// A kiv�lasztott record adataival friss�ti az �j panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewEUzenetSzabaly.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewEUzenetSzabaly);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az akt�v panel friss�t�se a f� list�ban kiv�lasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JavaScripts.ResetScroll(Page, cpeEUzenetSzabaly);
                EUzenetSzabalyGridViewBind(masterId);
                panelEUzenetSzabaly.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az allist�k egy oldalon megjelen�tett sorok sz�m�nak be�ll�t�sa
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;
    }

    #endregion

    #region EUzenetSzabaly Detail

    /// <summary>
    /// Allista funkci�gomjainak esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void EUzenetSzabalySubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedEUzenetSzabaly();
            EUzenetSzabalyGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEUzenetSzabaly));
        }
    }

    /// <summary>
    /// Kiv�lasztott record-ok t�rl�se
    /// </summary>
    private void deleteSelectedEUzenetSzabaly()
    {
        if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + "Invalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewEUzenetSzabaly, EErrorPanel1, ErrorUpdatePanel);

            INT_AutoEUzenetSzabalyService service = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewEUzenetSzabaly);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// A EUzenetSzabaly GridView adatk�t�se a KodcsoportID f�ggv�ny�ben alap�rtelmezett
    /// rendez�si param�terekkel
    /// </summary>
    /// <param name="KodcsoportId"></param>
    protected void EUzenetSzabalyGridViewBind(String KodcsoportId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState(ConstUiGridName, ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState(ConstUiGridName, ViewState);
    }
    void EUzenetSzabalySubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        EUzenetSzabalyGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEUzenetSzabaly));
    }

    /// <summary>
    /// EUzenetSzabaly GridView RowCommand esem�nylezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewEUzenetSzabaly_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEUzenetSzabaly, selectedRowNumber, "check");
        }
    }

    void EUzenetSzabalySubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        EUzenetSzabalyGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEUzenetSzabaly));
    }
    #endregion
}

