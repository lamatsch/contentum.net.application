﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EUzenetSzabalySearch.aspx.cs" Inherits="EUzenetSzabalySearch" Title="Automatikus üzenet szabály kereső" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,EUzenetSzabalySearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label_Nev" runat="server" Text="<%$Forditas:lblInterfeszAdatlaptipus|Interfész adatlaptípus:%>" Width="180px"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBox_Nev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>

                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label_Leiras" runat="server" Text="Leírás:" Width="180px"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBox_Leiras" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label runat="server" Text="Típus:" Width="180px" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                       <asp:DropDownList ID="DropDownListRuleType" runat="server" Width="300px" />
                                    </td>
                                    <td></td>
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label runat="server" Text="Főművelet:" Width="180px" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                       <asp:DropDownList ID="DropDownListOperationType" runat="server" Width="300px">
                                            <asp:ListItem Text="mind" Value="" />
                                            <asp:ListItem Text="érkeztet" Value="ERKEZTET" />
                                            <asp:ListItem Text="iktat" Value="IKTAT" />
                                            <%--<asp:ListItem Text="irattároz" Value="IRATTAROZ" />--%>
                                            <asp:ListItem Text="iktatás alszámra" Value="IKTATAS_FOSZAMMAL" />
                                            <asp:ListItem Text="nincs művelet" Value="NINCS" />
                                        </asp:DropDownList><br />
                                    </td>
                                    <td></td>
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label runat="server" Text="Utóműveletet végrehajt:" Width="180px" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                       <asp:CheckBoxList RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" ID="CheckBoxList_Utomuvelet" runat="server" Width="300px" />
                                    </td>
                                    <td></td>
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5" rowspan="2" class="mrUrlapMezo">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                        <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

