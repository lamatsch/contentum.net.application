﻿using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Contentum.eUtility.EKF;

public partial class EUzenetSzabalySearch : Contentum.eUtility.UI.PageBase
{
    const string MainOpPrefix = "Művelet: ";

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        static public bool Contain(string value)
        {
            if ((value == Yes) || (value == No) || (value == NotSet))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private Type _type = typeof(INT_AutoEUzenetSzabalySearch);

    /// <summary>
    /// Az oldal Init eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    private void InitControls()
    {
        // főművelet
        DropDownListRuleType.Items.Clear();
        DropDownListRuleType.Items.Add(new ListItem("mind", ""));
        foreach (EnumEKFRuleType name in Enum.GetValues(typeof(EnumEKFRuleType)))
        {
            DropDownListRuleType.Items.Add(new ListItem(name.ToString(), name.ToString()));
        }

        // utóműveletek
        CheckBoxList_Utomuvelet.Items.Clear();
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("szignál", EnumEKFOperationName.SZIGNALAS.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("elintéz", EnumEKFOperationName.ELINTEZETTE_NYILVANITAS.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("értesít", EnumEKFOperationName.ERTESITES.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("jogosultak", EnumEKFOperationName.JOGOSULTAK.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("tárgyszavak", EnumEKFOperationName.TARGYSZAVAK.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("auto válasz", EnumEKFOperationName.AUTO_VALASZ.ToString()));
        CheckBoxList_Utomuvelet.Items.Add(new ListItem("cél rendszer", EnumEKFOperationName.CELRENDSZER.ToString()));
    }

    /// <summary>
    /// Az oldal Load eseménykezelõje
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);
        if (!IsPostBack)
        {
            InitControls();

            INT_AutoEUzenetSzabalySearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (INT_AutoEUzenetSzabalySearch)Search.GetSearchObject(Page, new INT_AutoEUzenetSzabalySearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek feltöltése a keresési objektumból
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        var search = (INT_AutoEUzenetSzabalySearch)searchObject;
        if (search != null)
        {
            TextBox_Nev.Text = search.Nev.Value;
            TextBox_Leiras.Text = search.Leiras.Value;

            DropDownListRuleType.SelectedValue = String.IsNullOrEmpty(search.Tipus.Value) ? "" : search.Tipus.Value;

            foreach (ListItem listItem in DropDownListOperationType.Items)
            {
                listItem.Selected = !String.IsNullOrEmpty(listItem.Value) && search.WhereByManual.Contains(MainOpPrefix + listItem.Value);
                if (listItem.Selected)
                {
                    break;
                }
            }

            foreach (ListItem listItem in CheckBoxList_Utomuvelet.Items)
            {
                listItem.Selected = search.WhereByManual.Contains(listItem.Value);
            }

            Ervenyesseg_SearchFormComponent1.SetDefault(search.ErvKezd, search.ErvVege);
        }
    }

    /// <summary>
    /// Keresési objektum létrehozása a form komponenseinek értékeibõl
    /// </summary>
    private INT_AutoEUzenetSzabalySearch SetSearchObjectFromComponents()
    {
        var search = (INT_AutoEUzenetSzabalySearch)SearchHeader1.TemplateObject;

        if (search == null)
        {
            search = new INT_AutoEUzenetSzabalySearch();
        }

        if (String.IsNullOrEmpty(TextBox_Nev.Text))
        { 
            search.Nev.Clear();
        }
        else
        {
            search.Nev.LikeWhenNeeded(TextBox_Nev.Text);
        }

        if (String.IsNullOrEmpty(TextBox_Leiras.Text)) 
        { 
            search.Leiras.Clear();
        }
        else
        {
            search.Leiras.LikeWhenNeeded(TextBox_Leiras.Text);
        }

        if (String.IsNullOrEmpty(DropDownListRuleType.SelectedValue))
        { 
            search.Tipus.Clear();
        }
        else
        {
            search.Tipus.FilterIfNotEmpty(DropDownListRuleType.SelectedValue);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(search.ErvKezd, search.ErvVege);

        search.WhereByManual = "";

        var muveletek = new List<string>();
        var fomuvelet = DropDownListOperationType.SelectedValue;
        
        if (!String.IsNullOrEmpty(fomuvelet))
        {
            muveletek.Add(MainOpPrefix + fomuvelet);
        }

        foreach (ListItem item in CheckBoxList_Utomuvelet.Items)
        { 
            if (item.Selected) { muveletek.Add(item.Value); }
        }

        if (muveletek.Count > 0)
        {
            // BUG_12634
            Query query = new Query();
            query.BuildFromBusinessDocument(search);
            search.WhereByManual = !string.IsNullOrEmpty(query.Where) ? " AND " : "";

            // utóművelet keresés
            var op = "AND";
            search.WhereByManual += " ([Note] LIKE " +
                String.Join(" " + op + " [Note] LIKE ", muveletek.Select(m => "'%" + m + "%'").ToArray())
            + ")";
        }

        return search;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keresési form footer funkciógomjainak eseményeinek kezelése
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        var defSearch = GetDefaultSearchObject();
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(defSearch);
        }
        else if (e.CommandName == CommandName.Search)
        {
            var search = SetSearchObjectFromComponents();
            if (Search.IsIdentical(search, defSearch, search.WhereByManual, defSearch.WhereByManual))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                search.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, search);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// Új keresési objektum létrehozása az alapbeállításokkal
    /// </summary>
    /// <returns></returns>
    private INT_AutoEUzenetSzabalySearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return GetDefaultSearch();
    }

    private INT_AutoEUzenetSzabalySearch GetDefaultSearch()
    {
        INT_AutoEUzenetSzabalySearch search = new INT_AutoEUzenetSzabalySearch();
        return search;
    }
}
