using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class EmptyMasterPage : System.Web.UI.MasterPage, Contentum.eUtility.ILastLoginData
{
    //Authentication auth = new Authentication();

    protected void Page_Init(object sender, EventArgs e)
    {

        Response.CacheControl = "no-cache";

        Authentication.CheckLogin(Page);

        BelepesiAdatok.LoadBelepesiAdatok();

        //AjaxLogging.js referencia hozz�ad�sa
        Contentum.eUtility.JavaScripts.RegisterAjaxLoggingScript(Page);
        //Common.js referencia hozz�ad�sa
        Contentum.eUtility.JavaScripts.RegisterCommonScripts(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BelepesiAdatok.LoadBelepesiAdatok();
        foreach (Contentum.eUIControls.eErrorPanel errorPanel in UI.FindControls<Contentum.eUIControls.eErrorPanel>(Page.Form.Controls))
        {
            errorPanel.EmailButtonClick += new CommandEventHandler(errorPanel_EmailButtonClick);
        }
    }

    protected void errorPanel_EmailButtonClick(object sender, CommandEventArgs e)
    {
        Contentum.eUIControls.eErrorPanel errorPanel = (Contentum.eUIControls.eErrorPanel)sender;
        string[] parameters = e.CommandArgument.ToString().Split(';');
        string ErrorCode = parameters[0];
        string ErrorMessage = parameters[1];
        string CurrentLogEntry = parameters[2];

        bool IsSuccess = Notify.SendErrorInEmail(Page, ErrorCode, ErrorMessage, CurrentLogEntry);

        if (errorPanel != null)
        {
            JavaScripts.RegisterEmailFeedbackMessage(errorPanel, IsSuccess);
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
    }

    #region ILastLoginData Members

    public string FelhasznaloHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldFelhasznaloId.UniqueID;
        }
    }

    public string SzervezetHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldSzervezetId.UniqueID;
        }
    }

    public string FelhasznaloCsoporttagsagHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldCsoporttagId.UniqueID;
        }
    }

    public string HelyettesitesHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldHelyettesitesId.UniqueID;
        }
    }

    public string LoginTypeHiddenFieldUniqueId
    {
        get
        {
            return BelepesiAdatok.HiddenFieldLoginType.UniqueID;
        }
    }

    #endregion
}
