<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EsemenyekForm.aspx.cs" Inherits="EsemenyekForm" Title="Untitled Page" %>

<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="ottb" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="ftb" %>
<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="reqtb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 50px;
            min-width: 50px;
        }
    .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
    width: 200px;
}
    .mrUrlapInputComboBox
    {
       width: 205px;
    }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,EsemenyekFormHeaderTitle %>" />
    <br />
    
    <asp:HiddenField ID="ObjId_HiddenField" runat="server" />
    <asp:HiddenField ID="HelyettesitesId_HiddenField" runat="server" />
    
    <div style="padding:0px 2px">
        <eUI:eFormPanel ID="EFormPanelEsemeny" runat="server" style="padding-bottom:10px">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelErvKezd" Text="Esem�ny id�pontja:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <%--BUG_4537--%>
                           <%-- <cc:CalendarControl ID="CalendarControl_LetrehozasIdo" TimeVisible="true" runat="server" />--%>
                             <uc6:ReadOnlyTextBox ID="ReadOnlyTextBox_LetrehozasIdo" runat="server" ReadOnly="true" CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelAzonosito" Text="Azonos�t�:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc6:ReadOnlyTextBox ID="ReadOnlyTextBox_Azonosito" runat="server" ReadOnly="true" CssClass="mrUrlapInput" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_ObjTipus_Felhasznalo" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelObjektumTipus" Text="Ojektum t�pus:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <ottb:ObjTipusokTextBox ID="ObjTipusokTextBox_ObjTipus" runat="server"  CssClass="mrUrlapInput" />
                        </td>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelLoginUser" Text="V�grehajt�:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:FelhasznaloTextBox ID="FelhasznaloTextBox_LoginUser" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_Esemeny_Helyettesitett" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelEsemeny" Text="Esem�ny:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <ftb:FunkcioTextBox ID="FunkcioTextBox_Esemeny" runat="server" ReadOnly="true" />
                        </td>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelFelhasznalo" Text="Kinek a nev�ben:" runat="server" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:FelhasznaloTextBox ID="FelhasznaloTextBox_Felhasznalo" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_Szervezet" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelFelhasznaloSzervezet" runat="server" Text="V�grehajt� szervezete:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="2">
                            <uc8:CsoportTextBox ID="CsoportTextBox_Szervezet" runat="server" ReadOnly="true" CssClass="mrUrlapInput"/>
                        </td>
                    </tr>
                    <tr class="urlapSor" id="tr_Jogalap_Helyettesites" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelHelyettesites" runat="server" Text="Vh. jogalap:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <ktddl:KodTarakDropDownList ID="KodTarakDropDownList_HelyettesitesMod" runat="server" CssClass="mrUrlapInputComboBox" ReadOnly="true" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="tr_KeresesiFeltetel" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelKeresesiFeltetel" runat="server" Text="Keres�si felt�tel:" />
                        </td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="TextBox_KeresesiFeltetel" runat="server" ReadOnly="true" Rows="3"
                                Width="90%" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr id="tr_TalalatokSzama" runat="server">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelTalalatokSzama" runat="server" Text="Tal�latok sz�ma:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc6:ReadOnlyTextBox ID="ReadOnlyTextBox_TalalatokSzama" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
