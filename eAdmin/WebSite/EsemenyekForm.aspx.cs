using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

// TELJES K�D KIDOLGOZAND�!

public partial class EsemenyekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private string ObjId = "";
    private string ObjTip_Id = "";
    private string Funkcio_Id = "";
    private string Mode = "";

    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }

    #region Utils
    private string GetTablaObjTipId(string ObjTip_Kod)
    {
        string ObjTip_Id = String.Empty;
        if (!String.IsNullOrEmpty(ObjTip_Kod))
        {
            // objektum Id lek�r�se
            Contentum.eAdmin.Service.KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            search.Kod.Value = ObjTip_Kod;
            search.Kod.Operator = Query.Operators.equals;
            Result result = service.GetAllWithExtension(execParam, search, Contentum.eUtility.Constants.FilterType.ObjTipusok.Tabla);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    String Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    if (!String.IsNullOrEmpty(Id))
                    {
                        ObjTip_Id = Id;
                    }
                }
            }
        }
        return ObjTip_Id;
    }

    private string GetFunkcioIdByFunkcioNev(string FunkcioNev)
    {
        string Funkcio_Id = String.Empty;
        if (!String.IsNullOrEmpty(FunkcioNev))
        {
            // objektum Id lek�r�se
            Contentum.eAdmin.Service.KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            KRT_FunkciokSearch search = new KRT_FunkciokSearch();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            search.Kod.Value = FunkcioNev;
            search.Kod.Operator = Query.Operators.equals;
            Result result = service.GetAll(execParam, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    String Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                    if (!String.IsNullOrEmpty(Id))
                    {
                        Funkcio_Id = Id;
                    }
                }
            }
        }
        return Funkcio_Id;
    }
    #endregion Utils

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        // BUG_4537
        //CalendarControl_LetrehozasIdo.ReadOnly = true;
        ReadOnlyTextBox_LetrehozasIdo.ReadOnly = true;

        ReadOnlyTextBox_Azonosito.ReadOnly = true;
        ObjTipusokTextBox_ObjTipus.ReadOnly = true;
        FelhasznaloTextBox_LoginUser.ReadOnly = true;
        FunkcioTextBox_Esemeny.ReadOnly = true;
        FelhasznaloTextBox_Felhasznalo.ReadOnly = true;
        CsoportTextBox_Szervezet.ReadOnly = true;
        KodTarakDropDownList_HelyettesitesMod.ReadOnly = true;
        TextBox_KeresesiFeltetel.ReadOnly = true;
        ReadOnlyTextBox_TalalatokSzama.ReadOnly = true;

        labelErvKezd.CssClass = "mrUrlapInputWaterMarked";
        labelAzonosito.CssClass = "mrUrlapInputWaterMarked";
        labelObjektumTipus.CssClass = "mrUrlapInputWaterMarked";
        labelLoginUser.CssClass = "mrUrlapInputWaterMarked";
        labelEsemeny.CssClass = "mrUrlapInputWaterMarked";
        labelFelhasznalo.CssClass = "mrUrlapInputWaterMarked";
        labelFelhasznaloSzervezet.CssClass = "mrUrlapInputWaterMarked";
        labelHelyettesites.CssClass = "mrUrlapInputWaterMarked";
        labelKeresesiFeltetel.CssClass = "mrUrlapInputWaterMarked";
        labelTalalatokSzama.CssClass = "mrUrlapInputWaterMarked";

    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        //CalendarControl_LetrehozasIdo.ReadOnly = true;
        ReadOnlyTextBox_Azonosito.ReadOnly = true;
        ObjTipusokTextBox_ObjTipus.ReadOnly = true;
        FelhasznaloTextBox_LoginUser.ReadOnly = true;
        FunkcioTextBox_Esemeny.ReadOnly = true;
        FelhasznaloTextBox_Felhasznalo.ReadOnly = true;
        CsoportTextBox_Szervezet.ReadOnly = true;
        KodTarakDropDownList_HelyettesitesMod.ReadOnly = true;
        TextBox_KeresesiFeltetel.ReadOnly = true;
        ReadOnlyTextBox_TalalatokSzama.ReadOnly = true;

        //labelErvKezd.CssClass = "mrUrlapInputWaterMarked";
        labelAzonosito.CssClass = "mrUrlapInputWaterMarked";
        labelObjektumTipus.CssClass = "mrUrlapInputWaterMarked";
        labelLoginUser.CssClass = "mrUrlapInputWaterMarked";
        labelEsemeny.CssClass = "mrUrlapInputWaterMarked";
        labelFelhasznalo.CssClass = "mrUrlapInputWaterMarked";
        labelFelhasznaloSzervezet.CssClass = "mrUrlapInputWaterMarked";
        labelHelyettesites.CssClass = "mrUrlapInputWaterMarked";
        labelKeresesiFeltetel.CssClass = "mrUrlapInputWaterMarked";
        labelTalalatokSzama.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Esemeny" + Command);
                break;
        }

        Mode = Page.Request.QueryString.Get(QueryStringVars.Mode);
        ObjId = Page.Request.QueryString.Get(QueryStringVars.ObjektumId);
        ObjTip_Id = GetTablaObjTipId(Page.Request.QueryString.Get(QueryStringVars.TableName));

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Esemenyek krt_Esemenyek = (KRT_Esemenyek)result.Record;
                    LoadComponentsFromBusinessObject(krt_Esemenyek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {


            if (Mode == CommandName.Munkanaplo)
            {
                Funkcio_Id = GetFunkcioIdByFunkcioNev(Constants.Funkcio.EgyebEsemeny);
                ObjTip_Id = GetTablaObjTipId(Constants.TableNames.EREC_UgyUgyiratok);
                ObjTipusokTextBox_ObjTipus.ReadOnly = true;
                ObjTipusokTextBox_ObjTipus.Filter = Constants.FilterType.ObjTipusok.Tabla;
                // BUG_4537
                // Elvileg nincs m�dos�t�s �s �j
               // CalendarControl_LetrehozasIdo.Validate = true;

                tr_ObjTipus_Felhasznalo.Visible = false;
                tr_Esemeny_Helyettesitett.Visible = false;
                tr_Szervezet.Visible = false;
                tr_Jogalap_Helyettesites.Visible = false;
                tr_KeresesiFeltetel.Visible = false;
                tr_TalalatokSzama.Visible = false;

                labelAzonosito.Visible = false;
                ReadOnlyTextBox_Azonosito.Visible = false;
            }
        }


        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        else if (Command == CommandName.New)
        {
            if (Mode == CommandName.Munkanaplo)
            {
                if (!IsPostBack)
                    SetNewControlsForMunkanaplo();
            }
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Esemenyek"></param>
    private void SetNewControlsForMunkanaplo()
    {
        // BUG_4537
        // Elvileg nincs m�dos�t�s �s �j
        // CalendarControl_LetrehozasIdo.SetToday(); // LetrehozasIdo
        ReadOnlyTextBox_Azonosito.Text = ""; // krt_Esemenyek.Azonositoja;
        ObjTipusokTextBox_ObjTipus.Id_HiddenField = ObjTip_Id; // result.Ds.Tables[0].Rows[0]["ObjTip_Id_Nev"].ToString();
        ObjTipusokTextBox_ObjTipus.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
        ObjId_HiddenField.Value = ObjId;
        HelyettesitesId_HiddenField.Value = FelhasznaloProfil.HelyettesitesId(Page);
        FelhasznaloTextBox_LoginUser.Id_HiddenField = FelhasznaloProfil.LoginUserId(Page); //krt_Esemenyek.Felhasznalo_Id_User;
        FelhasznaloTextBox_LoginUser.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FunkcioTextBox_Esemeny.Id_HiddenField = Funkcio_Id; // result.Ds.Tables[0].Rows[0]["Funkciok_Nev"].ToString();
        FunkcioTextBox_Esemeny.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);
        FelhasznaloTextBox_Felhasznalo.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page); //krt_Esemenyek.Felhasznalo_Id_Login;
        FelhasznaloTextBox_Felhasznalo.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        CsoportTextBox_Szervezet.Id_HiddenField = FelhasznaloProfil.FelhasznaloSzerverzetId(Page); //krt_Esemenyek.Csoport_Id_FelelosUserSzerveze;
        CsoportTextBox_Szervezet.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        TextBox_KeresesiFeltetel.Text = ""; // krt_Esemenyek.KeresesiFeltetel;
        ReadOnlyTextBox_TalalatokSzama.Text = ""; //string.IsNullOrEmpty(krt_Esemenyek.KeresesiFeltetel) ? "" : krt_Esemenyek.TalalatokSzama;

        if (Page.Session[Constants.FelhasznaloProfil] != null)
        {
            Contentum.eUtility.FelhasznaloProfil f = (Contentum.eUtility.FelhasznaloProfil)Page.Session[Constants.FelhasznaloProfil];
            if (f != null)
            {
                if (f.Helyettesites != null && !String.IsNullOrEmpty(f.Helyettesites.HelyettesitesMod))
                {
                    KodTarakDropDownList_HelyettesitesMod.FillWithOneValue(kcs_HELYETTESITES_MOD, f.Helyettesites.HelyettesitesMod, FormHeader1.ErrorPanel);
                }
            }
        }

        FormHeader1.Record_Ver = "";
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Esemenyek"></param>
    private void LoadComponentsFromBusinessObject(KRT_Esemenyek krt_Esemenyek)
    {
        if (krt_Esemenyek != null)
        {
            // BUG_4537
            //CalendarControl_LetrehozasIdo.Text = krt_Esemenyek.Base.LetrehozasIdo; // LetrehozasIdo
            ReadOnlyTextBox_LetrehozasIdo.Text = krt_Esemenyek.Base.LetrehozasIdo; // LetrehozasIdo

            ReadOnlyTextBox_Azonosito.Text = krt_Esemenyek.Azonositoja;
            ObjTipusokTextBox_ObjTipus.Id_HiddenField = krt_Esemenyek.ObjTip_Id;
            ObjTipusokTextBox_ObjTipus.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
            ObjId_HiddenField.Value = krt_Esemenyek.Obj_Id;
            HelyettesitesId_HiddenField.Value = krt_Esemenyek.Helyettesites_Id;
            FelhasznaloTextBox_LoginUser.Id_HiddenField = krt_Esemenyek.Felhasznalo_Id_Login;
            FelhasznaloTextBox_LoginUser.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
            FunkcioTextBox_Esemeny.Id_HiddenField = krt_Esemenyek.Funkcio_Id;
            FunkcioTextBox_Esemeny.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);
            FelhasznaloTextBox_Felhasznalo.Id_HiddenField = krt_Esemenyek.Felhasznalo_Id_User;
            FelhasznaloTextBox_Felhasznalo.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
            CsoportTextBox_Szervezet.Id_HiddenField = krt_Esemenyek.Csoport_Id_FelelosUserSzerveze;
            CsoportTextBox_Szervezet.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            TextBox_KeresesiFeltetel.Text = krt_Esemenyek.KeresesiFeltetel;
            ReadOnlyTextBox_TalalatokSzama.Text = String.IsNullOrEmpty(krt_Esemenyek.KeresesiFeltetel) ? "" : krt_Esemenyek.TalalatokSzama;

            if (!String.IsNullOrEmpty(krt_Esemenyek.Helyettesites_Id))
            {
                KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = krt_Esemenyek.Helyettesites_Id;

                Result result_helyettesitesGet = service.Get(execParam);

                if (String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode))
                {
                    KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result_helyettesitesGet.Record;
                    if (krt_Helyettesitesek != null)
                    {
                        KodTarakDropDownList_HelyettesitesMod.FillWithOneValue(kcs_HELYETTESITES_MOD, krt_Helyettesitesek.HelyettesitesMod, FormHeader1.ErrorPanel);
                    }
                }
            }

            FormHeader1.Record_Ver = krt_Esemenyek.Base.Ver;
        }
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_Esemenyek GetBusinessObjectFromComponents()
    {
        KRT_Esemenyek krt_Esemenyek = new KRT_Esemenyek();
        krt_Esemenyek.Updated.SetValueAll(false);
        krt_Esemenyek.Base.Updated.SetValueAll(false);

        krt_Esemenyek.Azonositoja = ReadOnlyTextBox_Azonosito.Text;
        krt_Esemenyek.Updated.Azonositoja = pageView.GetUpdatedByView(ReadOnlyTextBox_Azonosito);

        krt_Esemenyek.ObjTip_Id = ObjTipusokTextBox_ObjTipus.Id_HiddenField;
        krt_Esemenyek.Updated.ObjTip_Id = pageView.GetUpdatedByView(ObjTipusokTextBox_ObjTipus);

        krt_Esemenyek.Obj_Id = ObjId_HiddenField.Value;
        krt_Esemenyek.Updated.Obj_Id = true;

        krt_Esemenyek.Felhasznalo_Id_Login = FelhasznaloTextBox_LoginUser.Id_HiddenField;
        krt_Esemenyek.Updated.Felhasznalo_Id_Login = pageView.GetUpdatedByView(FelhasznaloTextBox_LoginUser);

        krt_Esemenyek.Funkcio_Id = FunkcioTextBox_Esemeny.Id_HiddenField;
        krt_Esemenyek.Updated.Funkcio_Id = pageView.GetUpdatedByView(FunkcioTextBox_Esemeny);

        krt_Esemenyek.Felhasznalo_Id_User = FelhasznaloTextBox_Felhasznalo.Id_HiddenField;
        krt_Esemenyek.Updated.Felhasznalo_Id_User = pageView.GetUpdatedByView(FelhasznaloTextBox_Felhasznalo);

        krt_Esemenyek.Csoport_Id_FelelosUserSzerveze = CsoportTextBox_Szervezet.Id_HiddenField;
        krt_Esemenyek.Updated.Csoport_Id_FelelosUserSzerveze = pageView.GetUpdatedByView(CsoportTextBox_Szervezet);

        krt_Esemenyek.KeresesiFeltetel = TextBox_KeresesiFeltetel.Text;
        krt_Esemenyek.Updated.KeresesiFeltetel = pageView.GetUpdatedByView(TextBox_KeresesiFeltetel);

        krt_Esemenyek.TalalatokSzama = ReadOnlyTextBox_TalalatokSzama.Text;
        krt_Esemenyek.Updated.TalalatokSzama = pageView.GetUpdatedByView(ReadOnlyTextBox_TalalatokSzama);

        krt_Esemenyek.Helyettesites_Id = HelyettesitesId_HiddenField.Value;
        krt_Esemenyek.Updated.Helyettesites_Id = true;

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Esemenyek.Base.Ver = FormHeader1.Record_Ver;
        krt_Esemenyek.Base.Updated.Ver = true;

        // BUG_4537
        // Elvileg nincs m�dos�t�s �s �j
        //krt_Esemenyek.Base.LetrehozasIdo = CalendarControl_LetrehozasIdo.Text; // LetrehozasIdo
        krt_Esemenyek.Base.LetrehozasIdo = ReadOnlyTextBox_LetrehozasIdo.Text; // LetrehozasIdo
        krt_Esemenyek.Base.Updated.LetrehozasIdo = true;

        return krt_Esemenyek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(KodcsoportokTextBox1);
            //compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            //compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //compSelector.Add_ComponentOnClick(textRovidNev);
            //compSelector.Add_ComponentOnClick(textSorrend);
            //compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Esemeny" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                            KRT_Esemenyek krt_Esemenyek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Esemenyek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                 //ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                 //ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, FunkcioTextBox_Esemeny.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                                KRT_Esemenyek krt_Esemenyek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Esemenyek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
