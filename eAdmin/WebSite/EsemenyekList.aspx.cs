using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Contentum.eQuery;

public partial class EsemenyekList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "EsemenyekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //F� lista gombjainak l�that�s�g�nak be�ll�t�sa
        ListHeader1.NewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        #region BLG_2960
        //LZS
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        #endregion

        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_EsemenyekSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.EsemenyekLisHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.HashVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("EsemenyekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewEsemenyek.ClientID, "check", "cbModosithato");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewEsemenyek.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEsemenyek.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewEsemenyek.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEsemenyek.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewEsemenyek;


        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEsemenyek);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_EsemenyekSearch());
        
        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            EsemenyekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Lock);

        #region BLG_2960
        //LZS
        ListHeader1.ExportEnabled = true;
        #endregion
        ListHeader1.HashEnabled = true;
        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEsemenyek);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void EsemenyekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEsemenyek", ViewState, "KRT_Esemenyek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEsemenyek", ViewState, SortDirection.Descending);

        EsemenyekGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void EsemenyekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_EsemenyekSearch search = null;

        // BUG#7286: timeout probl�ma miatt default sz�r�st tesz�nk fel (mai napra sz�r�s)
        // Ha nincs bent a sessionben, a default sz�r�ssel beletessz�k:
        if (!Search.IsSearchObjectInSession(Page, typeof(KRT_EsemenyekSearch)))
        {
            search = (KRT_EsemenyekSearch)Search.DefaultSearchObjects.CreateSearchObjWithDefFilter_Esemenyek();
            
            // sessionbe ment�s:
            Search.SetSearchObject(Page, search);            
        }

        search = (KRT_EsemenyekSearch)Search.GetSearchObject(Page, new KRT_EsemenyekSearch(true));

        search.OrderBy = Search.GetOrderBy("gridViewEsemenyek", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewEsemenyek, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewEsemenyek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            //DataRowView drw = (DataRowView)e.Row.DataItem;

            //BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }

        //Lockol�s jelz�se
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewEsemenyek_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEsemenyek);
        ListHeader1.RefreshPagerLabel();
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EsemenyekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEsemenyek);
        EsemenyekGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewEsemenyek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEsemenyek, selectedRowNumber, "check");

            string id = gridViewEsemenyek.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewEsemenyek_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewEsemenyek);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEsemenyek.ClientID);
            string tableName = "Krt_Esemenyek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelEsemenyek.ClientID);
        }
    }


    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelEsemenyek_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    EsemenyekGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewEsemenyek));
                    ////gridViewEsemenyek_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewEsemenyek));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodtar();
            EsemenyekGridViewBind();
        }

        #region BLG_2960
        //LZS - Excel Export (Grid)
        //ListGrid export�l�sa Excel  file-ba, a sz�ks�ges oszlopsz�less�gekhez tartoz� widthfactor param�terk�nt �tadva.
        if (e.CommandName == CommandName.ExcelExport)
        {
            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            ex_Export.SaveGridView_ToExcel(exParam, gridViewEsemenyek, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, 1.1d, browser);
        }
        #endregion

    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedKodtar()
    {

        if (FunctionRights.GetFunkcioJog(Page, "KodtarInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewEsemenyek, EErrorPanel1, ErrorUpdatePanel);

            KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewEsemenyek);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }

            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }


    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKodtarRecords();
                EsemenyekGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKodtarRecords();
                EsemenyekGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedEsemenyek();
                break;

            case CommandName.Hash:
                CheckEsemenyekHash();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedKodtarRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewEsemenyek, "KRT_Esemenyek"
            , "KodtarLock", "KodtarForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedKodtarRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewEsemenyek, "KRT_Esemenyek"
            , "KodtarLock", "KodtarForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEsemenyek -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEsemenyek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEsemenyek, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Esemenyek");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewEsemenyek_Sorting(object sender, GridViewSortEventArgs e)
    {
        EsemenyekGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewEsemenyek", ViewState, e.SortExpression));
    }

    #endregion

    #region HASH
    private void CheckEsemenyekHash()
    {
        string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEsemenyek);
        if (string.IsNullOrEmpty(MasterListSelectedRowId))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.UINoSelectedItem);
            ErrorUpdatePanel.Update();
            return;
        }

        KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result resultcrc = service.CheckItemIntegrityCheckSum(execParam, MasterListSelectedRowId);
        if (!string.IsNullOrEmpty(resultcrc.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultcrc);
            ErrorUpdatePanel.Update();
            return;
        }

        if (resultcrc.Record != null)
        {
            if ((bool)resultcrc.Record)
            {
                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.Info, Resources.Error.ErrorCode_65123);
                ErrorUpdatePanel.Update();
                return;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_65122);
                ErrorUpdatePanel.Update();
                return;
            }
        }
    }
    #endregion
}
