<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="EsemenyekSearch.aspx.cs" Inherits="EsemenyekSearch" Title="Untitled Page" %>

<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc10" %>
<%--@ Register Src="Component/ObjektumTipusTextBox.ascx" TagName="ObjektumTipusTextBox" TagPrefix="uc6" --%>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc7" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc5" %>
<%@ Register Src="Component/DatumIdoIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="di" %>
<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList" TagPrefix="ktddl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,EsemenyekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFunkcio" runat="server" Text="Esem�ny:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc10:FunkcioTextBox ID="FunkcioTextBox" SearchMode="true" runat="server" CssClass="mrUrlapInput" Validate="false" AjaxEnabled="true" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" Text="Iktat�sz�m:"></asp:Label>
                                      </td>
                                      <td class="mrUrlapMezo">
                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="Objektum t�pus:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="ObjektumTipusTextBox" runat="server" CssClass="mrUrlapInput"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label4" runat="server" Text="Kinek a nev�ben:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox1" runat="server" Validate="false"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelModosithato" runat="server" Text="V�grehajt�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox2" runat="server" Validate="false"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" Text="V�grehajt� szervezete:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc9:CsoportTextBox ID="CsoportTextBox1" runat="server" Validate="false"/>
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelHelyettesitesMod" runat="server" Text="V�grehajt�i jogalap:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <ktddl:KodTarakDropDownList ID="KodTarakDropDownList_HelyettesitesMod" runat="server" Validate="false"/>
                                    </td>
                                </tr>                                
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKodcsoport" runat="server" Text="Esem�ny id�pontja:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <di:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1" runat="server" Validate="false" />
                                        </td>
                                </tr>
                                <tr class="urlapSor">
                                     <td colspan="2">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

