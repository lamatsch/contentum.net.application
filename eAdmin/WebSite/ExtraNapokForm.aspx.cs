﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class ExtraNapokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View módban a vezérlők engedélyezése és megjelenése
    /// </summary>
    private void SetViewControls()
    {
        ccNap.ReadOnly = true;
        ddlistTipus.ReadOnly = true;
        textMegjegyzes.ReadOnly = true;
        // BUG_4673
        ErvenyessegCalendarControl1.ReadOnly = true;
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";


    }

    /// <summary>
    /// Modify módban a vezérlő engedélyezése és megjelenése
    /// </summary>
    private void SetModifyControls()
    {
        //ccNap.ReadOnly = true;
    }

    private void InitComponents()
    {
        ExtraNapok.Jelzo.FillListControl(ddlistTipus);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ExtraNap" + Command);
                break;
        }

        if (!IsPostBack)
        {
            InitComponents();
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Extra_Napok krt_extraNapok = (KRT_Extra_Napok)result.Record;
                    LoadComponentsFromBusinessObject(krt_extraNapok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Extra_Napok krt_extraNapok)
    {
        FormHeader1.Record_Ver = krt_extraNapok.Base.Ver;

        ccNap.Text = krt_extraNapok.Datum;
        ddlistTipus.SelectedValue = krt_extraNapok.Jelzo;
        textMegjegyzes.Text = krt_extraNapok.Megjegyzes;
        // BUG_4673
        ErvenyessegCalendarControl1.ErvKezd = krt_extraNapok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_extraNapok.ErvVege;


        FormPart_CreatedModified1.SetComponentValues(krt_extraNapok.Base);
    }

    // form --> business object
    private KRT_Extra_Napok GetBusinessObjectFromComponents()
    {
        KRT_Extra_Napok krt_extraNapok = new KRT_Extra_Napok();
        krt_extraNapok.Updated.SetValueAll(false);
        krt_extraNapok.Base.Updated.SetValueAll(false);

        // BUG_10897
        //krt_extraNapok.Datum = ccNap.Text;
        krt_extraNapok.Datum = ccNap.SelectedDateText;
        krt_extraNapok.Updated.Datum = pageView.GetUpdatedByView(ccNap);

        krt_extraNapok.Jelzo = ddlistTipus.SelectedValue;
        krt_extraNapok.Updated.Jelzo = pageView.GetUpdatedByView(ddlistTipus);

        krt_extraNapok.Megjegyzes = textMegjegyzes.Text;
        krt_extraNapok.Updated.Megjegyzes = pageView.GetUpdatedByView(textMegjegyzes);

        krt_extraNapok.Base.Ver = FormHeader1.Record_Ver;
        krt_extraNapok.Base.Updated.Ver = true;

        // BUG_4673
        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_extraNapok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        return krt_extraNapok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(ccNap);
            compSelector.Add_ComponentOnClick(ddlistTipus);
            compSelector.Add_ComponentOnClick(textMegjegyzes);
            // BUG_4673
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "ExtraNap" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                            // BUG_4673
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            KRT_Extra_NapokSearch search = new KRT_Extra_NapokSearch();
                            // BUG_10897
                            //search.Datum.Value = ccNap.Text;
                            search.Datum.Value = ccNap.SelectedDateText;

                            search.Datum.Operator = Query.Operators.equals;
                            Result searchResult = service.GetAll(execParam, search);
                            if (String.IsNullOrEmpty(searchResult.ErrorCode))
                            {
                                if (searchResult.Ds.Tables[0].Rows.Count > 0)
                                {
                                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_2601);
                                }
                                else
                                {

                                    KRT_Extra_Napok krt_extraNapok = GetBusinessObjectFromComponents();

                                    //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                    Result result = service.Insert(execParam, krt_extraNapok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // ha egy másik formról hívták meg, adatok visszaküldése:
                                        // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                        // BUG_10897
                                        //if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, ccNap.Text))
                                        if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, ccNap.SelectedDateText))

                                        {
                                            JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                        }
                                        else
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                        }

                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                            }
                            break;

                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                // BUG_10669
                                KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                                // BUG_4673
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                KRT_Extra_NapokSearch search = new KRT_Extra_NapokSearch();
                                // BUG_10897
                                //search.Datum.Value = ccNap.Text;
                                search.Datum.Value = ccNap.SelectedDateText;

                                search.Datum.Operator = Query.Operators.equals;

                                search.Id.Value = recordId;
                                search.Id.Operator = Query.Operators.notequals;

                                Result searchResult = service.GetAll(execParam, search);
                                if (String.IsNullOrEmpty(searchResult.ErrorCode))
                                {
                                    if (searchResult.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_65124);
                                    }
                                    else
                                    {

                                        //   KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                                        KRT_Extra_Napok krt_extraNapok = GetBusinessObjectFromComponents();


                                        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                        execParam.Record_Id = recordId;

                                        Result result = service.Update(execParam, krt_extraNapok);

                                        if (String.IsNullOrEmpty(result.ErrorCode))
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                            JavaScripts.RegisterCloseWindowClientScript(Page);
                                        }
                                        else
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
            }
        }
    }
}

