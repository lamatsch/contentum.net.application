﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ExtraNapokList.aspx.cs" Inherits="ExtraNapokList" Title="Untitled Page" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc1:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelenítése-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Frissítés jelzése-->
    <uc2:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
        
    <!--Fő lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeExtraNapok" OnClientClick="return false;" />
           </td>
           <td style="text-align: left; vertical-align: top; width: 100%;">
             <asp:UpdatePanel ID="updatePanelExtraNapok" runat="server" OnLoad="updatePanelExtraNapok_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeExtraNapok" runat="server" TargetControlID="panelExtraNapok"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeExtraNapok" CollapseControlID="btnCpeExtraNapok"
                        ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                        ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeExtraNapok"
                        ExpandedSize="0" ExpandedText="Extra napok listája" CollapsedText="Extra napok listája">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="panelExtraNapok" runat="server" Width="100%"> 
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                              <asp:GridView id="gridViewExtraNapok" runat="server" GridLines="None" BorderWidth="1px" OnRowCommand="gridViewExtraNapok_RowCommand" 
                                 OnPreRender="gridViewExtraNapok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                 DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewExtraNapok_RowDataBound" OnSorting="gridViewExtraNapok_Sorting" AllowPaging="true">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>                                
                                                <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                 &nbsp;&nbsp;
                                                 <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                             </HeaderTemplate>
                                             <ItemTemplate>
                                                 <asp:CheckBox id="check" runat="server" Text='<%# Eval("Datum") %>' CssClass="HideCheckBoxText" />
                                             </ItemTemplate>
                                        </asp:TemplateField>   
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Datum" HeaderText="Nap" SortExpression="Datum">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Típus" SortExpression="Jelzo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <asp:Label ID="labelJelzo" runat="server" Text='<%# GetMunkanapJelzo(Eval("Jelzo")) %>'></asp:Label>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyzés" SortExpression="Megjegyzes">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="250px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                         </asp:TemplateField>
                                     </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                             </td>
                             </tr>
                         </table>
                    </asp:Panel>   
                 </ContentTemplate>
              </asp:UpdatePanel>  
           </td>
        </tr>
     </table>              
</asp:Content>

