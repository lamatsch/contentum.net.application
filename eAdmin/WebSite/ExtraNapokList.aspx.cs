﻿using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class ExtraNapokList : System.Web.UI.Page
{
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ExtraNapokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keresési objektum típusának megadása
        ListHeader1.SearchObjectType = typeof(KRT_Extra_NapokSearch);

        //Fő lista megjelenések testreszabása
        ListHeader1.HeaderLabel = Resources.List.ExtraNapokLisHeaderTitle;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //Fő lista gombokhoz kliens oldali szkriptek regisztrálása
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("ExtraNapokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelExtraNapok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ExtraNapokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelExtraNapok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewExtraNapok.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewExtraNapok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewExtraNapok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewExtraNapok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewExtraNapok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelExtraNapok.ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = gridViewExtraNapok;

        //Fő lista összes kiválasztása és az összes kiválasztás megszüntetése checkbox-aihoz szkriptek beállítása
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewExtraNapok);

        //Fő lista eseménykezelő függvényei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztrálása
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hibaüzenet panel elüntetése
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire jó */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_Extra_NapokSearch());

        //A fő lista adatkötés, ha nem postback a kérés
        if (!IsPostBack)
            ExtraNapokGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Fő lista jogosultságainak beállítása
        ListHeader1.EnableLeftButtonsByFunctionRight("ExtraNap");

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewExtraNapok);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }
    #endregion Base Page

    #region Master List

    //adatkötés meghívása default értékekkel
    protected void ExtraNapokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewExtraNapok", ViewState, "KRT_Extra_Napok.Datum");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewExtraNapok", ViewState);

        ExtraNapokGridViewBind(sortExpression, sortDirection);
    }


    //adatkötés webszolgáltatástól kapott adatokkal
    protected void ExtraNapokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_Extra_NapokSearch search = (KRT_Extra_NapokSearch)Search.GetSearchObject(Page, new KRT_Extra_NapokSearch());

        search.OrderBy = Search.GetOrderBy("gridViewExtraNapok", ViewState, SortExpression, SortDirection);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(gridViewExtraNapok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esemény kezelője, ami egy sor adatkötése után hívódik
    protected void gridViewExtraNapok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockolás jelzése
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender eseménykezelője
    protected void gridViewExtraNapok_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeExtraNapok);
        ListHeader1.RefreshPagerLabel();
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ExtraNapokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeExtraNapok);
        ExtraNapokGridViewBind();
    }

    //GridView RowCommand eseménykezelője, amit valamelyik sorban fellépett parancs vált ki
    protected void gridViewExtraNapok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiválasztása (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewExtraNapok, selectedRowNumber, "check");
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //Fő lista kiválasztott elem esetén érvényes funkciók regisztrálása
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ExtraNapokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelExtraNapok.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ExtraNapokForm.aspx"
                 , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelExtraNapok.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "Krt_Extra_Napok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelExtraNapok.ClientID);

        }
    }

    //UpdatePanel Load eseménykezelője
    protected void updatePanelExtraNapok_Load(object sender, EventArgs e)
    {
        //Fő lista frissítése
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ExtraNapokGridViewBind();
                    break;
            }
        }
    }

    //Fő lista bal oldali funkciógombjainak eseménykezelője
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //törlés parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedExtraNapok();
            ExtraNapokGridViewBind();
        }
    }

    //kiválasztott elemek törlése
    private void deleteSelectedExtraNapok()
    {

        if (FunctionRights.GetFunkcioJog(Page, "OrszagInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewExtraNapok, EErrorPanel1, ErrorUpdatePanel);

            KRT_Extra_NapokService service = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //Fő lista jobb oldali funkciógombjainak eseménykezelője
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail küldés parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedExtraNapokRecords();
                ExtraNapokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedExtraNapokRecords();
                ExtraNapokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedExtraNapok();
                break;
        }
    }

    //kiválasztott elemek zárolása
    private void lockSelectedExtraNapokRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewExtraNapok, "KRT_Extra_Napok"
            , "OrszagLock", "OrszagForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiválasztott emlemek elengedése
    /// </summary>
    private void unlockSelectedExtraNapokRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewExtraNapok, "KRT_Extra_Napok"
            , "OrszagLock", "OrszagForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewExtraNapok -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedExtraNapok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewExtraNapok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_ExtraNapok");
        }
    }


    //GridView Sorting esménykezelője
    protected void gridViewExtraNapok_Sorting(object sender, GridViewSortEventArgs e)
    {
        ExtraNapokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewExtraNapok", ViewState, e.SortExpression));
    }

    public string GetMunkanapJelzo(object oJelzo)
    {
        if (oJelzo == null)
            return KodTarak.EXTRANAPOK.Ervenytelen;

        string jelzo = oJelzo.ToString();

        return ExtraNapok.Jelzo.GetTextFromValue(jelzo);
    }

    #endregion
}
