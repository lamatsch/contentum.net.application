﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ExtraNapokSearch.aspx.cs" Inherits="ExtraNapokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc4" %>   
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc5" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,ExtraNapokSearchHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNap" runat="server" Text="Nap:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:DatumIntervallum_SearchCalendarControl runat="server" ID="NapIntervallum" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTipus" runat="server" Text="Típus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList runat="server" ID="ddlistTipus" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegjgyzes" runat="server" Text="Megjegyzés:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textMegjegyzes" runat="server" CssClass="mrUrlapInput" Rows="3"
                                    TextMode="MultiLine">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2">
                                <uc4:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" TartomanyVisible="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc3:talalatokszama_searchformcomponent id="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

