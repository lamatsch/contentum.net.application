﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ExtraNapokSearch : System.Web.UI.Page
{
    private Type _type = typeof(KRT_Extra_NapokSearch);

    private void InitComponents()
    {
        ExtraNapok.Jelzo.FillListControl(ddlistTipus, true);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            InitComponents();

            KRT_Extra_NapokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_Extra_NapokSearch)Search.GetSearchObject(Page, new KRT_Extra_NapokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_Extra_NapokSearch krt_extraNapokSearch = null;
        if (searchObject != null) krt_extraNapokSearch = (KRT_Extra_NapokSearch)searchObject;

        if (krt_extraNapokSearch != null)
        {
            NapIntervallum.SetComponentFromSearchObjectFields(krt_extraNapokSearch.Datum);
            if (!String.IsNullOrEmpty(krt_extraNapokSearch.Jelzo.Value))
            {
                ddlistTipus.SelectedValue = krt_extraNapokSearch.Jelzo.Value;
            }
            else
            {
                ddlistTipus.SelectedValue = KodTarak.EXTRANAPOK.Empty;
            }
            textMegjegyzes.Text = krt_extraNapokSearch.Megjegyzes.Value;
            Ervenyesseg_SearchFormComponent1.SetDefault(krt_extraNapokSearch.ErvKezd, krt_extraNapokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_Extra_NapokSearch SetSearchObjectFromComponents()
    {
        KRT_Extra_NapokSearch krt_extraNapokSearch = (KRT_Extra_NapokSearch)SearchHeader1.TemplateObject;
        if (krt_extraNapokSearch == null)
        {
            krt_extraNapokSearch = new KRT_Extra_NapokSearch();
        }

        NapIntervallum.SetSearchObjectFields(krt_extraNapokSearch.Datum);

        if (ddlistTipus.SelectedValue != KodTarak.EXTRANAPOK.Empty)
        {
            krt_extraNapokSearch.Jelzo.Value = ddlistTipus.SelectedValue;
            krt_extraNapokSearch.Jelzo.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(textMegjegyzes.Text))
        {
            krt_extraNapokSearch.Megjegyzes.Value = textMegjegyzes.Text;
            krt_extraNapokSearch.Megjegyzes.Operator = Search.GetOperatorByLikeCharater(textMegjegyzes.Text);
        }
 

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(krt_extraNapokSearch.ErvKezd, krt_extraNapokSearch.ErvVege);

        return krt_extraNapokSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_Extra_NapokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            //kiválasztott rekord törlése
            ////JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_Extra_NapokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_Extra_NapokSearch();
    }
}
