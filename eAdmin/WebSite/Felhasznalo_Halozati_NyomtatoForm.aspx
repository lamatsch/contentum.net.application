﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="Felhasznalo_Halozati_NyomtatoForm.aspx.cs" Inherits="Felhasznalo_Halozati_NyomtatoForm" %>

<%@ Register Src="~/Component/NyomtatokDropdownList.ascx" TagName="NyomtatokDropDown"
    TagPrefix="uc1" %>



<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc10:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Felhasznalo_Halozati_NyomtatoFormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel ID="Felhasznalo_HalozatiNyomtatokUpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption">
                                            <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label1" runat="server" Text="Felhasználó:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:FelhasznaloTextBox ID="FelhasznaloTextBox1" runat="server"></uc7:FelhasznaloTextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td style="width: 200px; height: 30px" class="mrUrlapCaption">
                                            <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label_Nyomtato" runat="server" Text="Nyomtató:"></asp:Label>
                                        </td>
                                        <td style="height: 30px" class="mrUrlapMezo">
                                            <uc1:NyomtatokDropDown ID="NyomtatokDropDown"
                                                runat="server" IsMultiSearchMode="false"/>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption_Top">
                                        </td>
                                        <td class="mrUrlapMezo">
                                            &nbsp;<br />
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption">
                                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="Érvényesség:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                            </uc6:ErvenyessegCalendarControl>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>

    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
