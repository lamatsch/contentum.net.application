﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Felhasznalo_Halozati_NyomtatoForm : System.Web.UI.Page
{
    private string Command = "";
    private PageView pageView = null;
    private String nyomtatoID = String.Empty;
    private String felhasznaloID = String.Empty;
    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        if (Command == CommandName.View || Command == CommandName.Modify )
        {
            nyomtatoID = Request.QueryString.Get(QueryStringVars.Id);
            felhasznaloID = Request.QueryString.Get(QueryStringVars.FelhasznaloId);
            if (String.IsNullOrEmpty(nyomtatoID) || String.IsNullOrEmpty(felhasznaloID))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_HalozatiNyomtatokSearch search = new KRT_HalozatiNyomtatokSearch();
                switch (Command)
                { 
                    case CommandName.View:
                        if (!String.IsNullOrEmpty(nyomtatoID))
                        {
                            try
                            {
                                GetPrinters(search, nyomtatoID);
                            }
                            catch (Exception exp)
                            {
                                Result resError = Contentum.eUtility.ResultException.GetResultFromException(exp);
                                resError.ErrorMessage = exp.Message;

                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                                if (ErrorUpdatePanel != null)
                                {
                                    ErrorUpdatePanel.Update();
                                }
                            }
                            NyomtatokDropDown.Enabled = false;
                        }
                        LoadComponentsFromBusinessObject();
                        FelhasznaloTextBox1.Enabled = false;
                        ErvenyessegCalendarControl1.Enabled = false;

                        
                        break;
                    case CommandName.Modify:
                        search = new KRT_HalozatiNyomtatokSearch();
                        if (!String.IsNullOrEmpty(nyomtatoID))
                        {
                            try
                            {
                                GetPrinters(search, nyomtatoID);
                            }
                            catch (Exception exp)
                            {
                                Result resError = Contentum.eUtility.ResultException.GetResultFromException(exp);
                                resError.ErrorMessage = exp.Message;

                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                                if (ErrorUpdatePanel != null)
                                {
                                    ErrorUpdatePanel.Update();
                                }
                            }
                            NyomtatokDropDown.Enabled = true;
                        }
                        LoadComponentsFromBusinessObject();
                        FelhasznaloTextBox1.Enabled = true;
                        ErvenyessegCalendarControl1.Enabled = true;
                        break;
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string felhasznaloId = UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id;
            String nyomtatoID = Request.QueryString.Get(QueryStringVars.Id);
            if (!String.IsNullOrEmpty(felhasznaloId))
            {
                FelhasznaloTextBox1.Id_HiddenField = felhasznaloId;
                FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
                //FelhasznaloTextBox1.Enabled = false;
            }
            KRT_HalozatiNyomtatokSearch search = new KRT_HalozatiNyomtatokSearch();
            if (!String.IsNullOrEmpty(nyomtatoID))
            {
                try
                {
                    GetPrinters(search, nyomtatoID);
                }
                catch (Exception exp)
                {
                    Result resError = Contentum.eUtility.ResultException.GetResultFromException(exp);
                    resError.ErrorMessage = exp.Message;

                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                    if (ErrorUpdatePanel != null)
                    {
                        ErrorUpdatePanel.Update();
                    }
                }
                NyomtatokDropDown.Enabled = false;
            }
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        pageView.SetViewOnPage(Command);
    }

    private void GetPrinters(KRT_HalozatiNyomtatokSearch search, string nyomtatoId)
    {//ha több van kiválasztás
        ExecParam ExecParam_Printers = UI.SetExecParamDefault(Page, new ExecParam());

        using (Contentum.eAdmin.Service.KRT_HalozatiNyomtatokService service_printer = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService())
        {
            var res = service_printer.GetAll(ExecParam_Printers, search);
            NyomtatokDropDown.FillDropDownList(res, "Id", "Nev", EErrorPanel1);
            if (!String.IsNullOrEmpty(nyomtatoId))
                NyomtatokDropDown.SelectedValue = nyomtatoId;
            if (res.Ds.Tables[0].Rows.Count < 2)
                NyomtatokDropDown.Enabled = false;
        }
        return;
    }
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_HalozatiNyomtato" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            using (var service = eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService())
                            {
                                KRT_Felhasznalok_Halozati_Nyomtatoi krt_felhasznalo_nyomtato = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = service.Insert(execParam, krt_felhasznalo_nyomtato);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            var res = LoadFelhasznalo_halozatiNyomtatoRecord();
                            if (res.IsError)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                                return;
                            }
                            string recordId = res.Ds.Tables[0].Rows[0]["Id"].ToString();

                            //recordId
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                using (var service = eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService())
                                {
                                    KRT_Felhasznalok_Halozati_Nyomtatoi krt_felhasznalo_nyomtato = GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    //Result result = service.Update(execParam, krt_felhasznalo_szerepkor);
                                    Result result = service.Update(execParam, krt_felhasznalo_nyomtato);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
    // business object --> form
    private void LoadComponentsFromBusinessObject()
    {
        KRT_Felhasznalok_Halozati_Nyomtatoi KRTFelhasznaloNyomtatoi = new KRT_Felhasznalok_Halozati_Nyomtatoi();
        FelhasznaloTextBox1.Id_HiddenField = felhasznaloID;
        FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FelhasznaloTextBox1.Enabled = false;
        var res = LoadFelhasznalo_halozatiNyomtatoRecord();
        if (!res.IsError)
        {
            KRTFelhasznaloNyomtatoi.ErvKezd = res.Ds.Tables[0].Rows[0]["ErvKezd"].ToString();
            KRTFelhasznaloNyomtatoi.ErvVege = res.Ds.Tables[0].Rows[0]["ErvVege"].ToString();
            KRTFelhasznaloNyomtatoi.Felhasznalo_Id = res.Ds.Tables[0].Rows[0]["Felhasznalo_Id"].ToString();
            KRTFelhasznaloNyomtatoi.Halozati_Nyomtato_Id = res.Ds.Tables[0].Rows[0]["Halozati_Nyomtato_Id"].ToString();
            KRTFelhasznaloNyomtatoi.Id = res.Ds.Tables[0].Rows[0]["Id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.LetrehozasIdo = res.Ds.Tables[0].Rows[0]["LetrehozasIdo"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Letrehozo_id = res.Ds.Tables[0].Rows[0]["Letrehozo_id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.ModositasIdo = res.Ds.Tables[0].Rows[0]["ModositasIdo"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Modosito_id = res.Ds.Tables[0].Rows[0]["Modosito_id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Note = res.Ds.Tables[0].Rows[0]["Note"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Stat_id = res.Ds.Tables[0].Rows[0]["Stat_id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Tranz_id = res.Ds.Tables[0].Rows[0]["Tranz_id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.UIAccessLog_id = res.Ds.Tables[0].Rows[0]["UIAccessLog_id"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Ver = res.Ds.Tables[0].Rows[0]["Ver"].ToString();
            KRTFelhasznaloNyomtatoi.Base.ZarolasIdo = res.Ds.Tables[0].Rows[0]["ZarolasIdo"].ToString();
            KRTFelhasznaloNyomtatoi.Base.Zarolo_id = res.Ds.Tables[0].Rows[0]["Zarolo_id"].ToString();
        }
        else
        { 
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            if (ErrorUpdatePanel != null)
            {
                ErrorUpdatePanel.Update();
            }
        }
                        
        

        ErvenyessegCalendarControl1.ErvKezd = KRTFelhasznaloNyomtatoi.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = KRTFelhasznaloNyomtatoi.ErvVege;
        FormHeader1.Record_Ver = KRTFelhasznaloNyomtatoi.Base.Ver;
        FormPart_CreatedModified1.SetComponentValues(KRTFelhasznaloNyomtatoi.Base);
    }
   
    // form --> business object
    private KRT_Felhasznalok_Halozati_Nyomtatoi GetBusinessObjectFromComponents()
    {
        KRT_Felhasznalok_Halozati_Nyomtatoi krt_felhasznalo_nyomtato = new KRT_Felhasznalok_Halozati_Nyomtatoi();
        krt_felhasznalo_nyomtato.Updated.SetValueAll(false);
        krt_felhasznalo_nyomtato.Base.Updated.SetValueAll(false);

        krt_felhasznalo_nyomtato.Halozati_Nyomtato_Id = NyomtatokDropDown.SelectedValue;
        krt_felhasznalo_nyomtato.Updated.Halozati_Nyomtato_Id = pageView.GetUpdatedByView(NyomtatokDropDown);

        krt_felhasznalo_nyomtato.Felhasznalo_Id = FelhasznaloTextBox1.Id_HiddenField;
        krt_felhasznalo_nyomtato.Updated.Felhasznalo_Id = pageView.GetUpdatedByView(FelhasznaloTextBox1);
        
        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_felhasznalo_nyomtato, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_felhasznalo_nyomtato.Base.Ver = FormHeader1.Record_Ver;
        krt_felhasznalo_nyomtato.Base.Updated.Ver = true;

        return krt_felhasznalo_nyomtato;
    }

    private Result LoadFelhasznalo_halozatiNyomtatoRecord()
    {
        ExecParam _ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_Felhasznalok_Halozati_NyomtatoiSearch search = new KRT_Felhasznalok_Halozati_NyomtatoiSearch();
        var res = new Result();
        using (var service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService())
        {
            search.Halozati_Nyomtato_Id.Value = nyomtatoID;
            search.Halozati_Nyomtato_Id.Operator = Query.Operators.equals;
            search.Halozati_Nyomtato_Id.Group = "100";
            search.Felhasznalo_Id.Value = felhasznaloID;
            search.Felhasznalo_Id.Operator = Query.Operators.equals;

            res = service.GetAll(_ExecParam, search);
            

        }
        return res;
    }
}