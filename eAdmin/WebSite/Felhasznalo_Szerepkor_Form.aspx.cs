using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Felhasznalo_Szerepkor_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo_Szerepkor" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Felhasznalo_Szerepkor businessObj = (KRT_Felhasznalo_Szerepkor)result.Record;
                    LoadComponentsFromBusinessObject(businessObj);

                    if (Command == CommandName.View)
                    {
                        // ha �truh�zott szerepk�r (van Helyettesites_Id), speci�lisan kezelj�k:
                        // a csoport a megb�z� csoportja, a csoporttags�g alapj�n hat�rozzuk meg
                        if (!String.IsNullOrEmpty(businessObj.Helyettesites_Id))
                        {
                            KRT_HelyettesitesekService service_helyettesitesek = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                            ExecParam execParam_helyettesitesek = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_helyettesitesek.Record_Id = businessObj.Helyettesites_Id;
                            Result result_helyettesitesekGet = service_helyettesitesek.Get(execParam_helyettesitesek);

                            if (String.IsNullOrEmpty(result_helyettesitesekGet.ErrorCode))
                            {
                                KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result_helyettesitesekGet.Record;

                                KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                                ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam_csoporttagok.Record_Id = krt_Helyettesitesek.CsoportTag_ID_helyettesitett;

                                Result result_csoporttagokGet = service_csoporttagok.Get(execParam_csoporttagok);

                                if (String.IsNullOrEmpty(result_csoporttagokGet.ErrorCode))
                                {
                                    KRT_CsoportTagok krt_CsoportTagok = (KRT_CsoportTagok)result_csoporttagokGet.Record;

                                    CsoportTextBox1.Id_HiddenField = krt_CsoportTagok.Csoport_Id;
                                    CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                }
                                else
                                {
                                    // hiba a csoporttags�g lek�rdez�s�n�l
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_helyettesitesekGet);
                                }

                            }
                            else
                            {
                                // hiba a helyettes�t�s lek�rdez�s�n�l
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_helyettesitesekGet);
                            }
                        }
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }


            }
        }
        else if (Command == CommandName.New)
        {
            string szerepkorId = Request.QueryString.Get(QueryStringVars.SzerepkorId);
            if (!String.IsNullOrEmpty(szerepkorId))
            {
                SzerepkorTextBox1.Id_HiddenField = szerepkorId;
                SzerepkorTextBox1.SetSzerepkorTextBoxById(FormHeader1.ErrorPanel);
                SzerepkorTextBox1.Enabled = false;
            }
            string felhasznaloId = Request.QueryString.Get(QueryStringVars.FelhasznaloId);
            if (!String.IsNullOrEmpty(felhasznaloId))
            {
                FelhasznaloTextBox1.Id_HiddenField = felhasznaloId;
                FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
                FelhasznaloTextBox1.Enabled = false;
            }
            string csoportId = Request.QueryString.Get(QueryStringVars.CsoportId);
            if (!String.IsNullOrEmpty(csoportId))
            {
                CsoportTextBox1.Id_HiddenField = csoportId;
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                CsoportTextBox1.Enabled = false;
            }
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }
    
    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor)
    {
        SzerepkorTextBox1.Id_HiddenField = krt_felhasznalo_szerepkor.Szerepkor_Id;
        SzerepkorTextBox1.SetSzerepkorTextBoxById(FormHeader1.ErrorPanel);
        SzerepkorTextBox1.Enabled = false;

        FelhasznaloTextBox1.Id_HiddenField = krt_felhasznalo_szerepkor.Felhasznalo_Id;
        FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FelhasznaloTextBox1.Enabled = false;

        CsoportTextBox1.Id_HiddenField = krt_felhasznalo_szerepkor.Csoport_Id;
        CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);        

        cbMegbizasbanKapott.Checked = String.IsNullOrEmpty(krt_felhasznalo_szerepkor.Helyettesites_Id) ? false : true;

        ErvenyessegCalendarControl1.ErvKezd = krt_felhasznalo_szerepkor.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_felhasznalo_szerepkor.ErvVege;

        if (!String.IsNullOrEmpty(krt_felhasznalo_szerepkor.Helyettesites_Id))
        {
            // megb�z�sban kapottat itt nem lehet m�dos�tani
            ErvenyessegCalendarControl1.ReadOnly = true;
            CsoportTextBox1.Enabled = false;
        }

        FormHeader1.Record_Ver = krt_felhasznalo_szerepkor.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_felhasznalo_szerepkor.Base);

        if (Command == CommandName.View)
        {
            ErvenyessegCalendarControl1.ReadOnly = true;
            CsoportTextBox1.ReadOnly = true;
        }
    }

    // form --> business object
    private KRT_Felhasznalo_Szerepkor GetBusinessObjectFromComponents()
    {
        KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = new KRT_Felhasznalo_Szerepkor();
        krt_felhasznalo_szerepkor.Updated.SetValueAll(false);
        krt_felhasznalo_szerepkor.Base.Updated.SetValueAll(false);

        krt_felhasznalo_szerepkor.Szerepkor_Id = SzerepkorTextBox1.Id_HiddenField;
        krt_felhasznalo_szerepkor.Updated.Szerepkor_Id = pageView.GetUpdatedByView(SzerepkorTextBox1);

        krt_felhasznalo_szerepkor.Felhasznalo_Id = FelhasznaloTextBox1.Id_HiddenField;
        krt_felhasznalo_szerepkor.Updated.Felhasznalo_Id = pageView.GetUpdatedByView(FelhasznaloTextBox1);

        krt_felhasznalo_szerepkor.Csoport_Id = CsoportTextBox1.Id_HiddenField;
        krt_felhasznalo_szerepkor.Updated.Csoport_Id = pageView.GetUpdatedByView(CsoportTextBox1);

        //krt_felhasznalo_szerepkor.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_felhasznalo_szerepkor.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_felhasznalo_szerepkor.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_felhasznalo_szerepkor.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_felhasznalo_szerepkor, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_felhasznalo_szerepkor.Base.Ver = FormHeader1.Record_Ver;
        krt_felhasznalo_szerepkor.Base.Updated.Ver = true;

        return krt_felhasznalo_szerepkor;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(SzerepkorTextBox1);
            compSelector.Add_ComponentOnClick(FelhasznaloTextBox1);
            compSelector.Add_ComponentOnClick(CsoportTextBox1);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                            KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_felhasznalo_szerepkor);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                /** 
                                 * <FIGYELEM!!!> 
                                 * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                                 * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                                 *  az �sszes felhaszn�l� Session-jeiben)
                                 */
                                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                                /**
                                 * </FIGYELEM>
                                 */


                                //// ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                //JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                                KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                //Result result = service.Update(execParam, krt_felhasznalo_szerepkor);
                                Result result = service.UpdateWithMegbizasControl(execParam, krt_felhasznalo_szerepkor);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


}
