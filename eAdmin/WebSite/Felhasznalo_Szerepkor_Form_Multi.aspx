<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="Felhasznalo_Szerepkor_Form_Multi.aspx.cs" Inherits="Felhasznalo_Szerepkor_Form_Multi" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/SzerepkorTextBox.ascx" TagName="SzerepkorTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Felhasznalo_Szerepkor_FormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel ID="Felhasznalo_SzrepekorUpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption">
                                            <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label1" runat="server" Text="Felhaszn�l�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:FelhasznaloTextBox ID="FelhasznaloTextBox1" runat="server"></uc7:FelhasznaloTextBox>
                                        </td>
                                    </tr>
             <%--                       <tr class="urlapSor">
                                        <td style="width: 200px; height: 30px" class="mrUrlapCaption">
                                            <asp:Label ID="Label2" runat="server" Text="Csoport:"></asp:Label>
                                        </td>
                                        <td style="height: 30px" class="mrUrlapMezo">
                                            <uc8:CsoportTextBox ID="CsoportTextBox1" Validate="false" runat="server"></uc8:CsoportTextBox>
                                        </td>
                                    </tr>             --%>                      
                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption_Top">
                                        </td>
                                        <td class="mrUrlapMezo">
                                            &nbsp;<br />
                                            &nbsp;
                                        </td>
                                    </tr>    
<%--                                    <tr class="urlapSor">
                                        <td style="width: 200px" class="mrUrlapCaption">
                                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                            </uc6:ErvenyessegCalendarControl>
                                        </td>
                                    </tr>--%>
                                </tbody>
                            </table>

                             <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;margin-top:10px; margin-bottom:10px;">
                                  <tr>
                                      <td style="width:50%;padding: 0px 10px;">
                                          <div style="text-align:left;padding-bottom: 2px;">
                                             <table cellpadding="0" cellspacing="0">
                                                 <tr>
                                                     <td style="width:auto">
                                                          <span style="font-weight:bold; text-decoration:underline;">Szerepk�r�k:</span>
                                                     </td>
                                                     <td style="width:100%; padding-left:5px;padding-right:8px">
                                                         <asp:TextBox ID="TextBoxSearch" runat="server" Width="100%" CausesValidation="false" OnTextChanged="TextBoxSearch_TextChanged" AutoPostBack="true"/>
                                                     </td>
                                                     <td style="width:auto">
                                                         <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/egyeb/kereses.gif" CausesValidation="false">
                                                         </asp:ImageButton>
                                                     </td>
                                                 </tr>
                                             </table>                
                                        </div>

                                      </td>
                                      <td>
                                      </td>
                                      <td style="width:50%;padding: 0px 10px;">
                                          <div style="text-align:left;padding-bottom: 2px;">
                                            <span style="font-weight:bold; text-decoration:underline;">Hozz�rendelt szerepk�r�k:</span>
                                        </div>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td style="width:50%;padding:0px 10px;">
                                        <asp:ListBox ID="SzerepkorokList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="AddButton" Text=">" Width="50px" style="margin-bottom: 5px;" ToolTip="Hozz�ad" CausesValidation="false" OnClick="AddButton_Click"/>
                                        <asp:Button runat="server" ID="RemoveButton" Text="<" Width="50px" style="margin-top: 5px;" ToolTip="T�r�l" CausesValidation="false" OnClick="RemoveButton_Click"/>
                                    </td>
                                    <td style="width:50%;padding:0px 10px;">
                                        <asp:ListBox ID="HozzaRendeltSzerepkorokList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                </tr> 
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td colspan="3" style="padding:10px;">
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                             </table>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
