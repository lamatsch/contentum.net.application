using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;
using System.Linq;

public partial class Felhasznalo_Szerepkor_Form_Multi : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo_Szerepkor" + Command);
                break;
        }

        if (Command == CommandName.Modify)
        {
            string felhasznaloId = Request.QueryString.Get(QueryStringVars.FelhasznaloId);
            if (!String.IsNullOrEmpty(felhasznaloId))
            {
                FelhasznaloTextBox1.Id_HiddenField = felhasznaloId;
                FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
                FelhasznaloTextBox1.Enabled = false;
            }

            if (!IsPostBack)
            {
                FillSzerepkorokList();
                FillHozzarendeltSzerepkorokList(felhasznaloId);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }   

    // form --> business object
    private KRT_Felhasznalo_Szerepkor GetBusinessObjectFromComponentsForInsert(string szerepkorId)
    {
        KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = new KRT_Felhasznalo_Szerepkor();
        krt_felhasznalo_szerepkor.Updated.SetValueAll(false);
        krt_felhasznalo_szerepkor.Base.Updated.SetValueAll(false);

        krt_felhasznalo_szerepkor.Szerepkor_Id = szerepkorId;
        krt_felhasznalo_szerepkor.Updated.Szerepkor_Id = true;

        krt_felhasznalo_szerepkor.Felhasznalo_Id = FelhasznaloTextBox1.Id_HiddenField;
        krt_felhasznalo_szerepkor.Updated.Felhasznalo_Id = true;

        krt_felhasznalo_szerepkor.ErvKezd = DateTime.Now.ToString();
        krt_felhasznalo_szerepkor.Updated.ErvKezd = true;

        krt_felhasznalo_szerepkor.ErvVege = new DateTime(4700, 12, 31).ToString();
        krt_felhasznalo_szerepkor.Updated.ErvVege = true;

        krt_felhasznalo_szerepkor.Base.Ver = "1";
        krt_felhasznalo_szerepkor.Base.Updated.Ver = true;

        return krt_felhasznalo_szerepkor;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;            
            compSelector.Add_ComponentOnClick(FelhasznaloTextBox1);            
            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + Command))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            List<string> szerepkorokIds = new List<string>();

                            foreach (ListItem item in HozzaRendeltSzerepkorokList.Items)
                            {
                                szerepkorokIds.Add(item.Value);
                            }

                            KRT_Felhasznalo_SzerepkorService felhasznaloSzerepkorokService = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                            KRT_SzerepkorokService szerepkorokService = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
                            felhasznalo.Id = FelhasznaloTextBox1.Id_HiddenField;
                            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();
                            search.OrderBy = "Szerepkor_Nev";

                            Result result = szerepkorokService.GetAllByFelhasznalo(execParam, felhasznalo, search);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                break;
                            }

                            List<string> CurrentSzerepkorIds = new List<string>();

                            foreach (DataRow row in result.Ds.Tables[0].Rows)
                            {
                                string szerepkorId = row["Id"].ToString();
                                CurrentSzerepkorIds.Add(szerepkorId);
                            }

                            List<string> AddSzerepkorIds = new List<string>();
                            List<string> RemovedSzerepkorIds = new List<string>();

                            foreach (string szerepkorId in szerepkorokIds)
                            {
                                if (!CurrentSzerepkorIds.Contains(szerepkorId))
                                {
                                    AddSzerepkorIds.Add(szerepkorId);
                                }
                            }

                            foreach (string szerepkorId in CurrentSzerepkorIds)
                            {
                                if (!szerepkorokIds.Contains(szerepkorId))
                                {
                                    RemovedSzerepkorIds.Add(szerepkorId);
                                }
                            }

                            foreach (string szerepkorId in AddSzerepkorIds)
                            {
                                KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = GetBusinessObjectFromComponentsForInsert(szerepkorId);

                                Result resultInsert = felhasznaloSzerepkorokService.Insert(execParam, krt_felhasznalo_szerepkor);

                                if (!String.IsNullOrEmpty(resultInsert.ErrorCode))
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultInsert);
                                    return;
                                }
                            }

                            var table = result.Ds.Tables[0].AsEnumerable();

                            List<ExecParam> execParams = new List<ExecParam>();
                            for (int i = 0; i < RemovedSzerepkorIds.Count; i++)
                            {
                                bool modosithato = string.IsNullOrEmpty(table.First(x => x["Id"].ToString() == RemovedSzerepkorIds[i])["Helyettesites_Id"].ToString());
                                if (modosithato)
                                {
                                    ExecParam newExecParam;
                                    newExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    newExecParam.Record_Id = RemovedSzerepkorIds[i];
                                    execParams.Add(newExecParam);
                                }
                            }
                            
                            Result invalidateResult = felhasznaloSzerepkorokService.MultiInvalidateWithMegbizasControl(execParams.ToArray());
                            if (!String.IsNullOrEmpty(invalidateResult.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, invalidateResult);
                                break;
                            }

                            if (AddSzerepkorIds.Count > 0 || RemovedSzerepkorIds.Count > 0)
                            {
                                /** 
                                 * <FIGYELEM!!!> 
                                 * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                                 * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                                 *  az �sszes felhaszn�l� Session-jeiben)
                                 */
                                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                                /**
                                 * </FIGYELEM>
                                 */
                            }

                            JavaScripts.RegisterCloseWindowClientScript(Page);

                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    void FillSzerepkorokList()
    {
        SzerepkorokList.Items.Clear();

        KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        KRT_SzerepkorokSearch search = new KRT_SzerepkorokSearch();
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.OrderBy = "Nev";
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(SzerepkorokList, result, "Nev", FormHeader1.ErrorPanel, null);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    void FillHozzarendeltSzerepkorokList(string felhasznaloId)
    {
        HozzaRendeltSzerepkorokList.Items.Clear();

        KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
        felhasznalo.Id = felhasznaloId;
        KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();
        search.OrderBy = "Szerepkor_Nev";

        Result result = service.GetAllByFelhasznalo(ExecParam, felhasznalo, search);

        UI.ListBoxFill(HozzaRendeltSzerepkorokList, result, "Id", "Szerepkor_Nev", FormHeader1.ErrorPanel, null);
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(SzerepkorokList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = HozzaRendeltSzerepkorokList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                HozzaRendeltSzerepkorokList.Items.Add(item);
            }
        }
    }

    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(HozzaRendeltSzerepkorokList);
        foreach (ListItem item in selectedItems)
        {
            HozzaRendeltSzerepkorokList.Items.Remove(item);
        }
    }

    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillSzerepkorokList();
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillSzerepkorokList();
    }

}
