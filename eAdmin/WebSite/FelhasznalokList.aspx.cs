using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class FelhasznalokList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();
    readonly string jsNincsKivalasztva = "alert('" + Resources.List.UI_NoSelectedItem + "');return false;";

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {


        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        CsoportokSubListHeader.RowCount_Changed += new EventHandler(CsoportokSubListHeader_RowCount_Changed);
        SzerepkorokSubListHeader.RowCount_Changed += new EventHandler(SzerepkorokSubListHeader_RowCount_Changed);

        CsoportokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsoportokSubListHeader_ErvenyessegFilter_Changed);
        SzerepkorokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(SzerepkorokSubListHeader_ErvenyessegFilter_Changed);

        SzerepkorokSubListHeader.MapVisible = true;

        // TODO: Csoporttags�gok sublistet megcsin�lni, egyel�re le van tiltva
        CsoportokTabPanel.Visible = false;
        TabContainer1.Tabs.Remove(CsoportokTabPanel);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.FelhasznalokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_FelhasznalokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        ListHeader1.RevalidateVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokSearch.aspx", ""
            , 800, 650, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FelhasznalokGridView.ClientID,"","check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(FelhasznalokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(FelhasznalokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(FelhasznalokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(FelhasznalokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = FelhasznalokGridView;

        CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsoportokGridView.ClientID);
        CsoportokSubListHeader.ButtonsClick += new CommandEventHandler(CsoportokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        CsoportokSubListHeader.AttachedGridView = CsoportokGridView;

        SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.MapOnClientClick= JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(SzerepkorokGridView.ClientID, "check", "cbMegbizasbanKapott", false);        
        SzerepkorokSubListHeader.ButtonsClick += new CommandEventHandler(SzerepkorokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        SzerepkorokSubListHeader.AttachedGridView = SzerepkorokGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_FelhasznalokSearch());

        if (!IsPostBack) FelhasznalokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        ListHeader1.RevalidateOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(FelhasznalokGridView.ClientID);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Invalidate);
        ListHeader1.RevalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Lock);
        
        CsoportokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTag" + CommandName.New);
        CsoportokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTag" + CommandName.View);
        CsoportokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTag" + CommandName.Modify);
        CsoportokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "CsoportTag" + CommandName.Invalidate);

        SzerepkorokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New);
        SzerepkorokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.View);
        SzerepkorokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        SzerepkorokSubListHeader.MapEnabled= FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        SzerepkorokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(FelhasznalokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);
    }


    #endregion


    #region Master List

    protected void FelhasznalokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FelhasznalokGridView", ViewState, "KRT_Felhasznalok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FelhasznalokGridView", ViewState);

        FelhasznalokGridViewBind(sortExpression, sortDirection);
    }

    protected void FelhasznalokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_FelhasznalokSearch search = (KRT_FelhasznalokSearch)Search.GetSearchObject(Page, new KRT_FelhasznalokSearch());
        search.OrderBy = Search.GetOrderBy("FelhasznalokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(FelhasznalokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

        Contentum.eUtility.EsemenyNaplo.Insert(ExecParam, "KRT_Felhasznalok", FunkcioKod, search, res);
    }

    protected void FelhasznalokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbEngedelyezett", "Engedelyezett");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void FelhasznalokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, FelhasznalokCPE);
        ListHeader1.RefreshPagerLabel();
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        FelhasznalokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, FelhasznalokCPE);
        FelhasznalokGridViewBind();
    }

    protected void FelhasznalokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FelhasznalokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);

            //FelhasznalokGridView_SelectRowCommand(id);
        }
    }

    //private void FelhasznalokGridView_SelectRowCommand(string felhasznaloId)
    //{
    //    ActiveTabRefresh(TabContainer1, felhasznaloId);
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "Krt_Felhasznalok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, FelhasznalokUpdatePanel.ClientID);

            CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.FelhasznaloId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.FelhasznaloId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            SzerepkorokSubListHeader.MapOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form_Multi.aspx"
                , "Command=" + CommandName.Modify + "&" + QueryStringVars.FelhasznaloId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    protected void FelhasznalokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FelhasznalokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedFelhasznalok();
            CallInvalidateButtonCommand();
            FelhasznalokGridViewBind();
        }

        if (e.CommandName == CommandName.Revalidate)
        {
            RevalidateSelectedFelhasznalok();
            FelhasznalokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedFelhasznaloRecords();
                FelhasznalokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedFelhasznaloRecords();
                FelhasznalokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedFelhasznalok();
                break;
        }
    }

    private void LockSelectedFelhasznaloRecords()
    {
        LockManager.LockSelectedGridViewRecords(FelhasznalokGridView, "KRT_Felhasznalok"
            , "FelhasznaloLock", "FelhasznaloForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedFelhasznaloRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(FelhasznalokGridView, "KRT_Felhasznalok"
            , "FelhasznaloLock", "FelhasznaloForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a FelhasznalokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedFelhasznalok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FelhasznaloInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);

                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a FelhasznalokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedFelhasznalok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Felhasznalok");
        }
    }

    protected void FelhasznalokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FelhasznalokGridViewBind(e.SortExpression, UI.GetSortToGridView("FelhasznalokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    private void RevalidateSelectedFelhasznalok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FelhasznaloInvalidate"))
        {
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            Result res = new Result();
            Result resError = new Result();
            int errorCount = 0;
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                xpm.Record_Id = selectedItemsList[i];
                res = service.Revalidate(xpm);
                if (res.IsError)
                {
                    resError = res;
                    errorCount++;
                }
            }

            if (errorCount > 0)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                if (errorCount > 1)
                {
                    EErrorPanel1.Header = EErrorPanel1.Header + " (Hib�k sz�ma: " + errorCount.ToString() + ")";
                }
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (FelhasznalokGridView.SelectedIndex == -1)
        {
            return;
        }
        string felhasznaloId = UI.GetGridViewSelectedRecordId(FelhasznalokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, felhasznaloId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string felhasznaloId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsoportokGridViewBind(felhasznaloId);
                CsoportokPanel.Visible = true;
                break;
            case 1:
                SzerepkorokGridViewBind(felhasznaloId);
                SzerepkorokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(CsoportokGridView);
                break;
            case 1:
                ui.GridViewClear(SzerepkorokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsoportokSubListHeader.RowCount = RowCount;
        SzerepkorokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
        {
            CsoportokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
        {
            SzerepkorokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
    }

    #endregion


    #region Csoportok Detail

    private void CsoportokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedCsoporttagok();
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
    }

    protected void CsoportokGridViewBind(string felhasznaloId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsoportokGridView", ViewState, "Csoport_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoportokGridView", ViewState);

        CsoportokGridViewBind(felhasznaloId, sortExpression, sortDirection);
    }

    protected void CsoportokGridViewBind(string felhasznaloId, String SortExpression, SortDirection SortDirection)
    {
        //if (!string.IsNullOrEmpty(felhasznaloId))
        //{
        //    KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
        //    ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //    KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
        //    szerepkor.Id = szerepkorId;
        //    KRT_Szerepkor_FunkcioSearch search = new KRT_Szerepkor_FunkcioSearch();
        //    search.OrderBy = Search.GetOrderBy("FunkciokGridView", ViewState, SortExpression, SortDirection);

        //    Result res = service.GetAllBySzerepkor(ExecParam, szerepkor, search);

        //    CsoportokSubListHeader.SetErvenyessegFields(search.ErvKezd,search.ErvVege);

        //    ui.GridViewFill(FunkciokGridView, res, FunkciokSubListHeader.RowCount, EErrorPanel1, ErrorUpdatePanel);

        //    UI.ClearGridViewRowSelection(FunkciokGridView);
        //}
        //else
        //{
        //    ui.GridViewClear(FunkciokGridView);
        //}
    }


    void CsoportokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
    }

    protected void CsoportokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
                    //{
                    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
                    //}
                    ActiveTabRefreshDetailList(CsoportokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsoporttagokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedCsoporttagok()
    {
        //if (FunctionRights.GetFunkcioJog(Page, "Szerepkor_FunkcioInvalidate"))
        //{
        //    List<string> deletableItemsList = ui.GetGridViewSelectedRows(FunkciokGridView, EErrorPanel1, ErrorUpdatePanel);
        //    KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService();
        //    ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        //    for (int i = 0; i < deletableItemsList.Count; i++)
        //    {
        //        execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParams[i].Record_Id = deletableItemsList[i];
        //    }
        //    Result result = service.MultiInvalidate(execParams);
        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        ErrorUpdatePanel.Update();
        //    }
        //}
        //else
        //{
        //    UI.RedirectToAccessDeniedErrorPage(Page);
        //}
    }

    protected void CsoportokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsoportokGridView, selectedRowNumber, "check");
        }
    }

    private void CsoportokGridView_RefreshOnClientClicks(string csoporttagokId)
    {
        string id = csoporttagokId;
        if (!String.IsNullOrEmpty(id))
        {
            CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void CsoportokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsoportokGridView.PageIndex;

        CsoportokGridView.PageIndex = CsoportokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != CsoportokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, CsoportokCPE);
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(CsoportokSubListHeader.Scrollable, CsoportokCPE);
        }
        CsoportokSubListHeader.PageCount = CsoportokGridView.PageCount;
        CsoportokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsoportokGridView);
    }


    void CsoportokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsoportokSubListHeader.RowCount);
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
    }

    protected void CsoportokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView)
            , e.SortExpression, UI.GetSortToGridView("CsoportokGridView", ViewState, e.SortExpression));
    }

    #endregion


    #region Szerepkorok Detail

    private void SzerepkorokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedSzerepkorok();
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
        }
    }

    protected void SzerepkorokGridViewBind(string felhasznaloId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SzerepkorokGridView", ViewState, "Szerepkor_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SzerepkorokGridView", ViewState);

        SzerepkorokGridViewBind(felhasznaloId, sortExpression, sortDirection);
    }

    protected void SzerepkorokGridViewBind(string felhasznaloId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(felhasznaloId))
        {
            KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
            felhasznalo.Id = felhasznaloId;
            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();

            search.OrderBy = Search.GetOrderBy("SzerepkorokGridView", ViewState, SortExpression, SortDirection);

            SzerepkorokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByFelhasznalo(ExecParam, felhasznalo, search);

            UI.GridViewFill(SzerepkorokGridView, res, SzerepkorokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);
        }
        else
        {
            ui.GridViewClear(SzerepkorokGridView);
        }
    }

    void SzerepkorokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
    }

    protected void SzerepkorokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
                    //{
                    //    SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
                    //}
                    ActiveTabRefreshDetailList(SzerepkorokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsoporttagokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedSzerepkorok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel);

            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = !UI.isGridViewCheckBoxChecked(SzerepkorokGridView, "cbMegbizasbanKapott", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            //Result result = service.MultiInvalidate(execParams.ToArray());
            Result result = service.MultiInvalidateWithMegbizasControl(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void SzerepkorokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SzerepkorokGridView, selectedRowNumber, "check");
        }
    }

    private void SzerepkorokGridView_RefreshOnClientClicks(string szerepkorId)
    {
        string id = szerepkorId;
        if (!String.IsNullOrEmpty(id))
        {
            SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            bool modosithato = !UI.isGridViewCheckBoxChecked(SzerepkorokGridView, "cbMegbizasbanKapott", id);

            if (modosithato)
            {
                SzerepkorokSubListHeader.ModifyEnabled = true;
                SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                SzerepkorokSubListHeader.ModifyEnabled = false;// nem engedj�k meg
            }
        }
    }

    protected void SzerepkorokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = SzerepkorokGridView.PageIndex;

        SzerepkorokGridView.PageIndex = SzerepkorokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != SzerepkorokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, SzerepkorokCPE);
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(SzerepkorokSubListHeader.Scrollable, SzerepkorokCPE);
        }
        SzerepkorokSubListHeader.PageCount = SzerepkorokGridView.PageCount;
        SzerepkorokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(SzerepkorokGridView);
    }

    void SzerepkorokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(SzerepkorokSubListHeader.RowCount);
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
    }

    protected void SzerepkorokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FelhasznalokGridView)
            , e.SortExpression, UI.GetSortToGridView("SzerepkorokGridView", ViewState, e.SortExpression));
    }

    #endregion



    #region INVALIDATE
    /// <summary>
    /// CallInvalidateButtonCommand
    /// </summary>
    private void CallInvalidateButtonCommand()
    {
        List<string> selectedItems = ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel);
        if (selectedItems == null || selectedItems.Count < 1)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedFelhasznalokNincsKivalasztva", jsNincsKivalasztva, true);
            return;
        }
        Dictionary<string, string> unique = new Dictionary<string, string>();
        foreach (string item in selectedItems)
        {
            if (!unique.ContainsKey(item))
                unique.Add(item, item);
        }
        System.Text.StringBuilder stringSelected = new System.Text.StringBuilder();

        string SelectedRecordId = UI.GetGridViewSelectedRecordId(FelhasznalokGridView);

        foreach (string s in unique.Values)
        {
            stringSelected.Append(s + ",");
        }
        stringSelected.Remove(stringSelected.Length - 1, 1);
        string queryString = string.Format("{0}={1}&{2}={3}&{4}={5}", CommandName.Command, CommandName.Invalidate, QueryStringVars.Mode, CommandName.Invalidate, QueryStringVars.Id, stringSelected.ToString());

        string js = JavaScripts.SetOnClientClickWithTimeout("Felhasznalok_Invalidate_Form.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Felhasznalok_Invalidate_Form", js, true);
    }
    #endregion INVALIDATE

}
