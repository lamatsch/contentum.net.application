<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FelhasznalokMultiSelect.aspx.cs" Inherits="FelhasznalokMultiSelect" Title="Untitled Page"%>
<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc3" %>
<%@ Register Src="~/Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc4"%>
<%@ Register Src="~/Component/FelhasznalokMultiSelectPanel.ascx" TagName="FelhasznalokMultiSelectPanel" TagPrefix="fmsp"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >    
    </asp:ScriptManager>
    
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,FelhasznalokLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always" RenderMode="Block">
                        <ContentTemplate>
                            <br />                    
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr>
                                    <td class="mrUrlapMezo">
                                        <fmsp:FelhasznalokMultiSelectPanel ID="fmspFelhasznalok" runat="server"
                                            CustomTextEnabled="true" Rows="10" Width="95%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; vertical-align: top;">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Megjegyzés:"></asp:Label><br />
                                        <asp:TextBox ID="MessageTextBox" runat="server" Rows="5" TextMode="MultiLine" Width="95%"></asp:TextBox></td>
                                </tr>
                            </table>       

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>

