using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class FelhasznalokMultiSelect : Contentum.eUtility.UI.PageBase
{
//    private bool disable_refreshLovList = false;
    private bool notify = false;
    private string hiddenFieldId = "";
    private string selectedRows = "";
    private string InitMessage
    {
        get
        {
            return Request.QueryString.Get("Message");
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //ScriptManager1.Services.Add(new ServiceReference("WrappedWebService/" + "Ajax.asmx"));

        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

        FormFooter1.Command = CommandName.New;
        
        // a megsem gombra torli a parent window hidden field erteket
        hiddenFieldId = Page.Request.QueryString.Get(QueryStringVars.HiddenFieldId);

        //LZS - BUG_4529
        notify = Convert.ToBoolean(Page.Request.QueryString.Get("Notify"));
        selectedRows = Page.Request.QueryString.Get("SelectedRows");

        if (!String.IsNullOrEmpty(hiddenFieldId))
        {
            String script = "";
            script += " if (window.dialogArguments) { ";
            script += " window.dialogArguments.parentWindow.document.getElementById('" + hiddenFieldId + "').value = ''; \n";
            script += " } else { "
                + " window.opener.document.getElementById('" + hiddenFieldId + "').value = ''; \n"
                + " }";
            FormFooter1.ImageButton_Cancel.OnClientClick = script + FormFooter1.ImageButton_Cancel.OnClientClick;
        }

        this.MessageTextBox.Text = InitMessage;


      
    }
    protected void Page_Load(object sender, EventArgs e)
    {   
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Save)
        {
            if (fmspFelhasznalok.Count > 0)
            {
                string selectedIds = String.Join(";", fmspFelhasznalok.SelectedIds);
                string selectedTexts = String.Join(";", fmspFelhasznalok.SelectedTexts);

                //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                // mask the common line break chars (depends on the client) for JavaScript
                string message = MessageTextBox.Text.Replace("\r", "\\r").Replace("\n", "\\n");

                //LZS - BUG_4529
                if (notify)
                {
                    Notify.SendSelectedItemsByEmail(Page, UI.StringToListBySeparator(selectedRows,','), UI.StringToListBySeparator(selectedTexts, ';'), message, "KRT_Szerepkorok");
                }

                JavaScripts.SendBackResultIdsListToCallingWindow(Page, selectedTexts, message);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                //disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayWarningOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }
}
