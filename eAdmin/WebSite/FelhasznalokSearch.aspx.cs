using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class FelhasznalokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_FelhasznalokSearch);


    private readonly ListItem authTypeWindows = new ListItem("Windows", "Windows");
    private readonly ListItem authTypeBasic = new ListItem("Basic", "Basic");
    private readonly ListItem emptyItem = new ListItem("Nincs megadva","Empty");

    #region radiobuttonlist
    private void FillRadioList()
    {
        rbListEngedelyzett.Items.Clear();
        rbListEngedelyzett.Items.Add(new ListItem("Enged�lyezve", Constants.Database.Yes));
        rbListEngedelyzett.Items.Add(new ListItem("Letiltva", Constants.Database.No));
        rbListEngedelyzett.Items.Add(new ListItem("�sszes", ""));
    }

    private void SetRadioList(string value)
    {
        rbListEngedelyzett.SelectedValue = value;
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.FelhasznalokSearchHeaderTitle;
       
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            ddownAuthType.Items.Clear();
            ddownAuthType.Items.Add(emptyItem);
            ddownAuthType.Items.Add(authTypeBasic);
            ddownAuthType.Items.Add(authTypeWindows);

            FillRadioList();

            KRT_FelhasznalokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_FelhasznalokSearch)Search.GetSearchObject(Page, new KRT_FelhasznalokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

        // Munkahely kiv�laszt�sn�l sz�r�s a szervezetre:
        Munkahely_PartnerTextBox.SetFilterType_Szervezet();
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_FelhasznalokSearch krt_felhasznalokSearch = null;
        if (searchObject != null) krt_felhasznalokSearch = (KRT_FelhasznalokSearch)searchObject;

        if (krt_felhasznalokSearch != null)
        {
            Usernev.Text = krt_felhasznalokSearch.UserNev.Value;
            Nev_TextBox.Text = krt_felhasznalokSearch.Nev.Value;

            PartnerTextBox1.Id_HiddenField = krt_felhasznalokSearch.Partner_id.Value;
            PartnerTextBox1.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);

            Munkahely_PartnerTextBox.Id_HiddenField = krt_felhasznalokSearch.Partner_Id_Munkahely.Value;
            Munkahely_PartnerTextBox.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);

            email_TextBox.Text = krt_felhasznalokSearch.EMail.Value;
            //System_checkbox.Checked = (krt_felhasznalokSearch.System.Value == "1") ? true : false;
            textTelefon.Text = krt_felhasznalokSearch.Telefonszam.Value;
            textBeosztas.Text = krt_felhasznalokSearch.Beosztas.Value;

            if (krt_felhasznalokSearch.Tipus.Value == authTypeBasic.Value || krt_felhasznalokSearch.Tipus.Value == authTypeWindows.Value)
            {
                ddownAuthType.SelectedValue = krt_felhasznalokSearch.Tipus.Value;
            }
            else
            {
                ddownAuthType.SelectedValue = emptyItem.Value;
            }

            SetRadioList(krt_felhasznalokSearch.Engedelyezett.Value);

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_felhasznalokSearch.ErvKezd, krt_felhasznalokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private KRT_FelhasznalokSearch SetSearchObjectFromComponents()
    {
        KRT_FelhasznalokSearch krt_felhasznalokSearch = (KRT_FelhasznalokSearch)SearchHeader1.TemplateObject;
        if (krt_felhasznalokSearch == null)
        {
            krt_felhasznalokSearch = new KRT_FelhasznalokSearch();
        }

        if (!String.IsNullOrEmpty(Usernev.Text))
        {
            krt_felhasznalokSearch.UserNev.Value = Usernev.Text;
            krt_felhasznalokSearch.UserNev.Operator = Search.GetOperatorByLikeCharater(Usernev.Text);
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            krt_felhasznalokSearch.Nev.Value = Nev_TextBox.Text;
            krt_felhasznalokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(PartnerTextBox1.Id_HiddenField))
        {
            krt_felhasznalokSearch.Partner_id.Value = PartnerTextBox1.Id_HiddenField;
            krt_felhasznalokSearch.Partner_id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Munkahely_PartnerTextBox.Id_HiddenField))
        {
            krt_felhasznalokSearch.Partner_Id_Munkahely.Value = Munkahely_PartnerTextBox.Id_HiddenField;
            krt_felhasznalokSearch.Partner_Id_Munkahely.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(email_TextBox.Text))
        {
            krt_felhasznalokSearch.EMail.Value = email_TextBox.Text;
            krt_felhasznalokSearch.EMail.Operator = Search.GetOperatorByLikeCharater(email_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(textBeosztas.Text))
        {
            krt_felhasznalokSearch.Beosztas.Value = textBeosztas.Text;
            krt_felhasznalokSearch.Beosztas.Operator = Search.GetOperatorByLikeCharater(textBeosztas.Text);
        }

        if (!String.IsNullOrEmpty(textTelefon.Text))
        {
            krt_felhasznalokSearch.Telefonszam.Value = textTelefon.Text;
            krt_felhasznalokSearch.Telefonszam.Operator = Search.GetOperatorByLikeCharater(textTelefon.Text);
        }

        if (ddownAuthType.SelectedValue == authTypeBasic.Value || ddownAuthType.SelectedValue == authTypeWindows.Value)
        {
            krt_felhasznalokSearch.Tipus.Value = ddownAuthType.SelectedValue;
            krt_felhasznalokSearch.Tipus.Operator = Query.Operators.equals;
        }
        else
        {
            krt_felhasznalokSearch.Tipus.Value = String.Empty;
        }

        if (!String.IsNullOrEmpty(rbListEngedelyzett.SelectedValue))
        {
            krt_felhasznalokSearch.Engedelyezett.Value = rbListEngedelyzett.SelectedValue;
            krt_felhasznalokSearch.Engedelyezett.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_felhasznalokSearch.ErvKezd, krt_felhasznalokSearch.ErvVege);        
              
        return krt_felhasznalokSearch;
    }

  

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_FelhasznalokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_FelhasznalokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_FelhasznalokSearch();
    }

}
