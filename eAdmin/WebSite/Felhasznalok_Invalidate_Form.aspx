﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="Felhasznalok_Invalidate_Form.aspx.cs"
    Inherits="Felhasznalok_Invalidate_Form"
    Title="Felhasználó érvénytelenítés" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager_Felhasznalok_Invalidate_Form" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader_Felhasznalok_Invalidate_Form" runat="server" HeaderTitle="<%$Resources:Form,FelhasznalokFormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <div class="alert">
                        <%--<span></span>--%>
                        <strong>Figyelmeztetés !</strong>
                        <br />
                        Hozzáférési jogosultságát felül kell vizsgálni !
                        <br />
                        A felhasználóhoz rendelt tételek elérhetőek az elszámoltatási jegyzőkönyvben.
                        <br />
                        E-mail értesítés kerül kiküldésre az Alkalmazásgazdának és az érintett felhasználónak.
                        <br />
                        <br />
                    </div>
                    <asp:UpdatePanel ID="Felhasznalok_Invalidate_Form_UpdatePanel" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="CsoportTagokInvalidateGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                PagerSettings-Visible="false" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" Checked='<%# Eval("Checked") %>' AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Felhasznalo_Nev" HeaderText="Felhasználó" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Felhasznalo_Nev" HeaderStyle-Width="200px" />

                                    <asp:BoundField DataField="Msg" HeaderText="Ellenőrzések" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Msg" />

                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>

                <uc2:FormFooter ID="FormFooter_Felhasznalok_Invalidate_Form" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
