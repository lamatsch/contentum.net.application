using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

public partial class Felhasznalok_Invalidate_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Mode = "";
    private PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!FunctionRights.GetFunkcioJog(Page, "FelhasznaloInvalidate"))
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
            return;
        }

        pageView = new PageView(Page, ViewState);
        Command = Request.QueryString.Get(CommandName.Command);
        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        FormHeader_Felhasznalok_Invalidate_Form.HeaderTitle = Resources.Form.FelhasznalokFormHeaderTitle;

        if (Mode == CommandName.Invalidate && Command == CommandName.Invalidate)
        {
            string ids = Request.QueryString.Get(QueryStringVars.Id);
            if (string.IsNullOrEmpty(ids))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel);
                return;
            }

            Result result = LoadFelhasznalok(ids.Split(','));
            StringBuilder warnMsg = new StringBuilder();
            if (result != null && result.GetCount > 0)
            {
                DataTable dt = CreateDataTable();
                string msg;
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    if (Ervenytelenites.VanFelhasznalohozKapcsoltUgy(Page, FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel, row["Id"].ToString(), out msg))
                    {
                        AddToDataTable(ref dt, row["Id"].ToString(), row["Nev"].ToString(), msg);
                    }
                    else
                    {
                        AddToDataTable(ref dt, row["Id"].ToString(), row["Nev"].ToString(), msg ?? string.Empty);
                    }
                    msg = null;
                }
                Result res = new Result();
                res.Ds = new DataSet();
                res.Ds.Tables.Add(dt);
                UI.GridViewFill(CsoportTagokInvalidateGridView, res, FormHeader_Felhasznalok_Invalidate_Form, FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel, Felhasznalok_Invalidate_Form_UpdatePanel);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter_Felhasznalok_Invalidate_Form.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(Footer_InvalidateButtonsClick);
        FormHeader_Felhasznalok_Invalidate_Form.ModeText = Resources.Form.FormHeaderInvalidate;
    }

    private void Footer_InvalidateButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Modify || e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, "FelhasznaloInvalidate"))
            {
                string[] deletableItemsList = UI.GetCheckedCheckBoxIdsInGridView(CsoportTagokInvalidateGridView);

                if (deletableItemsList == null || deletableItemsList.Length < 1)
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel);
                    return;
                }

                bool resultInvalidate = true;
                for (int i = 0; i < deletableItemsList.Length; i++)
                {
                    resultInvalidate &= InvalidateFelhasznalok(deletableItemsList[i]);
                }
                if (resultInvalidate)
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    #region SERVICE
    /// <summary>
    /// InvalidateFelhasznalok
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private bool InvalidateFelhasznalok(string id)
    {
        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        Result result = service.Invalidate(execParam);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel, result);
            Logger.Error("Felhasznalo torles sikertelen", execParam, result);
            return false;
        }
        return true;
    }
    #endregion

    /// <summary>
    /// LoadCsoportTagok
    /// </summary>
    /// <param name="ids"></param>
    private Result LoadFelhasznalok(IEnumerable<string> ids)
    {
        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        KRT_FelhasznalokSearch src = new KRT_FelhasznalokSearch();
        src.Id.In(ids);

        if (string.IsNullOrEmpty(src.Id.Value))
            return new Result();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAll(execParam, src);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader_Felhasznalok_Invalidate_Form.ErrorPanel, result);
            return null;
        }
        return result;
    }
    private DataTable CreateDataTable()
    {
        DataTable dt = new DataTable();

        #region COLUMNS
        DataColumn column1 = new DataColumn();
        column1.DataType = System.Type.GetType("System.Guid");
        column1.ColumnName = "Id";
        dt.Columns.Add(column1);

        DataColumn column3b = new DataColumn();
        column3b.DataType = System.Type.GetType("System.String");
        column3b.ColumnName = "Felhasznalo_Nev";
        dt.Columns.Add(column3b);

        DataColumn column6 = new DataColumn();
        column6.DataType = System.Type.GetType("System.Boolean");
        column6.ColumnName = "Checked";
        dt.Columns.Add(column6);

        DataColumn column5 = new DataColumn();
        column5.DataType = System.Type.GetType("System.String");
        column5.ColumnName = "Msg";
        dt.Columns.Add(column5);
        #endregion
        return dt;
    }
    private void AddToDataTable(ref DataTable dataTable, string id, string felhasznaloNev, string msg)
    {
        DataRow dr = dataTable.NewRow();

        dr["Id"] = id;
        dr["Felhasznalo_Nev"] = felhasznaloNev;
        dr["Checked"] = "true";
        if (!string.IsNullOrEmpty(msg))
            dr["Msg"] = msg;

        dataTable.Rows.Add(dr);
    }
}
