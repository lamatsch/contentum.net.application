<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ForditasokForm.aspx.cs" Inherits="ForditasokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>    
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>    



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,ForditasokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelNyelvKod" runat="server" Text="Nyelv:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="NyelvKod" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelOrgKod" runat="server" Text="Org:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="OrgKod" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelModul" runat="server" Text="Modul:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="Modul" runat="server" CssClass="mrUrlapInput"/>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelKomponens" runat="server" Text="Komponens:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="Komponens" runat="server" CssClass="mrUrlapInput"/>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelObjAzonosito" runat="server" Text="Elem azonosító:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="ObjAzonosito" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelForditas" runat="server" Text="Fordítás:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="Forditas" runat="server" CssClass="mrUrlapInput"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>