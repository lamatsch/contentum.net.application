using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ForditasokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        NyelvKod.ReadOnly = true;
        OrgKod.ReadOnly = true;
        Modul.ReadOnly = true;
        Komponens.ReadOnly = true;
        ObjAzonosito.ReadOnly = true;
        Forditas.ReadOnly = true;
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Forditas" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_ForditasokService service = eAdminService.ServiceFactory.GetKRT_ForditasokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Forditasok krt_Forditasok = (KRT_Forditasok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Forditasok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Forditasok"></param>
    private void LoadComponentsFromBusinessObject(KRT_Forditasok krt_Forditasok)
    {
        NyelvKod.Text = krt_Forditasok.NyelvKod;
        OrgKod.Text = krt_Forditasok.OrgKod;
        Modul.Text = krt_Forditasok.Modul;
        Komponens.Text = krt_Forditasok.Komponens;
        ObjAzonosito.Text = krt_Forditasok.ObjAzonosito;
        Forditas.Text = krt_Forditasok.Forditas;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_Forditasok.Base.Ver;
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_Forditasok GetBusinessObjectFromComponents()
    {
        KRT_Forditasok krt_Forditasok = new KRT_Forditasok();
        krt_Forditasok.Updated.SetValueAll(false);
        krt_Forditasok.Base.Updated.SetValueAll(false);

        krt_Forditasok.NyelvKod = NyelvKod.Text;
        krt_Forditasok.Updated.NyelvKod = pageView.GetUpdatedByView(NyelvKod);
        krt_Forditasok.OrgKod = OrgKod.Text;
        krt_Forditasok.Updated.OrgKod = pageView.GetUpdatedByView(OrgKod);
        krt_Forditasok.Modul = Modul.Text;
        krt_Forditasok.Updated.Modul = pageView.GetUpdatedByView(Modul);
        krt_Forditasok.Komponens = Komponens.Text;
        krt_Forditasok.Updated.Komponens = pageView.GetUpdatedByView(Komponens);
        krt_Forditasok.ObjAzonosito = ObjAzonosito.Text;
        krt_Forditasok.Updated.ObjAzonosito = pageView.GetUpdatedByView(ObjAzonosito);
        krt_Forditasok.Forditas = Forditas.Text;
        krt_Forditasok.Updated.Forditas = pageView.GetUpdatedByView(Forditas);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Forditasok.Base.Ver = FormHeader1.Record_Ver;
        krt_Forditasok.Base.Updated.Ver = true;


        return krt_Forditasok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(NyelvKod);
            compSelector.Add_ComponentOnClick(OrgKod);
            compSelector.Add_ComponentOnClick(Modul);
            compSelector.Add_ComponentOnClick(Komponens);
            compSelector.Add_ComponentOnClick(ObjAzonosito);
            compSelector.Add_ComponentOnClick(Forditas);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Forditas" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_ForditasokService service = eAdminService.ServiceFactory.GetKRT_ForditasokService();
                            KRT_Forditasok krt_Forditasok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Forditasok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ForditasokManager.ClearCache();
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, ObjAzonosito.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_ForditasokService service = eAdminService.ServiceFactory.GetKRT_ForditasokService();
                                KRT_Forditasok krt_Forditasok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Forditasok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ForditasokManager.ClearCache();
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
