<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForditasokList.aspx.cs" Inherits="ForditasokList" Title="Untitled Page" %>
 

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
        
    <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeForditasok" OnClientClick="return false;" />
           </td>
           <td style="text-align: left; vertical-align: top; width: 100%;">
             <asp:UpdatePanel ID="updatePanelForditasok" runat="server" OnLoad="updatePanelForditasok_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeForditasok" runat="server" TargetControlID="panelForditasok"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeForditasok" CollapseControlID="btnCpeForditasok"
                        ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                        ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeForditasok"
                        ExpandedSize="0" ExpandedText="Ford�t�sok list�ja" CollapsedText="Ford�t�sok list�ja">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="panelForditasok" runat="server" Width="100%"> 
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                              <asp:GridView id="gridViewForditasok" runat="server" GridLines="None" BorderWidth="1px" OnRowCommand="gridViewForditasok_RowCommand" 
                                 OnPreRender="gridViewForditasok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                 DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewForditasok_RowDataBound" OnSorting="gridViewForditasok_Sorting" AllowPaging="true">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>                                
                                                <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                 &nbsp;&nbsp;
                                                 <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                             </HeaderTemplate>
                                             <ItemTemplate>
                                                 <asp:CheckBox id="check" runat="server" Text='<%# Eval("ObjAzonosito") %>' CssClass="HideCheckBoxText" />
                                             </ItemTemplate>
                                        </asp:TemplateField>   
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="NyelvKod" HeaderText="Nyelv" SortExpression="KRT_Forditasok.NyelvKod">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="OrgKod" HeaderText="Org" SortExpression="KRT_Forditasok.OrgKod">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Modul" HeaderText="Modul" SortExpression="KRT_Forditasok.Modul">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Komponens" HeaderText="Komponens" SortExpression="KRT_Forditasok.Komponens">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ObjAzonosito" HeaderText="Elem" SortExpression="KRT_Forditasok.ObjAzonosito">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Forditas" HeaderText="Ford�t�s" SortExpression="KRT_Forditasok.Forditas">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                         </asp:TemplateField>
                                     </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                             </td>
                             </tr>
                         </table>
                    </asp:Panel>   
                 </ContentTemplate>
              </asp:UpdatePanel>  
           </td>
        </tr>
     </table>              
</asp:Content>