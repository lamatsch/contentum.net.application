<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ForditasokSearch.aspx.cs" Inherits="ForditasokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,ForditasokSearchHeaderTitle%>" />
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td> 
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%" >
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNyelvKod" runat="server" Text="Nyelv:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="NyelvKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelOrgKod" runat="server" Text="Org:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="OrgKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelModul" runat="server" Text="Modul:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Modul" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKomponens" runat="server" Text="Komponens:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Komponens" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelObjAzonosito" runat="server" Text="Elem azonosító:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="ObjAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelForditas" runat="server" Text="Fordítás:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Forditas" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2">
                                    </td>
                                 </tr>
                                <tr class="urlapSor">
                                     <td colspan="2">
                                         <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1" runat="server">
                                          </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                 </tr>

                             </tbody>
                        </table>
                     </eUI:eFormPanel>
                 <uc2:SearchFooter ID="SearchFooter1" runat="server" />
              </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

