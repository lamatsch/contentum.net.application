using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class ForditasokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_ForditasokSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
   
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);       
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_ForditasokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_ForditasokSearch)Search.GetSearchObject(Page, new KRT_ForditasokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_ForditasokSearch _KRT_ForditasokSearch = (KRT_ForditasokSearch)searchObject;

        if (_KRT_ForditasokSearch != null)
        {
            NyelvKod.Text = _KRT_ForditasokSearch.NyelvKod.Value;
            OrgKod.Text = _KRT_ForditasokSearch.OrgKod.Value;
            Modul.Text = _KRT_ForditasokSearch.Modul.Value;
            Komponens.Text = _KRT_ForditasokSearch.Komponens.Value;
            ObjAzonosito.Text = _KRT_ForditasokSearch.ObjAzonosito.Value;
            Forditas.Text = _KRT_ForditasokSearch.Forditas.Value;
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_ForditasokSearch SetSearchObjectFromComponents()
    {
        KRT_ForditasokSearch _KRT_ForditasokSearch = (KRT_ForditasokSearch)SearchHeader1.TemplateObject;

        if (_KRT_ForditasokSearch == null)
        {
            _KRT_ForditasokSearch = new KRT_ForditasokSearch();
        }

        _KRT_ForditasokSearch.NyelvKod.Value = NyelvKod.Text;
        _KRT_ForditasokSearch.NyelvKod.Operator = Search.GetOperatorByLikeCharater(NyelvKod.Text);
        _KRT_ForditasokSearch.OrgKod.Value = OrgKod.Text;
        _KRT_ForditasokSearch.OrgKod.Operator = Search.GetOperatorByLikeCharater(OrgKod.Text);
        _KRT_ForditasokSearch.Modul.Value = Modul.Text;
        _KRT_ForditasokSearch.Modul.Operator = Search.GetOperatorByLikeCharater(Modul.Text);
        _KRT_ForditasokSearch.Komponens.Value = Komponens.Text;
        _KRT_ForditasokSearch.Komponens.Operator = Search.GetOperatorByLikeCharater(Komponens.Text);
        _KRT_ForditasokSearch.ObjAzonosito.Value = ObjAzonosito.Text;
        _KRT_ForditasokSearch.ObjAzonosito.Operator = Search.GetOperatorByLikeCharater(ObjAzonosito.Text);
        _KRT_ForditasokSearch.Forditas.Value = Forditas.Text;
        _KRT_ForditasokSearch.Forditas.Operator = Search.GetOperatorByLikeCharater(Forditas.Text);


        return _KRT_ForditasokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_ForditasokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }       
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_ForditasokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_ForditasokSearch();
    }
}
