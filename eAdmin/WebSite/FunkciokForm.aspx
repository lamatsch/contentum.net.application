<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FunkciokForm.aspx.cs" Inherits="FunkciokForm" %>

    
    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
    
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,FunkciokFormHeaderTitle%>" />
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Nev_Label" runat="server" Text="Funkci� n�v:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Nev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="K�d:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Kod" runat="server" Enabled="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="height: 19px">
                            </td>
                            <td class="mrUrlapMezo" style="height: 19px">
                                <asp:CheckBox ID="Modosithato_CheckBox" runat="server" Enabled="False" Text="M�dos�that�" Checked="True" /></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Le�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Leiras_TextBox" runat="server" CssClass="mrUrlapInput" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="cbMunkanaplo" runat="server" Text="Munkanapl�ban megjelenjen"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="cbFeladatJelzo" runat="server" Text="Automatikus feladatjelz�s"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="cbKeziFeladatJelzo" runat="server" Text="K�zi feladatjelz�s"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" Enable_ErvKezd="true" Enable_ErvVege="true" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

