<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FunkciokList.aspx.cs" Inherits="FunkciokList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>
 <%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
   
    
<%--Tablazat / Grid--%> 

     <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="FunkciokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">                           

        <asp:UpdatePanel ID="FunkciokUpdatePanel" runat="server" OnLoad="FunkciokUpdatePanel_Load" >
            <ContentTemplate>     
            <ajaxToolkit:CollapsiblePanelExtender ID="FunkciokCPE" runat="server" TargetControlID="Panel1"
                    CollapsedSize="20" Collapsed="False" ExpandControlID="FunkciokCPEButton" CollapseControlID="FunkciokCPEButton"
                    ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                    ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="FunkciokCPEButton"
                    ExpandedSize="0" ExpandedText="Funkci�k list�ja" CollapsedText="Funkci�k list�ja">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="Panel1" runat="server">       
            <table style="width: 98%;" cellpadding="0" cellspacing="0">
            <tr>
            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                
                <asp:GridView ID="FunkciokGridView" runat="server" OnRowCommand="FunkciokGridView_RowCommand"
                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="FunkciokGridView_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="FunkciokGridView_Sorting"
                                    OnRowDataBound="FunkciokGridView_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="Kod">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        
                                        <asp:TemplateField>
                                                    <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label_Modosithato" runat="server" Text="M�dos�that�" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cb_Modosithato" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                        <asp:BoundField DataField="Leiras" HeaderText="Le�r�s" SortExpression="Leiras">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Munkanapl�ban" SortExpression="MunkanaploJelzo">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                  <asp:CheckBox runat="server" Enabled="false" ID="cbMunkanaplo" Checked='<%# Eval("MunkanaploJelzo") as string == "1" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Automatikus feladatjelz�s" SortExpression="FeladatJelzo" Visible="false">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                  <asp:CheckBox runat="server" Enabled="false" ID="cbFeladatJelzo" Checked='<%# Eval("FeladatJelzo") as string == "1" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="K�zi feladatjelz�s" SortExpression="KeziFeladatJelzo" Visible="false">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                  <asp:CheckBox runat="server" Enabled="false" ID="cbKeziFeladatJelzo" Checked='<%# Eval("KeziFeladatJelzo") as string == "1" ? true : false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                    
        </td></tr></table>
        </asp:Panel>
        </ContentTemplate>    
        </asp:UpdatePanel>
               </td>                            
    </tr>
    
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>

    <tr>
   
    <td style="text-align: left; vertical-align: top; width: 0px;">
    <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
    </td>
     <td style="text-align: left; vertical-align: top; width: 100%;"> 
     <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                                
            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel8"
            CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton"
            >
            
            </ajaxToolkit:CollapsiblePanelExtender>
            
                <asp:Panel ID="Panel8" runat="server">

                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" 
                    OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        
                        <ajaxToolkit:TabPanel ID="SzerepkorokTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltSzerepkorok" runat="server">
                                    <ContentTemplate>                                     
                                        <asp:Label ID="Header" runat="server" Text="Hozz�rendelt szerepk�r�k"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                          
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="SzerepkorokUpdatePanel" runat="server" OnLoad="SzerepkorokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="SzerepkorokPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="SzerepkorokSubListHeader" runat="server" />

                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                        <ajaxToolkit:CollapsiblePanelExtender ID="SzerepkorCPE" runat="server" TargetControlID="Panel2"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0"
                                              >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="Panel2" runat="server">

                                        <asp:GridView ID="SzerepkorokGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="SzerepkorokGridView_Sorting" OnPreRender="SzerepkorokGridView_PreRender" OnRowCommand="SzerepkorokGridView_RowCommand" DataKeyNames="Id" >        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />                                                
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>  
                                                <asp:BoundField DataField="Szerepkor_Nev" HeaderText="Szerepk�r neve" SortExpression="Szerepkor_Nev"> 
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje"                                                                 
                                                                SortExpression="LetrehozasIdo">             
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>                                                                
                                                              
                                                </Columns>                
                                            </asp:GridView> 

                                            </asp:Panel>
                                            
                                            </td></tr></table>
                                            
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>                
                               
                </asp:Panel>      
      </td></tr></table>                
                
                
                </td>
            </tr>
        </table>
        <br />    

</asp:Content>


