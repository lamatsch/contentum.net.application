using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class FunkciokList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();
    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";  

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FunkciokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        SzerepkorokSubListHeader.RowCount_Changed += new EventHandler(SzerepkorokSubListHeader_RowCount_Changed);

        SzerepkorokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(SzerepkorokSubListHeader_ErvenyessegFilter_Changed);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.FunkciokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_FunkciokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }



        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FunkciokSearch.aspx", ""
           , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("FunkciokForm.aspx"
            , QueryStringVars.Command + "="+CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID,EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FunkciokGridView.ClientID);
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(FunkciokGridView.ClientID, "check", "cb_Modosithato");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(FunkciokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(FunkciokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(FunkciokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(FunkciokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = FunkciokGridView;

        SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        SzerepkorokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(SzerepkorokGridView.ClientID);
        SzerepkorokSubListHeader.ButtonsClick += new CommandEventHandler(SzerepkorokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        SzerepkorokSubListHeader.AttachedGridView = SzerepkorokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(FunkciokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_FunkciokSearch());

        if (!IsPostBack) FunkciokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.New);
        //ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.Invalidate);
        //ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Funkcio" + CommandName.Lock);

        SzerepkorokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.New);
        SzerepkorokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.View);
        SzerepkorokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.Modify);
        SzerepkorokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(FunkciokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    
    }

    #endregion

    #region Master List

    protected void FunkciokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FunkciokGridView", ViewState, "KRT_Funkciok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FunkciokGridView", ViewState);

        FunkciokGridViewBind(sortExpression, sortDirection);
    }

    protected void FunkciokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_FunkciokSearch search = (KRT_FunkciokSearch)Search.GetSearchObject(Page, new KRT_FunkciokSearch());
        search.OrderBy = Search.GetOrderBy("FunkciokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(FunkciokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void FunkciokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cb_Modosithato", "Modosithato");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void FunkciokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, FunkciokCPE);
        ListHeader1.RefreshPagerLabel();

        ui.SetClientScriptToGridViewSelectDeSelectButton(FunkciokGridView);
    }
    

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        FunkciokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, FunkciokCPE);
        FunkciokGridViewBind();
    }
    
    protected void FunkciokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {        
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FunkciokGridView, selectedRowNumber, "check");


            string id = (sender as GridView).DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString();

            //FunkciokGridView_SelectRowCommand(id);
            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            // m�dos�that�s�g ellen�rz�se:
            bool modosithato = false;
            CheckBox ch_modosithato = UI.GetGridViewCheckBox(FunkciokGridView, "cb_Modosithato", id);
            if (ch_modosithato != null && ch_modosithato.Checked == true)
            {
                modosithato = true;
            }

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FunkciokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID);
            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("FunkciokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            string tableName = "Krt_Funkciok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, FunkciokUpdatePanel.ClientID);

            SzerepkorokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.FunkcioId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        
        }
    }



    protected void FunkciokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FunkciokGridViewBind();
                    ActiveTabRefresh(TabContainer1,UI.GetGridViewSelectedRecordId(FunkciokGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedFunkciok();
            FunkciokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedFunkcioRecords();
                FunkciokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedFunkcioRecords();
                FunkciokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedFunkciok();
                break;
        }
    }

    private void LockSelectedFunkcioRecords()
    {
        LockManager.LockSelectedGridViewRecords(FunkciokGridView, "KRT_Funkciok"
            , "FunkcioLock", "FunkcioForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedFunkcioRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(FunkciokGridView, "KRT_Funkciok"
            , "FunkcioLock", "FunkcioForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a FunkciokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedFunkciok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FunkcioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FunkciokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                // t�nyleges m�dos�that�s�g-ellen�rz�s majd a webservice oldalon
                bool modosithato = false;
                CheckBox cb_modosithato = UI.GetGridViewCheckBox(FunkciokGridView, "cb_Modosithato", deletableItemsList[i]);
                if (cb_modosithato.Checked == true) modosithato = true;

                if (modosithato)
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a FunkciokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedFunkciok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(FunkciokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Funkciok");
        }
    }

    protected void FunkciokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FunkciokGridViewBind(e.SortExpression, UI.GetSortToGridView("FunkciokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(FunkciokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (SzerepkorokGridView.SelectedIndex == -1)
        {
            return;
        }
        string funkcioId = UI.GetGridViewSelectedRecordId(FunkciokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, funkcioId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string funkcioId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                SzerepkorokGridViewBind(funkcioId);
                SzerepkorokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(SzerepkorokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        SzerepkorokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
        {
            SzerepkorokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }

    }

    #endregion

    #region Szerepkorok Detail

    private void SzerepkorokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Szerepkor_Funkciok();
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView));
        }
    }

    protected void SzerepkorokGridViewBind(string funkcioId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SzerepkorokGridView", ViewState, "Szerepkor_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SzerepkorokGridView", ViewState);

        SzerepkorokGridViewBind(funkcioId, sortExpression, sortDirection);
    }

    protected void SzerepkorokGridViewBind(string funkcioId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(funkcioId))
        {
            KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Funkciok funkcio = new KRT_Funkciok();
            funkcio.Id = funkcioId;
            KRT_Szerepkor_FunkcioSearch search = new KRT_Szerepkor_FunkcioSearch();
            search.OrderBy = Search.GetOrderBy("SzerepkorokGridView", ViewState, SortExpression, SortDirection);

            SzerepkorokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByFunkcio(ExecParam, funkcio, search);

            UI.GridViewFill(SzerepkorokGridView, res, SzerepkorokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

            ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);
        }
        else
        {
            ui.GridViewClear(SzerepkorokGridView);
        }
    }
    
    void SzerepkorokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView));
    }

    protected void SzerepkorokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(SzerepkorokTabPanel))
                    //{
                    //    SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView));
                    //}
                    ActiveTabRefreshDetailList(SzerepkorokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a SzerepkorokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_Szerepkor_Funkciok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Szerepkor_FunkcioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void SzerepkorokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SzerepkorokGridView, selectedRowNumber, "check");
        }
    }

    private void SzerepkorokGridView_RefreshOnClientClicks(string szerepkor_funkcio_Id)
    {
        string id = szerepkor_funkcio_Id;
        if (!String.IsNullOrEmpty(id))
        {
            SzerepkorokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            SzerepkorokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }


    protected void SzerepkorokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = SzerepkorokGridView.PageIndex;

        SzerepkorokGridView.PageIndex = SzerepkorokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != SzerepkorokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, SzerepkorCPE);
            SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(SzerepkorokSubListHeader.Scrollable, SzerepkorCPE);
        }
        SzerepkorokSubListHeader.PageCount = SzerepkorokGridView.PageCount;
        SzerepkorokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(SzerepkorokGridView);
    }

    void SzerepkorokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(SzerepkorokSubListHeader.RowCount);
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView));
    }


    protected void SzerepkorokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SzerepkorokGridViewBind(UI.GetGridViewSelectedRecordId(FunkciokGridView)
            , e.SortExpression, UI.GetSortToGridView("SzerepkorokGridView", ViewState, e.SortExpression));
    }

    #endregion



}
