using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;

public partial class FunkciokLovList : Contentum.eUtility.UI.PageBase
{
    
    private bool disable_refreshLovList = false;

    private bool FeladatMode = false;
    private bool FeladatDefinicioMode = false;
    private string ObjektumFilter = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FunkciokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        string qsFeldataMode = Request.QueryString.Get("FeladatMode");
        if (!String.IsNullOrEmpty(qsFeldataMode) && qsFeldataMode == "1")
        {
            FeladatMode = true;
        }
        string qsFeladatDefinicioMode = Request.QueryString.Get("FeladatDefinicioMode");
        if (!String.IsNullOrEmpty(qsFeladatDefinicioMode) && qsFeladatDefinicioMode == "1")
        {
            FeladatDefinicioMode = true;
            // nem lehetnek egyidej�leg bekapcsolva
            FeladatMode = false;
        }
        ObjektumFilter = Request.QueryString.Get(QueryStringVars.ObjektumTipusKod);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("FunkciokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick =
            JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            + JavaScripts.SetOnClientClick("FunkciokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'"
            , Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

        Form.DefaultButton = ButtonSearch.UniqueID;
        Form.DefaultFocus = ButtonSearch.UniqueID;
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text,false);
    }
    
    private void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
        KRT_FunkciokSearch krt_FunkciokSearch = null;

        if (searchObjectFromSession == true)
        {
            krt_FunkciokSearch = (KRT_FunkciokSearch) Search.GetSearchObject(Page, new KRT_FunkciokSearch());
            krt_FunkciokSearch.OrderBy = "Nev";
        }
        else
        {
            krt_FunkciokSearch = new KRT_FunkciokSearch();
            krt_FunkciokSearch.Nev.Value = SearchKey;
            krt_FunkciokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            krt_FunkciokSearch.OrderBy = "Nev";
        }

        if (FeladatMode)
        {
            krt_FunkciokSearch.KeziFeladatJelzo.Value = Constants.Database.Yes;
            krt_FunkciokSearch.KeziFeladatJelzo.Operator = Query.Operators.equals;
        }
        else if (FeladatDefinicioMode)
        {
            krt_FunkciokSearch.FeladatJelzo.Value = Constants.Database.Yes;
            krt_FunkciokSearch.FeladatJelzo.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ObjektumFilter))
        {
            if (krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch == null)
            {
                krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch = new KRT_ObjTipusokSearch();
            }
            krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch.ErvKezd.Clear();
            krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch.ErvVege.Clear();
            krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch.Kod.Value = ObjektumFilter;
            krt_FunkciokSearch.Extended_KRT_ObjTipusokSearch.Kod.Operator = Query.Operators.isnullorequals;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page,new ExecParam());

        krt_FunkciokSearch.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, krt_FunkciokSearch);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel,LovListHeader1.ErrorUpdatePanel);

        FillFunkciokDetailsDictionary(result);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, krt_FunkciokSearch.TopRow);
    }

    private Dictionary<string, string> FunkciokDetails
    {
        get
        {
            if(ViewState["FunkciokDetails"] == null)
                return null;

            return (Dictionary<string, string>)ViewState["FunkciokDetails"];
        }
        set
        {
            ViewState["FunkciokDetails"] = value;
        }
    }

    private void FillFunkciokDetailsDictionary(Result result)
    {
        FunkciokDetails = null;
        if (!result.IsError)
        {
            DataTable tableFunkciok = result.Ds.Tables[0];
            if (tableFunkciok.Rows.Count > 0)
            {
                Dictionary<string, string> _dictionaryFunkciok = new Dictionary<string, string>(tableFunkciok.Rows.Count);
                foreach (DataRow row in tableFunkciok.Rows)
                {
                    string id = row["Id"].ToString();
                    string ObjTipus_Id_AdatElem_Kod = row["ObjTipus_Id_AdatElem_Kod"].ToString();
                    if (!_dictionaryFunkciok.ContainsKey(id))
                    {
                        _dictionaryFunkciok.Add(id, ObjTipus_Id_AdatElem_Kod);
                    }
                }

                FunkciokDetails = _dictionaryFunkciok;
            }
        }
    }
        
    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                string tipusHiddenFieldId = Request.QueryString.Get(QueryStringVars.TipusHiddenFieldId);
                if (!String.IsNullOrEmpty(tipusHiddenFieldId))
                {
                    if (FunkciokDetails != null)
                    {
                        if (FunkciokDetails.ContainsKey(selectedId))
                        {
                            string objTipusAdatelem = FunkciokDetails[selectedId];
                            Dictionary<string, string> returnValues = new Dictionary<string,string>(1);
                            returnValues.Add(tipusHiddenFieldId, objTipusAdatelem);
                            JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnValues, "RegisterTipusReturnValue");
                        }
                    }
                }
                string callbackFunction = Request.QueryString.Get(QueryStringVars.ParentWindowCallbackFunction);
                if (!String.IsNullOrEmpty(callbackFunction))
                {
                    JavaScripts.RegisterParentWindowCallbackFunction(Page, callbackFunction);
                }
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    ,Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }

}
