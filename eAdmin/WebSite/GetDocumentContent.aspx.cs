﻿using System;
using System.Data;
using System.Web;
using System.Net;
using System.IO;
using Contentum.eUtility;
using Contentum.eDocument.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using Contentum.eIntegrator.Service;

public partial class GetDocumentContent : System.Web.UI.Page
{
    string externalLink = string.Empty;
    string filename = string.Empty;
    string guid = string.Empty;
    ContentDownloader contentDownloader = new ContentDownloader();
    protected void Page_PreRender(object sender, EventArgs e)
    {
        string currentVersion = String.Empty;
        string version = Request.QueryString.Get("version");
        string mode = Request.QueryString.Get("mode") ?? String.Empty;
        string filesource = string.Empty;

        switch (mode)
        {
            case "renderpart":
                RenderDocumentPart();
                break;
            case "display":
                DisplayDocument();
                return;
            case "zip":
                CompressFiles();
                return;
        }
        try
        {
            #region GUID alapján externalLink lekérése

            KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam ep = UI.SetExecParamDefault(Page);

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");
                char jogszint = 'O';

                Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                guid = ep.Record_Id;
                currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                filesource = (dokGetResult.Record as KRT_Dokumentumok).External_Source;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString.Get("barcode")))
            {
                string barCode = Request.QueryString.Get("barcode");

                KRT_DokumentumokSearch search = new KRT_DokumentumokSearch();
                //search.BarCode.Value = Request.QueryString.Get("barcode");
                //search.BarCode.Operator = Query.Operators.equals;

                search.Manual_BarCodeFilter.Value = barCode;
                search.Manual_BarCodeFilter.Operator = String.Empty; // biztosan legyen üres, hogy ne generálódjon bele a where feltételbe

                Result dokGetResult = dokService.GetAllWithExtensionAndJogosultak(ep, search, true);

                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                if (dokGetResult.Ds.Tables.Count < 1 || dokGetResult.Ds.Tables[0].Rows.Count < 1)
                {
                    //throw new ResultException(52794); // Önnek nincs joga, hogy megtekintse a dokumentum tartalmát!
                    throw new ResultException(52796); // A dokumentum keresése eredménytelen: Önnek nincs joga, hogy megtekintse a dokumentum tartalmát, vagy a dokumentum nem azonosítható!
                }
                else if (dokGetResult.Ds.Tables[0].Rows.Count > 1)
                {
                    throw new ResultException(52797); // A dokumentum keresése több dokumentumot is eredményezett: A dokumentum azonosítása sikertelen!
                }
                // Mivel a dokumentum egyelőre nincs mindig vonalkódja, és emiatt az iratkezelési elem felől is vizsgálunk,
                // ez az ellenőrzés jelenleg nem használható:
                //else if (dokGetResult.Ds.Tables[0].Rows[0]["BarCode"].ToString() != barCode)
                //{
                //    throw new ResultException(52798); // A dokumentum keresése vonalkód alapján eredménytelen: A talált dokumentum vonalkódja nem egyezik a keresési feltételben megadottal!	
                //}

                externalLink = dokGetResult.Ds.Tables[0].Rows[0]["External_Link"].ToString();
                filename = dokGetResult.Ds.Tables[0].Rows[0]["FajlNev"].ToString();
                guid = dokGetResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                currentVersion = dokGetResult.Ds.Tables[0].Rows[0]["VerzioJel"].ToString();
                filesource = dokGetResult.Ds.Tables[0].Rows[0]["External_Source"].ToString();
            }

            #endregion GUID alapján externalLink lekérése

            if (string.IsNullOrEmpty(Request.QueryString.Get("redirected")))
            {
                if (mode != "decrypt")
                {
                    Response.Redirect(Request.Url.AbsolutePath + "/" + EncodeFileName(filename) + "?id=" + guid +
                    (String.IsNullOrEmpty(version) ? String.Empty : ("&version=" + version)) +
                    "&redirected=true", false);
                }
                else
                {
                    Response.Redirect(Request.Url.AbsolutePath + "/" + EncodeFileName(filename) + "?id=" + guid +
                    (String.IsNullOrEmpty(version) ? String.Empty : ("&version=" + version)) +
                    "&redirected=true" + "&mode=decrypt", false);
                }
            }
            else
            {
                //verziók kezelése
                //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                if (!String.IsNullOrEmpty(version) && version != currentVersion)
                {
                    if (!filesource.Contains("UCM") && !filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                    {
                        ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                        xpmVersion.Record_Id = guid;
                        Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                        Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                        if (resVersion.IsError)
                        {
                            Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                            throw new ResultException(resVersion);
                        }

                        bool isVersion = false;
                        foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                        {
                            if (version == rowVersion["version"].ToString())
                            {
                                externalLink = rowVersion["url"].ToString();
                                isVersion = true;
                                break;
                            }
                        }

                        if (!isVersion)
                        {
                            Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                            throw new ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                        }
                    }
                }

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                if (filesource.Contains("SharePoint"))
                    GetFileBySharePoint(externalLink, filename, mode);
                else if (filesource.Contains("FileShare"))
                    GetFileByFileShare(externalLink, filename, mode);
                else if (filesource.Contains("UCM"))
                    GetFileByUCM(externalLink, filename, mode);
                else if (filesource.Contains("FileSystem"))
                    GetFileBySharePoint(externalLink, filename, mode);
                else if (filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                    contentDownloader.GetFileByHAIR(Page,externalLink, filename, string.Empty);
                else
                    throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", filesource));
            }
        }
        catch (ResultException resEx)
        {
            try
            {
                Response.ClearHeaders();
                Response.Write(ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
            }
            catch { }
        }
        catch (Exception exp)
        {
            try
            {
                Response.ClearHeaders();
                Response.Write(Resources.Error.ErrorCode_52795); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
                Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
            }
            catch { }
        }
        finally
        {
            Response.End();
        }
    }

    private void CompressFiles()
    {
        string externalLink = string.Empty;
        string filename = string.Empty;
        string guid = string.Empty;
        string currentVersion = String.Empty;
        string version = Request.QueryString.Get("version");
        string mode = Request.QueryString.Get("mode") ?? String.Empty;
        string filesource = string.Empty;
        string nev = String.Empty;

        string ids = Request.QueryString.Get("id");
        nev = Request.QueryString.Get("nev");

        if (String.IsNullOrEmpty(ids))
        { throw new ResultException("id cannot be empty"); }

        try
        {
            var idArr = ids.Split('_');
            DateTime current = DateTime.Now;

            if (string.IsNullOrEmpty(nev))
            {
                nev = "Request";
            }
            else
            {
                Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
                nev = illegalInFileName.Replace(nev, "_");
            }

            string zipfName = nev + current.Date.Day.ToString() + current.Date.Month.ToString() + current.Date.Year.ToString() + current.TimeOfDay.Duration().Hours.ToString() + current.TimeOfDay.Duration().Minutes.ToString() + current.TimeOfDay.Duration().Seconds.ToString() + ".zip";

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            //----------IDIDIDIDDIDIDD

            Response.ContentType = "application / zip";
            //Response.ContentType = "application/x-gzip-compressed";
            Response.CacheControl = "public";
            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            //Response.Charset = "utf-8";
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + zipfName + "\";");

            byte[] buffer = new byte[4096];

            ZipOutputStream zipOutputStream = new ZipOutputStream(Response.OutputStream);
            zipOutputStream.SetLevel(9);

            foreach (string id in idArr)
            {
                if (id.Trim() != string.Empty)
                {
                    KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                    ExecParam ep = UI.SetExecParamDefault(Page);

                    ep.Record_Id = id;
                    char jogszint = 'O';

                    Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                    if (dokGetResult.IsError)
                    {
                        throw new ResultException(dokGetResult);
                    }

                    externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                    filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                    guid = ep.Record_Id;
                    currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                    filesource = (dokGetResult.Record as KRT_Dokumentumok).External_Source;

                    //verziók kezelése
                    //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                    if (!String.IsNullOrEmpty(version) && version != currentVersion)
                    {
                        if (!filesource.Contains("UCM"))
                        {
                            ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                            xpmVersion.Record_Id = guid;
                            Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                            Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                            if (resVersion.IsError)
                            {
                                Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                                throw new ResultException(resVersion);
                            }

                            bool isVersion = false;
                            foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                            {
                                if (version == rowVersion["version"].ToString())
                                {
                                    externalLink = rowVersion["url"].ToString();
                                    isVersion = true;
                                    break;
                                }
                            }

                            if (!isVersion)
                            {
                                Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                                throw new ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                            }
                        }
                    }

                    System.IO.Stream fileToAdd = null;

                    if (filesource.Contains("SharePoint"))
                        fileToAdd = GetFileBySharePointForZip(externalLink, filename);
                    else if (filesource.Contains("FileShare"))
                        fileToAdd = GetFileByFileShareForZip(externalLink, filename);
                    else if (filesource.Contains("UCM"))
                        fileToAdd = GetFileByUCMForZip(externalLink, filename);
                    else if (filesource.Contains("FileSystem"))
                        fileToAdd = GetFileBySharePointForZip(externalLink, filename);
                    else if (filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                        fileToAdd = contentDownloader.GetFileByHAIRForZip(Page,externalLink, filename);
                    else
                        throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", filesource));

                    if (fileToAdd == null)
                        continue;

                    ZipEntry entry = new ZipEntry(ZipEntry.CleanName(filename));
                    entry.DateTime = DateTime.Now;
                    //entry.Size = fileToAdd.Length;

                    zipOutputStream.PutNextEntry(entry);

                    int count = fileToAdd.Read(buffer, 0, buffer.Length);
                    while (count > 0)
                    {
                        zipOutputStream.Write(buffer, 0, count);
                        count = fileToAdd.Read(buffer, 0, buffer.Length);
                        if (!Response.IsClientConnected)
                        {
                            break;
                        }
                        Response.Flush();
                    }

                    zipOutputStream.CloseEntry();

                    fileToAdd.Close();
                }
            }

            zipOutputStream.IsStreamOwner = false;
            zipOutputStream.Close();

            //Response.Flush();
            //Response.End();

            //HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        catch (ResultException resEx)
        {
            Response.ClearHeaders();
            Response.Write(ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
        }
        catch (Exception exp)
        {
            Response.ClearHeaders();
            Response.Write(Resources.Error.ErrorCode_52795); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
            Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
        }
        finally
        {
            Response.Flush();
            //Response.Close();
            Response.End();
        }
    }


    private void Decrypt(Stream s)
    {
        byte[] buffer = ReadAll(s);
        byte[] decryptedData;

        string error = "";
        Stream decryptedStream = CsatolmanyokUtility.ProcessFile(filename, buffer, out error);

        if (!string.IsNullOrEmpty(error))
        {
            Response.ClearHeaders();
            Response.Write("Kititkosítás hiba." + " Hibakód: 528001");
            Logger.Warn("GetDocumentumContent decrypt hiba. Filename: " + filename + ". Hiba: " + error);
        }
        else
        {
            try
            {
                decryptedData = ReadAll(decryptedStream);
                Response.ClearContent();
                Response.BinaryWrite(decryptedData);
            }
            catch (Exception ex)
            {
                Response.ClearHeaders();
                Response.Write("Kititkosítás hiba." + " Hibakód: 528001");
                Logger.Warn("GetDocumentumContent decrypt hiba. Filename: " + filename + ". Hiba: " + ex.Message);
            }
            finally
            {
                if (decryptedStream != null)
                {
                    decryptedStream.Close();
                }
            }
        }
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileBySharePoint(string externalLink, string filename, string mode)
    {
        Logger.Debug("GetFileBySharePoint kezdete");
        Logger.Debug(String.Format("externalLink={0}, filename={1}", externalLink, filename));

        #region Header bejegyzések beállítása

        WebRequest request = WebRequest.Create(externalLink);
        request.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        string responseFileName = filename;
        if (mode == "decrypt")
        {
            if (filename.EndsWith(".enc"))
            {
                responseFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }
        }
        // BUG_12301
        // int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(Page, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        ExecParam execParam = new ExecParam();
        if (!string.IsNullOrEmpty(Request.QueryString.Get("fid")))
        {
            execParam.Felhasznalo_Id = Request.QueryString.Get("fid");
        }
        if (String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            execParam = UI.SetExecParamDefault(Page, new ExecParam());
        }
        int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        string letoltesMod = csatolmanyLetoltesConfig == 1 ? "attachment" : "inline";

        // Content-Type és fájl nevének beállítása
        Response.ContentType = response.ContentType;
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", string.Format("{0};filename=\"", letoltesMod) + responseFileName + "\";");

        #endregion Header bejegyzések beállítása

        #region Fájltartalom letöltése és továbbítása a kliensnek

        Stream s = response.GetResponseStream();

        if (mode == "decrypt")
        {
            Decrypt(s);
        }
        else
        {

            byte[] buf = new byte[20480];

            while (true)
            {
                int readBytes = s.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                Response.OutputStream.Write(buf, 0, readBytes);
            }
        }

        s.Close();
        #endregion Fájltartalom letöltése és továbbítása a kliensnek

        Logger.Debug("GetFileBySharePoint vege");

    }


    private System.IO.Stream GetFileBySharePointForZip(string externalLink, string filename)
    {
        WebRequest request = WebRequest.Create(externalLink);

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return response.GetResponseStream();
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek FileShare esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByFileShare(string externalLink, string filename, string mode)
    {
        #region Header bejegyzések beállítása        

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        string responseFileName = filename;
        if (mode == "decrypt")
        {
            if (filename.EndsWith(".enc"))
            {
                responseFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }
        }

        // Content-Type és fájl nevének beállítása
        Response.ContentType = "application/octet-stream";
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + responseFileName + "\";");

        #endregion Header bejegyzések beállítása
        using (var file = File.OpenRead(externalLink))
        {
            if (mode == "decrypt")
            {
                Decrypt(file);
            }
            else
            {
                byte[] buf = new byte[20480];

                while (true)
                {
                    int readBytes = file.Read(buf, 0, buf.Length);

                    if (readBytes == 0)
                        break;

                    Response.OutputStream.Write(buf, 0, readBytes);
                }
            }
        }
    }

    private byte[] ReadAll(Stream stream)
    {
        byte[] buf = new byte[20480];

        MemoryStream ms = new MemoryStream();

        while (true)
        {
            int readBytes = stream.Read(buf, 0, buf.Length);

            if (readBytes == 0)
                break;

            ms.Write(buf, 0, readBytes);
        }

        ms.Position = 0;

        byte[] newBuf = new byte[ms.Length];

        ms.Read(newBuf, 0, (int)ms.Length);
        return newBuf;
    }

    private System.IO.Stream GetFileByFileShareForZip(string externalLink, string filename)
    {
        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return File.OpenRead(externalLink);
    }


    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByUCM(string externalLink, string filename, string mode)
    {
        if (!String.IsNullOrEmpty(externalLink))
        {
            GetFileBySharePoint(externalLink, filename, mode);
        }
    }

    private System.IO.Stream GetFileByUCMForZip(string externalLink, string filename)
    {
        if (!String.IsNullOrEmpty(externalLink))
        {
            return GetFileBySharePointForZip(externalLink, filename);
        }

        return null;
    }

    private void DisplayDocument()
    {
        //if (CheckJogosultsag())
        //{
        //    try
        //    {
        //        string externalLink = Request.QueryString.Get("externalLink");
        //        string fileName = Request.QueryString.Get("fileName");
        //        string version = Request.QueryString.Get("version");
        //        string fileext = Path.GetExtension(fileName);
        //        string mime = GetMimeFromFileExtension(System.IO.Path.GetExtension(fileName));
        //        string onerrror = "window.parent.document.getElementsByClassName('basic_overlay')[0].style = 'display: none'".Replace("'"[0], '"');

        //        if (fileext.Equals(".doc") || fileext.Equals(".docx"))
        //        {

        //            WordVisualizer.Core.Handlers.Renderers.WordDocumentDisplayRenderer renderer = new WordVisualizer.Core.Handlers.Renderers.WordDocumentDisplayRenderer();
        //            renderer.FileName = fileName;
        //            renderer.LoadImageUrl = "~/images/hu/egyeb/activity_indicator.gif";
        //            renderer.ExternalLink = externalLink;
        //            renderer.Version = version;
        //            renderer.FileStream = DokumentumCache.GetDokumentum(externalLink, version);
        //            Response.Clear();
        //            renderer.Render(Context);
        //        }
        //        else if (mime.Contains("image"))
        //        {
        //            string resp = string.Format("<img width = '100%'  height = '100%' onerror='{1}' src = '{0}' />", externalLink, onerrror);
        //            Response.Write(resp);
        //        }
        //        else
        //        {
        //            string resp = string.Format("<object width='100%' onerror='{2}'height='100%' type='{0}' data='{1}' ><embed width='100%' height='100%' type='{0}' src={1} onerror = '{2}'/></object>", mime, externalLink, onerrror);
        //            Response.Write(resp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Clear();
        //        Exception exception = new Exception(ResultError.GetErrorMessageFromResultObject(ResultException.GetResultFromException(ex)));
        //        WordVisualizer.Core.Handlers.Renderers.ExceptionRenderer renderer = new WordVisualizer.Core.Handlers.Renderers.ExceptionRenderer();
        //        renderer.Exception = exception;
        //        renderer.Render(Context);
        //    }
        //    Response.End();
        //}
    }
    /// <summary>
    /// Visszaadja a file mime típúsát a kiterjesztéséből.
    /// </summary>
    /// <param name="fileext">kiterjesztés</param>
    /// <returns>mime típus ha van találat, egyébként application/octet-stream</returns>
    public static string GetMimeFromFileExtension(string fileext) // test done
    {
        string mime;
        if (fileext.Substring(0, 1) != ".") fileext = "." + fileext;
        Dictionary<string, string> mappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) {

                #region ####
                {".323", "text/h323"},
                {".3g2", "video/3gpp2"},
                {".3gp", "video/3gpp"},
                {".3gp2", "video/3gpp2"},
                {".3gpp", "video/3gpp"},
                {".7z", "application/x-7z-compressed"},
                {".aa", "audio/audible"},
                {".AAC", "audio/aac"},
                {".aaf", "application/octet-stream"},
                {".aax", "audio/vnd.audible.aax"},
                {".ac3", "audio/ac3"},
                {".aca", "application/octet-stream"},
                {".accda", "application/msaccess.addin"},
                {".accdb", "application/msaccess"},
                {".accdc", "application/msaccess.cab"},
                {".accde", "application/msaccess"},
                {".accdr", "application/msaccess.runtime"},
                {".accdt", "application/msaccess"},
                {".accdw", "application/msaccess.webapplication"},
                {".accft", "application/msaccess.ftemplate"},
                {".acx", "application/internet-property-stream"},
                {".AddIn", "text/xml"},
                {".ade", "application/msaccess"},
                {".adobebridge", "application/x-bridge-url"},
                {".adp", "application/msaccess"},
                {".ADT", "audio/vnd.dlna.adts"},
                {".ADTS", "audio/aac"},
                {".afm", "application/octet-stream"},
                {".ai", "application/postscript"},
                {".aif", "audio/aiff"},
                {".aifc", "audio/aiff"},
                {".aiff", "audio/aiff"},
                {".air", "application/vnd.adobe.air-application-installer-package+zip"},
                {".amc", "application/mpeg"},
                {".anx", "application/annodex"},
                {".apk", "application/vnd.android.package-archive" },
                {".application", "application/x-ms-application"},
                {".art", "image/x-jg"},
                {".asa", "application/xml"},
                {".asax", "application/xml"},
                {".ascx", "application/xml"},
                {".asd", "application/octet-stream"},
                {".asf", "video/x-ms-asf"},
                {".ashx", "application/xml"},
                {".asi", "application/octet-stream"},
                {".asm", "text/plain"},
                {".asmx", "application/xml"},
                {".aspx", "application/xml"},
                {".asr", "video/x-ms-asf"},
                {".asx", "video/x-ms-asf"},
                {".atom", "application/atom+xml"},
                {".au", "audio/basic"},
                {".avi", "video/x-msvideo"},
                {".axa", "audio/annodex"},
                {".axs", "application/olescript"},
                {".axv", "video/annodex"},
                {".bas", "text/plain"},
                {".bcpio", "application/x-bcpio"},
                {".bin", "application/octet-stream"},
                {".bmp", "image/bmp"},
                {".c", "text/plain"},
                {".cab", "application/octet-stream"},
                {".caf", "audio/x-caf"},
                {".calx", "application/vnd.ms-office.calx"},
                {".cat", "application/vnd.ms-pki.seccat"},
                {".cc", "text/plain"},
                {".cd", "text/plain"},
                {".cdda", "audio/aiff"},
                {".cdf", "application/x-cdf"},
                {".cer", "application/x-x509-ca-cert"},
                {".cfg", "text/plain"},
                {".chm", "application/octet-stream"},
                {".class", "application/x-java-applet"},
                {".clp", "application/x-msclip"},
                {".cmd", "text/plain"},
                {".cmx", "image/x-cmx"},
                {".cnf", "text/plain"},
                {".cod", "image/cis-cod"},
                {".config", "application/xml"},
                {".contact", "text/x-ms-contact"},
                {".coverage", "application/xml"},
                {".cpio", "application/x-cpio"},
                {".cpp", "text/plain"},
                {".crd", "application/x-mscardfile"},
                {".crl", "application/pkix-crl"},
                {".crt", "application/x-x509-ca-cert"},
                {".cs", "text/plain"},
                {".csdproj", "text/plain"},
                {".csh", "application/x-csh"},
                {".csproj", "text/plain"},
                {".css", "text/css"},
                {".csv", "text/csv"},
                {".cur", "application/octet-stream"},
                {".cxx", "text/plain"},
                {".dat", "application/octet-stream"},
                {".datasource", "application/xml"},
                {".dbproj", "text/plain"},
                {".dcr", "application/x-director"},
                {".def", "text/plain"},
                {".deploy", "application/octet-stream"},
                {".der", "application/x-x509-ca-cert"},
                {".dgml", "application/xml"},
                {".dib", "image/bmp"},
                {".dif", "video/x-dv"},
                {".dir", "application/x-director"},
                {".disco", "text/xml"},
                {".divx", "video/divx"},
                {".dll", "application/x-msdownload"},
                {".dll.config", "text/xml"},
                {".dlm", "text/dlm"},
                {".doc", "application/msword"},
                {".docm", "application/vnd.ms-word.document.macroEnabled.12"},
                {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {".dot", "application/msword"},
                {".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
                {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
                {".dsp", "application/octet-stream"},
                {".dsw", "text/plain"},
                {".dtd", "text/xml"},
                {".dtsConfig", "text/xml"},
                {".dv", "video/x-dv"},
                {".dvi", "application/x-dvi"},
                {".dwf", "drawing/x-dwf"},
                {".dxf", "application/x-dxf" },
                {".dwp", "application/octet-stream"},
                {".dwg", "application/acad" },
                {".dxr", "application/x-director"},
                {".eml", "message/rfc822"},
                {".emz", "application/octet-stream"},
                {".eot", "application/vnd.ms-fontobject"},
                {".eps", "application/postscript"},
                {".etl", "application/etl"},
                {".etx", "text/x-setext"},
                {".evy", "application/envoy"},
                {".exe", "application/octet-stream"},
                {".exe.config", "text/xml"},
                {".fdf", "application/vnd.fdf"},
                {".fif", "application/fractals"},
                {".filters", "application/xml"},
                {".fla", "application/octet-stream"},
                {".flac", "audio/flac"},
                {".flr", "x-world/x-vrml"},
                {".flv", "video/x-flv"},
                {".fsscript", "application/fsharp-script"},
                {".fsx", "application/fsharp-script"},
                {".generictest", "application/xml"},
                {".gif", "image/gif"},
                {".gpx", "application/gpx+xml"},
                {".group", "text/x-ms-group"},
                {".gsm", "audio/x-gsm"},
                {".gtar", "application/x-gtar"},
                {".gz", "application/x-gzip"},
                {".h", "text/plain"},
                {".hdf", "application/x-hdf"},
                {".hdml", "text/x-hdml"},
                {".hhc", "application/x-oleobject"},
                {".hhk", "application/octet-stream"},
                {".hhp", "application/octet-stream"},
                {".hlp", "application/winhlp"},
                {".hpp", "text/plain"},
                {".hqx", "application/mac-binhex40"},
                {".hta", "application/hta"},
                {".htc", "text/x-component"},
                {".htm", "text/html"},
                {".html", "text/html"},
                {".htt", "text/webviewhtml"},
                {".hxa", "application/xml"},
                {".hxc", "application/xml"},
                {".hxd", "application/octet-stream"},
                {".hxe", "application/xml"},
                {".hxf", "application/xml"},
                {".hxh", "application/octet-stream"},
                {".hxi", "application/octet-stream"},
                {".hxk", "application/xml"},
                {".hxq", "application/octet-stream"},
                {".hxr", "application/octet-stream"},
                {".hxs", "application/octet-stream"},
                {".hxt", "text/html"},
                {".hxv", "application/xml"},
                {".hxw", "application/octet-stream"},
                {".hxx", "text/plain"},
                {".i", "text/plain"},
                {".ico", "image/x-icon"},
                {".ics", "application/octet-stream"},
                {".idl", "text/plain"},
                {".ief", "image/ief"},
                {".iii", "application/x-iphone"},
                {".inc", "text/plain"},
                {".inf", "application/octet-stream"},
                {".ini", "text/plain"},
                {".inl", "text/plain"},
                {".ins", "application/x-internet-signup"},
                {".ipa", "application/x-itunes-ipa"},
                {".ipg", "application/x-itunes-ipg"},
                {".ipproj", "text/plain"},
                {".ipsw", "application/x-itunes-ipsw"},
                {".iqy", "text/x-ms-iqy"},
                {".isp", "application/x-internet-signup"},
                {".ite", "application/x-itunes-ite"},
                {".itlp", "application/x-itunes-itlp"},
                {".itms", "application/x-itunes-itms"},
                {".itpc", "application/x-itunes-itpc"},
                {".IVF", "video/x-ivf"},
                {".jar", "application/java-archive"},
                {".java", "application/octet-stream"},
                {".jck", "application/liquidmotion"},
                {".jcz", "application/liquidmotion"},
                {".jfif", "image/pjpeg"},
                {".jnlp", "application/x-java-jnlp-file"},
                {".jpb", "application/octet-stream"},
                {".jpe", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".jpg", "image/jpeg"},
                {".js", "application/javascript"},
                {".json", "application/json"},
                {".jsx", "text/jscript"},
                {".jsxbin", "text/plain"},
                {".latex", "application/x-latex"},
                {".library-ms", "application/windows-library+xml"},
                {".lit", "application/x-ms-reader"},
                {".loadtest", "application/xml"},
                {".lpk", "application/octet-stream"},
                {".lsf", "video/x-la-asf"},
                {".lst", "text/plain"},
                {".lsx", "video/x-la-asf"},
                {".lzh", "application/octet-stream"},
                {".m13", "application/x-msmediaview"},
                {".m14", "application/x-msmediaview"},
                {".m1v", "video/mpeg"},
                {".m2t", "video/vnd.dlna.mpeg-tts"},
                {".m2ts", "video/vnd.dlna.mpeg-tts"},
                {".m2v", "video/mpeg"},
                {".m3u", "audio/x-mpegurl"},
                {".m3u8", "audio/x-mpegurl"},
                {".m4a", "audio/m4a"},
                {".m4b", "audio/m4b"},
                {".m4p", "audio/m4p"},
                {".m4r", "audio/x-m4r"},
                {".m4v", "video/x-m4v"},
                {".mac", "image/x-macpaint"},
                {".mak", "text/plain"},
                {".man", "application/x-troff-man"},
                {".manifest", "application/x-ms-manifest"},
                {".map", "text/plain"},
                {".master", "application/xml"},
                {".mbox", "application/mbox"},
                {".mda", "application/msaccess"},
                {".mdb", "application/x-msaccess"},
                {".mde", "application/msaccess"},
                {".mdp", "application/octet-stream"},
                {".me", "application/x-troff-me"},
                {".mfp", "application/x-shockwave-flash"},
                {".mht", "message/rfc822"},
                {".mhtml", "message/rfc822"},
                {".mid", "audio/mid"},
                {".midi", "audio/mid"},
                {".mix", "application/octet-stream"},
                {".mk", "text/plain"},
                {".mmf", "application/x-smaf"},
                {".mno", "text/xml"},
                {".mny", "application/x-msmoney"},
                {".mod", "video/mpeg"},
                {".mov", "video/quicktime"},
                {".movie", "video/x-sgi-movie"},
                {".mp2", "video/mpeg"},
                {".mp2v", "video/mpeg"},
                {".mp3", "audio/mpeg"},
                {".mp4", "video/mp4"},
                {".mp4v", "video/mp4"},
                {".mpa", "video/mpeg"},
                {".mpe", "video/mpeg"},
                {".mpeg", "video/mpeg"},
                {".mpf", "application/vnd.ms-mediapackage"},
                {".mpg", "video/mpeg"},
                {".mpp", "application/vnd.ms-project"},
                {".mpv2", "video/mpeg"},
                {".mqv", "video/quicktime"},
                {".ms", "application/x-troff-ms"},
                {".msg", "application/vnd.ms-outlook"},
                {".msi", "application/octet-stream"},
                {".mso", "application/octet-stream"},
                {".mts", "video/vnd.dlna.mpeg-tts"},
                {".mtx", "application/xml"},
                {".mvb", "application/x-msmediaview"},
                {".mvc", "application/x-miva-compiled"},
                {".mxp", "application/x-mmxp"},
                {".nc", "application/x-netcdf"},
                {".nsc", "video/x-ms-asf"},
                {".nws", "message/rfc822"},
                {".ocx", "application/octet-stream"},
                {".oda", "application/oda"},
                {".odb", "application/vnd.oasis.opendocument.database"},
                {".odc", "application/vnd.oasis.opendocument.chart"},
                {".odf", "application/vnd.oasis.opendocument.formula"},
                {".odg", "application/vnd.oasis.opendocument.graphics"},
                {".odh", "text/plain"},
                {".odi", "application/vnd.oasis.opendocument.image"},
                {".odl", "text/plain"},
                {".odm", "application/vnd.oasis.opendocument.text-master"},
                {".odp", "application/vnd.oasis.opendocument.presentation"},
                {".ods", "application/vnd.oasis.opendocument.spreadsheet"},
                {".odt", "application/vnd.oasis.opendocument.text"},
                {".oga", "audio/ogg"},
                {".ogg", "audio/ogg"},
                {".ogv", "video/ogg"},
                {".ogx", "application/ogg"},
                {".one", "application/onenote"},
                {".onea", "application/onenote"},
                {".onepkg", "application/onenote"},
                {".onetmp", "application/onenote"},
                {".onetoc", "application/onenote"},
                {".onetoc2", "application/onenote"},
                {".opus", "audio/ogg"},
                {".orderedtest", "application/xml"},
                {".osdx", "application/opensearchdescription+xml"},
                {".otf", "application/font-sfnt"},
                {".otg", "application/vnd.oasis.opendocument.graphics-template"},
                {".oth", "application/vnd.oasis.opendocument.text-web"},
                {".otp", "application/vnd.oasis.opendocument.presentation-template"},
                {".ots", "application/vnd.oasis.opendocument.spreadsheet-template"},
                {".ott", "application/vnd.oasis.opendocument.text-template"},
                {".oxt", "application/vnd.openofficeorg.extension"},
                {".p10", "application/pkcs10"},
                {".p12", "application/x-pkcs12"},
                {".p7b", "application/x-pkcs7-certificates"},
                {".p7c", "application/pkcs7-mime"},
                {".p7m", "application/pkcs7-mime"},
                {".p7r", "application/x-pkcs7-certreqresp"},
                {".p7s", "application/pkcs7-signature"},
                {".pbm", "image/x-portable-bitmap"},
                {".pcast", "application/x-podcast"},
                {".pct", "image/pict"},
                {".pcx", "application/octet-stream"},
                {".pcz", "application/octet-stream"},
                {".pdf", "application/pdf"},
                {".pfb", "application/octet-stream"},
                {".pfm", "application/octet-stream"},
                {".pfx", "application/x-pkcs12"},
                {".pgm", "image/x-portable-graymap"},
                {".pic", "image/pict"},
                {".pict", "image/pict"},
                {".pkgdef", "text/plain"},
                {".pkgundef", "text/plain"},
                {".pko", "application/vnd.ms-pki.pko"},
                {".pls", "audio/scpls"},
                {".pma", "application/x-perfmon"},
                {".pmc", "application/x-perfmon"},
                {".pml", "application/x-perfmon"},
                {".pmr", "application/x-perfmon"},
                {".pmw", "application/x-perfmon"},
                {".png", "image/png"},
                {".pnm", "image/x-portable-anymap"},
                {".pnt", "image/x-macpaint"},
                {".pntg", "image/x-macpaint"},
                {".pnz", "image/png"},
                {".pot", "application/vnd.ms-powerpoint"},
                {".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
                {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
                {".ppa", "application/vnd.ms-powerpoint"},
                {".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
                {".ppm", "image/x-portable-pixmap"},
                {".pps", "application/vnd.ms-powerpoint"},
                {".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
                {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
                {".ppt", "application/vnd.ms-powerpoint"},
                {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
                {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
                {".prf", "application/pics-rules"},
                {".prm", "application/octet-stream"},
                {".prx", "application/octet-stream"},
                {".ps", "application/postscript"},
                {".psc1", "application/PowerShell"},
                {".psd", "application/octet-stream"},
                {".psess", "application/xml"},
                {".psm", "application/octet-stream"},
                {".psp", "application/octet-stream"},
                {".pst", "application/vnd.ms-outlook"},
                {".pub", "application/x-mspublisher"},
                {".pwz", "application/vnd.ms-powerpoint"},
                {".qht", "text/x-html-insertion"},
                {".qhtm", "text/x-html-insertion"},
                {".qt", "video/quicktime"},
                {".qti", "image/x-quicktime"},
                {".qtif", "image/x-quicktime"},
                {".qtl", "application/x-quicktimeplayer"},
                {".qxd", "application/octet-stream"},
                {".ra", "audio/x-pn-realaudio"},
                {".ram", "audio/x-pn-realaudio"},
                {".rar", "application/x-rar-compressed"},
                {".ras", "image/x-cmu-raster"},
                {".rat", "application/rat-file"},
                {".rc", "text/plain"},
                {".rc2", "text/plain"},
                {".rct", "text/plain"},
                {".rdlc", "application/xml"},
                {".reg", "text/plain"},
                {".resx", "application/xml"},
                {".rf", "image/vnd.rn-realflash"},
                {".rgb", "image/x-rgb"},
                {".rgs", "text/plain"},
                {".rm", "application/vnd.rn-realmedia"},
                {".rmi", "audio/mid"},
                {".rmp", "application/vnd.rn-rn_music_package"},
                {".roff", "application/x-troff"},
                {".rpm", "audio/x-pn-realaudio-plugin"},
                {".rqy", "text/x-ms-rqy"},
                {".rtf", "application/rtf"},
                {".rtx", "text/richtext"},
                {".rvt", "application/octet-stream" },
                {".ruleset", "application/xml"},
                {".s", "text/plain"},
                {".safariextz", "application/x-safari-safariextz"},
                {".scd", "application/x-msschedule"},
                {".scr", "text/plain"},
                {".sct", "text/scriptlet"},
                {".sd2", "audio/x-sd2"},
                {".sdp", "application/sdp"},
                {".sea", "application/octet-stream"},
                {".searchConnector-ms", "application/windows-search-connector+xml"},
                {".setpay", "application/set-payment-initiation"},
                {".setreg", "application/set-registration-initiation"},
                {".settings", "application/xml"},
                {".sgimb", "application/x-sgimb"},
                {".sgml", "text/sgml"},
                {".sh", "application/x-sh"},
                {".shar", "application/x-shar"},
                {".shtml", "text/html"},
                {".sit", "application/x-stuffit"},
                {".sitemap", "application/xml"},
                {".skin", "application/xml"},
                {".skp", "application/x-koan" },
                {".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
                {".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
                {".slk", "application/vnd.ms-excel"},
                {".sln", "text/plain"},
                {".slupkg-ms", "application/x-ms-license"},
                {".smd", "audio/x-smd"},
                {".smi", "application/octet-stream"},
                {".smx", "audio/x-smd"},
                {".smz", "audio/x-smd"},
                {".snd", "audio/basic"},
                {".snippet", "application/xml"},
                {".snp", "application/octet-stream"},
                {".sol", "text/plain"},
                {".sor", "text/plain"},
                {".spc", "application/x-pkcs7-certificates"},
                {".spl", "application/futuresplash"},
                {".spx", "audio/ogg"},
                {".src", "application/x-wais-source"},
                {".srf", "text/plain"},
                {".SSISDeploymentManifest", "text/xml"},
                {".ssm", "application/streamingmedia"},
                {".sst", "application/vnd.ms-pki.certstore"},
                {".stl", "application/vnd.ms-pki.stl"},
                {".sv4cpio", "application/x-sv4cpio"},
                {".sv4crc", "application/x-sv4crc"},
                {".svc", "application/xml"},
                {".svg", "image/svg+xml"},
                {".swf", "application/x-shockwave-flash"},
                {".step", "application/step"},
                {".stp", "application/step"},
                {".t", "application/x-troff"},
                {".tar", "application/x-tar"},
                {".tcl", "application/x-tcl"},
                {".testrunconfig", "application/xml"},
                {".testsettings", "application/xml"},
                {".tex", "application/x-tex"},
                {".texi", "application/x-texinfo"},
                {".texinfo", "application/x-texinfo"},
                {".tgz", "application/x-compressed"},
                {".thmx", "application/vnd.ms-officetheme"},
                {".thn", "application/octet-stream"},
                {".tif", "image/tiff"},
                {".tiff", "image/tiff"},
                {".tlh", "text/plain"},
                {".tli", "text/plain"},
                {".toc", "application/octet-stream"},
                {".tr", "application/x-troff"},
                {".trm", "application/x-msterminal"},
                {".trx", "application/xml"},
                {".ts", "video/vnd.dlna.mpeg-tts"},
                {".tsv", "text/tab-separated-values"},
                {".ttf", "application/font-sfnt"},
                {".tts", "video/vnd.dlna.mpeg-tts"},
                {".txt", "text/plain"},
                {".u32", "application/octet-stream"},
                {".uls", "text/iuls"},
                {".user", "text/plain"},
                {".ustar", "application/x-ustar"},
                {".vb", "text/plain"},
                {".vbdproj", "text/plain"},
                {".vbk", "video/mpeg"},
                {".vbproj", "text/plain"},
                {".vbs", "text/vbscript"},
                {".vcf", "text/x-vcard"},
                {".vcproj", "application/xml"},
                {".vcs", "text/plain"},
                {".vcxproj", "application/xml"},
                {".vddproj", "text/plain"},
                {".vdp", "text/plain"},
                {".vdproj", "text/plain"},
                {".vdx", "application/vnd.ms-visio.viewer"},
                {".vml", "text/xml"},
                {".vscontent", "application/xml"},
                {".vsct", "text/xml"},
                {".vsd", "application/vnd.visio"},
                {".vsi", "application/ms-vsi"},
                {".vsix", "application/vsix"},
                {".vsixlangpack", "text/xml"},
                {".vsixmanifest", "text/xml"},
                {".vsmdi", "application/xml"},
                {".vspscc", "text/plain"},
                {".vss", "application/vnd.visio"},
                {".vsscc", "text/plain"},
                {".vssettings", "text/xml"},
                {".vssscc", "text/plain"},
                {".vst", "application/vnd.visio"},
                {".vstemplate", "text/xml"},
                {".vsto", "application/x-ms-vsto"},
                {".vsw", "application/vnd.visio"},
                {".vsx", "application/vnd.visio"},
                {".vtx", "application/vnd.visio"},
                {".wav", "audio/wav"},
                {".wave", "audio/wav"},
                {".wax", "audio/x-ms-wax"},
                {".wbk", "application/msword"},
                {".wbmp", "image/vnd.wap.wbmp"},
                {".wcm", "application/vnd.ms-works"},
                {".wdb", "application/vnd.ms-works"},
                {".wdp", "image/vnd.ms-photo"},
                {".webarchive", "application/x-safari-webarchive"},
                {".webm", "video/webm"},
                {".webp", "image/webp"}, /* https://en.wikipedia.org/wiki/WebP */
                {".webtest", "application/xml"},
                {".wiq", "application/xml"},
                {".wiz", "application/msword"},
                {".wks", "application/vnd.ms-works"},
                {".WLMP", "application/wlmoviemaker"},
                {".wlpginstall", "application/x-wlpg-detect"},
                {".wlpginstall3", "application/x-wlpg3-detect"},
                {".wm", "video/x-ms-wm"},
                {".wma", "audio/x-ms-wma"},
                {".wmd", "application/x-ms-wmd"},
                {".wmf", "application/x-msmetafile"},
                {".wml", "text/vnd.wap.wml"},
                {".wmlc", "application/vnd.wap.wmlc"},
                {".wmls", "text/vnd.wap.wmlscript"},
                {".wmlsc", "application/vnd.wap.wmlscriptc"},
                {".wmp", "video/x-ms-wmp"},
                {".wmv", "video/x-ms-wmv"},
                {".wmx", "video/x-ms-wmx"},
                {".wmz", "application/x-ms-wmz"},
                {".woff", "application/font-woff"},
                {".wpl", "application/vnd.ms-wpl"},
                {".wps", "application/vnd.ms-works"},
                {".wri", "application/x-mswrite"},
                {".wrl", "x-world/x-vrml"},
                {".wrz", "x-world/x-vrml"},
                {".wsc", "text/scriptlet"},
                {".wsdl", "text/xml"},
                {".wvx", "video/x-ms-wvx"},
                {".x", "application/directx"},
                {".xaf", "x-world/x-vrml"},
                {".xaml", "application/xaml+xml"},
                {".xap", "application/x-silverlight-app"},
                {".xbap", "application/x-ms-xbap"},
                {".xbm", "image/x-xbitmap"},
                {".xdr", "text/plain"},
                {".xht", "application/xhtml+xml"},
                {".xhtml", "application/xhtml+xml"},
                {".xla", "application/vnd.ms-excel"},
                {".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
                {".xlc", "application/vnd.ms-excel"},
                {".xld", "application/vnd.ms-excel"},
                {".xlk", "application/vnd.ms-excel"},
                {".xll", "application/vnd.ms-excel"},
                {".xlm", "application/vnd.ms-excel"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
                {".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".xlt", "application/vnd.ms-excel"},
                {".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
                {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
                {".xlw", "application/vnd.ms-excel"},
                {".xml", "text/xml"},
                {".xmp", "application/octet-stream" },
                {".xmta", "application/xml"},
                {".xof", "x-world/x-vrml"},
                {".XOML", "text/plain"},
                {".xpm", "image/x-xpixmap"},
                {".xps", "application/vnd.ms-xpsdocument"},
                {".xrm-ms", "text/xml"},
                {".xsc", "application/xml"},
                {".xsd", "text/xml"},
                {".xsf", "text/xml"},
                {".xsl", "text/xml"},
                {".xslt", "text/xml"},
                {".xsn", "application/octet-stream"},
                {".xss", "application/xml"},
                {".xspf", "application/xspf+xml"},
                {".xtp", "application/octet-stream"},
                {".xwd", "image/x-xwindowdump"},
                {".z", "application/x-compress"},
                {".zip", "application/zip"},            

                #endregion

                };
        if (mappings.TryGetValue(fileext, out mime))
        {
            return mime;
        }
        else
        {
            return "application/octet-stream";
        }
    }

    bool CheckJogosultsag()
    {
        try
        {
            //jogosultság ellenőrzés
            KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam ep = UI.SetExecParamDefault(Page);

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");
                char jogszint = 'O';

                Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }
            }
        }
        catch (Exception ex)
        {
            string errorMessage = ResultError.GetErrorMessageFromResultObject(ResultException.GetResultFromException(ex));
            Response.Write(errorMessage);
            return false;
        }

        return true;
    }

    private void RenderDocumentPart()
    {
        //try
        //{
        //    string fileName = Request.QueryString.Get("fileName");
        //    string partRelationship = Request.QueryString.Get("partrelationship");
        //    string externalLink = Request.QueryString.Get("externalLink");
        //    string version = Request.QueryString.Get("version");
        //    string fileext = Path.GetExtension(fileName);
        //    WordVisualizer.Core.Handlers.Renderers.RelationshipPartRenderer renderer = new WordVisualizer.Core.Handlers.Renderers.RelationshipPartRenderer();
        //    renderer.FileName = fileName;
        //    renderer.RelationshipId = partRelationship;
        //    renderer.FileStream = DokumentumCache.GetDokumentum(externalLink, version);
        //    Response.Clear();
        //    renderer.Render(Context);

        //}
        //catch (Exception ex)
        //{
        //    Response.Clear();
        //    WordVisualizer.Core.Handlers.Renderers.ExceptionRenderer renderer = new WordVisualizer.Core.Handlers.Renderers.ExceptionRenderer();
        //    renderer.Exception = ex;
        //    renderer.Render(Context);
        //}
        //Response.End();
    }

    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }
}