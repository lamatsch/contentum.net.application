﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="HalozatiNyomtatokForm.aspx.cs" Inherits="HalozatiNyomtatokForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Halozati_NyomtatoFormHeaderTitle%>" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <asp:UpdatePanel ID="HalozatiNyomtatokUpdatePanel" runat="server">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label6" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelNev" runat="server" Text="Név:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                                            </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="label2" runat="server" Text="Cím:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="requiredTextBoxCim" runat="server" />
                                            </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelNote" runat="server" Text="Megjegyzés:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textNote" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" >
                                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" bOpenDirectionTop="true" />
                                        </td>
                                    </tr>                        
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
