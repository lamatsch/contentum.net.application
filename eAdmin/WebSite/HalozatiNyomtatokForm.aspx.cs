﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;

public partial class HalozatiNyomtatokForm : System.Web.UI.Page
{
    private string Command = "";
    private PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                using(var service = eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService())
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        KRT_HalozatiNyomtatok krt_halozati_nyomtatok = (KRT_HalozatiNyomtatok)result.Record;
                        LoadComponentsFromBusinessObject(krt_halozati_nyomtatok);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        pageView.SetViewOnPage(Command);
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_HalozatiNyomtatok krt_halozati_nyomtatok)
    {
        requiredTextBoxNev.Text = krt_halozati_nyomtatok.Nev;
        requiredTextBoxCim.Text = krt_halozati_nyomtatok.Cim;
        textNote.Text = krt_halozati_nyomtatok.Base.Note;
       
        ErvenyessegCalendarControl1.ErvKezd = krt_halozati_nyomtatok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_halozati_nyomtatok.ErvVege;
        if (Command == CommandName.Modify)
        {
            ErvenyessegCalendarControl1.Enabled = true;
            requiredTextBoxCim.Enabled = true;
            requiredTextBoxNev.Enabled = true;
            textNote.Enabled = true;
        }

        FormHeader1.Record_Ver = krt_halozati_nyomtatok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_halozati_nyomtatok.Base);

        if (Command == CommandName.View)
        {
            ErvenyessegCalendarControl1.Enabled = false;
            requiredTextBoxCim.Enabled = false;
            requiredTextBoxNev.Enabled = false;
            textNote.Enabled = false;
        }
    }

    // form --> business object
    private KRT_HalozatiNyomtatok GetBusinessObjectFromComponents()
    {
        KRT_HalozatiNyomtatok krt_halozati_nyomtatok = new KRT_HalozatiNyomtatok();
        krt_halozati_nyomtatok.Updated.SetValueAll(false);
        krt_halozati_nyomtatok.Base.Updated.SetValueAll(false);
        
        krt_halozati_nyomtatok.Nev = requiredTextBoxNev.Text;
        krt_halozati_nyomtatok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        
        krt_halozati_nyomtatok.Cim = requiredTextBoxCim.Text;
        krt_halozati_nyomtatok.Updated.Cim = pageView.GetUpdatedByView(requiredTextBoxCim);

        krt_halozati_nyomtatok.Base.Note = textNote.Text;
        krt_halozati_nyomtatok.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_halozati_nyomtatok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_halozati_nyomtatok.Base.Ver = FormHeader1.Record_Ver;
        krt_halozati_nyomtatok.Base.Updated.Ver = true;
        return krt_halozati_nyomtatok;
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            using (var service = eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService())
                            {
                                KRT_HalozatiNyomtatok krt_halozati_nyomtatok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = service.Insert(execParam, krt_halozati_nyomtatok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                using (var service = eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService())
                                {
                                    KRT_HalozatiNyomtatok krt_halozati_nyomtatok = GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.Update(execParam, krt_halozati_nyomtatok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                    
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}