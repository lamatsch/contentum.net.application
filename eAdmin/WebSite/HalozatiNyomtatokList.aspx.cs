﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class HalozatiNyomtatokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "HalozatiNyomtatokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.HalozatiNyomtatokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_HalozatiNyomtatokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.RevalidateVisible = false;
        ListHeader1.SendObjectsVisible = false;
        
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
        

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("HalozatiNyomtatokSearch.aspx", ""
            , 800, 650, HalozatiNyomtatokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("HalozatiNyomtatokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, HalozatiNyomtatokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(HalozatiNyomtatokGridView.ClientID); 
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(HalozatiNyomtatokGridView.ClientID);

        //ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(HalozatiNyomtatokGridView.ClientID);
        //ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(HalozatiNyomtatokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(HalozatiNyomtatokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("HalozatiNyomtatokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, HalozatiNyomtatokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = HalozatiNyomtatokGridView;

        //hozzárendelt felhasználók
        HozzarendeltFelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        HozzarendeltFelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        HozzarendeltFelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        HozzarendeltFelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        HozzarendeltFelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(HozzarendeltFelhasznalokGridView.ClientID);
        HozzarendeltFelhasznalokSubListHeader.ButtonsClick += new CommandEventHandler(HozzarendeltFelhasznalokSubListHeader_ButtonsClick);
        //selectedRecordId kezelése
        HozzarendeltFelhasznalokSubListHeader.AttachedGridView = HozzarendeltFelhasznalokGridView;

        HozzarendeltFelhasznalokSubListHeader.ValidFilterVisible = false;
        
        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_HalozatiNyomtatokSearch());

        if (!IsPostBack) HalozatiNyomtatokGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtato" + CommandName.ViewHistory);

        HozzarendeltFelhasznalokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_HalozatiNyomtato" + CommandName.New);
        HozzarendeltFelhasznalokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_HalozatiNyomtato" + CommandName.View);
        HozzarendeltFelhasznalokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_HalozatiNyomtato" + CommandName.Modify);
        HozzarendeltFelhasznalokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_HalozatiNyomtato" + CommandName.Invalidate);

        //if (IsPostBack)
        
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView);
        String DetailListSelectedRowId = UI.GetGridViewSelectedRecordId(HozzarendeltFelhasznalokGridView);
        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId, DetailListSelectedRowId);
        if (String.IsNullOrEmpty(MasterListSelectedRowId))
        {
            ActiveTabClear();
        }
        ActiveTabRefreshOnClientClicks();
        ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        

        ui.SetClientScriptToGridViewSelectDeSelectButton(HalozatiNyomtatokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(HozzarendeltFelhasznalokGridView);
    }


    #endregion


    #region Master List

    protected void HalozatiNyomtatokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("HalozatiNyomtatokGridView", ViewState, "KRT_Halozati_Nyomtatok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("HalozatiNyomtatokGridView", ViewState);

        HalozatiNyomtatokGridViewBind(sortExpression, sortDirection);
    }

    protected void HalozatiNyomtatokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_HalozatiNyomtatokService service = eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_HalozatiNyomtatokSearch search = (KRT_HalozatiNyomtatokSearch)Search.GetSearchObject(Page, new KRT_HalozatiNyomtatokSearch());
        search.OrderBy = Search.GetOrderBy("HalozatiNyomtatokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(HalozatiNyomtatokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

        Contentum.eUtility.EsemenyNaplo.Insert(ExecParam, "KRT_HalozatiNyomtatok", FunkcioKod, search, res);
    }

    protected void HalozatiNyomtatokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }

    protected void HalozatiNyomtatokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, HalozatiNyomtatokCPE);
        ListHeader1.RefreshPagerLabel();
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        HalozatiNyomtatokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, HalozatiNyomtatokCPE);
        HalozatiNyomtatokGridViewBind();
    }

    protected void HalozatiNyomtatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(HalozatiNyomtatokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id, string felhasznaloId)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("HalozatiNyomtatokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, HalozatiNyomtatokUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("HalozatiNyomtatokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, HalozatiNyomtatokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            string tableName = "KRT_Halozati_Nyomtatok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, HalozatiNyomtatokUpdatePanel.ClientID);

            HozzarendeltFelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Halozati_NyomtatoForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, HozzarendeltFelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            
        }
    }

    protected void HalozatiNyomtatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    HalozatiNyomtatokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedHalozatiNyomtatok();
            HalozatiNyomtatokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.SendObjects:
                SendMailSelectedHalozatiNyomtatok();
                break;
        }
    }

    /// <summary>
    /// Törli a HalozatiNyomtatokGridView -ban kijelölt elemet
    /// </summary>
    private void deleteSelectedHalozatiNyomtatok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "HalozatiNyomtatoInvalidate"))
        {
            String Id = UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView);
            using (var service = eAdminService.ServiceFactory.GetKRT_HalozatiNyomtatokService())
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = Id;

                Result result = service.Invalidate(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a HalozatiNyomtatokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedHalozatiNyomtatok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(HalozatiNyomtatokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_HalozatiNyomtatok");
        }
    }

    protected void HalozatiNyomtatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        HalozatiNyomtatokGridViewBind(e.SortExpression, UI.GetSortToGridView("HalozatiNyomtatokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion
    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (HalozatiNyomtatokGridView.SelectedIndex == -1)
        {
            return;
        }
        string HalozatiNyomtatoId = UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, HalozatiNyomtatoId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string HalozatiNyomtatoId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                HozzarendeltFelhasznalokGridViewBind(HalozatiNyomtatoId);
                HozzarendeltFelhasznalokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(HozzarendeltFelhasznalokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        HozzarendeltFelhasznalokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(HozzarendeltFelhasznalokTabPanel))
        {
            HozzarendeltFelhasznalokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView), UI.GetGridViewSelectedRecordId(HozzarendeltFelhasznalokGridView));
        }
    }

    #endregion

    #region HozzarendeltFelhasznalok Detail

    private void HozzarendeltFelhasznalokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedHozzarendeltFelhasznalok();
            HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HozzarendeltFelhasznalokGridView));
        }
    }

    protected void HozzarendeltFelhasznalokGridViewBind(string nyomtatoId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("HozzarendeltFelhasznalokGridView", ViewState, "Szerepkor_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("HozzarendeltFelhasznalokGridView", ViewState);

        HozzarendeltFelhasznalokGridViewBind(nyomtatoId, sortExpression, sortDirection);
    }

    protected void HozzarendeltFelhasznalokGridViewBind(string nyomtatoId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(nyomtatoId))
        {
            var service = eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result res = service.GetByNyomtato(ExecParam, nyomtatoId);

            UI.GridViewFill(HozzarendeltFelhasznalokGridView, res, HozzarendeltFelhasznalokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(HozzarendeltFelhasznalokGridView);
        }
        else
        {
            ui.GridViewClear(HozzarendeltFelhasznalokGridView);
        }
    }

    void HozzarendeltFelhasznalokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
    }

    protected void HozzarendeltFelhasznalokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(HozzarendeltFelhasznalokTabPanel))
                    //{
                    //    HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
                    //}
                    ActiveTabRefreshDetailList(HozzarendeltFelhasznalokTabPanel);
                    break;
            }
        }
    }

    protected void HozzarendeltFelhasznalokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(HozzarendeltFelhasznalokGridView, selectedRowNumber, "check");
        }
    }

    private void HozzarendeltFelhasznalokGridView_RefreshOnClientClicks(string id, string felhasznaloId)
    {
        if (!String.IsNullOrEmpty(id) && ! String.IsNullOrEmpty(felhasznaloId))
        {
            HozzarendeltFelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Halozati_NyomtatoForm.aspx"
                , "Command=" + CommandName.View + "&" + QueryStringVars.FelhasznaloId + "=" + felhasznaloId + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, HozzarendeltFelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            HozzarendeltFelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Halozati_NyomtatoForm.aspx"
                , "Command=" + CommandName.Modify + "&" + QueryStringVars.FelhasznaloId + "=" + felhasznaloId + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, HozzarendeltFelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //HozzarendeltFelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Halozati_NyomtatoForm.aspx"
            //    , "Command=" + CommandName.Invalidate + "&" + QueryStringVars.FelhasznaloId + "=" + felhasznaloId + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, HozzarendeltFelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void HozzarendeltFelhasznalokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = HozzarendeltFelhasznalokGridView.PageIndex;

        HozzarendeltFelhasznalokGridView.PageIndex = HozzarendeltFelhasznalokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != HozzarendeltFelhasznalokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, HozzarendeltFelhasznalokCPE);
            HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(HozzarendeltFelhasznalokSubListHeader.Scrollable, HozzarendeltFelhasznalokCPE);
        }
        HozzarendeltFelhasznalokSubListHeader.PageCount = HozzarendeltFelhasznalokGridView.PageCount;
        HozzarendeltFelhasznalokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(HozzarendeltFelhasznalokGridView);
    }

    void HozzarendeltFelhasznalokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(HozzarendeltFelhasznalokSubListHeader.RowCount);
        HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView));
    }

    protected void HozzarendeltFelhasznalokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        HozzarendeltFelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView)
            , e.SortExpression, UI.GetSortToGridView("HozzarendeltFelhasznalokGridView", ViewState, e.SortExpression));
    }

    /// <summary>
    /// Törli a SzerepkörGridView -ban kijelölt elemet
    /// </summary>
    private void deleteSelectedHozzarendeltFelhasznalok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(HalozatiNyomtatokGridView);
            String DetailListSelectedRowId = UI.GetGridViewSelectedRecordId(HozzarendeltFelhasznalokGridView);

            using (var service = eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService())
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                var res = LoadFelhasznalo_halozatiNyomtatoRecord(MasterListSelectedRowId,DetailListSelectedRowId);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                    return;
                }
                if (res.Ds != null && res.Ds.Tables != null && res.Ds.Tables.Count == 1 && res.Ds.Tables[0].Rows != null && res.Ds.Tables[0].Rows.Count==1)
                    execParam.Record_Id = res.Ds.Tables[0].Rows[0]["Id"].ToString();

                Result result = service.Invalidate(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private Result LoadFelhasznalo_halozatiNyomtatoRecord(string nyomtatoID, string felhasznaloID)
    {
        ExecParam _ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_Felhasznalok_Halozati_NyomtatoiSearch search = new KRT_Felhasznalok_Halozati_NyomtatoiSearch();
        var res = new Result();
        using (var service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Felhasznalok_Halozati_NyomtatoiService())
        {
            search.Halozati_Nyomtato_Id.Value = nyomtatoID;
            search.Halozati_Nyomtato_Id.Operator = Query.Operators.equals;
            search.Halozati_Nyomtato_Id.Group = "100";
            search.Felhasznalo_Id.Value = felhasznaloID;
            search.Felhasznalo_Id.Operator = Query.Operators.equals;

            res = service.GetAll(_ExecParam, search);


        }
        return res;
    }


    #endregion

}