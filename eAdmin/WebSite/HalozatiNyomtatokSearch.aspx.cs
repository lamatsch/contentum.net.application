﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;

public partial class HalozatiNyomtatokSearch : System.Web.UI.Page
{
    private Type _type = typeof(KRT_HalozatiNyomtatokSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.HalozatiNyomtatokSearchHeaderTitle;
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            

            KRT_HalozatiNyomtatokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_HalozatiNyomtatokSearch)Search.GetSearchObject(Page, new KRT_HalozatiNyomtatokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_HalozatiNyomtatokSearch krt_halozati_nyomtatokSearch = null;
        if (searchObject != null) krt_halozati_nyomtatokSearch = (KRT_HalozatiNyomtatokSearch)searchObject;

        if (krt_halozati_nyomtatokSearch != null)
        {
            Nev_TextBox.Text = krt_halozati_nyomtatokSearch.Nev.Value;
            cim_TextBox.Text = krt_halozati_nyomtatokSearch.Cim.Value;
            
            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_halozati_nyomtatokSearch.ErvKezd, krt_halozati_nyomtatokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_HalozatiNyomtatokSearch SetSearchObjectFromComponents()
    {
        KRT_HalozatiNyomtatokSearch krt_halozati_nyomtatokSearch = (KRT_HalozatiNyomtatokSearch)SearchHeader1.TemplateObject;
        if (krt_halozati_nyomtatokSearch == null)
        {
            krt_halozati_nyomtatokSearch = new KRT_HalozatiNyomtatokSearch();
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            krt_halozati_nyomtatokSearch.Nev.Value = Nev_TextBox.Text;
            krt_halozati_nyomtatokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }
        if (!String.IsNullOrEmpty(cim_TextBox.Text))
        {
            krt_halozati_nyomtatokSearch.Cim.Value = cim_TextBox.Text;
            krt_halozati_nyomtatokSearch.Cim.Operator = Search.GetOperatorByLikeCharater(cim_TextBox.Text);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_halozati_nyomtatokSearch.ErvKezd, krt_halozati_nyomtatokSearch.ErvVege);

        return krt_halozati_nyomtatokSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_HalozatiNyomtatokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_HalozatiNyomtatokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_HalozatiNyomtatokSearch();
    }
}