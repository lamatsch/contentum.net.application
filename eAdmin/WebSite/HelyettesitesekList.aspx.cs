using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class HelyettesitesekList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";
    private const string funkcio_HelyettesitesFelvetelOsszesDolgozora = "HelyettesitesFelvetelOsszesDolgozora";

    public bool isUserAllowedToModifyAllEmployees = false;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "HelyettesitesekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        isUserAllowedToModifyAllEmployees = FunctionRights.GetFunkcioJog(Page, funkcio_HelyettesitesFelvetelOsszesDolgozora);

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.HelyettesitesekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_HelyettesitesekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        //ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("HelyettesitesekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("HelyettesitesekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(HelyettesitesekGridView.ClientID, "", "check");
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(HelyettesitesekGridView.ClientID, "check", "cbInvalidationAllowed", true);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(HelyettesitesekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(HelyettesitesekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(HelyettesitesekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(HelyettesitesekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = HelyettesitesekGridView;


        /*FunkciokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FunkciokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FunkciokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FunkciokGridView.ClientID);
        FunkciokSubListHeader.ButtonsClick += new CommandEventHandler(FunkciokSubListHeader_ButtonsClick);

        FelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FelhasznalokGridView.ClientID);
        FelhasznalokSubListHeader.ButtonsClick += new CommandEventHandler(FelhasznalokSubListHeader_ButtonsClick);*/


        ui.SetClientScriptToGridViewSelectDeSelectButton(HelyettesitesekGridView);
        /*ui.SetClientScriptToGridViewSelectDeSelectButton(FunkciokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);*/

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_HelyettesitesekSearch());

        if (!IsPostBack) HelyettesitesekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites" + CommandName.Lock);

        /*FunkciokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites_Funkcio" + CommandName.New);
        FunkciokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites_Funkcio" + CommandName.View);
        FunkciokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites_Funkcio" + CommandName.Modify);
        FunkciokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Helyettesites_Funkcio" + CommandName.Invalidate);

        FelhasznalokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Helyettesites" + CommandName.New);
        FelhasznalokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Helyettesites" + CommandName.View);
        FelhasznalokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Helyettesites" + CommandName.Modify);
        FelhasznalokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Helyettesites" + CommandName.Invalidate);*/

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(HelyettesitesekGridView);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }


    //// Minden gridview checkbox kezelese...
    //public void GridViewSelectAllCheckBox(object sender, CommandEventArgs e)
    //{
    //    ui.GridViewSelectAllCheckBox(UI.FindParentGridView((Control)sender));
    //}

    //public void GridViewDeSelectAllCheckBox(object sender, CommandEventArgs e)
    //{
    //    ui.GridViewDeSelectAllCheckBox(UI.FindParentGridView((Control)sender));
    //}

    #endregion

    #region Master List

    protected void HelyettesitesekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("HelyettesitesekGridView", ViewState, "KRT_Helyettesitesek.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("HelyettesitesekGridView", ViewState, SortDirection.Descending);

        HelyettesitesekGridViewBind(sortExpression, sortDirection);
    }

    protected void HelyettesitesekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_HelyettesitesekSearch search = null;

        if (!Search.IsSearchObjectInSession(Page, typeof(KRT_HelyettesitesekSearch)))
        {
            search = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
            search.OrderBy = Search.GetOrderBy("HelyettesitesekGridView", ViewState, SortExpression, SortDirection);
            search.TopRow = UI.GetTopRow(Page);

            search.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Helyettesites;
            search.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;
            search.HelyettesitesVege.Value = DateTime.Now.ToString();
            search.HelyettesitesVege.Operator = Contentum.eQuery.Query.Operators.greater;
            Search.SetSearchObject(Page, search);
        }
        // CR3413
        if (search == null) search = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
        search.OrderBy = Search.GetOrderBy("HelyettesitesekGridView", ViewState, SortExpression, SortDirection);
        // TODO: sz�r�sek hibakezel�se

        // ha nem kezelheti mindet
        // ha vezet�:
        // -  l�thatja az �sszes rekordot, ahol a helyettes�tett vagy a helyettes�t� az � szervezeti hierarchi�j�nak tagja
        // -  m�dos�thatja az �sszes rekordot, ahol a helyettes�tett vagy a helyettes�t� az � szervezeti hierarchi�j�nak tagja
        // -  helyettes�tett �s helyettes�t� csak az � szervezeti hierarchi�j�nak tagja lehet
        // ha dolgoz�, csak a saj�t csoportj�n bel�lieket l�thatja
        // -  l�thatja az �sszes rekordot, ahol � a helyettes�tett vagy a helyettes�t�
        // -  m�dos�thatja az �sszes rekordot, ahol � a helyettes�tett
        // -  helyettes�t�je csak az � csoportj�nak tagja lehet

        if (!isUserAllowedToModifyAllEmployees)
        {
            string CsoportTag_Id = FelhasznaloProfil.FelhasznaloCsoportTagId(Page);
            search.ExtendedFilter.CsoportTag_Id = CsoportTag_Id;
        }
        else
        {
            search.ExtendedFilter.CsoportTag_Id = "";
        }

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(HelyettesitesekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void HelyettesitesekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        CheckBox cbCurrentUserAllowedToModify = (CheckBox)e.Row.FindControl("cbCurrentUserAllowedToModify");
        CheckBox cbInvalidationAllowed = (CheckBox)e.Row.FindControl("cbInvalidationAllowed");
        CheckBox cbStartedAtPast = (CheckBox)e.Row.FindControl("cbStartedAtPast");
        CheckBox cbEndedAtPast = (CheckBox)e.Row.FindControl("cbEndedAtPast");

        #region m�dos�that�s�g
        if (cbCurrentUserAllowedToModify != null)
        {
            if (isUserAllowedToModifyAllEmployees)
            {
                cbCurrentUserAllowedToModify.Checked = true;
            }
            else
            {
                string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
                
                if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.dolgozo)
                {
                    string helyettesitett = UI.GetGridViewColumnText(e.Row, "Felhasznalo_ID_helyettesitett");
                    if (helyettesitett == FelhasznaloProfil.FelhasznaloId(Page))
                    {
                        cbCurrentUserAllowedToModify.Checked = true;
                    }
                    else
                    {
                        cbCurrentUserAllowedToModify.Checked = false;
                    }
                }
                else if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
                {
                    cbCurrentUserAllowedToModify.Checked = true;
                }
            }
        }
        #endregion m�dos�that�s�g

        #region �rv�nytelen�thet�s�g
        //if (cbInvalidationAllowed != null && cbStartedAtPast != null && cbCurrentUserAllowedToModify != null)
        //{
        //    cbInvalidationAllowed.Checked = !cbStartedAtPast.Checked && cbCurrentUserAllowedToModify.Checked;
        //}

        if (cbInvalidationAllowed != null && cbEndedAtPast != null && cbCurrentUserAllowedToModify != null)
        {
            cbInvalidationAllowed.Checked = cbCurrentUserAllowedToModify.Checked && !cbEndedAtPast.Checked;
        }
        #endregion �rv�nytelen�thet�s�g
    }

    protected void HelyettesitesekGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, HelyettesitesekCPE);
        ListHeader1.RefreshPagerLabel();

        //ui.SetClientScriptToGridViewSelectDeSelectButton(HelyettesitesekGridView);

    }
    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        HelyettesitesekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, HelyettesitesekCPE);
        HelyettesitesekGridViewBind();
    }

    protected void HelyettesitesekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(HelyettesitesekGridView, selectedRowNumber, "check");

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);

            ////HelyettesitesekGridView_SelectRowCommand(id);
        }
    }
      
    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            bool isCurrentUserAllowedToModify = UI.isGridViewCheckBoxChecked(HelyettesitesekGridView, "cbCurrentUserAllowedToModify", id);

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("HelyettesitesekForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID);

            if (isCurrentUserAllowedToModify)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("HelyettesitesekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_HelyettesitesMegtekintes
                    + "')) {" + JavaScripts.SetOnClientClick("HelyettesitesekForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, HelyettesitesekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                    + "} else return false;";
            }

            string tableName = "Krt_Helyettesitesek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, HelyettesitesekUpdatePanel.ClientID);

        }
    }

    protected void HelyettesitesekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    HelyettesitesekGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(HelyettesitesekGridView));
                    ////HelyettesitesekGridView_SelectRowCommand(UI.GetGridViewSelectedRecordId(HelyettesitesekGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedHelyettesitesek();
            HelyettesitesekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedHelyettesitesRecords();
                HelyettesitesekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedHelyettesitesRecords();
                HelyettesitesekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedHelyettesitesek();
                break;
        }
    }

    private void LockSelectedHelyettesitesRecords()
    {
        LockManager.LockSelectedGridViewRecords(HelyettesitesekGridView, "KRT_Helyettesitesek"
            , "HelyettesitesLock", "HelyettesitesForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedHelyettesitesRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(HelyettesitesekGridView, "KRT_Helyettesitesek"
            , "HelyettesitesLock", "HelyettesitesForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a HelyettesitesekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedHelyettesitesek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "HelyettesitesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(HelyettesitesekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = UI.isGridViewCheckBoxChecked(HelyettesitesekGridView, "cbInvalidationAllowed", deletableItemsList[i]);
                bool bStartedAtPast = UI.isGridViewCheckBoxChecked(HelyettesitesekGridView, "cbStartedAtPast", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            //Result result = service.MultiInvalidate(execParams.ToArray());
            Result result = service.MultiInvalidateHelyettesites(execParams.ToArray());
            if (Search.IsSearchObjectInSession(Page, typeof(KRT_HelyettesitesekSearch)))
            {
                KRT_HelyettesitesekSearch search = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
                search.HelyettesitesVege.Value = DateTime.Now.AddSeconds(2).ToString();
            }
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }            
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }
    
    /// <summary>
    /// Elkuldi emailben a HelyettesitesekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedHelyettesitesek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(HelyettesitesekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Helyettesitesek");
        }
    }

    protected void HelyettesitesekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        HelyettesitesekGridViewBind(e.SortExpression, UI.GetSortToGridView("HelyettesitesekGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    #endregion
}
