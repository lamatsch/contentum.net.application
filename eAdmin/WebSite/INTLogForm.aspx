<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="INTLogForm.aspx.cs" Inherits="INTLogForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/INTModulokTextBox.ascx" TagName="INTModulokTextBox" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="INTParameterek" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelINTModul" runat="server" Text="INTModul:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc7:INTModulokTextBox ID="INTModulokTextBox1" runat="server"></uc7:INTModulokTextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">                            
                            <asp:Label runat="server" Text="Sync kezdete:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelSyncStartDate" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">                            
                            <asp:Label runat="server" Text="Sync v�ge:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelSyncEndDate" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="Parancs:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelParancs" runat="server" ></asp:Label>
                        </td>
                    </tr>         
                      <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelHiba" runat="server" Text="Hiba:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textHiba" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="ExternalId:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelExternalId" runat="server" ></asp:Label>
                        </td>
                    </tr>  
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="St�tusz:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelStatus" runat="server" ></asp:Label>
                        </td>
                    </tr> 
                     <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="Elk�ld�tt dokumentum:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelDoumentumokIdSent" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="Error log:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelDokumentumokIdError" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="Checksum:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelPackageHash" runat="server" ></asp:Label>
                        </td>
                    </tr>
                     <tr class="urlapSor">
                        <td class="mrUrlapCaption" style="padding-left: 20px;">                            
                            <asp:Label runat="server" Text="Verzi�:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:Label ID="LabelPackageVer" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>

