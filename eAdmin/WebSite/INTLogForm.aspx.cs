using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eIntegrator.Service;

public partial class INTLogForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");

        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, No.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        INTModulokTextBox1.ReadOnly = true;
        labelINTModul.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INTLog" + Command);
                break;
        }

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                INT_LogService service = eIntegratorService.ServiceFactory.GetINT_LogService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    INT_Log int_Log = (INT_Log)result.Record;
                    LoadComponentsFromBusinessObject(int_Log);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

                string intModulID = Request.QueryString.Get("KodcsoportID");
                if (!String.IsNullOrEmpty(intModulID))
                {
                    INTModulokTextBox1.Id_HiddenField = intModulID;
                    INTModulokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
                    INTModulokTextBox1.Enabled = false;
                }

            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="int_Log"></param>
    private void LoadComponentsFromBusinessObject(INT_Log int_Log)
    {
        INTModulokTextBox1.Id_HiddenField = int_Log.Modul_id;
        INTModulokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        LabelSyncStartDate.Text = int_Log.Sync_StartDate;
        LabelSyncEndDate.Text = int_Log.Sync_EndDate;
        LabelParancs.Text = int_Log.Parancs;
        textHiba.Text = int_Log.HibaUzenet;
        LabelExternalId.Text = int_Log.ExternalId;
        LabelStatus.Text = GetAllapot(int_Log.Status);
        LabelDoumentumokIdSent.Text = String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('{0}GetDocumentContent.aspx?id={1}'); return false;\" style=\"text-decoration:underline\">{2}</a>", UI.GetAppSetting("eDocumentBusinessServiceUrl"), int_Log.Dokumentumok_Id_Sent, int_Log.Dokumentumok_Id_Sent);
        LabelDokumentumokIdError.Text = String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('{0}GetDocumentContent.aspx?id={1}'); return false;\" style=\"text-decoration:underline\">{2}</a>", UI.GetAppSetting("eDocumentBusinessServiceUrl"), int_Log.Dokumentumok_Id_Error, int_Log.Dokumentumok_Id_Error);
        LabelPackageHash.Text = int_Log.PackageHash;
        LabelPackageVer.Text = int_Log.PackageVer;


        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = int_Log.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(int_Log.Base);
    }

    private string GetAllapot(string kod)
    {
        switch (kod)
        {
            case "0":
                return "Folyamatban";
            case "1":
                return "Sikeres";
            default:
                return "Hiba";
        }
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(INTModulokTextBox1);
            compSelector.Add_ComponentOnClick(LabelSyncStartDate);
            compSelector.Add_ComponentOnClick(LabelSyncEndDate);
            compSelector.Add_ComponentOnClick(LabelParancs);

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

    }
}
