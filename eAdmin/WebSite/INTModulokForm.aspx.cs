using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service;
using System;
using System.Web.UI.WebControls;

public partial class INTModulokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region utility 
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxNev.TextBox.Enabled = false;
        textParancs.Enabled = false;
        textLeiras.Enabled = false;
        textGyakorisag.Enabled = false;
        textGyakorisagMertekEgyseg.Enabled = false;

        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        //textHossz.Enabled = false;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelParancs.CssClass = "mrUrlapInputWaterMarked";
        labelLeiras.CssClass = "mrUrlapInputWaterMarked";
        labelGyakorisag.CssClass = "mrUrlapInputWaterMarked";
        labelGyakorisagMertekEgyseg.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        //labelHossz.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        //requiredTextBoxKod.TextBox.Enabled = false;

        //labelKod.CssClass = "mrUrlapInputWaterMarked";
    }

    #endregion
    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INTModul" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    INT_Modulok int_Modulok = (INT_Modulok)result.Record;
                    LoadComponentsFromBusinessObject(int_Modulok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="int_Modulok"></param>
    private void LoadComponentsFromBusinessObject(INT_Modulok int_Modulok)
    {
        requiredTextBoxNev.Text = int_Modulok.Nev;
        textLeiras.Text = int_Modulok.Leiras;
        textParancs.Text = int_Modulok.Parancs;
        textGyakorisag.Text = int_Modulok.Gyakorisag;
        textGyakorisagMertekEgyseg.Text = int_Modulok.GyakorisagMertekegyseg;
        ErvenyessegCalendarControl1.ErvKezd = int_Modulok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = int_Modulok.ErvVege;        

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = int_Modulok.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(int_Modulok.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private INT_Modulok GetBusinessObjectFromComponents()
    {
        INT_Modulok int_Modulok = new INT_Modulok();
        int_Modulok.Updated.SetValueAll(false);
        int_Modulok.Base.Updated.SetValueAll(false);

        int_Modulok.Nev = requiredTextBoxNev.Text;
        int_Modulok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        int_Modulok.Leiras = textLeiras.Text;
        int_Modulok.Updated.Leiras = pageView.GetUpdatedByView(textLeiras);
        int_Modulok.Parancs = textParancs.Text;
        int_Modulok.Updated.Parancs = pageView.GetUpdatedByView(textParancs);
        int_Modulok.Gyakorisag = textGyakorisag.Text;
        int_Modulok.Updated.Gyakorisag = pageView.GetUpdatedByView(textGyakorisag);
        int_Modulok.GyakorisagMertekegyseg = textGyakorisagMertekEgyseg.Text;
        int_Modulok.Updated.GyakorisagMertekegyseg = pageView.GetUpdatedByView(textGyakorisagMertekEgyseg);

        ErvenyessegCalendarControl1.SetErvenyessegFields(int_Modulok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        int_Modulok.Base.Ver = FormHeader1.Record_Ver;
        int_Modulok.Base.Updated.Ver = true;

        return int_Modulok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            //compSelector.Add_ComponentOnClick(textHossz);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "INTModul" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();
                            INT_Modulok int_Modulok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, int_Modulok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();
                                INT_Modulok krt_KodCsoportok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_KodCsoportok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
