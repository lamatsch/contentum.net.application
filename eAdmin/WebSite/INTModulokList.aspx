<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="INTModulokList.aspx.cs" Inherits="Pages_INTModulokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <%--HiddenFields--%>
    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeINTModulok" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="INTModulokUpdatePanel" runat="server" OnLoad="INTModulokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpePartnerek" runat="server" TargetControlID="panelINTModulok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeINTModulok" CollapseControlID="btnCpeINTModulok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeINTModulok"
                            ExpandedSize="0" ExpandedText="Modulok list�ja" CollapsedText="Modulok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>

                        <asp:Panel ID="panelINTModulok" runat="server" Width="100%" printable="true">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="INTModulokGridView" runat="server" OnSorting="INTModulokGridView_Sorting"
                                            GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                            DataKeyNames="Id" AutoGenerateColumns="False" OnPreRender="INTModulokGridView_PreRender"
                                            AllowSorting="True" PagerSettings-Visible="false" AllowPaging="True"
                                            CellPadding="0" OnRowCommand="INTModulokGridView_RowCommand" OnRowDataBound="INTModulokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="INT_Modulok.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Leiras" HeaderText="Le�ras" SortExpression="INT_Modulok.Leiras">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Parancs" HeaderText="Parancs" SortExpression="INT_Modulok.Parancs">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Gyakorisag" HeaderText="Gyakoris�g" SortExpression="INT_Modulok.Gyakorisag">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GyakorisagMertekegyseg" HeaderText="Gyakoris�g m�rt�kegys�g" SortExpression="INT_Modulok.GyakorisagMertekegyseg">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UtolsoFutas" HeaderText="Utols� fut�s" SortExpression="INT_Modulok.UtolsoFutas" ReadOnly="true">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                        <div class="MediaPrintBreak" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2"></td>
        </tr>
        <!--Allist�k-->
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeDetail" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                                CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeDetail" CollapseControlID="btnCpeDetail" ExpandDirection="Vertical"
                                AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeDetail">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="panelDetail" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%" ActiveTabIndex="1">
                                    <!-- INT_Parameterek -->
                                    <ajaxToolkit:TabPanel ID="INTParameterekTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelINTParametek" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerINTParameterek" runat="server" Text="Param�terek"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="INTParameterekUpdatePanel" runat="server" OnLoad="INTParameterekUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="INTParameterekPanel" runat="server" Visible="true" Width="100%">
                                                        <uc1:SubListHeader ID="INTParameterekSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="INTParameterekCPE" runat="server" TargetControlID="panelINTParameterekList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelINTParameterekList" runat="server" printable="true">
                                                                        <asp:GridView ID="INTParameterekGridView" runat="server"
                                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="INTParameterekGridView_PreRender"
                                                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="INTParameterekGridView_Sorting"
                                                                            OnRowDataBound="INTParameterekGridView_RowDataBound" OnRowCommand="INTParameterekGridView_RowCommand">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image"
                                                                                    ShowSelectButton="True" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="INT_Parameterek.Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Ertek" HeaderText="�rt�k" SortExpression="INT_Parameterek.Ertek">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:LinkButton ID="linkModosithato" runat="server">M�dos�that�</asp:LinkButton>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbModosithato" runat="server" Enabled="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <PagerSettings Visible="False" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <!-- INT_Log -->
                                    <ajaxToolkit:TabPanel ID="INTLogTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelINTLog" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerINTLog" runat="server" Text="Log"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="INTLogUpdatePanel" runat="server" OnLoad="INTLogUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="INTLogPanel" runat="server" Visible="true" Width="100%">
                                                        <uc1:SubListHeader ID="INTLogSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="INTLogCPE" runat="server" TargetControlID="panelINTLogList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelINTLogList" runat="server" printable="true">
                                                                        <asp:GridView ID="INTLogGridView" runat="server"
                                                                            CellPadding="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E6E6E6"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="INTLogGridView_PreRender"
                                                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="INTLogGridView_Sorting"
                                                                            OnRowDataBound="INTLogGridView_RowDataBound" OnRowCommand="INTLogGridView_RowCommand" EnableModelValidation="True" OnDataBound="INTLogGridView_DataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image"
                                                                                    ShowSelectButton="True" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Sync_StartDate" HeaderText="Szinkroniz�l�s kezdete" SortExpression="INT_Log.Sync_StartDate">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Sync_EndDate" HeaderText="Szinkroniz�l�s v�ge" SortExpression="INT_Log.Sync_EndDate">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Parancs" HeaderText="Parancs" SortExpression="INT_Log.Parancs">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PackageVer" HeaderText="Csomag verzi�ja" SortExpression="INT_Log.PackageVer">
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                </asp:BoundField>                                                                                
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="labelStatusHeader" runat="server" Text="St�tusz"/>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelStatus" runat="server"/>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>                                                                                        
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="buttonKuldes" runat="server" Text="�jrak�ld�s" Visible="false"                                                                                      
                                                                                            CommandName="kuldes" CommandArgument='<%# Bind("Id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <PagerSettings Visible="False" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>


                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
