﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Data;
using System.Text;
using System.Linq;
using System.Globalization;
using Contentum.eIntegrator.Service;

public partial class Pages_INTModulokList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();
    readonly string jsNincsKivalasztva = "alert('" + Resources.List.UI_NoSelectedItem + "');return false;";
    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INTModulList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        INTParameterekSubListHeader.RowCount_Changed += new EventHandler(INTParameterekSubListHeader_RowCount_Changed);
        INTLogSubListHeader.RowCount_Changed += new EventHandler(INTLogSubListHeader_RowCount_Changed);

        INTParameterekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(INTParameterekSubListHeader_ErvenyessegFilter_Changed);
        INTLogSubListHeader.ErvenyessegFilter_Changed += new EventHandler(INTLogSubListHeader_ErvenyessegFilter_Changed);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);

        ListHeader1.RightFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);


        ListHeader1.HeaderLabel = "INTModulok listája";
        ListHeader1.SearchObjectType = typeof(INT_ModulokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        //ListHeader1.PrintHtmlVisible = true;
        ListHeader1.RevalidateVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("INTModulokSearch.aspx", ""
             , Defaults.PopupWidth, Defaults.PopupHeight, INTModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("INTModulokForm.aspx", "Command=New"
             , Defaults.PopupWidth, Defaults.PopupHeight, INTModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(INTModulokGridView.ClientID, "", "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(INTModulokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(INTModulokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(INTModulokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(INTModulokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, INTModulokUpdatePanel.ClientID, "", true);


        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = INTModulokGridView;

        INTParameterekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        INTParameterekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        INTParameterekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        INTParameterekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(INTParameterekGridView.ClientID);
        INTParameterekSubListHeader.ButtonsClick += new CommandEventHandler(INTParameterekSubListHeader_ButtonsClick);    

        //selectedRecordId kezelése
        INTParameterekSubListHeader.AttachedGridView = INTParameterekGridView;

        INTLogSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        
        INTLogSubListHeader.ButtonsClick += new CommandEventHandler(INTLogSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        INTLogSubListHeader.AttachedGridView = INTLogGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new INT_ModulokSearch());

        if (!IsPostBack) INTModulokGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        //ListHeader1.RevalidateOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(INTModulokGridView.ClientID);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.Invalidate);
        //ListHeader1.RevalidateEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.Revalidate);
        // ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Partner" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "INTModul" + CommandName.Lock);

        INTParameterekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "INTParameterList");
        INTParameterekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "INTParameter" + CommandName.New);
        INTParameterekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "INTParameter" + CommandName.View);
        INTParameterekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "INTParameter" + CommandName.Modify);
        INTParameterekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "INTParameter" + CommandName.Invalidate);

        INTLogPanel.Visible = FunctionRights.GetFunkcioJog(Page, "INTLogList");
        INTLogSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "INTLog" + CommandName.View);
        INTLogSubListHeader.NewVisible = false;
        INTLogSubListHeader.ModifyVisible = false;
        INTLogSubListHeader.InvalidateVisible = false;

        //LZS - BLG_13452
        INTLogSubListHeader.DeleteEnabled = INTLogSubListHeader.DeleteVisible = true;
        
        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(INTModulokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(INTModulokGridView);
    }


    #endregion

    #region Master List

    protected void INTModulokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("INTModulokGridView", ViewState, "INT_Modulok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("INTModulokGridView", ViewState);

        INTModulokGridViewBind(sortExpression, sortDirection);
    }

    protected void INTModulokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        INT_ModulokSearch search = (INT_ModulokSearch)Search.GetSearchObject(Page, new INT_ModulokSearch());
        search.OrderBy = Search.GetOrderBy("INTModulokGridView", ViewState, SortExpression, SortDirection);

        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        //UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(INTModulokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    public static void SetCheckBox(CheckBox cb, string value)
    {
        if (value == "1")
        {
            cb.Checked = true;
        }
        else
        {
            cb.Checked = false;
        }
    }

    protected void INTModulokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void INTModulokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpePartnerek);

        ListHeader1.RefreshPagerLabel();
    }

    private void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        INTModulokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpePartnerek);
        INTModulokGridViewBind();
    }

    protected void INTModulokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(INTModulokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("INTModulokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, INTModulokUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("INTModulokForm.aspx"
                 , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, INTModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            //string tableName = "KRT_Partnerek";
            //ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
            //     , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
            //     , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, INTModulokGridView.ClientID);

            INTParameterekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("INTParameterekForm.aspx"
                 , "Command=" + CommandName.New + "&" + QueryStringVars.PartnerId + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, INTParameterekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void INTModulokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    INTModulokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(INTModulokGridView));
                    break;
            }
        }

    }

    private void deleteSelectedModul()
    {
        if (FunctionRights.GetFunkcioJog(Page, "INTModulInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(INTModulokGridView, EErrorPanel1, ErrorUpdatePanel);
            INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void INTModulokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        INTModulokGridViewBind(e.SortExpression, UI.GetSortToGridView("INTModulokGridView", ViewState, e.SortExpression));
    }

    //private void RevalidateSelectedModulok()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "INTModulRevalidate"))
    //    {
    //        List<string> selectedItemsList = ui.GetGridViewSelectedRows(INTModulokGridView, EErrorPanel1, ErrorUpdatePanel);
    //        INT_ModulokService service = eIntegratorService.ServiceFactory.GetINT_ModulokService();

    //        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
    //        Result res = new Result();
    //        Result resError = new Result();
    //        int errorCount = 0;
    //        for (int i = 0; i < selectedItemsList.Count; i++)
    //        {
    //            xpm.Record_Id = selectedItemsList[i];
    //            res = service.Revalidate(xpm);
    //            if (res.IsError)
    //            {
    //                resError = res;
    //                errorCount++;
    //            }
    //        }

    //        if (errorCount > 0)
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
    //            if (errorCount > 1)
    //            {
    //                EErrorPanel1.Header = EErrorPanel1.Header + " (Hibák száma: " + errorCount.ToString() + ")";
    //            }
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    #endregion

    #region Mapping ListHeader Function Button Events

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedModul();
            INTModulokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedModulRecords();
                INTModulokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedPartnerRecords();
                INTModulokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedPartnerek();
                break;
        }
    }


    private void LockSelectedModulRecords()
    {
        LockManager.LockSelectedGridViewRecords(INTModulokGridView, "INT_Modulok"
             , "INTModulLock", "INTModulLock"
              , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedPartnerRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(INTModulokGridView, "INT_Modulok"
             , "INTModulLock", "INTModulLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void SendMailSelectedPartnerek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(INTModulokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "INT_Modulok");
        }
    }

    #endregion

    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(INTModulokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (INTModulokGridView.SelectedIndex == -1)
        {
            return;
        }

        string PartnerId = UI.GetGridViewSelectedRecordId(INTModulokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, PartnerId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string PartnerId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                INTParameterekGridViewBind(PartnerId);
                INTParameterekPanel.Visible = true;
                break;
            case 1:
                INTLogGridViewBind(PartnerId);
                INTLogPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(INTParameterekGridView);
                break;
            case 1:
                ui.GridViewClear(INTLogGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        INTParameterekSubListHeader.RowCount = RowCount;
        INTLogSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(INTParameterekTabPanel))
        {
            INTParameterekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(INTParameterekGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(INTLogTabPanel))
        {
            INTLogGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(INTLogGridView));
        }
    }




    #endregion

    #region INT_Parameterek

    //Data Binding(2)
    protected void INTParameterekGridViewBind(string parameterId)
    {
        string defaultSortExpression = "Nev";
        String sortExpression = Search.GetSortExpressionFromViewState("INTParameterekGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("INTParameterekGridView", ViewState);

        INTParameterekGridViewBind(parameterId, sortExpression, sortDirection);
    }

    protected void INTParameterekGridViewBind(string modulId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(modulId))
        {
            INT_ParameterekService service = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            INT_ParameterekSearch search = new INT_ParameterekSearch();

            search.Modul_id.Value = modulId;
            search.Modul_id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("INTParameterekGridView", ViewState, SortExpression, SortDirection);

            INTParameterekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAll(execParam, search);

            UI.GridViewFill(INTParameterekGridView, res, INTParameterekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerINTParameterek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(INTParameterekGridView);
        }
        else
        {
            ui.GridViewClear(INTParameterekGridView);
        }
    }

    //SubListHeader függvényei(4)
    private void INTParameterekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedParameterek();
            INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
        }
    }

    private void deleteSelectedParameterek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "INTParameterInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(INTParameterekGridView, EErrorPanel1, ErrorUpdatePanel);
            INT_ParameterekService service = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void INTParameterekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
    }

    private void INTParameterekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(INTParameterekSubListHeader.RowCount);
        INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
    }

    //GridView eseménykezelõi(3-4)
    //specialis, nem mindig kell kell
    protected void INTParameterekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok beállítása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            SetCheckBox(cbModosithato, drw["Karbantarthato"].ToString());

        }
    }

    protected void INTParameterekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(INTParameterekGridView, selectedRowNumber, "check");
        }
    }

    protected void INTParameterekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = INTParameterekGridView.PageIndex;

        INTParameterekGridView.PageIndex = INTParameterekSubListHeader.PageIndex;

        if (prev_PageIndex != INTParameterekGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, INTParameterekCPE);
            INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(INTParameterekSubListHeader.Scrollable, INTParameterekCPE);
        }

        INTParameterekSubListHeader.PageCount = INTParameterekGridView.PageCount;
        INTParameterekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(INTParameterekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(INTParameterekGridView);
    }

    protected void INTParameterekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView)
             , e.SortExpression, UI.GetSortToGridView("INTParameterekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel eseménykezelõi(1)
    protected void INTParameterekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(INTParameterekTabPanel))
                    //{
                    //    INTParameterekGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
                    //}
                    ActiveTabRefreshDetailList(INTParameterekTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek frissítése(1)
    private void INTParameterekGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            INTParameterekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("INTParameterekForm.aspx"
              , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
              , Defaults.PopupWidth, Defaults.PopupHeight, INTParameterekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            INTParameterekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("INTParameterekForm.aspx"
                 , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, INTParameterekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    #endregion INT_Parameterek

    #region INT_Log

    //Data Binding(2)
    protected void INTLogGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "Sync_StartDate, Parancs";
        String sortExpression = Search.GetSortExpressionFromViewState("INTLogGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("INTLogGridView", ViewState);

        INTLogGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void INTLogGridViewBind(string modulId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(modulId))
        {
            INT_LogService service = eIntegratorService.ServiceFactory.GetINT_LogService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            INT_LogSearch search = new INT_LogSearch();
            search.Modul_id.Value = modulId;
            search.Modul_id.Operator = Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("INTLogGridView", ViewState, SortExpression, SortDirection);

            INTLogSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAll(execParam, search);

            UI.GridViewFill(INTLogGridView, res, INTLogSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerINTLog);

            ui.SetClientScriptToGridViewSelectDeSelectButton(INTLogGridView);
        }
        else
        {
            ui.GridViewClear(INTLogGridView);
        }
    }

    //SubListHeader függvényei(4)
    private void INTLogSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        INT_LogService service = eIntegratorService.ServiceFactory.GetINT_LogService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        string selectedLogRecordId = UI.GetGridViewSelectedRecordId(INTLogGridView);
        execParam.Record_Id = selectedLogRecordId;

        Result res = service.Delete(execParam);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        }
        else
        {
            
        }

    }

    private void INTLogSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        INTLogGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
    }

    private void INTLogSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(INTLogSubListHeader.RowCount);
        INTLogGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
    }

    //GridView eseménykezelõi(3-4)
    //specialis, nem mindig kell kell
    protected void INTLogGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelStatus = (Label)e.Row.FindControl("labelStatus");

            DataRowView drw = (DataRowView)e.Row.DataItem;
            string status = drw["Status"] != null ? drw["Status"].ToString() : null;
            labelStatus.Text = drw["Status"] != null ? GetAllapot(drw["Status"].ToString()) : null;
        }
    }



    protected void INTLogGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(INTLogGridView, selectedRowNumber, "check");
        }
        if (e.CommandName == "kuldes")
        {
            int index = INTModulokGridView.SelectedIndex;
            if (index != -1)
            {
                INT_ASPDWH_FileService service = eIntegratorService.ServiceFactory.GetINT_ASPDWH_FileService();
                string modulId = INTModulokGridView.DataKeys[index].Value.ToString();
                string result = null;
                if (modulId == "2FA6B0F7-7C8F-40A9-ABFD-33F327E01F0E") //ügyirat
                {
                    result = service.SyncUgyirat();
                }
                else if (modulId == "73F2AC45-7127-43F4-B48D-782D3C489F9D") //statisztika
                {
                    result = service.SyncStatisztika();
                }
                if (!string.IsNullOrEmpty(result))
                {                    
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", result);
                }
            }

        }
    }

    protected void INTLogGridView_DataBound(object sender, EventArgs e)
    {
        if (INTLogGridView.Rows.Count > 0)
        {
            int index = INTModulokGridView.SelectedIndex;
            if (index != -1)
            {
                string modulId = INTModulokGridView.DataKeys[index].Value.ToString();
                DataRow lastLog = GetLastLog(modulId);

                if (lastLog != null)
                {
                    string logId = lastLog["Id"].ToString();

                    int foundIndex = -1;

                    for (int i = 0; i < INTLogGridView.DataKeys.Count; i++)
                    {
                        if (INTLogGridView.DataKeys[i].Value.ToString() == logId)
                        {
                            foundIndex = i;
                            break;
                        }
                    }

                    if (foundIndex > -1)
                    {
                        var row = INTLogGridView.Rows[foundIndex];
                        string status = lastLog["Status"].ToString();
                        //DataRowView drw = (DataRowView)row.DataItem;
                        //string status = drw["Status"] != null ? drw["Status"].ToString() : null;
                        Button buttonKuldes = (Button)row.FindControl("buttonKuldes");
                        buttonKuldes.Visible = status == "9" ? true : false;
                    }
                }
            }
        }
    }

    private DataRow GetLastLog(string modulId)
    {
        ExecParam logExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        INT_LogSearch search = new INT_LogSearch();
        search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;
        search.Modul_id.Value = modulId;
        search.ExternalId.Value = string.Empty;
        search.ExternalId.Operator = Contentum.eQuery.Query.Operators.notnull;
        search.OrderBy = " ModositasIdo DESC ";
        search.TopRow = 1;

        Result result = eIntegratorService.ServiceFactory.GetINT_LogService().GetAll(logExecParam, search);

        if (!result.IsError && result.Ds.Tables[0].Rows.Count > 0)
        {
            return result.Ds.Tables[0].Rows[0];
        }

        return null;
    }

    protected void INTLogGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = INTLogGridView.PageIndex;

        INTLogGridView.PageIndex = INTLogSubListHeader.PageIndex;

        if (prev_PageIndex != INTLogGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, INTLogCPE);
            INTLogGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(INTLogSubListHeader.Scrollable, INTLogCPE);
        }

        INTLogSubListHeader.PageCount = INTLogGridView.PageCount;
        INTLogSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(INTLogGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(INTLogGridView);
    }

    protected void INTLogGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        INTLogGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView)
             , e.SortExpression, UI.GetSortToGridView("INTLogGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel eseménykezelõi(1)
    protected void INTLogUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(INTLogTabPanel))
                    //{
                    //    INTLogGridViewBind(UI.GetGridViewSelectedRecordId(INTModulokGridView));
                    //}
                    ActiveTabRefreshDetailList(INTLogTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek frissítése(1)
    private void INTLogGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            INTLogSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("INTLogForm.aspx"
              , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
              , Defaults.PopupWidth, Defaults.PopupHeight, INTLogUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    #endregion

    private string GetAllapot(string kod)
    {
        switch (kod)
        {
            case "0":
                return "Folyamatban";
            case "1":
                return "Sikeres";
            default:
                return "Hiba";
        }
    }


}
