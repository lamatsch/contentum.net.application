<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="INTModulokSearch.aspx.cs" Inherits="INTModulokSearch" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="INTModulok list�ja" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr id="trModulNev" runat="server" class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textNev" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>                                                                                                
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelLeiras" runat="server" Text="Le�r�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textLeiras" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="padding-left: 20px;">
                                        <asp:Label ID="labelParancs" runat="server" Text="Parancs:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textParancs" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>                                
                            </tbody>
                        </table>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapMezo" colspan="2" style="padding-left: 60px">
                                        <uc3:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server"></uc3:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>                                
                                <tr class="urlapSor">
                                    <td colspan="2" style="padding-left: 90px">
                                        <div style="height: 3px"></div>
                                        <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                    </td>
                                </tr>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

