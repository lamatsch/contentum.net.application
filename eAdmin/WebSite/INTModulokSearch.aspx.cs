using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class INTModulokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(INT_ModulokSearch);
  
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        SearchHeader1.ButtonsClick +=
             new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
             new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            INT_ModulokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (INT_ModulokSearch)Search.GetSearchObject(Page, new INT_ModulokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            LoadComponentsFromSearchObject(searchObject);
        }

    }


    /// <summary>
    /// Form komponensek feltöltése a keresési objektumból
    /// </summary>
    /// 

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        LoadComponentsFromSearchObject(searchObject, false);
    }

    private void LoadComponentsFromSearchObject(object searchObject, bool resetState)
    {
        INT_ModulokSearch _INT_ModulokSearch = (INT_ModulokSearch)searchObject;

        if (_INT_ModulokSearch != null)
        {            
            textNev.Text = _INT_ModulokSearch.Nev.Value;
            textLeiras.Text = _INT_ModulokSearch.Leiras.Value;
            textParancs.Text= _INT_ModulokSearch.Parancs.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                _INT_ModulokSearch.ErvKezd, _INT_ModulokSearch.ErvVege);
        }
    }
      

    /// <summary>
    /// Keresési objektum létrehozása a form komponenseinek értékeiből
    /// </summary>
    private INT_ModulokSearch SetSearchObjectFromComponents()
    {
        INT_ModulokSearch _INT_ModulokSearch = (INT_ModulokSearch)SearchHeader1.TemplateObject;
        if (_INT_ModulokSearch == null)
        {
            _INT_ModulokSearch = new INT_ModulokSearch();
        }

        _INT_ModulokSearch.Nev.Value = textNev.Text;
        _INT_ModulokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _INT_ModulokSearch.Leiras.Value = textLeiras.Text;
        _INT_ModulokSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(textLeiras.Text);
        _INT_ModulokSearch.Parancs.Value = textParancs.Text;
        _INT_ModulokSearch.Parancs.Operator = Search.GetOperatorByLikeCharater(textParancs.Text);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(_INT_ModulokSearch.ErvKezd, _INT_ModulokSearch.ErvVege);

        return _INT_ModulokSearch;
    }
 
    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject(), true);
        }
        else if (e.CommandName == CommandName.Search)
        {
            INT_ModulokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }



    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
    }

    private INT_ModulokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        INT_ModulokSearch defaultSearchObject = new INT_ModulokSearch();
        return defaultSearchObject;
    }

  

}
