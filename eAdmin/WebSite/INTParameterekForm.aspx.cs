using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eIntegrator.Service;

public partial class INTParameterekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");

        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, No.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        INTModulokTextBox1.ReadOnly = true;        
        requiredTextBoxNev.ReadOnly = true;
        requiredTextBoxErtek.ReadOnly = true;
        //textRovidNev.ReadOnly= true;
        ddownKarbantarthato.ReadOnly = true;       
        ErvenyessegCalendarControl1.ReadOnly = true;


        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelINTModul.CssClass = "mrUrlapInputWaterMarked";                
        labelErtek.CssClass = "mrUrlapInputWaterMarked";        
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelKarbantarthato.CssClass = "mrUrlapInputWaterMarked";        

    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        INTModulokTextBox1.ReadOnly = true;        

        labelINTModul.CssClass = "mrUrlapInputWaterMarked";
        labelINTModul.CssClass = "mrUrlapInputWaterMarked";;
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "INTParameter" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                INT_ParameterekService service = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    INT_Parameterek int_Parameterek = (INT_Parameterek)result.Record;
                    LoadComponentsFromBusinessObject(int_Parameterek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

            }
        }
        else if (Command == CommandName.New)
        {
            string intModulID = Request.QueryString.Get("KodcsoportID");
            if (!String.IsNullOrEmpty(intModulID))
            {
                INTModulokTextBox1.Id_HiddenField = intModulID;
                INTModulokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
                INTModulokTextBox1.Enabled = false;
            }
            BoolString.FillDropDownList(ddownKarbantarthato);
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="int_Parameterek"></param>
    private void LoadComponentsFromBusinessObject(INT_Parameterek int_Parameterek)
    {   
        INTModulokTextBox1.Id_HiddenField = int_Parameterek.Modul_id;
        INTModulokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        requiredTextBoxErtek.Text = int_Parameterek.Ertek;
        requiredTextBoxNev.Text = int_Parameterek.Nev;
        BoolString.FillDropDownList(ddownKarbantarthato, int_Parameterek.Karbantarthato);
        ErvenyessegCalendarControl1.ErvKezd = int_Parameterek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = int_Parameterek.ErvVege;


        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = int_Parameterek.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(int_Parameterek.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private INT_Parameterek GetBusinessObjectFromComponents()
    {
        INT_Parameterek int_Parameterek = new INT_Parameterek();
        int_Parameterek.Updated.SetValueAll(false);
        int_Parameterek.Base.Updated.SetValueAll(false);

        int_Parameterek.Modul_id = INTModulokTextBox1.Id_HiddenField;
        int_Parameterek.Updated.Modul_id = pageView.GetUpdatedByView(INTModulokTextBox1);
        
        int_Parameterek.Ertek = requiredTextBoxErtek.Text;
        int_Parameterek.Updated.Ertek = pageView.GetUpdatedByView(requiredTextBoxErtek);
        int_Parameterek.Nev = requiredTextBoxNev.Text;
        int_Parameterek.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        int_Parameterek.Karbantarthato = ddownKarbantarthato.SelectedValue;
        int_Parameterek.Updated.Karbantarthato = pageView.GetUpdatedByView(ddownKarbantarthato);

        ErvenyessegCalendarControl1.SetErvenyessegFields(int_Parameterek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        int_Parameterek.Base.Ver = FormHeader1.Record_Ver;
        int_Parameterek.Base.Updated.Ver = true;

        return int_Parameterek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(INTModulokTextBox1);
            compSelector.Add_ComponentOnClick(requiredTextBoxErtek);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);            
            compSelector.Add_ComponentOnClick(ddownKarbantarthato);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "INTParameter" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            INT_ParameterekService service = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
                            INT_Parameterek int_Parameterek = GetBusinessObjectFromComponents();

                            if (int_Parameterek == null)
                                return;

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, int_Parameterek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {       
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                INT_ParameterekService service = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
                                INT_Parameterek int_Parameterek = GetBusinessObjectFromComponents();

                                if (int_Parameterek == null)
                                    return;

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, int_Parameterek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {                                  
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page,true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
