﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.AdoszamError = {};
Utility.AdoszamError.badVektorFormat = 'Az ellenőrző vektor formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.AdoszamError.badAdoszamInputFormat = 'Az adószám formátuma nem megfelelő!\n(8+1+2 kötőjellel (-) elválasztott számjegyet kell tartalmaznia, a 9. jegy csak 1 és 5 között lehet)';
Utility.AdoszamError.badAdoszamInputFormatEightCharsFormatEnabled = 'Az adószám formátuma nem megfelelő!\n(8 számjegyet, vagy 8+1+2 kötőjellel (-) elválasztott számjegyet kell tartalmaznia, a 9. jegy csak 1 és 5 között lehet)';
Utility.AdoszamError.badAdoszamFormat = 'Az adószám formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.AdoszamError.badAdoszamLength = 'Az adószám hossza ({0}) nem megfelelő!\n(A kívánt hossz: {1})';
Utility.AdoszamError.chekSumError = 'Az adószám ellenőrző összege hibás!';

Utility.AdoszamBehavior = function(element) {
    Utility.AdoszamBehavior.initializeBase(this, [element]);

    //Properties
    this.partnerBehaviorId = null;
    this.checkVektor = null;
    this.checksumValidatorId = null;
    this.checksumValidatorCalloutId = null;
    this.checkAdoszam = true;
    this.adoszamHiddenFieldId = null;
    this.kulfoldiAdoszamCheckBoxId = null;
    this.userId = null;
    this.loginId = null;
    this.isEightCharsFormatEnabled = true; // adószám csak 8 karakterrel engedélyezett-e
    //Variables
    this._partnerBehavior = null;
    this._adoszamHiddenField = null;
    this._adoszamLength = -1;
    this._checksumValidator = null;
    this._checksumValidatorCallout = null;
    this._kulfoldiAdoszamCheckBox = null;
    this._keyUpHandler = null;
    this._keyDownHandler = null;
    this._textChangedHandler = null;
    this._checkedChangedHandler = null;
    this._checksumValidationHandler = null;
    this._partnerChangedHandler = null;
}

Utility.AdoszamBehavior.prototype =
{

    initialize: function() {
        Utility.AdoszamBehavior.callBaseMethod(this, 'initialize');

        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);

        this._keyUpHandler = Function.createDelegate(this, this._OnKeyUp);
        this._keyDownHandler = Function.createDelegate(this, this._OnKeyDown);
        //this._partnerChangedHandler = Function.createDelegate(this, this._OnPartnerChanged);
        this._checkedChangedHandler = Function.createDelegate(this, this._OnCheckedChanged);

        var element = this.get_element();
        if (element) {
            $addHandler(element, "keyup", this._keyUpHandler);
            $addHandler(element, "keydown", this._keyDownHandler);
        }

        if (this.checkAdoszam) {

            //            this._textChangedHandler = Function.createDelegate(this, this._OnTextChanged);
            // 
            //            if(element)
            //            {
            //                $addHandler(element, "change", this._textChangedHandler);
            //            }

        }

        if (this.checksumValidatorId) {
            this._checksumValidator = $get(this.checksumValidatorId);

            if (this._checksumValidator) {
                this._checksumValidationHandler = Function.createDelegate(this, this._ChecksumValidate);
                this._checksumValidator.evaluationfunction = this._checksumValidationHandler;
            }
        }

        if (this.checkVektor) {
            this._adoszamLength = this.checkVektor.length + 1;
        }

        if (this.adoszamHiddenFieldId) {
            this._adoszamHiddenField = $get(this.adoszamHiddenFieldId);
        }
    },

    _OnApplicationLoad: function() {
        this._partnerChangedHandler = Function.createDelegate(this, this._OnPartnerChanged);

        if (this.partnerBehaviorId) {
            this._partnerBehavior = $find(this.partnerBehaviorId);
        }
        if (this._partnerBehavior) {
            this._partnerBehavior.add_hiddenfieldValueChanged(this._partnerChangedHandler);
        }

        if (this.kulfoldiAdoszamCheckBoxId) {
            this._kulfoldiAdoszamCheckBox = $get(this.kulfoldiAdoszamCheckBoxId);
            if (this._kulfoldiAdoszamCheckBox && this._checkedChangedHandler) {
                $addHandler(this._kulfoldiAdoszamCheckBox, "change", this._checkedChangedHandler);
            }
        }
    },

    dispose: function() {

        var element = this.get_element();

        if (element) {
            if (this._keyUpHandler) {
                $removeHandler(element, "keyup", this._keyUpHandler);
            }
            this._keyUpHandler = null;
            if (this._textChangedHandler) {
                $removeHandler(element, "change", this._textChangedHandler);
            }
            this._textChangedHandler = null;
            if (this._keyDownHandler) {
                $removeHandler(element, "keydown", this._keyDownHandler);
            }
            this._keyDownHandler = null;

            if (this._checksumValidator) {
                this._checksumValidator.evaluationfunction = null;
                this._checksumValidator.errormessage = null;
                this._checksumValidator = null;
            }

            this._checksumValidationHandler = null;

            this._checksumValidatorCallout = null;

            if (this._partnerChangedHandler && this._partnerBehavior) {
                this._partnerBehavior.remove_hiddenfieldValueChanged(this._partnerChangedHandler);
            }

            this._partnerBehavior = null;
            this._partnerChangedHandler = null;
        }

        if (this._kulfoldiAdoszamCheckBox && this._checkedChangedHandler) {
            $removeHandler(this._kulfoldiAdoszamCheckBox, "change", this._checkedChangedHandler);
        }

        Utility.AdoszamBehavior.callBaseMethod(this, 'dispose');
    },


    _OnKeyUp: function(ev) {
        var element = this.get_element();
        var text = element.value;
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k && k > 88 && k < 193) {
            var index = text.length - 1;
            switch (k) {
                case 192: //Ö
                    element.value = text.substring(0, index) + '0';
                    break;
                case 89: //Y
                    element.value = text.substring(0, index);
                    if (ev.shiftKey) element.value += 'Z';
                    else element.value += 'z';
                    break;
                case 90: //Z
                    element.value = text.substring(0, index);
                    if (ev.shiftKey) element.value += 'Y';
                    else element.value += 'y';
                    break;
                case 191: //Ü
                    element.value = text.substring(0, index) + '-';
                    break;
            }
        }

        this.raiseKeyUp(ev);
    },

    _OnKeyDown: function(ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k == Sys.UI.Key.enter) {
            return false;
        }
    },

    _OnTextChanged: function(ev) {
        if (this.checkAdoszam) {
            this.AlertAdoszamCheckError();
        }
    },

    _OnPartnerChanged: function(sender, args) {

        if (args && args != '') {
            var servicePath = "WrappedWebService/Ajax.asmx";
            var serviceMethod = "GetAdoszamByPartner";

            var params = { partnerId: args, userId: this.userId, loginId: this.loginId };

            // Invoke the web service
            Sys.Net.WebServiceProxy.invoke(servicePath, serviceMethod, false, params,
                                    Function.createDelegate(this, this.OnWsCallBack_GetAdoszamByPartnerSuccess),
                                    Function.createDelegate(this, this.OnWsCallBack_GetAdoszamByPartnerError), null);
        }
        else {
            this.OnWsCallBack_GetAdoszamByPartnerSuccess('');
        }
    },

    OnWsCallBack_GetAdoszamByPartnerSuccess: function(result) {
        var element = this.get_element();
        if (this._adoszamHiddenField && element) {
            if (result) {
                var adoszam = result[0];
                var isKulfoldi = false;
                if (result.length > 0 && result[1] == '1') {
                    isKulfoldi = true;
                }

                if (this._kulfoldiAdoszamCheckBox) {
                    this._kulfoldiAdoszamCheckBox.checked = isKulfoldi;
                }

                element.value = adoszam;
                this._adoszamHiddenField.value = result.join(';');
            }
            else {
                element.value = '';
                this._adoszamHiddenField.value = '';
            }
        }
    },

    OnWsCallBack_GetAdoszamByPartnerError: function(error) {
        alert(error.get_message());
        //AjaxLogging.ExceptionManager.getInstance().publishException(ERROR_CODE_WEB_SERVICE, error);
        this.OnWsCallBack_GetAdoszamByPartnerSuccess('');
    },

    _OnCheckedChanged: function(sender, args) {
        var element = this.get_element();
        if (element) {
            $common.tryFireEvent(element, 'change');
        }
    },

    AlertAdoszamCheckError: function() {
        var element = this.get_element();
        var check = this.CheckAdoszam(element.value);
        if (check != 1) {
            alert(check);
            element.value = '';
            return 0;
        }
        return 1;
    },

    _ChecksumValidate: function(sender, args) {
        var element = this.get_element();
        var adoszam = element.value;
        var isKulfoldi = false;

        if (this._kulfoldiAdoszamCheckBox != null) {
            isKulfoldi = this._kulfoldiAdoszamCheckBox.checked;
        }

        var check = this.CheckAdoszam(adoszam, isKulfoldi);

        if (check == 1) {
            return true;
        }
        else {
            this._SetValidatorErrorMessage(this._checksumValidator, check);
            return false;
        }
    },

    _SetValidatorErrorMessage: function(validator, message) {
        if (!validator && !message)
            return;

        validator.errormessage = message;
        if (validator.firstChild)
            validator.firstChild.nodeValue = message;

        this._FindValidatorCallout();

        if (this._checksumValidatorCallout) {
            if (this._checksumValidatorCallout._errorMessageCell) {
                this._checksumValidatorCallout._errorMessageCell.innerHTML = message.replace(/\n/g, '<br/>');
            }
        }
    },

    _FindValidatorCallout: function() {
        if (!this._checksumValidatorCallout) {
            if (this.checksumValidatorCalloutId) {
                this._checksumValidatorCallout = $find(this.checksumValidatorCalloutId);
            }
        }
    },

    CheckAdoszam: function(adoszam, isKulfoldi) {

        if (adoszam.trim() == '') return 1;

        if (isKulfoldi) return 1;

        arrayAdoszam = adoszam.split('-');

        var adoszamPattern = /^(\d{8})(-[1-5]-\d{2})$/g;
        if (this.isEightCharsFormatEnabled) {
            adoszamPattern = /^(\d{8})(-[1-5]-\d{2}|)$/g;
        }

        var checkRegexp = new RegExp(adoszamPattern);
        var checkRegexpResult = checkRegexp.exec(adoszam);

        if (!checkRegexpResult) {
            if (this.isEightCharsFormatEnabled) {
                return Utility.AdoszamError.badAdoszamInputFormatEightCharsFormatEnabled;
            }
            else {
                return Utility.AdoszamError.badAdoszamInputFormat;
            }
        }

        adoszam1 = checkRegexpResult[1];

        var vektor = this.checkVektor;
        if (vektor != null) {
            if (isNaN(parseInt(vektor))) {
                //rossz vektor
                return Utility.AdoszamError.badVektorFormat;
            }

            if (isNaN(parseInt(adoszam1))) {
                //rossz adoszam
                return Utility.AdoszamError.badAdoszamFormat;
            }

            var length = vektor.length;
            if (adoszam1.length != this._adoszamLength) {
                //hibas adoszam hossz
                return String.format(Utility.AdoszamError.badAdoszamLength, adoszam1.length, this._adoszamLength);
            }

            var checkSum = parseInt(adoszam.charAt(length));

            if (isNaN(checkSum)) {
                return Utility.AdoszamError.badAdoszamFormat;
            }

            var c = 0;

            for (var i = 0; i < length; i++) {
                var a = parseInt(adoszam.charAt(i));
                if (isNaN(a)) return Utility.AdoszamError.badAdoszamFormat;
                var b = parseInt(vektor.charAt(i));
                if (isNaN(b)) return Utility.AdoszamError.badVektorFormat;
                c = c + a * b;
            }

            //Adószám ellenőrző szám (8. jegy) képzése: jegy(8) = 10 - MOD10 (SUM (jegy(i)*vekt(i))) 
            // (i=1 - 7)  vekt=9731973
            c = (10 - (c % 10)) % 10;

            if (checkSum == c) {
                return 1;
            }
            else {
                return Utility.AdoszamError.chekSumError;
            }
        }

        return 1;
    },

    //
    // Behavior properties
    //
    get_PartnerBehaviorId: function() {
        return this.partnerBehaviorId;
    },

    set_PartnerBehaviorId: function(value) {
        if (this.partnerBehaviorId !== value) {
            this.partnerBehaviorId = value;
            this.raisePropertyChanged('PartnerBehaviorId');
        }
    },

    get_AdoszamHiddenFieldId: function() {
        return this.adoszamHiddenFieldId;
    },

    set_AdoszamHiddenFieldId: function(value) {
        if (this.adoszamHiddenFieldId !== value) {
            this.adoszamHiddenFieldId = value;
            this.raisePropertyChanged('AdoszamHiddenFieldId');
        }
    },

    get_KulfoldiAdoszamCheckBoxId: function() {
        return this.kulfoldiAdoszamCheckBoxId;
    },

    set_KulfoldiAdoszamCheckBoxId: function(value) {
        if (this.kulfoldiAdoszamCheckBoxId !== value) {
            this.kulfoldiAdoszamCheckBoxId = value;
            this.raisePropertyChanged('KulfoldiAdoszamCheckBoxId');
        }
    },

    get_CheckVektor: function() {
        return this.checkVektor;
    },

    set_CheckVektor: function(value) {
        if (this.checkVektor !== value) {
            this.checkVektor = value;
            this.raisePropertyChanged('CheckVektor');
        }
    },

    get_CheckAdoszam: function() {
        return this.checkAdoszam;
    },

    set_CheckAdoszam: function(value) {
        if (this.checkAdoszam !== value) {
            this.checkAdoszam = value;
            this.raisePropertyChanged('CheckAdoszam');
        }
    },

    get_ChecksumValidatorId: function() {
        return this.checksumValidatorId;
    },

    set_ChecksumValidatorId: function(value) {
        if (this.checksumValidatorId !== value) {
            this.checksumValidatorId = value;
            this.raisePropertyChanged('ChecksumValidatorId');
        }
    },

    get_ChecksumValidatorCalloutId: function() {
        return this.checksumValidatorCalloutId;
    },

    set_ChecksumValidatorCalloutId: function(value) {
        if (this.checksumValidatorCalloutId !== value) {
            this.checksumValidatorCalloutId = value;
            this.raisePropertyChanged('ChecksumValidatorCalloutId');
        }
    },


    get_UserId: function() {
        return this.userId;
    },

    set_UserId: function(value) {
        if (this.userId !== value) {
            this.userId = value;
            this.raisePropertyChanged('UserId');
        }
    },

    get_LoginId: function() {
        return this.loginId;
    },

    set_LoginId: function(value) {
        if (this.loginId !== value) {
            this.loginId = value;
            this.raisePropertyChanged('LoginId');
        }
    },

    get_IsEightCharsFormatEnabled: function() {
        return this.isEightCharsFormatEnabled;
    },

    set_IsEightCharsFormatEnabled: function(value) {
        if (this.isEightCharsFormatEnabled !== value) {
            this.isEightCharsFormatEnabled = value;
            this.raisePropertyChanged('IsEightCharsFormatEnabled');
        }
    },

    //
    // Events
    //

    add_keyUp: function(handler) {

        this.get_events().addHandler('keyUp', handler);
    },

    remove_keyUp: function(handler) {

        this.get_events().removeHandler('keyUp', handler);
    },

    raiseKeyUp: function(eventArgs) {

        var handler = this.get_events().getHandler('keyUp');
        if (handler) {
            handler(this, eventArgs);
        }
    }
}

Utility.AdoszamBehavior.registerClass('Utility.AdoszamBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();