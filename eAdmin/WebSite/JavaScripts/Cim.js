﻿// JScript File
///<reference name="MicrosoftAjax.js" />
///<reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.CimManager = function(element) {
    Utility.CimManager.initializeBase(this, [element]);

    //Properties
    this.orszagTextBoxClientId = '';
    this.iranyitoszamTextBoxClientId = '';
    this.iranyitoszamAutoCompleteClientId = '';
    this.telepulesTextBoxClientId = '';
    this.telepulesAutoCompleteClientId = '';
    this.kozteruletTextBoxClientId = '';
    this.kozteruletTipusTextBoxClientId = '';
    this.tobbiTextBoxClientId = '';
    this.cimTextBoxClientId = '';
    this.cimHiddenFieldClientId = '';
    // BUG_4731
    this.cimTextBoxCloneClientId = '';
    this.cimHiddenFieldCloneClientId = '';

    //Variables
    this.orszagTextBox = null;
    this.prevOrszag = '';
    this.iranyitoszamTextBox = null;
    this.iranyitoszamDropDrownList = null;
    this.isIranyitoszamDropDownListActive = false;
    this.iranyitoszamAutoComplete = null;
    this.prevIranyitoszam = '';
    this.telepulesTextBox = null;
    this.telepulesDropDownList = null;
    this.isTelepulesDropDownListActive = false;
    this.telepulesAutoComplete = null;
    this.prevTelepules = '';
    this.kozteruletTextBox = null;
    this.kozteruletTipusTextBox = null;
    this.tobbiTextBox = null;
    this.iranyitoszamFocusHandler = null;
    this.telepulesFocusHandler = null;
    this.cimChangeHandler = null;
    this.changeHandler = null;
    this.applicationLoadHandler = null;
}

Utility.CimManager.prototype = {
    initialize: function() {
        Utility.CimManager.callBaseMethod(this, 'initialize');

        this.orszagTextBox = $get(this.orszagTextBoxClientId);
        this.iranyitoszamTextBox = $get(this.iranyitoszamTextBoxClientId);
        this.telepulesTextBox = $get(this.telepulesTextBoxClientId);
        this.kozteruletTextBox = $get(this.kozteruletTextBoxClientId);
        this.kozteruletTipusTextBox = $get(this.kozteruletTipusTextBoxClientId);
        this.tobbiTextBox = $get(this.tobbiTextBoxClientId);

        this.changeHandler = Function.createDelegate(this, this.OnChange);
        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        this.telepulesFocusHandler = Function.createDelegate(this, this.OnTelepulesFocus);
        this.iranyitoszamFocusHandler = Function.createDelegate(this, this.OnIranyitoszamFocus);
        this.cimChangeHandler = Function.createDelegate(this, this.OnCimChanged);

        if (this.iranyitoszamTextBox) {
            $addHandler(this.iranyitoszamTextBox, 'focus', this.iranyitoszamFocusHandler);
        }

        if (this.telepulesTextBox) {
            $addHandler(this.telepulesTextBox, 'focus', this.telepulesFocusHandler);
        }

        if (this.cimTextBoxClientId.trim() != '' && this.cimHiddenFieldClientId.trim() != '') {

            if (this.orszagTextBox) {
                $addHandler(this.orszagTextBox, 'change', this.changeHandler);
            }

            if (this.iranyitoszamTextBox) {
                $addHandler(this.iranyitoszamTextBox, 'change', this.changeHandler);
                this.iranyitoszamDropDrownList = document.createElement('select');
                var bounds = Sys.UI.DomElement.getBounds(this.iranyitoszamTextBox);
                if (this.iranyitoszamDropDrownList) {
                    this.iranyitoszamDropDrownList.style.width = bounds.width + 'px';
                    this.AddListSearchExtender(this.iranyitoszamDropDrownList);
                }
            }

            if (this.iranyitoszamDropDrownList) {
                $addHandler(this.iranyitoszamDropDrownList, 'change', this.changeHandler);
                $addHandler(this.iranyitoszamDropDrownList, 'focus', this.iranyitoszamFocusHandler);
            }

            if (this.telepulesTextBox) {
                $addHandler(this.telepulesTextBox, 'change', this.changeHandler);
                this.telepulesDropDownList = document.createElement('select');
                var bounds = Sys.UI.DomElement.getBounds(this.telepulesTextBox);
                if (this.telepulesDropDownList) {
                    this.telepulesDropDownList.style.width = bounds.width + 'px';
                    this.AddListSearchExtender(this.telepulesDropDownList);
                }
            }

            if (this.telepulesDropDownList) {
                $addHandler(this.telepulesDropDownList, 'change', this.changeHandler);
                $addHandler(this.telepulesDropDownList, 'focus', this.telepulesFocusHandler);
            }

            if (this.kozteruletTextBox) {
                $addHandler(this.kozteruletTextBox, 'change', this.changeHandler);
            }

            if (this.kozteruletTipusTextBox) {
                $addHandler(this.kozteruletTipusTextBox, 'change', this.changeHandler);
            }

            if (this.tobbiTextBox) {
                $addHandler(this.tobbiTextBox, 'change', this.changeHandler);
            }

            var cimTextBox = $get(this.cimTextBoxClientId);
            if (cimTextBox) {
                $addHandler(cimTextBox, 'change', this.cimChangeHandler);
            }
        }

        this.prevOrszag = this.GetOrszag();
        this.prevIranyitoszam = this.GetIranyitoszam();
        this.prevTelepules = this.GetTelepules();

        Sys.Application.add_load(this.applicationLoadHandler);
    },

    dispose: function() {
        if (this.iranyitoszamTextBox) {
            if (this.iranyitoszamFocusHandler != null) {
                $removeHandler(this.iranyitoszamTextBox, 'focus', this.iranyitoszamFocusHandler);
            }
        }

        if (this.telepulesTextBox) {
            if (this.telepulesFocusHandler != null) {
                $removeHandler(this.telepulesTextBox, 'focus', this.telepulesFocusHandler);
            }
        }


        if (this.cimTextBoxClientId.trim() != '' && this.cimHiddenFieldClientId.trim() != '') {

            if (this.orszagTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.orszagTextBox, 'change', this.changeHandler);
                }
            }
            if (this.iranyitoszamTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.iranyitoszamTextBox, 'change', this.changeHandler);
                }

            }

            if (this.iranyitoszamDropDrownList) {
                if (this.changeHandler != null) {
                    $removeHandler(this.iranyitoszamDropDrownList, 'change', this.changeHandler);
                }
                if (this.iranyitoszamFocusHandler != null) {
                    $removeHandler(this.iranyitoszamDropDrownList, 'focus', this.iranyitoszamFocusHandler);
                }
            }


            if (this.telepulesTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.telepulesTextBox, 'change', this.changeHandler);
                }
            }

            if (this.telepulesDropDownList) {
                if (this.changeHandler != null) {
                    $removeHandler(this.telepulesDropDownList, 'change', this.changeHandler);
                }
                if (this.telepulesFocusHandler != null) {
                    $removeHandler(this.telepulesDropDownList, 'focus', this.telepulesFocusHandler);
                }
            }

            if (this.kozteruletTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.kozteruletTextBox, 'change', this.changeHandler);
                }
            }

            if (this.kozteruletTipusTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.kozteruletTipusTextBox, 'change', this.changeHandler);
                }
            }

            if (this.tobbiTextBox) {
                if (this.changeHandler != null) {
                    $removeHandler(this.tobbiTextBox, 'change', this.changeHandler);
                }
            }

            var cimTextBox = $get(this.cimTextBoxClientId);
            if (cimTextBox) {
                if (this.cimChangeHandler != null) {
                    $removeHandler(cimTextBox, 'change', this.cimChangeHandler);
                }
            }
        }

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
        }

        this.orszagTextBox = null;
        this.iranyitoszamTextBox = null;
        this.iranyitoszamDropDrownList = null;
        this.iranyitoszamAutoComplete = null;
        this.telepulesTextBox = null;
        this.telepulesDropDownList = null;
        this.telepulesAutoComplete = null;
        this.kozteruletTextBox = null;
        this.kozteruletTipusTextBox = null;
        this.tobbiTextBox = null;

        this.changeHandler = null;
        this.applicationLoadHandler = null;
        this.telepulesFocusHandler = null;
        this.iranyitoszamFocusHandler = null;
        this.cimChangeHandler = null;

        Utility.CimManager.callBaseMethod(this, 'dispose');
    },

    AddListSearchExtender: function(dropDownList) {
        if (dropDownList && typeof (AjaxControlToolkit) != 'undefined' && AjaxControlToolkit.ListSearchBehavior != undefined) {
            $create(AjaxControlToolkit.ListSearchBehavior, { "promptCssClass": "ListSearchExtenderPrompt", "promptText": "", "queryPattern": 1, "queryTimeout": 2000 }, null, null, dropDownList);
        }
    },

    OnApplicationLoad: function() {
        this.iranyitoszamAutoComplete = $find(this.iranyitoszamAutoCompleteClientId);
        this.telepulesAutoComplete = $find(this.telepulesAutoCompleteClientId);

    },

    OnChange: function(sender, args) {
        if (this.cimTextBoxClientId.trim() != '' && this.cimHiddenFieldClientId.trim() != '') {
            var cimTextBox = $get(this.cimTextBoxClientId);
            var cimHiddenField = $get(this.cimHiddenFieldClientId);

            if (cimHiddenField) {
                cimHiddenField.value = '';
            }

            if (cimTextBox) {
                cimTextBox.value = this.GetFullCim();
            }

        }
        // BUG_4731
        if (this.cimTextBoxCloneClientId.trim() != '' && this.cimHiddenFieldCloneClientId.trim() != '') {
            var cimTextBoxClone = $get(this.cimTextBoxCloneClientId);
            var cimHiddenFieldClone = $get(this.cimHiddenFieldCloneClientId);

            if (cimHiddenFieldClone) {
                cimHiddenFieldClone.value = '';
            }

            if (cimTextBoxClone) {
                cimTextBoxClone.value = this.GetFullCim();
            }

        }
    },

    GetFullCim: function() {
        var orszag = this.GetOrszag();
        var irsz = this.GetIranyitoszam();
        var telepules = this.GetTelepules();
        var kozterulet = this.GetWatermarkedTextBoxValue(this.kozteruletTextBox);
        var kozteruletTipus = this.GetWatermarkedTextBoxValue(this.kozteruletTipusTextBox);
        var tobbi = this.GetWatermarkedTextBoxValue(this.tobbiTextBox);

        var cim = orszag;

        if (irsz != '') {
            if (cim != '') {
                cim += ' ';
            }

            cim += irsz;
        }

        if (telepules != '') {
            if (cim != '') {
                cim += ' ';
            }

            cim += telepules;
        }

        if (kozterulet != '') {
            if (cim != '') {
                cim += ' ';
            }

            cim += kozterulet;
        }

        if (kozteruletTipus != '') {
            if (cim != '') {
                cim += ' ';
            }

            cim += kozteruletTipus;
        }

        if (tobbi != '') {
            if (cim != '') {
                cim += ' ';
            }

            cim += tobbi;
        }

        return cim;
    },

    GetWatermarkedTextBoxValue: function(TextBox) {
        var text = '';
        if (TextBox) {
            text = AjaxControlToolkit.TextBoxWrapper.get_Wrapper(TextBox).get_Value().trim();
        }

        return text;
    },

    GetDropDownListSelectedText: function(DropDownList) {
        var text = '';
        if (DropDownList) {
            if (DropDownList.selectedIndex > -1) {
                text = DropDownList.options[DropDownList.selectedIndex].text;
            }
        }

        return text;
    },

    GetOrszag: function() {
        if (this.orszagTextBox) {
            return this.GetWatermarkedTextBoxValue(this.orszagTextBox);
        }

        return '';
    },

    GetIranyitoszam: function() {
        if (this.isIranyitoszamDropDownListActive) {
            return this.GetDropDownListSelectedText(this.iranyitoszamDropDrownList);
        }

        return this.GetWatermarkedTextBoxValue(this.iranyitoszamTextBox);
    },

    GetTelepules: function() {
        if (this.isTelepulesDropDownListActive) {
            return this.GetDropDownListSelectedText(this.telepulesDropDownList);
        }

        return this.GetWatermarkedTextBoxValue(this.telepulesTextBox);
    },

    SetWatermarkedTextBoxValue: function(TextBox, value) {
        if (TextBox) {
            AjaxControlToolkit.TextBoxWrapper.get_Wrapper(TextBox).set_Value(value.trim());
        }
    },

    OnTelepulesFocus: function(sender, args) {
        if (this.GetIranyitoszam() != this.prevIranyitoszam || this.GetOrszag() != this.prevOrszag) {
            this.prevIranyitoszam = this.GetIranyitoszam();
            this.prevOrszag = this.GetOrszag();
            if (this.telepulesAutoComplete) {
                var contextKey = this.telepulesAutoComplete.get_contextKey();
                contextKey = contextKey.split(';')[0];
                contextKey = contextKey + ';' + this.GetOrszag() + ';' + this.GetIranyitoszam();
                this.telepulesAutoComplete.set_contextKey(contextKey);

                //ha egy találat kitölti a települést
                if (this.GetIranyitoszam().length > 0 && !this.isIranyitoszamDropDownListActive) {
                    this.SetAutoCompleteFirstResult(this.telepulesAutoComplete);
                }
                else {
                    if (this.isTelepulesDropDownListActive) {
                        this._ReplaceElement(this.telepulesDropDownList, this.telepulesTextBox);
                        this.SetWatermarkedTextBoxValue(this.telepulesTextBox, this.GetDropDownListSelectedText(this.telepulesDropDownList));
                    }
                }
            }
        }
    },

    OnIranyitoszamFocus: function(sender, args) {
        if (this.GetTelepules() != this.prevTelepules) {
            this.prevTelepules = this.GetTelepules();
            if (this.iranyitoszamAutoComplete) {
                var contextKey = this.iranyitoszamAutoComplete.get_contextKey();
                contextKey = contextKey.split(';')[0];
                contextKey = contextKey + ';' + this.GetTelepules();
                this.iranyitoszamAutoComplete.set_contextKey(contextKey);
                //ha egy találat kitölti az irányítószámot
                if (this.GetTelepules().length > 0 && !this.isTelepulesDropDownListActive) {
                    this.SetAutoCompleteFirstResult(this.iranyitoszamAutoComplete);
                }
                else {
                    if (this.isIranyitoszamDropDownListActive) {
                        this._ReplaceElement(this.iranyitoszamDropDrownList, this.iranyitoszamTextBox);
                        this.SetWatermarkedTextBoxValue(this.iranyitoszamTextBox, this.GetDropDownListSelectedText(this.iranyitoszamDropDrownList));
                    }
                }
            }
        }
    },

    SetAutoCompleteFirstResult: function(extender) {
        var servicePath = extender.get_servicePath();
        var serviceMethod = extender.get_serviceMethod();
        var contextKey = extender.get_contextKey();

        var params = { prefixText: '', count: 1000, contextKey: contextKey };

        // Invoke the web service
        Sys.Net.WebServiceProxy.invoke(servicePath, serviceMethod, false, params,
                                    Function.createDelegate(this, this.OnMethodComplete),
                                    Function.createDelegate(this, this.OnMethodFailed), extender);
    },

    OnMethodComplete: function(result, context) {
        if (result && result.length && result.length > 0 && AjaxControlToolkit.AutoCompleteBehavior.isInstanceOfType(context)) {
            var extender = context;
            var text = null;
            var value = null;

            // Get the text/value for the item
            try {
                var pair = Sys.Serialization.JavaScriptSerializer.deserialize('(' + result[0] + ')');
                if (String.isInstanceOfType(pair)) {
                    // If the web service only returned a regular string, use it for
                    // both the text and the value
                    text = pair;
                    value = pair;
                } else {
                    // Use the text and value pair returned from the web service
                    text = pair ? pair.First : null;
                    value = pair ? pair.Second : null;
                }
            } catch (ex) {
                text = result[0];
                value = result[0];
            }

            if (result.length > 1) {
                //var minLength = extender.get_minimumPrefixLength();
                //text = text.substring(0, minLength);
                var element = extender.get_element();

                var ddList = this._GetDropDownList(element);

                this._FillDropDownList(ddList, result);

                if (!this._GetIsActiveDropDownList(ddList)) {
                    this._ReplaceElement(element, ddList);
                }

                $common.tryFireEvent(ddList, "change");
            }
            else {
                extender._currentPrefix = text;
                var element = extender.get_element();
                var ddList = this._GetDropDownList(element);
                if (this._GetIsActiveDropDownList(ddList)) {
                    this._ReplaceElement(ddList, element);
                }
                var control = element.control;
                if (control && control.set_text) {
                    control.set_text(text);
                    $common.tryFireEvent(control, "change");
                }
                else {
                    element.value = text;
                    $common.tryFireEvent(element, "change");
                }
            }
        }
    },


    OnMethodFailed: function(err, response, context) {
    },

    _ReplaceElement: function(source, dest) {
        if (source && dest) {
            var parent = source.parentNode;
            if (parent) {
                parent.replaceChild(dest, source);
            }

            var isDropDownActive = (source.tagName == 'INPUT');
            var dropDownList = (isDropDownActive) ? dest : source;
            this._SetIsActiveDropDownList(dropDownList, isDropDownActive);

            window.setTimeout(function() { dest.focus(); }, 1);

        }
    },

    _SetIsActiveDropDownList: function(dropDownList, value) {
        if (dropDownList == this.iranyitoszamDropDrownList) {
            this.isIranyitoszamDropDownListActive = value;
        }

        if (dropDownList == this.telepulesDropDownList) {
            this.isTelepulesDropDownListActive = value;
        }
    },

    _GetIsActiveDropDownList: function(dropDownList) {
        if (dropDownList == this.iranyitoszamDropDrownList) {
            return this.isIranyitoszamDropDownListActive;
        }

        if (dropDownList == this.telepulesDropDownList) {
            return this.isTelepulesDropDownListActive;
        }
    },

    _GetDropDownList: function(element) {
        var ddList;

        if (element == this.iranyitoszamTextBox) {
            ddList = this.iranyitoszamDropDrownList;
        }
        else {
            ddList = this.telepulesDropDownList;
        }

        return ddList;
    },

    _FillDropDownList: function(dropDownList, result) {
        if (dropDownList) {
            var prevSelectedText = this.GetDropDownListSelectedText(dropDownList);
            Utility.DomElement.Select.Clear(dropDownList);
            var emptyOption = document.createElement('option');
            emptyOption.text = '';
            emptyOption.value = '';
            Utility.DomElement.Select.Add(dropDownList, emptyOption);
            var selectedOption;
            if (result && result.length) {

                for (var i = 0; i < result.length; i++) {
                    // Create the item                
                    var option = document.createElement('option');

                    // Get the text/value for the item
                    try {
                        var pair = Sys.Serialization.JavaScriptSerializer.deserialize('(' + result[i] + ')');
                        if (pair && pair.First) {
                            // Use the text and value pair returned from the web service
                            option.text = pair.First;
                            option.value = pair.Second;
                        } else {
                            // If the web service only returned a regular string, use it for
                            // both the text and the value
                            option.text = pair;
                            option.value = pair;
                        }
                    } catch (ex) {
                        option.text = result[i];
                        option.value = result[i];
                    }

                    if (i == 0) {
                        option.selected = true;
                        selectedOption = option;
                    }
                    if (option.text == prevSelectedText) {
                        selectedOption.selected = false;
                        option.selected = true;
                        selectedOption = option;
                    }


                    Utility.DomElement.Select.Add(dropDownList, option);
                }

            }
        }

    },

    OnCimChanged: function(sender, args) {
        this.Clear();
    },

    Clear: function() {
        this.SetWatermarkedTextBoxValue(this.orszagTextBox, '');
        this.SetWatermarkedTextBoxValue(this.iranyitoszamTextBox, '');
        this.SetWatermarkedTextBoxValue(this.telepulesTextBox, '');
        this.SetWatermarkedTextBoxValue(this.kozteruletTextBox, '');
        this.SetWatermarkedTextBoxValue(this.kozteruletTipusTextBox, '');
        this.SetWatermarkedTextBoxValue(this.tobbiTextBox, '');
    },

    //getter, setter methods
    get_OrszagTextBoxClientId: function() {
        return this.orszagTextBoxClientId;
    },

    set_OrszagTextBoxClientId: function(value) {
        if (this.orszagTextBoxClientId != value) {
            this.orszagTextBoxClientId = value;
        }
    },

    get_IranyitoszamTextBoxClientId: function() {
        return this.iranyitoszamTextBoxClientId;
    },

    set_IranyitoszamTextBoxClientId: function(value) {
        if (this.iranyitoszamTextBoxClientId != value) {
            this.iranyitoszamTextBoxClientId = value;
        }
    },

    get_IranyitoszamAutoCompleteClientId: function() {
        return this.iranyitoszamAutoCompleteClientId;
    },

    set_IranyitoszamAutoCompleteClientId: function(value) {
        if (this.iranyitoszamAutoCompleteClientId != value) {
            this.iranyitoszamAutoCompleteClientId = value;
        }
    },

    get_TelepulesTextBoxClientId: function() {
        return this.telepulesTextBoxClientId;
    },

    set_TelepulesTextBoxClientId: function(value) {
        if (this.telepulesTextBoxClientId != value) {
            this.telepulesTextBoxClientId = value;
        }
    },

    get_TelepulesAutoCompleteClientId: function() {
        return this.telepulesAutoCompleteClientId;
    },

    set_TelepulesAutoCompleteClientId: function(value) {
        if (this.telepulesAutoCompleteClientId != value) {
            this.telepulesAutoCompleteClientId = value;
        }
    },

    get_KozteruletTextBoxClientId: function() {
        return this.kozteruletTextBoxClientId;
    },

    set_KozteruletTextBoxClientId: function(value) {
        if (this.kozteruletTextBoxClientId != value) {
            this.kozteruletTextBoxClientId = value;
        }
    },

    get_KozteruletTipusTextBoxClientId: function() {
        return this.kozteruletTipusTextBoxClientId;
    },

    set_KozteruletTipusTextBoxClientId: function(value) {
        if (this.kozteruletTipusTextBoxClientId != value) {
            this.kozteruletTipusTextBoxClientId = value;
        }
    },

    get_TobbiTextBoxClientId: function() {
        return this.tobbiTextBoxClientId;
    },

    set_TobbiTextBoxClientId: function(value) {
        if (this.tobbiTextBoxClientId != value) {
            this.tobbiTextBoxClientId = value;
        }
    },

    get_CimTextBoxClientId: function() {
        return this.cimTextBoxClientId;
    },

    set_CimTextBoxClientId: function(value) {
        if (this.cimTextBoxClientId != value) {
            this.cimTextBoxClientId = value;
        }
    },

    get_CimHiddenFieldClientId: function() {
        return this.cimHiddenFieldClientId;
    },

    set_CimHiddenFieldClientId: function(value) {
        if (this.cimHiddenFieldClientId != value) {
            this.cimHiddenFieldClientId = value;
        }
    },
    // BUG_4731
    get_CimTextBoxCloneClientId: function () {
        return this.cimTextBoxCloneClientId;
    },

    set_CimTextBoxCloneClientId: function (value) {
        if (this.cimTextBoxCloneClientId != value) {
            this.cimTextBoxCloneClientId = value;
        }
    },

    get_CimHiddenFieldCloneClientId: function () {
        return this.cimHiddenFieldCloneClientId;
    },

    set_CimHiddenFieldCloneClientId: function (value) {
        if (this.cimHiddenFieldCloneClientId != value) {
            this.cimHiddenFieldCloneClientId = value;
        }
    }

}

Utility.CimManager.registerClass('Utility.CimManager', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
