﻿// JScript File
var ReadOnlyComponents = new Array();
var DisabledComponents = new Array();
var HiddenField_readOnlyComponents_id;
var HiddenField_disabledComponents_id;
var Id_componentRights;

function initValues(str_readOnlyComponents,str_disabledComponents)
{   
    var temp1 = new Array();    
    if (str_readOnlyComponents!=null && str_readOnlyComponents!="")
    {
        temp1 = str_readOnlyComponents.split(',');
        var i=0;
        for (i=0;i<temp1.length;i++)
        {
            var component = document.getElementById(temp1[i]);
            if (component != null)
            {
                component.disabled = true;
            }
        }
    }
    if (str_disabledComponents!=null && str_disabledComponents!="")
    {
        var temp2 = new Array();
        temp2 = str_disabledComponents.split(',');
        var i=0;
        for (i=0;i<temp2.length;i++)
        {
            var component = document.getElementById(temp2[i]);
            if (component != null)
            {
                component.disabled = true;
                component.value = "";
            }
        }
    }
}
