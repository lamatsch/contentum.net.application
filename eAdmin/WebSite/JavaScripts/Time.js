﻿// JScript File
Type.registerNamespace("Utility");

Utility.Time = function (element) {
    Utility.Time.initializeBase(this, [element]);

    //Properties
    this.hourTextBoxClientId = '';
    this.minuteTextBoxClientId = '';
    //LZS - BLG_335 - másolandó óra és perc textbox client id-jai. Ezeket a descriptorban adjuk át.
    this.clonehourTextBoxClientId = '';
    this.cloneminuteTextBoxClientId = '';

    this.arrowUpClientId = '';
    this.arrowDownClientId = '';
    //Variables
    this.hourTextBox = null;
    this.minuteTextBox = null;

    //LZS - BLG_335 - másolandó óra és perc textbox értékeket tároló változók
    this.clonehourTextBox = null;
    this.cloneminuteTextBox = null;

    this.arrowUp = null;
    this.arrowDown = null;
    this.hourTextBoxFocusHandler = null;
    this.hourTextBoxKeyPressHandler = null;
    this.minuteTextBoxFocusHandler = null;
    this.minuteTextBoxKeyPressHandler = null;
    this.focusOnMinute = false;
    this.arrowUpMouseDownHandler = null;
    this.arrowUpFocusHandler = null;
    this.arrowDownMouseDownHandler = null;
    this.arrowDownFocusHandler = null;
    this.applicationLoadHandler = null;
}

Utility.Time.prototype = {
    initialize: function () {
        Utility.Time.callBaseMethod(this, 'initialize');

        this.hourTextBox = $get(this.hourTextBoxClientId);
        this.minuteTextBox = $get(this.minuteTextBoxClientId);

        //LZS - BLG_335 - ClientId-k beírása a változókba
        this.clonehourTextBox = $get(this.clonehourTextBoxClientId);
        this.cloneminuteTextBox = $get(this.cloneminuteTextBoxClientId);

        this.arrowUp = $get(this.arrowUpClientId);
        this.arrowDown = $get(this.arrowDownClientId);

        this.arrowUpMouseDownHandler = Function.createDelegate(this, this.OnArrowUpMouseDown);
        this.arrowUpFocusHandler = Function.createDelegate(this, this.OnArrowUpFocus);
        this.arrowDownMouseDownHandler = Function.createDelegate(this, this.OnArrowDownMouseDown);
        this.arrowDownFocusHandler = Function.createDelegate(this, this.OnArrowDownFocus);
        this.hourTextBoxFocusHandler = Function.createDelegate(this, this.OnHourTextBoxFocus);
        this.hourTextBoxKeyPressHandler = Function.createDelegate(this, this.OnHourKeyPress);
        this.minuteTextBoxFocusHandler = Function.createDelegate(this, this.OnMinuteTextBoxFocus);
        this.minuteTextBoxKeyPressHandler = Function.createDelegate(this, this.OnMinuteKeyPress);
        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);

        if (this.arrowUp != null) {
            $addHandler(this.arrowUp, 'mousedown', this.arrowUpMouseDownHandler);
            $addHandler(this.arrowUp, 'focus', this.arrowUpFocusHandler);

        }

        if (this.arrowDown != null) {
            $addHandler(this.arrowDown, 'mousedown', this.arrowDownMouseDownHandler);
            $addHandler(this.arrowDown, 'focus', this.arrowDownFocusHandler);

        }

        if (this.hourTextBox != null) {
            $addHandler(this.hourTextBox, 'focus', this.hourTextBoxFocusHandler);
            $addHandler(this.hourTextBox, 'keypress', this.hourTextBoxKeyPressHandler);
        }

        if (this.minuteTextBox != null) {
            $addHandler(this.minuteTextBox, 'focus', this.minuteTextBoxFocusHandler);
            $addHandler(this.minuteTextBox, 'keypress', this.minuteTextBoxKeyPressHandler);
        }

        Sys.Application.add_load(this.applicationLoadHandler);
    },

    OnApplicationLoad: function () {
    },

    dispose: function () {

        if (this.arrowUpMouseDownHandler != null) {

            if (this.arrowUp != null) {
                $removeHandler(this.arrowUp, 'mousedown', this.arrowUpMouseDownHandler);
            }

            this.arrowUpMouseDownHandler = null;
        }

        if (this.arrowDownMouseDownHandler != null) {

            if (this.arrowDown != null) {
                $removeHandler(this.arrowDown, 'mousedown', this.arrowDownMouseDownHandler);
            }

            this.arrowDownMouseDownHandler = null;
        }

        if (this.minuteTextBoxFocusHandler != null) {

            if (this.minuteTextBox != null) {
                $removeHandler(this.minuteTextBox, 'focus', this.minuteTextBoxFocusHandler);
            }

            this.minuteTextBoxFocusHandler = null;
        }

        if (this.minuteTextBoxKeyPressHandler != null) {
            if (this.minuteTextBox != null) {
                $removeHandler(this.minuteTextBox, 'keypress', this.minuteTextBoxKeyPressHandler);
            }

            this.minuteTextBoxKeyPressHandler = null;
        }

        if (this.hourTextBoxFocusHandler != null) {

            if (this.hourTextBox != null) {
                $removeHandler(this.hourTextBox, 'focus', this.hourTextBoxFocusHandler);
            }

            this.hourTextBoxFocusHandler = null;
        }

        if (this.hourTextBoxKeyPressHandler != null) {
            if (this.hourTextBox != null) {
                $removeHandler(this.hourTextBox, 'keypress', this.hourTextBoxKeyPressHandler);
            }

            this.hourTextBoxKeyPressHandler = null;
        }

        if (this.arrowUpFocusHandler != null) {

            if (this.arrowUp != null) {
                $removeHandler(this.arrowUp, 'focus', this.arrowUpFocusHandler);
            }

            this.arrowUpFocusHandler = null;
        }

        if (this.arrowDownFocusHandler != null) {

            if (this.arrowDown != null) {
                $removeHandler(this.arrowDown, 'focus', this.arrowDownFocusHandler);
            }

            this.arrowDownFocusHandler = null;
        }

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
            this.applicationLoadHandler = null;
        }

        this.hourTextBox = null;
        this.minuteTextBox = null;

        //LZS - BLG_335 - változók nullázása.
        this.clonehourTextBox = null;
        this.cloneminuteTextBox = null;

        this.arrowUp = null;
        this.arrowDown = null;

        Utility.Time.callBaseMethod(this, 'dispose');
    },

    OnArrowUpMouseDown: function (sender, args) {
        if (this.focusOnMinute)
            this.changeMinute(true);
        else
            this.changeHour(true);
        return false;
    },

    OnArrowDownMouseDown: function (sender, args) {
        if (this.focusOnMinute)
            this.changeMinute(false);
        else
            this.changeHour(false);
        return false;
    },

    OnArrowUpFocus: function (sender, args) {
        //        if(this.focusOnMinute)
        //            this.minuteTextBox.focus();
        //        else
        //            this.hourTextBox.focus();       
        //        return false;
    },

    OnArrowDownFocus: function (sender, args) {
        //        if(this.focusOnMinute)
        //            this.minuteTextBox.focus();
        //        else
        //            this.hourTextBox.focus();       
        //        return false;
    },

    OnMinuteTextBoxFocus: function (sender, args) {
        this.focusOnMinute = true;
        return false;
    },

    OnHourTextBoxFocus: function (sender, args) {
        this.focusOnMinute = false;
        return false;
    },

    leftPadWithZero: function (element, len) {
        if (element != null) {
            var str = element.toString();
            while (str.length < len) {
                str = '0' + str;
            }

            return str;
        }
        else {
            return element;
        }
    },

    changeHour: function (increment) {
        var hour = parseInt(this.hourTextBox.value, 10); // force to parse with radix 10
        if (!isNaN(hour)) {
            if (increment)
                hour++;
            else
                hour--;

            if (hour > 23) hour = hour % 24;
            if (hour < 0) hour = (hour % 24) + 24;

            this.hourTextBox.value = this.leftPadWithZero(hour, 2);
            //LZS - BLG_335 - Ha van megadva másolandó textbox, akkor oda is beírjuk a hour értéket.
            if (this.clonehourTextBox)
            {
                this.clonehourTextBox.value = this.leftPadWithZero(hour, 2);
            }
        }
        else {
            this.hourTextBox.value = '00';
            //LZS - BLG_335 - Ha van megadva másolandó textbox, akkor oda is beírjuk a hour értéket.
            if (this.clonehourTextBox)
            {
                this.clonehourTextBox.value = '00';
            }
        }
    },

    changeMinute: function (increment) {
        var minute = parseInt(this.minuteTextBox.value, 10); // force to parse with radix 10
        if (!isNaN(minute)) {
            if (increment)
                minute++;
            else
                minute--;

            if (minute > 59) minute = minute % 60;
            if (minute < 0) minute = (minute % 60) + 60;

            this.minuteTextBox.value = this.leftPadWithZero(minute, 2);
            //LZS - BLG_335 - Ha van megadva másolandó textbox, akkor oda is beírjuk a minute értéket.
            if (this.cloneminuteTextBox )
            {
                this.cloneminuteTextBox.value = this.leftPadWithZero(minute, 2);
            }
        }
        else {
            this.minuteTextBox.value = '00';
            //LZS - BLG_335 - Ha van megadva másolandó textbox, akkor oda is beírjuk a minute értéket.
            if (this.cloneminuteTextBox )
            {
                this.cloneminuteTextBox.value = '00';
            }
        }
    },

    OnHourKeyPress: function (evt) {
        this._OnKeyPress(evt, true);
    },

    OnMinuteKeyPress: function (evt) {
        this._OnKeyPress(evt, false);
    },

    _OnKeyPress: function (evt, hour) {
        var scanCode;

        if ((evt.charCode == Sys.UI.Key.pageUp) ||
            (evt.charCode == Sys.UI.Key.pageDown) ||
            (evt.charCode == Sys.UI.Key.up) ||
            (evt.charCode == Sys.UI.Key.down) ||
            (evt.charCode == Sys.UI.Key.left) ||
            (evt.charCode == Sys.UI.Key.right) ||
            (evt.charCode == Sys.UI.Key.home) ||
            (evt.charCode == Sys.UI.Key.end) ||
            (evt.charCode == 46 /* Delete */) ||
            (evt.ctrlKey /* Control keys */)) {
            return;
        }

        if (evt.rawEvent.keyIdentifier) {
            // Safari
            // Note (Garbin): used the underlying rawEvent insted of the DomEvent instance.
            if (evt.rawEvent.ctrlKey || evt.rawEvent.altKey || evt.rawEvent.metaKey) {
                return;
            }

            if (evt.rawEvent.keyIdentifier.substring(0, 2) != "U+") {
                return;
            }

            scanCode = evt.rawEvent.charCode;
            if (scanCode == 63272 /* Delete */) {
                return;
            }
        } else {
            scanCode = evt.charCode;
        }

        if (scanCode && scanCode >= 0x20 /* space */) {
            var c = String.fromCharCode(scanCode);
            if (!this._processKey(c, hour)) {
                evt.preventDefault();
            }
        }
    },


    _processKey: function (key, hour) {
        var number = parseInt(key);
        if (isNaN(number)) {
            return false;
        }

        var index = 1;
        var value = '';

        if (hour) {
            index = this.getSelectionStart(this.hourTextBox);
            value = this.hourTextBox.value.trim();
        }
        else {
            index = this.getSelectionStart(this.minuteTextBox);
            value = this.minuteTextBox.value.trim();
        }

        if (value.length > 0) {
            var text = '';

            if (index == 0) {

                text = number + value;
            }

            if (index == 1) {
                text = value + number;
            }

            return this._processText(text, hour);
        }


        return true;
    },

    _processText: function (text, hour) {
        var number = parseInt(text);
        if (isNaN(number)) {
            return false;
        }

        if (hour) {
            if (number > 23 || number < 0) return false;
        }
        else {
            if (number > 59 || number < 0) return false;
        }

        return true;
    },

    getSelectionStart: function (o) {
        if (o.createTextRange) {
            if (document.selection != null && document.selection.createRange() != null) {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            }
            else {
                return o.selectionStart;
            }
        }
        else return o.selectionStart
    },

    //getter, setter methods
    get_HourTextBoxClientId: function () {
        return this.hourTextBoxClientId;
    },

    set_HourTextBoxClientId: function (value) {
        if (this.hourTextBoxClientId != value) {
            this.hourTextBoxClientId = value;
        }
    },

    get_MinuteTextBoxClientId: function () {
        return this.minuteTextBoxClientId;
    },

    set_MinuteTextBoxClientId: function (value) {
        if (this.minuteTextBoxClientId != value) {
            this.minuteTextBoxClientId = value;
        }
    },

    //LZS - BLG_335 - másolandó óra textbox getter
    get_cloneHourTextBoxClientId: function () {
        return this.clonehourTextBoxClientId;
    },

    //LZS - BLG_335 - másolandó óra textbox setter
    set_cloneHourTextBoxClientId: function (value) {
        if (this.clonehourTextBoxClientId != value) {
            this.clonehourTextBoxClientId = value;
        }
    },

    //LZS - BLG_335 - másolandó perc textbox getter
    get_cloneMinuteTextBoxClientId: function () {
        return this.cloneminuteTextBoxClientId;
    },

    //LZS - BLG_335 - másolandó perc textbox setter
    set_cloneMinuteTextBoxClientId: function (value) {
        if (this.cloneminuteTextBoxClientId != value) {
            this.cloneminuteTextBoxClientId = value;
        }
    },

    get_ArrowUpClientId: function () {
        return this.arrowUpClientId;
    },

    set_ArrowUpClientId: function (value) {
        if (this.arrowUpClientId != value) {
            this.arrowUpClientId = value;
        }
    },

    get_ArrowDownClientId: function () {
        return this.arrowDownClientId;
    },

    set_ArrowDownClientId: function (value) {
        if (this.arrowDownClientId != value) {
            this.arrowDownClientId = value;
        }
    }
}


Utility.Time.registerClass('Utility.Time', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded(); 