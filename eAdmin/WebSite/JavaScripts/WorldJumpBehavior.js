﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.WorldJumpBehavior = function(element)
{
    Utility.WorldJumpBehavior.initializeBase(this, [element]);
    
    //Properties
    this._autoCompleteExtenderId = null;
    this._delimeter = null;
    this._freeType = false;
    //Variables
    this._keyDownHandler = null;
    this._itemOverHandler = null;
    this._itemOutHandler = null;
    this._completeText = null;
    this._overText = null;
}

Utility.WorldJumpBehavior.prototype = 
{
    
    initialize : function() {
        Utility.WorldJumpBehavior.callBaseMethod(this, 'initialize');
        
        this._itemOverHandler = Function.createDelegate(this, this._onItemOver);
        this._itemOutHandler = Function.createDelegate(this, this._onItemOut);
   
        var element = this.get_element();
        if (element) 
        {
            if(this._autoCompleteExtenderId)
            {
                var autExt = $find(this._autoCompleteExtenderId);
                {
                    if(autExt)
                    {
                        //Default eseménykezelés                    
                        this._keyDownHandler = Function.createDelegate(autExt, autExt._onKeyDown);
                        
                        //Default eseménykezelés felülírása
                        $removeHandler(element, "keydown", autExt._keyDownHandler);
                        autExt._keyDownHandler = Function.createDelegate(this, this._onKeyDown)
                        $addHandler(element, "keydown", autExt._keyDownHandler);
                        
                        autExt.add_itemOver(this._itemOverHandler);
                        autExt.add_itemOut(this._itemOutHandler);
                    }
                }
            }
        }
       
        
    },
    
    dispose : function() {
        
        var element = this.get_element();
        
        var autExt = $find(this._autoCompleteExtenderId);
        {
            if(autExt)
            {
               autExt.remove_itemOver(this._itemOverHandler);
               autExt.remove_itemOut(this._itemOutHandler);
            }
        }   
      
        this._keyDownHandler = null;
        this._itemOverHandler = null;
        this._itemOutHandler = null;
        
        Utility.WorldJumpBehavior.callBaseMethod(this, 'dispose');
    },
    
    _CutForras: function(text)
    {    
        if(typeof(Utility.PartnerAutoComplete) =='function')
        {
            return Utility.PartnerAutoComplete.CutForras(text);
        }
        
        return text;
    },
     
    _onKeyDown: function(ev)
    {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
    
        if (this._HandleWorldJump(k))
        {
            ev.stopPropagation();     
            ev.preventDefault();
        }
        else
        {
            //Default eseménykezelés
            this._keyDownHandler(ev);
        }
    },
    
    _HandleWorldJump: function(key)
    {
        var element = this.get_element();
        
        //enterre szóugrás ha kell
        if(key === Sys.UI.Key.enter)
        {
           var selectedText = Utility.Selection.getSelectedText();
           if(selectedText.trim() != '')
           {             
                if(this._completeText)
                {
                    element.value = this._completeText;
                    var endIndex = element.value.length;
                    Utility.Selection.textboxSelect(element,endIndex,endIndex);
                    if(IsOnTestPage)
                    {
                        if(this._overText == this._completeText)
                        {
                            return false;
                        }
                    }
                    return true;
                }        
           }
        }
        //backspace kezelés
        if(key === Sys.UI.Key.backspace)
        {
           var selectedText = Utility.Selection.getSelectedText();
           if(selectedText.trim() != '')
           {
                var cutLength = selectedText.length;
                var cutIndex = element.value.length - cutLength;
                if(cutIndex > -1)
                {
                    element.value = element.value.substring(0,cutIndex);
                    var endIndex = element.value.length;
                    Utility.Selection.textboxSelect(element,endIndex,endIndex);
                    return false;
                }
           } 
        }
        
        //esc-re a kijelölés eltüntetése
        if(key == Sys.UI.Key.esc)
        {
            Utility.Selection.deleteSelectedText(element);
            var autExt = $find(this._autoCompleteExtenderId)
            if(autExt)
            {
                autExt._currentPrefix = element.value;
            }
        }
        
        return false;
    },
    
    
    _complementWorld : function(overText)
    {
        var autExt = $find(this._autoCompleteExtenderId);
        {
            if(autExt)
            {
               if(overText)
               {
                   var text = overText;

                   var e = this.get_element();
                   if(e)
                   {
                        var currentText = e.value;
                        currentText = this._getRawText(currentText);
                        if(currentText.length < text.length)
                        {
                            var endIndex = text.indexOf(' ',currentText.length);
                            if(IsOnTestPage)
                            {
                                if(!this._freeType)
                                {
                                    endIndex = text.length;
                                }    
                            }
                            if (endIndex == -1) endIndex = text.length;
                            if(endIndex > -1 && endIndex > currentText.length)
                            {
                                var newText;
                                if(IsOnTestPage)
                                {
                                    if(this._freeType)
                                    {
                                       newText = currentText + text.substring(currentText.length,endIndex);
                                    }
                                    else
                                    {
                                       newText = text.substring(0,endIndex);
                                    }
                                }
                                else
                                {
                                   newText = text.substring(0,endIndex); 
                                }
                                
                                e.value = newText;
                                this._completeText = text.substring(0,endIndex);
                                //ne okozzon a kiegészített szöveg autocomplete-et
                                autExt._currentPrefix = newText;
                                Utility.Selection.textboxSelect(e,currentText.length,endIndex);
                            }
                            
                        }
                        
                   }        
               }
            }
        }
    },
    
    _onItemOver: function (sender,eventArgs)
    {
        var e = this.get_element();
        var overText = eventArgs.get_text();
        overText = this._normalizeTextBoxText(e,overText);
        this._overText = overText;
        this._complementWorld(overText);
    },

    _normalizeTextBoxText: function(oTextBox,overText)
    {
        var e = oTextBox;
        Utility.Selection.deleteSelectedText(e);
        var text = e.value;
        text = this._getRawText(text);
        overText = this._getRawText(overText);
        var commonText = Utility.String.FindCommonStart(text, overText);
        if(commonText != text)
        {
            e.value = commonText;
        }
        
        return overText;
    },
    
    _getRawText: function(text)
    {
        text = text.split(this._delimeter)[0];
        text = this._CutForras(text);
        return text;
    },
    
    _onItemOut: function (sender,eventArgs)
    {
        this._completeText = null;
    },
    
    //
    // Behavior properties
    //
    
    get_AutoCompleteExtenderId : function() {
        return this._autoCompleteExtenderId;
    },

    set_AutoCompleteExtenderId : function(value) {
        if (this._autoCompleteExtenderId !== value) {
            this._autoCompleteExtenderId = value;
            this.raisePropertyChanged('AutoCompleteExtenderId');
        }
    },
    
    get_Delimeter : function() {
        return this._delimeter;
    },

    set_Delimeter : function(value) {
        if (this._delimeter !== value) {
            this._delimeter = value;
            this.raisePropertyChanged('Delimeter');
        }
    },
    
    get_FreeType : function() {
        return this._freeType;
    },

    set_FreeType : function(value) {
        if (this._freeType !== value) {
            this._freeType = value;
            this.raisePropertyChanged('FreeType');
        }
    }
}

Utility.WorldJumpBehavior.registerClass('Utility.WorldJumpBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();