﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />


    var eSetTextBoxesByUgykorUgytipusType = { All:0, OnlySzignalas:1, OnlyIntezesiIdo:2 };

    var ugykor_lastValue = '';
    var ugytipus_lastValue = '';
    
    var eljarasiszakasz_lastValue = '00';
    var irattipus_lastValue = '';

    var ugyGeneraltTargy_lastValue = '';
    var kezelo_lastValue = '';  //userId;  // eredetileg a formon beírjuk az aktuális felhasználót kezelőnek, ezért ezzel hasonlítunk
    var ugyintezo_lastValue = '';
    var ugyfelelos_lastValue = '';
    
    function clearIratHatarido()
    {
        var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
        var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);
        var ugyiratHataridoKitolasIratHiddenField = $get(UgyiratHataridoKitolas_Irat_HiddenField_Id)
        
        if (iratIntezesiHataridoTextBox)
        {
            iratIntezesiHataridoTextBox.value = "";
            
            if (iratIntezesiHataridoHourTextBox)
            {
                iratIntezesiHataridoHourTextBox.value = "00";
            }
            
            if (iratIntezesiHataridoMinuteTextBox)
            {
                iratIntezesiHataridoMinuteTextBox.value = "00";
            }
            // TOOLTIP
            iratIntezesiHataridoTextBox.title =  "";  

        }
        
        if (ugyiratHataridoKitolasIratHiddenField)
        {
            // kitolhat? | kötött?
            ugyiratHataridoKitolasIratHiddenField.value = "1|0";
        }
       
        setIratHataridoControlEnableState(true);                             
    }
    
    function setIratHataridoControlEnableState(isEnabled)
    {
        var bDisabled = !isEnabled;
        var classNameControls = (isEnabled ? "" : "disabledCalendarImage");
    
        var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
        var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);    
    
        var iratIntezesiHatarido_CalendarImage = $get(IraIratHatarido_CalendarImage_Id);
        var iratIntezesiHatarido_ArrowUpImage = $get(IraIratHatarido_ArrowUpImage_Id);
        var iratIntezesiHatarido_ArrowDownImage = $get(IraIratHatarido_ArrowDownImage_Id);

        if (iratIntezesiHataridoTextBox)
        {
            iratIntezesiHataridoTextBox.disabled = bDisabled;
        }
        
        if (iratIntezesiHatarido_CalendarImage)
        {
            iratIntezesiHatarido_CalendarImage.disabled = bDisabled;
            iratIntezesiHatarido_CalendarImage.className = classNameControls;
        }
                
        if (iratIntezesiHataridoHourTextBox)
        {
            iratIntezesiHataridoHourTextBox.disabled = bDisabled;
        }
                
        if (iratIntezesiHataridoMinuteTextBox)
        {
            iratIntezesiHataridoMinuteTextBox.disabled = bDisabled;
        }
                
        if (iratIntezesiHatarido_ArrowUpImage != null)
        {
            iratIntezesiHatarido_ArrowUpImage.disabled = bDisabled;
            iratIntezesiHatarido_ArrowUpImage.className = classNameControls;
        }
                
        if (iratIntezesiHatarido_ArrowDownImage != null)
        {
            iratIntezesiHatarido_ArrowDownImage.disabled = bDisabled;
            iratIntezesiHatarido_ArrowDownImage.className = classNameControls;
        } 
    }
    
    function setUgyiratHataridoControlEnableState(isEnabled)
    {
        var bDisabled = !isEnabled;
        var classNameControls = (isEnabled ? "" : "disabledCalendarImage");
    
        var ugyIntezesiHataridoTextBox = $get(UgyUgyiratHatarido_TextBox_Id);
        var ugyIntezesiHatarido_CalendarImage = $get(UgyUgyiratHatarido_CalendarImage_Id);

        if (ugyIntezesiHataridoTextBox)
        {
            ugyIntezesiHataridoTextBox.disabled = bDisabled;
        }
        
        if (ugyIntezesiHatarido_CalendarImage != null)
        {
            ugyIntezesiHatarido_CalendarImage.disabled = bDisabled;
            ugyIntezesiHatarido_CalendarImage.className = classNameControls;
        }        
        
    }
    
    function SetTextBoxesByIratMetaDefinicioForUgyiratModify()
    {
        // ellenőrzés
        if (!userId) return false;
        // Ügykör_Id:
         var UgykorHiddenField = $get(UgykorHiddenField_Id);
         var selectedUgykor = '';
         if (UgykorHiddenField)
        {
            selectedUgykor = UgykorHiddenField.value;
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        }
        
        var iktatasDatumUgyirat = '';
        var IktatasDatumUgyirat_HiddenField = $get(IktatasDatumUgyirat_HiddenFieldId);
        if (IktatasDatumUgyirat_HiddenField)
        {
            iktatasDatumUgyirat = IktatasDatumUgyirat_HiddenField.value;
        }
        
        // 1. ugyirat hatarido
        if (!selectedUgykor || selectedUgykor == '')
	    {                    
	        // nem kell ws hívás
        }
        else
        {                   
            Ajax_eRecord.GetIntezesiIdoWithToolTip(
                selectedUgykor,selectedUgytipus,userId,iktatasDatumUgyirat,OnWsCallBack_SetIntezesiHatarido);
        }
    }

    function SetTextBoxesByIratMetaDefinicio(type)
    {
        // ellenőrzés
        switch(type)
        {
            case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                if (!userId || !userCsoportId) return false;
                break;
            case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                if (!userId) return false;
                break;                
            case eSetTextBoxesByUgykorUgytipusType.All: default:
                if (!userId || !userCsoportId) return false;
                break;
        }

        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
        {
            if (UgykorDropDown.selectedIndex >= 0)
            { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        
        }
        
        // Eljárási szakasz:
        var EljarasiSzakaszHiddenField = $get(EljarasiSzakaszHiddenField_Id);
        var selectedEljarasiSzakasz = '00';
        if (EljarasiSzakaszHiddenField)
        {
            if (EljarasiSzakaszHiddenField.value != '')
            { selectedEljarasiSzakasz = EljarasiSzakaszHiddenField.value; }
        
        }        

        // Irattípus:
        var IrattipusDropDown = $get(IrattipusDropDown_Id);            
        var selectedIrattipus = '';
        if (IrattipusDropDown)
        {
            if (IrattipusDropDown.selectedIndex >= 0)
            { selectedIrattipus = IrattipusDropDown.options[IrattipusDropDown.selectedIndex].value; }
        
        }        

        // 1. ugyirat hatarido
        if (!selectedUgykor || selectedUgykor == '')
	    {                    
	        // nem kell ws hívás
        }
        else if (selectedUgykor == ugykor_lastValue && selectedUgytipus == ugytipus_lastValue)
        {
            // nem kell ws hívás
        }
        else
        {
            // ezt az információt még használjuk, ezért később végezzük csak el az értékadást
            //ugykor_lastValue = selectedUgykor;
            //ugytipus_lastValue = selectedUgytipus;
            
            switch(type)
            {
                case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor, selectedUgytipus, userId, userCsoportId, OnWsCallBack_SetFelelosCsoportok);
                    Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                        selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                    break;
                case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                    {
                        var iktatasDatumUgyirat = '';
                        var IktatasDatumUgyirat_HiddenField = $get(IktatasDatumUgyirat_HiddenFieldId);
                        if (IktatasDatumUgyirat_HiddenField)
                        {
                            iktatasDatumUgyirat = IktatasDatumUgyirat_HiddenField.value;
                        }                   
                        Ajax_eRecord.GetIntezesiIdoWithToolTip(
                            selectedUgykor, selectedUgytipus, userId, iktatasDatumUgyirat, OnWsCallBack_SetIntezesiHatarido);
                        Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                            selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                    }
                    break;                
                case eSetTextBoxesByUgykorUgytipusType.All: default:
                    {
                        var iktatasDatumUgyirat = '';
                        var IktatasDatumUgyirat_HiddenField = $get(IktatasDatumUgyirat_HiddenFieldId);
                        if (IktatasDatumUgyirat_HiddenField)
                        {
                            iktatasDatumUgyirat = IktatasDatumUgyirat_HiddenField.value;
                        }
                        Ajax_eRecord.GetIntezesiIdoWithToolTip(
                            selectedUgykor,selectedUgytipus,userId,iktatasDatumUgyirat,OnWsCallBack_SetIntezesiHatarido);
                        Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                            selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
                        Ajax_eRecord.GetGeneraltTargyByUgykorUgytipus(
                            selectedUgykor, selectedUgytipus, userId, OnWsCallBack_SetGeneraltTargy);
                    }
                    break;
            }
        }
        
        // 2. irat hatarido
        if (type == eSetTextBoxesByUgykorUgytipusType.All || type == eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo)
        {
            if (!selectedUgytipus || selectedUgytipus == ''
                || !selectedEljarasiSzakasz || selectedEljarasiSzakasz == '' || !selectedIrattipus || selectedIrattipus == '')
            {                    
                // nem kell ws hívás
                clearIratHatarido();
            }         
            else if (selectedUgykor != ugykor_lastValue || selectedUgytipus != ugytipus_lastValue
                || selectedEljarasiSzakasz != eljarasiszakasz_lastValue || selectedIrattipus != irattipus_lastValue)
            {
                // mivel az ügykör/ügytípus változott, itt mindenképp kell hívás
                eljarasiszakasz_lastValue = selectedEljarasiSzakasz; 
                irattipus_lastValue = selectedIrattipus;
                
                var iktatasDatumIrat = '';
                var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
                if (IktatasDatumIrat_HiddenField)
                {
                    iktatasDatumIrat = IktatasDatumIrat_HiddenField.value;
                }
                Ajax_eRecord.GetIratIntezesiIdoWithToolTip(
                        selectedUgykor,selectedUgytipus,selectedEljarasiSzakasz,selectedIrattipus,userId,iktatasDatumIrat,OnWsCallBack_SetIratHatarido);

            }
        }
        
        ugykor_lastValue = selectedUgykor;
        ugytipus_lastValue = selectedUgytipus;
    }
    
    function createItemToolTips(dropdown)
    {
        Utility.DomElement.Select.createItemToolTips(dropdown);
    }
    
    function clearDropDown(dropdown)
    {
        if (dropdown)
        {
            for (var i = dropdown.options.length - 1; i >= 0; i--)
            {
                dropdown.options[i].selected = false;
                dropdown.options[i] = null; // remove the option
            }
        }
    }
/*    
    function FillUgytipusDropDown()
    {
        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);
        
        if (!UgytipusDropDown ) return false;
        
         // Ügykör_Id:
         var UgykorHiddenField = $get(UgykorHiddenField_Id);
         
         if (!UgykorHiddenField || UgykorHiddenField.value == '') return false;

        Ajax_eRecord.GetUgytipusokByUgykorForDropDown(UgykorHiddenField.value, OnWsCallBack_FillUgytipusDropDown);

    }
    
    function OnWsCallBack_FillUgytipusDropDown(result)
    {
        // result feldolgozása: tömb, elemei Ugytipus és UgytipusNev párok egymástól pontosvesszővel pipe ("|") karakterrel elválasztva
        
        //alert(result);
       
       // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);
        
        if (!UgytipusDropDown ) return false;
        
        clearDropDown(UgytipusDropDown);
 
        resultArray = result;

        if (resultArray.length > 0)
        {
            for ( var i = 0; i < resultArray.length; i++)
            {
                var elementArray = resultArray[i].split('|');
                if (elementArray.length == 2)
                {
                    var ugytipus = elementArray[0];
                    var ugytipusNev = elementArray[1];
                    var option = document.createElement('option');
                    option.appendChild(document.createTextNode(ugytipusNev));
                    option.setAttribute('value', ugytipus);
                    option.title = ugytipusNev;
                    UgytipusDropDown.appendChild(option);
                }
            }
        }
        
        if (UgytipusDropDown.options.length == 0)
        {
            var option = document.createElement('option');
            option.appendChild(document.createTextNode('---'));
            option.setAttribute('value', '');
            UgytipusDropDown.appendChild(option);
        }
        
        UgytipusDropDown.options[0].selected = true;
        createItemToolTips(UgytipusDropDown);
        
        $common.tryFireEvent(UgytipusDropDown, 'change');
    }
*/
    
    
/*    
    function SetTextBoxesByUgykorUgytipus(type)
    {
        // ellenőrzés
        switch(type)
        {
            case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                if (!userId || !userCsoportId) return false;
                break;
            case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                if (!userId) return false;
                break;                
            case eSetTextBoxesByUgykorUgytipusType.All: default:
                if (!userId || !userCsoportId) return false;
                break;
        }

        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
        {
            if (UgykorDropDown.selectedIndex >= 0)
            { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        
        }
        
        if (!selectedUgykor || selectedUgykor == ''
            || (selectedUgykor == ugykor_lastValue && selectedUgytipus == ugytipus_lastValue))
        {                    
            // nem kell ws hívás
        }
        else
        {
            ugykor_lastValue = selectedUgykor;
            ugytipus_lastValue = selectedUgytipus;
            
            switch(type)
            {
                case eSetTextBoxesByUgykorUgytipusType.OnlySzignalas:
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
                    break;
                case eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo:
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor,selectedUgytipus,userId,OnWsCallBack_SetIntezesiHatarido);
                    break;                
                case eSetTextBoxesByUgykorUgytipusType.All: default:
                    Ajax_eRecord.GetIntezesiIdoWithToolTip(
                        selectedUgykor,selectedUgytipus,userId,OnWsCallBack_SetIntezesiHatarido);
                    Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                        selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
                    break;
            }


        }
    }    
*/ 

    function SetTextBoxesIrattipusByUgyirat(Ugyirat_Id)
    {
        if (!Ugyirat_Id) return false;
        
        // Eljárási szakasz:
        var EljarasiSzakaszHiddenField = $get(EljarasiSzakaszHiddenField_Id);
        var selectedEljarasiSzakasz = '00';
        if (EljarasiSzakaszHiddenField)
        {
            if (EljarasiSzakaszHiddenField.value != '')
            { selectedEljarasiSzakasz = EljarasiSzakaszHiddenField.value; }
        
        }       

        // Irattípus:
        var IrattipusDropDown = $get(IrattipusDropDown_Id);            
        var selectedIrattipus = '';
        if (IrattipusDropDown)
        {
            if (IrattipusDropDown.selectedIndex >= 0)
            { selectedIrattipus = IrattipusDropDown.options[IrattipusDropDown.selectedIndex].value; }
        
        }        

       if (!selectedEljarasiSzakasz || selectedEljarasiSzakasz == '' || !selectedIrattipus || selectedIrattipus == '')
        {                    
            // nem kell ws hívás
                clearIratHatarido();
        }
        else
        {           
            eljarasiszakasz_lastValue = selectedEljarasiSzakasz;
            irattipus_lastValue = selectedIrattipus;

            var iktatasDatumIrat = '';
            var IktatasDatumIrat_HiddenField = $get(IktatasDatumIrat_HiddenFieldId);
            if (IktatasDatumIrat_HiddenField)
            {
                iktatasDatumIrat = IktatasDatumIrat_HiddenField.value;
            }       
            Ajax_eRecord.GetIratIntezesiIdoWithToolTipFromUgyiratId(
                Ugyirat_Id,selectedEljarasiSzakasz,selectedIrattipus,userId,iktatasDatumIrat,OnWsCallBack_SetIratHatarido);

        }
        
    }
    
    function OnWsCallBack_SetIntezesiHatarido(result)
    {
        // result feldolgozása: 'intézési idő, kötöttség, számított határidő és esetleges tooltip szöveg pontosvesszővel (';') elválasztva
        
        //alert(result);
        
        var ugyIntezesiHataridoTextBox = $get(UgyUgyiratHatarido_TextBox_Id);
        var ugyiratHataridoKitolasUgyiratHiddenField = $get(UgyiratHataridoKitolas_Ugyirat_HiddenField_Id);
        
        if (ugyIntezesiHataridoTextBox)
        {
            var strIntezesiIdo = '';
            var strIntezesiIdoKotott = '';
            var shortDateStr = null;
            var tooltip = '';
            var strUgyiratHataridoKitolas = '';
                 
            var ugyIntezesiHataridoArray = new Array();   
            ugyIntezesiHataridoArray = result.split(';');
             
            if (ugyIntezesiHataridoArray.length == 5)
            {
                strIntezesiIdo = ugyIntezesiHataridoArray[0];
                strIntezesiIdoKotott = ugyIntezesiHataridoArray[1];
                shortDateStr = ugyIntezesiHataridoArray[2];
                tooltip = ugyIntezesiHataridoArray[3];
                strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[4];
            }
            

            if (shortDateStr != null)
            {
                // határidő formázott (yyyy.mm.dd) beírása
                ugyIntezesiHataridoTextBox.value = shortDateStr;
            }
                       
            ugyIntezesiHatarido_CalendarImage = $get(UgyUgyiratHatarido_CalendarImage_Id);
            if (strIntezesiIdoKotott == "1" || strUgyiratHataridoKitolas == "0")
            {
                setUgyiratHataridoControlEnableState(false);
            }
            else
            {
                setUgyiratHataridoControlEnableState(true);           
            }
            
            if (ugyiratHataridoKitolasUgyiratHiddenField)
            {
                if (strUgyiratHataridoKitolas == '0')
                {
                    ugyiratHataridoKitolasUgyiratHiddenField.value = '0';
                }
                else
                {
                    ugyiratHataridoKitolasUgyiratHiddenField.value = '1';
                }
            }
            
            // TOOLTIP
            ugyIntezesiHataridoTextBox.title = tooltip; 
        }
//        else
//        {
//            return false;
//        }
    }
   
   
/*    
    function SetTextBoxesFromSzignalasiJegyzek()
    {
        if (!userId || !userCsoportId)
        {
            return false;
        }

        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
        {
            if (UgykorDropDown.selectedIndex >= 0)
            { selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value; }
        }

        // Ügytípus:
        var UgytipusDropDown = $get(UgytipusDropDown_Id);            
        var selectedUgytipus = '';
        if (UgytipusDropDown)
        {
            if (UgytipusDropDown.selectedIndex >= 0)
            { selectedUgytipus = UgytipusDropDown.options[UgytipusDropDown.selectedIndex].value; }
        
        }

        if (!selectedUgykor || selectedUgykor == ''
            || (selectedUgykor == ugykor_lastValue && selectedUgytipus == ugytipus_lastValue))
        {                    
            // nem kell ws hívás
        }
        else
        {  
            ugykor_lastValue = selectedUgykor;
            ugytipus_lastValue = selectedUgytipus;
        
            Ajax_eRecord.GetFelelosCsoportokByUgykorUgytipus(
                selectedUgykor,selectedUgytipus,userId,userCsoportId,OnWsCallBack_SetFelelosCsoportok);
        }
            
    }       
*/
    
    function OnWsCallBack_SetIratHatarido(result)
    {
        // result feldolgozása: 'intézési idő, időegység, kötöttség, számított határidő, esetleges tooltip szöveg
        // és ügyirat határidő kitolhatóság pontosvesszővel (';') elválasztva
        
        //alert(result);
        
        var iratIntezesiHataridoTextBox = $get(IraIratHatarido_TextBox_Id);
        var iratIntezesiHataridoHourTextBox = $get(IraIratHatarido_HourTextBox_Id);
        var iratIntezesiHataridoMinuteTextBox = $get(IraIratHatarido_MinuteTextBox_Id);
        
        var ugyiratHataridoKitolasIratHiddenField = $get(UgyiratHataridoKitolas_Irat_HiddenField_Id);
        
        if (iratIntezesiHataridoTextBox)        
        {
            var strIntezesiIdo_irat = '';
            var strIntezesiIdoKotott_irat = '';
            var shortDateStr_irat = null;
            var hourStr_irat = null;
            var minuteStr_irat = null;
            var tooltip_irat = '';
            var strUgyiratHataridoKitolas_irat = '1';
            
            var iratIntezesiHataridoArray = new Array();   
            iratIntezesiHataridoArray = result.split(';');
             
            if (iratIntezesiHataridoArray.length == 7)
            {
                strIntezesiIdo_irat = iratIntezesiHataridoArray[0];
                strIntezesiIdoKotott_irat = iratIntezesiHataridoArray[1];
                shortDateStr_irat = iratIntezesiHataridoArray[2];
                hourStr_irat = iratIntezesiHataridoArray[3];
                minuteStr_irat = iratIntezesiHataridoArray[4];
                tooltip_irat = iratIntezesiHataridoArray[5];
                strUgyiratHataridoKitolas_irat = iratIntezesiHataridoArray[6];
            }
            else
            {
                // hiba
                //alert('Hiba: az irat intézési idő tömb nem megfelelő számú elemet tartalmaz!');
                return false;
            }
            
            if (strIntezesiIdoKotott_irat != '1')
            {
                strIntezesiIdoKotott_irat = '0';
            }
            
            if (ugyiratHataridoKitolasIratHiddenField)
            {
                if (strUgyiratHataridoKitolas_irat == '0')
                {
                    ugyiratHataridoKitolasIratHiddenField.value = '0' + '|' + strIntezesiIdoKotott_irat;
                }
                else
                {
                    ugyiratHataridoKitolasIratHiddenField.value = '1' + '|' + strIntezesiIdoKotott_irat;
                }
            }            
                        
            if (shortDateStr_irat != '')
            {     
                // határidő formázott (yyyy.mm.dd) beírása
                iratIntezesiHataridoTextBox.value = shortDateStr_irat;
                
                if (hourStr_irat != '')
                {
                    iratIntezesiHataridoHourTextBox.value = hourStr_irat;
                }
                else
                {
                    iratIntezesiHataridoHourTextBox.value = "00";
                }
                
                if (minuteStr_irat != '')
                {
                    iratIntezesiHataridoMinuteTextBox.value = minuteStr_irat;
                }
                else
                {
                    iratIntezesiHataridoMinuteTextBox.value = "00";
                }
            }
            else     // ha nincs előírás, akkor töröljük
            {
                clearIratHatarido();
            }
                           
            if (strIntezesiIdoKotott_irat == "1")
            {
                setIratHataridoControlEnableState(false);                                                  
            }     
            else
            {
                setIratHataridoControlEnableState(true);                                                     
            }

            // TOOLTIP
            iratIntezesiHataridoTextBox.title = tooltip_irat;
 
        }
        else
        {
            return false;
        }
    }

    function OnWsCallBack_SetFelelosCsoportok(result)
    {
        // result feldolgozása: ';'-vel elválasztva vannak az ügyfelelős;ügyintéző;kezelő -k
        //alert(result);
        
        if (result == null)
        {
            return false;
        }
        
        var resultArray = new Array();
        resultArray = result.split(';');

        if (result != '' && resultArray.length != 3)
        {
            return null;
        }
        else {

            var ugyFelelos_str = '';
            var ugyintezo_str = '';
            var kezelo_str = '';

            if (resultArray.length == 3) {
                ugyFelelos_str = resultArray[0];
                ugyintezo_str = resultArray[1];
                kezelo_str = resultArray[2];
            }

            if (ugyFelelos_str != null)
            {
                var ugyFelelosHiddenField = $get(Ugyfelelos_HiddenField_Id);
                var ugyFelelosTextBox = $get(Ugyfelelos_TextBox_Id);
                
                var ugyFelelosArray = new Array();
                ugyFelelosArray = ugyFelelos_str.split(',');
                if (ugyFelelosArray.length == 2)
                {                            
                    if (ugyFelelosHiddenField && ugyFelelosTextBox)
                    {
                        ugyFelelosHiddenField.value = ugyFelelosArray[0];
                        ugyFelelosTextBox.value = ugyFelelosArray[1];

                        ugyfelelos_lastValue = ugyFelelosArray[0];
                    }                                                        
                }
                else if (ugyFelelos_str == '')
                {
                    if (ugyFelelosHiddenField && ugyFelelosTextBox)
                    {
                        // kézzel beírtat nem írunk felül üres értékkel
                        if (ugyFelelosHiddenField.value == '' || ugyFelelosHiddenField.value == ugyfelelos_lastValue)
                        {
                            ugyFelelosHiddenField.value = '';
                            ugyFelelosTextBox.value = '';
                            
                            ugyfelelos_lastValue = '';
                        }
                    }    
                }
            }
            if (ugyintezo_str != null)
            {
                var ugyintezoHiddenField = $get(Ugyintezo_HiddenField_Id);
                var ugyintezoTextBox = $get(Ugyintezo_TextBox_Id);
                
                var ugyintezoArray = new Array();
                ugyintezoArray = ugyintezo_str.split(',');
                if (ugyintezoArray.length == 2)
                {                            
                    if (ugyintezoHiddenField && ugyintezoTextBox)
                    {
                        ugyintezoHiddenField.value = ugyintezoArray[0];
                        ugyintezoTextBox.value = ugyintezoArray[1];

                        ugyintezo_lastValue = ugyintezoArray[0];
                    }                            
                }
                else if (ugyintezo_str == '')
                {
                    if (ugyintezoHiddenField && ugyintezoTextBox)
                    {
                        // kézzel beírtat nem írunk felül üres értékkel
                        if (ugyintezoHiddenField.value == '' || ugyintezoHiddenField.value == ugyintezo_lastValue)
                        {
                            ugyintezoHiddenField.value = '';
                            ugyintezoTextBox.value = '';

                            ugyintezo_lastValue = '';
                        }
                    }   
                }
            }
            if (kezelo_str != null)
            {
                var kezeloHiddenField = $get(Kezelo_HiddenField_Id);
                var kezeloTextBox = $get(Kezelo_TextBox_Id);
                
                var kezeloArray = new Array();
                kezeloArray = kezelo_str.split(',');
                if (kezeloArray.length == 2)
                {
                    if (kezeloHiddenField && kezeloTextBox)
                    {
                        kezeloHiddenField.value = kezeloArray[0];
                        kezeloTextBox.value = kezeloArray[1];

                        kezelo_lastValue = kezeloArray[0];
                    }
                }
                else if (kezelo_str == '')
                {
                    if (kezeloHiddenField && kezeloTextBox) {
                        // kézzel beírtat nem írunk felül üres értékkel
                        if (kezeloHiddenField.value == '' || kezeloHiddenField.value == kezelo_lastValue)
                        {
                            kezeloHiddenField.value = '';
                            kezeloTextBox.value = '';

                            kezelo_lastValue = '';
                        }
                    }
                }
            }
        }     
        
    }
    
    function OnWsCallBack_SetGeneraltTargy(result)
    {
        // result feldolgozása: egyelőre csak az ügyirat tárgy mezőt töltjük

        //alert(result);
        
        if (result == null)
        {
            return false;
        }
         
        var ugyTargyTextBox = $get(UgyTargy_TextBox_Id);

        if (ugyTargyTextBox) {
            if (ugyTargyTextBox.value != '' && ugyTargyTextBox.value != ugyGeneraltTargy_lastValue) {
                return false;   // kézzel beírtat nem írunk felül
            }
            ugyTargyTextBox.value = result;
            ugyGeneraltTargy_lastValue = result;
        }
        
    }
    
    // érkeztetőkönyv dropdown:
    var emptyMessage = '[Nincs megadva elem]';
    
    Type.registerNamespace("Constants");
    
    Constants.IktatoKonyvekDropDownListMode = function() {
    }
    
    Constants.IktatoKonyvekDropDownListMode.prototype = {
        ErkeztetoKonyvek: 0,
        Iktatokonyvek: 1,
        IktatokonyvekWithRegiAdatok: 2
    }
    
    Constants.IktatoKonyvekDropDownListMode.registerEnum("Constants.IktatoKonyvekDropDownListMode", false);
    
    function fillErkeztetokonyvDropDownByEv(erkKonyv_ClientId, evTol_ClientId, evIg_ClientId, userId, userCsoportId, mode)
    {
        //alert('fillErkeztetokonyvDropDownByEv');
        
        var erkKonyv = $get(erkKonyv_ClientId);
        if (erkKonyv == null)
        {
            return false;
        }       
        
        var evTol_TextBox = $get(evTol_ClientId);
        var evIg_TextBox = $get(evIg_ClientId);
        
        if (evTol_TextBox && evIg_TextBox)
        {
            var evTol = evTol_TextBox.value;
            var evIg = evIg_TextBox.value;
            
            var _mode;
            if(arguments.length <6) _mode = Constants.IktatoKonyvekDropDownListMode.ErkeztetoKonyvek;
            else _mode = mode;
            
            Ajax_eRecord.GetErkeztetoKonyvekList(evTol,evIg,userId,userCsoportId,_mode,OnWsCallBack_FillErkKonyvDropDown, OnWsErrorCallBack_FillErkKonyvDropDown, erkKonyv);
        
        }
    }
    
    function fillAllErkeztetokonyvDropDownByEv(erkKonyv_ClientId, evTol_ClientId, evIg_ClientId, userId, userCsoportId, mode)
    {
        //alert('fillErkeztetokonyvDropDownByEv');
        
        var erkKonyv = $get(erkKonyv_ClientId);
        if (erkKonyv == null)
        {
            return false;
        }       
        
        var evTol_TextBox = $get(evTol_ClientId);
        var evIg_TextBox = $get(evIg_ClientId);
        
        if (evTol_TextBox && evIg_TextBox)
        {
            var evTol = evTol_TextBox.value;
            var evIg = evIg_TextBox.value;
            
            var _mode;
            if(arguments.length <6) _mode = Constants.IktatoKonyvekDropDownListMode.ErkeztetoKonyvek;
            else _mode = mode;
            
            Ajax_eRecord.GetAllErkeztetoKonyvekList(evTol,evIg,userId,userCsoportId,_mode,OnWsCallBack_FillErkKonyvDropDown, OnWsErrorCallBack_FillErkKonyvDropDown, erkKonyv);
        
        }
    }



    function OnWsCallBack_FillErkKonyvDropDown(returnValues, erkKonyv) { 
        if (returnValues != null && erkKonyv != null)
        {
            // selected item elmentése:
            var selectedOption = null;
            var addEmptyItem = false;
            if (erkKonyv.selectedIndex >= 0)
            {
                selectedOption = erkKonyv.options[erkKonyv.selectedIndex];
            }
            
            // dropdown törlése:
            var numberOfOptions = erkKonyv.options.length;	
            if(numberOfOptions > 0)
            {
                if(erkKonyv.options[0].value == '')
                {
                    addEmptyItem = true;
                }
            }
            var i=0;	
            for (i=0; i<numberOfOptions; i++) 
            {				 
                //Note: Always remove(0) and NOT remove(i)			 
                erkKonyv.remove(0);	
            }
            
            // üres elem hozzáadása:
            if(addEmptyItem)
            {
                var emptyOption = document.createElement('option');
                emptyOption.text = emptyMessage;
                emptyOption.value = '';
                erkKonyv.options.add(emptyOption);
            }
       
            
            var temp1;
            var err;
            try
            {
                temp1 = new Sys.Serialization.JavaScriptSerializer.deserialize(returnValues);
            }
            catch(err)
            {
                alert('Hiba az iktatókönyvek lekérése során!');
                return false;
            }
            
            //temp1 = returnValues.split(';');
            
//            if(!addEmptyItem && temp1.length == 1)
//            {
//                erkKonyv.disabled = true;
//            }
//            else
//            {
//                erkKonyv.disabled = false;
//            }
            
            for(var key in temp1)
            {
                var erkKonyvNev = temp1[key];
                var megkulJelzes = key;
                if (erkKonyvNev && megkulJelzes)
                {
                    var opt = document.createElement('option');
                    opt.text = erkKonyvNev;
                    opt.value = megkulJelzes;
                    
                    erkKonyv.options.add(opt);
                    
                    // ha ez volt a kiválasztott:
                    if (selectedOption && selectedOption.value == opt.value)
                    {
                        opt.selected = true;
                    }
                }
            }
            
            if(!addEmptyItem && erkKonyv.options.length == 1)
            {
                erkKonyv.disabled = true;
            }
            else
            {
                erkKonyv.disabled = false;
            }
//            
//            for (i=0;i<temp1.length;i++)
//            {
//                var temp2 = new Array();
//                temp2 = temp1[i].split('&');
//                var erkKonyvNev = temp2[0];
//                var megkulJelzes = temp2[1];
//                
//                if (erkKonyvNev && megkulJelzes)
//                {
//                    var opt = document.createElement('option');
//                    opt.text = erkKonyvNev;
//                    opt.value = megkulJelzes;
//                    
//                    erkKonyv.options.add(opt);
//                    
//                    // ha ez volt a kiválasztott:
//                    if (selectedOption && selectedOption.value == opt.value)
//                    {
//                        opt.selected = true;
//                    }
//                }
//            }
            
            $common.tryFireEvent(erkKonyv, 'change');
        }
        
    }
    
    function OnWsErrorCallBack_FillErkKonyvDropDown(error)
    {
        AjaxLogging.ExceptionManager.getInstance().publishException(ERROR_CODE_WEB_SERVICE, error);
    }
    
        
    function SetMerge_IrattariTetelszamTextBox()
    {
        // Ügykör_Id:
         var UgykorDropDown = $get(UgykorDropDown_Id);
         var selectedUgykor = '';
         if (UgykorDropDown)
         {
            if (UgykorDropDown.selectedIndex >= 0)
            {
                selectedUgykor = UgykorDropDown.options[UgykorDropDown.selectedIndex].value;
                if (selectedUgykor)
                {
                    Ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(
                        selectedUgykor,userId,OnWsCallBack_Merge_IrattariTetelszamTextBox);
                }                
                
            }
         }
         
    }
    
    function OnWsCallBack_Merge_IrattariTetelszamTextBox(result)
    {
        if (result == null)
        {
            return false;
        }
        
         var Merge_IrattariTetelszamTextBox = $get(Merge_IrattariTetelszamTextBox_Id);
         if (Merge_IrattariTetelszamTextBox)
         {
             Merge_IrattariTetelszamTextBox.value = result;
         }
    }