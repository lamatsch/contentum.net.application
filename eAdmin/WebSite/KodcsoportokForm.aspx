<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="KodcsoportokForm.aspx.cs" Inherits="KodcsoportokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KodcsoportokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelKod" runat="server" Text="K�d:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxKod" runat="server" />
                         </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption" >
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                    
                    <%--<tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelHossz" runat="server" Text="Hossz:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textHossz" runat="server" CssClass="mrUrlapInput"></asp:TextBox>&nbsp;
                            <ajaxToolkit:FilteredTextBoxExtender ID="filteredExtenderHossz" runat="server" FilterType="Numbers"
                                TargetControlID="textHossz">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>--%>
                    
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
     </div>
</asp:Content>