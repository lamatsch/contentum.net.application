<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KodcsoportokList.aspx.cs" Inherits="KodcsoportokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
        
    <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeKodcsoportok" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelKodcsoportok" runat="server" OnLoad="updatePanelKodcsoportok_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeKodcsoportok" runat="server" TargetControlID="panelKodcsoportok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeKodcsoportok" CollapseControlID="btnCpeKodcsoportok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeKodcsoportok"
                            ExpandedSize="0" ExpandedText="K�dcsoportok list�ja" CollapsedText="K�dcsoportok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelKodcsoportok" runat="server" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewKodcsoportok" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewKodcsoportok_RowCommand" 
                                     OnPreRender="gridViewKodcsoportok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                     DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewKodcsoportok_RowDataBound" OnSorting="gridViewKodcsoportok_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
                                            <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="Kod">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>      
                                            <asp:TemplateField>
                                                <HeaderStyle  CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkModosithato" runat="server">M�dos�that�</asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbModosithato" runat="server" Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="Hossz" HeaderText="Hossz" SortExpression="Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                       <PagerSettings Visible="False" />
                                     </asp:GridView>
                                   </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>                            
          </tr>
          <tr>
            <td style="text-align: left;" colspan="2">
            <div style="height:8px"></div>
            </td>
          </tr>
          <!--Allist�k-->
          <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeDetail" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;"> 
            <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                 CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeDetail" CollapseControlID="btnCpeDetail" ExpandDirection="Vertical"
                 AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeDetail">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="panelDetail" runat="server">
                    
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        <!--K�dt�rak lista-->
                        <ajaxToolkit:TabPanel ID="tabPanelKodtarak" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelKodtarak" runat="server">
                                    <ContentTemplate>                              
                                        <asp:Label ID="Header" runat="server" Text="K�dt�rak"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelKodtarak" runat="server" OnLoad="updatePanelKodtarak_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="panelKodtarak" runat="server" Visible="false" Width="100%">
                                        <uc1:SubListHeader ID="KodtarakSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeKodtarak" runat="server" TargetControlID="panelKodtarakList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelKodtarakList" runat="server">
                                            
                                                <asp:GridView ID="gridViewKodtarak" runat="server" AutoGenerateColumns="False" 
                                                     CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                                     OnPreRender="gridViewKodtarak_PreRender" OnRowCommand="gridViewKodtarak_RowCommand" DataKeyNames="Id" OnRowDataBound="gridViewKodtarak_RowDataBound"
                                                     OnSorting="gridViewKodtarak_Sorting">   
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>                                
                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                                 &nbsp;&nbsp;
                                                                 <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                             </HeaderTemplate>
                                                             <ItemTemplate>
                                                                 <asp:CheckBox id="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                                             </ItemTemplate>
                                                        </asp:TemplateField> 
                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField> 
                                                        <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="250px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="250px"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="Kod">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                                        </asp:BoundField>
                                                        <%--<asp:BoundField DataField="RovidNev" HeaderText="R�vidn�v" SortExpression="RovidNev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px"/>
                                                        </asp:BoundField>--%>
                                                        <asp:BoundField DataField="Egyeb" HeaderText="Egy�b" SortExpression="Egyeb">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="100px" />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>
                                                                <asp:LinkButton ID="linkModosithato" runat="server">M�dos�that�</asp:LinkButton>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbModosithato" runat="server" Enabled="false"/>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Sorrend" HeaderText="Megjelen�t�si sorrend" SortExpression="Sorrend">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="160px"/>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />   
                                                </asp:GridView>
                                              </asp:Panel>   
                                            </td>
                                          </tr>
                                        </table>                                          
                                     </asp:Panel>
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                  </asp:Panel> 
                </td>
            </tr>
        </table>
       </td>
      </tr>
   </table>
   <br /> 
</asp:Content>

