using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;

public partial class KodcsoportokList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";

        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == BoolString.Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID,gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KodcsoportokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        KodtarakSubListHeader.RowCount_Changed += new EventHandler(KodtarakSubListHeader_RowCount_Changed);

        KodtarakSubListHeader.ErvenyessegFilter_Changed += new EventHandler(KodtarakSubListHeader_ErvenyessegFilter_Changed);
    }
   
    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_KodCsoportokSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.KodcsoportokLisHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KodcsoportokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodcsoportok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("KodcsoportokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodcsoportok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewKodcsoportok.ClientID, "check", "cbModosithato");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewKodcsoportok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewKodcsoportok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewKodcsoportok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewKodcsoportok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodcsoportok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewKodcsoportok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKodcsoportok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //Allist�k gombjaihoz kliens oldali szkriptek regisztr�l�sa
        KodtarakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KodtarakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KodtarakSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        KodtarakSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewKodtarak.ClientID,"check","cbModosithato");

        //selectedRecordId kezel�se
        KodtarakSubListHeader.AttachedGridView = gridViewKodtarak;

        //Allist�k �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa

        //Allist�k esem�nykezel� f�ggv�nyei
        KodtarakSubListHeader.ButtonsClick += new
        System.Web.UI.WebControls.CommandEventHandler(KodtarakSubListHeader_ButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        //TabContainer beregisztr�l�sa a ScriptManager-ben
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //aszinkron postback control beregisztr�l�sa
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_KodCsoportokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack) 
            KodcsoportokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Kodcsoport" + CommandName.Lock);

        //Allist�k l�that�s�ga jogosults�g alapj�n
        panelKodtarak.Visible = FunctionRights.GetFunkcioJog(Page, "KodtarakList");

        //Allist�k funkci�inak enged�lyez�se jogosults�gok alapj�n
        KodtarakSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.New);
        KodtarakSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.View);
        KodtarakSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Modify);
        KodtarakSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Kodtar" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewKodcsoportok);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void KodcsoportokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewKodcsoportok", ViewState, "KRT_KodCsoportok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewKodcsoportok", ViewState);

        KodcsoportokGridViewBind(sortExpression,sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void KodcsoportokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodCsoportokSearch search = (KRT_KodCsoportokSearch)Search.GetSearchObject(Page, new KRT_KodCsoportokSearch());

        #region BUG_5937
        if (!string.IsNullOrEmpty(search.Kod.Value))
        {
            //BUG_11540
            if (!string.IsNullOrEmpty(search.ErvKezd.Value) || !string.IsNullOrEmpty(search.ErvVege.Value) || !string.IsNullOrEmpty(search.Nev.Value) ||
                (search.Modosithato.Value.ToUpper() != "X"))
                search.WhereByManual = " AND ";
            search.WhereByManual += " upper(" + search.Kod.Name + ") " + Search.GetOperatorByLikeCharater(search.Kod.Value) + " upper('" + search.Kod.Value.Replace('*', '%') + "') ";
            search.Kod.Operator = "";
        }
        #endregion

        search.OrderBy = Search.GetOrderBy("gridViewKodcsoportok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(gridViewKodcsoportok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewKodcsoportok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            BoolString.SetCheckBox(cbModosithato,drw["Modosithato"].ToString());
        }

        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewKodcsoportok_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeKodcsoportok);
        ListHeader1.RefreshPagerLabel();

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKodcsoportok);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KodcsoportokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeKodcsoportok);
        KodcsoportokGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewKodcsoportok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewKodcsoportok, selectedRowNumber, "check");

            string id = gridViewKodcsoportok.DataKeys[selectedRowNumber].Value.ToString() ;
            
            //Allist�k friss�t�se a kiv�lasztott sor f�ggv�ny�ben
            ActiveTabRefresh(TabContainer1, id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewKodcsoportok);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("KodcsoportokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodcsoportok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KodcsoportokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodcsoportok.ClientID);
            string tableName = "Krt_KodCsoportok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelKodcsoportok.ClientID);

            //Funkci�gombok enged�lyez�se a m�dos�that�s�g f�ggv�ny�ben

            //Allist�k �rv�nyes funkci�inak regisztr�l�sa
            KodtarakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("KodtarakForm.aspx"
               , "Command=" + CommandName.New + "&" + "KodcsoportID" + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarak.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelKodcsoportok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KodcsoportokGridViewBind();
                    ActiveTabRefresh(TabContainer1,UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodcsoport();
            KodcsoportokGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedKodcsoport()
    {

        if (FunctionRights.GetFunkcioJog(Page, "KodcsoportInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewKodcsoportok, EErrorPanel1, ErrorUpdatePanel);

            KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewKodcsoportok);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKodcsoportRecords();
                KodcsoportokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKodcsoportRecords();
                KodcsoportokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedKodCsoportok();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedKodcsoportRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewKodcsoportok, "KRT_KodCsoportok"
            , "KodcsoportLock", "KodcsoportForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedKodcsoportRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewKodcsoportok, "KRT_KodCsoportok"
            , "KodcsoportLock", "KodcsoportForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewKodcsoportok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedKodCsoportok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewKodcsoportok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_KodCsoportok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewKodcsoportok_Sorting(object sender, GridViewSortEventArgs e)
    {
        KodcsoportokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewKodcsoportok", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
            ActiveTabRefreshOnClientClicks();
        }
    }

    /// <summary>
    /// TabContainer ActiveTabChanged esem�nykezel�je
    /// A kiv�lasztott record adataival friss�ti az �j panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewKodcsoportok.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewKodcsoportok);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az akt�v panel friss�t�se a f� list�ban kiv�lasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JavaScripts.ResetScroll(Page, cpeKodtarak);
                KodtarakGridViewBind(masterId);
                panelKodtarak.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az akt�v panel tartalm�nak t�rl�se
    /// </summary>
    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(gridViewKodtarak);
                break;
        }
    }

    /// <summary>
    /// Az allist�k egy oldalon megjelen�tett sorok sz�m�nak be�ll�t�sa
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        KodtarakSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(tabPanelKodtarak))
        {
            gridViewKodtarak_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(gridViewKodtarak));
        }
    }

    #endregion

    #region Kodtarak Detail

    /// <summary>
    /// Allista funkci�gomjainak esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void KodtarakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodtarak();
            KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
        }
    }

    /// <summary>
    /// Kiv�lasztott record-ok t�rl�se
    /// </summary>
    private void deleteSelectedKodtarak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "KodtarInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewKodtarak, EErrorPanel1, ErrorUpdatePanel);

            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewKodtarak);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                string kodcsoport_Id = UI.GetGridViewSelectedRecordId(gridViewKodcsoportok);

                // v�ltoz�s jelz�se eRecordba (Cache friss�t�shez):            
                KodTarak.RefreshKodcsoportCacheByKodcsoportId_eRecordWebSite(Page, kodcsoport_Id);
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// A Kodtarak GridView adatk�t�se a KodcsoportID f�ggv�ny�ben alap�rtelmezett
    /// rendez�si param�terekkel
    /// </summary>
    /// <param name="KodcsoportId"></param>
    protected void KodtarakGridViewBind(String KodcsoportId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewKodtarak", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewKodtarak", ViewState);

        KodtarakGridViewBind(KodcsoportId, sortExpression, sortDirection);
    }

    /// <summary>
    /// A Kodtarak GridView adatk�t�se a KodcsoportID �s rendez�si param�terek f�ggv�ny�ben
    /// </summary>
    /// <param name="KodcsoportId"></param>
    /// <param name="SortExpression"></param>
    /// <param name="SortDirection"></param>
    protected void KodtarakGridViewBind(String KodcsoportId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(KodcsoportId))
        {
            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_KodTarakSearch _KRT_KodtarakSearch = new KRT_KodTarakSearch();
            _KRT_KodtarakSearch.OrderBy = Search.GetOrderBy("gridViewKodtarak", ViewState, SortExpression, SortDirection);
            _KRT_KodtarakSearch.KodCsoport_Id.Value = KodcsoportId;
            _KRT_KodtarakSearch.KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            KodtarakSubListHeader.SetErvenyessegFields(_KRT_KodtarakSearch.ErvKezd, _KRT_KodtarakSearch.ErvVege);

            Result res = service.GetAll(ExecParam, _KRT_KodtarakSearch);
            UI.GridViewFill(gridViewKodtarak, res, KodtarakSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKodtarak);
        }
        else
        {
            ui.GridViewClear(gridViewKodtarak);
        }
    }


    void KodtarakSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
    }

    /// <summary>
    /// Kodtarak updatepanel Load esem�nykezel�je
    /// Kodtarak panel friss�t�se, amennyiben akt�v
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void updatePanelKodtarak_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(tabPanelKodtarak))
                    //{
                    //    KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
                    //}
                    ActiveTabRefreshDetailList(tabPanelKodtarak);
                    break;
            }
        }
    }

    /// <summary>
    /// Kodtarak GridView RowCommand esem�nylezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewKodtarak, selectedRowNumber, "check");
        }
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewKodtarak_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drw = (DataRowView)e.Row.DataItem;
            CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");

            BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        }
    }

    /// <summary>
    /// A Kodtarak lista kiv�lasztott sor�ra �rv�nye funkci�gombok be�ll�t�sa
    /// </summary>
    /// <param name="selectedId"></param>
    private void gridViewKodtarak_RefreshOnClientClicks(string selectedId)
    {
        string id = selectedId;
        if (!String.IsNullOrEmpty(id))
        {
            KodtarakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("KodtarakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarak.ClientID, EventArgumentConst.refreshDetailList);
            bool modosithato = getModosithatosag(id, gridViewKodtarak);
            if (modosithato)
            {
                KodtarakSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("KodtarakForm.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarak.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                KodtarakSubListHeader.ModifyOnClientClick = jsNemModosithato;
            }
        }
    }

    /// <summary>
    /// Kodtarak GridView PreRender esem�nykezel�je
    /// Lapoz�s kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewKodtarak.PageIndex;
        gridViewKodtarak.PageIndex = KodtarakSubListHeader.PageIndex;
        
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != gridViewKodtarak.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeKodtarak);
            KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
        }
        else
        {
            UI.GridViewSetScrollable(KodtarakSubListHeader.Scrollable, cpeKodtarak);
        }

        KodtarakSubListHeader.PageCount = gridViewKodtarak.PageCount;
        KodtarakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(gridViewKodtarak);
    }

    void KodtarakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(KodtarakSubListHeader.RowCount);
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok));
    }

    /// <summary>
    /// Kodtarak GridView Sorting esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_Sorting(object sender, GridViewSortEventArgs e)
    {
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodcsoportok)
            , e.SortExpression, UI.GetSortToGridView("gridViewKodtarak", ViewState, e.SortExpression));        
    }

    #endregion

}

