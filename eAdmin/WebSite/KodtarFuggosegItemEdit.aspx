<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KodtarFuggosegItemEdit.aspx.cs" Inherits="KodtarFuggosegItemEdit" Title="K�dt�r f�gg�s�g m�dos�t�sa" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,KodcsoportokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <asp:UpdatePanel runat="server" ID="FormUpdatePanel" UpdateMode="Always">
                <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapNyitoSor">
                                <td class="mrUrlapCaption">
                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                <td class="mrUrlapMezo">
                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="LabelKodcsoportVezerlo" runat="server" Text="Vez�rl� k�dcsoport:" /></td>
                                <td class="mrUrlapMezo" colspan="3">
                                    <uc6:KodcsoportokTextBox ID="KodcsoportokVezerloTextBox" runat="server" SearchMode="true" Enabled="false" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKodcsoportFuggo" runat="server" Text="F�gg� k�dcsoport:" /></td>
                                <td class="mrUrlapMezo" colspan="3">
                                    <uc6:KodcsoportokTextBox ID="KodcsoportokFuggoTextBox" runat="server" SearchMode="true" Enabled="false" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label1" runat="server" Text="Kiv�lasztott elem:" /></td>
                                <td class="mrUrlapMezo" colspan="3">
                                    <asp:TextBox ID="TextBoxSelectedItem" runat="server" SearchMode="true" Enabled="false" Width="250px" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapMezo" colspan="5">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 49%">
                                                <asp:Label runat="server" Text="<%$Resources:Form,KodtarFuggosegItemEditUnAssigned %>" />
                                            </td>
                                            <td style="width: 49%">
                                                <asp:Label runat="server" Text="<%$Resources:Form,KodtarFuggosegItemEditAssigned %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox runat="server" ID="ListBoxUnAssigned" Width="100%" Rows="10" SelectionMode="Multiple" AutoPostBack="true" /></td>
                                            <td>
                                                <asp:ListBox runat="server" ID="ListBoxAssigned" Width="100%" Rows="10" SelectionMode="Multiple" AutoPostBack="true" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center">
                                                <asp:Button runat="server" ID="ButtonAddTo" Text=">>" OnClick="ButtonAddTo_Click" Width="80px" />
                                                <asp:Button runat="server" ID="ButtonRemoveFrom" Text="<<" OnClick="ButtonRemoveFrom_Click" Width="80px" />
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
