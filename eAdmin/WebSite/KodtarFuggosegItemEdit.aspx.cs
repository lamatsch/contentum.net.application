using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class KodtarFuggosegItemEdit : Contentum.eUtility.UI.PageBase
{
    #region MIX
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    #endregion

    #region CONSTANTS
    private const string ConstKrtTableName = "KRT_KodtarFuggoseg";
    private const string ConstCheckBoxNincs = "#CheckBox_Nincs#";
    private const string ConstCheckBoxItem = "#CheckBox_Item#";
    private const string ConstPipeLine = "|";
    private const string ConstDefaultKodtarFuggosegOrderBy = "Nev ASC";
    private const string ConsQueryStringVezerloKodtarIdParamName = "VKTID";
    private const string ConsQueryStringFuggoKodtarIdParamName = "FKTID";
    private const string ConsQueryStringEditTypeParamName = "TYPE";
    private const string ConsQueryStringIdValueParamName = "IDVALUE";
    private string ConstSessionKeyKodtarFuggosegFormReload = "ConstSessionKeyKodtarFuggosegFormReload";
    private char ConstSeparator = '|';

    public enum EnumEditItemyType
    {
        NINCS,
        FUGGO,
        VEZERLO,
        Nothing
    }
    #endregion

    #region UTILITY
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }

    #endregion

    #region PAGE EVENTS
    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);
        Session.Remove(ConstSessionKeyKodtarFuggosegFormReload);
        pageView = new PageView(Page, ViewState);
        FormHeader1.HeaderTitle = Resources.List.KodtarFuggosegListHeader;
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Command != null)
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, ConstKrtTableName + Command);
                else
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KRT_KodtarFuggosegKezeles");
                break;
        }

        #region CHECK INPUTS
        if (Command != CommandName.Modify)
            return;

        string id = null;
        if (!CheckInputId(out id))
            return;

        string vezerloKodtarId = null;
        string fuggoKodtarId = null;
        EnumEditItemyType type = EnumEditItemyType.Nothing;

        if (!CheckInputParameters(out type, out vezerloKodtarId, out fuggoKodtarId))
            return;
        #endregion

        Result result = null;
        if (!ServiceKodtarFuggosegGet(id, out result))
            return;

        if (result == null || result.Ds.Tables[0].Rows.Count < 1)
            return;

        #region SELECTED KODTAR
        Result resultSelectedKodtar = null;
        if (type == EnumEditItemyType.VEZERLO)
        {
            if (!ServiceKodtarGet(vezerloKodtarId, out resultSelectedKodtar))
                return;
            TextBoxSelectedItem.Text = ((KRT_KodTarak)resultSelectedKodtar.Record).Nev;
        }
        else if (type == EnumEditItemyType.FUGGO)
        {
            if (!ServiceKodtarGet(fuggoKodtarId, out resultSelectedKodtar))
                return;
            TextBoxSelectedItem.Text = ((KRT_KodTarak)resultSelectedKodtar.Record).Nev;
        }
        else
        {
            TextBoxSelectedItem.Text = Resources.Form.KodtarFuggosegNincsColumnTitle;
        }
        #endregion

        #region LOAD BO
        KRT_KodtarFuggoseg KRT_KodtarFuggoseg = new KRT_KodtarFuggoseg();
        DataSetHelper.LoadUpdateBusinessDocumentFromDataRow(KRT_KodtarFuggoseg, result.Ds.Tables[0].Rows[0]);

        LoadComponentsFromBusinessObject(KRT_KodtarFuggoseg);
        KodtarFuggosegDataClass data = JSONFunctions.DeSerialize(KRT_KodtarFuggoseg.Adat);
        #endregion

        #region LISTBOX
        if (type == EnumEditItemyType.VEZERLO)
        {
            SetVezerloListBox(data, vezerloKodtarId);
        }
        else if (type == EnumEditItemyType.FUGGO)
        {
            SetFuggoListBox(data, fuggoKodtarId);
        }
        else if (type == EnumEditItemyType.NINCS)
        {
            SetNincsListBox(data);
        }
        #endregion
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }


        SetVisibility();
    }

    /// <summary>
    /// Ment�s gomb folyamat ind�t�sa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        SaveButtonProcedure(e);
    }
    #endregion

    #region FILL LISTBOX
    /// <summary>
    /// ListBox kontrolok felt�lt�se
    /// </summary>
    /// <param name="data"></param>
    /// <param name="vezerloKodtarId"></param>
    private void SetVezerloListBox(KodtarFuggosegDataClass data, string vezerloKodtarId)
    {
        Result result = null;
        ServiceKodtarakGet(data.FuggoKodCsoportId, out result);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        List<KodtarFuggosegHelperClass> fuggoKcsElements = null;
        FillKodtarObjects(result, out fuggoKcsElements);

        List<KodtarFuggosegDataItemClass> items = null;
        List<KodtarFuggosegDataNincsClass> itemsNincs = null;
        FilterVezerloJsonData(data, vezerloKodtarId, out items, out itemsNincs);

        AddVezerloToListBoxes(vezerloKodtarId, fuggoKcsElements, items, itemsNincs);
    }
    /// <summary>
    ///  ListBox kontrolok felt�lt�se
    /// </summary>
    /// <param name="data"></param>
    /// <param name="fuggoKodtarId"></param>
    private void SetFuggoListBox(KodtarFuggosegDataClass data, string fuggoKodtarId)
    {
        Result result = null;
        ServiceKodtarakGet(data.VezerloKodCsoportId, out result);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        List<KodtarFuggosegHelperClass> vezerloKcsElements = null;
        FillKodtarObjects(result, out vezerloKcsElements);

        List<KodtarFuggosegDataItemClass> items = null;
        FilterFuggoJsonData(data, fuggoKodtarId, out items);

        AddFuggoToListBoxes(fuggoKodtarId, vezerloKcsElements, items);
    }
    /// <summary>
    /// ListBox kontrolok felt�lt�se
    /// </summary>
    /// <param name="data"></param>
    private void SetNincsListBox(KodtarFuggosegDataClass data)
    {
        Result result = null;
        ServiceKodtarakGet(data.VezerloKodCsoportId, out result);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        List<KodtarFuggosegHelperClass> vezerloKcsElements = null;
        FillKodtarObjects(result, out vezerloKcsElements);

        List<KodtarFuggosegDataNincsClass> itemsNincs = null;
        FilterNincsJsonData(data, data.VezerloKodCsoportId, out itemsNincs);

        AddNincsToListBoxes(vezerloKcsElements, itemsNincs);
    }

    /// <summary>
    /// Listbox kontrolok felt�lt�se
    /// </summary>
    /// <param name="vezerloId"></param>
    /// <param name="kodtarElementsFuggo"></param>
    /// <param name="items"></param>
    /// <param name="itemsNincs"></param>
    private void AddVezerloToListBoxes(string vezerloId, List<KodtarFuggosegHelperClass> kodtarElementsFuggo, List<KodtarFuggosegDataItemClass> items, List<KodtarFuggosegDataNincsClass> itemsNincs)
    {
        ListItem li = null;
        if (itemsNincs != null && itemsNincs.Count > 0)
        {
            li = new ListItem(Resources.Form.KodtarFuggosegNincsColumnTitle, EnumEditItemyType.NINCS.ToString() + ConstSeparator + vezerloId);
            li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.NINCS.ToString());
            ListBoxAssigned.Items.Add(li);
        }
        else
        {
            li = new ListItem(Resources.Form.KodtarFuggosegNincsColumnTitle, EnumEditItemyType.NINCS.ToString() + ConstSeparator + vezerloId);
            li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.NINCS.ToString());
            ListBoxUnAssigned.Items.Add(li);
        }
        foreach (KodtarFuggosegHelperClass fuggoKodtarItem in kodtarElementsFuggo)
        {
            if (items.Contains(new KodtarFuggosegDataItemClass() { FuggoKodtarId = fuggoKodtarItem.Id, VezerloKodTarId = vezerloId }))
            {
                li = new ListItem(fuggoKodtarItem.Nev, EnumEditItemyType.VEZERLO.ToString() + ConstSeparator + fuggoKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.VEZERLO.ToString());
                li.Attributes.Add(ConsQueryStringIdValueParamName, vezerloId);
                ListBoxAssigned.Items.Add(li);
            }
            else
            {
                li = new ListItem(fuggoKodtarItem.Nev, EnumEditItemyType.VEZERLO.ToString() + ConstSeparator + fuggoKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.VEZERLO.ToString());
                li.Attributes.Add(ConsQueryStringIdValueParamName, vezerloId);
                ListBoxUnAssigned.Items.Add(li);
            }
        }
    }
    /// <summary>
    /// Listbox kontrolok felt�lt�se
    /// </summary>
    /// <param name="fuggoId"></param>
    /// <param name="elementsVezerlo"></param>
    /// <param name="items"></param>
    private void AddFuggoToListBoxes(string fuggoId, List<KodtarFuggosegHelperClass> elementsVezerlo, List<KodtarFuggosegDataItemClass> items)
    {
        ListItem li = null;
        foreach (KodtarFuggosegHelperClass vezerloKodtarItem in elementsVezerlo)
        {
            if (items.Contains(new KodtarFuggosegDataItemClass() { FuggoKodtarId = fuggoId, VezerloKodTarId = vezerloKodtarItem.Id }))
            {
                li = new ListItem(vezerloKodtarItem.Nev, vezerloKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.FUGGO.ToString());
                li.Attributes.Add(ConsQueryStringIdValueParamName, fuggoId);
                ListBoxAssigned.Items.Add(li);
            }
            else
            {
                li = new ListItem(vezerloKodtarItem.Nev, vezerloKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.FUGGO.ToString());
                li.Attributes.Add(ConsQueryStringIdValueParamName, fuggoId);
                ListBoxUnAssigned.Items.Add(li);
            }
        }
    }
    /// <summary>
    /// Listbox kontrolok felt�lt�se
    /// </summary>
    /// <param name="fuggoId"></param>
    /// <param name="elementsVezerlo"></param>
    /// <param name="itemsNincs"></param>
    private void AddNincsToListBoxes(List<KodtarFuggosegHelperClass> elementsVezerlo, List<KodtarFuggosegDataNincsClass> itemsNincs)
    {
        ListItem li = null;
        foreach (KodtarFuggosegHelperClass vezerloKodtarItem in elementsVezerlo)
        {
            if (itemsNincs.Contains(new KodtarFuggosegDataNincsClass() { VezerloKodTarId = vezerloKodtarItem.Id }))
            {
                li = new ListItem(vezerloKodtarItem.Nev, vezerloKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.NINCS.ToString());
                ListBoxAssigned.Items.Add(li);
            }
            else
            {
                li = new ListItem(vezerloKodtarItem.Nev, vezerloKodtarItem.Id);
                li.Attributes.Add(ConsQueryStringEditTypeParamName, EnumEditItemyType.NINCS.ToString());
                ListBoxUnAssigned.Items.Add(li);
            }
        }
    }
    #endregion

    #region FILTER BO
    /// <summary>
    /// Adat mez� objektum sz�r�se.
    /// Csak 1 m�trix sornak megfelel� szerkeszt�shez.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <param name="Items"></param>
    /// <param name="ItemsNincs"></param>
    private void FilterVezerloJsonData(KodtarFuggosegDataClass data, string vezerloKodtarId, out List<KodtarFuggosegDataItemClass> Items, out List<KodtarFuggosegDataNincsClass> ItemsNincs)
    {
        Items = new List<KodtarFuggosegDataItemClass>();
        ItemsNincs = new List<KodtarFuggosegDataNincsClass>();
        foreach (var item in data.ItemsNincs)
        {
            if (item.VezerloKodTarId == vezerloKodtarId)
                ItemsNincs.Add(item);
        }
        foreach (var item in data.Items)
        {
            if (item.VezerloKodTarId == vezerloKodtarId)
                Items.Add(item);
        }
    }
    /// <summary>
    /// Adat mez� objektum sz�r�se.
    /// Csak 1 m�trix sornak megfelel� szerkeszt�shez.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="fuggoKodtarId"></param>
    /// <param name="Items"></param>
    /// <param name="ItemsNincs"></param>
    private void FilterFuggoJsonData(KodtarFuggosegDataClass data, string fuggoKodtarId, out List<KodtarFuggosegDataItemClass> Items)
    {
        Items = new List<KodtarFuggosegDataItemClass>();
        foreach (var item in data.Items)
        {
            if (item.FuggoKodtarId == fuggoKodtarId)
                Items.Add(item);
        }
    }
    /// <summary>
    /// Adat mez� objektum sz�r�se.
    /// Csak 1 m�trix sornak megfelel� szerkeszt�shez.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <param name="ItemsNincs"></param>
    private void FilterNincsJsonData(KodtarFuggosegDataClass data, string vezerloKodtarId, out List<KodtarFuggosegDataNincsClass> ItemsNincs)
    {
        ItemsNincs = new List<KodtarFuggosegDataNincsClass>();
        foreach (var item in data.ItemsNincs)
        {
            ItemsNincs.Add(item);
        }
    }
    #endregion

    private void FillKodtarObjects(Result result, out List<KodtarFuggosegHelperClass> kodtarElements)
    {
        kodtarElements = new List<KodtarFuggosegHelperClass>();
        KodtarFuggosegHelperClass temp;
        foreach (DataRow item in result.Ds.Tables[0].Rows)
        {
            temp = new KodtarFuggosegHelperClass();
            temp.Id = item["Id"].ToString();
            temp.KodCsoportId = item["KodCsoport_Id"].ToString();
            temp.Nev = item["Nev"].ToString();
            kodtarElements.Add(temp);
        }
    }

    #region BO <-> CONTROLS
    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="item"></param>
    private void LoadComponentsFromBusinessObject(KRT_KodtarFuggoseg item)
    {
        if (item == null)
            return;

        #region VEZERLO
        KodcsoportokVezerloTextBox.Id_HiddenField = item.Vezerlo_KodCsoport_Id;
        if (KodcsoportokVezerloTextBox.Id_HiddenField != "")
        {
            KodcsoportokVezerloTextBox.SetTextBoxById(null);
        }
        else
        {
            KodcsoportokVezerloTextBox.Text = "";
        }
        #endregion

        #region FUGGO
        KodcsoportokFuggoTextBox.Id_HiddenField = item.Fuggo_KodCsoport_Id;
        if (KodcsoportokFuggoTextBox.Id_HiddenField != "")
        {
            KodcsoportokFuggoTextBox.SetTextBoxById(null);
        }
        else
        {
            KodcsoportokFuggoTextBox.Text = "";
        }
        #endregion

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = item.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(item.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_KodtarFuggoseg GetBusinessObjectFromComponents()
    {
        KRT_KodtarFuggoseg item = new KRT_KodtarFuggoseg();
        item.Updated.SetValueAll(false);
        item.Base.Updated.SetValueAll(false);

        if (KodcsoportokVezerloTextBox.Id_HiddenField != "")
        {
            item.Vezerlo_KodCsoport_Id = KodcsoportokVezerloTextBox.Id_HiddenField;
        }
        if (KodcsoportokFuggoTextBox.Id_HiddenField != "")
        {
            item.Fuggo_KodCsoport_Id = KodcsoportokFuggoTextBox.Id_HiddenField;
        }
        item.Base.Ver = FormHeader1.Record_Ver;
        item.Base.Updated.Ver = true;

        return item;
    }
    #endregion

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            FormFooter1.SaveEnabled = false;
        }
    }

    #region CRUD PROCEDURES
    /// <summary>
    /// Start save procedure
    /// </summary>
    /// <param name="e"></param>
    private void SaveButtonProcedure(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + Command))
            {
                string inputVezerloKodtarId = null;
                string inputFuggoKodtarId = null;
                EnumEditItemyType type = EnumEditItemyType.Nothing;

                if (!CheckInputParameters(out type, out inputVezerloKodtarId, out inputFuggoKodtarId))
                    return;

                #region LOAD FRESH DATA
                string id = null;
                if (!CheckInputId(out id))
                    return;

                Result result = null;
                if (!ServiceKodtarFuggosegGet(id, out result))
                    return;

                KRT_KodtarFuggoseg KRT_KodtarFuggoseg = new KRT_KodtarFuggoseg();
                DataSetHelper.LoadUpdateBusinessDocumentFromDataRow(KRT_KodtarFuggoseg, result.Ds.Tables[0].Rows[0]);
                KodtarFuggosegDataClass data = JSONFunctions.DeSerialize(KRT_KodtarFuggoseg.Adat);
                #endregion

                data = LoadBusinessObjectFromControls(inputVezerloKodtarId, inputFuggoKodtarId, type, data);

                KRT_KodtarFuggoseg.Adat = JSONFunctions.Serialize(data);
                KRT_KodtarFuggoseg.Updated.Adat = true;

                ServiceUpdate(id, KRT_KodtarFuggoseg);
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
    /// <summary>
    /// BO felt�lt�se a kontrollb�l
    /// </summary>
    /// <param name="inputVezerloKodtarId"></param>
    /// <param name="inputFuggoKodtarId"></param>
    /// <param name="type"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    private KodtarFuggosegDataClass LoadBusinessObjectFromControls(string inputVezerloKodtarId, string inputFuggoKodtarId, EnumEditItemyType type, KodtarFuggosegDataClass data)
    {
        switch (type)
        {
            case EnumEditItemyType.NINCS:
                List<KodtarFuggosegDataNincsClass> listN = new List<KodtarFuggosegDataNincsClass>();
                foreach (ListItem item in ListBoxAssigned.Items)
                {
                    listN.Add(new KodtarFuggosegDataNincsClass()
                    {
                        Nincs = true,
                        VezerloKodTarId = item.Value
                    });
                }
                UpdateJsonDataNincs(listN, ref data);
                break;
            case EnumEditItemyType.FUGGO:
                List<KodtarFuggosegDataItemClass> listF = new List<KodtarFuggosegDataItemClass>();
                foreach (ListItem item in ListBoxAssigned.Items)
                {
                    listF.Add(new KodtarFuggosegDataItemClass()
                    {
                        VezerloKodTarId = item.Value,
                        FuggoKodtarId = inputFuggoKodtarId,
                        Aktiv = true
                    });
                }
                UpdateJsonDataFuggo(listF, inputFuggoKodtarId, ref data);
                break;
            case EnumEditItemyType.VEZERLO:
                List<KodtarFuggosegDataItemClass> listV = new List<KodtarFuggosegDataItemClass>();
                List<KodtarFuggosegDataNincsClass> listVN = new List<KodtarFuggosegDataNincsClass>();
                foreach (ListItem item in ListBoxAssigned.Items)
                {
                    string[] separated = item.Value.Split(new char[] { ConstSeparator }, StringSplitOptions.RemoveEmptyEntries);
                    if (separated.Length == 2)
                    {
                        if (separated[0] == EnumEditItemyType.VEZERLO.ToString())
                        {
                            listV.Add(new KodtarFuggosegDataItemClass()
                            {
                                VezerloKodTarId = inputVezerloKodtarId,
                                FuggoKodtarId = separated[1],
                                Aktiv = true
                            });
                        }
                        else if ((separated[0] == EnumEditItemyType.NINCS.ToString()))
                        {
                            listVN.Add(new KodtarFuggosegDataNincsClass()
                            {
                                VezerloKodTarId = inputVezerloKodtarId,
                                Nincs = true
                            });
                        }
                    }
                }
                UpdateJsonDataVezerlo(listV, listVN, inputVezerloKodtarId, ref data);
                break;
            default:
                break;
        }

        return data;
    }
    #endregion

    #region HELPER
    /// <summary>
    /// Check existing of id
    /// </summary>
    /// <returns></returns>
    private bool CheckInputId(out string id)
    {
        id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }
        return true;
    }

    private bool CheckInputParameters(out EnumEditItemyType type, out string vezerloKTId, out string fuggoKTId)
    {
        type = EnumEditItemyType.Nothing;
        vezerloKTId = null;
        fuggoKTId = null;

        string typeString = Request.QueryString.Get(ConsQueryStringEditTypeParamName);
        if (string.IsNullOrEmpty(typeString))
        {
            return false;
        }
        type = (EnumEditItemyType)Enum.Parse(typeof(EnumEditItemyType), typeString);
        switch (type)
        {
            case EnumEditItemyType.NINCS:
                break;
            case EnumEditItemyType.FUGGO:
                fuggoKTId = Request.QueryString.Get(ConsQueryStringFuggoKodtarIdParamName);
                if (string.IsNullOrEmpty(fuggoKTId))
                    return false;
                break;
            case EnumEditItemyType.VEZERLO:
                vezerloKTId = Request.QueryString.Get(ConsQueryStringVezerloKodtarIdParamName);
                if (string.IsNullOrEmpty(vezerloKTId))
                    return false;
                break;
            default:
                break;
        }
        return true;
    }
    /// <summary>
    /// Query stringben megkeresi az id param�tert
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private bool CheckInputFuggoKodtarId(out string id)
    {
        id = Request.QueryString.Get(ConsQueryStringFuggoKodtarIdParamName);
        if (String.IsNullOrEmpty(id))
        {
            // ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }
        return true;
    }
    /// <summary>
    /// L�that�s�gok be�ll�t�sa
    /// </summary>
    private void SetVisibility()
    {
        KodcsoportokVezerloTextBox.Enabled = false;
        KodcsoportokFuggoTextBox.Enabled = false;
    }
    #endregion

    #region SERVICE

    /// <summary>
    /// M�dos�t�s
    /// </summary>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    private Result ServiceUpdate(string id, KRT_KodtarFuggoseg item)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        Result result = service.Update(execParam, item);

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, string.Empty))
            {
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
            }
            else
            {
                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                JavaScripts.RegisterCloseWindowClientScript(Page, true);
            }
            Session[ConstSessionKeyKodtarFuggosegFormReload] = "true";
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        return result;
    }
    /// <summary>
    /// Lek�rdez�s azonos�t� alapj�n
    /// </summary>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    private bool ServiceKodtarFuggosegGet(string id, out Result item)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Id.Value = id;
        search.Id.Operator = Query.Operators.equals;

        Result result = item = service.GetAllWithExtension(execParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }

        return true;
    }
    /// <summary>
    /// Lek�rdez�s k�dcsoport alapj�n
    /// </summary>
    /// <param name="vezerloId"></param>
    /// <param name="fuggoKodCsoportId"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private bool ServiceKodtarakGet(string fuggoKodCsoportId, out Result result)
    {
        KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodTarakSearch search = new KRT_KodTarakSearch();
        search.KodCsoport_Id.Value = fuggoKodCsoportId;
        search.KodCsoport_Id.Operator = Query.Operators.equals;
        search.OrderBy = ConstDefaultKodtarFuggosegOrderBy;

        result = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
    }
    /// <summary>
    /// Visszaadja a k�dt�rat id alapj�n
    /// </summary>
    /// <param name="id"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    private bool ServiceKodtarGet(string id, out Result result)
    {
        KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        result = service.Get(execParam);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return false;
        }
        return true;
    }
    #endregion

    #region ASSIGN / UNASSIGN
    /// <summary>
    /// T�rl�s parancs
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonRemoveFrom_Click(object sender, EventArgs e)
    {
        List<ListItem> selected = new List<ListItem>();
        for (int i = 0; i < ListBoxAssigned.Items.Count; i++)
        {
            if (ListBoxAssigned.Items[i].Selected)
            {
                selected.Add(ListBoxAssigned.Items[i]);
            }
        }
        foreach (ListItem item in selected)
        {
            ListBoxUnAssigned.Items.Add(item);
            ListBoxAssigned.Items.Remove(item);
        }
    }
    /// <summary>
    /// Hozz�ad�s parancs
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonAddTo_Click(object sender, EventArgs e)
    {
        List<ListItem> selected = new List<ListItem>();

        for (int i = 0; i < ListBoxUnAssigned.Items.Count; i++)
        {
            if (ListBoxUnAssigned.Items[i].Selected)
            {
                selected.Add(ListBoxUnAssigned.Items[i]);
            }
        }
        foreach (ListItem item in selected)
        {
            ListBoxAssigned.Items.Add(item);
            ListBoxUnAssigned.Items.Remove(item);
        }
    }
    #endregion

    #region UPDATE JSON DATA
    /// <summary>
    /// BO friss�t�se a Nincs elemekkel
    /// </summary>
    /// <param name="toAddList"></param>
    /// <param name="data"></param>
    private void UpdateJsonDataNincs(List<KodtarFuggosegDataNincsClass> toAddList, ref KodtarFuggosegDataClass data)
    {
        data.ItemsNincs.Clear();
        data.ItemsNincs.AddRange(toAddList);
    }
    /// <summary>
    /// BO friss�t�se a F�gg� elemekkel
    /// </summary>
    /// <param name="toAddList"></param>
    /// <param name="fuggoKodtarId"></param>
    /// <param name="data"></param>
    private void UpdateJsonDataFuggo(List<KodtarFuggosegDataItemClass> toAddList, string fuggoKodtarId, ref KodtarFuggosegDataClass data)
    {
        for (int i = 0; i < data.Items.Count; i++)
        {
            if (data.Items[i].FuggoKodtarId == fuggoKodtarId)
                data.Items.Remove(data.Items[i]);
        }
        if (toAddList != null && toAddList.Count > 0)
        {
            data.Items.AddRange(toAddList);
        }
    }
    /// <summary>
    /// BO friss�t�se a Vez�rl� elemekkel
    /// </summary>
    /// <param name="toAddListVezerlo"></param>
    /// <param name="vezerloKodtarId"></param>
    /// <param name="data"></param>
    private void UpdateJsonDataVezerlo(List<KodtarFuggosegDataItemClass> toAddListVezerlo, List<KodtarFuggosegDataNincsClass> toAddListNincs, string vezerloKodtarId, ref KodtarFuggosegDataClass data)
    {
        for (int i = 0; i < data.Items.Count; i++)
        {
            if (data.Items[i].VezerloKodTarId == vezerloKodtarId)
                data.Items.Remove(data.Items[i]);
        }
        for (int i = 0; i < data.ItemsNincs.Count; i++)
        {
            if (data.ItemsNincs[i].VezerloKodTarId == vezerloKodtarId)
                data.ItemsNincs.Remove(data.ItemsNincs[i]);
        }

        if (toAddListVezerlo != null && toAddListVezerlo.Count > 0)
        {
            data.Items.AddRange(toAddListVezerlo);
        }
        if (toAddListNincs != null && toAddListNincs.Count > 0)
        {
            data.ItemsNincs.AddRange(toAddListNincs);
        }
    }
    #endregion
}
