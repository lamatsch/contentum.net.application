using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class KodtarFuggosegList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region CONSTANTS
    private const string ConstQueryDefaultOrder = "KRT_KodtarFuggoseg.LetrehozasIdo";
    private const string ConstUiGridName = "gridViewKodtarFuggoseg";
    private const string ConstKrtTableName = "KRT_KodtarFuggoseg";
    private const string ConsQueryStringParamName = "FuggoKtId";
    private string ConstSessionKeyKodtarFuggosegInsertAndOpenModify = "ConstSessionKeyKodtarFuggosegInsertAndOpenModify";
    #endregion

    #region Utils
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";

        public static void SetCheckBox(CheckBox cb, string value)
        {
            if (value == BoolString.Yes)
            {
                cb.Checked = true;
            }
            else
            {
                cb.Checked = false;
            }
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    /// <summary>
    /// A GridView sor�nak lek�rdez�se ID alapj�n
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected GridViewRow getSelectedRowByID(string ID, GridView gridView)
    {
        foreach (GridViewRow row in gridView.Rows)
        {
            if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == ID))
            {
                return row;
            }
        }
        return null;
    }

    /// <summary>
    /// GridView kiv�lasztott sor�nak m�dos�that�s�ga
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    protected bool getModosithatosag(string ID, GridView gridView)
    {
        bool modosithato = true;
        GridViewRow selectedRow = getSelectedRowByID(ID, gridView);
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, ConstKrtTableName + "List");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_KodtarFuggosegSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.KodtarFuggosegListHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("KodtarFuggosegSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarFuggoseg.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("KodtarFuggosegForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarFuggoseg.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewKodtarFuggoseg.ClientID, "check", "cbModosithato");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewKodtarFuggoseg.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewKodtarFuggoseg.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewKodtarFuggoseg.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewKodtarFuggoseg.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarFuggoseg.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewKodtarFuggoseg;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKodtarFuggoseg);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", GetDefaultSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            KodtarFuggosegGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        if (Session[ConstSessionKeyKodtarFuggosegInsertAndOpenModify] != null)
        {
            string script = "OpenNewWindow();";
            script += "var isIE = false || !!document.documentMode; if(isIE) { window.location.reload();}";
            script += "function OpenNewWindow() {";
            script += JavaScripts.SetOnClientClick_DisplayUpdateProgress(
                "KodtarFuggosegForm.aspx"
              , CommandName.Command + "=" + CommandName.Modify
              + "&" + QueryStringVars.Id + "=" + Session[ConstSessionKeyKodtarFuggosegInsertAndOpenModify].ToString()
              , Defaults.PopupWidth, Defaults.PopupHeight
              , updatePanelKodtarFuggoseg.ClientID, EventArgumentConst.refreshMasterList,
                CustomUpdateProgress1.UpdateProgress.ClientID);
            script += "}";

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "KodtarFuggosegFormUjraNyit", script, true);
            Session.Remove(ConstSessionKeyKodtarFuggosegInsertAndOpenModify);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
        }
    }
    #endregion Base Page

    #region Master List
    private KRT_KodtarFuggosegSearch GetDefaultSearch()
    {
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        if (!Page.IsPostBack)
        {
            search.Aktiv.Value = BoolString.Yes;
            search.Aktiv.Operator = Query.Operators.equals;
        }
        return search;
    }
    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void KodtarFuggosegGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState(ConstUiGridName, ViewState, ConstQueryDefaultOrder);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState(ConstUiGridName, ViewState);

        KodtarFuggosegGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void KodtarFuggosegGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_KodtarFuggosegSearch search = (KRT_KodtarFuggosegSearch)Search.GetSearchObject(Page, GetDefaultSearch());

        search.OrderBy = Search.GetOrderBy(ConstUiGridName, ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewKodtarFuggoseg, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewKodtarFuggoseg_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewKodtarFuggoseg_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeKodtarFuggoseg);
        ListHeader1.RefreshPagerLabel();

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewKodtarFuggoseg);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        KodtarFuggosegGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeKodtarFuggoseg);
        gridViewKodtarFuggoseg.PageIndex = ListHeader1.PageIndex;
        KodtarFuggosegGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewKodtarFuggoseg_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(id, gridViewKodtarFuggoseg);

            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("KodtarFuggosegForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarFuggoseg.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("KodtarFuggosegForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelKodtarFuggoseg.ClientID);
            string tableName = ConstKrtTableName;
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelKodtarFuggoseg.ClientID);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelKodtarFuggoseg_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    KodtarFuggosegGridViewBind();
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodcsoport();
            KodtarFuggosegGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedKodcsoport()
    {

        if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + "Invalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewKodtarFuggoseg, EErrorPanel1, ErrorUpdatePanel);

            KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewKodtarFuggoseg);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedKodcsoportRecords();
                KodtarFuggosegGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedKodcsoportRecords();
                KodtarFuggosegGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedKodtarFuggoseg();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedKodcsoportRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewKodtarFuggoseg, ConstKrtTableName
            , ConstKrtTableName + "Lock", ConstKrtTableName + "ForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedKodcsoportRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewKodtarFuggoseg, ConstKrtTableName
            , ConstKrtTableName + "Lock", ConstKrtTableName + "ForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewKodtarFuggoseg -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedKodtarFuggoseg()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewKodtarFuggoseg, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, ConstKrtTableName);
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewKodtarFuggoseg_Sorting(object sender, GridViewSortEventArgs e)
    {
        KodtarFuggosegGridViewBind(e.SortExpression, UI.GetSortToGridView(ConstUiGridName, ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {

    }

    /// <summary>
    /// TabContainer ActiveTabChanged esem�nykezel�je
    /// A kiv�lasztott record adataival friss�ti az �j panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewKodtarFuggoseg.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az akt�v panel friss�t�se a f� list�ban kiv�lasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JavaScripts.ResetScroll(Page, cpeKodtarFuggoseg);
                KodtarakGridViewBind(masterId);
                panelKodtarFuggoseg.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az akt�v panel tartalm�nak t�rl�se
    /// </summary>
    private void ActiveTabClear()
    {

    }

    /// <summary>
    /// Az allist�k egy oldalon megjelen�tett sorok sz�m�nak be�ll�t�sa
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {

    }

    #endregion

    #region Kodtarak Detail

    /// <summary>
    /// Allista funkci�gomjainak esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void KodtarakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedKodtarak();
            KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg));
        }
    }

    /// <summary>
    /// Kiv�lasztott record-ok t�rl�se
    /// </summary>
    private void deleteSelectedKodtarak()
    {
        if (FunctionRights.GetFunkcioJog(Page, ConstKrtTableName + "Invalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewKodtarFuggoseg, EErrorPanel1, ErrorUpdatePanel);

            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = getModosithatosag(deletableItemsList[i], gridViewKodtarFuggoseg);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                string kodcsoport_Id = UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg);

                // v�ltoz�s jelz�se eRecordba (Cache friss�t�shez):            
                KodTarak.RefreshKodcsoportCacheByKodcsoportId_eRecordWebSite(Page, kodcsoport_Id);
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// A Kodtarak GridView adatk�t�se a KodcsoportID f�ggv�ny�ben alap�rtelmezett
    /// rendez�si param�terekkel
    /// </summary>
    /// <param name="KodcsoportId"></param>
    protected void KodtarakGridViewBind(String KodcsoportId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState(ConstUiGridName, ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState(ConstUiGridName, ViewState);

        KodtarakGridViewBind(KodcsoportId, sortExpression, sortDirection);
    }

    /// <summary>
    /// A Kodtarak GridView adatk�t�se a KodcsoportID �s rendez�si param�terek f�ggv�ny�ben
    /// </summary>
    /// <param name="KodcsoportId"></param>
    /// <param name="SortExpression"></param>
    /// <param name="SortDirection"></param>
    protected void KodtarakGridViewBind(String KodcsoportId, String SortExpression, SortDirection SortDirection)
    {

    }


    void KodtarakSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg));
    }

    /// <summary>
    /// Kodtarak updatepanel Load esem�nykezel�je
    /// Kodtarak panel friss�t�se, amennyiben akt�v
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void updatePanelKodtarak_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Kodtarak GridView RowCommand esem�nylezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewKodtarFuggoseg, selectedRowNumber, "check");
        }
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewKodtarak_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    DataRowView drw = (DataRowView)e.Row.DataItem;
        //    CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");

        //    BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        //}
    }

    /// <summary>
    /// A Kodtarak lista kiv�lasztott sor�ra �rv�nye funkci�gombok be�ll�t�sa
    /// </summary>
    /// <param name="selectedId"></param>
    private void gridViewKodtarak_RefreshOnClientClicks(string selectedId)
    {

    }

    /// <summary>
    /// Kodtarak GridView PreRender esem�nykezel�je
    /// Lapoz�s kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewKodtarFuggoseg.PageIndex;

        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != gridViewKodtarFuggoseg.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeKodtarFuggoseg);
            KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg));
        }

    }

    void KodtarakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg));
    }

    /// <summary>
    /// Kodtarak GridView Sorting esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewKodtarak_Sorting(object sender, GridViewSortEventArgs e)
    {
        KodtarakGridViewBind(UI.GetGridViewSelectedRecordId(gridViewKodtarFuggoseg)
            , e.SortExpression, UI.GetSortToGridView(ConstUiGridName, ViewState, e.SortExpression));
    }

    #endregion

}

