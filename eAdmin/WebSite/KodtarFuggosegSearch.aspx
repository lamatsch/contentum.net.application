<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="KodtarFuggosegSearch.aspx.cs" Inherits="KodtarFuggosegSearch" Title="K�dt�r f�gg�s�g keres�" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,KodtarFuggosegSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="LabelKodcsoportVezerlo" runat="server" Text="Vez�rl� k�dcsoport:" /></td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <uc6:KodcsoportokTextBox ID="KodcsoportokVezerloTextBox" runat="server" SearchMode="true" />
                                    </td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKodcsoportFuggo" runat="server" Text="F�gg� k�dcsoport:" /></td>
                                    <td class="mrUrlapMezo" colspan="3">
                                        <uc6:KodcsoportokTextBox ID="KodcsoportokFuggoTextBox" runat="server" SearchMode="true" />
                                    </td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label runat="server" Text="Aktiv:" /></td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="DropDownListAktiv" runat="server">
                                            <asp:ListItem Selected="True" Value="X">-----</asp:ListItem>
                                            <asp:ListItem Text="Igen" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Nem" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server"></uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                    <td></td>
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                        <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

