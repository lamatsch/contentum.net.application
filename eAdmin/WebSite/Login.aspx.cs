using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

public partial class Login : Contentum.eUtility.UI.PageBase
{
    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";


    private void RegisterJavaScripts()
    {
        string helyettesites = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(kcs_HELYETTESITES_MOD, KodTarak.HELYETTESITES_MOD.Helyettesites, Page);
        string megbizas = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(kcs_HELYETTESITES_MOD, KodTarak.HELYETTESITES_MOD.Megbizas, Page);
        string js_AddHandler = @"
            function CsoportChangeHandler()
            {
                var ddl_csoport = $get('" + DropDownList_Csoport.ClientID + @"');
                var infoLabel = $get('" + labelInfo.ClientID + @"');

                if (ddl_csoport && infoLabel)
                {
                    var selectedValue = ddl_csoport.options[ddl_csoport.selectedIndex].value;
                    if (selectedValue)
                    {
                        var splitArray = selectedValue.split('|');

                        if (splitArray.length >= 3)
                        {
                            var helyettesitesMod = splitArray[2];
                            if (helyettesitesMod == '" + KodTarak.HELYETTESITES_MOD.Helyettesites + @"')
                            {
                                infoLabel.firstChild.nodeValue = '" + helyettesites + @"';
                            }
                            else if (helyettesitesMod == '" + KodTarak.HELYETTESITES_MOD.Megbizas + @"')
                            {
                                infoLabel.firstChild.nodeValue = '" + megbizas + @"';
                            }
                            else
                            {
                                infoLabel.firstChild.nodeValue = '';
                            }
                        }
                    }
                    else
                    {
                        infoLabel.firstChild.nodeValue = '';
                    }
                }
            }
         function pageLoad(){//be called automatically.
            
            var cascadingDropDown_csoport = $find('" + CascadingDropDown_CsoportTagsag.ClientID + @"');
            if (cascadingDropDown_csoport)
            {
                cascadingDropDown_csoport.add_selectionChanged(CsoportChangeHandler);
            }
         }
        ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "cascadingDropDown_handler", js_AddHandler, true);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Response.CacheControl = "no-cache";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Session.Remove(Constants.HelyettesitettId);

        //string Helyettesito_Id = FelhasznaloProfil.FelhasznaloId(Page);
        string Helyettesito_Id = FelhasznaloProfil.LoginUserId(Page);

        if (!String.IsNullOrEmpty(Helyettesito_Id))
        {
            Authentication.CheckHelyettesitettOrMoreGroups(Page, Helyettesito_Id);

            table_baseLogin.Visible = false;
            table_extraLogin.Visible = true;
            LoginButton.Visible = false;
            //OwnButton.Visible = true;
            //ElseButton.Visible = true;
            LoginByListButton.Visible = true;

            if (!IsPostBack)
            {
                RegisterJavaScripts();
            }

            CascadingDropDown_Felhasznalo.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
            CascadingDropDown_CsoportTagsag.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);

        }
        else
        {
            table_baseLogin.Visible = true;
            table_extraLogin.Visible = false;
            //OwnButton.Visible = false;
            //ElseButton.Visible = false;
            LoginByListButton.Visible = false;
            LoginButton.Visible = true;
            LoginType.Enabled = true;
            if (!IsPostBack)
            {
                UserName.Text = UI.GetSession(Page, Constants.UserName);

                switch (UI.GetSession(Page, Constants.LoginType))
                {
                    case "Windows":
                        LoginType.Checked = true; // defaultban....
                        break;
                    case "Basic":
                        LoginType.Checked = false;
                        break;
                    case "":
                        if (UI.GetAppSetting("DefaultLoginType") == "Windows")
                        {
                            LoginType.Checked = true;
                        }
                        else
                        {
                            LoginType.Checked = false;
                        }
                        break;
                }

                LoginType_CheckedChanged(sender, e);
            }
        }
        
    }

    //protected void OwnButton_Click(object sender, EventArgs e)
    //{
    //    Authentication.OwnButton_Click(Page);
    //}

    //protected void ElseButton_Click(object sender, EventArgs e)
    //{
    //    string helyettesitett = Helyettesitesek_DropDownList.SelectedValue;

    //    Authentication.SetHelyettesitesToSession(Page, helyettesitett);

    //    //Page.Session[Constants.HelyettesitettId] = helyettesitett;        
    //    //Page.Session[Constants.FelhasznaloId] = helyettesitett;
    //    //Authentication.OwnButton_Click(Page);


    //}

    protected void LoginByListButton_Click(object sender, EventArgs e)
    {
        string[] helyettesitesArray = DropDownList_Csoport.SelectedValue.Split("|".ToCharArray());

        //Helyettesites_Id + "|" + CsoportTag_ID_helyettesitett + "|" HelyettesitesMod;
        // A helyettesítési módot csak a belépéskori feliratozáshoz használjuk
        if (helyettesitesArray.Length >= 2)
        {
            string Helyettesites_Id = helyettesitesArray[0];
            string CsoportTag_Id = helyettesitesArray[1];

            if (String.IsNullOrEmpty(Helyettesites_Id))
            {
                Authentication.SetFelhasznaloCsoportToSession(Page, CsoportTag_Id);
                Authentication.OwnButton_Click(Page);
            }
            else
            {
                Authentication.SetHelyettesitesToSession(Page, Helyettesites_Id, CsoportTag_Id);
            }
        }
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        if (LoginType.Checked)
        {
            Session[Constants.LoginType] = "Windows";
            Session[Constants.UserName] = Page.User.Identity.Name;
            Session[Constants.Password] = "";
        }
        else
        {
            Session[Constants.LoginType] = "Basic";
            Session[Constants.UserName] = UserName.Text;
            Session[Constants.Password] = Password.Text;
        }
        //bernat.laszlo added
        string remoteAddr = Page.Request.ServerVariables["remote_addr"];
        try
        {
            Session[Constants.Munkaallomas] = System.Net.Dns.GetHostEntry(remoteAddr).HostName;
        }
        catch
        {
            Session[Constants.Munkaallomas] = remoteAddr;
        }
        //eddig

        Authentication.Login(Page);
    }

    protected void LoginType_CheckedChanged(object sender, EventArgs e)
    {
        if (LoginType.Checked)
        {
            UserName.Enabled = false;
            Password.Enabled = false;
        }
        else
        {
            UserName.Enabled = true;
            Password.Enabled = true;
        }
    }
}
