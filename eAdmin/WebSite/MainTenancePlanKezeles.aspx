﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="MaintenancePlanKezeles.aspx.cs" Inherits="MaintenancePlanKezeles" Title="Untitled Page" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    <Scripts>
        <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
    </Scripts>
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server"/>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
        
    </asp:UpdatePanel>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="saveBtn" EventName="Click" /> 
        </Triggers>
    </asp:UpdatePanel>--%>
    <asp:UpdateProgress id="UpdateProgress1" runat="server" DisplayAfter="30">
        <progresstemplate>
             <div class="updateProgress" id="UpdateProgressPanel" runat="server">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table>
                    <tr><td>
                    <img src="images/hu/egyeb/activity_indicator.gif" alt="" />
                    </td>
                    <td class="updateProgressText">
                    <asp:Label ID="Label_progress" runat="server" Text="Feldolgozás folyamatban..."></asp:Label>
                    </td></tr>
                    </table>
                    </eUI:eFormPanel> 
            </div>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="UpdateProgressPanel"
            VerticalSide="Middle" HorizontalSide="Center">
             </ajaxToolkit:AlwaysVisibleControlExtender>
        </progresstemplate>
        
    </asp:UpdateProgress>
    <%--<script type="text/javascript" language="javascript" src="JavaScripts/jquery-3.2.1.min.js"></script>--%>
    
    <table width="100%" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
        <tr>
            <td>
                <asp:GridView runat="server" ID="maintenancePlanGridView" CellPadding="0" CellSpacing="0" 
                    BorderWidth="1" GridLines="Both" AllowPaging="True" AutoGenerateColumns="false" PagerSettings-Visible="false" Width="832px">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <%--<asp:TemplateField>
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="../images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <%--<asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                            SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                            <HeaderStyle Width="25px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:CommandField>--%>
                        <asp:BoundField DataField="MTXPlanName" HeaderText="Maintenance plan név" SortExpression="MTXPlanName">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SubPlanName" HeaderText="Sub plan név" SortExpression="SubPlanName">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JobStart" HeaderText="Futás kezdete" SortExpression="JobStart">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JobEnd" HeaderText="Futás vége" SortExpression="JobEnd">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Sikeresség">
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxJobSucceeded" runat="server" Enabled="false" Checked='<%# Eval("JobSucceeded").ToString() == "True" ? true:false %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="savePanel" runat="server" Width="600px">
                    <table cellpadding="5" cellspacing="5" border="0" width="90%">
                        <tr>
                            <td colspan="2"><asp:Label ID="saveLabel" runat="server" Font-Bold="true" Text="Adatbázis mentése" /></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label1" runat="server" Text="Adatbázis kiválasztása:" /></td>
                            <td><asp:DropDownList ID="selectDDList1" runat="server" /></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label2" runat="server" Text="Mentési útvonal:" /></td>
                            <td><asp:TextBox ID="saveFileName" runat="server" Width="95%" Enabled="true" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<asp:UpdatePanel ID="updatePanelSave" runat="server">
                                <ContentTemplate>--%>
                                    <asp:Button runat="server" ID="saveBtn" OnClick="saveBtn_Click" Text="Mentés" />
                                <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>
                                
                            </td>
                        </tr>
                    </table>
                    
                    
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="loadPanel" runat="server" Width="600px">
                    <table cellpadding="5" cellspacing="5" border="0" width="90%">
                        <tr>
                            <td colspan="2"><asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Mentés visszaállítása" /></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label4" runat="server" Text="Adatbázis neve:" /></td>
                            <td>
                                <%--<asp:DropDownList ID="selectDDList2" runat="server" />--%>
                                <asp:TextBox ID="RestoreDbName" runat="server" Width="95%" Enabled="true" />
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label5" runat="server" Text="Állomány neve:" /></td>
                            <td>
                                <%--<input id="File1" type="file" />--%>
                                <%--<asp:FileUpload ID="FileUpload1" Width="95%" runat="server"  />--%>
                                <asp:TextBox ID="loadFileName" runat="server" Width="95%" Enabled="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button runat="server" ID="LoadBtn" Text="Visszaállítás" OnClick="LoadBtn_Click" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        </table>
</asp:Content>