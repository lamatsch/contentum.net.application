﻿using Contentum.eAdmin.BaseUtility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MaintenancePlanKezeles : System.Web.UI.Page
{
    private void setDefaults()
    {
        try
        {
            saveFileName.Text = String.Empty;
            RestoreDbName.Text = String.Empty;
            loadFileName.Text = String.Empty;
            if (selectDDList1.Items.Count>0)
                selectDDList1.SelectedIndex = 0;
        }
        catch (Exception)
        {

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (FunctionRights.GetFunkcioJog(Page, "MaintenancePlan"))
        {
            if (!IsPostBack)
            {
                PlanGridViewBind();
                getDatabaseLists();

                string disableItems = LoadBtn.ClientID + ".disabled = true;" + LoadBtn.ClientID + ".className = 'disableditem';"
                                + saveBtn.ClientID + ".disabled = true;" + saveBtn.ClientID + ".className = 'disableditem';$('#" + UpdateProgress1.ClientID + "').show();";
                string jsload = "if(typeof(Page_ClientValidate) != 'undefined') Page_ClientValidate('" + LoadBtn.ValidationGroup + "'); if(typeof(Page_ClientValidate) == 'undefined' || Page_IsValid){"
                            + disableItems
                            + Page.ClientScript.GetPostBackEventReference(new PostBackOptions(LoadBtn)) + ";};return false;";
                string jsSave = "if(typeof(Page_ClientValidate) != 'undefined') Page_ClientValidate('" + saveBtn.ValidationGroup + "'); if(typeof(Page_ClientValidate) == 'undefined' || Page_IsValid){"
                            + disableItems
                            + Page.ClientScript.GetPostBackEventReference(new PostBackOptions(saveBtn)) + ";};return false;";

                LoadBtn.OnClientClick = jsload;
                saveBtn.OnClientClick = jsSave;
            }
            
            ListHeader1.HeaderLabel = Resources.List.MaintenancePlanKezeles;
            ListHeader1.SetRightFunctionButtonsVisible(false);

            ListHeader1.ViewVisible = false;
            ListHeader1.ModifyVisible = false;
            ListHeader1.SearchVisible = false;
            ListHeader1.InvalidateVisible = false;
            ListHeader1.NewVisible = false;
            ListHeader1.IsFilteredLabelVisible = false;

            ListHeader1.HistoryVisible = false;

            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel.Update();
            }

            ListHeader1.AttachedGridView = maintenancePlanGridView;
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void getDatabaseLists()
    {
        MaintenancePlanService service = eAdminService.ServiceFactory.GetMaintenancePlanService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result databaseListRes = service.GetDbList(execParam);
        if (databaseListRes.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, databaseListRes);
            ErrorUpdatePanel.Update();
        }
        else
        {
            selectDDList1.Items.Add(new ListItem(String.Empty, String.Empty));
            foreach (DataRow drDatabase in databaseListRes.Ds.Tables[0].Rows)
            {
                String dbId = drDatabase["database_id"].ToString();
                String dbName = drDatabase["name"].ToString();
                selectDDList1.Items.Add(new ListItem(dbName, dbId));
            }
        }
    }

    protected void PlanGridViewBind()
    {
        MaintenancePlanService service = eAdminService.ServiceFactory.GetMaintenancePlanService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result mainTenancePlanRes = service.GetAll(execParam);
        if (mainTenancePlanRes.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, mainTenancePlanRes);
            ErrorUpdatePanel.Update();
        }
        else
        {
            UI.GridViewFill(maintenancePlanGridView, mainTenancePlanRes, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    private Boolean ReturnWithResourceError(String errorMessage)
    {
        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, errorMessage);
        return false;
    }

    private Boolean SaveDataBase()
    {
        String fileName = saveFileName.Text;
        try
        {
            if (selectDDList1.SelectedIndex == 0)
                return ReturnWithResourceError(Resources.Error.BackupDontSelectedDatabase);

            if (String.IsNullOrEmpty(fileName))
                return ReturnWithResourceError(Resources.Error.BackupDontSetSavePath);

            if (!fileName.Contains("."))
                return ReturnWithResourceError(Resources.Error.BackupFileExtensionError);

            ConnectionStringSettings connStringSettings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];
            string connectionString = connStringSettings.ConnectionString;

            String databaseName = selectDDList1.SelectedItem.Text;

            MaintenancePlanService service = eAdminService.ServiceFactory.GetMaintenancePlanService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result mainTenancePlanRes = service.BackupDb(execParam, databaseName, fileName);
            if (mainTenancePlanRes.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, mainTenancePlanRes);
                return false;
            }
        }
        catch (Exception ex)
        {
            return ReturnWithResourceError(ex.Message);
        }

        return true;
    }
    
    protected void saveBtn_Click(object sender, EventArgs e)
    {
        if (SaveDataBase())
        {
            //MessageBox.Show("Adatbázis mentés sikeresen lefutott, mentett állomány helye:\n" + fileName);
            setDefaults();
        }
    }

    private Boolean LoadDataBase()
    {
        String fileName = loadFileName.Text;
        try
        {
            if (String.IsNullOrEmpty(fileName))
                return ReturnWithResourceError(Resources.Error.RestoreDontSetFileUrl);

            if (!fileName.Contains("."))
                return ReturnWithResourceError(Resources.Error.BackupFileExtensionError);

            ConnectionStringSettings connStringSettings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];
            string connectionString = connStringSettings.ConnectionString;

            String databaseName = RestoreDbName.Text;
            if (String.IsNullOrEmpty(databaseName))
                return ReturnWithResourceError(Resources.Error.RestoreDontSetDatabaseName);

            MaintenancePlanService service = eAdminService.ServiceFactory.GetMaintenancePlanService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result mainTenancePlanRes = service.RestoreDb(execParam, databaseName, fileName);
            if (mainTenancePlanRes.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, mainTenancePlanRes);
                return false;
            }
        }
        catch (Exception ex)
        {
            return ReturnWithResourceError(ex.Message);
        }

        return true;
    }

    protected void LoadBtn_Click(object sender, EventArgs e)
    {
        if (LoadDataBase())
        {
            //MessageBox.Show("Adatbázis mentés sikeresen lefutott, mentett állomány helye:\n" + fileName);
            setDefaults();
        }
    }
}