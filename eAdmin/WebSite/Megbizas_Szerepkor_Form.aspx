<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="Megbizas_Szerepkor_Form.aspx.cs" Inherits="Megbizas_Szerepkor_Form" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Megbizas_Szerepkor_FormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegbizo" runat="server" Text="Megb�z�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:FelhasznaloTextBox ID="FelhasznaloTextBoxMegbizo" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td  class="mrUrlapCaption">
                                <asp:Label ID="labelCsoport" runat="server" Text="Csoport:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList id="Csoportok_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                            </td>
                        </tr>
                            
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelMegbizott" runat="server" Text="Felhaszn�l�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc7:FelhasznaloTextBox ID="FelhasznaloTextBoxMegbizott" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td style="width: 200px" class="mrUrlapCaption">
                                    <asp:Label ID="Label3" runat="server" Text="Megb�z�s �rv�nyess�ge:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                    </uc6:ErvenyessegCalendarControl>
                                </td>
                            </tr>
                            <tr id="tr_szerepkorok" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption" style="vertical-align:top;">
                                    <asp:Label ID="labelAtruhazhatoSzerepkorok" runat="server" Text="�truh�zhat� szerepk�r�k:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Panel ID="FelhasznaloSzerepkorokListPanel" runat="server">
                                        <asp:GridView ID="FelhasznaloSzerepkorokGridView" runat="server" CellPadding="0"
                                            CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                            AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="FelhasznaloSzerepkorokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                            Visible="false" OnClientClick="return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Szerepkor_Nev" HeaderText="Szerepk�r neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                    SortExpression="Szerepkor_Nev" HeaderStyle-Width="200px" />
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                        <br />
<%--                                        <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma:" CssClass="mrUrlapCaption"
                                            runat="server" /><asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />--%>
                                    </asp:Panel>
                                </td>
                            </tr>

                            <tr id="tr_kijelolt_tetelek_szama" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server"/>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelAtruhazhatokSzama" Text="�truh�zhat� t�telek sz�ma:" runat="server" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Label ID="labelAtruhazhatokSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td style="width: 200px" class="mrUrlapCaption_Top">
                                </td>
                                <td class="mrUrlapMezo">
                                    &nbsp;<br />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
