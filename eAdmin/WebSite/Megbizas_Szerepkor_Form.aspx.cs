using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Megbizas_Szerepkor_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private UI ui = new UI();

    private string HelyettesitesId = "";
    private string maxTetelszam = "0";

    #region Utils
    private string RemoveEmailAddressFromCsoportNev(string csoportnev)
    {
        if (String.IsNullOrEmpty(csoportnev))
        {
            return csoportnev;
        }
        else
        {
            return System.Text.RegularExpressions.Regex.Replace(csoportnev, @"\(.*@.*\)", "");
        }
    }

    private ListItem GetCsoportListItemByCsoporttagsag(string CsoportTag_Id)
    {
        ListItem resultList = null;

        KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

        search_csoporttagok.Id.Value = CsoportTag_Id;
        search_csoporttagok.Id.Operator = Query.Operators.equals;

        Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

        if (String.IsNullOrEmpty(result_csoporttagok.ErrorCode))
        {
            int cnt = result_csoporttagok.Ds.Tables[0].Rows.Count;
            if (cnt == 1)
            {
                System.Data.DataRow row = result_csoporttagok.Ds.Tables[0].Rows[0];
                //String CsoportTag_Id = row["Id"].ToString();
                String Csoport_Id = row["Csoport_Id"].ToString();
                String Csoport_Nev = RemoveEmailAddressFromCsoportNev(Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, Page));
                resultList = new ListItem(Csoport_Nev, String.Format("{0}|{1}", CsoportTag_Id, Csoport_Id));
            }
        }
        return resultList;
    }

    // a Csoportok_DropDown kiv�laszott elem�nek megadott tagj�t adja vissza
    private string GetSelectedDropDownElement(int index)
    {
        string selectedValue = Csoportok_DropDownList.SelectedValue;
        string result_item = String.Empty;
        if (!String.IsNullOrEmpty(selectedValue))
        {
            // elem
            string[] items = selectedValue.Split(new char[] { '|' });
            if (index < items.Length)
            {
                result_item = items[index];
            }
        }
        return result_item;
    }

    private string GetSelectedCsoportId()
    {
        return GetSelectedDropDownElement(1);
    }

    private string GetSelectedCsoportTagId()
    {
        return GetSelectedDropDownElement(0);
    }

    #endregion Utils

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Megbizas" + Command);
                break;
        }

        if (Command == CommandName.New)
        {
            HelyettesitesId = Request.QueryString.Get(QueryStringVars.HelyettesitesId);
            if (String.IsNullOrEmpty(HelyettesitesId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = HelyettesitesId;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Helyettesitesek businessObj = (KRT_Helyettesitesek)result.Record;
                    if (!IsPostBack)
                    {
                        LoadComponentsFromBusinessObject(businessObj);
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.View || Command == CommandName.Modify) // elvileg ilyen nem lehet
        {

            //String id = Request.QueryString.Get(QueryStringVars.Id);
            //if (String.IsNullOrEmpty(id))
            //{
            //    // nincs Id megadva:
            //    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            //}
            //else
            //{
            //    KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
            //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //    execParam.Record_Id = id;

            //    Result result = service.Get(execParam);
            //    if (String.IsNullOrEmpty(result.ErrorCode))
            //    {
            //        KRT_Helyettesitesek businessObj = (KRT_Helyettesitesek)result.Record;
            //        LoadComponentsFromBusinessObject(businessObj);
            //    }
            //    else
            //    {
            //        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            //    }
            //}
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
    + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
    + "else if (count==0) { return (confirm('" + Resources.Question.UIConfirmHeader_NoItemSelected + "\\n\\n" + Resources.Question.UICloseWindowQuestion + "'))}";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(FelhasznaloSzerepkorokGridView, FormHeader1.ErrorPanel, null).Count).ToString();

        SetClientScriptToGridViewSelectDeSelectButton(FelhasznaloSzerepkorokGridView);

        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes", "Javascripts/CheckBoxes.js");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // ha nincs mit kijel�lni, akkor nem kell a ment�sgomb
        if (labelAtruhazhatokSzamaDb.Text == "0")
        {
            FormFooter1.ImageButton_Save.Visible = false;
            tr_szerepkorok.Visible = false;
            tr_kijelolt_tetelek_szama.Visible = false;
        }

        // megb�z�s �rv�nyess�ge csak t�j�koztat�sul
        ErvenyessegCalendarControl1.ReadOnly = true;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Helyettesitesek krt_helyettesitesek)
    {
        FelhasznaloTextBoxMegbizo.Id_HiddenField = krt_helyettesitesek.Felhasznalo_ID_helyettesitett;
        FelhasznaloTextBoxMegbizo.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FelhasznaloTextBoxMegbizo.ReadOnly = true;

        Csoportok_DropDownList.Items.Clear();
        ListItem listItemCsoport = GetCsoportListItemByCsoporttagsag(krt_helyettesitesek.CsoportTag_ID_helyettesitett);
        if (listItemCsoport != null)
        {
            Csoportok_DropDownList.Items.Add(listItemCsoport);
        }
        Csoportok_DropDownList.ReadOnly = true;

        FelhasznaloTextBoxMegbizott.Id_HiddenField = krt_helyettesitesek.Felhasznalo_ID_helyettesito;
        FelhasznaloTextBoxMegbizott.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        FelhasznaloTextBoxMegbizott.ReadOnly = true;

        ErvenyessegCalendarControl1.ErvKezd = krt_helyettesitesek.HelyettesitesKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_helyettesitesek.HelyettesitesVege;

        FormHeader1.Record_Ver = krt_helyettesitesek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_helyettesitesek.Base);

        FelhasznaloSzerepkorokGridViewBind();
    }

    //// form --> business object
    //private KRT_Felhasznalo_Szerepkor GetBusinessObjectFromComponents()
    //{
    //KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = new KRT_Felhasznalo_Szerepkor();
    //krt_felhasznalo_szerepkor.Updated.SetValueAll(false);
    //krt_felhasznalo_szerepkor.Base.Updated.SetValueAll(false);

    //krt_felhasznalo_szerepkor.Szerepkor_Id = SzerepkorTextBox1.Id_HiddenField;
    //krt_felhasznalo_szerepkor.Updated.Szerepkor_Id = pageView.GetUpdatedByView(SzerepkorTextBox1);

    //krt_felhasznalo_szerepkor.Felhasznalo_Id = FelhasznaloTextBoxMegbizott.Id_HiddenField;
    //krt_felhasznalo_szerepkor.Updated.Felhasznalo_Id = pageView.GetUpdatedByView(FelhasznaloTextBoxMegbizott);

    //krt_felhasznalo_szerepkor.Csoport_Id = CsoportTextBox1.Id_HiddenField;
    //krt_felhasznalo_szerepkor.Updated.Csoport_Id = pageView.GetUpdatedByView(CsoportTextBox1);

    ////krt_felhasznalo_szerepkor.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
    ////krt_felhasznalo_szerepkor.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    ////krt_felhasznalo_szerepkor.ErvVege = ErvenyessegCalendarControl1.ErvVege;
    ////krt_felhasznalo_szerepkor.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    //ErvenyessegCalendarControl1.SetErvenyessegFields(krt_felhasznalo_szerepkor, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

    //krt_felhasznalo_szerepkor.Base.Ver = FormHeader1.Record_Ver;
    //krt_felhasznalo_szerepkor.Base.Updated.Ver = true;

    //return krt_felhasznalo_szerepkor;
    //}

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Csoportok_DropDownList);
            compSelector.Add_ComponentOnClick(FelhasznaloTextBoxMegbizott);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    protected void FelhasznaloSzerepkorokGridViewBind()
    {
        if (!String.IsNullOrEmpty(HelyettesitesId))
        {
            List<string> SzerepkorIds_Atruhazott_List = new List<string>(); ;
            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            // els� l�p�sben lek�rj�k a m�r �truh�zott szerepk�r�ket, ezeket m�r nem engedj�k ism�t �tadni
            ExecParam execParam_Atruhazott = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Felhasznalo_SzerepkorSearch search_Atruhazott = new KRT_Felhasznalo_SzerepkorSearch();
            search_Atruhazott.Helyettesites_Id.Value = HelyettesitesId;
            search_Atruhazott.Helyettesites_Id.Operator = Query.Operators.equals;

            Result result_Atruhazott = service.GetAllWithExtension(execParam_Atruhazott, search_Atruhazott);

            if (!String.IsNullOrEmpty(result_Atruhazott.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_Atruhazott);
            }

            foreach (System.Data.DataRow row in result_Atruhazott.Ds.Tables[0].Rows)
            {
                string felhasznalo_szerepkor_id = row["Szerepkor_Id"].ToString();
                if (!String.IsNullOrEmpty(felhasznalo_szerepkor_id))
                {
                    SzerepkorIds_Atruhazott_List.Add("'" + felhasznalo_szerepkor_id + "'");
                }
            }

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();

            search.Helyettesites_Id.Value = "";
            search.Helyettesites_Id.Operator = Query.Operators.isnull;


            search.Felhasznalo_Id.Value = FelhasznaloTextBoxMegbizo.Id_HiddenField;
            search.Felhasznalo_Id.Operator = Query.Operators.equals;

            // lehet konkr�t Id vagy null, ha minden csoportra vonatkozik, az�rt egyel�re ut�lag sz�rj�k
            search.CsoportTag_Id.Value = GetSelectedCsoportTagId();
            search.CsoportTag_Id.Operator = Query.Operators.isnullorequals;

            // sz�r�s csoport szerint is
            search.Csoport_Id.Value = GetSelectedCsoportId();
            search.Csoport_Id.Operator = Query.Operators.isnullorequals;

            if (SzerepkorIds_Atruhazott_List.Count > 0)
            {
                search.Szerepkor_Id.Value = String.Join(",", SzerepkorIds_Atruhazott_List.ToArray());
                search.Szerepkor_Id.Operator = Query.Operators.notinner;
            }

            search.OrderBy = "KRT_Szerepkorok.Nev";

            Result res = service.GetAllWithExtension(execParam, search);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                //// h�tulr�l kezdve kell, mert t�rl�nk, �s cs�szn�nak az indexek
                //for (int i = res.Ds.Tables[0].Rows.Count - 1; i >= 0; i--)
                //{
                //    string CsoportTag_Id = res.Ds.Tables[0].Rows[i]["CsoportTag_Id"].ToString();
                //    if (!String.IsNullOrEmpty(CsoportTag_Id) && CsoportTag_Id != GetSelectedCsoportTagId())
                //    {
                //        res.Ds.Tables[0].Rows.RemoveAt(i);
                //    }
                //}

                // tal�latok sz�m�nak ki�r�sa
                labelAtruhazhatokSzamaDb.Text = res.Ds.Tables[0].Rows.Count.ToString();
            }

            ui.GridViewFill(FelhasznaloSzerepkorokGridView, res, "", FormHeader1.ErrorPanel, null);

            ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznaloSzerepkorokGridView);

        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }

    }

    protected void FelhasznaloSzerepkorokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Megbizas" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            int selectedIds = ui.GetGridViewSelectedRows(FelhasznaloSzerepkorokGridView, FormHeader1.ErrorPanel, null).Count;

                            if (selectedIds > int.Parse(maxTetelszam))
                            {
                                string javaS = "alert ('{alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "'); return false; } ";
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                            }
                            else if (selectedIds == 0)
                            {
                                // nincs mit menteni, csak bez�rjuk az ablakot
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                                // hogy ne k�ldj�n vissza adatot a szerverre
                                EFormPanel1.Visible = false;
                            }
                            else
                            {
                                KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();

                                List<string> selectedItemsList_szerepkorok = ui.GetGridViewSelectedRows(FelhasznaloSzerepkorokGridView, FormHeader1.ErrorPanel, null);
                                String[] SzerepkorokIds = selectedItemsList_szerepkorok.ToArray();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = HelyettesitesId;

                                Result result = service.InsertSzerepkorokToMegbizas_Tomeges(execParam, SzerepkorokIds);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    /** 
                                     * <FIGYELEM!!!> 
                                     * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                                     * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                                     *  az �sszes felhaszn�l� Session-jeiben)
                                     */
                                    FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                                    /**
                                     * </FIGYELEM>
                                     */


                                    //// ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                    //JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }

                    //case CommandName.Modify:
                    //    {
                    //        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                    //        if (String.IsNullOrEmpty(recordId))
                    //        {
                    //            // nincs Id megadva:
                    //            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    //        }
                    //        else
                    //        {
                    //            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                    //            KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = GetBusinessObjectFromComponents();

                    //            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    //            execParam.Record_Id = recordId;

                    //            Result result = service.Update(execParam, krt_felhasznalo_szerepkor);

                    //            if (String.IsNullOrEmpty(result.ErrorCode))
                    //            {
                    //                JavaScripts.RegisterCloseWindowClientScript(Page);
                    //            }
                    //            else
                    //            {
                    //                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    //            }
                    //        }
                    //        break;
                    //    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

    }

    public void SetClientScriptToGridViewSelectDeSelectButton(GridView ParentGridView)
    {
        if (ParentGridView == null) return;
        if (ParentGridView.Rows.Count > 0)
        {
            ImageButton sib = (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton);
            if (sib != null)
            {
                sib.Attributes["onClick"] = "SelectAllCheckBox('" + ParentGridView.ClientID + "','check'); document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML = " + ParentGridView.Rows.Count + "; return false";
            }

            ImageButton desib = (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton);
            if (desib != null)
            {
                desib.Attributes["onClick"] = "DeSelectAllCheckBox('" + ParentGridView.ClientID + "','check'); document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML = 0; return false;";
            }
        }
    }
}
