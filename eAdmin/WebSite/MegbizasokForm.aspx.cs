using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MegbizasokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";

    private const string funkcio_MegbizasFelvetelOsszesDolgozora = "MegbizasFelvetelOsszesDolgozora";

    #region JavaScripts
    private string RegisterWarningAddSzerepkorJavaScript()
    {
        string js = "alert('" + Resources.Form.UI_Megbizas_WarningAddSzerepkorok + "');";

        return js;
    }
    #endregion JavaScripts


    #region Utils
    private ListItem[] GetCsoportokListItemsByFelhasznalo(string Felhasznalo_Id, bool bCsakFelhasznaloSzervezetbeli, bool bHierarchiabanLefele)
    {
        List<string> listSzervezetCsoportok = new List<string>();
        if (bCsakFelhasznaloSzervezetbeli == true)
        {
            string FelhasznaloSzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
            listSzervezetCsoportok.Add(FelhasznaloSzervezetId);
            if (bHierarchiabanLefele)
            {
                // minden, az aktu�lis felhaszn�l� szervezet�hez tartoz� csoport lek�rdez�se
                KRT_CsoportokService service_csoportok = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam_csoportok = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Tipus.Value = KodTarak.CsoprttagsagTipus.alszervezet;
                search_csoportok.Tipus.Operator = Query.Operators.equals;

                KRT_Csoportok szuloCsoport = new KRT_Csoportok();
                szuloCsoport.Id = FelhasznaloSzervezetId;
                Result result_alcsoportok = service_csoportok.GetAllBySzuloCsoport(execParam_csoportok, szuloCsoport, search_csoportok);

                if (String.IsNullOrEmpty(result_alcsoportok.ErrorCode))
                {
                    foreach (System.Data.DataRow row in result_alcsoportok.Ds.Tables[0].Rows)
                    {
                        string Id = row["Csoport_Id_Jogalany"].ToString();
                        if (!String.IsNullOrEmpty(Id))
                        {
                            listSzervezetCsoportok.Add(Id);
                        }
                    }
                }
            }
        }

        ListItem[] resultList = null;

        KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

        search_csoporttagok.Csoport_Id_Jogalany.Value = Felhasznalo_Id;
        search_csoporttagok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        search_csoporttagok.OrderBy = "KRT_CsoportTagok.Csoport_Id ASC, KRT_CsoportTagok.Tipus DESC";

        Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

        if (String.IsNullOrEmpty(result_csoporttagok.ErrorCode))
        {
            int cnt = result_csoporttagok.Ds.Tables[0].Rows.Count;
            if (cnt > 0)
            {
                string lastCsoport_Id = "";
                resultList = new ListItem[cnt];
                int i = 0;
                foreach (System.Data.DataRow row in result_csoporttagok.Ds.Tables[0].Rows)
                {
                    String Csoport_Id = row["Csoport_Id"].ToString();

                    if (lastCsoport_Id != Csoport_Id)
                    {
                        if (!bCsakFelhasznaloSzervezetbeli || listSzervezetCsoportok.Contains(Csoport_Id))
                        {
                            String CsoportTag_Id = row["Id"].ToString();
                            String Csoport_Nev = UI.RemoveEmailAddressFromCsoportNev(Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, Page));
                            resultList[i] = (new ListItem(Csoport_Nev, CsoportTag_Id));
                            i++;
                        }
                        lastCsoport_Id = Csoport_Id;
                    }
                }
                Array.Resize(ref resultList, i);
            }
        }
        return resultList;
    }

    private ListItem GetCsoportListItemByCsoporttagsag(string CsoportTag_Id)
    {
        ListItem resultList = null;

        if (!String.IsNullOrEmpty(CsoportTag_Id))
        {
            KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

            search_csoporttagok.Id.Value = CsoportTag_Id;
            search_csoporttagok.Id.Operator = Query.Operators.equals;

            search_csoporttagok.OrderBy = "KRT_CsoportTagok.Csoport_Id ASC, KRT_CsoportTagok.Tipus DESC";

            Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

            if (String.IsNullOrEmpty(result_csoporttagok.ErrorCode))
            {
                int cnt = result_csoporttagok.Ds.Tables[0].Rows.Count;
                if (cnt > 0)
                {
                    System.Data.DataRow row = result_csoporttagok.Ds.Tables[0].Rows[0];
                    //String CsoportTag_Id = row["Id"].ToString();
                    String Csoport_Id = row["Csoport_Id"].ToString();
                    String Csoport_Nev = UI.RemoveEmailAddressFromCsoportNev(Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, Page));
                    resultList = new ListItem(Csoport_Nev, CsoportTag_Id);
                }
            }
        }
        return resultList;
    }
    #endregion Utils

    private void SetViewControls()
    {
        Felhasznalo_ID_helyettesitett.ReadOnly = true;
        Felhasznalo_ID_helyettesito.ReadOnly = true;
        Megjegyzes.ReadOnly = true;
        MegbizasiIdo_ErvenyessegCalendarControl.ReadOnly = true;
        Csoportok_DropDownList.ReadOnly = true;

        labelMegbizo.CssClass = "mrUrlapInputWaterMarked";
        labelMegbizott.CssClass = "mrUrlapInputWaterMarked";
        labelMegjegyzes.CssClass = "mrUrlapInputWaterMarked";
        label16.CssClass = "mrUrlapInputWaterMarked";
        labelCsoport.CssClass = "mrUrlapInputWaterMarked";

    }

    private void SetModifyControls()
    {
        Felhasznalo_ID_helyettesitett.ReadOnly = true;
        Felhasznalo_ID_helyettesito.ReadOnly = true;
        Csoportok_DropDownList.ReadOnly = true;

        labelMegbizo.CssClass = "mrUrlapInputWaterMarked";
        labelMegbizott.CssClass = "mrUrlapInputWaterMarked";
        labelCsoport.CssClass = "mrUrlapInputWaterMarked";
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Megbizas" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result.Record;

                    if (krt_Helyettesitesek != null && krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                    {
                        LoadComponentsFromBusinessObject(krt_Helyettesitesek);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINemMegbizas);
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

            if (Command == CommandName.View)
            {
                SetViewControls();
            }
            if (Command == CommandName.Modify)
            {
                SetModifyControls();
            }
        }

        #region JavaScriptes d�tum ellen�rz�sek
        if (Command == CommandName.Modify || Command == CommandName.New)
        {
            FormFooter1.ImageButton_Save.OnClientClick += JavaScripts.SetCheckDateIntervallJavaScript(MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd_TextBox.ClientID
                , MegbizasiIdo_ErvenyessegCalendarControl.ErvVege_TextBox.ClientID);
        }
        
        if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_Save.OnClientClick += JavaScripts.SetDateIsInThePastJavaScript(MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd_TextBox.ClientID, "megb�z�s kezdete");
            FormFooter1.ImageButton_Save.OnClientClick += JavaScripts.SetDateIsInThePastJavaScript(MegbizasiIdo_ErvenyessegCalendarControl.ErvVege_TextBox.ClientID, "megb�z�s v�ge");
        }

        // ha m�ltbeli (az aktu�lis napot megel�z�), nem engedj�k m�dos�tani
        if (Command == CommandName.Modify)
        {
            DateTime KezdDate = new DateTime();
            if (DateTime.TryParse(MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd, out KezdDate)
                && KezdDate < DateTime.Today)
            {
                MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd_TextBox.ReadOnly = true;
            }
            else // egy�bk�nt nem lehet visszamen�leges
            {
                FormFooter1.ImageButton_Save.OnClientClick += JavaScripts.SetDateIsInThePastJavaScript(MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd_TextBox.ClientID, "megb�z�s kezdete");
            }
            //FormFooter1.ImageButton_Save.OnClientClick += JavaScripts.SetDateIsInThePastJavaScript(MegbizasiIdo_ErvenyessegCalendarControl.ErvVege_TextBox.ClientID, "megb�z�s v�ge");
        }

        #endregion JavaScriptes d�tum ellen�rz�sek


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.New)
        {
            // a megb�z� m�dos�t�sakor friss�lj�n a csoporttags�got tartalmaz� lista
            Felhasznalo_ID_helyettesitett.TextBox.TextChanged += new EventHandler(Felhasznalo_ID_helyettesitett_TextChanged);
            Felhasznalo_ID_helyettesitett.TextBox.AutoPostBack = true;
            Felhasznalo_ID_helyettesitett.RefreshCallingWindow = true;

            // A saj�t szervezet dolgoz�i, kiv�ve ha vezet� vagy van joga b�rkit felvenni 
            if (!FunctionRights.GetFunkcioJog(Page, funkcio_MegbizasFelvetelOsszesDolgozora))
            {
                string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
                if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
                {
                    Felhasznalo_ID_helyettesitett.CsakSajatSzervezetDolgozoi = true;
                    Felhasznalo_ID_helyettesito.CsakSajatSzervezetDolgozoi = true;
                }
                else if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.dolgozo)
                {
                    Felhasznalo_ID_helyettesitett.CsakSajatCsoportDolgozoi = true;
                    Felhasznalo_ID_helyettesito.CsakSajatCsoportDolgozoi = true;

                    Felhasznalo_ID_helyettesitett.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                    Felhasznalo_ID_helyettesitett.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                    Felhasznalo_ID_helyettesitett.ReadOnly = true;

                    Csoportok_DropDownList.Items.Clear();
                    //ListItem[] listItems = GetCsoportokListItemsByFelhasznalo(FelhasznaloProfil.FelhasznaloId(Page));
                    //if (listItems != null)
                    //{
                    //    Csoportok_DropDownList.Items.AddRange(listItems);
                    //}

                    // csak az aktu�lis bel�p�s szerinti csoporttags�g
                    ListItem listItem = GetCsoportListItemByCsoporttagsag(FelhasznaloProfil.FelhasznaloCsoportTagId(Page));
                    if (listItem != null)
                    {
                        Csoportok_DropDownList.Items.Add(listItem);
                    }
                    Csoportok_DropDownList.ReadOnly = true;

                }
            }
        }
        else if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    protected void Felhasznalo_ID_helyettesitett_TextChanged(object sender, EventArgs e)
    {
        Csoportok_DropDownList.Items.Clear();
        bool bCsakFelhasznaloSzervezetbeli = false;
        bool bHierarchiabanLefele = false;
        if (!String.IsNullOrEmpty(Felhasznalo_ID_helyettesitett.Id_HiddenField))
        {
            if (!FunctionRights.GetFunkcioJog(Page, funkcio_MegbizasFelvetelOsszesDolgozora))
            {
                bCsakFelhasznaloSzervezetbeli = true;
                string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
                if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
                {
                    bHierarchiabanLefele = true;
                }
            }

            ListItem[] listItems = GetCsoportokListItemsByFelhasznalo(Felhasznalo_ID_helyettesitett.Id_HiddenField, bCsakFelhasznaloSzervezetbeli, bHierarchiabanLefele);
            if (listItems != null)
            {
                Csoportok_DropDownList.Items.AddRange(listItems);
            }
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Helyettesitesek krt_Helyettesitesek)
    {
        Felhasznalo_ID_helyettesitett.Id_HiddenField = krt_Helyettesitesek.Felhasznalo_ID_helyettesitett;
        Felhasznalo_ID_helyettesitett.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Csoportok_DropDownList.Items.Clear();
        ListItem listItem = GetCsoportListItemByCsoporttagsag(krt_Helyettesitesek.CsoportTag_ID_helyettesitett);
        if (listItem != null)
        {
            Csoportok_DropDownList.Items.Add(listItem);
        }

        Felhasznalo_ID_helyettesito.Id_HiddenField = krt_Helyettesitesek.Felhasznalo_ID_helyettesito;
        Felhasznalo_ID_helyettesito.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        Megjegyzes.Text = krt_Helyettesitesek.Megjegyzes;

        // megb�z�s kezdete:
        MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd = krt_Helyettesitesek.HelyettesitesKezd;
        // megb�z�s v�ge:
        MegbizasiIdo_ErvenyessegCalendarControl.ErvVege = krt_Helyettesitesek.HelyettesitesVege;

        FormHeader1.Record_Ver = krt_Helyettesitesek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_Helyettesitesek.Base);
    }

    // form --> business object
    private KRT_Helyettesitesek GetBusinessObjectFromComponents()
    {
        KRT_Helyettesitesek krt_Helyettesitesek = new KRT_Helyettesitesek();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        krt_Helyettesitesek.Updated.SetValueAll(false);
        krt_Helyettesitesek.Base.Updated.SetValueAll(false);

        krt_Helyettesitesek.Felhasznalo_ID_helyettesitett = Felhasznalo_ID_helyettesitett.Id_HiddenField;
        // M�dos�tani ne lehessen a felhaszn�l�kat:
        if (Command == CommandName.New)
        {
            krt_Helyettesitesek.Updated.Felhasznalo_ID_helyettesitett = pageView.GetUpdatedByView(Felhasznalo_ID_helyettesitett);
        }

        krt_Helyettesitesek.CsoportTag_ID_helyettesitett = Csoportok_DropDownList.SelectedValue;
        if (Command == CommandName.New)
        {
            krt_Helyettesitesek.Updated.CsoportTag_ID_helyettesitett = pageView.GetUpdatedByView(Csoportok_DropDownList);
        }

        krt_Helyettesitesek.Felhasznalo_ID_helyettesito = Felhasznalo_ID_helyettesito.Id_HiddenField;
        if (Command == CommandName.New)
        {
            krt_Helyettesitesek.Updated.Felhasznalo_ID_helyettesito = pageView.GetUpdatedByView(Felhasznalo_ID_helyettesito);
        }

        // ezen az oldalon csak megb�z�s adhat� meg:
        krt_Helyettesitesek.HelyettesitesMod = KodTarak.HELYETTESITES_MOD.Megbizas;
        krt_Helyettesitesek.Updated.HelyettesitesMod = true;

        krt_Helyettesitesek.Megjegyzes = Megjegyzes.Text;
        krt_Helyettesitesek.Updated.Megjegyzes = pageView.GetUpdatedByView(Megjegyzes);

        krt_Helyettesitesek.HelyettesitesKezd = MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd;
        krt_Helyettesitesek.Updated.HelyettesitesKezd = pageView.GetUpdatedByView(MegbizasiIdo_ErvenyessegCalendarControl);

        krt_Helyettesitesek.HelyettesitesVege = MegbizasiIdo_ErvenyessegCalendarControl.ErvVege;
        krt_Helyettesitesek.Updated.HelyettesitesVege = pageView.GetUpdatedByView(MegbizasiIdo_ErvenyessegCalendarControl);
        
        //krt_Helyettesitesek.ErvKezd = MegbizasiIdo_ErvenyessegCalendarControl.ErvKezd;
        //krt_Helyettesitesek.Updated.ErvKezd = true;

        //krt_Helyettesitesek.ErvVege = MegbizasiIdo_ErvenyessegCalendarControl.ErvVege;
        //krt_Helyettesitesek.Updated.ErvVege = true;

        krt_Helyettesitesek.Base.Ver = FormHeader1.Record_Ver;
        krt_Helyettesitesek.Base.Updated.Ver = true;

        return krt_Helyettesitesek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Felhasznalo_ID_helyettesitett);
            compSelector.Add_ComponentOnClick(Felhasznalo_ID_helyettesito);
            compSelector.Add_ComponentOnClick(Megjegyzes);
            compSelector.Add_ComponentOnClick(MegbizasiIdo_ErvenyessegCalendarControl);

            FormFooter1.SaveEnabled = false;

        }
    }

    #region helyettes�t�s/megb�z�s l�tez�s check
    private bool CheckHelyettesitesOrMegbizasExists(ExecParam execParam, KRT_Helyettesitesek krt_Helyettesitesek, KRT_HelyettesitesekService service)
    {
        // ha van olyan bejegyz�s, ami �tfedi valamelyik megl�v�t, sz�lunk:
        // �tfed�si lehet�s�gek (egym�st �tfed� id�tartamok mellett)
        // 1. Aktu�lis defin�ci�: Helyettes�t�s, nincs csoportmegad�s
        //    �tfed:
        //    a) Helyettes�t�s, tetsz�leges csoportmegad�s
        //    b) Megb�z�s, tetsz�leges csoportmegad�s
        // 2. Aktu�lis defin�ci�: Helyettes�t�s, van csoportmegad�s
        //    �tfed:
        //    a) Helyettes�t�s, nincs csoportmegad�s
        //    b) Helyettes�t�s, egyez� csoportmegad�s
        //    b) Megb�z�s, egyez� csoportmegad�s
        // 3. Aktu�lis defin�ci�: Megb�z�s, van csoportmegad�s (itt k�telez�)
        //    �tfed:
        //    a) Helyettes�t�s, nincs csoportmegad�s
        //    b) Helyettes�t�s, egyez� csoportmegad�s
        //    b) Megb�z�s, egyez� csoportmegad�s

        KRT_HelyettesitesekSearch search_check = new KRT_HelyettesitesekSearch();

        if (!string.IsNullOrEmpty(execParam.Record_Id))
        {
            search_check.Id.Value = execParam.Record_Id;// saj�t mag�t ne vizsg�ljuk
            search_check.Id.Operator = Query.Operators.notequals;
        }

        search_check.Felhasznalo_ID_helyettesitett.Value = krt_Helyettesitesek.Felhasznalo_ID_helyettesitett;
        search_check.Felhasznalo_ID_helyettesitett.Operator = Query.Operators.equals;

        search_check.Felhasznalo_ID_helyettesito.Value = krt_Helyettesitesek.Felhasznalo_ID_helyettesito;
        search_check.Felhasznalo_ID_helyettesito.Operator = Query.Operators.equals;

        // nem lehet sem megb�z�s, sem helyettes�t�s
        //search_check.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Helyettesites;
        //search_check.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Megbizas;
        //search_check.HelyettesitesMod.Operator = Query.Operators.equals;

        // search.HelyettesitesKezd < HelyettesitesVege && search.HelyettesitesVege > HelyettesitesKezd
        search_check.HelyettesitesKezd.Value = krt_Helyettesitesek.HelyettesitesVege;
        search_check.HelyettesitesKezd.Operator = Query.Operators.lessorequal;

        search_check.HelyettesitesVege.Value = krt_Helyettesitesek.HelyettesitesKezd;
        search_check.HelyettesitesVege.Operator = Query.Operators.greaterorequal;

        Result result_Check = service.GetAllWithExtension(execParam, search_check);
        if (!String.IsNullOrEmpty(result_Check.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_Check);
            return true;
        }
        else
        {
            System.Data.DataRow[] foundRows = null;

            if (krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Helyettesites)
            {
                if (String.IsNullOrEmpty(krt_Helyettesitesek.CsoportTag_ID_helyettesitett))
                {
                    // 1. Helyettes�t�s, nincs csoportmegad�s
                    foundRows = result_Check.Ds.Tables[0].Select();
                }
                else
                {
                    // 2. Helyettes�t�s, van csoportmegad�s
                    foundRows = result_Check.Ds.Tables[0].Select("HelyettesitesMod = '" + KodTarak.HELYETTESITES_MOD.Helyettesites + "' and IsNull(CsoportTag_ID_helyettesitett, '') = ''");
                    int cnt = foundRows.Length;

                    System.Data.DataRow[] foundRows1 = result_Check.Ds.Tables[0].Select("CsoportTag_ID_helyettesitett = '" + krt_Helyettesitesek.CsoportTag_ID_helyettesitett + "'");
                    int cnt1 = foundRows1.Length;
                    if (cnt1 > 0)
                    {
                        Array.Resize(ref foundRows, cnt + cnt1);
                        Array.Copy(foundRows1, 0, foundRows, cnt, cnt1);
                    }
                }
            }
            else if (krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
            {
                // 3. Megb�z�s, van csoportmegad�s (itt k�telez�)
                foundRows = result_Check.Ds.Tables[0].Select("HelyettesitesMod = '" + KodTarak.HELYETTESITES_MOD.Helyettesites + "' and IsNull(CsoportTag_ID_helyettesitett, '') = ''");
                int cnt = foundRows.Length;

                System.Data.DataRow[] foundRows1 = result_Check.Ds.Tables[0].Select("CsoportTag_ID_helyettesitett = '" + krt_Helyettesitesek.CsoportTag_ID_helyettesitett + "'");
                int cnt1 = foundRows1.Length;
                if (cnt1 > 0)
                {
                    Array.Resize(ref foundRows, cnt + cnt1);
                    Array.Copy(foundRows1, 0, foundRows, cnt, cnt1);
                }
            }

            if (foundRows != null && foundRows.Length > 0)
            {
                // van m�r ilyen bejegyz�s:
                // Figyelmeztet�s, hogy ink�bb m�dos�tsa azt:

                string strHelyettesitesek = "";
                string strMegbizasok = "";

                string strWarningLabelText = "";
                foreach (System.Data.DataRow row in foundRows)
                {
                    string helyettesitesId = row["Id"].ToString();
                    string HelyettesitesMod = row["HelyettesitesMod"].ToString();
                    string csoportNev = row["Csoport_Nev_helyettesitett"].ToString();
                    string helyettesitesKezd = row["HelyettesitesKezd"].ToString();
                    string helyettesitesVege = row["HelyettesitesVege"].ToString();

                    if (helyettesitesKezd.Length > 10)
                    {
                        helyettesitesKezd = helyettesitesKezd.Substring(0, 10);
                    }

                    if (helyettesitesVege.Length > 10)
                    {
                        helyettesitesVege = helyettesitesVege.Substring(0, 10);
                    }

                    string strItemHeader = "<li>" + helyettesitesKezd + " - " + helyettesitesVege + (String.IsNullOrEmpty(csoportNev) ? "" : " (" + csoportNev + ")") + "</li><br />";

                    if (HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                    {
                        strMegbizasok += strItemHeader;

                        strMegbizasok += Resources.Form.UI_Megbizas_Idotartam
                            + "<a href=\"MegbizasokForm.aspx?" + QueryStringVars.Command + "=" + CommandName.Modify
                            + "&" + QueryStringVars.Id + "=" + helyettesitesId + "\" style=\"text-decoration:underline\">"
                            + Resources.Form.UI_Modositas + "</a>";

                        strMegbizasok += " " + Resources.Form.UI_Megbizas_Tartalom
                            + "<a href=\"MegbizasokList.aspx?"
                            + QueryStringVars.Id + "=" + helyettesitesId + "\" style=\"text-decoration:underline\" target=\"_blank\">"
                            + Resources.Form.UI_Modositas + "</a>" + "<br />";
                    }
                    else
                    {
                        strHelyettesitesek += strItemHeader;

                        strHelyettesitesek += Resources.Form.UI_Helyettesites_Idotartam
                            + "<a href=\"HelyettesitesekForm.aspx?" + QueryStringVars.Command + "=" + CommandName.Modify
                            + "&" + QueryStringVars.Id + "=" + helyettesitesId + "\" style=\"text-decoration:underline\">"
                                + Resources.Form.UI_Modositas + "</a>" + "<br />";
                    }
                }

                if (!String.IsNullOrEmpty(strHelyettesitesek))
                {
                    strWarningLabelText += Resources.Form.UI_MarVanIlyenHelyettesites + "<ol>" + strHelyettesitesek + "</ol>";
                }

                if (!String.IsNullOrEmpty(strMegbizasok))
                {
                    strWarningLabelText += Resources.Form.UI_MarVanIlyenMegbizas + "<ol>" + strMegbizasok + "</ol>";
                }

                if (!String.IsNullOrEmpty(strWarningLabelText))
                {
                    strWarningLabelText = Resources.Form.UI_MegbizasOrHelyettesites_InvalidateteOrModifyExisting + "<br /><br />" + strWarningLabelText;
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, strWarningLabelText);
                }
                return true;
            }
        }
        return false;
    }
    #endregion helyettes�t�s/megb�z�s l�tez�s check

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Megbizas" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                            KRT_Helyettesitesek krt_Helyettesitesek = GetBusinessObjectFromComponents();

                            if (krt_Helyettesitesek.Felhasznalo_ID_helyettesitett == krt_Helyettesitesek.Felhasznalo_ID_helyettesito)
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorNoSubstitute);
                            }
                            else
                            {
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                #region Ellen�rz�s az intervallumra, van-e m�r ilyen (akkor ink�bb m�dos�tsa azt)
                                // ha van olyan bejegyz�s, ami �tfedi valamelyik megl�v�t, sz�lunk:  
                                bool bHelyettesitesOrMegbizasExist = CheckHelyettesitesOrMegbizasExists(execParam, krt_Helyettesitesek, service);
                                if (bHelyettesitesOrMegbizasExist) return;
                                #endregion

                                Result result = service.Insert(execParam, krt_Helyettesitesek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "MegbizasAddSzerepkorWarning", RegisterWarningAddSzerepkorJavaScript(), true);
                                    // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                    // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Felhasznalo_ID_helyettesitett.Text))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }

                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }

                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                                KRT_Helyettesitesek krt_Helyettesitesek = GetBusinessObjectFromComponents();

                                //if (krt_Helyettesitesek.Felhasznalo_ID_helyettesitett == krt_Helyettesitesek.Felhasznalo_ID_helyettesito)
                                //{
                                //    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorNoSubstitute);
                                //}
                                //else
                                //{
                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    #region Ellen�rz�s az intervallumra, van-e m�r ilyen (akkor ink�bb m�dos�tsa azt)
                                    // ha van olyan bejegyz�s, ami �tfedi valamelyik megl�v�t, sz�lunk:  
                                    bool bHelyettesitesOrMegbizasExist = CheckHelyettesitesOrMegbizasExists(execParam, krt_Helyettesitesek, service);
                                    if (bHelyettesitesOrMegbizasExist) return;
                                    #endregion

                                    Result result = service.UpdateMegbizas(execParam, krt_Helyettesitesek);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                //}
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
