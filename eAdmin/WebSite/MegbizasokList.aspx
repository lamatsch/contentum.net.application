<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MegbizasokList.aspx.cs" Inherits="MegbizasokList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="MegbizasokCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="MegbizasokUpdatePanel" runat="server" OnLoad="MegbizasokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="MegbizasokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="MegbizasokCPEButton" CollapseControlID="MegbizasokCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="MegbizasokCPEButton"
                            ExpandedSize="0" ExpandedText="Megb�z�sok list�ja" CollapsedText="Megb�z�sok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="MegbizasokGridView" runat="server" OnRowCommand="MegbizasokGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="MegbizasokGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="MegbizasokGridView_Sorting"
                                            OnRowDataBound="MegbizasokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Felhasznalo_Nev_helyettesitett" HeaderText="Megb�z�" SortExpression="KRT_Felhasznalok1.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_ID_helyettesitett"
                                                    SortExpression="Felhasznalo_ID_helyettesitett">
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Csoport_Nev_helyettesitett" HeaderText="Csoport" SortExpression="KRT_Csoportok.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_Nev_helyettesito" HeaderText="Megb�zott" SortExpression="KRT_Felhasznalok2.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_ID_helyettesito"
                                                    SortExpression="Felhasznalo_ID_helyettesitot">
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="HelyettesitesKezd" HeaderText="Megb�z�s&nbsp;kezdete"
                                                    SortExpression="HelyettesitesKezd">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="HelyettesitesVege" HeaderText="Megb�z�s&nbsp;v�ge" SortExpression="HelyettesitesVege">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyz�s" SortExpression="Megjegyzes">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <%-- nem m�dos�that�k a hozz�adott jogok, ha a megb�z�s v�ge m�ltbeli id�pont --%>
                                                        <asp:CheckBox ID="cbNotExpired" runat="server" Checked='<%# (!(Eval("HelyettesitesVege") is DBNull) && DateTime.Now < (DateTime)(Eval("HelyettesitesVege"))) ? true : false %>'
                                                            Enabled="false" />
                                                        <%-- nem m�dos�thatja a rekordot, akinek ez nem megb�z�sa, �s nincs joga mindenki�t m�dos�tani --%>
                                                        <asp:CheckBox ID="cbCurrentUserAllowedToModify" runat="server" Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a megb�z�s kezdete m�ltbeli id�pont, azaz a megb�z�s m�r �rv�nybe l�pett --%>
                                                        <asp:CheckBox ID="cbStartedAtPast" runat="server" Checked='<%# (!(Eval("HelyettesitesKezd") is DBNull) && DateTime.Now > (DateTime)(Eval("HelyettesitesKezd"))) ? true : false %>'
                                                            Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a megb�z�s v�ge m�ltbeli id�pont --%>
                                                        <asp:CheckBox ID="cbEndedAtPast" runat="server" Checked='<%# (!(Eval("HelyettesitesVege") is DBNull) && DateTime.Now > (DateTime)(Eval("HelyettesitesVege"))) ? true : false %>' Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a felhaszn�l� nem jogosult vagy a v�ge m�ltbeli --%>
                                                        <asp:CheckBox ID="cbInvalidationAllowed" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="SzerepkorokTabPanel" runat="server" TabIndex="0">
                                    <HeaderTemplate>
                                        <asp:UpdatePanel ID="LabelUpdatePanelIdeIktatok" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="Label1" runat="server" Text="Megb�z�sban �tadott szerepk�r�k"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="SzerepkorokUpdatePanel" runat="server" OnLoad="SzerepkorokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="SzerepkorokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="SzerepkorokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="SzerepkorokCPE" runat="server" TargetControlID="Panel3"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel3" runat="server">
                                                                        <asp:GridView ID="SzerepkorokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="SzerepkorokGridView_Sorting"
                                                                            OnPreRender="SzerepkorokGridView_PreRender" OnRowCommand="SzerepkorokGridView_RowCommand"
                                                                            OnRowDataBound="SzerepkorokGridView_RowDataBound" OnSelectedIndexChanging="SzerepkorokGridView_SelectedIndexChanging"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Szerepkor_Nev" HeaderText="Szerepk�r neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Szerepkor_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="ErvKezd" HeaderText="�rv�nyess�g kezdete" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="ErvKezd" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="ErvVege" HeaderText="�rv�nyess�g v�ge" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="ErvVege" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemTemplate>
                                                                                        <%-- a megb�z�s idej�be es�, de m�r lez�rt hozz�rendel�s --%>
                                                                                        <asp:CheckBox ID="cbEndedAtPast" runat="server" Checked='<%# (!(Eval("ErvVege") is DBNull) && DateTime.Now > (DateTime)(Eval("ErvVege"))) ? true : false %>'
                                                                                            Enabled="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
