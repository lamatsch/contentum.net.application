<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="MegbizasokSearch.aspx.cs" Inherits="MegbizasokSearch" %>

<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc8" %>
<%--<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>--%>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,MegbizasokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label1" runat="server" Text="Megb�z�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc7:FelhasznaloTextBox ID="Felhasznalo_ID_helyettesitett" runat="server" SearchMode="true">
                                    </uc7:FelhasznaloTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Megb�zott:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc7:FelhasznaloTextBox ID="Felhasznalo_ID_helyettesito" runat="server" SearchMode="true">
                                    </uc7:FelhasznaloTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label4" runat="server" Text="Megb�z�s kezdete:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc4:DatumIntervallum_SearchCalendarControl ID="Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc4:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label5" runat="server" Text="Megb�z�s v�ge:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc4:DatumIntervallum_SearchCalendarControl ID="HelyettesitesVege_DatumIntervallum_SearchCalendarControl2"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc4:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label3" runat="server" Text="Megjegyz�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
<%--                            <tr class="urlapSor">
                                <td colspan="2" rowspan="2">
                                    <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                    </uc5:Ervenyesseg_SearchFormComponent>
                                </td>
                            </tr>--%>
                            <tr class="urlapSor">
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                </td>
                                <td class="mrUrlapMezo">
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td colspan="2">
                                    <uc8:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                        runat="server"></uc8:TalalatokSzama_SearchFormComponent>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
