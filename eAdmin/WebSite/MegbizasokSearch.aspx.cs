using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MegbizasokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_HelyettesitesekSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
               new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_HelyettesitesekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }


    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = null;
        if (searchObject != null) krt_HelyettesitesekSearch = (KRT_HelyettesitesekSearch)searchObject;

        if (krt_HelyettesitesekSearch != null)
        {
            Felhasznalo_ID_helyettesitett.Id_HiddenField = krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Value;
            Felhasznalo_ID_helyettesitett.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            Felhasznalo_ID_helyettesito.Id_HiddenField = krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Value;
            Felhasznalo_ID_helyettesito.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);
            
            Megjegyzes_TextBox.Text = krt_HelyettesitesekSearch.Megjegyzes.Value;

            Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                krt_HelyettesitesekSearch.HelyettesitesKezd);

            HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.SetComponentFromSearchObjectFields(
                krt_HelyettesitesekSearch.HelyettesitesVege);

            //Ervenyesseg_SearchFormComponent1.SetDefault(
            //    krt_HelyettesitesekSearch.ErvKezd, krt_HelyettesitesekSearch.ErvVege);
        }

    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_HelyettesitesekSearch SetSearchObjectFromComponents()
    {
        KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = (KRT_HelyettesitesekSearch)SearchHeader1.TemplateObject;
        if (krt_HelyettesitesekSearch == null)
        {
            krt_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();
        }

        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Value = Felhasznalo_ID_helyettesitett.Id_HiddenField;
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Operator = Search.GetOperatorByLikeCharater(Felhasznalo_ID_helyettesitett.Id_HiddenField);
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Value = Felhasznalo_ID_helyettesito.Id_HiddenField;
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Operator = Search.GetOperatorByLikeCharater(Felhasznalo_ID_helyettesito.Id_HiddenField);

        krt_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Megbizas;
        krt_HelyettesitesekSearch.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;

        Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            krt_HelyettesitesekSearch.HelyettesitesKezd);

        HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(
            krt_HelyettesitesekSearch.HelyettesitesVege);

        krt_HelyettesitesekSearch.Megjegyzes.Value = Megjegyzes_TextBox.Text;
                
        //Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
        //    krt_HelyettesitesekSearch.ErvKezd, krt_HelyettesitesekSearch.ErvVege);

        return krt_HelyettesitesekSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_HelyettesitesekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }       
    }


    private KRT_HelyettesitesekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();
        krt_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Megbizas;
        krt_HelyettesitesekSearch.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;

        return krt_HelyettesitesekSearch;
    }


    //private void setDefaultValues()
    //{
    //    Felhasznalo_ID_helyettesitett.Text = "*";

    //    Ervenyesseg_SearchFormComponent1.SetDefault();
    //    TalalatokSzama_SearchFormComponent1.SetDefault();
    //}
}
