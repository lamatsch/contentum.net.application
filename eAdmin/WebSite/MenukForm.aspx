<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="MenukForm.aspx.cs" Inherits="MenukForm" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc10" %>
    
    
    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>

<%@ Register Src="Component/ModulTextBox.ascx" TagName="ModulTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/MenuTextBox.ascx" TagName="MenuTextBox" TagPrefix="uc9" %>

<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc7" %>


<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,MenukFormHeaderTitle%>" />
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="Megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Nev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label_req_modul" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                &nbsp;<asp:Label ID="Label10" runat="server" Text="Link (modul):"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:ModulTextBox ID="ModulTextBox1" runat="server" Validate="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                <asp:Label ID="Label14" runat="server" Text="Link param�terei:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:TextBox ID="Parameter_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                &nbsp;<asp:Label ID="Label8" runat="server" Text="Hozz�rendelt funkci�:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc7:FunkcioTextBox ID="FunkcioTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                            <asp:Label ID="Label6" runat="server" Text="Alkalmaz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc10:KodtarakDropDownList ID="KodtarakDropDownList1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                &nbsp;</td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="Fomenu_CheckBox" runat="server" Text="F�men�" AutoPostBack="false" CausesValidation="false" /></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                <asp:Label ID="Label_req_szulomenu" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label_SzuloMenu" runat="server" Text="Sz�l� men�:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc9:MenuTextBox ID="Szulomenu_MenuTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label_ImageURL" runat="server" Text="K�p URL (F�men�n�l):" Enabled="False"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="ImageURL_TextBox" runat="server" CssClass="mrUrlapInput" Enabled="False"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label2" runat="server" Text="Sorrend:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Sorrend_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px" >
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
