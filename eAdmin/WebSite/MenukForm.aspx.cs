using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MenukForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcsKod_Alkalmazas = "ALKALMAZAS";

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        ModulTextBox1.ReadOnly = true;
        Parameter_TextBox.ReadOnly = true;
        FunkcioTextBox1.ReadOnly = true;
        KodtarakDropDownList1.ReadOnly = true;
        Fomenu_CheckBox.Enabled = false;
        Szulomenu_MenuTextBox.ReadOnly = true;
        ImageURL_TextBox.ReadOnly = true;
        Sorrend_TextBox.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Menu" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Menuk krt_menuk = (KRT_Menuk)result.Record;
                    LoadComponentsFromBusinessObject(krt_menuk);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string szuloMenuId = Request.QueryString.Get(QueryStringVars.SzuloMenuId);
            if (!String.IsNullOrEmpty(szuloMenuId))
            {
                Szulomenu_MenuTextBox.Id_HiddenField = szuloMenuId;
                KRT_Menuk szuloMenu = Szulomenu_MenuTextBox.SetMenuTextBoxById(FormHeader1.ErrorPanel);
                Szulomenu_MenuTextBox.Enabled = false;
                Fomenu_CheckBox.Checked = false;
                Fomenu_CheckBox.Enabled = false;
                if (szuloMenu != null)
                {
                    KodtarakDropDownList1.FillAndSetSelectedValue(kcsKod_Alkalmazas, szuloMenu.Kod, FormHeader1.ErrorPanel);
                    KodtarakDropDownList1.Enabled = false;
                }                
            }
            else if (!IsPostBack)
            {
                KodtarakDropDownList1.FillDropDownList(kcsKod_Alkalmazas, FormHeader1.ErrorPanel);
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);        

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        Szulomenu_MenuTextBox.SetLovOnClickToFiltered(KodtarakDropDownList1.DropDownList.ClientID);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Save.OnClientClick = "enableOrDisableValidator('" + Fomenu_CheckBox.ClientID
               + "',1,['"
               + Szulomenu_MenuTextBox.Validator.ClientID
               + "','"
               + ModulTextBox1.Validator.ClientID
               + "']);";
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Menuk krt_menuk)
    {
        Nev.Text = krt_menuk.Nev;

        ModulTextBox1.Id_HiddenField = krt_menuk.Modul_Id;
        ModulTextBox1.SetModulTextBoxById(FormHeader1.ErrorPanel);

        Parameter_TextBox.Text = krt_menuk.Parameter;

        FunkcioTextBox1.Id_HiddenField = krt_menuk.Funkcio_Id;
        FunkcioTextBox1.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);

        KodtarakDropDownList1.FillAndSetSelectedValue(kcsKod_Alkalmazas, krt_menuk.Kod, FormHeader1.ErrorPanel);
        if (Command == CommandName.View) KodtarakDropDownList1.Enabled = false;

        if (String.IsNullOrEmpty(krt_menuk.Menu_Id_Szulo))
        {
            // f�men�
            Fomenu_CheckBox.Checked = true;

            ModulTextBox1.Validate = false;
            Szulomenu_MenuTextBox.Validate = false;
            ImageURL_TextBox.Enabled = true;
            Label_ImageURL.Enabled = true;
            //Szulomenu_MenuTextBox.Enabled = false;
            Label_SzuloMenu.Enabled = false;
            UI.SetComponentToJSNonVisible(Label_req_szulomenu);
            Szulomenu_MenuTextBox.SetButtonsToJSNonVisible();
        }
        else 
        { 
            // nem f�men�
            Fomenu_CheckBox.Checked = false; 
        }

        Szulomenu_MenuTextBox.Id_HiddenField = krt_menuk.Menu_Id_Szulo;
        Szulomenu_MenuTextBox.SetMenuTextBoxById(FormHeader1.ErrorPanel);

        ImageURL_TextBox.Text = krt_menuk.ImageURL;
        Sorrend_TextBox.Text = krt_menuk.Sorrend;
        
        ErvenyessegCalendarControl1.ErvKezd = krt_menuk.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_menuk.ErvVege;

        FormHeader1.Record_Ver = krt_menuk.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_menuk.Base);

        if (Command == CommandName.View)
        {
            Nev.ReadOnly = true;
            ModulTextBox1.ReadOnly = true;
            Parameter_TextBox.ReadOnly = true;
            FunkcioTextBox1.ReadOnly = true;
            KodtarakDropDownList1.Enabled = false;
            Fomenu_CheckBox.Enabled = false;
            Szulomenu_MenuTextBox.Enabled = false;
            Sorrend_TextBox.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private KRT_Menuk GetBusinessObjectFromComponents()
    {
        KRT_Menuk krt_menuk = new KRT_Menuk();
        krt_menuk.Updated.SetValueAll(false);
        krt_menuk.Base.Updated.SetValueAll(false);

        krt_menuk.Nev = Nev.Text;
        krt_menuk.Updated.Nev = pageView.GetUpdatedByView(Nev);

        krt_menuk.Modul_Id = ModulTextBox1.Id_HiddenField;
        krt_menuk.Updated.Modul_Id = pageView.GetUpdatedByView(ModulTextBox1);

        krt_menuk.Parameter = Parameter_TextBox.Text;
        krt_menuk.Updated.Parameter = pageView.GetUpdatedByView(Parameter_TextBox);

        krt_menuk.Funkcio_Id = FunkcioTextBox1.Id_HiddenField;
        krt_menuk.Updated.Funkcio_Id = pageView.GetUpdatedByView(FunkcioTextBox1);

        krt_menuk.Kod = KodtarakDropDownList1.SelectedValue;
        krt_menuk.Updated.Kod = pageView.GetUpdatedByView(KodtarakDropDownList1);

        if (Fomenu_CheckBox.Checked == false)
        {
            krt_menuk.Menu_Id_Szulo = Szulomenu_MenuTextBox.Id_HiddenField;
            krt_menuk.Updated.Menu_Id_Szulo = pageView.GetUpdatedByView(Szulomenu_MenuTextBox);
        }

        krt_menuk.ImageURL = ImageURL_TextBox.Text;
        krt_menuk.Updated.ImageURL = pageView.GetUpdatedByView(ImageURL_TextBox);

        krt_menuk.Sorrend = Sorrend_TextBox.Text;
        krt_menuk.Updated.Sorrend = pageView.GetUpdatedByView(Sorrend_TextBox);

        //krt_menuk.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_menuk.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_menuk.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_menuk.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_menuk, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_menuk.Base.Ver = FormHeader1.Record_Ver;
        krt_menuk.Base.Updated.Ver = true;
      
        return krt_menuk;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(ModulTextBox1);
            compSelector.Add_ComponentOnClick(Parameter_TextBox);
            compSelector.Add_ComponentOnClick(FunkcioTextBox1);
            compSelector.Add_ComponentOnClick(KodtarakDropDownList1);
            compSelector.Add_ComponentOnClick(Fomenu_CheckBox);
            compSelector.Add_ComponentOnClick(Szulomenu_MenuTextBox);
            compSelector.Add_ComponentOnClick(ImageURL_TextBox);
            compSelector.Add_ComponentOnClick(Sorrend_TextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

            ImageURL_TextBox.Enabled = true;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Menu" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
                            KRT_Menuk krt_menuk = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_menuk);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
                                KRT_Menuk krt_menuk = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_menuk);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    
    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        Fomenu_CheckBox.Attributes["onclick"] =
        "enableOrDisableComponents('" + Fomenu_CheckBox.ClientID
                + "',0,['"
                + ImageURL_TextBox.ClientID
                + "','"
                + Label_ImageURL.ClientID
                + "']); "
        + " enableOrDisableComponents('" + Fomenu_CheckBox.ClientID
                + "',1,['"
                + Label_SzuloMenu.ClientID
                + "']);"
        + " showOrHideComponents('" + Fomenu_CheckBox.ClientID
                + "',1,['"
                + Szulomenu_MenuTextBox.ImageButton_Lov.ClientID
                + "','"
                + Szulomenu_MenuTextBox.ImageButton_New.ClientID
                + "','"
                + Szulomenu_MenuTextBox.ImageButton_View.ClientID
                + "','"
                + Label_req_modul.ClientID
                + "','"
                + Label_req_szulomenu.ClientID
                + "']);";
    }
        

}
