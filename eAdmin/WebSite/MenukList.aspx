<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MenukList.aspx.cs" Inherits="MenukList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
<%--Tablazat / Grid--%> 

    <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="MenukCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">
        <asp:UpdatePanel ID="MenukUpdatePanel" runat="server" OnLoad="MenukUpdatePanel_Load">
            <ContentTemplate>
                <ajaxToolkit:CollapsiblePanelExtender ID="MenukCPE" runat="server" TargetControlID="Panel1"
                    CollapsedSize="20" Collapsed="False" ExpandControlID="MenukCPEButton" CollapseControlID="MenukCPEButton"
                    ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                    ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="MenukCPEButton"
                    ExpandedSize="0" ExpandedText="Szerepk�r�k list�ja" CollapsedText="Szerepk�r�k list�ja">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="Panel1" runat="server">
                    <table style="width: 98%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                <asp:GridView ID="MenukGridView" runat="server" OnRowCommand="MenukGridView_RowCommand"
                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="MenukGridView_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="MenukGridView_Sorting"
                                    OnRowDataBound="MenukGridView_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="Nev" HeaderStyle-Width="200px" />
                                        <asp:BoundField DataField="Kod" HeaderText="K�d" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="Kod" HeaderStyle-Width="100px" />
                                            <asp:TemplateField>
                                                <HeaderStyle  CssClass="GridViewBorderHeader" Width="120px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="linkFomenu" runat="server">F�men�</asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbFomenu" runat="server" Enabled="false"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
            </td>                            
    </tr>
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>

    <tr>   
    <td style="text-align: left; vertical-align: top; width: 0px;">
    <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
    </td>
     <td style="text-align: left; vertical-align: top; width: 100%;"> 
     <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                
                
            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel8"
            CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton"
            >
            
            </ajaxToolkit:CollapsiblePanelExtender>
            
                <asp:Panel ID="Panel8" runat="server">

                
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" 
                        OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        
                        <ajaxToolkit:TabPanel ID="AlmenukTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelAlmenuk" runat="server">
                                    <ContentTemplate>                              
                                        <asp:Label ID="Header" runat="server" Text="Almen�k"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                        
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="AlmenukUpdatePanel" runat="server" OnLoad="AlmenukUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="AlmenukPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="AlmenukSubListHeader" runat="server" />

                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                            <ajaxToolkit:CollapsiblePanelExtender ID="AlmenukCPE" runat="server" TargetControlID="Panel2"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0"
                                              >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="Panel2" runat="server">

                                            <asp:GridView ID="AlmenukGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="AlmenukGridView_Sorting" OnPreRender="AlmenukGridView_PreRender" OnRowCommand="AlmenukGridView_RowCommand" DataKeyNames="Id" >        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>  
                                                <asp:BoundField DataField="Nev" HeaderText="N�v" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Nev" HeaderStyle-Width="200px" /> 
                                                <asp:BoundField DataField="Kod" HeaderText="K�d" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Kod" HeaderStyle-Width="100px" />    
                                                <asp:BoundField DataField="Sorrend" HeaderText="Sorrend" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Sorrend" HeaderStyle-Width="50px" />                      
                                                              
                                                </Columns>                
                                            </asp:GridView> 
                                            </asp:Panel>
                                            
                                            </td></tr></table>
                                            
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                                                
                </ajaxToolkit:TabContainer>                
                </asp:Panel>      
      </td></tr></table>                
                
                
                </td>
            </tr>
        </table>
        <br />     

</asp:Content>
