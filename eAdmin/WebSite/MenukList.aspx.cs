using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MenukList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MenukList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        AlmenukSubListHeader.RowCount_Changed += new EventHandler(AlmenukSubListHeader_RowCount_Changed);

        AlmenukSubListHeader.ErvenyessegFilter_Changed += new EventHandler(AlmenukSubListHeader_ErvenyessegFilter_Changed);
    }
       
    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.MenukListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_MenukSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("MenukSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, MenukUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, MenukUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(MenukGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(MenukGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(MenukGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(MenukGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(MenukGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, MenukUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = MenukGridView;

        AlmenukSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlmenukSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlmenukSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlmenukSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(AlmenukGridView.ClientID);
        AlmenukSubListHeader.ButtonsClick += new CommandEventHandler(AlmenukSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        AlmenukSubListHeader.AttachedGridView = AlmenukGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(MenukGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(AlmenukGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_MenukSearch());

        if (!IsPostBack) MenukGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Lock);

        AlmenukPanel.Visible = FunctionRights.GetFunkcioJog(Page, "MenukList");

        AlmenukSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.New);
        AlmenukSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.View);
        AlmenukSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Modify);
        AlmenukSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Menu" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MenukGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #endregion

    #region Master List

    protected void MenukGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MenukGridView", ViewState, "KRT_Menuk.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MenukGridView", ViewState);

        MenukGridViewBind(sortExpression, sortDirection);
    }

    protected void MenukGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_MenukSearch search = (KRT_MenukSearch)Search.GetSearchObject(Page, new KRT_MenukSearch());
        search.OrderBy = Search.GetOrderBy("MenukGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(MenukGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    protected void MenukGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbFomenu = (CheckBox)e.Row.FindControl("cbFomenu");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            if (String.IsNullOrEmpty(drw["Menu_Id_Szulo"].ToString()))
            {
                cbFomenu.Checked = true;
            }
            else
            {
                cbFomenu.Checked = false;            
            }
            //BoolString.SetCheckBox(cbModosithato, drw["Modosithato"].ToString());
        } 
        
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void MenukGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, MenukCPE);
        ListHeader1.RefreshPagerLabel();

        ui.SetClientScriptToGridViewSelectDeSelectButton(MenukGridView);

    }

    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        MenukGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, MenukCPE);
        MenukGridViewBind();
    }

    protected void MenukGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MenukGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);
        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, MenukUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, MenukUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "KRT_Menuk";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, MenukUpdatePanel.ClientID);

            AlmenukSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.SzuloMenuId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AlmenukUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }


    protected void MenukUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    MenukGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MenukGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedMenuk();
            MenukGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedMenuRecords();
                MenukGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedMenuRecords();
                MenukGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedMenuk();
                break;
        }
    }

    private void LockSelectedMenuRecords()
    {
        LockManager.LockSelectedGridViewRecords(MenukGridView, "KRT_Menuk"
            , "MenuLock", "MenuForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedMenuRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(MenukGridView, "KRT_Menuk"
            , "MenuLock", "MenuForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a MenukGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedMenuk()
    {
        if (FunctionRights.GetFunkcioJog(Page, "MenuInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(MenukGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a MenukGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedMenuk()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(MenukGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Menuk");
        }
    }
    
    protected void MenukGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MenukGridViewBind(e.SortExpression, UI.GetSortToGridView("MenukGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MenukGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (MenukGridView.SelectedIndex == -1)
        {
            return;
        }
        string menuId = UI.GetGridViewSelectedRecordId(MenukGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, menuId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string menuId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                AlmenukGridViewBind(menuId);
                AlmenukPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(AlmenukGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        AlmenukSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(AlmenukTabPanel))
        {
            AlmenukGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(AlmenukGridView));
        }

    }

    #endregion


    #region Almenuk Detail

    private void AlmenukSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Almenuk();
            AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView));
        }
    }

    protected void AlmenukGridViewBind(string menuId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("AlmenukGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("AlmenukGridView", ViewState);

        AlmenukGridViewBind(menuId, sortExpression, sortDirection);
    }

    protected void AlmenukGridViewBind(string menuId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(menuId))
        {
            KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            
            KRT_MenukSearch search = new KRT_MenukSearch();
            search.Menu_Id_Szulo.Value = menuId;
            search.Menu_Id_Szulo.Operator = Contentum.eQuery.Query.Operators.equals;
            search.OrderBy = Search.GetOrderBy("AlmenukGridView", ViewState, SortExpression, SortDirection);

            AlmenukSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAll(ExecParam, search);

            UI.GridViewFill(AlmenukGridView, res, AlmenukSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

            ui.SetClientScriptToGridViewSelectDeSelectButton(AlmenukGridView);
        }
        else
        {
            ui.GridViewClear(AlmenukGridView);
        }
    }


    void AlmenukSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView));
    }

    protected void AlmenukUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(AlmenukTabPanel))
                    //{
                    //    AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView));
                    //}
                    ActiveTabRefreshDetailList(AlmenukTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a AlmenukGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_Almenuk()
    {
        if (FunctionRights.GetFunkcioJog(Page, "MenuInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(AlmenukGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void AlmenukGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(AlmenukGridView, selectedRowNumber, "check");
        }
    }

    private void AlmenukGridView_RefreshOnClientClicks(string almenu_Id)
    {
        string id = almenu_Id;
        if (!String.IsNullOrEmpty(id))
        {
            AlmenukSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AlmenukUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            AlmenukSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("MenukForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AlmenukUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void AlmenukGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = AlmenukGridView.PageIndex;

        AlmenukGridView.PageIndex = AlmenukSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != AlmenukGridView.PageIndex )
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, AlmenukCPE);
            AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView));
        }
        else
        {
            UI.GridViewSetScrollable(AlmenukSubListHeader.Scrollable, AlmenukCPE);
        }
        AlmenukSubListHeader.PageCount = AlmenukGridView.PageCount;
        AlmenukSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(AlmenukGridView);
    }

    void AlmenukSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(AlmenukSubListHeader.RowCount);
        AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView));
    }

    protected void AlmenukGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        AlmenukGridViewBind(UI.GetGridViewSelectedRecordId(MenukGridView)
            , e.SortExpression, UI.GetSortToGridView("AlmenukGridView", ViewState, e.SortExpression));
    }

    #endregion
}
