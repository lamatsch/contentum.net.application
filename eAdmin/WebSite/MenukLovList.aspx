<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="MenukLovList.aspx.cs" Inherits="MenukLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,MenukLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Men� n�v:"></asp:Label><br />
                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%">*</asp:TextBox>&nbsp;
                                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="Keres�s">
                                            </asp:Button>
                                            <asp:Button ID="ButtonAdvancedSearch" runat="server" Text="R�szletes keres�s"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:ListBox ID="ListBoxSearchResult" runat="server" Width="95%" Rows="20"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.jpg"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
