using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class MenukLovList : Contentum.eUtility.UI.PageBase
{

    private bool disable_refreshLovList = false;
    private String filterType = "";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MenukList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (String.IsNullOrEmpty(filterType))
        {
            filterType = "";
        }
        else
        {
            //LovListHeader1.FilterTitle = "("+Resources.LovList.FilterTitle_Filtering + " " + filterType + ")";
        }

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("MenukSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick =
            JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            + JavaScripts.SetOnClientClick("MenukForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'"
            , Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

        Form.DefaultButton = ButtonSearch.UniqueID;
        Form.DefaultFocus = ButtonSearch.UniqueID;
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    private void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
        KRT_MenukSearch searchObject = null;

        if (searchObjectFromSession == true)
        {
            searchObject = (KRT_MenukSearch)Search.GetSearchObject(Page, new KRT_MenukSearch());
            searchObject.OrderBy = "Nev";
        }
        else
        {
            searchObject = new KRT_MenukSearch();
            searchObject.Nev.Value = SearchKey;
            searchObject.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            searchObject.OrderBy = "Nev";
        }

        // ha kell, sz�r�nk a t�pusra, ak�r fel�lv�gva a r�szletes keres�sn�l megadott t�pust is
        if (!String.IsNullOrEmpty(filterType))
        {
            searchObject.Kod.Value = filterType;
            searchObject.Kod.Operator = Query.Operators.equals;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        searchObject.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(execParam, searchObject);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, searchObject.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }
}
