<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="MenukSearch.aspx.cs" Inherits="MenukSearch" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc4" %>

<%@ Register Src="Component/ModulTextBox.ascx" TagName="ModulTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/MenuTextBox.ascx" TagName="MenuTextBox" TagPrefix="uc9" %>

<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
    
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,MenukSearchHeaderTitle%>" />
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Nev" runat="server" Validate="false" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                &nbsp;<asp:Label ID="Label10" runat="server" Text="Link (modul):"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:ModulTextBox ID="ModulTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                <asp:Label ID="Label14" runat="server" Text="Link param�terei:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:TextBox ID="Parameter_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                &nbsp;<asp:Label ID="Label8" runat="server" Text="Hozz�rendelt funkci�:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc7:FunkcioTextBox ID="FunkcioTextBox1" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 29px;">
                            <asp:Label ID="Label6" runat="server" Text="Alkalmaz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 29px;">
                            <div class="DisableWrap">
                                <uc4:KodtarakDropDownList ID="KodtarakDropDownList1" runat="server" />
                                &nbsp;</div></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                &nbsp;</td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="Fomenu_CheckBox" runat="server" Text="F�men�" /></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px">
                                &nbsp;<asp:Label ID="Label_SzuloMenu" runat="server" Text="Sz�l� men�:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc9:MenuTextBox ID="Szulomenu_MenuTextBox" runat="server" SearchMode="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label_ImageURL" runat="server" Text="K�p URL:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="ImageURL_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                &nbsp;
                                <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" />                                
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>      
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />          
                </td>
            </tr>
        </table>
</asp:Content>


