using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class MenukSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_MenukSearch);

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_MenukSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_MenukSearch)Search.GetSearchObject(Page, new KRT_MenukSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_MenukSearch krt_menukSearch = null;
        if (searchObject != null) krt_menukSearch = (KRT_MenukSearch)searchObject;

        if (krt_menukSearch != null)
        {
            Nev.Text = krt_menukSearch.Nev.Value;

            ModulTextBox1.Id_HiddenField = krt_menukSearch.Modul_Id.Value;
            ModulTextBox1.SetModulTextBoxById(SearchHeader1.ErrorPanel);

            Parameter_TextBox.Text = krt_menukSearch.Parameter.Value;

            FunkcioTextBox1.Id_HiddenField = krt_menukSearch.Funkcio_Id.Value;
            FunkcioTextBox1.SetFunkcioTextBoxById(SearchHeader1.ErrorPanel);

            if (!String.IsNullOrEmpty(krt_menukSearch.Kod.Value))
            {
                KodtarakDropDownList1.Enabled = true;
                //CheckBox_KodtarDropDownEnable.Checked = true;
                KodtarakDropDownList1.FillAndSetSelectedValue("Alkalmazas",
                        krt_menukSearch.Kod.Value, true, SearchHeader1.ErrorPanel);
            }
            else
            {
                KodtarakDropDownList1.FillDropDownList("Alkalmazas", true, SearchHeader1.ErrorPanel);
            }

            if (String.IsNullOrEmpty(krt_menukSearch.Menu_Id_Szulo.Value) 
                && krt_menukSearch.Menu_Id_Szulo.Operator == Query.Operators.isnull)
            {
                Fomenu_CheckBox.Checked = true;
                Szulomenu_MenuTextBox.ImageButton_Lov.Attributes["onload"] =
                    Fomenu_CheckBox.Attributes["onclick"];
            }
            else
            {
                Fomenu_CheckBox.Checked = false;
            }

            Szulomenu_MenuTextBox.Id_HiddenField = krt_menukSearch.Menu_Id_Szulo.Value;
            Szulomenu_MenuTextBox.SetMenuTextBoxById(SearchHeader1.ErrorPanel);

            ImageURL_TextBox.Text = krt_menukSearch.ImageURL.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_menukSearch.ErvKezd,krt_menukSearch.ErvVege);
        }
    }


    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_MenukSearch SetSearchObjectFromComponents()
    {
        KRT_MenukSearch krt_menukSearch = (KRT_MenukSearch)SearchHeader1.TemplateObject;
        if (krt_menukSearch == null)
        {
            krt_menukSearch = new KRT_MenukSearch();
        }
        
        if (!String.IsNullOrEmpty(Nev.Text))
        {
            krt_menukSearch.Nev.Value = Nev.Text;
            krt_menukSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev.Text);
        }
        if (!String.IsNullOrEmpty(ModulTextBox1.Id_HiddenField))
        {
            krt_menukSearch.Modul_Id.Value = ModulTextBox1.Id_HiddenField;
            krt_menukSearch.Modul_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Parameter_TextBox.Text))
        {
            krt_menukSearch.Parameter.Value = Parameter_TextBox.Text;
            krt_menukSearch.Parameter.Operator = Search.GetOperatorByLikeCharater(Parameter_TextBox.Text);
        }
        if (!String.IsNullOrEmpty(FunkcioTextBox1.Id_HiddenField))
        {
            krt_menukSearch.Funkcio_Id.Value = FunkcioTextBox1.Id_HiddenField;
            krt_menukSearch.Funkcio_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(KodtarakDropDownList1.SelectedValue))
        {
            krt_menukSearch.Kod.Value = KodtarakDropDownList1.SelectedValue;
            krt_menukSearch.Kod.Operator = Query.Operators.equals;
        }
        if (Fomenu_CheckBox.Checked == true)
        {
            krt_menukSearch.Menu_Id_Szulo.Value = "";
            krt_menukSearch.Menu_Id_Szulo.Operator = Query.Operators.isnull;
        }
        else if (!String.IsNullOrEmpty(Szulomenu_MenuTextBox.Id_HiddenField))
        {
            krt_menukSearch.Menu_Id_Szulo.Value = Szulomenu_MenuTextBox.Id_HiddenField;
            krt_menukSearch.Menu_Id_Szulo.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Parameter_TextBox.Text))
        {
            krt_menukSearch.Parameter.Value = Parameter_TextBox.Text;
            krt_menukSearch.Parameter.Operator = Search.GetOperatorByLikeCharater(Parameter_TextBox.Text);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_menukSearch.ErvKezd, krt_menukSearch.ErvVege);

        return krt_menukSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_MenukSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_MenukSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_MenukSearch();
    }
    

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        Fomenu_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + Fomenu_CheckBox.ClientID
                + "',1,['"
                + Label_SzuloMenu.ClientID
                + "']);"
        + " showOrHideComponents('" + Fomenu_CheckBox.ClientID
                + "',1,['"
                + Szulomenu_MenuTextBox.ImageButton_Lov.ClientID
                + "','"
                //+ Szulomenu_MenuTextBox.ImageButton_New.ClientID
                //+ "','"
                + Szulomenu_MenuTextBox.ImageButton_View.ClientID
                + "','"
                + Szulomenu_MenuTextBox.ImageButton_Reset.ClientID
                + "']);";

        //CheckBox_KodtarDropDownEnable.Attributes["onclick"] =
        //    " enableOrDisableComponents('" + CheckBox_KodtarDropDownEnable.ClientID
        //        + "',0,['"
        //        + KodtarakDropDownList1.DropDownList.ClientID
        //        + "']);";
        
    }
}
