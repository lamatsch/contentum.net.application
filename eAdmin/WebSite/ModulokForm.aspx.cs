using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ModulokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcsKod_MODUL_TIPUS = "MODUL_TIPUS";

   /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        // readonly-ra, vagy disabled-re �ll�tunk minden olyan komponenst,
        // amit k�l�n 'View' m�dban kell �ll�tani
        Nev.ReadOnly = true;
        Kod_TextBox.ReadOnly = true;
        Tipus_KodtarakDropDownList.ReadOnly = true;
        Leiras_TextBox.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Modul" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Modulok krt_Modulok = (KRT_Modulok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Modulok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                Tipus_KodtarakDropDownList.FillDropDownList(kcsKod_MODUL_TIPUS, FormHeader1.ErrorPanel);
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.ModulokFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Modulok krt_modulok)
    {
        Nev.Text = krt_modulok.Nev;

        Kod_TextBox.Text = krt_modulok.Kod;
        
        Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_MODUL_TIPUS
            , krt_modulok.Tipus, FormHeader1.ErrorPanel);

        Leiras_TextBox.Text = krt_modulok.Leiras;

        ErvenyessegCalendarControl1.ErvKezd = krt_modulok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_modulok.ErvVege;

        FormHeader1.Record_Ver = krt_modulok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_modulok.Base);

        //if (Command == CommandName.View)
        //{
        //    // readonly-ra, vagy disabled-re �ll�tunk minden olyan komponenst,
        //    // amit k�l�n 'View' m�dban kell �ll�tani
        //    Nev.ReadOnly = true;
        //    Kod_TextBox.ReadOnly = true;
        //    Tipus_KodtarakDropDownList.ReadOnly = true;
        //    Leiras_TextBox.ReadOnly = true;
        //    ErvenyessegCalendarControl1.ReadOnly = true;
        //}
    }

    // form --> business object
    private KRT_Modulok GetBusinessObjectFromComponents()
    {
        KRT_Modulok krt_Modulok = new KRT_Modulok();
        krt_Modulok.Updated.SetValueAll(false);
        krt_Modulok.Base.Updated.SetValueAll(false);

        krt_Modulok.Nev = Nev.Text;
        krt_Modulok.Updated.Nev = pageView.GetUpdatedByView(Nev);

        krt_Modulok.Kod = Kod_TextBox.Text;
        krt_Modulok.Updated.Kod = pageView.GetUpdatedByView(Kod_TextBox);

        krt_Modulok.Tipus = Tipus_KodtarakDropDownList.SelectedValue;
        krt_Modulok.Updated.Tipus = pageView.GetUpdatedByView(Tipus_KodtarakDropDownList);

        krt_Modulok.Leiras = Leiras_TextBox.Text;
        krt_Modulok.Updated.Leiras = pageView.GetUpdatedByView(Leiras_TextBox);

        //krt_Modulok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_Modulok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_Modulok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_Modulok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Modulok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_Modulok.Base.Ver = FormHeader1.Record_Ver;
        krt_Modulok.Base.Updated.Ver = true;

        return krt_Modulok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(Kod_TextBox);
            compSelector.Add_ComponentOnClick(Tipus_KodtarakDropDownList);
            compSelector.Add_ComponentOnClick(Leiras_TextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Modul" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
                            KRT_Modulok krt_Modulok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Modulok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
                                KRT_Modulok krt_Modulok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Modulok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
