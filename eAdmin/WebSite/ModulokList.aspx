<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="ModulokList.aspx.cs" Inherits="ModulokList" %>


<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
<%--Tablazat / Grid--%> 

    <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="ModulokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">
        <asp:UpdatePanel ID="ModulokUpdatePanel" runat="server" OnLoad="ModulokUpdatePanel_Load">
            <ContentTemplate>
                <ajaxToolkit:CollapsiblePanelExtender ID="ModulokCPE" runat="server" TargetControlID="Panel1"
                    CollapsedSize="20" Collapsed="False" ExpandControlID="ModulokCPEButton" CollapseControlID="ModulokCPEButton"
                    ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                    ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="ModulokCPEButton"
                    ExpandedSize="0" ExpandedText="Szerepk�r�k list�ja" CollapsedText="Szerepk�r�k list�ja">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="Panel1" runat="server">
                    <table style="width: 98%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                <asp:GridView ID="ModulokGridView" runat="server" OnRowCommand="ModulokGridView_RowCommand"
                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="ModulokGridView_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="ModulokGridView_Sorting"
                                    OnRowDataBound="ModulokGridView_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_Modulok.Nev" HeaderStyle-Width="200px" />
                                        <asp:BoundField DataField="Kod" HeaderText="K�d" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_Modulok.Kod" HeaderStyle-Width="200px" />
                                        <asp:BoundField DataField="Modul_Tipus" HeaderText="T�pus" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_KodTarak.Nev" HeaderStyle-Width="150px" />
                                        <asp:BoundField DataField="Leiras" HeaderText="Le�r�s" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_Modulok.Leiras" HeaderStyle-Width="200px" />                                        
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
            </td>                            
    </tr>
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>

        </table>
        <br />     

</asp:Content>
