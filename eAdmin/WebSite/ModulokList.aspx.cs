using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;


public partial class ModulokList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ModulokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.ModulokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_ModulokSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        //ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("ModulokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, ModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ModulokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, ModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ModulokGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(ModulokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(ModulokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(ModulokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(ModulokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ModulokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = ModulokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(ModulokGridView);
       
        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_ModulokSearch());

        if (!IsPostBack) ModulokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Modul" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(ModulokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
        }
    }


    #endregion

    #region Master List

    protected void ModulokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ModulokGridView", ViewState, "KRT_Modulok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ModulokGridView", ViewState);

        ModulokGridViewBind(sortExpression, sortDirection);
    }

    protected void ModulokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_ModulokSearch search = (KRT_ModulokSearch)Search.GetSearchObject(Page, new KRT_ModulokSearch());
        search.OrderBy = Search.GetOrderBy("ModulokGridView", ViewState, SortExpression, SortDirection);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithFK(ExecParam, search);

        UI.GridViewFill(ModulokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    protected void ModulokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void ModulokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, ModulokCPE);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ModulokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, ModulokCPE);
        ModulokGridViewBind();
    }

    protected void ModulokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ModulokGridView, selectedRowNumber, "check");

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);


        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ModulokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, ModulokUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ModulokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ModulokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "KRT_Modulok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, ModulokUpdatePanel.ClientID);
}
    }

    protected void ModulokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ModulokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(ModulokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedModulok();
            ModulokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedModulRecords();
                ModulokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedModulRecords();
                ModulokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedModulok();
                break;
        }
    }

    private void LockSelectedModulRecords()
    {
        LockManager.LockSelectedGridViewRecords(ModulokGridView, "KRT_Modulok"
            , "ModulLock", "ModulForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedModulRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(ModulokGridView, "KRT_Modulok"
            , "ModulLock", "ModulForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a ModulokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedModulok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ModulInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ModulokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a ModulokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedModulok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(ModulokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Modulok");
        }
    }

    protected void ModulokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ModulokGridViewBind(e.SortExpression, UI.GetSortToGridView("ModulokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    #endregion
}