using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class ModulokLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ModulokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //ButtonAdvancedSearch.Enabled = false;
        //ButtonAdvancedSearch.CssClass = "disableditem";

        //ImageButtonReszletesAdatok.Enabled = false;
        //ImageButtonReszletesAdatok.CssClass = "disableditem";

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("ModulokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = "if (document.forms[0]." + ListBoxSearchResult.ClientID + ".value == '') return false; "
            + JavaScripts.SetOnClientClick("ModulokForm.aspx", "Command=View&Id='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_ModulokService service = eAdminService.ServiceFactory.GetKRT_ModulokService();
        KRT_ModulokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_ModulokSearch)Search.GetSearchObject(Page, new KRT_ModulokSearch());
        }
        else
        {
            search = new KRT_ModulokSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.OrderBy = "Nev";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }
}
