<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/EmptyMasterPage.master" CodeFile="ModulokSearch.aspx.cs" Inherits="ModulokSearch" %>


<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc12" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoportokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                 <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Modul neve:"></asp:Label></td>
                            <td class="mrUrlapMezo">                                
                                <uc12:RequiredTextBox ID="Nev" runat="server" Validate="false"/>
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label7" runat="server" Text="K�d / URL:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Kod_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="Tipus_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Le�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Leiras_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2">
                                        <uc3:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server" />
                                        &nbsp; &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>


