using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ModulokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_ModulokSearch);

    private const string kcsKod_MODUL_TIPUS = "MODUL_TIPUS";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.ModulokSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_ModulokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_ModulokSearch)Search.GetSearchObject(Page, new KRT_ModulokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_ModulokSearch krt_ModulokSearch = null;
        if (searchObject != null) krt_ModulokSearch = (KRT_ModulokSearch)searchObject;

        if (krt_ModulokSearch != null)
        {
            Nev.Text = krt_ModulokSearch.Nev.Value;
            Kod_TextBox.Text = krt_ModulokSearch.Kod.Value;
            Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_MODUL_TIPUS, krt_ModulokSearch.Tipus.Value, true, SearchHeader1.ErrorPanel);
            Leiras_TextBox.Text = krt_ModulokSearch.Leiras.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_ModulokSearch.ErvKezd, krt_ModulokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_ModulokSearch SetSearchObjectFromComponents()
    {
        KRT_ModulokSearch krt_ModulokSearch = (KRT_ModulokSearch)SearchHeader1.TemplateObject;
        if (krt_ModulokSearch == null)
        {
            krt_ModulokSearch = new KRT_ModulokSearch();
        }

        if (!String.IsNullOrEmpty(Nev.Text))
        {
            krt_ModulokSearch.Nev.Value = Nev.Text;
            krt_ModulokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev.Text);
        }

        if (!String.IsNullOrEmpty(Kod_TextBox.Text))
        {
            krt_ModulokSearch.Kod.Value = Kod_TextBox.Text;
            krt_ModulokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(Kod_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Tipus_KodtarakDropDownList.SelectedValue))
        {
            krt_ModulokSearch.Tipus.Value = Tipus_KodtarakDropDownList.SelectedValue;
            krt_ModulokSearch.Tipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Leiras_TextBox.Text))
        {
            krt_ModulokSearch.Leiras.Value = Leiras_TextBox.Text;
            krt_ModulokSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(Leiras_TextBox.Text);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_ModulokSearch.ErvKezd, krt_ModulokSearch.ErvVege);

        return krt_ModulokSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_ModulokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_ModulokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_ModulokSearch();
    }

}


