using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ObjTipusokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    private void SetViewControls()
    {
        requiredTextBoxKod.ReadOnly = true;
        requiredTextBoxNev.ReadOnly = true;
        TipusObjTipusokTextBox.ReadOnly = true;
        SzuloObjTipusokTextBox.ReadOnly = true;
        KodcsoportokTextBox1.ReadOnly = true;
        textNote.ReadOnly = true;

        ErvenyessegCalendarControl1.ReadOnly = true;

        requiredTextBoxKod.Validate = false;
        requiredTextBoxNev.Validate = false;

        labelKod.CssClass = "mrUrlapInputWaterMarked";
        labelKodcsoport.CssClass = "mrUrlapInputWaterMarked";
        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelSzulo.CssClass = "mrUrlapInputWaterMarked";
        labelTipus.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:          
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ObjTipusok" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_ObjTipusok KRT_ObjTipusok = (KRT_ObjTipusok)result.Record;
                    LoadComponentsFromBusinessObject(KRT_ObjTipusok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.ObjTipusokFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_ObjTipusok KRT_ObjTipusok)
    {
        requiredTextBoxKod.Text = KRT_ObjTipusok.Kod;    
        requiredTextBoxNev.Text = KRT_ObjTipusok.Nev;

        TipusObjTipusokTextBox.Id_HiddenField = KRT_ObjTipusok.ObjTipus_Id_Tipus;
        TipusObjTipusokTextBox.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
        SzuloObjTipusokTextBox.Id_HiddenField = KRT_ObjTipusok.Obj_Id_Szulo;
        SzuloObjTipusokTextBox.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);
        
        KodcsoportokTextBox1.Id_HiddenField = KRT_ObjTipusok.KodCsoport_Id;
        KodcsoportokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        
        ErvenyessegCalendarControl1.ErvKezd = KRT_ObjTipusok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = KRT_ObjTipusok.ErvVege;
        
        textNote.Text = KRT_ObjTipusok.Base.Note;

        FormHeader1.Record_Ver = KRT_ObjTipusok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(KRT_ObjTipusok.Base);

        if (Command == CommandName.View)
        {
            // readonly-ra, vagy disabled-re �ll�tunk minden olyan komponenst,
            // amit k�l�n 'View' m�dban kell �ll�tani
            requiredTextBoxKod.ReadOnly = true;
            requiredTextBoxNev.ReadOnly = true;
            TipusObjTipusokTextBox.ReadOnly = true;
            SzuloObjTipusokTextBox.ReadOnly = true;
            KodcsoportokTextBox1.ReadOnly = true;
            textNote.ReadOnly = true;
            
            ErvenyessegCalendarControl1.ReadOnly = true;
            
            requiredTextBoxKod.Validate = false;
            requiredTextBoxNev.Validate = false;

        }
    }

    // form --> business object
    private KRT_ObjTipusok GetBusinessObjectFromComponents()
    {
        KRT_ObjTipusok KRT_ObjTipusok = new KRT_ObjTipusok();
        KRT_ObjTipusok.Updated.SetValueAll(false);
        KRT_ObjTipusok.Base.Updated.SetValueAll(false);

        KRT_ObjTipusok.Nev = requiredTextBoxNev.Text;
        KRT_ObjTipusok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        
        KRT_ObjTipusok.Kod = requiredTextBoxKod.Text;
        KRT_ObjTipusok.Updated.Kod = pageView.GetUpdatedByView(requiredTextBoxKod);        

        KRT_ObjTipusok.ObjTipus_Id_Tipus = TipusObjTipusokTextBox.Id_HiddenField;
        KRT_ObjTipusok.Updated.ObjTipus_Id_Tipus = pageView.GetUpdatedByView(TipusObjTipusokTextBox);

        KRT_ObjTipusok.Obj_Id_Szulo = SzuloObjTipusokTextBox.Id_HiddenField;
        KRT_ObjTipusok.Updated.Obj_Id_Szulo = pageView.GetUpdatedByView(SzuloObjTipusokTextBox);

        KRT_ObjTipusok.KodCsoport_Id = KodcsoportokTextBox1.Id_HiddenField;
        KRT_ObjTipusok.Updated.KodCsoport_Id = pageView.GetUpdatedByView(KodcsoportokTextBox1);
      
        KRT_ObjTipusok.Base.Note = KodcsoportokTextBox1.Id_HiddenField;
        KRT_ObjTipusok.Base.Updated.Note = pageView.GetUpdatedByView(KodcsoportokTextBox1);

        KRT_ObjTipusok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        KRT_ObjTipusok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        KRT_ObjTipusok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        KRT_ObjTipusok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        KRT_ObjTipusok.Base.Ver = FormHeader1.Record_Ver;
        KRT_ObjTipusok.Base.Updated.Ver = true;

        return KRT_ObjTipusok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(TipusObjTipusokTextBox);
            compSelector.Add_ComponentOnClick(SzuloObjTipusokTextBox);
            compSelector.Add_ComponentOnClick(KodcsoportokTextBox1);
            compSelector.Add_ComponentOnClick(textNote);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }
    
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "ObjTipusok" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
                            KRT_ObjTipusok KRT_ObjTipusok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, KRT_ObjTipusok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxKod.Text))
                                {
                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                    if (!String.IsNullOrEmpty(refreshCallingWindow)
                                        && refreshCallingWindow == "1"
                                        )
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
                                KRT_ObjTipusok KRT_ObjTipusok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, KRT_ObjTipusok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
