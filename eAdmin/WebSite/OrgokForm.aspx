<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="OrgokForm.aspx.cs" Inherits="OrgokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <script type="text/javascript">
        function SetJelszoVisiblity() {
            var authType = $get('<%=ddownAuthType.ClientID %>');
            var trJelszo = $get('<%=tr_Jelszo.ClientID %>');
            var validatorJelszo = $get('<%=requiredTextBoxFelhasznaloJelszo.Validator.ClientID %>');
            if (authType && trJelszo && authType.selectedIndex > -1) {
                if (authType.options[authType.selectedIndex].value == '<%=authTypeWindows.Value %>') {
                    trJelszo.style.display = 'none';
                    if (validatorJelszo) {
                        validatorJelszo.enabled = false;
                    }
                }
                else {
                    trJelszo.style.display = '';
                    if (validatorJelszo) {
                        validatorJelszo.enabled = true;
                    }
                }
            }
        }

        function pageLoad() {
            SetJelszoVisiblity();
            var authType = $get('<%=ddownAuthType.ClientID %>');
            if (authType) {
                $addHandler(authType, 'change', SetJelszoVisiblity);
            }
        }
    </script>

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,OrgokFormHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelKodStar" runat="server" CssClass="ReqStar" Text="*" />
                            <asp:Label ID="labelKod" runat="server" Text="K�d:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxKod" runat="server" LabelRequiredIndicatorID="labelKodStar" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNevStar" runat="server" CssClass="ReqStar" Text="*" />
                            <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxNev" runat="server" LabelRequiredIndicatorID="labelNevStar"
                                Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelSorszam" runat="server" Text="Sorsz�m:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc8:RequiredNumberBox ID="requiredNumberSorszam" runat="server" CssClass="mrUrlapInputNumber"
                                Validate="false"  />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc7:PartnerTextBox ID="partnerTextBoxPartner" runat="server" Validate="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyz�s:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="tbNote" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelErvenyessegStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ervenyessegCalendarControl" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <eUI:eFormPanel ID="EFormPanel_Szerepkor" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelSzerepkorNevStar" runat="server" CssClass="ReqStar" Text="*" />
                            <asp:Label ID="labelSzerepkorNev" runat="server" Text="Szerepk�r megnevez�s:" />
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxSzerepkorNev" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <eUI:eFormPanel ID="EFormPanel_Felhasznalo" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloUserNevStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="labelFelhasznaloUserNev" runat="server" Text="Felhaszn�l� n�v:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxFelhasznaloUserNev" runat="server" LabelRequiredIndicatorID="labelUserNevStar" />
                    </td>
                </tr>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloNevStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="labelFelhasznaloNev" runat="server" Text="N�v:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxFelhasznaloNev" runat="server" LabelRequiredIndicatorID="labelFelhasznaloNevStar" />
                    </td>
                </tr>
                <tr class="urlapSor" id="tr_Tipus_dropdown" runat="server">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloAuthTypeStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="labelFelhasznaloAuthType" runat="server" Text="Azonos�t�s tipusa:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:DropDownList ID="ddownAuthType" runat="server" CssClass="mrUrlapInputComboBox">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="urlapSor" id="tr_Jelszo_lejar_ido" runat="server">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloJelszoLejarStar" runat="server" CssClass="ReqStar"
                            Text="*"></asp:Label>
                        <asp:Label ID="labelFelhasznaloJelszoLejar" runat="server" Text="Jelsz� lej�r:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:CalendarControl ID="calendarFelhasznaloJelszoLejar" runat="server" />
                    </td>
                </tr>
                <tr class="urlapSor" id="tr_Jelszo" runat="server">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloJelszoStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="labelFelhasznaloJelszo" runat="server" Text="Jelsz�:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxFelhasznaloJelszo" Text="" runat="server"
                            LabelRequiredIndicatorID="labelFelhasznaloJelszoStar" />
                    </td>
                </tr>
<%--                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelFelhasznaloErvenyessegStar" runat="server" CssClass="ReqStar"
                            Text="*"></asp:Label>
                        <asp:Label ID="labelFelhasznaloErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc6:ErvenyessegCalendarControl ID="ervenyessegCalendarControlFelhasznalo" runat="server" />
                    </td>
                </tr>--%>
            </table>
        </eUI:eFormPanel>
        <eUI:eFormPanel ID="EFormPanel_Csoport" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelCsoportNevStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                        <asp:Label ID="labelCsoportNev" runat="server" Text="Csoport neve:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc5:RequiredTextBox ID="requiredTextBoxCsoportNev" runat="server" LabelRequiredIndicatorID="labelCsoportNevStar" />
                    </td>
                </tr>
<%--                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelCsoportErvenyessegStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                        <asp:Label ID="labelCsoportErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc6:ErvenyessegCalendarControl ID="ervenyessegCalendarControlCsoport" runat="server" />
                    </td>
                </tr>--%>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>
