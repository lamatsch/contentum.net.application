using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class OrgokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string Default_SzerepkorNev = "SETUP_ADMIN";
    private const string Default_CsoportNev = "Adminisztr�torok";
    //private const string Default_FelhasznaloUserNev = "Admin";
    private const string Default_FelhasznaloNev = "Adminisztr�tor";

    protected readonly ListItem authTypeWindows = new ListItem("Windows", "Windows");
    protected readonly ListItem authTypeBasic = new ListItem("Basic", "Basic");

    private const string Default_AuthType = "Basic";

    #region radiobuttonlist
    private void FillAuthDropDownList()
    {
        ddownAuthType.Items.Add(authTypeWindows);
        ddownAuthType.Items.Add(authTypeBasic);
    }
    #endregion

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxKod.Validate = false;

        ervenyessegCalendarControl.ReadOnly = true;
        requiredTextBoxNev.ReadOnly = true;
        requiredNumberSorszam.ReadOnly = true;
        tbNote.ReadOnly = true;

        partnerTextBoxPartner.ReadOnly = true;
        requiredTextBoxKod.ReadOnly = true;

        EFormPanel_Szerepkor.Visible = false;
        EFormPanel_Felhasznalo.Visible = false;
        EFormPanel_Csoport.Visible = false;

        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        labelKod.CssClass = "mrUrlapInputWaterMarked";
        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelSorszam.CssClass = "mrUrlapInputWaterMarked";

        labelKodStar.Visible = false;
        labelErvenyessegStar.Visible = false;
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        ervenyessegCalendarControl.ReadOnly = false;
        requiredTextBoxNev.ReadOnly = false;
        requiredNumberSorszam.ReadOnly = false;
        tbNote.ReadOnly = false;

        partnerTextBoxPartner.ReadOnly = true;
        requiredTextBoxKod.ReadOnly = true;

        EFormPanel_Szerepkor.Visible = false;
        EFormPanel_Felhasznalo.Visible = false;
        EFormPanel_Csoport.Visible = false;

        labelKod.CssClass = "mrUrlapInputWaterMarked";
        labelPartner.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Org" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Orgok krt_Orgok = (KRT_Orgok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Orgok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            requiredTextBoxSzerepkorNev.Text = Default_SzerepkorNev;
            //requiredTextBoxFelhasznaloUserNev.Text = Default_FelhasznaloUserNev;
            requiredTextBoxFelhasznaloNev.Text = Default_FelhasznaloNev;
            requiredTextBoxCsoportNev.Text = Default_CsoportNev;

            calendarFelhasznaloJelszoLejar.Text = DefaultDates.EndDate;

            if (!IsPostBack)
            {
                FillAuthDropDownList();
            }

            ddownAuthType.SelectedValue = Default_AuthType;
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Orgok"></param>
    private void LoadComponentsFromBusinessObject(KRT_Orgok krt_Orgok)
    {
        requiredTextBoxKod.Text = krt_Orgok.Kod;
        requiredTextBoxNev.Text = krt_Orgok.Nev;
        partnerTextBoxPartner.Id_HiddenField = krt_Orgok.Partner_id_szulo;
        partnerTextBoxPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        requiredNumberSorszam.Text = krt_Orgok.Sorszam;

        tbNote.Text = krt_Orgok.Base.Note;

        ervenyessegCalendarControl.ErvKezd = krt_Orgok.ErvKezd;
        ervenyessegCalendarControl.ErvVege = krt_Orgok.ErvVege;
        
        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = krt_Orgok.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_Orgok.Base);
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_Orgok"></param>
    private void LoadComponentsFromBusinessObject_Csoport(KRT_Csoportok krt_Csoportok)
    {
        requiredTextBoxCsoportNev.Text = krt_Csoportok.Nev;
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_Orgok GetBusinessObjectFromComponents()
    {
        KRT_Orgok krt_Orgok = new KRT_Orgok();
        krt_Orgok.Updated.SetValueAll(false);
        krt_Orgok.Base.Updated.SetValueAll(false);

        krt_Orgok.Kod = requiredTextBoxKod.Text;
        krt_Orgok.Updated.Kod = pageView.GetUpdatedByView(requiredTextBoxKod);

        krt_Orgok.Nev = requiredTextBoxNev.Text;
        krt_Orgok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);

        krt_Orgok.Partner_id_szulo = partnerTextBoxPartner.Id_HiddenField;
        krt_Orgok.Updated.Partner_id_szulo = pageView.GetUpdatedByView(partnerTextBoxPartner);

        krt_Orgok.Sorszam = requiredNumberSorszam.Text;
        krt_Orgok.Updated.Sorszam = pageView.GetUpdatedByView(requiredNumberSorszam);

        krt_Orgok.Base.Note = tbNote.Text;
        krt_Orgok.Base.Updated.Note = pageView.GetUpdatedByView(tbNote);

        ervenyessegCalendarControl.SetErvenyessegFields(krt_Orgok, pageView.GetUpdatedByView(ervenyessegCalendarControl));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Orgok.Base.Ver = FormHeader1.Record_Ver;
        krt_Orgok.Base.Updated.Ver = true;

        return krt_Orgok;
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    private KRT_Felhasznalok GetBusinessObjectFromComponents_Felhasznalo()
    {
        KRT_Felhasznalok krt_Felhasznalok = new KRT_Felhasznalok();
        krt_Felhasznalok.Updated.SetValueAll(false);
        krt_Felhasznalok.Base.Updated.SetValueAll(false);

        krt_Felhasznalok.UserNev = requiredTextBoxFelhasznaloUserNev.Text;
        krt_Felhasznalok.Updated.UserNev = pageView.GetUpdatedByView(requiredTextBoxFelhasznaloUserNev);

        krt_Felhasznalok.Nev = requiredTextBoxFelhasznaloNev.Text;
        krt_Felhasznalok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxFelhasznaloNev);

        krt_Felhasznalok.Tipus = ddownAuthType.SelectedValue;
        krt_Felhasznalok.Updated.Tipus = pageView.GetUpdatedByView(ddownAuthType);

        if (!String.IsNullOrEmpty(calendarFelhasznaloJelszoLejar.Text))
        {
            krt_Felhasznalok.JelszoLejaratIdo = calendarFelhasznaloJelszoLejar.Text;
            krt_Felhasznalok.Updated.JelszoLejaratIdo = pageView.GetUpdatedByView(calendarFelhasznaloJelszoLejar);
        }

        krt_Felhasznalok.Jelszo = requiredTextBoxFelhasznaloJelszo.Text;
        krt_Felhasznalok.Updated.Jelszo = pageView.GetUpdatedByView(requiredTextBoxFelhasznaloJelszo);

        krt_Felhasznalok.Engedelyezett = Constants.Database.Yes;
        krt_Felhasznalok.Updated.Engedelyezett = true;

        //ervenyessegCalendarControlFelhasznalo.SetErvenyessegFields(krt_Felhasznalok, pageView.GetUpdatedByView(ervenyessegCalendarControlFelhasznalo));
        ervenyessegCalendarControl.SetErvenyessegFields(krt_Felhasznalok, pageView.GetUpdatedByView(ervenyessegCalendarControl));

        return krt_Felhasznalok;
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    private KRT_Csoportok GetBusinessObjectFromComponents_Csoport()
    {
        KRT_Csoportok krt_Csoportok = new KRT_Csoportok();
        krt_Csoportok.Updated.SetValueAll(false);
        krt_Csoportok.Base.Updated.SetValueAll(false);

        krt_Csoportok.Nev = requiredTextBoxCsoportNev.Text;
        krt_Csoportok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxCsoportNev);

        krt_Csoportok.Tipus = KodTarak.CSOPORTTIPUS.Szervezet;
        krt_Csoportok.Updated.Tipus = true;

        krt_Csoportok.JogosultsagOroklesMod = "1";  // Dolgoz�k l�thatj�k egym�s t�eleit
        krt_Csoportok.Updated.JogosultsagOroklesMod = true;

        krt_Csoportok.System = "0";
        krt_Csoportok.Updated.System = true;
        
        //ervenyessegCalendarControlCsoport.SetErvenyessegFields(krt_Csoportok, pageView.GetUpdatedByView(ervenyessegCalendarControlCsoport));
        ervenyessegCalendarControl.SetErvenyessegFields(krt_Csoportok, pageView.GetUpdatedByView(ervenyessegCalendarControl));

        return krt_Csoportok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(calendarFelhasznaloJelszoLejar);
            compSelector.Add_ComponentOnClick(ddownAuthType);
            compSelector.Add_ComponentOnClick(ervenyessegCalendarControl);
            //compSelector.Add_ComponentOnClick(ervenyessegCalendarControlCsoport);
            //compSelector.Add_ComponentOnClick(ervenyessegCalendarControlFelhasznalo);
            compSelector.Add_ComponentOnClick(partnerTextBoxPartner);
            compSelector.Add_ComponentOnClick(requiredNumberSorszam);
            compSelector.Add_ComponentOnClick(requiredTextBoxCsoportNev);
            compSelector.Add_ComponentOnClick(requiredTextBoxFelhasznaloNev);
            compSelector.Add_ComponentOnClick(requiredTextBoxFelhasznaloUserNev);
            compSelector.Add_ComponentOnClick(requiredTextBoxFelhasznaloJelszo);
            compSelector.Add_ComponentOnClick(requiredTextBoxKod);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Org" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
                            KRT_Orgok krt_Orgok = GetBusinessObjectFromComponents();

                            KRT_Felhasznalok krt_Felhasznalok = GetBusinessObjectFromComponents_Felhasznalo();
                            KRT_Csoportok krt_Csoportok = GetBusinessObjectFromComponents_Csoport();

                            string SzerepkorNev = requiredTextBoxSzerepkorNev.Text;

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.CreateNewOrg(execParam, krt_Orgok, krt_Felhasznalok, krt_Csoportok, SzerepkorNev);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
                                KRT_Orgok krt_Orgok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Orgok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
