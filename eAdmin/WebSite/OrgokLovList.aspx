<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="OrgokLovList.aspx.cs" Inherits="OrgokLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,OrgokLovListHeaderTitle%>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                <ContentTemplate>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                        <tr>
                            <td>
                                <table>
                                    <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelKod" runat="server" Text="K�d:" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textBoxKod" runat="server" CssClass="mrUrlapInput">*</asp:TextBox>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelNev" runat="server" Text="N�v:" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textBoxNev" runat="server" CssClass="mrUrlapInput">*</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td colspan="4">
                                            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                AlternateText="R�szletes keres�s" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                <div class="listaFulFelsoCsikKicsi">
                                    <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; vertical-align: top;">
                                <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="20" Width="95%"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: left; vertical-align: top;">
                                <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.jpg"
                                    AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </eUI:eFormPanel>
        <br />
        <uc2:LovListFooter ID="LovListFooter1" runat="server" />
    </div>
</asp:Content>
