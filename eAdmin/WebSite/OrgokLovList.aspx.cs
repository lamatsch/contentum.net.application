using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;


public partial class OrgokLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;
    private const string beginSeperator = " (";
    private const string endSeperator = ")";

    private string getOrgKod()
    {
        string text = ListBoxSearchResult.SelectedItem.Text;
        int index = text.IndexOf(beginSeperator);
        if (index > -1)
        {
            return  text.Substring(0, index);
        }
        else
        {
            return text;
        }
    }

    private string getOrgNev()
    {
        string text = ListBoxSearchResult.SelectedItem.Text;
        int index = text.IndexOf(beginSeperator);
        if (index > -1)
        {
            return text.Substring(index + beginSeperator.Length,text.Length - index - beginSeperator.Length - endSeperator.Length);
        }
        else
        {
            return String.Empty;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "OrgokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("OrgokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
        + JavaScripts.SetOnClientClick("OrgokForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(false);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(false);
    }

    protected void FillListBoxSearchResult(bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
        KRT_OrgokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_OrgokSearch)Search.GetSearchObject(Page, new KRT_OrgokSearch());
            textBoxKod.Text = "";
            textBoxNev.Text = "";
        }
        else
        {
            search = new KRT_OrgokSearch();

            search.Kod.Value = textBoxKod.Text;
            search.Kod.Operator = Search.GetOperatorByLikeCharater(textBoxKod.Text);

            search.Nev.Value = textBoxNev.Text;
            search.Nev.Operator = Search.GetOperatorByLikeCharater(textBoxNev.Text);
        }
        search.OrderBy = "Sorszam, Nev, Kod";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page,Rendszerparameterek.LOVLIST_MAX_ROW);
        
        Result result = service.GetAll(execParam, search);

        List<string> columnsName = new List<string>(2);
        columnsName.Add("Kod");
        columnsName.Add("Nev");

        UI.ListBoxFill(ListBoxSearchResult, result,columnsName,beginSeperator,")", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = getOrgNev();

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                //ha kell a k�dot is visszaadjuk a h�v� formnak
                string kodTextBoxClientId = Request.QueryString.Get(QueryStringVars.KodTextBoxClientId);
                string selectedKod = getOrgKod();

                if (!String.IsNullOrEmpty(kodTextBoxClientId) && !String.IsNullOrEmpty(selectedKod))
                {
                    Dictionary<string, string> returnParams = new Dictionary<string, string>();
                    returnParams.Add(kodTextBoxClientId, selectedKod);
                    JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnParams, "KodReturnValue");
                }

                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult(true);
                    }
                    break;
            }
        }
    }

}
