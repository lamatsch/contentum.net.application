<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="OrgokSearch.aspx.cs" Inherits="OrgokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,OrgokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKod" runat="server" Text="K�d:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textBoxKod" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textBoxNev" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelSorszam" runat="server" Text="Sorsz�m:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc8:RequiredNumberBox ID="requiredNumberSorszam" runat="server" CssClass="mrUrlapInputNumber"
                                            Validate="false" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:PartnerTextBox ID="partnerTextBoxPartner" runat="server" Validate="false" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                        </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                    <tr class="urlapSor">
                                        <td colspan="5">
                                            <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                                runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
