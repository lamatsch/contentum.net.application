using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class OrgokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_OrgokSearch);


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_OrgokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_OrgokSearch)Search.GetSearchObject(Page, new KRT_OrgokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_OrgokSearch _KRT_OrgokSearch = (KRT_OrgokSearch)searchObject;

        if (_KRT_OrgokSearch != null)
        {
            textBoxKod.Text = _KRT_OrgokSearch.Kod.Value;
            textBoxNev.Text = _KRT_OrgokSearch.Nev.Value;
            requiredNumberSorszam.Text = _KRT_OrgokSearch.Sorszam.Value;

            partnerTextBoxPartner.Id_HiddenField = _KRT_OrgokSearch.Partner_id_szulo.Value;
            partnerTextBoxPartner.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);

            Ervenyesseg_SearchFormComponent1.SetDefault(
               _KRT_OrgokSearch.ErvKezd, _KRT_OrgokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_OrgokSearch SetSearchObjectFromComponents()
    {
        KRT_OrgokSearch _KRT_OrgokSearch = (KRT_OrgokSearch)SearchHeader1.TemplateObject;

        if (_KRT_OrgokSearch == null)
        {
            _KRT_OrgokSearch = new KRT_OrgokSearch();
        }

        _KRT_OrgokSearch.Kod.Value = textBoxKod.Text;
        _KRT_OrgokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(textBoxKod.Text);

        _KRT_OrgokSearch.Nev.Value = textBoxNev.Text;
        _KRT_OrgokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textBoxNev.Text);

        if (!String.IsNullOrEmpty(requiredNumberSorszam.Text))
        {
            _KRT_OrgokSearch.Sorszam.Value = requiredNumberSorszam.Text;
            _KRT_OrgokSearch.Sorszam.Operator = Query.Operators.equals;
        }

        if (partnerTextBoxPartner.Id_HiddenField != "")
        {
            _KRT_OrgokSearch.Partner_id_szulo.Value = partnerTextBoxPartner.Id_HiddenField;
            _KRT_OrgokSearch.Partner_id_szulo.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_OrgokSearch.ErvKezd, _KRT_OrgokSearch.ErvVege);

        return _KRT_OrgokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_OrgokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_OrgokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_OrgokSearch();
    }
}
