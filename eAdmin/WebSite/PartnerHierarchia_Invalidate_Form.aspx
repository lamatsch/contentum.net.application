﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="PartnerHierarchia_Invalidate_Form.aspx.cs"
    Inherits="PartnerHierarchia_Invalidate_Form"
    Title="Partnerhierarchia érvénytelenítés" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager_PartnerHierarchia_Invalidate_Form" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader_PartnerHierarchia_Invalidate_Form" runat="server" HeaderTitle="<%$Resources:Form,Felhasznalo_Szerepkor_FormHeaderTitle%>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <div class="alert">
                        <%--<span></span>--%>
                        <strong>Figyelmeztetés !</strong>
                        <br />
                        Hozzáférési jogosultságát felül kell vizsgálni !
                        <br />
                        A partnerhez rendelt tételek elérhetőek az elszámoltatási jegyzőkönyvben.
                        <br />
                        E-mail értesítés kerül kiküldésre az Alkalmazásgazdának és az érintett partnernek.
                        <br />
                        <br />
                    </div>
                    <asp:UpdatePanel ID="PartnerHierarchia_Invalidate_Form_UpdatePanel" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="InvalidateGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                PagerSettings-Visible="false" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />

                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" Checked='<%# Eval("Checked") %>' AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Nev" HeaderText="Partner név" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Nev" HeaderStyle-Width="150px" />
                                    <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Kapcsolt partner" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Partner_kapcsolt_Nev" HeaderStyle-Width="200px" />
                                    <asp:BoundField DataField="Msg" HeaderText="Ellenőrzések" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                        SortExpression="Msg" />

                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>

                <uc2:FormFooter ID="FormFooter_PartnerHierarchia_Invalidate_Form" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
