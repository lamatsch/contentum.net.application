using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

public partial class PartnerHierarchia_Invalidate_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Mode = "";
    private PageView pageView = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        Command = Request.QueryString.Get(CommandName.Command);
        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        FormHeader_PartnerHierarchia_Invalidate_Form.HeaderTitle = Resources.Form.PartnerhierarchiaFormHeaderTitle;

        FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatInvalidate");

        if (Mode == CommandName.Invalidate && Command == CommandName.Invalidate)
        {
            string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
            string ids = Request.QueryString.Get(QueryStringVars.Id);
            if (string.IsNullOrEmpty(ids))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel);
                return;
            }

            Result result = LoadTagok(partnerId, ids.Split(','));
            StringBuilder warnMsg = new StringBuilder();
            if (result != null && result.GetCount > 0)
            {
                DataTable dt = CreateDataTable();
                string msg;
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    if (VanFelhasznalohozKapcsoltUgy(row["Partner_id_kapcsolt"].ToString(), out msg))
                    {
                        AddToDataTable(ref dt, row, msg);
                    }
                    else
                    {
                        AddToDataTable(ref dt, row, msg ?? string.Empty);
                    }
                    msg = null;
                }
                Result res = new Result();
                res.Ds = new DataSet();
                res.Ds.Tables.Add(dt);
                UI.GridViewFill(InvalidateGridView, res, FormHeader_PartnerHierarchia_Invalidate_Form, FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel, PartnerHierarchia_Invalidate_Form_UpdatePanel);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter_PartnerHierarchia_Invalidate_Form.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(Footer_InvalidateButtonsClick);
        FormHeader_PartnerHierarchia_Invalidate_Form.ModeText = Resources.Form.FormHeaderInvalidate;
    }

    private void Footer_InvalidateButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Modify || e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, "PartnerkapcsolatInvalidate"))
            {
                string[] deletableItemsList = UI.GetCheckedCheckBoxIdsInGridView(InvalidateGridView);

                if (deletableItemsList == null || deletableItemsList.Length < 1)
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel);
                    return;
                }

                KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                ExecParam[] execParams = new ExecParam[deletableItemsList.Length];
                for (int i = 0; i < deletableItemsList.Length; i++)
                {
                    execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                    execParams[i].Record_Id = deletableItemsList[i];
                }
                Result result = service.MultiInvalidate(execParams);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel, result);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    #region SERVICE
    /// <summary>
    /// LoadTagok
    /// </summary>
    /// <param name="ids"></param>
    private Result LoadTagok(string partnerId, IEnumerable<string> ids)
    {
        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_Partnerek partner = new KRT_Partnerek();
        partner.Id = partnerId;
        KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
        search.Id.In(ids);
        search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.alarendelt;
        search.Tipus.Operator = Query.Operators.inner;        

        Result result = service.GetAllByPartner(ExecParam, partner, search);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel, result);
            return null;
        }
        return result;

    }

    /// <summary>
    /// VanFelhasznalohozKapcsoltUgy
    /// </summary>
    /// <param name="partnerId"></param>
    /// <param name="msg"></param>
    /// <returns></returns>
    private bool VanFelhasznalohozKapcsoltUgy(string partnerId, out string msg)
    {
        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
        search.Partner_id.Filter(partnerId);

        Result res = service.GetAll(ExecParam, search);

        msg = string.Empty;
        string felhasznaloId;
        if (!res.IsError)
        {
            if (res.GetCount < 1)
            {
                msg = "Nem rendelkezik a rendszerben hozzárendelt felhasználóval !";
                return false;
            }

            felhasznaloId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel, res);
            return false;
        }

        return Ervenytelenites.VanFelhasznalohozKapcsoltUgy(Page, FormHeader_PartnerHierarchia_Invalidate_Form.ErrorPanel, felhasznaloId, out msg);
    }
    #endregion

    private DataTable CreateDataTable()
    {
        DataTable dt = new DataTable();

        #region COLUMNS
        DataColumn column1 = new DataColumn();
        column1.DataType = System.Type.GetType("System.Guid");
        column1.ColumnName = "Id";
        dt.Columns.Add(column1);

        DataColumn column2 = new DataColumn();
        column2.DataType = System.Type.GetType("System.Guid");
        column2.ColumnName = "Partner_id";
        dt.Columns.Add(column2);

        DataColumn column3 = new DataColumn();
        column3.DataType = System.Type.GetType("System.Guid");
        column3.ColumnName = "Partner_id_kapcsolt";
        dt.Columns.Add(column3);

        DataColumn column3b = new DataColumn();
        column3b.DataType = System.Type.GetType("System.String");
        column3b.ColumnName = "Nev";
        dt.Columns.Add(column3b);

        DataColumn column4 = new DataColumn();
        column4.DataType = System.Type.GetType("System.String");
        column4.ColumnName = "Partner_kapcsolt_Nev";
        dt.Columns.Add(column4);

        DataColumn column6 = new DataColumn();
        column6.DataType = System.Type.GetType("System.Boolean");
        column6.ColumnName = "Checked";
        dt.Columns.Add(column6);

        DataColumn column5 = new DataColumn();
        column5.DataType = System.Type.GetType("System.String");
        column5.ColumnName = "Msg";
        dt.Columns.Add(column5);
        #endregion
        return dt;
    }
    private void AddToDataTable(ref DataTable dataTable, DataRow row, string msg)
    {
        DataRow dr = dataTable.NewRow();

        dr["Id"] = row["Id"];
        dr["Partner_id"] = row["Partner_id"];
        dr["Partner_id_kapcsolt"] = row["Partner_id_kapcsolt"];
        dr["Nev"] = row["Nev"]; ;
        dr["Partner_kapcsolt_Nev"] = row["Partner_kapcsolt_Nev"];
        dr["Checked"] = "true";
        if (!string.IsNullOrEmpty(msg))
            dr["Msg"] = msg;

        dataTable.Rows.Add(dr);
    }
}
