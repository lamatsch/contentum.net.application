<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="Partner_Felhasznalo_Form.aspx.cs" Inherits="Partner_Felhasznalo_Form" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Partner_Felhasznalo_FormHeaderTitle%>" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label2" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label>&nbsp;</td>
                            <td class="mrUrlapMezo">
                                <uc3:PartnerTextBox ID="PartnerTextBox1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px;">
                                <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelFelhasznalo" runat="server" Text="Felhasználó:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc4:FelhasznaloTextBox ID="FelhasznaloTextBox1" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px" >
                                &nbsp;</td>
                            <td class="mrUrlapMezo">
                                &nbsp;</td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>