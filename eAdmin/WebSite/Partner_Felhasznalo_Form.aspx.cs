using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Partner_Felhasznalo_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String Id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(Id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = Id;

                KRT_FelhasznalokService KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                Result FelhasznaloResult = KRT_FelhasznalokService.Get(execParam);
                if (String.IsNullOrEmpty(FelhasznaloResult.ErrorCode))
                {
                    KRT_Felhasznalok krt_Felhasznalok = (KRT_Felhasznalok)FelhasznaloResult.Record;
                    LoadComponentsFromBusinessObject(krt_Felhasznalok);

                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, FelhasznaloResult);
                }
            }

            PartnerTextBox1.ReadOnly = true;
            labelPartner.CssClass = "mrUrlapInputWaterMarked";

            if (Command == CommandName.View)
            {
                FelhasznaloTextBox1.ReadOnly = true;
                labelFelhasznalo.CssClass = "mrUrlapInputWaterMarked";
            }

            if (Command == CommandName.Modify)
            {
                FelhasznaloTextBox1.ReadOnly = false;
            }
        }
        else if (Command == CommandName.New)
        {
            string Id = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(Id))
            {
                PartnerTextBox1.Id_HiddenField = Id;
                PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
                PartnerTextBox1.ReadOnly = true;
                labelPartner.CssClass = "mrUrlapInputWaterMarked";
            }
        }

        FelhasznaloTextBox1.FilterType = Constants.FilterType.Felhasznalok.FelhasznalokWithoutPartner;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }



    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Felhasznalok KRT_Felhasznalok)
    {
        PartnerTextBox1.Id_HiddenField = KRT_Felhasznalok.Partner_id;
        PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        FelhasznaloTextBox1.Id_HiddenField = KRT_Felhasznalok.Id;
        FelhasznaloTextBox1.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);

        FormHeader1.Record_Ver = KRT_Felhasznalok.Base.Ver;

    }

    // form --> business object
    private KRT_Felhasznalok GetBusinessObjectFromComponents()
    {
        KRT_Felhasznalok KRT_Felhasznalok = new KRT_Felhasznalok();
        KRT_Felhasznalok.Updated.SetValueAll(false);
        KRT_Felhasznalok.Base.Updated.SetValueAll(false);

        KRT_Felhasznalok.Partner_id = PartnerTextBox1.Id_HiddenField;
        KRT_Felhasznalok.Updated.Partner_id = true;

        KRT_Felhasznalok.Id = FelhasznaloTextBox1.Id_HiddenField;
        KRT_Felhasznalok.Updated.Id = true;

        KRT_Felhasznalok.Base.Ver = FormHeader1.Record_Ver;
        KRT_Felhasznalok.Base.Updated.Ver = true;

        return KRT_Felhasznalok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(PartnerTextBox1);
            compSelector.Add_ComponentOnClick(FelhasznaloTextBox1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            AddOrModifyPartner(false);
                            break;
                        }
                    //LZS - BUG_12514
                    case CommandName.Modify:
                        {
                            AddOrModifyPartner(true);
                            break;
                        }

                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void AddOrModifyPartner(bool isModify)
    {
        KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        KRT_PartnerekService partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        String recordId = FelhasznaloTextBox1.Id_HiddenField;
        execParam.Record_Id = recordId;
        Result FelhasznaloResult = felhasznalokService.Get(execParam);

        if (String.IsNullOrEmpty(recordId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {
            if (String.IsNullOrEmpty(FelhasznaloResult.ErrorCode))
            {
                KRT_Felhasznalok krt_Felhasznalok = (KRT_Felhasznalok)FelhasznaloResult.Record;
                
                #region Invalidate old user
                if (isModify)
                {
                    execParam.Record_Id = Request.QueryString[QueryStringVars.Id].ToString();
                    ExecParam[] execParams = new ExecParam[1];
                    execParams[0] = execParam;

                    Result resultInvalidate = felhasznalokService.MultiDeletePartnerID(execParams);
                    if (resultInvalidate.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultInvalidate);
                        return;
                    }
                }
                #endregion

                if (String.IsNullOrEmpty(krt_Felhasznalok.Partner_id))
                {

                    FormHeader1.Record_Ver = krt_Felhasznalok.Base.Ver;

                    krt_Felhasznalok = GetBusinessObjectFromComponents();
                    execParam.Record_Id = recordId;

                    Result result = felhasznalokService.AddPartner(execParam, krt_Felhasznalok);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }


                }
                else
                {
                    execParam.Record_Id = krt_Felhasznalok.Partner_id;
                    Result resultPartner = partnerekService.Get(execParam);
                    if (String.IsNullOrEmpty(resultPartner.ErrorCode))
                    {
                        KRT_Partnerek partner = (KRT_Partnerek)resultPartner.Record;
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hozz�rendel�si hiba", "A felhaszn�l� m�r a \" " + partner.Nev + " \" nev� partnerhez van rendelve!");
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultPartner);
                    }
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, FelhasznaloResult);
            }
        }
    }

}
