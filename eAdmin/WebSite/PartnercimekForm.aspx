<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnercimekForm.aspx.cs" Inherits="PartnercimekForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>  
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnercimekFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                           
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc8:PartnerTextBox ID="PartnerTextBox1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="labelCim" runat="server" Text="C�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc9:CimekTextBox id="CimekTextBox1" runat="server">
                                            </uc9:CimekTextBox>
                                            <asp:HiddenField ID="hiddenCimTipus" runat="server" />      
                                       </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelCimFajta" runat="server" Text="C�m fajt�ja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:KodtarakDropDownList ID="KodtarakDropDownListCimFajta" runat="server"/>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                    </td>
                                </tr>
                                </tbody>
                            </table>                      
                    </eUI:eFormPanel>
                    <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                    <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

