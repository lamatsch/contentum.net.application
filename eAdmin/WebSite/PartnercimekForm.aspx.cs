using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class PartnercimekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kodcsoportCimFajta= "CIM_FAJTA";

    private void SetViewControls()
    {
        PartnerTextBox1.ReadOnly = true;
        CimekTextBox1.ReadOnly = true;
        //KodtarakDropDownListCimFajta.Enabled = false;
        KodtarakDropDownListCimFajta.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelCim.CssClass = "mrUrlapInputWaterMarked";
        labelCimFajta.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

    }
 
    private void SetModifyControls()
    {
        string cimId = Request.QueryString.Get(QueryStringVars.CimId);
        if (!String.IsNullOrEmpty(cimId))
        {
            CimekTextBox1.ReadOnly = true;
            labelCim.CssClass = "mrUrlapInputWaterMarked";
        }
        else
        {
            PartnerTextBox1.ReadOnly = true;

            labelPartner.CssClass = "mrUrlapInputWaterMarked";
        }
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerCim" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_PartnerCimek krt_partnercimek = (KRT_PartnerCimek)result.Record;
                    LoadComponentsFromBusinessObject(krt_partnercimek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

        }
        else if (Command == CommandName.New)
        {
            string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(partnerId))
            {
                PartnerTextBox1.Id_HiddenField = partnerId;
                PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
                PartnerTextBox1.ReadOnly = true;
                labelPartner.CssClass = "mrUrlapInputWaterMarked";
            }
            string cimId = Request.QueryString.Get(QueryStringVars.CimId);
            if (!String.IsNullOrEmpty(cimId))
            {
                CimekTextBox1.Id_HiddenField = cimId;
                CimekTextBox1.SetCimekTextBoxById(FormHeader1.ErrorPanel);
                CimekTextBox1.ReadOnly = true;
                labelCim.CssClass = "mrUrlapInputWaterMarked";
            }

            if (!IsPostBack)
            {
                KodtarakDropDownListCimFajta.FillDropDownList(kodcsoportCimFajta, true, FormHeader1.ErrorPanel);
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }

        string filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (!String.IsNullOrEmpty(filterType))
        {
            if (filterType == KodTarak.Cim_Tipus.Postai)
                FormHeader1.HeaderTitle = Resources.Form.PartnercimekFormHeaderTitle_Filtered_Postai;
            else
                FormHeader1.HeaderTitle = Resources.Form.PartnercimekFormHeaderTitle_Filtered_Egyeb;


            CimekTextBox1.OnClick_New = JavaScripts.SetOnClientClick("CimekForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType + "&" +
            QueryStringVars.HiddenFieldId + "=" + CimekTextBox1.HiddenField.ClientID + "&" + QueryStringVars.TextBoxId + "=" + CimekTextBox1.TextBox.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

            CimekTextBox1.OnClick_Lov = JavaScripts.SetOnClientClick("CimekLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + CimekTextBox1.HiddenField.ClientID + "&" + QueryStringVars.TextBoxId + "=" + CimekTextBox1.TextBox.ClientID +
            "&" + QueryStringVars.Filter + "=" + filterType
            , 900, 650, "", "", false);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        CimekTextBox1.TextBox.TextMode = TextBoxMode.MultiLine;
        CimekTextBox1.TextBox.Rows = 2;
        CimekTextBox1.TextBox.Width = Unit.Pixel(400);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_PartnerCimek krt_partnercimek)
    {
        PartnerTextBox1.Id_HiddenField = krt_partnercimek.Partner_id;
        PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        CimekTextBox1.Id_HiddenField = krt_partnercimek.Cim_Id;
        CimekTextBox1.SetCimekTextBoxById(FormHeader1.ErrorPanel);

        KodtarakDropDownListCimFajta.FillAndSetSelectedValue(kodcsoportCimFajta, krt_partnercimek.Fajta, true, FormHeader1.ErrorPanel);

        ErvenyessegCalendarControl1.ErvKezd = krt_partnercimek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_partnercimek.ErvVege;

        FormHeader1.Record_Ver = krt_partnercimek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_partnercimek.Base);
    }

    // form --> business object
    private KRT_PartnerCimek GetBusinessObjectFromComponents()
    {
        KRT_PartnerCimek krt_partnercimek = new KRT_PartnerCimek();
        krt_partnercimek.Updated.SetValueAll(false);
        krt_partnercimek.Base.Updated.SetValueAll(false);

        krt_partnercimek.Partner_id = PartnerTextBox1.Id_HiddenField;
        krt_partnercimek.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBox1);

        krt_partnercimek.Cim_Id = CimekTextBox1.Id_HiddenField;
        krt_partnercimek.Updated.Cim_Id = pageView.GetUpdatedByView(CimekTextBox1);

        krt_partnercimek.Fajta = KodtarakDropDownListCimFajta.SelectedValue;
        krt_partnercimek.Updated.Fajta = pageView.GetUpdatedByView(KodtarakDropDownListCimFajta);

        //krt_partnercimek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_partnercimek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_partnercimek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_partnercimek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_partnercimek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_partnercimek.Base.Ver = FormHeader1.Record_Ver;
        krt_partnercimek.Base.Updated.Ver = true;

        return krt_partnercimek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
                        
            compSelector.Add_ComponentOnClick(PartnerTextBox1);
            compSelector.Add_ComponentOnClick(CimekTextBox1);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListCimFajta);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "PartnerCim" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                            KRT_PartnerCimek krt_partnercimek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_partnercimek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                                KRT_PartnerCimek krt_partnercimek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_partnercimek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

}

