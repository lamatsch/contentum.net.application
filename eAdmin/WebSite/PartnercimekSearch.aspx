<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnercimekSearch.aspx.cs" Inherits="PartnercimekSearch" Title="Untitled Page" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc8" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,PartnercimekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="UpdatePanel1_Load">
                        <contenttemplate>
<eUI:eFormPanel id="EFormPanel1" runat="server"><TABLE cellSpacing=0 cellPadding=0 width="100%"><TBODY><TR class="urlapNyitoSor"><TD class="mrUrlapCaption"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD><TD class="mrUrlapMezo"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Tipus_Csoport_Label" runat="server" Text="T�pus:"></asp:Label></TD><TD class="mrUrlapMezo"><asp:DropDownList id="Tipus_Cimek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" OnSelectedIndexChanged="Tipus_Cimek_SelectedIndexChanged">
                        <asp:ListItem Value="1">Postai c&#237;m</asp:ListItem>
                        <asp:ListItem Value="2">Egy&#233;b c&#237;m</asp:ListItem>
                    </asp:DropDownList></TD></TR></TBODY></TABLE>&nbsp; <BR /><asp:Panel id="PostaiCim_Panel" runat="server"><TABLE cellSpacing=0 cellPadding=0 width="100%"><TBODY><TR class="urlapNyitoSor"><TD class="mrUrlapCaption"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD><TD class="mrUrlapMezo"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Tipus_PostaiCimek_Label" runat="server" Text="Postai c�m t�pus:"></asp:Label>&nbsp;</TD><TD class="mrUrlapMezo"><asp:DropDownList id="Tipus_PostaiCimek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                        </asp:DropDownList></TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Telepules_Label" runat="server" Text="Telep�l�s:"></asp:Label>&nbsp;</TD><TD class="mrUrlapMezo"><uc7:TelepulesTextBox id="TelepulesTextBox1" runat="server" Validate="false"></uc7:TelepulesTextBox> </TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Irsz_Label" runat="server" Text="Ir�ny�t�sz�m:" CssClass="FormLabelInside"></asp:Label></TD><TD class="mrUrlapMezo"><uc8:RequiredNumberBox id="Irsz_RequiredNumberBox" runat="server" Validate="false"></uc8:RequiredNumberBox> </TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Kozterulet_neve_Label" runat="server" Text="K�zter�let neve:"></asp:Label></TD><TD class="mrUrlapMezo"><asp:TextBox id="Kozterulet_neve_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox> <asp:DropDownList id="Kozterulet_tipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox">
                            <asp:ListItem>utca</asp:ListItem>
                        </asp:DropDownList></TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Hazszam_Label" runat="server" Text="H�zsz�m:"></asp:Label></TD><TD class="mrUrlapMezo"><uc8:RequiredNumberBox id="Hazszam_RequiredNumberBox" runat="server" Validate="false">
                        </uc8:RequiredNumberBox> </TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Hrsz_Label" runat="server" Text="Helyrajzi sz�m:"></asp:Label></TD><TD class="mrUrlapMezo"><asp:TextBox id="Hrsz_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></TD></TR></TBODY></TABLE></asp:Panel> <asp:Panel id="EgyebCimek_Panel" runat="server"><TABLE cellSpacing=0 cellPadding=0 width="100%"><TBODY><TR class="urlapNyitoSor"><TD class="mrUrlapCaption"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD><TD class="mrUrlapMezo"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD></TR><TR class="urlapSor"><TD class="mrUrlapCaption"><asp:Label id="Megnevezes_EgyebCimek_Label" runat="server" Text="Megnevez�s:"></asp:Label>&nbsp;</TD><TD class="mrUrlapMezo"><asp:TextBox id="Megnevezes_EgyebCimek_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></TD></TR></TBODY></TABLE></asp:Panel> <TABLE cellSpacing=0 cellPadding=0 width="100%"><TBODY><TR class="urlapNyitoSor"><TD class="mrUrlapCaption"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD><TD class="mrUrlapMezo"><IMG alt=" " src="images/hu/design/spacertrans.gif" /></TD></TR><TR class="urlapSor"><TD colSpan=2 rowSpan=2><uc5:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server" Validate="false"></uc5:Ervenyesseg_SearchFormComponent> </TD></TR><TR class="urlapSor"><TD colSpan=2 rowSpan=1></TD></TR><TR class="urlapSor"></TR><TR class="urlapSor"><TD colSpan=2>&nbsp;&nbsp;&nbsp; <uc3:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1" runat="server" __designer:wfdid="w9"></uc3:TalalatokSzama_SearchFormComponent></TD></TR></TBODY></TABLE></eUI:eFormPanel> 
</ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

