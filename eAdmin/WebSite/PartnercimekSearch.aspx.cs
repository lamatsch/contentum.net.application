using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class PartnercimekSearch : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Tipus_Cimek_DropDownList.AutoPostBack = true;

      
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            setDefaultValues();
        }

    }

    private void setDefaultValues()
    {
        EgyebCimek_Panel.Visible = false;
        PostaiCim_Panel.Visible = true;

        Tipus_Cimek_DropDownList.SelectedIndex = 0;

        Tipus_PostaiCimek_DropDownList.SelectedIndex = 0;
        TelepulesTextBox1.Text = "";
        Irsz_RequiredNumberBox.Text = "";
        Kozterulet_neve_TextBox.Text = "";
        Kozterulet_tipus_DropDownList.SelectedIndex = 0;
        Hazszam_RequiredNumberBox.Text = "";
        Hrsz_TextBox.Text = "";

        Megnevezes_EgyebCimek_TextBox.Text = "";
        
        Ervenyesseg_SearchFormComponent1.SetDefault();
        //TalalatokSzama_SearchFormComponent1.SetDefault();
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            setDefaultValues();
        }
    }


    protected void Tipus_Cimek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Tipus_Cimek_DropDownList.SelectedIndex == 0)
        {
            PostaiCim_Panel.Visible = true;
            EgyebCimek_Panel.Visible = false;
        }
        else
        {
            PostaiCim_Panel.Visible = false;
            EgyebCimek_Panel.Visible = true;
        }

    }
    protected void UpdatePanel1_Load(object sender, EventArgs e)
    {
       
    }
}
