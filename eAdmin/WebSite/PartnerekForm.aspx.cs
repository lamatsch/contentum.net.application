﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Text;

public partial class PartnerekForm : Contentum.eUtility.UI.PageBase
{
    private PageView pageView = null;

    private Authentication auth = new Authentication();
    private string Command = "";

    private string filterType = "All"; // lehetséges értékek: "All" (==""), "Szervezet", "Szemely"
    private const string kcs_PARTNER_TIPUS = "PARTNER_TIPUS";
    private const string kcs_CEGTIPUS = "CEGTIPUS";
    // BLG_346
    private const string kcs_IRAT_MINOSITES = "IRAT_MINOSITES";

    //LZS - BLG_1915
    private const string kcs_PARTNER_ALLAPOT = "PARTNER_ALLAPOT";
    private const string kod_JOVAHAGYANDO = "JOVAHAGYANDO";
    private const string kod_JOVAHAGYOTT = "JOVAHAGYOTT";

    private ComponentSelectControl compSelector = null;

    //BLG_5778
    private bool isTUK = false;

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");

        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, No.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }

    public static class Sex
    {
        public static readonly ListItem Male = new ListItem("Férfi", "1");
        public static readonly ListItem Female = new ListItem("Nõ", "2");
        public static readonly ListItem NotSet = new ListItem("Nincs megadva", Constants.BusinessDocument.nullString);
        public static void FillRadioList(RadioButtonList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Male);
            list.Items.Add(Female);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillRadioList(RadioButtonList list)
        {
            FillRadioList(list, NotSet.Value);
        }
        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            if (selectedValue == Male.Value || selectedValue == Female.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                foreach (ListItem item in list.Items)
                {
                    item.Selected = false;
                }
            }
        }
        public static string GetSelectedValue(RadioButtonList list)
        {
            if (list.SelectedValue == Male.Value || list.SelectedValue == Female.Value)
            {
                return list.SelectedValue;
            }
            else
            {
                return NotSet.Value;
            }
        }
    }

    private void SetViewControls()
    {
        //partner(közös)
        RequiredTextBoxNev.ReadOnly = true;
        KodtarakDropDownListPartnerTipus.ReadOnly = true;
        OrszagokTextBox1.ReadOnly = true;
        //ddownBelso.Enabled = false;
        ddownBelso.ReadOnly = true;
        KodtarakDropDownListPartnerAllapot.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
        KulsoAzonositoTextBox.ReadOnly = true;

        //szemely
        textTitulus.ReadOnly = true;
        RequiredTexBoxCsaladiNev.ReadOnly = true;
        RequiredTextBoxUtoNev.ReadOnly = true;
        textTovabbiUtonev.ReadOnly = true;
        textAnyjaCsaladiNeve.ReadOnly = true;
        textAnyjaUtoNev.ReadOnly = true;
        textAnyjaTovabbiUtoNev.ReadOnly = true;
        textSzuletesiCsaladiNev.ReadOnly = true;
        textSzuletesiUtoNev.ReadOnly = true;
        textSzuletesiTovabbiUtoNev.ReadOnly = true;
        OrszagTextBoxSzuletesi.ReadOnly = true;
        TelepulesTextBoxSzuletesi.ReadOnly = true;
        CalendarSzuletesiIdo.ReadOnly = true;
        textTajSzam.ReadOnly = true;
        textSzigSzam.ReadOnly = true;
        rbListNeme.Enabled = false;
        textSzemelyiAzonosito.ReadOnly = true;
        textAdoAzonosito.ReadOnly = true;
        textAdoszamSzemely.ReadOnly = true;
        // BLG_346
        textBeosztas.ReadOnly = true;
        KodtarakDropDownListMinositesiSzint.ReadOnly = true;

        //vallalkozas
        textAdoSzamVallalkozas.ReadOnly = true;
        textTBSzam.ReadOnly = true;
        textCegJegyzekSzam.ReadOnly = true;
        KodtarakDropDownListCegTipus.ReadOnly = true;

        //partner(közös)
        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelPartnerTipus.CssClass = "mrUrlapInputWaterMarked";
        labelOrszag.CssClass = "mrUrlapInputWaterMarked";
        labelBelso.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

        //szemely
        labelTitulus.CssClass = "mrUrlapInputWaterMarked";
        labelCsaladiNev.CssClass = "mrUrlapInputWaterMarked";
        labelUtoNev.CssClass = "mrUrlapInputWaterMarked";
        labelTovabbiUtonev.CssClass = "mrUrlapInputWaterMarked";
        labelTovabbiSzemely.CssClass = "mrUrlapInputWaterMarked";
        labelAnyjaCsaladiNeve.CssClass = "mrUrlapInputWaterMarked";
        labelAnyjaUtoNev.CssClass = "mrUrlapInputWaterMarked";
        labelAnyjaTovabbiUtoNev.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiCsaladiNev.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiUtoneNev.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiTovabbiUtoneNev.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiOrszag.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiHely.CssClass = "mrUrlapInputWaterMarked";
        labelSzuletesiIdo.CssClass = "mrUrlapInputWaterMarked";
        labelTajSzam.CssClass = "mrUrlapInputWaterMarked";
        labelSzemelyi.CssClass = "mrUrlapInputWaterMarked";
        labelNeme.CssClass = "mrUrlapInputWaterMarked";
        labelSzemelyiAzonosito.CssClass = "mrUrlapInputWaterMarked";
        labelAdoAzonosito.CssClass = "mrUrlapInputWaterMarked";
        labelAdoSzamSzemely.CssClass = "mrUrlapInputWaterMarked";
        // BLG_346
        labelBeosztas.CssClass = "mrUrlapInputWaterMarked";
        labelMinositesiSzint.CssClass = "mrUrlapInputWaterMarked";

        //vallalkozas
        labelTovabbiSzervezet.CssClass = "mrUrlapInputWaterMarked";
        labelAdoSzamVallalkozas.CssClass = "mrUrlapInputWaterMarked";
        labelTBSzam.CssClass = "mrUrlapInputWaterMarked";
        labelCegJegyzekSzam.CssClass = "mrUrlapInputWaterMarked";
        labelCegTipus.CssClass = "mrUrlapInputWaterMarked";

    }

    private void SetModifyControls()
    {
        KodtarakDropDownListPartnerTipus.ReadOnly = true;

        labelPartnerTipus.CssClass = "mrUrlapInputWaterMarked";
    }

    private void SetActivePanel()
    {
        if (Command == CommandName.DesignView)
        {
            panelSzemelyek.Visible = true;
            panelVallalkozasok.Visible = true;
            trPartnerNev.Visible = true;
            Sex.FillRadioList(rbListNeme);
        }
        else
        {

            switch (KodtarakDropDownListPartnerTipus.SelectedValue)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    panelSzemelyek.Visible = true;
                    panelVallalkozasok.Visible = false;
                    trPartnerNev.Visible = false;
                    #region BLG_5778
                    if (isTUK)
                    {
                        ParentsDataRow.Visible = false;
                        ParentsDataRow2.Visible = false;
                        TajSzigDataRow.Visible = false;
                        VatDataRow.Visible = false;
                        labelSzemelyiAzonosito.Visible = false;
                        textSzemelyiAzonosito.Visible = false;
                    }
                    #endregion
                    break;
                case KodTarak.Partner_Tipus.Szervezet:
                    panelSzemelyek.Visible = false;
                    //BLG_5778
                    panelVallalkozasok.Visible = !isTUK;
                    trPartnerNev.Visible = true;
                    break;
                case KodTarak.Partner_Tipus.Alkalmazas:
                    panelSzemelyek.Visible = false;
                    panelVallalkozasok.Visible = false;
                    trPartnerNev.Visible = true;
                    break;
                default:
                    goto case KodTarak.Partner_Tipus.Alkalmazas;
            }

            // New módban a csak olvasható mezõk ne látszódjanak:
            if (Command == CommandName.New)
            {
                trKulsoAzonosito.Visible = false;
            }
        }
    }

    private void FillComponents()
    {
        KodtarakDropDownListPartnerTipus.FillDropDownList(kcs_PARTNER_TIPUS, false, FormHeader1.ErrorPanel);
        KodtarakDropDownListPartnerTipus.SetSelectedValue(KodTarak.Partner_Tipus.Szemely);
        BoolString.FillDropDownList(ddownBelso);
        KodtarakDropDownListCegTipus.FillDropDownList(kcs_CEGTIPUS, true, FormHeader1.ErrorPanel);
        Sex.FillRadioList(rbListNeme);
        // BLG_346
        KodtarakDropDownListMinositesiSzint.FillDropDownList(kcs_IRAT_MINOSITES, true, FormHeader1.ErrorPanel);

        //LZS - BLG_1915
        KodtarakDropDownListPartnerAllapot.FillDropDownList(kcs_PARTNER_ALLAPOT, false, FormHeader1.ErrorPanel);

        //LZS - BLG_6234
        //NMHH case
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PARTNER_LETREHOZAS_JOVAHAGYANDO))
        {
            if (!FunctionRights.GetFunkcioJog(Page, "PartnerApproval"))
            {
                KodtarakDropDownListPartnerAllapot.Enabled = false;
                KodtarakDropDownListPartnerAllapot.SelectedValue = kod_JOVAHAGYANDO;
            }
            else
            {
                KodtarakDropDownListPartnerAllapot.Enabled = true;
            }
        }//FPH, BOPMH, CSEPEL case
        else
        {
            KodtarakDropDownListPartnerAllapot.SelectedValue = kod_JOVAHAGYOTT;
            KodtarakDropDownListPartnerAllapot.ReadOnly = true;
        }
    }

    private string getPartnerNev()
    {
        if (KodtarakDropDownListPartnerTipus.SelectedValue == KodTarak.Partner_Tipus.Szemely)
        {
            StringBuilder nev = new StringBuilder();
            if (!string.IsNullOrEmpty(textTitulus.Text.Trim()))
            {
                nev.Append(textTitulus.Text.Trim());
                nev.Append(" ");
            }
            nev.Append(RequiredTexBoxCsaladiNev.Text.Trim());
            nev.Append(" ");
            nev.Append(RequiredTextBoxUtoNev.Text.Trim());
            if (string.IsNullOrEmpty(textTovabbiUtonev.Text))
            {
                nev.Append(" ");
                nev.Append(textTovabbiUtonev.Text.Trim());
            }

            return nev.ToString();
        }
        else
        {
            return RequiredTextBoxNev.Text;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        #region BLG_5778
        ExecParam execParamTUK = UI.SetExecParamDefault(Page);
        isTUK = Rendszerparameterek.IsTUK(execParamTUK);
        #endregion

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, Command);
                compSelector = (ASP.component_componentselectcontrol_ascx)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Partner" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Partnerek krt_Partnerek = (KRT_Partnerek)result.Record;
                    LoadComponentsFromBusinessObject(krt_Partnerek);

                    #region BUG_6182
                    //LZS - migrált adatoknál az első változtatáskor nem hozta létre a historyba a rekordokat, 
                    //ezért ezekre betöltéskor nyomok egy mentést.
                    RecordHistoryService historyService = eAdminService.ServiceFactory.GetRecordHistoryService();

                    Result resHistoryData = historyService.GetAllByRecord(execParam, "KRT_Partnerek");

                    if (krt_Partnerek.Base.Ver == "1" && resHistoryData.Ds.Tables[0].Rows.Count == 0)
                    {
                        krt_Partnerek.Updated.Nev = true;
                        Result updateResult = service.Update(execParam, krt_Partnerek);

                        //aktuális verzió eltárolása
                        FormHeader1.Record_Ver = (Convert.ToInt32(krt_Partnerek.Base.Ver) + 1).ToString();
                    }
                    #endregion

                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            //komponensek feltöltése (dropdownlist,radiobuttonlist)
            FillComponents();

            SetFilterType();

        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {

            SetModifyControls();
        }

        SetActivePanel();

        SetFormTemplateVisibility();

        JavaScripts.SetAutoCompleteContextKey(Page, OrszagTextBoxSzuletesi.TextBox, TelepulesTextBoxSzuletesi.TextBox, TelepulesTextBoxSzuletesi.AutoCompleteExtenderClientID);
    }

    private void SetFilterType()
    {
        // van-e szûrés esetleg szervezetre, vagy szemelyre:
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType == null) filterType = "";
        switch (filterType)
        {
            case "Szervezet":
                FormHeader1.HeaderTitle = Resources.Form.PartnerekFormHeaderTitle_Filtered_Szervezet;
                KodtarakDropDownListPartnerTipus.SetSelectedValue(KodTarak.Partner_Tipus.Szervezet);
                KodtarakDropDownListPartnerTipus.Enabled = false;
                break;
            case "Szemely":
                FormHeader1.HeaderTitle = Resources.Form.PartnerekFormHeaderTitle_Filtered_Szemely;
                KodtarakDropDownListPartnerTipus.SetSelectedValue(KodTarak.Partner_Tipus.Szemely);
                KodtarakDropDownListPartnerTipus.Enabled = false;
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.ButtonsClick += FormHeader1_ButtonsClick;

        FormFooter1.ButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        KodtarakDropDownListPartnerTipus.DropDownList.AutoPostBack = true;
        KodtarakDropDownListPartnerTipus.DropDownList.SelectedIndexChanged += new EventHandler(PartnerTipusDropDownList_SelectedIndexChanged);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (IsPostBack && "loadPartner".Equals(Request.Params["__EVENTARGUMENT"]))
        {
            LoadComponentsFromId(RequiredTextBoxNev.Id_HiddenField);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        RequiredTextBoxNev.PartnerTipus = KodtarakDropDownListPartnerTipus.SelectedValue;
    }

    private void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        TemplateHandler(e);
    }

    private void LoadComponentsFromBusinessObject(KRT_Partnerek krt_Partnerek)
    {
        RequiredTextBoxNev.Text = krt_Partnerek.Nev;
        KodtarakDropDownListPartnerTipus.FillAndSetSelectedValue(kcs_PARTNER_TIPUS, krt_Partnerek.Tipus, FormHeader1.ErrorPanel);
        OrszagokTextBox1.Id_HiddenField = krt_Partnerek.Orszag_Id;
        OrszagokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        BoolString.FillDropDownList(ddownBelso, krt_Partnerek.Belso);
        ErvenyessegCalendarControl1.ErvKezd = krt_Partnerek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Partnerek.ErvVege;
        KulsoAzonositoTextBox.Text = krt_Partnerek.KulsoAzonositok;


        //LZS - BLG 1915
        if (!string.IsNullOrEmpty(krt_Partnerek.Allapot))
        {
            KodtarakDropDownListPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, krt_Partnerek.Allapot, FormHeader1.ErrorPanel);
        }
        else
        {
            KodtarakDropDownListPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, KodTarak.Partner_Allapot.Jovahagyando, FormHeader1.ErrorPanel);
        }


        if (!FunctionRights.GetFunkcioJog(Page, "PartnerApproval"))
        {
            KodtarakDropDownListPartnerAllapot.Enabled = false;
        }
        else
        {
            KodtarakDropDownListPartnerAllapot.Enabled = true;
        }

        switch (krt_Partnerek.Tipus)
        {
            case KodTarak.Partner_Tipus.Alkalmazas:
                {
                    break;
                }
            case KodTarak.Partner_Tipus.Szemely:
                {
                    if (krt_Partnerek is KRT_PartnerSzemelyek)
                    {
                        KRT_Szemelyek krt_Szemelyek = ((KRT_PartnerSzemelyek)krt_Partnerek).Szemelyek;
                        LoadSzemelyekFromBusinessObject(krt_Szemelyek);
                    }
                    break;
                }
            case KodTarak.Partner_Tipus.Szervezet:
                {
                    if (krt_Partnerek is KRT_PartnerVallalkozasok)
                    {
                        KRT_Vallalkozasok krt_Vallalkozasok = ((KRT_PartnerVallalkozasok)krt_Partnerek).Vallalkozasok;
                        LoadVallalkozasokFromBusinessObject(krt_Vallalkozasok);
                    }
                    break;
                }
            default:
                break;
        }
        if (string.IsNullOrEmpty(ErvenyessegCalendarControl1.ErvKezd))
        {
            ErvenyessegCalendarControl1.ErvKezd = DateTime.Today.ToString();
        }
        //aktuális verzió eltárolása
        FormHeader1.Record_Ver = krt_Partnerek.Base.Ver;
        //A modósitás,létrehozás form beállítása
        FormPart_CreatedModified1.SetComponentValues(krt_Partnerek.Base);
    }

    private void LoadSzemelyekFromBusinessObject(KRT_Szemelyek krt_Szemelyek)
    {
        textTitulus.Text = krt_Szemelyek.UjTitulis;
        RequiredTexBoxCsaladiNev.Text = krt_Szemelyek.UjCsaladiNev;
        RequiredTextBoxUtoNev.Text = krt_Szemelyek.UjUtonev;
        textTovabbiUtonev.Text = krt_Szemelyek.UjTovabbiUtonev;
        textAnyjaCsaladiNeve.Text = krt_Szemelyek.AnyjaNeveCsaladiNev;
        textAnyjaUtoNev.Text = krt_Szemelyek.AnyjaNeveElsoUtonev;
        textAnyjaTovabbiUtoNev.Text = krt_Szemelyek.AnyjaNeveTovabbiUtonev;

        textSzuletesiCsaladiNev.Text = krt_Szemelyek.SzuletesiCsaladiNev;
        textSzuletesiUtoNev.Text = krt_Szemelyek.SzuletesiElsoUtonev;
        textSzuletesiTovabbiUtoNev.Text = krt_Szemelyek.SzuletesiTovabbiUtonev;
        OrszagTextBoxSzuletesi.Text = krt_Szemelyek.SzuletesiOrszag;
        TelepulesTextBoxSzuletesi.Text = krt_Szemelyek.SzuletesiHely;
        CalendarSzuletesiIdo.Text = krt_Szemelyek.SzuletesiIdo;
        textTajSzam.Text = krt_Szemelyek.TAJSzam;
        textSzigSzam.Text = krt_Szemelyek.SZIGSzam;
        Sex.FillRadioList(rbListNeme, krt_Szemelyek.Neme);
        textSzemelyiAzonosito.Text = krt_Szemelyek.SzemelyiAzonosito;
        textAdoAzonosito.Text = krt_Szemelyek.Adoazonosito;
        textAdoszamSzemely.SetTextBoxByAdoszam(krt_Szemelyek.Adoszam, krt_Szemelyek.KulfoldiAdoszamJelolo);
        // BLG_346
        textBeosztas.Text = krt_Szemelyek.Beosztas;
        KodtarakDropDownListMinositesiSzint.FillAndSetSelectedValue(kcs_IRAT_MINOSITES, krt_Szemelyek.MinositesiSzint, true, FormHeader1.ErrorPanel);

    }

    private void LoadVallalkozasokFromBusinessObject(KRT_Vallalkozasok krt_Vallalkozasok)
    {
        textAdoSzamVallalkozas.SetTextBoxByAdoszam(krt_Vallalkozasok.Adoszam, krt_Vallalkozasok.KulfoldiAdoszamJelolo);
        textTBSzam.Text = krt_Vallalkozasok.TB_Torzsszam;
        textCegJegyzekSzam.Text = krt_Vallalkozasok.Cegjegyzekszam;
        KodtarakDropDownListCegTipus.FillAndSetSelectedValue(kcs_CEGTIPUS, krt_Vallalkozasok.Tipus, true, FormHeader1.ErrorPanel);
    }

    private KRT_Partnerek GetBusinessObjectFromComponents()
    {
        KRT_Partnerek krt_Partnerek = new KRT_Partnerek();
        krt_Partnerek.Updated.SetValueAll(false);
        krt_Partnerek.Base.Updated.SetValueAll(false);

        krt_Partnerek.Nev = RequiredTextBoxNev.Text;
        krt_Partnerek.Updated.Nev = pageView.GetUpdatedByView(RequiredTextBoxNev);
        if (!String.IsNullOrEmpty(KodtarakDropDownListPartnerTipus.SelectedValue))
        {
            krt_Partnerek.Tipus = KodtarakDropDownListPartnerTipus.SelectedValue;
            krt_Partnerek.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListPartnerTipus);
        }
        if (!string.IsNullOrEmpty(OrszagokTextBox1.Id_HiddenField))
        {
            krt_Partnerek.Orszag_Id = OrszagokTextBox1.Id_HiddenField;
            krt_Partnerek.Updated.Orszag_Id = pageView.GetUpdatedByView(OrszagokTextBox1);
        }
        else
        {
            krt_Partnerek.Orszag_Id = null;
            krt_Partnerek.Updated.Orszag_Id = pageView.GetUpdatedByView(OrszagokTextBox1);
        }
        krt_Partnerek.Belso = ddownBelso.SelectedValue;
        krt_Partnerek.Updated.Belso = pageView.GetUpdatedByView(ddownBelso);

        //krt_Partnerek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_Partnerek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_Partnerek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_Partnerek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Partnerek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyitási verzió megadása ellenõrzés miatt
        krt_Partnerek.Base.Ver = FormHeader1.Record_Ver;
        krt_Partnerek.Base.Updated.Ver = true;

        //LZS - BLG_1915
        krt_Partnerek.Allapot = KodtarakDropDownListPartnerAllapot.SelectedValue;
        krt_Partnerek.Updated.Allapot = pageView.GetUpdatedByView(KodtarakDropDownListPartnerAllapot);

        switch (krt_Partnerek.Tipus)
        {
            case KodTarak.Partner_Tipus.Szemely:
                {
                    KRT_Szemelyek krt_szemelyek = GetSzemelyekFromComponents();
                    KRT_PartnerSzemelyek krt_partnerSzemelyek = new KRT_PartnerSzemelyek(krt_Partnerek, krt_szemelyek);
                    return (KRT_Partnerek)krt_partnerSzemelyek;
                }
            case KodTarak.Partner_Tipus.Szervezet:
                {
                    KRT_Vallalkozasok krt_vallalkozasok = GetVallalkozasokFromComponents();
                    KRT_PartnerVallalkozasok krt_partnerVallalkozasok = new KRT_PartnerVallalkozasok(krt_Partnerek, krt_vallalkozasok);
                    return (KRT_Partnerek)krt_partnerVallalkozasok;
                }
            case KodTarak.Partner_Tipus.Alkalmazas:
                break;
            default:
                break;
        }

        return krt_Partnerek;
    }

    private KRT_Szemelyek GetSzemelyekFromComponents()
    {
        KRT_Szemelyek krt_Szemelyek = new KRT_Szemelyek();
        krt_Szemelyek.Updated.SetValueAll(false);

        krt_Szemelyek.UjTitulis = textTitulus.Text;
        krt_Szemelyek.Updated.UjTitulis = pageView.GetUpdatedByView(textTitulus);

        krt_Szemelyek.UjCsaladiNev = RequiredTexBoxCsaladiNev.Text;
        krt_Szemelyek.Updated.UjCsaladiNev = pageView.GetUpdatedByView(RequiredTexBoxCsaladiNev);
        krt_Szemelyek.UjUtonev = RequiredTextBoxUtoNev.Text;
        krt_Szemelyek.Updated.UjUtonev = pageView.GetUpdatedByView(RequiredTextBoxUtoNev);
        krt_Szemelyek.UjTovabbiUtonev = textTovabbiUtonev.Text;
        krt_Szemelyek.Updated.UjTovabbiUtonev = pageView.GetUpdatedByView(textTovabbiUtonev);

        krt_Szemelyek.AnyjaNeveCsaladiNev = textAnyjaCsaladiNeve.Text;
        krt_Szemelyek.Updated.AnyjaNeveCsaladiNev = pageView.GetUpdatedByView(textAnyjaCsaladiNeve);
        krt_Szemelyek.AnyjaNeveElsoUtonev = textAnyjaUtoNev.Text;
        krt_Szemelyek.Updated.AnyjaNeveElsoUtonev = pageView.GetUpdatedByView(textAnyjaUtoNev);
        krt_Szemelyek.AnyjaNeveTovabbiUtonev = textAnyjaTovabbiUtoNev.Text;
        krt_Szemelyek.Updated.AnyjaNeveTovabbiUtonev = pageView.GetUpdatedByView(textAnyjaTovabbiUtoNev);
        krt_Szemelyek.AnyjaNeve = string.Join(" ", new string[] { textAnyjaCsaladiNeve.Text, textAnyjaUtoNev.Text, textAnyjaTovabbiUtoNev.Text });
        krt_Szemelyek.Updated.AnyjaNeve = true;

        krt_Szemelyek.SzuletesiCsaladiNev = textSzuletesiCsaladiNev.Text;
        krt_Szemelyek.Updated.SzuletesiCsaladiNev = pageView.GetUpdatedByView(textSzuletesiCsaladiNev);
        krt_Szemelyek.SzuletesiElsoUtonev = textSzuletesiUtoNev.Text;
        krt_Szemelyek.Updated.SzuletesiElsoUtonev = pageView.GetUpdatedByView(textSzuletesiUtoNev);
        krt_Szemelyek.SzuletesiTovabbiUtonev = textSzuletesiTovabbiUtoNev.Text;
        krt_Szemelyek.Updated.SzuletesiTovabbiUtonev = pageView.GetUpdatedByView(textSzuletesiTovabbiUtoNev);
        krt_Szemelyek.SzuletesiNev = string.Join(" ", new string[] { textSzuletesiCsaladiNev.Text, textSzuletesiUtoNev.Text, textSzuletesiTovabbiUtoNev.Text });
        krt_Szemelyek.Updated.SzuletesiNev = true;

        krt_Szemelyek.SzuletesiOrszag = OrszagTextBoxSzuletesi.Text;
        krt_Szemelyek.Updated.SzuletesiOrszag = pageView.GetUpdatedByView(OrszagTextBoxSzuletesi);
        krt_Szemelyek.SzuletesiHely = TelepulesTextBoxSzuletesi.Text;
        krt_Szemelyek.Updated.SzuletesiHely = pageView.GetUpdatedByView(TelepulesTextBoxSzuletesi);
        krt_Szemelyek.SzuletesiIdo = CalendarSzuletesiIdo.Text;
        krt_Szemelyek.Updated.SzuletesiIdo = pageView.GetUpdatedByView(CalendarSzuletesiIdo);
        krt_Szemelyek.TAJSzam = textTajSzam.Text;
        krt_Szemelyek.Updated.TAJSzam = pageView.GetUpdatedByView(textTajSzam);
        krt_Szemelyek.SZIGSzam = textSzigSzam.Text;
        krt_Szemelyek.Updated.SZIGSzam = pageView.GetUpdatedByView(textSzigSzam);
        krt_Szemelyek.Neme = Sex.GetSelectedValue(rbListNeme);
        krt_Szemelyek.Updated.Neme = pageView.GetUpdatedByView(rbListNeme);
        krt_Szemelyek.SzemelyiAzonosito = textSzemelyiAzonosito.Text;
        krt_Szemelyek.Updated.SzemelyiAzonosito = pageView.GetUpdatedByView(textSzemelyiAzonosito);
        krt_Szemelyek.Adoazonosito = textAdoAzonosito.Text;
        krt_Szemelyek.Updated.Adoazonosito = pageView.GetUpdatedByView(textAdoAzonosito);
        krt_Szemelyek.Adoszam = textAdoszamSzemely.Text;
        krt_Szemelyek.Updated.Adoszam = pageView.GetUpdatedByView(textAdoszamSzemely);
        krt_Szemelyek.KulfoldiAdoszamJelolo = textAdoszamSzemely.KulfoldiAdoszamJelolo;
        krt_Szemelyek.Updated.KulfoldiAdoszamJelolo = pageView.GetUpdatedByView(textAdoszamSzemely) && textAdoszamSzemely.KulfoldiAdoszamCheckBoxVisible;
        // BLG_346
        krt_Szemelyek.Beosztas = textBeosztas.Text;
        krt_Szemelyek.Updated.Beosztas = pageView.GetUpdatedByView(textBeosztas);
        krt_Szemelyek.MinositesiSzint = KodtarakDropDownListMinositesiSzint.SelectedValue;
        krt_Szemelyek.Updated.MinositesiSzint = pageView.GetUpdatedByView(KodtarakDropDownListMinositesiSzint);

        return krt_Szemelyek;
    }

    private KRT_Vallalkozasok GetVallalkozasokFromComponents()
    {
        KRT_Vallalkozasok krt_Vallalkozasok = new KRT_Vallalkozasok();
        krt_Vallalkozasok.Updated.SetValueAll(false);

        krt_Vallalkozasok.Adoszam = textAdoSzamVallalkozas.Text;
        krt_Vallalkozasok.Updated.Adoszam = pageView.GetUpdatedByView(textAdoSzamVallalkozas);
        krt_Vallalkozasok.KulfoldiAdoszamJelolo = textAdoSzamVallalkozas.KulfoldiAdoszamJelolo;
        krt_Vallalkozasok.Updated.KulfoldiAdoszamJelolo = pageView.GetUpdatedByView(textAdoSzamVallalkozas) && textAdoSzamVallalkozas.KulfoldiAdoszamCheckBoxVisible;
        krt_Vallalkozasok.TB_Torzsszam = textTBSzam.Text;
        krt_Vallalkozasok.Updated.TB_Torzsszam = pageView.GetUpdatedByView(textTBSzam);
        krt_Vallalkozasok.Cegjegyzekszam = textCegJegyzekSzam.Text;
        krt_Vallalkozasok.Updated.Cegjegyzekszam = pageView.GetUpdatedByView(textCegJegyzekSzam);
        krt_Vallalkozasok.Tipus = KodtarakDropDownListCegTipus.SelectedValue;
        krt_Vallalkozasok.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListCegTipus);

        return krt_Vallalkozasok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) return;
        compSelector.Enabled = true;

        compSelector.Add_ComponentOnClick(RequiredTextBoxNev);
        compSelector.Add_ComponentOnClick(KodtarakDropDownListPartnerTipus);
        compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
        compSelector.Add_ComponentOnClick(OrszagokTextBox1);
        compSelector.Add_ComponentOnClick(ddownBelso);
        //személy
        compSelector.Add_ComponentOnClick(textTitulus);
        compSelector.Add_ComponentOnClick(RequiredTexBoxCsaladiNev);
        compSelector.Add_ComponentOnClick(RequiredTextBoxUtoNev);
        compSelector.Add_ComponentOnClick(textTovabbiUtonev);
        compSelector.Add_ComponentOnClick(textAnyjaCsaladiNeve);
        compSelector.Add_ComponentOnClick(textAnyjaUtoNev);
        compSelector.Add_ComponentOnClick(textAnyjaTovabbiUtoNev);
        compSelector.Add_ComponentOnClick(textSzuletesiCsaladiNev);
        compSelector.Add_ComponentOnClick(textSzuletesiUtoNev);
        compSelector.Add_ComponentOnClick(textSzuletesiTovabbiUtoNev);
        compSelector.Add_ComponentOnClick(OrszagTextBoxSzuletesi);
        compSelector.Add_ComponentOnClick(TelepulesTextBoxSzuletesi);
        compSelector.Add_ComponentOnClick(CalendarSzuletesiIdo);
        compSelector.Add_ComponentOnClick(textTajSzam);
        compSelector.Add_ComponentOnClick(textSzigSzam);
        compSelector.Add_ComponentOnClick(rbListNeme);
        compSelector.Add_ComponentOnClick(textSzemelyiAzonosito);
        compSelector.Add_ComponentOnClick(textAdoAzonosito);
        compSelector.Add_ComponentOnClick(textAdoszamSzemely);
        // BLG_346
        compSelector.Add_ComponentOnClick(textBeosztas);
        compSelector.Add_ComponentOnClick(KodtarakDropDownListMinositesiSzint);

        //vállalkozás
        compSelector.Add_ComponentOnClick(textAdoSzamVallalkozas);
        compSelector.Add_ComponentOnClick(textTBSzam);
        compSelector.Add_ComponentOnClick(textCegJegyzekSzam);
        compSelector.Add_ComponentOnClick(KodtarakDropDownListCegTipus);

        FormFooter1.SaveEnabled = false;
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Partner" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                            KRT_Partnerek krt_Partnerek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            // CR3321 teszt
                            Result result = service.Insert(execParam, krt_Partnerek);
                            //Result result = new Result();
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                bool tryFireChangeEvent = (Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1" ? true : false);

                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                // kivéve, ha explicit kérjük
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, getPartnerNev(), true, tryFireChangeEvent))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    // CR3321 Partner rögzítés csak keresés után
                                    string callbackFunction = Request.QueryString.Get(QueryStringVars.ParentWindowCallbackFunction);
                                    if (!String.IsNullOrEmpty(callbackFunction))
                                    {
                                        JavaScripts.RegisterParentWindowCallbackFunction(Page, callbackFunction);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                                KRT_Partnerek krt_Partnerek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Partnerek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    void PartnerTipusDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetActivePanel();
    }

    void LoadComponentsFromId(string id)
    {
        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = id;

        Result result = service.Get(execParam);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            KRT_Partnerek krt_Partnerek = (KRT_Partnerek)result.Record;
            LoadComponentsFromBusinessObject(krt_Partnerek);
        }
    }

    #region KISHERCEG
    private Type formTemplateType = typeof(KRT_Partnerek);
    private bool IsTemplateMode = false;
    private void SetFormTemplateVisibility()
    {
        Command = Request.QueryString.Get(CommandName.Command);
        if (Command == CommandName.New)
        {
            FormHeader1.TemplateObjectType = formTemplateType;
            FormHeader1.FormTemplateLoader1_Visibility = true;
        }
    }
    private void TemplateHandler(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            IsTemplateMode = true;
            LoadComponentsFromBusinessObject((KRT_Partnerek)FormHeader1.TemplateObject);
            IsTemplateMode = false;
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            IsTemplateMode = true;
            KRT_Partnerek partnerek = GetBusinessObjectFromComponents();

            KRT_Partnerek partnerekToSave = new KRT_Partnerek();
            partnerekToSave.Tipus = partnerek.Tipus;
            partnerekToSave.Belso = partnerek.Belso;

            partnerekToSave.Updated.Tipus = true;
            partnerekToSave.Updated.Belso = true;

            FormHeader1.NewTemplate(partnerekToSave);
            IsTemplateMode = false;
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            IsTemplateMode = true;
            KRT_Partnerek partnerek = GetBusinessObjectFromComponents();

            KRT_Partnerek partnerekToSave = new KRT_Partnerek();
            partnerekToSave.Id = partnerek.Id;
            partnerekToSave.Tipus = partnerek.Tipus;
            partnerekToSave.Belso = partnerek.Belso;
            partnerekToSave.Updated.Tipus = true;
            partnerekToSave.Updated.Belso = true;

            FormHeader1.SaveCurrentTemplate(partnerek);
            IsTemplateMode = false;
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        SetActivePanel();
    }
    #endregion
}
