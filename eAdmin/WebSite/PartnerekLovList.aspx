<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerekLovList.aspx.cs" Inherits="PartnerekLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc3" %> 
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc7" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,PartnerekLovListHeaderTitle%>" /> 
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <!--   // CR3321 Partner r�gz�t�s csak keres�s ut�n -->
                    <asp:TextBox ID="PartnerMegnevezes" runat="server" EnableViewState="true" OnTextChanged="PartnerMegnevezes_ValueChanged" style="display:none" ></asp:TextBox>
                    <asp:HiddenField ID="PartnerHiddenField" runat="server" />

                    <asp:TextBox ID="CimTextBox" runat="server" style="display:none" />
                    <asp:HiddenField ID="CimHiddenField" runat="server" />
                     <%--CR3321 end--%>
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                                    <asp:RadioButtonList ID="radioButtonList_Mode" runat="server"
                                        RepeatDirection="Horizontal" Width="200px" Visible="false" AutoPostBack="true">
                                        <asp:ListItem Value="P" Selected="True">Partnerek</asp:ListItem>
                                        <asp:ListItem Value="E">Eloszt��vek</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <BR />
                                    
                                    <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                    <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                    
                                    <TABLE ID="partnerekTable" style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0 runat="server">
                                    <TBODY>
                                    <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                     </tr>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                        <asp:Label id="Label1" runat="server" Font-Bold="true" Text="Partnern�v:"></asp:Label><BR />
                                        <asp:TextBox id="TextBoxSearch" runat="server" Width="50%">*</asp:TextBox>
                                        <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"></asp:ImageButton>
                                        <asp:ImageButton id="ButtonAdvancedSearch" runat="server" AlternateText="R�szletes keres�s" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton> 
                                        </TD>
                                    </TR>
                                    <TR>
                                    <TD style="TEXT-ALIGN: left">
                                    <ajaxToolkit:CollapsiblePanelExtender id="cpeCimSearch" runat="server" CollapsedText="" ExpandedText="" ExpandedSize="0" ImageControlID="btnCpeCimSearch" ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif" AutoExpand="false" AutoCollapse="false" ExpandDirection="Vertical" CollapseControlID="btnCpeCimSearch" ExpandControlID="btnCpeCimSearch" Collapsed="True" CollapsedSize="23" TargetControlID="panelCimSearch">
                                    </ajaxToolkit:CollapsiblePanelExtender> <asp:Panel style="PADDING-TOP: 3px" id="panelCimSearch" runat="server" Width="97.5%">
                                        <div style="display:inline;position:absolute;margin-left:-9px;">
                                            <img ID="btnCpeCimSearch" src="images/hu/Grid/minus.gif" runat="server" style="cursor:pointer;"/>
                                        </div>    
                                        <ajaxToolkit:TabContainer ID="TabContainerCimSearch" runat="server" Width="100%">
                                                        <!-- Postai c�mek -->
                                                        <ajaxToolkit:TabPanel ID="TabPanelPostai" runat="server" TabIndex="0">
                                                        <HeaderTemplate>
                                                        <asp:Label ID="labelPostaiHeader" runat="server" Text="Postai c�mek"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ContentTemplate>
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tbody>
                                                            <tr class="urlapNyitoSor">
                                                                <td class="mrUrlapCaption">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                <td class="mrUrlapMezo">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                <td class="mrUrlapSpacer">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                <td class="mrUrlapCaption">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                                <td class="mrUrlapMezo">
                                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                            </tr>
                                                            <tr class="urlapSor">
                                                                <td class="mrUrlapCaption">
                                                                    <asp:Label ID="labelTelepules" runat="server" Text="Telep�l�s:"></asp:Label></td>
                                                                <td class="mrUrlapMezo">
                                                                    <uc4:TelepulesTextBox ID="TelepulesTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort"/>                                        
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="mrUrlapCaption">
                                                                    <asp:Label ID="labelIranyitoszam" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo">
                                                                    <uc5:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort"/>
                                                                </td>
                                                            </tr>
                                                            <tr class="urlapSor">
                                                                <td class="mrUrlapCaption">
                                                                    <asp:Label ID="labelKozterulet" runat="server" Text="K�zter�let neve:"></asp:Label></td>
                                                                <td class="mrUrlapMezo">
                                                                    <uc6:KozteruletTextBox ID="KozteruletTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort"/>
                                                                </td> 
                                                                <td>
                                                                </td>
                                                                <td class="mrUrlapCaption">
                                                                    <asp:Label ID="labelKozteruletTipus" runat="server" Text="K�zter�let tipusa:"></asp:Label></td>
                                                                <td class="mrUrlapMezo">
                                                                    <uc7:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" Validate="false" CssClass="mrUrlapInputSearchShort"/>                                        
                                                                </td>

                                                            </tr>
                                                                <%--  // CR3321 Partner r�gz�t�s csak keres�s ut�n--%>
                                                                <%--            H�zsz�m sz�r�s--%>
                                                            <tr class="urlapSor">
                                                                <td class="mrUrlapCaption">
                                                                    <asp:Label ID="labelHazszam" runat="server" Text="H�zsz�m:"></asp:Label></td>
                                                                <td class="mrUrlapMezo" >
                                                                    <asp:TextBox ID="textHazszam" runat="server" CssClass="mrUrlapInput" Width="100px" Wrap="False"></asp:TextBox>
                                                                </td>
                                                                <td class="mrUrlapMezo" colspan="3"></td>
                                                             </tr>                               
                                                            </tbody>
                                                            </table>
                                                        </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <!-- Egy�b c�mek -->
                                                        <ajaxToolkit:TabPanel ID="TabPanelEgyeb" runat="server" TabIndex="1">
                                                        <HeaderTemplate>
                                                        <asp:Label ID="labelEgyebHeader" runat="server" Text="Egy�b c�mek"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ContentTemplate>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tbody>
                                                        <tr class="urlapNyitoSor">
                                                            <td class="mrUrlapCaption">
                                                                <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                            <td class="mrUrlapMezo">
                                                                <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                            <td class="mrUrlapSpacer">
                                                                <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                            <td class="mrUrlapCaption">
                                                                <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                            <td class="mrUrlapMezo">
                                                                <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                        </tr>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="labelTipus" runat="server" Text="T�pus:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc8:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />   
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td class="mrUrlapCaption">
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="labelEgyebCim" runat="server" Text="Tartalom:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="textEgyebCim" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                            </td>            
                                                            <td>
                                                            </td>
                                                            <td class="mrUrlapCaption">
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                            </td>
                                                        </tr>
                                                         </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                              </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </asp:Panel>
                                    </TD>
                                    </TR>
                                    <TR>
                                    <TD style="PADDING-BOTTOM: 5px; VERTICAL-ALIGN: top; PADDING-TOP: 5px; TEXT-ALIGN: left">
                                        <DIV class="listaFulFelsoCsikKicsi">
                                            <IMG alt="" src="images/hu/design/spacertrans.gif" />
                                        </DIV>
                                    </TD>
                                    </TR>
                                    <TR>
                                    <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                        <ajaxToolkit:CollapsiblePanelExtender id="LovListCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender> <asp:Panel id="Panel1" runat="server" Width="95%">
                                    <TABLE style="WIDTH: 102%" cellSpacing=0 cellPadding=0>
                                    <TBODY>
                                    <TR>
                                    <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left" id="headerBackGround" class="GridViewHeaderBackGroundStyle" runat="server">
                                        
                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                        <%-- <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" /> --%>
                                        
                                        <asp:GridView id="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle" PageSize="5000" 
                                        DataKeyNames="Id" AutoGenerateColumns="False" AllowSorting="False" 
                                        PagerSettings-Visible="false" AllowPaging="True" GridLines="Both" 
                                        BorderWidth="1" CellSpacing="0" CellPadding="0" 
                                        OnSelectedIndexChanging="GridViewSearchResult_SelectedIndexChanging" 
                                        OnPreRender="GridViewSearchResult_PreRender">
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                        <SelectedRowStyle CssClass="GridViewLovListSelectedRowStyle" />
                                                        <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                        <Columns>
                                                            <asp:BoundField DataField="Id" SortExpression="Id">
                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                            </asp:BoundField>                                                
                                                            <asp:BoundField DataField="Nev" HeaderText="Partner neve" SortExpression="Nev" >
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle Width="200px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="CimId" SortExpression="CimId">
                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Cim" HeaderText="Partner c&#237;me" SortExpression="Nev" >
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle Width="70%"/>
                                                            </asp:BoundField>  
                                                        </Columns>
                                                        <PagerSettings Visible="False" />
                                                    <EmptyDataTemplate>
                                                        <asp:Label runat="server" Text="<%$Resources:Search,NoSearchResult%>" ></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                            </asp:GridView> 
                                        </TD>
                                       </TR>
                                     </TBODY>
                                  </TABLE>
                              </asp:Panel> 
                           </TD>
                           </TR>
                           <TR>
                           <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                           <BR />
                                <asp:ImageButton id="ImageButtonReszletesAdatok" runat="server" 
                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" 
                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')">
                                </asp:ImageButton> 
                           </TD>
                           </TR>
                           </TBODY>
                           </TABLE>

                                    <TABLE ID="elosztoivekTable" style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0 runat="server">
                                    <TBODY>
                                    <tr ID="tr1" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="label2" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                     </tr>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                        <asp:Label id="Label3" runat="server" Font-Bold="true" Text="Eloszt��v neve:"></asp:Label><BR />
                                        <asp:TextBox id="TextBox1" runat="server" Width="50%"></asp:TextBox>
                                        <asp:ImageButton id="ImageButton1" onclick="ButtonElosztoivekSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"></asp:ImageButton>
                                        <asp:ImageButton id="ImageButton2" runat="server" AlternateText="R�szletes keres�s" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton> 
                                        </TD>
                                    </TR>
                                    <TR>
                                    <TD style="PADDING-BOTTOM: 5px; VERTICAL-ALIGN: top; PADDING-TOP: 5px; TEXT-ALIGN: left">
                                        <DIV class="listaFulFelsoCsikKicsi">
                                            <IMG alt="" src="images/hu/design/spacertrans.gif" />
                                        </DIV>
                                    </TD>
                                    </TR>
                                    <TR>
                                    <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                        <ajaxToolkit:CollapsiblePanelExtender id="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender> <asp:Panel id="Panel3" runat="server" Width="95%">
                                    <TABLE style="WIDTH: 102%" cellSpacing=0 cellPadding=0>
                                    <TBODY>
                                    <TR>
                                    <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left" id="TD1" class="GridViewHeaderBackGroundStyle" runat="server">
                                        
                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                        <asp:HiddenField ID="ElosztoivId_HiddenField" runat="server" />
                                        <asp:HiddenField ID="ElosztoivIndex_HiddenField" runat="server" />
                                        
                                        <asp:GridView id="gridViewElosztoivek" runat="server" CssClass="GridViewLovListStyle" PageSize="5000" 
                                        DataKeyNames="Id" AutoGenerateColumns="False" AllowSorting="False" 
                                        PagerSettings-Visible="false" AllowPaging="True" GridLines="Both" 
                                        BorderWidth="1" CellSpacing="0" CellPadding="0" 
                                        OnSelectedIndexChanging="GridViewElosztoivekSearchResult_SelectedIndexChanging" 
                                        OnPreRender="GridViewSearchResult_PreRender">
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                        <SelectedRowStyle CssClass="GridViewLovListSelectedRowStyle" />
                                                        <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                        <Columns>
                                                            <asp:BoundField DataField="Id" SortExpression="Id">
                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                            </asp:BoundField>                                                
                                                            <asp:BoundField DataField="NEV" HeaderText="Eloszt��v neve" SortExpression="NEV" >
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle Width="200px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Fajta_Nev" HeaderText="Eloszt��v t�pusa" SortExpression="Fajta_Nev" >
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle Width="200px" />
                                                            </asp:BoundField>
                                                       </Columns>
                                                        <PagerSettings Visible="False" />
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label12" runat="server" Text="<%$Resources:Search,NoSearchResult%>" ></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                            </asp:GridView> 
                                        </TD>
                                       </TR>
                                     </TBODY>
                                  </TABLE>
                              </asp:Panel> 
                           </TD>
                           </TR>
                           <TR>
                           <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                           <BR />
                                <asp:ImageButton id="ImageButton3" runat="server" 
                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" 
                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')">
                                </asp:ImageButton> 
                           </TD>
                           </TR>
                           </TBODY>
                           </TABLE>
                            
                                    
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
   
    <table>
        <tr>
            <td>
                 <uc2:LovListFooter ID="LovListFooter1" runat="server" />
            </td>
            <td >
                   <asp:ImageButton TabIndex = "-1" ID="NewImageButton" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/uj_letrehozasa.png" 
                onmouseover="swapByName(this.id,'uj_letrehozasa2.png')" 
                onmouseout="swapByName(this.id,'uj_letrehozasa.png')" 
                OnClientClick="window.close(); return false;" CommandName="New" 
                Visible="True" />         
            </td>
        </tr>
    </table>
   

</asp:Content>

