using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;
using System.Data;
using System.Text;

public partial class PartnerekLovList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterType = "All"; // lehets�ges �rt�kek: "All" (==""), "Szervezet", "Szemely"

    private const int tabIndexPostai = 0;
    private const int tabIndexEgyeb = 1;

    private const string kodcsoportCim = "CIM_TIPUS";

    private bool withElosztoivek;
    private bool elosztoivMode;

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(DataRow row)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (row["CimTipus"].ToString())
            {
                case KodTarak.Cim_Tipus.Postai:
                    string orszag = row["OrszagNev"].ToString();
                    cim.Add(orszag, delimeter);
                    string iranyitoszam = row["IRSZ"].ToString();
                    cim.Add(iranyitoszam, delimeter);
                    string telepules = row["TelepulesNev"].ToString();
                    cim.Add(telepules, delimeter);
                    // BLG_1347
                    //cim.Add(kozterulet, delimeterSpace);
                    string kozterulet = row["KozteruletNev"].ToString();
                    string hrsz = row["HRSZ"].ToString();
                    if (String.IsNullOrEmpty(kozterulet) && !String.IsNullOrEmpty(hrsz))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(hrsz, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(kozterulet, delimeterSpace);
                        string kozteruletTipus = row["KozteruletTipusNev"].ToString();
                        cim.Add(kozteruletTipus, delimeterSpace);
                        string hazszam = row["Hazszam"].ToString();
                        string hazszamIg = row["Hazszamig"].ToString();
                        string hazszamBetujel = row["HazszamBetujel"].ToString();
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = row["Lepcsohaz"].ToString();
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = row["Szint"].ToString();
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = row["Ajto"].ToString();
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = row["AjtoBetujel"].ToString();
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = row["CimTobbi"].ToString();
                    cim.Add(Cim, delimeter);
                    break;

                case "":
                    return String.Empty;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            throw e;
        }

        return text.ToString();
    }

    public bool Collapsed
    {
        get
        {
            if (cpeCimSearch.ClientState == null)
            {
                return cpeCimSearch.Collapsed;
            }
            else
            {
                return Boolean.Parse(cpeCimSearch.ClientState);
            }
        }
        set { cpeCimSearch.ClientState = value.ToString(); }
    }

    private GridViewRow prevouseRow = null;
    private bool alternateRow = false;
    private const string alternateRowStyle = "GridViewAlternateRowStyle";
    private const string rowStyle = "GridViewRowStyle";
    private const string selectedRowStyle = "GridViewLovListSelectedRowStyle";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerekList");
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        //bopmh
        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            NewImageButton.Visible = false;
        }

        try
        {
            radioButtonList_Mode.Visible = withElosztoivek = bool.Parse(Request.QueryString.Get(QueryStringVars.WithElosztoivek)) && FunctionRights.GetFunkcioJog(Page, "ElosztoivekList");
        }
        catch
        {
            radioButtonList_Mode.Visible = withElosztoivek = false;
        }

        registerJavascripts();
        if (withElosztoivek)
            elosztoivMode = radioButtonList_Mode.SelectedValue == "P" ? false : true;
        else
            elosztoivMode = false;

        elosztoivekTable.Visible = elosztoivMode;
        partnerekTable.Visible = !elosztoivMode;

        if (!elosztoivMode)
        {
            // van-e sz�r�s esetleg szervezetre, vagy szemelyre:
            filterType = Request.QueryString.Get(QueryStringVars.Filter);
            if (filterType == null) filterType = "";
            switch (filterType)
            {
                case "Szervezet":
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_Szervezet;
                    break;
                case "Szemely":
                    LovListHeader1.HeaderTitle = Resources.LovList.PartnerekLovListHeaderTitle_Filtered_Szemely;
                    break;
                case Constants.FilterType.Partnerek.BelsoSzemely:
                    goto case "Szemely";
            }

            LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

            LovListFooter1.ButtonsClick += new System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            //string query = "&" + QueryStringVars.HiddenFieldId + "=" + Request.QueryString.Get(QueryStringVars.HiddenFieldId)
            //          + "&" + QueryStringVars.TextBoxId + "=" + Request.QueryString.Get(QueryStringVars.TextBoxId);
            string query = "&" + QueryStringVars.HiddenFieldId + "=" + PartnerHiddenField.ClientID
                      + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID;


            if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.TryFireChangeEvent)))
            {
                query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
            }

            //if ((!String.IsNullOrEmpty(Request.QueryString.Get("CimTextBoxClientId")) && (!String.IsNullOrEmpty(Request.QueryString.Get("CimHiddenFieldClientId")))))
            //{
            //    query += "&CimTextBoxId=" + Request.QueryString.Get("CimTextBoxClientId") + "&CimHiddenFieldId=" + Request.QueryString.Get("CimHiddenFieldClientId");
            //}
            if ((CimTextBox != null) && (CimHiddenField != null))
            {
                query += "&CimTextBoxId=" + CimTextBox.ClientID + "&CimHiddenFieldId=" + CimHiddenField.ClientID;
            }

            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            string jsdoPostback = "function NewPartnerCallback(){__doPostBack('" + PartnerMegnevezes.ClientID + @"','');}";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NewPartnerCallback", jsdoPostback, true);

            //NewImageButton.OnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
            //            , CommandName.Command + "=" + CommandName.New
            //            + "&HiddenFieldId=" + cimid.ClientID + "&TextBoxId=" + cimtext.ClientID + "&" + QueryStringVars.ParentWindowCallbackFunction + "=NewPartnerCallback"
            //            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
            NewImageButton.OnClientClick = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query + "&" + QueryStringVars.ParentWindowCallbackFunction + "=NewPartnerCallback"
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

            ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", GridViewSelectedId.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("PartnerekSearch.aspx", ""
                , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

            if (!IsPostBack)
            {
                KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportCim, LovListHeader1.ErrorPanel);
                ListItem itemPostai = KodtarakDropDownListTipus.DropDownList.Items.FindByValue(KodTarak.Cim_Tipus.Postai);
                KodtarakDropDownListTipus.DropDownList.Items.Remove(itemPostai);
                //FillGridViewSearchResult(TextBoxSearch.Text, false);

                //FillGridViewElosztoivekSearchResult(TextBox1.Text, false);
            }
        }
        else
        {
            ImageButton3.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("ElosztoivekForm.aspx", "", GridViewSelectedIndex.ClientID);

            ImageButton2.OnClientClick = JavaScripts.SetOnClientClick("ElosztoivSearch.aspx", ""
                , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

            LovListFooter1.ButtonsClick += new
                System.Web.UI.WebControls.CommandEventHandler(LovListFooterElosztoiv_ButtonsClick);
        }

        LovListHeader1.ErrorPanel.Visible = false;
        LovListHeader1.ErrorUpdatePanel.Update();

        ScriptManager1.SetFocus(ButtonSearch);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!elosztoivMode)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            JavaScripts.ResetScroll(Page, LovListCPE);
                            FillGridViewSearchResult("", true);
                        }
                        break;
                }
            }

            // scrollozhat�s�g �ll�t�sa
            UI.SetLovListGridViewScrollable(GridViewSearchResult, LovListCPE, 12);
            if (!LovListCPE.ScrollContents)
            {
                Panel1.Width = Unit.Percentage(96.6);
            }
            else
            {
                Panel1.Width = Unit.Percentage(95);
            }
        }
        else
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
                            FillGridViewElosztoivekSearchResult("", true);
                        }
                        break;
                }
            }

            // scrollozhat�s�g �ll�t�sa
            UI.SetLovListGridViewScrollable(gridViewElosztoivek, CollapsiblePanelExtender2, 12);
            if (!CollapsiblePanelExtender2.ScrollContents)
            {
                Panel3.Width = Unit.Percentage(96.6);
            }
            else
            {
                Panel3.Width = Unit.Percentage(95);
            }
        }
    }


    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, LovListCPE);
        FillGridViewSearchResult(TextBoxSearch.Text,false);
    }

    private KRT_CimekSearch GetCimekFromComponents()
    {
        KRT_CimekSearch search = new KRT_CimekSearch();

        if (!Collapsed)
        {
            switch (TabContainerCimSearch.ActiveTabIndex)
            {
                case tabIndexPostai:
                    {
                        search.TelepulesNev.Value = TelepulesTextBox1.Text;
                        search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBox1.Text);
                        search.IRSZ.Value = IranyitoszamTextBox1.Text;
                        search.IRSZ.Operator = Search.GetOperatorByLikeCharater(IranyitoszamTextBox1.Text);
                        search.KozteruletNev.Value = KozteruletTextBox1.Text;
                        search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTextBox1.Text);
                        search.KozteruletTipusNev.Value = KozteruletTipusTextBox1.Text;
                        search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTipusTextBox1.Text);
                        // CR3321 Partner r�gz�t�s csak keres�s ut�n
                        //        H�zsz�m sz�r�s
                        search.Hazszam.Value = textHazszam.Text;

                        if (!String.IsNullOrEmpty(textHazszam.Text))
                        {
                            search.Hazszam.Operator = Search.GetOperatorByLikeCharater(textHazszam.Text);
                        }
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.equals;
                    }
                    break;
                case tabIndexEgyeb:
                    if (!String.IsNullOrEmpty(textEgyebCim.Text))
                    {
                        search.CimTobbi.Value = textEgyebCim.Text;
                        search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(textEgyebCim.Text);
                    }
                    if (String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                    {
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.notequals;
                    }
                    else
                    {
                        search.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                        search.Tipus.Operator = Query.Operators.equals;
                    }
                    break;
            }
        }

        return search;
    }

    protected void FillGridViewSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        KRT_PartnerekSearch searchPartner = null;

        if (searchObjectFromSession == true)
        {
            searchPartner = (KRT_PartnerekSearch)Search.GetSearchObject(Page, new KRT_PartnerekSearch());
            SetDefaultState();
        }
        else
        {
            searchPartner = new KRT_PartnerekSearch();
            searchPartner.Nev.Value = SearchKey;
            searchPartner.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }

        searchPartner.OrderBy = "KRT_Partnerek.Nev";

        // ha kell, sz�r�nk a t�pusra, ak�r fel�lv�gva a r�szletes keres�sn�l megadott t�pust is
        switch (filterType)
        {
            case "Szervezet":
                searchPartner.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                searchPartner.Tipus.Operator = Query.Operators.equals;
                break;
            case "Szemely":
                searchPartner.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
                searchPartner.Tipus.Operator = Query.Operators.equals;
                break;
            case Constants.FilterType.Partnerek.BelsoSzemely:
                searchPartner.Belso.Value = Constants.Database.Yes;
                searchPartner.Belso.Operator = Query.Operators.equals;
                goto case "Szemely";
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        searchPartner.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result;
        if (searchObjectFromSession)
        {
            result = service.GetAllWithExtnesions(execParam, searchPartner);
        }
        else
        {
            KRT_CimekSearch searchCim = GetCimekFromComponents();
            searchCim.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);
            result = service.GetAllWithCim(execParam, searchPartner, searchCim);
        }

        GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, searchPartner.TopRow);
    }

    //r�szletes keres�s ut�n az lovlist keres�si mez�inek alapl�llapotba �ll�t�sa
    private void SetDefaultState()
    {
        TextBoxSearch.Text = "";
        Collapsed = true;
        KodtarakDropDownListTipus.SetSelectedValue("");
        textEgyebCim.Text = "";
        TelepulesTextBox1.Text = "";
        IranyitoszamTextBox1.Text = "";
        KozteruletTextBox1.Text = "";
        KozteruletTipusTextBox1.Text = "";
        // CR3321
        textHazszam.Text = "";
    }

    private void GridViewFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }

        DataSet res = new DataSet();
        DataTable table = new DataTable();
        table.Columns.Add("Id",typeof(Guid));
        table.Columns.Add("Nev");
        table.Columns.Add("CimId");
        table.Columns.Add("Cim");
        res.Tables.Add(table);

        if (result.Ds != null)
        {
            try
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string nev = row["Nev"].ToString();
                        string cim = GetAppendedCim(row);
                        DataRow newRow = res.Tables[0].NewRow();
                        newRow["Id"] = row["Id"];
                        newRow["Nev"] = nev;
                        // BLG_1347
                        //newRow["Cim"] = row["CimNev"];
                        newRow["Cim"] = cim;
                        newRow["CimId"] = row["CimId"];
                        res.Tables[0].Rows.Add(newRow);
                    }
                }
                else
                {
                    HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
                    Panel1.Width = Unit.Percentage(96.6);
                    gridView.BorderWidth = Unit.Pixel(0);
                }
            }
            catch (Exception e)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), e.Message);
            }
        }
        else
        {
            HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
            Panel1.Width = Unit.Percentage(96.6);
            gridView.BorderWidth = Unit.Pixel(0);
        }

        gridView.DataSource = res;
        gridView.DataBind();
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    string cimTextBoxId = Request.QueryString.Get("CimTextBoxId");
                    string cimHiddenFieldId = Request.QueryString.Get("CimHiddenFieldId");

                    string cimText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string cimId = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);

                    bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);

                    //if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId) && !String.IsNullOrEmpty(cimText))
                    if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId))
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        // Sorrend fontos!!!
                        // el�bb hiddenfield, azt�n textbox (mert m�sk�pp a change lefut�sakor m�g nincs �t�rva a hiddenfield id,
                        // �s az esetleges kl�noz�sn�l helytelen (a m�dos�t�s el�tti) �rt�k ker�l be
                        parameters.Add(cimHiddenFieldId, cimId);
                        parameters.Add(cimTextBoxId, cimText);
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true, tryFireChangeEvent, true);
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        foreach (GridViewRow row in gridViewElosztoivek.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedId, GridViewSelectedIndex);
            }
        }

        base.Render(writer);

    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        if (!withElosztoivek)
        {
            //autocomplete-ek f�gg�s�geinek be�ll�t�sa, postback ut�n contextkey �jra be�ll�t�sa
            JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

            //tabf�lre kattintva is ny�ljon a panel, de ne csuk�djon
            string js = @"var postai = $get('" + labelPostaiHeader.ClientID + @"');
                    var egyeb = $get('" + labelEgyebHeader.ClientID + @"');
                    $addHandler(postai,'click',openPanel);
                    $addHandler(egyeb,'click',openPanel);
                    function openPanel(sender,e) {var a = $find('" + cpeCimSearch.ClientID + @"');
                    if(a.get_Collapsed()){a.expandPanel();};}";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenPanelByTab", js, true);
        }
    }

    protected void GridViewSearchResult_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewSearchResult.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedRowStyle;
    }

    //sorok sz�nez�se
    protected void GridViewSearchResult_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (prevouseRow != null)
                {

                    if (prevouseRow.Cells[0].Text == row.Cells[0].Text)
                    {
                        if (String.IsNullOrEmpty(HttpUtility.HtmlDecode(prevouseRow.Cells[2].Text).Trim()))
                        {
                            prevouseRow.Visible = false;

                        }
                    }
                    else
                    {
                        alternateRow = !alternateRow;
                    }
                }

                if (row.RowState == DataControlRowState.Normal || row.RowState == DataControlRowState.Alternate)
                {
                    if (alternateRow)
                    {
                        row.RowState = DataControlRowState.Alternate;
                        row.ControlStyle.CssClass = alternateRowStyle;
                    }
                    else
                    {
                        row.RowState = DataControlRowState.Normal;
                        row.ControlStyle.CssClass = rowStyle;
                    }
                }

                prevouseRow = row;
            }
        }
    }


    #region Eloszt��vek

    protected void ButtonElosztoivekSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
        FillGridViewElosztoivekSearchResult(TextBox1.Text, false);
    }

    protected void FillGridViewElosztoivekSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedIndex.Value = String.Empty;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        EREC_IraElosztoivekSearch searchElosztoiv = null;

        if (searchObjectFromSession == true)
        {
            searchElosztoiv = (EREC_IraElosztoivekSearch)Search.GetSearchObject(Page, new EREC_IraElosztoivekSearch());
            SetDefaultElosztoivState();
        }
        else
        {
            searchElosztoiv = new EREC_IraElosztoivekSearch();
            searchElosztoiv.NEV.Value = SearchKey;
            searchElosztoiv.NEV.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }

        searchElosztoiv.OrderBy = "NEV";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        searchElosztoiv.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, searchElosztoiv);

        GridViewElosztoivekFill(gridViewElosztoivek, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        //trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, searchElosztoiv.TopRow);
    }

    //r�szletes keres�s ut�n az lovlist keres�si mez�inek alapl�llapotba �ll�t�sa
    private void SetDefaultElosztoivState()
    {
        TextBox1.Text = "";
        //Collapsed = true;
        //KodtarakDropDownListTipus.SetSelectedValue("");
        //textEgyebCim.Text = "";
        //TelepulesTextBox1.Text = "";
        //IranyitoszamTextBox1.Text = "";
        //KozteruletTextBox1.Text = "";
        //KozteruletTipusTextBox1.Text = "";
    }

    private void GridViewElosztoivekFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }

        DataSet res = new DataSet();
        DataTable table = new DataTable();
        table.Columns.Add("Id", typeof(Guid));
        table.Columns.Add("Nev");
        table.Columns.Add("Fajta_Nev");
        res.Tables.Add(table);

        if (result.Ds != null)
        {
            try
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string nev = row["NEV"].ToString();
                        string Fajta_Nev = row["Fajta_Nev"].ToString();
                        DataRow newRow = res.Tables[0].NewRow();
                        newRow["Id"] = row["Id"];
                        newRow["Nev"] = nev;
                        newRow["Fajta_Nev"] = Fajta_Nev;
                        res.Tables[0].Rows.Add(newRow);
                    }
                }
                else
                {
                    HtmlTableCell td = (HtmlTableCell)Panel3.FindControl("headerBackGround");
                    Panel3.Width = Unit.Percentage(96.6);
                    gridView.BorderWidth = Unit.Pixel(0);
                }
            }
            catch (Exception e)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), e.Message);
            }
        }
        else
        {
            HtmlTableCell td = (HtmlTableCell)Panel3.FindControl("headerBackGround");
            Panel3.Width = Unit.Percentage(96.6);
            gridView.BorderWidth = Unit.Pixel(0);
        }

        gridView.DataSource = res;
        gridView.DataBind();
    }

    protected void GridViewElosztoivekSearchResult_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        gridViewElosztoivek.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedRowStyle;
    }

    protected void LovListFooterElosztoiv_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedId.Value) && !String.IsNullOrEmpty(GridViewSelectedIndex.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedId.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    gridViewElosztoivek.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(gridViewElosztoivek);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(gridViewElosztoivek, 1);

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true);

                    // c�m mez� t�rl�se a h�v� formon
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(Request.QueryString.Get("CimTextBoxId"), string.Empty);
                    parameters.Add(Request.QueryString.Get("CimHiddenFieldId"), string.Empty);
                    JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true);


                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, false);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedIndex.Value = String.Empty;
                GridViewSelectedId.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    #endregion

    // CR3321 Partner r�gz�t�s csak keres�s ut�n
    protected void PartnerMegnevezes_ValueChanged(object sender, EventArgs e)
    {
        string selectedId = PartnerHiddenField.Value;
        string selectedText = PartnerMegnevezes.Text;

        //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
        string cimTextBoxId = Request.QueryString.Get("CimTextBoxId");
        string cimHiddenFieldId = Request.QueryString.Get("CimHiddenFieldId");


        string cimText = CimTextBox.Text;
        string cimId = CimHiddenField.Value;

        bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

        JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);

       
        if (!String.IsNullOrEmpty(cimTextBoxId) && !String.IsNullOrEmpty(cimHiddenFieldId))
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            // Sorrend fontos!!!
            // el�bb hiddenfield, azt�n textbox (mert m�sk�pp a change lefut�sakor m�g nincs �t�rva a hiddenfield id,
            // �s az esetleges kl�noz�sn�l helytelen (a m�dos�t�s el�tti) �rt�k ker�l be
            parameters.Add(cimHiddenFieldId, cimId);
            parameters.Add(cimTextBoxId, cimText);
           
            JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "CimReturnValues", true, tryFireChangeEvent, true);
        }

        JavaScripts.RegisterCloseWindowClientScript(Page, false);
        disable_refreshLovList = true;
        
    }
}
