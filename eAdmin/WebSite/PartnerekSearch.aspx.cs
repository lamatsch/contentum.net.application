using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class PartnerekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_PartnerekSearch);

    private const string kcs_PARTNER_TIPUS = "PARTNER_TIPUS";
    private const string kcs_CEGTIPUS = "CEGTIPUS";
    private const string kcs_PARTNER_FORRAS = "PARTNER_FORRAS";
    // BLG_346
    private const string kcs_IRAT_MINOSITES = "IRAT_MINOSITES";

    //LZS - BLG_1915
    private const string kcs_PARTNER_ALLAPOT = "PARTNER_ALLAPOT";

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");
        public static readonly ListItem NotSet = new ListItem(Resources.Form.EmptyListItem, "X");
        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(NotSet);
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, NotSet.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value || value == NotSet.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = NotSet.Value;
            }
        }
        public static void SetSearchField(Field field, string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                field.Value = value;
                field.Operator = Query.Operators.equals;
            }
            else
            {
                field.Value = "";
                field.Operator = "";
            }
        }
    }

    public static class Sex
    {
        public static readonly ListItem Male = new ListItem("F�rfi", "F");
        public static readonly ListItem Female = new ListItem("N�", "N");
        public static readonly ListItem NotSet = new ListItem("Nincs megadva", "");
        public static void FillRadioList(RadioButtonList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Male);
            list.Items.Add(Female);
            list.Items.Add(NotSet);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillRadioList(RadioButtonList list)
        {
            FillRadioList(list, NotSet.Value);
        }
        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            if (selectedValue == Male.Value || selectedValue == Female.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = NotSet.Value;
            }
        }
        public static string GetSelectedValue(RadioButtonList list)
        {
            if (list.SelectedValue == Male.Value || list.SelectedValue == Female.Value)
            {
                return list.SelectedValue;
            }
            else
            {
                return NotSet.Value;
            }
        }

        public static void SetSearchField(Field field, string value)
        {
            if (value == Male.Value || value == Female.Value)
            {
                field.Value = value;
                field.Operator = Query.Operators.equals;
            }
            else
            {
                field.Value = "";
                field.Operator = "";
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

    }

    private void SetActivePanel()
    {
        switch (KodtarakDropDownListPartnerTipus.SelectedValue)
        {
            case KodTarak.Partner_Tipus.Szemely:
                panelSzemelyek.Visible = true;
                panelVallalkozasok.Visible = false;
                trPartnerNev.Visible = false;
                break;
            case KodTarak.Partner_Tipus.Szervezet:
                panelSzemelyek.Visible = false;
                panelVallalkozasok.Visible = true;
                trPartnerNev.Visible = true;
                break;
            case KodTarak.Partner_Tipus.Alkalmazas:
                panelSzemelyek.Visible = false;
                panelVallalkozasok.Visible = false;
                trPartnerNev.Visible = true;
                break;
            default:
                goto case KodTarak.Partner_Tipus.Alkalmazas;
        }
    }

    private void FillComponents()
    {
        KodtarakDropDownListPartnerTipus.FillAndSetEmptyValue(kcs_PARTNER_TIPUS, SearchHeader1.ErrorPanel);
        BoolString.FillDropDownList(ddownBelso);
        KodtarakDropDownListCegTipus.FillDropDownList(kcs_CEGTIPUS, true, SearchHeader1.ErrorPanel);
        Sex.FillRadioList(rbListNeme);
        KodtarakDropDownListPartnerForras.FillAndSetEmptyValue(kcs_PARTNER_FORRAS, SearchHeader1.ErrorPanel);
        // BLG_346
        KodtarakDropDownListMinositesiSzint.FillDropDownList(kcs_IRAT_MINOSITES, true, SearchHeader1.ErrorPanel);

        //LZS - BLG_1915
        KodtarakDropDownListPartnerAllapot.FillDropDownList(kcs_PARTNER_ALLAPOT, true, SearchHeader1.ErrorPanel);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        SearchHeader1.ButtonsClick +=
             new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
             new CommandEventHandler(SearchFooter1_ButtonsClick);

        KodtarakDropDownListPartnerTipus.DropDownList.AutoPostBack = true;
        KodtarakDropDownListPartnerTipus.DropDownList.SelectedIndexChanged += new EventHandler(PartnerTipusDropDownList_SelectedIndexChanged);


        if (!IsPostBack)
        {
            KRT_PartnerekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_PartnerekSearch)Search.GetSearchObject(Page, new KRT_PartnerekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            FillComponents();

            LoadComponentsFromSearchObject(searchObject);
        }

    }


    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    /// 

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        LoadComponentsFromSearchObject(searchObject, false);
    }

    private void LoadComponentsFromSearchObject(object searchObject, bool resetState)
    {
        KRT_PartnerekSearch _KRT_PartnerekSearch = (KRT_PartnerekSearch)searchObject;

        if (_KRT_PartnerekSearch != null)
        {
            KodtarakDropDownListPartnerTipus.SetSelectedValue(_KRT_PartnerekSearch.Tipus.Value);
            BoolString.SetSelectedValue(ddownBelso, _KRT_PartnerekSearch.Belso.Value);
            textNev.Text = _KRT_PartnerekSearch.Nev.Value;
            OrszagokTextBox1.Id_HiddenField = _KRT_PartnerekSearch.Orszag_Id.Value;
            OrszagokTextBox1.SetTextBoxById(SearchHeader1.ErrorPanel);
            KodtarakDropDownListPartnerForras.SetSelectedValue(_KRT_PartnerekSearch.Forras.Value);
            KulsoAzonositoTextBox.Text = _KRT_PartnerekSearch.KulsoAzonositok.Value;
            Ervenyesseg_SearchFormComponent1.SetDefault(_KRT_PartnerekSearch.ErvKezd, _KRT_PartnerekSearch.ErvVege);
            UtolsoHasznalatInterval.SetComponentFromSearchObjectFields(_KRT_PartnerekSearch.UtolsoHasznalat); //xx

            if (!resetState)
            {
                if (_KRT_PartnerekSearch.Tipus.Operator == Query.Operators.equals)
                {
                    switch (_KRT_PartnerekSearch.Tipus.Value)
                    {
                        case KodTarak.Partner_Tipus.Szemely:
                            {
                                LoadComponentsFromSzemelyek(_KRT_PartnerekSearch.SzemelyekSearch);
                                break;
                            }
                        case KodTarak.Partner_Tipus.Szervezet:
                            {
                                LoadComponentsFromVallalkozasok(_KRT_PartnerekSearch.VallalkozasokSearch);
                                break;
                            }
                        case KodTarak.Partner_Tipus.Alkalmazas:
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                LoadComponentsFromSzemelyek(_KRT_PartnerekSearch.SzemelyekSearch);
                LoadComponentsFromVallalkozasok(_KRT_PartnerekSearch.VallalkozasokSearch);
            }
        }

        SetActivePanel();
    }

    private void LoadComponentsFromSzemelyek(KRT_SzemelyekSearch krt_SzemelyekSearch)
    {
        textTitulus.Text = krt_SzemelyekSearch.UjTitulis.Value;
        textCsaladiNev.Text = krt_SzemelyekSearch.UjCsaladiNev.Value;
        textUtoNev.Text = krt_SzemelyekSearch.UjUtonev.Value;
        textAnyjaNeve.Text = krt_SzemelyekSearch.AnyjaNeve.Value;
        textApjaNeve.Text = krt_SzemelyekSearch.ApjaNeve.Value;
        textSzuletesiNev.Text = krt_SzemelyekSearch.SzuletesiNev.Value;
        OrszagTextBoxSzuletesi.Text = krt_SzemelyekSearch.SzuletesiOrszag.Value;
        TelepulesTextBoxSzuletesi.Text = krt_SzemelyekSearch.SzuletesiHely.Value;
        CalendarSzuletesiIdo.Text = krt_SzemelyekSearch.SzuletesiIdo.Value;
        textTajSzam.Text = krt_SzemelyekSearch.TAJSzam.Value;
        textSzigSzam.Text = krt_SzemelyekSearch.SZIGSzam.Value;
        Sex.FillRadioList(rbListNeme, krt_SzemelyekSearch.Neme.Value);
        textSzemelyiAzonosito.Text = krt_SzemelyekSearch.SzemelyiAzonosito.Value;
        textAdoAzonosito.Text = krt_SzemelyekSearch.Adoazonosito.Value;

        if (!String.IsNullOrEmpty(krt_SzemelyekSearch.KulfoldiAdoszamJelolo.Value))
        {
            rblKulfoldiAdoszamSzemely.SelectedValue = krt_SzemelyekSearch.KulfoldiAdoszamJelolo.Value;
        }
        textAdoszamSzemely.Text = krt_SzemelyekSearch.Adoszam.Value;
        // BLG_346
        textBeosztas.Text = krt_SzemelyekSearch.Beosztas.Value;
        KodtarakDropDownListMinositesiSzint.FillAndSetSelectedValue(kcs_IRAT_MINOSITES, krt_SzemelyekSearch.MinositesiSzint.Value, true, SearchHeader1.ErrorPanel);


    }

    private void LoadComponentsFromVallalkozasok(KRT_VallalkozasokSearch krt_VallalkozasokSearch)
    {
        if (!String.IsNullOrEmpty(krt_VallalkozasokSearch.KulfoldiAdoszamJelolo.Value))
        {
            rblKulfoldiAdoszamVallalkozas.SelectedValue = krt_VallalkozasokSearch.KulfoldiAdoszamJelolo.Value;
        }
        textAdoSzamVallalkozas.Text = krt_VallalkozasokSearch.Adoszam.Value;

        textTBSzam.Text = krt_VallalkozasokSearch.TB_Torzsszam.Value;
        textCegJegyzekSzam.Text = krt_VallalkozasokSearch.Cegjegyzekszam.Value;
        KodtarakDropDownListCegTipus.FillAndSetSelectedValue(kcs_CEGTIPUS, krt_VallalkozasokSearch.Tipus.Value, true, SearchHeader1.ErrorPanel);
    }


    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_PartnerekSearch SetSearchObjectFromComponents()
    {
        KRT_PartnerekSearch _KRT_PartnerekSearch = (KRT_PartnerekSearch)SearchHeader1.TemplateObject;
        if (_KRT_PartnerekSearch == null)
        {
            _KRT_PartnerekSearch = new KRT_PartnerekSearch();
        }

        //LZS - BLG_1915
        if (!String.IsNullOrEmpty(KodtarakDropDownListPartnerAllapot.SelectedValue))
        {
            _KRT_PartnerekSearch.Allapot.Value = KodtarakDropDownListPartnerAllapot.SelectedValue;
            _KRT_PartnerekSearch.Allapot.Operator = Query.Operators.equals;
        }

        _KRT_PartnerekSearch.Nev.Value = textNev.Text;
        _KRT_PartnerekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        if (!String.IsNullOrEmpty(OrszagokTextBox1.Id_HiddenField))
        {
            _KRT_PartnerekSearch.Orszag_Id.Value = OrszagokTextBox1.Id_HiddenField;
            _KRT_PartnerekSearch.Orszag_Id.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(KodtarakDropDownListPartnerTipus.SelectedValue))
        {
            _KRT_PartnerekSearch.Tipus.Value = KodtarakDropDownListPartnerTipus.SelectedValue;
            _KRT_PartnerekSearch.Tipus.Operator = Query.Operators.equals;
        }
        else
        {
            _KRT_PartnerekSearch.Tipus.Value = "";
            _KRT_PartnerekSearch.Tipus.Operator = "";
        }
        BoolString.SetSearchField(_KRT_PartnerekSearch.Belso, ddownBelso.SelectedValue);

        if (!String.IsNullOrEmpty(KodtarakDropDownListPartnerForras.SelectedValue))
        {
            _KRT_PartnerekSearch.Forras.Value = KodtarakDropDownListPartnerForras.SelectedValue;
            _KRT_PartnerekSearch.Forras.Operator = Query.Operators.equals;
        }

        _KRT_PartnerekSearch.KulsoAzonositok.Value = KulsoAzonositoTextBox.Text;
        _KRT_PartnerekSearch.KulsoAzonositok.Operator = Search.GetOperatorByLikeCharater(KulsoAzonositoTextBox.Text);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(_KRT_PartnerekSearch.ErvKezd, _KRT_PartnerekSearch.ErvVege);
        Letrehozas_SearchFormComponent.SetSearchObjectFields(_KRT_PartnerekSearch.LetrehozasKezd, _KRT_PartnerekSearch.LetrehozasVege);
       //  _KRT_PartnerekSearch.UtolsoHasznalat.Value = UtolsoHasznalatInterval.DatumKezd;
        // _KRT_PartnerekSearch.UtolsoHasznalat.ValueTo = UtolsoHasznalatInterval.DatumVege;
        // _KRT_PartnerekSearch.UtolsoHasznalat.Operator = Query.Operators.between;
        UtolsoHasznalatInterval.SetSearchObjectFields(_KRT_PartnerekSearch.UtolsoHasznalat);
        switch (KodtarakDropDownListPartnerTipus.SelectedValue)
        {
            case KodTarak.Partner_Tipus.Szemely:
                {
                    _KRT_PartnerekSearch.Nev.Value = "";
                    _KRT_PartnerekSearch.Nev.Operator = "";
                    GetSzemelyekFromComponents(_KRT_PartnerekSearch.SzemelyekSearch);
                    break;
                }
            case KodTarak.Partner_Tipus.Szervezet:
                {
                    GetVallalkozasokFromComponents(_KRT_PartnerekSearch.VallalkozasokSearch);
                    break;
                }
            case KodTarak.Partner_Tipus.Alkalmazas:
                {
                    break;
                }
            default:
                break;
        }
        return _KRT_PartnerekSearch;
    }

    private void GetSzemelyekFromComponents(KRT_SzemelyekSearch krt_SzemelyekSearch)
    {
        krt_SzemelyekSearch.UjTitulis.Value = textTitulus.Text;
        krt_SzemelyekSearch.UjTitulis.Operator = Search.GetOperatorByLikeCharater(textTitulus.Text);
        krt_SzemelyekSearch.UjCsaladiNev.Value = textCsaladiNev.Text;
        krt_SzemelyekSearch.UjCsaladiNev.Operator = Search.GetOperatorByLikeCharater(textCsaladiNev.Text);
        krt_SzemelyekSearch.UjUtonev.Value = textUtoNev.Text;
        krt_SzemelyekSearch.UjUtonev.Operator = Search.GetOperatorByLikeCharater(textUtoNev.Text);
        krt_SzemelyekSearch.AnyjaNeve.Value = textAnyjaNeve.Text;
        krt_SzemelyekSearch.AnyjaNeve.Operator = Search.GetOperatorByLikeCharater(textAnyjaNeve.Text);
        krt_SzemelyekSearch.ApjaNeve.Value = textApjaNeve.Text;
        krt_SzemelyekSearch.ApjaNeve.Operator = Search.GetOperatorByLikeCharater(textApjaNeve.Text);
        krt_SzemelyekSearch.SzuletesiNev.Value = textSzuletesiNev.Text;
        krt_SzemelyekSearch.SzuletesiNev.Operator = Search.GetOperatorByLikeCharater(textSzuletesiNev.Text);
        krt_SzemelyekSearch.SzuletesiOrszag.Value = OrszagTextBoxSzuletesi.Text;
        krt_SzemelyekSearch.SzuletesiOrszag.Operator = Search.GetOperatorByLikeCharater(OrszagTextBoxSzuletesi.Text);
        krt_SzemelyekSearch.SzuletesiHely.Value = TelepulesTextBoxSzuletesi.Text;
        krt_SzemelyekSearch.SzuletesiHely.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBoxSzuletesi.Text);
        if (!String.IsNullOrEmpty(CalendarSzuletesiIdo.Text))
        {
            krt_SzemelyekSearch.SzuletesiIdo.Value = CalendarSzuletesiIdo.Text;
            krt_SzemelyekSearch.SzuletesiIdo.Operator = Query.Operators.equals;
        }
        else
        {
            krt_SzemelyekSearch.SzuletesiIdo.Value = "";
            krt_SzemelyekSearch.SzuletesiIdo.Operator = "";
        }
        krt_SzemelyekSearch.TAJSzam.Value = textTajSzam.Text;
        krt_SzemelyekSearch.TAJSzam.Operator = Search.GetOperatorByLikeCharater(textTajSzam.Text);
        krt_SzemelyekSearch.SZIGSzam.Value = textSzigSzam.Text;
        krt_SzemelyekSearch.SZIGSzam.Operator = Search.GetOperatorByLikeCharater(textSzigSzam.Text);
        Sex.SetSearchField(krt_SzemelyekSearch.Neme, Sex.GetSelectedValue(rbListNeme));
        krt_SzemelyekSearch.SzemelyiAzonosito.Value = textSzemelyiAzonosito.Text;
        krt_SzemelyekSearch.SzemelyiAzonosito.Operator = Search.GetOperatorByLikeCharater(textSzemelyiAzonosito.Text);
        krt_SzemelyekSearch.Adoazonosito.Value = textAdoAzonosito.Text;
        krt_SzemelyekSearch.Adoazonosito.Operator = Search.GetOperatorByLikeCharater(textAdoAzonosito.Text);
        krt_SzemelyekSearch.Adoszam.Value = textAdoszamSzemely.Text;
        krt_SzemelyekSearch.Adoszam.Operator = Search.GetOperatorByLikeCharater(textAdoszamSzemely.Text);

        if (rblKulfoldiAdoszamSzemely.SelectedValue != "X")
        {
            krt_SzemelyekSearch.KulfoldiAdoszamJelolo.Value = rblKulfoldiAdoszamSzemely.SelectedValue;
            krt_SzemelyekSearch.KulfoldiAdoszamJelolo.Operator = (rblKulfoldiAdoszamSzemely.SelectedValue == "1" ? Query.Operators.equals : Query.Operators.isnullorequals);
        }

        // BLG_346
        krt_SzemelyekSearch.Beosztas.Value = textBeosztas.Text;
        krt_SzemelyekSearch.Beosztas.Operator = Search.GetOperatorByLikeCharater(textBeosztas.Text);
        if (!String.IsNullOrEmpty(KodtarakDropDownListMinositesiSzint.SelectedValue))
        {
            krt_SzemelyekSearch.MinositesiSzint.Value = KodtarakDropDownListMinositesiSzint.SelectedValue;
            krt_SzemelyekSearch.MinositesiSzint.Operator = Query.Operators.equals;
        }
        else
        {
            krt_SzemelyekSearch.MinositesiSzint.Value = "";
            krt_SzemelyekSearch.MinositesiSzint.Operator = "";
        }
    }

    private void GetVallalkozasokFromComponents(KRT_VallalkozasokSearch krt_VallalkozasokSearch)
    {
        krt_VallalkozasokSearch.Adoszam.Value = textAdoSzamVallalkozas.Text;
        krt_VallalkozasokSearch.Adoszam.Operator = Search.GetOperatorByLikeCharater(textAdoSzamVallalkozas.Text);
        if (rblKulfoldiAdoszamVallalkozas.SelectedValue != "X")
        {
            krt_VallalkozasokSearch.KulfoldiAdoszamJelolo.Value = rblKulfoldiAdoszamVallalkozas.SelectedValue;
            krt_VallalkozasokSearch.KulfoldiAdoszamJelolo.Operator = (rblKulfoldiAdoszamVallalkozas.SelectedValue == "1" ? Query.Operators.equals : Query.Operators.isnullorequals);
        }

        krt_VallalkozasokSearch.TB_Torzsszam.Value = textTBSzam.Text;
        krt_VallalkozasokSearch.TB_Torzsszam.Operator = Search.GetOperatorByLikeCharater(textTBSzam.Text);
        krt_VallalkozasokSearch.Cegjegyzekszam.Value = textCegJegyzekSzam.Text;
        krt_VallalkozasokSearch.Cegjegyzekszam.Operator = Search.GetOperatorByLikeCharater(textCegJegyzekSzam.Text);

        if (!String.IsNullOrEmpty(KodtarakDropDownListCegTipus.SelectedValue))
        {
            krt_VallalkozasokSearch.Tipus.Value = KodtarakDropDownListCegTipus.SelectedValue;
            krt_VallalkozasokSearch.Tipus.Operator = Query.Operators.equals;
        }
        else
        {
            krt_VallalkozasokSearch.Tipus.Value = "";
            krt_VallalkozasokSearch.Tipus.Operator = "";
        }

    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject(), true);
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_PartnerekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }



    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
    }

    private KRT_PartnerekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        KRT_PartnerekSearch defaultSearchObject = new KRT_PartnerekSearch();
        return defaultSearchObject;
    }

    void PartnerTipusDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetActivePanel();
    }

}
