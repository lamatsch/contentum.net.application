<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerkapcsolatokForm.aspx.cs" Inherits="PartnerkapcsolatokForm" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnerkapcsolatokFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>   
    <br />

    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor" ">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelPartner" runat="server" Text="Kiv�lasztott partner:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc4:PartnerTextBox ID="PartnerTextBoxPartner" runat="server" />
                                    </td>
                                </tr>

                                <tr id="kapcsoltPartnerRow" class="urlapSor" runat="server" visible="true">
                                    <td class="mrUrlapCaption" >
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelKapcsoltPartner" runat="server" Text="Kapcsolt partner:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc4:PartnerTextBox ID="PartnerTextBoxKapcsoltPartner" runat="server" />
                                    </td>
                                </tr>

                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelKapcsolatTipusa" runat="server" Text="Kapcsolat t�pusa:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:UpdatePanel ID="updatePanelKapcsolatTipusa" runat="server" RenderMode="Inline">
                                            <ContentTemplate>
                                                <uc8:KodtarakDropDownList ID="KodtarakDropDownListKapcsolatTipus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="KodtarakDropDownListKapcsolatTipus_SelectedIndexChanged" Validate="true"/>                                                
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:UpdatePanel runat="server" ID="ListUpdatePanel" Visible="false">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 10px; margin-bottom: 10px;">
                                                    <tr>
                                                        <td style="width: 50%; padding: 0px 10px;">
                                                            <div style="text-align: left; padding-bottom: 2px;">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: auto">
                                                                            <span style="font-weight: bold; text-decoration: underline;">Partnerek:</span>
                                                                        </td>
                                                                        <td style="width: 100%; padding-left: 5px; padding-right: 8px">
                                                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="100%" CausesValidation="false" OnTextChanged="TextBoxSearch_TextChanged" AutoPostBack="true" />
                                                                        </td>
                                                                        <td style="width: auto">
                                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/egyeb/kereses.gif" CausesValidation="false"></asp:ImageButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                        </td>
                                                        <td></td>
                                                        <td style="width: 50%; padding: 0px 10px;">
                                                            <div style="text-align: left; padding-bottom: 2px;">
                                                                <span style="font-weight: bold; text-decoration: underline;">Partner kapcsolatok:</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%; padding: 0px 10px;">
                                                            <asp:ListBox ID="PartnerekList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button runat="server" ID="AddButton" Text=">" Width="50px" Style="margin-bottom: 5px;" ToolTip="Hozz�ad" CausesValidation="false" OnClick="AddButton_Click" />
                                                            <asp:Button runat="server" ID="RemoveButton" Text="<" Width="50px" Style="margin-top: 5px;" ToolTip="T�r�l" CausesValidation="false" OnClick="RemoveButton_Click" />
                                                        </td>
                                                        <td style="width: 50%; padding: 0px 10px;">
                                                            <asp:ListBox ID="HierarchiaPartnerekList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                                        <td colspan="3" style="padding: 10px;">
                                                            <asp:Label ID="label5" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                    <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

