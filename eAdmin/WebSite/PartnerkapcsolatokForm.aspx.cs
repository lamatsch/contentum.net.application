﻿using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Web.Services;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Data;
using System.Linq;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;

public partial class PartnerkapcsolatokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    string queryStringKapcsolatTipus;
    KRT_Partnerek queryStringPartner;

    private const string kodcsoportPartneKapcsolat = "PARTNERKAPCSOLAT_TIPUSA";

    //BUG 7959
    private const string typeError = "A kiválasztott kapcsolat típusa nem rendelhető hozzá a kiválasztott partnerhez.";

    private void SetViewControls()
    {
        PartnerTextBoxPartner.ReadOnly = true;
        PartnerTextBoxKapcsoltPartner.ReadOnly = true;
        KodtarakDropDownListKapcsolatTipus.Enabled = false;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelKapcsoltPartner.CssClass = "mrUrlapInputWaterMarked";
        labelKapcsolatTipusa.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

    }

    private void SetModifyControls()
    {
        PartnerTextBoxPartner.ReadOnly = true;
        PartnerTextBoxKapcsoltPartner.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelKapcsoltPartner.CssClass = "mrUrlapInputWaterMarked";

        FormFooter1.ImageButton_SaveAndClose.Visible = FormFooter1.ImageButton_SaveAndClose.Enabled = true;
    }

    private static void RemoveDropDownListItems(DropDownList ddlist, string filter, bool contains)
    {
        ListItemCollection removableItmes = new ListItemCollection();
        foreach (ListItem item in ddlist.Items)
        {
            if (contains)
            {
                if (filter.Contains(item.Value))
                {
                    removableItmes.Add(item);
                }
            }
            else
            {
                if (!filter.Contains(item.Value))
                {
                    removableItmes.Add(item);
                }
            }
        }
        if (removableItmes.Count > 0)
        {
            foreach (ListItem item in removableItmes)
            {
                ddlist.Items.Remove(item);
            }
        }
    }

    private static void RemoveDropDownListItems(DropDownList ddlist, bool contains)
    {
        RemoveDropDownListItems(ddlist, KodTarak.PartnerKapcsolatTipus.alarendelt, contains);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        KodtarakDropDownListKapcsolatTipus.FillDropDownList(kodcsoportPartneKapcsolat, FormHeader1.ErrorPanel);
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                KodtarakDropDownListKapcsolatTipus.DropDownList.Items.Clear();
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Partnerkapcsolat" + Command);
                break;
        }

        String id = Request.QueryString.Get(QueryStringVars.Id);

        queryStringKapcsolatTipus = Request.QueryString.Get(QueryStringVars.PartnerKapcsolatTipus);

        KRT_PartnerekService partnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam partnerekExecParam = UI.SetExecParamDefault(this.Page, new ExecParam());

        if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal && !string.IsNullOrEmpty(Request.QueryString.Get("PartnerId")))
        {
            partnerekExecParam.Record_Id = Request.QueryString.Get("PartnerId");
        }
        else if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal && !string.IsNullOrEmpty(PartnerTextBoxPartner.Id_HiddenField))
        {
            partnerekExecParam.Record_Id = PartnerTextBoxPartner.Id_HiddenField;
        }
        else if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal && !string.IsNullOrEmpty(Request.QueryString.Get("PartnerId")))
        {
            partnerekExecParam.Record_Id = Request.QueryString.Get("PartnerId");
        }
        else if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.alarendelt && !string.IsNullOrEmpty(Request.QueryString.Get("PartnerId")))
        {
            partnerekExecParam.Record_Id = Request.QueryString.Get("PartnerId");
        }
        else
        {
            partnerekExecParam.Record_Id = Request.QueryString.Get("PartnerId");
        }

        Result partnerekResult = partnerekService.Get(partnerekExecParam);

        if (string.IsNullOrEmpty(partnerekResult.ErrorCode))
        {
            queryStringPartner = (KRT_Partnerek)partnerekResult.Record;
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, partnerekResult);
        }

        //BLG_2219
        //LZS
        //Modify esetet kiszedtem, módosítás esetén nem fut bele a kód ebbe az ágba.
        // BLG_6207, KA: ez így nem jó, kell a Modify is a nem alárendelt kapcsolat esetén
        if (Command == CommandName.View /*|| Command == CommandName.Modify*/
            || (Command == CommandName.Modify && !String.IsNullOrEmpty(id))
            )
        {
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_PartnerKapcsolatok KRT_PartnerKapcsolatok = (KRT_PartnerKapcsolatok)result.Record;
                    LoadComponentsFromBusinessObject(KRT_PartnerKapcsolatok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(partnerId))
            {
                PartnerTextBoxPartner.Id_HiddenField = partnerId;
                PartnerTextBoxPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
                PartnerTextBoxPartner.Enabled = false;
            }

            string partnerKapcsoltId = Request.QueryString.Get(QueryStringVars.Id);
            if (!String.IsNullOrEmpty(partnerKapcsoltId))
            {
                PartnerTextBoxKapcsoltPartner.Id_HiddenField = partnerKapcsoltId;
                PartnerTextBoxKapcsoltPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
                PartnerTextBoxKapcsoltPartner.Enabled = false;
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {

            #region BLG_2219
            //LZS
            //QueryString - ből kiszedjük PartnerId és a KapcsolatTipus értékét. 
            //Alárendelt kapcsolattípus esetén kitöltjük a PartnerTextBoxPartner controlt a partnerId alapján, majd feltöltjük a listbox controlokat tartalommal.
            //Ezekhez a FillPartnerekList() és a FillHierarchiaPartnerekList(partnerid) függvényeket kell használni.
            //Végül pedig láthatóvá kell tenni az egészet tartalmazó ListUpdatePanel - t, és elrejtjük az itt nem szükséges kapcsoltPartnerRow - t.

            string partnerid = Request.QueryString.Get(QueryStringVars.PartnerId);
            string partnerkapcsolattipus = Request.QueryString.Get(QueryStringVars.PartnerKapcsolatTipus);


            SetModifyControls();

            if (!IsPostBack && partnerkapcsolattipus == Constants.PartnerKapcsolatTipus.alarendelt)
            {
                PartnerTextBoxPartner.Id_HiddenField = partnerid;
                PartnerTextBoxPartner.SetPartnerTextBoxById(new Contentum.eUIControls.eErrorPanel());
                FillPartnerekList();
                FillHierarchiaPartnerekList(partnerid);
                ListUpdatePanel.Visible = true;
                kapcsoltPartnerRow.Visible = false;
            }

            #endregion
        }

        FilterKapcsolatTipusokDropdown();
    }


    private void FilterKapcsolatTipusokDropdown()
    {
        if (!String.IsNullOrEmpty(queryStringKapcsolatTipus) && queryStringPartner != null)
        {
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            bool isTUK = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.TUK, false);
            //string tipus = "";
            bool normal = queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal ? true : false;

            var list = KodTarak.PartnerKapcsolatTipus.KapcsolatMappings.FilterByPartnerTipus(queryStringPartner.Tipus).Where(x => x.IsNormal == normal && x.Belso == queryStringPartner.Belso).Select(x => x.KapcsolatTipus).ToArray();
            string[] filtered = list;
            if (!isTUK)
            {
                filtered = list.Except(new string[] { "M" }).ToArray();
            }

            if (filtered.Contains("fake"))
            {
                filtered = list.Except(new string[] { "fake" }).ToArray();
            }

            //LZS - Miért szedi ki?
            RemoveDropDownListItems(KodtarakDropDownListKapcsolatTipus.DropDownList, Search.GetSqlInnerString(filtered), false);

            if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal && KodTarak.PartnerKapcsolatTipus.GetPartnerTipus(queryStringPartner.Tipus) == "10")
            {
                if (queryStringPartner.Belso == "1")
                {
                    PartnerTextBoxKapcsoltPartner.SetFilterType_Szervezet();
                }
                else
                {
                    PartnerTextBoxKapcsoltPartner.SetFilterType_Szemely();
                }
            }

            if (queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal && queryStringPartner.Tipus == "20")
            {
                if (queryStringPartner.Belso == "0")
                {
                    PartnerTextBoxKapcsoltPartner.SetFilterType_Szervezet();
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
        if (!IsPostBack)
        {
            KodtarakDropDownListKapcsolatTipus_SelectedIndexChanged(null, null);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_PartnerKapcsolatok krt_partnerKapcsolatok)
    {
        PartnerTextBoxPartner.Id_HiddenField = krt_partnerKapcsolatok.Partner_id;
        PartnerTextBoxPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        PartnerTextBoxKapcsoltPartner.Id_HiddenField = krt_partnerKapcsolatok.Partner_id_kapcsolt;
        PartnerTextBoxKapcsoltPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        //KapcsolatTipusa.FillListByTipus(KodtarakDropDownListKapcsolatTipus.DropDownList, krt_partnerKapcsolatok.Tipus);
        KodtarakDropDownListKapcsolatTipus.SetSelectedValue(krt_partnerKapcsolatok.Tipus);

        ErvenyessegCalendarControl1.ErvKezd = krt_partnerKapcsolatok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_partnerKapcsolatok.ErvVege;

        FormHeader1.Record_Ver = krt_partnerKapcsolatok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_partnerKapcsolatok.Base);
    }

    // form --> business object
    private KRT_PartnerKapcsolatok GetBusinessObjectFromComponents()
    {
        KRT_PartnerKapcsolatok krt_partnerKapcsolatok = new KRT_PartnerKapcsolatok();
        krt_partnerKapcsolatok.Updated.SetValueAll(false);
        krt_partnerKapcsolatok.Base.Updated.SetValueAll(false);

        krt_partnerKapcsolatok.Partner_id = PartnerTextBoxPartner.Id_HiddenField;
        krt_partnerKapcsolatok.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBoxPartner);

        krt_partnerKapcsolatok.Partner_id_kapcsolt = PartnerTextBoxKapcsoltPartner.Id_HiddenField;
        krt_partnerKapcsolatok.Updated.Partner_id_kapcsolt = pageView.GetUpdatedByView(PartnerTextBoxKapcsoltPartner);

        krt_partnerKapcsolatok.Tipus = KodtarakDropDownListKapcsolatTipus.SelectedValue;
        krt_partnerKapcsolatok.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListKapcsolatTipus);

        krt_partnerKapcsolatok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        krt_partnerKapcsolatok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        krt_partnerKapcsolatok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        krt_partnerKapcsolatok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_partnerKapcsolatok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_partnerKapcsolatok.Base.Ver = FormHeader1.Record_Ver;
        krt_partnerKapcsolatok.Base.Updated.Ver = true;

        return krt_partnerKapcsolatok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(PartnerTextBoxPartner);
            //compSelector.Add_ComponentOnClick(PartnerTextBoxKapcsoltPartner);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListKapcsolatTipus);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

        }

    }

    #region BUG 7959 - kapcsolat típus ellenőrzés
    private Boolean CheckPartnerKapcsolatTypeAllowed(Boolean isNew)
    {
        //KRT_PartnerekService partnerekService = 
        //krt_partnerKapcsolatok.Partner_id = PartnerTextBoxPartner.Id_HiddenField;
        KRT_PartnerekService partnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam partnerekExecParam = UI.SetExecParamDefault(this.Page, new ExecParam());

        partnerekExecParam.Record_Id = (!isNew && KodtarakDropDownListKapcsolatTipus.SelectedValue == KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja)
            ? PartnerTextBoxKapcsoltPartner.Id_HiddenField : PartnerTextBoxPartner.Id_HiddenField;

        Result partnerekResult = partnerekService.Get(partnerekExecParam);

        Boolean allowed = false;

        if (string.IsNullOrEmpty(partnerekResult.ErrorCode))
        {
            KRT_Partnerek partner = (KRT_Partnerek)partnerekResult.Record;

            if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
            {
                if (partner.Belso == "1")
                {
                    KodTarak.PartnerKapcsolatTipus.normalszervezetbelso.Replace("\'", string.Empty);
                    String[] belsoSzervezetKapcsolatTipusok = (KodTarak.PartnerKapcsolatTipus.normalszervezetbelso.Replace("\'", string.Empty).Split(','));
                    allowed = belsoSzervezetKapcsolatTipusok.Contains(KodtarakDropDownListKapcsolatTipus.SelectedValue);
                }
                else
                {
                    allowed = KodtarakDropDownListKapcsolatTipus.SelectedValue == KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                }
            }
        }

        return allowed;
    }
    #endregion


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //BUG_4425 - LZS -
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Partnerkapcsolat" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            //BUG 7959
                            if (!CheckPartnerKapcsolatTypeAllowed(true))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, typeError);

                                //Ha nem tud menteni, akkor végezze el a beállításokat, hogy megmaradjon a partner id!
                                string partnerid = Request.QueryString.Get(QueryStringVars.PartnerId);

                                FillHierarchiaPartnerekList(partnerid);
                                FillPartnerekList();

                                return;
                            }
                            KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                            KRT_PartnerKapcsolatok krt_partnerKapcsolatok = GetBusinessObjectFromComponents();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_partnerKapcsolatok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                //visszaküldés a hívó form-nak kihagyva, nem tudni mit kéne visszaküldeni
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            // BUG_6207: Külön kell kezelni a két irányt ("Kapcsolatok" és "Partner hierarchia" felől indítottat):
                            if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.PartnerKapcsolatTipus)))
                            {
                                #region "Normál" kapcsolat
                                if (Request.QueryString.Get(QueryStringVars.PartnerKapcsolatTipus) == KodTarak.PartnerKapcsolatTipus.normal)
                                {
                                    //BUG 7959
                                    if (!CheckPartnerKapcsolatTypeAllowed(false))
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, typeError);
                                        return;
                                    }

                                    String recordId = Request.QueryString.Get(QueryStringVars.Id);
                                    if (String.IsNullOrEmpty(recordId))
                                    {
                                        // nincs Id megadva:
                                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                                    }
                                    else
                                    {
                                        KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                                        KRT_PartnerKapcsolatok krt_partnerKapcsolatok = GetBusinessObjectFromComponents();
                                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                        execParam.Record_Id = recordId;

                                        Result result = service.Update(execParam, krt_partnerKapcsolatok);

                                        if (!result.IsError)
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                            JavaScripts.RegisterCloseWindowClientScript(Page);
                                        }
                                        else
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                        }
                                    }
                                }
                                #endregion

                                else
                                {
                                    #region Partner hierarchia

                                    String recordId = Request.QueryString.Get(QueryStringVars.PartnerId);
                                    if (String.IsNullOrEmpty(recordId))
                                    {
                                        // nincs Id megadva:
                                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                                    }
                                    else
                                    {
                                        if (!HandlePartnerKapcsolatok(recordId, e.CommandName))
                                        {
                                            return;
                                        }
                                    }
                                    #endregion
                                }
                            }
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected bool HandlePartnerKapcsolatok(string recordId, string command)
    {
        #region BLG_2219
        //LZS
        //Módosítás esetén lekérjük a partnerkapcsolatok eredeti állapotát a GetPartnerKapcsolatok() függvénnyel.
        //Egy KRT_PartnerKapcsolatok objektumot feltöltünk a felületen lévő controlokból, majd pedig a listboxban kiválasztott elemeket egy foreach ciklusban hozzáadogatjuk,
        //és egyesével insertáljuk az adatbázisba akkor, ha még nem szerepelt benne az adott id - jú rekord.
        //Hiba esetén kiíratjuk azt a képernyő tetejére a DisplayErrorMessage() függvénnyel.
        //Végül egy invalidate rész is lefut, ahol az eredeti állapothoz képest törölt elemeket invalidáljuk az adatbázisban.
        //Hiba esetén itt is kiíratjuk azt a képernyő tetejére a DisplayErrorMessage() függvénnyel.

        KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
        List<KRT_PartnerKapcsolatok> partnerKapcsolatokBase;

        //BUG_12185
        //Alárendelt kapcsolatnál máshogy kell lekérdezni:
        if (KodtarakDropDownListKapcsolatTipus.SelectedValue == "11")
        {
            partnerKapcsolatokBase = GetPartnerKapcsolatokForAlarendelt(recordId);
        }
        else
        {
            partnerKapcsolatokBase = GetPartnerKapcsolatok(recordId, KodtarakDropDownListKapcsolatTipus.SelectedValue);
        }

        KRT_PartnerKapcsolatok krt_partnerKapcsolatok = new KRT_PartnerKapcsolatok();
        krt_partnerKapcsolatok = GetBusinessObjectFromComponents();
        //krt_partnerKapcsolatok.Partner_id_kapcsolt = recordId;
        krt_partnerKapcsolatok.Partner_id = recordId;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = recordId;

        Result result = null;
        Result resultInvalidated = null;

        foreach (ListItem item in HierarchiaPartnerekList.Items)
        {
            if (item != null)
            {
                //krt_partnerKapcsolatok.Partner_id = item.Value;
                krt_partnerKapcsolatok.Partner_id_kapcsolt = item.Value;

                //LZS - Ha tartalmazza az eredeti partnerkapcsolatok collection, akkor nem változott (maradt), és nincs vele teendő.
                //LZS - Ha nem, akkor beszúrjuk.
                //BUG_12185
                if (KodtarakDropDownListKapcsolatTipus.SelectedValue == "11")
                {
                    var matchesAlarendelt = partnerKapcsolatokBase.Where(p => p.Partner_id == krt_partnerKapcsolatok.Partner_id && p.Partner_id_kapcsolt == krt_partnerKapcsolatok.Partner_id_kapcsolt);
                    

                    if (matchesAlarendelt.Count() == 0)
                    {
                        result = service.Insert(execParam, krt_partnerKapcsolatok);

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                            return false;
                        }
                    }
                }
                else
                {
                    var matches = partnerKapcsolatokBase.Where(p => p.Id == krt_partnerKapcsolatok.Partner_id_kapcsolt && p.Partner_id_kapcsolt == krt_partnerKapcsolatok.Partner_id);

                    if (matches.Count() == 0)
                    {
                        result = service.Insert(execParam, krt_partnerKapcsolatok);

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                            return false;
                        }
                    }
                }


            }
        }

        //Invalidate, ha szükséges
        string[] baseKapcsolatok;
        string[] currentKapcsolatok = new string[HierarchiaPartnerekList.Items.Count];

        //Kigyűjtjük az Id-kat az eredeti állapotot tároló listából a baseKapcsolatok tömbbe.
        //BUG_12185
        if (KodtarakDropDownListKapcsolatTipus.SelectedValue != "11")
        {
            baseKapcsolatok = partnerKapcsolatokBase.Select(x => x.Id).ToArray<string>();


            //Kigyűjtjük az Id-kat a jelenlegi állapotot tároló listából a currentKapcsolatok tömbbe.
            for (int i = 0; i < HierarchiaPartnerekList.Items.Count; i++)
            {
                currentKapcsolatok[i] = HierarchiaPartnerekList.Items[i].Value;
            }

            //Képezzük a két tömb különségét. Ez lesz a invalidálandó id-k tömbje. [idForInvalidate]
            IEnumerable<string> idForInvalidate = baseKapcsolatok.Except(currentKapcsolatok);

            //Végigmegyünk egy ciklussal az idForInvalidate tömbbön, és egyesével invalidáljuk a hozzá tartozó rekordokat az adatbázisból.
            foreach (string id in idForInvalidate)
            {
                resultInvalidated = service.Invalidate(new ExecParam() { Record_Id = id });

                if (!String.IsNullOrEmpty(resultInvalidated.ErrorCode))
                {
                    ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, resultInvalidated);
                    return false;
                }
            }
        }

        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);

        if (command == CommandName.SaveAndClose)
        {
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }

        //BUG_4425 - LZS - Mentés után engedélyezzük a kapcsolatok legördülő lista változtatásának lehetőségét.
        KodtarakDropDownListKapcsolatTipus.Enabled = true;

        return true;
        #endregion
    }

    private string GetKodTarId(ExecParam execParam, string kod, string where)
    {
        Contentum.eAdmin.Service.KRT_KodTarakService kodTarakService = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        KRT_KodTarakSearch kodTarakSearch = new KRT_KodTarakSearch();
        kodTarakSearch.Kod.Value = kod;
        kodTarakSearch.Kod.Operator = Query.Operators.equals;
        kodTarakSearch.WhereByManual = where;

        Result kodTarakResult = kodTarakService.GetAllWithKodcsoport(execParam, kodTarakSearch);
        string result = "";
        if (!kodTarakResult.IsError && kodTarakResult.Ds.Tables[0].Rows.Count > 0)
        {
            result = kodTarakResult.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Kod"].ToString() == kod)["Id"].ToString();
        }
        return result;
    }

    private string GetKodTar(ExecParam execParam, string id, string where)
    {
        Contentum.eAdmin.Service.KRT_KodTarakService kodTarakService = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        KRT_KodTarakSearch kodTarakSearch = new KRT_KodTarakSearch();
        kodTarakSearch.Id.Value = id;
        kodTarakSearch.Id.Operator = Query.Operators.equals;
        kodTarakSearch.WhereByManual = where;

        Result kodTarakResult = kodTarakService.GetAllWithKodcsoport(execParam, kodTarakSearch);
        string result = "";
        if (!kodTarakResult.IsError && kodTarakResult.Ds.Tables[0].Rows.Count > 0)
        {
            result = kodTarakResult.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Id"].ToString() == id)["Kod"].ToString();
        }
        return result;
    }

    private string GetPartnerTipus(ExecParam execParam, string kapcsKod)
    {
        string kapcsKodGuid = "";
        string tipus = null;
        string tipusGuid = "";

        kapcsKodGuid = GetKodTarId(execParam, kapcsKod, " and KRT_KodCsoportok.Kod='PARTNERKAPCSOLAT_TIPUSA' ");
        //selectedTertvisszaGuid = GetKodTarId(execParam, tertivisszaKod, " and KRT_KodCsoportok.Kod='TERTIVEVENY_VISSZA_KOD' ");

        Contentum.eAdmin.Service.KRT_KodtarFuggosegService kodservice = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "PARTNERKAPCSOLAT_TIPUSA");
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "PARTNER_TIPUS");
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

        Result res = kodservice.GetAll(execParam, search);

        if (!res.IsError)
        {
            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                List<string> fuggoKodtarak = new List<string>();

                var item = fuggosegek.Items.FirstOrDefault(x => (x.VezerloKodTarId == kapcsKodGuid));


                if (item != null)
                {
                    tipusGuid = item.FuggoKodtarId;
                }
            }
        }

        if (!string.IsNullOrEmpty(tipusGuid))
        {
            tipus = GetKodTar(execParam, tipusGuid, " and KRT_KodCsoportok.Kod='PARTNER_TIPUS' ");
        }

        return tipus;
    }

    #region BLG_2219
    /// <summary>
    /// Feltölti az összes partnerrel a képernyő bal oldalán lévő PartnerekList listbox-ot név szerint rendezve. Szűrve a kiválasztott kapcsolattípusra
    /// </summary>
    void FillPartnerekList()
    {
        PartnerekList.Items.Clear();

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);
        KRT_PartnerekSearch search = new KRT_PartnerekSearch();
        search.OrderBy = "Nev";
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;

        string pTipus = Request.QueryString.Get(QueryStringVars.PartnerKapcsolatTipus);

        string tipus = "";
        string kapcsolatTipus = KodtarakDropDownListKapcsolatTipus.SelectedValue;
        string belso = "";
        tipus = GetPartnerTipus(ExecParam, kapcsolatTipus);

        if (string.IsNullOrEmpty(tipus))
        {
            bool normal = queryStringKapcsolatTipus == Constants.PartnerKapcsolatTipus.normal ? true : false;
            var list = KodTarak.PartnerKapcsolatTipus.KapcsolatMappings.FilterByPartnerTipus(queryStringPartner.Tipus).Where(x => x.IsNormal == normal && x.Belso == queryStringPartner.Belso && x.KapcsolatTipus == kapcsolatTipus).Select(x => new { x.ListazandoPartnerTipus, x.ListazandoPartnerBelso }).ToArray();
            var first = list.FirstOrDefault();

            if (first != null)
            {
                tipus = first.ListazandoPartnerTipus;
                belso = first.ListazandoPartnerBelso;
            }
        }

        search.Tipus.Value = Search.GetSqlInnerString(KodTarak.PartnerKapcsolatTipus.KapcsolatMapping.GetListazandoPartnerTipusok(tipus));
        search.Tipus.Operator = Query.Operators.inner;

        ////BUG_4885 - TASK_4907
        //search.Belso.Value = Constants.Database.Yes;
        search.Belso.Value = belso;
        search.Belso.Operator = Query.Operators.equals;

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);
        UI.ListBoxFill(PartnerekList, result, "Nev", FormHeader1.ErrorPanel, null);
        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }
    /// <summary>
    /// Feltölti a kiválasztott partnerhez tartozó partnerkapcsolatokkal a képernyő jobb oldalán lévő HierarchiaPartnerekList listbox-ot név szerint rendezve. Szűrve a kiválasztott kapcsolattípusra.
    /// </summary>
    /// <param name="partnerId"></param>
    void FillHierarchiaPartnerekList(string partnerId)
    {
        HierarchiaPartnerekList.Items.Clear();

        if (!String.IsNullOrEmpty(partnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = partnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();

            //if (KodtarakDropDownListKapcsolatTipus.SelectedValue == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere)
            //{
            //    search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.alarendelt;
            //    search.Tipus.Operator = Query.Operators.inner;
            //}
            //else
            {
                search.Tipus.Value = KodtarakDropDownListKapcsolatTipus.SelectedValue;
                search.Tipus.Operator = Query.Operators.equals;
            }

            search.OrderBy = "Nev";

            Result result = service.GetAllByPartner(ExecParam, partner, search);
            UI.ListBoxFill(HierarchiaPartnerekList, result, "Nev", FormHeader1.ErrorPanel, null);
        }

    }

    //BUG_12185
    //Alárendelt kapcsolatnál fordítva van a partner_id és a partner_id_kapcsolt
    //Ezt kezeli ez a metódus
    private List<KRT_PartnerKapcsolatok> GetPartnerKapcsolatokForAlarendelt(string partnerId)
    {
        List<KRT_PartnerKapcsolatok> partnerKapcsolatokBase = new List<KRT_PartnerKapcsolatok>();

        if (!String.IsNullOrEmpty(partnerId))
        {

            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = partnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();

            if (KodtarakDropDownListKapcsolatTipus.SelectedValue == KodTarak.PartnerKapcsolatTipus.alarendeltPartnere)
            {
                search.Tipus.Value = KodTarak.PartnerKapcsolatTipus.alarendelt;
                search.Tipus.Operator = Query.Operators.inner;
            }

            search.OrderBy = "Nev";

            Result result = service.GetAllByPartner(ExecParam, partner, search);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                return null;
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                KRT_PartnerKapcsolatok kapcsolat = new KRT_PartnerKapcsolatok();
                kapcsolat.Id = row["Id"].ToString();
                kapcsolat.ErvKezd = row["ErvKezd"].ToString();
                kapcsolat.ErvVege = row["ErvVege"].ToString();
                kapcsolat.Partner_id = row["Partner_id"].ToString();
                kapcsolat.Partner_id_kapcsolt = row["Partner_id_kapcsolt"].ToString();
                kapcsolat.Tipus = row["Tipus"].ToString();

                partnerKapcsolatokBase.Add(kapcsolat);
            }
        }

        return partnerKapcsolatokBase;
    }


    /// <summary>
    /// A partnerkapcsolatok eredeti állapotát adja vissza adatbázisból, és eltárolja egy KRT_PartnerKapcsolatok listában.
    /// </summary>
    /// <param name="partnerId"></param>
    /// <returns></returns>
    private List<KRT_PartnerKapcsolatok> GetPartnerKapcsolatok(string partnerId, string kapcsolatTipus)
    {
        List<KRT_PartnerKapcsolatok> partnerKapcsolatokBase = new List<KRT_PartnerKapcsolatok>();

        if (!String.IsNullOrEmpty(partnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = partnerId;
            KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();
            //BUG_4425 - LZS - szűrűnk a kiválasztott kapcsolattípusra
            search.Tipus.Value = kapcsolatTipus;
            search.Tipus.Operator = Query.Operators.equals;
            search.OrderBy = "Nev";

            Result result = service.GetAllByPartner(ExecParam, partner, search);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayErrorMessage(FormHeader1.ErrorPanel, result);
                return null;
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                KRT_PartnerKapcsolatok kapcsolat = new KRT_PartnerKapcsolatok();
                kapcsolat.Id = row["Id"].ToString();
                kapcsolat.ErvKezd = row["ErvKezd"].ToString();
                kapcsolat.ErvVege = row["ErvVege"].ToString();
                //LZS - fordítva van, de nem értem miért
                kapcsolat.Partner_id = row["Partner_id_kapcsolt"].ToString();
                kapcsolat.Partner_id_kapcsolt = row["Partner_id"].ToString();
                kapcsolat.Tipus = row["Tipus"].ToString();

                partnerKapcsolatokBase.Add(kapcsolat);
            }
        }

        return partnerKapcsolatokBase;
    }
    /// <summary>
    /// Partnerek listából a partnerkapcsolatok listához ad elemeket.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddButton_Click(object sender, EventArgs e)
    {
        if (KodtarakDropDownListKapcsolatTipus.SelectedValue == KodTarak.PartnerKapcsolatTipus.vezetoje && HierarchiaPartnerekList.Items.Count > 0)
            return;

        //BUG_4425 - LZS - Változtatás alatt letiltjuk a kapcsolatok legördülő lista változtatásának lehetőségét.
        KodtarakDropDownListKapcsolatTipus.Enabled = false;

        List<ListItem> selectedItems = GetSelectedItems(PartnerekList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = HierarchiaPartnerekList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                HierarchiaPartnerekList.Items.Add(item);
            }
        }
    }

    /// <summary>
    /// Partnerkapcsaolatok listából a vesz el elemeket.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        //BUG_4425 - LZS - Változtatás alatt letiltjuk a kapcsolatok legördülő lista változtatásának lehetőségét.
        KodtarakDropDownListKapcsolatTipus.Enabled = false;

        List<ListItem> selectedItems = GetSelectedItems(HierarchiaPartnerekList);
        foreach (ListItem item in selectedItems)
        {
            HierarchiaPartnerekList.Items.Remove(item);
        }
    }

    /// <summary>
    /// Az adott listboxban kijelölt elemeket adja vissza.
    /// </summary>
    /// <param name="listBox"></param>
    /// <returns></returns>
    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    /// <summary>
    /// Szűr a partnerek között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillPartnerekList();
    }

    /// <summary>
    /// Szűr a partnerek között.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillPartnerekList();
    }
    #endregion

    protected void KodtarakDropDownListKapcsolatTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        string partnerid = Request.QueryString.Get(QueryStringVars.PartnerId);

        FillHierarchiaPartnerekList(partnerid);
        FillPartnerekList();

    }
}
