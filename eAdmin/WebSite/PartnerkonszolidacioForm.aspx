<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerkonszolidacioForm.aspx.cs" Inherits="PartnerkonszolidacioForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc3" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc7" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc15" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/OrszagokTextBox.ascx" TagName="OrszagokTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc11" %>
<%@ Register Src="Component/AdoszamTextBox.ascx" TagName="AdoszamTextBox" TagPrefix="uc1" %>
<%@ Register Src="Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc6" %>
<%@ Register Src="~/Component/PlainPartnerTextBox.ascx" TagName="PlainPartnerTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc33" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" AsyncPostBackTimeout="360000"
        EnablePageMethods="true" ScriptMode="Release">
    </asp:ScriptManager>
    <br />
    <asp:Panel ID="rootPanel" runat="server">
        <asp:UpdatePanel ID="updatePanelPartnerKonszolidacio" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnerkonszolidacioFormHeaderTitle %>" />
                <table cellspacing="0" cellpadding="0" width="90%">
                    <tbody>
                        <tr>
                            <td>
                                <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Body="FIGYELEM!!! A konszolid�land� partner �sszes k�ldem�nye �s iratp�ld�nya a kijel�lt partnerhez lesz rendelve!" Header="Figyelmeztet�s!" IsWarning="true">
                                </eUI:eErrorPanel>
                                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tbody>
                                            <tr class="urlapNyitoSor">
                                                <td class="mrUrlapCaption" style="text-align: left;">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                                <td class="mrUrlapMezo" style="text-align: left;">
                                                    <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td style="text-align: left;">
                                                    <table>
                                                        <tr>
                                                            <td class="mrUrlapCaption_shortest" style="text-align: left; width: 50px; min-width: 50px; padding-right: 0px;">
                                                                <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" style="text-align: left;">
                                                                <uc4:EditablePartnerTextBox ID="PartnerTextBoxPartner" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td style="text-align: left;">
                                                    <table>
                                                        <tr>
                                                            <td class="mrUrlapCaption_shortest" style="text-align: left; width: 50px; min-width: 50px; padding-right: 0px;">
                                                                <asp:Label ID="labelKonszolidaltPartner" runat="server" Text="Konszolid�land� partner:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" style="text-align: left;" style="height: 30px">
                                                                <uc4:EditablePartnerTextBox ID="PartnerTextBoxKonszolidaltPartner" runat="server"  WithAllCimCheckBoxVisible="false" />
                                                                <asp:HiddenField ID="betoltveHiddenField" runat="server" Value="0" />
                                                            </td>
                                                            <td class="mrUrlapMezo" style="text-align: left;" style="height: 30px">
                                                                <asp:Button ID="btnLoadKonszolidaltPartnerData" runat="server" Text="Bet�lt�s" OnClick="btnLoadKonszolidaltPartnerData_Click" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <eUI:eFormPanel ID="EFormPanelPartner" runat="server">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblPartnerReqTipus" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblPartnerTipus" runat="server" Text="T�pus:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc7:KodtarakDropDownList ID="ddlPartnerTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trPartnerNev" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblPartnerReqNev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblPartnerNev" runat="server" Text="N�v:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc:PlainPartnerTextBox ID="txtPartnerNev" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblPartnerOrszag" runat="server" Text="Orsz�g:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc8:OrszagokTextBox ID="txtPartnerOrszagok" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblReqPartnerBelso" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblPartnerBelso" runat="server" Text="Bels�:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <asp:DropDownList ID="ddlPartnerBelso" runat="server" CssClass="mrUrlapInputComboBox">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblPartnerAllapot" runat="server" Text="�llapot:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc7:KodtarakDropDownList ID="ddlPartnerAllapot" runat="server" CssClass="mrUrlapInputComboBox" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trPartnerKulsoAzonosito" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:Label ID="lblPartnerKulsoAzonosito" runat="server" Text="K�ls� azonos�t�:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <asp:TextBox ID="txtPartnerKulsoAzonosito" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- Szem�lyek -->
                                                        <asp:Panel ID="panelPartnerSzemelyek" runat="server">
                                                            <div class="mrUrlapSubTitleWrapper">
                                                                <span class="mrUrlapSubTitleLeftSide"></span><span class="mrUrlapSubTitle">
                                                                    <asp:Label ID="lblPartnerTovabbiSzemely" runat="server" Text="Szem�ly tov�bbi adatai"></asp:Label>
                                                                </span><span class="mrUrlapSubTitleRightSide"></span>
                                                            </div>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td>
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerTitulus" runat="server" Text="Titulus:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerTitulus" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerReqCsaladiNev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                            <asp:Label ID="lblPartnerCsaladiNev" runat="server" Text="Csal�di n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc5:RequiredTextBox ID="txtPartnerCsaladiNev" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerReqUtonev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                            <asp:Label ID="lblPartnerUtoNev" runat="server" Text="Ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc5:RequiredTextBox ID="txtPartnerUtoNev" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerTovabbiUtonev" runat="server" Text="Tov�bbi ut�nevek:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerTovabbiUtonev" runat="server" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr id="PartnerParentsDataRow" runat="server" class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAnyjaCsaladiNeve" runat="server" Text="Anyja csal�di neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerAnyjaCsaladiNeve" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <%--BLG_1952--%>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAnyjaUtoNev" runat="server" Text="Anyja Ut�neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerAnyjaUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PartnerParentsDataRow2" runat="server" class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAnyjaTovabbiUtoNev" runat="server" Text="Anyja tov�bbi ut�nevei:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerAnyjaTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiCsaladiNev" runat="server" Text="Sz�let�si csal�di neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerSzuletesiCsaladiNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiUtoneNev" runat="server" Text="Sz�let�si ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerSzuletesiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiTovabbiUtoneNev" runat="server" Text="Sz�let�si tov�bbi ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerSzuletesiTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiIdo" runat="server" Text="Sz�let�si id�:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc11:CalendarControl ID="calPartnerSzuletesiIdo" runat="server" Validate="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiOrszag" runat="server" Text="Sz�let�si orsz�g:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc9:OrszagTextBox ID="txtPartnerSzuletesiOrszag" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzuletesiHely" runat="server" Text="Sz�let�si hely:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc10:TelepulesTextBox ID="txtPartnerSzuletesiHely" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PartnerTajSzigDataRow" runat="server" class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerTajSzam" runat="server" Text="TAJ sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerTajSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzemelyi" runat="server" Text="<%$Forditas:labelSzemelyi|Szem�lyi igazolv�ny sz�m:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerSzigSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerNeme" runat="server" Text="Neme:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:RadioButtonList ID="rblPartnerNeme" runat="server" CssClass="mrUrlapInputSearchRadioButtonList" RepeatDirection="Horizontal">
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerSzemelyiAzonosito" runat="server" Text="<%$Forditas:labelSzemelyiAzonosito|Szem�lyi azonos�t�:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerSzemelyiAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="PartnerVatDataRow" runat="server" class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAdoAzonosito" runat="server" Text="Ad�azonos�t� jel:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerAdoAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAdoSzamSzemely" runat="server" Text="Ad�sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc1:AdoszamTextBox ID="txtPartnerAdoszamSzemely" runat="server" CssClass="mrUrlapInputSearch" KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <%--BLG_346--%>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerBeosztas" runat="server" Text="<%$Forditas:labelBeosztas|Beoszt�s:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerBeosztas" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerMinositesiSzint" runat="server" Text="<%$Forditas:labelMinositesiSzint|Min�s�t�si szint:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc7:KodtarakDropDownList ID="ddlPartnerMinositesiSzint" runat="server" CssClass="mrUrlapInputComboBox" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:Panel>
                                                        <!-- V�llalkoz�sok -->
                                                        <asp:Panel ID="panelPartnerVallalkozasok" runat="server">
                                                            <div class="mrUrlapSubTitleWrapper">
                                                                <span class="mrUrlapSubTitleLeftSide"></span><span class="mrUrlapSubTitle">
                                                                    <asp:Label ID="lblPartnerTovabbiSzervezet" runat="server" Text="Szervezet tov�bbi adatai"></asp:Label>
                                                                </span><span class="mrUrlapSubTitleRightSide"></span>
                                                            </div>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;" style="padding-right: 232px">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td>
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;" style="padding-right: 230px">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerAdoSzamVallalkozas" runat="server" Text="Ad�sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc1:AdoszamTextBox ID="txtPartnerAdoSzamVallalkozas" runat="server" CssClass="mrUrlapInputSearch" KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="false" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerTBSzam" runat="server" Text="TB T�rzssz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerTBSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerCegJegyzekSzam" runat="server" Text="C�gjegyz�ksz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtPartnerCegJegyzekSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:Label ID="lblPartnerCegTipus" runat="server" Text="<%$Forditas:labelCegTipus|C�g t�pus:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc7:KodtarakDropDownList ID="ddlPartnerCegTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:Panel>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 40px;">
                                                                        <asp:Label ID="lblPartnerReqErvenyesseg" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblPartnerErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc6:ErvenyessegCalendarControl ID="calPartnerErvenyesseg" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </eUI:eFormPanel>
                                                </td>
                                                <td>
                                                    <eUI:eFormPanel ID="EFormPanelKonszolidalandoPartner" runat="server">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerTipus" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerTipus" runat="server" Visible="false" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerReqTipus" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblKonszolidalandoPartnerTipus" runat="server" Text="T�pus:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc7:KodtarakDropDownList ID="ddlKonszolidalandoPartnerTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerNev" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerNev" runat="server" Visible="false" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerReqNev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblKonszolidalandoPartnerNev" runat="server" Text="N�v:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc:PlainPartnerTextBox ID="txtKonszolidalandoPartnerNev" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerOrszag" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerOrszag" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerOrszag" runat="server" Text="Orsz�g:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc8:OrszagokTextBox ID="txtKonszolidalandoPartnerOrszagok" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerBelso" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerBelso" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblReqKonszolidalandoPartnerBelso" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblKonszolidalandoPartnerBelso" runat="server" Text="Bels�:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <asp:DropDownList ID="ddlKonszolidalandoPartnerBelso" runat="server" CssClass="mrUrlapInputComboBox">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerAllapot" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerAllapot" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerAllapot" runat="server" Text="�llapot:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc7:KodtarakDropDownList ID="ddlKonszolidalandoPartnerAllapot" runat="server" Enabled="false" CssClass="mrUrlapInputComboBox" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerKulsoAzonosito" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 20px; text-align: left;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerKulsoAzonosito" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerKulsoAzonosito" runat="server" Text="K�ls� azonos�t�:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <asp:TextBox ID="txtKonszolidalandoPartnerKulsoAzonosito" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- Szem�lyek -->
                                                        <asp:Panel ID="panelKonszolidalandoPartnerSzemelyek" runat="server">
                                                            <div class="mrUrlapSubTitleWrapper">
                                                                <span class="mrUrlapSubTitleLeftSide"></span><span class="mrUrlapSubTitle">
                                                                    <asp:Label ID="lblKonszolidalandoPartnerTovabbiSzemely" runat="server" Text="Szem�ly tov�bbi adatai"></asp:Label>
                                                                </span><span class="mrUrlapSubTitleRightSide"></span>
                                                            </div>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td>
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerTitulus" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerTitulus" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerTitulus" runat="server" Text="Titulus:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerTitulus" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerCsaladiNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerCsaladiNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerReqCsaladiNev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                            <asp:Label ID="lblKonszolidalandoPartnerCsaladiNev" runat="server" Text="Csal�di n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc5:RequiredTextBox ID="txtKonszolidalandoPartnerCsaladiNev" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerUtoNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerUtoNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerReqUtonev" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                            <asp:Label ID="lblKonszolidalandoPartnerUtoNev" runat="server" Text="Ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc5:RequiredTextBox ID="txtKonszolidalandoPartnerUtoNev" runat="server" Validate="false" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerTovabbiUtonev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerTovabbiUtonev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerTovabbiUtonev" runat="server" Text="Tov�bbi ut�nevek:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerTovabbiUtonev" runat="server" CssClass="mrUrlapInputSearch" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr id="KonszolidalandoPartnerParentsDataRow" runat="server" class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerAnyjaCsaladiNeve" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAnyjaCsaladiNeve" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAnyjaCsaladiNeve" runat="server" Text="Anyja csal�di neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerAnyjaCsaladiNeve" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <%--BLG_1952--%>
                                                                        <td id="tdKonszolidalandoPartnerAnyjaUtoNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAnyjaUtoNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAnyjaUtoNev" runat="server" Text="Anyja Ut�neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerAnyjaUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="KonszolidalandoPartnerParentsDataRow2" runat="server" class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerAnyjaTovabbiUtoNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAnyjaTovabbiUtoNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAnyjaTovabbiUtoNev" runat="server" Text="Anyja tov�bbi ut�nevei:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerAnyjaTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;"></td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;"></td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiCsaladiNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiCsaladiNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiCsaladiNev" runat="server" Text="Sz�let�si csal�di neve:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerSzuletesiCsaladiNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiUtoneNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiUtoneNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiUtoneNev" runat="server" Text="Sz�let�si ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerSzuletesiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiTovabbiUtoneNev" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiTovabbiUtoneNev" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiTovabbiUtoneNev" runat="server" Text="Sz�let�si tov�bbi ut�n�v:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerSzuletesiTovabbiUtoNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiIdo" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiIdo" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiIdo" runat="server" Text="Sz�let�si id�:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc11:CalendarControl ID="calKonszolidalandoPartnerSzuletesiIdo" runat="server" Validate="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiOrszag" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiOrszag" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiOrszag" runat="server" Text="Sz�let�si orsz�g:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc9:OrszagTextBox ID="txtKonszolidalandoPartnerSzuletesiOrszag" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerSzuletesiHely" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzuletesiHely" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzuletesiHely" runat="server" Text="Sz�let�si hely:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc10:TelepulesTextBox ID="txtKonszolidalandoPartnerSzuletesiHely" runat="server" CssClass="mrUrlapInputSearch" Validate="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="KonszolidalandoPartnerTajSzigDataRow" runat="server" class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerTajSzam" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerTajSzam" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerTajSzam" runat="server" Text="TAJ sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerTajSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerSzemelyi" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzemelyi" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzemelyi" runat="server" Text="<%$Forditas:labelSzemelyi|Szem�lyi igazolv�ny sz�m:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerSzigSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerNeme" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerNeme" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerNeme" runat="server" Text="Neme:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:RadioButtonList ID="rblKonszolidalandoPartnerNeme" runat="server" CssClass="mrUrlapInputSearchRadioButtonList" RepeatDirection="Horizontal">
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerSzemelyiAzonosito" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerSzemelyiAzonosito" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerSzemelyiAzonosito" runat="server" Text="<%$Forditas:labelSzemelyiAzonosito|Szem�lyi azonos�t�:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerSzemelyiAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="KonszolidalandoPartnerVatDataRow" runat="server" class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerAdoAzonosito" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAdoAzonosito" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAdoAzonosito" runat="server" Text="Ad�azonos�t� jel:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerAdoAzonosito" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerAdoSzamSzemely" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAdoSzamSzemely" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAdoSzamSzemely" runat="server" Text="Ad�sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc1:AdoszamTextBox ID="txtKonszolidalandoPartnerAdoszamSzemely" runat="server" CssClass="mrUrlapInputSearch" KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <%--BLG_346--%>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerBeosztas" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerBeosztas" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerBeosztas" runat="server" Text="<%$Forditas:labelBeosztas|Beoszt�s:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerBeosztas" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerMinositesiSzint" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerMinositesiSzint" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerMinositesiSzint" runat="server" Text="<%$Forditas:labelMinositesiSzint|Min�s�t�si szint:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc7:KodtarakDropDownList ID="ddlKonszolidalandoPartnerMinositesiSzint" runat="server" CssClass="mrUrlapInputComboBox" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:Panel>
                                                        <!-- V�llalkoz�sok -->
                                                        <asp:Panel ID="panelKonszolidalandoPartnerVallalkozasok" runat="server">
                                                            <div class="mrUrlapSubTitleWrapper">
                                                                <span class="mrUrlapSubTitleLeftSide"></span><span class="mrUrlapSubTitle">
                                                                    <asp:Label ID="lblKonszolidalandoPartnerTovabbiSzervezet" runat="server" Text="Szervezet tov�bbi adatai"></asp:Label>
                                                                </span><span class="mrUrlapSubTitleRightSide"></span>
                                                            </div>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr class="urlapNyitoSor">
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;" style="padding-right: 232px">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td>
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;" style="padding-right: 230px">
                                                                            <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerAdoSzamVallalkozas" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerAdoSzamVallalkozas" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerAdoSzamVallalkozas" runat="server" Text="Ad�sz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc1:AdoszamTextBox ID="txtKonszolidalandoPartnerAdoSzamVallalkozas" runat="server" CssClass="mrUrlapInputSearch" KulfoldiAdoszamCheckBoxVisible="true" Validate="false" ValidateFormat="false" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerTBSzam" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerTBSzam" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerTBSzam" runat="server" Text="TB T�rzssz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerTBSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="urlapSor">
                                                                        <td id="tdKonszolidalandoPartnerCegJegyzekSzam" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerCegJegyzekSzam" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerCegJegyzekSzam" runat="server" Text="C�gjegyz�ksz�m:"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <asp:TextBox ID="txtKonszolidalandoPartnerCegJegyzekSzam" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdKonszolidalandoPartnerCegTipus" runat="server" class="mrUrlapCaption_short" style="text-align: left;">
                                                                            <asp:CheckBox ID="chbKonszolidalandoPartnerCegTipus" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                            <asp:Label ID="lblKonszolidalandoPartnerCegTipus" runat="server" Text="<%$Forditas:labelCegTipus|C�g t�pus:%>"></asp:Label>
                                                                        </td>
                                                                        <td class="mrUrlapMezo" style="text-align: left;">
                                                                            <uc7:KodtarakDropDownList ID="ddlKonszolidalandoPartnerCegTipus" runat="server" CssClass="mrUrlapInputSearchComboBox" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:Panel>
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr class="urlapNyitoSor">
                                                                    <td class="mrUrlapCaption">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trKonszolidalandoPartnerErvenyesseg" runat="server" class="urlapSor">
                                                                    <td class="mrUrlapCaption" style="padding-left: 40px;">
                                                                        <asp:CheckBox ID="chbKonszolidalandoPartnerErvenyesseg" runat="server" AutoPostBack="true" OnCheckedChanged="chbKonszolidalandoPartner_CheckedChanged" />
                                                                        <asp:Label ID="lblKonszolidalandoPartnerReqErvenyesseg" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                        <asp:Label ID="lblKonszolidalandoPartnerErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="text-align: left;">
                                                                        <uc6:ErvenyessegCalendarControl ID="calKonszolidalandoPartnerErvenyesseg" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </eUI:eFormPanel>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </eUI:eFormPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrids" cellpadding="5px" cellspacing="5px" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapMezo" style="text-align: left; vertical-align: top; width: 45%;">
                                            <asp:Panel ID="PartnerPostaiCimekPanel" runat="server" Visible="true" Width="100%">
                                                <uc15:SubListHeader ID="PartnerPostaiCimekSubListHeader" runat="server" />
                                                <asp:Panel ID="panelPostaiCimekList" runat="server" printable="true">
                                                    <asp:GridView ID="PartnerPostaiCimekGridView" Enabled="false" runat="server"
                                                        CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                        AllowPaging="false" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="PartnerPostaiCimekGridView_PreRender"
                                                        AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="PartnerPostaiCimekGridView_Sorting"
                                                        OnRowDataBound="PartnerPostaiCimekGridView_RowDataBound" OnRowCommand="PartnerPostaiCimekGridView_RowCommand">
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                        <Columns>
                                                            <asp:BoundField DataField="PartnerCimFajta_Nev" HeaderText="C�m&nbsp;fajt�ja" SortExpression="PartnerCimFajta_Nev">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TelepulesNev" HeaderText="Telep&#252;l&#233;s" SortExpression="TelepulesNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IRSZ" HeaderText="Ir&#225;ny&#237;t&#243;sz&#225;m" SortExpression="IRSZ">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="KozteruletNev" HeaderText="K&#246;zter&#252;let" SortExpression="KozteruletNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="KozteruletTipusNev" HeaderText="K&#246;zter&#252;let t&#237;pus" SortExpression="KozteruletNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkHazszam" runat="server" CommandName="sort" CommandArgument="Hazszam">H�zsz�m</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelHazszam" runat="server" Text='<%# Eval("Hazszam") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Lepcsohaz" HeaderText="L&#233;pcs�h&#225;z" SortExpression="Lepcsohaz">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Szint" HeaderText="Szint" SortExpression="Szint">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkAjto" runat="server" CommandName="sort" CommandArgument="Ajto">Ajt�</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelAjto" runat="server" Text='<%# Eval("Ajto") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--BLG_1347--%>
                                                            <asp:TemplateField HeaderText="Hrsz." SortExpression="Hrsz">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkHrsz" runat="server" CommandName="sort" CommandArgument="Hrsz">Hrsz</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelHrsz" runat="server" Text='<%# Eval("Hrsz") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="T�bbi c�m" SortExpression="CimTobbi">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkCimTobbi" runat="server" CommandName="sort" CommandArgument="CimTobbi">T�bbi c�m</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelCimTobbi" runat="server" Text='<%# Eval("CimTobbi") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerSettings Visible="False" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </td>
                                        <td class="mrUrlapMezo" style="text-align: left; vertical-align: top;">
                                            <asp:Panel ID="Panel1" runat="server" Visible="true" Width="100%">
                                                <uc15:SubListHeader ID="KonszolidalandoPartnerPostaiCimekSubListHeader" runat="server" />
                                                <asp:Panel ID="panel2" runat="server" printable="true">
                                                    <asp:GridView ID="KonszolidalandoPartnerPostaiCimekGridView" runat="server"
                                                        CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                        AllowPaging="false" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="KonszolidalandoPartnerPostaiCimekGridView_PreRender"
                                                        AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="KonszolidalandoPartnerPostaiCimekGridView_Sorting"
                                                        OnRowDataBound="KonszolidalandoPartnerPostaiCimekGridView_RowDataBound" OnRowCommand="KonszolidalandoPartnerPostaiCimekGridView_RowCommand">
                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:ImageButton ID="SelectingRowsImageButton"
                                                                        runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                    &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PartnerCimFajta_Nev" HeaderText="C�m&nbsp;fajt�ja" SortExpression="PartnerCimFajta_Nev">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TelepulesNev" HeaderText="Telep&#252;l&#233;s" SortExpression="TelepulesNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IRSZ" HeaderText="Ir&#225;ny&#237;t&#243;sz&#225;m" SortExpression="IRSZ">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="KozteruletNev" HeaderText="K&#246;zter&#252;let" SortExpression="KozteruletNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="KozteruletTipusNev" HeaderText="K&#246;zter&#252;let t&#237;pus" SortExpression="KozteruletNev">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkHazszam" runat="server" CommandName="sort" CommandArgument="Hazszam">H�zsz�m</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelHazszam" runat="server" Text='<%# Eval("Hazszam") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Lepcsohaz" HeaderText="L&#233;pcs�h&#225;z" SortExpression="Lepcsohaz">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Szint" HeaderText="Szint" SortExpression="Szint">
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkAjto" runat="server" CommandName="sort" CommandArgument="Ajto">Ajt�</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelAjto" runat="server" Text='<%# Eval("Ajto") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--BLG_1347--%>
                                                            <asp:TemplateField HeaderText="Hrsz." SortExpression="Hrsz">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkHrsz" runat="server" CommandName="sort" CommandArgument="Hrsz">Hrsz</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelHrsz" runat="server" Text='<%# Eval("Hrsz") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="T�bbi c�m" SortExpression="CimTobbi">
                                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="linkKonszolidalandoCimTobbi" runat="server" CommandName="sort" CommandArgument="CimTobbi">T�bbi c�m</asp:LinkButton>
                                                                </HeaderTemplate>
                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labelKonszolidalandoCimTobbi" runat="server" Text='<%# Eval("CimTobbi") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerSettings Visible="False" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapMezo" style="text-align: left; vertical-align: top;">
                                            <asp:Panel ID="PartnerKapcsolatokPanel" runat="server" Visible="true" Width="100%">
                                                <uc15:SubListHeader ID="PartnerKapcsolatokSubListHeader" runat="server" />
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="text-align: left; width: 45%; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                            <asp:Panel ID="panelPartnerKapcsolatokList" runat="server" printable="true">
                                                                <asp:GridView ID="PartnerKapcsolatokGridView" runat="server"
                                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                                    OnRowCommand="PartnerKapcsolatokGridView_RowCommand" DataKeyNames="Id" OnSorting="PartnerKapcsolatokGridView_Sorting">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="TipusNev" HeaderText="Kapcsolat t�pusa" SortExpression="TipusNev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Partner neve" SortExpression="Partner_kapcsolt_Nev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Partner_kapcsolt_TipusNev" HeaderText="Partner t�pusa" SortExpression="Partner_kapcsolt_TipusNev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                        <td class="mrUrlapMezo" style="text-align: left; vertical-align: top;">
                                            <asp:Panel ID="KonszolidalandoPartnerKapcsolatokPanel" runat="server" Visible="true" Width="100%">
                                                <uc15:SubListHeader ID="KonszolidalandoPartnerKapcsolatokSubListHeader" runat="server" />
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                            <asp:Panel ID="panelKonszolidalandoPartnerKapcsolatokList" runat="server" printable="true">
                                                                <asp:GridView ID="KonszolidalandoPartnerKapcsolatokGridView" runat="server"
                                                                    CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                    GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                                                    OnRowCommand="KonszolidalandoPartnerKapcsolatokGridView_RowCommand" DataKeyNames="Id" OnSorting="KonszolidalandoPartnerKapcsolatokGridView_Sorting">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                            <HeaderTemplate>
                                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Partner_kapcsolt_Nev") %>' CssClass="HideCheckBoxText" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="TipusNev" HeaderText="Kapcsolat t�pusa" SortExpression="TipusNev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Partner_kapcsolt_Nev" HeaderText="Partner neve" SortExpression="Partner_kapcsolt_Nev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Partner_kapcsolt_TipusNev" HeaderText="Partner t�pusa" SortExpression="Partner_kapcsolt_TipusNev">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>

            </ContentTemplate>
        </asp:UpdatePanel>
         <!--Friss�t�s jelz�se-->
    <uc33:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    </asp:Panel>
</asp:Content>



