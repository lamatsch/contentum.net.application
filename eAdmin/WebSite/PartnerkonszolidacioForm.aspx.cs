﻿using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eQuery;
using System.Linq;
using System.Collections.Generic;

public partial class PartnerkonszolidacioForm : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    KRT_Partnerek selectedPartnerObj;
    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");

        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, No.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }
    public static class Sex
    {
        public static readonly ListItem Male = new ListItem("Férfi", "1");
        public static readonly ListItem Female = new ListItem("Nõ", "2");
        public static readonly ListItem NotSet = new ListItem("Nincs megadva", Constants.BusinessDocument.nullString);
        public static void FillRadioList(RadioButtonList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Male);
            list.Items.Add(Female);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillRadioList(RadioButtonList list)
        {
            FillRadioList(list, NotSet.Value);
        }
        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            if (selectedValue == Male.Value || selectedValue == Female.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                foreach (ListItem item in list.Items)
                {
                    item.Selected = false;
                }
            }
        }
        public static string GetSelectedValue(RadioButtonList list)
        {
            if (list.SelectedValue == Male.Value || list.SelectedValue == Female.Value)
            {
                return list.SelectedValue;
            }
            else
            {
                return NotSet.Value;
            }
        }
    }


    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private const string kcs_PARTNER_ALLAPOT = "PARTNER_ALLAPOT";
    private const string kcs_PARTNER_TIPUS = "PARTNER_TIPUS";
    private const string kcs_CEGTIPUS = "CEGTIPUS";
    private const string kcs_IRAT_MINOSITES = "IRAT_MINOSITES";

    private void SetViewControls()
    {
        PartnerTextBoxPartner.ReadOnly = true;
        PartnerTextBoxKonszolidaltPartner.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelKonszolidaltPartner.CssClass = "mrUrlapInputWaterMarked";
        EErrorPanel1.Visible = false;
    }

    //LZS - BLG_11828
    private void SetConsolidateControlsReadOnly(string partnerTipus, bool viewOnly)
    {
        //Partner panel disabled:
        EFormPanelPartner.Enabled = false;
        //Konszolidálandó partner disabled:
        SetConsolidatePartnerControlsReadOnly();

        //Checkboxes enabled/disabled
        chbKonszolidalandoPartnerTipus.Enabled =
        chbKonszolidalandoPartnerCegTipus.Enabled =
        chbKonszolidalandoPartnerTBSzam.Enabled =
        chbKonszolidalandoPartnerCegJegyzekSzam.Enabled =
        chbKonszolidalandoPartnerAdoAzonosito.Enabled =
        chbKonszolidalandoPartnerAdoSzamSzemely.Enabled =
        chbKonszolidalandoPartnerAdoSzamVallalkozas.Enabled =
        chbKonszolidalandoPartnerAllapot.Enabled =
        chbKonszolidalandoPartnerAnyjaCsaladiNeve.Enabled =
        chbKonszolidalandoPartnerAnyjaTovabbiUtoNev.Enabled =
        chbKonszolidalandoPartnerAnyjaUtoNev.Enabled =
        chbKonszolidalandoPartnerBelso.Enabled =
        chbKonszolidalandoPartnerCsaladiNev.Enabled =
        chbKonszolidalandoPartnerErvenyesseg.Enabled =
        chbKonszolidalandoPartnerKulsoAzonosito.Enabled =
        chbKonszolidalandoPartnerMinositesiSzint.Enabled =
        chbKonszolidalandoPartnerNeme.Enabled =
        chbKonszolidalandoPartnerNev.Enabled =
        chbKonszolidalandoPartnerOrszag.Enabled =
        chbKonszolidalandoPartnerSzemelyi.Enabled =
        chbKonszolidalandoPartnerSzemelyiAzonosito.Enabled =
        chbKonszolidalandoPartnerSzuletesiCsaladiNev.Enabled =
        chbKonszolidalandoPartnerSzuletesiHely.Enabled =
        chbKonszolidalandoPartnerSzuletesiIdo.Enabled =
        chbKonszolidalandoPartnerSzuletesiOrszag.Enabled =
        chbKonszolidalandoPartnerSzuletesiTovabbiUtoneNev.Enabled =
        chbKonszolidalandoPartnerSzuletesiUtoneNev.Enabled =
        chbKonszolidalandoPartnerTajSzam.Enabled =
        chbKonszolidalandoPartnerUtoNev.Enabled =
         PartnerTextBoxKonszolidaltPartner.Enabled = !viewOnly;

        //LZS - BLG_11828
        //Csak azt a panelt mutatjuk, ami az adott partnertípushoz tartozik:
        switch (partnerTipus)
        {
            case KodTarak.Partner_Tipus.Szemely:
                panelPartnerSzemelyek.Visible = true;
                panelKonszolidalandoPartnerSzemelyek.Visible = true;
                panelPartnerVallalkozasok.Visible = false;
                panelKonszolidalandoPartnerVallalkozasok.Visible = false;

                PartnerTextBoxKonszolidaltPartner.SetFilterType_Szemely();
                break;

            case KodTarak.Partner_Tipus.Szervezet:
                panelPartnerSzemelyek.Visible = false;
                panelKonszolidalandoPartnerSzemelyek.Visible = false;
                panelPartnerVallalkozasok.Visible = true;
                panelKonszolidalandoPartnerVallalkozasok.Visible = true;

                PartnerTextBoxKonszolidaltPartner.SetFilterType_Szervezet();
                break;

        }


    }

    private void SetConsolidatePartnerControlsReadOnly()
    {
        lblKonszolidalandoPartnerReqTipus.Enabled = false;
        lblKonszolidalandoPartnerTipus.Enabled = false;
        ddlKonszolidalandoPartnerTipus.Enabled = false;
        lblKonszolidalandoPartnerReqNev.Enabled = false;
        lblKonszolidalandoPartnerNev.Enabled = false;
        txtKonszolidalandoPartnerNev.Enabled = false;
        lblKonszolidalandoPartnerOrszag.Enabled = false;
        txtKonszolidalandoPartnerOrszagok.Enabled = false;
        lblReqKonszolidalandoPartnerBelso.Enabled = false;
        lblKonszolidalandoPartnerBelso.Enabled = false;
        ddlKonszolidalandoPartnerBelso.ReadOnly = true;
        ddlKonszolidalandoPartnerBelso.Enabled = false;
        lblKonszolidalandoPartnerAllapot.Enabled = false;
        ddlKonszolidalandoPartnerAllapot.Enabled = false;
        lblKonszolidalandoPartnerKulsoAzonosito.Enabled = false;
        txtKonszolidalandoPartnerKulsoAzonosito.Enabled = false;
        lblKonszolidalandoPartnerTovabbiSzemely.Enabled = false;
        lblKonszolidalandoPartnerTitulus.Enabled = false;
        txtKonszolidalandoPartnerTitulus.Enabled = false;
        lblKonszolidalandoPartnerReqCsaladiNev.Enabled = false;
        lblKonszolidalandoPartnerCsaladiNev.Enabled = false;
        txtKonszolidalandoPartnerCsaladiNev.Enabled = false;
        lblKonszolidalandoPartnerReqUtonev.Enabled = false;
        lblKonszolidalandoPartnerUtoNev.Enabled = false;
        txtKonszolidalandoPartnerUtoNev.Enabled = false;
        lblKonszolidalandoPartnerTovabbiUtonev.Enabled = false;
        txtKonszolidalandoPartnerTovabbiUtonev.Enabled = false;
        lblKonszolidalandoPartnerAnyjaCsaladiNeve.Enabled = false;
        txtKonszolidalandoPartnerAnyjaCsaladiNeve.Enabled = false;
        lblKonszolidalandoPartnerAnyjaUtoNev.Enabled = false;
        txtKonszolidalandoPartnerAnyjaUtoNev.Enabled = false;
        lblKonszolidalandoPartnerAnyjaTovabbiUtoNev.Enabled = false;
        txtKonszolidalandoPartnerAnyjaTovabbiUtoNev.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiCsaladiNev.Enabled = false;
        txtKonszolidalandoPartnerSzuletesiCsaladiNev.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiUtoneNev.Enabled = false;
        txtKonszolidalandoPartnerSzuletesiUtoNev.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiTovabbiUtoneNev.Enabled = false;
        txtKonszolidalandoPartnerSzuletesiTovabbiUtoNev.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiIdo.Enabled = false;
        calKonszolidalandoPartnerSzuletesiIdo.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiOrszag.Enabled = false;
        txtKonszolidalandoPartnerSzuletesiOrszag.Enabled = false;
        lblKonszolidalandoPartnerSzuletesiHely.Enabled = false;
        txtKonszolidalandoPartnerSzuletesiHely.Enabled = false;
        lblKonszolidalandoPartnerTajSzam.Enabled = false;
        txtKonszolidalandoPartnerTajSzam.Enabled = false;
        lblKonszolidalandoPartnerSzemelyi.Enabled = false;
        txtKonszolidalandoPartnerSzigSzam.Enabled = false;
        lblKonszolidalandoPartnerNeme.Enabled = false;
        rblKonszolidalandoPartnerNeme.Enabled = false;
        lblKonszolidalandoPartnerSzemelyiAzonosito.Enabled = false;
        txtKonszolidalandoPartnerSzemelyiAzonosito.Enabled = false;
        lblKonszolidalandoPartnerAdoAzonosito.Enabled = false;
        txtKonszolidalandoPartnerAdoAzonosito.Enabled = false;
        lblKonszolidalandoPartnerAdoSzamSzemely.Enabled = false;
        txtKonszolidalandoPartnerAdoszamSzemely.Enabled = false;
        lblKonszolidalandoPartnerBeosztas.Enabled = false;
        txtKonszolidalandoPartnerBeosztas.Enabled = false;
        lblKonszolidalandoPartnerMinositesiSzint.Enabled = false;
        ddlKonszolidalandoPartnerMinositesiSzint.Enabled = false;
        lblKonszolidalandoPartnerTovabbiSzervezet.Enabled = false;
        lblKonszolidalandoPartnerAdoSzamVallalkozas.Enabled = false;
        txtKonszolidalandoPartnerAdoSzamVallalkozas.Enabled = false;
        lblKonszolidalandoPartnerTBSzam.Enabled = false;
        txtKonszolidalandoPartnerTBSzam.Enabled = false;
        lblKonszolidalandoPartnerCegJegyzekSzam.Enabled = false;
        txtKonszolidalandoPartnerCegJegyzekSzam.Enabled = false;
        lblKonszolidalandoPartnerCegTipus.Enabled = false;
        ddlKonszolidalandoPartnerCegTipus.Enabled = false;
        lblKonszolidalandoPartnerReqErvenyesseg.Enabled = false;
        lblKonszolidalandoPartnerErvenyesseg.Enabled = false;
        calKonszolidalandoPartnerErvenyesseg.Enabled = false;

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
        KRT_Partnerek krt_Partnerek = null;

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerKonszolidacio" + Command);
                break;
        }

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_PartnerKapcsolatokService service = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_PartnerKapcsolatok KRT_PartnerKapcsolatok = (KRT_PartnerKapcsolatok)result.Record;
                    LoadComponentsFromBusinessObject(KRT_PartnerKapcsolatok);

                    KRT_PartnerekService partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                    ExecParam execParamPartnerek = UI.SetExecParamDefault(Page, new ExecParam());
                    execParamPartnerek.Record_Id = KRT_PartnerKapcsolatok.Partner_id;

                    Result resultPartnerek = service.Get(execParamPartnerek);
                    if (String.IsNullOrEmpty(resultPartnerek.ErrorCode))
                    {
                        krt_Partnerek = (KRT_Partnerek)resultPartnerek.Record;
                        LoadComponentsFromBusinessObject(krt_Partnerek);
                    }

                    SetForm(KRT_PartnerKapcsolatok.Partner_id, krt_Partnerek, true);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }



            }
        }
        else if (Command == CommandName.New)
        {
            SetForm(partnerId, krt_Partnerek, false);
        }

        //if (Command == CommandName.View)
        //{
        //    SetViewControls();
        //}


    }

    private void SetForm(string partnerId, KRT_Partnerek krt_Partnerek, bool viewOnly)
    {
        if (!String.IsNullOrEmpty(partnerId))
        {
            PartnerTextBoxPartner.Id_HiddenField = partnerId;
            PartnerTextBoxPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
            PartnerTextBoxPartner.ReadOnly = true;
            labelPartner.CssClass = "mrUrlapInputWaterMarked";
        }

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = partnerId;

        Result result = service.Get(execParam);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            krt_Partnerek = (KRT_Partnerek)result.Record;
            LoadComponentsFromBusinessObject(krt_Partnerek);
        }

        //LZS - BLG_11828
        //CommandName.New, CommandName.View és CommandName.Modify módban is:
        //Partner és konszolidálandó partner adatinak megjelenítése readonlyban
        SetConsolidateControlsReadOnly(krt_Partnerek.Tipus, viewOnly);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);


        registerJavascripts();

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (!IsPostBack)
        {
            string jsConfirm = "if(!confirm('FIGYELEM!!! \\nA konszolidálandó partner összes küldeménye és iratpéldánya a kijelölt partnerhez lesz rendelve!\\nA konszolidálandó partner érvénytelenítve lesz.\\nA művelet visszavonása nem lehetséges!\\nBiztosan végre kívánja hajtani az összevonást?')) return false;";
            FormFooter1.ImageButton_SaveAndClose.Visible = true;
            FormFooter1.ImageButton_SaveAndClose.OnClientClick = jsConfirm;
            FormFooter1.ImageButton_Save.Visible = false;
            FormFooter1.ImageButton_Save.Enabled = false;


            PartnerPostaiCimekSubListHeader.NewVisible = false;
            PartnerPostaiCimekSubListHeader.ModifyVisible = false;
            PartnerPostaiCimekSubListHeader.InvalidateVisible = false;
            PartnerPostaiCimekSubListHeader.ViewVisible = false;
            PartnerPostaiCimekSubListHeader.RightButtonsVisible = false;
            PartnerPostaiCimekSubListHeader.PageCount = 100;

            KonszolidalandoPartnerPostaiCimekSubListHeader.NewVisible = false;
            KonszolidalandoPartnerPostaiCimekSubListHeader.ModifyVisible = false;
            KonszolidalandoPartnerPostaiCimekSubListHeader.InvalidateVisible = false;
            KonszolidalandoPartnerPostaiCimekSubListHeader.ViewVisible = false;
            KonszolidalandoPartnerPostaiCimekSubListHeader.RightButtonsVisible = false;
            KonszolidalandoPartnerPostaiCimekSubListHeader.PageCount = 100;

            PartnerKapcsolatokSubListHeader.NewVisible = false;
            PartnerKapcsolatokSubListHeader.ModifyVisible = false;
            PartnerKapcsolatokSubListHeader.InvalidateVisible = false;
            PartnerKapcsolatokSubListHeader.ViewVisible = false;
            PartnerKapcsolatokSubListHeader.RightButtonsVisible = false;
            PartnerKapcsolatokSubListHeader.PageCount = 100;

            KonszolidalandoPartnerKapcsolatokSubListHeader.NewVisible = false;
            KonszolidalandoPartnerKapcsolatokSubListHeader.ModifyVisible = false;
            KonszolidalandoPartnerKapcsolatokSubListHeader.InvalidateVisible = false;
            KonszolidalandoPartnerKapcsolatokSubListHeader.ViewVisible = false;
            KonszolidalandoPartnerKapcsolatokSubListHeader.RightButtonsVisible = false;
            KonszolidalandoPartnerKapcsolatokSubListHeader.PageCount = 100;


            string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
            PartnerPostaiCimekGridViewBind(partnerId);
            PartnerKapcsolatokGridViewBind(partnerId);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Valamiért muszáj volt itt is levenni.
        FormFooter1.ImageButton_Save.Visible = false;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_PartnerKapcsolatok krt_partnerKapcsolatok)
    {
        PartnerTextBoxPartner.Id_HiddenField = krt_partnerKapcsolatok.Partner_id;
        PartnerTextBoxPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        PartnerTextBoxKonszolidaltPartner.Id_HiddenField = krt_partnerKapcsolatok.Partner_id_kapcsolt;
        PartnerTextBoxKonszolidaltPartner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        FormHeader1.Record_Ver = krt_partnerKapcsolatok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_partnerKapcsolatok.Base);
    }

    // form --> business object
    private KRT_PartnerKapcsolatok GetBusinessObjectFromComponents()
    {
        KRT_PartnerKapcsolatok krt_partnerKapcsolatok = new KRT_PartnerKapcsolatok();
        krt_partnerKapcsolatok.Updated.SetValueAll(false);
        krt_partnerKapcsolatok.Base.Updated.SetValueAll(false);

        krt_partnerKapcsolatok.Partner_id = PartnerTextBoxPartner.Id_HiddenField;
        krt_partnerKapcsolatok.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBoxPartner);

        krt_partnerKapcsolatok.Partner_id_kapcsolt = PartnerTextBoxKonszolidaltPartner.Id_HiddenField;
        krt_partnerKapcsolatok.Updated.Partner_id_kapcsolt = pageView.GetUpdatedByView(PartnerTextBoxKonszolidaltPartner);

        krt_partnerKapcsolatok.Tipus = KodTarak.PartnerKapcsolatTipus.konszolidalt;
        krt_partnerKapcsolatok.Updated.Tipus = true;

        krt_partnerKapcsolatok.Base.Ver = FormHeader1.Record_Ver;
        krt_partnerKapcsolatok.Base.Updated.Ver = true;

        return krt_partnerKapcsolatok;
    }

    protected virtual void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(PartnerTextBoxPartner);
            compSelector.Add_ComponentOnClick(PartnerTextBoxKonszolidaltPartner);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if (FunctionRights.GetFunkcioJog(Page, "PartnerKonszolidacio" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            rootPanel.Enabled = false;

                            if (PartnerTextBoxKonszolidaltPartner.Id_HiddenField != betoltveHiddenField.Value)
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alert", "alert('A kiválasztott konszolidálandó partner nem lett betöltve!');", true);
                                return;
                            }

                            //Id- eltárolása
                            string vallalkozasId = "";
                            string szemelyId = "";
                            KRT_PartnerKapcsolatok krt_partnerkapcsolatok = GetBusinessObjectFromComponents();
                            KRT_Partnerek krt_partner = new KRT_Partnerek();
                            krt_partner.Id = krt_partnerkapcsolatok.Partner_id;
                            krt_partner.ErvKezd = calPartnerErvenyesseg.ErvKezd;
                            krt_partner.ErvVege = calPartnerErvenyesseg.ErvVege;
                            krt_partner.Org = FelhasznaloProfil.OrgId(this.Session);

                            //Kijelölt konszolidalando partneradatok lekérése
                            KRT_Partnerek ConsolidatedPartner = GetSelectedKonszolidalando_PartnerAdatok(krt_partnerkapcsolatok.Partner_id_kapcsolt);

                            //Személy esetén
                            KRT_Szemelyek krt_szemely = null;
                            if (panelPartnerSzemelyek.Visible)
                            {
                                krt_partner.Tipus = KodTarak.Partner_Tipus.Szemely;

                                //KRT_SzemelyekService
                                Contentum.eAdmin.Service.KRT_SzemelyekService szemelyekService = eAdminService.ServiceFactory.GetKRT_SzemelyekService();

                                ExecParam execParamSzemely = UI.SetExecParamDefault(Page, new ExecParam());

                                KRT_SzemelyekSearch szemelyekSearch = new KRT_SzemelyekSearch();
                                szemelyekSearch.Partner_Id.Value = krt_partner.Id;
                                szemelyekSearch.Partner_Id.Operator = Query.Operators.equals;

                                Result resultSzemelyek = szemelyekService.GetAll(execParamSzemely, szemelyekSearch);

                                if (!resultSzemelyek.IsError && resultSzemelyek.Ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow row in resultSzemelyek.Ds.Tables[0].Rows)
                                    {
                                        szemelyId = (row["Id"].ToString());
                                    }
                                }

                                krt_szemely = GetSelectedKonszolidalando_SzemelyAdatok(krt_partner.Id);
                                krt_szemely.Id = szemelyId;
                                krt_szemely.Partner_Id = krt_szemely.Partner_Id;
                            }

                            //Szervezet esetén
                            KRT_Vallalkozasok krt_vallalkozas = null;
                            if (panelPartnerVallalkozasok.Visible)
                            {
                                //KRT_VallalkozasokService
                                Contentum.eAdmin.Service.KRT_VallalkozasokService vallalkozasokService = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();

                                krt_partner.Tipus = KodTarak.Partner_Tipus.Szervezet;

                                ExecParam execParamVallalkozas = UI.SetExecParamDefault(Page, new ExecParam());

                                KRT_VallalkozasokSearch vallalkozasSearch = new KRT_VallalkozasokSearch();
                                vallalkozasSearch.Partner_Id.Value = krt_partner.Id;
                                vallalkozasSearch.Partner_Id.Operator = Query.Operators.equals;

                                Result resultVallalkozasok = vallalkozasokService.GetAll(execParamVallalkozas, vallalkozasSearch);

                                if (!resultVallalkozasok.IsError && resultVallalkozasok.Ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow row in resultVallalkozasok.Ds.Tables[0].Rows)
                                    {
                                        vallalkozasId = (row["Id"].ToString());
                                    }
                                }

                                krt_vallalkozas = GetSelectedKonszolidalando_SzervezetAdatok(krt_partner.Id);
                                krt_vallalkozas.Id = vallalkozasId;
                                krt_vallalkozas.Partner_Id = krt_partner.Id;
                            }

                            //Kijelölt konszolidalando partnercímek lekérése
                            List<string> selectedKonszolidalandoPartnerCimIds = ui.GetGridViewSelectedRows(KonszolidalandoPartnerPostaiCimekGridView, EErrorPanel1, null);
                            //Kijelölt konszolidalando partnerkapcsolatok lekérése
                            List<string> selectedKonszolidalandoPartnerKapcsolatIds = ui.GetGridViewSelectedRows(KonszolidalandoPartnerKapcsolatokGridView, EErrorPanel1, null);

                            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Consolidate(execParam, krt_partner, selectedKonszolidalandoPartnerCimIds, selectedKonszolidalandoPartnerKapcsolatIds, krt_szemely, krt_vallalkozas, ConsolidatedPartner);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                //visszaküldés a hívó form-nak 
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                            }
                            else
                            {
                                rootPanel.Enabled = true;
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                FormHeader1.ErrorPanel.Visible = true;
                            }
                            break;


                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private KRT_Partnerek GetSelectedKonszolidalando_PartnerAdatok(string kapcsolt_Id)
    {
        KRT_Partnerek ConsolidatedPartner = new KRT_Partnerek();
        ConsolidatedPartner.Updated.SetValueAll(false);

        ConsolidatedPartner.Id = kapcsolt_Id;
        ConsolidatedPartner.Updated.Id = true;

        #region Általános partner adatok
        //Típus
        if (chbKonszolidalandoPartnerTipus.Checked)
        {
            ConsolidatedPartner.Tipus = ddlKonszolidalandoPartnerTipus.SelectedValue;
            ConsolidatedPartner.Updated.Tipus = true;
        }

        //Név marad az eredeti partner neve kell
        ConsolidatedPartner.Nev = txtPartnerNev.Text;
        ConsolidatedPartner.Updated.Nev = true;


        //Ország:
        if (chbKonszolidalandoPartnerOrszag.Checked)
        {
            ConsolidatedPartner.Orszag_Id = txtKonszolidalandoPartnerOrszagok.Id_HiddenField;
            ConsolidatedPartner.Updated.Orszag_Id = true;
        }

        //Belső:
        if (chbKonszolidalandoPartnerBelso.Checked)
        {
            ConsolidatedPartner.Belso = ddlKonszolidalandoPartnerBelso.SelectedValue;
            ConsolidatedPartner.Updated.Belso = true;
        }

        //Állapot:
        if (chbKonszolidalandoPartnerAllapot.Checked)
        {
            ConsolidatedPartner.Allapot = ddlKonszolidalandoPartnerAllapot.SelectedValue;
            ConsolidatedPartner.Updated.Allapot = true;
        }

        //Külső azonosító:
        if (chbKonszolidalandoPartnerKulsoAzonosito.Checked)
        {
            ConsolidatedPartner.KulsoAzonositok = txtKonszolidalandoPartnerKulsoAzonosito.Text;
            ConsolidatedPartner.Updated.KulsoAzonositok = true;
        }

        if (chbKonszolidalandoPartnerErvenyesseg.Checked)
        {
            ConsolidatedPartner.ErvKezd = calKonszolidalandoPartnerErvenyesseg.ErvKezd;
            ConsolidatedPartner.Updated.ErvKezd = true;

            ConsolidatedPartner.ErvVege = calKonszolidalandoPartnerErvenyesseg.ErvVege;
            ConsolidatedPartner.Updated.ErvVege = true;
        }

        #endregion

        return ConsolidatedPartner;
    }

    private KRT_Szemelyek GetSelectedKonszolidalando_SzemelyAdatok(string partnerId)
    {
        KRT_Szemelyek ConsolidatedSzemely = new KRT_Szemelyek();
        ConsolidatedSzemely.Updated.SetValueAll(false);

        #region Személy további adatok
        //Titulus:
        if (chbKonszolidalandoPartnerTitulus.Checked)
        {
            ConsolidatedSzemely.UjTitulis = txtKonszolidalandoPartnerTitulus.Text;
            ConsolidatedSzemely.Updated.UjTitulis = true;
        }

        //Családi név:
        if (chbKonszolidalandoPartnerCsaladiNev.Checked)
        {
            ConsolidatedSzemely.UjCsaladiNev = txtKonszolidalandoPartnerCsaladiNev.Text;
            ConsolidatedSzemely.Updated.UjCsaladiNev = true;
        }

        //Utónév:
        if (chbKonszolidalandoPartnerUtoNev.Checked)
        {
            ConsolidatedSzemely.UjUtonev = txtKonszolidalandoPartnerUtoNev.Text;
            ConsolidatedSzemely.Updated.UjUtonev = true;
        }

        //További utónevek:
        if (chbKonszolidalandoPartnerTovabbiUtonev.Checked)
        {
            ConsolidatedSzemely.UjTovabbiUtonev = txtKonszolidalandoPartnerTovabbiUtonev.Text;
            ConsolidatedSzemely.Updated.UjTovabbiUtonev = true;
        }

        //Anyja családi neve:
        if (chbKonszolidalandoPartnerAnyjaCsaladiNeve.Checked)
        {
            ConsolidatedSzemely.AnyjaNeveCsaladiNev = txtKonszolidalandoPartnerAnyjaCsaladiNeve.Text;
            ConsolidatedSzemely.Updated.AnyjaNeveCsaladiNev = true;
        }

        //Anyja utóneve:
        if (chbKonszolidalandoPartnerAnyjaUtoNev.Checked)
        {
            ConsolidatedSzemely.AnyjaNeveElsoUtonev = txtKonszolidalandoPartnerAnyjaUtoNev.Text;
            ConsolidatedSzemely.Updated.AnyjaNeveElsoUtonev = true;
        }

        //Anyja további utónevei:
        if (chbKonszolidalandoPartnerAnyjaTovabbiUtoNev.Checked)
        {
            ConsolidatedSzemely.AnyjaNeveTovabbiUtonev = txtKonszolidalandoPartnerAnyjaTovabbiUtoNev.Text;
            ConsolidatedSzemely.Updated.AnyjaNeveTovabbiUtonev = true;
        }
        //Születési családi név:
        if (chbKonszolidalandoPartnerSzuletesiCsaladiNev.Checked)
        {
            ConsolidatedSzemely.SzuletesiCsaladiNev = txtKonszolidalandoPartnerSzuletesiCsaladiNev.Text;
            ConsolidatedSzemely.Updated.SzuletesiCsaladiNev = true;
        }

        //Születési útónév:
        if (chbKonszolidalandoPartnerSzuletesiUtoneNev.Checked)
        {
            ConsolidatedSzemely.SzuletesiElsoUtonev = txtKonszolidalandoPartnerSzuletesiUtoNev.Text;
            ConsolidatedSzemely.Updated.SzuletesiElsoUtonev = true;
        }

        //Születési további utónév:
        if (chbKonszolidalandoPartnerSzuletesiTovabbiUtoneNev.Checked)
        {
            ConsolidatedSzemely.SzuletesiTovabbiUtonev = txtKonszolidalandoPartnerSzuletesiTovabbiUtoNev.Text;
            ConsolidatedSzemely.Updated.SzuletesiTovabbiUtonev = true;
        }

        //Születési idő:
        if (chbKonszolidalandoPartnerSzuletesiIdo.Checked)
        {
            ConsolidatedSzemely.SzuletesiIdo = calKonszolidalandoPartnerSzuletesiIdo.Text;
            ConsolidatedSzemely.Updated.SzuletesiIdo = true;
        }

        //Születési ország:
        if (chbKonszolidalandoPartnerSzuletesiOrszag.Checked)
        {
            ConsolidatedSzemely.SzuletesiOrszag = txtKonszolidalandoPartnerSzuletesiOrszag.Text;
            ConsolidatedSzemely.Updated.SzuletesiOrszag = true;

            ConsolidatedSzemely.SzuletesiOrszagId = txtKonszolidalandoPartnerSzuletesiOrszag.Id_HiddenField;
            ConsolidatedSzemely.Updated.SzuletesiOrszagId = true;
        }

        //Születési hely:
        if (chbKonszolidalandoPartnerSzuletesiHely.Checked)
        {
            ConsolidatedSzemely.SzuletesiHely = txtKonszolidalandoPartnerSzuletesiHely.Text;
            ConsolidatedSzemely.Updated.SzuletesiHely = true;

            ConsolidatedSzemely.SzuletesiHely_id = txtKonszolidalandoPartnerSzuletesiHely.Id_HiddenField;
            ConsolidatedSzemely.Updated.SzuletesiHely_id = true;
        }

        //TAJ szám:
        if (chbKonszolidalandoPartnerTajSzam.Checked)
        {
            ConsolidatedSzemely.TAJSzam = txtKonszolidalandoPartnerTajSzam.Text;
            ConsolidatedSzemely.Updated.TAJSzam = true;
        }

        //Szem. ig. szám:
        if (chbKonszolidalandoPartnerSzemelyi.Checked)
        {
            ConsolidatedSzemely.SZIGSzam = txtKonszolidalandoPartnerSzigSzam.Text;
            ConsolidatedSzemely.Updated.SZIGSzam = true;
        }

        //Neme:
        if (chbKonszolidalandoPartnerNeme.Checked)
        {
            ConsolidatedSzemely.Neme = rblKonszolidalandoPartnerNeme.SelectedValue;
            ConsolidatedSzemely.Updated.Neme = true;
        }

        //Szememélyi azonosító:
        if (chbKonszolidalandoPartnerSzemelyiAzonosito.Checked)
        {
            ConsolidatedSzemely.SzemelyiAzonosito = txtKonszolidalandoPartnerSzemelyiAzonosito.Text;
            ConsolidatedSzemely.Updated.SzemelyiAzonosito = true;
        }

        //Adóazonosító:
        if (chbKonszolidalandoPartnerAdoAzonosito.Checked)
        {
            ConsolidatedSzemely.Adoazonosito = txtKonszolidalandoPartnerAdoAzonosito.Text;
            ConsolidatedSzemely.Updated.Adoazonosito = true;
        }

        //Adószám:
        if (chbKonszolidalandoPartnerAdoSzamSzemely.Checked)
        {
            ConsolidatedSzemely.Adoszam = txtKonszolidalandoPartnerAdoszamSzemely.Text;
            ConsolidatedSzemely.Updated.Adoszam = true;
        }

        //Beosztás:
        if (chbKonszolidalandoPartnerBeosztas.Checked)
        {
            ConsolidatedSzemely.Beosztas = txtKonszolidalandoPartnerBeosztas.Text;
            ConsolidatedSzemely.Updated.Beosztas = true;
        }

        //Minősítési szint:
        if (chbKonszolidalandoPartnerMinositesiSzint.Checked)
        {
            ConsolidatedSzemely.MinositesiSzint = ddlKonszolidalandoPartnerMinositesiSzint.SelectedValue;
            ConsolidatedSzemely.Updated.MinositesiSzint = true;
        }

        #endregion

        return ConsolidatedSzemely;
    }

    private KRT_Vallalkozasok GetSelectedKonszolidalando_SzervezetAdatok(string partnerId)
    {
        KRT_Vallalkozasok ConsolidatedSzervezet = new KRT_Vallalkozasok();
        ConsolidatedSzervezet.Updated.SetValueAll(false);

        #region Szervezet további adatok
        //Adószám:
        if (chbKonszolidalandoPartnerAdoSzamVallalkozas.Checked)
        {
            ConsolidatedSzervezet.Adoszam = txtKonszolidalandoPartnerAdoSzamVallalkozas.Text;
            ConsolidatedSzervezet.Updated.Adoszam = true;
        }

        //TB Törzsszám:
        if (chbKonszolidalandoPartnerTBSzam.Checked)
        {
            ConsolidatedSzervezet.TB_Torzsszam = txtKonszolidalandoPartnerTBSzam.Text;
            ConsolidatedSzervezet.Updated.TB_Torzsszam = true;
        }

        //Cégjegyzékszám:
        if (chbKonszolidalandoPartnerCegJegyzekSzam.Checked)
        {
            ConsolidatedSzervezet.Cegjegyzekszam = txtKonszolidalandoPartnerCegJegyzekSzam.Text;
            ConsolidatedSzervezet.Updated.Cegjegyzekszam = true;
        }

        //Cégjegyzékszám:
        if (chbKonszolidalandoPartnerCegTipus.Checked)
        {
            ConsolidatedSzervezet.Tipus = ddlKonszolidalandoPartnerCegTipus.SelectedValue;
            ConsolidatedSzervezet.Updated.Tipus = true;
        }
        #endregion

        return ConsolidatedSzervezet;
    }

    private void LoadComponentsFromBusinessObject(KRT_Partnerek krt_Partnerek)
    {
        txtPartnerNev.Text = krt_Partnerek.Nev;
        ddlPartnerTipus.FillAndSetSelectedValue(kcs_PARTNER_TIPUS, krt_Partnerek.Tipus, FormHeader1.ErrorPanel);
        txtPartnerOrszagok.Id_HiddenField = krt_Partnerek.Orszag_Id;
        txtPartnerOrszagok.SetTextBoxById(FormHeader1.ErrorPanel);
        BoolString.FillDropDownList(ddlPartnerBelso, krt_Partnerek.Belso);
        calPartnerErvenyesseg.ErvKezd = krt_Partnerek.ErvKezd;
        calPartnerErvenyesseg.ErvVege = krt_Partnerek.ErvVege;
        txtPartnerKulsoAzonosito.Text = krt_Partnerek.KulsoAzonositok;


        //LZS - BLG 1915
        if (!string.IsNullOrEmpty(krt_Partnerek.Allapot))
        {
            ddlPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, krt_Partnerek.Allapot, FormHeader1.ErrorPanel);
        }
        else
        {
            ddlPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, KodTarak.Partner_Allapot.Jovahagyando, FormHeader1.ErrorPanel);
        }


        if (!FunctionRights.GetFunkcioJog(Page, "PartnerApproval"))
        {
            ddlPartnerAllapot.Enabled = false;
        }
        else
        {
            ddlPartnerAllapot.Enabled = true;
        }

        switch (krt_Partnerek.Tipus)
        {
            case KodTarak.Partner_Tipus.Alkalmazas:
                {
                    break;
                }
            case KodTarak.Partner_Tipus.Szemely:
                {
                    if (krt_Partnerek is KRT_PartnerSzemelyek)
                    {
                        KRT_Szemelyek krt_Szemelyek = ((KRT_PartnerSzemelyek)krt_Partnerek).Szemelyek;
                        LoadSzemelyekFromBusinessObject(krt_Szemelyek);
                    }
                    break;
                }
            case KodTarak.Partner_Tipus.Szervezet:
                {
                    if (krt_Partnerek is KRT_PartnerVallalkozasok)
                    {
                        KRT_Vallalkozasok krt_Vallalkozasok = ((KRT_PartnerVallalkozasok)krt_Partnerek).Vallalkozasok;
                        LoadVallalkozasokFromBusinessObject(krt_Vallalkozasok);
                    }
                    break;
                }
            default:
                break;
        }
        if (string.IsNullOrEmpty(calPartnerErvenyesseg.ErvKezd))
        {
            calPartnerErvenyesseg.ErvKezd = DateTime.Today.ToString();
        }
        //aktuális verzió eltárolása
        FormHeader1.Record_Ver = krt_Partnerek.Base.Ver;
        //A modósitás,létrehozás form beállítása
        FormPart_CreatedModified1.SetComponentValues(krt_Partnerek.Base);
    }

    private void LoadSzemelyekFromBusinessObject(KRT_Szemelyek krt_Szemelyek)
    {
        txtPartnerTitulus.Text = krt_Szemelyek.UjTitulis;
        txtPartnerCsaladiNev.Text = krt_Szemelyek.UjCsaladiNev;
        txtPartnerUtoNev.Text = krt_Szemelyek.UjUtonev;
        txtPartnerTovabbiUtonev.Text = krt_Szemelyek.UjTovabbiUtonev;
        txtPartnerAnyjaCsaladiNeve.Text = krt_Szemelyek.AnyjaNeveCsaladiNev;
        txtPartnerAnyjaUtoNev.Text = krt_Szemelyek.AnyjaNeveElsoUtonev;
        txtPartnerAnyjaTovabbiUtoNev.Text = krt_Szemelyek.AnyjaNeveTovabbiUtonev;

        txtPartnerSzuletesiCsaladiNev.Text = krt_Szemelyek.SzuletesiCsaladiNev;
        txtPartnerSzuletesiUtoNev.Text = krt_Szemelyek.SzuletesiElsoUtonev;
        txtPartnerSzuletesiTovabbiUtoNev.Text = krt_Szemelyek.SzuletesiTovabbiUtonev;
        txtPartnerSzuletesiOrszag.Text = krt_Szemelyek.SzuletesiOrszag;
        txtPartnerSzuletesiHely.Text = krt_Szemelyek.SzuletesiHely;
        calPartnerSzuletesiIdo.Text = krt_Szemelyek.SzuletesiIdo;
        txtPartnerTajSzam.Text = krt_Szemelyek.TAJSzam;
        txtPartnerSzigSzam.Text = krt_Szemelyek.SZIGSzam;
        Sex.FillRadioList(rblPartnerNeme, krt_Szemelyek.Neme);
        txtPartnerSzemelyiAzonosito.Text = krt_Szemelyek.SzemelyiAzonosito;
        txtPartnerAdoAzonosito.Text = krt_Szemelyek.Adoazonosito;
        txtPartnerAdoszamSzemely.SetTextBoxByAdoszam(krt_Szemelyek.Adoszam, krt_Szemelyek.KulfoldiAdoszamJelolo);
        // BLG_346
        txtPartnerBeosztas.Text = krt_Szemelyek.Beosztas;
        ddlPartnerMinositesiSzint.FillAndSetSelectedValue(kcs_IRAT_MINOSITES, krt_Szemelyek.MinositesiSzint, true, FormHeader1.ErrorPanel);

    }

    private void LoadVallalkozasokFromBusinessObject(KRT_Vallalkozasok krt_Vallalkozasok)
    {
        txtPartnerAdoSzamVallalkozas.SetTextBoxByAdoszam(krt_Vallalkozasok.Adoszam, krt_Vallalkozasok.KulfoldiAdoszamJelolo);
        txtPartnerTBSzam.Text = krt_Vallalkozasok.TB_Torzsszam;
        txtPartnerCegJegyzekSzam.Text = krt_Vallalkozasok.Cegjegyzekszam;
        ddlPartnerCegTipus.FillAndSetSelectedValue(kcs_CEGTIPUS, krt_Vallalkozasok.Tipus, true, FormHeader1.ErrorPanel);
    }

    private void LoadComponentsFromBusinessObjectKonszolidalando(KRT_Partnerek krt_Partnerek)
    {
        txtKonszolidalandoPartnerNev.Text = krt_Partnerek.Nev;
        ddlKonszolidalandoPartnerTipus.FillAndSetSelectedValue(kcs_PARTNER_TIPUS, krt_Partnerek.Tipus, FormHeader1.ErrorPanel);
        txtKonszolidalandoPartnerOrszagok.Id_HiddenField = krt_Partnerek.Orszag_Id;
        txtKonszolidalandoPartnerOrszagok.SetTextBoxById(FormHeader1.ErrorPanel);
        BoolString.FillDropDownList(ddlKonszolidalandoPartnerBelso, krt_Partnerek.Belso);
        calKonszolidalandoPartnerErvenyesseg.ErvKezd = krt_Partnerek.ErvKezd;
        calKonszolidalandoPartnerErvenyesseg.ErvVege = krt_Partnerek.ErvVege;
        txtKonszolidalandoPartnerKulsoAzonosito.Text = krt_Partnerek.KulsoAzonositok;


        //LZS - BLG 1915
        if (!string.IsNullOrEmpty(krt_Partnerek.Allapot))
        {
            ddlKonszolidalandoPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, krt_Partnerek.Allapot, FormHeader1.ErrorPanel);
        }
        else
        {
            ddlKonszolidalandoPartnerAllapot.FillAndSetSelectedValue(kcs_PARTNER_ALLAPOT, KodTarak.Partner_Allapot.Jovahagyando, FormHeader1.ErrorPanel);
        }



        switch (krt_Partnerek.Tipus)
        {
            case KodTarak.Partner_Tipus.Alkalmazas:
                {
                    break;
                }
            case KodTarak.Partner_Tipus.Szemely:
                {
                    if (krt_Partnerek is KRT_PartnerSzemelyek)
                    {
                        KRT_Szemelyek krt_Szemelyek = ((KRT_PartnerSzemelyek)krt_Partnerek).Szemelyek;
                        LoadSzemelyekFromBusinessObjectKonszolidalando(krt_Szemelyek);
                    }
                    break;
                }
            case KodTarak.Partner_Tipus.Szervezet:
                {
                    if (krt_Partnerek is KRT_PartnerVallalkozasok)
                    {
                        KRT_Vallalkozasok krt_Vallalkozasok = ((KRT_PartnerVallalkozasok)krt_Partnerek).Vallalkozasok;
                        LoadVallalkozasokFromBusinessObjectKonszolidalando(krt_Vallalkozasok);
                    }
                    break;
                }
            default:
                break;
        }
        if (string.IsNullOrEmpty(calPartnerErvenyesseg.ErvKezd))
        {
            calPartnerErvenyesseg.ErvKezd = DateTime.Today.ToString();
        }
        //aktuális verzió eltárolása
        FormHeader1.Record_Ver = krt_Partnerek.Base.Ver;
        //A modósitás,létrehozás form beállítása
        FormPart_CreatedModified1.SetComponentValues(krt_Partnerek.Base);
    }

    private void LoadSzemelyekFromBusinessObjectKonszolidalando(KRT_Szemelyek krt_Szemelyek)
    {
        txtKonszolidalandoPartnerTitulus.Text = krt_Szemelyek.UjTitulis;
        txtKonszolidalandoPartnerCsaladiNev.Text = krt_Szemelyek.UjCsaladiNev;
        txtKonszolidalandoPartnerUtoNev.Text = krt_Szemelyek.UjUtonev;
        txtKonszolidalandoPartnerTovabbiUtonev.Text = krt_Szemelyek.UjTovabbiUtonev;
        txtKonszolidalandoPartnerAnyjaCsaladiNeve.Text = krt_Szemelyek.AnyjaNeveCsaladiNev;
        txtKonszolidalandoPartnerAnyjaUtoNev.Text = krt_Szemelyek.AnyjaNeveElsoUtonev;
        txtKonszolidalandoPartnerAnyjaTovabbiUtoNev.Text = krt_Szemelyek.AnyjaNeveTovabbiUtonev;

        txtKonszolidalandoPartnerSzuletesiCsaladiNev.Text = krt_Szemelyek.SzuletesiCsaladiNev;
        txtKonszolidalandoPartnerSzuletesiUtoNev.Text = krt_Szemelyek.SzuletesiElsoUtonev;
        txtKonszolidalandoPartnerSzuletesiTovabbiUtoNev.Text = krt_Szemelyek.SzuletesiTovabbiUtonev;
        txtKonszolidalandoPartnerSzuletesiOrszag.Text = krt_Szemelyek.SzuletesiOrszag;
        txtKonszolidalandoPartnerSzuletesiHely.Text = krt_Szemelyek.SzuletesiHely;
        calKonszolidalandoPartnerSzuletesiIdo.Text = krt_Szemelyek.SzuletesiIdo;
        txtKonszolidalandoPartnerTajSzam.Text = krt_Szemelyek.TAJSzam;
        txtKonszolidalandoPartnerSzigSzam.Text = krt_Szemelyek.SZIGSzam;
        Sex.FillRadioList(rblKonszolidalandoPartnerNeme, krt_Szemelyek.Neme);
        txtKonszolidalandoPartnerSzemelyiAzonosito.Text = krt_Szemelyek.SzemelyiAzonosito;
        txtKonszolidalandoPartnerAdoAzonosito.Text = krt_Szemelyek.Adoazonosito;
        txtKonszolidalandoPartnerAdoszamSzemely.SetTextBoxByAdoszam(krt_Szemelyek.Adoszam, krt_Szemelyek.KulfoldiAdoszamJelolo);
        // BLG_346
        txtKonszolidalandoPartnerBeosztas.Text = krt_Szemelyek.Beosztas;
        ddlKonszolidalandoPartnerMinositesiSzint.FillAndSetSelectedValue(kcs_IRAT_MINOSITES, krt_Szemelyek.MinositesiSzint, true, FormHeader1.ErrorPanel);

    }

    private void ClearSzemelyAdatokKonszolidalando()
    {
        txtKonszolidalandoPartnerTitulus.Text = string.Empty;
        txtKonszolidalandoPartnerCsaladiNev.Text = string.Empty;
        txtKonszolidalandoPartnerUtoNev.Text = string.Empty;
        txtKonszolidalandoPartnerTovabbiUtonev.Text = string.Empty;
        txtKonszolidalandoPartnerAnyjaCsaladiNeve.Text = string.Empty;
        txtKonszolidalandoPartnerAnyjaUtoNev.Text = string.Empty;
        txtKonszolidalandoPartnerAnyjaTovabbiUtoNev.Text = string.Empty;

        txtKonszolidalandoPartnerSzuletesiCsaladiNev.Text = string.Empty;
        txtKonszolidalandoPartnerSzuletesiUtoNev.Text = string.Empty;
        txtKonszolidalandoPartnerSzuletesiTovabbiUtoNev.Text = string.Empty;
        txtKonszolidalandoPartnerSzuletesiOrszag.Text = string.Empty;
        txtKonszolidalandoPartnerSzuletesiHely.Text = string.Empty;
        calKonszolidalandoPartnerSzuletesiIdo.Text = string.Empty;
        txtKonszolidalandoPartnerTajSzam.Text = string.Empty;
        txtKonszolidalandoPartnerSzigSzam.Text = string.Empty;
        Sex.FillRadioList(rblKonszolidalandoPartnerNeme, Sex.NotSet.Value);
        txtKonszolidalandoPartnerSzemelyiAzonosito.Text = string.Empty;
        txtKonszolidalandoPartnerAdoAzonosito.Text = string.Empty;
        txtKonszolidalandoPartnerAdoszamSzemely.Text = string.Empty;
        // BLG_346
        txtKonszolidalandoPartnerBeosztas.Text = string.Empty;
        ddlKonszolidalandoPartnerMinositesiSzint.FillAndSetEmptyValue(kcs_CEGTIPUS, FormHeader1.ErrorPanel);

    }

    private void LoadVallalkozasokFromBusinessObjectKonszolidalando(KRT_Vallalkozasok krt_Vallalkozasok)
    {
        txtKonszolidalandoPartnerAdoSzamVallalkozas.SetTextBoxByAdoszam(krt_Vallalkozasok.Adoszam, krt_Vallalkozasok.KulfoldiAdoszamJelolo);
        txtKonszolidalandoPartnerTBSzam.Text = krt_Vallalkozasok.TB_Torzsszam;
        txtKonszolidalandoPartnerCegJegyzekSzam.Text = krt_Vallalkozasok.Cegjegyzekszam;
        ddlKonszolidalandoPartnerCegTipus.FillAndSetSelectedValue(kcs_CEGTIPUS, krt_Vallalkozasok.Tipus, true, FormHeader1.ErrorPanel);
    }

    private void ClearVallalkozasAdatokKonszolidalando()
    {
        txtKonszolidalandoPartnerAdoSzamVallalkozas.Text = string.Empty;
        txtKonszolidalandoPartnerTBSzam.Text = string.Empty;
        txtKonszolidalandoPartnerCegJegyzekSzam.Text = string.Empty;
        ddlKonszolidalandoPartnerCegTipus.FillAndSetEmptyValue(kcs_CEGTIPUS, FormHeader1.ErrorPanel);
    }

    private void ClearPartnerAdatokKonszolidalando()
    {
        txtKonszolidalandoPartnerNev.Text = string.Empty;
        txtKonszolidalandoPartnerOrszagok.Text = string.Empty;
        BoolString.FillDropDownList(ddlKonszolidalandoPartnerBelso, BoolString.No.Value);
        ddlKonszolidalandoPartnerAllapot.FillAndSetEmptyValue(kcs_PARTNER_ALLAPOT, FormHeader1.ErrorPanel);
        txtKonszolidalandoPartnerKulsoAzonosito.Text = string.Empty;
        ddlKonszolidalandoPartnerTipus.FillAndSetEmptyValue(kcs_PARTNER_TIPUS, FormHeader1.ErrorPanel);
    }

    private void ClearGridViewsData()
    {
        KonszolidalandoPartnerPostaiCimekGridView.DataSource = null;
        KonszolidalandoPartnerPostaiCimekGridView.DataBind();
        KonszolidalandoPartnerKapcsolatokGridView.DataSource = null;
        KonszolidalandoPartnerKapcsolatokGridView.DataBind();
    }

    protected void chbKonszolidalandoPartner_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox currentCheckbox = sender as CheckBox;
        string currentTrName = currentCheckbox.ID.Replace("chb", "tr");
        string currentTdName = currentCheckbox.ID.Replace("chb", "td");

        ContentPlaceHolder myPlaceHolder = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
        HtmlTableRow tr = (myPlaceHolder.FindControl(currentTrName)) as HtmlTableRow;
        HtmlTableCell td = (myPlaceHolder.FindControl(currentTdName)) as HtmlTableCell;

        if (tr != null)
        {
            if (currentCheckbox.Checked)
            {
                tr.BgColor = "#DDE7EF";
            }
            else if (!currentCheckbox.Checked)
            {
                tr.BgColor = "";
            }
        }
        else if (td != null)
        {
            if (currentCheckbox.Checked)
            {
                td.BgColor = "#DDE7EF";
            }
            else if (!currentCheckbox.Checked)
            {
                td.BgColor = "";
            }
        }

    }

    protected void btnLoadKonszolidaltPartnerData_Click(object sender, EventArgs e)
    {
        LoadKonszolidalandoPartnerToForm();
    }

    private void LoadKonszolidalandoPartnerToForm()
    {
        string partnerId = PartnerTextBoxKonszolidaltPartner.Id_HiddenField;
        betoltveHiddenField.Value = partnerId;

        if (!String.IsNullOrEmpty(partnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = partnerId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek krt_Partnerek = (KRT_Partnerek)result.Record;

                if (krt_Partnerek.Tipus != ddlPartnerTipus.SelectedValue)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "alert", "alert('A kiválasztott konszolidálandó partner típusa más mint a kijelölt partner típusa!');", true);
                    FormFooter1.ImageButton_SaveAndClose.Enabled = false;

                    ClearPartnerAdatokKonszolidalando();
                    ClearSzemelyAdatokKonszolidalando();
                    ClearVallalkozasAdatokKonszolidalando();
                    ClearGridViewsData();

                    return;
                }
                else
                {
                    FormFooter1.ImageButton_SaveAndClose.Enabled = true;
                }

                LoadComponentsFromBusinessObjectKonszolidalando(krt_Partnerek);
                KonszolidalandoPartnerPostaiCimekGridViewBind(partnerId);
                KonszolidalandoPartnerKapcsolatokGridViewBind(partnerId);
            }
        }

        FormFooter1.Visible = true;
    }

    

    #region PartnerPostaiCimek gridview
    protected void PartnerPostaiCimekGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev";
        String sortExpression = Search.GetSortExpressionFromViewState("PostaiCimekGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PostaiCimekGridView", ViewState);

        PartnerPostaiCimekGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void PartnerPostaiCimekGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;

            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.OrderBy = Search.GetOrderBy("PostaiCimekGridView", ViewState, SortExpression, SortDirection);

            PartnerPostaiCimekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByPartner(execParam, partner, null, search);

            UI.GridViewFill(PartnerPostaiCimekGridView, res, PartnerPostaiCimekSubListHeader, EErrorPanel1, null);
            //UI.SetTabHeaderRowCountText(res, headerPostaiCimek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerPostaiCimekGridView);
        }
        else
        {
            ui.GridViewClear(PartnerPostaiCimekGridView);
        }
    }

    //GridView eseménykezelõi(3-4)
    //specialis, nem mindig kell kell
    protected void PartnerPostaiCimekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //a több oszlopból vett mezõk beállítása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            string hazszamIg = drv["Hazszamig"].ToString();
            string hazszamBetujel = drv["HazszamBetujel"].ToString();
            string ajtoBetujel = drv["AjtoBetujel"].ToString();

            Label hazszam = (Label)e.Row.FindControl("labelHazszam");
            Label ajto = (Label)e.Row.FindControl("labelAjto");

            if (!String.IsNullOrEmpty(hazszamIg))
            {
                if (hazszam != null)
                    hazszam.Text += "-" + hazszamIg;
            }
            if (!String.IsNullOrEmpty(hazszamBetujel))
            {
                if (hazszam != null)
                    hazszam.Text += "/" + hazszamBetujel;
            }
            if (!String.IsNullOrEmpty(ajtoBetujel))
            {
                if (ajto != null)
                    ajto.Text += "/" + ajtoBetujel;
            }

        }

    }

    protected void PartnerPostaiCimekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PartnerPostaiCimekGridView, selectedRowNumber, "check");
        }
    }

    protected void PartnerPostaiCimekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = PartnerPostaiCimekGridView.PageIndex;

        PartnerPostaiCimekGridView.PageIndex = PartnerPostaiCimekSubListHeader.PageIndex;

        //if (prev_PageIndex != PartnerPostaiCimekGridView.PageIndex)
        //{
        //    //scroll állapotának törlése
        //    JavaScripts.ResetScroll(Page, PostaiCimekCPE);
        //    PartnerPostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerPostaiCimekGridView));
        //}
        //else
        //{
        //    UI.GridViewSetScrollable(PartnerPostaiCimekSubListHeader.Scrollable, PostaiCimekCPE);
        //}

        PartnerPostaiCimekSubListHeader.PageCount = PartnerPostaiCimekGridView.PageCount;
        PartnerPostaiCimekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(PartnerPostaiCimekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerPostaiCimekGridView);
    }

    protected void PartnerPostaiCimekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PartnerPostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(PartnerPostaiCimekGridView)
             , e.SortExpression, UI.GetSortToGridView("PartnerPostaiCimekGridView", ViewState, e.SortExpression));
    }
    #endregion

    #region KonszolidalandoPartnerPostaiCimek gridview
    protected void KonszolidalandoPartnerPostaiCimekGridViewBind(string PartnerId)
    {
        string defaultSortExpression = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev";
        String sortExpression = Search.GetSortExpressionFromViewState("KonszolidalandoPostaiCimekGridView", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KonszolidalandoPostaiCimekGridView", ViewState);

        KonszolidalandoPartnerPostaiCimekGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void KonszolidalandoPartnerPostaiCimekGridViewBind(string PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = PartnerId;

            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.OrderBy = Search.GetOrderBy("PostaiCimekGridView", ViewState, SortExpression, SortDirection);

            KonszolidalandoPartnerPostaiCimekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result resAll = service.GetAllByPartner(execParam, partner, null, search);

            UI.GridViewFill(KonszolidalandoPartnerPostaiCimekGridView, resAll, KonszolidalandoPartnerPostaiCimekSubListHeader, EErrorPanel1, null);
            //UI.SetTabHeaderRowCountText(res, headerPostaiCimek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(KonszolidalandoPartnerPostaiCimekGridView);
        }
        else
        {
            ui.GridViewClear(PartnerPostaiCimekGridView);
        }
    }

    //GridView eseménykezelõi(3-4)
    //specialis, nem mindig kell kell
    protected void KonszolidalandoPartnerPostaiCimekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //a több oszlopból vett mezõk beállítása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            string hazszamIg = drv["Hazszamig"].ToString();
            string hazszamBetujel = drv["HazszamBetujel"].ToString();
            string ajtoBetujel = drv["AjtoBetujel"].ToString();

            Label hazszam = (Label)e.Row.FindControl("labelHazszam");
            Label ajto = (Label)e.Row.FindControl("labelAjto");

            if (!String.IsNullOrEmpty(hazszamIg))
            {
                if (hazszam != null)
                    hazszam.Text += "-" + hazszamIg;
            }
            if (!String.IsNullOrEmpty(hazszamBetujel))
            {
                if (hazszam != null)
                    hazszam.Text += "/" + hazszamBetujel;
            }
            if (!String.IsNullOrEmpty(ajtoBetujel))
            {
                if (ajto != null)
                    ajto.Text += "/" + ajtoBetujel;
            }

        }

    }

    protected void KonszolidalandoPartnerPostaiCimekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KonszolidalandoPartnerPostaiCimekGridView, selectedRowNumber, "check");
        }
    }

    protected void KonszolidalandoPartnerPostaiCimekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = KonszolidalandoPartnerPostaiCimekGridView.PageIndex;

        KonszolidalandoPartnerPostaiCimekGridView.PageIndex = KonszolidalandoPartnerPostaiCimekSubListHeader.PageIndex;

        //if (prev_PageIndex != KonszolidalandoPartnerPostaiCimekGridView.PageIndex)
        //{
        //    //scroll állapotának törlése
        //    JavaScripts.ResetScroll(Page, PostaiCimekCPE);
        //    KonszolidalandoPartnerPostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(KonszolidalandoPartnerPostaiCimekGridView));
        //}
        //else
        //{
        //    UI.GridViewSetScrollable(PartnerPostaiCimekSubListHeader.Scrollable, PostaiCimekCPE);
        //}

        PartnerPostaiCimekSubListHeader.PageCount = PartnerPostaiCimekGridView.PageCount;
        PartnerPostaiCimekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(PartnerPostaiCimekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerPostaiCimekGridView);
    }

    protected void KonszolidalandoPartnerPostaiCimekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KonszolidalandoPartnerPostaiCimekGridViewBind(UI.GetGridViewSelectedRecordId(KonszolidalandoPartnerPostaiCimekGridView)
             , e.SortExpression, UI.GetSortToGridView("KonszolidalandoPartnerPostaiCimekGridView", ViewState, e.SortExpression));
    }
    #endregion

    #region PartnerKapcsolatok Detail

    //Data Binding(2)
    protected void PartnerKapcsolatokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("PartnerKapcsolatokGridView", ViewState, "Partner_kapcsolt_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PartnerKapcsolatokGridView", ViewState);

        PartnerKapcsolatokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void PartnerKapcsolatokGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            ExecParam execPar = UI.SetExecParamDefault(Page, new ExecParam());
            execPar.Record_Id = PartnerId;

            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result_partnerGet = service.Get(execPar);

            if (!result_partnerGet.IsError)
            {
                selectedPartnerObj = (KRT_Partnerek)result_partnerGet.Record;
            }

            if (selectedPartnerObj != null)
            {
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_Partnerek partner = new KRT_Partnerek();
                partner.Id = PartnerId;
                KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();

                bool isTUK = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.TUK, false);
                var list = KodTarak.PartnerKapcsolatTipus.KapcsolatMappings.FilterByPartnerTipus(selectedPartnerObj.Tipus).Where(x => x.IsNormal == false && x.Belso == selectedPartnerObj.Belso).Select(x => x.KapcsolatTipus).ToArray();
                string[] filtered = list;
                if (!isTUK)
                {
                    filtered = list.Except(new string[] { "M" }).ToArray();
                }

                if (filtered.Contains("fake"))
                {
                    PartnerKapcsolatokPanel.Enabled = false;
                }

                PartnerKapcsolatokPanel.Enabled = !(selectedPartnerObj.Belso == "1" && selectedPartnerObj.Tipus == "20");

                search.Tipus.Value = Search.GetSqlInnerString(filtered);

                if (!string.IsNullOrEmpty(search.Tipus.Value))
                {
                    search.Tipus.Operator = Query.Operators.inner;
                    search.OrderBy = Search.GetOrderBy("PartnerKapcsolatokGridView", ViewState, SortExpression, SortDirection);

                    PartnerKapcsolatokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

                    Result res = service.GetAllByPartner(ExecParam, partner, search);
                    UI.GridViewFill(PartnerKapcsolatokGridView, res, PartnerKapcsolatokSubListHeader, EErrorPanel1, null);

                    ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerKapcsolatokGridView);
                }
            }
            else
            {
                ui.GridViewClear(PartnerKapcsolatokGridView);
            }

        }

    }

    //GridView eseménykezelõi(3-4)
    protected void PartnerKapcsolatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PartnerKapcsolatokGridView, selectedRowNumber, "check");
        }
    }



    protected void PartnerKapcsolatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PartnerKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(PartnerKapcsolatokGridView)
             , e.SortExpression, UI.GetSortToGridView("PartnerKapcsolatokGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region KonszolidalandoPartnerKapcsolatok Detail

    //Data Binding(2)
    protected void KonszolidalandoPartnerKapcsolatokGridViewBind(String PartnerId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("KonszolidalandoPartnerKapcsolatokGridView", ViewState, "Partner_kapcsolt_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("KonszolidalandoPartnerKapcsolatokGridView", ViewState);

        KonszolidalandoPartnerKapcsolatokGridViewBind(PartnerId, sortExpression, sortDirection);
    }

    protected void KonszolidalandoPartnerKapcsolatokGridViewBind(String PartnerId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(PartnerId))
        {
            ExecParam execPar = UI.SetExecParamDefault(Page, new ExecParam());
            execPar.Record_Id = PartnerId;

            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result_partnerGet = service.Get(execPar);

            if (!result_partnerGet.IsError)
            {
                selectedPartnerObj = (KRT_Partnerek)result_partnerGet.Record;
            }

            if (selectedPartnerObj != null)
            {

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_Partnerek partner = new KRT_Partnerek();
                partner.Id = PartnerId;
                KRT_PartnerKapcsolatokSearch search = new KRT_PartnerKapcsolatokSearch();

                bool isTUK = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.TUK, false);
                var list = KodTarak.PartnerKapcsolatTipus.KapcsolatMappings.FilterByPartnerTipus(selectedPartnerObj.Tipus).Where(x => x.IsNormal == false && x.Belso == selectedPartnerObj.Belso).Select(x => x.KapcsolatTipus).ToArray();
                string[] filtered = list;
                if (!isTUK)
                {
                    filtered = list.Except(new string[] { "M" }).ToArray();
                }

                if (filtered.Contains("fake"))
                {
                    KonszolidalandoPartnerKapcsolatokPanel.Enabled = false;
                }

                KonszolidalandoPartnerKapcsolatokPanel.Enabled = !(selectedPartnerObj.Belso == "1" && selectedPartnerObj.Tipus == "20");

                search.Tipus.Value = Search.GetSqlInnerString(filtered);

                if (!string.IsNullOrEmpty(search.Tipus.Value))
                {
                    search.Tipus.Operator = Query.Operators.inner;
                    search.OrderBy = Search.GetOrderBy("KonszolidalandoPartnerKapcsolatokGridView", ViewState, SortExpression, SortDirection);

                    KonszolidalandoPartnerKapcsolatokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

                    Result res = service.GetAllByPartner(ExecParam, partner, search);
                    UI.GridViewFill(KonszolidalandoPartnerKapcsolatokGridView, res, KonszolidalandoPartnerKapcsolatokSubListHeader, EErrorPanel1, null);

                    ui.SetClientScriptToGridViewSelectDeSelectButton(KonszolidalandoPartnerKapcsolatokGridView);
                }
            }
            else
            {
                ui.GridViewClear(KonszolidalandoPartnerKapcsolatokGridView);
            }
        }


    }

    //GridView eseménykezelõi(3-4)
    protected void KonszolidalandoPartnerKapcsolatokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(KonszolidalandoPartnerKapcsolatokGridView, selectedRowNumber, "check");
        }
    }



    protected void KonszolidalandoPartnerKapcsolatokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        KonszolidalandoPartnerKapcsolatokGridViewBind(UI.GetGridViewSelectedRecordId(KonszolidalandoPartnerKapcsolatokGridView)
             , e.SortExpression, UI.GetSortToGridView("KonszolidalandoPartnerKapcsolatokGridView", ViewState, e.SortExpression));
    }

    #endregion
}
