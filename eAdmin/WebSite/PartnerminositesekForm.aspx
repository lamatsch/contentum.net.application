<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="PartnerminositesekForm.aspx.cs" Inherits="PartnerminositesekForm" Title="Untitled Page" %>


<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>  
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>  
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc7" %>  
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc8" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc10" %>


  
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,PartnerminositesekFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelPartner" runat="server" Text="Partner:" CssClass="mrUrlapInputWaterMarked"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc4:PartnerTextBox ID="PartnerTextBox1" runat="server" ReadOnly="true" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelAllapot" runat="server" Text="�llapot:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="KodtarakDropDownListAllapot" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelIgenylo" runat="server" Text="Ig�nyl�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:FelhasznaloTextBox Id="FelhasznaloTextBoxIgenylo" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelKertMinosites" runat="server" Text="K�rt min�s�t�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="KodtarakDropDownListMinosites" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelIgenylesKezdVeg" runat="server" Text="Ig�nyelt kezdet �s v�g:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControlIgenyelt" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelIgenylolap" runat="server" Text="Ig�nyl�lap:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textIgenylolap" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label8" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelElbiralo" runat="server" Text="Elb�r�l�:"></asp:Label>&nbsp;</td>
                                    <td class="mrUrlapMezo">
                                        <uc7:FelhasznaloTextBox Id="FelhasznaloTextBoxElbiralo" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label7" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelKerelemBeadasa" runat="server" Text="Ig�nyl�s bead�s�nak ideje:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:CalendarControl ID="CalendarControlKerelemBeadasa" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDontesIdeje" runat="server" Text="D�nt�s ideje:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc8:CalendarControl ID="CalendarControlDontesIdeje" runat="server" Validate="false" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDontesDokumentum" runat="server" Text="D�nt�s dokumentum:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textDontesDokumentum" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
                    <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>


