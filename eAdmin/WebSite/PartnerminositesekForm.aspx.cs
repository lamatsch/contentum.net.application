using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class PartnerminositesekForm : Contentum.eUtility.UI.PageBase
{
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private Authentication auth = new Authentication();
    private string Command = "";

    private const string kodcsoportMinositesAllapot = "PMINOSITES_ALLAPOT";
    private const string kodcsoportPartnerMinosites = "MINOSITESI_JELOLES";

    private void SetViewControls()
    {
        PartnerTextBox1.ReadOnly = true;
        KodtarakDropDownListAllapot.Enabled = false;
        FelhasznaloTextBoxIgenylo.ReadOnly = true;
        KodtarakDropDownListMinosites.Enabled = false;
        ErvenyessegCalendarControlIgenyelt.ReadOnly = true;
        textIgenylolap.ReadOnly = true;
        FelhasznaloTextBoxElbiralo.ReadOnly = true;
        CalendarControlKerelemBeadasa.ReadOnly = true;
        CalendarControlDontesIdeje.ReadOnly = true;
        textDontesDokumentum.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelAllapot.CssClass = "mrUrlapInputWaterMarked";
        labelIgenylo.CssClass = "mrUrlapInputWaterMarked";
        labelKertMinosites.CssClass = "mrUrlapInputWaterMarked";
        labelIgenylesKezdVeg.CssClass = "mrUrlapInputWaterMarked";
        labelIgenylolap.CssClass = "mrUrlapInputWaterMarked";
        labelElbiralo.CssClass = "mrUrlapInputWaterMarked";
        labelKerelemBeadasa.CssClass = "mrUrlapInputWaterMarked";
        labelDontesIdeje.CssClass = "mrUrlapInputWaterMarked";
        labelDontesDokumentum.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

    }

    private void SetModifyControls()
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, Command);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerMinosites" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_PartnerMinositesekService service = eAdminService.ServiceFactory.GetKRT_PartnerMinositesekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_PartnerMinositesek krt_PartnerMinositesek = (KRT_PartnerMinositesek)result.Record;
                    LoadComponentsFromBusinessObject(krt_PartnerMinositesek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            //Létrehozási értékek inicializálása

            if (!IsPostBack)
            {
                KodtarakDropDownListAllapot.FillDropDownList(kodcsoportMinositesAllapot, false, FormHeader1.ErrorPanel);
                KodtarakDropDownListMinosites.FillDropDownList(kodcsoportPartnerMinosites, false, FormHeader1.ErrorPanel);
            }

            string partnerId = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(partnerId))
            {
                PartnerTextBox1.Id_HiddenField = partnerId;
                PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
            }

            CalendarControlKerelemBeadasa.Text = DateTime.Today.ToShortDateString();



        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {

            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadComponentsFromBusinessObject(KRT_PartnerMinositesek krt_PartnerMinositesek)
    {

        PartnerTextBox1.Id_HiddenField = krt_PartnerMinositesek.Partner_id;
        PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        KodtarakDropDownListAllapot.FillAndSetSelectedValue(kodcsoportMinositesAllapot, krt_PartnerMinositesek.ALLAPOT, false, FormHeader1.ErrorPanel);
        FelhasznaloTextBoxIgenylo.Id_HiddenField = krt_PartnerMinositesek.Felhasznalo_id_kero;
        FelhasznaloTextBoxIgenylo.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        KodtarakDropDownListMinosites.FillAndSetSelectedValue(kodcsoportPartnerMinosites, krt_PartnerMinositesek.KertMinosites, false, FormHeader1.ErrorPanel);
        ErvenyessegCalendarControlIgenyelt.ErvKezd = krt_PartnerMinositesek.KertKezdDat;
        ErvenyessegCalendarControlIgenyelt.ErvVege = krt_PartnerMinositesek.KertVegeDat;
        textIgenylolap.Text = krt_PartnerMinositesek.KerelemAzonosito;
        FelhasznaloTextBoxElbiralo.Id_HiddenField = krt_PartnerMinositesek.Felhasznalo_id_donto;
        FelhasznaloTextBoxElbiralo.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
        CalendarControlKerelemBeadasa.Text = krt_PartnerMinositesek.KerelemBeadIdo;
        CalendarControlDontesIdeje.Text = krt_PartnerMinositesek.KerelemDontesIdo;
        textDontesDokumentum.Text = krt_PartnerMinositesek.DontesAzonosito;
        
        ErvenyessegCalendarControl1.ErvKezd = krt_PartnerMinositesek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_PartnerMinositesek.ErvVege;

        //aktuális verzió ektárolása
        FormHeader1.Record_Ver = krt_PartnerMinositesek.Base.Ver;
        //A modósitás,létrehozás form beállítása
        FormPart_CreatedModified1.SetComponentValues(krt_PartnerMinositesek.Base);
    }

    private KRT_PartnerMinositesek GetBusinessObjectFromComponents()
    {
        KRT_PartnerMinositesek krt_PartnerekMinositesek = new KRT_PartnerMinositesek();
        krt_PartnerekMinositesek.Updated.SetValueAll(false);
        krt_PartnerekMinositesek.Base.Updated.SetValueAll(false);

        krt_PartnerekMinositesek.Partner_id = PartnerTextBox1.Id_HiddenField;
        krt_PartnerekMinositesek.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBox1);
        krt_PartnerekMinositesek.ALLAPOT = KodtarakDropDownListAllapot.SelectedValue;
        krt_PartnerekMinositesek.Updated.ALLAPOT = pageView.GetUpdatedByView(KodtarakDropDownListAllapot);
        krt_PartnerekMinositesek.Felhasznalo_id_kero = FelhasznaloTextBoxIgenylo.Id_HiddenField;
        krt_PartnerekMinositesek.Updated.Felhasznalo_id_kero = pageView.GetUpdatedByView(FelhasznaloTextBoxIgenylo);
        krt_PartnerekMinositesek.KertMinosites = KodtarakDropDownListMinosites.SelectedValue;
        krt_PartnerekMinositesek.Updated.KertMinosites = pageView.GetUpdatedByView(KodtarakDropDownListMinosites);
        krt_PartnerekMinositesek.KertKezdDat = ErvenyessegCalendarControlIgenyelt.ErvKezd;
        krt_PartnerekMinositesek.Updated.KertKezdDat = pageView.GetUpdatedByView(ErvenyessegCalendarControlIgenyelt);
        krt_PartnerekMinositesek.KertVegeDat = ErvenyessegCalendarControlIgenyelt.ErvVege;
        krt_PartnerekMinositesek.Updated.KertVegeDat = pageView.GetUpdatedByView(ErvenyessegCalendarControlIgenyelt);
        krt_PartnerekMinositesek.KerelemAzonosito = textIgenylolap.Text;
        krt_PartnerekMinositesek.Updated.KerelemAzonosito = pageView.GetUpdatedByView(textIgenylolap);
        krt_PartnerekMinositesek.Felhasznalo_id_donto = FelhasznaloTextBoxElbiralo.Id_HiddenField;
        krt_PartnerekMinositesek.Updated.Felhasznalo_id_donto = pageView.GetUpdatedByView(FelhasznaloTextBoxElbiralo);
        krt_PartnerekMinositesek.KerelemBeadIdo = CalendarControlKerelemBeadasa.Text;
        krt_PartnerekMinositesek.Updated.KerelemBeadIdo = pageView.GetUpdatedByView(CalendarControlKerelemBeadasa);
        krt_PartnerekMinositesek.KerelemDontesIdo = CalendarControlDontesIdeje.Text;
        krt_PartnerekMinositesek.Updated.KerelemDontesIdo = pageView.GetUpdatedByView(CalendarControlDontesIdeje);
        krt_PartnerekMinositesek.DontesAzonosito = textDontesDokumentum.Text;
        krt_PartnerekMinositesek.Updated.DontesAzonosito = pageView.GetUpdatedByView(textDontesDokumentum);

        //krt_PartnerekMinositesek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_PartnerekMinositesek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_PartnerekMinositesek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_PartnerekMinositesek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_PartnerekMinositesek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyitási verzió megadása ellenőrzés miatt
        krt_PartnerekMinositesek.Base.Ver = FormHeader1.Record_Ver;
        krt_PartnerekMinositesek.Base.Updated.Ver = true;

        return krt_PartnerekMinositesek;
    }


    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) return;
        compSelector.Enabled = true;

        compSelector.Add_ComponentOnClick(PartnerTextBox1);
        compSelector.Add_ComponentOnClick(KodtarakDropDownListAllapot);
        compSelector.Add_ComponentOnClick(FelhasznaloTextBoxIgenylo);
        compSelector.Add_ComponentOnClick(KodtarakDropDownListMinosites);
        compSelector.Add_ComponentOnClick(ErvenyessegCalendarControlIgenyelt);
        compSelector.Add_ComponentOnClick(textIgenylolap);
        compSelector.Add_ComponentOnClick(FelhasznaloTextBoxElbiralo);
        compSelector.Add_ComponentOnClick(CalendarControlKerelemBeadasa);
        compSelector.Add_ComponentOnClick(CalendarControlDontesIdeje);
        compSelector.Add_ComponentOnClick(textDontesDokumentum);
        compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

        FormFooter1.SaveEnabled = false;
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "PartnerMinosites" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_PartnerMinositesekService service = eAdminService.ServiceFactory.GetKRT_PartnerMinositesekService();
                            KRT_PartnerMinositesek krt_PartnerMinositek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_PartnerMinositek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                //nincs visszaküldés
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_PartnerMinositesekService service = eAdminService.ServiceFactory.GetKRT_PartnerMinositesekService();
                                KRT_PartnerMinositesek krt_PartnerMinositesek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_PartnerMinositesek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page,true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
