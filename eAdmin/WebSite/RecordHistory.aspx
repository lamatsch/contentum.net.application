<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="RecordHistory.aspx.cs" Inherits="RecordHistory" %>

<%@ Register Src="Component/HistoryListHeader.ascx" TagName="HistoryListHeader" TagPrefix="uc3" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc1" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc8" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc3:HistoryListHeader ID="HistoryListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc8:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table width="95%" cellpadding="0" cellspacing="0">        
        <tr>
            <td style="text-align: left; height: 321px; vertical-align: top;">
                <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                    <ContentTemplate>
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <asp:GridView  ID="HistoryGridView" runat="server" OnRowCommand="HistoryGridView_RowCommand"
                                        CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                        PagerSettings-Visible="false" AllowSorting="True" OnPreRender="HistoryGridView_PreRender"
                                        AutoGenerateColumns="false" DataKeyNames="RowId">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>                                            
                                            <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                <HeaderStyle Width="25px" />
                                                <ItemStyle BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>
                                            <asp:BoundField DataField="Operation" HeaderText="M�velet">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Oszlop neve">
                                                <HeaderStyle Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="oszlopNeve" runat="server"
                                                        Text='<%# GetGlobalResourceObject("RecordHistory", Eval("ColumnName").ToString()) == null ? Eval("ColumnName").ToString() : GetGlobalResourceObject("RecordHistory", Eval("ColumnName").ToString()).ToString() %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="OldValue" HeaderText="R�gi �rt�k">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NewValue" HeaderText="�j �rt�k">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Executor" HeaderText="V�grehajt�">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ExecutionTime" HeaderText="V�grehajt�s ideje">
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundField>
                                            </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <uc2:FormFooter ID="FormFooter" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
