using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Data;

public partial class RecordHistory : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();

    protected void Page_Init(object sender, EventArgs e)
    {
        /// Jogosultságellenőrzés:

        KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string tableName = Request.QueryString.Get(QueryStringVars.TableName);
        string MuveletKod_ViewHistory = "ViewHistory";

        Result result = service.GetByTableAndOperation(execParam, tableName, MuveletKod_ViewHistory);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
            UI.RedirectToErrorPageByErrorMsg(Page, resultError.ErrorMessage);
        }
        else
        {
            KRT_Funkciok funkcio = (KRT_Funkciok)result.Record;
            String funkcioKod = funkcio.Kod;
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        //HistoryListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("HistorySearch.aspx", ""
        //    , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HistoryListHeader1.HeaderLabel = Resources.List.RecordHistoryHeaderTitle;

        HistoryListHeader1.SearchEnabled = false;
        HistoryListHeader1.ViewEnabled = false;
        HistoryListHeader1.ExportEnabled = false;
        HistoryListHeader1.PrintEnabled = false;

        if (!IsPostBack) HistoryGridViewBind();

    }

    //LZS - BUG_7062
    protected void Page_PreRender(object sender, EventArgs e)
    {
        string tableName = Request.QueryString.Get(QueryStringVars.TableName);
        string cellText = "";

        if (tableName == "Krt_Extra_Napok")
        {
            foreach (GridViewRow row in HistoryGridView.Rows)
            {
                for (int i = 0; i < HistoryGridView.Columns.Count; i++)
                {
                    cellText = row.Cells[i].Text;

                    if (i == 3 || i == 4)
                    {
                        switch (cellText)
                        {
                            case KodTarak.EXTRANAPOK.MunkaszunetiNap:
                                row.Cells[i].Text = KodTarak.EXTRANAPOK_NEV.MunkaszunetiNap;
                                break;

                            case KodTarak.EXTRANAPOK.RendkivuliMunkanap:
                                row.Cells[i].Text = KodTarak.EXTRANAPOK_NEV.RendkivuliMunkanap;
                                break;

                        }
                        
                    }
                }
            }
        }
    }

    protected void HistoryGridViewBind()
    {
        string recordId = Request.QueryString.Get(QueryStringVars.Id);
        string tableName = Request.QueryString.Get(QueryStringVars.TableName);

        if (tableName == "KRT_Partnerek")
        {

            if (!String.IsNullOrEmpty(recordId) && !String.IsNullOrEmpty(tableName))
            {
                RecordHistoryService service = eAdminService.ServiceFactory.GetRecordHistoryService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                ExecParam.Record_Id = recordId;
                Result resPartnerHistoryData = service.GetAllByRecord(ExecParam, tableName);

                //LZS - BUG_6182
                #region BUG_6182

                //Partneradatok lekérése
                KRT_PartnerekService partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                ExecParam exec_Partner = UI.SetExecParamDefault(Page, new ExecParam());
                exec_Partner.Record_Id = recordId;
                Result resultPartner = partnerekService.Get(exec_Partner);

                KRT_Partnerek krt_partner = (KRT_Partnerek)resultPartner.Record;



                if (krt_partner != null)
                {
                    //Result set-ek mergelése: resPartnerHistoryData + resSzemelyHistoryData
                    Result mergedResult = resPartnerHistoryData;
                    Result resMoreHistoryData = null;

                    switch (krt_partner.Tipus)
                    {
                        //Személy adatok lekérése
                        case KodTarak.Partner_Tipus.Szemely:
                            string szemelyId = "";
                            KRT_SzemelyekService szemelyekService = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
                            ExecParam exec_Szemely = UI.SetExecParamDefault(Page, new ExecParam());
                            KRT_SzemelyekSearch szemely_search = new KRT_SzemelyekSearch();

                            szemely_search.Partner_Id.Value = krt_partner.Id;
                            szemely_search.Partner_Id.Operator = Query.Operators.equals;
                            Result resultSzemely = szemelyekService.GetAll(exec_Szemely, szemely_search);

                            if (String.IsNullOrEmpty(resultSzemely.ErrorCode))
                            {
                                if (resultSzemely.Ds.Tables[0].Rows.Count == 1)
                                {
                                    foreach (DataRow item in resultSzemely.Ds.Tables[0].Rows)
                                    {
                                        szemelyId = item["Id"].ToString();
                                    }
                                }
                            }

                            exec_Szemely.Record_Id = szemelyId;
                            resMoreHistoryData = service.GetAllByRecord(exec_Szemely, "KRT_Szemelyek");
                            break;

                        case KodTarak.Partner_Tipus.Szervezet:
                            string szervezetId = "";
                            KRT_VallalkozasokService szervezetService = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
                            ExecParam exec_Szervezet = UI.SetExecParamDefault(Page, new ExecParam());
                            KRT_VallalkozasokSearch szervezet_search = new KRT_VallalkozasokSearch();

                            szervezet_search.Partner_Id.Value = krt_partner.Id;
                            szervezet_search.Partner_Id.Operator = Query.Operators.equals;
                            Result resultSzervezet = szervezetService.GetAll(exec_Szervezet, szervezet_search);

                            if (String.IsNullOrEmpty(resultSzervezet.ErrorCode))
                            {
                                if (resultSzervezet.Ds.Tables[0].Rows.Count == 1)
                                {
                                    foreach (DataRow item in resultSzervezet.Ds.Tables[0].Rows)
                                    {
                                        szervezetId = item["Id"].ToString();
                                    }
                                }
                            }

                            exec_Szervezet.Record_Id = szervezetId;
                            resMoreHistoryData = service.GetAllByRecord(exec_Szervezet, "KRT_Vallalkozasok");
                            break;

                    }



                    if (resMoreHistoryData != null)
                    {

                        mergedResult.Ds.Tables[0].Merge(resMoreHistoryData.Ds.Tables[0]);

                        DataView dataview = new DataView(mergedResult.Ds.Tables[0]);
                        dataview.Sort = "ExecutionTime ASC";
                        DataTable sortedDataTable = dataview.ToTable();
                        mergedResult.Ds.Tables.Clear();
                        mergedResult.Ds.Tables.Add(sortedDataTable);
                    }

                    //if (res.Ds != null)
                    //{
                    ui.GridViewFill(HistoryGridView, mergedResult, EErrorPanel1, ErrorUpdatePanel);
                    //}
                }
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
            }

            #endregion


        }
        else
        {
            if (!String.IsNullOrEmpty(recordId) && !String.IsNullOrEmpty(tableName))
            {
                RecordHistoryService service = eAdminService.ServiceFactory.GetRecordHistoryService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                ExecParam.Record_Id = recordId;
                Result res = service.GetAllByRecord(ExecParam, tableName);

                //if (res.Ds != null)
                //{
                ui.GridViewFill(HistoryGridView, res, EErrorPanel1, ErrorUpdatePanel);
                //}
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
            }
        }

    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {

    }
    protected void HistoryGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = HistoryGridView.PageIndex;

        HistoryGridView.PageIndex = (HistoryListHeader1.PageIndex < 0 ? 0 : HistoryListHeader1.PageIndex);
        HistoryListHeader1.PageCount = HistoryGridView.PageCount;

        if (prev_PageIndex != HistoryGridView.PageIndex)
        {
            HistoryGridViewBind();
            UI.ClearGridViewRowSelection(HistoryGridView);
            //ActiveTabClear();
        }

        HistoryListHeader1.PagerLabel = (HistoryGridView.PageIndex + 1).ToString() + "/" + HistoryGridView.PageCount.ToString();
    }

    protected void HistoryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}
