using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class RendszerParameterekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxNev.TextBox.Enabled = false;
        textErtek.Enabled = false;
        ddownKarbantarthato.Enabled = false;
        textNote.Enabled = false;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelErtek.CssClass = "mrUrlapInputWaterMarked";
        labelKarbantarthato.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
 
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        requiredTextBoxNev.TextBox.Enabled = false;
        ddownKarbantarthato.Enabled = false;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelKarbantarthato.CssClass = "mrUrlapInputWaterMarked";

    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                ErvenyessegCalendarControl1.Enabled = true;
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "RendszerParameter" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_Parameterek krt_Parameterek = GetBusinessObjectById();
                if (krt_Parameterek != null)
                    LoadComponentsFromBusinessObject(krt_Parameterek);
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    private KRT_Parameterek GetBusinessObjectById()
    {
        String id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {
            KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Parameterek krt_Parameterek = (KRT_Parameterek)result.Record;
                return krt_Parameterek;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }

        return null;
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_KodTarak"></param>
    private void LoadComponentsFromBusinessObject(KRT_Parameterek krt_Parameterek)
    {
        requiredTextBoxNev.Text = krt_Parameterek.Nev;
        textErtek.Text = krt_Parameterek.Ertek;
        ddownKarbantarthato.SelectedValue = krt_Parameterek.Karbantarthato;
        textNote.Text = krt_Parameterek.Base.Note;
        ErvenyessegCalendarControl1.ErvKezd = krt_Parameterek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Parameterek.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_Parameterek.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_Parameterek.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_Parameterek GetBusinessObjectFromComponents()
    {
        KRT_Parameterek krt_Parameterek = null;

        if (Command == CommandName.Modify)
        {
            krt_Parameterek = GetBusinessObjectById();
        }

        if (krt_Parameterek == null)
        {
            krt_Parameterek = new KRT_Parameterek();
        }

        krt_Parameterek.Updated.SetValueAll(false);
        krt_Parameterek.Base.Updated.SetValueAll(false);

        krt_Parameterek.Nev = requiredTextBoxNev.Text;
        krt_Parameterek.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);
        krt_Parameterek.Ertek = textErtek.Text;
        krt_Parameterek.Updated.Ertek = pageView.GetUpdatedByView(textErtek);
        krt_Parameterek.Karbantarthato = ddownKarbantarthato.SelectedItem.Value;
        krt_Parameterek.Updated.Karbantarthato = pageView.GetUpdatedByView(ddownKarbantarthato);
        krt_Parameterek.Base.Note = textNote.Text;
        krt_Parameterek.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        //krt_Parameterek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_Parameterek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_Parameterek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_Parameterek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Parameterek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        //krt_Parameterek.Base.Ver = FormHeader1.Record_Ver;
        krt_Parameterek.Base.Updated.Ver = true;

        return krt_Parameterek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(textErtek);
            compSelector.Add_ComponentOnClick(ddownKarbantarthato);
            compSelector.Add_ComponentOnClick(textNote);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
                            KRT_Parameterek krt_Parameterek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Parameterek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
                                KRT_Parameterek krt_Parameterek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Parameterek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
