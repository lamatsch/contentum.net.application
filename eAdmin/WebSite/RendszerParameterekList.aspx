<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RendszerParameterekList.aspx.cs" Inherits="RendszerParameterekList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
        
    <!--F� lista-->
    <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td style="text-align: left; vertical-align: top; width: 0px;">
       <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeRendszerParameterek" OnClientClick="return false;" />
       </td>
         <td style="text-align: left; vertical-align: top; width: 100%;">           
            <asp:UpdatePanel ID="updatePanelRendszerParameterek" runat="server" OnLoad="updatePanelRendszerParameterek_Load">
                <ContentTemplate>
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeRendszerParameterek" runat="server" TargetControlID="panelRendszerParameterek"
                        CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeRendszerParameterek" CollapseControlID="btnCpeRendszerParameterek"
                        ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                        ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeRendszerParameterek"
                        ExpandedSize="0" ExpandedText="K�dt�rak list�ja" CollapsedText="K�dt�rak list�ja">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="panelRendszerParameterek" runat="server" Width="100%"> 
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                 <asp:GridView id="gridViewRendszerParameterek" runat="server" GridLines="None" BorderWidth="1px" OnRowCommand="gridViewRendszerParameterek_RowCommand" 
                                 OnPreRender="gridViewRendszerParameterek_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                 DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewRendszerParameterek_RowDataBound" OnSorting="gridViewRendszerParameterek_Sorting" AllowPaging="true">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>                                
                                                <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                 &nbsp;&nbsp;
                                                 <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                             </HeaderTemplate>
                                             <ItemTemplate>
                                                 <asp:CheckBox id="check" runat="server" Text='<%# Eval("Nev") %>' CssClass="HideCheckBoxText" />
                                             </ItemTemplate>
                                        </asp:TemplateField>   
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                        <HeaderStyle Width="25px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>  
                                        <asp:BoundField DataField="Nev" HeaderText="Param�ter neve" SortExpression="Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Ertek" HeaderText="Param�ter �rt�ke" SortExpression="Ertek">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="120px" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="linkKarbantarthato" runat="server">Karbantarthat�</asp:LinkButton>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbKarbantarthato" runat="server" Enabled="false"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:BoundField DataField="Note" HeaderText="Megjegyz&#233;s" SortExpression="Note">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                         </asp:BoundField>
                                         <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                   <PagerSettings Visible="False" />
                                 </asp:GridView>
                               </td>
                             </tr>
                         </table>
                      </asp:Panel>   
                  </ContentTemplate>
            </asp:UpdatePanel>
       </td>
     </tr>
   </table>  
</asp:Content>
