using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class RendszerParameterekList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    #region Utils
    static class BoolString
    {
        public const string YesOld = "I";
        public const string Yes = "1";
        public const string NoOld = "N";
        public const string No = "0";
    }

    //Az I/N �rt�k �tkonvert�l�sa checkbox �llapotaira
    protected void SetCheckBox(CheckBox cb, string value)
    {
        if (value == BoolString.Yes || value == BoolString.YesOld)
        {
            cb.Checked = true;
        }
        else
        {
            cb.Checked = false;
        }
    }

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }

    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        bool modosithato = false;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            CheckBox cbKarbantarthato = (CheckBox)selectedRow.FindControl("cbKarbantarthato");
            if (cbKarbantarthato != null)
            {
                modosithato = cbKarbantarthato.Checked;
            }
        }
        return modosithato;
    }
    #endregion

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "RendszerParameterekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_ParameterekSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.RendszerParameterekLisHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("RendszerParameterekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRendszerParameterek.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("RendszerParameterekForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRendszerParameterek.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewRendszerParameterek.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewRendszerParameterek.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewRendszerParameterek.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewRendszerParameterek.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewRendszerParameterek.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewRendszerParameterek;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewRendszerParameterek);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_ParameterekSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            RendszerParameterekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "RendszerParameter" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewRendszerParameterek);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void RendszerParameterekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewRendszerParameterek", ViewState, "KRT_Parameterek.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewRendszerParameterek", ViewState);

        RendszerParameterekGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void RendszerParameterekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_ParameterekSearch search = (KRT_ParameterekSearch)Search.GetSearchObject(Page, new KRT_ParameterekSearch());
     
        search.OrderBy = Search.GetOrderBy("gridViewRendszerParameterek", ViewState, SortExpression, SortDirection);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(gridViewRendszerParameterek, res,ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewRendszerParameterek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbKarbantarthato = (CheckBox)e.Row.FindControl("cbKarbantarthato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            SetCheckBox(cbKarbantarthato, drw["Karbantarthato"].ToString());
        }

        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewRendszerParameterek_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeRendszerParameterek);
        ListHeader1.RefreshPagerLabel();
    }
    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        RendszerParameterekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeRendszerParameterek);
        RendszerParameterekGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewRendszerParameterek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewRendszerParameterek, selectedRowNumber, "check");

            string id = gridViewRendszerParameterek.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewRendszerParameterek_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //modos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(gridViewRendszerParameterek);
            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("RendszerParameterekForm.aspx"
                ,CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                ,Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRendszerParameterek.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("RendszerParameterekForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelRendszerParameterek.ClientID);
            
            string tableName = "Krt_Parameterek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelRendszerParameterek.ClientID);

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelRendszerParameterek_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    RendszerParameterekGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewRendszerParameterek));                    
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedRendszerParameter();
            RendszerParameterekGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedRendszerParameter()
    {

        if (FunctionRights.GetFunkcioJog(Page, "RendszerParameterInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewRendszerParameterek, EErrorPanel1, ErrorUpdatePanel);

            KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedRendszerParameterRecords();
                RendszerParameterekGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedRendszerParameterRecords();
                RendszerParameterekGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedRendszerParameterek();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedRendszerParameterRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewRendszerParameterek, "KRT_RendszerParameterek"
            , "RendszerParameterLock", "RendszerParameterForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedRendszerParameterRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewRendszerParameterek, "KRT_RendszerParameterek"
            , "RendszerParameterLock", "RendszerParameterForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewRendszerParameterek -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedRendszerParameterek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewRendszerParameterek, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_RendszerParameterek");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewRendszerParameterek_Sorting(object sender, GridViewSortEventArgs e)
    {
        RendszerParameterekGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewRendszerParameterek", ViewState, e.SortExpression));
    }

    #endregion

}