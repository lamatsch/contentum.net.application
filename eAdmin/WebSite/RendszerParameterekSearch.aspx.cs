using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class RendszerParameterekSearch : Contentum.eUtility.UI.PageBase
{

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        static public bool Contain(string value)
        {
            if ((value == Yes) || (value == No) || (value == NotSet))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private Type _type = typeof(KRT_ParameterekSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_ParameterekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_ParameterekSearch)Search.GetSearchObject(Page, new KRT_ParameterekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_ParameterekSearch _KRT_ParameterekSearch = (KRT_ParameterekSearch)searchObject;

        if (_KRT_ParameterekSearch != null)
        {
            textNev.Text = _KRT_ParameterekSearch.Nev.Value;
            textErtek.Text = _KRT_ParameterekSearch.Ertek.Value;

            if (BoolString.Contain(_KRT_ParameterekSearch.Karbantarthato.Value))
            {
                ddownKarbantarthato.SelectedValue = _KRT_ParameterekSearch.Karbantarthato.Value;
            }
            else
            {
                ddownKarbantarthato.SelectedValue = BoolString.NotSet;
            }

            //megjegyz�s kiolvas�sa session-b�l
            string note = "";
            if (_KRT_ParameterekSearch.WhereByManual != "")
            {
                note = (string)(Session["note"] ?? "");
            }
            textNote.Text = note;

            Ervenyesseg_SearchFormComponent1.SetDefault(
               _KRT_ParameterekSearch.ErvKezd, _KRT_ParameterekSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_ParameterekSearch SetSearchObjectFromComponents()
    {
        KRT_ParameterekSearch _KRT_ParameterekSearch = (KRT_ParameterekSearch)SearchHeader1.TemplateObject;
        
        if (_KRT_ParameterekSearch == null)
        {
            _KRT_ParameterekSearch = new KRT_ParameterekSearch();
        }

        _KRT_ParameterekSearch.Nev.Value = textNev.Text;
        _KRT_ParameterekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _KRT_ParameterekSearch.Ertek.Value = textErtek.Text;
        _KRT_ParameterekSearch.Ertek.Operator = Search.GetOperatorByLikeCharater(textErtek.Text);
        _KRT_ParameterekSearch.Karbantarthato.Value = ddownKarbantarthato.SelectedValue;
        if (ddownKarbantarthato.SelectedValue != BoolString.NotSet)
        {
            _KRT_ParameterekSearch.Karbantarthato.Operator = Query.Operators.equals;
        }
        else
        {
            _KRT_ParameterekSearch.Karbantarthato.Operator = "";
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_ParameterekSearch.ErvKezd, _KRT_ParameterekSearch.ErvVege);

        //a megjegyz�sek k�z�tti keres�st manu�lisan lehet megoldani
        string note = textNote.Text;
        if (note != "")
        {
            Session["note"] = note;
            string noteOperator = Search.GetOperatorByLikeCharater(note);
            if (noteOperator == Query.Operators.like)
            {
                note = note.Replace("*", "%");
            }
            _KRT_ParameterekSearch.WhereByManual = " AND Note " + noteOperator + " '" + note + "'";
        }

        return _KRT_ParameterekSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_ParameterekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()) && searchObject.WhereByManual == "")
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }        
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_ParameterekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_ParameterekSearch();
    }


}
