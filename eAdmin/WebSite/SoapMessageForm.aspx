﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
                       AutoEventWireup="true"
                       CodeFile="SoapMessageForm.aspx.cs"
                       Inherits="SoapMessageForm"
                       Title="Rendszerközti üzenetek"
                       ValidateRequest ="false"
     %>      
             
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
             
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>                 

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
	<uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Rendszerközti üzenetek" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td style="text-align: left; vertical-align: top; width: 100%;">
				<asp:Panel ID="EFormPanel" runat="server">
                                              <%-- OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged" --%>
					<ajaxToolkit:TabContainer ID="FormTabContainer"
					                          runat="server"
                                              Width="100%"
                                              OnActiveTabChanged="FormTabContainer_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged">

                        <%-- Page 1 --%>
						<ajaxToolkit:TabPanel ID="TabPage1Panel" runat="server" TabIndex="0">
							<HeaderTemplate>
								<asp:Label ID="TabPage1PanelHeader" runat="server" Text="Általános"></asp:Label>
							</HeaderTemplate>
							<ContentTemplate>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>                                                          
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label id="labelForrasrendszer" runat="server" Text="Forrásrendszer:" />
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <uc3:RequiredTextBox id="ForrasRendszer_RequiredTextBox" runat="server" MaxLength="20" Validate="false" />
                                            </td>
                                        </tr>
                                        <tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                            <td class="mrUrlapCaption" runat="server">
                                                <asp:Label ID="Label2" runat="server" Text="Célrendszer:"></asp:Label></td>
                                            <td class="mrUrlapMezo" runat="server">
                                                <asp:TextBox ID="CelRendszer_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Label1" runat="server" Text="Futtató felhasználó neve:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="FuttatoFelhasznalo_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                        </tr>                            
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label14" runat="server" Text="Webszervíz hívás címe:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Url" runat="server" CssClass="mrUrlapInput"  Width="390px"></asp:TextBox></td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Label16" runat="server" Text="Metódus neve:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="MetodusNev_TextBox" runat="server" CssClass="mrUrlapInput" ></asp:TextBox></td>
                                        </tr>                            
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Label3" runat="server" Text="Hibaüzenet szövege:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Hibauzenet_TextBox" runat="server" CssClass="mrUrlapInput" ></asp:TextBox></td>
                                        </tr>                               
                                        <tr class ="urlapSor">
                                             <td class="mrUrlapCaption">
                                              Belső üzenet : </td>
                                            <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rbListLocal" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                            </asp:RadioButtonList>   
                                             </td>                      
                                         </tr>
                                         <tr class ="urlapSor">
                                             <td class="mrUrlapCaption">
                                              Sikeres : </td>
                                            <td class="mrUrlapMezo">
                                            <asp:RadioButtonList ID="rbListSikeres" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                            </asp:RadioButtonList>   
                                             </td>                      
                                         </tr>
                                         <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                              Érvényesség : </td>
                                            <td class="mrUrlapMezo">
                                                &nbsp;<uc6:ErvenyessegCalendarControl ID="Ervenyesseg_CalendarControl"
                                                    runat="server"></uc6:ErvenyessegCalendarControl>
                                                &nbsp;</td>
                                        </tr>                            
                                    </tbody>
                                </table>    
							</ContentTemplate>
						</ajaxToolkit:TabPanel>						
						<%-- Page 2 --%>
						<ajaxToolkit:TabPanel ID="TabPage2Panel" runat="server" Enabled="true" TabIndex="1">
							<HeaderTemplate>
							    <asp:UpdatePanel ID="LabelUpdatePanelPage2" runat="server">
							        <ContentTemplate>							
								        <asp:Label ID="TabPage2PanelHeader" runat="server" Text="Kérés"></asp:Label>
								    </ContentTemplate>
								</asp:UpdatePanel>
							</HeaderTemplate>                                                        
							<ContentTemplate>         
                                <table>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lblKeresFogadas" runat="server" Text="Kérés fogadásának időpontja:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="tbKeresFogadas" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>                                                                        
                                    <td> <asp:ImageButton ID="DownloadRequest" runat="server" CausesValidation="false" ImageUrl="~/images/hu/trapezgomb/letoltes_trap.jpg" Visible="false" OnClick="DownloadButton_OnClick"
                                onmouseover="swapByName(this.id,'letoltes_trap2.jpg')" onmouseout="swapByName(this.id,'letoltes_trap.jpg')"  CommandName ="Request"/></td>
                                 </tr> 
                                    <tr>						
                                      <asp:TextBox id="RequestData_TextBox" readonly="true"  Width ="100%" runat="server" AutoPostBack="false" TextMode="MultiLine" ></asp:TextBox>
                                    </tr>

                                    </table>                                              
							</ContentTemplate>
						</ajaxToolkit:TabPanel>

                        <%-- Page 3 --%>
						<ajaxToolkit:TabPanel ID="TabPage3Panel" runat="server" Enabled="true" TabIndex="1">
							<HeaderTemplate>
							    <asp:UpdatePanel ID="LabelUpdatePanelPage3" runat="server">
							        <ContentTemplate>							
								        <asp:Label ID="TabPage3PanelHeader" runat="server" Text="Válasz"></asp:Label>
								    </ContentTemplate>
								</asp:UpdatePanel>
							</HeaderTemplate>
							<ContentTemplate>
                                     <table>
                                          <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="lblValaszKuldes" runat="server" Text="Válasz küldésének időpontja:"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="tbValaszKuldes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                </td>
                                                  <td> <asp:ImageButton ID="DownloadResponse" runat="server" CausesValidation="false" ImageUrl="~/images/hu/trapezgomb/letoltes_trap.jpg" Visible="false" OnClick="DownloadButton_OnClick"
                                onmouseover="swapByName(this.id,'letoltes_trap2.jpg')" onmouseout="swapByName(this.id,'letoltes_trap.jpg')"  CommandName ="Response"/></td>
                                           </tr> 
                                                         <tr>
								<asp:TextBox runat="server" TextMode="MultiLine" Width="100%" readonly="true" ID ="ResponseData_TextBox"></asp:TextBox>                                
                                                         
                                                             </tr>                                         
                                         </table>        
							</ContentTemplate>
						</ajaxToolkit:TabPanel>
						
					</ajaxToolkit:TabContainer>
				</asp:Panel>				
			</td>
		</tr>
	</table>
    <uc2:FormFooter ID="FormFooter1" runat="server" />	
</asp:Content>
