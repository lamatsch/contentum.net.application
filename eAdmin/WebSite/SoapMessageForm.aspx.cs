﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using System.Collections.Specialized;

public partial class SoapMessageForm : Contentum.eUtility.UI.PageBase
{
    private string Command { get; set; }
    private string Startup { get; set; }
    protected string FunkcioBase { get; set; }
    private BaseDocument BaseObject { get; set; }
    private string ParentId { get; set; }
    private KRT_SoapMessageLog ParentObject { get; set; }

    Contentum.eUtility.PageView pageView = null;

    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, FormTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(FormTabContainer);
                
        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        string command = Request.QueryString.Get(CommandName.Command);

        Command = string.IsNullOrEmpty(command) ? "View" : command ;
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        FunkcioBase = "SoapMessage";
        if (!IsPostBack)
        {
            FillRadioList();
            
        }


        if (Command == CommandName.DesignView)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
            compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
            FormFooter1.Controls.Add(compSelector);            
        }
        else
        {
            // ha Modify módban próbáljuk megnyitni, de csak View joga van, átirányítjuk, ha egyik sincs, hiba)
            if (Command == CommandName.Modify
                && !FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.Modify)
                && FunctionRights.GetFunkcioJog(Page, FunkcioBase + CommandName.View))
            {
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
            }
            else
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioBase + Command);
            }
        }

        SetEnabledAllTabsByFunctionRights();
        ParentId= Request.QueryString.Get(QueryStringVars.Id);

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            
            if (String.IsNullOrEmpty(ParentId))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                if (!IsPostBack)
                {
                    KRT_SoapMessageLog record = GetDataObject(ParentId);
                    LoadComponentsFromBusinessObject(record);
                    ParentObject = record;
                    BaseObject = record.Base;                    
                    CheckRights();
                    DisableTabsByMissingObjectRights(ParentId);
                }
            }
        }

        InitializePageTabs();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (!IsPostBack)
        {
            // Default panel beallitasa
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);

            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in FormTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        FormTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    FormTabContainer.ActiveTab = TabPage1Panel;
                }
            }
            else
            {
                FormTabContainer.ActiveTab = TabPage1Panel;
            }
            ActiveTabRefresh(FormTabContainer);
        }

        //Legyen bezár gomb.
        FormFooter1.Command = CommandName.View;
        FormFooter1.ButtonsClick += new
          System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        SetComponentsVisibility();
       
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        
        if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_Close.Visible = false;
        }

    }

    private void CheckRights()
    {
        string linkURL = String.Empty;
        #region Átirányítás View módba vagy ModifyLink beállítása
        if (Command == CommandName.Modify || Command == CommandName.View)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            if (!String.IsNullOrEmpty(BaseObject.Zarolo_id) && BaseObject.Zarolo_id != FelhasznaloProfil.FelhasznaloId(Page))
            {
                Response.Redirect(Page.Request.Url.AbsolutePath);
            }
        }
        else if (Command == CommandName.View)
        {
            string FunkcioBase = null;
            String funkcioJog = FunkcioBase + CommandName.Modify;

            if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
            {
                string qsOriginalParts = QueryStringVars.Id + "=" + ParentId + "&"
                    + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                linkURL = "FormWithTab.aspx?"
                                        + QueryStringVars.Command + "=" + CommandName.Modify
                                        + "&" + qsOriginalParts
                                        + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + FormTabContainer.ClientID + "') + '";
            }


        }
        // ModifyLink beállítása
        JavaScripts.RegisterSetLinkOnForm(Page, (String.IsNullOrEmpty(linkURL) ? false : true), linkURL, FormHeader1.ModifyLink);
    }

    private KRT_SoapMessageLog GetDataObject(string id)
    {
        Contentum.eAdmin.Service.KRT_SoapMessageLogService service = eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();
        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
        ep.Record_Id = id;
        Result result = service.Get(ep);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            TabPage1Panel.Visible = false;
            return null;
        }

        return result.Record as KRT_SoapMessageLog;
    }
    #endregion Átirányítás View módba vagy ModifyLink beállítása


    #region Forms Tab

    protected void FormTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {       
     //   ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }    
    
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        setAllTabsActive(false);
        setActiveTabs();
    }
    


    protected virtual void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    protected virtual void SetEnabledAllTabs(Boolean value)
    {
        TabPage1Panel.Enabled = value;
        TabPage2Panel.Enabled = value;
        TabPage3Panel.Enabled = value;
    }

    protected virtual void SetEnabledAllTabsByFunctionRights()
    {
        string command = string.IsNullOrEmpty(Command) ? CommandName.View : Command;
        TabPage1Panel.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + command);
        TabPage2Panel.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + command);
        TabPage3Panel.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioBase + command);
    }



    public override void Load_ComponentSelectModul()
    {
       // base.Load_ComponentSelectModul();

        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            SetEnabledAllTabs(false);
            FormTabContainer.Tabs[TabPage1Panel.TabIndex].Enabled = true;

            compSelector.Add_ComponentOnClick(TabPage1Panel);
            compSelector.Add_ComponentOnClick(TabPage2Panel);
            compSelector.Add_ComponentOnClick(TabPage3Panel);
            compSelector.Add_ComponentOnClick(ForrasRendszer_RequiredTextBox);
            compSelector.Add_ComponentOnClick(CelRendszer_TextBox);
            compSelector.Add_ComponentOnClick(Url);
            compSelector.Add_ComponentOnClick(FuttatoFelhasznalo_TextBox);
            compSelector.Add_ComponentOnClick(MetodusNev_TextBox);
            compSelector.Add_ComponentOnClick(Hibauzenet_TextBox);
            compSelector.Add_ComponentOnClick(Ervenyesseg_CalendarControl);

            FormFooter1.SaveEnabled = false;
        }
    }

    #endregion

    #region  Page1 Controls

    private void FillRadioList()
    {
        rbListLocal.Items.Clear();
        rbListLocal.Items.Add(new ListItem("Belső üzenet", Constants.Database.Yes));
        rbListLocal.Items.Add(new ListItem("Nem belső üzenet", Constants.Database.No));
        rbListSikeres.Items.Clear();
        rbListSikeres.Items.Add(new ListItem("Sikeres", Constants.Database.Yes));
        rbListSikeres.Items.Add(new ListItem("Nem Sikeres", Constants.Database.No));
    }


    private void SetComponentsVisibility()
    {
        ForrasRendszer_RequiredTextBox.ReadOnly = true;
        CelRendszer_TextBox.ReadOnly = true;
        MetodusNev_TextBox.ReadOnly = true;
        Url.ReadOnly = true;
        Hibauzenet_TextBox.ReadOnly = true;
        FuttatoFelhasznalo_TextBox.ReadOnly = true;
        rbListSikeres.Enabled = false;
        rbListLocal.Enabled = false;
                
        if (Command == CommandName.View
            || Command == CommandName.Modify)
        {
            

        }        

        if (Command == CommandName.View)
        {
            
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_SoapMessageLog krt_soapmessagelog)
    {

        ForrasRendszer_RequiredTextBox.Text = krt_soapmessagelog.ForrasRendszer;
        CelRendszer_TextBox.Text = krt_soapmessagelog.CelRendszer;
        MetodusNev_TextBox.Text = krt_soapmessagelog.MetodusNeve;
        Url.Text = krt_soapmessagelog.Url;
        FuttatoFelhasznalo_TextBox.Text = krt_soapmessagelog.FuttatoFelhasznalo;
        Hibauzenet_TextBox.Text = krt_soapmessagelog.Hiba;


        #region Kérés fogadásának időpontja 
        tbKeresFogadas.Text = krt_soapmessagelog.KeresFogadas;
        tbKeresFogadas.ReadOnly = true;
        #endregion

        #region Kérés küldésének időpontja 
        tbValaszKuldes.Text = krt_soapmessagelog.KeresKuldes;
        tbValaszKuldes.ReadOnly = true;
        #endregion

        #region Belső üzenet?
        rbListLocal.SelectedValue = krt_soapmessagelog.Local;
        #endregion

        #region Sikeres?
        rbListSikeres.SelectedValue = krt_soapmessagelog.Sikeres;
        #endregion
        FormHeader1.Record_Ver = krt_soapmessagelog.Base.Ver;
        
        #region Request Data 
        if (!string.IsNullOrEmpty(krt_soapmessagelog.RequestData)) {         
        RequestData_TextBox.Text = (krt_soapmessagelog.RequestData);
        RequestData_TextBox.Rows = 26;
        }
        #endregion
        
        
        
        #region Response Data 
        if (!string.IsNullOrEmpty(krt_soapmessagelog.ResponseData))
        {
            ResponseData_TextBox.Text = (krt_soapmessagelog.ResponseData);            
            ResponseData_TextBox.Rows = 26;
        }
        #endregion


    }

    // form --> business object
    private KRT_SoapMessageLog GetBusinessObjectFromComponents()
    {
        KRT_SoapMessageLog krt_soapmessagelog = new KRT_SoapMessageLog();
        krt_soapmessagelog.Base.Updated.SetValueAll(false);
        krt_soapmessagelog.Base.Ver = FormHeader1.Record_Ver;
        krt_soapmessagelog.Base.Updated.Ver = true;
        krt_soapmessagelog.Updated.SetValueAll(false);


        if (!String.IsNullOrEmpty(ForrasRendszer_RequiredTextBox.Text))
        {
            krt_soapmessagelog.ForrasRendszer = ForrasRendszer_RequiredTextBox.Text;
            krt_soapmessagelog.Updated.ForrasRendszer = true;
        }

        if (!String.IsNullOrEmpty(CelRendszer_TextBox.Text))
        {
            krt_soapmessagelog.CelRendszer = CelRendszer_TextBox.Text;
            krt_soapmessagelog.Updated.CelRendszer = true;
        }

        if (!String.IsNullOrEmpty(MetodusNev_TextBox.Text))
        {
            krt_soapmessagelog.MetodusNeve = MetodusNev_TextBox.Text;
            krt_soapmessagelog.Updated.MetodusNeve = true;
        }

        if (!String.IsNullOrEmpty(FuttatoFelhasznalo_TextBox.Text))
        {
            krt_soapmessagelog.FuttatoFelhasznalo = FuttatoFelhasznalo_TextBox.Text;
            krt_soapmessagelog.Updated.FuttatoFelhasznalo = true;
        }

        if (!String.IsNullOrEmpty(Hibauzenet_TextBox.Text))
        {
            krt_soapmessagelog.Hiba = Hibauzenet_TextBox.Text;
            krt_soapmessagelog.Updated.Hiba = true;
        }


        if (!String.IsNullOrEmpty(Url.Text))
        {
            krt_soapmessagelog.Url = Url.Text;
            krt_soapmessagelog.Updated.Url = true;
        }
        if (!String.IsNullOrEmpty(RequestData_TextBox.Text))
        {
            krt_soapmessagelog.RequestData = (RequestData_TextBox.Text);
            krt_soapmessagelog.Updated.RequestData = true;
        }
        if (!String.IsNullOrEmpty(ResponseData_TextBox.Text))
        {
            krt_soapmessagelog.ResponseData = (ResponseData_TextBox.Text);
            krt_soapmessagelog.Updated.ResponseData = true;
        }

        #region Kérés fogadásának időpontja 
        //TODO
        #endregion

        #region Kérés küldésének dátuma
        //TODO
        #endregion

        #region Belső üzenet?
        if (!String.IsNullOrEmpty(rbListLocal.SelectedValue))
        {
            krt_soapmessagelog.Local = rbListLocal.SelectedValue;
            krt_soapmessagelog.Updated.Local = true;
        }
        #endregion

        #region Sikeres?
        if (!String.IsNullOrEmpty(rbListSikeres.SelectedValue))
        {
            krt_soapmessagelog.Sikeres = rbListSikeres.SelectedValue;
            krt_soapmessagelog.Updated.Sikeres = true;
        }
        #endregion


        return krt_soapmessagelog;
    }
    protected void DownloadButton_OnClick(object sender,EventArgs e)
    {
        var ib = sender as ImageButton;
        
        string xml = "";
        string fajlnevprefix = "";
        
        if (ib.CommandName.Equals("Request"))
        {
            xml = RequestData_TextBox.Text;
            fajlnevprefix = String.Format("{0}_{1}_{2}", MetodusNev_TextBox.Text, tbKeresFogadas.Text.Replace(":", "_").Replace(".", "").Replace(" ", "_"), "REQUEST");
        }
        else if (ib.CommandName.Equals("Response"))
        {
            xml = ResponseData_TextBox.Text;
            fajlnevprefix = String.Format("{0}_{1}_{2}", MetodusNev_TextBox.Text, tbKeresFogadas.Text.Replace(":", "_").Replace(".", "").Replace(" ", "_"), "RESPONSE");
        }
        if (!String.IsNullOrEmpty(xml))
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.CacheControl = "no-cache";
            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "utf-8";

            string fileName = String.Format("{0}.xml", fajlnevprefix);
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + EncodeFileName(fileName) + "\";");            
            Response.Write(xml);
            Response.End();
        }

    }
    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "SoapMessage" + Command))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_SoapMessageLogService service = eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();
                                KRT_SoapMessageLog krt_xmlexport = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_xmlexport);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
    #endregion

    #region Nem használt metódusok 
    protected void InitializePageTabs() { }
    protected virtual void setActiveTabs() { }
    protected virtual void setAllTabsActive(bool value) { }
    protected virtual void DisableTabsByMissingObjectRights(string id) { }
    #endregion

}
