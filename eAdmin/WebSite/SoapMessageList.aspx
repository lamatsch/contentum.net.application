<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="SoapMessageList.aspx.cs" Inherits="SoapMessageList" Title="XML exportok kezel�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="SoapMessageCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
    <asp:UpdatePanel ID="SoapMessageUpdatePanel" Runat="server" OnLoad="SoapMessageUpdatePanel_Load">
        <ContentTemplate>
            <ajaxToolkit:CollapsiblePanelExtender ID="SoapMessageCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" ExpandControlID="SoapMessageCPEButton" CollapseControlID="SoapMessageCPEButton"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="SoapMessageCPEButton"
                ExpandedSize="0" ExpandedText="Rendszerk�zi �zenetek list�ja" CollapsedText="Rendszerk�zi list�ja">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel ID="Panel1" runat="server">
             <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">     
                <asp:GridView ID="SoapMessageGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                    OnRowCommand="SoapMessageGridView_RowCommand" OnPreRender="SoapMessageGridView_PreRender"
                    OnSorting="SoapMessageGridView_Sorting" OnRowDataBound="SoapMessageGridView_RowDataBound"
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                &nbsp;&nbsp;
                                <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:CommandField>                        
                        <asp:BoundField DataField="ForrasRendszer" HeaderText="Forr�s rendszer" SortExpression="ForrasRendszer">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CelRendszer" HeaderText="C�l rendszer" SortExpression="CelRendszer">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Url" HeaderText="Webszerv�z c�me" SortExpression="Url">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MetodusNeve" HeaderText="Met�dus neve" SortExpression="MetodusNeve">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="KeresFogadas" HeaderText="K�r�s id�pontja" SortExpression="KeresFogadas">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="KeresKuldes" HeaderText="V�lasz  id�pontja" SortExpression="KeresKuldes">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FuttatoFelhasznalo" HeaderText="H�v� felhaszn�l�" SortExpression="FuttatoFelhasznalo">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText ="K�r�s">
                         <HeaderStyle CssClass="GridViewBorderHeader" Width="40px" />
                         <ItemStyle CssClass="GridViewBoundFieldItemStyle" VerticalAlign="Middle" HorizontalAlign="Center"/>
                         <ItemTemplate>
                            <asp:HyperLink ID="DownloadRequest" Title="K�r�s let�lt�se" 
                                runat="server" ImageUrl="~/images/hu/egyeb/xml.gif" NavigateUrl='<%#"~/SoapMessageList.aspx?Command=DownloadRequest&Id=" + Eval("Id") %>'
                                Visible='<%#!String.IsNullOrEmpty(Eval("RequestData").ToString()) %>'/>
                         </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText ="V�lasz">
                         <HeaderStyle CssClass="GridViewBorderHeader" Width="40px" />
                         <ItemStyle CssClass="GridViewBoundFieldItemStyle" VerticalAlign="Middle" HorizontalAlign="Center"/>
                         <ItemTemplate>
                            <asp:HyperLink ID="DownloadResponse" Title="V�lasz let�lt�se" style="text-align:center"
                                runat="server" ImageUrl="~/images/hu/egyeb/xml.gif" NavigateUrl='<%#"~/SoapMessageList.aspx?Command=DownloadResponse&Id=" + Eval("Id") %>'
                                Visible='<%#!String.IsNullOrEmpty(Eval("RequestData").ToString()) %>'/>
                         </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Sikeres" HeaderText="Sikeres?" SortExpression="Sikeres">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                        
                        <asp:BoundField DataField="Hiba" HeaderText="Hiba�zenet" SortExpression="Hiba">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                        
                        <asp:TemplateField HeaderText="Bels�?" SortExpression="Local">
                                            <HeaderStyle CssClass="GridViewBorderHeader"  Width ="70px"/>
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                            
                                             <ItemTemplate>                                           
                                                 <asp:Label ID="labelLocal" runat="server" Text='<%#(Eval("Local").ToString() == "1") ? "Igen" :"Nem"  %>' />                                                 
                                             </ItemTemplate>
                                        </asp:TemplateField>                        
                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                       </asp:TemplateField>                   
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
                </td>
                                 </tr>
                             </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
           </td>
        </tr>
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
       
    </table>
</asp:Content>

