﻿using Contentum.eAdmin.BaseUtility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

public partial class SoapMessageList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();
    int stat_Index;

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SoapMessageList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        string CommandName = Request.QueryString.Get(QueryStringVars.Command);
        string RequestId = Request.QueryString.Get(QueryStringVars.Id);
        DownloadData(RequestId, CommandName);


        //bernat.laszlo added
        stat_Index = 0;

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region BLG_2936
        //LZS - Excel Export (Grid)
        //Excel export ikon láthatóságának beállítása ’true’ értékre.
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        #endregion


        ListHeader1.HeaderLabel = "Rendszerközti üzenetek.";
        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = "KRT_SoapMessageLogSearch";
        ListHeader1.SearchObjectType = typeof(KRT_SoapMessageLog);
        // A baloldali, alapból invisible ikonok megjelenítése:
        //ListHeader1.PrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
        ListHeader1.NewVisible = false;
        JavaScripts.RegisterPopupWindowClientScript(Page);
        //JavaScripts.RegisterActiveTabChangedClientScript(Page, Contaer);
        //ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("SoapMessageSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SoapMessageUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("SoapMessageForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, SoapMessageUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(SoapMessageGridView.ClientID);
        //ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraIktatokonyvekListajaPrintForm.aspx");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(SoapMessageGridView.ClientID);

        /*ListHeader1.IktatokonyvLezarasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + SoapMessageGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";*/

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(SoapMessageGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(SoapMessageGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(SoapMessageGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, SoapMessageUpdatePanel.ClientID, "", true);

        //SelectedRecordId
        ListHeader1.AttachedGridView = SoapMessageGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(SoapMessageGridView);


        Search.SetIdsToSearchObject(Page, "Id", new KRT_SoapMessageLog(), "KRT_SoapMessageLogSearch");

        if (!IsPostBack) SoapMessageGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.Lock);
        // A BLG alapján ezeknek nincs értelme. 
        ListHeader1.ModifyVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;

        #region BLG_2936
        //LZS
        //Excel export ikon aktiválása a funkciójog alapján.
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "SoapMessage" + CommandName.ExcelExport);
        #endregion

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(SoapMessageGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
        }
    }

    #endregion

    #region Master List

    protected void SoapMessageGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SoapMessageGridView", ViewState, "Id");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SoapMessageGridView", ViewState);

        SoapMessageGridViewBind(sortExpression, sortDirection);
    }

    protected void SoapMessageGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        //Object o = SoapMessageGridView.DataSource;
        UI.ClearGridViewRowSelection(SoapMessageGridView);

        KRT_SoapMessageLogService service = eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_SoapMessageLogSearch search = new KRT_SoapMessageLogSearch();
        if (!IsPostBack)
        {
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, "KRT_SoapMessageLogSearch"))
            {
                //default search object
                // sessionbe mentés
                search.KeresFogadas.Value = DateTime.Now.Subtract(new TimeSpan(24, 0, 0)).ToString();
                search.KeresFogadas.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                //BUG 7061 - Hozzuk az utolsó 1 nap 1 óra helyett , vmint ne szűrjünk csak külsőre.
                //search.Local.Value = Constants.Database.No;
                //search.Local.Operator = Contentum.eQuery.Query.Operators.equals;

                Search.SetSearchObject_CustomSessionName(Page, search, "KRT_SoapMessageLogSearch");
            }
        }
        search = (KRT_SoapMessageLogSearch)Search.GetSearchObject_CustomSessionName
            (Page, new KRT_SoapMessageLogSearch(), "KRT_SoapMessageLogSearch");

        search.OrderBy = Search.GetOrderBy("SoapMessageGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(SoapMessageGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    protected void SoapMessageGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = SoapMessageGridView.PageIndex;

        SoapMessageGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = SoapMessageGridView.PageCount;

        if (prev_PageIndex != SoapMessageGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, SoapMessageCPE);
            SoapMessageGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SoapMessageCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(SoapMessageGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        SoapMessageGridViewBind();
    }

    protected void SoapMessageGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SoapMessageGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("SoapMessageForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, SoapMessageUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("SoapMessageForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SoapMessageUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "KRT_SoapMessageLog";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, SoapMessageUpdatePanel.ClientID);

        }
    }

    protected void SoapMessageUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    SoapMessageGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        #region BLG_2936
        //LZS - Excel Export (Grid)
        //ListGrid exportálása Excel  file-ba, a szükséges oszlopszélességeket paraméterként átadva.
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            ex_Export.SaveGridView_ToExcel(exParam, SoapMessageGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, 1.1d, browser);
        }
        #endregion

        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedSoapMessage();
            SoapMessageGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedSoapMessageRecords();
                SoapMessageGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedSoapMessageRecords();
                SoapMessageGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedSoapMessage();
                break;
        }
    }

    private void LockSelectedSoapMessageRecords()
    {
        LockManager.LockSelectedGridViewRecords(SoapMessageGridView, "KRT_SoapMessage"
            , "SoapMessageLock", "SoapMessageForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedSoapMessageRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(SoapMessageGridView, "KRT_SoapMessage"
            , "SoapMessageLock", "SoapMessageForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Törli a SoapMessageGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedSoapMessage()
    {
        if (FunctionRights.GetFunkcioJog(Page, "SoapMessageInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SoapMessageGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_SoapMessageLogService service = eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a SoapMessageGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedSoapMessage()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(SoapMessageGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_SoapMessage");
        }
    }

    protected void SoapMessageGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SoapMessageGridViewBind(e.SortExpression, UI.GetSortToGridView("SoapMessageGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }


    private void DownloadData(string ExportId, string Command)
    {
        if (string.IsNullOrEmpty(Command))
            return;

        if (!String.IsNullOrEmpty(ExportId))
        {
            KRT_SoapMessageLogService service = eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();
            ExecParam xpm = UI.SetExecParamDefault(this.Page);
            xpm.Record_Id = ExportId;
            Result res = service.Get(xpm);

            if (res.IsError)
            {
                throw new Contentum.eUtility.ResultException(res);
            }

            KRT_SoapMessageLog _SoapMessageLog = (KRT_SoapMessageLog)res.Record;
            string xml = "";
            string fajlnevprefix = "";

            if (Command == "DownloadRequest")
            {
                xml = _SoapMessageLog.RequestData;
                fajlnevprefix = String.Format("{0}_{1}_{2}", _SoapMessageLog.MetodusNeve.ToUpper(), _SoapMessageLog.KeresFogadas.ToString().Replace(":", "_").Replace(".", "").Replace(" ", "_"), "REQUEST");
            }
            else if (Command == "DownloadResponse")
            {
                xml = _SoapMessageLog.ResponseData;
                fajlnevprefix = String.Format("{0}_{1}_{2}", _SoapMessageLog.MetodusNeve.ToUpper(), _SoapMessageLog.KeresKuldes.ToString().Replace(":", "_").Replace(".", "").Replace(" ", "_"), "RESPONSE");
            }

            if (!String.IsNullOrEmpty(xml))
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                Response.CacheControl = "no-cache";
                Response.HeaderEncoding = System.Text.Encoding.UTF8;
                Response.Charset = "utf-8";

                string fileName = String.Format("{0}.xml", fajlnevprefix);
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + EncodeFileName(fileName) + "\";");
                Response.Write(xml);
                Response.End();
            }
        }
    }

    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }

    #endregion


    #region Detail Tab


    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {/*
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(SoapMessageGridView));
            ActiveTabRefreshOnClientClicks();
        }*/
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (SoapMessageGridView.SelectedIndex == -1)
        {
            return;
        }
        string IraIktatoKonyvId = UI.GetGridViewSelectedRecordId(SoapMessageGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, IraIktatoKonyvId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string IraIktatoKonyvId)
    {/*
        switch (sender.ActiveTab.TabIndex)
        {
            
            case 0:
                JogosultakGridViewBind(IraIktatoKonyvId);
                JogosultakPanel.Visible = true;
                break;
        }*/
    }


    private void ActiveTabClear()
    {
        /*
        switch (TabContainer1.ActiveTabIndex)
        {
            
            case 0:
                ui.GridViewClear(JogosultakGridView);
                break;
        }*/
    }

    protected void SetDetailRowCountOnAllTab(string RowCount)
    {
        /*
        Session[Constants.DetailRowCount] = RowCount;
                
        JogosultakSubListHeader.RowCount = RowCount;*/
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        /*
        if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
        {
            JogosultakGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JogosultakGridView), UI.GetGridViewSelectedRecordId(SoapMessageGridView));
        }*/
    }


    #endregion
    /*
    #region Jogosultak Detail

    private void JogosultakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Jogosultak();
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(SoapMessageGridView));
        }
    }

    protected void JogosultakGridViewBind(string IraIktatoKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);

        JogosultakGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
    }

    protected void JogosultakGridViewBind(string SoapMessageId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(SoapMessageId))
        {
            
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            
            Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
            search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);
                        
            Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, SoapMessageId,search);

            UI.GridViewFill(JogosultakGridView, res, JogosultakSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
        }
        else
        {
            ui.GridViewClear(JogosultakGridView);
        }
    }

    private void deleteSelected_Jogosultak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "SoapMessageInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, EErrorPanel1, ErrorUpdatePanel);
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:                    
                    ActiveTabRefreshDetailList(JogosultakTabPanel);
                    break;
            }
        }
    }

    protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
        }
    }

    private void JogosultakGridView_RefreshOnClientClicks(string id, string ExportId)
    {
        if (!String.IsNullOrEmpty(id))
        {
            JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("SoapMessageJogosultakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                  + "&" + QueryStringVars.SoapMessageId + "=" + ExportId
                , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID);
        }
    }

    protected void JogosultakGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JogosultakGridView.PageIndex;

        JogosultakGridView.PageIndex = JogosultakSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != JogosultakGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, JogosultakCPE);
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(SoapMessageGridView));
        }
        else
        {
            UI.GridViewSetScrollable(JogosultakSubListHeader.Scrollable, JogosultakCPE);
        }
        JogosultakSubListHeader.PageCount = JogosultakGridView.PageCount;
        JogosultakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JogosultakGridView);
    }


    void JogosultakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(JogosultakSubListHeader.RowCount);
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(SoapMessageGridView));
    }

    protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(SoapMessageGridView)
            , e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    }

    #endregion
    */
    #region Statusz értékének beállítása    

    protected void SoapMessageGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (stat_Index == 0)
        {
            stat_Index = GetColumnIndexByName(e.Row, "Sikeres");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[stat_Index].Text = e.Row.Cells[stat_Index].Text.Equals("1") ? "Igen" : "Nem";
        }
    }

    protected int GetColumnIndexByName(GridViewRow row, string columnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
                if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                    break;
            columnIndex++;
        }
        return columnIndex;
    }
    #endregion


}

