<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="SoapMessageSearch.aspx.cs" Inherits="SoapMessageSearch" Title="Rendszerk�zti �zenetek keres�se" %>

<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>


<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<%@ Register Src="Component/DatumIdoIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="di" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>  
                             <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="labelKeresDatuma" runat="server" Text="K�r�s fogad�s�nak id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <di:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>                       
                            <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="labelKeresValaszDatuma" runat="server" Text="K�r�s/v�lasz k�ld�s�nek id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <di:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl2"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>                           
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label id="labelForrasrendszer" runat="server" Text="Forr�srendszer:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox id="ForrasRendszer_RequiredTextBox" runat="server" MaxLength="20" Validate="false" />
                                </td>
                            </tr>
                            <tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="C�lrendszer:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="CelRendszer_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor" visible="false">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label1" runat="server" Text="Futtat� felhaszn�l� neve:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="FuttatoFelhasznalo_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>                            

                                <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label14" runat="server" Text="Webszerv�z h�v�s c�me:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Url" runat="server" CssClass="mrUrlapInput"  Width="390px"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label16" runat="server" Text="Met�dus neve:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="MetodusNev_TextBox" runat="server" CssClass="mrUrlapInput" ></asp:TextBox></td>
                            </tr>                            
                        <tr class="urlapSor" visible="false">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label3"  runat="server" Text="Hiba�zenet sz�vege:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Hibauzenet_TextBox" runat="server" CssClass="mrUrlapInput" ></asp:TextBox></td>
                            </tr>        
                       
                            <tr class ="urlapSor">
                                 <td class="mrUrlapCaption">
                                  Bels� �zenet : </td>
                                <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbListLocal" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>   
                                 </td>                      
                              </tr>
                             <tr class ="urlapSor">
                                 <td class="mrUrlapCaption">
                                  Sikeres : </td>
                                <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbListSikeres" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>   
                                 </td>                      
                              </tr>

                            <tr class="urlapSor">
                                <td colspan="2">
                                    &nbsp;<uc9:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1"
                                        runat="server"></uc9:Ervenyesseg_SearchFormComponent>
                                    &nbsp;</td>
                            </tr>
                            <tr class="urlapSor">
                                <td colspan="2">
                                    <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                        runat="server"></uc10:TalalatokSzama_SearchFormComponent>
                                    &nbsp; &nbsp; &nbsp;&nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;</td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
