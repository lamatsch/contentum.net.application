using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Web.UI.WebControls;

public partial class SoapMessageSearch : Contentum.eUtility.UI.PageBase

{
    private Type _type = typeof(EREC_IraIktatoKonyvekSearch);
    private const string kcsKod_IKT_SZAM_OSZTAS = "IKT_SZAM_OSZTAS";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.CustomTemplateTipusNev = "KRT_SoapMessageLogSearch";
        SearchHeader1.TemplateObjectType = _type;
    }
    private void FillRadioList()
    {
        rbListLocal.Items.Clear();
        rbListLocal.Items.Add(new ListItem("Bels� �zenet", Constants.Database.Yes));
        rbListLocal.Items.Add(new ListItem("Nem bels� �zenet", Constants.Database.No));
        rbListLocal.Items.Add(new ListItem("�sszes", ""));
        rbListSikeres.Items.Clear();
        rbListSikeres.Items.Add(new ListItem("Sikeres", Constants.Database.Yes));
        rbListSikeres.Items.Add(new ListItem("Nem Sikeres", Constants.Database.No));
        rbListSikeres.Items.Add(new ListItem("�sszes", ""));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = "Rendszerk�zti �zenetek keres�se";

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);
       

        if (!IsPostBack)
        {
            FillRadioList();
            KRT_SoapMessageLogSearch searchObject = null;
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, "KRT_SoapMessageLogSearch"))
            {
                
                searchObject = (KRT_SoapMessageLogSearch)Search.GetSearchObject_CustomSessionName
                            (Page, new KRT_SoapMessageLogSearch(), "KRT_SoapMessageLogSearch");
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_SoapMessageLogSearch krt_soapmessagelogsearch = null;
        if (searchObject != null) krt_soapmessagelogsearch = (KRT_SoapMessageLogSearch)searchObject;

        if (krt_soapmessagelogsearch != null)
        {
        
            ForrasRendszer_RequiredTextBox.Text = krt_soapmessagelogsearch.ForrasRendszer.Value;
            CelRendszer_TextBox.Text = krt_soapmessagelogsearch.CelRendszer.Value;
            MetodusNev_TextBox.Text = krt_soapmessagelogsearch.MetodusNeve.Value;
            Url.Text = krt_soapmessagelogsearch.Url.Value;
            FuttatoFelhasznalo_TextBox.Text = krt_soapmessagelogsearch.FuttatoFelhasznalo.Value;
            Hibauzenet_TextBox.Text = krt_soapmessagelogsearch.Hiba.Value;
            

            #region K�r�s fogad�s�nak id�pontja 
            switch (krt_soapmessagelogsearch.KeresFogadas.Operator)
            {
                case Query.Operators.between:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = krt_soapmessagelogsearch.KeresFogadas.Value;
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = krt_soapmessagelogsearch.KeresFogadas.ValueTo;
                    break;
                case Query.Operators.greaterorequal:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = krt_soapmessagelogsearch.KeresFogadas.Value;
                    break;
                case Query.Operators.lessorequal:
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = krt_soapmessagelogsearch.KeresFogadas.Value;
                    break;
                default:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = string.Empty;
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = string.Empty;
                    break;
            }
            #endregion

            #region K�r�s k�ld�s�nek id�pontja 
            switch (krt_soapmessagelogsearch.KeresKuldes.Operator)
            {
                case Query.Operators.between:
                    DatumIntervallum_SearchCalendarControl2.DatumKezdValue = krt_soapmessagelogsearch.KeresKuldes.Value;
                    DatumIntervallum_SearchCalendarControl2.DatumVegeValue = krt_soapmessagelogsearch.KeresKuldes.ValueTo;
                    break;
                case Query.Operators.greaterorequal:
                    DatumIntervallum_SearchCalendarControl2.DatumKezdValue = krt_soapmessagelogsearch.KeresKuldes.Value;
                    break;
                case Query.Operators.lessorequal:
                    DatumIntervallum_SearchCalendarControl2.DatumVegeValue = krt_soapmessagelogsearch.KeresKuldes.Value;
                    break;
                default:
                    DatumIntervallum_SearchCalendarControl2.DatumKezdValue = string.Empty;
                    DatumIntervallum_SearchCalendarControl2.DatumVegeValue = string.Empty;
                    break;
            }
            #endregion

            #region Bels� �zenet?
            rbListLocal.SelectedValue  = krt_soapmessagelogsearch.Local.Value;
            #endregion

            #region Sikeres?
            rbListSikeres.SelectedValue = krt_soapmessagelogsearch.Sikeres.Value;
            #endregion

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_soapmessagelogsearch.ErvKezd, krt_soapmessagelogsearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private KRT_SoapMessageLogSearch SetSearchObjectFromComponents()
    {
        KRT_SoapMessageLogSearch krt_soapmessagelogsearch = (KRT_SoapMessageLogSearch)SearchHeader1.TemplateObject;
        if (krt_soapmessagelogsearch == null)
        {
            krt_soapmessagelogsearch = new KRT_SoapMessageLogSearch();
        }

        
        if (!String.IsNullOrEmpty(ForrasRendszer_RequiredTextBox.Text))
        {
            krt_soapmessagelogsearch.ForrasRendszer.Value = ForrasRendszer_RequiredTextBox.Text;
            krt_soapmessagelogsearch.ForrasRendszer.Operator = Search.GetOperatorByLikeCharater(ForrasRendszer_RequiredTextBox.Text);
        }

        if (!String.IsNullOrEmpty(CelRendszer_TextBox.Text))
        {
            krt_soapmessagelogsearch.CelRendszer.Value = CelRendszer_TextBox.Text;
            krt_soapmessagelogsearch.CelRendszer.Operator = Search.GetOperatorByLikeCharater(CelRendszer_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(MetodusNev_TextBox.Text))
        {
            krt_soapmessagelogsearch.MetodusNeve.Value = MetodusNev_TextBox.Text;
            krt_soapmessagelogsearch.MetodusNeve.Operator = Search.GetOperatorByLikeCharater(MetodusNev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(FuttatoFelhasznalo_TextBox.Text))
        {
            krt_soapmessagelogsearch.FuttatoFelhasznalo.Value = FuttatoFelhasznalo_TextBox.Text;
            krt_soapmessagelogsearch.FuttatoFelhasznalo.Operator = Search.GetOperatorByLikeCharater(FuttatoFelhasznalo_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Hibauzenet_TextBox.Text))
        {
            krt_soapmessagelogsearch.Hiba.Value = Hibauzenet_TextBox.Text;
            krt_soapmessagelogsearch.Hiba.Operator = Search.GetOperatorByLikeCharater(Hibauzenet_TextBox.Text);
        }


        if (!String.IsNullOrEmpty(Url.Text))
        {
            krt_soapmessagelogsearch.Url.Value = Url.Text;
            krt_soapmessagelogsearch.Url.Operator = Search.GetOperatorByLikeCharater(Url.Text);
        }

        #region K�r�s fogad�s�nak id�pontja 
        if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumKezdValue))
        {
            krt_soapmessagelogsearch.KeresFogadas.Value = DatumIntervallum_SearchCalendarControl1.DatumKezdValue;
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumVegeValue))
            {
                krt_soapmessagelogsearch.KeresFogadas.ValueTo = DatumIntervallum_SearchCalendarControl1.DatumVegeValue;
                krt_soapmessagelogsearch.KeresFogadas.Operator = Query.Operators.between;
            }
            else
            {
                krt_soapmessagelogsearch.KeresFogadas.Operator = Query.Operators.greaterorequal;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumVegeValue))
            {
                krt_soapmessagelogsearch.KeresFogadas.Value = DatumIntervallum_SearchCalendarControl1.DatumVegeValue;
                krt_soapmessagelogsearch.KeresFogadas.Operator = Query.Operators.lessorequal;
            }
        }
        #endregion

        #region K�r�s k�ld�s�nek d�tuma
        if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl2.DatumKezdValue))
        {
            krt_soapmessagelogsearch.KeresKuldes.Value = DatumIntervallum_SearchCalendarControl2.DatumKezdValue;
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl2.DatumVegeValue))
            {
                krt_soapmessagelogsearch.KeresKuldes.ValueTo = DatumIntervallum_SearchCalendarControl2.DatumVegeValue;
                krt_soapmessagelogsearch.KeresKuldes.Operator = Query.Operators.between;
            }
            else
            {
                krt_soapmessagelogsearch.KeresKuldes.Operator = Query.Operators.greaterorequal;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl2.DatumVegeValue))
            {
                krt_soapmessagelogsearch.KeresKuldes.Value = DatumIntervallum_SearchCalendarControl2.DatumVegeValue;
                krt_soapmessagelogsearch.KeresKuldes.Operator = Query.Operators.lessorequal;
            }
        }
        #endregion

        #region Bels� �zenet?
        if (!String.IsNullOrEmpty(rbListLocal.SelectedValue))
        {
            krt_soapmessagelogsearch.Local.Value = rbListLocal.SelectedValue;
            krt_soapmessagelogsearch.Local.Operator = Query.Operators.equals;
        }
        #endregion

        #region Sikeres?
        if (!String.IsNullOrEmpty(rbListSikeres.SelectedValue))
        {
            krt_soapmessagelogsearch.Sikeres.Value = rbListSikeres.SelectedValue;
            krt_soapmessagelogsearch.Sikeres.Operator = Query.Operators.equals;
        }
        #endregion

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_soapmessagelogsearch.ErvKezd, krt_soapmessagelogsearch.ErvVege);

        return krt_soapmessagelogsearch;
    }



    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added (Mentett tal�lati list�k)
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_SoapMessageLogSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                //Search.RemoveSearchObjectFromSession(Page, _type);
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, "KRT_SoapMessageLogSearch");
            }
            else
            {
                //Search.SetSearchObject(Page, searchObject);
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject_CustomSessionName(Page, searchObject, "KRT_SoapMessageLogSearch");
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_SoapMessageLogSearch GetDefaultSearchObject()
    {
        return new KRT_SoapMessageLogSearch();
    }

}
