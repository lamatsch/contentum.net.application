using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;
using System.Linq;

public partial class Szerepkor_Felhasznalo_Form_Multi : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo_Szerepkor" + Command);
                break;
        }

        
        if (Command == CommandName.Modify)
        {
            string szerepkorId = Request.QueryString.Get(QueryStringVars.SzerepkorId);
            if (!String.IsNullOrEmpty(szerepkorId))
            {
                SzerepkorTextBox1.Id_HiddenField = szerepkorId;
                SzerepkorTextBox1.SetSzerepkorTextBoxById(FormHeader1.ErrorPanel);
                SzerepkorTextBox1.Enabled = false;
            }

            if (!IsPostBack)
            {
                FillFelhasznalokList();
                FillHozzarendeltFelhasznalokList(szerepkorId);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }
   
    // form --> business object
    private KRT_Felhasznalo_Szerepkor GetBusinessObjectFromComponentsForInsert(string felhasznaloId)
    {
        KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = new KRT_Felhasznalo_Szerepkor();
        krt_felhasznalo_szerepkor.Updated.SetValueAll(false);
        krt_felhasznalo_szerepkor.Base.Updated.SetValueAll(false);

        krt_felhasznalo_szerepkor.Szerepkor_Id = SzerepkorTextBox1.Id_HiddenField;
        krt_felhasznalo_szerepkor.Updated.Szerepkor_Id = true;

        krt_felhasznalo_szerepkor.Felhasznalo_Id = felhasznaloId;
        krt_felhasznalo_szerepkor.Updated.Felhasznalo_Id = true;

        krt_felhasznalo_szerepkor.ErvKezd = DateTime.Now.ToString();
        krt_felhasznalo_szerepkor.Updated.ErvKezd = true;

        krt_felhasznalo_szerepkor.ErvVege = new DateTime(4700, 12, 31).ToString();
        krt_felhasznalo_szerepkor.Updated.ErvVege = true;

        krt_felhasznalo_szerepkor.Base.Ver = "1";
        krt_felhasznalo_szerepkor.Base.Updated.Ver = true;

        return krt_felhasznalo_szerepkor;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            compSelector.Add_ComponentOnClick(SzerepkorTextBox1);           
            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + Command))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            List<string> felhasznalokIds = new List<string>();

                            foreach (ListItem item in HozzaRendeltFelhasznalokList.Items)
                            {
                                felhasznalokIds.Add(item.Value);
                            }

                            KRT_Felhasznalo_SzerepkorService felhasznaloSzerepkorokService = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
                            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                            KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
                            szerepkor.Id = SzerepkorTextBox1.Id_HiddenField;
                            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();                            
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.GetAllBySzerepkor(ExecParam, szerepkor, search);

                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                break;
                            }

                            List<string> CurrentFelhasznaloIds = new List<string>();

                            foreach (DataRow row in result.Ds.Tables[0].Rows)
                            {
                                string szerepkorId = row["Id"].ToString();
                                CurrentFelhasznaloIds.Add(szerepkorId);
                            }

                            List<string> AddFelhasznaloIds = new List<string>();
                            List<string> RemovedFelhasznaloIds = new List<string>();

                            foreach (string szerepkorId in felhasznalokIds)
                            {
                                if (!CurrentFelhasznaloIds.Contains(szerepkorId))
                                {
                                    AddFelhasznaloIds.Add(szerepkorId);
                                }
                            }

                            foreach (string szerepkorId in CurrentFelhasznaloIds)
                            {
                                if (!felhasznalokIds.Contains(szerepkorId))
                                {
                                    RemovedFelhasznaloIds.Add(szerepkorId);
                                }
                            }

                            foreach (string felhasznaloId in AddFelhasznaloIds)
                            {
                                KRT_Felhasznalo_Szerepkor krt_felhasznalo_szerepkor = GetBusinessObjectFromComponentsForInsert(felhasznaloId);

                                Result resultInsert = felhasznaloSzerepkorokService.Insert(execParam, krt_felhasznalo_szerepkor);

                                if (!String.IsNullOrEmpty(resultInsert.ErrorCode))
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultInsert);
                                    return;
                                }
                            }

                            var table = result.Ds.Tables[0].AsEnumerable();

                            List<ExecParam> execParams = new List<ExecParam>();
                            for (int i = 0; i < RemovedFelhasznaloIds.Count; i++)
                            {
                                bool modosithato = string.IsNullOrEmpty(table.First(x => x["Id"].ToString() == RemovedFelhasznaloIds[i])["Helyettesites_Id"].ToString());
                                if (modosithato)
                                {
                                    ExecParam newExecParam;
                                    newExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    newExecParam.Record_Id = RemovedFelhasznaloIds[i];
                                    execParams.Add(newExecParam);
                                }
                            }
                            
                            Result invalidateResult = felhasznaloSzerepkorokService.MultiInvalidateWithMegbizasControl(execParams.ToArray());
                            if (!String.IsNullOrEmpty(invalidateResult.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, invalidateResult);
                                break;
                            }

                            if (AddFelhasznaloIds.Count > 0 || RemovedFelhasznaloIds.Count > 0)
                            {
                                /** 
                                 * <FIGYELEM!!!> 
                                 * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                                 * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                                 *  az �sszes felhaszn�l� Session-jeiben)
                                 */
                                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                                /**
                                 * </FIGYELEM>
                                 */
                            }

                            JavaScripts.RegisterCloseWindowClientScript(Page);

                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    void FillFelhasznalokList()
    {
        FelhasznalokList.Items.Clear();

        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.OrderBy = "Nev";
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(FelhasznalokList, result, "Nev", FormHeader1.ErrorPanel, null);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    void FillHozzarendeltFelhasznalokList(string szerepkorId)
    {
        HozzaRendeltFelhasznalokList.Items.Clear();

        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
        szerepkor.Id = szerepkorId;
        KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();
        search.OrderBy = "Felhasznalo_Nev";

        Result result = service.GetAllBySzerepkor(ExecParam, szerepkor, search);

        UI.ListBoxFill(HozzaRendeltFelhasznalokList, result, "Id", "Felhasznalo_Nev", FormHeader1.ErrorPanel, null);
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(FelhasznalokList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = HozzaRendeltFelhasznalokList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                HozzaRendeltFelhasznalokList.Items.Add(item);
            }
        }
    }

    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(HozzaRendeltFelhasznalokList);
        foreach (ListItem item in selectedItems)
        {
            HozzaRendeltFelhasznalokList.Items.Remove(item);
        }
    }

    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillFelhasznalokList();
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillFelhasznalokList();
    }

}
