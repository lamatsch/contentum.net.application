<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="Szerepkor_Funkcio_Form.aspx.cs" Inherits="Szerepkor_Funkcio_Form" %>

<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc8" %>

    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>

<%@ Register Src="Component/SzerepkorTextBox.ascx" TagName="SzerepkorTextBox" TagPrefix="uc9" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,Szerepkor_Funkcio_FormHeaderTitle%>" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px">
                                <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label_Szerepkor" runat="server" Text="Szerepk�r:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc9:SzerepkorTextBox Id="SzerepkorTextBox1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px; height: 30px;">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label2" runat="server" Text="Funkci�:"></asp:Label>&nbsp;</td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <uc8:FunkcioTextBox Id="FunkcioTextBox1" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_Top" style="width: 200px">
                                </td>
                            <td class="mrUrlapMezo">
                                &nbsp;<br />
                                &nbsp;</td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="width: 200px" >
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>



