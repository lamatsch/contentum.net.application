using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Szerepkor_Funkcio_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private void SetViewControls()
    {
        SzerepkorTextBox1.ReadOnly = true;
        FunkcioTextBox1.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        SzerepkorTextBox1.ReadOnly = true;
        FunkcioTextBox1.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Szerepkor_Funkcio" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService(); 
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Szerepkor_Funkcio krt_Szerepkor_Funkcio = (KRT_Szerepkor_Funkcio)result.Record;
                    LoadComponentsFromBusinessObject(krt_Szerepkor_Funkcio);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string szerepkorId = Request.QueryString.Get(QueryStringVars.SzerepkorId);
            if (!String.IsNullOrEmpty(szerepkorId))
            {
                SzerepkorTextBox1.Id_HiddenField = szerepkorId;
                SzerepkorTextBox1.SetSzerepkorTextBoxById(FormHeader1.ErrorPanel);
                //SzerepkorTextBox1.Enabled = false;
                SzerepkorTextBox1.ReadOnly = true;
            }
            string funkcioId = Request.QueryString.Get(QueryStringVars.FunkcioId);
            if (!String.IsNullOrEmpty(funkcioId))
            {
                FunkcioTextBox1.Id_HiddenField = funkcioId;
                FunkcioTextBox1.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);
                //FunkcioTextBox1.Enabled = false;
                FunkcioTextBox1.ReadOnly = true;
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        else if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        
        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Szerepkor_Funkcio krt_szerepkor_funkcio)
    {
        SzerepkorTextBox1.Id_HiddenField = krt_szerepkor_funkcio.Szerepkor_Id;
        SzerepkorTextBox1.SetSzerepkorTextBoxById(FormHeader1.ErrorPanel);
        //SzerepkorTextBox1.Enabled = false;

        FunkcioTextBox1.Id_HiddenField = krt_szerepkor_funkcio.Funkcio_Id;
        FunkcioTextBox1.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);
        //FunkcioTextBox1.Enabled = false;

        ErvenyessegCalendarControl1.ErvKezd = krt_szerepkor_funkcio.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_szerepkor_funkcio.ErvVege;

        FormHeader1.Record_Ver = krt_szerepkor_funkcio.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_szerepkor_funkcio.Base);
    }

    // form --> business object
    private KRT_Szerepkor_Funkcio GetBusinessObjectFromComponents()
    {
        KRT_Szerepkor_Funkcio krt_szerepkor_funkcio = new KRT_Szerepkor_Funkcio();
        krt_szerepkor_funkcio.Updated.SetValueAll(false);
        krt_szerepkor_funkcio.Base.Updated.SetValueAll(false);

        krt_szerepkor_funkcio.Szerepkor_Id = SzerepkorTextBox1.Id_HiddenField;
        krt_szerepkor_funkcio.Updated.Szerepkor_Id = pageView.GetUpdatedByView(SzerepkorTextBox1);

        krt_szerepkor_funkcio.Funkcio_Id = FunkcioTextBox1.Id_HiddenField;
        krt_szerepkor_funkcio.Updated.Funkcio_Id = pageView.GetUpdatedByView(FunkcioTextBox1);

        //krt_szerepkor_funkcio.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_szerepkor_funkcio.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_szerepkor_funkcio.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_szerepkor_funkcio.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_szerepkor_funkcio, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_szerepkor_funkcio.Base.Ver = FormHeader1.Record_Ver;
        krt_szerepkor_funkcio.Base.Updated.Ver = true;

        return krt_szerepkor_funkcio;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(SzerepkorTextBox1);
            compSelector.Add_ComponentOnClick(FunkcioTextBox1);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService();
                            KRT_Szerepkor_Funkcio krt_szerepkor_funkcio = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_szerepkor_funkcio);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                /** 
                                  * <FIGYELEM!!!> 
                                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                                  *  az �sszes felhaszn�l� Session-jeiben)
                                  */
                                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                                /**
                                  * </FIGYELEM>
                                 */

                                //// ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                //JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text);
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                             String recordId = Request.QueryString.Get(QueryStringVars.Id);
                             if (String.IsNullOrEmpty(recordId))
                             {
                                 // nincs Id megadva:
                                 ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                             }
                             else
                             {
                                 KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService();
                                 KRT_Szerepkor_Funkcio krt_szerepkor_funkcio = GetBusinessObjectFromComponents();

                                 ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                 execParam.Record_Id = recordId;

                                 Result result = service.Update(execParam, krt_szerepkor_funkcio);

                                 if (String.IsNullOrEmpty(result.ErrorCode))
                                 {
                                     JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                     JavaScripts.RegisterCloseWindowClientScript(Page);
                                 }
                                 else
                                 {
                                     ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                 }
                             }
                             break;
                        }                        
                }
            }        
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

}
