using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class SzerepkorokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Szerepkor" + Command);
                break;
        }
               

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {                
                KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());                    
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Szerepkorok krt_Szerepkorok = (KRT_Szerepkorok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Szerepkorok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
                
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        //LZS - BUG_4529
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Szerepkorok krt_Szerepkorok)
    {
        Nev.Text = krt_Szerepkorok.Nev;
        //Note_TextBox.Text = krt_Szerepkorok.Base.Note;
        ErvenyessegCalendarControl1.ErvKezd = krt_Szerepkorok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Szerepkorok.ErvVege;

        FormHeader1.Record_Ver = krt_Szerepkorok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_Szerepkorok.Base);

        if (Command == CommandName.View)
        {
            Nev.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private KRT_Szerepkorok GetBusinessObjectFromComponents()
    {
        KRT_Szerepkorok krt_Szerepkorok = new KRT_Szerepkorok();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        krt_Szerepkorok.Updated.SetValueAll(false);
        krt_Szerepkorok.Base.Updated.SetValueAll(false);

        krt_Szerepkorok.Nev = Nev.Text;
        krt_Szerepkorok.Updated.Nev = pageView.GetUpdatedByView(Nev);

        //LZS - BUG_4529
        krt_Szerepkorok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        krt_Szerepkorok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //LZS - BUG_4529
        krt_Szerepkorok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        krt_Szerepkorok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Szerepkorok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_Szerepkorok.Base.Ver = FormHeader1.Record_Ver;
        krt_Szerepkorok.Base.Updated.Ver = true;

        return krt_Szerepkorok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Szerepkor" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            //LZS - BUG_4529
                            if (Convert.ToDateTime(ErvenyessegCalendarControl1.ErvKezd) > Convert.ToDateTime(ErvenyessegCalendarControl1.ErvVege))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIErvVegeErvKezdete_Header, Resources.Error.UIErvVegeErvKezdete);
                                break;
                            }

                            KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
                            KRT_Szerepkorok krt_Szerepkorok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Szerepkorok);
                            
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);                                
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            //LZS - BUG_4529
                            if (Convert.ToDateTime(ErvenyessegCalendarControl1.ErvKezd) > Convert.ToDateTime(ErvenyessegCalendarControl1.ErvVege))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIErvVegeErvKezdete_Header, Resources.Error.UIErvVegeErvKezdete);
                                break;
                            }

                            if (DateTime.Now > Convert.ToDateTime(ErvenyessegCalendarControl1.ErvVege))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIErvVegeErvKezdete_Header, Resources.Error.UIErvVegeMaiNap);
                                break;
                            }
                                                                                   

                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
                                KRT_Szerepkorok krt_Szerepkorok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Szerepkorok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    

}
