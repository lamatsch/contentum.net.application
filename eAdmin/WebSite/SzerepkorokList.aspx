<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="true" CodeFile="SzerepkorokList.aspx.cs" Inherits="SzerepkorokList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%--<%@ Register TagPrefix="obout" Namespace="Obout.Grid" Assembly="obout_Grid_NET" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="SzerepkorokCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="SzerepkorokUpdatePanel" runat="server" OnLoad="SzerepkorokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="SzerepkorokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="SzerepkorokCPEButton" CollapseControlID="SzerepkorokCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="SzerepkorokCPEButton"
                            ExpandedSize="0" ExpandedText="Szerepk�r�k list�ja" CollapsedText="Szerepk�r�k list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <%--  		<obout:Grid id="grid1" runat="server" AllowSorting="true"
			CallbackMode="true" Serialize="true" AllowAddingRecords="false" AllowColumnResizing="true" 
			 AutoGenerateColumns="false" AllowGrouping="true" AllowFiltering="true" 
			 AllowKeyNavigation="true" AllowMultiRecordSelection="true" AllowPageSizeSelection="true" 
			  AllowRecordSelection="true" EnableRecordHover="true" ShowTotalNumberOfPages="true" ShowLoadingMessage="true" FolderStyle="style/style_5" 
			   AllowPaging="true"  ShowTooltipOnResize="true" Width="100%" PageSize="50">
			   
			<Columns>				
				<obout:Column ID="Column1" DataField="Id" SortOrder="Desc" Width="30%" ReadOnly="true" HeaderText="Product ID" runat="server"/>				
				<obout:Column ID="Column2" DataField="Nev" HeaderText="N�v" Width="70%"  runat="server" />				
			</Columns>			
			<ScrollingSettings ScrollHeight="250" />
		</obout:Grid>	
--%>
                                        <asp:GridView ID="SzerepkorokGridView" runat="server" OnRowCommand="SzerepkorokGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="SzerepkorokGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="SzerepkorokGridView_Sorting"
                                            OnRowDataBound="SzerepkorokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Nev" HeaderText="N�v" SortExpression="Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="FunkciokTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltFunkciok" runat="server">
                                                <ContentTemplate>                                           
                                                    <asp:Label ID="Header" runat="server" Text="Hozz�rendelt funkci�k"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                      
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="FunkciokUpdatePanel" runat="server" OnLoad="FunkciokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="FunkciokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="FunkciokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="FunkciokCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <asp:GridView ID="FunkciokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="FunkciokGridView_Sorting"
                                                                            OnPreRender="FunkciokGridView_PreRender" OnRowCommand="FunkciokGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Funkcio_Nev" HeaderText="Funkci� neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Funkcio_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="FelhasznalokTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltFelhasznalok" runat="server">
                                                <ContentTemplate>                                           
                                                    <asp:Label ID="Label1" runat="server" Text="Hozz�rendelt felhaszn�l�k"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                    
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="FelhasznalokUpdatePanel" runat="server" OnLoad="FelhasznalokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="FelhasznalokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="FelhasznalokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="FelhasznalokCPE" runat="server" TargetControlID="Panel3"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel3" runat="server">
                                                                        <asp:GridView ID="FelhasznalokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="FelhasznalokGridView_Sorting"
                                                                            OnPreRender="FelhasznalokGridView_PreRender" OnRowCommand="FelhasznalokGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Felhasznalo_Nev" HeaderText="Felhaszn�l� neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Felhasznalo_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="Csoport_Nev" HeaderText="Csoport" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Csoport_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="LetrehozasIdo" HeaderStyle-Width="150px" />
                                                                                <asp:TemplateField HeaderText="Megb�z�sban kapott" SortExpression="Helyettesites_Id">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbMegbizasbanKapott" runat="server" Checked='<%# string.IsNullOrEmpty(Eval("Helyettesites_Id").ToString()) ? false : true  %>'
                                                                                            Enabled="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
