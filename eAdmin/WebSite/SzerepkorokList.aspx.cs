using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;


public partial class SzerepkorokList : Contentum.eUtility.UI.PageBase
{
    //LZS - BUG_4529
    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SzerepkorokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        FunkciokSubListHeader.RowCount_Changed += new EventHandler(FunkciokSubListHeader_RowCount_Changed);
        FelhasznalokSubListHeader.RowCount_Changed += new EventHandler(FelhasznalokSubListHeader_RowCount_Changed);

        FunkciokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(FunkciokSubListHeader_ErvenyessegFilter_Changed);
        FelhasznalokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(FelhasznalokSubListHeader_ErvenyessegFilter_Changed);

        FelhasznalokSubListHeader.MapVisible = true;



    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.SzerepkorokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_SzerepkorokSearch);

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        // BUG_4081
        //ListHeader1.PrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("SzerepkorokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("SzerepkorokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(SzerepkorokGridView.ClientID);
        // BUG_4081
        //ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClick("TemplateManagerTeszt.aspx", "tablaNev=KRT_szerepkorok", Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(SzerepkorokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(SzerepkorokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(SzerepkorokGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(SzerepkorokGridView.ClientID);

        //LZS - BUG_4529
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID + '&' + "Notify=true" + '&' + "SelectedRows=" + string.Join(",", ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel).ToArray())
                    , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = SzerepkorokGridView;

        FunkciokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FunkciokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FunkciokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FunkciokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FunkciokGridView.ClientID);
        FunkciokSubListHeader.ButtonsClick += new CommandEventHandler(FunkciokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        FunkciokSubListHeader.AttachedGridView = FunkciokGridView;

        FelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.MapOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        FelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //FelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FelhasznalokGridView.ClientID);
        FelhasznalokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(FelhasznalokGridView.ClientID, "check", "cbMegbizasbanKapott", false);
        FelhasznalokSubListHeader.ButtonsClick += new CommandEventHandler(FelhasznalokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        FelhasznalokSubListHeader.AttachedGridView = FelhasznalokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(SzerepkorokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(FunkciokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);

        /* Breaked Records */
        //LZS - BUG_4529
        KRT_SzerepkorokSearch szerepkorokSearch = new KRT_SzerepkorokSearch();

        if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            szerepkorokSearch.ErvKezd.Clear();
            szerepkorokSearch.ErvVege.Clear();
        }
        else
        {
            szerepkorokSearch = new KRT_SzerepkorokSearch();
        }

        Search.SetIdsToSearchObject(Page, "Id", szerepkorokSearch);

        if (!IsPostBack) SzerepkorokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor" + CommandName.Lock);

        FunkciokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Szerepkor_FunkcioList");
        FelhasznalokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorList");

        FunkciokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.New);
        FunkciokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.View);
        FunkciokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.Modify);
        FunkciokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Szerepkor_Funkcio" + CommandName.Invalidate);

        FelhasznalokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.New);
        FelhasznalokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.View);
        FelhasznalokSubListHeader.MapEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        FelhasznalokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Modify);
        FelhasznalokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo_Szerepkor" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(SzerepkorokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }


    #endregion

    #region Master List

    protected void SzerepkorokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("SzerepkorokGridView", ViewState, "KRT_Szerepkorok.Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("SzerepkorokGridView", ViewState);

        SzerepkorokGridViewBind(sortExpression, sortDirection);
    }

    protected void SzerepkorokGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //KRT_SzerepkorokSearch szerepkorsearch = new KRT_SzerepkorokSearch();
        //szerepkorsearch.ErvVege.Clear();
        //szerepkorsearch.ErvKezd.Clear();


        KRT_SzerepkorokSearch search = (KRT_SzerepkorokSearch)Search.GetSearchObject(Page, new KRT_SzerepkorokSearch());
        search.OrderBy = Search.GetOrderBy("SzerepkorokGridView", ViewState, SortExpression, SortDirection);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);




        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(SzerepkorokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


        /*grid1.DataSource = res.Ds;
        grid1.DataBind();*/

    }

    protected void SzerepkorokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void SzerepkorokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, SzerepkorokCPE);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        SzerepkorokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, SzerepkorokCPE);
        SzerepkorokGridViewBind();
    }

    protected void SzerepkorokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(SzerepkorokGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);

            //SzerepkorokGridView_SelectRowCommand(id);

        }
    }

    //private void SzerepkorokGridView_SelectRowCommand(string szerepkorId)
    //{
    //    string id = szerepkorId;

    //    ActiveTabRefresh(TabContainer1, id);

    //    //RefreshOnClientClicksByMasterListSelectedRow(id);

    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //LZS - BUG_4529
            // m�dos�that�s�g ellen�rz�se:
            bool modosithato = true;
            if (ListHeader1.IsFilteredLabelText.Contains("�rv�nytelen"))
            {
                modosithato = false;
            }


            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("SzerepkorokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID);

            //LZS - BUG_4529
            ListHeader1.SendObjectsOnClientClick = null;
            ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID + '&' + "Notify=true" + '&' + "SelectedRows=" + string.Join(",", ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel).ToArray())
                        , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, "", true);

            //LZS - BUG_4529
            if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("SzerepkorokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, SzerepkorokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            string tableName = "KRT_Szerepkorok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, SzerepkorokUpdatePanel.ClientID);

            FunkciokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.SzerepkorId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            FelhasznalokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.SzerepkorId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            FelhasznalokSubListHeader.MapOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Felhasznalo_Form_Multi.aspx"
                , "Command=" + CommandName.Modify + "&" + QueryStringVars.SzerepkorId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    protected void SzerepkorokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    SzerepkorokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedSzerepkorok();
            SzerepkorokGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedSzerepkorRecords();
                SzerepkorokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedSzerepkorRecords();
                SzerepkorokGridViewBind();
                break;
            case CommandName.SendObjects:
                //LZS - BUG_4529
                //SendMailSelectedSzerepkorok();
                break;
        }
    }

    private void LockSelectedSzerepkorRecords()
    {
        LockManager.LockSelectedGridViewRecords(SzerepkorokGridView, "KRT_Szerepkorok"
            , "SzerepkorLock", "SzerepkorForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedSzerepkorRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(SzerepkorokGridView, "KRT_Szerepkorok"
            , "SzerepkorLock", "SzerepkorForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a SzerepkorokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedSzerepkorok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "SzerepkorInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_SzerepkorokService service = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a SzerepkorokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedSzerepkorok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(SzerepkorokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Szerepkorok");
        }
    }

    protected void SzerepkorokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        SzerepkorokGridViewBind(e.SortExpression, UI.GetSortToGridView("SzerepkorokGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (SzerepkorokGridView.SelectedIndex == -1)
        {
            return;
        }
        string szerepkorId = UI.GetGridViewSelectedRecordId(SzerepkorokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, szerepkorId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string szerepkorId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                FunkciokGridViewBind(szerepkorId);
                FunkciokPanel.Visible = true;
                break;
            case 1:
                FelhasznalokGridViewBind(szerepkorId);
                FelhasznalokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(FunkciokGridView);
                break;
            case 1:
                ui.GridViewClear(FelhasznalokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        FunkciokSubListHeader.RowCount = RowCount;
        FelhasznalokSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(FelhasznalokTabPanel))
        {
            FelhasznalokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(FelhasznalokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(FunkciokTabPanel))
        {
            FunkciokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(FunkciokGridView));
        }

    }


    #endregion


    #region Funkciok Detail

    private void FunkciokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Szerepkor_Funkciok();
            FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
    }

    protected void FunkciokGridViewBind(string szerepkorId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FunkciokGridView", ViewState, "Funkcio_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FunkciokGridView", ViewState);

        FunkciokGridViewBind(szerepkorId, sortExpression, sortDirection);
    }

    protected void FunkciokGridViewBind(string szerepkorId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(szerepkorId))
        {
            KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
            szerepkor.Id = szerepkorId;
            KRT_Szerepkor_FunkcioSearch search = new KRT_Szerepkor_FunkcioSearch();
            search.OrderBy = Search.GetOrderBy("FunkciokGridView", ViewState, SortExpression, SortDirection);

            FunkciokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllBySzerepkor(ExecParam, szerepkor, search);

            UI.GridViewFill(FunkciokGridView, res, FunkciokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header);

            ui.SetClientScriptToGridViewSelectDeSelectButton(FunkciokGridView);
        }
        else
        {
            ui.GridViewClear(FunkciokGridView);
        }
    }


    void FunkciokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
    }

    protected void FunkciokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(FunkciokTabPanel))
                    //{
                    //    FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
                    //}
                    ActiveTabRefreshDetailList(FunkciokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a FunkciokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_Szerepkor_Funkciok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Szerepkor_FunkcioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FunkciokGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_Szerepkor_FunkcioService service = eAdminService.ServiceFactory.GetKRT_Szerepkor_FunkcioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void FunkciokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FunkciokGridView, selectedRowNumber, "check");
        }
    }

    private void FunkciokGridView_RefreshOnClientClicks(string szerepkor_funkcio_Id)
    {
        string id = szerepkor_funkcio_Id;
        if (!String.IsNullOrEmpty(id))
        {
            FunkciokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            FunkciokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Szerepkor_Funkcio_Form.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FunkciokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void FunkciokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FunkciokGridView.PageIndex;

        FunkciokGridView.PageIndex = FunkciokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != FunkciokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, FunkciokCPE);
            FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(FunkciokSubListHeader.Scrollable, FunkciokCPE);
        }
        FunkciokSubListHeader.PageCount = FunkciokGridView.PageCount;
        FunkciokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(FunkciokGridView);
    }


    void FunkciokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(FunkciokSubListHeader.RowCount);
        FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
    }

    protected void FunkciokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FunkciokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView)
            , e.SortExpression, UI.GetSortToGridView("FunkciokGridView", ViewState, e.SortExpression));
    }

    #endregion


    #region Felhasznalok Detail

    private void FelhasznalokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Felhasznalo_Szerepkor();
            FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
    }

    protected void FelhasznalokGridViewBind(string szerepkorId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FelhasznalokGridView", ViewState, "Felhasznalo_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FelhasznalokGridView", ViewState);

        FelhasznalokGridViewBind(szerepkorId, sortExpression, sortDirection);
    }

    protected void FelhasznalokGridViewBind(string szerepkorId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(szerepkorId))
        {
            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
            szerepkor.Id = szerepkorId;
            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();
            search.OrderBy = Search.GetOrderBy("FelhasznalokGridView", ViewState, SortExpression, SortDirection);

            FelhasznalokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllBySzerepkor(ExecParam, szerepkor, search);

            UI.GridViewFill(FelhasznalokGridView, res, FelhasznalokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(FelhasznalokGridView);
        }
        else
        {
            ui.GridViewClear(FelhasznalokGridView);
        }
    }

    void FelhasznalokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
    }

    protected void FelhasznalokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(FelhasznalokTabPanel))
                    //{
                    //    FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
                    //}
                    ActiveTabRefreshDetailList(FelhasznalokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a FelhasznalokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_Felhasznalo_Szerepkor()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FelhasznalokGridView, EErrorPanel1, ErrorUpdatePanel);

            KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                bool modosithato = !UI.isGridViewCheckBoxChecked(FelhasznalokGridView, "cbMegbizasbanKapott", deletableItemsList[i]);
                if (modosithato)
                {
                    ExecParam execParam;
                    execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = deletableItemsList[i];
                    execParams.Add(execParam);
                }
            }
            //Result result = service.MultiInvalidate(execParams.ToArray());
            Result result = service.MultiInvalidateWithMegbizasControl(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            else
            {
                /** 
                  * <FIGYELEM!!!> 
                  * Ez nem m�soland� �t a t�bbi formra, ez csak itt kell!!!
                  * (Ezzel el�id�z�nk egy funkci�- �s szerepk�rlista-friss�t�st
                  *  az �sszes felhaszn�l� Session-jeiben)
                  */
                FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
                /**
                 * </FIGYELEM>
                 */
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void FelhasznalokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FelhasznalokGridView, selectedRowNumber, "check");

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //FelhasznalokGridView_RefreshOnClientClicks(id);            
        }
    }

    private void FelhasznalokGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            FelhasznalokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //FelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            bool modosithato = !UI.isGridViewCheckBoxChecked(FelhasznalokGridView, "cbMegbizasbanKapott", id);

            if (modosithato)
            {
                FelhasznalokSubListHeader.ModifyEnabled = true;
                FelhasznalokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("Felhasznalo_Szerepkor_Form.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth, Defaults.PopupHeight, FelhasznalokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }
            else
            {
                FelhasznalokSubListHeader.ModifyEnabled = false;// nem engedj�k meg
            }
        }
    }

    protected void FelhasznalokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FelhasznalokGridView.PageIndex;

        FelhasznalokGridView.PageIndex = FelhasznalokSubListHeader.PageIndex;

        if (prev_PageIndex != FelhasznalokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, FelhasznalokCPE);
            FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
        }
        else
        {
            UI.GridViewSetScrollable(FelhasznalokSubListHeader.Scrollable, FelhasznalokCPE);
        }

        FelhasznalokSubListHeader.PageCount = FelhasznalokGridView.PageCount;
        FelhasznalokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(FelhasznalokGridView);
    }

    void FelhasznalokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(FelhasznalokSubListHeader.RowCount);
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView));
    }

    protected void FelhasznalokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FelhasznalokGridViewBind(UI.GetGridViewSelectedRecordId(SzerepkorokGridView)
            , e.SortExpression, UI.GetSortToGridView("FelhasznalokGridView", ViewState, e.SortExpression));
    }

    #endregion



}
