using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class SzerepkorokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_SzerepkorokSearch);
  
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += 
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);        

        if (!IsPostBack)
        {
            KRT_SzerepkorokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_SzerepkorokSearch)Search.GetSearchObject(Page, new KRT_SzerepkorokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }    

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_SzerepkorokSearch krt_szerepkorokSearch = null;
        if (searchObject != null) krt_szerepkorokSearch = (KRT_SzerepkorokSearch)searchObject;

        if (krt_szerepkorokSearch != null)
        {
            Nev.Text = krt_szerepkorokSearch.Nev.Value;
            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_szerepkorokSearch.ErvKezd, krt_szerepkorokSearch.ErvVege);
        }

    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_SzerepkorokSearch SetSearchObjectFromComponents()
    {
        KRT_SzerepkorokSearch krt_szerepkorokSearch = (KRT_SzerepkorokSearch)SearchHeader1.TemplateObject;
        if (krt_szerepkorokSearch == null)
        {
            krt_szerepkorokSearch = new KRT_SzerepkorokSearch();
        }

        if (!String.IsNullOrEmpty(Nev.Text))
        {
            krt_szerepkorokSearch.Nev.Value = Nev.Text;
            krt_szerepkorokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev.Text);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_szerepkorokSearch.ErvKezd, krt_szerepkorokSearch.ErvVege);
        
        return krt_szerepkorokSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());            
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_SzerepkorokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
        
    }
    
   
    private KRT_SzerepkorokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_SzerepkorokSearch();        
    }



}
