<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"  EnableEventValidation="True"
CodeFile="TargySzavakLovList.aspx.cs" Inherits="TargySzavakLovList" Title="<%$Resources:LovList,TargySzavakLovListHeaderTitle%>" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,TargySzavakLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <BR />
                            <TABLE style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0>
                                <TBODY>
                                    <TR id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <TD>
                                            <asp:Label id="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>">
                                            </asp:Label>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD>
                                            <TABLE>
                                                <TBODY>
                                                    <TR class="urlapSor">
                                                        <TD class="mrUrlapMezo">
                                                            <asp:TextBox id="TargySzavak_TextBoxSearch" runat="server">*</asp:TextBox>
                                                        </TD>
                                                        <TD class="mrUrlapCaption">
                                                            <asp:Label id="labelCsoportNevTulaj" runat="server" Text="Tulajdonos szervezet:" Font-Bold="True"></asp:Label>
                                                        </TD>
                                                        <TD class="mrUrlapMezo">
                                                            <uc5:CsoportTextBox id="CsoportTextBoxTulaj" runat="server"></uc5:CsoportTextBox>
                                                        </TD>
                                                    </TR>
                                                    <TR class="urlapSor">
                                                        <TD colSpan="4">
                                                            <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s"></asp:ImageButton>
                                                            <asp:ImageButton id="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                        </TD>
                                                    </TR>
                                                </TBODY>
                                            </TABLE>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="PADDING-BOTTOM: 5px; VERTICAL-ALIGN: top; PADDING-TOP: 5px; TEXT-ALIGN: left">
                                            <DIV class="listaFulFelsoCsikKicsi">
                                                <IMG alt="" src="images/hu/design/spacertrans.gif" />
                                            </DIV>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                            <ajaxToolkit:CollapsiblePanelExtender id="TargySzavakCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel id="Panel1" runat="server">
                                                <TABLE style="WIDTH: 98%" cellSpacing=0 cellPadding=0>
                                                    <TBODY>
                                                        <TR>
                                                            <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left" class="GridViewHeaderBackGroundStyle">
                                                            <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField id="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField id="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView id="GridViewSearchResult" runat="server"
                                                                    CssClass="GridViewLovListStyle" CellPadding="0" BorderWidth="1px"
                                                                    AllowPaging="True" PagerSettings-Visible="false" AutoGenerateColumns="False"
                                                                    DataKeyNames="Id"
                                                                    OnRowDataBound="GridView_RowDataBound">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TargySzavak" SortExpression="TargySzavak" HeaderText="T�rgyszavak">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Tipus" SortExpression="Tipus" HeaderText="T�pus">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
					                                                    <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelErtekTartozikHozza" Text="�rt�k van" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:CheckBox ID="cbErtekTartozikHozza" runat="server" AutoPostBack="false" Text='<%# Eval("Tipus") %>' Enabled="false" CssClass="HideCheckBoxText" />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>
                                                                        <asp:BoundField DataField="AlapertelmezettErtek" SortExpression="AlapertelmezettErtek"  HeaderText="Alap�rtelmezett �rt�k">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField AccessibleHeaderText="AlapertelmezettErtek" HeaderText="Alap�rtelmezett �rt�k"
                                                                            SortExpression="AlapertelmezettErtek_Lekepezett">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <%-- A Labeleket nem akarjuk besz�rk�teni, ez�rt csak a checkboxra �ll�tjuk be az Enabled="false"-t --%>
                                                                                <eUI:DynamicValueControl ID="DVC_AlapertelmezettErtek" runat="server"
                                                                                DefaultControlTypeSource='<%# Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label %>'
                                                                                ControlTypeSource='<%# (Eval("ControlTypeSource") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox : Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.Label  %>'
                                                                                Value='<%# Eval("AlapertelmezettErtek_Lekepezett") %>' Validate="false" Enabled='<%# (Eval("ControlTypeSource") as string) == Contentum.eUtility.KodTarak.CONTROLTYPE_SOURCE.CheckBox ? false : true  %>'
                                                                                />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="RegExp" SortExpression="RegExp" HeaderText="Regul�ris kif.">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ToolTip" SortExpression="ToolTip" HeaderText="Eszk�ztipp">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>  
                                                                        <asp:BoundField DataField="SPSSzinkronizalt" SortExpression="SPSSzinkronizalt" HeaderText="Szinkroniz�lt">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ControlTypeSource" SortExpression="ControlTypeSource" HeaderText="Vez�rl�elem forr�sa">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ControlTypeDataSource" SortExpression="ControlTypeDataSource" HeaderText="Vez�rl�elem adatforr�sa">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField> 
					                                                    <asp:TemplateField>
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
						                                                    <HeaderTemplate>
							                                                    <asp:Label ID="labelSPSSzinkronizalt" Text="Szinkroniz�lt" runat="server" />
						                                                    </HeaderTemplate>
						                                                    <ItemTemplate>
							                                                    <asp:CheckBox ID="cbSPSSzinkronizalt" runat="server" AutoPostBack="false" Text='<%# Eval("SPSSzinkronizalt") %>' Enabled="false" CssClass="HideCheckBoxText" />
						                                                    </ItemTemplate>
					                                                    </asp:TemplateField>                                                                                                                                                
                                                                        <asp:BoundField DataField="Csoport_Id_Tulaj" SortExpression="Csoport_Id_Tulaj">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CsoportNevTulaj" SortExpression="CsoportNevTulaj" HeaderText="Tulajdonos">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                        </asp:BoundField>                                                                        
                                                                    </Columns>

                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>

                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>

                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </TD>
                                                        </TR>
                                                    </TBODY>
                                                </TABLE>
                                            </asp:Panel>
                                            <%--
                                            <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                            </asp:ListBox>
                                            --%>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
                                            &nbsp; <BR />
                                            <asp:ImageButton id="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </TD>
                                    </TR>
                                </TBODY>
                            </TABLE>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>

