using Contentum.eAdmin.BaseUtility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TargyszavakForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    //TODO: QueryStringVar, nem CommandName
    private string Csoport_Id_Tulaj = "";

    private const string kcs_CONTROLTYPE_SOURCE = "CONTROLTYPE_SOURCE";

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    #region Utils

    public string[][] KodCsoportok
    {
        get
        {
            if (ViewState["KodCsoportokList"] == null)
            {
                #region felt�lt�s

                Contentum.eAdmin.Service.KRT_KodCsoportokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch search = new Contentum.eQuery.BusinessDocuments.KRT_KodCsoportokSearch();
                search.OrderBy = " KRT_KodCsoportok.Nev ASC ";

                Result result = service.GetAll(execParam, search);

                if (!result.IsError)
                {
                    List<string[]> listItems = new List<string[]>();
                    listItems.Add(new string[2] { "", Resources.Form.EmptyListItem });
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                        {
                            System.Data.DataRow row = result.Ds.Tables[0].Rows[i];
                            string Kod = row["Kod"].ToString();
                            string Nev = row["Nev"].ToString();
                            if (!String.IsNullOrEmpty(Kod))
                            {
                                listItems.Add(new string[2] { String.IsNullOrEmpty(Nev) ? Kod : Nev, Kod });
                            }
                        }
                    }

                    ViewState["KodCsoportokList"] = listItems.ToArray();
                }
                #endregion felt�lt�s
            }
            return ViewState["KodCsoportokList"] as string[][];
        }
    }

    // mag�t�l nem �ll vissza a st�lus, ha ReadOnly=true-r�l visszav�ltunk false-ra
    protected void SetTextBoxReadOnly(TextBox tbox, bool isReadOnly, string normalCssClass)
    {
        tbox.ReadOnly = isReadOnly;
        //tbox.Enabled = !isReadOnly;
        if (!isReadOnly)
        {
            // vissza�ll�tjuk a st�lust
            tbox.CssClass = normalCssClass;
        }

    }

    #endregion Utils

    private String RegisterCheckErtekJavaScript(TextBox ertekTextBox, TextBox regExpTextBox)
    {
        string js = "";
        if (ertekTextBox != null && ertekTextBox.Visible == true && regExpTextBox != null && regExpTextBox.Visible == true)
        {
            js = @"var text = $get('" + ertekTextBox.ClientID + @"');
                   var regexp = $get('" + regExpTextBox.ClientID + @"');
                    if(text && text.value != '' && regexp && regexp.value != '') {
                        var re= new RegExp(regexp.value);
                        if (!re.test(text.value)) {
                            alert('" + String.Format(Resources.Form.RegularExpressionValidationMessage, "(RegExp:' + regexp.value +')'") + @");
                            return false;
                        }
                    }";
        }
        return js;
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxTargySzavak.ReadOnly = true;
        cbTipus.Enabled = false;
        ErvenyessegCalendarControl1.ReadOnly = true;
        TextBoxBelsoAzonosito.ReadOnly = true;
        TextBoxRegexp.ReadOnly = true;
        TextBoxToolTip.ReadOnly = true;
        DynamicValueControl_AlapertelmezettErtek.ReadOnly = true;
        KodTarakDropDownList_ControlTypeSource.ReadOnly = true;
        DynamicValueControl_ControlTypeDataSource.ReadOnly = true;

        textNote.ReadOnly = true;
        CsoportTextBoxTulaj.ReadOnly = true;
        ParentTargyszo.ReadOnly = true;

        labelTargySzavak.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        cbTipus.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelCsoportNevTulaj.CssClass = "mrUrlapInputWaterMarked";
        labelBelsoAzonosito.CssClass = "mrUrlapInputWaterMarked";
        labelRegexp.CssClass = "mrUrlapInputWaterMarked";
        labelToolTip.CssClass = "mrUrlapInputWaterMarked";
        labelAlapertelmezettErtek.CssClass = "mrUrlapInputWaterMarked";
        labelControlTypeSource.CssClass = "mrUrlapInputWaterMarked";
        labelControlTypeDataSource.CssClass = "mrUrlapInputWaterMarked";
        labelParentTargyszo.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls(bool bFunctionMeta)
    {
        // TODO: mikor lehet m�dos�tani?
        //CsoportTextBoxTulaj.ViewMode = true;
        CsoportTextBoxTulaj.SearchMode = true;

        if (!bFunctionMeta && !cbTipus.Checked)
        {
            MetaAdatokUpdatePanel.Visible = false;
        }
        else
        {
            cbTipus.Enabled = bFunctionMeta;
            SetTextBoxReadOnly(TextBoxRegexp, !bFunctionMeta || !cbTipus.Checked, "mrUrlapInput");
            SetTextBoxReadOnly(TextBoxToolTip, !bFunctionMeta || !cbTipus.Checked, "mrUrlapInput");
            //SetTextBoxReadOnly(TextBoxAlapertelmezettErtek, !bFunctionMeta || !cbTipus.Checked, "mrUrlapInput");
            DynamicValueControl_AlapertelmezettErtek.ReadOnly = (!bFunctionMeta || !cbTipus.Checked);
            DynamicValueControl_ControlTypeDataSource.ReadOnly = (!bFunctionMeta || !cbTipus.Checked);
            KodTarakDropDownList_ControlTypeSource.ReadOnly = (!bFunctionMeta || !cbTipus.Checked);
            ParentTargyszo.ReadOnly = (!bFunctionMeta || !cbTipus.Checked);
        }
    }


    /// <summary>
    /// New m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    /// <param name="bFunctionMeta">Az �rt�kkel rendelkez� t�rgyszavak (azaz metadatok) felvitel�re is van-e jog</param>
    private void SetNewControls(bool bFunctionMeta)
    {
        CsoportTextBoxTulaj.SearchMode = true;
        cbTipus.Enabled = bFunctionMeta;
        cbTipus.Checked = bFunctionMeta;
        //SetTextBoxReadOnly(TextBoxRegexp, !bFunctionMeta && !cbTipus.Checked, "mrUrlapInput");
        //SetTextBoxReadOnly(TextBoxToolTip, !bFunctionMeta && !cbTipus.Checked, "mrUrlapInput");
        //SetTextBoxReadOnly(TextBoxAlapertelmezettErtek, !bFunctionMeta && !cbTipus.Checked, "mrUrlapInput");

        MetaAdatokUpdatePanel.Visible = bFunctionMeta;

        trBelsoAzonosito.Visible = false;
        trSPSSzinkronizalt.Visible = false;
        ParentTargyszo.ReadOnly = true;
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        CsoportTextBoxTulaj.Validate = false;
        CsoportTextBoxTulaj.SzervezetCsoport = true;

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Targyszavak" + Command);

                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_TargySzavakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
               {
                   EREC_TargySzavak erec_Targyszavak = (EREC_TargySzavak)result.Record;
                   LoadComponentsFromBusinessObject(erec_Targyszavak);

                   if (Command == CommandName.View)
                   {
                       SetViewControls();
                   }
                   if (Command == CommandName.Modify)
                   {
                       if ((erec_Targyszavak.Tipus == "1" && !FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese"))
                           || erec_Targyszavak.SPSSzinkronizalt == "1" // SPS-szinkroniz�lt t�rgysz� fel�leten m�r
                           )
                       {
                           //SetViewControls();
                           UI.popupRedirect(Page, "TargyszavakForm.aspx?" + QueryStringVars.Id + "=" + id
                               + "&" + QueryStringVars.Command + "=" + CommandName.View);
                       }
                       else
                       {
                           SetModifyControls(FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese"));
                       }
                   }
               }
               else
               {
                   ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
               }
            }
        }


        if (Command == CommandName.New)
        {
            Csoport_Id_Tulaj = Request.QueryString.Get(QueryStringVars.Csoport_Id_Tulaj); // TODO: hibakezel�s
            //Record_Csoport_Id_Tulaj_HiddenField.Value = Csoport_Id_Tulaj;
            if (!String.IsNullOrEmpty(Csoport_Id_Tulaj))
            {
                CsoportTextBoxTulaj.Id_HiddenField = Csoport_Id_Tulaj;
                CsoportTextBoxTulaj.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                CsoportTextBoxTulaj.ReadOnly = true;
            }
            SetNewControls(FunctionRights.GetFunkcioJog(Page, "MetaadatokKezelese"));
            KodTarakDropDownList_ControlTypeSource.FillAndSetEmptyValue(kcs_CONTROLTYPE_SOURCE, FormHeader1.ErrorPanel);
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        //cbTipus.CheckedChanged += new EventHandler(TipusChanged);
        cbTipus.Attributes["onclick"] += "__doPostBack('" + MetaAdatokUpdatePanel.ClientID + "','" + EventArgumentConst.refresh + "');";

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (Command == CommandName.Modify || Command == CommandName.New)
        {
            FormFooter1.ImageButton_Save.OnClientClick = RegisterCheckErtekJavaScript(DynamicValueControl_AlapertelmezettErtek.Control as TextBox, TextBoxRegexp);

            string PostBack_ControlTypeSource = Page.ClientScript.GetPostBackEventReference(MetaAdatokUpdatePanel, "ChangeType") + ";";
            KodTarakDropDownList_ControlTypeSource.DropDownList.Attributes["onchange"] += PostBack_ControlTypeSource;     
        }
    }

    protected void FillKodCsoportokDropDownList(DropDownList ddl)
    {
        if (ddl != null)
        {
            ddl.Items.Clear();
            
            string[][] listItems = this.KodCsoportok;

            if (listItems != null)
            {
                // felt�lt�s
                ddl.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));
                foreach (string[] items in listItems)
                {
                    if (items.Length > 1)
                    {
                        ddl.Items.Add(new ListItem(items[0], items[1]));
                    }
                }
            }
            else
            {
                ddl.Items.Add(new ListItem("", "Hiba a felt�lt�s sor�n!"));
            }

        }
    }

    // BLG_608
    protected void FillKodCsoportokListBox(ListBox listBox)
    {
        if (listBox != null)
        {
            listBox.Items.Clear();

            string[][] listItems = this.KodCsoportok;

            if (listItems != null)
            {
                // felt�lt�s
               // listBox.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));
                foreach (string[] items in listItems)
                {
                    if (items.Length > 1)
                    {
                        listBox.Items.Add(new ListItem(items[0], items[1]));
                    }
                }
            }
            else
            {
                listBox.Items.Add(new ListItem("", "Hiba a felt�lt�s sor�n!"));
            }

        }
    }

    protected void SetDynamicValueControl(Contentum.eUIControls.DynamicValueControl dvc, string controlTypeSource, string controlTypeDataSource)
    {
        if (dvc != null)
        {
            if (controlTypeSource == KodTarak.CONTROLTYPE_SOURCE.FuggoKodTarakDropDownList)
            {
                controlTypeSource = KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList;
            }

            dvc.ControlTypeSource = controlTypeSource;

            switch (controlTypeSource)
            {
                case KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList:
                    Component_KodtarakDropDownList ktddl = dvc.Control as Component_KodtarakDropDownList;
                    if (ktddl != null)
                    {
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            ktddl.FillAndSetEmptyValue(controlTypeDataSource, FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            ktddl.FillAndSetSelectedValue(controlTypeDataSource, dvc.Value, true, FormHeader1.ErrorPanel);
                        }
                    }                   
                    break;
                // BLG_608
                case KodTarak.CONTROLTYPE_SOURCE.KodTarakListBox:
                    Component_KodtarakListBox ktlb = dvc.Control as Component_KodtarakListBox;
                    if (ktlb != null)
                    {
                        dvc.Control.GetType().GetProperty("Ismetlodo").SetValue(dvc.Control, dvc.Ismetlodo, null);
                        if (String.IsNullOrEmpty(dvc.Value))
                        {
                            ktlb.FillListBox(controlTypeDataSource, FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            ktlb.FillAndSetSelectedValues(controlTypeDataSource, dvc.Value, FormHeader1.ErrorPanel);
                        }
                    }
                    break;
            }

            if (dvc.Control is UI.ILovListTextBox)
            {
                UI.ILovListTextBox lovtb = dvc.Control as UI.ILovListTextBox;
                if (lovtb != null)
                {
                    lovtb.SetTextBoxById(FormHeader1.ErrorPanel, null);
                }
            }
        }
    }

    protected void ControlTypeSourceChanged()
    {
        DynamicValueControl_AlapertelmezettErtek.Value = "";
        DynamicValueControl_ControlTypeDataSource.Value = "";
        switch (KodTarakDropDownList_ControlTypeSource.SelectedValue)
        {
            // BLG_608
            case KodTarak.CONTROLTYPE_SOURCE.KodTarakListBox:
            case KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList:
            case KodTarak.CONTROLTYPE_SOURCE.FuggoKodTarakDropDownList:
                SetDynamicValueControl(DynamicValueControl_ControlTypeDataSource, KodTarak.CONTROLTYPE_SOURCE.eDropDownList, null);
                if (DynamicValueControl_ControlTypeDataSource.Control != null)
                {
                    DropDownList ddl = DynamicValueControl_ControlTypeDataSource.Control as DropDownList;
                    if (ddl != null)
                    {
                        FillKodCsoportokDropDownList(ddl);

                        ddl.Attributes["onchange"] += Page.ClientScript.GetPostBackEventReference(MetaAdatokUpdatePanel, "ChangeSource") + ";";
                    }
                }
                DynamicValueControl_ControlTypeDataSource.ReadOnly = false;
                break;
            default:
                SetDynamicValueControl(DynamicValueControl_ControlTypeDataSource, KodTarak.CONTROLTYPE_SOURCE.CustomTextBox, "");
                DynamicValueControl_ControlTypeDataSource.ReadOnly = true;
                break;
        }

        SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, KodTarakDropDownList_ControlTypeSource.SelectedValue, DynamicValueControl_ControlTypeDataSource.Value);

        if (KodTarakDropDownList_ControlTypeSource.SelectedValue == KodTarak.CONTROLTYPE_SOURCE.FuggoKodTarakDropDownList)
        {
            ParentTargyszo.ReadOnly = false;
        }
        else
        {
            ParentTargyszo.ReadOnly = true;

        }
    }

        protected void MetaAdatokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refresh:
                    TipusChanged(sender, e);
                    break;
                case "ChangeType":
                    ControlTypeSourceChanged();
                    break;
                case "ChangeSource":
                    DynamicValueControl_AlapertelmezettErtek.Value = "";
                    if ((KodTarakDropDownList_ControlTypeSource.SelectedValue == KodTarak.CONTROLTYPE_SOURCE.KodTarakDropDownList
                        || KodTarakDropDownList_ControlTypeSource.SelectedValue == KodTarak.CONTROLTYPE_SOURCE.FuggoKodTarakDropDownList)
                        && DynamicValueControl_ControlTypeDataSource.ControlTypeSource == KodTarak.CONTROLTYPE_SOURCE.eDropDownList)
                    {
                        SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, KodTarakDropDownList_ControlTypeSource.SelectedValue, DynamicValueControl_ControlTypeDataSource.Value);
                    }
                    break;
            }
        }
    }


    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="erec_Targyszavak"></param>
    private void LoadComponentsFromBusinessObject(EREC_TargySzavak erec_Targyszavak)
    {
        //Record_Csoport_Id_Tulaj_HiddenField.Value = erec_Targyszavak.Csoport_Id_Tulaj;
        requiredTextBoxTargySzavak.Text = erec_Targyszavak.TargySzavak;

        if (!String.IsNullOrEmpty(erec_Targyszavak.Csoport_Id_Tulaj))
        {
            CsoportTextBoxTulaj.Id_HiddenField = erec_Targyszavak.Csoport_Id_Tulaj;
            CsoportTextBoxTulaj.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            CsoportTextBoxTulaj.ReadOnly = true;
        }

        cbSPSSzinkronizalt.Checked = (erec_Targyszavak.SPSSzinkronizalt == "1" ? true : false);
        TextBoxRegexp.Text = erec_Targyszavak.RegExp;
        TextBoxToolTip.Text = erec_Targyszavak.ToolTip;
        //TextBoxAlapertelmezettErtek.Text = erec_Targyszavak.AlapertelmezettErtek;
        if (!IsPostBack)
        {
            KodTarakDropDownList_ControlTypeSource.FillAndSetSelectedValue(kcs_CONTROLTYPE_SOURCE, erec_Targyszavak.ControlTypeSource, true, FormHeader1.ErrorPanel);
            ControlTypeSourceChanged();

            DynamicValueControl_ControlTypeDataSource.Value = erec_Targyszavak.ControlTypeDataSource;
            DynamicValueControl_AlapertelmezettErtek.Value = erec_Targyszavak.AlapertelmezettErtek;
            SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, erec_Targyszavak.ControlTypeSource, DynamicValueControl_ControlTypeDataSource.Value);
        }
        TextBoxBelsoAzonosito.Text = erec_Targyszavak.BelsoAzonosito;

        cbTipus.Checked = (erec_Targyszavak.Tipus == "1" ? true : false);
        ErvenyessegCalendarControl1.ErvKezd = erec_Targyszavak.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_Targyszavak.ErvVege;
        textNote.Text = erec_Targyszavak.Base.Note;

        ParentTargyszo.Id_HiddenField = erec_Targyszavak.Targyszo_Id_Parent;
        ParentTargyszo.SetTargySzavakTextBoxById(FormHeader1.ErrorPanel);

        //aktu�lis verzi� elt�rol�sa
        FormHeader1.Record_Ver = erec_Targyszavak.Base.Ver;
        //A m�dos�t�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(erec_Targyszavak.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private EREC_TargySzavak GetBusinessObjectFromComponents()
    {
        EREC_TargySzavak erec_Targyszavak = new EREC_TargySzavak();
        erec_Targyszavak.Updated.SetValueAll(false);
        erec_Targyszavak.Base.Updated.SetValueAll(false);

        //erec_Targyszavak.Csoport_Id_Tulaj = Record_Csoport_Id_Tulaj_HiddenField.Value;
        //erec_Targyszavak.Updated.Csoport_Id_Tulaj = pageView.GetUpdatedByView(Record_Csoport_Id_Tulaj_HiddenField);

        erec_Targyszavak.Csoport_Id_Tulaj = CsoportTextBoxTulaj.Id_HiddenField;
        erec_Targyszavak.Updated.Csoport_Id_Tulaj = pageView.GetUpdatedByView(CsoportTextBoxTulaj);
        erec_Targyszavak.TargySzavak = requiredTextBoxTargySzavak.Text;
        erec_Targyszavak.Updated.TargySzavak = pageView.GetUpdatedByView(requiredTextBoxTargySzavak);

        erec_Targyszavak.RegExp = TextBoxRegexp.Text;
        erec_Targyszavak.Updated.RegExp = pageView.GetUpdatedByView(TextBoxRegexp);

        erec_Targyszavak.ToolTip = TextBoxToolTip.Text;
        erec_Targyszavak.Updated.ToolTip = pageView.GetUpdatedByView(TextBoxToolTip);

        //erec_Targyszavak.AlapertelmezettErtek = TextBoxAlapertelmezettErtek.Text;
        erec_Targyszavak.AlapertelmezettErtek = DynamicValueControl_AlapertelmezettErtek.Value;
        erec_Targyszavak.Updated.AlapertelmezettErtek = pageView.GetUpdatedByView(DynamicValueControl_AlapertelmezettErtek);

        erec_Targyszavak.ControlTypeSource = KodTarakDropDownList_ControlTypeSource.SelectedValue;
        erec_Targyszavak.Updated.ControlTypeSource = pageView.GetUpdatedByView(KodTarakDropDownList_ControlTypeSource);

        erec_Targyszavak.ControlTypeDataSource = DynamicValueControl_ControlTypeDataSource.Value;
        erec_Targyszavak.Updated.ControlTypeDataSource = pageView.GetUpdatedByView(DynamicValueControl_ControlTypeDataSource);

        erec_Targyszavak.Tipus = (cbTipus.Checked ? "1" : "0");
        erec_Targyszavak.Updated.Tipus = pageView.GetUpdatedByView(cbTipus);
        erec_Targyszavak.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        erec_Targyszavak.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Targyszavak.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        erec_Targyszavak.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        erec_Targyszavak.Base.Note = textNote.Text;
        erec_Targyszavak.Base.Updated.Note = pageView.GetUpdatedByView(textNote);

        erec_Targyszavak.Targyszo_Id_Parent = ParentTargyszo.Id_HiddenField;
        erec_Targyszavak.Updated.Targyszo_Id_Parent = pageView.GetUpdatedByView(ParentTargyszo);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        erec_Targyszavak.Base.Ver = FormHeader1.Record_Ver;
        erec_Targyszavak.Base.Updated.Ver = true;

        if (Command == CommandName.New)
        {
            // csak els� felviteln�l �ll�tjuk, k�s�bb nem m�dos�that� 
            erec_Targyszavak.SPSSzinkronizalt = "0";
            erec_Targyszavak.Updated.SPSSzinkronizalt = true;
            erec_Targyszavak.BelsoAzonosito = Contentum.eUtility.eDocumentService.ConvertToIdentifier(erec_Targyszavak.TargySzavak);
            erec_Targyszavak.Updated.BelsoAzonosito = true;
        }

        return erec_Targyszavak;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxTargySzavak);
            compSelector.Add_ComponentOnClick(cbTipus);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textNote);

            compSelector.Add_ComponentOnClick(KodTarakDropDownList_ControlTypeSource);
            compSelector.Add_ComponentOnClick(DynamicValueControl_ControlTypeDataSource);
            compSelector.Add_ComponentOnClick(DynamicValueControl_AlapertelmezettErtek);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void TipusChanged(object sender, EventArgs e)
    {
        if (cbTipus.Checked == true)
        {
            SetTextBoxReadOnly(TextBoxRegexp, false, "mrUrlapInput");
            SetTextBoxReadOnly(TextBoxToolTip, false, "mrUrlapInput");
            //SetTextBoxReadOnly(TextBoxAlapertelmezettErtek, false, "mrUrlapInput");
            DynamicValueControl_AlapertelmezettErtek.ReadOnly = false;
            DynamicValueControl_ControlTypeDataSource.ReadOnly = false;
            KodTarakDropDownList_ControlTypeSource.ReadOnly = false;
        }
        else
        {
            SetTextBoxReadOnly(TextBoxRegexp, true, "mrUrlapInput");
            TextBoxRegexp.Text = "";
            SetTextBoxReadOnly(TextBoxToolTip, true, "mrUrlapInput");
            TextBoxToolTip.Text = "";
            //SetTextBoxReadOnly(TextBoxAlapertelmezettErtek, true, "mrUrlapInput");
            //TextBoxAlapertelmezettErtek.Text = "";
            KodTarakDropDownList_ControlTypeSource.ReadOnly = true;
            DynamicValueControl_AlapertelmezettErtek.ReadOnly = true;
            DynamicValueControl_ControlTypeDataSource.ReadOnly = true;
            ParentTargyszo.ReadOnly = true;

            // mez�k ki�r�t�se
            KodTarakDropDownList_ControlTypeSource.SetSelectedValue("");
            ControlTypeSourceChanged();
            DynamicValueControl_ControlTypeDataSource.Value = "";
            DynamicValueControl_AlapertelmezettErtek.Value = "";
            SetDynamicValueControl(DynamicValueControl_AlapertelmezettErtek, null, DynamicValueControl_ControlTypeDataSource.Value);
            ParentTargyszo.Id_HiddenField = "";
            ParentTargyszo.SetTargySzavakTextBoxById(FormHeader1.ErrorPanel);



        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Targyszavak" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_TargySzavakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();
                            EREC_TargySzavak erec_Targyszavak = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, erec_Targyszavak);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxTargySzavak.Text))
                                {
                                    String TipusHiddenFieldId = Request.QueryString.Get(QueryStringVars.TipusHiddenFieldId);
                                    if (!String.IsNullOrEmpty(TipusHiddenFieldId))
                                    {
                                        Dictionary<string, string> dictionaryTipus = new Dictionary<string, string>();
                                        dictionaryTipus.Add(TipusHiddenFieldId, erec_Targyszavak.Tipus);

                                        String RegExpHiddenFieldId = Request.QueryString.Get(QueryStringVars.RegExpHiddenFieldId);
                                        if (!String.IsNullOrEmpty(RegExpHiddenFieldId))
                                        {
                                            dictionaryTipus.Add(RegExpHiddenFieldId, erec_Targyszavak.RegExp);

                                            String AlapertelmezettErtekHiddenFieldId = Request.QueryString.Get(QueryStringVars.AlapertelmezettErtekHiddenFieldId);
                                            if (!String.IsNullOrEmpty(AlapertelmezettErtekHiddenFieldId))
                                            {
                                                dictionaryTipus.Add(AlapertelmezettErtekHiddenFieldId, erec_Targyszavak.AlapertelmezettErtek);

                                                String ToolTipHiddenFieldId = Request.QueryString.Get(QueryStringVars.ToolTipHiddenFieldId);
                                                if (!String.IsNullOrEmpty(ToolTipHiddenFieldId))
                                                {
                                                    dictionaryTipus.Add(ToolTipHiddenFieldId, erec_Targyszavak.ToolTip);
                                                }
                                            }
                                            String ControlTypeSourceHiddenFieldId = Request.QueryString.Get(QueryStringVars.ControlTypeSourceHiddenFieldId);
                                            if (!String.IsNullOrEmpty(ControlTypeSourceHiddenFieldId))
                                            {
                                                dictionaryTipus.Add(ControlTypeSourceHiddenFieldId, erec_Targyszavak.ControlTypeSource);

                                                String ControlTypeDataSource = Request.QueryString.Get(QueryStringVars.ControlTypeDataSourceHiddenFieldId);
                                                if (!String.IsNullOrEmpty(ControlTypeDataSource))
                                                {
                                                    dictionaryTipus.Add(ControlTypeDataSource, erec_Targyszavak.ControlTypeDataSource);
                                                }
                                            }
                                        }

                                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, dictionaryTipus, "ReturnValuesToParentWindowTipus", false);
                                    }

                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);

                                    if (refreshCallingWindow == "1")
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_TargySzavakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_TargySzavakService();
                                EREC_TargySzavak erec_Targyszavak = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_Targyszavak);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);

                                    String TipusHiddenFieldId = Request.QueryString.Get(QueryStringVars.TipusHiddenFieldId);
                                    if (!String.IsNullOrEmpty(TipusHiddenFieldId))
                                    {
                                        Dictionary<string, string> dictionaryTipus = new Dictionary<string, string>();
                                        dictionaryTipus.Add(TipusHiddenFieldId, erec_Targyszavak.Tipus);

                                        String RegExpHiddenFieldId = Request.QueryString.Get(QueryStringVars.RegExpHiddenFieldId);
                                        if (!String.IsNullOrEmpty(RegExpHiddenFieldId))
                                        {
                                            dictionaryTipus.Add(RegExpHiddenFieldId, erec_Targyszavak.RegExp);

                                            String AlapertelmezettErtekHiddenFieldId = Request.QueryString.Get(QueryStringVars.AlapertelmezettErtekHiddenFieldId);
                                            if (!String.IsNullOrEmpty(AlapertelmezettErtekHiddenFieldId))
                                            {
                                                dictionaryTipus.Add(AlapertelmezettErtekHiddenFieldId, erec_Targyszavak.AlapertelmezettErtek);

                                                String ToolTipHiddenFieldId = Request.QueryString.Get(QueryStringVars.ToolTipHiddenFieldId);
                                                if (!String.IsNullOrEmpty(ToolTipHiddenFieldId))
                                                {
                                                    dictionaryTipus.Add(ToolTipHiddenFieldId, erec_Targyszavak.ToolTip);
                                                }
                                            }

                                            String ControlTypeSourceHiddenFieldId = Request.QueryString.Get(QueryStringVars.ControlTypeSourceHiddenFieldId);
                                            if (!String.IsNullOrEmpty(ControlTypeSourceHiddenFieldId))
                                            {
                                                dictionaryTipus.Add(ControlTypeSourceHiddenFieldId, erec_Targyszavak.ControlTypeSource);

                                                String ControlTypeDataSource = Request.QueryString.Get(QueryStringVars.ControlTypeDataSourceHiddenFieldId);
                                                if (!String.IsNullOrEmpty(ControlTypeDataSource))
                                                {
                                                    dictionaryTipus.Add(ControlTypeDataSource, erec_Targyszavak.ControlTypeDataSource);
                                                }
                                            }
                                        }

                                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, dictionaryTipus, "ReturnValuesToParentWindowTipus", false);
                                    }

                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);

                                    if (refreshCallingWindow == "1")
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }

                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
