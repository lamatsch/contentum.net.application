﻿using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TaroltEljarasLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TaroltEljarasList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        var service = eAdminService.ServiceFactory.GetStoredProcedureService();
        StoredProcedureSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (StoredProcedureSearch)Search.GetSearchObject(Page, new StoredProcedureSearch());
        }
        else
        {
            search = new StoredProcedureSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.OrderBy = "name";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result);
            LovListHeader1.ErrorUpdatePanel.Update();
        }
        else
        {
            foreach (DataRow drDatabase in result.Ds.Tables[0].Rows)
            {
                String Id = drDatabase["Id"].ToString();
                String Name = drDatabase["name"].ToString();
                ListBoxSearchResult.Items.Add(new ListItem(Name, Id));
            }
        }


        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

}