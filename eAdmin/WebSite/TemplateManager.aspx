<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TemplateManager.aspx.cs" Inherits="TemplateManager" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,TemplateManagerHeaderTitle%>" />
    <br />
    <div class="popupBody">
         <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
<BR />
<TABLE style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0>
<TBODY>
<TR>
<TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
<asp:Label id="Label1" runat="server" Text="Templ�t neve:" Font-Bold="true">
</asp:Label>
<BR />
<asp:TextBox id="TextBoxSearch" runat="server" Width="50%">
</asp:TextBox>
&nbsp; 
<asp:Button id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" Text="Keres�s">
</asp:Button> 
<asp:Button id="ButtonAdvancedSearch" runat="server" Text="R�szletes keres�s">
</asp:Button> 
</TD>
</TR>
<TR>
<TD style="PADDING-BOTTOM: 5px; VERTICAL-ALIGN: top; PADDING-TOP: 5px; TEXT-ALIGN: left">
<DIV class="listaFulFelsoCsikKicsi">
<IMG alt="" src="images/hu/design/spacertrans.gif" />
</DIV>
</TD>
</TR>
<TR>
<TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
<asp:ListBox id="ListBoxSearchResult" runat="server" Width="95%" Rows="20" Height="306px">
</asp:ListBox> 
</TD>
</TR>
<TR>
<TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
<asp:ImageButton id="ImageButtonReszletesAdatok" runat="server" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.jpg">
</asp:ImageButton> 
</TD>
</TR>
<TR>
<TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
<asp:Label id="Label2" runat="server" Text="Pdf konverzi�:" Font-Bold="true">
</asp:Label> 
<asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText">
</asp:CheckBox>
 </TD>
 </TR>
 </TBODY>
 </TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
        </eUI:eFormPanel>
        <br />
        <uc2:LovListFooter ID="LovListFooter1" runat="server" />
    </div> 
</asp:Content>
