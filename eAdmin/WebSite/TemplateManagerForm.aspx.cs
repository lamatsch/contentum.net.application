using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class TemplateManagerForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kodcsoportTipus = "TEMPMAN_FILETIPUS";

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxNev.ReadOnly = true;
        KodtarakDropDownListTipus.ReadOnly = true;
        ModulTextBox1.ReadOnly = true;
        DokumentumokTextBox1.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelTipus.CssClass = "mrUrlapInputWaterMarked";
        labelModul.CssClass = "mrUrlapInputWaterMarked";
        labelDokumentum.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TemplateManager" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_TemplateManagerService service = eAdminService.ServiceFactory.GetKRT_TemplateManagerService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_TemplateManager krt_TemplateManager = (KRT_TemplateManager)result.Record;
                    LoadComponentsFromBusinessObject(krt_TemplateManager);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportTipus, FormHeader1.ErrorPanel);
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_TemplateManager"></param>
    private void LoadComponentsFromBusinessObject(KRT_TemplateManager krt_TemplateManager)
    {
        requiredTextBoxNev.Text = krt_TemplateManager.Nev;
        KodtarakDropDownListTipus.FillAndSetSelectedValue(kodcsoportTipus, krt_TemplateManager.Tipus, true, FormHeader1.ErrorPanel);
        ModulTextBox1.Id_HiddenField = krt_TemplateManager.KRT_Modul_Id;
        ModulTextBox1.SetModulTextBoxById(FormHeader1.ErrorPanel);
        DokumentumokTextBox1.Id_HiddenField = krt_TemplateManager.KRT_Dokumentum_Id;
        DokumentumokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);
        ErvenyessegCalendarControl1.ErvKezd = krt_TemplateManager.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_TemplateManager.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_TemplateManager.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_TemplateManager.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_TemplateManager GetBusinessObjectFromComponents()
    {
        KRT_TemplateManager krt_TemplateManager = new KRT_TemplateManager();
        krt_TemplateManager.Updated.SetValueAll(false);
        krt_TemplateManager.Base.Updated.SetValueAll(false);

        krt_TemplateManager.Nev = requiredTextBoxNev.Text;
        if (!String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
        {
            krt_TemplateManager.Tipus = KodtarakDropDownListTipus.SelectedValue;
        }
        else
        {
            krt_TemplateManager.Typed.Tipus = System.Data.SqlTypes.SqlString.Null;
        }
        krt_TemplateManager.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownListTipus);
        krt_TemplateManager.KRT_Modul_Id = ModulTextBox1.Id_HiddenField;
        krt_TemplateManager.Updated.KRT_Modul_Id = pageView.GetUpdatedByView(ModulTextBox1);
        krt_TemplateManager.KRT_Dokumentum_Id = DokumentumokTextBox1.Id_HiddenField;
        krt_TemplateManager.Updated.KRT_Dokumentum_Id = pageView.GetUpdatedByView(DokumentumokTextBox1);
        krt_TemplateManager.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);

        //krt_TemplateManager.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_TemplateManager.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        //krt_TemplateManager.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_TemplateManager.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_TemplateManager, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_TemplateManager.Base.Ver = FormHeader1.Record_Ver;
        krt_TemplateManager.Base.Updated.Ver = true;

        return krt_TemplateManager;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListTipus);
            compSelector.Add_ComponentOnClick(ModulTextBox1);
            compSelector.Add_ComponentOnClick(DokumentumokTextBox1);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;

        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "TemplateManager" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_TemplateManagerService service = eAdminService.ServiceFactory.GetKRT_TemplateManagerService();
                            KRT_TemplateManager krt_TemplateManager = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_TemplateManager);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_TemplateManagerService service = eAdminService.ServiceFactory.GetKRT_TemplateManagerService();
                                KRT_TemplateManager krt_TemplateManager = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_TemplateManager);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
