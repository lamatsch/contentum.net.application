using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Contentum.eQuery;

public partial class TemplateManagerList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TemplateManagerList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_TemplateManagerSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.TemplateManagerListHeader;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("TemplateManagerSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTemplateManager.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("TemplateManagerForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTemplateManager.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeletableConfirm(gridViewTemplateManager.ClientID, "check", "cbModosithato");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewTemplateManager.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewTemplateManager.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewTemplateManager.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewTemplateManager.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTemplateManager.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewTemplateManager;


        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewTemplateManager);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, email-n�l */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_TemplateManagerSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            TemplateManagerGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "TemplateManager" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewTemplateManager);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void TemplateManagerGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewTemplateManager", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewTemplateManager", ViewState);

        TemplateManagerGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void TemplateManagerGridViewBind(String SortExpression, SortDirection SortDirection)
    {


        KRT_TemplateManagerService service = eAdminService.ServiceFactory.GetKRT_TemplateManagerService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_TemplateManagerSearch search = (KRT_TemplateManagerSearch)Search.GetSearchObject(Page, new KRT_TemplateManagerSearch());

        search.OrderBy = Search.GetOrderBy("gridViewTemplateManager", ViewState, SortExpression, SortDirection);

        Result res = service.GetAllWithFK(ExecParam, search);

        UI.GridViewFill(gridViewTemplateManager, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewTemplateManager_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewTemplateManager_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewTemplateManager.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewTemplateManager.PageIndex = ListHeader1.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewTemplateManager.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeTemplateManager);
            TemplateManagerGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeTemplateManager);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewTemplateManager);

        //oldalsz�mok be�ll�t�sa
        ListHeader1.PageCount = gridViewTemplateManager.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewTemplateManager);

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        TemplateManagerGridViewBind();
    }


    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewTemplateManager_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewTemplateManager, selectedRowNumber, "check");

            string id = gridViewTemplateManager.DataKeys[selectedRowNumber].Value.ToString();
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("TemplateManagerForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTemplateManager.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("TemplateManagerForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelTemplateManager.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "Krt_TemplateManager";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelTemplateManager.ClientID);
        }
    }


    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelTemplateManager_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    TemplateManagerGridViewBind();
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTemplateManager();
            TemplateManagerGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedTemplateManager()
    {

        if (FunctionRights.GetFunkcioJog(Page, "TemplateManagerInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewTemplateManager, EErrorPanel1, ErrorUpdatePanel);

            KRT_TemplateManagerService service = eAdminService.ServiceFactory.GetKRT_TemplateManagerService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }


    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedTemplateManagerRecords();
                TemplateManagerGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedTemplateManagerRecords();
                TemplateManagerGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedTemplateManager();
                TemplateManagerGridViewBind();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedTemplateManagerRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewTemplateManager, "KRT_TemplateManager"
            , "TemplateManagerLock", "TemplateManagerForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedTemplateManagerRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewTemplateManager, "KRT_TemplateManager"
            , "TemplateManagerLock", "TemplateManagerForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewTemplateManager -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedTemplateManager()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewTemplateManager, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_TemplateManager");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewTemplateManager_Sorting(object sender, GridViewSortEventArgs e)
    {
        TemplateManagerGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewTemplateManager", ViewState, e.SortExpression));
    }

    #endregion
}
