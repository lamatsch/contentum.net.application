<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TemplateManagerSearch.aspx.cs" Inherits="TemplateManagerSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc3" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc5" %>
<%@ Register Src="Component/ModulTextBox.ascx" TagName="ModulTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/DokumentumokTextBox.ascx" TagName="DokumentumokTextBox" TagPrefix="uc7" %>    

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,TemplateManagerSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelTipus" runat="server" Text="T�pus:" CssClass="mrUrlapInputComboBox"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:KodtarakDropDownList ID="KodtarakDropDownListTipus" runat="server"></uc5:KodtarakDropDownList>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelModul" runat="server" Text="Modul neve:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ModulTextBox ID="ModulTextBox1" CssClass="mrUrlapInput" runat="server" SearchMode="true" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDokumentum" runat="server" Text="Dokumentum neve:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:DokumentumokTextBox ID="DokumentumokTextBox1" CssClass="mrUrlapInput" runat="server" SearchMode="true" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                     <uc4:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                    </uc4:Ervenyesseg_SearchFormComponent>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                     <td colspan="5">
                                     <uc3:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1" runat="server">
                                     </uc3:TalalatokSzama_SearchFormComponent>
                                     </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

