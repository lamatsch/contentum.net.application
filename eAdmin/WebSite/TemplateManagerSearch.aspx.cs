using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class TemplateManagerSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_TemplateManagerSearch);

    private const string kodcsoportTipus = "TEMPMAN_FILETIPUS";

    private void setEqualsOperator(string value, Field field)
    {
        if (!String.IsNullOrEmpty(value))
        {
            field.Value = value;
            field.Operator = Query.Operators.equals;
        }
        else
        {
            field.Value = "";
            field.Operator = "";
        }
    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_TemplateManagerSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_TemplateManagerSearch)Search.GetSearchObject(Page, new KRT_TemplateManagerSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_TemplateManagerSearch _KRT_TemplateManagerSearch = (KRT_TemplateManagerSearch)searchObject;

        if (_KRT_TemplateManagerSearch != null)
        {
            textNev.Text = _KRT_TemplateManagerSearch.Nev.Value;
            KodtarakDropDownListTipus.FillAndSetSelectedValue(kodcsoportTipus, _KRT_TemplateManagerSearch.Tipus.Value, true, SearchHeader1.ErrorPanel);
            ModulTextBox1.Id_HiddenField = _KRT_TemplateManagerSearch.KRT_Modul_Id.Value;
            ModulTextBox1.SetModulTextBoxById(SearchHeader1.ErrorPanel);
            DokumentumokTextBox1.Id_HiddenField = _KRT_TemplateManagerSearch.KRT_Dokumentum_Id.Value;
            DokumentumokTextBox1.SetTextBoxById(SearchHeader1.ErrorPanel);

            Ervenyesseg_SearchFormComponent1.SetDefault(
               _KRT_TemplateManagerSearch.ErvKezd, _KRT_TemplateManagerSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_TemplateManagerSearch SetSearchObjectFromComponents()
    {
        KRT_TemplateManagerSearch _KRT_TemplateManagerSearch = (KRT_TemplateManagerSearch)SearchHeader1.TemplateObject;

        if (_KRT_TemplateManagerSearch == null)
        {
            _KRT_TemplateManagerSearch = new KRT_TemplateManagerSearch();
        }

        _KRT_TemplateManagerSearch.Nev.Value = textNev.Text;
        _KRT_TemplateManagerSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        setEqualsOperator(KodtarakDropDownListTipus.SelectedValue, _KRT_TemplateManagerSearch.Tipus);
        setEqualsOperator(ModulTextBox1.Id_HiddenField, _KRT_TemplateManagerSearch.KRT_Modul_Id);
        setEqualsOperator(DokumentumokTextBox1.Id_HiddenField, _KRT_TemplateManagerSearch.KRT_Dokumentum_Id);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _KRT_TemplateManagerSearch.ErvKezd, _KRT_TemplateManagerSearch.ErvVege);

        return _KRT_TemplateManagerSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_TemplateManagerSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_TemplateManagerSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_TemplateManagerSearch();
    }
}
