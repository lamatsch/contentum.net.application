<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TemplateManagerTeszt.aspx.cs" Inherits="TemplateManagerTeszt" Title="Untitled Page" ValidateRequest="false" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,TemplateManagerHeaderTitle%>" />
    <br />
    <div class="popupBody">
        &nbsp;
         <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
<BR /><TABLE style="WIDTH: 90%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="Label2" runat="server" Font-Bold="true" Text="Pdf konverzió:">
</asp:Label> <asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText">
</asp:CheckBox> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
             <table style="width: 70%">
                 <tr>
                     <td style="height: 21px; text-align: center">
                         <asp:ImageButton ID="ImageOk" runat="server" ImageUrl="~/images/hu/ovalgomb/kivalaszt.jpg"
                             OnClick="ImageButton_Click" onmouseout="swapByName(this.id,'kivalaszt.jpg')"
                             onmouseover="swapByName(this.id,'kivalaszt2.jpg')" Visible="True" />&nbsp;</td>
                     <td style="height: 21px; text-align: center">
                         &nbsp;<asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                             OnClientClick="window.close(); return false;" onmouseout="swapByName(this.id,'megsem.jpg')"
                             onmouseover="swapByName(this.id,'megsem2.jpg')" Visible="True" /></td>
                 </tr>
             </table>
             &nbsp;
        </eUI:eFormPanel>
        <br />
    </div> 
</asp:Content>