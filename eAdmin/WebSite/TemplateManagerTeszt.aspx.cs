using Contentum.eAdmin.Utility;
using System;

public partial class TemplateManagerTeszt : System.Web.UI.Page
{
    
//    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "TemplateManagerList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        String tablaNev = Request.QueryString.Get("tablaNev");
        ImageOk.Attributes.Add("onclick", "javascript:window.open('TemplateWordTeszt.aspx?tablaNev=" + tablaNev + "&check=" + check.Checked + "')");
    }

    protected void ImageButton_Click(object sender, EventArgs e)
    {
        JavaScripts.RegisterCloseWindowClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }
}
