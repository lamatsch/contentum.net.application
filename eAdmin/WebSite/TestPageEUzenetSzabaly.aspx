<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TestPageEUzenetSzabaly.aspx.cs" Inherits="TestPageEUzenetSzabaly" Title="E-�zenet szab�lyok teszt futtat�sa" %>

<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="ucFCS" %>
<%@ Register Src="~/Component/FelhasznaloCsoportTextBox.ascx" TagPrefix="uc1" TagName="FelhasznaloCsoportTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="PanelMain" HorizontalAlign="Left">
        <%--        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
        <h1>E-�zenet szab�lyok teszt futtat�sa</h1>


        <br />
        <asp:Panel runat="server" ID="PanelSelectedebeadvany">
            <div style="font-weight: bold; background-color: lightslategray; color: white; margin: 3px;">
                <table>
                    <tr>
                        <td align="left">KIV�LASZTOTT EBEADV�NY
     
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel7" runat="server">
            <div style="margin: 20px;">
                <div style="max-width: 1200px; overflow-x: auto; white-space: nowrap;">
                    <asp:GridView ID="GridViewSelectedEbeadvany" runat="server" CellPadding="4" Width="500px"
                        EnableModelValidation="True" ForeColor="#333333" GridLines="None" DataKeyNames="Id" EmptyDataText="Nincsenek eBeadv�ny t�telek">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="60" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="HiddenFieldEBeadvanId" Value='<%# Eval("Id") %>' />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="KIV�LASZT"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
        <div style="margin: 20px;">
            <asp:Button ID="ButtonExecuteRules" runat="server" OnClick="ButtonExecuteRules_Click" Text="Szab�ly(ok) lefuttat�sa a kiv�lasztott eBeadv�nyra" Width="600px" />
        </div>
        
        <asp:Panel runat="server" ID="Panel5">
            <div style="font-weight: bold; background-color: lightslategray; color: white; margin: 3px;">
                <table>
                    <tr>
                        <td align="left">SZAB�LY FUT�SNAPL� 
     
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel6" runat="server">
            <div style="margin: 20px;">
                <asp:TextBox ID="TextBoxErrors" runat="server" Text="" Width="800px" TextMode="MultiLine" Rows="30" Wrap="true"></asp:TextBox>
                <br />
            </div>
        </asp:Panel>




    </asp:Panel>
</asp:Content>
