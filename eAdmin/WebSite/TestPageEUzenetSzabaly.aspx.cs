﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.EKF;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestPageEUzenetSzabaly : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    #region Base Page
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InitControls();
        }
    }
    #endregion Base Page

    private void SetEUzenetGrid()
    {
        Result result = GetEBeadvanyok();
    }
    private void RefreshGrids()
    {
        FillEBeadvanyokList();
    }
    private void InitControls()
    {
        try
        {
            RefreshGrids();
        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
            return;
        }
    }
    private void FillEBeadvanyokList()
    {
        try
        {
            Result resultEUzenet = GetEBeadvanyok();
            GridViewSelectedEbeadvany.DataSource = resultEUzenet.Ds.Tables[0];
            GridViewSelectedEbeadvany.DataBind();
        }
        catch (Exception exc)
        {
            JavascriptMessageBox(exc);
            return;
        }
    }
  
    private void JavascriptMessageBox(Exception exc)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
      "Hiba",
      "alert('EK= " + exc.Message + "');",
      true);
        return;
    }

    #region SERVICES
    private Result GetIktatoErkeztetoKonyvek(string iktatoErkezteto)
    {
        Contentum.eRecord.Service.EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
        search.IktatoErkezteto.Value = iktatoErkezteto;
        search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

        //search.LezarasDatuma.Value = "";
        //search.LezarasDatuma.Operator = Contentum.eQuery.Query.Operators.isnull;

        search.OrderBy = "Iktatohely";

        Result res = service.GetAll(ExecParam, search);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new ResultException(res);

        return res;
    }
    private Result GetPartnerek()
    {
        Contentum.eAdmin.Service.KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_PartnerekSearch searchPa = new KRT_PartnerekSearch();
        KRT_CimekSearch searchCi = new KRT_CimekSearch();
        Result res = service.GetAllWithCim(ExecParam, searchPa, searchCi);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new ResultException(res);

        return res;
    }
    private Result GetFelhasznalok()
    {
        Contentum.eAdmin.Service.KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_FelhasznalokSearch searchPa = new KRT_FelhasznalokSearch();

        Result res = service.GetAll(ExecParam, searchPa);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new ResultException(res);

        return res;
    }
    private Result GetCsoportok()
    {
        Contentum.eAdmin.Service.KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_CsoportokSearch searchPa = new KRT_CsoportokSearch();

        Result res = service.GetAll(ExecParam, searchPa);
        if (!string.IsNullOrEmpty(res.ErrorCode))
            throw new ResultException(res);

        return res;
    }
    private Result GetAutoEUzenetSzabalyok()
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        INT_AutoEUzenetSzabalyService ekfService = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        INT_AutoEUzenetSzabalySearch search = new INT_AutoEUzenetSzabalySearch();
        search.OrderBy = "Sorrend ASC";
        Result resultGetRules = ekfService.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(resultGetRules.ErrorCode))
            throw new ResultException(resultGetRules);
        return resultGetRules;
    }

    private Result GetAutoEUzenetSzabaly(string id)
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        INT_AutoEUzenetSzabalyService ekfService = Contentum.eAdmin.BaseUtility.eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        execParam.Record_Id = id;
        Result resultGetRules = ekfService.Get(execParam);

        if (!string.IsNullOrEmpty(resultGetRules.ErrorCode))
            throw new ResultException(resultGetRules);
        return resultGetRules;
    }
    private Result GetEBeadvanyok()
    {      
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eRecord.Service.EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
        search.TopRow = 100;
        search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.Irany.Value = "0";
        search.Irany.Operator = Contentum.eQuery.Query.Operators.equals;
        search.Cel.Value = "WEB";
        search.Cel.Operator = Contentum.eQuery.Query.Operators.equals;
        //search.KR_Fiok.Value = "BPFPH";
       // search.KR_Fiok.Operator = Contentum.eQuery.Query.Operators.equals;
        search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.ErvVege.Value = DateTime.Now.ToString();
        search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greater;

        Result eBeadvanyResult = eBeadvService.GetAll(execParam, search);
        if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
            throw new ResultException(eBeadvanyResult);
        return eBeadvanyResult;
    }

    private Result GetIrattariTetelek()
    {
        ExecParam execParam = Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        Contentum.eRecord.Service.EREC_IraIrattariTetelekService svc = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
        Result result = svc.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result.ErrorCode))
            throw new ResultException(result);
        return result;
    }

    private void AddAutoEUzenetSzabaly(EKFRuleClass rule)
    {
        INT_AutoEUzenetSzabaly szabaly = new INT_AutoEUzenetSzabaly();
        szabaly.Id = rule.IdProperty.ToString();
        szabaly.Szabaly = EKFManager.EKFSerialize(rule);
        szabaly.Leallito = rule.ParametersProperty.StopRuleProperty.Value.ToString();
        szabaly.Leiras = rule.ParametersProperty.DescriptionProperty;
        szabaly.Nev = rule.ParametersProperty.NameProperty ?? "NA";
        szabaly.Tipus = rule.ParametersProperty.TypeProperty.ToString();
        szabaly.Base.Note = rule.ToString();

        INT_AutoEUzenetSzabalyService ekfInstance = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result resultInsert = ekfInstance.Insert(execParam, szabaly);
        if (!string.IsNullOrEmpty(resultInsert.ErrorCode))
            throw new Exception(resultInsert.ErrorCode + " " + resultInsert.ErrorMessage);
    }
    private void UpdateAutoEUzenetSzabaly(INT_AutoEUzenetSzabaly szabaly, EKFRuleClass rule)
    {
        szabaly.Szabaly = EKFManager.EKFSerialize(rule);
        szabaly.Leallito = rule.ParametersProperty.StopRuleProperty.Value.ToString();
        szabaly.Leiras = rule.ParametersProperty.DescriptionProperty;
        szabaly.Nev = rule.ParametersProperty.NameProperty ?? "NA";
        szabaly.Tipus = rule.ParametersProperty.TypeProperty.ToString();
        szabaly.Base.Note = rule.ToString();

        INT_AutoEUzenetSzabalyService ekfInstance = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = szabaly.Id;
        Result resultUpdate = ekfInstance.Update(execParam, szabaly);
        if (!string.IsNullOrEmpty(resultUpdate.ErrorCode))
            throw new Exception(resultUpdate.ErrorCode + " " + resultUpdate.ErrorMessage);
    }
    #endregion

    #region UI MAIN PROCEDURE

    private void StartUIExecution()
    {
        string selectedEBeadvanyId = null;
        GridViewRow row = GridViewSelectedEbeadvany.SelectedRow;
        if (row == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
          "Hiba",
          "alert('Nincs kiválasztott tétel');",
          true);
            return;
        }
        Control c = row.Cells[0].FindControl("HiddenFieldEBeadvanId");
        if (c is HiddenField)
        {
            selectedEBeadvanyId = ((HiddenField)c).Value;
        }
        if (string.IsNullOrEmpty(selectedEBeadvanyId))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
            "Hiba",
            "alert('Paraméter hiba');",
            true);
            return;
        }

        try
        {
            EKFManager man = new EKFManager(new Guid(selectedEBeadvanyId), Contentum.eAdmin.Utility.UI.SetExecParamDefault(Page, new ExecParam()));
            man.Start();
            if (man.OccuredMessages.Count > 0)
            {
                foreach (string item in man.OccuredMessages)
                {
                    TextBoxErrors.Text += item + Environment.NewLine;
                }
            }
        }
        catch (Exception exc)
        {
            TextBoxErrors.Text += exc.Message;
        }
    }

    #endregion

    #region HELPER
    #endregion

    protected void ButtonExecuteRules_Click(object sender, EventArgs e)
    {
        StartUIExecution();
    }
}

