﻿using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebMetodusLovList : Contentum.eUtility.UI.PageBase
{
    private string szolgaltatas = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "WebSzolgaltatasList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        szolgaltatas = Request.QueryString.Get("WebSzervizId");

        LovListFooter1.ButtonsClick += new
            CommandEventHandler(LovListFooter_ButtonsClick);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }

    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        var service = eAdminService.ServiceFactory.GetKRT_Log_WebServiceService();
        KRT_LogSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_LogSearch)Search.GetSearchObject(Page, new KRT_LogSearch());
        }
        else
        {
            search = new KRT_LogSearch();

            search.KRT_Log_WebServiceSearch.Name.Operator = Search.GetOperatorByLikeCharater(szolgaltatas);
            search.KRT_Log_WebServiceSearch.Name.Value = szolgaltatas;

            search.KRT_Log_WebServiceSearch.Method.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.KRT_Log_WebServiceSearch.Method.Value = SearchKey;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetMethodsByService(ExecParam, search);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result);
            LovListHeader1.ErrorUpdatePanel.Update();
        }
        else
        {
            foreach (DataRow drDatabase in result.Ds.Tables[0].Rows)
            {
                String Id = drDatabase["Id"].ToString();
                String Name = drDatabase["Method"].ToString();
                ListBoxSearchResult.Items.Add(new ListItem(Name, Id));
            }
        }


        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

}