using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Az �sszes t�bl�ban jelenl�v� szabv�nyos oszlopok �rt�kei
    /// </summary>
    [Serializable()]
    public class BaseDocument : Base_BaseDocument
    {
        [NonSerializedAttribute]           
        public BaseDocumentTyped Typed = new BaseDocumentTyped();
        [NonSerializedAttribute]
        public BaseDocumentUpdated Updated = new BaseDocumentUpdated();
                      
        /// <summary>
        /// Ver property </summary>
        public String Ver
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ver); }
            set { Typed.Ver = Utility.SetSqlInt32FromString(value, Typed.Ver); }                                            
        }
                   
           
        /// <summary>
        /// Note property </summary>
        public String Note
        {
            get { return Utility.GetStringFromSqlString(Typed.Note); }
            set { Typed.Note = Utility.SetSqlStringFromString(value, Typed.Note); }                                            
        }
                   
           
        /// <summary>
        /// Stat_id property </summary>
        public String Stat_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Stat_id); }
            set { Typed.Stat_id = Utility.SetSqlGuidFromString(value, Typed.Stat_id); }                                            
        }
                   
           
        ///// <summary>
        ///// ErvKezd property </summary>
        //public String ErvKezd
        //{
        //    get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
        //    set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        //}
                   
           
        ///// <summary>
        ///// ErvVege property </summary>
        //public String ErvVege
        //{
        //    get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
        //    set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        //}
                   
           
        /// <summary>
        /// Letrehozo_id property </summary>
        public String Letrehozo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Letrehozo_id); }
            set { Typed.Letrehozo_id = Utility.SetSqlGuidFromString(value, Typed.Letrehozo_id); }                                            
        }
                   
           
        /// <summary>
        /// LetrehozasIdo property </summary>
        public String LetrehozasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LetrehozasIdo); }
            set { Typed.LetrehozasIdo = Utility.SetSqlDateTimeFromString(value, Typed.LetrehozasIdo); }                                            
        }
                   
           
        /// <summary>
        /// Modosito_id property </summary>
        public String Modosito_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modosito_id); }
            set { Typed.Modosito_id = Utility.SetSqlGuidFromString(value, Typed.Modosito_id); }                                            
        }
                   
           
        /// <summary>
        /// ModositasIdo property </summary>
        public String ModositasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ModositasIdo); }
            set { Typed.ModositasIdo = Utility.SetSqlDateTimeFromString(value, Typed.ModositasIdo); }                                            
        }
                   
           
        /// <summary>
        /// Zarolo_id property </summary>
        public String Zarolo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Zarolo_id); }
            set { Typed.Zarolo_id = Utility.SetSqlGuidFromString(value, Typed.Zarolo_id); }                                            
        }
                   
           
        /// <summary>
        /// ZarolasIdo property </summary>
        public String ZarolasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ZarolasIdo); }
            set { Typed.ZarolasIdo = Utility.SetSqlDateTimeFromString(value, Typed.ZarolasIdo); }                                            
        }
                   
           
        /// <summary>
        /// Tranz_id property </summary>
        public String Tranz_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tranz_id); }
            set { Typed.Tranz_id = Utility.SetSqlGuidFromString(value, Typed.Tranz_id); }                                            
        }
                   
           
        /// <summary>
        /// UIAccessLog_id property </summary>
        public String UIAccessLog_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UIAccessLog_id); }
            set { Typed.UIAccessLog_id = Utility.SetSqlGuidFromString(value, Typed.UIAccessLog_id); }                                            
        }                           
    }
}
