
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// _SzabvanyosOszlopok BusinessDocument Class </summary>
    [Serializable()]    
    public abstract class Base_BaseDocument
    {
        /// <summary>
        /// Az összes táblában jelenlévő szabányos oszlopokból melyet kell update-elnie a web szolgálatatásnak 
        /// </summary>
        public class BaseDocumentUpdated
        {
            private bool _Ver = true;
            public bool Ver
            {
                get { return _Ver; }
                set { _Ver = value; }
            }

            private bool _Note = true;
            public bool Note
            {
                get { return _Note; }
                set { _Note = value; }
            }

            private bool _Stat_id = true;
            public bool Stat_id
            {
                get { return _Stat_id; }
                set { _Stat_id = value; }
            }

            //private bool _ErvKezd = true;
            //public bool ErvKezd
            //{
            //    get { return _ErvKezd; }
            //    set { _ErvKezd = value; }
            //}

            //private bool _ErvVege = true;
            //public bool ErvVege
            //{
            //    get { return _ErvVege; }
            //    set { _ErvVege = value; }
            //}
            private bool _Letrehozo_id = true;
            public bool Letrehozo_id
            {
                get { return _Letrehozo_id; }
                set { _Letrehozo_id = value; }
            }
            private bool _LetrehozasIdo = true;
            public bool LetrehozasIdo
            {
                get { return _LetrehozasIdo; }
                set { _LetrehozasIdo = value; }
            }
            private bool _Modosito_id = true;
            public bool Modosito_id
            {
                get { return _Modosito_id; }
                set { _Modosito_id = value; }
            }
            private bool _ModositasIdo = true;
            public bool ModositasIdo
            {
                get { return _ModositasIdo; }
                set { _ModositasIdo = value; }
            }
            private bool _Zarolo_id = true;
            public bool Zarolo_id
            {
                get { return _Zarolo_id; }
                set { _Zarolo_id = value; }
            }
            private bool _ZarolasIdo = true;
            public bool ZarolasIdo
            {
                get { return _ZarolasIdo; }
                set { _ZarolasIdo = value; }
            }
            private bool _Tranz_id = true;
            public bool Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }
            }
            private bool _UIAccessLog_id = true;
            public bool UIAccessLog_id
            {
                get { return _UIAccessLog_id; }
                set { _UIAccessLog_id = value; }
            }
            
            public void SetValueAll(bool Value)
            {
                Ver = Value;
                Note = Value;
                Stat_id = Value;
                //ErvKezd = Value;
                //ErvVege = Value;
                Letrehozo_id = Value;
                LetrehozasIdo = Value;
                Modosito_id = Value;
                ModositasIdo = Value;
                Zarolo_id = Value;
                ZarolasIdo = Value;
                Tranz_id = Value;
                UIAccessLog_id = Value;
            }
        }

        public class BaseDocumentTyped
        {

            private SqlInt32 _Ver = SqlInt32.Null;

            /// <summary>
            /// Ver Base property </summary>
            public SqlInt32 Ver
            {
                get { return _Ver; }
                set { _Ver = value; }
            }


            private SqlString _Note = SqlString.Null;

            /// <summary>
            /// Note Base property </summary>
            public SqlString Note
            {
                get { return _Note; }
                set { _Note = value; }
            }


            private SqlGuid _Stat_id = SqlGuid.Null;

            /// <summary>
            /// Stat_id Base property </summary>
            public SqlGuid Stat_id
            {
                get { return _Stat_id; }
                set { _Stat_id = value; }
            }


            //private SqlDateTime _ErvKezd = SqlDateTime.Null;

            ///// <summary>
            ///// ErvKezd Base property </summary>
            //public SqlDateTime ErvKezd
            //{
            //    get { return _ErvKezd; }
            //    set { _ErvKezd = value; }
            //}


            //private SqlDateTime _ErvVege = SqlDateTime.Null;

            ///// <summary>
            ///// ErvVege Base property </summary>
            //public SqlDateTime ErvVege
            //{
            //    get { return _ErvVege; }
            //    set { _ErvVege = value; }
            //}


            private SqlGuid _Letrehozo_id = SqlGuid.Null;

            /// <summary>
            /// Letrehozo_id Base property </summary>
            public SqlGuid Letrehozo_id
            {
                get { return _Letrehozo_id; }
                set { _Letrehozo_id = value; }
            }


            private SqlDateTime _LetrehozasIdo = SqlDateTime.Null;

            /// <summary>
            /// LetrehozasIdo Base property </summary>
            public SqlDateTime LetrehozasIdo
            {
                get { return _LetrehozasIdo; }
                set { _LetrehozasIdo = value; }
            }


            private SqlGuid _Modosito_id = SqlGuid.Null;

            /// <summary>
            /// Modosito_id Base property </summary>
            public SqlGuid Modosito_id
            {
                get { return _Modosito_id; }
                set { _Modosito_id = value; }
            }


            private SqlDateTime _ModositasIdo = SqlDateTime.Null;

            /// <summary>
            /// ModositasIdo Base property </summary>
            public SqlDateTime ModositasIdo
            {
                get { return _ModositasIdo; }
                set { _ModositasIdo = value; }
            }


            private SqlGuid _Zarolo_id = SqlGuid.Null;

            /// <summary>
            /// Zarolo_id Base property </summary>
            public SqlGuid Zarolo_id
            {
                get { return _Zarolo_id; }
                set { _Zarolo_id = value; }
            }


            private SqlDateTime _ZarolasIdo = SqlDateTime.Null;

            /// <summary>
            /// ZarolasIdo Base property </summary>
            public SqlDateTime ZarolasIdo
            {
                get { return _ZarolasIdo; }
                set { _ZarolasIdo = value; }
            }


            private SqlGuid _Tranz_id = SqlGuid.Null;

            /// <summary>
            /// Tranz_id Base property </summary>
            public SqlGuid Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }
            }


            private SqlGuid _UIAccessLog_id = SqlGuid.Null;

            /// <summary>
            /// UIAccessLog_id Base property </summary>
            public SqlGuid UIAccessLog_id
            {
                get { return _UIAccessLog_id; }
                set { _UIAccessLog_id = value; }
            }
        }
    }
}
