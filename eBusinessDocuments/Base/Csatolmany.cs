using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// A felt�ltend� csatolm�ny adatait tartalmazza
    /// </summary>
    [Serializable()]
    public class Csatolmany
    {
        private String _Nev = null;
        /// <summary>
        /// F�jl neve
        /// </summary>
        public String Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }

        private byte[] _Tartalom = null;
        /// <summary>
        /// Bin�ris f�jltartalom
        /// </summary>
        public byte[] Tartalom
        {
            get { return _Tartalom; }
            set { _Tartalom = value; }
        }

        private string _TartalomHash = null;
        /// <summary>
        /// A tartalomra sz�m�tott hash �rt�k
        /// </summary>
        public string TartalomHash
        {
            get { return _TartalomHash; }
            set { _TartalomHash = value; }
        }

        private String _SourceSharePath = null;
        /// <summary>
        /// A csatolm�ny el�r�si �tvonala
        /// </summary>
        public String SourceSharePath
        {
            get { return _SourceSharePath; }
            set { _SourceSharePath = value; }
        }

        private String _Megnyithato = null;
        /// <summary>
        /// F�jl 'Megnyithat�' tulajdons�ga
        /// </summary>
        public String Megnyithato
        {
            get { return _Megnyithato; }
            set { _Megnyithato = value; }
        }

        private String _Olvashato = null;
        /// <summary>
        /// F�jl 'Olvashat�' tulajdons�ga
        /// </summary>
        public String Olvashato
        {
            get { return _Olvashato; }
            set { _Olvashato = value; }
        }

        private string _Titkositas = null;
        /// <summary>
        /// Titkos�tott-e a f�jl
        /// </summary>
        public string Titkositas
        {
            get { return _Titkositas; }
            set { _Titkositas = value; }
        }

        private String _ElektronikusAlairas = null;
        /// <summary>
        /// Elektronikus al��r�s m�dja
        /// </summary>
        public String ElektronikusAlairas
        {
            get { return _ElektronikusAlairas; }
            set { _ElektronikusAlairas = value; }
        }

        private string _BarCode = null;
        /// <summary>
        /// Csatolm�ny vonalk�dja
        /// </summary>
        public string BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }

        private Alairas _AlairasAdatok = new Alairas();
        /// <summary>
        /// F�jl al��r�s�ra vonatkoz� inform�ci�k
        /// </summary>
        public Alairas AlairasAdatok
        {
            get { return _AlairasAdatok; }
            set { _AlairasAdatok = value; }
        }

        private OCR _ocrAdatok = new OCR();
        /// <summary>
        /// OCR adatok
        /// </summary>
        public OCR OCRAdatok
        {
            get { return _ocrAdatok; }
            set { _ocrAdatok = value; }
        }

        private string _SablonAzonosito;

        public string SablonAzonosito
        {
            get { return _SablonAzonosito; }
            set { _SablonAzonosito = value; }
        }


        /// <summary>
        /// F�jl al��r�s�ra vonatkoz� inform�ci�k
        /// </summary>
        [Serializable()]
        public class Alairas
        {
            private bool _CsatolmanyAlairando = false;
            /// <summary>
            /// Al��rand�-e a f�jl
            /// </summary>
            public bool CsatolmanyAlairando
            {
                get { return _CsatolmanyAlairando; }
                set { _CsatolmanyAlairando = value; }
            }

            private string _AlairasSzabaly_Id;
            /// <summary>
            /// Al��r�sszab�ly id
            /// </summary>
            public string AlairasSzabaly_Id 
            {
                get { return _AlairasSzabaly_Id; }
                set { _AlairasSzabaly_Id = value; }
            }

            private string _EASzKod;
            /// <summary>
            /// Az al��r�sszab�ly k�dja -  Szerver oldali al��r�skor ez megy a '<config_item></config_item>' r�szbe
            /// </summary>
            public string EASzKod
            {
                get { return _EASzKod; }
                set { _EASzKod = value; }
            }

            private int _AlairasSzint = 0;
            /// <summary>
            /// Az AlairasSzabaly_Id -hoz tartoz� al��r�sszint a KRT_AlairasTipusok t�bl�b�l
            /// </summary>
            public int AlairasSzint
            {
                get { return _AlairasSzint; }
                set { _AlairasSzint = value; }
            }

            private string _AlairasMod;
            /// <summary>
            /// Al��r�sm�d
            /// </summary>
            public string AlairasMod
            {
                get { return _AlairasMod; }
                set { _AlairasMod = value; }
            }

            private string _AlairasTulajdonos;
            /// <summary>
            /// Al��r�s tulajdonosa
            /// </summary>
            public string AlairasTulajdonos
            {
                get { return _AlairasTulajdonos; }
                set { _AlairasTulajdonos = value; }
            }

            private string _Tanusitvany_Id;
            /// <summary>
            /// Tanus�tv�ny Id
            /// </summary>
            public string Tanusitvany_Id
            {
                get { return _Tanusitvany_Id; }
                set { _Tanusitvany_Id = value; }
            }

            private int _KivarasiIdo = 0;
            /// <summary>
            /// A kiv�r�si id� PERCben megadva
            /// </summary>
            public int KivarasiIdo
            {
                get { return _KivarasiIdo; }
                set { _KivarasiIdo = value; }
            }

            /// <summary>
            /// Az eDocumentService Upload-j�nak �tadand� xml r�szlet
            /// </summary>            
            public string GetInnerXml()
            {
                return String.Format("<alairaskell>{0}</alairaskell>"
                                    + "<alairasSzabalyId>{1}</alairasSzabalyId>"
                                    + "<alairasSzint>{2}</alairasSzint>"
                                    + "<easzKod>{3}</easzKod>"
                                    + "<tanusitvanyId>{4}</tanusitvanyId>"
                                    + "<kivarasiIdo>{5}</kivarasiIdo>"
                                    + "<alairasMod>{6}</alairasMod>"
                                    + "<alairasTulajdonos>{7}</alairasTulajdonos>"
                                    , ((this.CsatolmanyAlairando == true) ? "IGEN" : "NEM")
                                    , this.AlairasSzabaly_Id
                                    , this.AlairasSzint
                                    , this.EASzKod
                                    , this.Tanusitvany_Id
                                    , this.KivarasiIdo
                                    , this.AlairasMod
                                    , this.AlairasTulajdonos
                                    );
            }
        }

        /// <summary>
        /// OCR adatok
        /// </summary>
        [Serializable()]
        public class OCR
        {
            private string _ocrallapot = "0";
            /// <summary>
            /// OCR �llapota
            /// </summary>
            public string OCRAllapot
            {
                get { return _ocrallapot; }
                set { _ocrallapot = value; }
            }

            private string _ocrprioritas = "0";
            /// <summary>
            /// OCR priorit�sa
            /// </summary>
            public string OCRPrioritas
            {
                get { return _ocrprioritas; }
                set { _ocrprioritas = value; }
            }

        }
    }
}
