﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Iktatáshoz szükséges kiegészítő paraméterek
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(EPostazasiParameterek))]
    public class EPostazasiParameterek
    {
        /// <summary>
        /// Paraméterek kiírása, pontosvesszővel elválasztva,
        /// név=érték párokként (csak kitöltött mezők)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            System.Reflection.PropertyInfo[] BusinessDocumentProperties = this.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo P in BusinessDocumentProperties)
            {
                if (P != null)
                {
                    System.Reflection.PropertyInfo BusinessDocumentProperty = this.GetType().GetProperty(P.Name);

                    object FieldObject = BusinessDocumentProperty.GetValue(this, null);
                    if (FieldObject != null && !String.IsNullOrEmpty(FieldObject.ToString()))
                    {
                        sb.AppendFormat("{0}={1};", P.Name, FieldObject.ToString());
                    }
                }
            }

            return sb.ToString();
            //return base.ToString();
        }

        /// <summary>
        /// Ha értéke true, az oldalak száma és az ahhoz
        /// kapcsolódó információk is visszaadásra kerülnek
        /// az első eredménytáblában
        /// </summary>
        public bool IsForStatistics
        {
            get { return Utility.GetBoolFromSqlChars(Typed.IsForStatistics); }
            set { Typed.IsForStatistics = Utility.SetSqlCharsFromBool(value, Typed.IsForStatistics); }
        }

        /// <summary>
        /// Ha értéke true, a közönséges tételek nem kerülnek az eredményhalmazba
        /// </summary>
        public bool IgnoreKozonsegesTetelek
        {
            get { return Utility.GetBoolFromSqlChars(Typed.IgnoreKozonsegesTetelek); }
            set { Typed.IgnoreKozonsegesTetelek = Utility.SetSqlCharsFromBool(value, Typed.IgnoreKozonsegesTetelek); }
        }

        /// <summary>
        /// Ha értéke true, a nyilvántartott tételek nem kerülnek az eredményhalmazba
        /// </summary>
        public bool IgnoreNyilvantartottTetelek
        {
            get { return Utility.GetBoolFromSqlChars(Typed.IgnoreNyilvantartottTetelek); }
            set { Typed.IgnoreNyilvantartottTetelek = Utility.SetSqlCharsFromBool(value, Typed.IgnoreNyilvantartottTetelek); }
        }

        /// <summary>
        /// StartDate property 
        /// E-feladójegyzékbe exportálandó tételek ettől a dátumtól (EREC_KuldKuldemenyek.BelyegzoDatuma)
        /// </summary>
        public String StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDate); }
            set { Typed.StartDate = Utility.SetSqlDateTimeFromString(value, Typed.StartDate); }
        }

        /// <summary>
        /// EndDate property 
        /// E-feladójegyzékbe exportálandó tételek: eddig a dátumig (EREC_KuldKuldemenyek.BelyegzoDatuma)
        /// </summary>
        public String EndDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.EndDate); }
            set { Typed.EndDate = Utility.SetSqlDateTimeFromString(value, Typed.EndDate); }
        }

        /// <summary>
        /// WindowSize property 
        /// E-feladójegyzékbe exportálandó tételek: egy lapra (fájlba, eredménytáblába, XML-be) ennyi tétel
        /// </summary>
        public int WindowSize
        {
            get { return (int)Typed.WindowSize; }
            set { Typed.WindowSize = (System.Data.SqlTypes.SqlInt32)value; }
        }

        /// <summary>
        /// StartPage property 
        /// E-feladójegyzékbe exportálandó tételek: ettől a lapszámtól (a lapok mérete a WindowSize függvénye)
        /// </summary>
        public int StartPage
        {
            get { return (int)Typed.StartPage; }
            set { Typed.StartPage = (System.Data.SqlTypes.SqlInt32)value; }
        }

        ///// <summary>
        ///// PageCount property 
        ///// E-feladójegyzékbe exportálandó tételek: a startpagetől kezdve ennyi lap (a lapok mérete a WindowSize függvénye)
        ///// ha értéke negatív, akkor az összes
        ///// </summary>
        //public int PageCount
        //{
        //    get { return Typed.PageCount.IsNull ? -1 : (int)Typed.PageCount; }
        //    set { Typed.PageCount = (value < 0 ? System.Data.SqlTypes.SqlInt32.Null : (System.Data.SqlTypes.SqlInt32)value); }
        //}

        /// <summary>
        /// Postakonyv_Id property
        /// E-feladójegyzékbe exportálandó tételek: ebből a postakönyvből
        /// </summary>
        public string Postakonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Postakonyv_Id); }
            set { Typed.Postakonyv_Id = Utility.SetSqlGuidFromString(value, Typed.Postakonyv_Id); }
        }

        /// <summary>
        /// Kikuldo_Id property
        /// E-feladójegyzékbe exportálandó tételek: csak ennek a csoportnak a küldeményei (kereséskor opcionálisan megadható)
        /// </summary>
        public string Kikuldo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kikuldo_Id); }
            set { Typed.Kikuldo_Id = Utility.SetSqlGuidFromString(value, Typed.Kikuldo_Id); }
        }


        public TypedFields Typed = new TypedFields();

        public class TypedFields
        {
            private System.Data.SqlTypes.SqlGuid postakonyv_Id = System.Data.SqlTypes.SqlGuid.Null;
            /// <summary>
            /// Postakonyv_Id property
            /// E-feladójegyzékbe exportálandó tételek: ebből a postakönyvből
            /// </summary>
            public System.Data.SqlTypes.SqlGuid Postakonyv_Id
            {
                get { return postakonyv_Id; }
                set { postakonyv_Id = value; }
            }

            private System.Data.SqlTypes.SqlDateTime startDate = System.Data.SqlTypes.SqlDateTime.Null;
            /// <summary>
            /// StartDate property 
            /// E-feladójegyzékbe exportálandó tételek ettől a dátumtól (EREC_KuldKuldemenyek.BelyegzoDatuma)
            /// </summary>
            public System.Data.SqlTypes.SqlDateTime StartDate
            {
                get { return startDate; }
                set { startDate = value; }
            }

            private System.Data.SqlTypes.SqlDateTime endDate = System.Data.SqlTypes.SqlDateTime.Null;
            /// <summary>
            /// EndDate property 
            /// E-feladójegyzékbe exportálandó tételek: eddig a dátumig (EREC_KuldKuldemenyek.BelyegzoDatuma)
            /// </summary>
            public System.Data.SqlTypes.SqlDateTime EndDate
            {
                get { return endDate; }
                set { endDate = value; }
            }

            private System.Data.SqlTypes.SqlGuid kikuldo_Id = System.Data.SqlTypes.SqlGuid.Null;
            /// <summary>
            /// Kikuldo_Id property
            /// E-feladójegyzékbe exportálandó tételek: csak ennek a csoportnak a küldeményei (kereséskor opcionálisan megadható)
            /// </summary>
            public System.Data.SqlTypes.SqlGuid Kikuldo_Id
            {
                get { return kikuldo_Id; }
                set { kikuldo_Id = value; }
            }

            private System.Data.SqlTypes.SqlChars isForStatistics = new System.Data.SqlTypes.SqlChars("0");
            /// <summary>
            /// Ha értéke "1", az oldalak száma és az ahhoz
            /// kapcsolódó információk is visszaadásra kerülnek
            /// az első eredménytáblában
            /// </summary>
            public System.Data.SqlTypes.SqlChars IsForStatistics
            {
                get { return isForStatistics; }
                set { isForStatistics = value; }
            }

            private System.Data.SqlTypes.SqlChars ignoreKozonsegesTetelek = new System.Data.SqlTypes.SqlChars("0");
            /// <summary>
            /// Ha értéke "1", a közönséges tételek nem kerülnek az eredményhalmazba
            /// </summary>
            public System.Data.SqlTypes.SqlChars IgnoreKozonsegesTetelek
            {
                get { return ignoreKozonsegesTetelek; }
                set { ignoreKozonsegesTetelek = value; }
            }

            private System.Data.SqlTypes.SqlChars ignoreNyilvantartottTetelek = new System.Data.SqlTypes.SqlChars("0");
            /// <summary>
            /// Ha értéke "1", a nyilvántartott tételek nem kerülnek az eredményhalmazba
            /// </summary>
            public System.Data.SqlTypes.SqlChars IgnoreNyilvantartottTetelek
            {
                get { return ignoreNyilvantartottTetelek; }
                set { ignoreNyilvantartottTetelek = value; }
            }

            #region "Lapozás"
            private System.Data.SqlTypes.SqlInt32 windowSize = 100000;
            /// <summary>
            /// WindowSize property 
            /// E-feladójegyzékbe exportálandó tételek: egy lapra (fájlba, eredménytáblába, XML-be) ennyi tétel
            /// </summary>
            public System.Data.SqlTypes.SqlInt32 WindowSize
            {
                get { return windowSize; }
                set { windowSize = value; }
            }

            private System.Data.SqlTypes.SqlInt32 startPage = 1;
            /// <summary>
            /// StartPage property 
            /// E-feladójegyzékbe exportálandó tételek: ettől a lapszámtól (a lapok mérete a WindowSize függvénye)
            /// </summary>
            public System.Data.SqlTypes.SqlInt32 StartPage
            {
                get { return startPage; }
                set { startPage = value; }
            }

            //private System.Data.SqlTypes.SqlInt32 pageCount = -1;
            ///// <summary>
            ///// PageCount property 
            ///// E-feladójegyzékbe exportálandó tételek: a startpagetől kezdve ennyi lap (a lapok mérete a WindowSize függvénye)
            ///// ha értéke negatív, akkor az összes
            ///// </summary>
            //public System.Data.SqlTypes.SqlInt32 PageCount
            //{
            //    get { return pageCount; }
            //    set { pageCount = value; }
            //}
            #endregion "Lapozás"
        }
    }
}
