using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// �rkeztet�shez sz�ks�ges kieg�sz�t� param�terek
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(ErkeztetesParameterek))]
    public class ErkeztetesParameterek
    {
        private List<EREC_Mellekletek> mellekletek;

        public List<EREC_Mellekletek> Mellekletek
        {
            get { return mellekletek; }
            set { mellekletek = value; }
        }

        private bool _TomegesErkeztetesVegrehajtasa = false;

        public bool TomegesErkeztetesVegrehajtasa
        {
            get { return _TomegesErkeztetesVegrehajtasa; }
            set { _TomegesErkeztetesVegrehajtasa = value; }
        }

        private DateTime _TomegesErkeztetesDatuma;

        public DateTime TomegesErkeztetesDatuma
        {
            get { return _TomegesErkeztetesDatuma; }
            set { _TomegesErkeztetesDatuma = value; }
        }
    }

    /// <summary>
    /// Sz�mla �rkeztet�shez sz�ks�ges kieg�sz�t� param�terek
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(SzamlaErkeztetesParameterek))]
    public class SzamlaErkeztetesParameterek : ErkeztetesParameterek
    {
        private bool _BindPartnerCim = false;
        /// <summary>
        /// Ha nem l�tezik a partner �s c�m k�z�tti kapcsolat, automatikusan l�trej�jj�n-e
        /// </summary>
        public bool BindPartnerCim
        {
            get { return _BindPartnerCim; }
            set { _BindPartnerCim = value; }
        }

        private bool _InsertSzemelyOrVallakozasToPartnerIfMissing = false;
        /// <summary>
        /// Ha nem l�tezik a partnerhez tartoz� szem�ly vagy v�llalkoz�s rekord,
        /// automatikusan l�trej�jj�n-e, pl. ad�sz�m besz�r�s�hoz
        /// </summary>
        public bool InsertSzemelyOrVallakozasToPartnerIfMissing
        {
            get { return _InsertSzemelyOrVallakozasToPartnerIfMissing; }
            set { _InsertSzemelyOrVallakozasToPartnerIfMissing = value; }
        }

        private string _Vonalkod = null;
        /// <summary>
        /// Vonalkod (k�ldem�ny)
        /// </summary>
        public string Vonalkod
        {
            get { return _Vonalkod; }
            set { _Vonalkod = value; }
        }

        private string _BeerkezesIdeje = null;
        /// <summary>
        /// Sz�mla be�rkez�s�nek/�rkeztet�s�nek id�pontja
        /// </summary>
        public string BeerkezesIdeje
        {
            get { return _BeerkezesIdeje; }
            set { _BeerkezesIdeje = value; }
        }

        private string _Adoszam = null;
        /// <summary>
        /// Ad�sz�m (partner)
        /// </summary>
        public string Adoszam
        {
            get { return _Adoszam; }
            set { _Adoszam = value; }
        }

        private bool _IsKulfoldiAdoszam = false;
        /// <summary>
        /// K�lf�ldi ad�sz�m-e (partner)
        /// </summary>
        public bool IsKulfoldiAdoszam
        {
            get { return _IsKulfoldiAdoszam; }
            set { _IsKulfoldiAdoszam = value; }
        }

        private string _Csoport_Id_SzervezetiEgyseg = String.Empty;
        /// <summary>
        /// Szervezeti egys�g azonos�t�ja (Bels� c�mzett)
        /// </summary>
        public string Csoport_Id_SzervezetiEgyseg
        {
            get { return _Csoport_Id_SzervezetiEgyseg; }
            set { _Csoport_Id_SzervezetiEgyseg = value; }
        }

        private string _SzervezetiEgyseg = String.Empty;
        /// <summary>
        /// Szervezeti egys�g neve (Bels� c�mzett)
        /// </summary>
        public string SzervezetiEgyseg
        {
            get { return _SzervezetiEgyseg; }
            set { _SzervezetiEgyseg = value; }
        }

    }
}
