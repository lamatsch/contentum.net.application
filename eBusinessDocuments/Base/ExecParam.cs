using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.IO;
using System.Collections;
using System.Xml;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot(ElementName="CallingChain",IsNullable=true,Namespace="")]
    public class CallingChain : Stack<string>,System.Xml.Serialization.IXmlSerializable
    {

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.ReadStartElement("CallingChain");
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<string>));
            List<string> temp = (List<string>)serializer.Deserialize(reader);
            temp.Reverse();
            foreach (string item in temp)
            {
                this.Push(item);
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            int count = this.Count;
            List<string> temp = new List<string>(count);
            foreach (string item in this)
            {
                temp.Add(item);
            }
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<string>));
            serializer.Serialize(writer, temp);
        }

        #endregion
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot(ElementName="LogData",IsNullable=true,Namespace="")]
    public class LogData
    {
        private KRT_Log_Login logIn = null;

        public KRT_Log_Login LogIn
        {
            get { return logIn; }
            set { logIn = value; }
        }

        private KRT_Log_Page page = null;

        public KRT_Log_Page Page
        {
            get { return page; }
            set { page = value; }
        }

        private List<KRT_Log_WebService> webServices = new List<KRT_Log_WebService>();

        public List<KRT_Log_WebService> WebServices
        {
            get { return webServices; }
            set { webServices = value; }
        }

        private KRT_Log_StoredProcedure storedProcedure = null;

        public KRT_Log_StoredProcedure StoredProcedure
        {
            get { return storedProcedure; }
            set { storedProcedure = value; }
        }
    }

    [Serializable()]
    public class Paging
    {
        private int pageNumber = -1;
        public int PageNumber 
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        private int pageSize = -1;
        public int PageSize 
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        private string selectedRowId = "";
        public string SelectedRowId
        {
            get { return selectedRowId; }
            set { selectedRowId = value; }
        }
    }

    /// <summary>
    /// Inform�ci�kat tartalmaz a m�veletet v�gz� felhaszn�l�r�l,
    /// illetve az objektumr�l, melyen a m�veletet v�gezni kell (Record_Id mez�)
    /// </summary>
    [Serializable()]
    public class ExecParam : BaseExecParam
    {
        [NonSerializedAttribute]
        public BaseExecParamTyped Typed = new BaseExecParamTyped();

        private Boolean _Fake = false;
        
        public Boolean Fake
        {
            get { return _Fake; }
            set { _Fake = value; }
        }    
        
        /// <summary>
        /// A m�veletet ind�t� alkalmaz�s azonos�t�ja
        /// </summary>
        public String Alkalmazas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Alkalmazas_Id); }
            set { Typed.Alkalmazas_Id = Utility.SetSqlGuidFromString(value, Typed.Alkalmazas_Id); }
        }      

        /// <summary>
        /// A m�veletet v�gz� felhaszn�l� azonos�t�ja.
        /// Helyettes�t�s eset�n a helyettes�tett felhaszn�l�
        /// </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }
        }

        /// <summary>
        /// Az a felhaszn�l�, aki t�nylegesen haszn�lja a rendszert
        /// (Helyettes�t�s eset�n � lesz, aki helyettes�t, �s Felhasznalo_Id lesz a helyettes�tett)
        /// </summary>
        public String LoginUser_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.LoginUser_Id); }
            set { Typed.LoginUser_Id = Utility.SetSqlGuidFromString(value, Typed.LoginUser_Id); }
        }

        /// <summary>
        /// Helyettes�t�s vagy megb�z�s eset�n a helyettes�t�si rekord azonos�t�ja.
        /// </summary>
        public String Helyettesites_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Helyettesites_Id); }
            set { Typed.Helyettesites_Id = Utility.SetSqlGuidFromString(value, Typed.Helyettesites_Id); }
        }

        /// <summary>
        /// A felhaszn�l� (helyettes�t�s eset�n a helyettes�tett) szervezete.
        /// </summary>
        public String FelhasznaloSzervezet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloSzervezet_Id); }
            set { Typed.FelhasznaloSzervezet_Id = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloSzervezet_Id); }
        }

        /// <summary>
        /// A felhaszn�l� (helyettes�t�s eset�n a helyettes�tett) a bel�p�skor kiv�lasztott csoporttags�g�nak azonos�t�ja.
        /// </summary>
        public String CsoportTag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CsoportTag_Id); }
            set { Typed.CsoportTag_Id = Utility.SetSqlGuidFromString(value, Typed.CsoportTag_Id); }
        }
        
        private String _UserHostAddress = "";
        /// <summary>
        /// A m�veletet ind�t� kliens g�p c�me
        /// </summary>
        public String UserHostAddress
        {
            get { return _UserHostAddress; }
            set { _UserHostAddress = value; }
        }

        /// <summary>
        /// Az objektum azonos�t�ja, melyen a m�velet v�grehajtand�
        /// </summary>
        public String Record_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Record_Id); }
            set { Typed.Record_Id = Utility.SetSqlGuidFromString(value, Typed.Record_Id); }
        }

        /// <summary>
        /// Nem haszn�lt
        /// </summary>
        public String UIAccessLog_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UIAccessLog_Id); }
            set { Typed.UIAccessLog_Id = Utility.SetSqlGuidFromString(value, Typed.UIAccessLog_Id); }
        }

        /// <summary>
        /// Page szint� log bejegyz�s azonos�t�ja        
        /// </summary>
        public String Page_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Page_Id); }
            set { Typed.Page_Id = Utility.SetSqlGuidFromString(value, Typed.Page_Id); }
        }

        /// <summary>
        /// Hiv�si l�nc t�rol�sa
        /// </summary>
        public CallingChain CallingChain
        {
            get { return Typed.CallingChain; }
            set { Typed.CallingChain= value; }
        }

        /// <summary>
        /// List�k lapoz�s�hoz sz�ks�ges inform�ci�k
        /// </summary>
        public Paging Paging
        {
            get { return Typed.Paging; }
            set { Typed.Paging = value; }
        }

        /// <summary>
        /// A felhaszn�l� orgja
        /// </summary>
        public string Org_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org_Id); }
            set { Typed.Org_Id = Utility.SetSqlGuidFromString(value, Typed.Page_Id); }
        }

        private string _FunkcioKod;

        public string FunkcioKod
        {
            get { return _FunkcioKod; }
            set { _FunkcioKod = value; }
        }

        /// <summary>
        /// A web servicek h�v�si l�nc�nak elment�se
        /// </summary>
      
        /// <summary>
        /// K�sz�t egy �j ExecParam objektumot ugyanazokkal a mez��rt�kekkel, mint a megadott ExecParam
        /// </summary>
        /// <returns></returns>
        public ExecParam Clone()
        {
            //---
            ////ExecParam newExecParam = (ExecParam)this.MemberwiseClone();
            //---

            //---
            //System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //MemoryStream stream = new MemoryStream();
            //bFormatter.Serialize(stream, this);
            //stream.Seek(0, SeekOrigin.Begin);
            //ExecParam newExecParam = (ExecParam)bFormatter.Deserialize(stream);
            //---

            //---
            ExecParam newExecParam = new ExecParam();

            newExecParam.Alkalmazas_Id = this.Alkalmazas_Id;
            newExecParam.Felhasznalo_Id = this.Felhasznalo_Id;
            newExecParam.LoginUser_Id = this.LoginUser_Id;
            newExecParam.Helyettesites_Id = this.Helyettesites_Id;
            newExecParam.FelhasznaloSzervezet_Id = this.FelhasznaloSzervezet_Id;
            newExecParam.CsoportTag_Id = this.CsoportTag_Id;
            newExecParam.UserHostAddress = this.UserHostAddress;
            newExecParam.Record_Id = this.Record_Id;
            newExecParam.UIAccessLog_Id = this.UIAccessLog_Id;
            newExecParam.Page_Id = this.Page_Id;
            //referencia m�sol�sa
            newExecParam.CallingChain = this.CallingChain;
            //---
            newExecParam.Org_Id = this.Org_Id;
            // CR 3107 : Post�z�sn�l �j iratp�ld�ny l�trehoz�s�n�l Postaz�s funkci�k�d n�z�se
            // CR kapcs�n kl�noz�skor a Funkciokod-ot is kl�nozza 
            newExecParam.FunkcioKod = this.FunkcioKod;

            return newExecParam;
        }

    }

    /// <summary>
    /// ExecParam oszt�ly t�rol� oszt�lya, mely t�pusosan t�rolja a mez�k �rt�keit
    /// A mez�k jelent�se megegyezik az ExecParam oszt�lyn�l le�rtakkal
    /// </summary>
    [Serializable()]
    public abstract class BaseExecParam
    {
        public class BaseExecParamTyped
        {
            private SqlGuid _Alkalmazas_Id = SqlGuid.Null;

            /// <summary>
            /// Alkalmazas_Id Base property </summary>
            public SqlGuid Alkalmazas_Id
            {
                get { return _Alkalmazas_Id; }
                set { _Alkalmazas_Id = value; }
            }

            private SqlGuid _Felhasznalo_Id = SqlGuid.Null;

            /// <summary>
            /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }
            }

            private SqlGuid _LoginUser_Id = SqlGuid.Null;

            /// <summary>
            /// Felhasznalo_Id Base property </summary>
            public SqlGuid LoginUser_Id
            {
                get { return _LoginUser_Id; }
                set { _LoginUser_Id = value; }
            }

            private SqlGuid _Helyettesites_Id = SqlGuid.Null;

            /// <summary>
            /// Felhasznalo_Id Base property </summary>
            public SqlGuid Helyettesites_Id
            {
                get { return _Helyettesites_Id; }
                set { _Helyettesites_Id = value; }
            }
            
            private SqlGuid _FelhasznaloSzervezet_Id = SqlGuid.Null;

            /// <summary>
            /// FelhasznaloSzervezet_Id Base property
            /// </summary>
            public SqlGuid FelhasznaloSzervezet_Id
            {
                get { return _FelhasznaloSzervezet_Id; }
                set { _FelhasznaloSzervezet_Id = value; }
            }

            private SqlGuid _CsoportTag_Id = SqlGuid.Null;

            /// <summary>
            /// CsoportTag_Id Base property
            /// </summary>
            public SqlGuid CsoportTag_Id
            {
                get { return _CsoportTag_Id; }
                set { _CsoportTag_Id = value; }
            }

            private SqlGuid _Record_Id = SqlGuid.Null;

            /// <summary>
            /// Record_Id Base property </summary>
            public SqlGuid Record_Id
            {
                get { return _Record_Id; }
                set { _Record_Id = value; }
            }

            private SqlGuid _UIAccessLog_Id = SqlGuid.Null;

            /// <summary>
            /// UIAccessLog_Id Base property </summary>
            public SqlGuid UIAccessLog_Id
            {
                get { return _UIAccessLog_Id; }
                set { _UIAccessLog_Id = value; }
            }

            private SqlGuid _Page_Id = SqlGuid.Null;

            /// <summary>
            /// Loggol�shoz a h�v� oldal Id-j�nak let�rol�sa
            /// </summary>
            public SqlGuid Page_Id
            {
                get { return _Page_Id; }
                set { _Page_Id = value; }
            }

            private CallingChain _CallingChain = new CallingChain();

            /// <summary>
            /// Hiv�si l�nc t�rol�sa
            /// </summary>
            public CallingChain CallingChain
            {
                get { return _CallingChain; }
                set { _CallingChain = value; }
            }

            private Paging paging = new Paging();

            public Paging Paging {
                get { return paging; }
                set { paging = value; }
            }

            /// <summary>
            /// A felhaszn�l� orgja
            /// </summary>
            private SqlGuid _Org_Id = SqlGuid.Null;

            public SqlGuid Org_Id
            {
                get { return _Org_Id; }
                set { _Org_Id = value; }
            }



        }
    }               
    
}
