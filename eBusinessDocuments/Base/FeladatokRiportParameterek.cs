﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Feladat riport generáláshoz szükséges kiegészítő paraméterek
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(FeladatokRiportParameterek))]
    public class FeladatokRiportParameterek
    {
        public static string GetSQLParamName(string ParamName)
        {
            string SQLParamName = String.Empty;
            switch (ParamName)
            {
                case "Datum":
                    SQLParamName = "@datum";
                    break;
                case "Felhasznalo":
                    SQLParamName = "@Felhasznalo";
                    break;
                case "Szervezet":
                    SQLParamName = "@Szervezet";
                    break;
                case "LejaratTol":
                    SQLParamName = "@LejaratKatTol";
                    break;
                case "LejaratIg":
                    SQLParamName = "@LejaratKatIg";
                    break;
                case "PrioritasTol":
                    SQLParamName = "@PrioritasTol";
                    break;
                case "PrioritasIg":
                    SQLParamName = "@PrioritasIg";
                    break;
            }

            return SQLParamName;
        }

        private SqlDateTime _Datum = SqlDateTime.Null;
        /// <summary>
        /// Datum
        /// </summary>
        public SqlDateTime Datum
        {
            get { return _Datum; }
            set { _Datum = value; }
        }

        private SqlString _Felhasznalo = SqlString.Null;
        /// <summary>
        /// Felhasznalo
        /// </summary>
        public SqlString Felhasznalo
        {
            get { return _Felhasznalo; }
            set { _Felhasznalo = value; }
        }

        private SqlString _Szervezet = SqlString.Null;
        /// <summary>
        /// Szervezet
        /// </summary>
        public SqlString Szervezet
        {
            get { return _Szervezet; }
            set { _Szervezet = value; }
        }

        private SqlInt32 _LejaratTol = SqlInt32.Null;
        /// <summary>
        /// LejaratTol
        /// </summary>
        public SqlInt32 LejaratTol
        {
            get { return _LejaratTol; }
            set { _LejaratTol = value; }
        }

        private SqlInt32 _LejaratIg = SqlInt32.Null;
        /// <summary>
        /// LejaratIg
        /// </summary>
        public SqlInt32 LejaratIg
        {
            get { return _LejaratIg; }
            set { _LejaratIg = value; }
        }

        private SqlInt32 _PrioritasTol = SqlInt32.Null;
        /// <summary>
        /// PrioritasTol
        /// </summary>
        public SqlInt32 PrioritasTol
        {
            get { return _PrioritasTol; }
            set { _PrioritasTol = value; }
        }

        private SqlInt32 _PrioritasIg = SqlInt32.Null;
        /// <summary>
        /// PrioritasIg
        /// </summary>
        public SqlInt32 PrioritasIg
        {
            get { return _PrioritasIg; }
            set { _PrioritasIg = value; }
        }

    }
}
