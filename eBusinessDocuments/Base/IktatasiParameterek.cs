﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Iktatáshoz szükséges kiegészítő paraméterek
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(IktatasiParameterek))]
    public class IktatasiParameterek
    {
        private string iratpeldany_Vonalkod = null;
        /// <summary>
        /// Iratpéldány vonalkódja
        /// </summary>
        public string Iratpeldany_Vonalkod
        {
            get { return iratpeldany_Vonalkod; }
            set { iratpeldany_Vonalkod = value; }
        }

        private bool iratpeldanyVonalkodGeneralasHaNincs = false;
        /// <summary>
        /// Generálódjon-e vonalkód az iratpéldánynak (ha van megadva vonalkód, nem fog generálódni)
        /// </summary>
        public bool IratpeldanyVonalkodGeneralasHaNincs
        {
            get { return iratpeldanyVonalkodGeneralasHaNincs; }
            set { iratpeldanyVonalkodGeneralasHaNincs = value; }
        }

        private string regiAdatAzonosito = null;
        /// <summary>
        /// Migrált előzmény ügyirat azonosítója
        /// </summary>
        public string RegiAdatAzonosito
        {
            get { return regiAdatAzonosito; }
            set { regiAdatAzonosito = value; }
        }

        private string regiAdatId = null;
        /// <summary>
        /// Migrált előzmény ügyirat Id-ja
        /// </summary>
        public string RegiAdatId
        {
            get { return regiAdatId; }
            set { regiAdatId = value; }
        }

        private string szerelendoUgyiratId = null;
        /// <summary>
        /// EDOK-beli, lezárt iktatókönyvben lévő előzmény ügyirat Id-ja
        /// </summary>
        public string SzerelendoUgyiratId
        {
            get { return szerelendoUgyiratId; }
            set { szerelendoUgyiratId = value; }
        }

        private string adoszam = null;
        /// <summary>
        /// Adószám
        /// </summary>
        public string Adoszam
        {
            get { return adoszam; }
            set { adoszam = value; }
        }

        private string ugykorId = null;
        /// <summary>
        /// Ügykör azonosítója
        /// </summary>
        public string UgykorId
        {
            get { return ugykorId; }
            set { ugykorId = value; }
        }

        private string ugytipus = null;
        /// <summary>
        /// Ügytípus azonosítója
        /// </summary>
        public string Ugytipus
        {
            get { return ugytipus; }
            set { ugytipus = value; }
        }

        private string kuldemenyId = null;
        /// <summary>
        /// Iktatandó küldemény azonosítója
        /// </summary>
        public string KuldemenyId
        {
            get { return kuldemenyId; }
            set { kuldemenyId = value; }
        }

        private bool ugyiratUjranyitasaHaLezart = false;
        /// <summary>
        /// Alszámos iktatásnál, ha lezárt az ügyirat, újra legyen-e nyitva?
        /// </summary>
        public bool UgyiratUjranyitasaHaLezart
        {
            get { return ugyiratUjranyitasaHaLezart; }
            set { ugyiratUjranyitasaHaLezart = value; }
        }

        private bool ugyiratPeldanySzukseges = false;
        /// <summary>
        /// Készüljön-e ügyiratpéldány
        /// </summary>
        public bool UgyiratPeldanySzukseges
        {
            get { return ugyiratPeldanySzukseges; }
            set { ugyiratPeldanySzukseges = value; }
        }

        private bool keszitoPeldanyaSzukseges = false;
        /// <summary>
        /// Készüljön-e iratpéldány az iktató személy részére?
        /// </summary>
        public bool KeszitoPeldanyaSzukseges
        {
            get { return keszitoPeldanyaSzukseges; }
            set { keszitoPeldanyaSzukseges = value; }
        }

        private bool atiktatas = false;
        /// <summary>
        /// Átiktatásról van-e szó?
        /// </summary>
        public bool Atiktatas
        {
            get { return atiktatas; }
            set { atiktatas = value; }
        }

        private string iratId;
        /// <summary>
        /// Irat Id-ja
        /// </summary>
        public string IratId
        {
            get { return iratId; }
            set { iratId = value; }
        }

        private bool emptyUgyiratSztorno = false;
        /// <summary>
        /// Átiktatás után ha üres lesz az ügyirat, sztornózódjon-e?
        /// </summary>
        public bool EmptyUgyiratSztorno
        {
            get { return emptyUgyiratSztorno; }
            set { emptyUgyiratSztorno = value; }
        }

        
        private string ugyiratUjHatarido = null;
         
        /// <summary>
        /// Alszámra iktatásnál ügyirat határidő módosítás
        /// </summary>    
        public string UgyiratUjHatarido
        {
            get { return ugyiratUjHatarido; }
            set { ugyiratUjHatarido = value; }
        }


        private bool munkaPeldany = false;

        /// <summary>
        /// Ha true, munkapéldány készül, nem iktatott irat/ügyirat --> nem kap főszámot/alszámot
        /// </summary>
        public bool MunkaPeldany 
        {
            get { return munkaPeldany; }
            set { munkaPeldany = value; } 
        }

        private Csatolmany dokumentum = null;

        public Csatolmany Dokumentum
        {
            get { return dokumentum; }
            set { dokumentum = value; }
        }

        private List<EREC_Mellekletek> mellekletek;
        /// <summary>
        /// Mellékletek
        /// (TÜK-ös belső irat iktatásnál fordul elő, BLG#1402)
        /// </summary>
        public List<EREC_Mellekletek> Mellekletek
        {
            get { return mellekletek; }
            set { mellekletek = value; }
        }

        private List<EREC_PldIratPeldanyok> _Peldanyok;

        /// <summary>
        /// A bejövő irat példányainak soszámai adhatóak meg
        /// </summary>
        public List<EREC_PldIratPeldanyok> Peldanyok
        {
            get { return _Peldanyok; }
            set { _Peldanyok = value; }
        }
        private bool _TomegesIktatasVegrehajtasa = false;

        public bool TomegesIktatasVegrehajtasa
        {
            get { return _TomegesIktatasVegrehajtasa; }
            set { _TomegesIktatasVegrehajtasa = value; }
        }

        private DateTime _TomegesIktatasDatuma;

        public DateTime TomegesIktatasDatuma
        {
            get { return _TomegesIktatasDatuma; }
            set { _TomegesIktatasDatuma = value; }
        }

        // BUG_6117
        private bool _isSZUR = false;

        public bool IsSZUR
        {
            get { return _isSZUR; }
            set { _isSZUR = value; }
        }

        public string IT1 { get; set; }
        public string IT2 { get; set; }
        public string IT3 { get; set; }
    }
}
