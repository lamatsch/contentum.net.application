using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.Data;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Bejelentkezési információk
    /// </summary>
    [Serializable()]
    public class Login : BaseLogin
    {
        [NonSerializedAttribute]
        public BaseLoginTyped Typed = new BaseLoginTyped();

        /// <summary>
        /// Felhasználónév
        /// </summary>
        public String FelhasznaloNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FelhasznaloNev); }
            set { Typed.FelhasznaloNev = Utility.SetSqlStringFromString(value, Typed.FelhasznaloNev); }
        }

        /// <summary>
        /// Jelszó
        /// </summary>
        public String Jelszo
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelszo); }
            set { Typed.Jelszo = Utility.SetSqlStringFromString(value, Typed.Jelszo); }
        }

        /// <summary>
        /// Bejelentkezés típusa (Windows/Basic)
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public abstract class BaseLogin
    {
        public class BaseLoginTyped
        {
            private SqlString _FelhasznaloNev = SqlString.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlString FelhasznaloNev
            {
                get { return _FelhasznaloNev; }
                set { _FelhasznaloNev = value; }
            }

            private SqlString _Jelszo = SqlString.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlString Jelszo
            {
                get { return _Jelszo; }
                set { _Jelszo = value; }
            }

            private SqlString _Tipus = SqlString.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }
        }
    }


}
