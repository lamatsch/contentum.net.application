﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public class Objektum
    {
        private string _Id;

        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Type;

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _Azonosito;

        public string Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
    }
}
