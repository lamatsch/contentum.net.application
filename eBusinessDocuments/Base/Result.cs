using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.Data;
using System.Data.SqlClient;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Ez az oszt�ly tartalmazza a szabv�nyos web szolg�ltat�sok �ltal visszaadott eredm�nyt
    /// </summary>
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(Result))]
    public class Result
    {
        private String _error = null;
        private String _errormessage = null;
        private String _errortype = "0";
        private ErrorDetails _errorDetail = null;
        private String _uid = null;
        private DataSet _ds = null;
        private Object _record = null;
        private bool _IsUpdated = false;
        private bool _IsLogExist = false;
        private SerializableSqlCommand _SqlComm = null;
        /// <summary>
        /// Hiba�zenet k�dja. Az ASP.Net -es fel�leten a k�d alapj�n resource f�jlb�l �r�dik ki a hibat�pushoz tartoz�
        /// hiba�zenet.
        /// </summary>
        public String ErrorCode
        {
            get { return _error; }
            set { _error = value; }
        }
        /// <summary>
        /// Az ErrorCode hibat�pushoz tartoz�, a hib�t pontos�t� egyedi hiba�zenet
        /// </summary>
        public String ErrorMessage
        {
            get { return _errormessage; }
            set { _errormessage = value; }
        }

        /// <summary>
        /// A hiba s�lyoss�g�nak be�ll�t�sa: warning(figyelmeztet�s, a felhaszn�l� viselked�s�ben elt�r a megkiv�nt�l), error(igazi hiba, a rendszer rossz)
        /// </summary>
        public String ErrorType
        {
            get { return _errortype; }
            set { _errortype = value; }
        }

        public List<T> GetData<T>()
        {
            var resultList = new List<T>();          
            if(!HasData()) { return resultList; }
            if(this.Record != null)// ha ez nem null akkor record_id alapj�n volt a lek�rdez�s ez�rt csak egy elemet adunk vissza
            {            
                resultList.Add((T)this.Record);
                return resultList;
            }

            foreach(System.Data.DataRow row in this.Ds.Tables[0].Rows)
            {
                var newObject = Activator.CreateInstance(typeof(T));
                Utility.LoadBusinessDocumentFromDataRow(newObject, row);
                resultList.Add((T)newObject);
            }
            return resultList;
        }

        public bool HasData()
        {
            bool isDSEmpty = this.Ds == null || this.Ds.Tables == null || this.Ds.Tables.Count == 0 || this.Ds.Tables[0].Rows == null || this.Ds.Tables[0].Rows.Count == 0;
            bool isRecordEmpty = this.Record == null;

            if (isDSEmpty  && isRecordEmpty )
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hiba r�szletez�se, hiba forr�s�nak megad�sa
        /// </summary>
        public ErrorDetails ErrorDetail
        {
            get { return _errorDetail; }
            set { _errorDetail = value; }
        }

        /// <summary>
        /// A Bussiness Objektum egyedi azonos�t�ja (pl. besz�r�s ut�n sz�ks�ges tudni, mi lett az �rt�ke)
        /// </summary>
        public String Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }

        /// <summary>
        /// (pl. a get, getAll �ltal visszaadott) eredm�nyhalmaz
        /// </summary>
        public DataSet Ds
        {
            get { return _ds; }
            set { _ds = value; }
        }

        /// <summary>
        /// Get webmet�dusok �ltal visszaadott �zleti objektum
        /// </summary>
        public Object Record
        {
            get { return _record; }
            set { _record = value; }
        }

        public bool IsLogExist
        {
            get { return _IsLogExist; }
            set { _IsLogExist = value; }
        }

        /// <summary>
        /// T�rt�nt-e hiba a m�velet (webszolg�ltat�s) v�grehajt�sakor?
        /// </summary>
        public bool IsError
        {
            get
            {
                if (!String.IsNullOrEmpty(ErrorCode))
                    return true;
                else
                    return false;
            }
        }

        public bool IsUpdated
        {
            get { return _IsUpdated; }
            set { _IsUpdated = value; }
        }

        /// <summary>
        /// DataSet elements count.
        /// </summary>
        public int GetCount
        {
            get
            {
                if (Ds == null || Ds.Tables == null || Ds.Tables.Count < 1 || Ds.Tables[0].Rows.Count < 1)
                    return 0;
                else
                    return Ds.Tables[0].Rows.Count;
            }
        }

        /// <summary>
        /// GetRows
        /// </summary>
        public DataRowCollection GetRows()
        {
            if (Ds == null || Ds.Tables == null || Ds.Tables.Count < 1 || Ds.Tables[0].Rows.Count < 1)
                return null;
            else
                return Ds.Tables[0].Rows;
        }

        public SerializableSqlCommand SqlCommand
        {
            get { return _SqlComm; }
            set { _SqlComm = value; }
        }

        //internal string Serialize()
        //{
        //    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(this.GetType());

        //    System.IO.TextWriter tw = new System.IO.StringWriter();

        //    ser.Serialize(tw, this);

        //    return tw.ToString();
        //}

    }

    [Serializable()]
    public class SerializableSqlParams
    {
        public string Name;
        public string Value;

        public SerializableSqlParams()
        {
            this.Name = string.Empty;
            this.Value = string.Empty;
        }

        public SerializableSqlParams(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }
    }



    [Serializable()]
    public class SerializableSqlCommand
    {
        public string CommandText;
        public SerializableSqlParams[] Parameters;

        public SerializableSqlCommand()
        {
            this.CommandText = string.Empty;
            this.Parameters = new SerializableSqlParams[0];
        }

        public SerializableSqlCommand(SqlCommand sqlComm)
        {
            this.CommandText = sqlComm.CommandText;
            this.Parameters = new SerializableSqlParams[sqlComm.Parameters.Count];

            int i = 0;

            foreach (SqlParameter sp in sqlComm.Parameters)
            {
                this.Parameters[i] = new SerializableSqlParams(sp.ParameterName, sp.Value.ToString());
                i = i + 1;
            }
        }

        public string GetParamValue(string Name)
        {
            foreach (SerializableSqlParams param in this.Parameters)
            {
                if (param.Name == Name)
                {
                    return param.Value;
                }
            }

            return string.Empty;
        }
    }



    /// <summary>
    /// Hiba r�szletez�s�re: mely objektum, mely mez�j�vel volt probl�ma
    /// </summary>
    [Serializable()]
    public class ErrorDetails
    {
        private int code = 0;
        /// <summary>
        /// Hiba k�dja
        /// </summary>
        public int Code
        {
            get { return code; }
            set { code = value; }
        }

        private string message;
        /// <summary>
        /// Hiba�zenet
        /// </summary>
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private string objectType;
        /// <summary>
        /// Objektum t�pusa (pl. EREC_UgyUgyiratok)
        /// </summary>
        public string ObjectType
        {
            get { return objectType; }
            set { objectType = value; }
        }

        private string objectId;
        /// <summary>
        /// Objektum azonos�t�ja
        /// </summary>
        public string ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        private string columnType;
        /// <summary>
        /// Objektum probl�m�s oszlop�nak t�pusa (pl. Allapot)
        /// </summary>
        public string ColumnType
        {
            get { return columnType; }
            set { columnType = value; }
        }

        private string columnValue;
        /// <summary>
        /// Objektum probl�m�s oszlop�nak �rt�ke
        /// </summary>
        public string ColumnValue
        {
            get { return columnValue; }
            set { columnValue = value; }
        }

        /// <summary>
        /// Default konstruktor
        /// </summary>
        public ErrorDetails()
        {
        }

        public ErrorDetails(string message, string objectType, string objectId, string columnType, string columnValue)
        {
            this.Message = message;
            this.ObjectType = objectType;
            this.ObjectId = objectId;
            this.ColumnType = columnType;
            this.ColumnValue = columnValue;
        }

        public ErrorDetails(int code, string objectType, string objectId, string columnType, string columnValue)
        {
            this.Code = code;
            this.ObjectType = objectType;
            this.ObjectId = objectId;
            this.ColumnType = columnType;
            this.ColumnValue = columnValue;
        }

        /// <summary>
        /// Volt-e hiba?
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if (Code > 0 || !String.IsNullOrEmpty(Message))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

    }

}
