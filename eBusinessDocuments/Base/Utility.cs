using Contentum.eQuery;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Xml;

[assembly: CLSCompliant(true)]
namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// Functions Class </summary>  
    [Serializable]
    public static class Utility
    {
        const string EmptyGuidString = "00000000-0000-0000-0000-000000000000";
        const string nullString = "<null>";
        const string ErrorCode00001 = "ErrorCode:00001;";
        const string ErrorCode00002 = "ErrorCode:00002;";
        const string ErrorCode00003 = "ErrorCode:00003;";


        public static String GetStringFromSqlXml(SqlXml Value)
        {
            if (Value.IsNull)
                return String.Empty;
            return Value.Value;
        }

        public static SqlXml SetSqlXmlFromString(string NewValue, SqlXml OldValue)
        {
            if (NewValue.Trim() == nullString)
                return SqlXml.Null;

            if (NewValue.Length == 0 && OldValue.IsNull)
                return OldValue;

            System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
            xml.LoadXml(NewValue);

            MemoryStream memStream = null;
            try
            {
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //byte[] bytes = new byte[NewValue.Length];
                byte[] bytes = new byte[encoding.GetByteCount(NewValue)];

                encoding.GetBytes(NewValue, 0, NewValue.Length, bytes, 0);
                memStream = new MemoryStream(bytes);
                SqlXml _ret = new SqlXml(memStream);
                return _ret;
            }
            catch
            {
                return SqlXml.Null;
            }
        }

        public static SqlChars SetSqlCharsFromString(string NewValue, SqlChars OldValue)
        {
            if (NewValue.Trim() == nullString)
                return SqlChars.Null;

            if (NewValue.Length == 0 && OldValue.IsNull)
                return OldValue;
            SqlChars _ret = new SqlChars(NewValue);
            return _ret;
        }

        public static String GetStringFromSqlChars(SqlChars Value)
        {
            if (Value.IsNull)
                return String.Empty;
            return Value.Value.GetValue(0).ToString();
        }

        public static SqlChars SetSqlCharsFromBool(bool NewValue, SqlChars OldValue)
        {
            SqlChars _ret = new SqlChars(NewValue ? "1" : "0");
            return _ret;
        }

        public static bool GetBoolFromSqlChars(SqlChars Value)
        {
            return GetBoolFromSqlChars(Value, false);
        }

        public static bool GetBoolFromSqlChars(SqlChars Value, bool DefaultValue)
        {
            if (Value.IsNull)
                return DefaultValue;
            return new String(Value.Value) == "1" ? true : (new String(Value.Value) == "0" ? false : DefaultValue);
        }

        public static String GetStringFromSqlGuid(SqlGuid Value)
        {
            if (Value.IsNull)
                return String.Empty;
            return Value.Value.ToString();
        }

        public static SqlGuid SetSqlGuidFromString(String NewValue, SqlGuid OldValue)
        {
            if (!string.IsNullOrEmpty(NewValue))
                NewValue = NewValue.Trim(new char[] { '\'' });
            if (String.IsNullOrEmpty(NewValue) || NewValue.Trim() == nullString || NewValue.Trim() == "")
                return SqlGuid.Null;

            if (OldValue.IsNull && NewValue.Length == 0)
                return OldValue;

            try
            {
                return SqlGuid.Parse(NewValue);
            }
            catch (Exception e)
            {
                throw new Exception(ErrorCode00002 + e.Message, e.InnerException);
            }
        }

        public static SqlString SetSqlStringFromString(string NewValue, SqlString OldValue)
        {
            if (NewValue == null || NewValue.Trim() == nullString)
                return SqlString.Null;

            if (NewValue.Length == 0 && OldValue.IsNull)
                return OldValue;
            return NewValue;
        }

        public static String GetStringFromSqlString(SqlString Value)
        {
            if (Value.IsNull)
                return String.Empty;
            return Value.Value;
        }

        public static String GetStringFromSqlInt32(SqlInt32 Value)
        {
            return GetStringFromSqlInt32(Value, "0");
        }

        public static String GetStringFromSqlBoolean(SqlBoolean Value)
        {
            return Value.ToString();
        }
        public static String GetStringFromSqlBoolean(SqlInt32 Value)
        {
            return Value.ToString();
        }

        public static String GetStringFromSqlInt32(SqlInt32 Value, string DefaultValue)
        {
            if (Value.IsNull)
                return DefaultValue;
            return Value.ToString();
        }

        public static SqlInt32 SetSqlInt32FromString(String NewValue, SqlInt32 OldValue)
        {
            if (NewValue.Trim() == nullString || NewValue.Length == 0)
                return SqlInt32.Null;

            if (NewValue.Trim() == "" && OldValue.IsNull)
                return SqlInt32.Null;

            if (NewValue.Trim() == "0")
                return 0;
            try
            {
                return SqlInt32.Parse(NewValue);
            }
            catch (Exception e)
            {
                throw new Exception(ErrorCode00001 + e.Message, e.InnerException);
            }
        }
        public static SqlInt32 SetSqlInt32FromBooleanString(String NewValue, SqlInt32 OldValue)
        {
            if (NewValue.Trim() == nullString || NewValue.Length == 0)
                return SqlInt32.Null;

            if (NewValue.Trim() == "" && OldValue.IsNull)
                return SqlInt32.Null;

            if (NewValue.Trim().ToUpper() == bool.FalseString.ToUpper())
                return 0;
            else if (NewValue.Trim().ToUpper() == bool.TrueString.ToUpper())
                return 1;
            else
                return 0;
        }

        public static String GetStringFromSqlDouble(SqlDouble Value)
        {
            if (Value.IsNull)
                return String.Empty;
            // A "." �s "," tizedes elv�laszt�k miatt hib�t okozhat az ide-oda alak�t�s
            // return Value.ToString()

            try
            {

                Double dValue = (double)Value;
                return dValue.ToString();
            }
            catch (Exception e)
            {
                throw new Exception(ErrorCode00001 + e.Message, e.InnerException);
            }
        }

        public static SqlDouble SetSqlDoubleFromString(String NewValue, SqlDouble OldValue)
        {
            if (NewValue.Trim() == nullString || NewValue.Length == 0)
                return SqlDouble.Null;

            if (NewValue.Trim() == "" && OldValue.IsNull)
                return SqlDouble.Null;

            try
            {
                // A "."-ot nem �rtelmezi tizedesk�nt
                //return SqlDouble.Parse(NewValue);

                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                nfi.NumberDecimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                System.Globalization.NumberStyles ns = System.Globalization.NumberStyles.Float;

                Double dNewValue = Double.Parse(NewValue, ns, nfi);

                SqlDouble sqldNewValue = new SqlDouble(dNewValue);

                return sqldNewValue;
            }
            catch (Exception e)
            {
                throw new Exception(ErrorCode00001 + e.Message, e.InnerException);
            }
        }

        public static String GetStringFromSqlDateTime(SqlDateTime Value)
        {
            if (Value.IsNull)
                return String.Empty;
            return Value.ToString();
        }

        public static SqlDateTime SetSqlDateTimeFromString(String NewValue, SqlDateTime OldValue)
        {
            if (NewValue == null || NewValue.Trim() == nullString|| NewValue == string.Empty)
                return SqlDateTime.Null;

            if (NewValue.Length == 0 && OldValue.IsNull)
                return SqlDateTime.Null;

            try
            {
                return SqlDateTime.Parse(NewValue);
            }
            catch (Exception e)
            {
                throw new Exception(ErrorCode00003 + e.Message, e.InnerException);
            }
        }

        // SqlBinary
        public static string GetStringFromSqlBinary(SqlBinary value_sqlBinary)
        {
            if (value_sqlBinary.IsNull)
            {
                return string.Empty;
            }

            return System.Convert.ToBase64String(value_sqlBinary.Value);
        }

        public static SqlBinary SetSqlBinaryFromString(string newValue, SqlBinary oldValue)
        {
            if (newValue.Trim() == nullString)
            {
                return SqlBinary.Null;
            }

            if (newValue.Length == 0 && oldValue.IsNull)
            {
                return SqlBinary.Null;
            }

            return new SqlBinary(System.Convert.FromBase64String(newValue));
        }

        #region refactored from Utility.cs files in App_Code folders:

        public const string customCollation = "collate Hungarian_Technical_CI_AS";

        public class Parameter
        {
            private string _Name = "";

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            private string _Type = "";

            public string Type
            {
                get { return _Type; }
                set { _Type = value; }
            }

            private int _Length = 0;

            public int Length
            {
                get { return _Length; }
                set { _Length = value; }
            }
        }

        public static String GetConnectionString(HttpApplicationState App)
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];

            return conn.ConnectionString;
        }

        public static SqlConnection GetSqlConnection(String ConnectionString)
        {
            try
            {
                SqlConnection sqlconn = new SqlConnection(ConnectionString);
                return sqlconn;
            }
            catch (SqlException e)
            {
                throw new Exception(ErrorCode00002 + e.Message, e.InnerException);
            }
        }

        public static Dictionary<String, Parameter> GetStoredProcedureParameter(SqlConnection Connection, String StoredProcedureName)
        {
            // CSAK IDEIGLENES:
            SqlConnection newConnection = GetSqlConnection(Connection.ConnectionString);
            newConnection.Open();

            Dictionary<String, Parameter> Parameters = new Dictionary<string, Parameter>();

            /* Tarolteljaras hivasa ami visszaadja egy tarolteljaras parametereit */
            SqlCommand SqlCommParam = new SqlCommand("[sp_GetProcedureDetails]");
            SqlCommParam.Parameters.Add(new SqlParameter("@ProcedureName", SqlDbType.NVarChar));
            SqlCommParam.Parameters["@ProcedureName"].Size = 50;
            SqlCommParam.Parameters["@ProcedureName"].Value = StoredProcedureName;
            SqlCommParam.CommandType = CommandType.StoredProcedure;
            //SqlCommParam.Connection = Connection;
            SqlCommParam.Connection = newConnection;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = SqlCommParam;

            try
            {
                da.Fill(ds);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Parameter _Par = new Parameter();
                    _Par.Name = ds.Tables[0].Rows[i]["name"].ToString();
                    _Par.Type = ds.Tables[0].Rows[i]["ctype"].ToString();
                    _Par.Length = (int)ds.Tables[0].Rows[i]["length"];
                    Parameters.Add(ds.Tables[0].Rows[i]["name"].ToString().Substring(1), _Par);
                }
            }
            catch (Exception e)
            {
                newConnection.Close();
                throw e;
            }
            finally
            {
                newConnection.Close();

                ds.Dispose();
                da.Dispose();
            }

            return Parameters;
        }
        public static void AddDefaultParameterToGetStoredProcedure(SqlCommand SqlComm, ExecParam ExecParam)
        {
            SqlComm.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;
            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;
        }

        public static void AddDefaultParameterToInsertStoredProcedure(SqlCommand SqlComm)
        {
            SqlComm.Parameters.Add(new SqlParameter("@ResultUid", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;
        }

        public static void AddDefaultParameterToUpdateStoredProcedure(SqlCommand SqlComm, ExecParam ExecParam, DateTime ExecutionTime)
        {
            AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);

            if (SqlComm.Parameters.Contains("@id"))
            {
                SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;
            }
            else
            {
                SqlComm.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;
            }
        }

        public static void AddDefaultParameterToDeleteStoredProcedure(SqlCommand SqlComm, ExecParam ExecParam, DateTime ExecutionTime)
        {
            SqlComm.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;

            AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);
        }

        public static void AddDefaultParameterToInvalidateStoredProcedure(SqlCommand SqlComm, ExecParam ExecParam, DateTime ExecutionTime)
        {
            SqlComm.CommandType = CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;

            AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);
        }

        public static void AddDefaultParameterToRevalidateStoredProcedure(SqlCommand SqlComm, ExecParam ExecParam)
        {
            SqlComm.Parameters.Add(new SqlParameter("@id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@id"].Value = ExecParam.Typed.Record_Id;
            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;
        }

        public static void AddDefaultParameterToGetAllStoredProcedure(SqlCommand SqlComm, Query _Query, ExecParam ExecParam, String OrderBy, Int32 TopRow)
        {
            SqlComm.Parameters.Add(new SqlParameter("@Where", SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Size = -1; //BUG_12573 - TODO: use -1 for nvarchar(max)
            SqlComm.Parameters["@Where"].Value = _Query.Where;

            if (OrderBy.Trim() != "")
            {
                SqlComm.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 200;
                SqlComm.Parameters["@OrderBy"].Value = (OrderBy == nullString) ? "" : " order by " + OrderBy;
            }

            SqlComm.Parameters.Add(new SqlParameter("@TopRow", SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = TopRow.ToString();

            SqlComm.CommandType = CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            // Ha van be�ll�tva lapoz�s, be�ll�tjuk a t�rolt elj�r�s param�tereit:
            if (ExecParam.Paging.PageNumber >= 0 && ExecParam.Paging.PageSize > 0)
            {
                SqlComm.Parameters.Add(new SqlParameter("@pageNumber", SqlDbType.Int));
                SqlComm.Parameters["@pageNumber"].Value = ExecParam.Paging.PageNumber;

                SqlComm.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.Int));
                SqlComm.Parameters["@pageSize"].Value = ExecParam.Paging.PageSize;

                if (!String.IsNullOrEmpty(ExecParam.Paging.SelectedRowId))
                {
                    SqlComm.Parameters.Add(new SqlParameter("@SelectedRowId", SqlDbType.NVarChar));
                    SqlComm.Parameters["@SelectedRowId"].Value = ExecParam.Paging.SelectedRowId;
                }
            }
        }

        public static void AddExecuteDefaultParameter(SqlCommand SqlComm, ExecParam ExecParam, DateTime ExecutionTime)
        {
            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutionTime", SqlDbType.DateTime));
            SqlComm.Parameters["@ExecutionTime"].Value = ExecutionTime;
        }

        public static void LoadBusinessDocumentFromDataAdapter(object BusinessDocument, SqlCommand sqlcommand)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlcommand;
            da.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (PropertyInfo P in Properties)
                    {
                        //dt.Rows[0][GetOrdinal(P.Name)];
                        if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                        {
                            P.SetValue(BusinessDocument, ds.Tables[0].Rows[0][P.Name].ToString(), null);
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        // csak ha l�tezik ilyen oszlop az adatb�zisban
                        if (ds.Tables[0].Columns.Contains(P.Name))
                        {
                            if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, ds.Tables[0].Rows[0][P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                da.Dispose();
            }
        }

        public static bool HasData(Result result)
        {
            if (result == null) { return false; }
            if (result.Ds == null) { return false; }
            if (result.Ds.Tables == null) { return false; }
            if (result.Ds.Tables.Count == 0) { return false; }
            if (result.Ds.Tables[0].Rows.Count == 0) { return false; }

            return true;
        }

        public static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                            }
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void LoadUpdateBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
                    object UpdatedObject = UpdatedField.GetValue(BusinessDocument);
                    foreach (PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                                PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                                if (UpdatedProperty != null)
                                {
                                    UpdatedProperty.SetValue(UpdatedObject, true, null);
                                }
                            }
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool LoadBusinessDocumentToParameter(object BusinessDocument, SqlCommand SqlComm, Dictionary<String, Utility.Parameter> Parameters)
        {
            if (BusinessDocument != null && SqlComm != null)
            {
                SqlComm.CommandType = CommandType.StoredProcedure;
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    XmlElement rootNode = xmlDocument.CreateElement("root");
                    xmlDocument.AppendChild(rootNode);


                    FieldInfo TypedField = BusinessDocument.GetType().GetField("Typed");
                    object TypedObject = TypedField.GetValue(BusinessDocument);
                    PropertyInfo[] TypedProperties = TypedObject.GetType().GetProperties();


                    FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
                    object UpdatedObject = UpdatedField.GetValue(BusinessDocument);

                    foreach (PropertyInfo P in TypedProperties)
                    {
                        Parameter _Par = new Parameter();
                        Parameters.TryGetValue(P.Name, out _Par);

                        if (_Par != null)
                        {
                            PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                            bool CanUpdate = (bool)UpdatedProperty.GetValue(UpdatedObject, null);

                            if (CanUpdate)
                            {
                                XmlElement childNode = xmlDocument.CreateElement(P.Name);
                                rootNode.AppendChild(childNode);

                                SqlComm.Parameters.Add(new SqlParameter(_Par.Name, GetSqlType(_Par.Type)));
                                SqlComm.Parameters[_Par.Name].Size = _Par.Length;
                                SqlComm.Parameters[_Par.Name].Value = P.GetValue(TypedObject, null);
                            }
                        }
                    }

                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);
                    FieldInfo BaseTypedField = BaseObject.GetType().GetField("Typed");
                    object BaseTypedObject = BaseTypedField.GetValue(BaseObject);
                    PropertyInfo[] BaseTypedProperties = BaseTypedObject.GetType().GetProperties();


                    FieldInfo BaseUpdatedField = BaseObject.GetType().GetField("Updated");
                    object BaseUpdatedObject = BaseUpdatedField.GetValue(BaseObject);

                    foreach (PropertyInfo P in BaseTypedProperties)
                    {
                        Parameter _Par = new Parameter();
                        Parameters.TryGetValue(P.Name, out _Par);

                        if (_Par != null && !rootNode.InnerXml.Contains("<" + P.Name + " />"))
                        {
                            PropertyInfo BaseUpdatedProperty = BaseUpdatedObject.GetType().GetProperty(P.Name);
                            bool CanUpdate = (bool)BaseUpdatedProperty.GetValue(BaseUpdatedObject, null);

                            if (CanUpdate)
                            {
                                XmlElement childNode = xmlDocument.CreateElement(P.Name);
                                rootNode.AppendChild(childNode);

                                SqlComm.Parameters.Add(new SqlParameter(_Par.Name, GetSqlType(_Par.Type)));
                                SqlComm.Parameters[_Par.Name].Size = _Par.Length;
                                SqlComm.Parameters[_Par.Name].Value = P.GetValue(BaseTypedObject, null);
                            }
                        }
                    }

                    SqlComm.Parameters.Add(new SqlParameter("@UpdatedColumns", SqlDbType.Xml));
                    SqlComm.Parameters["@UpdatedColumns"].Value = xmlDocument.InnerXml.ToString();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return true;
        }

        public static SqlDbType GetSqlType(String type)
        {
            SqlDbType _ret = new SqlDbType();
            _ret = SqlDbType.NVarChar; // default ertek!
            switch (type)
            {
                case "uniqueidentifier":
                    _ret = SqlDbType.UniqueIdentifier;
                    break;
                case "nvarchar":
                    _ret = SqlDbType.NVarChar;
                    break;
                case "int":
                    _ret = SqlDbType.Int;
                    break;
                case "float":
                    _ret = SqlDbType.Float;
                    break;
                case "char":
                    _ret = SqlDbType.Char;
                    break;
                case "datetime":
                    _ret = SqlDbType.DateTime;
                    break;
                case "smalldatetime":
                    _ret = SqlDbType.SmallDateTime;
                    break;
                case "image":
                    _ret = SqlDbType.Image;
                    break;
                case "varbinary":
                    _ret = SqlDbType.VarBinary;
                    break;
            }
            return _ret;
        }
        
        internal class CustomComparer : IComparer<string>
        {
            #region IComparer<string> Members
            public int Compare(string x, string y)
            {
                return String.Compare(x, y, true, new System.Globalization.CultureInfo(0x0001040e, false));
            }

            #endregion
        }

        public static string[] SearchInStringArrayByPrefix(string[] StringArray, string prefixText, int count, bool useCustomComparer)
        {
            if (StringArray != null)
            {
                int index;
                if (!useCustomComparer)
                {
                    index = Array.BinarySearch(StringArray, prefixText,
                      new System.Collections.CaseInsensitiveComparer());
                }
                else
                {
                    index = Array.BinarySearch<string>(StringArray, prefixText, new CustomComparer());
                }
                if (index < 0)
                {
                    index = ~index;
                }

                int matchingCount;
                for (matchingCount = 0;
                     matchingCount < count && index + matchingCount <
                     StringArray.Length;
                     matchingCount++)
                {
                    if (!StringArray[index +
                      matchingCount].StartsWith(prefixText,
                      StringComparison.CurrentCultureIgnoreCase))
                    {
                        break;
                    }
                }

                String[] returnValue = new string[matchingCount];
                if (matchingCount > 0)
                {
                    Array.Copy(StringArray, index, returnValue, 0,
                      matchingCount);
                }
                return returnValue;
            }
            else
            {
                return null;
            }
        }

        public static Pair GetAutoCompleteItem(string item)
        {
            return new JavaScriptSerializer().Deserialize<Pair>(item);
        }

        public static string[] SearchInStringPairArrayByPrefix(string[] StringPairArray, string prefixText, int count, bool useCustomComparer)
        {
            string[] StringArray = new string[StringPairArray.Length];
            for (int i = 0; i < StringPairArray.Length; i++)
            {
                StringArray[i] = (string)GetAutoCompleteItem(StringPairArray[i]).First;
            }

            if (StringArray != null)
            {
                int index;
                if (!useCustomComparer)
                {
                    index = Array.BinarySearch(StringArray, prefixText,
                      new System.Collections.CaseInsensitiveComparer());
                }
                else
                {
                    index = Array.BinarySearch<string>(StringArray, prefixText, new CustomComparer());
                }
                if (index < 0)
                {
                    index = ~index;
                }

                int matchingCount;
                for (matchingCount = 0;
                     matchingCount < count && index + matchingCount <
                     StringArray.Length;
                     matchingCount++)
                {
                    if (!StringArray[index +
                      matchingCount].StartsWith(prefixText,
                      StringComparison.CurrentCultureIgnoreCase))
                    {
                        break;
                    }
                }

                String[] returnValue = new string[matchingCount];
                if (matchingCount > 0)
                {
                    Array.Copy(StringPairArray, index, returnValue, 0,
                      matchingCount);
                }
                return returnValue;
            }
            else
            {
                return null;
            }
        }

        public static string[] SearchInStringArrayByPrefix(string[] StringArray, string prefixText, int count)
        {
            return SearchInStringArrayByPrefix(StringArray, prefixText, count, false);
        }
        
        public static string[] DataSourceColumnToStringArray(Result res, String ColumnName)
        {
            if (res.Ds != null)
            {
                string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
                for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
                {
                    StringArray.SetValue(res.Ds.Tables[0].Rows[i][ColumnName].ToString(), i);
                }
                return StringArray;
            }
            else
            {
                return null;
            }
        }

        public static string[] DataSourceColumnToStringArray(Result res, String[] ColumnsName)
        {
            if (res.Ds != null)
            {
                string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
                String temp = "";
                for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
                {
                    foreach (String ColumnName in ColumnsName)
                    {
                        temp += res.Ds.Tables[0].Rows[i][ColumnName].ToString() + " | ";
                        //                    StringArray.SetValue(res.Ds.Tables[0].Rows[i][ColumnName].ToString(), i);
                    }
                    StringArray.SetValue(temp, i);
                    temp = "";
                }
                return StringArray;
            }
            else
            {
                return null;
            }
        }
        
        public static string CreateAutoCompleteItem(string text, string value)
        {
            return new JavaScriptSerializer().Serialize(new Pair(text, value));
        }

        public static string[] DataSourceColumnToStringPairArray(Result res, String ColumnValue, String ColumnText)
        {
            if (res.Ds != null)
            {
                string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
                string value = "";
                string text = "";
                for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
                {
                    value = res.Ds.Tables[0].Rows[i][ColumnValue].ToString();
                    text = res.Ds.Tables[0].Rows[i][ColumnText].ToString();
                    string item = CreateAutoCompleteItem(text, value);
                    StringArray.SetValue(item, i);
                }
                return StringArray;
            }
            else
            {
                return null;
            }
        }

        public static System.Net.CredentialCache GetCacheSetting(string Url, string _BusinessServiceAuthentication, string _BusinessServiceUserDomain, string _BusinessServiceUserName, string _BusinessServicePassword)
        {
            System.Net.CredentialCache cache = new System.Net.CredentialCache();

            if (_BusinessServiceAuthentication == "Windows")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           System.Net.CredentialCache.DefaultNetworkCredentials);
                return cache;

            }
            if (_BusinessServiceAuthentication == "Basic")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Basic",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }
            if (_BusinessServiceAuthentication == "NTLM")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }

            return null;
        }

        public static string xmlPrettify(string input)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(input);
                return doc.OuterXml;
            }
            catch (Exception)
            {
                return input;
            }
        }

        public static string GetKRBoolean(string edokInput)
        {
            if ("1".Equals(edokInput))
                return "true";
            return "false";
        }

        public static string GetEDOKBoolean(bool krInput)
        {
            if (krInput)
                return "1";
            return "0";
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input

            var md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }

        public static string EncodeXMLString(string xmlString)
        {
            return System.Security.SecurityElement.Escape(xmlString);
        }

        #endregion
    }
}
