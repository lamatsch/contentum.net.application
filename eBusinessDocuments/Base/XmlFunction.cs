﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    internal static class XmlFunction
    {
        private static System.Text.Encoding XmlEncoding = System.Text.Encoding.UTF8;

        public static string ObjectToXml(Object objToXml,
                             bool includeNameSpace)
        {
            if (objToXml == null) return null;

            StreamWriter stWriter = null;
            System.Xml.Serialization.XmlSerializer xmlSerializer;
            string buffer;
            try
            {
                xmlSerializer =
                      new System.Xml.Serialization.XmlSerializer(objToXml.GetType());

                MemoryStream memStream = new MemoryStream();
                stWriter = new StreamWriter(memStream);
                if (!includeNameSpace)
                {

                    System.Xml.Serialization.XmlSerializerNamespaces xs =
                                         new System.Xml.Serialization.XmlSerializerNamespaces();

                    //To remove namespace and any other inline 
                    //information tag                      
                    xs.Add("", "");
                    xmlSerializer.Serialize(stWriter, objToXml, xs);
                }
                else
                {
                    xmlSerializer.Serialize(stWriter, objToXml);
                }
                buffer = XmlEncoding.GetString(memStream.GetBuffer()).TrimEnd('\0');
                buffer = buffer.Substring(40);

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (stWriter != null) stWriter.Close();
            }
            return buffer;
        }

        public static object XmlToObject(string xml, Type type)
        {
            if (xml == null || type == null) return null;
            System.Xml.Serialization.XmlSerializer xmlSerializer;
            MemoryStream memStream = null;
            try
            {
                xmlSerializer = new System.Xml.Serialization.XmlSerializer(type);
                //byte[] bytes = new byte[xml.Length];
                byte[] bytes = new byte[XmlEncoding.GetByteCount(xml)];

                XmlEncoding.GetBytes(xml, 0, xml.Length, bytes, 0);
                memStream = new MemoryStream(bytes);
                object objectFromXml = xmlSerializer.Deserialize(memStream);
                return objectFromXml;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (memStream != null) memStream.Close();
            }
        }
    }
}
