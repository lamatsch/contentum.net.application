using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class InputDTO
    {
        private string _alairtFileNev = null;

        public string alairtFileNev
        {
            get { return _alairtFileNev; }
            set { _alairtFileNev = value; }
        }
        private byte[] _alairtTartalom = null;

        public byte[] alairtTartalom
        {
            get { return _alairtTartalom; }
            set { _alairtTartalom = value; }
        }
        private DateTime _egovElsoatvetIdeje;

        public DateTime egovElsoatvetIdeje
        {
            get { return _egovElsoatvetIdeje; }
            set { _egovElsoatvetIdeje = value; }
        }
        private string _egovErkeztetoSzam = null;

        public string egovErkeztetoSzam
        {
            get { return _egovErkeztetoSzam; }
            set { _egovErkeztetoSzam = value; }
        }
        private string _egovTargy = null;

        public string egovTargy
        {
            get { return _egovTargy; }
            set { _egovTargy = value; }
        }
        private string _fileNev = null;

        public string fileNev
        {
            get { return _fileNev; }
            set { _fileNev = value; }
        }
        private string _kuldemenyErkeztetoKonyv = null;

        public string kuldemenyErkeztetoKonyv
        {
            get { return _kuldemenyErkeztetoKonyv; }
            set { _kuldemenyErkeztetoKonyv = value; }
        }
        private string _prtCime = null;

        public string prtCime
        {
            get { return _prtCime; }
            set { _prtCime = value; }
        }
        private string _prtId = null;

        public string prtId
        {
            get { return _prtId; }
            set { _prtId = value; }
        }
        private string _prtMegnevezes = null;

        public string prtMegnevezes
        {
            get { return _prtMegnevezes; }
            set { _prtMegnevezes = value; }
        }
        private byte[] _tartalom = null;

        public byte[] tartalom
        {
            get { return _tartalom; }
            set { _tartalom = value; }
        }
    }

    [Serializable()]
    public class KuldemenyDTO
    {
        private string _bekuldo = null;

        public string bekuldo
        {
            get { return _bekuldo; }
            set { _bekuldo = value; }
        }
        private string _bekuldoCime = null;

        public string bekuldoCime
        {
            get { return _bekuldoCime; }
            set { _bekuldoCime = value; }
        }
        private int _bekuldoCimeId;

        public int bekuldoCimeId
        {
            get { return _bekuldoCimeId; }
            set { _bekuldoCimeId = value; }
        }
        private int _bekuldoId;

        public int bekuldoId
        {
            get { return _bekuldoId; }
            set { _bekuldoId = value; }
        }
        private int _cimzettId;

        public int cimzettId
        {
            get { return _cimzettId; }
            set { _cimzettId = value; }
        }
        private DateTime _elsoAtvetIdeje;

        public DateTime elsoAtvetIdeje
        {
            get { return _elsoAtvetIdeje; }
            set { _elsoAtvetIdeje = value; }
        }
        private string _eredetinyilvszam = null;

        public string eredetinyilvszam
        {
            get { return _eredetinyilvszam; }
            set { _eredetinyilvszam = value; }
        }
        private string _erkeztetoSzam = null;

        public string erkeztetoSzam
        {
            get { return _erkeztetoSzam; }
            set { _erkeztetoSzam = value; }
        }
        private int _felelosId;

        public int felelosId
        {
            get { return _felelosId; }
            set { _felelosId = value; }
        }
        private string _futarjegyzek1 = null;

        public string futarjegyzek1
        {
            get { return _futarjegyzek1; }
            set { _futarjegyzek1 = value; }
        }
        private string _futarjegyzek2 = null;

        public string futarjegyzek2
        {
            get { return _futarjegyzek2; }
            set { _futarjegyzek2 = value; }
        }
        private string _futarjegyzek3 = null;

        public string futarjegyzek3
        {
            get { return _futarjegyzek3; }
            set { _futarjegyzek3 = value; }
        }
        private string _futarjegyzek4 = null;

        public string futarjegyzek4
        {
            get { return _futarjegyzek4; }
            set { _futarjegyzek4 = value; }
        }
        private string _guid = null;

        public string guid
        {
            get { return _guid; }
            set { _guid = value; }
        }
        private int _hatokor;

        public int hatokor
        {
            get { return _hatokor; }
            set { _hatokor = value; }
        }
        private string _hivatkozasiSzam = null;

        public string hivatkozasiSzam
        {
            get { return _hivatkozasiSzam; }
            set { _hivatkozasiSzam = value; }
        }
        private int _iktatnikell;

        public int iktatnikell
        {
            get { return _iktatnikell; }
            set { _iktatnikell = value; }
        }
        private int _kiadmanyozoId;

        public int kiadmanyozoId
        {
            get { return _kiadmanyozoId; }
            set { _kiadmanyozoId = value; }
        }
        private string _kiadmanyozva = null;

        public string kiadmanyozva
        {
            get { return _kiadmanyozva; }
            set { _kiadmanyozva = value; }
        }
        private string _kuldemenyErkeztetoKonyv = null;

        public string kuldemenyErkeztetoKonyv
        {
            get { return _kuldemenyErkeztetoKonyv; }
            set { _kuldemenyErkeztetoKonyv = value; }
        }
        private int _kuldesModja;

        public int kuldesModja
        {
            get { return _kuldesModja; }
            set { _kuldesModja = value; }
        }
        private int _kulid;

        public int kulid
        {
            get { return _kulid; }
            set { _kulid = value; }
        }
        private string _megelozoiktatoszam = null;

        public string megelozoiktatoszam
        {
            get { return _megelozoiktatoszam; }
            set { _megelozoiktatoszam = value; }
        }
        private bool _minositett;

        public bool minositett
        {
            get { return _minositett; }
            set { _minositett = value; }
        }
        private int _minositettjel;

        public int minositettjel
        {
            get { return _minositettjel; }
            set { _minositettjel = value; }
        }
        private DateTime _minositettkezdete;

        public DateTime minositettkezdete
        {
            get { return _minositettkezdete; }
            set { _minositettkezdete = value; }
        }
        private DateTime _minositettvege;

        public DateTime minositettvege
        {
            get { return _minositettvege; }
            set { _minositettvege = value; }
        }
        private bool _nato;

        public bool nato
        {
            get { return _nato; }
            set { _nato = value; }
        }
        private string _peldanydarab1 = null;

        public string peldanydarab1
        {
            get { return _peldanydarab1; }
            set { _peldanydarab1 = value; }
        }
        private string _peldanydarab2 = null;

        public string peldanydarab2
        {
            get { return _peldanydarab2; }
            set { _peldanydarab2 = value; }
        }
        private string _ragszam = null;

        public string ragszam
        {
            get { return _ragszam; }
            set { _ragszam = value; }
        }
        private string _referenciaszam = null;

        public string referenciaszam
        {
            get { return _referenciaszam; }
            set { _referenciaszam = value; }
        }
        private DateTime _silence;

        public DateTime silence
        {
            get { return _silence; }
            set { _silence = value; }
        }
        private int _surgosseg;

        public int surgosseg
        {
            get { return _surgosseg; }
            set { _surgosseg = value; }
        }
        private string _targy = null;

        public string targy
        {
            get { return _targy; }
            set { _targy = value; }
        }
        private string _tovabbitoSzervezet = null;

        public string tovabbitoSzervezet
        {
            get { return _tovabbitoSzervezet; }
            set { _tovabbitoSzervezet = value; }
        }
        private int _ugyintezesAlapja;

        public int ugyintezesAlapja
        {
            get { return _ugyintezesAlapja; }
            set { _ugyintezesAlapja = value; }
        }
        private string _vanCsatolmany = null;

        public string vanCsatolmany
        {
            get { return _vanCsatolmany; }
            set { _vanCsatolmany = value; }
        }
        private bool _voltelozmeny;

        public bool voltelozmeny
        {
            get { return _voltelozmeny; }
            set { _voltelozmeny = value; }
        }
    }

    [Serializable()]
    public class IktatoszamokDTO
    {
        private string _hivatkozasiSzam = null;

        public string hivatkozasiSzam
        {
            get { return _hivatkozasiSzam; }
            set { _hivatkozasiSzam = value; }
        }
        private string _iktatoszam = null;

        public string iktatoszam
        {
            get { return _iktatoszam; }
            set { _iktatoszam = value; }
        }
    }

    [Serializable()]
    public class KiadmanyokDTO
    {
        private string _erkeztetoSzam = null;
        private byte[] _alairtAllomany = null;
        private byte[] _alaNemIrtAllomany = null;

        public string ErkeztetoSzam
        {
            get { return _erkeztetoSzam; }
            set { _erkeztetoSzam = value; }
        }

        public byte[] AlairtAllomany
        {
            get { return _alairtAllomany; }
            set { _alairtAllomany = value; }
        }

        public byte[] AlaNemIrtAllomany
        {
            get { return _alaNemIrtAllomany; }
            set { _alaNemIrtAllomany = value; }
        }
    }
}
