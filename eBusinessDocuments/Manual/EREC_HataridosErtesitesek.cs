﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class EREC_HataridosErtesitesek: EREC_HataridosFeladatok
    {
        //private string _Obj_type;

        //public string Obj_type
        //{
        //    get { return _Obj_type; }
        //    set { _Obj_type = value; }
        //}

        private string _ObjektumSzukites;

        public string ObjektumSzukites
        {
            get { return _ObjektumSzukites; }
            set { _ObjektumSzukites = value; }
        }

        private string _Funkcio_Futtatando;

        public string Funkcio_Futtatando
        {
            get { return _Funkcio_Futtatando; }
            set { _Funkcio_Futtatando = value; }
        }

        private string _Targy;

        public string Targy
        {
            get { return _Targy; }
            set { _Targy = value; }
        }

        private string _UgyintezoId;

        public string UgyintezoId
        {
            get { return _UgyintezoId; }
            set { _UgyintezoId = value; }
        }

        private string _UgyintezoNev;

        public string UgyintezoNev
        {
            get { return _UgyintezoNev; }
            set { _UgyintezoNev = value; }
        }


    }
}
