﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public partial class EREC_eBeadvanyok
    {
        public void Set_PR_Parameterek(PR_Parameterek value)
        {
            if (value != null)
            {
                this.PR_Parameterek = XmlFunction.ObjectToXml(value, false);
            }
            else
            {
                this.PR_Parameterek = String.Empty;
            }
        }

        public PR_Parameterek Get_PR_Parameterek()
        {
            if (String.IsNullOrEmpty(this.PR_Parameterek))
            {
                return new PR_Parameterek();
            }
            else
            {
                return XmlFunction.XmlToObject(this.PR_Parameterek, typeof(PR_Parameterek)) as PR_Parameterek;
            }
        }
    }
}
