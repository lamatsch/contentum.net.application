using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public class KRT_PartnerSzemelyek:KRT_Partnerek
    {
        [NonSerialized]
        public KRT_Szemelyek Szemelyek = new KRT_Szemelyek();

        private bool _updatePartnerNevBySzemelyAdatok = true;
        /// <summary>
        /// A KRT_Partnerek.Nev mez� �rt�ke Insert/Update sor�n fel�l�r�djon-e a KRT_Szemelyek-ben megadott adatokkal
        /// (A csal�di n�v, keresztn�v, ... �sszef�z�s�vel)
        /// </summary>
        public bool UpdatePartnerNevBySzemelyAdatok
        {
            get { return _updatePartnerNevBySzemelyAdatok; }
            set { this._updatePartnerNevBySzemelyAdatok = value; }
        }

        public KRT_PartnerSzemelyek()
            :base()
        {
        }

        public KRT_PartnerSzemelyek(KRT_Partnerek krt_partnerek, KRT_Szemelyek krt_szemelyek)
            :base()
        {
            base.Base = krt_partnerek.Base;
            base.Typed = krt_partnerek.Typed;
            base.Updated = krt_partnerek.Updated;
            this.Szemelyek = krt_szemelyek;
            this.Szemelyek.Partner_Id = krt_partnerek.Id;
            this.Szemelyek.ErvKezd = krt_partnerek.ErvKezd;
            this.Szemelyek.ErvVege = krt_partnerek.ErvVege;
            this.Szemelyek.Base = krt_partnerek.Base;
        }
    }
}
