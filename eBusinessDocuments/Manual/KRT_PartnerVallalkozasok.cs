using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public class KRT_PartnerVallalkozasok:KRT_Partnerek
    {
        [NonSerialized]
        public KRT_Vallalkozasok Vallalkozasok = new KRT_Vallalkozasok();

        public KRT_PartnerVallalkozasok()
            :base()
        {
        }

        public KRT_PartnerVallalkozasok(KRT_Partnerek krt_partnerek, KRT_Vallalkozasok krt_vallalkozasok)
            :base()
        {
            base.Base = krt_partnerek.Base;
            base.Typed = krt_partnerek.Typed;
            base.Updated = krt_partnerek.Updated;
            this.Vallalkozasok = krt_vallalkozasok;
            this.Vallalkozasok.Partner_Id = krt_partnerek.Id;
            this.Vallalkozasok.ErvKezd = krt_partnerek.ErvKezd;
            this.Vallalkozasok.ErvVege = krt_partnerek.ErvVege;
            this.Vallalkozasok.Base = krt_partnerek.Base;
        }
    }
}
