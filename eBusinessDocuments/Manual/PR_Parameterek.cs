﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public class PR_Parameterek
    {
        private string _InputFormat;

        public string InputFormat
        {
            get { return _InputFormat; }
            set { _InputFormat = value; }
        }

        private string _OutputFormat;

        public string OutputFormat
        {
            get { return _OutputFormat; }
            set { _OutputFormat = value; }
        }

        private string _OutputFormatDefinition;

        public string OutputFormatDefinition
        {
            get { return _OutputFormatDefinition; }
            set { _OutputFormatDefinition = value; }
        }
    }
}
