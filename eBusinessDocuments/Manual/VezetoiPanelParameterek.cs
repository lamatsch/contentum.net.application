﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    public class VezetoiPanelParameterek
    {
        public class TypedVezetoiPanelParameterek
        {
            private SqlDateTime _KezdDat = SqlDateTime.Null;

            public SqlDateTime KezdDat
            {
                get { return _KezdDat; }
                set { _KezdDat = value; }
            }


            private SqlDateTime _VegeDat = SqlDateTime.Null;


            public SqlDateTime VegeDat
            {
                get { return _VegeDat; }
                set { _VegeDat = value; }
            } 

            private SqlGuid _AlSzervezetId = SqlGuid.Null;

            public SqlGuid AlSzervezetId
            {
                get { return _AlSzervezetId; }
                set { _AlSzervezetId = value; }
            }  
        }

        [NonSerializedAttribute]
        public TypedVezetoiPanelParameterek Typed = new TypedVezetoiPanelParameterek();

        public String AlSzervezetId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AlSzervezetId); }
            set { Typed.AlSzervezetId = Utility.SetSqlGuidFromString(value, Typed.AlSzervezetId); }
        }

        public String KezdDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KezdDat); }
            set { Typed.KezdDat = Utility.SetSqlDateTimeFromString(value, Typed.KezdDat); }
        }

        public String VegeDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VegeDat); }
            set { Typed.VegeDat = Utility.SetSqlDateTimeFromString(value, Typed.VegeDat); }
        }
        // BUG_8847
        Boolean _isNMHH = false;
        public Boolean isNMHH
        {
            get { return _isNMHH; }
            set { _isNMHH = value;}
        }
    }
}
