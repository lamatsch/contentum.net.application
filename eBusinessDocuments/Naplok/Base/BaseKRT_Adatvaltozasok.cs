
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Adatvaltozasok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Adatvaltozasok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_AdatvaltozasokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_Tabla = true;
               public bool ObjTipus_Id_Tabla
               {
                   get { return _ObjTipus_Id_Tabla; }
                   set { _ObjTipus_Id_Tabla = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_Oszlop = true;
               public bool ObjTipus_Id_Oszlop
               {
                   get { return _ObjTipus_Id_Oszlop; }
                   set { _ObjTipus_Id_Oszlop = value; }
               }
                                                            
                                 
               private bool _RegiErtek = true;
               public bool RegiErtek
               {
                   get { return _RegiErtek; }
                   set { _RegiErtek = value; }
               }
                                                            
                                 
               private bool _UjErtek = true;
               public bool UjErtek
               {
                   get { return _UjErtek; }
                   set { _UjErtek = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                   Id = Value;               
                   Obj_Id = Value;               
                   ObjTipus_Id_Tabla = Value;               
                   ObjTipus_Id_Oszlop = Value;               
                   RegiErtek = Value;               
                   UjErtek = Value;               
                   ErvKezd = Value;               
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_AdatvaltozasokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_Tabla = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_Tabla Base property </summary>
            public SqlGuid ObjTipus_Id_Tabla
            {
                get { return _ObjTipus_Id_Tabla; }
                set { _ObjTipus_Id_Tabla = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_Oszlop = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_Oszlop Base property </summary>
            public SqlGuid ObjTipus_Id_Oszlop
            {
                get { return _ObjTipus_Id_Oszlop; }
                set { _ObjTipus_Id_Oszlop = value; }                                                        
            }        
                   
           
        private SqlString _RegiErtek = SqlString.Null;
           
        /// <summary>
        /// RegiErtek Base property </summary>
            public SqlString RegiErtek
            {
                get { return _RegiErtek; }
                set { _RegiErtek = value; }                                                        
            }        
                   
           
        private SqlString _UjErtek = SqlString.Null;
           
        /// <summary>
        /// UjErtek Base property </summary>
            public SqlString UjErtek
            {
                get { return _UjErtek; }
                set { _UjErtek = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}