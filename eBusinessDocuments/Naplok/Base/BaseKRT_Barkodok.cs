
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Barkodok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Barkodok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_BarkodokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_type = true;
               public bool Obj_type
               {
                   get { return _Obj_type; }
                   set { _Obj_type = value; }
               }
                                                            
                                 
               private bool _KodType = true;
               public bool KodType
               {
                   get { return _KodType; }
                   set { _KodType = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _Tranz_id = true;
               public bool Tranz_id
               {
                   get { return _Tranz_id; }
                   set { _Tranz_id = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Kod = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_type = Value;               
                    
                   KodType = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   Tranz_id = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_BarkodokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_type = SqlString.Null;
           
        /// <summary>
        /// Obj_type Base property </summary>
            public SqlString Obj_type
            {
                get { return _Obj_type; }
                set { _Obj_type = value; }                                                        
            }        
                   
           
        private SqlChars _KodType = SqlChars.Null;
           
        /// <summary>
        /// KodType Base property </summary>
            public SqlChars KodType
            {
                get { return _KodType; }
                set { _KodType = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlGuid _Tranz_id = SqlGuid.Null;
           
        /// <summary>
        /// Tranz_id Base property </summary>
            public SqlGuid Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }                                                        
            }        
                           }
    }    
}