
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Esemenyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Esemenyek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_EsemenyekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Azonositoja = true;
               public bool Azonositoja
               {
                   get { return _Azonositoja; }
                   set { _Azonositoja = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_User = true;
               public bool Felhasznalo_Id_User
               {
                   get { return _Felhasznalo_Id_User; }
                   set { _Felhasznalo_Id_User = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_FelelosUserSzerveze = true;
               public bool Csoport_Id_FelelosUserSzerveze
               {
                   get { return _Csoport_Id_FelelosUserSzerveze; }
                   set { _Csoport_Id_FelelosUserSzerveze = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_Login = true;
               public bool Felhasznalo_Id_Login
               {
                   get { return _Felhasznalo_Id_Login; }
                   set { _Felhasznalo_Id_Login = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Cel = true;
               public bool Csoport_Id_Cel
               {
                   get { return _Csoport_Id_Cel; }
                   set { _Csoport_Id_Cel = value; }
               }
                                                            
                                 
               private bool _Helyettesites_Id = true;
               public bool Helyettesites_Id
               {
                   get { return _Helyettesites_Id; }
                   set { _Helyettesites_Id = value; }
               }
                                                            
                                 
               private bool _Tranzakcio_Id = true;
               public bool Tranzakcio_Id
               {
                   get { return _Tranzakcio_Id; }
                   set { _Tranzakcio_Id = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id = true;
               public bool Funkcio_Id
               {
                   get { return _Funkcio_Id; }
                   set { _Funkcio_Id = value; }
               }
                                                            
                                 
               private bool _CsoportHierarchia = true;
               public bool CsoportHierarchia
               {
                   get { return _CsoportHierarchia; }
                   set { _CsoportHierarchia = value; }
               }
                                                            
                                 
               private bool _KeresesiFeltetel = true;
               public bool KeresesiFeltetel
               {
                   get { return _KeresesiFeltetel; }
                   set { _KeresesiFeltetel = value; }
               }
                                                            
                                 
               private bool _TalalatokSzama = true;
               public bool TalalatokSzama
               {
                   get { return _TalalatokSzama; }
                   set { _TalalatokSzama = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _Munkaallomas = true;
               public bool Munkaallomas
               {
                   get { return _Munkaallomas; }
                   set { _Munkaallomas = value; }
               }
                                                            
                                 
               private bool _MuveletKimenete = true;
               public bool MuveletKimenete
               {
                   get { return _MuveletKimenete; }
                   set { _MuveletKimenete = value; }
               }
                                                            
                                 
               private bool _CheckSum = true;
               public bool CheckSum
               {
                   get { return _CheckSum; }
                   set { _CheckSum = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Azonositoja = Value;               
                    
                   Felhasznalo_Id_User = Value;               
                    
                   Csoport_Id_FelelosUserSzerveze = Value;               
                    
                   Felhasznalo_Id_Login = Value;               
                    
                   Csoport_Id_Cel = Value;               
                    
                   Helyettesites_Id = Value;               
                    
                   Tranzakcio_Id = Value;               
                    
                   Funkcio_Id = Value;               
                    
                   CsoportHierarchia = Value;               
                    
                   KeresesiFeltetel = Value;               
                    
                   TalalatokSzama = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   Munkaallomas = Value;               
                    
                   MuveletKimenete = Value;               
                    
                   CheckSum = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_EsemenyekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Azonositoja = SqlString.Null;
           
        /// <summary>
        /// Azonositoja Base property </summary>
            public SqlString Azonositoja
            {
                get { return _Azonositoja; }
                set { _Azonositoja = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_User = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_User Base property </summary>
            public SqlGuid Felhasznalo_Id_User
            {
                get { return _Felhasznalo_Id_User; }
                set { _Felhasznalo_Id_User = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_FelelosUserSzerveze = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_FelelosUserSzerveze Base property </summary>
            public SqlGuid Csoport_Id_FelelosUserSzerveze
            {
                get { return _Csoport_Id_FelelosUserSzerveze; }
                set { _Csoport_Id_FelelosUserSzerveze = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_Login = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_Login Base property </summary>
            public SqlGuid Felhasznalo_Id_Login
            {
                get { return _Felhasznalo_Id_Login; }
                set { _Felhasznalo_Id_Login = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Cel = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Cel Base property </summary>
            public SqlGuid Csoport_Id_Cel
            {
                get { return _Csoport_Id_Cel; }
                set { _Csoport_Id_Cel = value; }                                                        
            }        
                   
           
        private SqlGuid _Helyettesites_Id = SqlGuid.Null;
           
        /// <summary>
        /// Helyettesites_Id Base property </summary>
            public SqlGuid Helyettesites_Id
            {
                get { return _Helyettesites_Id; }
                set { _Helyettesites_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Tranzakcio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Tranzakcio_Id Base property </summary>
            public SqlGuid Tranzakcio_Id
            {
                get { return _Tranzakcio_Id; }
                set { _Tranzakcio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id Base property </summary>
            public SqlGuid Funkcio_Id
            {
                get { return _Funkcio_Id; }
                set { _Funkcio_Id = value; }                                                        
            }        
                   
           
        private SqlString _CsoportHierarchia = SqlString.Null;
           
        /// <summary>
        /// CsoportHierarchia Base property </summary>
            public SqlString CsoportHierarchia
            {
                get { return _CsoportHierarchia; }
                set { _CsoportHierarchia = value; }                                                        
            }        
                   
           
        private SqlString _KeresesiFeltetel = SqlString.Null;
           
        /// <summary>
        /// KeresesiFeltetel Base property </summary>
            public SqlString KeresesiFeltetel
            {
                get { return _KeresesiFeltetel; }
                set { _KeresesiFeltetel = value; }                                                        
            }        
                   
           
        private SqlInt32 _TalalatokSzama = SqlInt32.Null;
           
        /// <summary>
        /// TalalatokSzama Base property </summary>
            public SqlInt32 TalalatokSzama
            {
                get { return _TalalatokSzama; }
                set { _TalalatokSzama = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlString _Munkaallomas = SqlString.Null;
           
        /// <summary>
        /// Munkaallomas Base property </summary>
            public SqlString Munkaallomas
            {
                get { return _Munkaallomas; }
                set { _Munkaallomas = value; }                                                        
            }        
                   
           
        private SqlString _MuveletKimenete = SqlString.Null;
           
        /// <summary>
        /// MuveletKimenete Base property </summary>
            public SqlString MuveletKimenete
            {
                get { return _MuveletKimenete; }
                set { _MuveletKimenete = value; }                                                        
            }        
                   
           
        private SqlString _CheckSum = SqlString.Null;
           
        /// <summary>
        /// CheckSum Base property </summary>
            public SqlString CheckSum
            {
                get { return _CheckSum; }
                set { _CheckSum = value; }                                                        
            }        
                           }
    }    
}