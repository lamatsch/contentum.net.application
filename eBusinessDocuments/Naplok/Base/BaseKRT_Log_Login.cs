
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_Login BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Log_Login
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Log_LoginBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Date = true;
               public bool Date
               {
                   get { return _Date; }
                   set { _Date = value; }
               }
                                                            
                                 
               private bool _Status = true;
               public bool Status
               {
                   get { return _Status; }
                   set { _Status = value; }
               }
                                                            
                                 
               private bool _StartDate = true;
               public bool StartDate
               {
                   get { return _StartDate; }
                   set { _StartDate = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Nev = true;
               public bool Felhasznalo_Nev
               {
                   get { return _Felhasznalo_Nev; }
                   set { _Felhasznalo_Nev = value; }
               }
                                                            
                                 
               private bool _CsoportTag_Id = true;
               public bool CsoportTag_Id
               {
                   get { return _CsoportTag_Id; }
                   set { _CsoportTag_Id = value; }
               }
                                                            
                                 
               private bool _Helyettesito_Id = true;
               public bool Helyettesito_Id
               {
                   get { return _Helyettesito_Id; }
                   set { _Helyettesito_Id = value; }
               }
                                                            
                                 
               private bool _Helyettesito_Nev = true;
               public bool Helyettesito_Nev
               {
                   get { return _Helyettesito_Nev; }
                   set { _Helyettesito_Nev = value; }
               }
                                                            
                                 
               private bool _Helyettesites_Id = true;
               public bool Helyettesites_Id
               {
                   get { return _Helyettesites_Id; }
                   set { _Helyettesites_Id = value; }
               }
                                                            
                                 
               private bool _Helyettesites_Mod = true;
               public bool Helyettesites_Mod
               {
                   get { return _Helyettesites_Mod; }
                   set { _Helyettesites_Mod = value; }
               }
                                                            
                                 
               private bool _LoginType = true;
               public bool LoginType
               {
                   get { return _LoginType; }
                   set { _LoginType = value; }
               }
                                                            
                                 
               private bool _UserHostAddress = true;
               public bool UserHostAddress
               {
                   get { return _UserHostAddress; }
                   set { _UserHostAddress = value; }
               }
                                                            
                                 
               private bool _Level = true;
               public bool Level
               {
                   get { return _Level; }
                   set { _Level = value; }
               }
                                                            
                                 
               private bool _HibaKod = true;
               public bool HibaKod
               {
                   get { return _HibaKod; }
                   set { _HibaKod = value; }
               }
                                                            
                                 
               private bool _HibaUzenet = true;
               public bool HibaUzenet
               {
                   get { return _HibaUzenet; }
                   set { _HibaUzenet = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Date = Value;               
                    
                   Status = Value;               
                    
                   StartDate = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   Felhasznalo_Nev = Value;               
                    
                   CsoportTag_Id = Value;               
                    
                   Helyettesito_Id = Value;               
                    
                   Helyettesito_Nev = Value;               
                    
                   Helyettesites_Id = Value;               
                    
                   Helyettesites_Mod = Value;               
                    
                   LoginType = Value;               
                    
                   UserHostAddress = Value;               
                    
                   Level = Value;               
                    
                   HibaKod = Value;               
                    
                   HibaUzenet = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Log_LoginBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlDateTime _Date = SqlDateTime.Null;
           
        /// <summary>
        /// Date Base property </summary>
            public SqlDateTime Date
            {
                get { return _Date; }
                set { _Date = value; }                                                        
            }        
                   
           
        private SqlChars _Status = SqlChars.Null;
           
        /// <summary>
        /// Status Base property </summary>
            public SqlChars Status
            {
                get { return _Status; }
                set { _Status = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// StartDate Base property </summary>
            public SqlDateTime StartDate
            {
                get { return _StartDate; }
                set { _StartDate = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlString _Felhasznalo_Nev = SqlString.Null;
           
        /// <summary>
        /// Felhasznalo_Nev Base property </summary>
            public SqlString Felhasznalo_Nev
            {
                get { return _Felhasznalo_Nev; }
                set { _Felhasznalo_Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _CsoportTag_Id = SqlGuid.Null;
           
        /// <summary>
        /// CsoportTag_Id Base property </summary>
            public SqlGuid CsoportTag_Id
            {
                get { return _CsoportTag_Id; }
                set { _CsoportTag_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Helyettesito_Id = SqlGuid.Null;
           
        /// <summary>
        /// Helyettesito_Id Base property </summary>
            public SqlGuid Helyettesito_Id
            {
                get { return _Helyettesito_Id; }
                set { _Helyettesito_Id = value; }                                                        
            }        
                   
           
        private SqlString _Helyettesito_Nev = SqlString.Null;
           
        /// <summary>
        /// Helyettesito_Nev Base property </summary>
            public SqlString Helyettesito_Nev
            {
                get { return _Helyettesito_Nev; }
                set { _Helyettesito_Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _Helyettesites_Id = SqlGuid.Null;
           
        /// <summary>
        /// Helyettesites_Id Base property </summary>
            public SqlGuid Helyettesites_Id
            {
                get { return _Helyettesites_Id; }
                set { _Helyettesites_Id = value; }                                                        
            }        
                   
           
        private SqlString _Helyettesites_Mod = SqlString.Null;
           
        /// <summary>
        /// Helyettesites_Mod Base property </summary>
            public SqlString Helyettesites_Mod
            {
                get { return _Helyettesites_Mod; }
                set { _Helyettesites_Mod = value; }                                                        
            }        
                   
           
        private SqlString _LoginType = SqlString.Null;
           
        /// <summary>
        /// LoginType Base property </summary>
            public SqlString LoginType
            {
                get { return _LoginType; }
                set { _LoginType = value; }                                                        
            }        
                   
           
        private SqlString _UserHostAddress = SqlString.Null;
           
        /// <summary>
        /// UserHostAddress Base property </summary>
            public SqlString UserHostAddress
            {
                get { return _UserHostAddress; }
                set { _UserHostAddress = value; }                                                        
            }        
                   
           
        private SqlString _Level = SqlString.Null;
           
        /// <summary>
        /// Level Base property </summary>
            public SqlString Level
            {
                get { return _Level; }
                set { _Level = value; }                                                        
            }        
                   
           
        private SqlString _HibaKod = SqlString.Null;
           
        /// <summary>
        /// HibaKod Base property </summary>
            public SqlString HibaKod
            {
                get { return _HibaKod; }
                set { _HibaKod = value; }                                                        
            }        
                   
           
        private SqlString _HibaUzenet = SqlString.Null;
           
        /// <summary>
        /// HibaUzenet Base property </summary>
            public SqlString HibaUzenet
            {
                get { return _HibaUzenet; }
                set { _HibaUzenet = value; }                                                        
            }        
                           }
    }    
}