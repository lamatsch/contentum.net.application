
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_Page BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Log_Page
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Log_PageBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Date = true;
               public bool Date
               {
                   get { return _Date; }
                   set { _Date = value; }
               }
                                                            
                                 
               private bool _Status = true;
               public bool Status
               {
                   get { return _Status; }
                   set { _Status = value; }
               }
                                                            
                                 
               private bool _StartDate = true;
               public bool StartDate
               {
                   get { return _StartDate; }
                   set { _StartDate = value; }
               }
                                                            
                                 
               private bool _Modul_Id = true;
               public bool Modul_Id
               {
                   get { return _Modul_Id; }
                   set { _Modul_Id = value; }
               }
                                                            
                                 
               private bool _Login_Tranz_id = true;
               public bool Login_Tranz_id
               {
                   get { return _Login_Tranz_id; }
                   set { _Login_Tranz_id = value; }
               }
                                                            
                                 
               private bool _Name = true;
               public bool Name
               {
                   get { return _Name; }
                   set { _Name = value; }
               }
                                                            
                                 
               private bool _Url = true;
               public bool Url
               {
                   get { return _Url; }
                   set { _Url = value; }
               }
                                                            
                                 
               private bool _QueryString = true;
               public bool QueryString
               {
                   get { return _QueryString; }
                   set { _QueryString = value; }
               }
                                                            
                                 
               private bool _Command = true;
               public bool Command
               {
                   get { return _Command; }
                   set { _Command = value; }
               }
                                                            
                                 
               private bool _IsPostBack = true;
               public bool IsPostBack
               {
                   get { return _IsPostBack; }
                   set { _IsPostBack = value; }
               }
                                                            
                                 
               private bool _IsAsync = true;
               public bool IsAsync
               {
                   get { return _IsAsync; }
                   set { _IsAsync = value; }
               }
                                                            
                                 
               private bool _Level = true;
               public bool Level
               {
                   get { return _Level; }
                   set { _Level = value; }
               }
                                                            
                                 
               private bool _HibaKod = true;
               public bool HibaKod
               {
                   get { return _HibaKod; }
                   set { _HibaKod = value; }
               }
                                                            
                                 
               private bool _HibaUzenet = true;
               public bool HibaUzenet
               {
                   get { return _HibaUzenet; }
                   set { _HibaUzenet = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Date = Value;               
                    
                   Status = Value;               
                    
                   StartDate = Value;               
                    
                   Modul_Id = Value;               
                    
                   Login_Tranz_id = Value;               
                    
                   Name = Value;               
                    
                   Url = Value;               
                    
                   QueryString = Value;               
                    
                   Command = Value;               
                    
                   IsPostBack = Value;               
                    
                   IsAsync = Value;               
                    
                   Level = Value;               
                    
                   HibaKod = Value;               
                    
                   HibaUzenet = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Log_PageBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlDateTime _Date = SqlDateTime.Null;
           
        /// <summary>
        /// Date Base property </summary>
            public SqlDateTime Date
            {
                get { return _Date; }
                set { _Date = value; }                                                        
            }        
                   
           
        private SqlChars _Status = SqlChars.Null;
           
        /// <summary>
        /// Status Base property </summary>
            public SqlChars Status
            {
                get { return _Status; }
                set { _Status = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// StartDate Base property </summary>
            public SqlDateTime StartDate
            {
                get { return _StartDate; }
                set { _StartDate = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_Id Base property </summary>
            public SqlGuid Modul_Id
            {
                get { return _Modul_Id; }
                set { _Modul_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Login_Tranz_id = SqlGuid.Null;
           
        /// <summary>
        /// Login_Tranz_id Base property </summary>
            public SqlGuid Login_Tranz_id
            {
                get { return _Login_Tranz_id; }
                set { _Login_Tranz_id = value; }                                                        
            }        
                   
           
        private SqlString _Name = SqlString.Null;
           
        /// <summary>
        /// Name Base property </summary>
            public SqlString Name
            {
                get { return _Name; }
                set { _Name = value; }                                                        
            }        
                   
           
        private SqlString _Url = SqlString.Null;
           
        /// <summary>
        /// Url Base property </summary>
            public SqlString Url
            {
                get { return _Url; }
                set { _Url = value; }                                                        
            }        
                   
           
        private SqlString _QueryString = SqlString.Null;
           
        /// <summary>
        /// QueryString Base property </summary>
            public SqlString QueryString
            {
                get { return _QueryString; }
                set { _QueryString = value; }                                                        
            }        
                   
           
        private SqlString _Command = SqlString.Null;
           
        /// <summary>
        /// Command Base property </summary>
            public SqlString Command
            {
                get { return _Command; }
                set { _Command = value; }                                                        
            }        
                   
           
        private SqlChars _IsPostBack = SqlChars.Null;
           
        /// <summary>
        /// IsPostBack Base property </summary>
            public SqlChars IsPostBack
            {
                get { return _IsPostBack; }
                set { _IsPostBack = value; }                                                        
            }        
                   
           
        private SqlChars _IsAsync = SqlChars.Null;
           
        /// <summary>
        /// IsAsync Base property </summary>
            public SqlChars IsAsync
            {
                get { return _IsAsync; }
                set { _IsAsync = value; }                                                        
            }        
                   
           
        private SqlString _Level = SqlString.Null;
           
        /// <summary>
        /// Level Base property </summary>
            public SqlString Level
            {
                get { return _Level; }
                set { _Level = value; }                                                        
            }        
                   
           
        private SqlString _HibaKod = SqlString.Null;
           
        /// <summary>
        /// HibaKod Base property </summary>
            public SqlString HibaKod
            {
                get { return _HibaKod; }
                set { _HibaKod = value; }                                                        
            }        
                   
           
        private SqlString _HibaUzenet = SqlString.Null;
           
        /// <summary>
        /// HibaUzenet Base property </summary>
            public SqlString HibaUzenet
            {
                get { return _HibaUzenet; }
                set { _HibaUzenet = value; }                                                        
            }        
                           }
    }    
}