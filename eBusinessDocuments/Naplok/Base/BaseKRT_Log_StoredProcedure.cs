
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_StoredProcedure BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Log_StoredProcedure
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Log_StoredProcedureBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Date = true;
               public bool Date
               {
                   get { return _Date; }
                   set { _Date = value; }
               }
                                                            
                                 
               private bool _Status = true;
               public bool Status
               {
                   get { return _Status; }
                   set { _Status = value; }
               }
                                                            
                                 
               private bool _StartDate = true;
               public bool StartDate
               {
                   get { return _StartDate; }
                   set { _StartDate = value; }
               }
                                                            
                                 
               private bool _WS_StartDate = true;
               public bool WS_StartDate
               {
                   get { return _WS_StartDate; }
                   set { _WS_StartDate = value; }
               }
                                                            
                                 
               private bool _Name = true;
               public bool Name
               {
                   get { return _Name; }
                   set { _Name = value; }
               }
                                                            
                                 
               private bool _Level = true;
               public bool Level
               {
                   get { return _Level; }
                   set { _Level = value; }
               }
                                                            
                                 
               private bool _HibaKod = true;
               public bool HibaKod
               {
                   get { return _HibaKod; }
                   set { _HibaKod = value; }
               }
                                                            
                                 
               private bool _HibaUzenet = true;
               public bool HibaUzenet
               {
                   get { return _HibaUzenet; }
                   set { _HibaUzenet = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Date = Value;               
                    
                   Status = Value;               
                    
                   StartDate = Value;               
                    
                   WS_StartDate = Value;               
                    
                   Name = Value;               
                    
                   Level = Value;               
                    
                   HibaKod = Value;               
                    
                   HibaUzenet = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Log_StoredProcedureBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlDateTime _Date = SqlDateTime.Null;
           
        /// <summary>
        /// Date Base property </summary>
            public SqlDateTime Date
            {
                get { return _Date; }
                set { _Date = value; }                                                        
            }        
                   
           
        private SqlChars _Status = SqlChars.Null;
           
        /// <summary>
        /// Status Base property </summary>
            public SqlChars Status
            {
                get { return _Status; }
                set { _Status = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// StartDate Base property </summary>
            public SqlDateTime StartDate
            {
                get { return _StartDate; }
                set { _StartDate = value; }                                                        
            }        
                   
           
        private SqlDateTime _WS_StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// WS_StartDate Base property </summary>
            public SqlDateTime WS_StartDate
            {
                get { return _WS_StartDate; }
                set { _WS_StartDate = value; }                                                        
            }        
                   
           
        private SqlString _Name = SqlString.Null;
           
        /// <summary>
        /// Name Base property </summary>
            public SqlString Name
            {
                get { return _Name; }
                set { _Name = value; }                                                        
            }        
                   
           
        private SqlString _Level = SqlString.Null;
           
        /// <summary>
        /// Level Base property </summary>
            public SqlString Level
            {
                get { return _Level; }
                set { _Level = value; }                                                        
            }        
                   
           
        private SqlString _HibaKod = SqlString.Null;
           
        /// <summary>
        /// HibaKod Base property </summary>
            public SqlString HibaKod
            {
                get { return _HibaKod; }
                set { _HibaKod = value; }                                                        
            }        
                   
           
        private SqlString _HibaUzenet = SqlString.Null;
           
        /// <summary>
        /// HibaUzenet Base property </summary>
            public SqlString HibaUzenet
            {
                get { return _HibaUzenet; }
                set { _HibaUzenet = value; }                                                        
            }        
                           }
    }    
}