
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_WebService BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Log_WebService
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Log_WebServiceBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Date = true;
               public bool Date
               {
                   get { return _Date; }
                   set { _Date = value; }
               }
                                                            
                                 
               private bool _Status = true;
               public bool Status
               {
                   get { return _Status; }
                   set { _Status = value; }
               }
                                                            
                                 
               private bool _StartDate = true;
               public bool StartDate
               {
                   get { return _StartDate; }
                   set { _StartDate = value; }
               }
                                                            
                                 
               private bool _ParentWS_StartDate = true;
               public bool ParentWS_StartDate
               {
                   get { return _ParentWS_StartDate; }
                   set { _ParentWS_StartDate = value; }
               }
                                                            
                                 
               private bool _Machine = true;
               public bool Machine
               {
                   get { return _Machine; }
                   set { _Machine = value; }
               }
                                                            
                                 
               private bool _Url = true;
               public bool Url
               {
                   get { return _Url; }
                   set { _Url = value; }
               }
                                                            
                                 
               private bool _Name = true;
               public bool Name
               {
                   get { return _Name; }
                   set { _Name = value; }
               }
                                                            
                                 
               private bool _Method = true;
               public bool Method
               {
                   get { return _Method; }
                   set { _Method = value; }
               }
                                                            
                                 
               private bool _Level = true;
               public bool Level
               {
                   get { return _Level; }
                   set { _Level = value; }
               }
                                                            
                                 
               private bool _HibaKod = true;
               public bool HibaKod
               {
                   get { return _HibaKod; }
                   set { _HibaKod = value; }
               }
                                                            
                                 
               private bool _HibaUzenet = true;
               public bool HibaUzenet
               {
                   get { return _HibaUzenet; }
                   set { _HibaUzenet = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Date = Value;               
                    
                   Status = Value;               
                    
                   StartDate = Value;               
                    
                   ParentWS_StartDate = Value;               
                    
                   Machine = Value;               
                    
                   Url = Value;               
                    
                   Name = Value;               
                    
                   Method = Value;               
                    
                   Level = Value;               
                    
                   HibaKod = Value;               
                    
                   HibaUzenet = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Log_WebServiceBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlDateTime _Date = SqlDateTime.Null;
           
        /// <summary>
        /// Date Base property </summary>
            public SqlDateTime Date
            {
                get { return _Date; }
                set { _Date = value; }                                                        
            }        
                   
           
        private SqlChars _Status = SqlChars.Null;
           
        /// <summary>
        /// Status Base property </summary>
            public SqlChars Status
            {
                get { return _Status; }
                set { _Status = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// StartDate Base property </summary>
            public SqlDateTime StartDate
            {
                get { return _StartDate; }
                set { _StartDate = value; }                                                        
            }        
                   
           
        private SqlDateTime _ParentWS_StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// ParentWS_StartDate Base property </summary>
            public SqlDateTime ParentWS_StartDate
            {
                get { return _ParentWS_StartDate; }
                set { _ParentWS_StartDate = value; }                                                        
            }        
                   
           
        private SqlString _Machine = SqlString.Null;
           
        /// <summary>
        /// Machine Base property </summary>
            public SqlString Machine
            {
                get { return _Machine; }
                set { _Machine = value; }                                                        
            }        
                   
           
        private SqlString _Url = SqlString.Null;
           
        /// <summary>
        /// Url Base property </summary>
            public SqlString Url
            {
                get { return _Url; }
                set { _Url = value; }                                                        
            }        
                   
           
        private SqlString _Name = SqlString.Null;
           
        /// <summary>
        /// Name Base property </summary>
            public SqlString Name
            {
                get { return _Name; }
                set { _Name = value; }                                                        
            }        
                   
           
        private SqlString _Method = SqlString.Null;
           
        /// <summary>
        /// Method Base property </summary>
            public SqlString Method
            {
                get { return _Method; }
                set { _Method = value; }                                                        
            }        
                   
           
        private SqlString _Level = SqlString.Null;
           
        /// <summary>
        /// Level Base property </summary>
            public SqlString Level
            {
                get { return _Level; }
                set { _Level = value; }                                                        
            }        
                   
           
        private SqlString _HibaKod = SqlString.Null;
           
        /// <summary>
        /// HibaKod Base property </summary>
            public SqlString HibaKod
            {
                get { return _HibaKod; }
                set { _HibaKod = value; }                                                        
            }        
                   
           
        private SqlString _HibaUzenet = SqlString.Null;
           
        /// <summary>
        /// HibaUzenet Base property </summary>
            public SqlString HibaUzenet
            {
                get { return _HibaUzenet; }
                set { _HibaUzenet = value; }                                                        
            }        
                           }
    }    
}