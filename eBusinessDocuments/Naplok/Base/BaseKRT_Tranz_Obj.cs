
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Tranz_Obj BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Tranz_Obj
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Tranz_ObjBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_AdatElem = true;
               public bool ObjTip_Id_AdatElem
               {
                   get { return _ObjTip_Id_AdatElem; }
                   set { _ObjTip_Id_AdatElem = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _Tranz_id = true;
               public bool Tranz_id
               {
                   get { return _Tranz_id; }
                   set { _Tranz_id = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ObjTip_Id_AdatElem = Value;               
                    
                   Obj_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   Tranz_id = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Tranz_ObjBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_AdatElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_AdatElem Base property </summary>
            public SqlGuid ObjTip_Id_AdatElem
            {
                get { return _ObjTip_Id_AdatElem; }
                set { _ObjTip_Id_AdatElem = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlGuid _Tranz_id = SqlGuid.Null;
           
        /// <summary>
        /// Tranz_id Base property </summary>
            public SqlGuid Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }                                                        
            }        
                           }
    }    
}