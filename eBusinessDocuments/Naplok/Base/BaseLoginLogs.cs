
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// LoginLogs BusinessDocument Class </summary>
    [Serializable()]
    public class BaseLoginLogs
    {
        [System.Xml.Serialization.XmlType("BaseLoginLogsBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _MachineIPAddress = true;
               public bool MachineIPAddress
               {
                   get { return _MachineIPAddress; }
                   set { _MachineIPAddress = value; }
               }
                                                            
                                 
               private bool _MachineName = true;
               public bool MachineName
               {
                   get { return _MachineName; }
                   set { _MachineName = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _Jelszo = true;
               public bool Jelszo
               {
                   get { return _Jelszo; }
                   set { _Jelszo = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   MachineIPAddress = Value;               
                    
                   MachineName = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   Jelszo = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseLoginLogsBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _MachineIPAddress = SqlString.Null;
           
        /// <summary>
        /// MachineIPAddress Base property </summary>
            public SqlString MachineIPAddress
            {
                get { return _MachineIPAddress; }
                set { _MachineIPAddress = value; }                                                        
            }        
                   
           
        private SqlString _MachineName = SqlString.Null;
           
        /// <summary>
        /// MachineName Base property </summary>
            public SqlString MachineName
            {
                get { return _MachineName; }
                set { _MachineName = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlString _Jelszo = SqlString.Null;
           
        /// <summary>
        /// Jelszo Base property </summary>
            public SqlString Jelszo
            {
                get { return _Jelszo; }
                set { _Jelszo = value; }                                                        
            }        
                           }
    }    
}