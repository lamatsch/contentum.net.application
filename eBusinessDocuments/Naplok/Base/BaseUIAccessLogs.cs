
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// UIAccessLogs BusinessDocument Class </summary>
    [Serializable()]
    public class BaseUIAccessLogs
    {
        [System.Xml.Serialization.XmlType("BaseUIAccessLogsBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _LoginLog_Id = true;
               public bool LoginLog_Id
               {
                   get { return _LoginLog_Id; }
                   set { _LoginLog_Id = value; }
               }
                                                            
                                 
               private bool _StartDateOfAsp = true;
               public bool StartDateOfAsp
               {
                   get { return _StartDateOfAsp; }
                   set { _StartDateOfAsp = value; }
               }
                                                            
                                 
               private bool _EndDateOfAsp = true;
               public bool EndDateOfAsp
               {
                   get { return _EndDateOfAsp; }
                   set { _EndDateOfAsp = value; }
               }
                                                            
                                 
               private bool _StartOfDBConn = true;
               public bool StartOfDBConn
               {
                   get { return _StartOfDBConn; }
                   set { _StartOfDBConn = value; }
               }
                                                            
                                 
               private bool _EndOfDBConn = true;
               public bool EndOfDBConn
               {
                   get { return _EndOfDBConn; }
                   set { _EndOfDBConn = value; }
               }
                                                            
                                 
               private bool _PathInfo = true;
               public bool PathInfo
               {
                   get { return _PathInfo; }
                   set { _PathInfo = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   LoginLog_Id = Value;               
                    
                   StartDateOfAsp = Value;               
                    
                   EndDateOfAsp = Value;               
                    
                   StartOfDBConn = Value;               
                    
                   EndOfDBConn = Value;               
                    
                   PathInfo = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseUIAccessLogsBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _LoginLog_Id = SqlGuid.Null;
           
        /// <summary>
        /// LoginLog_Id Base property </summary>
            public SqlGuid LoginLog_Id
            {
                get { return _LoginLog_Id; }
                set { _LoginLog_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDateOfAsp = SqlDateTime.Null;
           
        /// <summary>
        /// StartDateOfAsp Base property </summary>
            public SqlDateTime StartDateOfAsp
            {
                get { return _StartDateOfAsp; }
                set { _StartDateOfAsp = value; }                                                        
            }        
                   
           
        private SqlDateTime _EndDateOfAsp = SqlDateTime.Null;
           
        /// <summary>
        /// EndDateOfAsp Base property </summary>
            public SqlDateTime EndDateOfAsp
            {
                get { return _EndDateOfAsp; }
                set { _EndDateOfAsp = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartOfDBConn = SqlDateTime.Null;
           
        /// <summary>
        /// StartOfDBConn Base property </summary>
            public SqlDateTime StartOfDBConn
            {
                get { return _StartOfDBConn; }
                set { _StartOfDBConn = value; }                                                        
            }        
                   
           
        private SqlDateTime _EndOfDBConn = SqlDateTime.Null;
           
        /// <summary>
        /// EndOfDBConn Base property </summary>
            public SqlDateTime EndOfDBConn
            {
                get { return _EndOfDBConn; }
                set { _EndOfDBConn = value; }                                                        
            }        
                   
           
        private SqlString _PathInfo = SqlString.Null;
           
        /// <summary>
        /// PathInfo Base property </summary>
            public SqlString PathInfo
            {
                get { return _PathInfo; }
                set { _PathInfo = value; }                                                        
            }        
                           }
    }    
}