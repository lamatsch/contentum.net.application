
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// WSAccessLogs BusinessDocument Class </summary>
    [Serializable()]
    public class BaseWSAccessLogs
    {
        [System.Xml.Serialization.XmlType("BaseWSAccessLogsBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _LoginLog_Id = true;
               public bool LoginLog_Id
               {
                   get { return _LoginLog_Id; }
                   set { _LoginLog_Id = value; }
               }
                                                            
                                 
               private bool _UIAccessLog_Id = true;
               public bool UIAccessLog_Id
               {
                   get { return _UIAccessLog_Id; }
                   set { _UIAccessLog_Id = value; }
               }
                                                            
                                 
               private bool _StartDateOfWS = true;
               public bool StartDateOfWS
               {
                   get { return _StartDateOfWS; }
                   set { _StartDateOfWS = value; }
               }
                                                            
                                 
               private bool _EndDateOfWS = true;
               public bool EndDateOfWS
               {
                   get { return _EndDateOfWS; }
                   set { _EndDateOfWS = value; }
               }
                                                            
                                 
               private bool _StartOfDBConn = true;
               public bool StartOfDBConn
               {
                   get { return _StartOfDBConn; }
                   set { _StartOfDBConn = value; }
               }
                                                            
                                 
               private bool _EndOfDBConn = true;
               public bool EndOfDBConn
               {
                   get { return _EndOfDBConn; }
                   set { _EndOfDBConn = value; }
               }
                                                            
                                 
               private bool _PathInfo = true;
               public bool PathInfo
               {
                   get { return _PathInfo; }
                   set { _PathInfo = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   LoginLog_Id = Value;               
                    
                   UIAccessLog_Id = Value;               
                    
                   StartDateOfWS = Value;               
                    
                   EndDateOfWS = Value;               
                    
                   StartOfDBConn = Value;               
                    
                   EndOfDBConn = Value;               
                    
                   PathInfo = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseWSAccessLogsBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _LoginLog_Id = SqlGuid.Null;
           
        /// <summary>
        /// LoginLog_Id Base property </summary>
            public SqlGuid LoginLog_Id
            {
                get { return _LoginLog_Id; }
                set { _LoginLog_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _UIAccessLog_Id = SqlGuid.Null;
           
        /// <summary>
        /// UIAccessLog_Id Base property </summary>
            public SqlGuid UIAccessLog_Id
            {
                get { return _UIAccessLog_Id; }
                set { _UIAccessLog_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartDateOfWS = SqlDateTime.Null;
           
        /// <summary>
        /// StartDateOfWS Base property </summary>
            public SqlDateTime StartDateOfWS
            {
                get { return _StartDateOfWS; }
                set { _StartDateOfWS = value; }                                                        
            }        
                   
           
        private SqlDateTime _EndDateOfWS = SqlDateTime.Null;
           
        /// <summary>
        /// EndDateOfWS Base property </summary>
            public SqlDateTime EndDateOfWS
            {
                get { return _EndDateOfWS; }
                set { _EndDateOfWS = value; }                                                        
            }        
                   
           
        private SqlDateTime _StartOfDBConn = SqlDateTime.Null;
           
        /// <summary>
        /// StartOfDBConn Base property </summary>
            public SqlDateTime StartOfDBConn
            {
                get { return _StartOfDBConn; }
                set { _StartOfDBConn = value; }                                                        
            }        
                   
           
        private SqlDateTime _EndOfDBConn = SqlDateTime.Null;
           
        /// <summary>
        /// EndOfDBConn Base property </summary>
            public SqlDateTime EndOfDBConn
            {
                get { return _EndOfDBConn; }
                set { _EndOfDBConn = value; }                                                        
            }        
                   
           
        private SqlString _PathInfo = SqlString.Null;
           
        /// <summary>
        /// PathInfo Base property </summary>
            public SqlString PathInfo
            {
                get { return _PathInfo; }
                set { _PathInfo = value; }                                                        
            }        
                           }
    }    
}