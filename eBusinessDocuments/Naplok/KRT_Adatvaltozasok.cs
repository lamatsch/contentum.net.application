
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Adatvaltozasok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Adatvaltozasok : BaseKRT_Adatvaltozasok
    {
           public BaseTyped Typed = new BaseTyped();
           public BaseDocument Base = new BaseDocument();
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_Tabla property </summary>
        public String ObjTipus_Id_Tabla
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_Tabla); }
            set { Typed.ObjTipus_Id_Tabla = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_Tabla); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_Oszlop property </summary>
        public String ObjTipus_Id_Oszlop
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_Oszlop); }
            set { Typed.ObjTipus_Id_Oszlop = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_Oszlop); }                                            
        }
                   
           
        /// <summary>
        /// RegiErtek property </summary>
        public String RegiErtek
        {
            get { return Utility.GetStringFromSqlString(Typed.RegiErtek); }
            set { Typed.RegiErtek = Utility.SetSqlStringFromString(value, Typed.RegiErtek); }                                            
        }
                   
           
        /// <summary>
        /// UjErtek property </summary>
        public String UjErtek
        {
            get { return Utility.GetStringFromSqlString(Typed.UjErtek); }
            set { Typed.UjErtek = Utility.SetSqlStringFromString(value, Typed.UjErtek); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}