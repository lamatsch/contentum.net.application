
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Esemenyek BusinessDocument Class
    /// Ebben vannak az �gyviteli esem�nyek l�p�sei (Azaz pl. irat iktat�s, sign�l�s...)
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    Munkaallomas,
    ///    MuveletKimenete
    /// </summary>
    [Serializable()]
    public class KRT_Esemenyek : BaseKRT_Esemenyek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property
        /// Objektum id, amire az esemeny vonatkozik
        /// </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Azonositoja property
        /// Objektum kulso azonositoja. (pl ha irat, iktatosz�m...)
        /// kell ez?
        /// </summary>
        public String Azonositoja
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonositoja); }
            set { Typed.Azonositoja = Utility.SetSqlStringFromString(value, Typed.Azonositoja); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_User property
        /// Helyettesitett vegrehajto felhasznalo, alapertelmezetten megegyezik a prt_id_login-nal
        /// </summary>
        public String Felhasznalo_Id_User
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_User); }
            set { Typed.Felhasznalo_Id_User = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_User); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_FelelosUserSzerveze property
        /// Helyettesitett vegrehajto felhasznalo PRT_ID_MUNKAHELY-e
        /// </summary>
        public String Csoport_Id_FelelosUserSzerveze
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_FelelosUserSzerveze); }
            set { Typed.Csoport_Id_FelelosUserSzerveze = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_FelelosUserSzerveze); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_Login property
        /// Helyettesito vegrehajto felhasznalo, az esemeny tenyleges vegrehajtoja
        /// </summary>
        public String Felhasznalo_Id_Login
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_Login); }
            set { Typed.Felhasznalo_Id_Login = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_Login); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Cel property
        /// Csoport Id
        /// </summary>
        public String Csoport_Id_Cel
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Cel); }
            set { Typed.Csoport_Id_Cel = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Cel); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesites_Id property
        /// Helyettes�t�s vagy megb�z�s eset�n a KRT_Helyettesitesek azonos�t�.
        /// </summary>
        public String Helyettesites_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Helyettesites_Id); }
            set { Typed.Helyettesites_Id = Utility.SetSqlGuidFromString(value, Typed.Helyettesites_Id); }                                            
        }
                   
           
        /// <summary>
        /// Tranzakcio_Id property
        /// tranzakci� Id, nem Tranz_id?
        /// </summary>
        public String Tranzakcio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tranzakcio_Id); }
            set { Typed.Tranzakcio_Id = Utility.SetSqlGuidFromString(value, Typed.Tranzakcio_Id); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id property
        /// KRT_Funkciok FK
        /// </summary>
        public String Funkcio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id); }
            set { Typed.Funkcio_Id = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id); }                                            
        }
                   
           
        /// <summary>
        /// CsoportHierarchia property
        /// Aktu�lis felhaszn�l�i csoport hierarchia let�rol�sa
        /// </summary>
        public String CsoportHierarchia
        {
            get { return Utility.GetStringFromSqlString(Typed.CsoportHierarchia); }
            set { Typed.CsoportHierarchia = Utility.SetSqlStringFromString(value, Typed.CsoportHierarchia); }                                            
        }
                   
           
        /// <summary>
        /// KeresesiFeltetel property
        /// Select where felt�tel let�rol�sa
        /// </summary>
        public String KeresesiFeltetel
        {
            get { return Utility.GetStringFromSqlString(Typed.KeresesiFeltetel); }
            set { Typed.KeresesiFeltetel = Utility.SetSqlStringFromString(value, Typed.KeresesiFeltetel); }                                            
        }
                   
           
        /// <summary>
        /// TalalatokSzama property
        /// Az elozo select rekordsz�ma
        /// </summary>
        public String TalalatokSzama
        {
            get { return Utility.GetStringFromSqlInt32(Typed.TalalatokSzama); }
            set { Typed.TalalatokSzama = Utility.SetSqlInt32FromString(value, Typed.TalalatokSzama); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        public String Munkaallomas
        {
            get { return Utility.GetStringFromSqlString(Typed.Munkaallomas); }
            set { Typed.Munkaallomas = Utility.SetSqlStringFromString(value, Typed.Munkaallomas); }                                            
        }
                   
           
        /// <summary>
        /// MuveletKimenete property
        /// 
        /// </summary>
        public String MuveletKimenete
        {
            get { return Utility.GetStringFromSqlString(Typed.MuveletKimenete); }
            set { Typed.MuveletKimenete = Utility.SetSqlStringFromString(value, Typed.MuveletKimenete); }                                            
        }
                   
           
        /// <summary>
        /// CheckSum property
        /// 
        /// </summary>
        public String CheckSum
        {
            get { return Utility.GetStringFromSqlString(Typed.CheckSum); }
            set { Typed.CheckSum = Utility.SetSqlStringFromString(value, Typed.CheckSum); }                                            
        }
                           }
   
}