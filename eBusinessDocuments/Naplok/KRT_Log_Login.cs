
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_Login BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Log_Login : BaseKRT_Log_Login
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Date property </summary>
        public String Date
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Date); }
            set { Typed.Date = Utility.SetSqlDateTimeFromString(value, Typed.Date); }                                            
        }
                   
           
        /// <summary>
        /// Status property </summary>
        public String Status
        {
            get { return Utility.GetStringFromSqlChars(Typed.Status); }
            set { Typed.Status = Utility.SetSqlCharsFromString(value, Typed.Status); }                                            
        }
                   
           
        /// <summary>
        /// StartDate property </summary>
        public String StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDate); }
            set { Typed.StartDate = Utility.SetSqlDateTimeFromString(value, Typed.StartDate); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Nev property </summary>
        public String Felhasznalo_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Felhasznalo_Nev); }
            set { Typed.Felhasznalo_Nev = Utility.SetSqlStringFromString(value, Typed.Felhasznalo_Nev); }                                            
        }
                   
           
        /// <summary>
        /// CsoportTag_Id property </summary>
        public String CsoportTag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CsoportTag_Id); }
            set { Typed.CsoportTag_Id = Utility.SetSqlGuidFromString(value, Typed.CsoportTag_Id); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesito_Id property </summary>
        public String Helyettesito_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Helyettesito_Id); }
            set { Typed.Helyettesito_Id = Utility.SetSqlGuidFromString(value, Typed.Helyettesito_Id); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesito_Nev property </summary>
        public String Helyettesito_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Helyettesito_Nev); }
            set { Typed.Helyettesito_Nev = Utility.SetSqlStringFromString(value, Typed.Helyettesito_Nev); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesites_Id property </summary>
        public String Helyettesites_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Helyettesites_Id); }
            set { Typed.Helyettesites_Id = Utility.SetSqlGuidFromString(value, Typed.Helyettesites_Id); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesites_Mod property </summary>
        public String Helyettesites_Mod
        {
            get { return Utility.GetStringFromSqlString(Typed.Helyettesites_Mod); }
            set { Typed.Helyettesites_Mod = Utility.SetSqlStringFromString(value, Typed.Helyettesites_Mod); }                                            
        }
                   
           
        /// <summary>
        /// LoginType property </summary>
        public String LoginType
        {
            get { return Utility.GetStringFromSqlString(Typed.LoginType); }
            set { Typed.LoginType = Utility.SetSqlStringFromString(value, Typed.LoginType); }                                            
        }
                   
           
        /// <summary>
        /// UserHostAddress property </summary>
        public String UserHostAddress
        {
            get { return Utility.GetStringFromSqlString(Typed.UserHostAddress); }
            set { Typed.UserHostAddress = Utility.SetSqlStringFromString(value, Typed.UserHostAddress); }                                            
        }
                   
           
        /// <summary>
        /// Level property </summary>
        public String Level
        {
            get { return Utility.GetStringFromSqlString(Typed.Level); }
            set { Typed.Level = Utility.SetSqlStringFromString(value, Typed.Level); }                                            
        }
                   
           
        /// <summary>
        /// HibaKod property </summary>
        public String HibaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaKod); }
            set { Typed.HibaKod = Utility.SetSqlStringFromString(value, Typed.HibaKod); }                                            
        }
                   
           
        /// <summary>
        /// HibaUzenet property </summary>
        public String HibaUzenet
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaUzenet); }
            set { Typed.HibaUzenet = Utility.SetSqlStringFromString(value, Typed.HibaUzenet); }                                            
        }
                           }
   
}