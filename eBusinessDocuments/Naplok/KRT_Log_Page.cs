
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_Page BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Log_Page : BaseKRT_Log_Page
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Date property </summary>
        public String Date
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Date); }
            set { Typed.Date = Utility.SetSqlDateTimeFromString(value, Typed.Date); }                                            
        }
                   
           
        /// <summary>
        /// Status property </summary>
        public String Status
        {
            get { return Utility.GetStringFromSqlChars(Typed.Status); }
            set { Typed.Status = Utility.SetSqlCharsFromString(value, Typed.Status); }                                            
        }
                   
           
        /// <summary>
        /// StartDate property </summary>
        public String StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDate); }
            set { Typed.StartDate = Utility.SetSqlDateTimeFromString(value, Typed.StartDate); }                                            
        }
                   
           
        /// <summary>
        /// Modul_Id property </summary>
        public String Modul_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modul_Id); }
            set { Typed.Modul_Id = Utility.SetSqlGuidFromString(value, Typed.Modul_Id); }                                            
        }
                   
           
        /// <summary>
        /// Login_Tranz_id property </summary>
        public String Login_Tranz_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Login_Tranz_id); }
            set { Typed.Login_Tranz_id = Utility.SetSqlGuidFromString(value, Typed.Login_Tranz_id); }                                            
        }
                   
           
        /// <summary>
        /// Name property </summary>
        public String Name
        {
            get { return Utility.GetStringFromSqlString(Typed.Name); }
            set { Typed.Name = Utility.SetSqlStringFromString(value, Typed.Name); }                                            
        }
                   
           
        /// <summary>
        /// Url property </summary>
        public String Url
        {
            get { return Utility.GetStringFromSqlString(Typed.Url); }
            set { Typed.Url = Utility.SetSqlStringFromString(value, Typed.Url); }                                            
        }
                   
           
        /// <summary>
        /// QueryString property </summary>
        public String QueryString
        {
            get { return Utility.GetStringFromSqlString(Typed.QueryString); }
            set { Typed.QueryString = Utility.SetSqlStringFromString(value, Typed.QueryString); }                                            
        }
                   
           
        /// <summary>
        /// Command property </summary>
        public String Command
        {
            get { return Utility.GetStringFromSqlString(Typed.Command); }
            set { Typed.Command = Utility.SetSqlStringFromString(value, Typed.Command); }                                            
        }
                   
           
        /// <summary>
        /// IsPostBack property </summary>
        public String IsPostBack
        {
            get { return Utility.GetStringFromSqlChars(Typed.IsPostBack); }
            set { Typed.IsPostBack = Utility.SetSqlCharsFromString(value, Typed.IsPostBack); }                                            
        }
                   
           
        /// <summary>
        /// IsAsync property </summary>
        public String IsAsync
        {
            get { return Utility.GetStringFromSqlChars(Typed.IsAsync); }
            set { Typed.IsAsync = Utility.SetSqlCharsFromString(value, Typed.IsAsync); }                                            
        }
                   
           
        /// <summary>
        /// Level property </summary>
        public String Level
        {
            get { return Utility.GetStringFromSqlString(Typed.Level); }
            set { Typed.Level = Utility.SetSqlStringFromString(value, Typed.Level); }                                            
        }
                   
           
        /// <summary>
        /// HibaKod property </summary>
        public String HibaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaKod); }
            set { Typed.HibaKod = Utility.SetSqlStringFromString(value, Typed.HibaKod); }                                            
        }
                   
           
        /// <summary>
        /// HibaUzenet property </summary>
        public String HibaUzenet
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaUzenet); }
            set { Typed.HibaUzenet = Utility.SetSqlStringFromString(value, Typed.HibaUzenet); }                                            
        }
                           }
   
}