
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Log_WebService BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Log_WebService : BaseKRT_Log_WebService
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Date property </summary>
        public String Date
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Date); }
            set { Typed.Date = Utility.SetSqlDateTimeFromString(value, Typed.Date); }                                            
        }
                   
           
        /// <summary>
        /// Status property </summary>
        public String Status
        {
            get { return Utility.GetStringFromSqlChars(Typed.Status); }
            set { Typed.Status = Utility.SetSqlCharsFromString(value, Typed.Status); }                                            
        }
                   
           
        /// <summary>
        /// StartDate property </summary>
        public String StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDate); }
            set { Typed.StartDate = Utility.SetSqlDateTimeFromString(value, Typed.StartDate); }                                            
        }
                   
           
        /// <summary>
        /// ParentWS_StartDate property </summary>
        public String ParentWS_StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ParentWS_StartDate); }
            set { Typed.ParentWS_StartDate = Utility.SetSqlDateTimeFromString(value, Typed.ParentWS_StartDate); }                                            
        }
                   
           
        /// <summary>
        /// Machine property </summary>
        public String Machine
        {
            get { return Utility.GetStringFromSqlString(Typed.Machine); }
            set { Typed.Machine = Utility.SetSqlStringFromString(value, Typed.Machine); }                                            
        }
                   
           
        /// <summary>
        /// Url property </summary>
        public String Url
        {
            get { return Utility.GetStringFromSqlString(Typed.Url); }
            set { Typed.Url = Utility.SetSqlStringFromString(value, Typed.Url); }                                            
        }
                   
           
        /// <summary>
        /// Name property </summary>
        public String Name
        {
            get { return Utility.GetStringFromSqlString(Typed.Name); }
            set { Typed.Name = Utility.SetSqlStringFromString(value, Typed.Name); }                                            
        }
                   
           
        /// <summary>
        /// Method property </summary>
        public String Method
        {
            get { return Utility.GetStringFromSqlString(Typed.Method); }
            set { Typed.Method = Utility.SetSqlStringFromString(value, Typed.Method); }                                            
        }
                   
           
        /// <summary>
        /// Level property </summary>
        public String Level
        {
            get { return Utility.GetStringFromSqlString(Typed.Level); }
            set { Typed.Level = Utility.SetSqlStringFromString(value, Typed.Level); }                                            
        }
                   
           
        /// <summary>
        /// HibaKod property </summary>
        public String HibaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaKod); }
            set { Typed.HibaKod = Utility.SetSqlStringFromString(value, Typed.HibaKod); }                                            
        }
                   
           
        /// <summary>
        /// HibaUzenet property </summary>
        public String HibaUzenet
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaUzenet); }
            set { Typed.HibaUzenet = Utility.SetSqlStringFromString(value, Typed.HibaUzenet); }                                            
        }
                           }
   
}