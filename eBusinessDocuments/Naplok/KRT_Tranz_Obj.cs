
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Tranz_Obj BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Tranz_Obj : BaseKRT_Tranz_Obj
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_AdatElem property </summary>
        public String ObjTip_Id_AdatElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_AdatElem); }
            set { Typed.ObjTip_Id_AdatElem = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_AdatElem); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// Tranz_id property </summary>
        public String Tranz_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tranz_id); }
            set { Typed.Tranz_id = Utility.SetSqlGuidFromString(value, Typed.Tranz_id); }                                            
        }
                           }
   
}