
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// LoginLogs BusinessDocument Class </summary>
    [Serializable()]
    public class LoginLogs : BaseLoginLogs
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// MachineIPAddress property </summary>
        public String MachineIPAddress
        {
            get { return Utility.GetStringFromSqlString(Typed.MachineIPAddress); }
            set { Typed.MachineIPAddress = Utility.SetSqlStringFromString(value, Typed.MachineIPAddress); }                                            
        }
                   
           
        /// <summary>
        /// MachineName property </summary>
        public String MachineName
        {
            get { return Utility.GetStringFromSqlString(Typed.MachineName); }
            set { Typed.MachineName = Utility.SetSqlStringFromString(value, Typed.MachineName); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Jelszo property </summary>
        public String Jelszo
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelszo); }
            set { Typed.Jelszo = Utility.SetSqlStringFromString(value, Typed.Jelszo); }                                            
        }
                           }
   
}