
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// UIAccessLogs BusinessDocument Class </summary>
    [Serializable()]
    public class UIAccessLogs : BaseUIAccessLogs
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// LoginLog_Id property </summary>
        public String LoginLog_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.LoginLog_Id); }
            set { Typed.LoginLog_Id = Utility.SetSqlGuidFromString(value, Typed.LoginLog_Id); }                                            
        }
                   
           
        /// <summary>
        /// StartDateOfAsp property </summary>
        public String StartDateOfAsp
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDateOfAsp); }
            set { Typed.StartDateOfAsp = Utility.SetSqlDateTimeFromString(value, Typed.StartDateOfAsp); }                                            
        }
                   
           
        /// <summary>
        /// EndDateOfAsp property </summary>
        public String EndDateOfAsp
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.EndDateOfAsp); }
            set { Typed.EndDateOfAsp = Utility.SetSqlDateTimeFromString(value, Typed.EndDateOfAsp); }                                            
        }
                   
           
        /// <summary>
        /// StartOfDBConn property </summary>
        public String StartOfDBConn
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartOfDBConn); }
            set { Typed.StartOfDBConn = Utility.SetSqlDateTimeFromString(value, Typed.StartOfDBConn); }                                            
        }
                   
           
        /// <summary>
        /// EndOfDBConn property </summary>
        public String EndOfDBConn
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.EndOfDBConn); }
            set { Typed.EndOfDBConn = Utility.SetSqlDateTimeFromString(value, Typed.EndOfDBConn); }                                            
        }
                   
           
        /// <summary>
        /// PathInfo property </summary>
        public String PathInfo
        {
            get { return Utility.GetStringFromSqlString(Typed.PathInfo); }
            set { Typed.PathInfo = Utility.SetSqlStringFromString(value, Typed.PathInfo); }                                            
        }
                           }
   
}