
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// WSAccessLogs BusinessDocument Class </summary>
    [Serializable()]
    public class WSAccessLogs : BaseWSAccessLogs
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// LoginLog_Id property </summary>
        public String LoginLog_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.LoginLog_Id); }
            set { Typed.LoginLog_Id = Utility.SetSqlGuidFromString(value, Typed.LoginLog_Id); }                                            
        }
                   
           
        /// <summary>
        /// UIAccessLog_Id property </summary>
        public String UIAccessLog_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UIAccessLog_Id); }
            set { Typed.UIAccessLog_Id = Utility.SetSqlGuidFromString(value, Typed.UIAccessLog_Id); }                                            
        }
                   
           
        /// <summary>
        /// StartDateOfWS property </summary>
        public String StartDateOfWS
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartDateOfWS); }
            set { Typed.StartDateOfWS = Utility.SetSqlDateTimeFromString(value, Typed.StartDateOfWS); }                                            
        }
                   
           
        /// <summary>
        /// EndDateOfWS property </summary>
        public String EndDateOfWS
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.EndDateOfWS); }
            set { Typed.EndDateOfWS = Utility.SetSqlDateTimeFromString(value, Typed.EndDateOfWS); }                                            
        }
                   
           
        /// <summary>
        /// StartOfDBConn property </summary>
        public String StartOfDBConn
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.StartOfDBConn); }
            set { Typed.StartOfDBConn = Utility.SetSqlDateTimeFromString(value, Typed.StartOfDBConn); }                                            
        }
                   
           
        /// <summary>
        /// EndOfDBConn property </summary>
        public String EndOfDBConn
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.EndOfDBConn); }
            set { Typed.EndOfDBConn = Utility.SetSqlDateTimeFromString(value, Typed.EndOfDBConn); }                                            
        }
                   
           
        /// <summary>
        /// PathInfo property </summary>
        public String PathInfo
        {
            get { return Utility.GetStringFromSqlString(Typed.PathInfo); }
            set { Typed.PathInfo = Utility.SetSqlStringFromString(value, Typed.PathInfo); }                                            
        }
                           }
   
}