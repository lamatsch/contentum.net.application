
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Extra_Napok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Extra_Napok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Extra_NapokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Datum = true;
               public bool Datum
               {
                   get { return _Datum; }
                   set { _Datum = value; }
               }
                                                            
                                 
               private bool _Jelzo = true;
               public bool Jelzo
               {
                   get { return _Jelzo; }
                   set { _Jelzo = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Datum = Value;               
                    
                   Jelzo = Value;               
                    
                   Megjegyzes = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Extra_NapokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _Datum = SqlDateTime.Null;
           
        /// <summary>
        /// Datum Base property </summary>
            public SqlDateTime Datum
            {
                get { return _Datum; }
                set { _Datum = value; }                                                        
            }        
                   
           
        private SqlInt32 _Jelzo = SqlInt32.Null;
           
        /// <summary>
        /// Jelzo Base property </summary>
            public SqlInt32 Jelzo
            {
                get { return _Jelzo; }
                set { _Jelzo = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}