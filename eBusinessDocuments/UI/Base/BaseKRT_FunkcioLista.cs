
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_FunkcioLista BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_FunkcioLista
    {
        [System.Xml.Serialization.XmlType("BaseKRT_FunkcioListaBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id = true;
               public bool Funkcio_Id
               {
                   get { return _Funkcio_Id; }
                   set { _Funkcio_Id = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Hivott = true;
               public bool Funkcio_Id_Hivott
               {
                   get { return _Funkcio_Id_Hivott; }
                   set { _Funkcio_Id_Hivott = value; }
               }
                                                            
                                 
               private bool _FutasiSorrend = true;
               public bool FutasiSorrend
               {
                   get { return _FutasiSorrend; }
                   set { _FutasiSorrend = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Funkcio_Id = Value;               
                    
                   Funkcio_Id_Hivott = Value;               
                    
                   FutasiSorrend = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_FunkcioListaBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id Base property </summary>
            public SqlGuid Funkcio_Id
            {
                get { return _Funkcio_Id; }
                set { _Funkcio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Hivott = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Hivott Base property </summary>
            public SqlGuid Funkcio_Id_Hivott
            {
                get { return _Funkcio_Id_Hivott; }
                set { _Funkcio_Id_Hivott = value; }                                                        
            }        
                   
           
        private SqlInt32 _FutasiSorrend = SqlInt32.Null;
           
        /// <summary>
        /// FutasiSorrend Base property </summary>
            public SqlInt32 FutasiSorrend
            {
                get { return _FutasiSorrend; }
                set { _FutasiSorrend = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}