
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Menuk BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Menuk
    {
        [System.Xml.Serialization.XmlType("BaseKRT_MenukBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Menu_Id_Szulo = true;
               public bool Menu_Id_Szulo
               {
                   get { return _Menu_Id_Szulo; }
                   set { _Menu_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id = true;
               public bool Funkcio_Id
               {
                   get { return _Funkcio_Id; }
                   set { _Funkcio_Id = value; }
               }
                                                            
                                 
               private bool _Modul_Id = true;
               public bool Modul_Id
               {
                   get { return _Modul_Id; }
                   set { _Modul_Id = value; }
               }
                                                            
                                 
               private bool _Parameter = true;
               public bool Parameter
               {
                   get { return _Parameter; }
                   set { _Parameter = value; }
               }
                                                            
                                 
               private bool _ImageURL = true;
               public bool ImageURL
               {
                   get { return _ImageURL; }
                   set { _ImageURL = value; }
               }
                                                            
                                 
               private bool _Sorrend = true;
               public bool Sorrend
               {
                   get { return _Sorrend; }
                   set { _Sorrend = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Menu_Id_Szulo = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   Funkcio_Id = Value;               
                    
                   Modul_Id = Value;               
                    
                   Parameter = Value;               
                    
                   ImageURL = Value;               
                    
                   Sorrend = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_MenukBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Menu_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// Menu_Id_Szulo Base property </summary>
            public SqlGuid Menu_Id_Szulo
            {
                get { return _Menu_Id_Szulo; }
                set { _Menu_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id Base property </summary>
            public SqlGuid Funkcio_Id
            {
                get { return _Funkcio_Id; }
                set { _Funkcio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_Id Base property </summary>
            public SqlGuid Modul_Id
            {
                get { return _Modul_Id; }
                set { _Modul_Id = value; }                                                        
            }        
                   
           
        private SqlString _Parameter = SqlString.Null;
           
        /// <summary>
        /// Parameter Base property </summary>
            public SqlString Parameter
            {
                get { return _Parameter; }
                set { _Parameter = value; }                                                        
            }        
                   
           
        private SqlString _ImageURL = SqlString.Null;
           
        /// <summary>
        /// ImageURL Base property </summary>
            public SqlString ImageURL
            {
                get { return _ImageURL; }
                set { _ImageURL = value; }                                                        
            }        
                   
           
        private SqlString _Sorrend = SqlString.Null;
           
        /// <summary>
        /// Sorrend Base property </summary>
            public SqlString Sorrend
            {
                get { return _Sorrend; }
                set { _Sorrend = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}