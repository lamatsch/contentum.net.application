
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Modul_Funkcio BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Modul_Funkcio
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Modul_FunkcioBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id = true;
               public bool Funkcio_Id
               {
                   get { return _Funkcio_Id; }
                   set { _Funkcio_Id = value; }
               }
                                                            
                                 
               private bool _Modul_Id = true;
               public bool Modul_Id
               {
                   get { return _Modul_Id; }
                   set { _Modul_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Funkcio_Id = Value;               
                    
                   Modul_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Modul_FunkcioBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id Base property </summary>
            public SqlGuid Funkcio_Id
            {
                get { return _Funkcio_Id; }
                set { _Funkcio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_Id Base property </summary>
            public SqlGuid Modul_Id
            {
                get { return _Modul_Id; }
                set { _Modul_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}