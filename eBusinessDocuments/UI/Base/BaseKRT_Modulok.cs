
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Modulok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Modulok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_ModulokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _SelectString = true;
               public bool SelectString
               {
                   get { return _SelectString; }
                   set { _SelectString = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _Parameterek = true;
               public bool Parameterek
               {
                   get { return _Parameterek; }
                   set { _Parameterek = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_Adatelem = true;
               public bool ObjTip_Id_Adatelem
               {
                   get { return _ObjTip_Id_Adatelem; }
                   set { _ObjTip_Id_Adatelem = value; }
               }
                                                            
                                 
               private bool _ObjTip_Adatelem = true;
               public bool ObjTip_Adatelem
               {
                   get { return _ObjTip_Adatelem; }
                   set { _ObjTip_Adatelem = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Tipus = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   Leiras = Value;               
                    
                   SelectString = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   Parameterek = Value;               
                    
                   ObjTip_Id_Adatelem = Value;               
                    
                   ObjTip_Adatelem = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_ModulokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _SelectString = SqlString.Null;
           
        /// <summary>
        /// SelectString Base property </summary>
            public SqlString SelectString
            {
                get { return _SelectString; }
                set { _SelectString = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlString _Parameterek = SqlString.Null;
           
        /// <summary>
        /// Parameterek Base property </summary>
            public SqlString Parameterek
            {
                get { return _Parameterek; }
                set { _Parameterek = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_Adatelem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_Adatelem Base property </summary>
            public SqlGuid ObjTip_Id_Adatelem
            {
                get { return _ObjTip_Id_Adatelem; }
                set { _ObjTip_Id_Adatelem = value; }                                                        
            }        
                   
           
        private SqlString _ObjTip_Adatelem = SqlString.Null;
           
        /// <summary>
        /// ObjTip_Adatelem Base property </summary>
            public SqlString ObjTip_Adatelem
            {
                get { return _ObjTip_Adatelem; }
                set { _ObjTip_Adatelem = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}