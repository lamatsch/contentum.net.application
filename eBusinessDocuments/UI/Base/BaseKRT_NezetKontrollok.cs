
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_NezetKontrollok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_NezetKontrollok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_NezetKontrollokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Nezet_Id = true;
               public bool Nezet_Id
               {
                   get { return _Nezet_Id; }
                   set { _Nezet_Id = value; }
               }
                                                            
                                 
               private bool _Muvelet_Id = true;
               public bool Muvelet_Id
               {
                   get { return _Muvelet_Id; }
                   set { _Muvelet_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _Feltetel = true;
               public bool Feltetel
               {
                   get { return _Feltetel; }
                   set { _Feltetel = value; }
               }
                                                            
                                 
               private bool _ObjTipus_id_UIElem = true;
               public bool ObjTipus_id_UIElem
               {
                   get { return _ObjTipus_id_UIElem; }
                   set { _ObjTipus_id_UIElem = value; }
               }
                                                            
                                 
               private bool _Tiltas = true;
               public bool Tiltas
               {
                   get { return _Tiltas; }
                   set { _Tiltas = value; }
               }
                                                            
                                 
               private bool _ObjTipus_id_Adatelem = true;
               public bool ObjTipus_id_Adatelem
               {
                   get { return _ObjTipus_id_Adatelem; }
                   set { _ObjTipus_id_Adatelem = value; }
               }
                                                            
                                 
               private bool _ObjStat_id_Adatelem = true;
               public bool ObjStat_id_Adatelem
               {
                   get { return _ObjStat_id_Adatelem; }
                   set { _ObjStat_id_Adatelem = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Nezet_Id = Value;               
                    
                   Muvelet_Id = Value;               
                    
                   Csoport_Id = Value;               
                    
                   Feltetel = Value;               
                    
                   ObjTipus_id_UIElem = Value;               
                    
                   Tiltas = Value;               
                    
                   ObjTipus_id_Adatelem = Value;               
                    
                   ObjStat_id_Adatelem = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_NezetKontrollokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Nezet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Nezet_Id Base property </summary>
            public SqlGuid Nezet_Id
            {
                get { return _Nezet_Id; }
                set { _Nezet_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Muvelet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Muvelet_Id Base property </summary>
            public SqlGuid Muvelet_Id
            {
                get { return _Muvelet_Id; }
                set { _Muvelet_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlString _Feltetel = SqlString.Null;
           
        /// <summary>
        /// Feltetel Base property </summary>
            public SqlString Feltetel
            {
                get { return _Feltetel; }
                set { _Feltetel = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_id_UIElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_id_UIElem Base property </summary>
            public SqlGuid ObjTipus_id_UIElem
            {
                get { return _ObjTipus_id_UIElem; }
                set { _ObjTipus_id_UIElem = value; }                                                        
            }        
                   
           
        private SqlString _Tiltas = SqlString.Null;
           
        /// <summary>
        /// Tiltas Base property </summary>
            public SqlString Tiltas
            {
                get { return _Tiltas; }
                set { _Tiltas = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_id_Adatelem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_id_Adatelem Base property </summary>
            public SqlGuid ObjTipus_id_Adatelem
            {
                get { return _ObjTipus_id_Adatelem; }
                set { _ObjTipus_id_Adatelem = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjStat_id_Adatelem = SqlGuid.Null;
           
        /// <summary>
        /// ObjStat_id_Adatelem Base property </summary>
            public SqlGuid ObjStat_id_Adatelem
            {
                get { return _ObjStat_id_Adatelem; }
                set { _ObjStat_id_Adatelem = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}