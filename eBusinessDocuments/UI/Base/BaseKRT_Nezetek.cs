
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Nezetek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Nezetek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_NezetekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Form_Id = true;
               public bool Form_Id
               {
                   get { return _Form_Id; }
                   set { _Form_Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _Muvelet_Id = true;
               public bool Muvelet_Id
               {
                   get { return _Muvelet_Id; }
                   set { _Muvelet_Id = value; }
               }
                                                            
                                 
               private bool _DisableControls = true;
               public bool DisableControls
               {
                   get { return _DisableControls; }
                   set { _DisableControls = value; }
               }
                                                            
                                 
               private bool _InvisibleControls = true;
               public bool InvisibleControls
               {
                   get { return _InvisibleControls; }
                   set { _InvisibleControls = value; }
               }
                                                            
                                 
               private bool _ReadOnlyControls = true;
               public bool ReadOnlyControls
               {
                   get { return _ReadOnlyControls; }
                   set { _ReadOnlyControls = value; }
               }
                                                            
                                 
               private bool _TabIndexControls = true;
               public bool TabIndexControls
               {
                   get { return _TabIndexControls; }
                   set { _TabIndexControls = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Form_Id = Value;               
                    
                   Nev = Value;               
                    
                   Leiras = Value;               
                    
                   Csoport_Id = Value;               
                    
                   Muvelet_Id = Value;               
                    
                   DisableControls = Value;               
                    
                   InvisibleControls = Value;               
                    
                   ReadOnlyControls = Value;               
                    
                   TabIndexControls = Value;               
                    
                   Prioritas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_NezetekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Form_Id = SqlGuid.Null;
           
        /// <summary>
        /// Form_Id Base property </summary>
            public SqlGuid Form_Id
            {
                get { return _Form_Id; }
                set { _Form_Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Muvelet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Muvelet_Id Base property </summary>
            public SqlGuid Muvelet_Id
            {
                get { return _Muvelet_Id; }
                set { _Muvelet_Id = value; }                                                        
            }        
                   
           
        private SqlString _DisableControls = SqlString.Null;
           
        /// <summary>
        /// DisableControls Base property </summary>
            public SqlString DisableControls
            {
                get { return _DisableControls; }
                set { _DisableControls = value; }                                                        
            }        
                   
           
        private SqlString _InvisibleControls = SqlString.Null;
           
        /// <summary>
        /// InvisibleControls Base property </summary>
            public SqlString InvisibleControls
            {
                get { return _InvisibleControls; }
                set { _InvisibleControls = value; }                                                        
            }        
                   
           
        private SqlString _ReadOnlyControls = SqlString.Null;
           
        /// <summary>
        /// ReadOnlyControls Base property </summary>
            public SqlString ReadOnlyControls
            {
                get { return _ReadOnlyControls; }
                set { _ReadOnlyControls = value; }                                                        
            }        
                   
           
        private SqlString _TabIndexControls = SqlString.Null;
           
        /// <summary>
        /// TabIndexControls Base property </summary>
            public SqlString TabIndexControls
            {
                get { return _TabIndexControls; }
                set { _TabIndexControls = value; }                                                        
            }        
                   
           
        private SqlInt32 _Prioritas = SqlInt32.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlInt32 Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}