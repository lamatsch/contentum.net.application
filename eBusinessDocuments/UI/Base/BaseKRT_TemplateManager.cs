
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_TemplateManager BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_TemplateManager
    {
        [System.Xml.Serialization.XmlType("BaseKRT_TemplateManagerBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _KRT_Modul_Id = true;
               public bool KRT_Modul_Id
               {
                   get { return _KRT_Modul_Id; }
                   set { _KRT_Modul_Id = value; }
               }
                                                            
                                 
               private bool _KRT_Dokumentum_Id = true;
               public bool KRT_Dokumentum_Id
               {
                   get { return _KRT_Dokumentum_Id; }
                   set { _KRT_Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Nev = Value;               
                    
                   Tipus = Value;               
                    
                   KRT_Modul_Id = Value;               
                    
                   KRT_Dokumentum_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_TemplateManagerBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _KRT_Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// KRT_Modul_Id Base property </summary>
            public SqlGuid KRT_Modul_Id
            {
                get { return _KRT_Modul_Id; }
                set { _KRT_Modul_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KRT_Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// KRT_Dokumentum_Id Base property </summary>
            public SqlGuid KRT_Dokumentum_Id
            {
                get { return _KRT_Dokumentum_Id; }
                set { _KRT_Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}