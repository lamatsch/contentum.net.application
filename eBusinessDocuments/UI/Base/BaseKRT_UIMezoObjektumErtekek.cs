
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_UIMezoObjektumErtekek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_UIMezoObjektumErtekek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_UIMezoObjektumErtekekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _TemplateTipusNev = true;
               public bool TemplateTipusNev
               {
                   get { return _TemplateTipusNev; }
                   set { _TemplateTipusNev = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Alapertelmezett = true;
               public bool Alapertelmezett
               {
                   get { return _Alapertelmezett; }
                   set { _Alapertelmezett = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _TemplateXML = true;
               public bool TemplateXML
               {
                   get { return _TemplateXML; }
                   set { _TemplateXML = value; }
               }
                                                            
                                 
               private bool _UtolsoHasznIdo = true;
               public bool UtolsoHasznIdo
               {
                   get { return _UtolsoHasznIdo; }
                   set { _UtolsoHasznIdo = value; }
               }
                                                            
                                 
               private bool _Org_Id = true;
               public bool Org_Id
               {
                   get { return _Org_Id; }
                   set { _Org_Id = value; }
               }
                                                            
                                 
               private bool _Publikus = true;
               public bool Publikus
               {
                   get { return _Publikus; }
                   set { _Publikus = value; }
               }
                                                            
                                 
               private bool _Szervezet_Id = true;
               public bool Szervezet_Id
               {
                   get { return _Szervezet_Id; }
                   set { _Szervezet_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   TemplateTipusNev = Value;               
                    
                   Nev = Value;               
                    
                   Alapertelmezett = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   TemplateXML = Value;               
                    
                   UtolsoHasznIdo = Value;               
                    
                   Org_Id = Value;               
                    
                   Publikus = Value;               
                    
                   Szervezet_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_UIMezoObjektumErtekekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _TemplateTipusNev = SqlString.Null;
           
        /// <summary>
        /// TemplateTipusNev Base property </summary>
            public SqlString TemplateTipusNev
            {
                get { return _TemplateTipusNev; }
                set { _TemplateTipusNev = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlChars _Alapertelmezett = SqlChars.Null;
           
        /// <summary>
        /// Alapertelmezett Base property </summary>
            public SqlChars Alapertelmezett
            {
                get { return _Alapertelmezett; }
                set { _Alapertelmezett = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlXml _TemplateXML = SqlXml.Null;
           
        /// <summary>
        /// TemplateXML Base property </summary>
            public SqlXml TemplateXML
            {
                get { return _TemplateXML; }
                set { _TemplateXML = value; }                                                        
            }        
                   
           
        private SqlDateTime _UtolsoHasznIdo = SqlDateTime.Null;
           
        /// <summary>
        /// UtolsoHasznIdo Base property </summary>
            public SqlDateTime UtolsoHasznIdo
            {
                get { return _UtolsoHasznIdo; }
                set { _UtolsoHasznIdo = value; }                                                        
            }        
                   
           
        private SqlGuid _Org_Id = SqlGuid.Null;
           
        /// <summary>
        /// Org_Id Base property </summary>
            public SqlGuid Org_Id
            {
                get { return _Org_Id; }
                set { _Org_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Publikus = SqlChars.Null;
           
        /// <summary>
        /// Publikus Base property </summary>
            public SqlChars Publikus
            {
                get { return _Publikus; }
                set { _Publikus = value; }                                                        
            }        
                   
           
        private SqlGuid _Szervezet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Szervezet_Id Base property </summary>
            public SqlGuid Szervezet_Id
            {
                get { return _Szervezet_Id; }
                set { _Szervezet_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}