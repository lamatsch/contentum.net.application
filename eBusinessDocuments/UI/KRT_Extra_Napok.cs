
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Extra_Napok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Extra_Napok : BaseKRT_Extra_Napok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Datum property </summary>
        public String Datum
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Datum); }
            set { Typed.Datum = Utility.SetSqlDateTimeFromString(value, Typed.Datum); }                                            
        }
                   
           
        /// <summary>
        /// Jelzo property </summary>
        public String Jelzo
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Jelzo); }
            set { Typed.Jelzo = Utility.SetSqlInt32FromString(value, Typed.Jelzo); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}