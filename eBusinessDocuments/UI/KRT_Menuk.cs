
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Menuk BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Menuk : BaseKRT_Menuk
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Menu_Id_Szulo property </summary>
        public String Menu_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Menu_Id_Szulo); }
            set { Typed.Menu_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.Menu_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id property </summary>
        public String Funkcio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id); }
            set { Typed.Funkcio_Id = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id); }                                            
        }
                   
           
        /// <summary>
        /// Modul_Id property </summary>
        public String Modul_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modul_Id); }
            set { Typed.Modul_Id = Utility.SetSqlGuidFromString(value, Typed.Modul_Id); }                                            
        }
                   
           
        /// <summary>
        /// Parameter property </summary>
        public String Parameter
        {
            get { return Utility.GetStringFromSqlString(Typed.Parameter); }
            set { Typed.Parameter = Utility.SetSqlStringFromString(value, Typed.Parameter); }                                            
        }
                   
           
        /// <summary>
        /// ImageURL property </summary>
        public String ImageURL
        {
            get { return Utility.GetStringFromSqlString(Typed.ImageURL); }
            set { Typed.ImageURL = Utility.SetSqlStringFromString(value, Typed.ImageURL); }                                            
        }
                   
           
        /// <summary>
        /// Sorrend property </summary>
        public String Sorrend
        {
            get { return Utility.GetStringFromSqlString(Typed.Sorrend); }
            set { Typed.Sorrend = Utility.SetSqlStringFromString(value, Typed.Sorrend); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}