
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Modulok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Modulok : BaseKRT_Modulok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// SelectString property </summary>
        public String SelectString
        {
            get { return Utility.GetStringFromSqlString(Typed.SelectString); }
            set { Typed.SelectString = Utility.SetSqlStringFromString(value, Typed.SelectString); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// Parameterek property </summary>
        public String Parameterek
        {
            get { return Utility.GetStringFromSqlString(Typed.Parameterek); }
            set { Typed.Parameterek = Utility.SetSqlStringFromString(value, Typed.Parameterek); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_Adatelem property </summary>
        public String ObjTip_Id_Adatelem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_Adatelem); }
            set { Typed.ObjTip_Id_Adatelem = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_Adatelem); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Adatelem property </summary>
        public String ObjTip_Adatelem
        {
            get { return Utility.GetStringFromSqlString(Typed.ObjTip_Adatelem); }
            set { Typed.ObjTip_Adatelem = Utility.SetSqlStringFromString(value, Typed.ObjTip_Adatelem); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}