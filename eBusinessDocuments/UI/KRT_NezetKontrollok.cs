
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_NezetKontrollok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_NezetKontrollok : BaseKRT_NezetKontrollok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Nezet_Id property </summary>
        public String Nezet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Nezet_Id); }
            set { Typed.Nezet_Id = Utility.SetSqlGuidFromString(value, Typed.Nezet_Id); }                                            
        }
                   
           
        /// <summary>
        /// Muvelet_Id property </summary>
        public String Muvelet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Muvelet_Id); }
            set { Typed.Muvelet_Id = Utility.SetSqlGuidFromString(value, Typed.Muvelet_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Feltetel property </summary>
        public String Feltetel
        {
            get { return Utility.GetStringFromSqlString(Typed.Feltetel); }
            set { Typed.Feltetel = Utility.SetSqlStringFromString(value, Typed.Feltetel); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_id_UIElem property </summary>
        public String ObjTipus_id_UIElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_id_UIElem); }
            set { Typed.ObjTipus_id_UIElem = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_id_UIElem); }                                            
        }
                   
           
        /// <summary>
        /// Tiltas property </summary>
        public String Tiltas
        {
            get { return Utility.GetStringFromSqlString(Typed.Tiltas); }
            set { Typed.Tiltas = Utility.SetSqlStringFromString(value, Typed.Tiltas); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_id_Adatelem property </summary>
        public String ObjTipus_id_Adatelem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_id_Adatelem); }
            set { Typed.ObjTipus_id_Adatelem = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_id_Adatelem); }                                            
        }
                   
           
        /// <summary>
        /// ObjStat_id_Adatelem property </summary>
        public String ObjStat_id_Adatelem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjStat_id_Adatelem); }
            set { Typed.ObjStat_id_Adatelem = Utility.SetSqlGuidFromString(value, Typed.ObjStat_id_Adatelem); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}