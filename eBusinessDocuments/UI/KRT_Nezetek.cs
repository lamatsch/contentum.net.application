
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Nezetek BusinessDocument Class
    /// CheckConstraint_fn
    /// 
    /// �J: Muvelet_Id
    /// </summary>
    [Serializable()]
    public class KRT_Nezetek : BaseKRT_Nezetek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// A CSOP_ID_TULAJ mellett ez valojaban mar nem szukseges.
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Form_Id property
        /// Hivatkoz�s a Form
        /// </summary>
        public String Form_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Form_Id); }
            set { Typed.Form_Id = Utility.SetSqlGuidFromString(value, Typed.Form_Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Nezet neve. Celszeru, hogy egyedi legyen, mert ez beszedes, olvashato, az ID nem.
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// Tudnivalok a nezetrol
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property
        /// Minden objektumnak KELL legyen egy ERVENYES tulajdonosa (hogy garantaltan mindig legyen nem elveheto kezeloi jog). Default az indulo Admin, aki nem torolheto.
        /// </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Muvelet_Id property
        /// Muvelet, amit a n�zet v�grehajt
        /// </summary>
        public String Muvelet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Muvelet_Id); }
            set { Typed.Muvelet_Id = Utility.SetSqlGuidFromString(value, Typed.Muvelet_Id); }                                            
        }
                   
           
        /// <summary>
        /// DisableControls property
        /// ??
        /// </summary>
        public String DisableControls
        {
            get { return Utility.GetStringFromSqlString(Typed.DisableControls); }
            set { Typed.DisableControls = Utility.SetSqlStringFromString(value, Typed.DisableControls); }                                            
        }
                   
           
        /// <summary>
        /// InvisibleControls property
        /// Nem l�that� konrolok list�ja (UniqueId-k) vesszovel elv�lasztva
        /// </summary>
        public String InvisibleControls
        {
            get { return Utility.GetStringFromSqlString(Typed.InvisibleControls); }
            set { Typed.InvisibleControls = Utility.SetSqlStringFromString(value, Typed.InvisibleControls); }                                            
        }
                   
           
        /// <summary>
        /// ReadOnlyControls property
        /// ??
        /// </summary>
        public String ReadOnlyControls
        {
            get { return Utility.GetStringFromSqlString(Typed.ReadOnlyControls); }
            set { Typed.ReadOnlyControls = Utility.SetSqlStringFromString(value, Typed.ReadOnlyControls); }                                            
        }
                   
           
        /// <summary>
        /// TabIndexControls property
        /// ??
        /// </summary>
        public String TabIndexControls
        {
            get { return Utility.GetStringFromSqlString(Typed.TabIndexControls); }
            set { Typed.TabIndexControls = Utility.SetSqlStringFromString(value, Typed.TabIndexControls); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property
        /// Sorrendben kell felaj�nlani a legnagyobb priorit�s� n�zetet (azaz azt, mellyel a lehetos�gek k�z�l a legt�bbet lehet v�grehajtani...)
        /// 3., 2007.04.04. Konzult�ci�: A n�zet kiv�laszt�s a csoport jogok alapj�n t�rt�nik, ha t�bb n�zet lesz egy adott ascx laphoz, akkor ezekbol a legnagyobb lehetos�geket biztos�t�t
        ///      v�lasztja a program. (azt, hogy melyik n�zettel lehet a legt�bb funkci�t el�rni, a n�zetben egy "s�ly" attrib�tummal jel�lj�k, e szerint van cs�kkeno sorrendbe rendezve.
        /// </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlInt32FromString(value, Typed.Prioritas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}