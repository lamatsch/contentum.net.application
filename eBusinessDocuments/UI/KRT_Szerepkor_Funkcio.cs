
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Szerepkor_Funkcio BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Szerepkor_Funkcio : BaseKRT_Szerepkor_Funkcio
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id property </summary>
        public String Funkcio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id); }
            set { Typed.Funkcio_Id = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id); }                                            
        }
                   
           
        /// <summary>
        /// Szerepkor_Id property </summary>
        public String Szerepkor_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Szerepkor_Id); }
            set { Typed.Szerepkor_Id = Utility.SetSqlGuidFromString(value, Typed.Szerepkor_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}