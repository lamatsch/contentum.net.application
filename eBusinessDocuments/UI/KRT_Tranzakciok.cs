
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Tranzakciok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Tranzakciok : BaseKRT_Tranzakciok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Alkalmazas_Id property </summary>
        public String Alkalmazas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Alkalmazas_Id); }
            set { Typed.Alkalmazas_Id = Utility.SetSqlGuidFromString(value, Typed.Alkalmazas_Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}