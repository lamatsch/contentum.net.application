
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_UIMezoObjektumErtekek BusinessDocument Class
    /// Ebben a t�bl�ban t�roljuk a felhaszn�l�i fel�leten be�rt mez�k �rt�keit.
    /// Ezen s�m�k, template-ek ment�se az al�bbi helyeken lehet hasznos:
    /// 1., keres� formok �ltal�nosan haszn�lt keres�si felt�tel�nek ment�s�re
    /// 2., Nagy t�meg� adatr�gz�t�sn�l ("t�meges" �rkeztet�s, iktat�s), a default
    ///      �rt�kek ment�s�re.
    /// 3., Bonyolult �rlapok kit�lt�s�n�l, ha olyan hiba van, ami miatt az adott �rlap
    ///      adatait nem lehet menteni, a kit�lt�si �rt�keket ideiglenesen ebbe a t�rol�ba 
    ///      menthejt�k, a sz�ks�ges adat pontos�t�sa ut�n a visszat�lt�s ut�n csak ezt
    ///      kell megadni.
    /// 
    /// A templateTipus valamif�lek�ppen az objektum -ra vonatkozik, de lehet olyan k�perny� is,
    /// amelyik "leveg�ben" l�g: p�ld�ul �sszetett keres� form, mely t�bb objektumb�l keres.
    /// Az ilyen t�pusokat t�bb helyr�l is h�vhatjuk. Azaz, ezt a template -et t�bb k�perny�ben
    /// is felhaszn�lhatjuk... Ha egy adott rekord �llapot�t�l f�gg�en a n�zet tilt bizonyos mez�ket,
    /// ezekbe a visszat�lt�sn�l nem hozzuk vissza az adatokat.
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_UIMezoObjektumErtekek : BaseKRT_UIMezoObjektumErtekek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// TemplateTipusNev property
        /// A template t�pusa (ami sokszor egyezik a t�bl�val), de lehet teljesen egyedi is.
        /// </summary>
        public String TemplateTipusNev
        {
            get { return Utility.GetStringFromSqlString(Typed.TemplateTipusNev); }
            set { Typed.TemplateTipusNev = Utility.SetSqlStringFromString(value, Typed.TemplateTipusNev); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// A template neve
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Alapertelmezett property
        /// Alap�rtemezett template Igen/nem (1/0).
        /// </summary>
        public String Alapertelmezett
        {
            get { return Utility.GetStringFromSqlChars(Typed.Alapertelmezett); }
            set { Typed.Alapertelmezett = Utility.SetSqlCharsFromString(value, Typed.Alapertelmezett); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property
        /// El�g lenne, ha a letrehozo_id lenne benne, de az�rt hagytuk m�gis, mivel �gy lehetnek nem felhaszn�l�hoz k�t�tt,
        /// �ltal�nos haszn�lat� template ment�sek.
        /// </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// TemplateXML property
        /// Ez a mentett "�rlap" xml form�tumban
        /// </summary>
        public String TemplateXML
        {
            get { return Utility.GetStringFromSqlXml(Typed.TemplateXML); }
            set { Typed.TemplateXML = Utility.SetSqlXmlFromString(value, Typed.TemplateXML); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoHasznIdo property
        /// Az objektum utols� bet�lt�si ideje
        /// </summary>
        public String UtolsoHasznIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoHasznIdo); }
            set { Typed.UtolsoHasznIdo = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoHasznIdo); }                                            
        }
                   
           
        /// <summary>
        /// Org_Id property
        /// 
        /// </summary>
        public String Org_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org_Id); }
            set { Typed.Org_Id = Utility.SetSqlGuidFromString(value, Typed.Org_Id); }                                            
        }
                   
           
        /// <summary>
        /// Publikus property
        /// 
        /// </summary>
        public String Publikus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Publikus); }
            set { Typed.Publikus = Utility.SetSqlCharsFromString(value, Typed.Publikus); }                                            
        }
                   
           
        /// <summary>
        /// Szervezet_Id property
        /// Publikus template eset�n a felhaszn�l� szervezete, melyen bel�l publikus lesz a template
        /// </summary>
        public String Szervezet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Szervezet_Id); }
            set { Typed.Szervezet_Id = Utility.SetSqlGuidFromString(value, Typed.Szervezet_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}