
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ACL BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_ACL
    {
        [System.Xml.Serialization.XmlType("BaseKRT_ACLBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Szulo = true;
               public bool Obj_Id_Szulo
               {
                   get { return _Obj_Id_Szulo; }
                   set { _Obj_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_Szulo = true;
               public bool ObjTipus_Id_Szulo
               {
                   get { return _ObjTipus_Id_Szulo; }
                   set { _ObjTipus_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _Szulo_ACL_Id = true;
               public bool Szulo_ACL_Id
               {
                   get { return _Szulo_ACL_Id; }
                   set { _Szulo_ACL_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_ACL_Orokles_Mod = true;
               public bool Csoport_ACL_Orokles_Mod
               {
                   get { return _Csoport_ACL_Orokles_Mod; }
                   set { _Csoport_ACL_Orokles_Mod = value; }
               }
                                                            
                                 
               private bool _Jogtargy_Tabla_Nev = true;
               public bool Jogtargy_Tabla_Nev
               {
                   get { return _Jogtargy_Tabla_Nev; }
                   set { _Jogtargy_Tabla_Nev = value; }
               }
                                                            
                                 
               private bool _Jogtargy_Sor_Id = true;
               public bool Jogtargy_Sor_Id
               {
                   get { return _Jogtargy_Sor_Id; }
                   set { _Jogtargy_Sor_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Nev = Value;               
                    
                   Tipus = Value;               
                    
                   Obj_Id_Szulo = Value;               
                    
                   ObjTipus_Id_Szulo = Value;               
                    
                   Szulo_ACL_Id = Value;               
                    
                   Csoport_ACL_Orokles_Mod = Value;               
                    
                   Jogtargy_Tabla_Nev = Value;               
                    
                   Jogtargy_Sor_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_ACLBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Szulo Base property </summary>
            public SqlGuid Obj_Id_Szulo
            {
                get { return _Obj_Id_Szulo; }
                set { _Obj_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_Szulo Base property </summary>
            public SqlGuid ObjTipus_Id_Szulo
            {
                get { return _ObjTipus_Id_Szulo; }
                set { _ObjTipus_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlGuid _Szulo_ACL_Id = SqlGuid.Null;
           
        /// <summary>
        /// Szulo_ACL_Id Base property </summary>
            public SqlGuid Szulo_ACL_Id
            {
                get { return _Szulo_ACL_Id; }
                set { _Szulo_ACL_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Csoport_ACL_Orokles_Mod = SqlChars.Null;
           
        /// <summary>
        /// Csoport_ACL_Orokles_Mod Base property </summary>
            public SqlChars Csoport_ACL_Orokles_Mod
            {
                get { return _Csoport_ACL_Orokles_Mod; }
                set { _Csoport_ACL_Orokles_Mod = value; }                                                        
            }        
                   
           
        private SqlString _Jogtargy_Tabla_Nev = SqlString.Null;
           
        /// <summary>
        /// Jogtargy_Tabla_Nev Base property </summary>
            public SqlString Jogtargy_Tabla_Nev
            {
                get { return _Jogtargy_Tabla_Nev; }
                set { _Jogtargy_Tabla_Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _Jogtargy_Sor_Id = SqlGuid.Null;
           
        /// <summary>
        /// Jogtargy_Sor_Id Base property </summary>
            public SqlGuid Jogtargy_Sor_Id
            {
                get { return _Jogtargy_Sor_Id; }
                set { _Jogtargy_Sor_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}