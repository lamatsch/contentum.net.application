
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Adatkiegeszitesek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Adatkiegeszitesek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_AdatkiegeszitesekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_Kapcsolt = true;
               public bool ObjTip_Id_Kapcsolt
               {
                   get { return _ObjTip_Id_Kapcsolt; }
                   set { _ObjTip_Id_Kapcsolt = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Kapcsolt = true;
               public bool Obj_Id_Kapcsolt
               {
                   get { return _Obj_Id_Kapcsolt; }
                   set { _Obj_Id_Kapcsolt = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_Adat = true;
               public bool ObjTip_Id_Adat
               {
                   get { return _ObjTip_Id_Adat; }
                   set { _ObjTip_Id_Adat = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Adat = true;
               public bool Obj_Id_Adat
               {
                   get { return _Obj_Id_Adat; }
                   set { _Obj_Id_Adat = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id_Kapcsolt = Value;               
                    
                   Obj_Id_Kapcsolt = Value;               
                    
                   ObjTip_Id_Adat = Value;               
                    
                   Obj_Id_Adat = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_AdatkiegeszitesekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_Kapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_Kapcsolt Base property </summary>
            public SqlGuid ObjTip_Id_Kapcsolt
            {
                get { return _ObjTip_Id_Kapcsolt; }
                set { _ObjTip_Id_Kapcsolt = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Kapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Kapcsolt Base property </summary>
            public SqlGuid Obj_Id_Kapcsolt
            {
                get { return _Obj_Id_Kapcsolt; }
                set { _Obj_Id_Kapcsolt = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_Adat = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_Adat Base property </summary>
            public SqlGuid ObjTip_Id_Adat
            {
                get { return _ObjTip_Id_Adat; }
                set { _ObjTip_Id_Adat = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Adat = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Adat Base property </summary>
            public SqlGuid Obj_Id_Adat
            {
                get { return _Obj_Id_Adat; }
                set { _Obj_Id_Adat = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}