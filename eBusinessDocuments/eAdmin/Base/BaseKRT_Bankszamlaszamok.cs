
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Bankszamlaszamok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Bankszamlaszamok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_BankszamlaszamokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Partner_Id = true;
               public bool Partner_Id
               {
                   get { return _Partner_Id; }
                   set { _Partner_Id = value; }
               }
                                                            
                                 
               private bool _Bankszamlaszam = true;
               public bool Bankszamlaszam
               {
                   get { return _Bankszamlaszam; }
                   set { _Bankszamlaszam = value; }
               }
                                                            
                                 
               private bool _Partner_Id_Bank = true;
               public bool Partner_Id_Bank
               {
                   get { return _Partner_Id_Bank; }
                   set { _Partner_Id_Bank = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Partner_Id = Value;               
                    
                   Bankszamlaszam = Value;               
                    
                   Partner_Id_Bank = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_BankszamlaszamokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id Base property </summary>
            public SqlGuid Partner_Id
            {
                get { return _Partner_Id; }
                set { _Partner_Id = value; }                                                        
            }        
                   
           
        private SqlString _Bankszamlaszam = SqlString.Null;
           
        /// <summary>
        /// Bankszamlaszam Base property </summary>
            public SqlString Bankszamlaszam
            {
                get { return _Bankszamlaszam; }
                set { _Bankszamlaszam = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_Bank = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_Bank Base property </summary>
            public SqlGuid Partner_Id_Bank
            {
                get { return _Partner_Id_Bank; }
                set { _Partner_Id_Bank = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}