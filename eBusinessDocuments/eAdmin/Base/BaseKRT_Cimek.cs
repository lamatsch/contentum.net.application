
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Cimek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Cimek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_CimekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Kategoria = true;
               public bool Kategoria
               {
                   get { return _Kategoria; }
                   set { _Kategoria = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _KulsoAzonositok = true;
               public bool KulsoAzonositok
               {
                   get { return _KulsoAzonositok; }
                   set { _KulsoAzonositok = value; }
               }
                                                            
                                 
               private bool _Forras = true;
               public bool Forras
               {
                   get { return _Forras; }
                   set { _Forras = value; }
               }
                                                            
                                 
               private bool _Orszag_Id = true;
               public bool Orszag_Id
               {
                   get { return _Orszag_Id; }
                   set { _Orszag_Id = value; }
               }
                                                            
                                 
               private bool _OrszagNev = true;
               public bool OrszagNev
               {
                   get { return _OrszagNev; }
                   set { _OrszagNev = value; }
               }
                                                            
                                 
               private bool _Telepules_Id = true;
               public bool Telepules_Id
               {
                   get { return _Telepules_Id; }
                   set { _Telepules_Id = value; }
               }
                                                            
                                 
               private bool _TelepulesNev = true;
               public bool TelepulesNev
               {
                   get { return _TelepulesNev; }
                   set { _TelepulesNev = value; }
               }
                                                            
                                 
               private bool _IRSZ = true;
               public bool IRSZ
               {
                   get { return _IRSZ; }
                   set { _IRSZ = value; }
               }
                                                            
                                 
               private bool _CimTobbi = true;
               public bool CimTobbi
               {
                   get { return _CimTobbi; }
                   set { _CimTobbi = value; }
               }
                                                            
                                 
               private bool _Kozterulet_Id = true;
               public bool Kozterulet_Id
               {
                   get { return _Kozterulet_Id; }
                   set { _Kozterulet_Id = value; }
               }
                                                            
                                 
               private bool _KozteruletNev = true;
               public bool KozteruletNev
               {
                   get { return _KozteruletNev; }
                   set { _KozteruletNev = value; }
               }
                                                            
                                 
               private bool _KozteruletTipus_Id = true;
               public bool KozteruletTipus_Id
               {
                   get { return _KozteruletTipus_Id; }
                   set { _KozteruletTipus_Id = value; }
               }
                                                            
                                 
               private bool _KozteruletTipusNev = true;
               public bool KozteruletTipusNev
               {
                   get { return _KozteruletTipusNev; }
                   set { _KozteruletTipusNev = value; }
               }
                                                            
                                 
               private bool _Hazszam = true;
               public bool Hazszam
               {
                   get { return _Hazszam; }
                   set { _Hazszam = value; }
               }
                                                            
                                 
               private bool _Hazszamig = true;
               public bool Hazszamig
               {
                   get { return _Hazszamig; }
                   set { _Hazszamig = value; }
               }
                                                            
                                 
               private bool _HazszamBetujel = true;
               public bool HazszamBetujel
               {
                   get { return _HazszamBetujel; }
                   set { _HazszamBetujel = value; }
               }
                                                            
                                 
               private bool _MindketOldal = true;
               public bool MindketOldal
               {
                   get { return _MindketOldal; }
                   set { _MindketOldal = value; }
               }
                                                            
                                 
               private bool _HRSZ = true;
               public bool HRSZ
               {
                   get { return _HRSZ; }
                   set { _HRSZ = value; }
               }
                                                            
                                 
               private bool _Lepcsohaz = true;
               public bool Lepcsohaz
               {
                   get { return _Lepcsohaz; }
                   set { _Lepcsohaz = value; }
               }
                                                            
                                 
               private bool _Szint = true;
               public bool Szint
               {
                   get { return _Szint; }
                   set { _Szint = value; }
               }
                                                            
                                 
               private bool _Ajto = true;
               public bool Ajto
               {
                   get { return _Ajto; }
                   set { _Ajto = value; }
               }
                                                            
                                 
               private bool _AjtoBetujel = true;
               public bool AjtoBetujel
               {
                   get { return _AjtoBetujel; }
                   set { _AjtoBetujel = value; }
               }
                                                            
                                 
               private bool _Tobbi = true;
               public bool Tobbi
               {
                   get { return _Tobbi; }
                   set { _Tobbi = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Kategoria = Value;               
                    
                   Tipus = Value;               
                    
                   Nev = Value;               
                    
                   KulsoAzonositok = Value;               
                    
                   Forras = Value;               
                    
                   Orszag_Id = Value;               
                    
                   OrszagNev = Value;               
                    
                   Telepules_Id = Value;               
                    
                   TelepulesNev = Value;               
                    
                   IRSZ = Value;               
                    
                   CimTobbi = Value;               
                    
                   Kozterulet_Id = Value;               
                    
                   KozteruletNev = Value;               
                    
                   KozteruletTipus_Id = Value;               
                    
                   KozteruletTipusNev = Value;               
                    
                   Hazszam = Value;               
                    
                   Hazszamig = Value;               
                    
                   HazszamBetujel = Value;               
                    
                   MindketOldal = Value;               
                    
                   HRSZ = Value;               
                    
                   Lepcsohaz = Value;               
                    
                   Szint = Value;               
                    
                   Ajto = Value;               
                    
                   AjtoBetujel = Value;               
                    
                   Tobbi = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_CimekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlChars _Kategoria = SqlChars.Null;
           
        /// <summary>
        /// Kategoria Base property </summary>
            public SqlChars Kategoria
            {
                get { return _Kategoria; }
                set { _Kategoria = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _KulsoAzonositok = SqlString.Null;
           
        /// <summary>
        /// KulsoAzonositok Base property </summary>
            public SqlString KulsoAzonositok
            {
                get { return _KulsoAzonositok; }
                set { _KulsoAzonositok = value; }                                                        
            }        
                   
           
        private SqlChars _Forras = SqlChars.Null;
           
        /// <summary>
        /// Forras Base property </summary>
            public SqlChars Forras
            {
                get { return _Forras; }
                set { _Forras = value; }                                                        
            }        
                   
           
        private SqlGuid _Orszag_Id = SqlGuid.Null;
           
        /// <summary>
        /// Orszag_Id Base property </summary>
            public SqlGuid Orszag_Id
            {
                get { return _Orszag_Id; }
                set { _Orszag_Id = value; }                                                        
            }        
                   
           
        private SqlString _OrszagNev = SqlString.Null;
           
        /// <summary>
        /// OrszagNev Base property </summary>
            public SqlString OrszagNev
            {
                get { return _OrszagNev; }
                set { _OrszagNev = value; }                                                        
            }        
                   
           
        private SqlGuid _Telepules_Id = SqlGuid.Null;
           
        /// <summary>
        /// Telepules_Id Base property </summary>
            public SqlGuid Telepules_Id
            {
                get { return _Telepules_Id; }
                set { _Telepules_Id = value; }                                                        
            }        
                   
           
        private SqlString _TelepulesNev = SqlString.Null;
           
        /// <summary>
        /// TelepulesNev Base property </summary>
            public SqlString TelepulesNev
            {
                get { return _TelepulesNev; }
                set { _TelepulesNev = value; }                                                        
            }        
                   
           
        private SqlString _IRSZ = SqlString.Null;
           
        /// <summary>
        /// IRSZ Base property </summary>
            public SqlString IRSZ
            {
                get { return _IRSZ; }
                set { _IRSZ = value; }                                                        
            }        
                   
           
        private SqlString _CimTobbi = SqlString.Null;
           
        /// <summary>
        /// CimTobbi Base property </summary>
            public SqlString CimTobbi
            {
                get { return _CimTobbi; }
                set { _CimTobbi = value; }                                                        
            }        
                   
           
        private SqlGuid _Kozterulet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kozterulet_Id Base property </summary>
            public SqlGuid Kozterulet_Id
            {
                get { return _Kozterulet_Id; }
                set { _Kozterulet_Id = value; }                                                        
            }        
                   
           
        private SqlString _KozteruletNev = SqlString.Null;
           
        /// <summary>
        /// KozteruletNev Base property </summary>
            public SqlString KozteruletNev
            {
                get { return _KozteruletNev; }
                set { _KozteruletNev = value; }                                                        
            }        
                   
           
        private SqlGuid _KozteruletTipus_Id = SqlGuid.Null;
           
        /// <summary>
        /// KozteruletTipus_Id Base property </summary>
            public SqlGuid KozteruletTipus_Id
            {
                get { return _KozteruletTipus_Id; }
                set { _KozteruletTipus_Id = value; }                                                        
            }        
                   
           
        private SqlString _KozteruletTipusNev = SqlString.Null;
           
        /// <summary>
        /// KozteruletTipusNev Base property </summary>
            public SqlString KozteruletTipusNev
            {
                get { return _KozteruletTipusNev; }
                set { _KozteruletTipusNev = value; }                                                        
            }        
                   
           
        private SqlString _Hazszam = SqlString.Null;
           
        /// <summary>
        /// Hazszam Base property </summary>
            public SqlString Hazszam
            {
                get { return _Hazszam; }
                set { _Hazszam = value; }                                                        
            }        
                   
           
        private SqlString _Hazszamig = SqlString.Null;
           
        /// <summary>
        /// Hazszamig Base property </summary>
            public SqlString Hazszamig
            {
                get { return _Hazszamig; }
                set { _Hazszamig = value; }                                                        
            }        
                   
           
        private SqlString _HazszamBetujel = SqlString.Null;
           
        /// <summary>
        /// HazszamBetujel Base property </summary>
            public SqlString HazszamBetujel
            {
                get { return _HazszamBetujel; }
                set { _HazszamBetujel = value; }                                                        
            }        
                   
           
        private SqlChars _MindketOldal = SqlChars.Null;
           
        /// <summary>
        /// MindketOldal Base property </summary>
            public SqlChars MindketOldal
            {
                get { return _MindketOldal; }
                set { _MindketOldal = value; }                                                        
            }        
                   
           
        private SqlString _HRSZ = SqlString.Null;
           
        /// <summary>
        /// HRSZ Base property </summary>
            public SqlString HRSZ
            {
                get { return _HRSZ; }
                set { _HRSZ = value; }                                                        
            }        
                   
           
        private SqlString _Lepcsohaz = SqlString.Null;
           
        /// <summary>
        /// Lepcsohaz Base property </summary>
            public SqlString Lepcsohaz
            {
                get { return _Lepcsohaz; }
                set { _Lepcsohaz = value; }                                                        
            }        
                   
           
        private SqlString _Szint = SqlString.Null;
           
        /// <summary>
        /// Szint Base property </summary>
            public SqlString Szint
            {
                get { return _Szint; }
                set { _Szint = value; }                                                        
            }        
                   
           
        private SqlString _Ajto = SqlString.Null;
           
        /// <summary>
        /// Ajto Base property </summary>
            public SqlString Ajto
            {
                get { return _Ajto; }
                set { _Ajto = value; }                                                        
            }        
                   
           
        private SqlString _AjtoBetujel = SqlString.Null;
           
        /// <summary>
        /// AjtoBetujel Base property </summary>
            public SqlString AjtoBetujel
            {
                get { return _AjtoBetujel; }
                set { _AjtoBetujel = value; }                                                        
            }        
                   
           
        private SqlString _Tobbi = SqlString.Null;
           
        /// <summary>
        /// Tobbi Base property </summary>
            public SqlString Tobbi
            {
                get { return _Tobbi; }
                set { _Tobbi = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}