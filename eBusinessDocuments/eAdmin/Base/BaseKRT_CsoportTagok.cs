
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_CsoportTagok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_CsoportTagok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_CsoportTagokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Jogalany = true;
               public bool Csoport_Id_Jogalany
               {
                   get { return _Csoport_Id_Jogalany; }
                   set { _Csoport_Id_Jogalany = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_Jogalany = true;
               public bool ObjTip_Id_Jogalany
               {
                   get { return _ObjTip_Id_Jogalany; }
                   set { _ObjTip_Id_Jogalany = value; }
               }
                                                            
                                 
               private bool _ErtesitesMailCim = true;
               public bool ErtesitesMailCim
               {
                   get { return _ErtesitesMailCim; }
                   set { _ErtesitesMailCim = value; }
               }
                                                            
                                 
               private bool _ErtesitesKell = true;
               public bool ErtesitesKell
               {
                   get { return _ErtesitesKell; }
                   set { _ErtesitesKell = value; }
               }
                                                            
                                 
               private bool _System = true;
               public bool System
               {
                   get { return _System; }
                   set { _System = value; }
               }
                                                            
                                 
               private bool _Orokolheto = true;
               public bool Orokolheto
               {
                   get { return _Orokolheto; }
                   set { _Orokolheto = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Tipus = Value;               
                    
                   Csoport_Id = Value;               
                    
                   Csoport_Id_Jogalany = Value;               
                    
                   ObjTip_Id_Jogalany = Value;               
                    
                   ErtesitesMailCim = Value;               
                    
                   ErtesitesKell = Value;               
                    
                   System = Value;               
                    
                   Orokolheto = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_CsoportTagokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Jogalany = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Jogalany Base property </summary>
            public SqlGuid Csoport_Id_Jogalany
            {
                get { return _Csoport_Id_Jogalany; }
                set { _Csoport_Id_Jogalany = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_Jogalany = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_Jogalany Base property </summary>
            public SqlGuid ObjTip_Id_Jogalany
            {
                get { return _ObjTip_Id_Jogalany; }
                set { _ObjTip_Id_Jogalany = value; }                                                        
            }        
                   
           
        private SqlString _ErtesitesMailCim = SqlString.Null;
           
        /// <summary>
        /// ErtesitesMailCim Base property </summary>
            public SqlString ErtesitesMailCim
            {
                get { return _ErtesitesMailCim; }
                set { _ErtesitesMailCim = value; }                                                        
            }        
                   
           
        private SqlChars _ErtesitesKell = SqlChars.Null;
           
        /// <summary>
        /// ErtesitesKell Base property </summary>
            public SqlChars ErtesitesKell
            {
                get { return _ErtesitesKell; }
                set { _ErtesitesKell = value; }                                                        
            }        
                   
           
        private SqlChars _System = SqlChars.Null;
           
        /// <summary>
        /// System Base property </summary>
            public SqlChars System
            {
                get { return _System; }
                set { _System = value; }                                                        
            }        
                   
           
        private SqlChars _Orokolheto = SqlChars.Null;
           
        /// <summary>
        /// Orokolheto Base property </summary>
            public SqlChars Orokolheto
            {
                get { return _Orokolheto; }
                set { _Orokolheto = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}