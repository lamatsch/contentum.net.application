
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Csoportok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Csoportok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_CsoportokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Jogalany = true;
               public bool Jogalany
               {
                   get { return _Jogalany; }
                   set { _Jogalany = value; }
               }
                                                            
                                 
               private bool _ErtesitesEmail = true;
               public bool ErtesitesEmail
               {
                   get { return _ErtesitesEmail; }
                   set { _ErtesitesEmail = value; }
               }
                                                            
                                 
               private bool _System = true;
               public bool System
               {
                   get { return _System; }
                   set { _System = value; }
               }
                                                            
                                 
               private bool _Adatforras = true;
               public bool Adatforras
               {
                   get { return _Adatforras; }
                   set { _Adatforras = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_Szulo = true;
               public bool ObjTipus_Id_Szulo
               {
                   get { return _ObjTipus_Id_Szulo; }
                   set { _ObjTipus_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _Kiszolgalhato = true;
               public bool Kiszolgalhato
               {
                   get { return _Kiszolgalhato; }
                   set { _Kiszolgalhato = value; }
               }
                                                            
                                 
               private bool _JogosultsagOroklesMod = true;
               public bool JogosultsagOroklesMod
               {
                   get { return _JogosultsagOroklesMod; }
                   set { _JogosultsagOroklesMod = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   Tipus = Value;               
                    
                   Jogalany = Value;               
                    
                   ErtesitesEmail = Value;               
                    
                   System = Value;               
                    
                   Adatforras = Value;               
                    
                   ObjTipus_Id_Szulo = Value;               
                    
                   Kiszolgalhato = Value;               
                    
                   JogosultsagOroklesMod = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_CsoportokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlChars _Jogalany = SqlChars.Null;
           
        /// <summary>
        /// Jogalany Base property </summary>
            public SqlChars Jogalany
            {
                get { return _Jogalany; }
                set { _Jogalany = value; }                                                        
            }        
                   
           
        private SqlString _ErtesitesEmail = SqlString.Null;
           
        /// <summary>
        /// ErtesitesEmail Base property </summary>
            public SqlString ErtesitesEmail
            {
                get { return _ErtesitesEmail; }
                set { _ErtesitesEmail = value; }                                                        
            }        
                   
           
        private SqlChars _System = SqlChars.Null;
           
        /// <summary>
        /// System Base property </summary>
            public SqlChars System
            {
                get { return _System; }
                set { _System = value; }                                                        
            }        
                   
           
        private SqlChars _Adatforras = SqlChars.Null;
           
        /// <summary>
        /// Adatforras Base property </summary>
            public SqlChars Adatforras
            {
                get { return _Adatforras; }
                set { _Adatforras = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_Szulo Base property </summary>
            public SqlGuid ObjTipus_Id_Szulo
            {
                get { return _ObjTipus_Id_Szulo; }
                set { _ObjTipus_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlChars _Kiszolgalhato = SqlChars.Null;
           
        /// <summary>
        /// Kiszolgalhato Base property </summary>
            public SqlChars Kiszolgalhato
            {
                get { return _Kiszolgalhato; }
                set { _Kiszolgalhato = value; }                                                        
            }        
                   
           
        private SqlChars _JogosultsagOroklesMod = SqlChars.Null;
           
        /// <summary>
        /// JogosultsagOroklesMod Base property </summary>
            public SqlChars JogosultsagOroklesMod
            {
                get { return _JogosultsagOroklesMod; }
                set { _JogosultsagOroklesMod = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}