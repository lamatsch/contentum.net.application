
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Felhasznalo_Szerepkor BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Felhasznalo_Szerepkor
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Felhasznalo_SzerepkorBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _CsoportTag_Id = true;
               public bool CsoportTag_Id
               {
                   get { return _CsoportTag_Id; }
                   set { _CsoportTag_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _Szerepkor_Id = true;
               public bool Szerepkor_Id
               {
                   get { return _Szerepkor_Id; }
                   set { _Szerepkor_Id = value; }
               }
                                                            
                                 
               private bool _Helyettesites_Id = true;
               public bool Helyettesites_Id
               {
                   get { return _Helyettesites_Id; }
                   set { _Helyettesites_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   CsoportTag_Id = Value;               
                    
                   Csoport_Id = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   Szerepkor_Id = Value;               
                    
                   Helyettesites_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_Felhasznalo_SzerepkorBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _CsoportTag_Id = SqlGuid.Null;
           
        /// <summary>
        /// CsoportTag_Id Base property </summary>
            public SqlGuid CsoportTag_Id
            {
                get { return _CsoportTag_Id; }
                set { _CsoportTag_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Szerepkor_Id = SqlGuid.Null;
           
        /// <summary>
        /// Szerepkor_Id Base property </summary>
            public SqlGuid Szerepkor_Id
            {
                get { return _Szerepkor_Id; }
                set { _Szerepkor_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Helyettesites_Id = SqlGuid.Null;
           
        /// <summary>
        /// Helyettesites_Id Base property </summary>
            public SqlGuid Helyettesites_Id
            {
                get { return _Helyettesites_Id; }
                set { _Helyettesites_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}