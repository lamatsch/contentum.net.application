
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Felhasznalok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Felhasznalok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_FelhasznalokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Partner_id = true;
               public bool Partner_id
               {
                   get { return _Partner_id; }
                   set { _Partner_id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _UserNev = true;
               public bool UserNev
               {
                   get { return _UserNev; }
                   set { _UserNev = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Jelszo = true;
               public bool Jelszo
               {
                   get { return _Jelszo; }
                   set { _Jelszo = value; }
               }
                                                            
                                 
               private bool _JelszoLejaratIdo = true;
               public bool JelszoLejaratIdo
               {
                   get { return _JelszoLejaratIdo; }
                   set { _JelszoLejaratIdo = value; }
               }
                                                            
                                 
               private bool _System = true;
               public bool System
               {
                   get { return _System; }
                   set { _System = value; }
               }
                                                            
                                 
               private bool _Kiszolgalo = true;
               public bool Kiszolgalo
               {
                   get { return _Kiszolgalo; }
                   set { _Kiszolgalo = value; }
               }
                                                            
                                 
               private bool _DefaultPrivatKulcs = true;
               public bool DefaultPrivatKulcs
               {
                   get { return _DefaultPrivatKulcs; }
                   set { _DefaultPrivatKulcs = value; }
               }
                                                            
                                 
               private bool _Partner_Id_Munkahely = true;
               public bool Partner_Id_Munkahely
               {
                   get { return _Partner_Id_Munkahely; }
                   set { _Partner_Id_Munkahely = value; }
               }
                                                            
                                 
               private bool _MaxMinosites = true;
               public bool MaxMinosites
               {
                   get { return _MaxMinosites; }
                   set { _MaxMinosites = value; }
               }
                                                            
                                 
               private bool _EMail = true;
               public bool EMail
               {
                   get { return _EMail; }
                   set { _EMail = value; }
               }
                                                            
                                 
               private bool _Engedelyezett = true;
               public bool Engedelyezett
               {
                   get { return _Engedelyezett; }
                   set { _Engedelyezett = value; }
               }
                                                            
                                 
               private bool _Telefonszam = true;
               public bool Telefonszam
               {
                   get { return _Telefonszam; }
                   set { _Telefonszam = value; }
               }
                                                            
                                 
               private bool _Beosztas = true;
               public bool Beosztas
               {
                   get { return _Beosztas; }
                   set { _Beosztas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }

            private bool _WrongPassCnt = true;
            public bool WrongPassCnt
            {
                get { return _WrongPassCnt; }
                set { _WrongPassCnt = value; }
            }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Partner_id = Value;               
                    
                   Tipus = Value;               
                    
                   UserNev = Value;               
                    
                   Nev = Value;               
                    
                   Jelszo = Value;               
                    
                   JelszoLejaratIdo = Value;               
                    
                   System = Value;               
                    
                   Kiszolgalo = Value;               
                    
                   DefaultPrivatKulcs = Value;               
                    
                   Partner_Id_Munkahely = Value;               
                    
                   MaxMinosites = Value;               
                    
                   EMail = Value;               
                    
                   Engedelyezett = Value;               
                    
                   Telefonszam = Value;               
                    
                   Beosztas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;

                   WrongPassCnt = Value;
            }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_FelhasznalokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_id Base property </summary>
            public SqlGuid Partner_id
            {
                get { return _Partner_id; }
                set { _Partner_id = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _UserNev = SqlString.Null;
           
        /// <summary>
        /// UserNev Base property </summary>
            public SqlString UserNev
            {
                get { return _UserNev; }
                set { _UserNev = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Jelszo = SqlString.Null;
           
        /// <summary>
        /// Jelszo Base property </summary>
            public SqlString Jelszo
            {
                get { return _Jelszo; }
                set { _Jelszo = value; }                                                        
            }        
                   
           
        private SqlDateTime _JelszoLejaratIdo = SqlDateTime.Null;
           
        /// <summary>
        /// JelszoLejaratIdo Base property </summary>
            public SqlDateTime JelszoLejaratIdo
            {
                get { return _JelszoLejaratIdo; }
                set { _JelszoLejaratIdo = value; }                                                        
            }        
                   
           
        private SqlChars _System = SqlChars.Null;
           
        /// <summary>
        /// System Base property </summary>
            public SqlChars System
            {
                get { return _System; }
                set { _System = value; }                                                        
            }        
                   
           
        private SqlChars _Kiszolgalo = SqlChars.Null;
           
        /// <summary>
        /// Kiszolgalo Base property </summary>
            public SqlChars Kiszolgalo
            {
                get { return _Kiszolgalo; }
                set { _Kiszolgalo = value; }                                                        
            }        
                   
           
        private SqlString _DefaultPrivatKulcs = SqlString.Null;
           
        /// <summary>
        /// DefaultPrivatKulcs Base property </summary>
            public SqlString DefaultPrivatKulcs
            {
                get { return _DefaultPrivatKulcs; }
                set { _DefaultPrivatKulcs = value; }                                                        
            }


            private SqlInt32 _WrongPassCnt = SqlInt32.Null;

            /// <summary>
            /// WronPassCnt Base property </summary>
            public SqlInt32 WrongPassCnt
            {
                get { return _WrongPassCnt; }
                set { _WrongPassCnt = value; }
            }


            private SqlGuid _Partner_Id_Munkahely = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_Munkahely Base property </summary>
            public SqlGuid Partner_Id_Munkahely
            {
                get { return _Partner_Id_Munkahely; }
                set { _Partner_Id_Munkahely = value; }                                                        
            }        
                   
           
        private SqlString _MaxMinosites = SqlString.Null;
           
        /// <summary>
        /// MaxMinosites Base property </summary>
            public SqlString MaxMinosites
            {
                get { return _MaxMinosites; }
                set { _MaxMinosites = value; }                                                        
            }        
                   
           
        private SqlString _EMail = SqlString.Null;
           
        /// <summary>
        /// EMail Base property </summary>
            public SqlString EMail
            {
                get { return _EMail; }
                set { _EMail = value; }                                                        
            }        
                   
           
        private SqlChars _Engedelyezett = SqlChars.Null;
           
        /// <summary>
        /// Engedelyezett Base property </summary>
            public SqlChars Engedelyezett
            {
                get { return _Engedelyezett; }
                set { _Engedelyezett = value; }                                                        
            }        
                   
           
        private SqlString _Telefonszam = SqlString.Null;
           
        /// <summary>
        /// Telefonszam Base property </summary>
            public SqlString Telefonszam
            {
                get { return _Telefonszam; }
                set { _Telefonszam = value; }                                                        
            }        
                   
           
        private SqlString _Beosztas = SqlString.Null;
           
        /// <summary>
        /// Beosztas Base property </summary>
            public SqlString Beosztas
            {
                get { return _Beosztas; }
                set { _Beosztas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}