﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// KRT_Szemelyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Felhasznalok_Halozati_Nyomtatoi
    {
        [System.Xml.Serialization.XmlType("BaseKRT_Felhasznalok_Halozati_NyomtatoiBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Felhasznalo_Id = true;
            public bool Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }
            }


            private bool _Halozati_Nyomtato_Id = true;
            public bool Halozati_Nyomtato_Id
            {
                get { return _Halozati_Nyomtato_Id; }
                set { _Halozati_Nyomtato_Id = value; }
            }



            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Felhasznalo_Id = Value;

                Halozati_Nyomtato_Id = Value;

                ErvKezd = Value;

                ErvVege = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseKRT_HalozatiNyomtatokBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlGuid _Felhasznalo_Id = SqlGuid.Null;

            /// <summary>
            /// Nev Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }
            }


            private SqlGuid _Halozati_Nyomtato_Id = SqlGuid.Null;

            /// <summary>
            /// Cim Base property </summary>
            public SqlGuid Halozati_Nyomtato_Id
            {
                get { return _Halozati_Nyomtato_Id; }
                set { _Halozati_Nyomtato_Id = value; }
            }


            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }
        }
    }
}
