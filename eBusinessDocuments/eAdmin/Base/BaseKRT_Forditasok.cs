
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Forditasok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Forditasok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_ForditasokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _NyelvKod = true;
               public bool NyelvKod
               {
                   get { return _NyelvKod; }
                   set { _NyelvKod = value; }
               }
                                                            
                                 
               private bool _OrgKod = true;
               public bool OrgKod
               {
                   get { return _OrgKod; }
                   set { _OrgKod = value; }
               }
                                                            
                                 
               private bool _Modul = true;
               public bool Modul
               {
                   get { return _Modul; }
                   set { _Modul = value; }
               }
                                                            
                                 
               private bool _Komponens = true;
               public bool Komponens
               {
                   get { return _Komponens; }
                   set { _Komponens = value; }
               }
                                                            
                                 
               private bool _ObjAzonosito = true;
               public bool ObjAzonosito
               {
                   get { return _ObjAzonosito; }
                   set { _ObjAzonosito = value; }
               }
                                                            
                                 
               private bool _Forditas = true;
               public bool Forditas
               {
                   get { return _Forditas; }
                   set { _Forditas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   NyelvKod = Value;               
                    
                   OrgKod = Value;               
                    
                   Modul = Value;               
                    
                   Komponens = Value;               
                    
                   ObjAzonosito = Value;               
                    
                   Forditas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_ForditasokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _NyelvKod = SqlString.Null;
           
        /// <summary>
        /// NyelvKod Base property </summary>
            public SqlString NyelvKod
            {
                get { return _NyelvKod; }
                set { _NyelvKod = value; }                                                        
            }        
                   
           
        private SqlString _OrgKod = SqlString.Null;
           
        /// <summary>
        /// OrgKod Base property </summary>
            public SqlString OrgKod
            {
                get { return _OrgKod; }
                set { _OrgKod = value; }                                                        
            }        
                   
           
        private SqlString _Modul = SqlString.Null;
           
        /// <summary>
        /// Modul Base property </summary>
            public SqlString Modul
            {
                get { return _Modul; }
                set { _Modul = value; }                                                        
            }        
                   
           
        private SqlString _Komponens = SqlString.Null;
           
        /// <summary>
        /// Komponens Base property </summary>
            public SqlString Komponens
            {
                get { return _Komponens; }
                set { _Komponens = value; }                                                        
            }        
                   
           
        private SqlString _ObjAzonosito = SqlString.Null;
           
        /// <summary>
        /// ObjAzonosito Base property </summary>
            public SqlString ObjAzonosito
            {
                get { return _ObjAzonosito; }
                set { _ObjAzonosito = value; }                                                        
            }        
                   
           
        private SqlString _Forditas = SqlString.Null;
           
        /// <summary>
        /// Forditas Base property </summary>
            public SqlString Forditas
            {
                get { return _Forditas; }
                set { _Forditas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}