
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Funkciok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Funkciok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_FunkciokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_AdatElem = true;
               public bool ObjTipus_Id_AdatElem
               {
                   get { return _ObjTipus_Id_AdatElem; }
                   set { _ObjTipus_Id_AdatElem = value; }
               }
                                                            
                                 
               private bool _ObjStat_Id_Kezd = true;
               public bool ObjStat_Id_Kezd
               {
                   get { return _ObjStat_Id_Kezd; }
                   set { _ObjStat_Id_Kezd = value; }
               }
                                                            
                                 
               private bool _Alkalmazas_Id = true;
               public bool Alkalmazas_Id
               {
                   get { return _Alkalmazas_Id; }
                   set { _Alkalmazas_Id = value; }
               }
                                                            
                                 
               private bool _Muvelet_Id = true;
               public bool Muvelet_Id
               {
                   get { return _Muvelet_Id; }
                   set { _Muvelet_Id = value; }
               }
                                                            
                                 
               private bool _ObjStat_Id_Veg = true;
               public bool ObjStat_Id_Veg
               {
                   get { return _ObjStat_Id_Veg; }
                   set { _ObjStat_Id_Veg = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Szulo = true;
               public bool Funkcio_Id_Szulo
               {
                   get { return _Funkcio_Id_Szulo; }
                   set { _Funkcio_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _Csoportosito = true;
               public bool Csoportosito
               {
                   get { return _Csoportosito; }
                   set { _Csoportosito = value; }
               }
                                                            
                                 
               private bool _Modosithato = true;
               public bool Modosithato
               {
                   get { return _Modosithato; }
                   set { _Modosithato = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _MunkanaploJelzo = true;
               public bool MunkanaploJelzo
               {
                   get { return _MunkanaploJelzo; }
                   set { _MunkanaploJelzo = value; }
               }
                                                            
                                 
               private bool _FeladatJelzo = true;
               public bool FeladatJelzo
               {
                   get { return _FeladatJelzo; }
                   set { _FeladatJelzo = value; }
               }
                                                            
                                 
               private bool _KeziFeladatJelzo = true;
               public bool KeziFeladatJelzo
               {
                   get { return _KeziFeladatJelzo; }
                   set { _KeziFeladatJelzo = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   ObjTipus_Id_AdatElem = Value;               
                    
                   ObjStat_Id_Kezd = Value;               
                    
                   Alkalmazas_Id = Value;               
                    
                   Muvelet_Id = Value;               
                    
                   ObjStat_Id_Veg = Value;               
                    
                   Leiras = Value;               
                    
                   Funkcio_Id_Szulo = Value;               
                    
                   Csoportosito = Value;               
                    
                   Modosithato = Value;               
                    
                   Org = Value;               
                    
                   MunkanaploJelzo = Value;               
                    
                   FeladatJelzo = Value;               
                    
                   KeziFeladatJelzo = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_FunkciokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_AdatElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_AdatElem Base property </summary>
            public SqlGuid ObjTipus_Id_AdatElem
            {
                get { return _ObjTipus_Id_AdatElem; }
                set { _ObjTipus_Id_AdatElem = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjStat_Id_Kezd = SqlGuid.Null;
           
        /// <summary>
        /// ObjStat_Id_Kezd Base property </summary>
            public SqlGuid ObjStat_Id_Kezd
            {
                get { return _ObjStat_Id_Kezd; }
                set { _ObjStat_Id_Kezd = value; }                                                        
            }        
                   
           
        private SqlGuid _Alkalmazas_Id = SqlGuid.Null;
           
        /// <summary>
        /// Alkalmazas_Id Base property </summary>
            public SqlGuid Alkalmazas_Id
            {
                get { return _Alkalmazas_Id; }
                set { _Alkalmazas_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Muvelet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Muvelet_Id Base property </summary>
            public SqlGuid Muvelet_Id
            {
                get { return _Muvelet_Id; }
                set { _Muvelet_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjStat_Id_Veg = SqlGuid.Null;
           
        /// <summary>
        /// ObjStat_Id_Veg Base property </summary>
            public SqlGuid ObjStat_Id_Veg
            {
                get { return _ObjStat_Id_Veg; }
                set { _ObjStat_Id_Veg = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Szulo Base property </summary>
            public SqlGuid Funkcio_Id_Szulo
            {
                get { return _Funkcio_Id_Szulo; }
                set { _Funkcio_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlChars _Csoportosito = SqlChars.Null;
           
        /// <summary>
        /// Csoportosito Base property </summary>
            public SqlChars Csoportosito
            {
                get { return _Csoportosito; }
                set { _Csoportosito = value; }                                                        
            }        
                   
           
        private SqlChars _Modosithato = SqlChars.Null;
           
        /// <summary>
        /// Modosithato Base property </summary>
            public SqlChars Modosithato
            {
                get { return _Modosithato; }
                set { _Modosithato = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlChars _MunkanaploJelzo = SqlChars.Null;
           
        /// <summary>
        /// MunkanaploJelzo Base property </summary>
            public SqlChars MunkanaploJelzo
            {
                get { return _MunkanaploJelzo; }
                set { _MunkanaploJelzo = value; }                                                        
            }        
                   
           
        private SqlChars _FeladatJelzo = SqlChars.Null;
           
        /// <summary>
        /// FeladatJelzo Base property </summary>
            public SqlChars FeladatJelzo
            {
                get { return _FeladatJelzo; }
                set { _FeladatJelzo = value; }                                                        
            }        
                   
           
        private SqlChars _KeziFeladatJelzo = SqlChars.Null;
           
        /// <summary>
        /// KeziFeladatJelzo Base property </summary>
            public SqlChars KeziFeladatJelzo
            {
                get { return _KeziFeladatJelzo; }
                set { _KeziFeladatJelzo = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}