﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// KRT_HalozatiNyomtatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_HalozatiNyomtatok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_HalozatiNyomtatokBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Nev = true;
            public bool Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private bool _Cim = true;
            public bool Cim
            {
                get { return _Cim; }
                set { _Cim = value; }
            }



            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Nev = Value;

                Cim = Value;

                ErvKezd = Value;

                ErvVege = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseKRT_HalozatiNyomtatokBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlString _Nev = SqlString.Null;

            /// <summary>
            /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private SqlString _Cim = SqlString.Null;

            /// <summary>
            /// Cim Base property </summary>
            public SqlString Cim
            {
                get { return _Cim; }
                set { _Cim = value; }
            }


            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }
        }
    }
}
