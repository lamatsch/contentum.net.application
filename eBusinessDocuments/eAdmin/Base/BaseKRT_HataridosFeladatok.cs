
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_HataridosFeladatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_HataridosFeladatok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_HataridosFeladatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_id = true;
               public bool Felhasznalo_id
               {
                   get { return _Felhasznalo_id; }
                   set { _Felhasznalo_id = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }
                                                            
                                 
               private bool _Feladat = true;
               public bool Feladat
               {
                   get { return _Feladat; }
                   set { _Feladat = value; }
               }
                                                            
                                 
               private bool _ReagalasiIdo = true;
               public bool ReagalasiIdo
               {
                   get { return _ReagalasiIdo; }
                   set { _ReagalasiIdo = value; }
               }
                                                            
                                 
               private bool _Scontro = true;
               public bool Scontro
               {
                   get { return _Scontro; }
                   set { _Scontro = value; }
               }
                                                            
                                 
               private bool _Hatarido = true;
               public bool Hatarido
               {
                   get { return _Hatarido; }
                   set { _Hatarido = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Felhasznalo_id = Value;               
                    
                   Kod = Value;               
                    
                   Prioritas = Value;               
                    
                   Feladat = Value;               
                    
                   ReagalasiIdo = Value;               
                    
                   Scontro = Value;               
                    
                   Hatarido = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_HataridosFeladatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_id Base property </summary>
            public SqlGuid Felhasznalo_id
            {
                get { return _Felhasznalo_id; }
                set { _Felhasznalo_id = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlInt32 _Prioritas = SqlInt32.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlInt32 Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }        
                   
           
        private SqlString _Feladat = SqlString.Null;
           
        /// <summary>
        /// Feladat Base property </summary>
            public SqlString Feladat
            {
                get { return _Feladat; }
                set { _Feladat = value; }                                                        
            }        
                   
           
        private SqlDateTime _ReagalasiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// ReagalasiIdo Base property </summary>
            public SqlDateTime ReagalasiIdo
            {
                get { return _ReagalasiIdo; }
                set { _ReagalasiIdo = value; }                                                        
            }        
                   
           
        private SqlDateTime _Scontro = SqlDateTime.Null;
           
        /// <summary>
        /// Scontro Base property </summary>
            public SqlDateTime Scontro
            {
                get { return _Scontro; }
                set { _Scontro = value; }                                                        
            }        
                   
           
        private SqlDateTime _Hatarido = SqlDateTime.Null;
           
        /// <summary>
        /// Hatarido Base property </summary>
            public SqlDateTime Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}