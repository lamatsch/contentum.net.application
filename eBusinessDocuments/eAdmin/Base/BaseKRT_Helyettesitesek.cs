
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Helyettesitesek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Helyettesitesek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_HelyettesitesekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_ID_helyettesitett = true;
               public bool Felhasznalo_ID_helyettesitett
               {
                   get { return _Felhasznalo_ID_helyettesitett; }
                   set { _Felhasznalo_ID_helyettesitett = value; }
               }
                                                            
                                 
               private bool _CsoportTag_ID_helyettesitett = true;
               public bool CsoportTag_ID_helyettesitett
               {
                   get { return _CsoportTag_ID_helyettesitett; }
                   set { _CsoportTag_ID_helyettesitett = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_ID_helyettesito = true;
               public bool Felhasznalo_ID_helyettesito
               {
                   get { return _Felhasznalo_ID_helyettesito; }
                   set { _Felhasznalo_ID_helyettesito = value; }
               }
                                                            
                                 
               private bool _HelyettesitesMod = true;
               public bool HelyettesitesMod
               {
                   get { return _HelyettesitesMod; }
                   set { _HelyettesitesMod = value; }
               }
                                                            
                                 
               private bool _HelyettesitesKezd = true;
               public bool HelyettesitesKezd
               {
                   get { return _HelyettesitesKezd; }
                   set { _HelyettesitesKezd = value; }
               }
                                                            
                                 
               private bool _HelyettesitesVege = true;
               public bool HelyettesitesVege
               {
                   get { return _HelyettesitesVege; }
                   set { _HelyettesitesVege = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Felhasznalo_ID_helyettesitett = Value;               
                    
                   CsoportTag_ID_helyettesitett = Value;               
                    
                   Felhasznalo_ID_helyettesito = Value;               
                    
                   HelyettesitesMod = Value;               
                    
                   HelyettesitesKezd = Value;               
                    
                   HelyettesitesVege = Value;               
                    
                   Megjegyzes = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_HelyettesitesekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_ID_helyettesitett = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_ID_helyettesitett Base property </summary>
            public SqlGuid Felhasznalo_ID_helyettesitett
            {
                get { return _Felhasznalo_ID_helyettesitett; }
                set { _Felhasznalo_ID_helyettesitett = value; }                                                        
            }        
                   
           
        private SqlGuid _CsoportTag_ID_helyettesitett = SqlGuid.Null;
           
        /// <summary>
        /// CsoportTag_ID_helyettesitett Base property </summary>
            public SqlGuid CsoportTag_ID_helyettesitett
            {
                get { return _CsoportTag_ID_helyettesitett; }
                set { _CsoportTag_ID_helyettesitett = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_ID_helyettesito = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_ID_helyettesito Base property </summary>
            public SqlGuid Felhasznalo_ID_helyettesito
            {
                get { return _Felhasznalo_ID_helyettesito; }
                set { _Felhasznalo_ID_helyettesito = value; }                                                        
            }        
                   
           
        private SqlString _HelyettesitesMod = SqlString.Null;
           
        /// <summary>
        /// HelyettesitesMod Base property </summary>
            public SqlString HelyettesitesMod
            {
                get { return _HelyettesitesMod; }
                set { _HelyettesitesMod = value; }                                                        
            }        
                   
           
        private SqlDateTime _HelyettesitesKezd = SqlDateTime.Null;
           
        /// <summary>
        /// HelyettesitesKezd Base property </summary>
            public SqlDateTime HelyettesitesKezd
            {
                get { return _HelyettesitesKezd; }
                set { _HelyettesitesKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _HelyettesitesVege = SqlDateTime.Null;
           
        /// <summary>
        /// HelyettesitesVege Base property </summary>
            public SqlDateTime HelyettesitesVege
            {
                get { return _HelyettesitesVege; }
                set { _HelyettesitesVege = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}