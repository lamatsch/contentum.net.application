
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Jogosultak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Jogosultak
    {
        [System.Xml.Serialization.XmlType("BaseKRT_JogosultakBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Jogalany = true;
               public bool Csoport_Id_Jogalany
               {
                   get { return _Csoport_Id_Jogalany; }
                   set { _Csoport_Id_Jogalany = value; }
               }
                                                            
                                 
               private bool _Jogszint = true;
               public bool Jogszint
               {
                   get { return _Jogszint; }
                   set { _Jogszint = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _Orokolheto = true;
               public bool Orokolheto
               {
                   get { return _Orokolheto; }
                   set { _Orokolheto = value; }
               }
                                                            
                                 
               private bool _Kezi = true;
               public bool Kezi
               {
                   get { return _Kezi; }
                   set { _Kezi = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Csoport_Id_Jogalany = Value;               
                    
                   Jogszint = Value;               
                    
                   Obj_Id = Value;               
                    
                   Orokolheto = Value;               
                    
                   Kezi = Value;               
                    
                   Tipus = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_JogosultakBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Jogalany = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Jogalany Base property </summary>
            public SqlGuid Csoport_Id_Jogalany
            {
                get { return _Csoport_Id_Jogalany; }
                set { _Csoport_Id_Jogalany = value; }                                                        
            }        
                   
           
        private SqlString _Jogszint = SqlString.Null;
           
        /// <summary>
        /// Jogszint Base property </summary>
            public SqlString Jogszint
            {
                get { return _Jogszint; }
                set { _Jogszint = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Orokolheto = SqlChars.Null;
           
        /// <summary>
        /// Orokolheto Base property </summary>
            public SqlChars Orokolheto
            {
                get { return _Orokolheto; }
                set { _Orokolheto = value; }                                                        
            }        
                   
           
        private SqlChars _Kezi = SqlChars.Null;
           
        /// <summary>
        /// Kezi Base property </summary>
            public SqlChars Kezi
            {
                get { return _Kezi; }
                set { _Kezi = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}