
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodCsoportok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_KodCsoportok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_KodCsoportokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _KodTarak_Id_KodcsoportTipus = true;
               public bool KodTarak_Id_KodcsoportTipus
               {
                   get { return _KodTarak_Id_KodcsoportTipus; }
                   set { _KodTarak_Id_KodcsoportTipus = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Modosithato = true;
               public bool Modosithato
               {
                   get { return _Modosithato; }
                   set { _Modosithato = value; }
               }
                                                            
                                 
               private bool _Hossz = true;
               public bool Hossz
               {
                   get { return _Hossz; }
                   set { _Hossz = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _BesorolasiSema = true;
               public bool BesorolasiSema
               {
                   get { return _BesorolasiSema; }
                   set { _BesorolasiSema = value; }
               }
                                                            
                                 
               private bool _KiegAdat = true;
               public bool KiegAdat
               {
                   get { return _KiegAdat; }
                   set { _KiegAdat = value; }
               }
                                                            
                                 
               private bool _KiegMezo = true;
               public bool KiegMezo
               {
                   get { return _KiegMezo; }
                   set { _KiegMezo = value; }
               }
                                                            
                                 
               private bool _KiegAdattipus = true;
               public bool KiegAdattipus
               {
                   get { return _KiegAdattipus; }
                   set { _KiegAdattipus = value; }
               }
                                                            
                                 
               private bool _KiegSzotar = true;
               public bool KiegSzotar
               {
                   get { return _KiegSzotar; }
                   set { _KiegSzotar = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   KodTarak_Id_KodcsoportTipus = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   Modosithato = Value;               
                    
                   Hossz = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   Leiras = Value;               
                    
                   BesorolasiSema = Value;               
                    
                   KiegAdat = Value;               
                    
                   KiegMezo = Value;               
                    
                   KiegAdattipus = Value;               
                    
                   KiegSzotar = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_KodCsoportokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _KodTarak_Id_KodcsoportTipus = SqlGuid.Null;
           
        /// <summary>
        /// KodTarak_Id_KodcsoportTipus Base property </summary>
            public SqlGuid KodTarak_Id_KodcsoportTipus
            {
                get { return _KodTarak_Id_KodcsoportTipus; }
                set { _KodTarak_Id_KodcsoportTipus = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlChars _Modosithato = SqlChars.Null;
           
        /// <summary>
        /// Modosithato Base property </summary>
            public SqlChars Modosithato
            {
                get { return _Modosithato; }
                set { _Modosithato = value; }                                                        
            }        
                   
           
        private SqlInt32 _Hossz = SqlInt32.Null;
           
        /// <summary>
        /// Hossz Base property </summary>
            public SqlInt32 Hossz
            {
                get { return _Hossz; }
                set { _Hossz = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlChars _BesorolasiSema = SqlChars.Null;
           
        /// <summary>
        /// BesorolasiSema Base property </summary>
            public SqlChars BesorolasiSema
            {
                get { return _BesorolasiSema; }
                set { _BesorolasiSema = value; }                                                        
            }        
                   
           
        private SqlChars _KiegAdat = SqlChars.Null;
           
        /// <summary>
        /// KiegAdat Base property </summary>
            public SqlChars KiegAdat
            {
                get { return _KiegAdat; }
                set { _KiegAdat = value; }                                                        
            }        
                   
           
        private SqlString _KiegMezo = SqlString.Null;
           
        /// <summary>
        /// KiegMezo Base property </summary>
            public SqlString KiegMezo
            {
                get { return _KiegMezo; }
                set { _KiegMezo = value; }                                                        
            }        
                   
           
        private SqlChars _KiegAdattipus = SqlChars.Null;
           
        /// <summary>
        /// KiegAdattipus Base property </summary>
            public SqlChars KiegAdattipus
            {
                get { return _KiegAdattipus; }
                set { _KiegAdattipus = value; }                                                        
            }        
                   
           
        private SqlString _KiegSzotar = SqlString.Null;
           
        /// <summary>
        /// KiegSzotar Base property </summary>
            public SqlString KiegSzotar
            {
                get { return _KiegSzotar; }
                set { _KiegSzotar = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}