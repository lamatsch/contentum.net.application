
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodtarFuggoseg BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_KodtarFuggoseg
    {
        [System.Xml.Serialization.XmlType("BaseKRT_KodtarFuggosegBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Vezerlo_KodCsoport_Id = true;
               public bool Vezerlo_KodCsoport_Id
               {
                   get { return _Vezerlo_KodCsoport_Id; }
                   set { _Vezerlo_KodCsoport_Id = value; }
               }
                                                            
                                 
               private bool _Fuggo_KodCsoport_Id = true;
               public bool Fuggo_KodCsoport_Id
               {
                   get { return _Fuggo_KodCsoport_Id; }
                   set { _Fuggo_KodCsoport_Id = value; }
               }
                                                            
                                 
               private bool _Adat = true;
               public bool Adat
               {
                   get { return _Adat; }
                   set { _Adat = value; }
               }
                                                            
                                 
               private bool _Aktiv = true;
               public bool Aktiv
               {
                   get { return _Aktiv; }
                   set { _Aktiv = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Vezerlo_KodCsoport_Id = Value;               
                    
                   Fuggo_KodCsoport_Id = Value;               
                    
                   Adat = Value;               
                    
                   Aktiv = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_KodtarFuggosegBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Vezerlo_KodCsoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Vezerlo_KodCsoport_Id Base property </summary>
            public SqlGuid Vezerlo_KodCsoport_Id
            {
                get { return _Vezerlo_KodCsoport_Id; }
                set { _Vezerlo_KodCsoport_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Fuggo_KodCsoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Fuggo_KodCsoport_Id Base property </summary>
            public SqlGuid Fuggo_KodCsoport_Id
            {
                get { return _Fuggo_KodCsoport_Id; }
                set { _Fuggo_KodCsoport_Id = value; }                                                        
            }        
                   
           
        private SqlString _Adat = SqlString.Null;
           
        /// <summary>
        /// Adat Base property </summary>
            public SqlString Adat
            {
                get { return _Adat; }
                set { _Adat = value; }                                                        
            }        
                   
           
        private SqlChars _Aktiv = SqlChars.Null;
           
        /// <summary>
        /// Aktiv Base property </summary>
            public SqlChars Aktiv
            {
                get { return _Aktiv; }
                set { _Aktiv = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}