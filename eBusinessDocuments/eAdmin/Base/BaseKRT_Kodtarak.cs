
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodTarak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_KodTarak
    {
        [System.Xml.Serialization.XmlType("BaseKRT_KodTarakBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _KodCsoport_Id = true;
               public bool KodCsoport_Id
               {
                   get { return _KodCsoport_Id; }
                   set { _KodCsoport_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_AdatElem = true;
               public bool ObjTip_Id_AdatElem
               {
                   get { return _ObjTip_Id_AdatElem; }
                   set { _ObjTip_Id_AdatElem = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _RovidNev = true;
               public bool RovidNev
               {
                   get { return _RovidNev; }
                   set { _RovidNev = value; }
               }
                                                            
                                 
               private bool _Egyeb = true;
               public bool Egyeb
               {
                   get { return _Egyeb; }
                   set { _Egyeb = value; }
               }
                                                            
                                 
               private bool _Modosithato = true;
               public bool Modosithato
               {
                   get { return _Modosithato; }
                   set { _Modosithato = value; }
               }
                                                            
                                 
               private bool _Sorrend = true;
               public bool Sorrend
               {
                   get { return _Sorrend; }
                   set { _Sorrend = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   KodCsoport_Id = Value;               
                    
                   ObjTip_Id_AdatElem = Value;               
                    
                   Obj_Id = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   RovidNev = Value;               
                    
                   Egyeb = Value;               
                    
                   Modosithato = Value;               
                    
                   Sorrend = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_KodTarakBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _KodCsoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// KodCsoport_Id Base property </summary>
            public SqlGuid KodCsoport_Id
            {
                get { return _KodCsoport_Id; }
                set { _KodCsoport_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_AdatElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_AdatElem Base property </summary>
            public SqlGuid ObjTip_Id_AdatElem
            {
                get { return _ObjTip_Id_AdatElem; }
                set { _ObjTip_Id_AdatElem = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _RovidNev = SqlString.Null;
           
        /// <summary>
        /// RovidNev Base property </summary>
            public SqlString RovidNev
            {
                get { return _RovidNev; }
                set { _RovidNev = value; }                                                        
            }        
                   
           
        private SqlString _Egyeb = SqlString.Null;
           
        /// <summary>
        /// Egyeb Base property </summary>
            public SqlString Egyeb
            {
                get { return _Egyeb; }
                set { _Egyeb = value; }                                                        
            }        
                   
           
        private SqlChars _Modosithato = SqlChars.Null;
           
        /// <summary>
        /// Modosithato Base property </summary>
            public SqlChars Modosithato
            {
                get { return _Modosithato; }
                set { _Modosithato = value; }                                                        
            }        
                   
           
        private SqlString _Sorrend = SqlString.Null;
           
        /// <summary>
        /// Sorrend Base property </summary>
            public SqlString Sorrend
            {
                get { return _Sorrend; }
                set { _Sorrend = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}