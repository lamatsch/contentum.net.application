
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_OBJEKTUMJOG_OROKLESEK BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_OBJEKTUMJOG_OROKLESEK
    {
        [System.Xml.Serialization.XmlType("BaseKRT_OBJEKTUMJOG_OROKLESEKBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Tipus = true;
               public bool Obj_Tipus
               {
                   get { return _Obj_Tipus; }
                   set { _Obj_Tipus = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Orokos = true;
               public bool Obj_Id_Orokos
               {
                   get { return _Obj_Id_Orokos; }
                   set { _Obj_Id_Orokos = value; }
               }
                                                            
                                 
               private bool _Obj_Tipus_Orokos = true;
               public bool Obj_Tipus_Orokos
               {
                   get { return _Obj_Tipus_Orokos; }
                   set { _Obj_Tipus_Orokos = value; }
               }
                                                            
                                 
               private bool _JogszintOrokolheto = true;
               public bool JogszintOrokolheto
               {
                   get { return _JogszintOrokolheto; }
                   set { _JogszintOrokolheto = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   Obj_Tipus = Value;               
                    
                   Obj_Id_Orokos = Value;               
                    
                   Obj_Tipus_Orokos = Value;               
                    
                   JogszintOrokolheto = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_OBJEKTUMJOG_OROKLESEKBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Tipus = SqlString.Null;
           
        /// <summary>
        /// Obj_Tipus Base property </summary>
            public SqlString Obj_Tipus
            {
                get { return _Obj_Tipus; }
                set { _Obj_Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Orokos = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Orokos Base property </summary>
            public SqlGuid Obj_Id_Orokos
            {
                get { return _Obj_Id_Orokos; }
                set { _Obj_Id_Orokos = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Tipus_Orokos = SqlString.Null;
           
        /// <summary>
        /// Obj_Tipus_Orokos Base property </summary>
            public SqlString Obj_Tipus_Orokos
            {
                get { return _Obj_Tipus_Orokos; }
                set { _Obj_Tipus_Orokos = value; }                                                        
            }        
                   
           
        private SqlChars _JogszintOrokolheto = SqlChars.Null;
           
        /// <summary>
        /// JogszintOrokolheto Base property </summary>
            public SqlChars JogszintOrokolheto
            {
                get { return _JogszintOrokolheto; }
                set { _JogszintOrokolheto = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}