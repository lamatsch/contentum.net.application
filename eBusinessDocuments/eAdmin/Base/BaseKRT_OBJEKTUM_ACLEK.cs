
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_OBJEKTUM_ACLEK BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_OBJEKTUM_ACLEK
    {
        [System.Xml.Serialization.XmlType("BaseKRT_OBJEKTUM_ACLEKBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Obj_Tipus = true;
               public bool Obj_Tipus
               {
                   get { return _Obj_Tipus; }
                   set { _Obj_Tipus = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ACL_ID = true;
               public bool ACL_ID
               {
                   get { return _ACL_ID; }
                   set { _ACL_ID = value; }
               }
                                                            
                                 
               private bool _Jogtargy_Sor_Id = true;
               public bool Jogtargy_Sor_Id
               {
                   get { return _Jogtargy_Sor_Id; }
                   set { _Jogtargy_Sor_Id = value; }
               }
                                                            
                                 
               private bool _OrokolhetoJogszint = true;
               public bool OrokolhetoJogszint
               {
                   get { return _OrokolhetoJogszint; }
                   set { _OrokolhetoJogszint = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Obj_Tipus = Value;               
                    
                   Obj_Id = Value;               
                    
                   ACL_ID = Value;               
                    
                   Jogtargy_Sor_Id = Value;               
                    
                   OrokolhetoJogszint = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_OBJEKTUM_ACLEKBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Tipus = SqlString.Null;
           
        /// <summary>
        /// Obj_Tipus Base property </summary>
            public SqlString Obj_Tipus
            {
                get { return _Obj_Tipus; }
                set { _Obj_Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ACL_ID = SqlGuid.Null;
           
        /// <summary>
        /// ACL_ID Base property </summary>
            public SqlGuid ACL_ID
            {
                get { return _ACL_ID; }
                set { _ACL_ID = value; }                                                        
            }        
                   
           
        private SqlChars _Jogtargy_Sor_Id = SqlChars.Null;
           
        /// <summary>
        /// Jogtargy_Sor_Id Base property </summary>
            public SqlChars Jogtargy_Sor_Id
            {
                get { return _Jogtargy_Sor_Id; }
                set { _Jogtargy_Sor_Id = value; }                                                        
            }        
                   
           
        private SqlChars _OrokolhetoJogszint = SqlChars.Null;
           
        /// <summary>
        /// OrokolhetoJogszint Base property </summary>
            public SqlChars OrokolhetoJogszint
            {
                get { return _OrokolhetoJogszint; }
                set { _OrokolhetoJogszint = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}