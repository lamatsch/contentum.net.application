
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ObjTipusok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_ObjTipusok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_ObjTipusokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ObjTipus_Id_Tipus = true;
               public bool ObjTipus_Id_Tipus
               {
                   get { return _ObjTipus_Id_Tipus; }
                   set { _ObjTipus_Id_Tipus = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Szulo = true;
               public bool Obj_Id_Szulo
               {
                   get { return _Obj_Id_Szulo; }
                   set { _Obj_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _KodCsoport_Id = true;
               public bool KodCsoport_Id
               {
                   get { return _KodCsoport_Id; }
                   set { _KodCsoport_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ObjTipus_Id_Tipus = Value;               
                    
                   Kod = Value;               
                    
                   Nev = Value;               
                    
                   Obj_Id_Szulo = Value;               
                    
                   KodCsoport_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_ObjTipusokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTipus_Id_Tipus = SqlGuid.Null;
           
        /// <summary>
        /// ObjTipus_Id_Tipus Base property </summary>
            public SqlGuid ObjTipus_Id_Tipus
            {
                get { return _ObjTipus_Id_Tipus; }
                set { _ObjTipus_Id_Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Szulo Base property </summary>
            public SqlGuid Obj_Id_Szulo
            {
                get { return _Obj_Id_Szulo; }
                set { _Obj_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlGuid _KodCsoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// KodCsoport_Id Base property </summary>
            public SqlGuid KodCsoport_Id
            {
                get { return _KodCsoport_Id; }
                set { _KodCsoport_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}