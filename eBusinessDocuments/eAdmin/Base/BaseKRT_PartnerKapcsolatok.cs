
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_PartnerKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Partner_id = true;
               public bool Partner_id
               {
                   get { return _Partner_id; }
                   set { _Partner_id = value; }
               }
                                                            
                                 
               private bool _Partner_id_kapcsolt = true;
               public bool Partner_id_kapcsolt
               {
                   get { return _Partner_id_kapcsolt; }
                   set { _Partner_id_kapcsolt = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Tipus = Value;               
                    
                   Partner_id = Value;               
                    
                   Partner_id_kapcsolt = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_id Base property </summary>
            public SqlGuid Partner_id
            {
                get { return _Partner_id; }
                set { _Partner_id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_id_kapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// Partner_id_kapcsolt Base property </summary>
            public SqlGuid Partner_id_kapcsolt
            {
                get { return _Partner_id_kapcsolt; }
                set { _Partner_id_kapcsolt = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}