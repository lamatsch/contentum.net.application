
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerMinositesek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_PartnerMinositesek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerMinositesekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ALLAPOT = true;
               public bool ALLAPOT
               {
                   get { return _ALLAPOT; }
                   set { _ALLAPOT = value; }
               }
                                                            
                                 
               private bool _Partner_id = true;
               public bool Partner_id
               {
                   get { return _Partner_id; }
                   set { _Partner_id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_id_kero = true;
               public bool Felhasznalo_id_kero
               {
                   get { return _Felhasznalo_id_kero; }
                   set { _Felhasznalo_id_kero = value; }
               }
                                                            
                                 
               private bool _KertMinosites = true;
               public bool KertMinosites
               {
                   get { return _KertMinosites; }
                   set { _KertMinosites = value; }
               }
                                                            
                                 
               private bool _KertKezdDat = true;
               public bool KertKezdDat
               {
                   get { return _KertKezdDat; }
                   set { _KertKezdDat = value; }
               }
                                                            
                                 
               private bool _KertVegeDat = true;
               public bool KertVegeDat
               {
                   get { return _KertVegeDat; }
                   set { _KertVegeDat = value; }
               }
                                                            
                                 
               private bool _KerelemAzonosito = true;
               public bool KerelemAzonosito
               {
                   get { return _KerelemAzonosito; }
                   set { _KerelemAzonosito = value; }
               }
                                                            
                                 
               private bool _KerelemBeadIdo = true;
               public bool KerelemBeadIdo
               {
                   get { return _KerelemBeadIdo; }
                   set { _KerelemBeadIdo = value; }
               }
                                                            
                                 
               private bool _KerelemDontesIdo = true;
               public bool KerelemDontesIdo
               {
                   get { return _KerelemDontesIdo; }
                   set { _KerelemDontesIdo = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_id_donto = true;
               public bool Felhasznalo_id_donto
               {
                   get { return _Felhasznalo_id_donto; }
                   set { _Felhasznalo_id_donto = value; }
               }
                                                            
                                 
               private bool _DontesAzonosito = true;
               public bool DontesAzonosito
               {
                   get { return _DontesAzonosito; }
                   set { _DontesAzonosito = value; }
               }
                                                            
                                 
               private bool _Minosites = true;
               public bool Minosites
               {
                   get { return _Minosites; }
                   set { _Minosites = value; }
               }
                                                            
                                 
               private bool _MinositesKezdDat = true;
               public bool MinositesKezdDat
               {
                   get { return _MinositesKezdDat; }
                   set { _MinositesKezdDat = value; }
               }
                                                            
                                 
               private bool _MinositesVegDat = true;
               public bool MinositesVegDat
               {
                   get { return _MinositesVegDat; }
                   set { _MinositesVegDat = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ALLAPOT = Value;               
                    
                   Partner_id = Value;               
                    
                   Felhasznalo_id_kero = Value;               
                    
                   KertMinosites = Value;               
                    
                   KertKezdDat = Value;               
                    
                   KertVegeDat = Value;               
                    
                   KerelemAzonosito = Value;               
                    
                   KerelemBeadIdo = Value;               
                    
                   KerelemDontesIdo = Value;               
                    
                   Felhasznalo_id_donto = Value;               
                    
                   DontesAzonosito = Value;               
                    
                   Minosites = Value;               
                    
                   MinositesKezdDat = Value;               
                    
                   MinositesVegDat = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerMinositesekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlChars _ALLAPOT = SqlChars.Null;
           
        /// <summary>
        /// ALLAPOT Base property </summary>
            public SqlChars ALLAPOT
            {
                get { return _ALLAPOT; }
                set { _ALLAPOT = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_id Base property </summary>
            public SqlGuid Partner_id
            {
                get { return _Partner_id; }
                set { _Partner_id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_id_kero = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_id_kero Base property </summary>
            public SqlGuid Felhasznalo_id_kero
            {
                get { return _Felhasznalo_id_kero; }
                set { _Felhasznalo_id_kero = value; }                                                        
            }        
                   
           
        private SqlString _KertMinosites = SqlString.Null;
           
        /// <summary>
        /// KertMinosites Base property </summary>
            public SqlString KertMinosites
            {
                get { return _KertMinosites; }
                set { _KertMinosites = value; }                                                        
            }        
                   
           
        private SqlDateTime _KertKezdDat = SqlDateTime.Null;
           
        /// <summary>
        /// KertKezdDat Base property </summary>
            public SqlDateTime KertKezdDat
            {
                get { return _KertKezdDat; }
                set { _KertKezdDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _KertVegeDat = SqlDateTime.Null;
           
        /// <summary>
        /// KertVegeDat Base property </summary>
            public SqlDateTime KertVegeDat
            {
                get { return _KertVegeDat; }
                set { _KertVegeDat = value; }                                                        
            }        
                   
           
        private SqlString _KerelemAzonosito = SqlString.Null;
           
        /// <summary>
        /// KerelemAzonosito Base property </summary>
            public SqlString KerelemAzonosito
            {
                get { return _KerelemAzonosito; }
                set { _KerelemAzonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _KerelemBeadIdo = SqlDateTime.Null;
           
        /// <summary>
        /// KerelemBeadIdo Base property </summary>
            public SqlDateTime KerelemBeadIdo
            {
                get { return _KerelemBeadIdo; }
                set { _KerelemBeadIdo = value; }                                                        
            }        
                   
           
        private SqlDateTime _KerelemDontesIdo = SqlDateTime.Null;
           
        /// <summary>
        /// KerelemDontesIdo Base property </summary>
            public SqlDateTime KerelemDontesIdo
            {
                get { return _KerelemDontesIdo; }
                set { _KerelemDontesIdo = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_id_donto = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_id_donto Base property </summary>
            public SqlGuid Felhasznalo_id_donto
            {
                get { return _Felhasznalo_id_donto; }
                set { _Felhasznalo_id_donto = value; }                                                        
            }        
                   
           
        private SqlString _DontesAzonosito = SqlString.Null;
           
        /// <summary>
        /// DontesAzonosito Base property </summary>
            public SqlString DontesAzonosito
            {
                get { return _DontesAzonosito; }
                set { _DontesAzonosito = value; }                                                        
            }        
                   
           
        private SqlString _Minosites = SqlString.Null;
           
        /// <summary>
        /// Minosites Base property </summary>
            public SqlString Minosites
            {
                get { return _Minosites; }
                set { _Minosites = value; }                                                        
            }        
                   
           
        private SqlDateTime _MinositesKezdDat = SqlDateTime.Null;
           
        /// <summary>
        /// MinositesKezdDat Base property </summary>
            public SqlDateTime MinositesKezdDat
            {
                get { return _MinositesKezdDat; }
                set { _MinositesKezdDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _MinositesVegDat = SqlDateTime.Null;
           
        /// <summary>
        /// MinositesVegDat Base property </summary>
            public SqlDateTime MinositesVegDat
            {
                get { return _MinositesVegDat; }
                set { _MinositesVegDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}