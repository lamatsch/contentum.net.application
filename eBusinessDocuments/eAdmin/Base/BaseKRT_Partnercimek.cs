
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerCimek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_PartnerCimek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerCimekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Partner_id = true;
               public bool Partner_id
               {
                   get { return _Partner_id; }
                   set { _Partner_id = value; }
               }
                                                            
                                 
               private bool _Cim_Id = true;
               public bool Cim_Id
               {
                   get { return _Cim_Id; }
                   set { _Cim_Id = value; }
               }
                                                            
                                 
               private bool _UtolsoHasznalatSiker = true;
               public bool UtolsoHasznalatSiker
               {
                   get { return _UtolsoHasznalatSiker; }
                   set { _UtolsoHasznalatSiker = value; }
               }
                                                            
                                 
               private bool _UtolsoHasznalatiIdo = true;
               public bool UtolsoHasznalatiIdo
               {
                   get { return _UtolsoHasznalatiIdo; }
                   set { _UtolsoHasznalatiIdo = value; }
               }
                                                            
                                 
               private bool _eMailKuldes = true;
               public bool eMailKuldes
               {
                   get { return _eMailKuldes; }
                   set { _eMailKuldes = value; }
               }
                                                            
                                 
               private bool _Fajta = true;
               public bool Fajta
               {
                   get { return _Fajta; }
                   set { _Fajta = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Partner_id = Value;               
                    
                   Cim_Id = Value;               
                    
                   UtolsoHasznalatSiker = Value;               
                    
                   UtolsoHasznalatiIdo = Value;               
                    
                   eMailKuldes = Value;               
                    
                   Fajta = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerCimekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_id Base property </summary>
            public SqlGuid Partner_id
            {
                get { return _Partner_id; }
                set { _Partner_id = value; }                                                        
            }        
                   
           
        private SqlGuid _Cim_Id = SqlGuid.Null;
           
        /// <summary>
        /// Cim_Id Base property </summary>
            public SqlGuid Cim_Id
            {
                get { return _Cim_Id; }
                set { _Cim_Id = value; }                                                        
            }        
                   
           
        private SqlChars _UtolsoHasznalatSiker = SqlChars.Null;
           
        /// <summary>
        /// UtolsoHasznalatSiker Base property </summary>
            public SqlChars UtolsoHasznalatSiker
            {
                get { return _UtolsoHasznalatSiker; }
                set { _UtolsoHasznalatSiker = value; }                                                        
            }        
                   
           
        private SqlDateTime _UtolsoHasznalatiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// UtolsoHasznalatiIdo Base property </summary>
            public SqlDateTime UtolsoHasznalatiIdo
            {
                get { return _UtolsoHasznalatiIdo; }
                set { _UtolsoHasznalatiIdo = value; }                                                        
            }        
                   
           
        private SqlChars _eMailKuldes = SqlChars.Null;
           
        /// <summary>
        /// eMailKuldes Base property </summary>
            public SqlChars eMailKuldes
            {
                get { return _eMailKuldes; }
                set { _eMailKuldes = value; }                                                        
            }        
                   
           
        private SqlString _Fajta = SqlString.Null;
           
        /// <summary>
        /// Fajta Base property </summary>
            public SqlString Fajta
            {
                get { return _Fajta; }
                set { _Fajta = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}