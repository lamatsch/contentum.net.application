
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// KRT_Partnerek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Partnerek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_PartnerekBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Org = true;
            public bool Org
            {
                get { return _Org; }
                set { _Org = value; }
            }


            private bool _Orszag_Id = true;
            public bool Orszag_Id
            {
                get { return _Orszag_Id; }
                set { _Orszag_Id = value; }
            }


            private bool _Tipus = true;
            public bool Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }


            private bool _Nev = true;
            public bool Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private bool _KulsoAzonositok = true;
            public bool KulsoAzonositok
            {
                get { return _KulsoAzonositok; }
                set { _KulsoAzonositok = value; }
            }


            private bool _PublikusKulcs = true;
            public bool PublikusKulcs
            {
                get { return _PublikusKulcs; }
                set { _PublikusKulcs = value; }
            }


            private bool _Kiszolgalo = true;
            public bool Kiszolgalo
            {
                get { return _Kiszolgalo; }
                set { _Kiszolgalo = value; }
            }


            private bool _LetrehozoSzervezet = true;
            public bool LetrehozoSzervezet
            {
                get { return _LetrehozoSzervezet; }
                set { _LetrehozoSzervezet = value; }
            }


            private bool _MaxMinosites = true;
            public bool MaxMinosites
            {
                get { return _MaxMinosites; }
                set { _MaxMinosites = value; }
            }


            private bool _MinositesKezdDat = true;
            public bool MinositesKezdDat
            {
                get { return _MinositesKezdDat; }
                set { _MinositesKezdDat = value; }
            }


            private bool _MinositesVegDat = true;
            public bool MinositesVegDat
            {
                get { return _MinositesVegDat; }
                set { _MinositesVegDat = value; }
            }


            private bool _MaxMinositesSzervezet = true;
            public bool MaxMinositesSzervezet
            {
                get { return _MaxMinositesSzervezet; }
                set { _MaxMinositesSzervezet = value; }
            }


            private bool _Belso = true;
            public bool Belso
            {
                get { return _Belso; }
                set { _Belso = value; }
            }


            private bool _Forras = true;
            public bool Forras
            {
                get { return _Forras; }
                set { _Forras = value; }
            }
            private bool _AspAdoTorolve = true;
            public bool AspAdoTorolve
            {
                get { return _AspAdoTorolve; }
                set { _AspAdoTorolve = value; }
            }

            private bool _Allapot = true;
            public bool Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }


            private bool _UtolsoHasznalat = true;
            public bool UtolsoHasznalat
            {
                get { return _UtolsoHasznalat; }
                set { _UtolsoHasznalat = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Org = Value;

                Orszag_Id = Value;

                Tipus = Value;

                Nev = Value;

                KulsoAzonositok = Value;

                PublikusKulcs = Value;

                Kiszolgalo = Value;

                LetrehozoSzervezet = Value;

                MaxMinosites = Value;

                MinositesKezdDat = Value;

                MinositesVegDat = Value;

                MaxMinositesSzervezet = Value;

                Belso = Value;

                Forras = Value;

                Allapot = Value;

                AspAdoTorolve = Value;

                ErvKezd = Value;

                ErvVege = Value;

                UtolsoHasznalat = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseKRT_PartnerekBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlGuid _Org = SqlGuid.Null;

            /// <summary>
            /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }
            }


            private SqlGuid _Orszag_Id = SqlGuid.Null;

            /// <summary>
            /// Orszag_Id Base property </summary>
            public SqlGuid Orszag_Id
            {
                get { return _Orszag_Id; }
                set { _Orszag_Id = value; }
            }


            private SqlString _Tipus = SqlString.Null;

            /// <summary>
            /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }


            private SqlString _Nev = SqlString.Null;

            /// <summary>
            /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private SqlString _KulsoAzonositok = SqlString.Null;

            /// <summary>
            /// KulsoAzonositok Base property </summary>
            public SqlString KulsoAzonositok
            {
                get { return _KulsoAzonositok; }
                set { _KulsoAzonositok = value; }
            }


            private SqlString _PublikusKulcs = SqlString.Null;

            /// <summary>
            /// PublikusKulcs Base property </summary>
            public SqlString PublikusKulcs
            {
                get { return _PublikusKulcs; }
                set { _PublikusKulcs = value; }
            }


            private SqlGuid _Kiszolgalo = SqlGuid.Null;

            /// <summary>
            /// Kiszolgalo Base property </summary>
            public SqlGuid Kiszolgalo
            {
                get { return _Kiszolgalo; }
                set { _Kiszolgalo = value; }
            }


            private SqlGuid _LetrehozoSzervezet = SqlGuid.Null;

            /// <summary>
            /// LetrehozoSzervezet Base property </summary>
            public SqlGuid LetrehozoSzervezet
            {
                get { return _LetrehozoSzervezet; }
                set { _LetrehozoSzervezet = value; }
            }


            private SqlString _MaxMinosites = SqlString.Null;

            /// <summary>
            /// MaxMinosites Base property </summary>
            public SqlString MaxMinosites
            {
                get { return _MaxMinosites; }
                set { _MaxMinosites = value; }
            }


            private SqlDateTime _MinositesKezdDat = SqlDateTime.Null;

            /// <summary>
            /// MinositesKezdDat Base property </summary>
            public SqlDateTime MinositesKezdDat
            {
                get { return _MinositesKezdDat; }
                set { _MinositesKezdDat = value; }
            }


            private SqlDateTime _MinositesVegDat = SqlDateTime.Null;

            /// <summary>
            /// MinositesVegDat Base property </summary>
            public SqlDateTime MinositesVegDat
            {
                get { return _MinositesVegDat; }
                set { _MinositesVegDat = value; }
            }


            private SqlString _MaxMinositesSzervezet = SqlString.Null;

            /// <summary>
            /// MaxMinositesSzervezet Base property </summary>
            public SqlString MaxMinositesSzervezet
            {
                get { return _MaxMinositesSzervezet; }
                set { _MaxMinositesSzervezet = value; }
            }


            private SqlChars _Belso = SqlChars.Null;

            /// <summary>
            /// Belso Base property </summary>
            public SqlChars Belso
            {
                get { return _Belso; }
                set { _Belso = value; }
            }


            private SqlChars _Forras = SqlChars.Null;

            /// <summary>
            /// Forras Base property </summary>
            public SqlChars Forras
            {
                get { return _Forras; }
                set { _Forras = value; }
            }
            

            private SqlChars _AspAdoTorolve = SqlChars.Null;

            /// <summary>
            /// Allapot Base property </summary>
            public SqlChars AspAdoTorolve
            {
                get { return _AspAdoTorolve; }
                set { _AspAdoTorolve = value; }
            }


            private SqlString _Allapot = SqlString.Null;

            /// <summary>
            /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }


            private SqlDateTime _UtolsoHasznalat = SqlDateTime.Null;

            /// <summary>
            /// UtolsoHasznalat Base property </summary>
            public SqlDateTime UtolsoHasznalat
            {
                get { return _UtolsoHasznalat; }
                set { _UtolsoHasznalat = value; }
            }
        }
    }
}