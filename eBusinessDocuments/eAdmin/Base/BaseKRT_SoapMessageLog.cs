
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_SoapMessageLog BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_SoapMessageLog
    {
        [System.Xml.Serialization.XmlType("BaseKRT_SoapMessageLogBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _ForrasRendszer = true;
               public bool ForrasRendszer
               {
                   get { return _ForrasRendszer; }
                   set { _ForrasRendszer = value; }
               }
                                                            
                                 
               private bool _Url = true;
               public bool Url
               {
                   get { return _Url; }
                   set { _Url = value; }
               }
                                                            
                                 
               private bool _CelRendszer = true;
               public bool CelRendszer
               {
                   get { return _CelRendszer; }
                   set { _CelRendszer = value; }
               }
                                                            
                                 
               private bool _MetodusNeve = true;
               public bool MetodusNeve
               {
                   get { return _MetodusNeve; }
                   set { _MetodusNeve = value; }
               }
                                                            
                                 
               private bool _KeresFogadas = true;
               public bool KeresFogadas
               {
                   get { return _KeresFogadas; }
                   set { _KeresFogadas = value; }
               }
                                                            
                                 
               private bool _KeresKuldes = true;
               public bool KeresKuldes
               {
                   get { return _KeresKuldes; }
                   set { _KeresKuldes = value; }
               }
                                                            
                                 
               private bool _RequestData = true;
               public bool RequestData
               {
                   get { return _RequestData; }
                   set { _RequestData = value; }
               }
                                                            
                                 
               private bool _ResponseData = true;
               public bool ResponseData
               {
                   get { return _ResponseData; }
                   set { _ResponseData = value; }
               }
                                                            
                                 
               private bool _FuttatoFelhasznalo = true;
               public bool FuttatoFelhasznalo
               {
                   get { return _FuttatoFelhasznalo; }
                   set { _FuttatoFelhasznalo = value; }
               }
                                                            
                                 
               private bool _Local = true;
               public bool Local
               {
                   get { return _Local; }
                   set { _Local = value; }
               }
                                                            
                                 
               private bool _Hiba = true;
               public bool Hiba
               {
                   get { return _Hiba; }
                   set { _Hiba = value; }
               }
                                                            
                                 
               private bool _Sikeres = true;
               public bool Sikeres
               {
                   get { return _Sikeres; }
                   set { _Sikeres = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   ForrasRendszer = Value;               
                    
                   Url = Value;               
                    
                   CelRendszer = Value;               
                    
                   MetodusNeve = Value;               
                    
                   KeresFogadas = Value;               
                    
                   KeresKuldes = Value;               
                    
                   RequestData = Value;               
                    
                   ResponseData = Value;               
                    
                   FuttatoFelhasznalo = Value;               
                    
                   Local = Value;               
                    
                   Hiba = Value;               
                    
                   Sikeres = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_SoapMessageLogBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlString _ForrasRendszer = SqlString.Null;
           
        /// <summary>
        /// ForrasRendszer Base property </summary>
            public SqlString ForrasRendszer
            {
                get { return _ForrasRendszer; }
                set { _ForrasRendszer = value; }                                                        
            }        
                   
           
        private SqlString _Url = SqlString.Null;
           
        /// <summary>
        /// Url Base property </summary>
            public SqlString Url
            {
                get { return _Url; }
                set { _Url = value; }                                                        
            }        
                   
           
        private SqlString _CelRendszer = SqlString.Null;
           
        /// <summary>
        /// CelRendszer Base property </summary>
            public SqlString CelRendszer
            {
                get { return _CelRendszer; }
                set { _CelRendszer = value; }                                                        
            }        
                   
           
        private SqlString _MetodusNeve = SqlString.Null;
           
        /// <summary>
        /// MetodusNeve Base property </summary>
            public SqlString MetodusNeve
            {
                get { return _MetodusNeve; }
                set { _MetodusNeve = value; }                                                        
            }        
                   
           
        private SqlDateTime _KeresFogadas = SqlDateTime.Null;
           
        /// <summary>
        /// KeresFogadas Base property </summary>
            public SqlDateTime KeresFogadas
            {
                get { return _KeresFogadas; }
                set { _KeresFogadas = value; }                                                        
            }        
                   
           
        private SqlDateTime _KeresKuldes = SqlDateTime.Null;
           
        /// <summary>
        /// KeresKuldes Base property </summary>
            public SqlDateTime KeresKuldes
            {
                get { return _KeresKuldes; }
                set { _KeresKuldes = value; }                                                        
            }        
                   
           
        private SqlString _RequestData = SqlString.Null;
           
        /// <summary>
        /// RequestData Base property </summary>
            public SqlString RequestData
            {
                get { return _RequestData; }
                set { _RequestData = value; }                                                        
            }        
                   
           
        private SqlString _ResponseData = SqlString.Null;
           
        /// <summary>
        /// ResponseData Base property </summary>
            public SqlString ResponseData
            {
                get { return _ResponseData; }
                set { _ResponseData = value; }                                                        
            }        
                   
           
        private SqlString _FuttatoFelhasznalo = SqlString.Null;
           
        /// <summary>
        /// FuttatoFelhasznalo Base property </summary>
            public SqlString FuttatoFelhasznalo
            {
                get { return _FuttatoFelhasznalo; }
                set { _FuttatoFelhasznalo = value; }                                                        
            }        
                   
           
        private SqlChars _Local = SqlChars.Null;
           
        /// <summary>
        /// Local Base property </summary>
            public SqlChars Local
            {
                get { return _Local; }
                set { _Local = value; }                                                        
            }        
                   
           
        private SqlString _Hiba = SqlString.Null;
           
        /// <summary>
        /// Hiba Base property </summary>
            public SqlString Hiba
            {
                get { return _Hiba; }
                set { _Hiba = value; }                                                        
            }        
                   
           
        private SqlChars _Sikeres = SqlChars.Null;
           
        /// <summary>
        /// Sikeres Base property </summary>
            public SqlChars Sikeres
            {
                get { return _Sikeres; }
                set { _Sikeres = value; }                                                        
            }        
                           }
    }    
}