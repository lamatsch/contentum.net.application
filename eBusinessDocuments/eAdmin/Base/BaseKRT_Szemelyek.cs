
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Szemelyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Szemelyek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_SzemelyekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Partner_Id = true;
               public bool Partner_Id
               {
                   get { return _Partner_Id; }
                   set { _Partner_Id = value; }
               }
                                                            
                                 
               private bool _AnyjaNeve = true;
               public bool AnyjaNeve
               {
                   get { return _AnyjaNeve; }
                   set { _AnyjaNeve = value; }
               }
                                                            
                                 
               private bool _AnyjaNeveCsaladiNev = true;
               public bool AnyjaNeveCsaladiNev
               {
                   get { return _AnyjaNeveCsaladiNev; }
                   set { _AnyjaNeveCsaladiNev = value; }
               }
                                                            
                                 
               private bool _AnyjaNeveElsoUtonev = true;
               public bool AnyjaNeveElsoUtonev
               {
                   get { return _AnyjaNeveElsoUtonev; }
                   set { _AnyjaNeveElsoUtonev = value; }
               }
                                                            
                                 
               private bool _AnyjaNeveTovabbiUtonev = true;
               public bool AnyjaNeveTovabbiUtonev
               {
                   get { return _AnyjaNeveTovabbiUtonev; }
                   set { _AnyjaNeveTovabbiUtonev = value; }
               }
                                                            
                                 
               private bool _ApjaNeve = true;
               public bool ApjaNeve
               {
                   get { return _ApjaNeve; }
                   set { _ApjaNeve = value; }
               }
                                                            
                                 
               private bool _SzuletesiNev = true;
               public bool SzuletesiNev
               {
                   get { return _SzuletesiNev; }
                   set { _SzuletesiNev = value; }
               }
                                                            
                                 
               private bool _SzuletesiCsaladiNev = true;
               public bool SzuletesiCsaladiNev
               {
                   get { return _SzuletesiCsaladiNev; }
                   set { _SzuletesiCsaladiNev = value; }
               }
                                                            
                                 
               private bool _SzuletesiElsoUtonev = true;
               public bool SzuletesiElsoUtonev
               {
                   get { return _SzuletesiElsoUtonev; }
                   set { _SzuletesiElsoUtonev = value; }
               }
                                                            
                                 
               private bool _SzuletesiTovabbiUtonev = true;
               public bool SzuletesiTovabbiUtonev
               {
                   get { return _SzuletesiTovabbiUtonev; }
                   set { _SzuletesiTovabbiUtonev = value; }
               }
                                                            
                                 
               private bool _SzuletesiOrszag = true;
               public bool SzuletesiOrszag
               {
                   get { return _SzuletesiOrszag; }
                   set { _SzuletesiOrszag = value; }
               }
                                                            
                                 
               private bool _SzuletesiOrszagId = true;
               public bool SzuletesiOrszagId
               {
                   get { return _SzuletesiOrszagId; }
                   set { _SzuletesiOrszagId = value; }
               }
                                                            
                                 
               private bool _UjTitulis = true;
               public bool UjTitulis
               {
                   get { return _UjTitulis; }
                   set { _UjTitulis = value; }
               }
                                                            
                                 
               private bool _UjCsaladiNev = true;
               public bool UjCsaladiNev
               {
                   get { return _UjCsaladiNev; }
                   set { _UjCsaladiNev = value; }
               }
                                                            
                                 
               private bool _UjUtonev = true;
               public bool UjUtonev
               {
                   get { return _UjUtonev; }
                   set { _UjUtonev = value; }
               }
                                                            
                                 
               private bool _UjTovabbiUtonev = true;
               public bool UjTovabbiUtonev
               {
                   get { return _UjTovabbiUtonev; }
                   set { _UjTovabbiUtonev = value; }
               }
                                                            
                                 
               private bool _SzuletesiHely = true;
               public bool SzuletesiHely
               {
                   get { return _SzuletesiHely; }
                   set { _SzuletesiHely = value; }
               }
                                                            
                                 
               private bool _SzuletesiHely_id = true;
               public bool SzuletesiHely_id
               {
                   get { return _SzuletesiHely_id; }
                   set { _SzuletesiHely_id = value; }
               }
                                                            
                                 
               private bool _SzuletesiIdo = true;
               public bool SzuletesiIdo
               {
                   get { return _SzuletesiIdo; }
                   set { _SzuletesiIdo = value; }
               }
                                                            
                                 
               private bool _Allampolgarsag = true;
               public bool Allampolgarsag
               {
                   get { return _Allampolgarsag; }
                   set { _Allampolgarsag = value; }
               }
                                                            
                                 
               private bool _TAJSzam = true;
               public bool TAJSzam
               {
                   get { return _TAJSzam; }
                   set { _TAJSzam = value; }
               }
                                                            
                                 
               private bool _SZIGSzam = true;
               public bool SZIGSzam
               {
                   get { return _SZIGSzam; }
                   set { _SZIGSzam = value; }
               }
                                                            
                                 
               private bool _Neme = true;
               public bool Neme
               {
                   get { return _Neme; }
                   set { _Neme = value; }
               }
                                                            
                                 
               private bool _SzemelyiAzonosito = true;
               public bool SzemelyiAzonosito
               {
                   get { return _SzemelyiAzonosito; }
                   set { _SzemelyiAzonosito = value; }
               }
                                                            
                                 
               private bool _Adoazonosito = true;
               public bool Adoazonosito
               {
                   get { return _Adoazonosito; }
                   set { _Adoazonosito = value; }
               }
                                                            
                                 
               private bool _Adoszam = true;
               public bool Adoszam
               {
                   get { return _Adoszam; }
                   set { _Adoszam = value; }
               }
                                                            
                                 
               private bool _KulfoldiAdoszamJelolo = true;
               public bool KulfoldiAdoszamJelolo
               {
                   get { return _KulfoldiAdoszamJelolo; }
                   set { _KulfoldiAdoszamJelolo = value; }
               }
                                                            
                                 
               private bool _Beosztas = true;
               public bool Beosztas
               {
                   get { return _Beosztas; }
                   set { _Beosztas = value; }
               }
                                                            
                                 
               private bool _MinositesiSzint = true;
               public bool MinositesiSzint
               {
                   get { return _MinositesiSzint; }
                   set { _MinositesiSzint = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Partner_Id = Value;               
                    
                   AnyjaNeve = Value;               
                    
                   AnyjaNeveCsaladiNev = Value;               
                    
                   AnyjaNeveElsoUtonev = Value;               
                    
                   AnyjaNeveTovabbiUtonev = Value;               
                    
                   ApjaNeve = Value;               
                    
                   SzuletesiNev = Value;               
                    
                   SzuletesiCsaladiNev = Value;               
                    
                   SzuletesiElsoUtonev = Value;               
                    
                   SzuletesiTovabbiUtonev = Value;               
                    
                   SzuletesiOrszag = Value;               
                    
                   SzuletesiOrszagId = Value;               
                    
                   UjTitulis = Value;               
                    
                   UjCsaladiNev = Value;               
                    
                   UjUtonev = Value;               
                    
                   UjTovabbiUtonev = Value;               
                    
                   SzuletesiHely = Value;               
                    
                   SzuletesiHely_id = Value;               
                    
                   SzuletesiIdo = Value;               
                    
                   Allampolgarsag = Value;               
                    
                   TAJSzam = Value;               
                    
                   SZIGSzam = Value;               
                    
                   Neme = Value;               
                    
                   SzemelyiAzonosito = Value;               
                    
                   Adoazonosito = Value;               
                    
                   Adoszam = Value;               
                    
                   KulfoldiAdoszamJelolo = Value;               
                    
                   Beosztas = Value;               
                    
                   MinositesiSzint = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_SzemelyekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id Base property </summary>
            public SqlGuid Partner_Id
            {
                get { return _Partner_Id; }
                set { _Partner_Id = value; }                                                        
            }        
                   
           
        private SqlString _AnyjaNeve = SqlString.Null;
           
        /// <summary>
        /// AnyjaNeve Base property </summary>
            public SqlString AnyjaNeve
            {
                get { return _AnyjaNeve; }
                set { _AnyjaNeve = value; }                                                        
            }        
                   
           
        private SqlString _AnyjaNeveCsaladiNev = SqlString.Null;
           
        /// <summary>
        /// AnyjaNeveCsaladiNev Base property </summary>
            public SqlString AnyjaNeveCsaladiNev
            {
                get { return _AnyjaNeveCsaladiNev; }
                set { _AnyjaNeveCsaladiNev = value; }                                                        
            }        
                   
           
        private SqlString _AnyjaNeveElsoUtonev = SqlString.Null;
           
        /// <summary>
        /// AnyjaNeveElsoUtonev Base property </summary>
            public SqlString AnyjaNeveElsoUtonev
            {
                get { return _AnyjaNeveElsoUtonev; }
                set { _AnyjaNeveElsoUtonev = value; }                                                        
            }        
                   
           
        private SqlString _AnyjaNeveTovabbiUtonev = SqlString.Null;
           
        /// <summary>
        /// AnyjaNeveTovabbiUtonev Base property </summary>
            public SqlString AnyjaNeveTovabbiUtonev
            {
                get { return _AnyjaNeveTovabbiUtonev; }
                set { _AnyjaNeveTovabbiUtonev = value; }                                                        
            }        
                   
           
        private SqlString _ApjaNeve = SqlString.Null;
           
        /// <summary>
        /// ApjaNeve Base property </summary>
            public SqlString ApjaNeve
            {
                get { return _ApjaNeve; }
                set { _ApjaNeve = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiNev = SqlString.Null;
           
        /// <summary>
        /// SzuletesiNev Base property </summary>
            public SqlString SzuletesiNev
            {
                get { return _SzuletesiNev; }
                set { _SzuletesiNev = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiCsaladiNev = SqlString.Null;
           
        /// <summary>
        /// SzuletesiCsaladiNev Base property </summary>
            public SqlString SzuletesiCsaladiNev
            {
                get { return _SzuletesiCsaladiNev; }
                set { _SzuletesiCsaladiNev = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiElsoUtonev = SqlString.Null;
           
        /// <summary>
        /// SzuletesiElsoUtonev Base property </summary>
            public SqlString SzuletesiElsoUtonev
            {
                get { return _SzuletesiElsoUtonev; }
                set { _SzuletesiElsoUtonev = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiTovabbiUtonev = SqlString.Null;
           
        /// <summary>
        /// SzuletesiTovabbiUtonev Base property </summary>
            public SqlString SzuletesiTovabbiUtonev
            {
                get { return _SzuletesiTovabbiUtonev; }
                set { _SzuletesiTovabbiUtonev = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiOrszag = SqlString.Null;
           
        /// <summary>
        /// SzuletesiOrszag Base property </summary>
            public SqlString SzuletesiOrszag
            {
                get { return _SzuletesiOrszag; }
                set { _SzuletesiOrszag = value; }                                                        
            }        
                   
           
        private SqlGuid _SzuletesiOrszagId = SqlGuid.Null;
           
        /// <summary>
        /// SzuletesiOrszagId Base property </summary>
            public SqlGuid SzuletesiOrszagId
            {
                get { return _SzuletesiOrszagId; }
                set { _SzuletesiOrszagId = value; }                                                        
            }        
                   
           
        private SqlString _UjTitulis = SqlString.Null;
           
        /// <summary>
        /// UjTitulis Base property </summary>
            public SqlString UjTitulis
            {
                get { return _UjTitulis; }
                set { _UjTitulis = value; }                                                        
            }        
                   
           
        private SqlString _UjCsaladiNev = SqlString.Null;
           
        /// <summary>
        /// UjCsaladiNev Base property </summary>
            public SqlString UjCsaladiNev
            {
                get { return _UjCsaladiNev; }
                set { _UjCsaladiNev = value; }                                                        
            }        
                   
           
        private SqlString _UjUtonev = SqlString.Null;
           
        /// <summary>
        /// UjUtonev Base property </summary>
            public SqlString UjUtonev
            {
                get { return _UjUtonev; }
                set { _UjUtonev = value; }                                                        
            }        
                   
           
        private SqlString _UjTovabbiUtonev = SqlString.Null;
           
        /// <summary>
        /// UjTovabbiUtonev Base property </summary>
            public SqlString UjTovabbiUtonev
            {
                get { return _UjTovabbiUtonev; }
                set { _UjTovabbiUtonev = value; }                                                        
            }        
                   
           
        private SqlString _SzuletesiHely = SqlString.Null;
           
        /// <summary>
        /// SzuletesiHely Base property </summary>
            public SqlString SzuletesiHely
            {
                get { return _SzuletesiHely; }
                set { _SzuletesiHely = value; }                                                        
            }        
                   
           
        private SqlGuid _SzuletesiHely_id = SqlGuid.Null;
           
        /// <summary>
        /// SzuletesiHely_id Base property </summary>
            public SqlGuid SzuletesiHely_id
            {
                get { return _SzuletesiHely_id; }
                set { _SzuletesiHely_id = value; }                                                        
            }        
                   
           
        private SqlDateTime _SzuletesiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// SzuletesiIdo Base property </summary>
            public SqlDateTime SzuletesiIdo
            {
                get { return _SzuletesiIdo; }
                set { _SzuletesiIdo = value; }                                                        
            }        
                   
           
        private SqlString _Allampolgarsag = SqlString.Null;
           
        /// <summary>
        /// Allampolgarsag Base property </summary>
            public SqlString Allampolgarsag
            {
                get { return _Allampolgarsag; }
                set { _Allampolgarsag = value; }                                                        
            }        
                   
           
        private SqlString _TAJSzam = SqlString.Null;
           
        /// <summary>
        /// TAJSzam Base property </summary>
            public SqlString TAJSzam
            {
                get { return _TAJSzam; }
                set { _TAJSzam = value; }                                                        
            }        
                   
           
        private SqlString _SZIGSzam = SqlString.Null;
           
        /// <summary>
        /// SZIGSzam Base property </summary>
            public SqlString SZIGSzam
            {
                get { return _SZIGSzam; }
                set { _SZIGSzam = value; }                                                        
            }        
                   
           
        private SqlChars _Neme = SqlChars.Null;
           
        /// <summary>
        /// Neme Base property </summary>
            public SqlChars Neme
            {
                get { return _Neme; }
                set { _Neme = value; }                                                        
            }        
                   
           
        private SqlString _SzemelyiAzonosito = SqlString.Null;
           
        /// <summary>
        /// SzemelyiAzonosito Base property </summary>
            public SqlString SzemelyiAzonosito
            {
                get { return _SzemelyiAzonosito; }
                set { _SzemelyiAzonosito = value; }                                                        
            }        
                   
           
        private SqlString _Adoazonosito = SqlString.Null;
           
        /// <summary>
        /// Adoazonosito Base property </summary>
            public SqlString Adoazonosito
            {
                get { return _Adoazonosito; }
                set { _Adoazonosito = value; }                                                        
            }        
                   
           
        private SqlString _Adoszam = SqlString.Null;
           
        /// <summary>
        /// Adoszam Base property </summary>
            public SqlString Adoszam
            {
                get { return _Adoszam; }
                set { _Adoszam = value; }                                                        
            }        
                   
           
        private SqlChars _KulfoldiAdoszamJelolo = SqlChars.Null;
           
        /// <summary>
        /// KulfoldiAdoszamJelolo Base property </summary>
            public SqlChars KulfoldiAdoszamJelolo
            {
                get { return _KulfoldiAdoszamJelolo; }
                set { _KulfoldiAdoszamJelolo = value; }                                                        
            }        
                   
           
        private SqlString _Beosztas = SqlString.Null;
           
        /// <summary>
        /// Beosztas Base property </summary>
            public SqlString Beosztas
            {
                get { return _Beosztas; }
                set { _Beosztas = value; }                                                        
            }        
                   
           
        private SqlString _MinositesiSzint = SqlString.Null;
           
        /// <summary>
        /// MinositesiSzint Base property </summary>
            public SqlString MinositesiSzint
            {
                get { return _MinositesiSzint; }
                set { _MinositesiSzint = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}