
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Telepulesek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Telepulesek
    {
        [System.Xml.Serialization.XmlType("BaseKRT_TelepulesekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _IRSZ = true;
               public bool IRSZ
               {
                   get { return _IRSZ; }
                   set { _IRSZ = value; }
               }
                                                            
                                 
               private bool _Telepules_Id_Fo = true;
               public bool Telepules_Id_Fo
               {
                   get { return _Telepules_Id_Fo; }
                   set { _Telepules_Id_Fo = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Orszag_Id = true;
               public bool Orszag_Id
               {
                   get { return _Orszag_Id; }
                   set { _Orszag_Id = value; }
               }
                                                            
                                 
               private bool _Megye = true;
               public bool Megye
               {
                   get { return _Megye; }
                   set { _Megye = value; }
               }
                                                            
                                 
               private bool _Regio = true;
               public bool Regio
               {
                   get { return _Regio; }
                   set { _Regio = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   IRSZ = Value;               
                    
                   Telepules_Id_Fo = Value;               
                    
                   Nev = Value;               
                    
                   Orszag_Id = Value;               
                    
                   Megye = Value;               
                    
                   Regio = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_TelepulesekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _IRSZ = SqlString.Null;
           
        /// <summary>
        /// IRSZ Base property </summary>
            public SqlString IRSZ
            {
                get { return _IRSZ; }
                set { _IRSZ = value; }                                                        
            }        
                   
           
        private SqlGuid _Telepules_Id_Fo = SqlGuid.Null;
           
        /// <summary>
        /// Telepules_Id_Fo Base property </summary>
            public SqlGuid Telepules_Id_Fo
            {
                get { return _Telepules_Id_Fo; }
                set { _Telepules_Id_Fo = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _Orszag_Id = SqlGuid.Null;
           
        /// <summary>
        /// Orszag_Id Base property </summary>
            public SqlGuid Orszag_Id
            {
                get { return _Orszag_Id; }
                set { _Orszag_Id = value; }                                                        
            }        
                   
           
        private SqlString _Megye = SqlString.Null;
           
        /// <summary>
        /// Megye Base property </summary>
            public SqlString Megye
            {
                get { return _Megye; }
                set { _Megye = value; }                                                        
            }        
                   
           
        private SqlString _Regio = SqlString.Null;
           
        /// <summary>
        /// Regio Base property </summary>
            public SqlString Regio
            {
                get { return _Regio; }
                set { _Regio = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}