
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_USER_SETTINGS BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_USER_SETTINGS
    {
        [System.Xml.Serialization.XmlType("BaseKRT_USER_SETTINGSBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _BEALLITASTIPUS = true;
               public bool BEALLITASTIPUS
               {
                   get { return _BEALLITASTIPUS; }
                   set { _BEALLITASTIPUS = value; }
               }
                                                            
                                 
               private bool _BEALLITASOK_XML = true;
               public bool BEALLITASOK_XML
               {
                   get { return _BEALLITASOK_XML; }
                   set { _BEALLITASOK_XML = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Csoport_Id = Value;               
                    
                   BEALLITASTIPUS = Value;               
                    
                   BEALLITASOK_XML = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_USER_SETTINGSBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlChars _BEALLITASTIPUS = SqlChars.Null;
           
        /// <summary>
        /// BEALLITASTIPUS Base property </summary>
            public SqlChars BEALLITASTIPUS
            {
                get { return _BEALLITASTIPUS; }
                set { _BEALLITASTIPUS = value; }                                                        
            }        
                   
           
        private SqlString _BEALLITASOK_XML = SqlString.Null;
           
        /// <summary>
        /// BEALLITASOK_XML Base property </summary>
            public SqlString BEALLITASOK_XML
            {
                get { return _BEALLITASOK_XML; }
                set { _BEALLITASOK_XML = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}