
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Vallalkozasok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Vallalkozasok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_VallalkozasokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Partner_Id = true;
               public bool Partner_Id
               {
                   get { return _Partner_Id; }
                   set { _Partner_Id = value; }
               }
                                                            
                                 
               private bool _Adoszam = true;
               public bool Adoszam
               {
                   get { return _Adoszam; }
                   set { _Adoszam = value; }
               }
                                                            
                                 
               private bool _KulfoldiAdoszamJelolo = true;
               public bool KulfoldiAdoszamJelolo
               {
                   get { return _KulfoldiAdoszamJelolo; }
                   set { _KulfoldiAdoszamJelolo = value; }
               }
                                                            
                                 
               private bool _TB_Torzsszam = true;
               public bool TB_Torzsszam
               {
                   get { return _TB_Torzsszam; }
                   set { _TB_Torzsszam = value; }
               }
                                                            
                                 
               private bool _Cegjegyzekszam = true;
               public bool Cegjegyzekszam
               {
                   get { return _Cegjegyzekszam; }
                   set { _Cegjegyzekszam = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Partner_Id = Value;               
                    
                   Adoszam = Value;               
                    
                   KulfoldiAdoszamJelolo = Value;               
                    
                   TB_Torzsszam = Value;               
                    
                   Cegjegyzekszam = Value;               
                    
                   Tipus = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_VallalkozasokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id Base property </summary>
            public SqlGuid Partner_Id
            {
                get { return _Partner_Id; }
                set { _Partner_Id = value; }                                                        
            }        
                   
           
        private SqlString _Adoszam = SqlString.Null;
           
        /// <summary>
        /// Adoszam Base property </summary>
            public SqlString Adoszam
            {
                get { return _Adoszam; }
                set { _Adoszam = value; }                                                        
            }        
                   
           
        private SqlChars _KulfoldiAdoszamJelolo = SqlChars.Null;
           
        /// <summary>
        /// KulfoldiAdoszamJelolo Base property </summary>
            public SqlChars KulfoldiAdoszamJelolo
            {
                get { return _KulfoldiAdoszamJelolo; }
                set { _KulfoldiAdoszamJelolo = value; }                                                        
            }        
                   
           
        private SqlString _TB_Torzsszam = SqlString.Null;
           
        /// <summary>
        /// TB_Torzsszam Base property </summary>
            public SqlString TB_Torzsszam
            {
                get { return _TB_Torzsszam; }
                set { _TB_Torzsszam = value; }                                                        
            }        
                   
           
        private SqlString _Cegjegyzekszam = SqlString.Null;
           
        /// <summary>
        /// Cegjegyzekszam Base property </summary>
            public SqlString Cegjegyzekszam
            {
                get { return _Cegjegyzekszam; }
                set { _Cegjegyzekszam = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}