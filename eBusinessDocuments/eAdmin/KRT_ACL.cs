
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ACL BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_ACL : BaseKRT_ACL
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Szulo property </summary>
        public String Obj_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Szulo); }
            set { Typed.Obj_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_Szulo property </summary>
        public String ObjTipus_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_Szulo); }
            set { Typed.ObjTipus_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// Szulo_ACL_Id property </summary>
        public String Szulo_ACL_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Szulo_ACL_Id); }
            set { Typed.Szulo_ACL_Id = Utility.SetSqlGuidFromString(value, Typed.Szulo_ACL_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_ACL_Orokles_Mod property </summary>
        public String Csoport_ACL_Orokles_Mod
        {
            get { return Utility.GetStringFromSqlChars(Typed.Csoport_ACL_Orokles_Mod); }
            set { Typed.Csoport_ACL_Orokles_Mod = Utility.SetSqlCharsFromString(value, Typed.Csoport_ACL_Orokles_Mod); }                                            
        }
                   
           
        /// <summary>
        /// Jogtargy_Tabla_Nev property </summary>
        public String Jogtargy_Tabla_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Jogtargy_Tabla_Nev); }
            set { Typed.Jogtargy_Tabla_Nev = Utility.SetSqlStringFromString(value, Typed.Jogtargy_Tabla_Nev); }                                            
        }
                   
           
        /// <summary>
        /// Jogtargy_Sor_Id property </summary>
        public String Jogtargy_Sor_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Jogtargy_Sor_Id); }
            set { Typed.Jogtargy_Sor_Id = Utility.SetSqlGuidFromString(value, Typed.Jogtargy_Sor_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}