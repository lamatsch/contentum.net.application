
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Adatkiegeszitesek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Adatkiegeszitesek : BaseKRT_Adatkiegeszitesek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_Kapcsolt property </summary>
        public String ObjTip_Id_Kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_Kapcsolt); }
            set { Typed.ObjTip_Id_Kapcsolt = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_Kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Kapcsolt property </summary>
        public String Obj_Id_Kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Kapcsolt); }
            set { Typed.Obj_Id_Kapcsolt = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_Adat property </summary>
        public String ObjTip_Id_Adat
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_Adat); }
            set { Typed.ObjTip_Id_Adat = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_Adat); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Adat property </summary>
        public String Obj_Id_Adat
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Adat); }
            set { Typed.Obj_Id_Adat = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Adat); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}