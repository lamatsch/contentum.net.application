
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Bankszamlaszamok BusinessDocument Class
    /// Partnerek bankszámlaszáma(i)
    /// </summary>
    [Serializable()]
    public class KRT_Bankszamlaszamok : BaseKRT_Bankszamlaszamok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// Bankszamlaszam property
        /// Bankszámlaszám
        /// </summary>
        public String Bankszamlaszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Bankszamlaszam); }
            set { Typed.Bankszamlaszam = Utility.SetSqlStringFromString(value, Typed.Bankszamlaszam); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_Bank property
        /// Partner id
        /// </summary>
        public String Partner_Id_Bank
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Bank); }
            set { Typed.Partner_Id_Bank = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Bank); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}