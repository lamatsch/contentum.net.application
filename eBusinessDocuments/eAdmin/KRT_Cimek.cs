
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Cimek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Cimek : BaseKRT_Cimek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Kategoria property </summary>
        public String Kategoria
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kategoria); }
            set { Typed.Kategoria = Utility.SetSqlCharsFromString(value, Typed.Kategoria); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// KulsoAzonositok property </summary>
        public String KulsoAzonositok
        {
            get { return Utility.GetStringFromSqlString(Typed.KulsoAzonositok); }
            set { Typed.KulsoAzonositok = Utility.SetSqlStringFromString(value, Typed.KulsoAzonositok); }                                            
        }
                   
           
        /// <summary>
        /// Forras property </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlChars(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlCharsFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// Orszag_Id property </summary>
        public String Orszag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Orszag_Id); }
            set { Typed.Orszag_Id = Utility.SetSqlGuidFromString(value, Typed.Orszag_Id); }                                            
        }
                   
           
        /// <summary>
        /// OrszagNev property </summary>
        public String OrszagNev
        {
            get { return Utility.GetStringFromSqlString(Typed.OrszagNev); }
            set { Typed.OrszagNev = Utility.SetSqlStringFromString(value, Typed.OrszagNev); }                                            
        }
                   
           
        /// <summary>
        /// Telepules_Id property </summary>
        public String Telepules_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Telepules_Id); }
            set { Typed.Telepules_Id = Utility.SetSqlGuidFromString(value, Typed.Telepules_Id); }                                            
        }
                   
           
        /// <summary>
        /// TelepulesNev property </summary>
        public String TelepulesNev
        {
            get { return Utility.GetStringFromSqlString(Typed.TelepulesNev); }
            set { Typed.TelepulesNev = Utility.SetSqlStringFromString(value, Typed.TelepulesNev); }                                            
        }
                   
           
        /// <summary>
        /// IRSZ property </summary>
        public String IRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.IRSZ); }
            set { Typed.IRSZ = Utility.SetSqlStringFromString(value, Typed.IRSZ); }                                            
        }
                   
           
        /// <summary>
        /// CimTobbi property </summary>
        public String CimTobbi
        {
            get { return Utility.GetStringFromSqlString(Typed.CimTobbi); }
            set { Typed.CimTobbi = Utility.SetSqlStringFromString(value, Typed.CimTobbi); }                                            
        }
                   
           
        /// <summary>
        /// Kozterulet_Id property </summary>
        public String Kozterulet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kozterulet_Id); }
            set { Typed.Kozterulet_Id = Utility.SetSqlGuidFromString(value, Typed.Kozterulet_Id); }                                            
        }
                   
           
        /// <summary>
        /// KozteruletNev property </summary>
        public String KozteruletNev
        {
            get { return Utility.GetStringFromSqlString(Typed.KozteruletNev); }
            set { Typed.KozteruletNev = Utility.SetSqlStringFromString(value, Typed.KozteruletNev); }                                            
        }
                   
           
        /// <summary>
        /// KozteruletTipus_Id property </summary>
        public String KozteruletTipus_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KozteruletTipus_Id); }
            set { Typed.KozteruletTipus_Id = Utility.SetSqlGuidFromString(value, Typed.KozteruletTipus_Id); }                                            
        }
                   
           
        /// <summary>
        /// KozteruletTipusNev property </summary>
        public String KozteruletTipusNev
        {
            get { return Utility.GetStringFromSqlString(Typed.KozteruletTipusNev); }
            set { Typed.KozteruletTipusNev = Utility.SetSqlStringFromString(value, Typed.KozteruletTipusNev); }                                            
        }
                   
           
        /// <summary>
        /// Hazszam property </summary>
        public String Hazszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Hazszam); }
            set { Typed.Hazszam = Utility.SetSqlStringFromString(value, Typed.Hazszam); }                                            
        }
                   
           
        /// <summary>
        /// Hazszamig property </summary>
        public String Hazszamig
        {
            get { return Utility.GetStringFromSqlString(Typed.Hazszamig); }
            set { Typed.Hazszamig = Utility.SetSqlStringFromString(value, Typed.Hazszamig); }                                            
        }
                   
           
        /// <summary>
        /// HazszamBetujel property </summary>
        public String HazszamBetujel
        {
            get { return Utility.GetStringFromSqlString(Typed.HazszamBetujel); }
            set { Typed.HazszamBetujel = Utility.SetSqlStringFromString(value, Typed.HazszamBetujel); }                                            
        }
                   
           
        /// <summary>
        /// MindketOldal property </summary>
        public String MindketOldal
        {
            get { return Utility.GetStringFromSqlChars(Typed.MindketOldal); }
            set { Typed.MindketOldal = Utility.SetSqlCharsFromString(value, Typed.MindketOldal); }                                            
        }
                   
           
        /// <summary>
        /// HRSZ property </summary>
        public String HRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.HRSZ); }
            set { Typed.HRSZ = Utility.SetSqlStringFromString(value, Typed.HRSZ); }                                            
        }
                   
           
        /// <summary>
        /// Lepcsohaz property </summary>
        public String Lepcsohaz
        {
            get { return Utility.GetStringFromSqlString(Typed.Lepcsohaz); }
            set { Typed.Lepcsohaz = Utility.SetSqlStringFromString(value, Typed.Lepcsohaz); }                                            
        }
                   
           
        /// <summary>
        /// Szint property </summary>
        public String Szint
        {
            get { return Utility.GetStringFromSqlString(Typed.Szint); }
            set { Typed.Szint = Utility.SetSqlStringFromString(value, Typed.Szint); }                                            
        }
                   
           
        /// <summary>
        /// Ajto property </summary>
        public String Ajto
        {
            get { return Utility.GetStringFromSqlString(Typed.Ajto); }
            set { Typed.Ajto = Utility.SetSqlStringFromString(value, Typed.Ajto); }                                            
        }
                   
           
        /// <summary>
        /// AjtoBetujel property </summary>
        public String AjtoBetujel
        {
            get { return Utility.GetStringFromSqlString(Typed.AjtoBetujel); }
            set { Typed.AjtoBetujel = Utility.SetSqlStringFromString(value, Typed.AjtoBetujel); }                                            
        }
                   
           
        /// <summary>
        /// Tobbi property </summary>
        public String Tobbi
        {
            get { return Utility.GetStringFromSqlString(Typed.Tobbi); }
            set { Typed.Tobbi = Utility.SetSqlStringFromString(value, Typed.Tobbi); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}