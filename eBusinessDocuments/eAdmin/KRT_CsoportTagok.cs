
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_CsoportTagok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_CsoportTagok : BaseKRT_CsoportTagok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Jogalany property </summary>
        public String Csoport_Id_Jogalany
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Jogalany); }
            set { Typed.Csoport_Id_Jogalany = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Jogalany); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_Jogalany property </summary>
        public String ObjTip_Id_Jogalany
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_Jogalany); }
            set { Typed.ObjTip_Id_Jogalany = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_Jogalany); }                                            
        }
                   
           
        /// <summary>
        /// ErtesitesMailCim property </summary>
        public String ErtesitesMailCim
        {
            get { return Utility.GetStringFromSqlString(Typed.ErtesitesMailCim); }
            set { Typed.ErtesitesMailCim = Utility.SetSqlStringFromString(value, Typed.ErtesitesMailCim); }                                            
        }
                   
           
        /// <summary>
        /// ErtesitesKell property </summary>
        public String ErtesitesKell
        {
            get { return Utility.GetStringFromSqlChars(Typed.ErtesitesKell); }
            set { Typed.ErtesitesKell = Utility.SetSqlCharsFromString(value, Typed.ErtesitesKell); }                                            
        }
                   
           
        /// <summary>
        /// System property </summary>
        public String System
        {
            get { return Utility.GetStringFromSqlChars(Typed.System); }
            set { Typed.System = Utility.SetSqlCharsFromString(value, Typed.System); }                                            
        }
                   
           
        /// <summary>
        /// Orokolheto property </summary>
        public String Orokolheto
        {
            get { return Utility.GetStringFromSqlChars(Typed.Orokolheto); }
            set { Typed.Orokolheto = Utility.SetSqlCharsFromString(value, Typed.Orokolheto); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}