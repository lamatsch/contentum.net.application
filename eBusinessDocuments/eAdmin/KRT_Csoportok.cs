
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Csoportok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Csoportok : BaseKRT_Csoportok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Kod property
        /// Csoport k�dja
        /// </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Csoport megnevez�se
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// Csoportt�pusok KCS: CSOPORTTIPUS
        /// 0 - Szervezet
        /// 1 - Dolgoz�
        /// 2 - Szerepk�r
        /// 4 - Projekt
        /// 5 - Test�let, bizotts�g
        /// 6 - Hivatal vezet�s
        /// 7 - �nkorm�nyzat
        /// 9 - Org
        /// M - Mindenki
        /// R - Rendszergazd�k
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Jogalany property
        /// Jogalanyk�nt �rtelmezhet� csoport?  (1- Iigen, /0 vagy NULL - Nem)
        /// </summary>
        public String Jogalany
        {
            get { return Utility.GetStringFromSqlChars(Typed.Jogalany); }
            set { Typed.Jogalany = Utility.SetSqlCharsFromString(value, Typed.Jogalany); }                                            
        }
                   
           
        /// <summary>
        /// ErtesitesEmail property
        /// �rtes�t�si e_mail c�m (2007.06.18, KovAx)
        /// </summary>
        public String ErtesitesEmail
        {
            get { return Utility.GetStringFromSqlString(Typed.ErtesitesEmail); }
            set { Typed.ErtesitesEmail = Utility.SetSqlStringFromString(value, Typed.ErtesitesEmail); }                                            
        }
                   
           
        /// <summary>
        /// System property
        /// Csak a rendszer kezelheti? 1-I/0-N
        /// </summary>
        public String System
        {
            get { return Utility.GetStringFromSqlChars(Typed.System); }
            set { Typed.System = Utility.SetSqlCharsFromString(value, Typed.System); }                                            
        }
                   
           
        /// <summary>
        /// Adatforras property
        /// AD-b�l intef�szelve (A),  vagy manu�lisan (M).
        /// </summary>
        public String Adatforras
        {
            get { return Utility.GetStringFromSqlChars(Typed.Adatforras); }
            set { Typed.Adatforras = Utility.SetSqlCharsFromString(value, Typed.Adatforras); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_Szulo property
        /// A SZULO_ID tartalma vagy egy partner ID (belso szervezet tipusu), vagy egy felhasznalo ID. 
        /// Ezt meg kell tudni mondani, hogy melyik. Ez a szulo tipus mezo feladata. 
        /// Kodszotaras ertek volt, most az obtipusok t�bl�b�l j�n...
        /// </summary>
        public String ObjTipus_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_Szulo); }
            set { Typed.ObjTipus_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// Kiszolgalhato property
        /// 1-igen, 0-nem
        /// </summary>
        public String Kiszolgalhato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kiszolgalhato); }
            set { Typed.Kiszolgalhato = Utility.SetSqlCharsFromString(value, Typed.Kiszolgalhato); }                                            
        }
                   
           
        /// <summary>
        /// JogosultsagOroklesMod property
        /// A felhaszn�l�k hozz�f�r�si jogosults�g�nak �r�kl�si m�dja adott szervezeten bel�l:
        ///  1- egym�s t�teleit l�thatj�k
        ///  0 vagy Null: - egym�s t�teleit nem l�thatj�k
        /// </summary>
        public String JogosultsagOroklesMod
        {
            get { return Utility.GetStringFromSqlChars(Typed.JogosultsagOroklesMod); }
            set { Typed.JogosultsagOroklesMod = Utility.SetSqlCharsFromString(value, Typed.JogosultsagOroklesMod); }                                            
        }
    
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}