
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Felhasznalo_Szerepkor BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Felhasznalo_Szerepkor : BaseKRT_Felhasznalo_Szerepkor
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// CsoportTag_Id property </summary>
        public String CsoportTag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CsoportTag_Id); }
            set { Typed.CsoportTag_Id = Utility.SetSqlGuidFromString(value, Typed.CsoportTag_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Szerepkor_Id property </summary>
        public String Szerepkor_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Szerepkor_Id); }
            set { Typed.Szerepkor_Id = Utility.SetSqlGuidFromString(value, Typed.Szerepkor_Id); }                                            
        }
                   
           
        /// <summary>
        /// Helyettesites_Id property </summary>
        public String Helyettesites_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Helyettesites_Id); }
            set { Typed.Helyettesites_Id = Utility.SetSqlGuidFromString(value, Typed.Helyettesites_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}