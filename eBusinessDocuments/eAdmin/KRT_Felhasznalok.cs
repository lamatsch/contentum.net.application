
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Felhasznalok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Felhasznalok : BaseKRT_Felhasznalok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id property </summary>
        public String Partner_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id); }
            set { Typed.Partner_id = Utility.SetSqlGuidFromString(value, Typed.Partner_id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// UserNev property </summary>
        public String UserNev
        {
            get { return Utility.GetStringFromSqlString(Typed.UserNev); }
            set { Typed.UserNev = Utility.SetSqlStringFromString(value, Typed.UserNev); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Jelszo property </summary>
        public String Jelszo
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelszo); }
            set { Typed.Jelszo = Utility.SetSqlStringFromString(value, Typed.Jelszo); }                                            
        }
                   
           
        /// <summary>
        /// JelszoLejaratIdo property </summary>
        public String JelszoLejaratIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.JelszoLejaratIdo); }
            set { Typed.JelszoLejaratIdo = Utility.SetSqlDateTimeFromString(value, Typed.JelszoLejaratIdo); }                                            
        }
                   
           
        /// <summary>
        /// System property </summary>
        public String System
        {
            get { return Utility.GetStringFromSqlChars(Typed.System); }
            set { Typed.System = Utility.SetSqlCharsFromString(value, Typed.System); }                                            
        }
                   
           
        /// <summary>
        /// Kiszolgalo property </summary>
        public String Kiszolgalo
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kiszolgalo); }
            set { Typed.Kiszolgalo = Utility.SetSqlCharsFromString(value, Typed.Kiszolgalo); }                                            
        }
                   
           
        /// <summary>
        /// DefaultPrivatKulcs property </summary>
        public String DefaultPrivatKulcs
        {
            get { return Utility.GetStringFromSqlString(Typed.DefaultPrivatKulcs); }
            set { Typed.DefaultPrivatKulcs = Utility.SetSqlStringFromString(value, Typed.DefaultPrivatKulcs); }                                            
        }
                
        /// <summary>
        /// WrongPassCnt property </summary>
        public String WrongPassCnt
        {
            get { return Utility.GetStringFromSqlInt32(Typed.WrongPassCnt); }
            set { Typed.WrongPassCnt = Utility.SetSqlInt32FromString(value, Typed.WrongPassCnt); }
        }


        /// <summary>
        /// Partner_Id_Munkahely property </summary>
        public String Partner_Id_Munkahely
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Munkahely); }
            set { Typed.Partner_Id_Munkahely = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Munkahely); }                                            
        }
                   
           
        /// <summary>
        /// MaxMinosites property </summary>
        public String MaxMinosites
        {
            get { return Utility.GetStringFromSqlString(Typed.MaxMinosites); }
            set { Typed.MaxMinosites = Utility.SetSqlStringFromString(value, Typed.MaxMinosites); }                                            
        }
                   
           
        /// <summary>
        /// EMail property </summary>
        public String EMail
        {
            get { return Utility.GetStringFromSqlString(Typed.EMail); }
            set { Typed.EMail = Utility.SetSqlStringFromString(value, Typed.EMail); }                                            
        }
                   
           
        /// <summary>
        /// Engedelyezett property </summary>
        public String Engedelyezett
        {
            get { return Utility.GetStringFromSqlChars(Typed.Engedelyezett); }
            set { Typed.Engedelyezett = Utility.SetSqlCharsFromString(value, Typed.Engedelyezett); }                                            
        }
                   
           
        /// <summary>
        /// Telefonszam property </summary>
        public String Telefonszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Telefonszam); }
            set { Typed.Telefonszam = Utility.SetSqlStringFromString(value, Typed.Telefonszam); }                                            
        }
                   
           
        /// <summary>
        /// Beosztas property </summary>
        public String Beosztas
        {
            get { return Utility.GetStringFromSqlString(Typed.Beosztas); }
            set { Typed.Beosztas = Utility.SetSqlStringFromString(value, Typed.Beosztas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}