﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    // <summary>
    /// KRT_HalozatiNyomtatok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Felhasznalok_Halozati_Nyomtatoi : BaseKRT_Felhasznalok_Halozati_Nyomtatoi
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// Felhasznalo_Id property
        /// Felhasznalo_Id
        /// </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }
        }


        /// <summary>
        /// Halozati_Nyomtato_Id property
        /// Halozati_Nyomtato_Id
        /// </summary>
        public String Halozati_Nyomtato_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Halozati_Nyomtato_Id); }
            set { Typed.Halozati_Nyomtato_Id = Utility.SetSqlGuidFromString(value, Typed.Halozati_Nyomtato_Id); }
        }

        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }
        }


        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }
        }
    }
}
