
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Forditasok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Forditasok : BaseKRT_Forditasok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// NyelvKod property
        /// pl. hu-HU
        /// </summary>
        public String NyelvKod
        {
            get { return Utility.GetStringFromSqlString(Typed.NyelvKod); }
            set { Typed.NyelvKod = Utility.SetSqlStringFromString(value, Typed.NyelvKod); }                                            
        }
                   
           
        /// <summary>
        /// OrgKod property
        /// pl. FPH, BOPMH, NMHH
        /// </summary>
        public String OrgKod
        {
            get { return Utility.GetStringFromSqlString(Typed.OrgKod); }
            set { Typed.OrgKod = Utility.SetSqlStringFromString(value, Typed.OrgKod); }                                            
        }
                   
           
        /// <summary>
        /// Modul property
        /// pl. eAdmin, eRecord
        /// </summary>
        public String Modul
        {
            get { return Utility.GetStringFromSqlString(Typed.Modul); }
            set { Typed.Modul = Utility.SetSqlStringFromString(value, Typed.Modul); }                                            
        }
                   
           
        /// <summary>
        /// Komponens property
        /// ASP-NET oldal vagy control neve, el�r�s� �ttal.
        /// pl EgyszerusitettIktatasForm.aspx
        /// pl.Component/ListHeader.ascx
        /// </summary>
        public String Komponens
        {
            get { return Utility.GetStringFromSqlString(Typed.Komponens); }
            set { Typed.Komponens = Utility.SetSqlStringFromString(value, Typed.Komponens); }                                            
        }
                   
           
        /// <summary>
        /// ObjAzonosito property
        /// A ford�tand� elem azonos�t�ja: label id, k�dt�r id.....
        /// </summary>
        public String ObjAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.ObjAzonosito); }
            set { Typed.ObjAzonosito = Utility.SetSqlStringFromString(value, Typed.ObjAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// Forditas property
        /// 
        /// </summary>
        public String Forditas
        {
            get { return Utility.GetStringFromSqlString(Typed.Forditas); }
            set { Typed.Forditas = Utility.SetSqlStringFromString(value, Typed.Forditas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}