
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Funkciok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Funkciok : BaseKRT_Funkciok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_AdatElem property </summary>
        public String ObjTipus_Id_AdatElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_AdatElem); }
            set { Typed.ObjTipus_Id_AdatElem = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_AdatElem); }                                            
        }
                   
           
        /// <summary>
        /// ObjStat_Id_Kezd property </summary>
        public String ObjStat_Id_Kezd
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjStat_Id_Kezd); }
            set { Typed.ObjStat_Id_Kezd = Utility.SetSqlGuidFromString(value, Typed.ObjStat_Id_Kezd); }                                            
        }
                   
           
        /// <summary>
        /// Alkalmazas_Id property </summary>
        public String Alkalmazas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Alkalmazas_Id); }
            set { Typed.Alkalmazas_Id = Utility.SetSqlGuidFromString(value, Typed.Alkalmazas_Id); }                                            
        }
                   
           
        /// <summary>
        /// Muvelet_Id property </summary>
        public String Muvelet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Muvelet_Id); }
            set { Typed.Muvelet_Id = Utility.SetSqlGuidFromString(value, Typed.Muvelet_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjStat_Id_Veg property </summary>
        public String ObjStat_Id_Veg
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjStat_Id_Veg); }
            set { Typed.ObjStat_Id_Veg = Utility.SetSqlGuidFromString(value, Typed.ObjStat_Id_Veg); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id_Szulo property </summary>
        public String Funkcio_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id_Szulo); }
            set { Typed.Funkcio_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// Csoportosito property </summary>
        public String Csoportosito
        {
            get { return Utility.GetStringFromSqlChars(Typed.Csoportosito); }
            set { Typed.Csoportosito = Utility.SetSqlCharsFromString(value, Typed.Csoportosito); }                                            
        }
                   
           
        /// <summary>
        /// Modosithato property </summary>
        public String Modosithato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Modosithato); }
            set { Typed.Modosithato = Utility.SetSqlCharsFromString(value, Typed.Modosithato); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// MunkanaploJelzo property </summary>
        public String MunkanaploJelzo
        {
            get { return Utility.GetStringFromSqlChars(Typed.MunkanaploJelzo); }
            set { Typed.MunkanaploJelzo = Utility.SetSqlCharsFromString(value, Typed.MunkanaploJelzo); }                                            
        }
                   
           
        /// <summary>
        /// FeladatJelzo property </summary>
        public String FeladatJelzo
        {
            get { return Utility.GetStringFromSqlChars(Typed.FeladatJelzo); }
            set { Typed.FeladatJelzo = Utility.SetSqlCharsFromString(value, Typed.FeladatJelzo); }                                            
        }
                   
           
        /// <summary>
        /// KeziFeladatJelzo property </summary>
        public String KeziFeladatJelzo
        {
            get { return Utility.GetStringFromSqlChars(Typed.KeziFeladatJelzo); }
            set { Typed.KeziFeladatJelzo = Utility.SetSqlCharsFromString(value, Typed.KeziFeladatJelzo); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}