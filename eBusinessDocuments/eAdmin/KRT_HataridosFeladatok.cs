
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_HataridosFeladatok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_HataridosFeladatok : BaseKRT_HataridosFeladatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_id property </summary>
        public String Felhasznalo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_id); }
            set { Typed.Felhasznalo_id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_id); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlInt32FromString(value, Typed.Prioritas); }                                            
        }
                   
           
        /// <summary>
        /// Feladat property </summary>
        public String Feladat
        {
            get { return Utility.GetStringFromSqlString(Typed.Feladat); }
            set { Typed.Feladat = Utility.SetSqlStringFromString(value, Typed.Feladat); }                                            
        }
                   
           
        /// <summary>
        /// ReagalasiIdo property </summary>
        public String ReagalasiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ReagalasiIdo); }
            set { Typed.ReagalasiIdo = Utility.SetSqlDateTimeFromString(value, Typed.ReagalasiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Scontro property </summary>
        public String Scontro
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Scontro); }
            set { Typed.Scontro = Utility.SetSqlDateTimeFromString(value, Typed.Scontro); }                                            
        }
                   
           
        /// <summary>
        /// Hatarido property </summary>
        public String Hatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Hatarido); }
            set { Typed.Hatarido = Utility.SetSqlDateTimeFromString(value, Typed.Hatarido); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}