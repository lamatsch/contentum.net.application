
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Helyettesitesek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Helyettesitesek : BaseKRT_Helyettesitesek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_ID_helyettesitett property </summary>
        public String Felhasznalo_ID_helyettesitett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_ID_helyettesitett); }
            set { Typed.Felhasznalo_ID_helyettesitett = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_ID_helyettesitett); }                                            
        }
                   
           
        /// <summary>
        /// CsoportTag_ID_helyettesitett property </summary>
        public String CsoportTag_ID_helyettesitett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CsoportTag_ID_helyettesitett); }
            set { Typed.CsoportTag_ID_helyettesitett = Utility.SetSqlGuidFromString(value, Typed.CsoportTag_ID_helyettesitett); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_ID_helyettesito property </summary>
        public String Felhasznalo_ID_helyettesito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_ID_helyettesito); }
            set { Typed.Felhasznalo_ID_helyettesito = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_ID_helyettesito); }                                            
        }
                   
           
        /// <summary>
        /// HelyettesitesMod property </summary>
        public String HelyettesitesMod
        {
            get { return Utility.GetStringFromSqlString(Typed.HelyettesitesMod); }
            set { Typed.HelyettesitesMod = Utility.SetSqlStringFromString(value, Typed.HelyettesitesMod); }                                            
        }
                   
           
        /// <summary>
        /// HelyettesitesKezd property </summary>
        public String HelyettesitesKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.HelyettesitesKezd); }
            set { Typed.HelyettesitesKezd = Utility.SetSqlDateTimeFromString(value, Typed.HelyettesitesKezd); }                                            
        }
                   
           
        /// <summary>
        /// HelyettesitesVege property </summary>
        public String HelyettesitesVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.HelyettesitesVege); }
            set { Typed.HelyettesitesVege = Utility.SetSqlDateTimeFromString(value, Typed.HelyettesitesVege); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}