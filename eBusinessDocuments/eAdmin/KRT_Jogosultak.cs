
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Jogosultak BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Jogosultak : BaseKRT_Jogosultak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Jogalany property </summary>
        public String Csoport_Id_Jogalany
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Jogalany); }
            set { Typed.Csoport_Id_Jogalany = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Jogalany); }                                            
        }
                   
           
        /// <summary>
        /// Jogszint property </summary>
        public String Jogszint
        {
            get { return Utility.GetStringFromSqlString(Typed.Jogszint); }
            set { Typed.Jogszint = Utility.SetSqlStringFromString(value, Typed.Jogszint); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// Orokolheto property </summary>
        public String Orokolheto
        {
            get { return Utility.GetStringFromSqlChars(Typed.Orokolheto); }
            set { Typed.Orokolheto = Utility.SetSqlCharsFromString(value, Typed.Orokolheto); }                                            
        }
                   
           
        /// <summary>
        /// Kezi property </summary>
        public String Kezi
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kezi); }
            set { Typed.Kezi = Utility.SetSqlCharsFromString(value, Typed.Kezi); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}