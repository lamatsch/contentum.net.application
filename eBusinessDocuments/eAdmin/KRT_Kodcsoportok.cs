
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodCsoportok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_KodCsoportok : BaseKRT_KodCsoportok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// KodTarak_Id_KodcsoportTipus property </summary>
        public String KodTarak_Id_KodcsoportTipus
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KodTarak_Id_KodcsoportTipus); }
            set { Typed.KodTarak_Id_KodcsoportTipus = Utility.SetSqlGuidFromString(value, Typed.KodTarak_Id_KodcsoportTipus); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Modosithato property </summary>
        public String Modosithato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Modosithato); }
            set { Typed.Modosithato = Utility.SetSqlCharsFromString(value, Typed.Modosithato); }                                            
        }
                   
           
        /// <summary>
        /// Hossz property </summary>
        public String Hossz
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Hossz); }
            set { Typed.Hossz = Utility.SetSqlInt32FromString(value, Typed.Hossz); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// BesorolasiSema property </summary>
        public String BesorolasiSema
        {
            get { return Utility.GetStringFromSqlChars(Typed.BesorolasiSema); }
            set { Typed.BesorolasiSema = Utility.SetSqlCharsFromString(value, Typed.BesorolasiSema); }                                            
        }
                   
           
        /// <summary>
        /// KiegAdat property </summary>
        public String KiegAdat
        {
            get { return Utility.GetStringFromSqlChars(Typed.KiegAdat); }
            set { Typed.KiegAdat = Utility.SetSqlCharsFromString(value, Typed.KiegAdat); }                                            
        }
                   
           
        /// <summary>
        /// KiegMezo property </summary>
        public String KiegMezo
        {
            get { return Utility.GetStringFromSqlString(Typed.KiegMezo); }
            set { Typed.KiegMezo = Utility.SetSqlStringFromString(value, Typed.KiegMezo); }                                            
        }
                   
           
        /// <summary>
        /// KiegAdattipus property </summary>
        public String KiegAdattipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.KiegAdattipus); }
            set { Typed.KiegAdattipus = Utility.SetSqlCharsFromString(value, Typed.KiegAdattipus); }                                            
        }
                   
           
        /// <summary>
        /// KiegSzotar property </summary>
        public String KiegSzotar
        {
            get { return Utility.GetStringFromSqlString(Typed.KiegSzotar); }
            set { Typed.KiegSzotar = Utility.SetSqlStringFromString(value, Typed.KiegSzotar); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}