
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodtarFuggoseg BusinessDocument Class
    /// F�gg� k�dt�rak
    /// </summary>
    [Serializable()]
    public class KRT_KodtarFuggoseg : BaseKRT_KodtarFuggoseg
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Vezerlo_KodCsoport_Id property
        /// Kodcsoport
        /// </summary>
        public String Vezerlo_KodCsoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Vezerlo_KodCsoport_Id); }
            set { Typed.Vezerlo_KodCsoport_Id = Utility.SetSqlGuidFromString(value, Typed.Vezerlo_KodCsoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Fuggo_KodCsoport_Id property
        /// Sok esetben a Kodt�r �rt�k konkr�t t�bl�t jelent... 
        /// Ezen esetekben (pl. kcs.JOGALANY_TIPUS) a v�gfelhaszn�l�s nem a Kod, hanem 
        /// az AdatElem (legt�bbsz�r t�bla) GUID -ja.
        /// Az�rt nevezz�k az oszlopot AdatElem -nek, mivel Meta defin�ci�t
        /// Min�s�t�siSzint-�gyK�d-Elj�r�siSzint-T�rgyk�d �sszerendel�sre is megadhatunk.
        /// </summary>
        public String Fuggo_KodCsoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Fuggo_KodCsoport_Id); }
            set { Typed.Fuggo_KodCsoport_Id = Utility.SetSqlGuidFromString(value, Typed.Fuggo_KodCsoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// Adat property
        /// 
        /// </summary>
        public String Adat
        {
            get { return Utility.GetStringFromSqlString(Typed.Adat); }
            set { Typed.Adat = Utility.SetSqlStringFromString(value, Typed.Adat); }                                            
        }
                   
           
        /// <summary>
        /// Aktiv property
        /// Review: Nvarchar(80) Sorrend
        /// </summary>
        public String Aktiv
        {
            get { return Utility.GetStringFromSqlChars(Typed.Aktiv); }
            set { Typed.Aktiv = Utility.SetSqlCharsFromString(value, Typed.Aktiv); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}