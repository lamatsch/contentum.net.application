
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_KodTarak BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_KodTarak : BaseKRT_KodTarak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// KodCsoport_Id property </summary>
        public String KodCsoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KodCsoport_Id); }
            set { Typed.KodCsoport_Id = Utility.SetSqlGuidFromString(value, Typed.KodCsoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_AdatElem property </summary>
        public String ObjTip_Id_AdatElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_AdatElem); }
            set { Typed.ObjTip_Id_AdatElem = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_AdatElem); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// RovidNev property </summary>
        public String RovidNev
        {
            get { return Utility.GetStringFromSqlString(Typed.RovidNev); }
            set { Typed.RovidNev = Utility.SetSqlStringFromString(value, Typed.RovidNev); }                                            
        }
                   
           
        /// <summary>
        /// Egyeb property </summary>
        public String Egyeb
        {
            get { return Utility.GetStringFromSqlString(Typed.Egyeb); }
            set { Typed.Egyeb = Utility.SetSqlStringFromString(value, Typed.Egyeb); }                                            
        }
                   
           
        /// <summary>
        /// Modosithato property </summary>
        public String Modosithato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Modosithato); }
            set { Typed.Modosithato = Utility.SetSqlCharsFromString(value, Typed.Modosithato); }                                            
        }
                   
           
        /// <summary>
        /// Sorrend property </summary>
        public String Sorrend
        {
            get { return Utility.GetStringFromSqlString(Typed.Sorrend); }
            set { Typed.Sorrend = Utility.SetSqlStringFromString(value, Typed.Sorrend); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}