
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_OBJEKTUMJOG_OROKLESEK BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_OBJEKTUMJOG_OROKLESEK : BaseKRT_OBJEKTUMJOG_OROKLESEK
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Tipus property </summary>
        public String Obj_Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Tipus); }
            set { Typed.Obj_Tipus = Utility.SetSqlStringFromString(value, Typed.Obj_Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Orokos property </summary>
        public String Obj_Id_Orokos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Orokos); }
            set { Typed.Obj_Id_Orokos = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Orokos); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Tipus_Orokos property </summary>
        public String Obj_Tipus_Orokos
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Tipus_Orokos); }
            set { Typed.Obj_Tipus_Orokos = Utility.SetSqlStringFromString(value, Typed.Obj_Tipus_Orokos); }                                            
        }
                   
           
        /// <summary>
        /// JogszintOrokolheto property </summary>
        public String JogszintOrokolheto
        {
            get { return Utility.GetStringFromSqlChars(Typed.JogszintOrokolheto); }
            set { Typed.JogszintOrokolheto = Utility.SetSqlCharsFromString(value, Typed.JogszintOrokolheto); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}