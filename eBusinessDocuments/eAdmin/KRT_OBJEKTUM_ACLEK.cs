
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_OBJEKTUM_ACLEK BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_OBJEKTUM_ACLEK : BaseKRT_OBJEKTUM_ACLEK
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Tipus property </summary>
        public String Obj_Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Tipus); }
            set { Typed.Obj_Tipus = Utility.SetSqlStringFromString(value, Typed.Obj_Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ACL_ID property </summary>
        public String ACL_ID
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ACL_ID); }
            set { Typed.ACL_ID = Utility.SetSqlGuidFromString(value, Typed.ACL_ID); }                                            
        }
                   
           
        /// <summary>
        /// Jogtargy_Sor_Id property </summary>
        public String Jogtargy_Sor_Id
        {
            get { return Utility.GetStringFromSqlChars(Typed.Jogtargy_Sor_Id); }
            set { Typed.Jogtargy_Sor_Id = Utility.SetSqlCharsFromString(value, Typed.Jogtargy_Sor_Id); }                                            
        }
                   
           
        /// <summary>
        /// OrokolhetoJogszint property </summary>
        public String OrokolhetoJogszint
        {
            get { return Utility.GetStringFromSqlChars(Typed.OrokolhetoJogszint); }
            set { Typed.OrokolhetoJogszint = Utility.SetSqlCharsFromString(value, Typed.OrokolhetoJogszint); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}