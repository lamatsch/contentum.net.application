
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ObjTipusok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_ObjTipusok : BaseKRT_ObjTipusok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTipus_Id_Tipus property </summary>
        public String ObjTipus_Id_Tipus
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTipus_Id_Tipus); }
            set { Typed.ObjTipus_Id_Tipus = Utility.SetSqlGuidFromString(value, Typed.ObjTipus_Id_Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Szulo property </summary>
        public String Obj_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Szulo); }
            set { Typed.Obj_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// KodCsoport_Id property </summary>
        public String KodCsoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KodCsoport_Id); }
            set { Typed.KodCsoport_Id = Utility.SetSqlGuidFromString(value, Typed.KodCsoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}