
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Partnerek BusinessDocument Class
    /// Az ERecord -ban ez t�rolja a szervezeteket (k�ls�, bels�).
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Partnerek : BaseKRT_Partnerek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Orszag_Id property
        /// Orsz�g Id
        /// </summary>
        public String Orszag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Orszag_Id); }
            set { Typed.Orszag_Id = Utility.SetSqlGuidFromString(value, Typed.Orszag_Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// KCS: Partner_Tipus_JDV -b�l, DNvarchar(2).
        /// 10: Szervezet
        /// 20: Szemely...
        /// 30: Alkalmaz�s (kiz�r�lag technikai userek r�sz�re)
        /// 
        /// 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Partner megnevezese
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// KulsoAzonositok property
        /// Tetsz�leges k�ls� azonos�t�k
        /// </summary>
        public String KulsoAzonositok
        {
            get { return Utility.GetStringFromSqlString(Typed.KulsoAzonositok); }
            set { Typed.KulsoAzonositok = Utility.SetSqlStringFromString(value, Typed.KulsoAzonositok); }                                            
        }
                   
           
        /// <summary>
        /// PublikusKulcs property
        /// Partner digit�lis al��r�s�hoz
        /// </summary>
        public String PublikusKulcs
        {
            get { return Utility.GetStringFromSqlString(Typed.PublikusKulcs); }
            set { Typed.PublikusKulcs = Utility.SetSqlStringFromString(value, Typed.PublikusKulcs); }                                            
        }
                   
           
        /// <summary>
        /// Kiszolgalo property
        /// ??
        /// </summary>
        public String Kiszolgalo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kiszolgalo); }
            set { Typed.Kiszolgalo = Utility.SetSqlGuidFromString(value, Typed.Kiszolgalo); }                                            
        }
                   
           
        /// <summary>
        /// LetrehozoSzervezet property
        /// L�trehoz� szervezet Id-je
        /// </summary>
        public String LetrehozoSzervezet
        {
            get { return Utility.GetStringFromSqlGuid(Typed.LetrehozoSzervezet); }
            set { Typed.LetrehozoSzervezet = Utility.SetSqlGuidFromString(value, Typed.LetrehozoSzervezet); }                                            
        }
                   
           
        /// <summary>
        /// MaxMinosites property
        /// nem haszn�lt
        /// </summary>
        public String MaxMinosites
        {
            get { return Utility.GetStringFromSqlString(Typed.MaxMinosites); }
            set { Typed.MaxMinosites = Utility.SetSqlStringFromString(value, Typed.MaxMinosites); }                                            
        }
                   
           
        /// <summary>
        /// MinositesKezdDat property
        /// nem haszn�lt
        /// </summary>
        public String MinositesKezdDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesKezdDat); }
            set { Typed.MinositesKezdDat = Utility.SetSqlDateTimeFromString(value, Typed.MinositesKezdDat); }                                            
        }
                   
           
        /// <summary>
        /// MinositesVegDat property
        /// nem haszn�lt
        /// </summary>
        public String MinositesVegDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesVegDat); }
            set { Typed.MinositesVegDat = Utility.SetSqlDateTimeFromString(value, Typed.MinositesVegDat); }                                            
        }
                   
           
        /// <summary>
        /// MaxMinositesSzervezet property
        /// nem haszn�lt
        /// </summary>
        public String MaxMinositesSzervezet
        {
            get { return Utility.GetStringFromSqlString(Typed.MaxMinositesSzervezet); }
            set { Typed.MaxMinositesSzervezet = Utility.SetSqlStringFromString(value, Typed.MaxMinositesSzervezet); }                                            
        }
                   
           
        /// <summary>
        /// Belso property
        /// 1-bels�, 0-k�ls�
        /// </summary>
        public String Belso
        {
            get { return Utility.GetStringFromSqlChars(Typed.Belso); }
            set { Typed.Belso = Utility.SetSqlCharsFromString(value, Typed.Belso); }                                            
        }
                   
           
        /// <summary>
        /// Forras property
        /// ??Partner adatbazisba kerulesenk forrasa. Leggyakoribb a felhasznalo kezi rogzites, de lehet cimtar, SAP, OApplication, stb.
        /// </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlChars(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlCharsFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoHasznalat property
        /// 
        /// </summary>
        public String UtolsoHasznalat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoHasznalat); }
            set { Typed.UtolsoHasznalat = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoHasznalat); }                                            
        }

        public string AspAdoTorolve
        {
            get { return Utility.GetStringFromSqlChars(Typed.AspAdoTorolve); }
            set { Typed.AspAdoTorolve = Utility.SetSqlCharsFromString(value, Typed.AspAdoTorolve); }
        }
    }  
}