
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Parameterek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Parameterek : BaseKRT_Parameterek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_id property </summary>
        public String Felhasznalo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_id); }
            set { Typed.Felhasznalo_id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Ertek property </summary>
        public String Ertek
        {
            get { return Utility.GetStringFromSqlString(Typed.Ertek); }
            set { Typed.Ertek = Utility.SetSqlStringFromString(value, Typed.Ertek); }                                            
        }
                   
           
        /// <summary>
        /// Karbantarthato property </summary>
        public String Karbantarthato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Karbantarthato); }
            set { Typed.Karbantarthato = Utility.SetSqlCharsFromString(value, Typed.Karbantarthato); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}