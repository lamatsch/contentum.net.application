
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_PartnerKapcsolatok : BaseKRT_PartnerKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id property </summary>
        public String Partner_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id); }
            set { Typed.Partner_id = Utility.SetSqlGuidFromString(value, Typed.Partner_id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id_kapcsolt property </summary>
        public String Partner_id_kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id_kapcsolt); }
            set { Typed.Partner_id_kapcsolt = Utility.SetSqlGuidFromString(value, Typed.Partner_id_kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}