
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerMinositesek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_PartnerMinositesek : BaseKRT_PartnerMinositesek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ALLAPOT property </summary>
        public String ALLAPOT
        {
            get { return Utility.GetStringFromSqlChars(Typed.ALLAPOT); }
            set { Typed.ALLAPOT = Utility.SetSqlCharsFromString(value, Typed.ALLAPOT); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id property </summary>
        public String Partner_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id); }
            set { Typed.Partner_id = Utility.SetSqlGuidFromString(value, Typed.Partner_id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_id_kero property </summary>
        public String Felhasznalo_id_kero
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_id_kero); }
            set { Typed.Felhasznalo_id_kero = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_id_kero); }                                            
        }
                   
           
        /// <summary>
        /// KertMinosites property </summary>
        public String KertMinosites
        {
            get { return Utility.GetStringFromSqlString(Typed.KertMinosites); }
            set { Typed.KertMinosites = Utility.SetSqlStringFromString(value, Typed.KertMinosites); }                                            
        }
                   
           
        /// <summary>
        /// KertKezdDat property </summary>
        public String KertKezdDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KertKezdDat); }
            set { Typed.KertKezdDat = Utility.SetSqlDateTimeFromString(value, Typed.KertKezdDat); }                                            
        }
                   
           
        /// <summary>
        /// KertVegeDat property </summary>
        public String KertVegeDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KertVegeDat); }
            set { Typed.KertVegeDat = Utility.SetSqlDateTimeFromString(value, Typed.KertVegeDat); }                                            
        }
                   
           
        /// <summary>
        /// KerelemAzonosito property </summary>
        public String KerelemAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.KerelemAzonosito); }
            set { Typed.KerelemAzonosito = Utility.SetSqlStringFromString(value, Typed.KerelemAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// KerelemBeadIdo property </summary>
        public String KerelemBeadIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KerelemBeadIdo); }
            set { Typed.KerelemBeadIdo = Utility.SetSqlDateTimeFromString(value, Typed.KerelemBeadIdo); }                                            
        }
                   
           
        /// <summary>
        /// KerelemDontesIdo property </summary>
        public String KerelemDontesIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KerelemDontesIdo); }
            set { Typed.KerelemDontesIdo = Utility.SetSqlDateTimeFromString(value, Typed.KerelemDontesIdo); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_id_donto property </summary>
        public String Felhasznalo_id_donto
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_id_donto); }
            set { Typed.Felhasznalo_id_donto = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_id_donto); }                                            
        }
                   
           
        /// <summary>
        /// DontesAzonosito property </summary>
        public String DontesAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.DontesAzonosito); }
            set { Typed.DontesAzonosito = Utility.SetSqlStringFromString(value, Typed.DontesAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// Minosites property </summary>
        public String Minosites
        {
            get { return Utility.GetStringFromSqlString(Typed.Minosites); }
            set { Typed.Minosites = Utility.SetSqlStringFromString(value, Typed.Minosites); }                                            
        }
                   
           
        /// <summary>
        /// MinositesKezdDat property </summary>
        public String MinositesKezdDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesKezdDat); }
            set { Typed.MinositesKezdDat = Utility.SetSqlDateTimeFromString(value, Typed.MinositesKezdDat); }                                            
        }
                   
           
        /// <summary>
        /// MinositesVegDat property </summary>
        public String MinositesVegDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesVegDat); }
            set { Typed.MinositesVegDat = Utility.SetSqlDateTimeFromString(value, Typed.MinositesVegDat); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}