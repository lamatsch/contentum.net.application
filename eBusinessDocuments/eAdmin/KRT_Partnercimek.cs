
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_PartnerCimek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_PartnerCimek : BaseKRT_PartnerCimek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id property </summary>
        public String Partner_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id); }
            set { Typed.Partner_id = Utility.SetSqlGuidFromString(value, Typed.Partner_id); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id property </summary>
        public String Cim_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id); }
            set { Typed.Cim_Id = Utility.SetSqlGuidFromString(value, Typed.Cim_Id); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoHasznalatSiker property </summary>
        public String UtolsoHasznalatSiker
        {
            get { return Utility.GetStringFromSqlChars(Typed.UtolsoHasznalatSiker); }
            set { Typed.UtolsoHasznalatSiker = Utility.SetSqlCharsFromString(value, Typed.UtolsoHasznalatSiker); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoHasznalatiIdo property </summary>
        public String UtolsoHasznalatiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoHasznalatiIdo); }
            set { Typed.UtolsoHasznalatiIdo = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoHasznalatiIdo); }                                            
        }
                   
           
        /// <summary>
        /// eMailKuldes property </summary>
        public String eMailKuldes
        {
            get { return Utility.GetStringFromSqlChars(Typed.eMailKuldes); }
            set { Typed.eMailKuldes = Utility.SetSqlCharsFromString(value, Typed.eMailKuldes); }                                            
        }
                   
           
        /// <summary>
        /// Fajta property </summary>
        public String Fajta
        {
            get { return Utility.GetStringFromSqlString(Typed.Fajta); }
            set { Typed.Fajta = Utility.SetSqlStringFromString(value, Typed.Fajta); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}