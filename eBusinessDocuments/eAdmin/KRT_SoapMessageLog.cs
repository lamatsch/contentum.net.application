
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_SoapMessageLog BusinessDocument Class
    /// A Contentum.Net kimen� / bej�v� webszerv�z h�v�sok naploz�s�hoz sz�ks�ges adatmodell.
    /// </summary>
    [Serializable()]
    public class KRT_SoapMessageLog : BaseKRT_SoapMessageLog
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// ForrasRendszer property
        /// 
        /// </summary>
        public String ForrasRendszer
        {
            get { return Utility.GetStringFromSqlString(Typed.ForrasRendszer); }
            set { Typed.ForrasRendszer = Utility.SetSqlStringFromString(value, Typed.ForrasRendszer); }                                            
        }
                   
           
        /// <summary>
        /// Url property
        /// 
        /// </summary>
        public String Url
        {
            get { return Utility.GetStringFromSqlString(Typed.Url); }
            set { Typed.Url = Utility.SetSqlStringFromString(value, Typed.Url); }                                            
        }
                   
           
        /// <summary>
        /// CelRendszer property
        /// 
        /// </summary>
        public String CelRendszer
        {
            get { return Utility.GetStringFromSqlString(Typed.CelRendszer); }
            set { Typed.CelRendszer = Utility.SetSqlStringFromString(value, Typed.CelRendszer); }                                            
        }
                   
           
        /// <summary>
        /// MetodusNeve property
        /// 
        /// </summary>
        public String MetodusNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.MetodusNeve); }
            set { Typed.MetodusNeve = Utility.SetSqlStringFromString(value, Typed.MetodusNeve); }                                            
        }
                   
           
        /// <summary>
        /// KeresFogadas property
        /// 
        /// </summary>
        public String KeresFogadas
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KeresFogadas); }
            set { Typed.KeresFogadas = Utility.SetSqlDateTimeFromString(value, Typed.KeresFogadas); }                                            
        }
                   
           
        /// <summary>
        /// KeresKuldes property
        /// 
        /// </summary>
        public String KeresKuldes
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KeresKuldes); }
            set { Typed.KeresKuldes = Utility.SetSqlDateTimeFromString(value, Typed.KeresKuldes); }                                            
        }
                   
           
        /// <summary>
        /// RequestData property
        /// 
        /// </summary>
        public String RequestData
        {
            get { return Utility.GetStringFromSqlString(Typed.RequestData); }
            set { Typed.RequestData = Utility.SetSqlStringFromString(value, Typed.RequestData); }                                            
        }
                   
           
        /// <summary>
        /// ResponseData property
        /// 
        /// </summary>
        public String ResponseData
        {
            get { return Utility.GetStringFromSqlString(Typed.ResponseData); }
            set { Typed.ResponseData = Utility.SetSqlStringFromString(value, Typed.ResponseData); }                                            
        }
                   
           
        /// <summary>
        /// FuttatoFelhasznalo property
        /// 
        /// </summary>
        public String FuttatoFelhasznalo
        {
            get { return Utility.GetStringFromSqlString(Typed.FuttatoFelhasznalo); }
            set { Typed.FuttatoFelhasznalo = Utility.SetSqlStringFromString(value, Typed.FuttatoFelhasznalo); }                                            
        }
                   
           
        /// <summary>
        /// Local property
        /// 
        /// </summary>
        public String Local
        {
            get { return Utility.GetStringFromSqlChars(Typed.Local); }
            set { Typed.Local = Utility.SetSqlCharsFromString(value, Typed.Local); }                                            
        }
                   
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        public String Hiba
        {
            get { return Utility.GetStringFromSqlString(Typed.Hiba); }
            set { Typed.Hiba = Utility.SetSqlStringFromString(value, Typed.Hiba); }                                            
        }
                   
           
        /// <summary>
        /// Sikeres property
        /// 
        /// </summary>
        public String Sikeres
        {
            get { return Utility.GetStringFromSqlChars(Typed.Sikeres); }
            set { Typed.Sikeres = Utility.SetSqlCharsFromString(value, Typed.Sikeres); }                                            
        }
                           }
   
}