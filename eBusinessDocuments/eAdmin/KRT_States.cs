
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_States BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_States : BaseKRT_States
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Kod property </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Stat_Id_Szulo property </summary>
        public String Stat_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Stat_Id_Szulo); }
            set { Typed.Stat_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.Stat_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}