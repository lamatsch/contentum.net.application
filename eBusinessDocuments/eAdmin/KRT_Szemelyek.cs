
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Szemelyek BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Szemelyek : BaseKRT_Szemelyek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// AnyjaNeve property
        /// Anyja neve. Ha tudjuk, akkor reszletesen az ANYJA_VEZETEKNEVE+ANYJA_KERESZTNEVEI.
        /// </summary>
        public String AnyjaNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.AnyjaNeve); }
            set { Typed.AnyjaNeve = Utility.SetSqlStringFromString(value, Typed.AnyjaNeve); }                                            
        }
                   
           
        /// <summary>
        /// AnyjaNeveCsaladiNev property
        /// 
        /// </summary>
        public String AnyjaNeveCsaladiNev
        {
            get { return Utility.GetStringFromSqlString(Typed.AnyjaNeveCsaladiNev); }
            set { Typed.AnyjaNeveCsaladiNev = Utility.SetSqlStringFromString(value, Typed.AnyjaNeveCsaladiNev); }                                            
        }
                   
           
        /// <summary>
        /// AnyjaNeveElsoUtonev property
        /// 
        /// </summary>
        public String AnyjaNeveElsoUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.AnyjaNeveElsoUtonev); }
            set { Typed.AnyjaNeveElsoUtonev = Utility.SetSqlStringFromString(value, Typed.AnyjaNeveElsoUtonev); }                                            
        }
                   
           
        /// <summary>
        /// AnyjaNeveTovabbiUtonev property
        /// 
        /// </summary>
        public String AnyjaNeveTovabbiUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.AnyjaNeveTovabbiUtonev); }
            set { Typed.AnyjaNeveTovabbiUtonev = Utility.SetSqlStringFromString(value, Typed.AnyjaNeveTovabbiUtonev); }                                            
        }
                   
           
        /// <summary>
        /// ApjaNeve property
        /// Apja neve (KANYV-igeny). Ha tudjuk, akkor az APJA_VEZETEKNEVE+APJA_KERESZTNEVEI.
        /// </summary>
        public String ApjaNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.ApjaNeve); }
            set { Typed.ApjaNeve = Utility.SetSqlStringFromString(value, Typed.ApjaNeve); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiNev property
        /// Most elozo nev a hivatalos megnevezese.
        /// </summary>
        public String SzuletesiNev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiNev); }
            set { Typed.SzuletesiNev = Utility.SetSqlStringFromString(value, Typed.SzuletesiNev); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiCsaladiNev property
        /// 
        /// </summary>
        public String SzuletesiCsaladiNev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiCsaladiNev); }
            set { Typed.SzuletesiCsaladiNev = Utility.SetSqlStringFromString(value, Typed.SzuletesiCsaladiNev); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiElsoUtonev property
        /// 
        /// </summary>
        public String SzuletesiElsoUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiElsoUtonev); }
            set { Typed.SzuletesiElsoUtonev = Utility.SetSqlStringFromString(value, Typed.SzuletesiElsoUtonev); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiTovabbiUtonev property
        /// 
        /// </summary>
        public String SzuletesiTovabbiUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiTovabbiUtonev); }
            set { Typed.SzuletesiTovabbiUtonev = Utility.SetSqlStringFromString(value, Typed.SzuletesiTovabbiUtonev); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiOrszag property
        /// Szem�ly sz�let�si hely�nek orsz�ga
        /// </summary>
        public String SzuletesiOrszag
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiOrszag); }
            set { Typed.SzuletesiOrszag = Utility.SetSqlStringFromString(value, Typed.SzuletesiOrszag); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiOrszagId property
        /// Sz�let�si orsz�g Id
        /// </summary>
        public String SzuletesiOrszagId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SzuletesiOrszagId); }
            set { Typed.SzuletesiOrszagId = Utility.SetSqlGuidFromString(value, Typed.SzuletesiOrszagId); }                                            
        }
                   
           
        /// <summary>
        /// UjTitulis property
        /// Szem�ly titulusa: Dr, id.
        /// </summary>
        public String UjTitulis
        {
            get { return Utility.GetStringFromSqlString(Typed.UjTitulis); }
            set { Typed.UjTitulis = Utility.SetSqlStringFromString(value, Typed.UjTitulis); }                                            
        }
                   
           
        /// <summary>
        /// UjCsaladiNev property
        /// Szem�ly "�j" csal�dneve(i)
        /// </summary>
        public String UjCsaladiNev
        {
            get { return Utility.GetStringFromSqlString(Typed.UjCsaladiNev); }
            set { Typed.UjCsaladiNev = Utility.SetSqlStringFromString(value, Typed.UjCsaladiNev); }                                            
        }
                   
           
        /// <summary>
        /// UjUtonev property
        /// Szem�ly aktu�lis ut�neve
        /// </summary>
        public String UjUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.UjUtonev); }
            set { Typed.UjUtonev = Utility.SetSqlStringFromString(value, Typed.UjUtonev); }                                            
        }
                   
           
        /// <summary>
        /// UjTovabbiUtonev property
        /// 
        /// </summary>
        public String UjTovabbiUtonev
        {
            get { return Utility.GetStringFromSqlString(Typed.UjTovabbiUtonev); }
            set { Typed.UjTovabbiUtonev = Utility.SetSqlStringFromString(value, Typed.UjTovabbiUtonev); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiHely property
        /// Szuletesi hely szovegesen. Ha megvan, akkor a telepules ID a SZUL_HELY-ben van.
        /// </summary>
        public String SzuletesiHely
        {
            get { return Utility.GetStringFromSqlString(Typed.SzuletesiHely); }
            set { Typed.SzuletesiHely = Utility.SetSqlStringFromString(value, Typed.SzuletesiHely); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiHely_id property
        /// Review: mire mutat? A cimeket kell legel�sz�r Kovax -al megtervezni! Telepules ID. Szovegesen is megvan a SZUL_HELY_STR-ben.
        /// </summary>
        public String SzuletesiHely_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SzuletesiHely_id); }
            set { Typed.SzuletesiHely_id = Utility.SetSqlGuidFromString(value, Typed.SzuletesiHely_id); }                                            
        }
                   
           
        /// <summary>
        /// SzuletesiIdo property
        /// Szuletesi datum.
        /// </summary>
        public String SzuletesiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SzuletesiIdo); }
            set { Typed.SzuletesiIdo = Utility.SetSqlDateTimeFromString(value, Typed.SzuletesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Allampolgarsag property
        /// 
        /// </summary>
        public String Allampolgarsag
        {
            get { return Utility.GetStringFromSqlString(Typed.Allampolgarsag); }
            set { Typed.Allampolgarsag = Utility.SetSqlStringFromString(value, Typed.Allampolgarsag); }                                            
        }
                   
           
        /// <summary>
        /// TAJSzam property
        /// Szem�ly TAJ sz�ma
        /// </summary>
        public String TAJSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.TAJSzam); }
            set { Typed.TAJSzam = Utility.SetSqlStringFromString(value, Typed.TAJSzam); }                                            
        }
                   
           
        /// <summary>
        /// SZIGSzam property
        /// Aktualis szemelyi igazolvany szama.
        /// </summary>
        public String SZIGSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.SZIGSzam); }
            set { Typed.SZIGSzam = Utility.SetSqlStringFromString(value, Typed.SZIGSzam); }                                            
        }
                   
           
        /// <summary>
        /// Neme property
        /// Szem�ly neme
        /// F�rfi: 1
        /// N�: 2
        /// </summary>
        public String Neme
        {
            get { return Utility.GetStringFromSqlChars(Typed.Neme); }
            set { Typed.Neme = Utility.SetSqlCharsFromString(value, Typed.Neme); }                                            
        }
                   
           
        /// <summary>
        /// SzemelyiAzonosito property
        /// A regi szemelyi szam.
        /// </summary>
        public String SzemelyiAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.SzemelyiAzonosito); }
            set { Typed.SzemelyiAzonosito = Utility.SetSqlStringFromString(value, Typed.SzemelyiAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// Adoazonosito property
        /// Ad�azonosit� jel.
        /// </summary>
        public String Adoazonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Adoazonosito); }
            set { Typed.Adoazonosito = Utility.SetSqlStringFromString(value, Typed.Adoazonosito); }                                            
        }
                   
           
        /// <summary>
        /// Adoszam property
        /// Egyeni vallalkozonal, illetve adoszamos maganszemelynel van.
        /// </summary>
        public String Adoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Adoszam); }
            set { Typed.Adoszam = Utility.SetSqlStringFromString(value, Typed.Adoszam); }                                            
        }
                   
           
        /// <summary>
        /// KulfoldiAdoszamJelolo property
        /// Belf�ldi vagy k�lf�ldi ad�sz�m:
        /// Ha �rt�ke '1', az ad�sz�m k�lf�ldi (a magyar form�tum nem alkalmazhat�)
        /// Minden m�s �rt�k eset�n az ad�sz�m belf�ldi.
        /// </summary>
        public String KulfoldiAdoszamJelolo
        {
            get { return Utility.GetStringFromSqlChars(Typed.KulfoldiAdoszamJelolo); }
            set { Typed.KulfoldiAdoszamJelolo = Utility.SetSqlCharsFromString(value, Typed.KulfoldiAdoszamJelolo); }                                            
        }
                   
           
        /// <summary>
        /// Beosztas property
        /// 
        /// </summary>
        public String Beosztas
        {
            get { return Utility.GetStringFromSqlString(Typed.Beosztas); }
            set { Typed.Beosztas = Utility.SetSqlStringFromString(value, Typed.Beosztas); }                                            
        }
                   
           
        /// <summary>
        /// MinositesiSzint property
        /// 
        /// </summary>
        public String MinositesiSzint
        {
            get { return Utility.GetStringFromSqlString(Typed.MinositesiSzint); }
            set { Typed.MinositesiSzint = Utility.SetSqlStringFromString(value, Typed.MinositesiSzint); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}