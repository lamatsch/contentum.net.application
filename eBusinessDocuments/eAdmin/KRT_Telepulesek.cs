
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Telepulesek BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Telepulesek : BaseKRT_Telepulesek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// IRSZ property </summary>
        public String IRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.IRSZ); }
            set { Typed.IRSZ = Utility.SetSqlStringFromString(value, Typed.IRSZ); }                                            
        }
                   
           
        /// <summary>
        /// Telepules_Id_Fo property </summary>
        public String Telepules_Id_Fo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Telepules_Id_Fo); }
            set { Typed.Telepules_Id_Fo = Utility.SetSqlGuidFromString(value, Typed.Telepules_Id_Fo); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Orszag_Id property </summary>
        public String Orszag_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Orszag_Id); }
            set { Typed.Orszag_Id = Utility.SetSqlGuidFromString(value, Typed.Orszag_Id); }                                            
        }
                   
           
        /// <summary>
        /// Megye property </summary>
        public String Megye
        {
            get { return Utility.GetStringFromSqlString(Typed.Megye); }
            set { Typed.Megye = Utility.SetSqlStringFromString(value, Typed.Megye); }                                            
        }
                   
           
        /// <summary>
        /// Regio property </summary>
        public String Regio
        {
            get { return Utility.GetStringFromSqlString(Typed.Regio); }
            set { Typed.Regio = Utility.SetSqlStringFromString(value, Typed.Regio); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}