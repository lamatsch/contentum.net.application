
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_USER_SETTINGS BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_USER_SETTINGS : BaseKRT_USER_SETTINGS
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// BEALLITASTIPUS property </summary>
        public String BEALLITASTIPUS
        {
            get { return Utility.GetStringFromSqlChars(Typed.BEALLITASTIPUS); }
            set { Typed.BEALLITASTIPUS = Utility.SetSqlCharsFromString(value, Typed.BEALLITASTIPUS); }                                            
        }
                   
           
        /// <summary>
        /// BEALLITASOK_XML property </summary>
        public String BEALLITASOK_XML
        {
            get { return Utility.GetStringFromSqlString(Typed.BEALLITASOK_XML); }
            set { Typed.BEALLITASOK_XML = Utility.SetSqlStringFromString(value, Typed.BEALLITASOK_XML); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}