
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Vallalkozasok BusinessDocument Class
    /// CheckConstraint_fn
    /// </summary>
    [Serializable()]
    public class KRT_Vallalkozasok : BaseKRT_Vallalkozasok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// Adoszam property
        /// V�llalkoz�s ad�sz�ma
        /// </summary>
        public String Adoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Adoszam); }
            set { Typed.Adoszam = Utility.SetSqlStringFromString(value, Typed.Adoszam); }                                            
        }
                   
           
        /// <summary>
        /// KulfoldiAdoszamJelolo property
        /// Belf�ldi vagy k�lf�ldi ad�sz�m:
        /// Ha �rt�ke '1', az ad�sz�m k�lf�ldi (a magyar form�tum nem alkalmazhat�)
        /// Minden m�s �rt�k eset�n az ad�sz�m belf�ldi.
        /// </summary>
        public String KulfoldiAdoszamJelolo
        {
            get { return Utility.GetStringFromSqlChars(Typed.KulfoldiAdoszamJelolo); }
            set { Typed.KulfoldiAdoszamJelolo = Utility.SetSqlCharsFromString(value, Typed.KulfoldiAdoszamJelolo); }                                            
        }
                   
           
        /// <summary>
        /// TB_Torzsszam property
        /// ??
        /// </summary>
        public String TB_Torzsszam
        {
            get { return Utility.GetStringFromSqlString(Typed.TB_Torzsszam); }
            set { Typed.TB_Torzsszam = Utility.SetSqlStringFromString(value, Typed.TB_Torzsszam); }                                            
        }
                   
           
        /// <summary>
        /// Cegjegyzekszam property
        /// C�gb�r�s�gi c�gjegyz�ksz�m
        /// </summary>
        public String Cegjegyzekszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Cegjegyzekszam); }
            set { Typed.Cegjegyzekszam = Utility.SetSqlStringFromString(value, Typed.Cegjegyzekszam); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// KCS:CEGTIPUS, K�dt�rb�l, Nvarchar(2).
        /// pl.:
        /// 01 - V�llalkoz�
        /// 02 - Bt
        /// 03 - Kft
        /// 04 - Rt
        /// 05 - Kht
        /// 06 - Kkt 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}