﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;

/// <summary>
/// Summary description for Message
/// </summary>
namespace Contentum.eBusinessDocuments
{
    public class Message
    {
        private List<String> _AddressLogins = new List<string>();

        public List<String> AddressLogins
        {
            get { return _AddressLogins; }
            set { _AddressLogins = value; }
        }

        private string _Title;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _TitleLinkUrl;

        public string TitleLinkUrl
        {
            get { return _TitleLinkUrl; }
            set { this._TitleLinkUrl = value; }
        }

        private string _Body;

        public string Body
        {
            get { return _Body; }
            set { _Body = value; }
        }

        private string _BodyLinkUrl;

        public string BodyLinkUrl
        {
            get { return _BodyLinkUrl; }
            set { _BodyLinkUrl = value; }
        }

        private string _SkinName;

        public string SkinName
        {
            get { return _SkinName; }
            set { _SkinName = value; }
        }

        private int _DelaySeconds = -1;

        public int DelaySeconds
        {
            get { return _DelaySeconds; }
            set { _DelaySeconds = value; }
        }

        public Message()
        {
        }

        public void ValidateProperties()
        {
            if (AddressLogins.Count == 0)
            {
                throw new ArgumentException("Count = 0", "Message.AddressLogins");
            }

            if (this.Title == null)
            {
                throw new ArgumentNullException("Message.Title");
            }

            if (this.Body == null)
            {
                throw new ArgumentNullException("Message.Body");
            }
        }
    }
}
