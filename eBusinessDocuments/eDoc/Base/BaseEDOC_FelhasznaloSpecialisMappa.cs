
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EDOC_FelhasznaloSpecialisMappa BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEDOC_FelhasznaloSpecialisMappa
    {
        [System.Xml.Serialization.XmlType("BaseEDOC_FelhasznaloSpecialisMappaBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Tipus_Id = true;
               public bool Tipus_Id
               {
                   get { return _Tipus_Id; }
                   set { _Tipus_Id = value; }
               }
                                                            
                                 
               private bool _Mappa_Id = true;
               public bool Mappa_Id
               {
                   get { return _Mappa_Id; }
                   set { _Mappa_Id = value; }
               }
                                                            
                                 
               private bool _Mapp_Azonosito = true;
               public bool Mapp_Azonosito
               {
                   get { return _Mapp_Azonosito; }
                   set { _Mapp_Azonosito = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   Tipus = Value;               
                    
                   Tipus_Id = Value;               
                    
                   Mappa_Id = Value;               
                    
                   Mapp_Azonosito = Value;               
                    
                   Leiras = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEDOC_FelhasznaloSpecialisMappaBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Tipus_Id = SqlGuid.Null;
           
        /// <summary>
        /// Tipus_Id Base property </summary>
            public SqlGuid Tipus_Id
            {
                get { return _Tipus_Id; }
                set { _Tipus_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Mappa_Id = SqlGuid.Null;
           
        /// <summary>
        /// Mappa_Id Base property </summary>
            public SqlGuid Mappa_Id
            {
                get { return _Mappa_Id; }
                set { _Mappa_Id = value; }                                                        
            }        
                   
           
        private SqlString _Mapp_Azonosito = SqlString.Null;
           
        /// <summary>
        /// Mapp_Azonosito Base property </summary>
            public SqlString Mapp_Azonosito
            {
                get { return _Mapp_Azonosito; }
                set { _Mapp_Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}