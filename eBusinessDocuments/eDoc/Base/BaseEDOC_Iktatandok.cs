
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EDOC_Iktatandok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEDOC_Iktatandok
    {
        [System.Xml.Serialization.XmlType("BaseEDOC_IktatandokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_IktatastKero = true;
               public bool Felhasznalo_Id_IktatastKero
               {
                   get { return _Felhasznalo_Id_IktatastKero; }
                   set { _Felhasznalo_Id_IktatastKero = value; }
               }
                                                            
                                 
               private bool _IraIratok_Id = true;
               public bool IraIratok_Id
               {
                   get { return _IraIratok_Id; }
                   set { _IraIratok_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Leiras = Value;               
                    
                   Felhasznalo_Id_IktatastKero = Value;               
                    
                   IraIratok_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEDOC_IktatandokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_IktatastKero = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_IktatastKero Base property </summary>
            public SqlGuid Felhasznalo_Id_IktatastKero
            {
                get { return _Felhasznalo_Id_IktatastKero; }
                set { _Felhasznalo_Id_IktatastKero = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIratok_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIratok_Id Base property </summary>
            public SqlGuid IraIratok_Id
            {
                get { return _IraIratok_Id; }
                set { _IraIratok_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}