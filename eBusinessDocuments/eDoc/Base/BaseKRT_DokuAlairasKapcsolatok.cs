
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokuAlairasKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_DokuAlairasKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_DokuAlairasKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_id = true;
               public bool Dokumentum_id
               {
                   get { return _Dokumentum_id; }
                   set { _Dokumentum_id = value; }
               }
                                                            
                                 
               private bool _DokumentumAlairas_Id = true;
               public bool DokumentumAlairas_Id
               {
                   get { return _DokumentumAlairas_Id; }
                   set { _DokumentumAlairas_Id = value; }
               }
                                                            
                                 
               private bool _DokuKapcsolatSorrend = true;
               public bool DokuKapcsolatSorrend
               {
                   get { return _DokuKapcsolatSorrend; }
                   set { _DokuKapcsolatSorrend = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Dokumentum_id = Value;               
                    
                   DokumentumAlairas_Id = Value;               
                    
                   DokuKapcsolatSorrend = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_DokuAlairasKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_id Base property </summary>
            public SqlGuid Dokumentum_id
            {
                get { return _Dokumentum_id; }
                set { _Dokumentum_id = value; }                                                        
            }        
                   
           
        private SqlGuid _DokumentumAlairas_Id = SqlGuid.Null;
           
        /// <summary>
        /// DokumentumAlairas_Id Base property </summary>
            public SqlGuid DokumentumAlairas_Id
            {
                get { return _DokumentumAlairas_Id; }
                set { _DokumentumAlairas_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _DokuKapcsolatSorrend = SqlInt32.Null;
           
        /// <summary>
        /// DokuKapcsolatSorrend Base property </summary>
            public SqlInt32 DokuKapcsolatSorrend
            {
                get { return _DokuKapcsolatSorrend; }
                set { _DokuKapcsolatSorrend = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}