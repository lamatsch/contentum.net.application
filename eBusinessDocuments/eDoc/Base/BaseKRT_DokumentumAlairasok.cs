
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokumentumAlairasok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_DokumentumAlairasok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumAlairasokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id_Alairt = true;
               public bool Dokumentum_Id_Alairt
               {
                   get { return _Dokumentum_Id_Alairt; }
                   set { _Dokumentum_Id_Alairt = value; }
               }
                                                            
                                 
               private bool _AlairasMod = true;
               public bool AlairasMod
               {
                   get { return _AlairasMod; }
                   set { _AlairasMod = value; }
               }
                                                            
                                 
               private bool _AlairasSzabaly_Id = true;
               public bool AlairasSzabaly_Id
               {
                   get { return _AlairasSzabaly_Id; }
                   set { _AlairasSzabaly_Id = value; }
               }
                                                            
                                 
               private bool _AlairasRendben = true;
               public bool AlairasRendben
               {
                   get { return _AlairasRendben; }
                   set { _AlairasRendben = value; }
               }
                                                            
                                 
               private bool _KivarasiIdoVege = true;
               public bool KivarasiIdoVege
               {
                   get { return _KivarasiIdoVege; }
                   set { _KivarasiIdoVege = value; }
               }
                                                            
                                 
               private bool _AlairasVeglegRendben = true;
               public bool AlairasVeglegRendben
               {
                   get { return _AlairasVeglegRendben; }
                   set { _AlairasVeglegRendben = value; }
               }
                                                            
                                 
               private bool _Idopecset = true;
               public bool Idopecset
               {
                   get { return _Idopecset; }
                   set { _Idopecset = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Alairo = true;
               public bool Csoport_Id_Alairo
               {
                   get { return _Csoport_Id_Alairo; }
                   set { _Csoport_Id_Alairo = value; }
               }
                                                            
                                 
               private bool _AlairasTulajdonos = true;
               public bool AlairasTulajdonos
               {
                   get { return _AlairasTulajdonos; }
                   set { _AlairasTulajdonos = value; }
               }
                                                            
                                 
               private bool _Tanusitvany_Id = true;
               public bool Tanusitvany_Id
               {
                   get { return _Tanusitvany_Id; }
                   set { _Tanusitvany_Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Dokumentum_Id_Alairt = Value;               
                    
                   AlairasMod = Value;               
                    
                   AlairasSzabaly_Id = Value;               
                    
                   AlairasRendben = Value;               
                    
                   KivarasiIdoVege = Value;               
                    
                   AlairasVeglegRendben = Value;               
                    
                   Idopecset = Value;               
                    
                   Csoport_Id_Alairo = Value;               
                    
                   AlairasTulajdonos = Value;               
                    
                   Tanusitvany_Id = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumAlairasokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id_Alairt = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id_Alairt Base property </summary>
            public SqlGuid Dokumentum_Id_Alairt
            {
                get { return _Dokumentum_Id_Alairt; }
                set { _Dokumentum_Id_Alairt = value; }                                                        
            }        
                   
           
        private SqlString _AlairasMod = SqlString.Null;
           
        /// <summary>
        /// AlairasMod Base property </summary>
            public SqlString AlairasMod
            {
                get { return _AlairasMod; }
                set { _AlairasMod = value; }                                                        
            }        
                   
           
        private SqlGuid _AlairasSzabaly_Id = SqlGuid.Null;
           
        /// <summary>
        /// AlairasSzabaly_Id Base property </summary>
            public SqlGuid AlairasSzabaly_Id
            {
                get { return _AlairasSzabaly_Id; }
                set { _AlairasSzabaly_Id = value; }                                                        
            }        
                   
           
        private SqlChars _AlairasRendben = SqlChars.Null;
           
        /// <summary>
        /// AlairasRendben Base property </summary>
            public SqlChars AlairasRendben
            {
                get { return _AlairasRendben; }
                set { _AlairasRendben = value; }                                                        
            }        
                   
           
        private SqlDateTime _KivarasiIdoVege = SqlDateTime.Null;
           
        /// <summary>
        /// KivarasiIdoVege Base property </summary>
            public SqlDateTime KivarasiIdoVege
            {
                get { return _KivarasiIdoVege; }
                set { _KivarasiIdoVege = value; }                                                        
            }        
                   
           
        private SqlChars _AlairasVeglegRendben = SqlChars.Null;
           
        /// <summary>
        /// AlairasVeglegRendben Base property </summary>
            public SqlChars AlairasVeglegRendben
            {
                get { return _AlairasVeglegRendben; }
                set { _AlairasVeglegRendben = value; }                                                        
            }        
                   
           
        private SqlDateTime _Idopecset = SqlDateTime.Null;
           
        /// <summary>
        /// Idopecset Base property </summary>
            public SqlDateTime Idopecset
            {
                get { return _Idopecset; }
                set { _Idopecset = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Alairo = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Alairo Base property </summary>
            public SqlGuid Csoport_Id_Alairo
            {
                get { return _Csoport_Id_Alairo; }
                set { _Csoport_Id_Alairo = value; }                                                        
            }        
                   
           
        private SqlString _AlairasTulajdonos = SqlString.Null;
           
        /// <summary>
        /// AlairasTulajdonos Base property </summary>
            public SqlString AlairasTulajdonos
            {
                get { return _AlairasTulajdonos; }
                set { _AlairasTulajdonos = value; }                                                        
            }        
                   
           
        private SqlGuid _Tanusitvany_Id = SqlGuid.Null;
           
        /// <summary>
        /// Tanusitvany_Id Base property </summary>
            public SqlGuid Tanusitvany_Id
            {
                get { return _Tanusitvany_Id; }
                set { _Tanusitvany_Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}