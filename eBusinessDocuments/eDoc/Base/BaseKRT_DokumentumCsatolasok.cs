
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokumentumCsatolasok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_DokumentumCsatolasok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumCsatolasokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_Dokumentum = true;
               public bool ObjTip_Id_Dokumentum
               {
                   get { return _ObjTip_Id_Dokumentum; }
                   set { _ObjTip_Id_Dokumentum = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Dokumentum = true;
               public bool Obj_Id_Dokumentum
               {
                   get { return _Obj_Id_Dokumentum; }
                   set { _Obj_Id_Dokumentum = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Tipus_Id = true;
               public bool Tipus_Id
               {
                   get { return _Tipus_Id; }
                   set { _Tipus_Id = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ObjTip_Id_Dokumentum = Value;               
                    
                   Obj_Id_Dokumentum = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   Tipus = Value;               
                    
                   Tipus_Id = Value;               
                    
                   Leiras = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumCsatolasokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_Dokumentum = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_Dokumentum Base property </summary>
            public SqlGuid ObjTip_Id_Dokumentum
            {
                get { return _ObjTip_Id_Dokumentum; }
                set { _ObjTip_Id_Dokumentum = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Dokumentum = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Dokumentum Base property </summary>
            public SqlGuid Obj_Id_Dokumentum
            {
                get { return _Obj_Id_Dokumentum; }
                set { _Obj_Id_Dokumentum = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Tipus_Id = SqlGuid.Null;
           
        /// <summary>
        /// Tipus_Id Base property </summary>
            public SqlGuid Tipus_Id
            {
                get { return _Tipus_Id; }
                set { _Tipus_Id = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}