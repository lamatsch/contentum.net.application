
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokumentumKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_DokumentumKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id_Fo = true;
               public bool Dokumentum_Id_Fo
               {
                   get { return _Dokumentum_Id_Fo; }
                   set { _Dokumentum_Id_Fo = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id_Al = true;
               public bool Dokumentum_Id_Al
               {
                   get { return _Dokumentum_Id_Al; }
                   set { _Dokumentum_Id_Al = value; }
               }
                                                            
                                 
               private bool _DokumentumKapcsolatJelleg = true;
               public bool DokumentumKapcsolatJelleg
               {
                   get { return _DokumentumKapcsolatJelleg; }
                   set { _DokumentumKapcsolatJelleg = value; }
               }
                                                            
                                 
               private bool _DokuKapcsolatSorrend = true;
               public bool DokuKapcsolatSorrend
               {
                   get { return _DokuKapcsolatSorrend; }
                   set { _DokuKapcsolatSorrend = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Tipus = Value;               
                    
                   Dokumentum_Id_Fo = Value;               
                    
                   Dokumentum_Id_Al = Value;               
                    
                   DokumentumKapcsolatJelleg = Value;               
                    
                   DokuKapcsolatSorrend = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id_Fo = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id_Fo Base property </summary>
            public SqlGuid Dokumentum_Id_Fo
            {
                get { return _Dokumentum_Id_Fo; }
                set { _Dokumentum_Id_Fo = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id_Al = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id_Al Base property </summary>
            public SqlGuid Dokumentum_Id_Al
            {
                get { return _Dokumentum_Id_Al; }
                set { _Dokumentum_Id_Al = value; }                                                        
            }        
                   
           
        private SqlString _DokumentumKapcsolatJelleg = SqlString.Null;
           
        /// <summary>
        /// DokumentumKapcsolatJelleg Base property </summary>
            public SqlString DokumentumKapcsolatJelleg
            {
                get { return _DokumentumKapcsolatJelleg; }
                set { _DokumentumKapcsolatJelleg = value; }                                                        
            }        
                   
           
        private SqlInt32 _DokuKapcsolatSorrend = SqlInt32.Null;
           
        /// <summary>
        /// DokuKapcsolatSorrend Base property </summary>
            public SqlInt32 DokuKapcsolatSorrend
            {
                get { return _DokuKapcsolatSorrend; }
                set { _DokuKapcsolatSorrend = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}