
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Dokumentumok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Dokumentumok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _FajlNev = true;
               public bool FajlNev
               {
                   get { return _FajlNev; }
                   set { _FajlNev = value; }
               }
                                                            
                                 
               private bool _VerzioJel = true;
               public bool VerzioJel
               {
                   get { return _VerzioJel; }
                   set { _VerzioJel = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Formatum = true;
               public bool Formatum
               {
                   get { return _Formatum; }
                   set { _Formatum = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Meret = true;
               public bool Meret
               {
                   get { return _Meret; }
                   set { _Meret = value; }
               }
                                                            
                                 
               private bool _TartalomHash = true;
               public bool TartalomHash
               {
                   get { return _TartalomHash; }
                   set { _TartalomHash = value; }
               }
                                                            
                                 
               private bool _AlairtTartalomHash = true;
               public bool AlairtTartalomHash
               {
                   get { return _AlairtTartalomHash; }
                   set { _AlairtTartalomHash = value; }
               }
                                                            
                                 
               private bool _KivonatHash = true;
               public bool KivonatHash
               {
                   get { return _KivonatHash; }
                   set { _KivonatHash = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id_Kovetkezo = true;
               public bool Dokumentum_Id_Kovetkezo
               {
                   get { return _Dokumentum_Id_Kovetkezo; }
                   set { _Dokumentum_Id_Kovetkezo = value; }
               }
                                                            
                                 
               private bool _Alkalmazas_Id = true;
               public bool Alkalmazas_Id
               {
                   get { return _Alkalmazas_Id; }
                   set { _Alkalmazas_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _KulsoAzonositok = true;
               public bool KulsoAzonositok
               {
                   get { return _KulsoAzonositok; }
                   set { _KulsoAzonositok = value; }
               }
                                                            
                                 
               private bool _External_Source = true;
               public bool External_Source
               {
                   get { return _External_Source; }
                   set { _External_Source = value; }
               }
                                                            
                                 
               private bool _External_Link = true;
               public bool External_Link
               {
                   get { return _External_Link; }
                   set { _External_Link = value; }
               }
                                                            
                                 
               private bool _External_Id = true;
               public bool External_Id
               {
                   get { return _External_Id; }
                   set { _External_Id = value; }
               }
                                                            
                                 
               private bool _External_Info = true;
               public bool External_Info
               {
                   get { return _External_Info; }
                   set { _External_Info = value; }
               }
                                                            
                                 
               private bool _CheckedOut = true;
               public bool CheckedOut
               {
                   get { return _CheckedOut; }
                   set { _CheckedOut = value; }
               }
                                                            
                                 
               private bool _CheckedOutTime = true;
               public bool CheckedOutTime
               {
                   get { return _CheckedOutTime; }
                   set { _CheckedOutTime = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _OCRPrioritas = true;
               public bool OCRPrioritas
               {
                   get { return _OCRPrioritas; }
                   set { _OCRPrioritas = value; }
               }
                                                            
                                 
               private bool _OCRAllapot = true;
               public bool OCRAllapot
               {
                   get { return _OCRAllapot; }
                   set { _OCRAllapot = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _WorkFlowAllapot = true;
               public bool WorkFlowAllapot
               {
                   get { return _WorkFlowAllapot; }
                   set { _WorkFlowAllapot = value; }
               }
                                                            
                                 
               private bool _Megnyithato = true;
               public bool Megnyithato
               {
                   get { return _Megnyithato; }
                   set { _Megnyithato = value; }
               }
                                                            
                                 
               private bool _Olvashato = true;
               public bool Olvashato
               {
                   get { return _Olvashato; }
                   set { _Olvashato = value; }
               }
                                                            
                                 
               private bool _ElektronikusAlairas = true;
               public bool ElektronikusAlairas
               {
                   get { return _ElektronikusAlairas; }
                   set { _ElektronikusAlairas = value; }
               }
                                                            
                                 
               private bool _AlairasFelulvizsgalat = true;
               public bool AlairasFelulvizsgalat
               {
                   get { return _AlairasFelulvizsgalat; }
                   set { _AlairasFelulvizsgalat = value; }
               }
                                                            
                                 
               private bool _Titkositas = true;
               public bool Titkositas
               {
                   get { return _Titkositas; }
                   set { _Titkositas = value; }
               }
                                                            
                                 
               private bool _SablonAzonosito = true;
               public bool SablonAzonosito
               {
                   get { return _SablonAzonosito; }
                   set { _SablonAzonosito = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _AlairasFelulvizsgalatEredmeny = true;
               public bool AlairasFelulvizsgalatEredmeny
               {
                   get { return _AlairasFelulvizsgalatEredmeny; }
                   set { _AlairasFelulvizsgalatEredmeny = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   FajlNev = Value;               
                    
                   VerzioJel = Value;               
                    
                   Nev = Value;               
                    
                   Tipus = Value;               
                    
                   Formatum = Value;               
                    
                   Leiras = Value;               
                    
                   Meret = Value;               
                    
                   TartalomHash = Value;               
                    
                   AlairtTartalomHash = Value;               
                    
                   KivonatHash = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   Dokumentum_Id_Kovetkezo = Value;               
                    
                   Alkalmazas_Id = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   KulsoAzonositok = Value;               
                    
                   External_Source = Value;               
                    
                   External_Link = Value;               
                    
                   External_Id = Value;               
                    
                   External_Info = Value;               
                    
                   CheckedOut = Value;               
                    
                   CheckedOutTime = Value;               
                    
                   BarCode = Value;               
                    
                   OCRPrioritas = Value;               
                    
                   OCRAllapot = Value;               
                    
                   Allapot = Value;               
                    
                   WorkFlowAllapot = Value;               
                    
                   Megnyithato = Value;               
                    
                   Olvashato = Value;               
                    
                   ElektronikusAlairas = Value;               
                    
                   AlairasFelulvizsgalat = Value;               
                    
                   Titkositas = Value;               
                    
                   SablonAzonosito = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   AlairasFelulvizsgalatEredmeny = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_DokumentumokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _FajlNev = SqlString.Null;
           
        /// <summary>
        /// FajlNev Base property </summary>
            public SqlString FajlNev
            {
                get { return _FajlNev; }
                set { _FajlNev = value; }                                                        
            }        
                   
           
        private SqlString _VerzioJel = SqlString.Null;
           
        /// <summary>
        /// VerzioJel Base property </summary>
            public SqlString VerzioJel
            {
                get { return _VerzioJel; }
                set { _VerzioJel = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Formatum = SqlString.Null;
           
        /// <summary>
        /// Formatum Base property </summary>
            public SqlString Formatum
            {
                get { return _Formatum; }
                set { _Formatum = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlInt32 _Meret = SqlInt32.Null;
           
        /// <summary>
        /// Meret Base property </summary>
            public SqlInt32 Meret
            {
                get { return _Meret; }
                set { _Meret = value; }                                                        
            }        
                   
           
        private SqlString _TartalomHash = SqlString.Null;
           
        /// <summary>
        /// TartalomHash Base property </summary>
            public SqlString TartalomHash
            {
                get { return _TartalomHash; }
                set { _TartalomHash = value; }                                                        
            }        
                   
           
        private SqlBinary _AlairtTartalomHash = SqlBinary.Null;
           
        /// <summary>
        /// AlairtTartalomHash Base property </summary>
            public SqlBinary AlairtTartalomHash
            {
                get { return _AlairtTartalomHash; }
                set { _AlairtTartalomHash = value; }                                                        
            }        
                   
           
        private SqlString _KivonatHash = SqlString.Null;
           
        /// <summary>
        /// KivonatHash Base property </summary>
            public SqlString KivonatHash
            {
                get { return _KivonatHash; }
                set { _KivonatHash = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id_Kovetkezo = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id_Kovetkezo Base property </summary>
            public SqlGuid Dokumentum_Id_Kovetkezo
            {
                get { return _Dokumentum_Id_Kovetkezo; }
                set { _Dokumentum_Id_Kovetkezo = value; }                                                        
            }        
                   
           
        private SqlGuid _Alkalmazas_Id = SqlGuid.Null;
           
        /// <summary>
        /// Alkalmazas_Id Base property </summary>
            public SqlGuid Alkalmazas_Id
            {
                get { return _Alkalmazas_Id; }
                set { _Alkalmazas_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlString _KulsoAzonositok = SqlString.Null;
           
        /// <summary>
        /// KulsoAzonositok Base property </summary>
            public SqlString KulsoAzonositok
            {
                get { return _KulsoAzonositok; }
                set { _KulsoAzonositok = value; }                                                        
            }        
                   
           
        private SqlString _External_Source = SqlString.Null;
           
        /// <summary>
        /// External_Source Base property </summary>
            public SqlString External_Source
            {
                get { return _External_Source; }
                set { _External_Source = value; }                                                        
            }        
                   
           
        private SqlString _External_Link = SqlString.Null;
           
        /// <summary>
        /// External_Link Base property </summary>
            public SqlString External_Link
            {
                get { return _External_Link; }
                set { _External_Link = value; }                                                        
            }        
                   
           
        private SqlGuid _External_Id = SqlGuid.Null;
           
        /// <summary>
        /// External_Id Base property </summary>
            public SqlGuid External_Id
            {
                get { return _External_Id; }
                set { _External_Id = value; }                                                        
            }        
                   
           
        private SqlString _External_Info = SqlString.Null;
           
        /// <summary>
        /// External_Info Base property </summary>
            public SqlString External_Info
            {
                get { return _External_Info; }
                set { _External_Info = value; }                                                        
            }        
                   
           
        private SqlGuid _CheckedOut = SqlGuid.Null;
           
        /// <summary>
        /// CheckedOut Base property </summary>
            public SqlGuid CheckedOut
            {
                get { return _CheckedOut; }
                set { _CheckedOut = value; }                                                        
            }        
                   
           
        private SqlDateTime _CheckedOutTime = SqlDateTime.Null;
           
        /// <summary>
        /// CheckedOutTime Base property </summary>
            public SqlDateTime CheckedOutTime
            {
                get { return _CheckedOutTime; }
                set { _CheckedOutTime = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlInt32 _OCRPrioritas = SqlInt32.Null;
           
        /// <summary>
        /// OCRPrioritas Base property </summary>
            public SqlInt32 OCRPrioritas
            {
                get { return _OCRPrioritas; }
                set { _OCRPrioritas = value; }                                                        
            }        
                   
           
        private SqlChars _OCRAllapot = SqlChars.Null;
           
        /// <summary>
        /// OCRAllapot Base property </summary>
            public SqlChars OCRAllapot
            {
                get { return _OCRAllapot; }
                set { _OCRAllapot = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _WorkFlowAllapot = SqlString.Null;
           
        /// <summary>
        /// WorkFlowAllapot Base property </summary>
            public SqlString WorkFlowAllapot
            {
                get { return _WorkFlowAllapot; }
                set { _WorkFlowAllapot = value; }                                                        
            }        
                   
           
        private SqlChars _Megnyithato = SqlChars.Null;
           
        /// <summary>
        /// Megnyithato Base property </summary>
            public SqlChars Megnyithato
            {
                get { return _Megnyithato; }
                set { _Megnyithato = value; }                                                        
            }        
                   
           
        private SqlChars _Olvashato = SqlChars.Null;
           
        /// <summary>
        /// Olvashato Base property </summary>
            public SqlChars Olvashato
            {
                get { return _Olvashato; }
                set { _Olvashato = value; }                                                        
            }        
                   
           
        private SqlString _ElektronikusAlairas = SqlString.Null;
           
        /// <summary>
        /// ElektronikusAlairas Base property </summary>
            public SqlString ElektronikusAlairas
            {
                get { return _ElektronikusAlairas; }
                set { _ElektronikusAlairas = value; }                                                        
            }        
                   
           
        private SqlDateTime _AlairasFelulvizsgalat = SqlDateTime.Null;
           
        /// <summary>
        /// AlairasFelulvizsgalat Base property </summary>
            public SqlDateTime AlairasFelulvizsgalat
            {
                get { return _AlairasFelulvizsgalat; }
                set { _AlairasFelulvizsgalat = value; }                                                        
            }        
                   
           
        private SqlChars _Titkositas = SqlChars.Null;
           
        /// <summary>
        /// Titkositas Base property </summary>
            public SqlChars Titkositas
            {
                get { return _Titkositas; }
                set { _Titkositas = value; }                                                        
            }        
                   
           
        private SqlString _SablonAzonosito = SqlString.Null;
           
        /// <summary>
        /// SablonAzonosito Base property </summary>
            public SqlString SablonAzonosito
            {
                get { return _SablonAzonosito; }
                set { _SablonAzonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlString _AlairasFelulvizsgalatEredmeny = SqlString.Null;
           
        /// <summary>
        /// AlairasFelulvizsgalatEredmeny Base property </summary>
            public SqlString AlairasFelulvizsgalatEredmeny
            {
                get { return _AlairasFelulvizsgalatEredmeny; }
                set { _AlairasFelulvizsgalatEredmeny = value; }                                                        
            }        
                           }
    }    
}