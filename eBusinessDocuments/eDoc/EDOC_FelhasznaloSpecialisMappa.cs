
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EDOC_FelhasznaloSpecialisMappa BusinessDocument Class </summary>
    [Serializable()]
    public class EDOC_FelhasznaloSpecialisMappa : BaseEDOC_FelhasznaloSpecialisMappa
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Tipus_Id property </summary>
        public String Tipus_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tipus_Id); }
            set { Typed.Tipus_Id = Utility.SetSqlGuidFromString(value, Typed.Tipus_Id); }                                            
        }
                   
           
        /// <summary>
        /// Mappa_Id property </summary>
        public String Mappa_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Mappa_Id); }
            set { Typed.Mappa_Id = Utility.SetSqlGuidFromString(value, Typed.Mappa_Id); }                                            
        }
                   
           
        /// <summary>
        /// Mapp_Azonosito property </summary>
        public String Mapp_Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Mapp_Azonosito); }
            set { Typed.Mapp_Azonosito = Utility.SetSqlStringFromString(value, Typed.Mapp_Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}