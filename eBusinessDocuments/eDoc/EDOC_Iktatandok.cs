
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EDOC_Iktatandok BusinessDocument Class </summary>
    [Serializable()]
    public class EDOC_Iktatandok : BaseEDOC_Iktatandok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_IktatastKero property </summary>
        public String Felhasznalo_Id_IktatastKero
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_IktatastKero); }
            set { Typed.Felhasznalo_Id_IktatastKero = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_IktatastKero); }                                            
        }
                   
           
        /// <summary>
        /// IraIratok_Id property </summary>
        public String IraIratok_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIratok_Id); }
            set { Typed.IraIratok_Id = Utility.SetSqlGuidFromString(value, Typed.IraIratok_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}