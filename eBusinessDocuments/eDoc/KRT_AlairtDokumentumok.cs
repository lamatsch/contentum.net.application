
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_AlairtDokumentumok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_AlairtDokumentumok : BaseKRT_AlairtDokumentumok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasMod property </summary>
        public String AlairasMod
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasMod); }
            set { Typed.AlairasMod = Utility.SetSqlStringFromString(value, Typed.AlairasMod); }                                            
        }
                   
           
        /// <summary>
        /// AlairtFajlnev property </summary>
        public String AlairtFajlnev
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairtFajlnev); }
            set { Typed.AlairtFajlnev = Utility.SetSqlStringFromString(value, Typed.AlairtFajlnev); }                                            
        }
                   
           
        /// <summary>
        /// AlairtTartalomHash property </summary>
        public String AlairtTartalomHash
        {
            get { return Utility.GetStringFromSqlBinary(Typed.AlairtTartalomHash); }
            set { Typed.AlairtTartalomHash = Utility.SetSqlBinaryFromString(value, Typed.AlairtTartalomHash); }                                            
        }
                   
           
        /// <summary>
        /// IratAlairasSzabaly_Id property </summary>
        public String IratAlairasSzabaly_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IratAlairasSzabaly_Id); }
            set { Typed.IratAlairasSzabaly_Id = Utility.SetSqlGuidFromString(value, Typed.IratAlairasSzabaly_Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasRendben property </summary>
        public String AlairasRendben
        {
            get { return Utility.GetStringFromSqlChars(Typed.AlairasRendben); }
            set { Typed.AlairasRendben = Utility.SetSqlCharsFromString(value, Typed.AlairasRendben); }                                            
        }
                   
           
        /// <summary>
        /// KivarasiIdoVege property </summary>
        public String KivarasiIdoVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KivarasiIdoVege); }
            set { Typed.KivarasiIdoVege = Utility.SetSqlDateTimeFromString(value, Typed.KivarasiIdoVege); }                                            
        }
                   
           
        /// <summary>
        /// AlairasVeglegRendben property </summary>
        public String AlairasVeglegRendben
        {
            get { return Utility.GetStringFromSqlChars(Typed.AlairasVeglegRendben); }
            set { Typed.AlairasVeglegRendben = Utility.SetSqlCharsFromString(value, Typed.AlairasVeglegRendben); }                                            
        }
                   
           
        /// <summary>
        /// Idopecset property </summary>
        public String Idopecset
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Idopecset); }
            set { Typed.Idopecset = Utility.SetSqlDateTimeFromString(value, Typed.Idopecset); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Alairo property </summary>
        public String Csoport_Id_Alairo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Alairo); }
            set { Typed.Csoport_Id_Alairo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Alairo); }                                            
        }
                   
           
        /// <summary>
        /// AlairasTulajdonos property </summary>
        public String AlairasTulajdonos
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasTulajdonos); }
            set { Typed.AlairasTulajdonos = Utility.SetSqlStringFromString(value, Typed.AlairasTulajdonos); }                                            
        }
                   
           
        /// <summary>
        /// Tanusitvany_Id property </summary>
        public String Tanusitvany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tanusitvany_Id); }
            set { Typed.Tanusitvany_Id = Utility.SetSqlGuidFromString(value, Typed.Tanusitvany_Id); }                                            
        }
                   
           
        /// <summary>
        /// External_Link property </summary>
        public String External_Link
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Link); }
            set { Typed.External_Link = Utility.SetSqlStringFromString(value, Typed.External_Link); }                                            
        }
                   
           
        /// <summary>
        /// External_Id property </summary>
        public String External_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.External_Id); }
            set { Typed.External_Id = Utility.SetSqlGuidFromString(value, Typed.External_Id); }                                            
        }
                   
           
        /// <summary>
        /// External_Source property </summary>
        public String External_Source
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Source); }
            set { Typed.External_Source = Utility.SetSqlStringFromString(value, Typed.External_Source); }                                            
        }
                   
           
        /// <summary>
        /// External_Info property </summary>
        public String External_Info
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Info); }
            set { Typed.External_Info = Utility.SetSqlStringFromString(value, Typed.External_Info); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}