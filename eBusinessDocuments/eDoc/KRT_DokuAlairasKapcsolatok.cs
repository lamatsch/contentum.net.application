
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokuAlairasKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_DokuAlairasKapcsolatok : BaseKRT_DokuAlairasKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_id property </summary>
        public String Dokumentum_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_id); }
            set { Typed.Dokumentum_id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_id); }                                            
        }
                   
           
        /// <summary>
        /// DokumentumAlairas_Id property </summary>
        public String DokumentumAlairas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.DokumentumAlairas_Id); }
            set { Typed.DokumentumAlairas_Id = Utility.SetSqlGuidFromString(value, Typed.DokumentumAlairas_Id); }                                            
        }
                   
           
        /// <summary>
        /// DokuKapcsolatSorrend property </summary>
        public String DokuKapcsolatSorrend
        {
            get { return Utility.GetStringFromSqlInt32(Typed.DokuKapcsolatSorrend); }
            set { Typed.DokuKapcsolatSorrend = Utility.SetSqlInt32FromString(value, Typed.DokuKapcsolatSorrend); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}