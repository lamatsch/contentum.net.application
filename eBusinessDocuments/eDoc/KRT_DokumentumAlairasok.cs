
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokumentumAlairasok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_DokumentumAlairasok : BaseKRT_DokumentumAlairasok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id_Alairt property </summary>
        public String Dokumentum_Id_Alairt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id_Alairt); }
            set { Typed.Dokumentum_Id_Alairt = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id_Alairt); }                                            
        }
                   
           
        /// <summary>
        /// AlairasMod property </summary>
        public String AlairasMod
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasMod); }
            set { Typed.AlairasMod = Utility.SetSqlStringFromString(value, Typed.AlairasMod); }                                            
        }
                   
           
        /// <summary>
        /// AlairasSzabaly_Id property </summary>
        public String AlairasSzabaly_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AlairasSzabaly_Id); }
            set { Typed.AlairasSzabaly_Id = Utility.SetSqlGuidFromString(value, Typed.AlairasSzabaly_Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasRendben property </summary>
        public String AlairasRendben
        {
            get { return Utility.GetStringFromSqlChars(Typed.AlairasRendben); }
            set { Typed.AlairasRendben = Utility.SetSqlCharsFromString(value, Typed.AlairasRendben); }                                            
        }
                   
           
        /// <summary>
        /// KivarasiIdoVege property </summary>
        public String KivarasiIdoVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KivarasiIdoVege); }
            set { Typed.KivarasiIdoVege = Utility.SetSqlDateTimeFromString(value, Typed.KivarasiIdoVege); }                                            
        }
                   
           
        /// <summary>
        /// AlairasVeglegRendben property </summary>
        public String AlairasVeglegRendben
        {
            get { return Utility.GetStringFromSqlChars(Typed.AlairasVeglegRendben); }
            set { Typed.AlairasVeglegRendben = Utility.SetSqlCharsFromString(value, Typed.AlairasVeglegRendben); }                                            
        }
                   
           
        /// <summary>
        /// Idopecset property </summary>
        public String Idopecset
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Idopecset); }
            set { Typed.Idopecset = Utility.SetSqlDateTimeFromString(value, Typed.Idopecset); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Alairo property </summary>
        public String Csoport_Id_Alairo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Alairo); }
            set { Typed.Csoport_Id_Alairo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Alairo); }                                            
        }
                   
           
        /// <summary>
        /// AlairasTulajdonos property </summary>
        public String AlairasTulajdonos
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasTulajdonos); }
            set { Typed.AlairasTulajdonos = Utility.SetSqlStringFromString(value, Typed.AlairasTulajdonos); }                                            
        }
                   
           
        /// <summary>
        /// Tanusitvany_Id property </summary>
        public String Tanusitvany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tanusitvany_Id); }
            set { Typed.Tanusitvany_Id = Utility.SetSqlGuidFromString(value, Typed.Tanusitvany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}