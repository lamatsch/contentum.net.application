
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_DokumentumKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_DokumentumKapcsolatok : BaseKRT_DokumentumKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id_Fo property </summary>
        public String Dokumentum_Id_Fo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id_Fo); }
            set { Typed.Dokumentum_Id_Fo = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id_Fo); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id_Al property </summary>
        public String Dokumentum_Id_Al
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id_Al); }
            set { Typed.Dokumentum_Id_Al = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id_Al); }                                            
        }
                   
           
        /// <summary>
        /// DokumentumKapcsolatJelleg property </summary>
        public String DokumentumKapcsolatJelleg
        {
            get { return Utility.GetStringFromSqlString(Typed.DokumentumKapcsolatJelleg); }
            set { Typed.DokumentumKapcsolatJelleg = Utility.SetSqlStringFromString(value, Typed.DokumentumKapcsolatJelleg); }                                            
        }
                   
           
        /// <summary>
        /// DokuKapcsolatSorrend property </summary>
        public String DokuKapcsolatSorrend
        {
            get { return Utility.GetStringFromSqlInt32(Typed.DokuKapcsolatSorrend); }
            set { Typed.DokuKapcsolatSorrend = Utility.SetSqlInt32FromString(value, Typed.DokuKapcsolatSorrend); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}