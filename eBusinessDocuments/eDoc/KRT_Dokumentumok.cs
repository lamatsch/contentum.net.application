
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Dokumentumok BusinessDocument Class
    /// New : beker�lt az alkalmaz�s Id, ahonnan beker�lt, vagy �tvett�k
    /// </summary>
    [Serializable()]
    public class KRT_Dokumentumok : BaseKRT_Dokumentumok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// Org (alkalmaz�sk�rnyezet) azonos�t�ja
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// FajlNev property
        /// Eredeti dokumentum fajlneve (kiterjesztessel egyutt).
        /// </summary>
        public String FajlNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FajlNev); }
            set { Typed.FajlNev = Utility.SetSqlStringFromString(value, Typed.FajlNev); }                                            
        }
                   
           
        /// <summary>
        /// VerzioJel property
        /// A verzio azonositja (FPH h�rmas ...)
        /// </summary>
        public String VerzioJel
        {
            get { return Utility.GetStringFromSqlString(Typed.VerzioJel); }
            set { Typed.VerzioJel = Utility.SetSqlStringFromString(value, Typed.VerzioJel); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// A dokumentum szabad szoveges megnevezese.
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// KCS: DOKUMENTUMTIPUS
        /// 01 - Sz�mla
        /// 02 - Szerz�d�s
        /// 11 - Besz�mol�
        /// 12 - Elemz�s; ellen�rz�s;el�terjeszt�s;
        /// 13 - Feljegyz�s
        /// 14 - Jogszab�ly
        /// 15 - P�ly�zat
        /// 16 - Prezent�ci�
        /// 17 - Tanulm�ny
        /// 18 - Digitaliz�lt dokumentum
        /// 19 - Bont�si jegyz�k�nyv
        /// 20 - �tv�teli elismerv�ny
        /// 21 - Egy�b
        /// 
        /// 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Formatum property
        /// KCS: DOKUMENTUM_FORMATUM F�jln�v kiterjeszt�s (�s annak felhaszn�l�i szint� �rtelmez�se)
        /// </summary>
        public String Formatum
        {
            get { return Utility.GetStringFromSqlString(Typed.Formatum); }
            set { Typed.Formatum = Utility.SetSqlStringFromString(value, Typed.Formatum); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// Szabad szoveg
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Meret property
        /// Fajlmeret (byte).
        /// </summary>
        public String Meret
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Meret); }
            set { Typed.Meret = Utility.SetSqlInt32FromString(value, Typed.Meret); }                                            
        }
                   
           
        /// <summary>
        /// TartalomHash property
        /// Eredeti dokumentum teljes tartalm�nak ujjlenyomata.
        /// </summary>
        public String TartalomHash
        {
            get { return Utility.GetStringFromSqlString(Typed.TartalomHash); }
            set { Typed.TartalomHash = Utility.SetSqlStringFromString(value, Typed.TartalomHash); }                                            
        }
                   
           
        /// <summary>
        /// AlairtTartalomHash property
        /// Detached al��r�si m�d eset�n az �rintett dokumentumok konkaten�lt TartalomHash �rt�k�nek elektronikusan al��rt form�ja.
        /// </summary>
        public String AlairtTartalomHash
        {
            get { return Utility.GetStringFromSqlBinary(Typed.AlairtTartalomHash); }
            set { Typed.AlairtTartalomHash = Utility.SetSqlBinaryFromString(value, Typed.AlairtTartalomHash); }                                            
        }
                   
           
        /// <summary>
        /// KivonatHash property
        /// Eredeti dokumentum kivonat�nak (beltartalm�nak) ujjlenyomata (docx-n�l �rtelmezett).
        /// </summary>
        public String KivonatHash
        {
            get { return Utility.GetStringFromSqlString(Typed.KivonatHash); }
            set { Typed.KivonatHash = Utility.SetSqlStringFromString(value, Typed.KivonatHash); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// Egy dokumentum minden verziojaban ugyanaz az ertek (az elso verzio ID-je).
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id_Kovetkezo property
        /// K�vetkez� verzi� ID-je. Ha �res, akkor � az aktu�lis (verzio), a munkapeldany.
        /// </summary>
        public String Dokumentum_Id_Kovetkezo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id_Kovetkezo); }
            set { Typed.Dokumentum_Id_Kovetkezo = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id_Kovetkezo); }                                            
        }
                   
           
        /// <summary>
        /// Alkalmazas_Id property
        /// Alkalmaz�s Id 
        /// </summary>
        public String Alkalmazas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Alkalmazas_Id); }
            set { Typed.Alkalmazas_Id = Utility.SetSqlGuidFromString(value, Typed.Alkalmazas_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Letrehozo (vagy mas, aki letrehozatta) csoportja. Megfelel az eRecordbeli PRT_ID_FELELOS-nek. Mindig legyen legalabb egy csoport, amelynek explicit karbantartasi joga van az adott objektumra. Ezt a jogat mas szabalyok korlatozhatjak, de nem fordulhat elo olyan eset, hogy egy akarmilyen szulo ervenytelenitese objektumokat kezelhetetlenne tesz!
        /// </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// KulsoAzonositok property
        /// Az adott alkalmaz�s szeinti saj�t egyedi azonos�t�. (A 2007.6.6 -i megbesz�l�sen mer�lt fel a sz�ks�gess�ge.)
        /// </summary>
        public String KulsoAzonositok
        {
            get { return Utility.GetStringFromSqlString(Typed.KulsoAzonositok); }
            set { Typed.KulsoAzonositok = Utility.SetSqlStringFromString(value, Typed.KulsoAzonositok); }                                            
        }
                   
           
        /// <summary>
        /// External_Source property
        /// K�ls� dokumentumt�r forr�s t�pusa: KCS: DOKUMENTUM_FORRAS_TIPUS (1-SharePoint, 2-File system, 3-Database).
        /// 
        /// </summary>
        public String External_Source
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Source); }
            set { Typed.External_Source = Utility.SetSqlStringFromString(value, Typed.External_Source); }                                            
        }
                   
           
        /// <summary>
        /// External_Link property
        /// A dokumentum el�r�s teljes linkje. SharePoint eset�n URL, f�jlrendszer eset�n teljes el�r�si �t (g�p, �tvonal, ...)
        /// </summary>
        public String External_Link
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Link); }
            set { Typed.External_Link = Utility.SetSqlStringFromString(value, Typed.External_Link); }                                            
        }
                   
           
        /// <summary>
        /// External_Id property
        /// SharePoint for��s eset�n �rtelmezett adat, SharePoint ID (GUID).
        /// </summary>
        public String External_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.External_Id); }
            set { Typed.External_Id = Utility.SetSqlGuidFromString(value, Typed.External_Id); }                                            
        }
                   
           
        /// <summary>
        /// External_Info property
        /// A k�ls� forr�s t�pus�t�l f�gg� szerkezet�, az el�r�si adatokat tartalmaz� sz�veg, pl. egyedi jellemz�k a dokumentumt�rr�l.
        /// </summary>
        public String External_Info
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Info); }
            set { Typed.External_Info = Utility.SetSqlStringFromString(value, Typed.External_Info); }                                            
        }
                   
           
        /// <summary>
        /// CheckedOut property
        /// 
        /// </summary>
        public String CheckedOut
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CheckedOut); }
            set { Typed.CheckedOut = Utility.SetSqlGuidFromString(value, Typed.CheckedOut); }                                            
        }
                   
           
        /// <summary>
        /// CheckedOutTime property
        /// 
        /// </summary>
        public String CheckedOutTime
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.CheckedOutTime); }
            set { Typed.CheckedOutTime = Utility.SetSqlDateTimeFromString(value, Typed.CheckedOutTime); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// OCRPrioritas property
        /// Az OCR-ezend� (multipage TIFF) f�jlok OCR-ez�s�nek priorit�s�t meghat�roz� �rt�k.
        /// NULL - nem OCR-ezend� dokumentum
        /// 1-9: priorit�s (a magasabb �rt�k jelenti a nagyobb priorit�st)
        /// </summary>
        public String OCRPrioritas
        {
            get { return Utility.GetStringFromSqlInt32(Typed.OCRPrioritas); }
            set { Typed.OCRPrioritas = Utility.SetSqlInt32FromString(value, Typed.OCRPrioritas); }                                            
        }
                   
           
        /// <summary>
        /// OCRAllapot property
        /// Az OCR-ezend� (multipage TIFF) f�jlok OCR �llapota.
        /// NULL - nem OCR-ezend� dokumentum
        /// 1 - OCR-ezend� dokumentum
        /// 2 - OCR-ez�s alatt l�v� dokumentum
        /// 3 - OCR-ezett dokumentum
        /// </summary>
        public String OCRAllapot
        {
            get { return Utility.GetStringFromSqlChars(Typed.OCRAllapot); }
            set { Typed.OCRAllapot = Utility.SetSqlCharsFromString(value, Typed.OCRAllapot); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// Dokumentum�llapot. KCS: DOKUMENTUM_ALLAPOT.
        /// (0 - Iktat�rendszeren k�v�li, 1 - Iktat�rendszerben nyilv�ntartott) 
        /// ObjStatok szinon�m�b�l j�n (Sz�r�s miatt denormaliz�lt!)
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// WorkFlowAllapot property
        /// Dokumentum workflow �llapot. KCS: DOKUMENTUM_WORKFLOW_ALLAPOT.
        /// 
        /// </summary>
        public String WorkFlowAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.WorkFlowAllapot); }
            set { Typed.WorkFlowAllapot = Utility.SetSqlStringFromString(value, Typed.WorkFlowAllapot); }                                            
        }
                   
           
        /// <summary>
        /// Megnyithato property
        /// 1-igen, 0-nem
        /// </summary>
        public String Megnyithato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Megnyithato); }
            set { Typed.Megnyithato = Utility.SetSqlCharsFromString(value, Typed.Megnyithato); }                                            
        }
                   
           
        /// <summary>
        /// Olvashato property
        /// 1-igen, 0-nem
        /// </summary>
        public String Olvashato
        {
            get { return Utility.GetStringFromSqlChars(Typed.Olvashato); }
            set { Typed.Olvashato = Utility.SetSqlCharsFromString(value, Typed.Olvashato); }                                            
        }
                   
           
        /// <summary>
        /// ElektronikusAlairas property
        /// A dokumentum al��r�s min�s�t�se.
        /// KCS:DOKUMETUM_ALAIRAS_PKI  vagy a DOKUMENTUM_ALAIRAS_MANUALIS,
        /// a PKI_INTEGRACO rendszerparam�ter be�ll�t�s�t�l f�gg�en(PKI/MANUALIS):
        /// 0 - Nincs vagy nem hiteles(mindk�t k�dcsoport)
        /// 1 - Min�s�tett al��r�s/Hiteles al��r�s
        /// 2 - Fokozott biztons�g� al��r�s/Hiteles al��r�s
        /// 3 - Hivatali bels� al��r�s/Nem hiteles al��r�s
        /// 4 - Egyszer� al��r�s
        /// 8 - Hiteles al��r�s (mindk�t k�dcsoport)
        /// 9 - V�glegesen nem hiteles al��r�s/Nem hiteles al��r�s
        /// 
        /// </summary>
        public String ElektronikusAlairas
        {
            get { return Utility.GetStringFromSqlString(Typed.ElektronikusAlairas); }
            set { Typed.ElektronikusAlairas = Utility.SetSqlStringFromString(value, Typed.ElektronikusAlairas); }                                            
        }
                   
           
        /// <summary>
        /// AlairasFelulvizsgalat property
        /// Az al��r�s �rv�nyess�g�nek �s/vagy az al��rt dokumentum s�rtetlens�g�nek utols� vizsg�lati id�pontja.
        /// </summary>
        public String AlairasFelulvizsgalat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AlairasFelulvizsgalat); }
            set { Typed.AlairasFelulvizsgalat = Utility.SetSqlDateTimeFromString(value, Typed.AlairasFelulvizsgalat); }                                            
        }
                   
           
        /// <summary>
        /// Titkositas property
        /// Tartalom titkos�t�sa: 0 Nem, 1 Igen
        /// </summary>
        public String Titkositas
        {
            get { return Utility.GetStringFromSqlChars(Typed.Titkositas); }
            set { Typed.Titkositas = Utility.SetSqlCharsFromString(value, Typed.Titkositas); }                                            
        }
                   
           
        /// <summary>
        /// SablonAzonosito property
        /// <ContentType>+<Verzi�>
        /// </summary>
        public String SablonAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.SablonAzonosito); }
            set { Typed.SablonAzonosito = Utility.SetSqlStringFromString(value, Typed.SablonAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// AlairasFelulvizsgalatEredmeny property
        /// 
        /// </summary>
        public String AlairasFelulvizsgalatEredmeny
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasFelulvizsgalatEredmeny); }
            set { Typed.AlairasFelulvizsgalatEredmeny = Utility.SetSqlStringFromString(value, Typed.AlairasFelulvizsgalatEredmeny); }                                            
        }
                           }
   
}