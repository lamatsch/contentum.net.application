﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    public class KRT_DokumentumokList
    {
        private List<KRT_Dokumentumok> _Dokumentumok = new List<KRT_Dokumentumok>();

        public List<KRT_Dokumentumok> Dokumentumok
        {
            get { return _Dokumentumok; }
            set { _Dokumentumok = value; }
        }

        public KRT_DokumentumokList()
        {
        }

        public KRT_DokumentumokList(List<KRT_Dokumentumok> dokumentumok)
        {
            this._Dokumentumok = dokumentumok;
        }
    }
}
