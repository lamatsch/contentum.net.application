
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_AutoEUzenetSzabaly BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_AutoEUzenetSzabaly
    {
        [System.Xml.Serialization.XmlType("BaseINT_AutoEUzenetSzabalyBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Szabaly = true;
               public bool Szabaly
               {
                   get { return _Szabaly; }
                   set { _Szabaly = value; }
               }
                                                            
                                 
               private bool _Sorrend = true;
               public bool Sorrend
               {
                   get { return _Sorrend; }
                   set { _Sorrend = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Leallito = true;
               public bool Leallito
               {
                   get { return _Leallito; }
                   set { _Leallito = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Nev = Value;               
                    
                   Leiras = Value;               
                    
                   Szabaly = Value;               
                    
                   Sorrend = Value;               
                    
                   Tipus = Value;               
                    
                   Leallito = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_AutoEUzenetSzabalyBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _Szabaly = SqlString.Null;
           
        /// <summary>
        /// Szabaly Base property </summary>
            public SqlString Szabaly
            {
                get { return _Szabaly; }
                set { _Szabaly = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorrend = SqlInt32.Null;
           
        /// <summary>
        /// Sorrend Base property </summary>
            public SqlInt32 Sorrend
            {
                get { return _Sorrend; }
                set { _Sorrend = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlChars _Leallito = SqlChars.Null;
           
        /// <summary>
        /// Leallito Base property </summary>
            public SqlChars Leallito
            {
                get { return _Leallito; }
                set { _Leallito = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}