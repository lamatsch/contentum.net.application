
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_AutoEUzenetSzabalyNaplo BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_AutoEUzenetSzabalyNaplo
    {
        [System.Xml.Serialization.XmlType("BaseINT_AutoEUzenetSzabalyNaploBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ESzabalyId = true;
               public bool ESzabalyId
               {
                   get { return _ESzabalyId; }
                   set { _ESzabalyId = value; }
               }
                                                            
                                 
               private bool _EUzenetId = true;
               public bool EUzenetId
               {
                   get { return _EUzenetId; }
                   set { _EUzenetId = value; }
               }
                                                            
                                 
               private bool _EUzenetTipusId = true;
               public bool EUzenetTipusId
               {
                   get { return _EUzenetTipusId; }
                   set { _EUzenetTipusId = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ESzabalyId = Value;               
                    
                   EUzenetId = Value;               
                    
                   EUzenetTipusId = Value;               
                    
                   Megjegyzes = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_AutoEUzenetSzabalyNaploBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ESzabalyId = SqlGuid.Null;
           
        /// <summary>
        /// ESzabalyId Base property </summary>
            public SqlGuid ESzabalyId
            {
                get { return _ESzabalyId; }
                set { _ESzabalyId = value; }                                                        
            }        
                   
           
        private SqlGuid _EUzenetId = SqlGuid.Null;
           
        /// <summary>
        /// EUzenetId Base property </summary>
            public SqlGuid EUzenetId
            {
                get { return _EUzenetId; }
                set { _EUzenetId = value; }                                                        
            }        
                   
           
        private SqlString _EUzenetTipusId = SqlString.Null;
           
        /// <summary>
        /// EUzenetTipusId Base property </summary>
            public SqlString EUzenetTipusId
            {
                get { return _EUzenetTipusId; }
                set { _EUzenetTipusId = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}