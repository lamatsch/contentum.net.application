
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_ExternalIDs BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_ExternalIDs
    {
        [System.Xml.Serialization.XmlType("BaseINT_ExternalIDsBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Modul_Id = true;
               public bool Modul_Id
               {
                   get { return _Modul_Id; }
                   set { _Modul_Id = value; }
               }
                                                            
                                 
               private bool _Edok_Type = true;
               public bool Edok_Type
               {
                   get { return _Edok_Type; }
                   set { _Edok_Type = value; }
               }
                                                            
                                 
               private bool _Edok_Id = true;
               public bool Edok_Id
               {
                   get { return _Edok_Id; }
                   set { _Edok_Id = value; }
               }
                                                            
                                 
               private bool _Edok_ExpiredDate = true;
               public bool Edok_ExpiredDate
               {
                   get { return _Edok_ExpiredDate; }
                   set { _Edok_ExpiredDate = value; }
               }
                                                            
                                 
               private bool _External_Group = true;
               public bool External_Group
               {
                   get { return _External_Group; }
                   set { _External_Group = value; }
               }
                                                            
                                 
               private bool _External_Id = true;
               public bool External_Id
               {
                   get { return _External_Id; }
                   set { _External_Id = value; }
               }
                                                            
                                 
               private bool _Deleted = true;
               public bool Deleted
               {
                   get { return _Deleted; }
                   set { _Deleted = value; }
               }
                                                            
                                 
               private bool _LastSync = true;
               public bool LastSync
               {
                   get { return _LastSync; }
                   set { _LastSync = value; }
               }
                                                            
                                 
               private bool _State = true;
               public bool State
               {
                   get { return _State; }
                   set { _State = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Modul_Id = Value;               
                    
                   Edok_Type = Value;               
                    
                   Edok_Id = Value;               
                    
                   Edok_ExpiredDate = Value;               
                    
                   External_Group = Value;               
                    
                   External_Id = Value;               
                    
                   Deleted = Value;               
                    
                   LastSync = Value;               
                    
                   State = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_ExternalIDsBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_Id Base property </summary>
            public SqlGuid Modul_Id
            {
                get { return _Modul_Id; }
                set { _Modul_Id = value; }                                                        
            }        
                   
           
        private SqlString _Edok_Type = SqlString.Null;
           
        /// <summary>
        /// Edok_Type Base property </summary>
            public SqlString Edok_Type
            {
                get { return _Edok_Type; }
                set { _Edok_Type = value; }                                                        
            }        
                   
           
        private SqlGuid _Edok_Id = SqlGuid.Null;
           
        /// <summary>
        /// Edok_Id Base property </summary>
            public SqlGuid Edok_Id
            {
                get { return _Edok_Id; }
                set { _Edok_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _Edok_ExpiredDate = SqlDateTime.Null;
           
        /// <summary>
        /// Edok_ExpiredDate Base property </summary>
            public SqlDateTime Edok_ExpiredDate
            {
                get { return _Edok_ExpiredDate; }
                set { _Edok_ExpiredDate = value; }                                                        
            }        
                   
           
        private SqlString _External_Group = SqlString.Null;
           
        /// <summary>
        /// External_Group Base property </summary>
            public SqlString External_Group
            {
                get { return _External_Group; }
                set { _External_Group = value; }                                                        
            }        
                   
           
        private SqlGuid _External_Id = SqlGuid.Null;
           
        /// <summary>
        /// External_Id Base property </summary>
            public SqlGuid External_Id
            {
                get { return _External_Id; }
                set { _External_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Deleted = SqlChars.Null;
           
        /// <summary>
        /// Deleted Base property </summary>
            public SqlChars Deleted
            {
                get { return _Deleted; }
                set { _Deleted = value; }                                                        
            }        
                   
           
        private SqlDateTime _LastSync = SqlDateTime.Null;
           
        /// <summary>
        /// LastSync Base property </summary>
            public SqlDateTime LastSync
            {
                get { return _LastSync; }
                set { _LastSync = value; }                                                        
            }        
                   
           
        private SqlString _State = SqlString.Null;
           
        /// <summary>
        /// State Base property </summary>
            public SqlString State
            {
                get { return _State; }
                set { _State = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}