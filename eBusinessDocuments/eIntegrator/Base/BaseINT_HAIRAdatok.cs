
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// INT_HAIRAdatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_HAIRAdatok
    {
        [System.Xml.Serialization.XmlType("BaseINT_HAIRAdatokBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Obj_Id = true;
            public bool Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }
            }


            private bool _ObjTip_Id = true;
            public bool ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }
            }


            private bool _Tipus = true;
            public bool Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }

            private bool _Ertek = true;
            public bool Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }
            }

            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }


           private bool _Tranz_id = true;
            public bool Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Obj_Id = Value;

                ObjTip_Id = Value;

                Tipus = Value;

                Ertek = Value;

                ErvKezd = Value;

                ErvVege = Value;

                Tranz_id = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseINT_HAIRAdatokBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlGuid _Obj_Id = SqlGuid.Null;

            /// <summary>
            /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }
            }


            private SqlGuid _ObjTip_Id = SqlGuid.Null;

            /// <summary>
            /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }
            }


            private SqlString _Tipus = SqlString.Null;

            /// <summary>
            /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }

            private SqlString _Ertek = SqlString.Null;

            /// <summary>
            /// Ertek Base property </summary>
            public SqlString Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }
            }

            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }
        }
    }
}