
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_Log BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_Log
    {
        [System.Xml.Serialization.XmlType("BaseINT_LogBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Modul_id = true;
               public bool Modul_id
               {
                   get { return _Modul_id; }
                   set { _Modul_id = value; }
               }
                                                            
                                 
               private bool _Sync_StartDate = true;
               public bool Sync_StartDate
               {
                   get { return _Sync_StartDate; }
                   set { _Sync_StartDate = value; }
               }
                                                            
                                 
               private bool _Parancs = true;
               public bool Parancs
               {
                   get { return _Parancs; }
                   set { _Parancs = value; }
               }
                                                            
                                 
               private bool _Machine = true;
               public bool Machine
               {
                   get { return _Machine; }
                   set { _Machine = value; }
               }
                                                            
                                 
               private bool _Sync_EndDate = true;
               public bool Sync_EndDate
               {
                   get { return _Sync_EndDate; }
                   set { _Sync_EndDate = value; }
               }
                                                            
                                 
               private bool _HibaKod = true;
               public bool HibaKod
               {
                   get { return _HibaKod; }
                   set { _HibaKod = value; }
               }
                                                            
                                 
               private bool _HibaUzenet = true;
               public bool HibaUzenet
               {
                   get { return _HibaUzenet; }
                   set { _HibaUzenet = value; }
               }
                                                            
                                 
               private bool _ExternalId = true;
               public bool ExternalId
               {
                   get { return _ExternalId; }
                   set { _ExternalId = value; }
               }
                                                            
                                 
               private bool _Status = true;
               public bool Status
               {
                   get { return _Status; }
                   set { _Status = value; }
               }
                                                            
                                 
               private bool _Dokumentumok_Id_Sent = true;
               public bool Dokumentumok_Id_Sent
               {
                   get { return _Dokumentumok_Id_Sent; }
                   set { _Dokumentumok_Id_Sent = value; }
               }
                                                            
                                 
               private bool _Dokumentumok_Id_Error = true;
               public bool Dokumentumok_Id_Error
               {
                   get { return _Dokumentumok_Id_Error; }
                   set { _Dokumentumok_Id_Error = value; }
               }
                                                            
                                 
               private bool _PackageHash = true;
               public bool PackageHash
               {
                   get { return _PackageHash; }
                   set { _PackageHash = value; }
               }
                                                            
                                 
               private bool _PackageVer = true;
               public bool PackageVer
               {
                   get { return _PackageVer; }
                   set { _PackageVer = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Modul_id = Value;               
                    
                   Sync_StartDate = Value;               
                    
                   Parancs = Value;               
                    
                   Machine = Value;               
                    
                   Sync_EndDate = Value;               
                    
                   HibaKod = Value;               
                    
                   HibaUzenet = Value;               
                    
                   ExternalId = Value;               
                    
                   Status = Value;               
                    
                   Dokumentumok_Id_Sent = Value;               
                    
                   Dokumentumok_Id_Error = Value;               
                    
                   PackageHash = Value;               
                    
                   PackageVer = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_LogBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_id Base property </summary>
            public SqlGuid Modul_id
            {
                get { return _Modul_id; }
                set { _Modul_id = value; }                                                        
            }        
                   
           
        private SqlDateTime _Sync_StartDate = SqlDateTime.Null;
           
        /// <summary>
        /// Sync_StartDate Base property </summary>
            public SqlDateTime Sync_StartDate
            {
                get { return _Sync_StartDate; }
                set { _Sync_StartDate = value; }                                                        
            }        
                   
           
        private SqlString _Parancs = SqlString.Null;
           
        /// <summary>
        /// Parancs Base property </summary>
            public SqlString Parancs
            {
                get { return _Parancs; }
                set { _Parancs = value; }                                                        
            }        
                   
           
        private SqlString _Machine = SqlString.Null;
           
        /// <summary>
        /// Machine Base property </summary>
            public SqlString Machine
            {
                get { return _Machine; }
                set { _Machine = value; }                                                        
            }        
                   
           
        private SqlDateTime _Sync_EndDate = SqlDateTime.Null;
           
        /// <summary>
        /// Sync_EndDate Base property </summary>
            public SqlDateTime Sync_EndDate
            {
                get { return _Sync_EndDate; }
                set { _Sync_EndDate = value; }                                                        
            }        
                   
           
        private SqlString _HibaKod = SqlString.Null;
           
        /// <summary>
        /// HibaKod Base property </summary>
            public SqlString HibaKod
            {
                get { return _HibaKod; }
                set { _HibaKod = value; }                                                        
            }        
                   
           
        private SqlString _HibaUzenet = SqlString.Null;
           
        /// <summary>
        /// HibaUzenet Base property </summary>
            public SqlString HibaUzenet
            {
                get { return _HibaUzenet; }
                set { _HibaUzenet = value; }                                                        
            }        
                   
           
        private SqlString _ExternalId = SqlString.Null;
           
        /// <summary>
        /// ExternalId Base property </summary>
            public SqlString ExternalId
            {
                get { return _ExternalId; }
                set { _ExternalId = value; }                                                        
            }        
                   
           
        private SqlString _Status = SqlString.Null;
           
        /// <summary>
        /// Status Base property </summary>
            public SqlString Status
            {
                get { return _Status; }
                set { _Status = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentumok_Id_Sent = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentumok_Id_Sent Base property </summary>
            public SqlGuid Dokumentumok_Id_Sent
            {
                get { return _Dokumentumok_Id_Sent; }
                set { _Dokumentumok_Id_Sent = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentumok_Id_Error = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentumok_Id_Error Base property </summary>
            public SqlGuid Dokumentumok_Id_Error
            {
                get { return _Dokumentumok_Id_Error; }
                set { _Dokumentumok_Id_Error = value; }                                                        
            }        
                   
           
        private SqlString _PackageHash = SqlString.Null;
           
        /// <summary>
        /// PackageHash Base property </summary>
            public SqlString PackageHash
            {
                get { return _PackageHash; }
                set { _PackageHash = value; }                                                        
            }        
                   
           
        private SqlInt32 _PackageVer = SqlInt32.Null;
           
        /// <summary>
        /// PackageVer Base property </summary>
            public SqlInt32 PackageVer
            {
                get { return _PackageVer; }
                set { _PackageVer = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}