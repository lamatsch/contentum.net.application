
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_Modulok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_Modulok
    {
        [System.Xml.Serialization.XmlType("BaseINT_ModulokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Statusz = true;
               public bool Statusz
               {
                   get { return _Statusz; }
                   set { _Statusz = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Parancs = true;
               public bool Parancs
               {
                   get { return _Parancs; }
                   set { _Parancs = value; }
               }
                                                            
                
			   private bool _Gyakorisag = true;
               public bool Gyakorisag
               {
                   get { return _Gyakorisag; }
                   set { _Gyakorisag = value; }
               }
                 
               private bool _GyakorisagMertekegyseg = true;
               public bool GyakorisagMertekegyseg
               {
                   get { return _GyakorisagMertekegyseg; }
                   set { _GyakorisagMertekegyseg = value; }
               }
                                                            
                                 
               private bool _UtolsoFutas = true;
               public bool UtolsoFutas
               {
                   get { return _UtolsoFutas; }
                   set { _UtolsoFutas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _Tranz_id = true;
               public bool Tranz_id
               {
                   get { return _Tranz_id; }
                   set { _Tranz_id = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Nev = Value;               
                    
                   Statusz = Value;               
                    
                   Leiras = Value;               
                    
                   Parancs = Value;               
                   
				   Gyakorisag = Value;               
 
                   GyakorisagMertekegyseg = Value;               
                    
                   UtolsoFutas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   Tranz_id = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_ModulokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlChars _Statusz = SqlChars.Null;
           
        /// <summary>
        /// Statusz Base property </summary>
            public SqlChars Statusz
            {
                get { return _Statusz; }
                set { _Statusz = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _Parancs = SqlString.Null;
           
        /// <summary>
        /// Parancs Base property </summary>
            public SqlString Parancs
            {
                get { return _Parancs; }
                set { _Parancs = value; }                                                        
            }        
                   
		private SqlInt32 _Gyakorisag = SqlInt32.Null;
           
        /// <summary>
        /// Gyakorisag Base property </summary>
            public SqlInt32 Gyakorisag
            {
                get { return _Gyakorisag; }
                set { _Gyakorisag = value; }                                                        
            }                   

        private SqlString _GyakorisagMertekegyseg = SqlString.Null;
           
        /// <summary>
        /// GyakorisagMertekegyseg Base property </summary>
            public SqlString GyakorisagMertekegyseg
            {
                get { return _GyakorisagMertekegyseg; }
                set { _GyakorisagMertekegyseg = value; }                                                        
            }        
                   
           
        private SqlDateTime _UtolsoFutas = SqlDateTime.Null;
           
        /// <summary>
        /// UtolsoFutas Base property </summary>
            public SqlDateTime UtolsoFutas
            {
                get { return _UtolsoFutas; }
                set { _UtolsoFutas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlGuid _Tranz_id = SqlGuid.Null;
           
        /// <summary>
        /// Tranz_id Base property </summary>
            public SqlGuid Tranz_id
            {
                get { return _Tranz_id; }
                set { _Tranz_id = value; }                                                        
            }        
                           }
    }    
}