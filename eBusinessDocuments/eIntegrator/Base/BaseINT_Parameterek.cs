
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_Parameterek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseINT_Parameterek
    {
        [System.Xml.Serialization.XmlType("BaseINT_ParameterekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Modul_id = true;
               public bool Modul_id
               {
                   get { return _Modul_id; }
                   set { _Modul_id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_id = true;
               public bool Felhasznalo_id
               {
                   get { return _Felhasznalo_id; }
                   set { _Felhasznalo_id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Ertek = true;
               public bool Ertek
               {
                   get { return _Ertek; }
                   set { _Ertek = value; }
               }
                                                            
                                 
               private bool _Karbantarthato = true;
               public bool Karbantarthato
               {
                   get { return _Karbantarthato; }
                   set { _Karbantarthato = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Modul_id = Value;               
                    
                   Felhasznalo_id = Value;               
                    
                   Nev = Value;               
                    
                   Ertek = Value;               
                    
                   Karbantarthato = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseINT_ParameterekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_id Base property </summary>
            public SqlGuid Modul_id
            {
                get { return _Modul_id; }
                set { _Modul_id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_id Base property </summary>
            public SqlGuid Felhasznalo_id
            {
                get { return _Felhasznalo_id; }
                set { _Felhasznalo_id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Ertek = SqlString.Null;
           
        /// <summary>
        /// Ertek Base property </summary>
            public SqlString Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }                                                        
            }        
                   
           
        private SqlChars _Karbantarthato = SqlChars.Null;
           
        /// <summary>
        /// Karbantarthato Base property </summary>
            public SqlChars Karbantarthato
            {
                get { return _Karbantarthato; }
                set { _Karbantarthato = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}