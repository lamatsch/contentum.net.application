
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_AutoEUzenetSzabaly BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class INT_AutoEUzenetSzabaly : BaseINT_AutoEUzenetSzabaly
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Szabaly property
        /// 
        /// </summary>
        public String Szabaly
        {
            get { return Utility.GetStringFromSqlString(Typed.Szabaly); }
            set { Typed.Szabaly = Utility.SetSqlStringFromString(value, Typed.Szabaly); }                                            
        }
                   
           
        /// <summary>
        /// Sorrend property
        /// 
        /// </summary>
        public String Sorrend
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorrend); }
            set { Typed.Sorrend = Utility.SetSqlInt32FromString(value, Typed.Sorrend); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Leallito property
        /// 
        /// </summary>
        public String Leallito
        {
            get { return Utility.GetStringFromSqlChars(Typed.Leallito); }
            set { Typed.Leallito = Utility.SetSqlCharsFromString(value, Typed.Leallito); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}