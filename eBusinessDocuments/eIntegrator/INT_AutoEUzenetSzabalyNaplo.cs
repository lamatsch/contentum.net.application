
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_AutoEUzenetSzabalyNaplo BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class INT_AutoEUzenetSzabalyNaplo : BaseINT_AutoEUzenetSzabalyNaplo
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ESzabalyId property
        /// 
        /// </summary>
        public String ESzabalyId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ESzabalyId); }
            set { Typed.ESzabalyId = Utility.SetSqlGuidFromString(value, Typed.ESzabalyId); }                                            
        }
                   
           
        /// <summary>
        /// EUzenetId property
        /// 
        /// </summary>
        public String EUzenetId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.EUzenetId); }
            set { Typed.EUzenetId = Utility.SetSqlGuidFromString(value, Typed.EUzenetId); }                                            
        }
                   
           
        /// <summary>
        /// EUzenetTipusId property
        /// 
        /// </summary>
        public String EUzenetTipusId
        {
            get { return Utility.GetStringFromSqlString(Typed.EUzenetTipusId); }
            set { Typed.EUzenetTipusId = Utility.SetSqlStringFromString(value, Typed.EUzenetTipusId); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// 
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}