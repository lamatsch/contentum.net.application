
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_ExternalIDs BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class INT_ExternalIDs : BaseINT_ExternalIDs
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Modul_Id property
        /// 
        /// </summary>
        public String Modul_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modul_Id); }
            set { Typed.Modul_Id = Utility.SetSqlGuidFromString(value, Typed.Modul_Id); }                                            
        }
                   
           
        /// <summary>
        /// Edok_Type property
        /// 
        /// </summary>
        public String Edok_Type
        {
            get { return Utility.GetStringFromSqlString(Typed.Edok_Type); }
            set { Typed.Edok_Type = Utility.SetSqlStringFromString(value, Typed.Edok_Type); }                                            
        }
                   
           
        /// <summary>
        /// Edok_Id property
        /// 
        /// </summary>
        public String Edok_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Edok_Id); }
            set { Typed.Edok_Id = Utility.SetSqlGuidFromString(value, Typed.Edok_Id); }                                            
        }
                   
           
        /// <summary>
        /// Edok_ExpiredDate property
        /// 
        /// </summary>
        public String Edok_ExpiredDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Edok_ExpiredDate); }
            set { Typed.Edok_ExpiredDate = Utility.SetSqlDateTimeFromString(value, Typed.Edok_ExpiredDate); }                                            
        }
                   
           
        /// <summary>
        /// External_Group property
        /// 
        /// </summary>
        public String External_Group
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Group); }
            set { Typed.External_Group = Utility.SetSqlStringFromString(value, Typed.External_Group); }                                            
        }
                   
           
        /// <summary>
        /// External_Id property
        /// 
        /// </summary>
        public String External_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.External_Id); }
            set { Typed.External_Id = Utility.SetSqlGuidFromString(value, Typed.External_Id); }                                            
        }
                   
           
        /// <summary>
        /// Deleted property
        /// 
        /// </summary>
        public String Deleted
        {
            get { return Utility.GetStringFromSqlChars(Typed.Deleted); }
            set { Typed.Deleted = Utility.SetSqlCharsFromString(value, Typed.Deleted); }                                            
        }
                   
           
        /// <summary>
        /// LastSync property
        /// 
        /// </summary>
        public String LastSync
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LastSync); }
            set { Typed.LastSync = Utility.SetSqlDateTimeFromString(value, Typed.LastSync); }                                            
        }
                   
           
        /// <summary>
        /// State property
        /// 
        /// </summary>
        public String State
        {
            get { return Utility.GetStringFromSqlString(Typed.State); }
            set { Typed.State = Utility.SetSqlStringFromString(value, Typed.State); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}