
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_Log BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class INT_Log : BaseINT_Log
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// 
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Modul_id property
        /// 
        /// </summary>
        public String Modul_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modul_id); }
            set { Typed.Modul_id = Utility.SetSqlGuidFromString(value, Typed.Modul_id); }                                            
        }
                   
           
        /// <summary>
        /// Sync_StartDate property
        /// 
        /// </summary>
        public String Sync_StartDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Sync_StartDate); }
            set { Typed.Sync_StartDate = Utility.SetSqlDateTimeFromString(value, Typed.Sync_StartDate); }                                            
        }
                   
           
        /// <summary>
        /// Parancs property
        /// 
        /// </summary>
        public String Parancs
        {
            get { return Utility.GetStringFromSqlString(Typed.Parancs); }
            set { Typed.Parancs = Utility.SetSqlStringFromString(value, Typed.Parancs); }                                            
        }
                   
           
        /// <summary>
        /// Machine property
        /// 
        /// </summary>
        public String Machine
        {
            get { return Utility.GetStringFromSqlString(Typed.Machine); }
            set { Typed.Machine = Utility.SetSqlStringFromString(value, Typed.Machine); }                                            
        }
                   
           
        /// <summary>
        /// Sync_EndDate property
        /// 
        /// </summary>
        public String Sync_EndDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Sync_EndDate); }
            set { Typed.Sync_EndDate = Utility.SetSqlDateTimeFromString(value, Typed.Sync_EndDate); }                                            
        }
                   
           
        /// <summary>
        /// HibaKod property
        /// 
        /// </summary>
        public String HibaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaKod); }
            set { Typed.HibaKod = Utility.SetSqlStringFromString(value, Typed.HibaKod); }                                            
        }
                   
           
        /// <summary>
        /// HibaUzenet property
        /// 
        /// </summary>
        public String HibaUzenet
        {
            get { return Utility.GetStringFromSqlString(Typed.HibaUzenet); }
            set { Typed.HibaUzenet = Utility.SetSqlStringFromString(value, Typed.HibaUzenet); }                                            
        }
                   
           
        /// <summary>
        /// ExternalId property
        /// 
        /// </summary>
        public String ExternalId
        {
            get { return Utility.GetStringFromSqlString(Typed.ExternalId); }
            set { Typed.ExternalId = Utility.SetSqlStringFromString(value, Typed.ExternalId); }                                            
        }
                   
           
        /// <summary>
        /// Status property
        /// 
        /// </summary>
        public String Status
        {
            get { return Utility.GetStringFromSqlString(Typed.Status); }
            set { Typed.Status = Utility.SetSqlStringFromString(value, Typed.Status); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentumok_Id_Sent property
        /// 
        /// </summary>
        public String Dokumentumok_Id_Sent
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentumok_Id_Sent); }
            set { Typed.Dokumentumok_Id_Sent = Utility.SetSqlGuidFromString(value, Typed.Dokumentumok_Id_Sent); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentumok_Id_Error property
        /// 
        /// </summary>
        public String Dokumentumok_Id_Error
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentumok_Id_Error); }
            set { Typed.Dokumentumok_Id_Error = Utility.SetSqlGuidFromString(value, Typed.Dokumentumok_Id_Error); }                                            
        }
                   
           
        /// <summary>
        /// PackageHash property
        /// 
        /// </summary>
        public String PackageHash
        {
            get { return Utility.GetStringFromSqlString(Typed.PackageHash); }
            set { Typed.PackageHash = Utility.SetSqlStringFromString(value, Typed.PackageHash); }                                            
        }
                   
           
        /// <summary>
        /// PackageVer property
        /// 
        /// </summary>
        public String PackageVer
        {
            get { return Utility.GetStringFromSqlInt32(Typed.PackageVer); }
            set { Typed.PackageVer = Utility.SetSqlInt32FromString(value, Typed.PackageVer); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}