
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// INT_Modulok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class INT_Modulok : BaseINT_Modulok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// 
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Statusz property
        /// 
        /// </summary>
        public String Statusz
        {
            get { return Utility.GetStringFromSqlChars(Typed.Statusz); }
            set { Typed.Statusz = Utility.SetSqlCharsFromString(value, Typed.Statusz); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Parancs property
        /// 
        /// </summary>
        public String Parancs
        {
            get { return Utility.GetStringFromSqlString(Typed.Parancs); }
            set { Typed.Parancs = Utility.SetSqlStringFromString(value, Typed.Parancs); }                                            
        }
                   
		/// <summary>
        /// Gyakorisag property
        /// 
        /// </summary>
        public String Gyakorisag
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Gyakorisag); }
            set { Typed.Gyakorisag = Utility.SetSqlInt32FromString(value, Typed.Gyakorisag); }                                            
        }
           
        /// <summary>
        /// GyakorisagMertekegyseg property
        /// 
        /// </summary>
        public String GyakorisagMertekegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.GyakorisagMertekegyseg); }
            set { Typed.GyakorisagMertekegyseg = Utility.SetSqlStringFromString(value, Typed.GyakorisagMertekegyseg); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoFutas property
        /// 
        /// </summary>
        public String UtolsoFutas
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoFutas); }
            set { Typed.UtolsoFutas = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoFutas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// Tranz_id property
        /// 
        /// </summary>
        public String Tranz_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tranz_id); }
            set { Typed.Tranz_id = Utility.SetSqlGuidFromString(value, Typed.Tranz_id); }                                            
        }
                           }
   
}