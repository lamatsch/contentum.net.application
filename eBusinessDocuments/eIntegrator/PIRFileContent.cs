﻿using System;

namespace Contentum.eBusinessDocuments
{
    public class PIRFileContent
    {
        String _id;
        String _vevoBesorolas;
        // BLG_3876
        String _adoszam;
        String _fileName;
        byte[] _content;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        // BLG_3876
        public string Adoszam
        {
            get { return _adoszam; }
            set { _adoszam = value; }
        }
        //CR3359 Számla átadásnál(PIR interface) új csoportosító mező(ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        public string VevoBesorolas
        {
            get { return _vevoBesorolas; }
            set { _vevoBesorolas = value; }
        }

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public byte[] Content
        {
            get { return _content; }
            set { _content = value; }
        }

        // BLG_3876
        //public PIRFileContent(String id, String vevoBesorolas, String fileName, byte[] content)
        //{
        //    _id = id;
        //    _vevoBesorolas = vevoBesorolas;
        //    _fileName = fileName;
        //    _content = content;
        //}
        public PIRFileContent(String id, String adoszam, String vevoBesorolas, String fileName, byte[] content)
        {
            _id = id;
            _adoszam = adoszam;
            _vevoBesorolas = vevoBesorolas;
            _fileName = fileName;
            _content = content;
        }

        public PIRFileContent()
        {
            _id = null;
            // BLG_3876
            _adoszam = null;
            _vevoBesorolas = null;
            _fileName = "";
            _content = null;
        }
    }
}