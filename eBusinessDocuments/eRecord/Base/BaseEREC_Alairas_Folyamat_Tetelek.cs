
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Alairas_Folyamat_Tetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Alairas_Folyamat_Tetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Alairas_Folyamat_TetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _AlairasFolyamat_Id = true;
               public bool AlairasFolyamat_Id
               {
                   get { return _AlairasFolyamat_Id; }
                   set { _AlairasFolyamat_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _Csatolmany_Id = true;
               public bool Csatolmany_Id
               {
                   get { return _Csatolmany_Id; }
                   set { _Csatolmany_Id = value; }
               }
                                                            
                                 
               private bool _AlairasStatus = true;
               public bool AlairasStatus
               {
                   get { return _AlairasStatus; }
                   set { _AlairasStatus = value; }
               }
                                                            
                                 
               private bool _Hiba = true;
               public bool Hiba
               {
                   get { return _Hiba; }
                   set { _Hiba = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   AlairasFolyamat_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   Csatolmany_Id = Value;               
                    
                   AlairasStatus = Value;               
                    
                   Hiba = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Alairas_Folyamat_TetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _AlairasFolyamat_Id = SqlGuid.Null;
           
        /// <summary>
        /// AlairasFolyamat_Id Base property </summary>
            public SqlGuid AlairasFolyamat_Id
            {
                get { return _AlairasFolyamat_Id; }
                set { _AlairasFolyamat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csatolmany_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csatolmany_Id Base property </summary>
            public SqlGuid Csatolmany_Id
            {
                get { return _Csatolmany_Id; }
                set { _Csatolmany_Id = value; }                                                        
            }        
                   
           
        private SqlString _AlairasStatus = SqlString.Null;
           
        /// <summary>
        /// AlairasStatus Base property </summary>
            public SqlString AlairasStatus
            {
                get { return _AlairasStatus; }
                set { _AlairasStatus = value; }                                                        
            }        
                   
           
        private SqlString _Hiba = SqlString.Null;
           
        /// <summary>
        /// Hiba Base property </summary>
            public SqlString Hiba
            {
                get { return _Hiba; }
                set { _Hiba = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}