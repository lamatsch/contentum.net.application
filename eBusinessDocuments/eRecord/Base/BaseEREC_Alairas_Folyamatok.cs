
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Alairas_Folyamatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Alairas_Folyamatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Alairas_FolyamatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _AlairasStarted = true;
               public bool AlairasStarted
               {
                   get { return _AlairasStarted; }
                   set { _AlairasStarted = value; }
               }
                                                            
                                 
               private bool _AlairasStopped = true;
               public bool AlairasStopped
               {
                   get { return _AlairasStopped; }
                   set { _AlairasStopped = value; }
               }
                                                            
                                 
               private bool _AlairasStatus = true;
               public bool AlairasStatus
               {
                   get { return _AlairasStatus; }
                   set { _AlairasStatus = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   AlairasStarted = Value;               
                    
                   AlairasStopped = Value;               
                    
                   AlairasStatus = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Alairas_FolyamatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _AlairasStarted = SqlDateTime.Null;
           
        /// <summary>
        /// AlairasStarted Base property </summary>
            public SqlDateTime AlairasStarted
            {
                get { return _AlairasStarted; }
                set { _AlairasStarted = value; }                                                        
            }        
                   
           
        private SqlDateTime _AlairasStopped = SqlDateTime.Null;
           
        /// <summary>
        /// AlairasStopped Base property </summary>
            public SqlDateTime AlairasStopped
            {
                get { return _AlairasStopped; }
                set { _AlairasStopped = value; }                                                        
            }        
                   
           
        private SqlString _AlairasStatus = SqlString.Null;
           
        /// <summary>
        /// AlairasStatus Base property </summary>
            public SqlString AlairasStatus
            {
                get { return _AlairasStatus; }
                set { _AlairasStatus = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}