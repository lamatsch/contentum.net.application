
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Csatolmanyok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Csatolmanyok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_CsatolmanyokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Lapszam = true;
               public bool Lapszam
               {
                   get { return _Lapszam; }
                   set { _Lapszam = value; }
               }
                                                            
                                 
               private bool _KapcsolatJelleg = true;
               public bool KapcsolatJelleg
               {
                   get { return _KapcsolatJelleg; }
                   set { _KapcsolatJelleg = value; }
               }
                                                            
                                 
               private bool _DokumentumSzerep = true;
               public bool DokumentumSzerep
               {
                   get { return _DokumentumSzerep; }
                   set { _DokumentumSzerep = value; }
               }
                                                            
                                 
               private bool _IratAlairoJel = true;
               public bool IratAlairoJel
               {
                   get { return _IratAlairoJel; }
                   set { _IratAlairoJel = value; }
               }
                                                            
                                 
               private bool _CustomId = true;
               public bool CustomId
               {
                   get { return _CustomId; }
                   set { _CustomId = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   Leiras = Value;               
                    
                   Lapszam = Value;               
                    
                   KapcsolatJelleg = Value;               
                    
                   DokumentumSzerep = Value;               
                    
                   IratAlairoJel = Value;               
                    
                   CustomId = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_CsatolmanyokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlInt32 _Lapszam = SqlInt32.Null;
           
        /// <summary>
        /// Lapszam Base property </summary>
            public SqlInt32 Lapszam
            {
                get { return _Lapszam; }
                set { _Lapszam = value; }                                                        
            }        
                   
           
        private SqlString _KapcsolatJelleg = SqlString.Null;
           
        /// <summary>
        /// KapcsolatJelleg Base property </summary>
            public SqlString KapcsolatJelleg
            {
                get { return _KapcsolatJelleg; }
                set { _KapcsolatJelleg = value; }                                                        
            }        
                   
           
        private SqlString _DokumentumSzerep = SqlString.Null;
           
        /// <summary>
        /// DokumentumSzerep Base property </summary>
            public SqlString DokumentumSzerep
            {
                get { return _DokumentumSzerep; }
                set { _DokumentumSzerep = value; }                                                        
            }        
                   
           
        private SqlChars _IratAlairoJel = SqlChars.Null;
           
        /// <summary>
        /// IratAlairoJel Base property </summary>
            public SqlChars IratAlairoJel
            {
                get { return _IratAlairoJel; }
                set { _IratAlairoJel = value; }                                                        
            }        
                   
           
        private SqlString _CustomId = SqlString.Null;
           
        /// <summary>
        /// CustomId Base property </summary>
            public SqlString CustomId
            {
                get { return _CustomId; }
                set { _CustomId = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}