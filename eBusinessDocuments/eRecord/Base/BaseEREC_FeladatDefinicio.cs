
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_FeladatDefinicio BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_FeladatDefinicio
    {
        [System.Xml.Serialization.XmlType("BaseEREC_FeladatDefinicioBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Kivalto = true;
               public bool Funkcio_Id_Kivalto
               {
                   get { return _Funkcio_Id_Kivalto; }
                   set { _Funkcio_Id_Kivalto = value; }
               }
                                                            
                                 
               private bool _Obj_Metadefinicio_Id = true;
               public bool Obj_Metadefinicio_Id
               {
                   get { return _Obj_Metadefinicio_Id; }
                   set { _Obj_Metadefinicio_Id = value; }
               }
                                                            
                                 
               private bool _ObjStateValue_Id = true;
               public bool ObjStateValue_Id
               {
                   get { return _ObjStateValue_Id; }
                   set { _ObjStateValue_Id = value; }
               }
                                                            
                                 
               private bool _FeladatDefinicioTipus = true;
               public bool FeladatDefinicioTipus
               {
                   get { return _FeladatDefinicioTipus; }
                   set { _FeladatDefinicioTipus = value; }
               }
                                                            
                                 
               private bool _FeladatSorszam = true;
               public bool FeladatSorszam
               {
                   get { return _FeladatSorszam; }
                   set { _FeladatSorszam = value; }
               }
                                                            
                                 
               private bool _MuveletDefinicio = true;
               public bool MuveletDefinicio
               {
                   get { return _MuveletDefinicio; }
                   set { _MuveletDefinicio = value; }
               }
                                                            
                                 
               private bool _SpecMuveletDefinicio = true;
               public bool SpecMuveletDefinicio
               {
                   get { return _SpecMuveletDefinicio; }
                   set { _SpecMuveletDefinicio = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Inditando = true;
               public bool Funkcio_Id_Inditando
               {
                   get { return _Funkcio_Id_Inditando; }
                   set { _Funkcio_Id_Inditando = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Futtatando = true;
               public bool Funkcio_Id_Futtatando
               {
                   get { return _Funkcio_Id_Futtatando; }
                   set { _Funkcio_Id_Futtatando = value; }
               }
                                                            
                                 
               private bool _BazisObjLeiro = true;
               public bool BazisObjLeiro
               {
                   get { return _BazisObjLeiro; }
                   set { _BazisObjLeiro = value; }
               }
                                                            
                                 
               private bool _FelelosTipus = true;
               public bool FelelosTipus
               {
                   get { return _FelelosTipus; }
                   set { _FelelosTipus = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Felelos = true;
               public bool Obj_Id_Felelos
               {
                   get { return _Obj_Id_Felelos; }
                   set { _Obj_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _FelelosLeiro = true;
               public bool FelelosLeiro
               {
                   get { return _FelelosLeiro; }
                   set { _FelelosLeiro = value; }
               }
                                                            
                                 
               private bool _Idobazis = true;
               public bool Idobazis
               {
                   get { return _Idobazis; }
                   set { _Idobazis = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_DateCol = true;
               public bool ObjTip_Id_DateCol
               {
                   get { return _ObjTip_Id_DateCol; }
                   set { _ObjTip_Id_DateCol = value; }
               }
                                                            
                                 
               private bool _AtfutasiIdo = true;
               public bool AtfutasiIdo
               {
                   get { return _AtfutasiIdo; }
                   set { _AtfutasiIdo = value; }
               }
                                                            
                                 
               private bool _Idoegyseg = true;
               public bool Idoegyseg
               {
                   get { return _Idoegyseg; }
                   set { _Idoegyseg = value; }
               }
                                                            
                                 
               private bool _FeladatLeiras = true;
               public bool FeladatLeiras
               {
                   get { return _FeladatLeiras; }
                   set { _FeladatLeiras = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }
                                                            
                                 
               private bool _LezarasPrioritas = true;
               public bool LezarasPrioritas
               {
                   get { return _LezarasPrioritas; }
                   set { _LezarasPrioritas = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_type = true;
               public bool Obj_type
               {
                   get { return _Obj_type; }
                   set { _Obj_type = value; }
               }
                                                            
                                 
               private bool _Obj_SzukitoFeltetel = true;
               public bool Obj_SzukitoFeltetel
               {
                   get { return _Obj_SzukitoFeltetel; }
                   set { _Obj_SzukitoFeltetel = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Funkcio_Id_Kivalto = Value;               
                    
                   Obj_Metadefinicio_Id = Value;               
                    
                   ObjStateValue_Id = Value;               
                    
                   FeladatDefinicioTipus = Value;               
                    
                   FeladatSorszam = Value;               
                    
                   MuveletDefinicio = Value;               
                    
                   SpecMuveletDefinicio = Value;               
                    
                   Funkcio_Id_Inditando = Value;               
                    
                   Funkcio_Id_Futtatando = Value;               
                    
                   BazisObjLeiro = Value;               
                    
                   FelelosTipus = Value;               
                    
                   Obj_Id_Felelos = Value;               
                    
                   FelelosLeiro = Value;               
                    
                   Idobazis = Value;               
                    
                   ObjTip_Id_DateCol = Value;               
                    
                   AtfutasiIdo = Value;               
                    
                   Idoegyseg = Value;               
                    
                   FeladatLeiras = Value;               
                    
                   Tipus = Value;               
                    
                   Prioritas = Value;               
                    
                   LezarasPrioritas = Value;               
                    
                   Allapot = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_type = Value;               
                    
                   Obj_SzukitoFeltetel = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_FeladatDefinicioBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Kivalto = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Kivalto Base property </summary>
            public SqlGuid Funkcio_Id_Kivalto
            {
                get { return _Funkcio_Id_Kivalto; }
                set { _Funkcio_Id_Kivalto = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Metadefinicio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Metadefinicio_Id Base property </summary>
            public SqlGuid Obj_Metadefinicio_Id
            {
                get { return _Obj_Metadefinicio_Id; }
                set { _Obj_Metadefinicio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjStateValue_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjStateValue_Id Base property </summary>
            public SqlGuid ObjStateValue_Id
            {
                get { return _ObjStateValue_Id; }
                set { _ObjStateValue_Id = value; }                                                        
            }        
                   
           
        private SqlString _FeladatDefinicioTipus = SqlString.Null;
           
        /// <summary>
        /// FeladatDefinicioTipus Base property </summary>
            public SqlString FeladatDefinicioTipus
            {
                get { return _FeladatDefinicioTipus; }
                set { _FeladatDefinicioTipus = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeladatSorszam = SqlInt32.Null;
           
        /// <summary>
        /// FeladatSorszam Base property </summary>
            public SqlInt32 FeladatSorszam
            {
                get { return _FeladatSorszam; }
                set { _FeladatSorszam = value; }                                                        
            }        
                   
           
        private SqlString _MuveletDefinicio = SqlString.Null;
           
        /// <summary>
        /// MuveletDefinicio Base property </summary>
            public SqlString MuveletDefinicio
            {
                get { return _MuveletDefinicio; }
                set { _MuveletDefinicio = value; }                                                        
            }        
                   
           
        private SqlString _SpecMuveletDefinicio = SqlString.Null;
           
        /// <summary>
        /// SpecMuveletDefinicio Base property </summary>
            public SqlString SpecMuveletDefinicio
            {
                get { return _SpecMuveletDefinicio; }
                set { _SpecMuveletDefinicio = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Inditando = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Inditando Base property </summary>
            public SqlGuid Funkcio_Id_Inditando
            {
                get { return _Funkcio_Id_Inditando; }
                set { _Funkcio_Id_Inditando = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Futtatando = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Futtatando Base property </summary>
            public SqlGuid Funkcio_Id_Futtatando
            {
                get { return _Funkcio_Id_Futtatando; }
                set { _Funkcio_Id_Futtatando = value; }                                                        
            }        
                   
           
        private SqlString _BazisObjLeiro = SqlString.Null;
           
        /// <summary>
        /// BazisObjLeiro Base property </summary>
            public SqlString BazisObjLeiro
            {
                get { return _BazisObjLeiro; }
                set { _BazisObjLeiro = value; }                                                        
            }        
                   
           
        private SqlString _FelelosTipus = SqlString.Null;
           
        /// <summary>
        /// FelelosTipus Base property </summary>
            public SqlString FelelosTipus
            {
                get { return _FelelosTipus; }
                set { _FelelosTipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Felelos Base property </summary>
            public SqlGuid Obj_Id_Felelos
            {
                get { return _Obj_Id_Felelos; }
                set { _Obj_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlString _FelelosLeiro = SqlString.Null;
           
        /// <summary>
        /// FelelosLeiro Base property </summary>
            public SqlString FelelosLeiro
            {
                get { return _FelelosLeiro; }
                set { _FelelosLeiro = value; }                                                        
            }        
                   
           
        private SqlString _Idobazis = SqlString.Null;
           
        /// <summary>
        /// Idobazis Base property </summary>
            public SqlString Idobazis
            {
                get { return _Idobazis; }
                set { _Idobazis = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_DateCol = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_DateCol Base property </summary>
            public SqlGuid ObjTip_Id_DateCol
            {
                get { return _ObjTip_Id_DateCol; }
                set { _ObjTip_Id_DateCol = value; }                                                        
            }        
                   
           
        private SqlInt32 _AtfutasiIdo = SqlInt32.Null;
           
        /// <summary>
        /// AtfutasiIdo Base property </summary>
            public SqlInt32 AtfutasiIdo
            {
                get { return _AtfutasiIdo; }
                set { _AtfutasiIdo = value; }                                                        
            }        
                   
           
        private SqlString _Idoegyseg = SqlString.Null;
           
        /// <summary>
        /// Idoegyseg Base property </summary>
            public SqlString Idoegyseg
            {
                get { return _Idoegyseg; }
                set { _Idoegyseg = value; }                                                        
            }        
                   
           
        private SqlString _FeladatLeiras = SqlString.Null;
           
        /// <summary>
        /// FeladatLeiras Base property </summary>
            public SqlString FeladatLeiras
            {
                get { return _FeladatLeiras; }
                set { _FeladatLeiras = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Prioritas = SqlString.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlString Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }        
                   
           
        private SqlString _LezarasPrioritas = SqlString.Null;
           
        /// <summary>
        /// LezarasPrioritas Base property </summary>
            public SqlString LezarasPrioritas
            {
                get { return _LezarasPrioritas; }
                set { _LezarasPrioritas = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_type = SqlString.Null;
           
        /// <summary>
        /// Obj_type Base property </summary>
            public SqlString Obj_type
            {
                get { return _Obj_type; }
                set { _Obj_type = value; }                                                        
            }        
                   
           
        private SqlString _Obj_SzukitoFeltetel = SqlString.Null;
           
        /// <summary>
        /// Obj_SzukitoFeltetel Base property </summary>
            public SqlString Obj_SzukitoFeltetel
            {
                get { return _Obj_SzukitoFeltetel; }
                set { _Obj_SzukitoFeltetel = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}