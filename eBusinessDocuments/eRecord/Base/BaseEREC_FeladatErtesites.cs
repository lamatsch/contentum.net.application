
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_FeladatErtesites BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_FeladatErtesites
    {
        [System.Xml.Serialization.XmlType("BaseEREC_FeladatErtesitesBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _FeladatDefinicio_id = true;
               public bool FeladatDefinicio_id
               {
                   get { return _FeladatDefinicio_id; }
                   set { _FeladatDefinicio_id = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _Ertesites = true;
               public bool Ertesites
               {
                   get { return _Ertesites; }
                   set { _Ertesites = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   FeladatDefinicio_id = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   Ertesites = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_FeladatErtesitesBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _FeladatDefinicio_id = SqlGuid.Null;
           
        /// <summary>
        /// FeladatDefinicio_id Base property </summary>
            public SqlGuid FeladatDefinicio_id
            {
                get { return _FeladatDefinicio_id; }
                set { _FeladatDefinicio_id = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Ertesites = SqlChars.Null;
           
        /// <summary>
        /// Ertesites Base property </summary>
            public SqlChars Ertesites
            {
                get { return _Ertesites; }
                set { _Ertesites = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}