
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_HataridosFeladatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_HataridosFeladatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_HataridosFeladatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Esemeny_Id_Kivalto = true;
               public bool Esemeny_Id_Kivalto
               {
                   get { return _Esemeny_Id_Kivalto; }
                   set { _Esemeny_Id_Kivalto = value; }
               }
                                                            
                                 
               private bool _Esemeny_Id_Lezaro = true;
               public bool Esemeny_Id_Lezaro
               {
                   get { return _Esemeny_Id_Lezaro; }
                   set { _Esemeny_Id_Lezaro = value; }
               }
                                                            
                                 
               private bool _Funkcio_Id_Inditando = true;
               public bool Funkcio_Id_Inditando
               {
                   get { return _Funkcio_Id_Inditando; }
                   set { _Funkcio_Id_Inditando = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_Felelos = true;
               public bool Felhasznalo_Id_Felelos
               {
                   get { return _Felhasznalo_Id_Felelos; }
                   set { _Felhasznalo_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Kiado = true;
               public bool Csoport_Id_Kiado
               {
                   get { return _Csoport_Id_Kiado; }
                   set { _Csoport_Id_Kiado = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_Kiado = true;
               public bool Felhasznalo_Id_Kiado
               {
                   get { return _Felhasznalo_Id_Kiado; }
                   set { _Felhasznalo_Id_Kiado = value; }
               }
                                                            
                                 
               private bool _HataridosFeladat_Id = true;
               public bool HataridosFeladat_Id
               {
                   get { return _HataridosFeladat_Id; }
                   set { _HataridosFeladat_Id = value; }
               }
                                                            
                                 
               private bool _ReszFeladatSorszam = true;
               public bool ReszFeladatSorszam
               {
                   get { return _ReszFeladatSorszam; }
                   set { _ReszFeladatSorszam = value; }
               }
                                                            
                                 
               private bool _FeladatDefinicio_Id = true;
               public bool FeladatDefinicio_Id
               {
                   get { return _FeladatDefinicio_Id; }
                   set { _FeladatDefinicio_Id = value; }
               }
                                                            
                                 
               private bool _FeladatDefinicio_Id_Lezaro = true;
               public bool FeladatDefinicio_Id_Lezaro
               {
                   get { return _FeladatDefinicio_Id_Lezaro; }
                   set { _FeladatDefinicio_Id_Lezaro = value; }
               }
                                                            
                                 
               private bool _Forras = true;
               public bool Forras
               {
                   get { return _Forras; }
                   set { _Forras = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Altipus = true;
               public bool Altipus
               {
                   get { return _Altipus; }
                   set { _Altipus = value; }
               }
                                                            
                                 
               private bool _Memo = true;
               public bool Memo
               {
                   get { return _Memo; }
                   set { _Memo = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }
                                                            
                                 
               private bool _LezarasPrioritas = true;
               public bool LezarasPrioritas
               {
                   get { return _LezarasPrioritas; }
                   set { _LezarasPrioritas = value; }
               }
                                                            
                                 
               private bool _KezdesiIdo = true;
               public bool KezdesiIdo
               {
                   get { return _KezdesiIdo; }
                   set { _KezdesiIdo = value; }
               }
                                                            
                                 
               private bool _IntezkHatarido = true;
               public bool IntezkHatarido
               {
                   get { return _IntezkHatarido; }
                   set { _IntezkHatarido = value; }
               }
                                                            
                                 
               private bool _LezarasDatuma = true;
               public bool LezarasDatuma
               {
                   get { return _LezarasDatuma; }
                   set { _LezarasDatuma = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_type = true;
               public bool Obj_type
               {
                   get { return _Obj_type; }
                   set { _Obj_type = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Megoldas = true;
               public bool Megoldas
               {
                   get { return _Megoldas; }
                   set { _Megoldas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Esemeny_Id_Kivalto = Value;               
                    
                   Esemeny_Id_Lezaro = Value;               
                    
                   Funkcio_Id_Inditando = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Felhasznalo_Id_Felelos = Value;               
                    
                   Csoport_Id_Kiado = Value;               
                    
                   Felhasznalo_Id_Kiado = Value;               
                    
                   HataridosFeladat_Id = Value;               
                    
                   ReszFeladatSorszam = Value;               
                    
                   FeladatDefinicio_Id = Value;               
                    
                   FeladatDefinicio_Id_Lezaro = Value;               
                    
                   Forras = Value;               
                    
                   Tipus = Value;               
                    
                   Altipus = Value;               
                    
                   Memo = Value;               
                    
                   Leiras = Value;               
                    
                   Prioritas = Value;               
                    
                   LezarasPrioritas = Value;               
                    
                   KezdesiIdo = Value;               
                    
                   IntezkHatarido = Value;               
                    
                   LezarasDatuma = Value;               
                    
                   Azonosito = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_type = Value;               
                    
                   Allapot = Value;               
                    
                   Megoldas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_HataridosFeladatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Esemeny_Id_Kivalto = SqlGuid.Null;
           
        /// <summary>
        /// Esemeny_Id_Kivalto Base property </summary>
            public SqlGuid Esemeny_Id_Kivalto
            {
                get { return _Esemeny_Id_Kivalto; }
                set { _Esemeny_Id_Kivalto = value; }                                                        
            }        
                   
           
        private SqlGuid _Esemeny_Id_Lezaro = SqlGuid.Null;
           
        /// <summary>
        /// Esemeny_Id_Lezaro Base property </summary>
            public SqlGuid Esemeny_Id_Lezaro
            {
                get { return _Esemeny_Id_Lezaro; }
                set { _Esemeny_Id_Lezaro = value; }                                                        
            }        
                   
           
        private SqlGuid _Funkcio_Id_Inditando = SqlGuid.Null;
           
        /// <summary>
        /// Funkcio_Id_Inditando Base property </summary>
            public SqlGuid Funkcio_Id_Inditando
            {
                get { return _Funkcio_Id_Inditando; }
                set { _Funkcio_Id_Inditando = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_Felelos Base property </summary>
            public SqlGuid Felhasznalo_Id_Felelos
            {
                get { return _Felhasznalo_Id_Felelos; }
                set { _Felhasznalo_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Kiado = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Kiado Base property </summary>
            public SqlGuid Csoport_Id_Kiado
            {
                get { return _Csoport_Id_Kiado; }
                set { _Csoport_Id_Kiado = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_Kiado = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_Kiado Base property </summary>
            public SqlGuid Felhasznalo_Id_Kiado
            {
                get { return _Felhasznalo_Id_Kiado; }
                set { _Felhasznalo_Id_Kiado = value; }                                                        
            }        
                   
           
        private SqlGuid _HataridosFeladat_Id = SqlGuid.Null;
           
        /// <summary>
        /// HataridosFeladat_Id Base property </summary>
            public SqlGuid HataridosFeladat_Id
            {
                get { return _HataridosFeladat_Id; }
                set { _HataridosFeladat_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _ReszFeladatSorszam = SqlInt32.Null;
           
        /// <summary>
        /// ReszFeladatSorszam Base property </summary>
            public SqlInt32 ReszFeladatSorszam
            {
                get { return _ReszFeladatSorszam; }
                set { _ReszFeladatSorszam = value; }                                                        
            }        
                   
           
        private SqlGuid _FeladatDefinicio_Id = SqlGuid.Null;
           
        /// <summary>
        /// FeladatDefinicio_Id Base property </summary>
            public SqlGuid FeladatDefinicio_Id
            {
                get { return _FeladatDefinicio_Id; }
                set { _FeladatDefinicio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _FeladatDefinicio_Id_Lezaro = SqlGuid.Null;
           
        /// <summary>
        /// FeladatDefinicio_Id_Lezaro Base property </summary>
            public SqlGuid FeladatDefinicio_Id_Lezaro
            {
                get { return _FeladatDefinicio_Id_Lezaro; }
                set { _FeladatDefinicio_Id_Lezaro = value; }                                                        
            }        
                   
           
        private SqlChars _Forras = SqlChars.Null;
           
        /// <summary>
        /// Forras Base property </summary>
            public SqlChars Forras
            {
                get { return _Forras; }
                set { _Forras = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Altipus = SqlString.Null;
           
        /// <summary>
        /// Altipus Base property </summary>
            public SqlString Altipus
            {
                get { return _Altipus; }
                set { _Altipus = value; }                                                        
            }        
                   
           
        private SqlChars _Memo = SqlChars.Null;
           
        /// <summary>
        /// Memo Base property </summary>
            public SqlChars Memo
            {
                get { return _Memo; }
                set { _Memo = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _Prioritas = SqlString.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlString Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }        
                   
           
        private SqlString _LezarasPrioritas = SqlString.Null;
           
        /// <summary>
        /// LezarasPrioritas Base property </summary>
            public SqlString LezarasPrioritas
            {
                get { return _LezarasPrioritas; }
                set { _LezarasPrioritas = value; }                                                        
            }        
                   
           
        private SqlDateTime _KezdesiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// KezdesiIdo Base property </summary>
            public SqlDateTime KezdesiIdo
            {
                get { return _KezdesiIdo; }
                set { _KezdesiIdo = value; }                                                        
            }        
                   
           
        private SqlDateTime _IntezkHatarido = SqlDateTime.Null;
           
        /// <summary>
        /// IntezkHatarido Base property </summary>
            public SqlDateTime IntezkHatarido
            {
                get { return _IntezkHatarido; }
                set { _IntezkHatarido = value; }                                                        
            }        
                   
           
        private SqlDateTime _LezarasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// LezarasDatuma Base property </summary>
            public SqlDateTime LezarasDatuma
            {
                get { return _LezarasDatuma; }
                set { _LezarasDatuma = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_type = SqlString.Null;
           
        /// <summary>
        /// Obj_type Base property </summary>
            public SqlString Obj_type
            {
                get { return _Obj_type; }
                set { _Obj_type = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _Megoldas = SqlString.Null;
           
        /// <summary>
        /// Megoldas Base property </summary>
            public SqlString Megoldas
            {
                get { return _Megoldas; }
                set { _Megoldas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}