
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraElosztoivTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraElosztoivTetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraElosztoivTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _ElosztoIv_Id = true;
               public bool ElosztoIv_Id
               {
                   get { return _ElosztoIv_Id; }
                   set { _ElosztoIv_Id = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Partner_Id = true;
               public bool Partner_Id
               {
                   get { return _Partner_Id; }
                   set { _Partner_Id = value; }
               }
                                                            
                                 
               private bool _Cim_Id = true;
               public bool Cim_Id
               {
                   get { return _Cim_Id; }
                   set { _Cim_Id = value; }
               }
                                                            
                                 
               private bool _CimSTR = true;
               public bool CimSTR
               {
                   get { return _CimSTR; }
                   set { _CimSTR = value; }
               }
                                                            
                                 
               private bool _NevSTR = true;
               public bool NevSTR
               {
                   get { return _NevSTR; }
                   set { _NevSTR = value; }
               }
                                                            
                                 
               private bool _Kuldesmod = true;
               public bool Kuldesmod
               {
                   get { return _Kuldesmod; }
                   set { _Kuldesmod = value; }
               }
                                                            
                                 
               private bool _Visszavarolag = true;
               public bool Visszavarolag
               {
                   get { return _Visszavarolag; }
                   set { _Visszavarolag = value; }
               }
                                                            
                                 
               private bool _VisszavarasiIdo = true;
               public bool VisszavarasiIdo
               {
                   get { return _VisszavarasiIdo; }
                   set { _VisszavarasiIdo = value; }
               }
                                                            
                                 
               private bool _Vissza_Nap_Mulva = true;
               public bool Vissza_Nap_Mulva
               {
                   get { return _Vissza_Nap_Mulva; }
                   set { _Vissza_Nap_Mulva = value; }
               }
                                                            
                                 
               private bool _AlairoSzerep = true;
               public bool AlairoSzerep
               {
                   get { return _AlairoSzerep; }
                   set { _AlairoSzerep = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   ElosztoIv_Id = Value;               
                    
                   Sorszam = Value;               
                    
                   Partner_Id = Value;               
                    
                   Cim_Id = Value;               
                    
                   CimSTR = Value;               
                    
                   NevSTR = Value;               
                    
                   Kuldesmod = Value;               
                    
                   Visszavarolag = Value;               
                    
                   VisszavarasiIdo = Value;               
                    
                   Vissza_Nap_Mulva = Value;               
                    
                   AlairoSzerep = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraElosztoivTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ElosztoIv_Id = SqlGuid.Null;
           
        /// <summary>
        /// ElosztoIv_Id Base property </summary>
            public SqlGuid ElosztoIv_Id
            {
                get { return _ElosztoIv_Id; }
                set { _ElosztoIv_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id Base property </summary>
            public SqlGuid Partner_Id
            {
                get { return _Partner_Id; }
                set { _Partner_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Cim_Id = SqlGuid.Null;
           
        /// <summary>
        /// Cim_Id Base property </summary>
            public SqlGuid Cim_Id
            {
                get { return _Cim_Id; }
                set { _Cim_Id = value; }                                                        
            }        
                   
           
        private SqlString _CimSTR = SqlString.Null;
           
        /// <summary>
        /// CimSTR Base property </summary>
            public SqlString CimSTR
            {
                get { return _CimSTR; }
                set { _CimSTR = value; }                                                        
            }        
                   
           
        private SqlString _NevSTR = SqlString.Null;
           
        /// <summary>
        /// NevSTR Base property </summary>
            public SqlString NevSTR
            {
                get { return _NevSTR; }
                set { _NevSTR = value; }                                                        
            }        
                   
           
        private SqlString _Kuldesmod = SqlString.Null;
           
        /// <summary>
        /// Kuldesmod Base property </summary>
            public SqlString Kuldesmod
            {
                get { return _Kuldesmod; }
                set { _Kuldesmod = value; }                                                        
            }        
                   
           
        private SqlChars _Visszavarolag = SqlChars.Null;
           
        /// <summary>
        /// Visszavarolag Base property </summary>
            public SqlChars Visszavarolag
            {
                get { return _Visszavarolag; }
                set { _Visszavarolag = value; }                                                        
            }        
                   
           
        private SqlDateTime _VisszavarasiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// VisszavarasiIdo Base property </summary>
            public SqlDateTime VisszavarasiIdo
            {
                get { return _VisszavarasiIdo; }
                set { _VisszavarasiIdo = value; }                                                        
            }        
                   
           
        private SqlInt32 _Vissza_Nap_Mulva = SqlInt32.Null;
           
        /// <summary>
        /// Vissza_Nap_Mulva Base property </summary>
            public SqlInt32 Vissza_Nap_Mulva
            {
                get { return _Vissza_Nap_Mulva; }
                set { _Vissza_Nap_Mulva = value; }                                                        
            }        
                   
           
        private SqlChars _AlairoSzerep = SqlChars.Null;
           
        /// <summary>
        /// AlairoSzerep Base property </summary>
            public SqlChars AlairoSzerep
            {
                get { return _AlairoSzerep; }
                set { _AlairoSzerep = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}