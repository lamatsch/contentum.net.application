
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraElosztoivek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraElosztoivek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraElosztoivekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Ev = true;
               public bool Ev
               {
                   get { return _Ev; }
                   set { _Ev = value; }
               }
                                                            
                                 
               private bool _Fajta = true;
               public bool Fajta
               {
                   get { return _Fajta; }
                   set { _Fajta = value; }
               }
                                                            
                                 
               private bool _Kod = true;
               public bool Kod
               {
                   get { return _Kod; }
                   set { _Kod = value; }
               }
                                                            
                                 
               private bool _NEV = true;
               public bool NEV
               {
                   get { return _NEV; }
                   set { _NEV = value; }
               }
                                                            
                                 
               private bool _Hasznalat = true;
               public bool Hasznalat
               {
                   get { return _Hasznalat; }
                   set { _Hasznalat = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Ev = Value;               
                    
                   Fajta = Value;               
                    
                   Kod = Value;               
                    
                   NEV = Value;               
                    
                   Hasznalat = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraElosztoivekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlInt32 _Ev = SqlInt32.Null;
           
        /// <summary>
        /// Ev Base property </summary>
            public SqlInt32 Ev
            {
                get { return _Ev; }
                set { _Ev = value; }                                                        
            }        
                   
           
        private SqlString _Fajta = SqlString.Null;
           
        /// <summary>
        /// Fajta Base property </summary>
            public SqlString Fajta
            {
                get { return _Fajta; }
                set { _Fajta = value; }                                                        
            }        
                   
           
        private SqlString _Kod = SqlString.Null;
           
        /// <summary>
        /// Kod Base property </summary>
            public SqlString Kod
            {
                get { return _Kod; }
                set { _Kod = value; }                                                        
            }        
                   
           
        private SqlString _NEV = SqlString.Null;
           
        /// <summary>
        /// NEV Base property </summary>
            public SqlString NEV
            {
                get { return _NEV; }
                set { _NEV = value; }                                                        
            }        
                   
           
        private SqlString _Hasznalat = SqlString.Null;
           
        /// <summary>
        /// Hasznalat Base property </summary>
            public SqlString Hasznalat
            {
                get { return _Hasznalat; }
                set { _Hasznalat = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}