
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIktatoKonyvek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraIktatoKonyvek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraIktatoKonyvekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Ev = true;
               public bool Ev
               {
                   get { return _Ev; }
                   set { _Ev = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _DefaultIrattariTetelszam = true;
               public bool DefaultIrattariTetelszam
               {
                   get { return _DefaultIrattariTetelszam; }
                   set { _DefaultIrattariTetelszam = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _MegkulJelzes = true;
               public bool MegkulJelzes
               {
                   get { return _MegkulJelzes; }
                   set { _MegkulJelzes = value; }
               }
                                                            
                                 
               private bool _Iktatohely = true;
               public bool Iktatohely
               {
                   get { return _Iktatohely; }
                   set { _Iktatohely = value; }
               }
                                                            
                                 
               private bool _KozpontiIktatasJelzo = true;
               public bool KozpontiIktatasJelzo
               {
                   get { return _KozpontiIktatasJelzo; }
                   set { _KozpontiIktatasJelzo = value; }
               }
                                                            
                                 
               private bool _UtolsoFoszam = true;
               public bool UtolsoFoszam
               {
                   get { return _UtolsoFoszam; }
                   set { _UtolsoFoszam = value; }
               }
                                                            
                                 
               private bool _UtolsoSorszam = true;
               public bool UtolsoSorszam
               {
                   get { return _UtolsoSorszam; }
                   set { _UtolsoSorszam = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Olvaso = true;
               public bool Csoport_Id_Olvaso
               {
                   get { return _Csoport_Id_Olvaso; }
                   set { _Csoport_Id_Olvaso = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _IktSzamOsztas = true;
               public bool IktSzamOsztas
               {
                   get { return _IktSzamOsztas; }
                   set { _IktSzamOsztas = value; }
               }
                                                            
                                 
               private bool _FormatumKod = true;
               public bool FormatumKod
               {
                   get { return _FormatumKod; }
                   set { _FormatumKod = value; }
               }
                                                            
                                 
               private bool _Titkos = true;
               public bool Titkos
               {
                   get { return _Titkos; }
                   set { _Titkos = value; }
               }
                                                            
                                 
               private bool _IktatoErkezteto = true;
               public bool IktatoErkezteto
               {
                   get { return _IktatoErkezteto; }
                   set { _IktatoErkezteto = value; }
               }
                                                            
                                 
               private bool _LezarasDatuma = true;
               public bool LezarasDatuma
               {
                   get { return _LezarasDatuma; }
                   set { _LezarasDatuma = value; }
               }
                                                            
                                 
               private bool _PostakonyvVevokod = true;
               public bool PostakonyvVevokod
               {
                   get { return _PostakonyvVevokod; }
                   set { _PostakonyvVevokod = value; }
               }
                                                            
                                 
               private bool _PostakonyvMegallapodasAzon = true;
               public bool PostakonyvMegallapodasAzon
               {
                   get { return _PostakonyvMegallapodasAzon; }
                   set { _PostakonyvMegallapodasAzon = value; }
               }
                                                            
                                 
               private bool _EFeladoJegyzekUgyfelAdatok = true;
               public bool EFeladoJegyzekUgyfelAdatok
               {
                   get { return _EFeladoJegyzekUgyfelAdatok; }
                   set { _EFeladoJegyzekUgyfelAdatok = value; }
               }
                                                            
                                 
               private bool _HitelesExport_Dok_ID = true;
               public bool HitelesExport_Dok_ID
               {
                   get { return _HitelesExport_Dok_ID; }
                   set { _HitelesExport_Dok_ID = value; }
               }
                                                            
                                 
               private bool _Ujranyitando = true;
               public bool Ujranyitando
               {
                   get { return _Ujranyitando; }
                   set { _Ujranyitando = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _Statusz = true;
               public bool Statusz
               {
                   get { return _Statusz; }
                   set { _Statusz = value; }
               }
                                                            
                                 
               private bool _Terjedelem = true;
               public bool Terjedelem
               {
                   get { return _Terjedelem; }
                   set { _Terjedelem = value; }
               }
                                                            
                                 
               private bool _SelejtezesDatuma = true;
               public bool SelejtezesDatuma
               {
                   get { return _SelejtezesDatuma; }
                   set { _SelejtezesDatuma = value; }
               }
                                                            
                                 
               private bool _KezelesTipusa = true;
               public bool KezelesTipusa
               {
                   get { return _KezelesTipusa; }
                   set { _KezelesTipusa = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Ev = Value;               
                    
                   Azonosito = Value;               
                    
                   DefaultIrattariTetelszam = Value;               
                    
                   Nev = Value;               
                    
                   MegkulJelzes = Value;               
                    
                   Iktatohely = Value;               
                    
                   KozpontiIktatasJelzo = Value;               
                    
                   UtolsoFoszam = Value;               
                    
                   UtolsoSorszam = Value;               
                    
                   Csoport_Id_Olvaso = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   IktSzamOsztas = Value;               
                    
                   FormatumKod = Value;               
                    
                   Titkos = Value;               
                    
                   IktatoErkezteto = Value;               
                    
                   LezarasDatuma = Value;               
                    
                   PostakonyvVevokod = Value;               
                    
                   PostakonyvMegallapodasAzon = Value;               
                    
                   EFeladoJegyzekUgyfelAdatok = Value;               
                    
                   HitelesExport_Dok_ID = Value;               
                    
                   Ujranyitando = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   Statusz = Value;               
                    
                   Terjedelem = Value;               
                    
                   SelejtezesDatuma = Value;               
                    
                   KezelesTipusa = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraIktatoKonyvekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlInt32 _Ev = SqlInt32.Null;
           
        /// <summary>
        /// Ev Base property </summary>
            public SqlInt32 Ev
            {
                get { return _Ev; }
                set { _Ev = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlGuid _DefaultIrattariTetelszam = SqlGuid.Null;
           
        /// <summary>
        /// DefaultIrattariTetelszam Base property </summary>
            public SqlGuid DefaultIrattariTetelszam
            {
                get { return _DefaultIrattariTetelszam; }
                set { _DefaultIrattariTetelszam = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _MegkulJelzes = SqlString.Null;
           
        /// <summary>
        /// MegkulJelzes Base property </summary>
            public SqlString MegkulJelzes
            {
                get { return _MegkulJelzes; }
                set { _MegkulJelzes = value; }                                                        
            }        
                   
           
        private SqlString _Iktatohely = SqlString.Null;
           
        /// <summary>
        /// Iktatohely Base property </summary>
            public SqlString Iktatohely
            {
                get { return _Iktatohely; }
                set { _Iktatohely = value; }                                                        
            }        
                   
           
        private SqlChars _KozpontiIktatasJelzo = SqlChars.Null;
           
        /// <summary>
        /// KozpontiIktatasJelzo Base property </summary>
            public SqlChars KozpontiIktatasJelzo
            {
                get { return _KozpontiIktatasJelzo; }
                set { _KozpontiIktatasJelzo = value; }                                                        
            }        
                   
           
        private SqlInt32 _UtolsoFoszam = SqlInt32.Null;
           
        /// <summary>
        /// UtolsoFoszam Base property </summary>
            public SqlInt32 UtolsoFoszam
            {
                get { return _UtolsoFoszam; }
                set { _UtolsoFoszam = value; }                                                        
            }        
                   
           
        private SqlInt32 _UtolsoSorszam = SqlInt32.Null;
           
        /// <summary>
        /// UtolsoSorszam Base property </summary>
            public SqlInt32 UtolsoSorszam
            {
                get { return _UtolsoSorszam; }
                set { _UtolsoSorszam = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Olvaso = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Olvaso Base property </summary>
            public SqlGuid Csoport_Id_Olvaso
            {
                get { return _Csoport_Id_Olvaso; }
                set { _Csoport_Id_Olvaso = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlString _IktSzamOsztas = SqlString.Null;
           
        /// <summary>
        /// IktSzamOsztas Base property </summary>
            public SqlString IktSzamOsztas
            {
                get { return _IktSzamOsztas; }
                set { _IktSzamOsztas = value; }                                                        
            }        
                   
           
        private SqlChars _FormatumKod = SqlChars.Null;
           
        /// <summary>
        /// FormatumKod Base property </summary>
            public SqlChars FormatumKod
            {
                get { return _FormatumKod; }
                set { _FormatumKod = value; }                                                        
            }        
                   
           
        private SqlChars _Titkos = SqlChars.Null;
           
        /// <summary>
        /// Titkos Base property </summary>
            public SqlChars Titkos
            {
                get { return _Titkos; }
                set { _Titkos = value; }                                                        
            }        
                   
           
        private SqlChars _IktatoErkezteto = SqlChars.Null;
           
        /// <summary>
        /// IktatoErkezteto Base property </summary>
            public SqlChars IktatoErkezteto
            {
                get { return _IktatoErkezteto; }
                set { _IktatoErkezteto = value; }                                                        
            }        
                   
           
        private SqlDateTime _LezarasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// LezarasDatuma Base property </summary>
            public SqlDateTime LezarasDatuma
            {
                get { return _LezarasDatuma; }
                set { _LezarasDatuma = value; }                                                        
            }        
                   
           
        private SqlString _PostakonyvVevokod = SqlString.Null;
           
        /// <summary>
        /// PostakonyvVevokod Base property </summary>
            public SqlString PostakonyvVevokod
            {
                get { return _PostakonyvVevokod; }
                set { _PostakonyvVevokod = value; }                                                        
            }        
                   
           
        private SqlString _PostakonyvMegallapodasAzon = SqlString.Null;
           
        /// <summary>
        /// PostakonyvMegallapodasAzon Base property </summary>
            public SqlString PostakonyvMegallapodasAzon
            {
                get { return _PostakonyvMegallapodasAzon; }
                set { _PostakonyvMegallapodasAzon = value; }                                                        
            }        
                   
           
        private SqlString _EFeladoJegyzekUgyfelAdatok = SqlString.Null;
           
        /// <summary>
        /// EFeladoJegyzekUgyfelAdatok Base property </summary>
            public SqlString EFeladoJegyzekUgyfelAdatok
            {
                get { return _EFeladoJegyzekUgyfelAdatok; }
                set { _EFeladoJegyzekUgyfelAdatok = value; }                                                        
            }        
                   
           
        private SqlGuid _HitelesExport_Dok_ID = SqlGuid.Null;
           
        /// <summary>
        /// HitelesExport_Dok_ID Base property </summary>
            public SqlGuid HitelesExport_Dok_ID
            {
                get { return _HitelesExport_Dok_ID; }
                set { _HitelesExport_Dok_ID = value; }                                                        
            }        
                   
           
        private SqlChars _Ujranyitando = SqlChars.Null;
           
        /// <summary>
        /// Ujranyitando Base property </summary>
            public SqlChars Ujranyitando
            {
                get { return _Ujranyitando; }
                set { _Ujranyitando = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlChars _Statusz = SqlChars.Null;
           
        /// <summary>
        /// Statusz Base property </summary>
            public SqlChars Statusz
            {
                get { return _Statusz; }
                set { _Statusz = value; }                                                        
            }        
                   
           
        private SqlString _Terjedelem = SqlString.Null;
           
        /// <summary>
        /// Terjedelem Base property </summary>
            public SqlString Terjedelem
            {
                get { return _Terjedelem; }
                set { _Terjedelem = value; }                                                        
            }        
                   
           
        private SqlDateTime _SelejtezesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// SelejtezesDatuma Base property </summary>
            public SqlDateTime SelejtezesDatuma
            {
                get { return _SelejtezesDatuma; }
                set { _SelejtezesDatuma = value; }                                                        
            }        
                   
           
        private SqlChars _KezelesTipusa = SqlChars.Null;
           
        /// <summary>
        /// KezelesTipusa Base property </summary>
            public SqlChars KezelesTipusa
            {
                get { return _KezelesTipusa; }
                set { _KezelesTipusa = value; }                                                        
            }        
                           }
    }    
}