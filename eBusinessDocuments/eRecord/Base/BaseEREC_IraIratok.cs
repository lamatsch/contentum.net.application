
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIratok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraIratok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraIratokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _PostazasIranya = true;
               public bool PostazasIranya
               {
                   get { return _PostazasIranya; }
                   set { _PostazasIranya = value; }
               }
                                                            
                                 
               private bool _Alszam = true;
               public bool Alszam
               {
                   get { return _Alszam; }
                   set { _Alszam = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _UtolsoSorszam = true;
               public bool UtolsoSorszam
               {
                   get { return _UtolsoSorszam; }
                   set { _UtolsoSorszam = value; }
               }
                                                            
                                 
               private bool _UgyUgyIratDarab_Id = true;
               public bool UgyUgyIratDarab_Id
               {
                   get { return _UgyUgyIratDarab_Id; }
                   set { _UgyUgyIratDarab_Id = value; }
               }
                                                            
                                 
               private bool _Kategoria = true;
               public bool Kategoria
               {
                   get { return _Kategoria; }
                   set { _Kategoria = value; }
               }
                                                            
                                 
               private bool _HivatkozasiSzam = true;
               public bool HivatkozasiSzam
               {
                   get { return _HivatkozasiSzam; }
                   set { _HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _IktatasDatuma = true;
               public bool IktatasDatuma
               {
                   get { return _IktatasDatuma; }
                   set { _IktatasDatuma = value; }
               }
                                                            
                                 
               private bool _ExpedialasDatuma = true;
               public bool ExpedialasDatuma
               {
                   get { return _ExpedialasDatuma; }
                   set { _ExpedialasDatuma = value; }
               }
                                                            
                                 
               private bool _ExpedialasModja = true;
               public bool ExpedialasModja
               {
                   get { return _ExpedialasModja; }
                   set { _ExpedialasModja = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Expedial = true;
               public bool FelhasznaloCsoport_Id_Expedial
               {
                   get { return _FelhasznaloCsoport_Id_Expedial; }
                   set { _FelhasznaloCsoport_Id_Expedial = value; }
               }
                                                            
                                 
               private bool _Targy = true;
               public bool Targy
               {
                   get { return _Targy; }
                   set { _Targy = value; }
               }
                                                            
                                 
               private bool _Jelleg = true;
               public bool Jelleg
               {
                   get { return _Jelleg; }
                   set { _Jelleg = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Iktato = true;
               public bool FelhasznaloCsoport_Id_Iktato
               {
                   get { return _FelhasznaloCsoport_Id_Iktato; }
                   set { _FelhasznaloCsoport_Id_Iktato = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Kiadmany = true;
               public bool FelhasznaloCsoport_Id_Kiadmany
               {
                   get { return _FelhasznaloCsoport_Id_Kiadmany; }
                   set { _FelhasznaloCsoport_Id_Kiadmany = value; }
               }
                                                            
                                 
               private bool _UgyintezesAlapja = true;
               public bool UgyintezesAlapja
               {
                   get { return _UgyintezesAlapja; }
                   set { _UgyintezesAlapja = value; }
               }
                                                            
                                 
               private bool _AdathordozoTipusa = true;
               public bool AdathordozoTipusa
               {
                   get { return _AdathordozoTipusa; }
                   set { _AdathordozoTipusa = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Ugyfelelos = true;
               public bool Csoport_Id_Ugyfelelos
               {
                   get { return _Csoport_Id_Ugyfelelos; }
                   set { _Csoport_Id_Ugyfelelos = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Orzo = true;
               public bool FelhasznaloCsoport_Id_Orzo
               {
                   get { return _FelhasznaloCsoport_Id_Orzo; }
                   set { _FelhasznaloCsoport_Id_Orzo = value; }
               }
                                                            
                                 
               private bool _KiadmanyozniKell = true;
               public bool KiadmanyozniKell
               {
                   get { return _KiadmanyozniKell; }
                   set { _KiadmanyozniKell = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Ugyintez = true;
               public bool FelhasznaloCsoport_Id_Ugyintez
               {
                   get { return _FelhasznaloCsoport_Id_Ugyintez; }
                   set { _FelhasznaloCsoport_Id_Ugyintez = value; }
               }
                                                            
                                 
               private bool _Hatarido = true;
               public bool Hatarido
               {
                   get { return _Hatarido; }
                   set { _Hatarido = value; }
               }
                                                            
                                 
               private bool _MegorzesiIdo = true;
               public bool MegorzesiIdo
               {
                   get { return _MegorzesiIdo; }
                   set { _MegorzesiIdo = value; }
               }
                                                            
                                 
               private bool _IratFajta = true;
               public bool IratFajta
               {
                   get { return _IratFajta; }
                   set { _IratFajta = value; }
               }
                                                            
                                 
               private bool _Irattipus = true;
               public bool Irattipus
               {
                   get { return _Irattipus; }
                   set { _Irattipus = value; }
               }
                                                            
                                 
               private bool _KuldKuldemenyek_Id = true;
               public bool KuldKuldemenyek_Id
               {
                   get { return _KuldKuldemenyek_Id; }
                   set { _KuldKuldemenyek_Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _GeneraltTargy = true;
               public bool GeneraltTargy
               {
                   get { return _GeneraltTargy; }
                   set { _GeneraltTargy = value; }
               }
                                                            
                                 
               private bool _IratMetaDef_Id = true;
               public bool IratMetaDef_Id
               {
                   get { return _IratMetaDef_Id; }
                   set { _IratMetaDef_Id = value; }
               }
                                                            
                                 
               private bool _IrattarbaKuldDatuma  = true;
               public bool IrattarbaKuldDatuma 
               {
                   get { return _IrattarbaKuldDatuma ; }
                   set { _IrattarbaKuldDatuma  = value; }
               }
                                                            
                                 
               private bool _IrattarbaVetelDat = true;
               public bool IrattarbaVetelDat
               {
                   get { return _IrattarbaVetelDat; }
                   set { _IrattarbaVetelDat = value; }
               }
                                                            
                                 
               private bool _Ugyirat_Id = true;
               public bool Ugyirat_Id
               {
                   get { return _Ugyirat_Id; }
                   set { _Ugyirat_Id = value; }
               }
                                                            
                                 
               private bool _Minosites = true;
               public bool Minosites
               {
                   get { return _Minosites; }
                   set { _Minosites = value; }
               }
                                                            
                                 
               private bool _Elintezett = true;
               public bool Elintezett
               {
                   get { return _Elintezett; }
                   set { _Elintezett = value; }
               }
                                                            
                                 
               private bool _IratHatasaUgyintezesre = true;
               public bool IratHatasaUgyintezesre
               {
                   get { return _IratHatasaUgyintezesre; }
                   set { _IratHatasaUgyintezesre = value; }
               }
                                                            
                                 
               private bool _FelfuggesztesOka = true;
               public bool FelfuggesztesOka
               {
                   get { return _FelfuggesztesOka; }
                   set { _FelfuggesztesOka = value; }
               }
                                                            
                                 
               private bool _LezarasOka = true;
               public bool LezarasOka
               {
                   get { return _LezarasOka; }
                   set { _LezarasOka = value; }
               }
                                                            
                                 
               private bool _Munkaallomas = true;
               public bool Munkaallomas
               {
                   get { return _Munkaallomas; }
                   set { _Munkaallomas = value; }
               }
                                                            
                                 
               private bool _UgyintezesModja = true;
               public bool UgyintezesModja
               {
                   get { return _UgyintezesModja; }
                   set { _UgyintezesModja = value; }
               }
                                                            
                                 
               private bool _IntezesIdopontja = true;
               public bool IntezesIdopontja
               {
                   get { return _IntezesIdopontja; }
                   set { _IntezesIdopontja = value; }
               }
                                                            
                                 
               private bool _IntezesiIdo = true;
               public bool IntezesiIdo
               {
                   get { return _IntezesiIdo; }
                   set { _IntezesiIdo = value; }
               }
                                                            
                                 
               private bool _IntezesiIdoegyseg = true;
               public bool IntezesiIdoegyseg
               {
                   get { return _IntezesiIdoegyseg; }
                   set { _IntezesiIdoegyseg = value; }
               }
                                                            
                                 
               private bool _UzemzavarKezdete = true;
               public bool UzemzavarKezdete
               {
                   get { return _UzemzavarKezdete; }
                   set { _UzemzavarKezdete = value; }
               }
                                                            
                                 
               private bool _UzemzavarVege = true;
               public bool UzemzavarVege
               {
                   get { return _UzemzavarVege; }
                   set { _UzemzavarVege = value; }
               }
                                                            
                                 
               private bool _UzemzavarIgazoloIratSzama = true;
               public bool UzemzavarIgazoloIratSzama
               {
                   get { return _UzemzavarIgazoloIratSzama; }
                   set { _UzemzavarIgazoloIratSzama = value; }
               }
                                                            
                                 
               private bool _FelfuggesztettNapokSzama = true;
               public bool FelfuggesztettNapokSzama
               {
                   get { return _FelfuggesztettNapokSzama; }
                   set { _FelfuggesztettNapokSzama = value; }
               }
                                                            
                                 
               private bool _VisszafizetesJogcime = true;
               public bool VisszafizetesJogcime
               {
                   get { return _VisszafizetesJogcime; }
                   set { _VisszafizetesJogcime = value; }
               }
                                                            
                                 
               private bool _VisszafizetesOsszege = true;
               public bool VisszafizetesOsszege
               {
                   get { return _VisszafizetesOsszege; }
                   set { _VisszafizetesOsszege = value; }
               }
                                                            
                                 
               private bool _VisszafizetesHatarozatSzama = true;
               public bool VisszafizetesHatarozatSzama
               {
                   get { return _VisszafizetesHatarozatSzama; }
                   set { _VisszafizetesHatarozatSzama = value; }
               }
                                                            
                                 
               private bool _Partner_Id_VisszafizetesCimzet = true;
               public bool Partner_Id_VisszafizetesCimzet
               {
                   get { return _Partner_Id_VisszafizetesCimzet; }
                   set { _Partner_Id_VisszafizetesCimzet = value; }
               }
                                                            
                                 
               private bool _Partner_Nev_VisszafizetCimzett = true;
               public bool Partner_Nev_VisszafizetCimzett
               {
                   get { return _Partner_Nev_VisszafizetCimzett; }
                   set { _Partner_Nev_VisszafizetCimzett = value; }
               }
                                                            
                                 
               private bool _VisszafizetesHatarido = true;
               public bool VisszafizetesHatarido
               {
                   get { return _VisszafizetesHatarido; }
                   set { _VisszafizetesHatarido = value; }
               }
                                                            
                                 
               private bool _Minosito = true;
               public bool Minosito
               {
                   get { return _Minosito; }
                   set { _Minosito = value; }
               }
                                                            
                                 
               private bool _MinositesErvenyessegiIdeje = true;
               public bool MinositesErvenyessegiIdeje
               {
                   get { return _MinositesErvenyessegiIdeje; }
                   set { _MinositesErvenyessegiIdeje = value; }
               }
                                                            
                                 
               private bool _TerjedelemMennyiseg = true;
               public bool TerjedelemMennyiseg
               {
                   get { return _TerjedelemMennyiseg; }
                   set { _TerjedelemMennyiseg = value; }
               }
                                                            
                                 
               private bool _TerjedelemMennyisegiEgyseg = true;
               public bool TerjedelemMennyisegiEgyseg
               {
                   get { return _TerjedelemMennyisegiEgyseg; }
                   set { _TerjedelemMennyisegiEgyseg = value; }
               }
                                                            
                                 
               private bool _TerjedelemMegjegyzes = true;
               public bool TerjedelemMegjegyzes
               {
                   get { return _TerjedelemMegjegyzes; }
                   set { _TerjedelemMegjegyzes = value; }
               }
                                                            
                                 
               private bool _SelejtezesDat = true;
               public bool SelejtezesDat
               {
                   get { return _SelejtezesDat; }
                   set { _SelejtezesDat = value; }
               }
                                                            
                                 
               private bool _FelhCsoport_Id_Selejtezo = true;
               public bool FelhCsoport_Id_Selejtezo
               {
                   get { return _FelhCsoport_Id_Selejtezo; }
                   set { _FelhCsoport_Id_Selejtezo = value; }
               }
                                                            
                                 
               private bool _LeveltariAtvevoNeve = true;
               public bool LeveltariAtvevoNeve
               {
                   get { return _LeveltariAtvevoNeve; }
                   set { _LeveltariAtvevoNeve = value; }
               }
                                                            
                                 
               private bool _ModositasErvenyessegKezdete = true;
               public bool ModositasErvenyessegKezdete
               {
                   get { return _ModositasErvenyessegKezdete; }
                   set { _ModositasErvenyessegKezdete = value; }
               }
                                                            
                                 
               private bool _MegszuntetoHatarozat = true;
               public bool MegszuntetoHatarozat
               {
                   get { return _MegszuntetoHatarozat; }
                   set { _MegszuntetoHatarozat = value; }
               }
                                                            
                                 
               private bool _FelulvizsgalatDatuma = true;
               public bool FelulvizsgalatDatuma
               {
                   get { return _FelulvizsgalatDatuma; }
                   set { _FelulvizsgalatDatuma = value; }
               }
                                                            
                                 
               private bool _Felulvizsgalo_id = true;
               public bool Felulvizsgalo_id
               {
                   get { return _Felulvizsgalo_id; }
                   set { _Felulvizsgalo_id = value; }
               }
                                                            
                                 
               private bool _MinositesFelulvizsgalatEredm = true;
               public bool MinositesFelulvizsgalatEredm
               {
                   get { return _MinositesFelulvizsgalatEredm; }
                   set { _MinositesFelulvizsgalatEredm = value; }
               }
                                                            
                                 
               private bool _UgyintezesKezdoDatuma = true;
               public bool UgyintezesKezdoDatuma
               {
                   get { return _UgyintezesKezdoDatuma; }
                   set { _UgyintezesKezdoDatuma = value; }
               }
                                                            
                                 
               private bool _EljarasiSzakasz = true;
               public bool EljarasiSzakasz
               {
                   get { return _EljarasiSzakasz; }
                   set { _EljarasiSzakasz = value; }
               }
                                                            
                                 
               private bool _HatralevoNapok = true;
               public bool HatralevoNapok
               {
                   get { return _HatralevoNapok; }
                   set { _HatralevoNapok = value; }
               }
                                                            
                                 
               private bool _HatralevoMunkaNapok = true;
               public bool HatralevoMunkaNapok
               {
                   get { return _HatralevoMunkaNapok; }
                   set { _HatralevoMunkaNapok = value; }
               }
                                                            
                                 
               private bool _Ugy_Fajtaja = true;
               public bool Ugy_Fajtaja
               {
                   get { return _Ugy_Fajtaja; }
                   set { _Ugy_Fajtaja = value; }
               }
                                                            
                                 
               private bool _MinositoSzervezet = true;
               public bool MinositoSzervezet
               {
                   get { return _MinositoSzervezet; }
                   set { _MinositoSzervezet = value; }
               }
                                                            
                                 
               private bool _MinositesFelulvizsgalatBizTag = true;
               public bool MinositesFelulvizsgalatBizTag
               {
                   get { return _MinositesFelulvizsgalatBizTag; }
                   set { _MinositesFelulvizsgalatBizTag = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   PostazasIranya = Value;               
                    
                   Alszam = Value;               
                    
                   Sorszam = Value;               
                    
                   UtolsoSorszam = Value;               
                    
                   UgyUgyIratDarab_Id = Value;               
                    
                   Kategoria = Value;               
                    
                   HivatkozasiSzam = Value;               
                    
                   IktatasDatuma = Value;               
                    
                   ExpedialasDatuma = Value;               
                    
                   ExpedialasModja = Value;               
                    
                   FelhasznaloCsoport_Id_Expedial = Value;               
                    
                   Targy = Value;               
                    
                   Jelleg = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   FelhasznaloCsoport_Id_Iktato = Value;               
                    
                   FelhasznaloCsoport_Id_Kiadmany = Value;               
                    
                   UgyintezesAlapja = Value;               
                    
                   AdathordozoTipusa = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Csoport_Id_Ugyfelelos = Value;               
                    
                   FelhasznaloCsoport_Id_Orzo = Value;               
                    
                   KiadmanyozniKell = Value;               
                    
                   FelhasznaloCsoport_Id_Ugyintez = Value;               
                    
                   Hatarido = Value;               
                    
                   MegorzesiIdo = Value;               
                    
                   IratFajta = Value;               
                    
                   Irattipus = Value;               
                    
                   KuldKuldemenyek_Id = Value;               
                    
                   Allapot = Value;               
                    
                   Azonosito = Value;               
                    
                   GeneraltTargy = Value;               
                    
                   IratMetaDef_Id = Value;               
                    
                   IrattarbaKuldDatuma  = Value;               
                    
                   IrattarbaVetelDat = Value;               
                    
                   Ugyirat_Id = Value;               
                    
                   Minosites = Value;               
                    
                   Elintezett = Value;               
                    
                   IratHatasaUgyintezesre = Value;               
                    
                   FelfuggesztesOka = Value;               
                    
                   LezarasOka = Value;               
                    
                   Munkaallomas = Value;               
                    
                   UgyintezesModja = Value;               
                    
                   IntezesIdopontja = Value;               
                    
                   IntezesiIdo = Value;               
                    
                   IntezesiIdoegyseg = Value;               
                    
                   UzemzavarKezdete = Value;               
                    
                   UzemzavarVege = Value;               
                    
                   UzemzavarIgazoloIratSzama = Value;               
                    
                   FelfuggesztettNapokSzama = Value;               
                    
                   VisszafizetesJogcime = Value;               
                    
                   VisszafizetesOsszege = Value;               
                    
                   VisszafizetesHatarozatSzama = Value;               
                    
                   Partner_Id_VisszafizetesCimzet = Value;               
                    
                   Partner_Nev_VisszafizetCimzett = Value;               
                    
                   VisszafizetesHatarido = Value;               
                    
                   Minosito = Value;               
                    
                   MinositesErvenyessegiIdeje = Value;               
                    
                   TerjedelemMennyiseg = Value;               
                    
                   TerjedelemMennyisegiEgyseg = Value;               
                    
                   TerjedelemMegjegyzes = Value;               
                    
                   SelejtezesDat = Value;               
                    
                   FelhCsoport_Id_Selejtezo = Value;               
                    
                   LeveltariAtvevoNeve = Value;               
                    
                   ModositasErvenyessegKezdete = Value;               
                    
                   MegszuntetoHatarozat = Value;               
                    
                   FelulvizsgalatDatuma = Value;               
                    
                   Felulvizsgalo_id = Value;               
                    
                   MinositesFelulvizsgalatEredm = Value;               
                    
                   UgyintezesKezdoDatuma = Value;               
                    
                   EljarasiSzakasz = Value;               
                    
                   HatralevoNapok = Value;               
                    
                   HatralevoMunkaNapok = Value;               
                    
                   Ugy_Fajtaja = Value;               
                    
                   MinositoSzervezet = Value;               
                    
                   MinositesFelulvizsgalatBizTag = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraIratokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlChars _PostazasIranya = SqlChars.Null;
           
        /// <summary>
        /// PostazasIranya Base property </summary>
            public SqlChars PostazasIranya
            {
                get { return _PostazasIranya; }
                set { _PostazasIranya = value; }                                                        
            }        
                   
           
        private SqlInt32 _Alszam = SqlInt32.Null;
           
        /// <summary>
        /// Alszam Base property </summary>
            public SqlInt32 Alszam
            {
                get { return _Alszam; }
                set { _Alszam = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlInt32 _UtolsoSorszam = SqlInt32.Null;
           
        /// <summary>
        /// UtolsoSorszam Base property </summary>
            public SqlInt32 UtolsoSorszam
            {
                get { return _UtolsoSorszam; }
                set { _UtolsoSorszam = value; }                                                        
            }        
                   
           
        private SqlGuid _UgyUgyIratDarab_Id = SqlGuid.Null;
           
        /// <summary>
        /// UgyUgyIratDarab_Id Base property </summary>
            public SqlGuid UgyUgyIratDarab_Id
            {
                get { return _UgyUgyIratDarab_Id; }
                set { _UgyUgyIratDarab_Id = value; }                                                        
            }        
                   
           
        private SqlString _Kategoria = SqlString.Null;
           
        /// <summary>
        /// Kategoria Base property </summary>
            public SqlString Kategoria
            {
                get { return _Kategoria; }
                set { _Kategoria = value; }                                                        
            }        
                   
           
        private SqlString _HivatkozasiSzam = SqlString.Null;
           
        /// <summary>
        /// HivatkozasiSzam Base property </summary>
            public SqlString HivatkozasiSzam
            {
                get { return _HivatkozasiSzam; }
                set { _HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlDateTime _IktatasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// IktatasDatuma Base property </summary>
            public SqlDateTime IktatasDatuma
            {
                get { return _IktatasDatuma; }
                set { _IktatasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _ExpedialasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// ExpedialasDatuma Base property </summary>
            public SqlDateTime ExpedialasDatuma
            {
                get { return _ExpedialasDatuma; }
                set { _ExpedialasDatuma = value; }                                                        
            }        
                   
           
        private SqlString _ExpedialasModja = SqlString.Null;
           
        /// <summary>
        /// ExpedialasModja Base property </summary>
            public SqlString ExpedialasModja
            {
                get { return _ExpedialasModja; }
                set { _ExpedialasModja = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Expedial = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Expedial
            {
                get { return _FelhasznaloCsoport_Id_Expedial; }
                set { _FelhasznaloCsoport_Id_Expedial = value; }                                                        
            }        
                   
           
        private SqlString _Targy = SqlString.Null;
           
        /// <summary>
        /// Targy Base property </summary>
            public SqlString Targy
            {
                get { return _Targy; }
                set { _Targy = value; }                                                        
            }        
                   
           
        private SqlString _Jelleg = SqlString.Null;
           
        /// <summary>
        /// Jelleg Base property </summary>
            public SqlString Jelleg
            {
                get { return _Jelleg; }
                set { _Jelleg = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Iktato = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Iktato Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Iktato
            {
                get { return _FelhasznaloCsoport_Id_Iktato; }
                set { _FelhasznaloCsoport_Id_Iktato = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Kiadmany = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kiadmany Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Kiadmany
            {
                get { return _FelhasznaloCsoport_Id_Kiadmany; }
                set { _FelhasznaloCsoport_Id_Kiadmany = value; }                                                        
            }        
                   
           
        private SqlString _UgyintezesAlapja = SqlString.Null;
           
        /// <summary>
        /// UgyintezesAlapja Base property </summary>
            public SqlString UgyintezesAlapja
            {
                get { return _UgyintezesAlapja; }
                set { _UgyintezesAlapja = value; }                                                        
            }        
                   
           
        private SqlString _AdathordozoTipusa = SqlString.Null;
           
        /// <summary>
        /// AdathordozoTipusa Base property </summary>
            public SqlString AdathordozoTipusa
            {
                get { return _AdathordozoTipusa; }
                set { _AdathordozoTipusa = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Ugyfelelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Ugyfelelos Base property </summary>
            public SqlGuid Csoport_Id_Ugyfelelos
            {
                get { return _Csoport_Id_Ugyfelelos; }
                set { _Csoport_Id_Ugyfelelos = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Orzo = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }                                                        
            }        
                   
           
        private SqlChars _KiadmanyozniKell = SqlChars.Null;
           
        /// <summary>
        /// KiadmanyozniKell Base property </summary>
            public SqlChars KiadmanyozniKell
            {
                get { return _KiadmanyozniKell; }
                set { _KiadmanyozniKell = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Ugyintez = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Ugyintez
            {
                get { return _FelhasznaloCsoport_Id_Ugyintez; }
                set { _FelhasznaloCsoport_Id_Ugyintez = value; }                                                        
            }        
                   
           
        private SqlDateTime _Hatarido = SqlDateTime.Null;
           
        /// <summary>
        /// Hatarido Base property </summary>
            public SqlDateTime Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }                                                        
            }        
                   
           
        private SqlDateTime _MegorzesiIdo = SqlDateTime.Null;
           
        /// <summary>
        /// MegorzesiIdo Base property </summary>
            public SqlDateTime MegorzesiIdo
            {
                get { return _MegorzesiIdo; }
                set { _MegorzesiIdo = value; }                                                        
            }        
                   
           
        private SqlString _IratFajta = SqlString.Null;
           
        /// <summary>
        /// IratFajta Base property </summary>
            public SqlString IratFajta
            {
                get { return _IratFajta; }
                set { _IratFajta = value; }                                                        
            }        
                   
           
        private SqlString _Irattipus = SqlString.Null;
           
        /// <summary>
        /// Irattipus Base property </summary>
            public SqlString Irattipus
            {
                get { return _Irattipus; }
                set { _Irattipus = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemenyek_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemenyek_Id Base property </summary>
            public SqlGuid KuldKuldemenyek_Id
            {
                get { return _KuldKuldemenyek_Id; }
                set { _KuldKuldemenyek_Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _GeneraltTargy = SqlString.Null;
           
        /// <summary>
        /// GeneraltTargy Base property </summary>
            public SqlString GeneraltTargy
            {
                get { return _GeneraltTargy; }
                set { _GeneraltTargy = value; }                                                        
            }        
                   
           
        private SqlGuid _IratMetaDef_Id = SqlGuid.Null;
           
        /// <summary>
        /// IratMetaDef_Id Base property </summary>
            public SqlGuid IratMetaDef_Id
            {
                get { return _IratMetaDef_Id; }
                set { _IratMetaDef_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _IrattarbaKuldDatuma  = SqlDateTime.Null;
           
        /// <summary>
        /// IrattarbaKuldDatuma  Base property </summary>
            public SqlDateTime IrattarbaKuldDatuma 
            {
                get { return _IrattarbaKuldDatuma ; }
                set { _IrattarbaKuldDatuma  = value; }                                                        
            }        
                   
           
        private SqlDateTime _IrattarbaVetelDat = SqlDateTime.Null;
           
        /// <summary>
        /// IrattarbaVetelDat Base property </summary>
            public SqlDateTime IrattarbaVetelDat
            {
                get { return _IrattarbaVetelDat; }
                set { _IrattarbaVetelDat = value; }                                                        
            }        
                   
           
        private SqlGuid _Ugyirat_Id = SqlGuid.Null;
           
        /// <summary>
        /// Ugyirat_Id Base property </summary>
            public SqlGuid Ugyirat_Id
            {
                get { return _Ugyirat_Id; }
                set { _Ugyirat_Id = value; }                                                        
            }        
                   
           
        private SqlString _Minosites = SqlString.Null;
           
        /// <summary>
        /// Minosites Base property </summary>
            public SqlString Minosites
            {
                get { return _Minosites; }
                set { _Minosites = value; }                                                        
            }        
                   
           
        private SqlChars _Elintezett = SqlChars.Null;
           
        /// <summary>
        /// Elintezett Base property </summary>
            public SqlChars Elintezett
            {
                get { return _Elintezett; }
                set { _Elintezett = value; }                                                        
            }        
                   
           
        private SqlString _IratHatasaUgyintezesre = SqlString.Null;
           
        /// <summary>
        /// IratHatasaUgyintezesre Base property </summary>
            public SqlString IratHatasaUgyintezesre
            {
                get { return _IratHatasaUgyintezesre; }
                set { _IratHatasaUgyintezesre = value; }                                                        
            }        
                   
           
        private SqlString _FelfuggesztesOka = SqlString.Null;
           
        /// <summary>
        /// FelfuggesztesOka Base property </summary>
            public SqlString FelfuggesztesOka
            {
                get { return _FelfuggesztesOka; }
                set { _FelfuggesztesOka = value; }                                                        
            }        
                   
           
        private SqlString _LezarasOka = SqlString.Null;
           
        /// <summary>
        /// LezarasOka Base property </summary>
            public SqlString LezarasOka
            {
                get { return _LezarasOka; }
                set { _LezarasOka = value; }                                                        
            }        
                   
           
        private SqlString _Munkaallomas = SqlString.Null;
           
        /// <summary>
        /// Munkaallomas Base property </summary>
            public SqlString Munkaallomas
            {
                get { return _Munkaallomas; }
                set { _Munkaallomas = value; }                                                        
            }        
                   
           
        private SqlString _UgyintezesModja = SqlString.Null;
           
        /// <summary>
        /// UgyintezesModja Base property </summary>
            public SqlString UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }                                                        
            }        
                   
           
        private SqlDateTime _IntezesIdopontja = SqlDateTime.Null;
           
        /// <summary>
        /// IntezesIdopontja Base property </summary>
            public SqlDateTime IntezesIdopontja
            {
                get { return _IntezesIdopontja; }
                set { _IntezesIdopontja = value; }                                                        
            }        
                   
           
        private SqlString _IntezesiIdo = SqlString.Null;
           
        /// <summary>
        /// IntezesiIdo Base property </summary>
            public SqlString IntezesiIdo
            {
                get { return _IntezesiIdo; }
                set { _IntezesiIdo = value; }                                                        
            }        
                   
           
        private SqlString _IntezesiIdoegyseg = SqlString.Null;
           
        /// <summary>
        /// IntezesiIdoegyseg Base property </summary>
            public SqlString IntezesiIdoegyseg
            {
                get { return _IntezesiIdoegyseg; }
                set { _IntezesiIdoegyseg = value; }                                                        
            }        
                   
           
        private SqlDateTime _UzemzavarKezdete = SqlDateTime.Null;
           
        /// <summary>
        /// UzemzavarKezdete Base property </summary>
            public SqlDateTime UzemzavarKezdete
            {
                get { return _UzemzavarKezdete; }
                set { _UzemzavarKezdete = value; }                                                        
            }        
                   
           
        private SqlDateTime _UzemzavarVege = SqlDateTime.Null;
           
        /// <summary>
        /// UzemzavarVege Base property </summary>
            public SqlDateTime UzemzavarVege
            {
                get { return _UzemzavarVege; }
                set { _UzemzavarVege = value; }                                                        
            }        
                   
           
        private SqlString _UzemzavarIgazoloIratSzama = SqlString.Null;
           
        /// <summary>
        /// UzemzavarIgazoloIratSzama Base property </summary>
            public SqlString UzemzavarIgazoloIratSzama
            {
                get { return _UzemzavarIgazoloIratSzama; }
                set { _UzemzavarIgazoloIratSzama = value; }                                                        
            }        
                   
           
        private SqlInt32 _FelfuggesztettNapokSzama = SqlInt32.Null;
           
        /// <summary>
        /// FelfuggesztettNapokSzama Base property </summary>
            public SqlInt32 FelfuggesztettNapokSzama
            {
                get { return _FelfuggesztettNapokSzama; }
                set { _FelfuggesztettNapokSzama = value; }                                                        
            }        
                   
           
        private SqlString _VisszafizetesJogcime = SqlString.Null;
           
        /// <summary>
        /// VisszafizetesJogcime Base property </summary>
            public SqlString VisszafizetesJogcime
            {
                get { return _VisszafizetesJogcime; }
                set { _VisszafizetesJogcime = value; }                                                        
            }        
                   
           
        private SqlInt32 _VisszafizetesOsszege = SqlInt32.Null;
           
        /// <summary>
        /// VisszafizetesOsszege Base property </summary>
            public SqlInt32 VisszafizetesOsszege
            {
                get { return _VisszafizetesOsszege; }
                set { _VisszafizetesOsszege = value; }                                                        
            }        
                   
           
        private SqlString _VisszafizetesHatarozatSzama = SqlString.Null;
           
        /// <summary>
        /// VisszafizetesHatarozatSzama Base property </summary>
            public SqlString VisszafizetesHatarozatSzama
            {
                get { return _VisszafizetesHatarozatSzama; }
                set { _VisszafizetesHatarozatSzama = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_VisszafizetesCimzet = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_VisszafizetesCimzet Base property </summary>
            public SqlGuid Partner_Id_VisszafizetesCimzet
            {
                get { return _Partner_Id_VisszafizetesCimzet; }
                set { _Partner_Id_VisszafizetesCimzet = value; }                                                        
            }        
                   
           
        private SqlString _Partner_Nev_VisszafizetCimzett = SqlString.Null;
           
        /// <summary>
        /// Partner_Nev_VisszafizetCimzett Base property </summary>
            public SqlString Partner_Nev_VisszafizetCimzett
            {
                get { return _Partner_Nev_VisszafizetCimzett; }
                set { _Partner_Nev_VisszafizetCimzett = value; }                                                        
            }        
                   
           
        private SqlDateTime _VisszafizetesHatarido = SqlDateTime.Null;
           
        /// <summary>
        /// VisszafizetesHatarido Base property </summary>
            public SqlDateTime VisszafizetesHatarido
            {
                get { return _VisszafizetesHatarido; }
                set { _VisszafizetesHatarido = value; }                                                        
            }        
                   
           
        private SqlGuid _Minosito = SqlGuid.Null;
           
        /// <summary>
        /// Minosito Base property </summary>
            public SqlGuid Minosito
            {
                get { return _Minosito; }
                set { _Minosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _MinositesErvenyessegiIdeje = SqlDateTime.Null;
           
        /// <summary>
        /// MinositesErvenyessegiIdeje Base property </summary>
            public SqlDateTime MinositesErvenyessegiIdeje
            {
                get { return _MinositesErvenyessegiIdeje; }
                set { _MinositesErvenyessegiIdeje = value; }                                                        
            }        
                   
           
        private SqlInt32 _TerjedelemMennyiseg = SqlInt32.Null;
           
        /// <summary>
        /// TerjedelemMennyiseg Base property </summary>
            public SqlInt32 TerjedelemMennyiseg
            {
                get { return _TerjedelemMennyiseg; }
                set { _TerjedelemMennyiseg = value; }                                                        
            }        
                   
           
        private SqlString _TerjedelemMennyisegiEgyseg = SqlString.Null;
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg Base property </summary>
            public SqlString TerjedelemMennyisegiEgyseg
            {
                get { return _TerjedelemMennyisegiEgyseg; }
                set { _TerjedelemMennyisegiEgyseg = value; }                                                        
            }        
                   
           
        private SqlString _TerjedelemMegjegyzes = SqlString.Null;
           
        /// <summary>
        /// TerjedelemMegjegyzes Base property </summary>
            public SqlString TerjedelemMegjegyzes
            {
                get { return _TerjedelemMegjegyzes; }
                set { _TerjedelemMegjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _SelejtezesDat = SqlDateTime.Null;
           
        /// <summary>
        /// SelejtezesDat Base property </summary>
            public SqlDateTime SelejtezesDat
            {
                get { return _SelejtezesDat; }
                set { _SelejtezesDat = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhCsoport_Id_Selejtezo = SqlGuid.Null;
           
        /// <summary>
        /// FelhCsoport_Id_Selejtezo Base property </summary>
            public SqlGuid FelhCsoport_Id_Selejtezo
            {
                get { return _FelhCsoport_Id_Selejtezo; }
                set { _FelhCsoport_Id_Selejtezo = value; }                                                        
            }        
                   
           
        private SqlString _LeveltariAtvevoNeve = SqlString.Null;
           
        /// <summary>
        /// LeveltariAtvevoNeve Base property </summary>
            public SqlString LeveltariAtvevoNeve
            {
                get { return _LeveltariAtvevoNeve; }
                set { _LeveltariAtvevoNeve = value; }                                                        
            }        
                   
           
        private SqlDateTime _ModositasErvenyessegKezdete = SqlDateTime.Null;
           
        /// <summary>
        /// ModositasErvenyessegKezdete Base property </summary>
            public SqlDateTime ModositasErvenyessegKezdete
            {
                get { return _ModositasErvenyessegKezdete; }
                set { _ModositasErvenyessegKezdete = value; }                                                        
            }        
                   
           
        private SqlString _MegszuntetoHatarozat = SqlString.Null;
           
        /// <summary>
        /// MegszuntetoHatarozat Base property </summary>
            public SqlString MegszuntetoHatarozat
            {
                get { return _MegszuntetoHatarozat; }
                set { _MegszuntetoHatarozat = value; }                                                        
            }        
                   
           
        private SqlDateTime _FelulvizsgalatDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// FelulvizsgalatDatuma Base property </summary>
            public SqlDateTime FelulvizsgalatDatuma
            {
                get { return _FelulvizsgalatDatuma; }
                set { _FelulvizsgalatDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _Felulvizsgalo_id = SqlGuid.Null;
           
        /// <summary>
        /// Felulvizsgalo_id Base property </summary>
            public SqlGuid Felulvizsgalo_id
            {
                get { return _Felulvizsgalo_id; }
                set { _Felulvizsgalo_id = value; }                                                        
            }        
                   
           
        private SqlString _MinositesFelulvizsgalatEredm = SqlString.Null;
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm Base property </summary>
            public SqlString MinositesFelulvizsgalatEredm
            {
                get { return _MinositesFelulvizsgalatEredm; }
                set { _MinositesFelulvizsgalatEredm = value; }                                                        
            }        
                   
           
        private SqlDateTime _UgyintezesKezdoDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// UgyintezesKezdoDatuma Base property </summary>
            public SqlDateTime UgyintezesKezdoDatuma
            {
                get { return _UgyintezesKezdoDatuma; }
                set { _UgyintezesKezdoDatuma = value; }                                                        
            }        
                   
           
        private SqlString _EljarasiSzakasz = SqlString.Null;
           
        /// <summary>
        /// EljarasiSzakasz Base property </summary>
            public SqlString EljarasiSzakasz
            {
                get { return _EljarasiSzakasz; }
                set { _EljarasiSzakasz = value; }                                                        
            }        
                   
           
        private SqlInt32 _HatralevoNapok = SqlInt32.Null;
           
        /// <summary>
        /// HatralevoNapok Base property </summary>
            public SqlInt32 HatralevoNapok
            {
                get { return _HatralevoNapok; }
                set { _HatralevoNapok = value; }                                                        
            }        
                   
           
        private SqlInt32 _HatralevoMunkaNapok = SqlInt32.Null;
           
        /// <summary>
        /// HatralevoMunkaNapok Base property </summary>
            public SqlInt32 HatralevoMunkaNapok
            {
                get { return _HatralevoMunkaNapok; }
                set { _HatralevoMunkaNapok = value; }                                                        
            }        
                   
           
        private SqlString _Ugy_Fajtaja = SqlString.Null;
           
        /// <summary>
        /// Ugy_Fajtaja Base property </summary>
            public SqlString Ugy_Fajtaja
            {
                get { return _Ugy_Fajtaja; }
                set { _Ugy_Fajtaja = value; }                                                        
            }        
                   
           
        private SqlGuid _MinositoSzervezet = SqlGuid.Null;
           
        /// <summary>
        /// MinositoSzervezet Base property </summary>
            public SqlGuid MinositoSzervezet
            {
                get { return _MinositoSzervezet; }
                set { _MinositoSzervezet = value; }                                                        
            }        
                   
           
        private SqlString _MinositesFelulvizsgalatBizTag = SqlString.Null;
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag Base property </summary>
            public SqlString MinositesFelulvizsgalatBizTag
            {
                get { return _MinositesFelulvizsgalatBizTag; }
                set { _MinositesFelulvizsgalatBizTag = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}