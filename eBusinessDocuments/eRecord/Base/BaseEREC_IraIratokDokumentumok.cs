
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIratokDokumentumok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraIratokDokumentumok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraIratokDokumentumokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Lapszam = true;
               public bool Lapszam
               {
                   get { return _Lapszam; }
                   set { _Lapszam = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _Forras = true;
               public bool Forras
               {
                   get { return _Forras; }
                   set { _Forras = value; }
               }
                                                            
                                 
               private bool _Formatum = true;
               public bool Formatum
               {
                   get { return _Formatum; }
                   set { _Formatum = value; }
               }
                                                            
                                 
               private bool _Vonalkodozas = true;
               public bool Vonalkodozas
               {
                   get { return _Vonalkodozas; }
                   set { _Vonalkodozas = value; }
               }
                                                            
                                 
               private bool _DokumentumSzerep = true;
               public bool DokumentumSzerep
               {
                   get { return _DokumentumSzerep; }
                   set { _DokumentumSzerep = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   Leiras = Value;               
                    
                   Lapszam = Value;               
                    
                   BarCode = Value;               
                    
                   Forras = Value;               
                    
                   Formatum = Value;               
                    
                   Vonalkodozas = Value;               
                    
                   DokumentumSzerep = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraIratokDokumentumokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlInt32 _Lapszam = SqlInt32.Null;
           
        /// <summary>
        /// Lapszam Base property </summary>
            public SqlInt32 Lapszam
            {
                get { return _Lapszam; }
                set { _Lapszam = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlString _Forras = SqlString.Null;
           
        /// <summary>
        /// Forras Base property </summary>
            public SqlString Forras
            {
                get { return _Forras; }
                set { _Forras = value; }                                                        
            }        
                   
           
        private SqlString _Formatum = SqlString.Null;
           
        /// <summary>
        /// Formatum Base property </summary>
            public SqlString Formatum
            {
                get { return _Formatum; }
                set { _Formatum = value; }                                                        
            }        
                   
           
        private SqlChars _Vonalkodozas = SqlChars.Null;
           
        /// <summary>
        /// Vonalkodozas Base property </summary>
            public SqlChars Vonalkodozas
            {
                get { return _Vonalkodozas; }
                set { _Vonalkodozas = value; }                                                        
            }        
                   
           
        private SqlString _DokumentumSzerep = SqlString.Null;
           
        /// <summary>
        /// DokumentumSzerep Base property </summary>
            public SqlString DokumentumSzerep
            {
                get { return _DokumentumSzerep; }
                set { _DokumentumSzerep = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}