
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIrattariTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraIrattariTetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraIrattariTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _IrattariTetelszam = true;
               public bool IrattariTetelszam
               {
                   get { return _IrattariTetelszam; }
                   set { _IrattariTetelszam = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _IrattariJel = true;
               public bool IrattariJel
               {
                   get { return _IrattariJel; }
                   set { _IrattariJel = value; }
               }
                                                            
                                 
               private bool _MegorzesiIdo = true;
               public bool MegorzesiIdo
               {
                   get { return _MegorzesiIdo; }
                   set { _MegorzesiIdo = value; }
               }
                                                            
                                 
               private bool _Idoegyseg = true;
               public bool Idoegyseg
               {
                   get { return _Idoegyseg; }
                   set { _Idoegyseg = value; }
               }
                                                            
                                 
               private bool _Folyo_CM = true;
               public bool Folyo_CM
               {
                   get { return _Folyo_CM; }
                   set { _Folyo_CM = value; }
               }
                                                            
                                 
               private bool _AgazatiJel_Id = true;
               public bool AgazatiJel_Id
               {
                   get { return _AgazatiJel_Id; }
                   set { _AgazatiJel_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _EvenTuliIktatas = true;
               public bool EvenTuliIktatas
               {
                   get { return _EvenTuliIktatas; }
                   set { _EvenTuliIktatas = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   IrattariTetelszam = Value;               
                    
                   Nev = Value;               
                    
                   IrattariJel = Value;               
                    
                   MegorzesiIdo = Value;               
                    
                   Idoegyseg = Value;               
                    
                   Folyo_CM = Value;               
                    
                   AgazatiJel_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   EvenTuliIktatas = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraIrattariTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlString _IrattariTetelszam = SqlString.Null;
           
        /// <summary>
        /// IrattariTetelszam Base property </summary>
            public SqlString IrattariTetelszam
            {
                get { return _IrattariTetelszam; }
                set { _IrattariTetelszam = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _IrattariJel = SqlString.Null;
           
        /// <summary>
        /// IrattariJel Base property </summary>
            public SqlString IrattariJel
            {
                get { return _IrattariJel; }
                set { _IrattariJel = value; }                                                        
            }        
                   
           
        private SqlString _MegorzesiIdo = SqlString.Null;
           
        /// <summary>
        /// MegorzesiIdo Base property </summary>
            public SqlString MegorzesiIdo
            {
                get { return _MegorzesiIdo; }
                set { _MegorzesiIdo = value; }                                                        
            }        
                   
           
        private SqlString _Idoegyseg = SqlString.Null;
           
        /// <summary>
        /// Idoegyseg Base property </summary>
            public SqlString Idoegyseg
            {
                get { return _Idoegyseg; }
                set { _Idoegyseg = value; }                                                        
            }        
                   
           
        private SqlString _Folyo_CM = SqlString.Null;
           
        /// <summary>
        /// Folyo_CM Base property </summary>
            public SqlString Folyo_CM
            {
                get { return _Folyo_CM; }
                set { _Folyo_CM = value; }                                                        
            }        
                   
           
        private SqlGuid _AgazatiJel_Id = SqlGuid.Null;
           
        /// <summary>
        /// AgazatiJel_Id Base property </summary>
            public SqlGuid AgazatiJel_Id
            {
                get { return _AgazatiJel_Id; }
                set { _AgazatiJel_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlChars _EvenTuliIktatas = SqlChars.Null;
           
        /// <summary>
        /// EvenTuliIktatas Base property </summary>
            public SqlChars EvenTuliIktatas
            {
                get { return _EvenTuliIktatas; }
                set { _EvenTuliIktatas = value; }                                                        
            }        
                           }
    }    
}