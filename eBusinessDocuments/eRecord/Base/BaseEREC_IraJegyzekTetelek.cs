
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraJegyzekTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraJegyzekTetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraJegyzekTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Jegyzek_Id = true;
               public bool Jegyzek_Id
               {
                   get { return _Jegyzek_Id; }
                   set { _Jegyzek_Id = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_type = true;
               public bool Obj_type
               {
                   get { return _Obj_type; }
                   set { _Obj_type = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _SztornozasDat = true;
               public bool SztornozasDat
               {
                   get { return _SztornozasDat; }
                   set { _SztornozasDat = value; }
               }
                                                            
                                 
               private bool _AtadasDatuma = true;
               public bool AtadasDatuma
               {
                   get { return _AtadasDatuma; }
                   set { _AtadasDatuma = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Jegyzek_Id = Value;               
                    
                   Sorszam = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_type = Value;               
                    
                   Allapot = Value;               
                    
                   Megjegyzes = Value;               
                    
                   Azonosito = Value;               
                    
                   SztornozasDat = Value;               
                    
                   AtadasDatuma = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraJegyzekTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Jegyzek_Id = SqlGuid.Null;
           
        /// <summary>
        /// Jegyzek_Id Base property </summary>
            public SqlGuid Jegyzek_Id
            {
                get { return _Jegyzek_Id; }
                set { _Jegyzek_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_type = SqlString.Null;
           
        /// <summary>
        /// Obj_type Base property </summary>
            public SqlString Obj_type
            {
                get { return _Obj_type; }
                set { _Obj_type = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornozasDat Base property </summary>
            public SqlDateTime SztornozasDat
            {
                get { return _SztornozasDat; }
                set { _SztornozasDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _AtadasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// AtadasDatuma Base property </summary>
            public SqlDateTime AtadasDatuma
            {
                get { return _AtadasDatuma; }
                set { _AtadasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}