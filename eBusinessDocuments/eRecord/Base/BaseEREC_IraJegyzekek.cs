
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraJegyzekek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraJegyzekek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraJegyzekekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Minosites = true;
               public bool Minosites
               {
                   get { return _Minosites; }
                   set { _Minosites = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_felelos = true;
               public bool Csoport_Id_felelos
               {
                   get { return _Csoport_Id_felelos; }
                   set { _Csoport_Id_felelos = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Vegrehaj = true;
               public bool FelhasznaloCsoport_Id_Vegrehaj
               {
                   get { return _FelhasznaloCsoport_Id_Vegrehaj; }
                   set { _FelhasznaloCsoport_Id_Vegrehaj = value; }
               }
                                                            
                                 
               private bool _Partner_Id_LeveltariAtvevo = true;
               public bool Partner_Id_LeveltariAtvevo
               {
                   get { return _Partner_Id_LeveltariAtvevo; }
                   set { _Partner_Id_LeveltariAtvevo = value; }
               }
                                                            
                                 
               private bool _LezarasDatuma = true;
               public bool LezarasDatuma
               {
                   get { return _LezarasDatuma; }
                   set { _LezarasDatuma = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _VegrehajtasDatuma = true;
               public bool VegrehajtasDatuma
               {
                   get { return _VegrehajtasDatuma; }
                   set { _VegrehajtasDatuma = value; }
               }
                                                            
                                 
               private bool _Foszam = true;
               public bool Foszam
               {
                   get { return _Foszam; }
                   set { _Foszam = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _MegsemmisitesDatuma = true;
               public bool MegsemmisitesDatuma
               {
                   get { return _MegsemmisitesDatuma; }
                   set { _MegsemmisitesDatuma = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_FelelosSzervezet = true;
               public bool Csoport_Id_FelelosSzervezet
               {
                   get { return _Csoport_Id_FelelosSzervezet; }
                   set { _Csoport_Id_FelelosSzervezet = value; }
               }
                                                            
                                 
               private bool _IraIktatokonyv_Id = true;
               public bool IraIktatokonyv_Id
               {
                   get { return _IraIktatokonyv_Id; }
                   set { _IraIktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _VegrehajtasKezdoDatuma = true;
               public bool VegrehajtasKezdoDatuma
               {
                   get { return _VegrehajtasKezdoDatuma; }
                   set { _VegrehajtasKezdoDatuma = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Tipus = Value;               
                    
                   Minosites = Value;               
                    
                   Csoport_Id_felelos = Value;               
                    
                   FelhasznaloCsoport_Id_Vegrehaj = Value;               
                    
                   Partner_Id_LeveltariAtvevo = Value;               
                    
                   LezarasDatuma = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   Nev = Value;               
                    
                   VegrehajtasDatuma = Value;               
                    
                   Foszam = Value;               
                    
                   Azonosito = Value;               
                    
                   MegsemmisitesDatuma = Value;               
                    
                   Csoport_Id_FelelosSzervezet = Value;               
                    
                   IraIktatokonyv_Id = Value;               
                    
                   Allapot = Value;               
                    
                   VegrehajtasKezdoDatuma = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraJegyzekekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Minosites = SqlString.Null;
           
        /// <summary>
        /// Minosites Base property </summary>
            public SqlString Minosites
            {
                get { return _Minosites; }
                set { _Minosites = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_felelos Base property </summary>
            public SqlGuid Csoport_Id_felelos
            {
                get { return _Csoport_Id_felelos; }
                set { _Csoport_Id_felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Vegrehaj = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Vegrehaj
            {
                get { return _FelhasznaloCsoport_Id_Vegrehaj; }
                set { _FelhasznaloCsoport_Id_Vegrehaj = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_LeveltariAtvevo = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_LeveltariAtvevo Base property </summary>
            public SqlGuid Partner_Id_LeveltariAtvevo
            {
                get { return _Partner_Id_LeveltariAtvevo; }
                set { _Partner_Id_LeveltariAtvevo = value; }                                                        
            }        
                   
           
        private SqlDateTime _LezarasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// LezarasDatuma Base property </summary>
            public SqlDateTime LezarasDatuma
            {
                get { return _LezarasDatuma; }
                set { _LezarasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlDateTime _VegrehajtasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VegrehajtasDatuma Base property </summary>
            public SqlDateTime VegrehajtasDatuma
            {
                get { return _VegrehajtasDatuma; }
                set { _VegrehajtasDatuma = value; }                                                        
            }        
                   
           
        private SqlInt32 _Foszam = SqlInt32.Null;
           
        /// <summary>
        /// Foszam Base property </summary>
            public SqlInt32 Foszam
            {
                get { return _Foszam; }
                set { _Foszam = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _MegsemmisitesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// MegsemmisitesDatuma Base property </summary>
            public SqlDateTime MegsemmisitesDatuma
            {
                get { return _MegsemmisitesDatuma; }
                set { _MegsemmisitesDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_FelelosSzervezet = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_FelelosSzervezet Base property </summary>
            public SqlGuid Csoport_Id_FelelosSzervezet
            {
                get { return _Csoport_Id_FelelosSzervezet; }
                set { _Csoport_Id_FelelosSzervezet = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIktatokonyv_Id Base property </summary>
            public SqlGuid IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _VegrehajtasKezdoDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VegrehajtasKezdoDatuma Base property </summary>
            public SqlDateTime VegrehajtasKezdoDatuma
            {
                get { return _VegrehajtasKezdoDatuma; }
                set { _VegrehajtasKezdoDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}