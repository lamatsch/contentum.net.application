
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezFeljegyzesek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraKezFeljegyzesek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezFeljegyzesekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _KezelesTipus = true;
               public bool KezelesTipus
               {
                   get { return _KezelesTipus; }
                   set { _KezelesTipus = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Note = true;
               public bool Note
               {
                   get { return _Note; }
                   set { _Note = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   KezelesTipus = Value;               
                    
                   Leiras = Value;               
                    
                   Note = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezFeljegyzesekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlString _KezelesTipus = SqlString.Null;
           
        /// <summary>
        /// KezelesTipus Base property </summary>
            public SqlString KezelesTipus
            {
                get { return _KezelesTipus; }
                set { _KezelesTipus = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _Note = SqlString.Null;
           
        /// <summary>
        /// Note Base property </summary>
            public SqlString Note
            {
                get { return _Note; }
                set { _Note = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}