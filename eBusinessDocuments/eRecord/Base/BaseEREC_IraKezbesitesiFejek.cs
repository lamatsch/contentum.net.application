
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezbesitesiFejek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraKezbesitesiFejek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezbesitesiFejekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _CsomagAzonosito = true;
               public bool CsomagAzonosito
               {
                   get { return _CsomagAzonosito; }
                   set { _CsomagAzonosito = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _UtolsoNyomtatasIdo = true;
               public bool UtolsoNyomtatasIdo
               {
                   get { return _UtolsoNyomtatasIdo; }
                   set { _UtolsoNyomtatasIdo = value; }
               }
                                                            
                                 
               private bool _UtolsoNyomtatas_Id = true;
               public bool UtolsoNyomtatas_Id
               {
                   get { return _UtolsoNyomtatas_Id; }
                   set { _UtolsoNyomtatas_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   CsomagAzonosito = Value;               
                    
                   Tipus = Value;               
                    
                   UtolsoNyomtatasIdo = Value;               
                    
                   UtolsoNyomtatas_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezbesitesiFejekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _CsomagAzonosito = SqlString.Null;
           
        /// <summary>
        /// CsomagAzonosito Base property </summary>
            public SqlString CsomagAzonosito
            {
                get { return _CsomagAzonosito; }
                set { _CsomagAzonosito = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlDateTime _UtolsoNyomtatasIdo = SqlDateTime.Null;
           
        /// <summary>
        /// UtolsoNyomtatasIdo Base property </summary>
            public SqlDateTime UtolsoNyomtatasIdo
            {
                get { return _UtolsoNyomtatasIdo; }
                set { _UtolsoNyomtatasIdo = value; }                                                        
            }        
                   
           
        private SqlGuid _UtolsoNyomtatas_Id = SqlGuid.Null;
           
        /// <summary>
        /// UtolsoNyomtatas_Id Base property </summary>
            public SqlGuid UtolsoNyomtatas_Id
            {
                get { return _UtolsoNyomtatas_Id; }
                set { _UtolsoNyomtatas_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}