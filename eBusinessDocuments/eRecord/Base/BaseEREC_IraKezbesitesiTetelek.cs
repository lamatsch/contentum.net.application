
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezbesitesiTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraKezbesitesiTetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezbesitesiTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KezbesitesFej_Id = true;
               public bool KezbesitesFej_Id
               {
                   get { return _KezbesitesFej_Id; }
                   set { _KezbesitesFej_Id = value; }
               }
                                                            
                                 
               private bool _AtveteliFej_Id = true;
               public bool AtveteliFej_Id
               {
                   get { return _AtveteliFej_Id; }
                   set { _AtveteliFej_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_type = true;
               public bool Obj_type
               {
                   get { return _Obj_type; }
                   set { _Obj_type = value; }
               }
                                                            
                                 
               private bool _AtadasDat = true;
               public bool AtadasDat
               {
                   get { return _AtadasDat; }
                   set { _AtadasDat = value; }
               }
                                                            
                                 
               private bool _AtvetelDat = true;
               public bool AtvetelDat
               {
                   get { return _AtvetelDat; }
                   set { _AtvetelDat = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _UtolsoNyomtatas_Id = true;
               public bool UtolsoNyomtatas_Id
               {
                   get { return _UtolsoNyomtatas_Id; }
                   set { _UtolsoNyomtatas_Id = value; }
               }
                                                            
                                 
               private bool _UtolsoNyomtatasIdo = true;
               public bool UtolsoNyomtatasIdo
               {
                   get { return _UtolsoNyomtatasIdo; }
                   set { _UtolsoNyomtatasIdo = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_Atado_USER = true;
               public bool Felhasznalo_Id_Atado_USER
               {
                   get { return _Felhasznalo_Id_Atado_USER; }
                   set { _Felhasznalo_Id_Atado_USER = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_Atado_LOGIN = true;
               public bool Felhasznalo_Id_Atado_LOGIN
               {
                   get { return _Felhasznalo_Id_Atado_LOGIN; }
                   set { _Felhasznalo_Id_Atado_LOGIN = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Cel = true;
               public bool Csoport_Id_Cel
               {
                   get { return _Csoport_Id_Cel; }
                   set { _Csoport_Id_Cel = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_AtvevoUser = true;
               public bool Felhasznalo_Id_AtvevoUser
               {
                   get { return _Felhasznalo_Id_AtvevoUser; }
                   set { _Felhasznalo_Id_AtvevoUser = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id_AtvevoLogin = true;
               public bool Felhasznalo_Id_AtvevoLogin
               {
                   get { return _Felhasznalo_Id_AtvevoLogin; }
                   set { _Felhasznalo_Id_AtvevoLogin = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Azonosito_szoveges = true;
               public bool Azonosito_szoveges
               {
                   get { return _Azonosito_szoveges; }
                   set { _Azonosito_szoveges = value; }
               }
                                                            
                                 
               private bool _UgyintezesModja = true;
               public bool UgyintezesModja
               {
                   get { return _UgyintezesModja; }
                   set { _UgyintezesModja = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KezbesitesFej_Id = Value;               
                    
                   AtveteliFej_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Obj_type = Value;               
                    
                   AtadasDat = Value;               
                    
                   AtvetelDat = Value;               
                    
                   Megjegyzes = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   UtolsoNyomtatas_Id = Value;               
                    
                   UtolsoNyomtatasIdo = Value;               
                    
                   Felhasznalo_Id_Atado_USER = Value;               
                    
                   Felhasznalo_Id_Atado_LOGIN = Value;               
                    
                   Csoport_Id_Cel = Value;               
                    
                   Felhasznalo_Id_AtvevoUser = Value;               
                    
                   Felhasznalo_Id_AtvevoLogin = Value;               
                    
                   Allapot = Value;               
                    
                   Azonosito_szoveges = Value;               
                    
                   UgyintezesModja = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraKezbesitesiTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KezbesitesFej_Id = SqlGuid.Null;
           
        /// <summary>
        /// KezbesitesFej_Id Base property </summary>
            public SqlGuid KezbesitesFej_Id
            {
                get { return _KezbesitesFej_Id; }
                set { _KezbesitesFej_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _AtveteliFej_Id = SqlGuid.Null;
           
        /// <summary>
        /// AtveteliFej_Id Base property </summary>
            public SqlGuid AtveteliFej_Id
            {
                get { return _AtveteliFej_Id; }
                set { _AtveteliFej_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Obj_type = SqlString.Null;
           
        /// <summary>
        /// Obj_type Base property </summary>
            public SqlString Obj_type
            {
                get { return _Obj_type; }
                set { _Obj_type = value; }                                                        
            }        
                   
           
        private SqlDateTime _AtadasDat = SqlDateTime.Null;
           
        /// <summary>
        /// AtadasDat Base property </summary>
            public SqlDateTime AtadasDat
            {
                get { return _AtadasDat; }
                set { _AtadasDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _AtvetelDat = SqlDateTime.Null;
           
        /// <summary>
        /// AtvetelDat Base property </summary>
            public SqlDateTime AtvetelDat
            {
                get { return _AtvetelDat; }
                set { _AtvetelDat = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlGuid _UtolsoNyomtatas_Id = SqlGuid.Null;
           
        /// <summary>
        /// UtolsoNyomtatas_Id Base property </summary>
            public SqlGuid UtolsoNyomtatas_Id
            {
                get { return _UtolsoNyomtatas_Id; }
                set { _UtolsoNyomtatas_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _UtolsoNyomtatasIdo = SqlDateTime.Null;
           
        /// <summary>
        /// UtolsoNyomtatasIdo Base property </summary>
            public SqlDateTime UtolsoNyomtatasIdo
            {
                get { return _UtolsoNyomtatasIdo; }
                set { _UtolsoNyomtatasIdo = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_Atado_USER = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_Atado_USER Base property </summary>
            public SqlGuid Felhasznalo_Id_Atado_USER
            {
                get { return _Felhasznalo_Id_Atado_USER; }
                set { _Felhasznalo_Id_Atado_USER = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_Atado_LOGIN = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_Atado_LOGIN Base property </summary>
            public SqlGuid Felhasznalo_Id_Atado_LOGIN
            {
                get { return _Felhasznalo_Id_Atado_LOGIN; }
                set { _Felhasznalo_Id_Atado_LOGIN = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Cel = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Cel Base property </summary>
            public SqlGuid Csoport_Id_Cel
            {
                get { return _Csoport_Id_Cel; }
                set { _Csoport_Id_Cel = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_AtvevoUser = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_AtvevoUser Base property </summary>
            public SqlGuid Felhasznalo_Id_AtvevoUser
            {
                get { return _Felhasznalo_Id_AtvevoUser; }
                set { _Felhasznalo_Id_AtvevoUser = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id_AtvevoLogin = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id_AtvevoLogin Base property </summary>
            public SqlGuid Felhasznalo_Id_AtvevoLogin
            {
                get { return _Felhasznalo_Id_AtvevoLogin; }
                set { _Felhasznalo_Id_AtvevoLogin = value; }                                                        
            }        
                   
           
        private SqlChars _Allapot = SqlChars.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlChars Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito_szoveges = SqlString.Null;
           
        /// <summary>
        /// Azonosito_szoveges Base property </summary>
            public SqlString Azonosito_szoveges
            {
                get { return _Azonosito_szoveges; }
                set { _Azonosito_szoveges = value; }                                                        
            }        
                   
           
        private SqlString _UgyintezesModja = SqlString.Null;
           
        /// <summary>
        /// UgyintezesModja Base property </summary>
            public SqlString UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}