 
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraOnkormAdatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IraOnkormAdatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IraOnkormAdatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _IraIratok_Id = true;
               public bool IraIratok_Id
               {
                   get { return _IraIratok_Id; }
                   set { _IraIratok_Id = value; }
               }
                                                            
                                 
               private bool _UgyFajtaja = true;
               public bool UgyFajtaja
               {
                   get { return _UgyFajtaja; }
                   set { _UgyFajtaja = value; }
               }
                                                            
                                 
               private bool _DontestHozta = true;
               public bool DontestHozta
               {
                   get { return _DontestHozta; }
                   set { _DontestHozta = value; }
               }
                                                            
                                 
               private bool _DontesFormaja = true;
               public bool DontesFormaja
               {
                   get { return _DontesFormaja; }
                   set { _DontesFormaja = value; }
               }
                                                            
                                 
               private bool _UgyintezesHataridore = true;
               public bool UgyintezesHataridore
               {
                   get { return _UgyintezesHataridore; }
                   set { _UgyintezesHataridore = value; }
               }
                                                            
                                 
               private bool _HataridoTullepes = true;
               public bool HataridoTullepes
               {
                   get { return _HataridoTullepes; }
                   set { _HataridoTullepes = value; }
               }
                                                            
                                 
               private bool _JogorvoslatiEljarasTipusa = true;
               public bool JogorvoslatiEljarasTipusa
               {
                   get { return _JogorvoslatiEljarasTipusa; }
                   set { _JogorvoslatiEljarasTipusa = value; }
               }
                                                            
                                 
               private bool _JogorvoslatiDontesTipusa = true;
               public bool JogorvoslatiDontesTipusa
               {
                   get { return _JogorvoslatiDontesTipusa; }
                   set { _JogorvoslatiDontesTipusa = value; }
               }
                                                            
                                 
               private bool _JogorvoslatiDontestHozta = true;
               public bool JogorvoslatiDontestHozta
               {
                   get { return _JogorvoslatiDontestHozta; }
                   set { _JogorvoslatiDontestHozta = value; }
               }
                                                            
                                 
               private bool _JogorvoslatiDontesTartalma = true;
               public bool JogorvoslatiDontesTartalma
               {
                   get { return _JogorvoslatiDontesTartalma; }
                   set { _JogorvoslatiDontesTartalma = value; }
               }
                                                            
                                 
               private bool _JogorvoslatiDontes = true;
               public bool JogorvoslatiDontes
               {
                   get { return _JogorvoslatiDontes; }
                   set { _JogorvoslatiDontes = value; }
               }
                                                            
                                 
               private bool _HatosagiEllenorzes = true;
               public bool HatosagiEllenorzes
               {
                   get { return _HatosagiEllenorzes; }
                   set { _HatosagiEllenorzes = value; }
               }
                                                            
                                 
               private bool _MunkaorakSzama = true;
               public bool MunkaorakSzama
               {
                   get { return _MunkaorakSzama; }
                   set { _MunkaorakSzama = value; }
               }
                                                            
                                 
               private bool _EljarasiKoltseg = true;
               public bool EljarasiKoltseg
               {
                   get { return _EljarasiKoltseg; }
                   set { _EljarasiKoltseg = value; }
               }
                                                            
                                 
               private bool _KozigazgatasiBirsagMerteke = true;
               public bool KozigazgatasiBirsagMerteke
               {
                   get { return _KozigazgatasiBirsagMerteke; }
                   set { _KozigazgatasiBirsagMerteke = value; }
               }
                                                            
                                 
               private bool _SommasEljDontes = true;
               public bool SommasEljDontes
               {
                   get { return _SommasEljDontes; }
                   set { _SommasEljDontes = value; }
               }
                                                            
                                 
               private bool _NyolcNapBelulNemSommas = true;
               public bool NyolcNapBelulNemSommas
               {
                   get { return _NyolcNapBelulNemSommas; }
                   set { _NyolcNapBelulNemSommas = value; }
               }
                                                            
                                 
               private bool _FuggoHatalyuHatarozat = true;
               public bool FuggoHatalyuHatarozat
               {
                   get { return _FuggoHatalyuHatarozat; }
                   set { _FuggoHatalyuHatarozat = value; }
               }
                                                            
                                 
               private bool _FuggoHatHatalybaLepes = true;
               public bool FuggoHatHatalybaLepes
               {
                   get { return _FuggoHatHatalybaLepes; }
                   set { _FuggoHatHatalybaLepes = value; }
               }
                                                            
                                 
               private bool _FuggoHatalyuVegzes = true;
               public bool FuggoHatalyuVegzes
               {
                   get { return _FuggoHatalyuVegzes; }
                   set { _FuggoHatalyuVegzes = value; }
               }
                                                            
                                 
               private bool _FuggoVegzesHatalyba = true;
               public bool FuggoVegzesHatalyba
               {
                   get { return _FuggoVegzesHatalyba; }
                   set { _FuggoVegzesHatalyba = value; }
               }
                                                            
                                 
               private bool _HatAltalVisszafizOsszeg = true;
               public bool HatAltalVisszafizOsszeg
               {
                   get { return _HatAltalVisszafizOsszeg; }
                   set { _HatAltalVisszafizOsszeg = value; }
               }
                                                            
                                 
               private bool _HatTerheloEljKtsg = true;
               public bool HatTerheloEljKtsg
               {
                   get { return _HatTerheloEljKtsg; }
                   set { _HatTerheloEljKtsg = value; }
               }
                                                            
                                 
               private bool _FelfuggHatarozat = true;
               public bool FelfuggHatarozat
               {
                   get { return _FelfuggHatarozat; }
                   set { _FelfuggHatarozat = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   IraIratok_Id = Value;               
                    
                   UgyFajtaja = Value;               
                    
                   DontestHozta = Value;               
                    
                   DontesFormaja = Value;               
                    
                   UgyintezesHataridore = Value;               
                    
                   HataridoTullepes = Value;               
                    
                   JogorvoslatiEljarasTipusa = Value;               
                    
                   JogorvoslatiDontesTipusa = Value;               
                    
                   JogorvoslatiDontestHozta = Value;               
                    
                   JogorvoslatiDontesTartalma = Value;               
                    
                   JogorvoslatiDontes = Value;               
                    
                   HatosagiEllenorzes = Value;               
                    
                   MunkaorakSzama = Value;               
                    
                   EljarasiKoltseg = Value;               
                    
                   KozigazgatasiBirsagMerteke = Value;               
                    
                   SommasEljDontes = Value;               
                    
                   NyolcNapBelulNemSommas = Value;               
                    
                   FuggoHatalyuHatarozat = Value;               
                    
                   FuggoHatHatalybaLepes = Value;               
                    
                   FuggoHatalyuVegzes = Value;               
                    
                   FuggoVegzesHatalyba = Value;               
                    
                   HatAltalVisszafizOsszeg = Value;               
                    
                   HatTerheloEljKtsg = Value;               
                    
                   FelfuggHatarozat = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IraOnkormAdatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIratok_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIratok_Id Base property </summary>
            public SqlGuid IraIratok_Id
            {
                get { return _IraIratok_Id; }
                set { _IraIratok_Id = value; }                                                        
            }        
                   
           
        private SqlString _UgyFajtaja = SqlString.Null;
           
        /// <summary>
        /// UgyFajtaja Base property </summary>
            public SqlString UgyFajtaja
            {
                get { return _UgyFajtaja; }
                set { _UgyFajtaja = value; }                                                        
            }        
                   
           
        private SqlString _DontestHozta = SqlString.Null;
           
        /// <summary>
        /// DontestHozta Base property </summary>
            public SqlString DontestHozta
            {
                get { return _DontestHozta; }
                set { _DontestHozta = value; }                                                        
            }        
                   
           
        private SqlString _DontesFormaja = SqlString.Null;
           
        /// <summary>
        /// DontesFormaja Base property </summary>
            public SqlString DontesFormaja
            {
                get { return _DontesFormaja; }
                set { _DontesFormaja = value; }                                                        
            }        
                   
           
        private SqlString _UgyintezesHataridore = SqlString.Null;
           
        /// <summary>
        /// UgyintezesHataridore Base property </summary>
            public SqlString UgyintezesHataridore
            {
                get { return _UgyintezesHataridore; }
                set { _UgyintezesHataridore = value; }                                                        
            }        
                   
           
        private SqlInt32 _HataridoTullepes = SqlInt32.Null;
           
        /// <summary>
        /// HataridoTullepes Base property </summary>
            public SqlInt32 HataridoTullepes
            {
                get { return _HataridoTullepes; }
                set { _HataridoTullepes = value; }                                                        
            }        
                   
           
        private SqlString _JogorvoslatiEljarasTipusa = SqlString.Null;
           
        /// <summary>
        /// JogorvoslatiEljarasTipusa Base property </summary>
            public SqlString JogorvoslatiEljarasTipusa
            {
                get { return _JogorvoslatiEljarasTipusa; }
                set { _JogorvoslatiEljarasTipusa = value; }                                                        
            }        
                   
           
        private SqlString _JogorvoslatiDontesTipusa = SqlString.Null;
           
        /// <summary>
        /// JogorvoslatiDontesTipusa Base property </summary>
            public SqlString JogorvoslatiDontesTipusa
            {
                get { return _JogorvoslatiDontesTipusa; }
                set { _JogorvoslatiDontesTipusa = value; }                                                        
            }        
                   
           
        private SqlString _JogorvoslatiDontestHozta = SqlString.Null;
           
        /// <summary>
        /// JogorvoslatiDontestHozta Base property </summary>
            public SqlString JogorvoslatiDontestHozta
            {
                get { return _JogorvoslatiDontestHozta; }
                set { _JogorvoslatiDontestHozta = value; }                                                        
            }        
                   
           
        private SqlString _JogorvoslatiDontesTartalma = SqlString.Null;
           
        /// <summary>
        /// JogorvoslatiDontesTartalma Base property </summary>
            public SqlString JogorvoslatiDontesTartalma
            {
                get { return _JogorvoslatiDontesTartalma; }
                set { _JogorvoslatiDontesTartalma = value; }                                                        
            }        
                   
           
        private SqlString _JogorvoslatiDontes = SqlString.Null;
           
        /// <summary>
        /// JogorvoslatiDontes Base property </summary>
            public SqlString JogorvoslatiDontes
            {
                get { return _JogorvoslatiDontes; }
                set { _JogorvoslatiDontes = value; }                                                        
            }        
                   
           
        private SqlChars _HatosagiEllenorzes = SqlChars.Null;
           
        /// <summary>
        /// HatosagiEllenorzes Base property </summary>
            public SqlChars HatosagiEllenorzes
            {
                get { return _HatosagiEllenorzes; }
                set { _HatosagiEllenorzes = value; }                                                        
            }        
                   
           
        private SqlDouble _MunkaorakSzama = SqlDouble.Null;
           
        /// <summary>
        /// MunkaorakSzama Base property </summary>
            public SqlDouble MunkaorakSzama
            {
                get { return _MunkaorakSzama; }
                set { _MunkaorakSzama = value; }                                                        
            }        
                   
           
        private SqlInt32 _EljarasiKoltseg = SqlInt32.Null;
           
        /// <summary>
        /// EljarasiKoltseg Base property </summary>
            public SqlInt32 EljarasiKoltseg
            {
                get { return _EljarasiKoltseg; }
                set { _EljarasiKoltseg = value; }                                                        
            }        
                   
           
        private SqlInt32 _KozigazgatasiBirsagMerteke = SqlInt32.Null;
           
        /// <summary>
        /// KozigazgatasiBirsagMerteke Base property </summary>
            public SqlInt32 KozigazgatasiBirsagMerteke
            {
                get { return _KozigazgatasiBirsagMerteke; }
                set { _KozigazgatasiBirsagMerteke = value; }                                                        
            }        
                   
           
        private SqlString _SommasEljDontes = SqlString.Null;
           
        /// <summary>
        /// SommasEljDontes Base property </summary>
            public SqlString SommasEljDontes
            {
                get { return _SommasEljDontes; }
                set { _SommasEljDontes = value; }                                                        
            }        
                   
           
        private SqlString _NyolcNapBelulNemSommas = SqlString.Null;
           
        /// <summary>
        /// NyolcNapBelulNemSommas Base property </summary>
            public SqlString NyolcNapBelulNemSommas
            {
                get { return _NyolcNapBelulNemSommas; }
                set { _NyolcNapBelulNemSommas = value; }                                                        
            }        
                   
           
        private SqlString _FuggoHatalyuHatarozat = SqlString.Null;
           
        /// <summary>
        /// FuggoHatalyuHatarozat Base property </summary>
            public SqlString FuggoHatalyuHatarozat
            {
                get { return _FuggoHatalyuHatarozat; }
                set { _FuggoHatalyuHatarozat = value; }                                                        
            }        
                   
           
        private SqlString _FuggoHatHatalybaLepes = SqlString.Null;
           
        /// <summary>
        /// FuggoHatHatalybaLepes Base property </summary>
            public SqlString FuggoHatHatalybaLepes
            {
                get { return _FuggoHatHatalybaLepes; }
                set { _FuggoHatHatalybaLepes = value; }                                                        
            }        
                   
           
        private SqlString _FuggoHatalyuVegzes = SqlString.Null;
           
        /// <summary>
        /// FuggoHatalyuVegzes Base property </summary>
            public SqlString FuggoHatalyuVegzes
            {
                get { return _FuggoHatalyuVegzes; }
                set { _FuggoHatalyuVegzes = value; }                                                        
            }        
                   
           
        private SqlString _FuggoVegzesHatalyba = SqlString.Null;
           
        /// <summary>
        /// FuggoVegzesHatalyba Base property </summary>
            public SqlString FuggoVegzesHatalyba
            {
                get { return _FuggoVegzesHatalyba; }
                set { _FuggoVegzesHatalyba = value; }                                                        
            }        
                   
           
        private SqlInt32 _HatAltalVisszafizOsszeg = SqlInt32.Null;
           
        /// <summary>
        /// HatAltalVisszafizOsszeg Base property </summary>
            public SqlInt32 HatAltalVisszafizOsszeg
            {
                get { return _HatAltalVisszafizOsszeg; }
                set { _HatAltalVisszafizOsszeg = value; }                                                        
            }        
                   
           
        private SqlInt32 _HatTerheloEljKtsg = SqlInt32.Null;
           
        /// <summary>
        /// HatTerheloEljKtsg Base property </summary>
            public SqlInt32 HatTerheloEljKtsg
            {
                get { return _HatTerheloEljKtsg; }
                set { _HatTerheloEljKtsg = value; }                                                        
            }        
                   
           
        private SqlString _FelfuggHatarozat = SqlString.Null;
           
        /// <summary>
        /// FelfuggHatarozat Base property </summary>
            public SqlString FelfuggHatarozat
            {
                get { return _FelfuggHatarozat; }
                set { _FelfuggHatarozat = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}