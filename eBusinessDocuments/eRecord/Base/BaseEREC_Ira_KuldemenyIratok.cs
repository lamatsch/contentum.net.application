
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Ira_KuldemenyIratok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Ira_KuldemenyIratok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Ira_KuldemenyIratokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Kuldemeny_Id = true;
               public bool Kuldemeny_Id
               {
                   get { return _Kuldemeny_Id; }
                   set { _Kuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _Irat_Id = true;
               public bool Irat_Id
               {
                   get { return _Irat_Id; }
                   set { _Irat_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Kuldemeny_Id = Value;               
                    
                   Irat_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Ira_KuldemenyIratokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Kuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kuldemeny_Id Base property </summary>
            public SqlGuid Kuldemeny_Id
            {
                get { return _Kuldemeny_Id; }
                set { _Kuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Irat_Id = SqlGuid.Null;
           
        /// <summary>
        /// Irat_Id Base property </summary>
            public SqlGuid Irat_Id
            {
                get { return _Irat_Id; }
                set { _Irat_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}