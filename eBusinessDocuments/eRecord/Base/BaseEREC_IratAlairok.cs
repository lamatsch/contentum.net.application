
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratAlairok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IratAlairok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IratAlairokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _PldIratPeldany_Id = true;
               public bool PldIratPeldany_Id
               {
                   get { return _PldIratPeldany_Id; }
                   set { _PldIratPeldany_Id = value; }
               }
                                                            
                                 
               private bool _Objtip_Id = true;
               public bool Objtip_Id
               {
                   get { return _Objtip_Id; }
                   set { _Objtip_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _AlairasDatuma = true;
               public bool AlairasDatuma
               {
                   get { return _AlairasDatuma; }
                   set { _AlairasDatuma = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _AlairoSzerep = true;
               public bool AlairoSzerep
               {
                   get { return _AlairoSzerep; }
                   set { _AlairoSzerep = value; }
               }
                                                            
                                 
               private bool _AlairasSorrend = true;
               public bool AlairasSorrend
               {
                   get { return _AlairasSorrend; }
                   set { _AlairasSorrend = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Alairo = true;
               public bool FelhasznaloCsoport_Id_Alairo
               {
                   get { return _FelhasznaloCsoport_Id_Alairo; }
                   set { _FelhasznaloCsoport_Id_Alairo = value; }
               }
                                                            
                                 
               private bool _FelhaszCsoport_Id_Helyettesito = true;
               public bool FelhaszCsoport_Id_Helyettesito
               {
                   get { return _FelhaszCsoport_Id_Helyettesito; }
                   set { _FelhaszCsoport_Id_Helyettesito = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _AlairasMod = true;
               public bool AlairasMod
               {
                   get { return _AlairasMod; }
                   set { _AlairasMod = value; }
               }
                                                            
                                 
               private bool _AlairasSzabaly_Id = true;
               public bool AlairasSzabaly_Id
               {
                   get { return _AlairasSzabaly_Id; }
                   set { _AlairasSzabaly_Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _FelhaszCsop_Id_HelyettesAlairo = true;
               public bool FelhaszCsop_Id_HelyettesAlairo
               {
                   get { return _FelhaszCsop_Id_HelyettesAlairo; }
                   set { _FelhaszCsop_Id_HelyettesAlairo = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   PldIratPeldany_Id = Value;               
                    
                   Objtip_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   AlairasDatuma = Value;               
                    
                   Leiras = Value;               
                    
                   AlairoSzerep = Value;               
                    
                   AlairasSorrend = Value;               
                    
                   FelhasznaloCsoport_Id_Alairo = Value;               
                    
                   FelhaszCsoport_Id_Helyettesito = Value;               
                    
                   Azonosito = Value;               
                    
                   AlairasMod = Value;               
                    
                   AlairasSzabaly_Id = Value;               
                    
                   Allapot = Value;               
                    
                   FelhaszCsop_Id_HelyettesAlairo = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IratAlairokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _PldIratPeldany_Id = SqlGuid.Null;
           
        /// <summary>
        /// PldIratPeldany_Id Base property </summary>
            public SqlGuid PldIratPeldany_Id
            {
                get { return _PldIratPeldany_Id; }
                set { _PldIratPeldany_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Objtip_Id = SqlGuid.Null;
           
        /// <summary>
        /// Objtip_Id Base property </summary>
            public SqlGuid Objtip_Id
            {
                get { return _Objtip_Id; }
                set { _Objtip_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _AlairasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// AlairasDatuma Base property </summary>
            public SqlDateTime AlairasDatuma
            {
                get { return _AlairasDatuma; }
                set { _AlairasDatuma = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _AlairoSzerep = SqlString.Null;
           
        /// <summary>
        /// AlairoSzerep Base property </summary>
            public SqlString AlairoSzerep
            {
                get { return _AlairoSzerep; }
                set { _AlairoSzerep = value; }                                                        
            }        
                   
           
        private SqlInt32 _AlairasSorrend = SqlInt32.Null;
           
        /// <summary>
        /// AlairasSorrend Base property </summary>
            public SqlInt32 AlairasSorrend
            {
                get { return _AlairasSorrend; }
                set { _AlairasSorrend = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Alairo = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Alairo
            {
                get { return _FelhasznaloCsoport_Id_Alairo; }
                set { _FelhasznaloCsoport_Id_Alairo = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhaszCsoport_Id_Helyettesito = SqlGuid.Null;
           
        /// <summary>
        /// FelhaszCsoport_Id_Helyettesito Base property </summary>
            public SqlGuid FelhaszCsoport_Id_Helyettesito
            {
                get { return _FelhaszCsoport_Id_Helyettesito; }
                set { _FelhaszCsoport_Id_Helyettesito = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _AlairasMod = SqlString.Null;
           
        /// <summary>
        /// AlairasMod Base property </summary>
            public SqlString AlairasMod
            {
                get { return _AlairasMod; }
                set { _AlairasMod = value; }                                                        
            }        
                   
           
        private SqlGuid _AlairasSzabaly_Id = SqlGuid.Null;
           
        /// <summary>
        /// AlairasSzabaly_Id Base property </summary>
            public SqlGuid AlairasSzabaly_Id
            {
                get { return _AlairasSzabaly_Id; }
                set { _AlairasSzabaly_Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhaszCsop_Id_HelyettesAlairo = SqlGuid.Null;
           
        /// <summary>
        /// FelhaszCsop_Id_HelyettesAlairo Base property </summary>
            public SqlGuid FelhaszCsop_Id_HelyettesAlairo
            {
                get { return _FelhaszCsop_Id_HelyettesAlairo; }
                set { _FelhaszCsop_Id_HelyettesAlairo = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}