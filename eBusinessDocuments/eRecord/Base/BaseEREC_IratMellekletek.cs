
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratMellekletek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IratMellekletek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IratMellekletekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _KuldMellekletek_Id = true;
               public bool KuldMellekletek_Id
               {
                   get { return _KuldMellekletek_Id; }
                   set { _KuldMellekletek_Id = value; }
               }
                                                            
                                 
               private bool _AdathordozoTipus = true;
               public bool AdathordozoTipus
               {
                   get { return _AdathordozoTipus; }
                   set { _AdathordozoTipus = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _MennyisegiEgyseg = true;
               public bool MennyisegiEgyseg
               {
                   get { return _MennyisegiEgyseg; }
                   set { _MennyisegiEgyseg = value; }
               }
                                                            
                                 
               private bool _Mennyiseg = true;
               public bool Mennyiseg
               {
                   get { return _Mennyiseg; }
                   set { _Mennyiseg = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   KuldMellekletek_Id = Value;               
                    
                   AdathordozoTipus = Value;               
                    
                   Megjegyzes = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   MennyisegiEgyseg = Value;               
                    
                   Mennyiseg = Value;               
                    
                   BarCode = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IratMellekletekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldMellekletek_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldMellekletek_Id Base property </summary>
            public SqlGuid KuldMellekletek_Id
            {
                get { return _KuldMellekletek_Id; }
                set { _KuldMellekletek_Id = value; }                                                        
            }        
                   
           
        private SqlString _AdathordozoTipus = SqlString.Null;
           
        /// <summary>
        /// AdathordozoTipus Base property </summary>
            public SqlString AdathordozoTipus
            {
                get { return _AdathordozoTipus; }
                set { _AdathordozoTipus = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlString _MennyisegiEgyseg = SqlString.Null;
           
        /// <summary>
        /// MennyisegiEgyseg Base property </summary>
            public SqlString MennyisegiEgyseg
            {
                get { return _MennyisegiEgyseg; }
                set { _MennyisegiEgyseg = value; }                                                        
            }        
                   
           
        private SqlInt32 _Mennyiseg = SqlInt32.Null;
           
        /// <summary>
        /// Mennyiseg Base property </summary>
            public SqlInt32 Mennyiseg
            {
                get { return _Mennyiseg; }
                set { _Mennyiseg = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}