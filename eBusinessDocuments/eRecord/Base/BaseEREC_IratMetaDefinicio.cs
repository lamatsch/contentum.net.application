
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratMetaDefinicio BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IratMetaDefinicio
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IratMetaDefinicioBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Ugykor_Id = true;
               public bool Ugykor_Id
               {
                   get { return _Ugykor_Id; }
                   set { _Ugykor_Id = value; }
               }
                                                            
                                 
               private bool _UgykorKod = true;
               public bool UgykorKod
               {
                   get { return _UgykorKod; }
                   set { _UgykorKod = value; }
               }
                                                            
                                 
               private bool _Ugytipus = true;
               public bool Ugytipus
               {
                   get { return _Ugytipus; }
                   set { _Ugytipus = value; }
               }
                                                            
                                 
               private bool _UgytipusNev = true;
               public bool UgytipusNev
               {
                   get { return _UgytipusNev; }
                   set { _UgytipusNev = value; }
               }
                                                            
                                 
               private bool _EljarasiSzakasz = true;
               public bool EljarasiSzakasz
               {
                   get { return _EljarasiSzakasz; }
                   set { _EljarasiSzakasz = value; }
               }
                                                            
                                 
               private bool _Irattipus = true;
               public bool Irattipus
               {
                   get { return _Irattipus; }
                   set { _Irattipus = value; }
               }
                                                            
                                 
               private bool _GeneraltTargy = true;
               public bool GeneraltTargy
               {
                   get { return _GeneraltTargy; }
                   set { _GeneraltTargy = value; }
               }
                                                            
                                 
               private bool _Rovidnev = true;
               public bool Rovidnev
               {
                   get { return _Rovidnev; }
                   set { _Rovidnev = value; }
               }
                                                            
                                 
               private bool _UgyFajta = true;
               public bool UgyFajta
               {
                   get { return _UgyFajta; }
                   set { _UgyFajta = value; }
               }
                                                            
                                 
               private bool _UgyiratIntezesiIdo = true;
               public bool UgyiratIntezesiIdo
               {
                   get { return _UgyiratIntezesiIdo; }
                   set { _UgyiratIntezesiIdo = value; }
               }
                                                            
                                 
               private bool _Idoegyseg = true;
               public bool Idoegyseg
               {
                   get { return _Idoegyseg; }
                   set { _Idoegyseg = value; }
               }
                                                            
                                 
               private bool _UgyiratIntezesiIdoKotott = true;
               public bool UgyiratIntezesiIdoKotott
               {
                   get { return _UgyiratIntezesiIdoKotott; }
                   set { _UgyiratIntezesiIdoKotott = value; }
               }
                                                            
                                 
               private bool _UgyiratHataridoKitolas = true;
               public bool UgyiratHataridoKitolas
               {
                   get { return _UgyiratHataridoKitolas; }
                   set { _UgyiratHataridoKitolas = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Ugykor_Id = Value;               
                    
                   UgykorKod = Value;               
                    
                   Ugytipus = Value;               
                    
                   UgytipusNev = Value;               
                    
                   EljarasiSzakasz = Value;               
                    
                   Irattipus = Value;               
                    
                   GeneraltTargy = Value;               
                    
                   Rovidnev = Value;               
                    
                   UgyFajta = Value;               
                    
                   UgyiratIntezesiIdo = Value;               
                    
                   Idoegyseg = Value;               
                    
                   UgyiratIntezesiIdoKotott = Value;               
                    
                   UgyiratHataridoKitolas = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IratMetaDefinicioBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Ugykor_Id = SqlGuid.Null;
           
        /// <summary>
        /// Ugykor_Id Base property </summary>
            public SqlGuid Ugykor_Id
            {
                get { return _Ugykor_Id; }
                set { _Ugykor_Id = value; }                                                        
            }        
                   
           
        private SqlString _UgykorKod = SqlString.Null;
           
        /// <summary>
        /// UgykorKod Base property </summary>
            public SqlString UgykorKod
            {
                get { return _UgykorKod; }
                set { _UgykorKod = value; }                                                        
            }        
                   
           
        private SqlString _Ugytipus = SqlString.Null;
           
        /// <summary>
        /// Ugytipus Base property </summary>
            public SqlString Ugytipus
            {
                get { return _Ugytipus; }
                set { _Ugytipus = value; }                                                        
            }        
                   
           
        private SqlString _UgytipusNev = SqlString.Null;
           
        /// <summary>
        /// UgytipusNev Base property </summary>
            public SqlString UgytipusNev
            {
                get { return _UgytipusNev; }
                set { _UgytipusNev = value; }                                                        
            }        
                   
           
        private SqlString _EljarasiSzakasz = SqlString.Null;
           
        /// <summary>
        /// EljarasiSzakasz Base property </summary>
            public SqlString EljarasiSzakasz
            {
                get { return _EljarasiSzakasz; }
                set { _EljarasiSzakasz = value; }                                                        
            }        
                   
           
        private SqlString _Irattipus = SqlString.Null;
           
        /// <summary>
        /// Irattipus Base property </summary>
            public SqlString Irattipus
            {
                get { return _Irattipus; }
                set { _Irattipus = value; }                                                        
            }        
                   
           
        private SqlString _GeneraltTargy = SqlString.Null;
           
        /// <summary>
        /// GeneraltTargy Base property </summary>
            public SqlString GeneraltTargy
            {
                get { return _GeneraltTargy; }
                set { _GeneraltTargy = value; }                                                        
            }        
                   
           
        private SqlString _Rovidnev = SqlString.Null;
           
        /// <summary>
        /// Rovidnev Base property </summary>
            public SqlString Rovidnev
            {
                get { return _Rovidnev; }
                set { _Rovidnev = value; }                                                        
            }        
                   
           
        private SqlString _UgyFajta = SqlString.Null;
           
        /// <summary>
        /// UgyFajta Base property </summary>
            public SqlString UgyFajta
            {
                get { return _UgyFajta; }
                set { _UgyFajta = value; }                                                        
            }        
                   
           
        private SqlInt32 _UgyiratIntezesiIdo = SqlInt32.Null;
           
        /// <summary>
        /// UgyiratIntezesiIdo Base property </summary>
            public SqlInt32 UgyiratIntezesiIdo
            {
                get { return _UgyiratIntezesiIdo; }
                set { _UgyiratIntezesiIdo = value; }                                                        
            }        
                   
           
        private SqlString _Idoegyseg = SqlString.Null;
           
        /// <summary>
        /// Idoegyseg Base property </summary>
            public SqlString Idoegyseg
            {
                get { return _Idoegyseg; }
                set { _Idoegyseg = value; }                                                        
            }        
                   
           
        private SqlChars _UgyiratIntezesiIdoKotott = SqlChars.Null;
           
        /// <summary>
        /// UgyiratIntezesiIdoKotott Base property </summary>
            public SqlChars UgyiratIntezesiIdoKotott
            {
                get { return _UgyiratIntezesiIdoKotott; }
                set { _UgyiratIntezesiIdoKotott = value; }                                                        
            }        
                   
           
        private SqlChars _UgyiratHataridoKitolas = SqlChars.Null;
           
        /// <summary>
        /// UgyiratHataridoKitolas Base property </summary>
            public SqlChars UgyiratHataridoKitolas
            {
                get { return _UgyiratHataridoKitolas; }
                set { _UgyiratHataridoKitolas = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}