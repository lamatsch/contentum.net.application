
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Irat_Iktatokonyvei BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Irat_Iktatokonyvei
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Irat_IktatokonyveiBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _IraIktatokonyv_Id = true;
               public bool IraIktatokonyv_Id
               {
                   get { return _IraIktatokonyv_Id; }
                   set { _IraIktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Iktathat = true;
               public bool Csoport_Id_Iktathat
               {
                   get { return _Csoport_Id_Iktathat; }
                   set { _Csoport_Id_Iktathat = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   IraIktatokonyv_Id = Value;               
                    
                   Csoport_Id_Iktathat = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Irat_IktatokonyveiBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIktatokonyv_Id Base property </summary>
            public SqlGuid IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Iktathat = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Iktathat Base property </summary>
            public SqlGuid Csoport_Id_Iktathat
            {
                get { return _Csoport_Id_Iktathat; }
                set { _Csoport_Id_Iktathat = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}