
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratelemKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IratelemKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IratelemKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Melleklet_Id = true;
               public bool Melleklet_Id
               {
                   get { return _Melleklet_Id; }
                   set { _Melleklet_Id = value; }
               }
                                                            
                                 
               private bool _Csatolmany_Id = true;
               public bool Csatolmany_Id
               {
                   get { return _Csatolmany_Id; }
                   set { _Csatolmany_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Melleklet_Id = Value;               
                    
                   Csatolmany_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IratelemKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Melleklet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Melleklet_Id Base property </summary>
            public SqlGuid Melleklet_Id
            {
                get { return _Melleklet_Id; }
                set { _Melleklet_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csatolmany_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csatolmany_Id Base property </summary>
            public SqlGuid Csatolmany_Id
            {
                get { return _Csatolmany_Id; }
                set { _Csatolmany_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}