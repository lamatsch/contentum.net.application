
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_IrattariHelyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IrattariHelyek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IrattariHelyekBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Ertek = true;
            public bool Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }
            }
            ////// CR3246 Irattári Helyek kezelésének módosítása
            //   private bool _IrattarTipus = true;
            //   public bool IrattarTipus
            //   {
            //       get { return _IrattarTipus; }
            //       set { _IrattarTipus = value; }
            //   }

            // CR3246 Irattári Helyek kezelésének módosítása
            private bool _Felelos_Csoport_Id = true;
            public bool Felelos_Csoport_Id
            {
                get { return _Felelos_Csoport_Id; }
                set { _Felelos_Csoport_Id = value; }
            }

            private bool _Nev = true;
            public bool Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private bool _Vonalkod = true;
            public bool Vonalkod
            {
                get { return _Vonalkod; }
                set { _Vonalkod = value; }
            }


            private bool _SzuloId = true;
            public bool SzuloId
            {
                get { return _SzuloId; }
                set { _SzuloId = value; }
            }

            private bool _Kapacitas = true;
            public bool Kapacitas
            {
                get { return _Kapacitas; }
                set { _Kapacitas = value; }
            }

            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Ertek = Value;

                Nev = Value;

                Vonalkod = Value;

                SzuloId = Value;

                Kapacitas = Value;

                ErvKezd = Value;

                ErvVege = Value;

                // CR3246 Irattári Helyek kezelésének módosítása
                //IrattarTipus = Value;

                // CR3246 Irattári Helyek kezelésének módosítása
                Felelos_Csoport_Id = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseEREC_IrattariHelyekBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlString _Ertek = SqlString.Null;

            /// <summary>
            /// Ertek Base property </summary>
            public SqlString Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }
            }

            // CR3246 Irattári Helyek kezelésének módosítása
            //    private SqlString _IrattarTipus = SqlString.Null;

            //    /// <summary>
            //    /// Ertek Base property </summary>
            //public SqlString IrattarTipus
            //    {
            //        get { return _IrattarTipus; }
            //        set { _IrattarTipus = value; }
            //    }

            // CR3246 Irattári Helyek kezelésének módosítása
            private SqlGuid _Felelos_Csoport_Id = SqlGuid.Null;

            /// <summary>
            /// Felelos_Csoport_Id Base property </summary>
            public SqlGuid Felelos_Csoport_Id
            {
                get { return _Felelos_Csoport_Id; }
                set { _Felelos_Csoport_Id = value; }
            }


            private SqlString _Nev = SqlString.Null;

            /// <summary>
            /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }


            private SqlString _Vonalkod = SqlString.Null;

            /// <summary>
            /// Vonalkod Base property </summary>
            public SqlString Vonalkod
            {
                get { return _Vonalkod; }
                set { _Vonalkod = value; }
            }


            private SqlGuid _SzuloId = SqlGuid.Null;

            /// <summary>
            /// SzuloId Base property </summary>
            public SqlGuid SzuloId
            {
                get { return _SzuloId; }
                set { _SzuloId = value; }
            }

            private SqlDouble _Kapacitas = SqlDouble.Null;

            /// <summary>
            /// EllenorzoOsszeg Base property </summary>
            public SqlDouble Kapacitas
            {
                get { return _Kapacitas; }
                set { _Kapacitas = value; }
            }

            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }
        }
    }
}