
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IrattariKikero BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IrattariKikero
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IrattariKikeroBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Keres_note = true;
               public bool Keres_note
               {
                   get { return _Keres_note; }
                   set { _Keres_note = value; }
               }
                                                            
                                 
               private bool _FelhasznalasiCel = true;
               public bool FelhasznalasiCel
               {
                   get { return _FelhasznalasiCel; }
                   set { _FelhasznalasiCel = value; }
               }
                                                            
                                 
               private bool _DokumentumTipus = true;
               public bool DokumentumTipus
               {
                   get { return _DokumentumTipus; }
                   set { _DokumentumTipus = value; }
               }
                                                            
                                 
               private bool _Indoklas_note = true;
               public bool Indoklas_note
               {
                   get { return _Indoklas_note; }
                   set { _Indoklas_note = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Kikero = true;
               public bool FelhasznaloCsoport_Id_Kikero
               {
                   get { return _FelhasznaloCsoport_Id_Kikero; }
                   set { _FelhasznaloCsoport_Id_Kikero = value; }
               }
                                                            
                                 
               private bool _KeresDatuma = true;
               public bool KeresDatuma
               {
                   get { return _KeresDatuma; }
                   set { _KeresDatuma = value; }
               }
                                                            
                                 
               private bool _KulsoAzonositok = true;
               public bool KulsoAzonositok
               {
                   get { return _KulsoAzonositok; }
                   set { _KulsoAzonositok = value; }
               }
                                                            
                                 
               private bool _UgyUgyirat_Id = true;
               public bool UgyUgyirat_Id
               {
                   get { return _UgyUgyirat_Id; }
                   set { _UgyUgyirat_Id = value; }
               }
                                                            
                                 
               private bool _Tertiveveny_Id = true;
               public bool Tertiveveny_Id
               {
                   get { return _Tertiveveny_Id; }
                   set { _Tertiveveny_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _KikerKezd = true;
               public bool KikerKezd
               {
                   get { return _KikerKezd; }
                   set { _KikerKezd = value; }
               }
                                                            
                                 
               private bool _KikerVege = true;
               public bool KikerVege
               {
                   get { return _KikerVege; }
                   set { _KikerVege = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Jovahagy = true;
               public bool FelhasznaloCsoport_Id_Jovahagy
               {
                   get { return _FelhasznaloCsoport_Id_Jovahagy; }
                   set { _FelhasznaloCsoport_Id_Jovahagy = value; }
               }
                                                            
                                 
               private bool _JovagyasDatuma = true;
               public bool JovagyasDatuma
               {
                   get { return _JovagyasDatuma; }
                   set { _JovagyasDatuma = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Kiado = true;
               public bool FelhasznaloCsoport_Id_Kiado
               {
                   get { return _FelhasznaloCsoport_Id_Kiado; }
                   set { _FelhasznaloCsoport_Id_Kiado = value; }
               }
                                                            
                                 
               private bool _KiadasDatuma = true;
               public bool KiadasDatuma
               {
                   get { return _KiadasDatuma; }
                   set { _KiadasDatuma = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Visszaad = true;
               public bool FelhasznaloCsoport_Id_Visszaad
               {
                   get { return _FelhasznaloCsoport_Id_Visszaad; }
                   set { _FelhasznaloCsoport_Id_Visszaad = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Visszave = true;
               public bool FelhasznaloCsoport_Id_Visszave
               {
                   get { return _FelhasznaloCsoport_Id_Visszave; }
                   set { _FelhasznaloCsoport_Id_Visszave = value; }
               }
                                                            
                                 
               private bool _VisszaadasDatuma = true;
               public bool VisszaadasDatuma
               {
                   get { return _VisszaadasDatuma; }
                   set { _VisszaadasDatuma = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _Irattar_Id = true;
               public bool Irattar_Id
               {
                   get { return _Irattar_Id; }
                   set { _Irattar_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Keres_note = Value;               
                    
                   FelhasznalasiCel = Value;               
                    
                   DokumentumTipus = Value;               
                    
                   Indoklas_note = Value;               
                    
                   FelhasznaloCsoport_Id_Kikero = Value;               
                    
                   KeresDatuma = Value;               
                    
                   KulsoAzonositok = Value;               
                    
                   UgyUgyirat_Id = Value;               
                    
                   Tertiveveny_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   KikerKezd = Value;               
                    
                   KikerVege = Value;               
                    
                   FelhasznaloCsoport_Id_Jovahagy = Value;               
                    
                   JovagyasDatuma = Value;               
                    
                   FelhasznaloCsoport_Id_Kiado = Value;               
                    
                   KiadasDatuma = Value;               
                    
                   FelhasznaloCsoport_Id_Visszaad = Value;               
                    
                   FelhasznaloCsoport_Id_Visszave = Value;               
                    
                   VisszaadasDatuma = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   Allapot = Value;               
                    
                   BarCode = Value;               
                    
                   Irattar_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IrattariKikeroBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Keres_note = SqlString.Null;
           
        /// <summary>
        /// Keres_note Base property </summary>
            public SqlString Keres_note
            {
                get { return _Keres_note; }
                set { _Keres_note = value; }                                                        
            }        
                   
           
        private SqlChars _FelhasznalasiCel = SqlChars.Null;
           
        /// <summary>
        /// FelhasznalasiCel Base property </summary>
            public SqlChars FelhasznalasiCel
            {
                get { return _FelhasznalasiCel; }
                set { _FelhasznalasiCel = value; }                                                        
            }        
                   
           
        private SqlChars _DokumentumTipus = SqlChars.Null;
           
        /// <summary>
        /// DokumentumTipus Base property </summary>
            public SqlChars DokumentumTipus
            {
                get { return _DokumentumTipus; }
                set { _DokumentumTipus = value; }                                                        
            }        
                   
           
        private SqlString _Indoklas_note = SqlString.Null;
           
        /// <summary>
        /// Indoklas_note Base property </summary>
            public SqlString Indoklas_note
            {
                get { return _Indoklas_note; }
                set { _Indoklas_note = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Kikero = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kikero Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Kikero
            {
                get { return _FelhasznaloCsoport_Id_Kikero; }
                set { _FelhasznaloCsoport_Id_Kikero = value; }                                                        
            }        
                   
           
        private SqlDateTime _KeresDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// KeresDatuma Base property </summary>
            public SqlDateTime KeresDatuma
            {
                get { return _KeresDatuma; }
                set { _KeresDatuma = value; }                                                        
            }        
                   
           
        private SqlString _KulsoAzonositok = SqlString.Null;
           
        /// <summary>
        /// KulsoAzonositok Base property </summary>
            public SqlString KulsoAzonositok
            {
                get { return _KulsoAzonositok; }
                set { _KulsoAzonositok = value; }                                                        
            }        
                   
           
        private SqlGuid _UgyUgyirat_Id = SqlGuid.Null;
           
        /// <summary>
        /// UgyUgyirat_Id Base property </summary>
            public SqlGuid UgyUgyirat_Id
            {
                get { return _UgyUgyirat_Id; }
                set { _UgyUgyirat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Tertiveveny_Id = SqlGuid.Null;
           
        /// <summary>
        /// Tertiveveny_Id Base property </summary>
            public SqlGuid Tertiveveny_Id
            {
                get { return _Tertiveveny_Id; }
                set { _Tertiveveny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _KikerKezd = SqlDateTime.Null;
           
        /// <summary>
        /// KikerKezd Base property </summary>
            public SqlDateTime KikerKezd
            {
                get { return _KikerKezd; }
                set { _KikerKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _KikerVege = SqlDateTime.Null;
           
        /// <summary>
        /// KikerVege Base property </summary>
            public SqlDateTime KikerVege
            {
                get { return _KikerVege; }
                set { _KikerVege = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Jovahagy = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Jovahagy Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Jovahagy
            {
                get { return _FelhasznaloCsoport_Id_Jovahagy; }
                set { _FelhasznaloCsoport_Id_Jovahagy = value; }                                                        
            }        
                   
           
        private SqlDateTime _JovagyasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// JovagyasDatuma Base property </summary>
            public SqlDateTime JovagyasDatuma
            {
                get { return _JovagyasDatuma; }
                set { _JovagyasDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Kiado = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kiado Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Kiado
            {
                get { return _FelhasznaloCsoport_Id_Kiado; }
                set { _FelhasznaloCsoport_Id_Kiado = value; }                                                        
            }        
                   
           
        private SqlDateTime _KiadasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// KiadasDatuma Base property </summary>
            public SqlDateTime KiadasDatuma
            {
                get { return _KiadasDatuma; }
                set { _KiadasDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Visszaad = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Visszaad Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Visszaad
            {
                get { return _FelhasznaloCsoport_Id_Visszaad; }
                set { _FelhasznaloCsoport_Id_Visszaad = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Visszave = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Visszave Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Visszave
            {
                get { return _FelhasznaloCsoport_Id_Visszave; }
                set { _FelhasznaloCsoport_Id_Visszave = value; }                                                        
            }        
                   
           
        private SqlDateTime _VisszaadasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VisszaadasDatuma Base property </summary>
            public SqlDateTime VisszaadasDatuma
            {
                get { return _VisszaadasDatuma; }
                set { _VisszaadasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlGuid _Irattar_Id = SqlGuid.Null;
           
        /// <summary>
        /// Irattar_Id Base property </summary>
            public SqlGuid Irattar_Id
            {
                get { return _Irattar_Id; }
                set { _Irattar_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}