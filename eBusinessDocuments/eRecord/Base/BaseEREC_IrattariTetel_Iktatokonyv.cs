
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IrattariTetel_Iktatokonyv BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_IrattariTetel_Iktatokonyv
    {
        [System.Xml.Serialization.XmlType("BaseEREC_IrattariTetel_IktatokonyvBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _IrattariTetel_Id = true;
               public bool IrattariTetel_Id
               {
                   get { return _IrattariTetel_Id; }
                   set { _IrattariTetel_Id = value; }
               }
                                                            
                                 
               private bool _Iktatokonyv_Id = true;
               public bool Iktatokonyv_Id
               {
                   get { return _Iktatokonyv_Id; }
                   set { _Iktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   IrattariTetel_Id = Value;               
                    
                   Iktatokonyv_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_IrattariTetel_IktatokonyvBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IrattariTetel_Id = SqlGuid.Null;
           
        /// <summary>
        /// IrattariTetel_Id Base property </summary>
            public SqlGuid IrattariTetel_Id
            {
                get { return _IrattariTetel_Id; }
                set { _IrattariTetel_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Iktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// Iktatokonyv_Id Base property </summary>
            public SqlGuid Iktatokonyv_Id
            {
                get { return _Iktatokonyv_Id; }
                set { _Iktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}