
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldBekuldok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_KuldBekuldok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_KuldBekuldokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _Partner_Id_Bekuldo = true;
               public bool Partner_Id_Bekuldo
               {
                   get { return _Partner_Id_Bekuldo; }
                   set { _Partner_Id_Bekuldo = value; }
               }
                                                            
                                 
               private bool _PartnerCim_Id_Bekuldo = true;
               public bool PartnerCim_Id_Bekuldo
               {
                   get { return _PartnerCim_Id_Bekuldo; }
                   set { _PartnerCim_Id_Bekuldo = value; }
               }
                                                            
                                 
               private bool _NevSTR = true;
               public bool NevSTR
               {
                   get { return _NevSTR; }
                   set { _NevSTR = value; }
               }
                                                            
                                 
               private bool _CimSTR = true;
               public bool CimSTR
               {
                   get { return _CimSTR; }
                   set { _CimSTR = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   Partner_Id_Bekuldo = Value;               
                    
                   PartnerCim_Id_Bekuldo = Value;               
                    
                   NevSTR = Value;               
                    
                   CimSTR = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_KuldBekuldokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_Bekuldo = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_Bekuldo Base property </summary>
            public SqlGuid Partner_Id_Bekuldo
            {
                get { return _Partner_Id_Bekuldo; }
                set { _Partner_Id_Bekuldo = value; }                                                        
            }        
                   
           
        private SqlGuid _PartnerCim_Id_Bekuldo = SqlGuid.Null;
           
        /// <summary>
        /// PartnerCim_Id_Bekuldo Base property </summary>
            public SqlGuid PartnerCim_Id_Bekuldo
            {
                get { return _PartnerCim_Id_Bekuldo; }
                set { _PartnerCim_Id_Bekuldo = value; }                                                        
            }        
                   
           
        private SqlString _NevSTR = SqlString.Null;
           
        /// <summary>
        /// NevSTR Base property </summary>
            public SqlString NevSTR
            {
                get { return _NevSTR; }
                set { _NevSTR = value; }                                                        
            }        
                   
           
        private SqlString _CimSTR = SqlString.Null;
           
        /// <summary>
        /// CimSTR Base property </summary>
            public SqlString CimSTR
            {
                get { return _CimSTR; }
                set { _CimSTR = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}