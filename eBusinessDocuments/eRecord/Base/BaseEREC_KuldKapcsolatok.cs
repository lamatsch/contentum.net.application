
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_KuldKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_KuldKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KapcsolatTipus = true;
               public bool KapcsolatTipus
               {
                   get { return _KapcsolatTipus; }
                   set { _KapcsolatTipus = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Kezi = true;
               public bool Kezi
               {
                   get { return _Kezi; }
                   set { _Kezi = value; }
               }
                                                            
                                 
               private bool _Kuld_Kuld_Beepul = true;
               public bool Kuld_Kuld_Beepul
               {
                   get { return _Kuld_Kuld_Beepul; }
                   set { _Kuld_Kuld_Beepul = value; }
               }
                                                            
                                 
               private bool _Kuld_Kuld_Felepul = true;
               public bool Kuld_Kuld_Felepul
               {
                   get { return _Kuld_Kuld_Felepul; }
                   set { _Kuld_Kuld_Felepul = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KapcsolatTipus = Value;               
                    
                   Leiras = Value;               
                    
                   Kezi = Value;               
                    
                   Kuld_Kuld_Beepul = Value;               
                    
                   Kuld_Kuld_Felepul = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_KuldKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _KapcsolatTipus = SqlString.Null;
           
        /// <summary>
        /// KapcsolatTipus Base property </summary>
            public SqlString KapcsolatTipus
            {
                get { return _KapcsolatTipus; }
                set { _KapcsolatTipus = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlChars _Kezi = SqlChars.Null;
           
        /// <summary>
        /// Kezi Base property </summary>
            public SqlChars Kezi
            {
                get { return _Kezi; }
                set { _Kezi = value; }                                                        
            }        
                   
           
        private SqlGuid _Kuld_Kuld_Beepul = SqlGuid.Null;
           
        /// <summary>
        /// Kuld_Kuld_Beepul Base property </summary>
            public SqlGuid Kuld_Kuld_Beepul
            {
                get { return _Kuld_Kuld_Beepul; }
                set { _Kuld_Kuld_Beepul = value; }                                                        
            }        
                   
           
        private SqlGuid _Kuld_Kuld_Felepul = SqlGuid.Null;
           
        /// <summary>
        /// Kuld_Kuld_Felepul Base property </summary>
            public SqlGuid Kuld_Kuld_Felepul
            {
                get { return _Kuld_Kuld_Felepul; }
                set { _Kuld_Kuld_Felepul = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}