
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldKuldemenyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_KuldKuldemenyek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_KuldKuldemenyekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _BeerkezesIdeje = true;
               public bool BeerkezesIdeje
               {
                   get { return _BeerkezesIdeje; }
                   set { _BeerkezesIdeje = value; }
               }
                                                            
                                 
               private bool _FelbontasDatuma = true;
               public bool FelbontasDatuma
               {
                   get { return _FelbontasDatuma; }
                   set { _FelbontasDatuma = value; }
               }
                                                            
                                 
               private bool _IraIktatokonyv_Id = true;
               public bool IraIktatokonyv_Id
               {
                   get { return _IraIktatokonyv_Id; }
                   set { _IraIktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _KuldesMod = true;
               public bool KuldesMod
               {
                   get { return _KuldesMod; }
                   set { _KuldesMod = value; }
               }
                                                            
                                 
               private bool _Erkezteto_Szam = true;
               public bool Erkezteto_Szam
               {
                   get { return _Erkezteto_Szam; }
                   set { _Erkezteto_Szam = value; }
               }
                                                            
                                 
               private bool _HivatkozasiSzam = true;
               public bool HivatkozasiSzam
               {
                   get { return _HivatkozasiSzam; }
                   set { _HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _Targy = true;
               public bool Targy
               {
                   get { return _Targy; }
                   set { _Targy = value; }
               }
                                                            
                                 
               private bool _Tartalom = true;
               public bool Tartalom
               {
                   get { return _Tartalom; }
                   set { _Tartalom = value; }
               }
                                                            
                                 
               private bool _RagSzam = true;
               public bool RagSzam
               {
                   get { return _RagSzam; }
                   set { _RagSzam = value; }
               }
                                                            
                                 
               private bool _Surgosseg = true;
               public bool Surgosseg
               {
                   get { return _Surgosseg; }
                   set { _Surgosseg = value; }
               }
                                                            
                                 
               private bool _BelyegzoDatuma = true;
               public bool BelyegzoDatuma
               {
                   get { return _BelyegzoDatuma; }
                   set { _BelyegzoDatuma = value; }
               }
                                                            
                                 
               private bool _UgyintezesModja = true;
               public bool UgyintezesModja
               {
                   get { return _UgyintezesModja; }
                   set { _UgyintezesModja = value; }
               }
                                                            
                                 
               private bool _PostazasIranya = true;
               public bool PostazasIranya
               {
                   get { return _PostazasIranya; }
                   set { _PostazasIranya = value; }
               }
                                                            
                                 
               private bool _Tovabbito = true;
               public bool Tovabbito
               {
                   get { return _Tovabbito; }
                   set { _Tovabbito = value; }
               }
                                                            
                                 
               private bool _PeldanySzam = true;
               public bool PeldanySzam
               {
                   get { return _PeldanySzam; }
                   set { _PeldanySzam = value; }
               }
                                                            
                                 
               private bool _IktatniKell = true;
               public bool IktatniKell
               {
                   get { return _IktatniKell; }
                   set { _IktatniKell = value; }
               }
                                                            
                                 
               private bool _Iktathato = true;
               public bool Iktathato
               {
                   get { return _Iktathato; }
                   set { _Iktathato = value; }
               }
                                                            
                                 
               private bool _SztornirozasDat = true;
               public bool SztornirozasDat
               {
                   get { return _SztornirozasDat; }
                   set { _SztornirozasDat = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id_Szulo = true;
               public bool KuldKuldemeny_Id_Szulo
               {
                   get { return _KuldKuldemeny_Id_Szulo; }
                   set { _KuldKuldemeny_Id_Szulo = value; }
               }
                                                            
                                 
               private bool _Erkeztetes_Ev = true;
               public bool Erkeztetes_Ev
               {
                   get { return _Erkeztetes_Ev; }
                   set { _Erkeztetes_Ev = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Cimzett = true;
               public bool Csoport_Id_Cimzett
               {
                   get { return _Csoport_Id_Cimzett; }
                   set { _Csoport_Id_Cimzett = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Expedial = true;
               public bool FelhasznaloCsoport_Id_Expedial
               {
                   get { return _FelhasznaloCsoport_Id_Expedial; }
                   set { _FelhasznaloCsoport_Id_Expedial = value; }
               }
                                                            
                                 
               private bool _ExpedialasIdeje = true;
               public bool ExpedialasIdeje
               {
                   get { return _ExpedialasIdeje; }
                   set { _ExpedialasIdeje = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Orzo = true;
               public bool FelhasznaloCsoport_Id_Orzo
               {
                   get { return _FelhasznaloCsoport_Id_Orzo; }
                   set { _FelhasznaloCsoport_Id_Orzo = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Atvevo = true;
               public bool FelhasznaloCsoport_Id_Atvevo
               {
                   get { return _FelhasznaloCsoport_Id_Atvevo; }
                   set { _FelhasznaloCsoport_Id_Atvevo = value; }
               }
                                                            
                                 
               private bool _Partner_Id_Bekuldo = true;
               public bool Partner_Id_Bekuldo
               {
                   get { return _Partner_Id_Bekuldo; }
                   set { _Partner_Id_Bekuldo = value; }
               }
                                                            
                                 
               private bool _Cim_Id = true;
               public bool Cim_Id
               {
                   get { return _Cim_Id; }
                   set { _Cim_Id = value; }
               }
                                                            
                                 
               private bool _CimSTR_Bekuldo = true;
               public bool CimSTR_Bekuldo
               {
                   get { return _CimSTR_Bekuldo; }
                   set { _CimSTR_Bekuldo = value; }
               }
                                                            
                                 
               private bool _NevSTR_Bekuldo = true;
               public bool NevSTR_Bekuldo
               {
                   get { return _NevSTR_Bekuldo; }
                   set { _NevSTR_Bekuldo = value; }
               }
                                                            
                                 
               private bool _AdathordozoTipusa = true;
               public bool AdathordozoTipusa
               {
                   get { return _AdathordozoTipusa; }
                   set { _AdathordozoTipusa = value; }
               }
                                                            
                                 
               private bool _ElsodlegesAdathordozoTipusa = true;
               public bool ElsodlegesAdathordozoTipusa
               {
                   get { return _ElsodlegesAdathordozoTipusa; }
                   set { _ElsodlegesAdathordozoTipusa = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Alairo = true;
               public bool FelhasznaloCsoport_Id_Alairo
               {
                   get { return _FelhasznaloCsoport_Id_Alairo; }
                   set { _FelhasznaloCsoport_Id_Alairo = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Bonto = true;
               public bool FelhasznaloCsoport_Id_Bonto
               {
                   get { return _FelhasznaloCsoport_Id_Bonto; }
                   set { _FelhasznaloCsoport_Id_Bonto = value; }
               }
                                                            
                                 
               private bool _CsoportFelelosEloszto_Id = true;
               public bool CsoportFelelosEloszto_Id
               {
                   get { return _CsoportFelelosEloszto_Id; }
                   set { _CsoportFelelosEloszto_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos_Elozo = true;
               public bool Csoport_Id_Felelos_Elozo
               {
                   get { return _Csoport_Id_Felelos_Elozo; }
                   set { _Csoport_Id_Felelos_Elozo = value; }
               }
                                                            
                                 
               private bool _Kovetkezo_Felelos_Id = true;
               public bool Kovetkezo_Felelos_Id
               {
                   get { return _Kovetkezo_Felelos_Id; }
                   set { _Kovetkezo_Felelos_Id = value; }
               }
                                                            
                                 
               private bool _Elektronikus_Kezbesitesi_Allap = true;
               public bool Elektronikus_Kezbesitesi_Allap
               {
                   get { return _Elektronikus_Kezbesitesi_Allap; }
                   set { _Elektronikus_Kezbesitesi_Allap = value; }
               }
                                                            
                                 
               private bool _Kovetkezo_Orzo_Id = true;
               public bool Kovetkezo_Orzo_Id
               {
                   get { return _Kovetkezo_Orzo_Id; }
                   set { _Kovetkezo_Orzo_Id = value; }
               }
                                                            
                                 
               private bool _Fizikai_Kezbesitesi_Allapot = true;
               public bool Fizikai_Kezbesitesi_Allapot
               {
                   get { return _Fizikai_Kezbesitesi_Allapot; }
                   set { _Fizikai_Kezbesitesi_Allapot = value; }
               }
                                                            
                                 
               private bool _IraIratok_Id = true;
               public bool IraIratok_Id
               {
                   get { return _IraIratok_Id; }
                   set { _IraIratok_Id = value; }
               }
                                                            
                                 
               private bool _BontasiMegjegyzes = true;
               public bool BontasiMegjegyzes
               {
                   get { return _BontasiMegjegyzes; }
                   set { _BontasiMegjegyzes = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Minosites = true;
               public bool Minosites
               {
                   get { return _Minosites; }
                   set { _Minosites = value; }
               }
                                                            
                                 
               private bool _MegtagadasIndoka = true;
               public bool MegtagadasIndoka
               {
                   get { return _MegtagadasIndoka; }
                   set { _MegtagadasIndoka = value; }
               }
                                                            
                                 
               private bool _Megtagado_Id = true;
               public bool Megtagado_Id
               {
                   get { return _Megtagado_Id; }
                   set { _Megtagado_Id = value; }
               }
                                                            
                                 
               private bool _MegtagadasDat = true;
               public bool MegtagadasDat
               {
                   get { return _MegtagadasDat; }
                   set { _MegtagadasDat = value; }
               }
                                                            
                                 
               private bool _KimenoKuldemenyFajta = true;
               public bool KimenoKuldemenyFajta
               {
                   get { return _KimenoKuldemenyFajta; }
                   set { _KimenoKuldemenyFajta = value; }
               }
                                                            
                                 
               private bool _Elsobbsegi = true;
               public bool Elsobbsegi
               {
                   get { return _Elsobbsegi; }
                   set { _Elsobbsegi = value; }
               }
                                                            
                                 
               private bool _Ajanlott = true;
               public bool Ajanlott
               {
                   get { return _Ajanlott; }
                   set { _Ajanlott = value; }
               }
                                                            
                                 
               private bool _Tertiveveny = true;
               public bool Tertiveveny
               {
                   get { return _Tertiveveny; }
                   set { _Tertiveveny = value; }
               }
                                                            
                                 
               private bool _SajatKezbe = true;
               public bool SajatKezbe
               {
                   get { return _SajatKezbe; }
                   set { _SajatKezbe = value; }
               }
                                                            
                                 
               private bool _E_ertesites = true;
               public bool E_ertesites
               {
                   get { return _E_ertesites; }
                   set { _E_ertesites = value; }
               }
                                                            
                                 
               private bool _E_elorejelzes = true;
               public bool E_elorejelzes
               {
                   get { return _E_elorejelzes; }
                   set { _E_elorejelzes = value; }
               }
                                                            
                                 
               private bool _PostaiLezaroSzolgalat = true;
               public bool PostaiLezaroSzolgalat
               {
                   get { return _PostaiLezaroSzolgalat; }
                   set { _PostaiLezaroSzolgalat = value; }
               }
                                                            
                                 
               private bool _Ar = true;
               public bool Ar
               {
                   get { return _Ar; }
                   set { _Ar = value; }
               }
                                                            
                                 
               private bool _KimenoKuld_Sorszam  = true;
               public bool KimenoKuld_Sorszam 
               {
                   get { return _KimenoKuld_Sorszam ; }
                   set { _KimenoKuld_Sorszam  = value; }
               }
                                                            
                                 
               private bool _IrattarId = true;
               public bool IrattarId
               {
                   get { return _IrattarId; }
                   set { _IrattarId = value; }
               }
                                                            
                                 
               private bool _IrattariHely = true;
               public bool IrattariHely
               {
                   get { return _IrattariHely; }
                   set { _IrattariHely = value; }
               }
                                                            
                                 
               private bool _TovabbitasAlattAllapot = true;
               public bool TovabbitasAlattAllapot
               {
                   get { return _TovabbitasAlattAllapot; }
                   set { _TovabbitasAlattAllapot = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _BoritoTipus = true;
               public bool BoritoTipus
               {
                   get { return _BoritoTipus; }
                   set { _BoritoTipus = value; }
               }
                                                            
                                 
               private bool _MegorzesJelzo = true;
               public bool MegorzesJelzo
               {
                   get { return _MegorzesJelzo; }
                   set { _MegorzesJelzo = value; }
               }
                                                            
                                 
               private bool _KulsoAzonosito = true;
               public bool KulsoAzonosito
               {
                   get { return _KulsoAzonosito; }
                   set { _KulsoAzonosito = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _IktatastNemIgenyel = true;
               public bool IktatastNemIgenyel
               {
                   get { return _IktatastNemIgenyel; }
                   set { _IktatastNemIgenyel = value; }
               }
                                                            
                                 
               private bool _KezbesitesModja = true;
               public bool KezbesitesModja
               {
                   get { return _KezbesitesModja; }
                   set { _KezbesitesModja = value; }
               }
                                                            
                                 
               private bool _Munkaallomas = true;
               public bool Munkaallomas
               {
                   get { return _Munkaallomas; }
                   set { _Munkaallomas = value; }
               }
                                                            
                                 
               private bool _SerultKuldemeny = true;
               public bool SerultKuldemeny
               {
                   get { return _SerultKuldemeny; }
                   set { _SerultKuldemeny = value; }
               }
                                                            
                                 
               private bool _TevesCimzes = true;
               public bool TevesCimzes
               {
                   get { return _TevesCimzes; }
                   set { _TevesCimzes = value; }
               }
                                                            
                                 
               private bool _TevesErkeztetes = true;
               public bool TevesErkeztetes
               {
                   get { return _TevesErkeztetes; }
                   set { _TevesErkeztetes = value; }
               }
                                                            
                                 
               private bool _CimzesTipusa = true;
               public bool CimzesTipusa
               {
                   get { return _CimzesTipusa; }
                   set { _CimzesTipusa = value; }
               }
                                                            
                                 
               private bool _FutarJegyzekListaSzama = true;
               public bool FutarJegyzekListaSzama
               {
                   get { return _FutarJegyzekListaSzama; }
                   set { _FutarJegyzekListaSzama = value; }
               }
                                                            
                                 
               private bool _KezbesitoAtvetelIdopontja = true;
               public bool KezbesitoAtvetelIdopontja
               {
                   get { return _KezbesitoAtvetelIdopontja; }
                   set { _KezbesitoAtvetelIdopontja = value; }
               }
                                                            
                                 
               private bool _KezbesitoAtvevoNeve = true;
               public bool KezbesitoAtvevoNeve
               {
                   get { return _KezbesitoAtvevoNeve; }
                   set { _KezbesitoAtvevoNeve = value; }
               }
                                                            
                                 
               private bool _Minosito = true;
               public bool Minosito
               {
                   get { return _Minosito; }
                   set { _Minosito = value; }
               }
                                                            
                                 
               private bool _MinositesErvenyessegiIdeje = true;
               public bool MinositesErvenyessegiIdeje
               {
                   get { return _MinositesErvenyessegiIdeje; }
                   set { _MinositesErvenyessegiIdeje = value; }
               }
                                                            
                                 
               private bool _TerjedelemMennyiseg = true;
               public bool TerjedelemMennyiseg
               {
                   get { return _TerjedelemMennyiseg; }
                   set { _TerjedelemMennyiseg = value; }
               }
                                                            
                                 
               private bool _TerjedelemMennyisegiEgyseg = true;
               public bool TerjedelemMennyisegiEgyseg
               {
                   get { return _TerjedelemMennyisegiEgyseg; }
                   set { _TerjedelemMennyisegiEgyseg = value; }
               }
                                                            
                                 
               private bool _TerjedelemMegjegyzes = true;
               public bool TerjedelemMegjegyzes
               {
                   get { return _TerjedelemMegjegyzes; }
                   set { _TerjedelemMegjegyzes = value; }
               }
                                                            
                                 
               private bool _ModositasErvenyessegKezdete = true;
               public bool ModositasErvenyessegKezdete
               {
                   get { return _ModositasErvenyessegKezdete; }
                   set { _ModositasErvenyessegKezdete = value; }
               }
                                                            
                                 
               private bool _MegszuntetoHatarozat = true;
               public bool MegszuntetoHatarozat
               {
                   get { return _MegszuntetoHatarozat; }
                   set { _MegszuntetoHatarozat = value; }
               }
                                                            
                                 
               private bool _FelulvizsgalatDatuma = true;
               public bool FelulvizsgalatDatuma
               {
                   get { return _FelulvizsgalatDatuma; }
                   set { _FelulvizsgalatDatuma = value; }
               }
                                                            
                                 
               private bool _Felulvizsgalo_id = true;
               public bool Felulvizsgalo_id
               {
                   get { return _Felulvizsgalo_id; }
                   set { _Felulvizsgalo_id = value; }
               }
                                                            
                                 
               private bool _MinositesFelulvizsgalatEredm = true;
               public bool MinositesFelulvizsgalatEredm
               {
                   get { return _MinositesFelulvizsgalatEredm; }
                   set { _MinositesFelulvizsgalatEredm = value; }
               }
                                                            
                                 
               private bool _MinositoSzervezet = true;
               public bool MinositoSzervezet
               {
                   get { return _MinositoSzervezet; }
                   set { _MinositoSzervezet = value; }
               }
                                                            
                                 
               private bool _Partner_Id_BekuldoKapcsolt = true;
               public bool Partner_Id_BekuldoKapcsolt
               {
                   get { return _Partner_Id_BekuldoKapcsolt; }
                   set { _Partner_Id_BekuldoKapcsolt = value; }
               }
                                                            
                                 
               private bool _MinositesFelulvizsgalatBizTag = true;
               public bool MinositesFelulvizsgalatBizTag
               {
                   get { return _MinositesFelulvizsgalatBizTag; }
                   set { _MinositesFelulvizsgalatBizTag = value; }
               }
                                                            
                                 
               private bool _Erteknyilvanitas = true;
               public bool Erteknyilvanitas
               {
                   get { return _Erteknyilvanitas; }
                   set { _Erteknyilvanitas = value; }
               }
                                                            
                                 
               private bool _ErteknyilvanitasOsszege = true;
               public bool ErteknyilvanitasOsszege
               {
                   get { return _ErteknyilvanitasOsszege; }
                   set { _ErteknyilvanitasOsszege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Allapot = Value;               
                    
                   BeerkezesIdeje = Value;               
                    
                   FelbontasDatuma = Value;               
                    
                   IraIktatokonyv_Id = Value;               
                    
                   KuldesMod = Value;               
                    
                   Erkezteto_Szam = Value;               
                    
                   HivatkozasiSzam = Value;               
                    
                   Targy = Value;               
                    
                   Tartalom = Value;               
                    
                   RagSzam = Value;               
                    
                   Surgosseg = Value;               
                    
                   BelyegzoDatuma = Value;               
                    
                   UgyintezesModja = Value;               
                    
                   PostazasIranya = Value;               
                    
                   Tovabbito = Value;               
                    
                   PeldanySzam = Value;               
                    
                   IktatniKell = Value;               
                    
                   Iktathato = Value;               
                    
                   SztornirozasDat = Value;               
                    
                   KuldKuldemeny_Id_Szulo = Value;               
                    
                   Erkeztetes_Ev = Value;               
                    
                   Csoport_Id_Cimzett = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   FelhasznaloCsoport_Id_Expedial = Value;               
                    
                   ExpedialasIdeje = Value;               
                    
                   FelhasznaloCsoport_Id_Orzo = Value;               
                    
                   FelhasznaloCsoport_Id_Atvevo = Value;               
                    
                   Partner_Id_Bekuldo = Value;               
                    
                   Cim_Id = Value;               
                    
                   CimSTR_Bekuldo = Value;               
                    
                   NevSTR_Bekuldo = Value;               
                    
                   AdathordozoTipusa = Value;               
                    
                   ElsodlegesAdathordozoTipusa = Value;               
                    
                   FelhasznaloCsoport_Id_Alairo = Value;               
                    
                   BarCode = Value;               
                    
                   FelhasznaloCsoport_Id_Bonto = Value;               
                    
                   CsoportFelelosEloszto_Id = Value;               
                    
                   Csoport_Id_Felelos_Elozo = Value;               
                    
                   Kovetkezo_Felelos_Id = Value;               
                    
                   Elektronikus_Kezbesitesi_Allap = Value;               
                    
                   Kovetkezo_Orzo_Id = Value;               
                    
                   Fizikai_Kezbesitesi_Allapot = Value;               
                    
                   IraIratok_Id = Value;               
                    
                   BontasiMegjegyzes = Value;               
                    
                   Tipus = Value;               
                    
                   Minosites = Value;               
                    
                   MegtagadasIndoka = Value;               
                    
                   Megtagado_Id = Value;               
                    
                   MegtagadasDat = Value;               
                    
                   KimenoKuldemenyFajta = Value;               
                    
                   Elsobbsegi = Value;               
                    
                   Ajanlott = Value;               
                    
                   Tertiveveny = Value;               
                    
                   SajatKezbe = Value;               
                    
                   E_ertesites = Value;               
                    
                   E_elorejelzes = Value;               
                    
                   PostaiLezaroSzolgalat = Value;               
                    
                   Ar = Value;               
                    
                   KimenoKuld_Sorszam  = Value;               
                    
                   IrattarId = Value;               
                    
                   IrattariHely = Value;               
                    
                   TovabbitasAlattAllapot = Value;               
                    
                   Azonosito = Value;               
                    
                   BoritoTipus = Value;               
                    
                   MegorzesJelzo = Value;               
                    
                   KulsoAzonosito = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   IktatastNemIgenyel = Value;               
                    
                   KezbesitesModja = Value;               
                    
                   Munkaallomas = Value;               
                    
                   SerultKuldemeny = Value;               
                    
                   TevesCimzes = Value;               
                    
                   TevesErkeztetes = Value;               
                    
                   CimzesTipusa = Value;               
                    
                   FutarJegyzekListaSzama = Value;               
                    
                   KezbesitoAtvetelIdopontja = Value;               
                    
                   KezbesitoAtvevoNeve = Value;               
                    
                   Minosito = Value;               
                    
                   MinositesErvenyessegiIdeje = Value;               
                    
                   TerjedelemMennyiseg = Value;               
                    
                   TerjedelemMennyisegiEgyseg = Value;               
                    
                   TerjedelemMegjegyzes = Value;               
                    
                   ModositasErvenyessegKezdete = Value;               
                    
                   MegszuntetoHatarozat = Value;               
                    
                   FelulvizsgalatDatuma = Value;               
                    
                   Felulvizsgalo_id = Value;               
                    
                   MinositesFelulvizsgalatEredm = Value;               
                    
                   MinositoSzervezet = Value;               
                    
                   Partner_Id_BekuldoKapcsolt = Value;               
                    
                   MinositesFelulvizsgalatBizTag = Value;               
                    
                   Erteknyilvanitas = Value;               
                    
                   ErteknyilvanitasOsszege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_KuldKuldemenyekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _BeerkezesIdeje = SqlDateTime.Null;
           
        /// <summary>
        /// BeerkezesIdeje Base property </summary>
            public SqlDateTime BeerkezesIdeje
            {
                get { return _BeerkezesIdeje; }
                set { _BeerkezesIdeje = value; }                                                        
            }        
                   
           
        private SqlDateTime _FelbontasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// FelbontasDatuma Base property </summary>
            public SqlDateTime FelbontasDatuma
            {
                get { return _FelbontasDatuma; }
                set { _FelbontasDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIktatokonyv_Id Base property </summary>
            public SqlGuid IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlString _KuldesMod = SqlString.Null;
           
        /// <summary>
        /// KuldesMod Base property </summary>
            public SqlString KuldesMod
            {
                get { return _KuldesMod; }
                set { _KuldesMod = value; }                                                        
            }        
                   
           
        private SqlInt32 _Erkezteto_Szam = SqlInt32.Null;
           
        /// <summary>
        /// Erkezteto_Szam Base property </summary>
            public SqlInt32 Erkezteto_Szam
            {
                get { return _Erkezteto_Szam; }
                set { _Erkezteto_Szam = value; }                                                        
            }        
                   
           
        private SqlString _HivatkozasiSzam = SqlString.Null;
           
        /// <summary>
        /// HivatkozasiSzam Base property </summary>
            public SqlString HivatkozasiSzam
            {
                get { return _HivatkozasiSzam; }
                set { _HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlString _Targy = SqlString.Null;
           
        /// <summary>
        /// Targy Base property </summary>
            public SqlString Targy
            {
                get { return _Targy; }
                set { _Targy = value; }                                                        
            }        
                   
           
        private SqlString _Tartalom = SqlString.Null;
           
        /// <summary>
        /// Tartalom Base property </summary>
            public SqlString Tartalom
            {
                get { return _Tartalom; }
                set { _Tartalom = value; }                                                        
            }        
                   
           
        private SqlString _RagSzam = SqlString.Null;
           
        /// <summary>
        /// RagSzam Base property </summary>
            public SqlString RagSzam
            {
                get { return _RagSzam; }
                set { _RagSzam = value; }                                                        
            }        
                   
           
        private SqlString _Surgosseg = SqlString.Null;
           
        /// <summary>
        /// Surgosseg Base property </summary>
            public SqlString Surgosseg
            {
                get { return _Surgosseg; }
                set { _Surgosseg = value; }                                                        
            }        
                   
           
        private SqlDateTime _BelyegzoDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// BelyegzoDatuma Base property </summary>
            public SqlDateTime BelyegzoDatuma
            {
                get { return _BelyegzoDatuma; }
                set { _BelyegzoDatuma = value; }                                                        
            }        
                   
           
        private SqlString _UgyintezesModja = SqlString.Null;
           
        /// <summary>
        /// UgyintezesModja Base property </summary>
            public SqlString UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }                                                        
            }        
                   
           
        private SqlString _PostazasIranya = SqlString.Null;
           
        /// <summary>
        /// PostazasIranya Base property </summary>
            public SqlString PostazasIranya
            {
                get { return _PostazasIranya; }
                set { _PostazasIranya = value; }                                                        
            }        
                   
           
        private SqlString _Tovabbito = SqlString.Null;
           
        /// <summary>
        /// Tovabbito Base property </summary>
            public SqlString Tovabbito
            {
                get { return _Tovabbito; }
                set { _Tovabbito = value; }                                                        
            }        
                   
           
        private SqlInt32 _PeldanySzam = SqlInt32.Null;
           
        /// <summary>
        /// PeldanySzam Base property </summary>
            public SqlInt32 PeldanySzam
            {
                get { return _PeldanySzam; }
                set { _PeldanySzam = value; }                                                        
            }        
                   
           
        private SqlString _IktatniKell = SqlString.Null;
           
        /// <summary>
        /// IktatniKell Base property </summary>
            public SqlString IktatniKell
            {
                get { return _IktatniKell; }
                set { _IktatniKell = value; }                                                        
            }        
                   
           
        private SqlString _Iktathato = SqlString.Null;
           
        /// <summary>
        /// Iktathato Base property </summary>
            public SqlString Iktathato
            {
                get { return _Iktathato; }
                set { _Iktathato = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornirozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id_Szulo = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id_Szulo Base property </summary>
            public SqlGuid KuldKuldemeny_Id_Szulo
            {
                get { return _KuldKuldemeny_Id_Szulo; }
                set { _KuldKuldemeny_Id_Szulo = value; }                                                        
            }        
                   
           
        private SqlInt32 _Erkeztetes_Ev = SqlInt32.Null;
           
        /// <summary>
        /// Erkeztetes_Ev Base property </summary>
            public SqlInt32 Erkeztetes_Ev
            {
                get { return _Erkeztetes_Ev; }
                set { _Erkeztetes_Ev = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Cimzett = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Cimzett Base property </summary>
            public SqlGuid Csoport_Id_Cimzett
            {
                get { return _Csoport_Id_Cimzett; }
                set { _Csoport_Id_Cimzett = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Expedial = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Expedial
            {
                get { return _FelhasznaloCsoport_Id_Expedial; }
                set { _FelhasznaloCsoport_Id_Expedial = value; }                                                        
            }        
                   
           
        private SqlDateTime _ExpedialasIdeje = SqlDateTime.Null;
           
        /// <summary>
        /// ExpedialasIdeje Base property </summary>
            public SqlDateTime ExpedialasIdeje
            {
                get { return _ExpedialasIdeje; }
                set { _ExpedialasIdeje = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Orzo = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Atvevo = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Atvevo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Atvevo
            {
                get { return _FelhasznaloCsoport_Id_Atvevo; }
                set { _FelhasznaloCsoport_Id_Atvevo = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_Bekuldo = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_Bekuldo Base property </summary>
            public SqlGuid Partner_Id_Bekuldo
            {
                get { return _Partner_Id_Bekuldo; }
                set { _Partner_Id_Bekuldo = value; }                                                        
            }        
                   
           
        private SqlGuid _Cim_Id = SqlGuid.Null;
           
        /// <summary>
        /// Cim_Id Base property </summary>
            public SqlGuid Cim_Id
            {
                get { return _Cim_Id; }
                set { _Cim_Id = value; }                                                        
            }        
                   
           
        private SqlString _CimSTR_Bekuldo = SqlString.Null;
           
        /// <summary>
        /// CimSTR_Bekuldo Base property </summary>
            public SqlString CimSTR_Bekuldo
            {
                get { return _CimSTR_Bekuldo; }
                set { _CimSTR_Bekuldo = value; }                                                        
            }        
                   
           
        private SqlString _NevSTR_Bekuldo = SqlString.Null;
           
        /// <summary>
        /// NevSTR_Bekuldo Base property </summary>
            public SqlString NevSTR_Bekuldo
            {
                get { return _NevSTR_Bekuldo; }
                set { _NevSTR_Bekuldo = value; }                                                        
            }        
                   
           
        private SqlString _AdathordozoTipusa = SqlString.Null;
           
        /// <summary>
        /// AdathordozoTipusa Base property </summary>
            public SqlString AdathordozoTipusa
            {
                get { return _AdathordozoTipusa; }
                set { _AdathordozoTipusa = value; }                                                        
            }        
                   
           
        private SqlString _ElsodlegesAdathordozoTipusa = SqlString.Null;
           
        /// <summary>
        /// ElsodlegesAdathordozoTipusa Base property </summary>
            public SqlString ElsodlegesAdathordozoTipusa
            {
                get { return _ElsodlegesAdathordozoTipusa; }
                set { _ElsodlegesAdathordozoTipusa = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Alairo = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Alairo
            {
                get { return _FelhasznaloCsoport_Id_Alairo; }
                set { _FelhasznaloCsoport_Id_Alairo = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Bonto = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Bonto Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Bonto
            {
                get { return _FelhasznaloCsoport_Id_Bonto; }
                set { _FelhasznaloCsoport_Id_Bonto = value; }                                                        
            }        
                   
           
        private SqlGuid _CsoportFelelosEloszto_Id = SqlGuid.Null;
           
        /// <summary>
        /// CsoportFelelosEloszto_Id Base property </summary>
            public SqlGuid CsoportFelelosEloszto_Id
            {
                get { return _CsoportFelelosEloszto_Id; }
                set { _CsoportFelelosEloszto_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos_Elozo = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo Base property </summary>
            public SqlGuid Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }                                                        
            }        
                   
           
        private SqlGuid _Kovetkezo_Felelos_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kovetkezo_Felelos_Id Base property </summary>
            public SqlGuid Kovetkezo_Felelos_Id
            {
                get { return _Kovetkezo_Felelos_Id; }
                set { _Kovetkezo_Felelos_Id = value; }                                                        
            }        
                   
           
        private SqlString _Elektronikus_Kezbesitesi_Allap = SqlString.Null;
           
        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap Base property </summary>
            public SqlString Elektronikus_Kezbesitesi_Allap
            {
                get { return _Elektronikus_Kezbesitesi_Allap; }
                set { _Elektronikus_Kezbesitesi_Allap = value; }                                                        
            }        
                   
           
        private SqlGuid _Kovetkezo_Orzo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kovetkezo_Orzo_Id Base property </summary>
            public SqlGuid Kovetkezo_Orzo_Id
            {
                get { return _Kovetkezo_Orzo_Id; }
                set { _Kovetkezo_Orzo_Id = value; }                                                        
            }        
                   
           
        private SqlString _Fizikai_Kezbesitesi_Allapot = SqlString.Null;
           
        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot Base property </summary>
            public SqlString Fizikai_Kezbesitesi_Allapot
            {
                get { return _Fizikai_Kezbesitesi_Allapot; }
                set { _Fizikai_Kezbesitesi_Allapot = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIratok_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIratok_Id Base property </summary>
            public SqlGuid IraIratok_Id
            {
                get { return _IraIratok_Id; }
                set { _IraIratok_Id = value; }                                                        
            }        
                   
           
        private SqlString _BontasiMegjegyzes = SqlString.Null;
           
        /// <summary>
        /// BontasiMegjegyzes Base property </summary>
            public SqlString BontasiMegjegyzes
            {
                get { return _BontasiMegjegyzes; }
                set { _BontasiMegjegyzes = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Minosites = SqlString.Null;
           
        /// <summary>
        /// Minosites Base property </summary>
            public SqlString Minosites
            {
                get { return _Minosites; }
                set { _Minosites = value; }                                                        
            }        
                   
           
        private SqlString _MegtagadasIndoka = SqlString.Null;
           
        /// <summary>
        /// MegtagadasIndoka Base property </summary>
            public SqlString MegtagadasIndoka
            {
                get { return _MegtagadasIndoka; }
                set { _MegtagadasIndoka = value; }                                                        
            }        
                   
           
        private SqlGuid _Megtagado_Id = SqlGuid.Null;
           
        /// <summary>
        /// Megtagado_Id Base property </summary>
            public SqlGuid Megtagado_Id
            {
                get { return _Megtagado_Id; }
                set { _Megtagado_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _MegtagadasDat = SqlDateTime.Null;
           
        /// <summary>
        /// MegtagadasDat Base property </summary>
            public SqlDateTime MegtagadasDat
            {
                get { return _MegtagadasDat; }
                set { _MegtagadasDat = value; }                                                        
            }        
                   
           
        private SqlString _KimenoKuldemenyFajta = SqlString.Null;
           
        /// <summary>
        /// KimenoKuldemenyFajta Base property </summary>
            public SqlString KimenoKuldemenyFajta
            {
                get { return _KimenoKuldemenyFajta; }
                set { _KimenoKuldemenyFajta = value; }                                                        
            }        
                   
           
        private SqlChars _Elsobbsegi = SqlChars.Null;
           
        /// <summary>
        /// Elsobbsegi Base property </summary>
            public SqlChars Elsobbsegi
            {
                get { return _Elsobbsegi; }
                set { _Elsobbsegi = value; }                                                        
            }        
                   
           
        private SqlChars _Ajanlott = SqlChars.Null;
           
        /// <summary>
        /// Ajanlott Base property </summary>
            public SqlChars Ajanlott
            {
                get { return _Ajanlott; }
                set { _Ajanlott = value; }                                                        
            }        
                   
           
        private SqlChars _Tertiveveny = SqlChars.Null;
           
        /// <summary>
        /// Tertiveveny Base property </summary>
            public SqlChars Tertiveveny
            {
                get { return _Tertiveveny; }
                set { _Tertiveveny = value; }                                                        
            }        
                   
           
        private SqlChars _SajatKezbe = SqlChars.Null;
           
        /// <summary>
        /// SajatKezbe Base property </summary>
            public SqlChars SajatKezbe
            {
                get { return _SajatKezbe; }
                set { _SajatKezbe = value; }                                                        
            }        
                   
           
        private SqlChars _E_ertesites = SqlChars.Null;
           
        /// <summary>
        /// E_ertesites Base property </summary>
            public SqlChars E_ertesites
            {
                get { return _E_ertesites; }
                set { _E_ertesites = value; }                                                        
            }        
                   
           
        private SqlChars _E_elorejelzes = SqlChars.Null;
           
        /// <summary>
        /// E_elorejelzes Base property </summary>
            public SqlChars E_elorejelzes
            {
                get { return _E_elorejelzes; }
                set { _E_elorejelzes = value; }                                                        
            }        
                   
           
        private SqlChars _PostaiLezaroSzolgalat = SqlChars.Null;
           
        /// <summary>
        /// PostaiLezaroSzolgalat Base property </summary>
            public SqlChars PostaiLezaroSzolgalat
            {
                get { return _PostaiLezaroSzolgalat; }
                set { _PostaiLezaroSzolgalat = value; }                                                        
            }        
                   
           
        private SqlString _Ar = SqlString.Null;
           
        /// <summary>
        /// Ar Base property </summary>
            public SqlString Ar
            {
                get { return _Ar; }
                set { _Ar = value; }                                                        
            }        
                   
           
        private SqlInt32 _KimenoKuld_Sorszam  = SqlInt32.Null;
           
        /// <summary>
        /// KimenoKuld_Sorszam  Base property </summary>
            public SqlInt32 KimenoKuld_Sorszam 
            {
                get { return _KimenoKuld_Sorszam ; }
                set { _KimenoKuld_Sorszam  = value; }                                                        
            }        
                   
           
        private SqlGuid _IrattarId = SqlGuid.Null;
           
        /// <summary>
        /// IrattarId Base property </summary>
            public SqlGuid IrattarId
            {
                get { return _IrattarId; }
                set { _IrattarId = value; }                                                        
            }        
                   
           
        private SqlString _IrattariHely = SqlString.Null;
           
        /// <summary>
        /// IrattariHely Base property </summary>
            public SqlString IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }                                                        
            }        
                   
           
        private SqlString _TovabbitasAlattAllapot = SqlString.Null;
           
        /// <summary>
        /// TovabbitasAlattAllapot Base property </summary>
            public SqlString TovabbitasAlattAllapot
            {
                get { return _TovabbitasAlattAllapot; }
                set { _TovabbitasAlattAllapot = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _BoritoTipus = SqlString.Null;
           
        /// <summary>
        /// BoritoTipus Base property </summary>
            public SqlString BoritoTipus
            {
                get { return _BoritoTipus; }
                set { _BoritoTipus = value; }                                                        
            }        
                   
           
        private SqlChars _MegorzesJelzo = SqlChars.Null;
           
        /// <summary>
        /// MegorzesJelzo Base property </summary>
            public SqlChars MegorzesJelzo
            {
                get { return _MegorzesJelzo; }
                set { _MegorzesJelzo = value; }                                                        
            }        
                   
           
        private SqlString _KulsoAzonosito = SqlString.Null;
           
        /// <summary>
        /// KulsoAzonosito Base property </summary>
            public SqlString KulsoAzonosito
            {
                get { return _KulsoAzonosito; }
                set { _KulsoAzonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlChars _IktatastNemIgenyel = SqlChars.Null;
           
        /// <summary>
        /// IktatastNemIgenyel Base property </summary>
            public SqlChars IktatastNemIgenyel
            {
                get { return _IktatastNemIgenyel; }
                set { _IktatastNemIgenyel = value; }                                                        
            }        
                   
           
        private SqlString _KezbesitesModja = SqlString.Null;
           
        /// <summary>
        /// KezbesitesModja Base property </summary>
            public SqlString KezbesitesModja
            {
                get { return _KezbesitesModja; }
                set { _KezbesitesModja = value; }                                                        
            }        
                   
           
        private SqlString _Munkaallomas = SqlString.Null;
           
        /// <summary>
        /// Munkaallomas Base property </summary>
            public SqlString Munkaallomas
            {
                get { return _Munkaallomas; }
                set { _Munkaallomas = value; }                                                        
            }        
                   
           
        private SqlChars _SerultKuldemeny = SqlChars.Null;
           
        /// <summary>
        /// SerultKuldemeny Base property </summary>
            public SqlChars SerultKuldemeny
            {
                get { return _SerultKuldemeny; }
                set { _SerultKuldemeny = value; }                                                        
            }        
                   
           
        private SqlChars _TevesCimzes = SqlChars.Null;
           
        /// <summary>
        /// TevesCimzes Base property </summary>
            public SqlChars TevesCimzes
            {
                get { return _TevesCimzes; }
                set { _TevesCimzes = value; }                                                        
            }        
                   
           
        private SqlChars _TevesErkeztetes = SqlChars.Null;
           
        /// <summary>
        /// TevesErkeztetes Base property </summary>
            public SqlChars TevesErkeztetes
            {
                get { return _TevesErkeztetes; }
                set { _TevesErkeztetes = value; }                                                        
            }        
                   
           
        private SqlString _CimzesTipusa = SqlString.Null;
           
        /// <summary>
        /// CimzesTipusa Base property </summary>
            public SqlString CimzesTipusa
            {
                get { return _CimzesTipusa; }
                set { _CimzesTipusa = value; }                                                        
            }        
                   
           
        private SqlString _FutarJegyzekListaSzama = SqlString.Null;
           
        /// <summary>
        /// FutarJegyzekListaSzama Base property </summary>
            public SqlString FutarJegyzekListaSzama
            {
                get { return _FutarJegyzekListaSzama; }
                set { _FutarJegyzekListaSzama = value; }                                                        
            }        
                   
           
        private SqlDateTime _KezbesitoAtvetelIdopontja = SqlDateTime.Null;
           
        /// <summary>
        /// KezbesitoAtvetelIdopontja Base property </summary>
            public SqlDateTime KezbesitoAtvetelIdopontja
            {
                get { return _KezbesitoAtvetelIdopontja; }
                set { _KezbesitoAtvetelIdopontja = value; }                                                        
            }        
                   
           
        private SqlString _KezbesitoAtvevoNeve = SqlString.Null;
           
        /// <summary>
        /// KezbesitoAtvevoNeve Base property </summary>
            public SqlString KezbesitoAtvevoNeve
            {
                get { return _KezbesitoAtvevoNeve; }
                set { _KezbesitoAtvevoNeve = value; }                                                        
            }        
                   
           
        private SqlGuid _Minosito = SqlGuid.Null;
           
        /// <summary>
        /// Minosito Base property </summary>
            public SqlGuid Minosito
            {
                get { return _Minosito; }
                set { _Minosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _MinositesErvenyessegiIdeje = SqlDateTime.Null;
           
        /// <summary>
        /// MinositesErvenyessegiIdeje Base property </summary>
            public SqlDateTime MinositesErvenyessegiIdeje
            {
                get { return _MinositesErvenyessegiIdeje; }
                set { _MinositesErvenyessegiIdeje = value; }                                                        
            }        
                   
           
        private SqlInt32 _TerjedelemMennyiseg = SqlInt32.Null;
           
        /// <summary>
        /// TerjedelemMennyiseg Base property </summary>
            public SqlInt32 TerjedelemMennyiseg
            {
                get { return _TerjedelemMennyiseg; }
                set { _TerjedelemMennyiseg = value; }                                                        
            }        
                   
           
        private SqlString _TerjedelemMennyisegiEgyseg = SqlString.Null;
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg Base property </summary>
            public SqlString TerjedelemMennyisegiEgyseg
            {
                get { return _TerjedelemMennyisegiEgyseg; }
                set { _TerjedelemMennyisegiEgyseg = value; }                                                        
            }        
                   
           
        private SqlString _TerjedelemMegjegyzes = SqlString.Null;
           
        /// <summary>
        /// TerjedelemMegjegyzes Base property </summary>
            public SqlString TerjedelemMegjegyzes
            {
                get { return _TerjedelemMegjegyzes; }
                set { _TerjedelemMegjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _ModositasErvenyessegKezdete = SqlDateTime.Null;
           
        /// <summary>
        /// ModositasErvenyessegKezdete Base property </summary>
            public SqlDateTime ModositasErvenyessegKezdete
            {
                get { return _ModositasErvenyessegKezdete; }
                set { _ModositasErvenyessegKezdete = value; }                                                        
            }        
                   
           
        private SqlString _MegszuntetoHatarozat = SqlString.Null;
           
        /// <summary>
        /// MegszuntetoHatarozat Base property </summary>
            public SqlString MegszuntetoHatarozat
            {
                get { return _MegszuntetoHatarozat; }
                set { _MegszuntetoHatarozat = value; }                                                        
            }        
                   
           
        private SqlDateTime _FelulvizsgalatDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// FelulvizsgalatDatuma Base property </summary>
            public SqlDateTime FelulvizsgalatDatuma
            {
                get { return _FelulvizsgalatDatuma; }
                set { _FelulvizsgalatDatuma = value; }                                                        
            }        
                   
           
        private SqlGuid _Felulvizsgalo_id = SqlGuid.Null;
           
        /// <summary>
        /// Felulvizsgalo_id Base property </summary>
            public SqlGuid Felulvizsgalo_id
            {
                get { return _Felulvizsgalo_id; }
                set { _Felulvizsgalo_id = value; }                                                        
            }        
                   
           
        private SqlString _MinositesFelulvizsgalatEredm = SqlString.Null;
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm Base property </summary>
            public SqlString MinositesFelulvizsgalatEredm
            {
                get { return _MinositesFelulvizsgalatEredm; }
                set { _MinositesFelulvizsgalatEredm = value; }                                                        
            }        
                   
           
        private SqlGuid _MinositoSzervezet = SqlGuid.Null;
           
        /// <summary>
        /// MinositoSzervezet Base property </summary>
            public SqlGuid MinositoSzervezet
            {
                get { return _MinositoSzervezet; }
                set { _MinositoSzervezet = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_BekuldoKapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_BekuldoKapcsolt Base property </summary>
            public SqlGuid Partner_Id_BekuldoKapcsolt
            {
                get { return _Partner_Id_BekuldoKapcsolt; }
                set { _Partner_Id_BekuldoKapcsolt = value; }                                                        
            }        
                   
           
        private SqlString _MinositesFelulvizsgalatBizTag = SqlString.Null;
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag Base property </summary>
            public SqlString MinositesFelulvizsgalatBizTag
            {
                get { return _MinositesFelulvizsgalatBizTag; }
                set { _MinositesFelulvizsgalatBizTag = value; }                                                        
            }        
                   
           
        private SqlChars _Erteknyilvanitas = SqlChars.Null;
           
        /// <summary>
        /// Erteknyilvanitas Base property </summary>
            public SqlChars Erteknyilvanitas
            {
                get { return _Erteknyilvanitas; }
                set { _Erteknyilvanitas = value; }                                                        
            }        
                   
           
        private SqlInt32 _ErteknyilvanitasOsszege = SqlInt32.Null;
           
        /// <summary>
        /// ErteknyilvanitasOsszege Base property </summary>
            public SqlInt32 ErteknyilvanitasOsszege
            {
                get { return _ErteknyilvanitasOsszege; }
                set { _ErteknyilvanitasOsszege = value; }                                                        
            }        
                           }
    }    
}