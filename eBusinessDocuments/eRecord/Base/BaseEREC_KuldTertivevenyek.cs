
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldTertivevenyek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_KuldTertivevenyek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_KuldTertivevenyekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Kuldemeny_Id = true;
               public bool Kuldemeny_Id
               {
                   get { return _Kuldemeny_Id; }
                   set { _Kuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _Ragszam = true;
               public bool Ragszam
               {
                   get { return _Ragszam; }
                   set { _Ragszam = value; }
               }
                                                            
                                 
               private bool _TertivisszaKod = true;
               public bool TertivisszaKod
               {
                   get { return _TertivisszaKod; }
                   set { _TertivisszaKod = value; }
               }
                                                            
                                 
               private bool _TertivisszaDat = true;
               public bool TertivisszaDat
               {
                   get { return _TertivisszaDat; }
                   set { _TertivisszaDat = value; }
               }
                                                            
                                 
               private bool _AtvevoSzemely = true;
               public bool AtvevoSzemely
               {
                   get { return _AtvevoSzemely; }
                   set { _AtvevoSzemely = value; }
               }
                                                            
                                 
               private bool _AtvetelDat = true;
               public bool AtvetelDat
               {
                   get { return _AtvetelDat; }
                   set { _AtvetelDat = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                            
                                 
               private bool _KezbVelelemBeallta = true;
               public bool KezbVelelemBeallta
               {
                   get { return _KezbVelelemBeallta; }
                   set { _KezbVelelemBeallta = value; }
               }
                                                            
                                 
               private bool _KezbVelelemDatuma = true;
               public bool KezbVelelemDatuma
               {
                   get { return _KezbVelelemDatuma; }
                   set { _KezbVelelemDatuma = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Kuldemeny_Id = Value;               
                    
                   Ragszam = Value;               
                    
                   TertivisszaKod = Value;               
                    
                   TertivisszaDat = Value;               
                    
                   AtvevoSzemely = Value;               
                    
                   AtvetelDat = Value;               
                    
                   BarCode = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;               
                    
                   KezbVelelemBeallta = Value;               
                    
                   KezbVelelemDatuma = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_KuldTertivevenyekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Kuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kuldemeny_Id Base property </summary>
            public SqlGuid Kuldemeny_Id
            {
                get { return _Kuldemeny_Id; }
                set { _Kuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlString _Ragszam = SqlString.Null;
           
        /// <summary>
        /// Ragszam Base property </summary>
            public SqlString Ragszam
            {
                get { return _Ragszam; }
                set { _Ragszam = value; }                                                        
            }        
                   
           
        private SqlString _TertivisszaKod = SqlString.Null;
           
        /// <summary>
        /// TertivisszaKod Base property </summary>
            public SqlString TertivisszaKod
            {
                get { return _TertivisszaKod; }
                set { _TertivisszaKod = value; }                                                        
            }        
                   
           
        private SqlDateTime _TertivisszaDat = SqlDateTime.Null;
           
        /// <summary>
        /// TertivisszaDat Base property </summary>
            public SqlDateTime TertivisszaDat
            {
                get { return _TertivisszaDat; }
                set { _TertivisszaDat = value; }                                                        
            }        
                   
           
        private SqlString _AtvevoSzemely = SqlString.Null;
           
        /// <summary>
        /// AtvevoSzemely Base property </summary>
            public SqlString AtvevoSzemely
            {
                get { return _AtvevoSzemely; }
                set { _AtvevoSzemely = value; }                                                        
            }        
                   
           
        private SqlDateTime _AtvetelDat = SqlDateTime.Null;
           
        /// <summary>
        /// AtvetelDat Base property </summary>
            public SqlDateTime AtvetelDat
            {
                get { return _AtvetelDat; }
                set { _AtvetelDat = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                   
           
        private SqlChars _KezbVelelemBeallta = SqlChars.Null;
           
        /// <summary>
        /// KezbVelelemBeallta Base property </summary>
            public SqlChars KezbVelelemBeallta
            {
                get { return _KezbVelelemBeallta; }
                set { _KezbVelelemBeallta = value; }                                                        
            }        
                   
           
        private SqlDateTime _KezbVelelemDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// KezbVelelemDatuma Base property </summary>
            public SqlDateTime KezbVelelemDatuma
            {
                get { return _KezbVelelemDatuma; }
                set { _KezbVelelemDatuma = value; }                                                        
            }        
                           }
    }    
}