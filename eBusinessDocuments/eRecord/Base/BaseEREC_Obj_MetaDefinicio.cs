
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_MetaDefinicio BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Obj_MetaDefinicio
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_MetaDefinicioBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Objtip_Id = true;
               public bool Objtip_Id
               {
                   get { return _Objtip_Id; }
                   set { _Objtip_Id = value; }
               }
                                                            
                                 
               private bool _Objtip_Id_Column = true;
               public bool Objtip_Id_Column
               {
                   get { return _Objtip_Id_Column; }
                   set { _Objtip_Id_Column = value; }
               }
                                                            
                                 
               private bool _ColumnValue = true;
               public bool ColumnValue
               {
                   get { return _ColumnValue; }
                   set { _ColumnValue = value; }
               }
                                                            
                                 
               private bool _DefinicioTipus = true;
               public bool DefinicioTipus
               {
                   get { return _DefinicioTipus; }
                   set { _DefinicioTipus = value; }
               }
                                                            
                                 
               private bool _Felettes_Obj_Meta = true;
               public bool Felettes_Obj_Meta
               {
                   get { return _Felettes_Obj_Meta; }
                   set { _Felettes_Obj_Meta = value; }
               }
                                                            
                                 
               private bool _MetaXSD = true;
               public bool MetaXSD
               {
                   get { return _MetaXSD; }
                   set { _MetaXSD = value; }
               }
                                                            
                                 
               private bool _ImportXML = true;
               public bool ImportXML
               {
                   get { return _ImportXML; }
                   set { _ImportXML = value; }
               }
                                                            
                                 
               private bool _EgyebXML = true;
               public bool EgyebXML
               {
                   get { return _EgyebXML; }
                   set { _EgyebXML = value; }
               }
                                                            
                                 
               private bool _ContentType = true;
               public bool ContentType
               {
                   get { return _ContentType; }
                   set { _ContentType = value; }
               }
                                                            
                                 
               private bool _SPSSzinkronizalt = true;
               public bool SPSSzinkronizalt
               {
                   get { return _SPSSzinkronizalt; }
                   set { _SPSSzinkronizalt = value; }
               }
                                                            
                                 
               private bool _SPS_CTT_Id = true;
               public bool SPS_CTT_Id
               {
                   get { return _SPS_CTT_Id; }
                   set { _SPS_CTT_Id = value; }
               }
                                                            
                                 
               private bool _WorkFlowVezerles = true;
               public bool WorkFlowVezerles
               {
                   get { return _WorkFlowVezerles; }
                   set { _WorkFlowVezerles = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Objtip_Id = Value;               
                    
                   Objtip_Id_Column = Value;               
                    
                   ColumnValue = Value;               
                    
                   DefinicioTipus = Value;               
                    
                   Felettes_Obj_Meta = Value;               
                    
                   MetaXSD = Value;               
                    
                   ImportXML = Value;               
                    
                   EgyebXML = Value;               
                    
                   ContentType = Value;               
                    
                   SPSSzinkronizalt = Value;               
                    
                   SPS_CTT_Id = Value;               
                    
                   WorkFlowVezerles = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_MetaDefinicioBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Objtip_Id = SqlGuid.Null;
           
        /// <summary>
        /// Objtip_Id Base property </summary>
            public SqlGuid Objtip_Id
            {
                get { return _Objtip_Id; }
                set { _Objtip_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Objtip_Id_Column = SqlGuid.Null;
           
        /// <summary>
        /// Objtip_Id_Column Base property </summary>
            public SqlGuid Objtip_Id_Column
            {
                get { return _Objtip_Id_Column; }
                set { _Objtip_Id_Column = value; }                                                        
            }        
                   
           
        private SqlString _ColumnValue = SqlString.Null;
           
        /// <summary>
        /// ColumnValue Base property </summary>
            public SqlString ColumnValue
            {
                get { return _ColumnValue; }
                set { _ColumnValue = value; }                                                        
            }        
                   
           
        private SqlString _DefinicioTipus = SqlString.Null;
           
        /// <summary>
        /// DefinicioTipus Base property </summary>
            public SqlString DefinicioTipus
            {
                get { return _DefinicioTipus; }
                set { _DefinicioTipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Felettes_Obj_Meta = SqlGuid.Null;
           
        /// <summary>
        /// Felettes_Obj_Meta Base property </summary>
            public SqlGuid Felettes_Obj_Meta
            {
                get { return _Felettes_Obj_Meta; }
                set { _Felettes_Obj_Meta = value; }                                                        
            }        
                   
           
        private SqlXml _MetaXSD = SqlXml.Null;
           
        /// <summary>
        /// MetaXSD Base property </summary>
            public SqlXml MetaXSD
            {
                get { return _MetaXSD; }
                set { _MetaXSD = value; }                                                        
            }        
                   
           
        private SqlXml _ImportXML = SqlXml.Null;
           
        /// <summary>
        /// ImportXML Base property </summary>
            public SqlXml ImportXML
            {
                get { return _ImportXML; }
                set { _ImportXML = value; }                                                        
            }        
                   
           
        private SqlXml _EgyebXML = SqlXml.Null;
           
        /// <summary>
        /// EgyebXML Base property </summary>
            public SqlXml EgyebXML
            {
                get { return _EgyebXML; }
                set { _EgyebXML = value; }                                                        
            }        
                   
           
        private SqlString _ContentType = SqlString.Null;
           
        /// <summary>
        /// ContentType Base property </summary>
            public SqlString ContentType
            {
                get { return _ContentType; }
                set { _ContentType = value; }                                                        
            }        
                   
           
        private SqlChars _SPSSzinkronizalt = SqlChars.Null;
           
        /// <summary>
        /// SPSSzinkronizalt Base property </summary>
            public SqlChars SPSSzinkronizalt
            {
                get { return _SPSSzinkronizalt; }
                set { _SPSSzinkronizalt = value; }                                                        
            }        
                   
           
        private SqlString _SPS_CTT_Id = SqlString.Null;
           
        /// <summary>
        /// SPS_CTT_Id Base property </summary>
            public SqlString SPS_CTT_Id
            {
                get { return _SPS_CTT_Id; }
                set { _SPS_CTT_Id = value; }                                                        
            }        
                   
           
        private SqlChars _WorkFlowVezerles = SqlChars.Null;
           
        /// <summary>
        /// WorkFlowVezerles Base property </summary>
            public SqlChars WorkFlowVezerles
            {
                get { return _WorkFlowVezerles; }
                set { _WorkFlowVezerles = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}