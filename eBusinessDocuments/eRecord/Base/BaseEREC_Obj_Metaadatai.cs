
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_MetaAdatai BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Obj_MetaAdatai
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_MetaAdataiBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Obj_MetaDefinicio_Id = true;
               public bool Obj_MetaDefinicio_Id
               {
                   get { return _Obj_MetaDefinicio_Id; }
                   set { _Obj_MetaDefinicio_Id = value; }
               }
                                                            
                                 
               private bool _Targyszavak_Id = true;
               public bool Targyszavak_Id
               {
                   get { return _Targyszavak_Id; }
                   set { _Targyszavak_Id = value; }
               }
                                                            
                                 
               private bool _AlapertelmezettErtek = true;
               public bool AlapertelmezettErtek
               {
                   get { return _AlapertelmezettErtek; }
                   set { _AlapertelmezettErtek = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Opcionalis = true;
               public bool Opcionalis
               {
                   get { return _Opcionalis; }
                   set { _Opcionalis = value; }
               }
                                                            
                                 
               private bool _Ismetlodo = true;
               public bool Ismetlodo
               {
                   get { return _Ismetlodo; }
                   set { _Ismetlodo = value; }
               }
                                                            
                                 
               private bool _Funkcio = true;
               public bool Funkcio
               {
                   get { return _Funkcio; }
                   set { _Funkcio = value; }
               }
                                                            
                                 
               private bool _ControlTypeSource = true;
               public bool ControlTypeSource
               {
                   get { return _ControlTypeSource; }
                   set { _ControlTypeSource = value; }
               }
                                                            
                                 
               private bool _ControlTypeDataSource = true;
               public bool ControlTypeDataSource
               {
                   get { return _ControlTypeDataSource; }
                   set { _ControlTypeDataSource = value; }
               }
                                                            
                                 
               private bool _SPSSzinkronizalt = true;
               public bool SPSSzinkronizalt
               {
                   get { return _SPSSzinkronizalt; }
                   set { _SPSSzinkronizalt = value; }
               }
                                                            
                                 
               private bool _SPS_Field_Id = true;
               public bool SPS_Field_Id
               {
                   get { return _SPS_Field_Id; }
                   set { _SPS_Field_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Obj_MetaDefinicio_Id = Value;               
                    
                   Targyszavak_Id = Value;               
                    
                   AlapertelmezettErtek = Value;               
                    
                   Sorszam = Value;               
                    
                   Opcionalis = Value;               
                    
                   Ismetlodo = Value;               
                    
                   Funkcio = Value;               
                    
                   ControlTypeSource = Value;               
                    
                   ControlTypeDataSource = Value;               
                    
                   SPSSzinkronizalt = Value;               
                    
                   SPS_Field_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_MetaAdataiBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_MetaDefinicio_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_MetaDefinicio_Id Base property </summary>
            public SqlGuid Obj_MetaDefinicio_Id
            {
                get { return _Obj_MetaDefinicio_Id; }
                set { _Obj_MetaDefinicio_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Targyszavak_Id = SqlGuid.Null;
           
        /// <summary>
        /// Targyszavak_Id Base property </summary>
            public SqlGuid Targyszavak_Id
            {
                get { return _Targyszavak_Id; }
                set { _Targyszavak_Id = value; }                                                        
            }        
                   
           
        private SqlString _AlapertelmezettErtek = SqlString.Null;
           
        /// <summary>
        /// AlapertelmezettErtek Base property </summary>
            public SqlString AlapertelmezettErtek
            {
                get { return _AlapertelmezettErtek; }
                set { _AlapertelmezettErtek = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlChars _Opcionalis = SqlChars.Null;
           
        /// <summary>
        /// Opcionalis Base property </summary>
            public SqlChars Opcionalis
            {
                get { return _Opcionalis; }
                set { _Opcionalis = value; }                                                        
            }        
                   
           
        private SqlChars _Ismetlodo = SqlChars.Null;
           
        /// <summary>
        /// Ismetlodo Base property </summary>
            public SqlChars Ismetlodo
            {
                get { return _Ismetlodo; }
                set { _Ismetlodo = value; }                                                        
            }        
                   
           
        private SqlString _Funkcio = SqlString.Null;
           
        /// <summary>
        /// Funkcio Base property </summary>
            public SqlString Funkcio
            {
                get { return _Funkcio; }
                set { _Funkcio = value; }                                                        
            }        
                   
           
        private SqlString _ControlTypeSource = SqlString.Null;
           
        /// <summary>
        /// ControlTypeSource Base property </summary>
            public SqlString ControlTypeSource
            {
                get { return _ControlTypeSource; }
                set { _ControlTypeSource = value; }                                                        
            }        
                   
           
        private SqlString _ControlTypeDataSource = SqlString.Null;
           
        /// <summary>
        /// ControlTypeDataSource Base property </summary>
            public SqlString ControlTypeDataSource
            {
                get { return _ControlTypeDataSource; }
                set { _ControlTypeDataSource = value; }                                                        
            }        
                   
           
        private SqlChars _SPSSzinkronizalt = SqlChars.Null;
           
        /// <summary>
        /// SPSSzinkronizalt Base property </summary>
            public SqlChars SPSSzinkronizalt
            {
                get { return _SPSSzinkronizalt; }
                set { _SPSSzinkronizalt = value; }                                                        
            }        
                   
           
        private SqlGuid _SPS_Field_Id = SqlGuid.Null;
           
        /// <summary>
        /// SPS_Field_Id Base property </summary>
            public SqlGuid SPS_Field_Id
            {
                get { return _SPS_Field_Id; }
                set { _SPS_Field_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}