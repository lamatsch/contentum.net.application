
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_TargySzavai BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Obj_TargySzavai
    {
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_TargySzavaiBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _TargySzo_Id = true;
               public bool TargySzo_Id
               {
                   get { return _TargySzo_Id; }
                   set { _TargySzo_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_AdatElem = true;
               public bool ObjTip_Id_AdatElem
               {
                   get { return _ObjTip_Id_AdatElem; }
                   set { _ObjTip_Id_AdatElem = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _ErtekTipus_Id = true;
               public bool ErtekTipus_Id
               {
                   get { return _ErtekTipus_Id; }
                   set { _ErtekTipus_Id = value; }
               }
                                                            
                                 
               private bool _Ertek = true;
               public bool Ertek
               {
                   get { return _Ertek; }
                   set { _Ertek = value; }
               }
                                                            
                                 
               private bool _xml = true;
               public bool xml
               {
                   get { return _xml; }
                   set { _xml = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                   Id = Value;               
                   TargySzo_Id = Value;               
                   ObjTip_Id_AdatElem = Value;               
                   Obj_Id = Value;               
                   Tipus = Value;               
                   ErtekTipus_Id = Value;               
                   Ertek = Value;               
                   xml = Value;               
                   ErvKezd = Value;               
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_Obj_TargySzavaiBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _TargySzo_Id = SqlGuid.Null;
           
        /// <summary>
        /// TargySzo_Id Base property </summary>
            public SqlGuid TargySzo_Id
            {
                get { return _TargySzo_Id; }
                set { _TargySzo_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_AdatElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_AdatElem Base property </summary>
            public SqlGuid ObjTip_Id_AdatElem
            {
                get { return _ObjTip_Id_AdatElem; }
                set { _ObjTip_Id_AdatElem = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _ErtekTipus_Id = SqlGuid.Null;
           
        /// <summary>
        /// ErtekTipus_Id Base property </summary>
            public SqlGuid ErtekTipus_Id
            {
                get { return _ErtekTipus_Id; }
                set { _ErtekTipus_Id = value; }                                                        
            }        
                   
           
        private SqlString _Ertek = SqlString.Null;
           
        /// <summary>
        /// Ertek Base property </summary>
            public SqlString Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }                                                        
            }        
                   
           
        private SqlXml _xml = SqlXml.Null;
           
        /// <summary>
        /// xml Base property </summary>
            public SqlXml xml
            {
                get { return _xml; }
                set { _xml = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}