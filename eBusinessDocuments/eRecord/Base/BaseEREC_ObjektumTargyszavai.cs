
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_ObjektumTargyszavai BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_ObjektumTargyszavai
    {
        [System.Xml.Serialization.XmlType("BaseEREC_ObjektumTargyszavaiBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Targyszo_Id = true;
               public bool Targyszo_Id
               {
                   get { return _Targyszo_Id; }
                   set { _Targyszo_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Metaadatai_Id = true;
               public bool Obj_Metaadatai_Id
               {
                   get { return _Obj_Metaadatai_Id; }
                   set { _Obj_Metaadatai_Id = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id = true;
               public bool ObjTip_Id
               {
                   get { return _ObjTip_Id; }
                   set { _ObjTip_Id = value; }
               }
                                                            
                                 
               private bool _Targyszo = true;
               public bool Targyszo
               {
                   get { return _Targyszo; }
                   set { _Targyszo = value; }
               }
                                                            
                                 
               private bool _Forras = true;
               public bool Forras
               {
                   get { return _Forras; }
                   set { _Forras = value; }
               }
                                                            
                                 
               private bool _SPSSzinkronizalt = true;
               public bool SPSSzinkronizalt
               {
                   get { return _SPSSzinkronizalt; }
                   set { _SPSSzinkronizalt = value; }
               }
                                                            
                                 
               private bool _Ertek = true;
               public bool Ertek
               {
                   get { return _Ertek; }
                   set { _Ertek = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Targyszo_XML = true;
               public bool Targyszo_XML
               {
                   get { return _Targyszo_XML; }
                   set { _Targyszo_XML = value; }
               }
                                                            
                                 
               private bool _Targyszo_XML_Ervenyes = true;
               public bool Targyszo_XML_Ervenyes
               {
                   get { return _Targyszo_XML_Ervenyes; }
                   set { _Targyszo_XML_Ervenyes = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Targyszo_Id = Value;               
                    
                   Obj_Metaadatai_Id = Value;               
                    
                   Obj_Id = Value;               
                    
                   ObjTip_Id = Value;               
                    
                   Targyszo = Value;               
                    
                   Forras = Value;               
                    
                   SPSSzinkronizalt = Value;               
                    
                   Ertek = Value;               
                    
                   Sorszam = Value;               
                    
                   Targyszo_XML = Value;               
                    
                   Targyszo_XML_Ervenyes = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_ObjektumTargyszavaiBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Targyszo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Targyszo_Id Base property </summary>
            public SqlGuid Targyszo_Id
            {
                get { return _Targyszo_Id; }
                set { _Targyszo_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Metaadatai_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Metaadatai_Id Base property </summary>
            public SqlGuid Obj_Metaadatai_Id
            {
                get { return _Obj_Metaadatai_Id; }
                set { _Obj_Metaadatai_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlGuid Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id Base property </summary>
            public SqlGuid ObjTip_Id
            {
                get { return _ObjTip_Id; }
                set { _ObjTip_Id = value; }                                                        
            }        
                   
           
        private SqlString _Targyszo = SqlString.Null;
           
        /// <summary>
        /// Targyszo Base property </summary>
            public SqlString Targyszo
            {
                get { return _Targyszo; }
                set { _Targyszo = value; }                                                        
            }        
                   
           
        private SqlString _Forras = SqlString.Null;
           
        /// <summary>
        /// Forras Base property </summary>
            public SqlString Forras
            {
                get { return _Forras; }
                set { _Forras = value; }                                                        
            }        
                   
           
        private SqlChars _SPSSzinkronizalt = SqlChars.Null;
           
        /// <summary>
        /// SPSSzinkronizalt Base property </summary>
            public SqlChars SPSSzinkronizalt
            {
                get { return _SPSSzinkronizalt; }
                set { _SPSSzinkronizalt = value; }                                                        
            }        
                   
           
        private SqlString _Ertek = SqlString.Null;
           
        /// <summary>
        /// Ertek Base property </summary>
            public SqlString Ertek
            {
                get { return _Ertek; }
                set { _Ertek = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlXml _Targyszo_XML = SqlXml.Null;
           
        /// <summary>
        /// Targyszo_XML Base property </summary>
            public SqlXml Targyszo_XML
            {
                get { return _Targyszo_XML; }
                set { _Targyszo_XML = value; }                                                        
            }        
                   
           
        private SqlChars _Targyszo_XML_Ervenyes = SqlChars.Null;
           
        /// <summary>
        /// Targyszo_XML_Ervenyes Base property </summary>
            public SqlChars Targyszo_XML_Ervenyes
            {
                get { return _Targyszo_XML_Ervenyes; }
                set { _Targyszo_XML_Ervenyes = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}