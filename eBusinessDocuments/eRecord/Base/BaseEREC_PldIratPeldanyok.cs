
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_PldIratPeldanyok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_PldIratPeldanyok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_PldIratPeldanyokBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _IraIrat_Id = true;
            public bool IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }
            }


            private bool _UgyUgyirat_Id_Kulso = true;
            public bool UgyUgyirat_Id_Kulso
            {
                get { return _UgyUgyirat_Id_Kulso; }
                set { _UgyUgyirat_Id_Kulso = value; }
            }


            private bool _Sorszam = true;
            public bool Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }
            }


            private bool _SztornirozasDat = true;
            public bool SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }
            }


            private bool _AtvetelDatuma = true;
            public bool AtvetelDatuma
            {
                get { return _AtvetelDatuma; }
                set { _AtvetelDatuma = value; }
            }


            private bool _Eredet = true;
            public bool Eredet
            {
                get { return _Eredet; }
                set { _Eredet = value; }
            }


            private bool _KuldesMod = true;
            public bool KuldesMod
            {
                get { return _KuldesMod; }
                set { _KuldesMod = value; }
            }


            private bool _Ragszam = true;
            public bool Ragszam
            {
                get { return _Ragszam; }
                set { _Ragszam = value; }
            }


            private bool _UgyintezesModja = true;
            public bool UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }
            }


            private bool _VisszaerkezesiHatarido = true;
            public bool VisszaerkezesiHatarido
            {
                get { return _VisszaerkezesiHatarido; }
                set { _VisszaerkezesiHatarido = value; }
            }


            private bool _Visszavarolag = true;
            public bool Visszavarolag
            {
                get { return _Visszavarolag; }
                set { _Visszavarolag = value; }
            }


            private bool _VisszaerkezesDatuma = true;
            public bool VisszaerkezesDatuma
            {
                get { return _VisszaerkezesDatuma; }
                set { _VisszaerkezesDatuma = value; }
            }


            private bool _Cim_id_Cimzett = true;
            public bool Cim_id_Cimzett
            {
                get { return _Cim_id_Cimzett; }
                set { _Cim_id_Cimzett = value; }
            }


            private bool _Partner_Id_Cimzett = true;
            public bool Partner_Id_Cimzett
            {
                get { return _Partner_Id_Cimzett; }
                set { _Partner_Id_Cimzett = value; }
            }
			
			private bool _Partner_Id_CimzettKapcsolt = true;
            public bool Partner_Id_CimzettKapcsolt
            {
                   get { return _Partner_Id_CimzettKapcsolt; }
                   set { _Partner_Id_CimzettKapcsolt = value; }
            }

            private bool _Csoport_Id_Felelos = true;
            public bool Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private bool _FelhasznaloCsoport_Id_Orzo = true;
            public bool FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }
            }


            private bool _Csoport_Id_Felelos_Elozo = true;
            public bool Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }
            }


            private bool _CimSTR_Cimzett = true;
            public bool CimSTR_Cimzett
            {
                get { return _CimSTR_Cimzett; }
                set { _CimSTR_Cimzett = value; }
            }


            private bool _NevSTR_Cimzett = true;
            public bool NevSTR_Cimzett
            {
                get { return _NevSTR_Cimzett; }
                set { _NevSTR_Cimzett = value; }
            }


            private bool _Tovabbito = true;
            public bool Tovabbito
            {
                get { return _Tovabbito; }
                set { _Tovabbito = value; }
            }


            private bool _IraIrat_Id_Kapcsolt = true;
            public bool IraIrat_Id_Kapcsolt
            {
                get { return _IraIrat_Id_Kapcsolt; }
                set { _IraIrat_Id_Kapcsolt = value; }
            }


            private bool _IrattariHely = true;
            public bool IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }
            }


            private bool _Gener_Id = true;
            public bool Gener_Id
            {
                get { return _Gener_Id; }
                set { _Gener_Id = value; }
            }


            private bool _PostazasDatuma = true;
            public bool PostazasDatuma
            {
                get { return _PostazasDatuma; }
                set { _PostazasDatuma = value; }
            }


            private bool _BarCode = true;
            public bool BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }
            }


            private bool _Allapot = true;
            public bool Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private bool _Azonosito = true;
            public bool Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }
            }


            private bool _Kovetkezo_Felelos_Id = true;
            public bool Kovetkezo_Felelos_Id
            {
                get { return _Kovetkezo_Felelos_Id; }
                set { _Kovetkezo_Felelos_Id = value; }
            }


            private bool _Elektronikus_Kezbesitesi_Allap = true;
            public bool Elektronikus_Kezbesitesi_Allap
            {
                get { return _Elektronikus_Kezbesitesi_Allap; }
                set { _Elektronikus_Kezbesitesi_Allap = value; }
            }


            private bool _Kovetkezo_Orzo_Id = true;
            public bool Kovetkezo_Orzo_Id
            {
                get { return _Kovetkezo_Orzo_Id; }
                set { _Kovetkezo_Orzo_Id = value; }
            }


            private bool _Fizikai_Kezbesitesi_Allapot = true;
            public bool Fizikai_Kezbesitesi_Allapot
            {
                get { return _Fizikai_Kezbesitesi_Allapot; }
                set { _Fizikai_Kezbesitesi_Allapot = value; }
            }


            private bool _TovabbitasAlattAllapot = true;
            public bool TovabbitasAlattAllapot
            {
                get { return _TovabbitasAlattAllapot; }
                set { _TovabbitasAlattAllapot = value; }
            }


            private bool _PostazasAllapot = true;
            public bool PostazasAllapot
            {
                get { return _PostazasAllapot; }
                set { _PostazasAllapot = value; }
            }


            private bool _ValaszElektronikus = true;
            public bool ValaszElektronikus
            {
                get { return _ValaszElektronikus; }
                set { _ValaszElektronikus = value; }
            }


            private bool _SelejtezesDat = true;
            public bool SelejtezesDat
            {
                get { return _SelejtezesDat; }
                set { _SelejtezesDat = value; }
            }


            private bool _FelhCsoport_Id_Selejtezo = true;
            public bool FelhCsoport_Id_Selejtezo
            {
                get { return _FelhCsoport_Id_Selejtezo; }
                set { _FelhCsoport_Id_Selejtezo = value; }
            }


            private bool _LeveltariAtvevoNeve = true;
            public bool LeveltariAtvevoNeve
            {
                get { return _LeveltariAtvevoNeve; }
                set { _LeveltariAtvevoNeve = value; }
            }

			private bool _IrattarId = true;
               public bool IrattarId
               {
                   get { return _IrattarId; }
                   set { _IrattarId = value; }
               }

            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            private bool _IratPeldanyMegsemmisitesDatuma = true;
            public bool IratPeldanyMegsemmisitesDatuma
            {
                get { return _IratPeldanyMegsemmisitesDatuma; }
                set { _IratPeldanyMegsemmisitesDatuma = value; }
            }

            private bool _IratPeldanyMegsemmisitve = true;
            public bool IratPeldanyMegsemmisitve
            {
                get { return _IratPeldanyMegsemmisitve; }
                set { _IratPeldanyMegsemmisitve = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                IraIrat_Id = Value;

                UgyUgyirat_Id_Kulso = Value;

                Sorszam = Value;

                SztornirozasDat = Value;

                AtvetelDatuma = Value;

                Eredet = Value;

                KuldesMod = Value;

                Ragszam = Value;

                UgyintezesModja = Value;

                VisszaerkezesiHatarido = Value;

                Visszavarolag = Value;

                VisszaerkezesDatuma = Value;

                Cim_id_Cimzett = Value;

                Partner_Id_Cimzett = Value;
				
				Partner_Id_CimzettKapcsolt = Value;				  

                Csoport_Id_Felelos = Value;

                FelhasznaloCsoport_Id_Orzo = Value;

                Csoport_Id_Felelos_Elozo = Value;

                CimSTR_Cimzett = Value;

                NevSTR_Cimzett = Value;

                Tovabbito = Value;

                IraIrat_Id_Kapcsolt = Value;

                IrattariHely = Value;

                Gener_Id = Value;

                PostazasDatuma = Value;

                BarCode = Value;

                Allapot = Value;

                Azonosito = Value;

                Kovetkezo_Felelos_Id = Value;

                Elektronikus_Kezbesitesi_Allap = Value;

                Kovetkezo_Orzo_Id = Value;

                Fizikai_Kezbesitesi_Allapot = Value;

                TovabbitasAlattAllapot = Value;

                PostazasAllapot = Value;

                ValaszElektronikus = Value;

                SelejtezesDat = Value;

                FelhCsoport_Id_Selejtezo = Value;

                LeveltariAtvevoNeve = Value;

				IrattarId = Value;               

                ErvKezd = Value;

                ErvVege = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseEREC_PldIratPeldanyokBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlGuid _IraIrat_Id = SqlGuid.Null;

            /// <summary>
            /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }
            }


            private SqlGuid _UgyUgyirat_Id_Kulso = SqlGuid.Null;

            /// <summary>
            /// UgyUgyirat_Id_Kulso Base property </summary>
            public SqlGuid UgyUgyirat_Id_Kulso
            {
                get { return _UgyUgyirat_Id_Kulso; }
                set { _UgyUgyirat_Id_Kulso = value; }
            }


            private SqlInt32 _Sorszam = SqlInt32.Null;

            /// <summary>
            /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }
            }


            private SqlDateTime _SztornirozasDat = SqlDateTime.Null;

            /// <summary>
            /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }
            }


            private SqlDateTime _AtvetelDatuma = SqlDateTime.Null;

            /// <summary>
            /// AtvetelDatuma Base property </summary>
            public SqlDateTime AtvetelDatuma
            {
                get { return _AtvetelDatuma; }
                set { _AtvetelDatuma = value; }
            }


            private SqlString _Eredet = SqlString.Null;

            /// <summary>
            /// Eredet Base property </summary>
            public SqlString Eredet
            {
                get { return _Eredet; }
                set { _Eredet = value; }
            }


            private SqlString _KuldesMod = SqlString.Null;

            /// <summary>
            /// KuldesMod Base property </summary>
            public SqlString KuldesMod
            {
                get { return _KuldesMod; }
                set { _KuldesMod = value; }
            }


            private SqlString _Ragszam = SqlString.Null;

            /// <summary>
            /// Ragszam Base property </summary>
            public SqlString Ragszam
            {
                get { return _Ragszam; }
                set { _Ragszam = value; }
            }


            private SqlString _UgyintezesModja = SqlString.Null;

            /// <summary>
            /// UgyintezesModja Base property </summary>
            public SqlString UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }
            }


            private SqlDateTime _VisszaerkezesiHatarido = SqlDateTime.Null;

            /// <summary>
            /// VisszaerkezesiHatarido Base property </summary>
            public SqlDateTime VisszaerkezesiHatarido
            {
                get { return _VisszaerkezesiHatarido; }
                set { _VisszaerkezesiHatarido = value; }
            }


            private SqlString _Visszavarolag = SqlString.Null;

            /// <summary>
            /// Visszavarolag Base property </summary>
            public SqlString Visszavarolag
            {
                get { return _Visszavarolag; }
                set { _Visszavarolag = value; }
            }


            private SqlDateTime _VisszaerkezesDatuma = SqlDateTime.Null;

            /// <summary>
            /// VisszaerkezesDatuma Base property </summary>
            public SqlDateTime VisszaerkezesDatuma
            {
                get { return _VisszaerkezesDatuma; }
                set { _VisszaerkezesDatuma = value; }
            }


            private SqlGuid _Cim_id_Cimzett = SqlGuid.Null;

            /// <summary>
            /// Cim_id_Cimzett Base property </summary>
            public SqlGuid Cim_id_Cimzett
            {
                get { return _Cim_id_Cimzett; }
                set { _Cim_id_Cimzett = value; }
            }


            private SqlGuid _Partner_Id_Cimzett = SqlGuid.Null;

            /// <summary>
            /// Partner_Id_Cimzett Base property </summary>
            public SqlGuid Partner_Id_Cimzett
            {
                get { return _Partner_Id_Cimzett; }
                set { _Partner_Id_Cimzett = value; }
            }

			private SqlGuid _Partner_Id_CimzettKapcsolt = SqlGuid.Null;
           
			/// <summary>
			/// Partner_Id_CimzettKapcsolt Base property </summary>
            public SqlGuid Partner_Id_CimzettKapcsolt
            {
                get { return _Partner_Id_CimzettKapcsolt; }
                set { _Partner_Id_CimzettKapcsolt = value; }                                                        
            }    

            private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private SqlGuid _FelhasznaloCsoport_Id_Orzo = SqlGuid.Null;

            /// <summary>
            /// FelhasznaloCsoport_Id_Orzo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }
            }


            private SqlGuid _Csoport_Id_Felelos_Elozo = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Felelos_Elozo Base property </summary>
            public SqlGuid Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }
            }


            private SqlString _CimSTR_Cimzett = SqlString.Null;

            /// <summary>
            /// CimSTR_Cimzett Base property </summary>
            public SqlString CimSTR_Cimzett
            {
                get { return _CimSTR_Cimzett; }
                set { _CimSTR_Cimzett = value; }
            }


            private SqlString _NevSTR_Cimzett = SqlString.Null;

            /// <summary>
            /// NevSTR_Cimzett Base property </summary>
            public SqlString NevSTR_Cimzett
            {
                get { return _NevSTR_Cimzett; }
                set { _NevSTR_Cimzett = value; }
            }


            private SqlString _Tovabbito = SqlString.Null;

            /// <summary>
            /// Tovabbito Base property </summary>
            public SqlString Tovabbito
            {
                get { return _Tovabbito; }
                set { _Tovabbito = value; }
            }


            private SqlGuid _IraIrat_Id_Kapcsolt = SqlGuid.Null;

            /// <summary>
            /// IraIrat_Id_Kapcsolt Base property </summary>
            public SqlGuid IraIrat_Id_Kapcsolt
            {
                get { return _IraIrat_Id_Kapcsolt; }
                set { _IraIrat_Id_Kapcsolt = value; }
            }


            private SqlString _IrattariHely = SqlString.Null;

            /// <summary>
            /// IrattariHely Base property </summary>
            public SqlString IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }
            }


            private SqlGuid _Gener_Id = SqlGuid.Null;

            /// <summary>
            /// Gener_Id Base property </summary>
            public SqlGuid Gener_Id
            {
                get { return _Gener_Id; }
                set { _Gener_Id = value; }
            }


            private SqlDateTime _PostazasDatuma = SqlDateTime.Null;

            /// <summary>
            /// PostazasDatuma Base property </summary>
            public SqlDateTime PostazasDatuma
            {
                get { return _PostazasDatuma; }
                set { _PostazasDatuma = value; }
            }


            private SqlString _BarCode = SqlString.Null;

            /// <summary>
            /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }
            }


            private SqlString _Allapot = SqlString.Null;

            /// <summary>
            /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private SqlString _Azonosito = SqlString.Null;

            /// <summary>
            /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }
            }


            private SqlGuid _Kovetkezo_Felelos_Id = SqlGuid.Null;

            /// <summary>
            /// Kovetkezo_Felelos_Id Base property </summary>
            public SqlGuid Kovetkezo_Felelos_Id
            {
                get { return _Kovetkezo_Felelos_Id; }
                set { _Kovetkezo_Felelos_Id = value; }
            }


            private SqlString _Elektronikus_Kezbesitesi_Allap = SqlString.Null;

            /// <summary>
            /// Elektronikus_Kezbesitesi_Allap Base property </summary>
            public SqlString Elektronikus_Kezbesitesi_Allap
            {
                get { return _Elektronikus_Kezbesitesi_Allap; }
                set { _Elektronikus_Kezbesitesi_Allap = value; }
            }


            private SqlGuid _Kovetkezo_Orzo_Id = SqlGuid.Null;

            /// <summary>
            /// Kovetkezo_Orzo_Id Base property </summary>
            public SqlGuid Kovetkezo_Orzo_Id
            {
                get { return _Kovetkezo_Orzo_Id; }
                set { _Kovetkezo_Orzo_Id = value; }
            }


            private SqlString _Fizikai_Kezbesitesi_Allapot = SqlString.Null;

            /// <summary>
            /// Fizikai_Kezbesitesi_Allapot Base property </summary>
            public SqlString Fizikai_Kezbesitesi_Allapot
            {
                get { return _Fizikai_Kezbesitesi_Allapot; }
                set { _Fizikai_Kezbesitesi_Allapot = value; }
            }


            private SqlString _TovabbitasAlattAllapot = SqlString.Null;

            /// <summary>
            /// TovabbitasAlattAllapot Base property </summary>
            public SqlString TovabbitasAlattAllapot
            {
                get { return _TovabbitasAlattAllapot; }
                set { _TovabbitasAlattAllapot = value; }
            }


            private SqlString _PostazasAllapot = SqlString.Null;

            /// <summary>
            /// PostazasAllapot Base property </summary>
            public SqlString PostazasAllapot
            {
                get { return _PostazasAllapot; }
                set { _PostazasAllapot = value; }
            }


            private SqlChars _ValaszElektronikus = SqlChars.Null;

            /// <summary>
            /// ValaszElektronikus Base property </summary>
            public SqlChars ValaszElektronikus
            {
                get { return _ValaszElektronikus; }
                set { _ValaszElektronikus = value; }
            }


            private SqlDateTime _SelejtezesDat = SqlDateTime.Null;

            /// <summary>
            /// SelejtezesDat Base property </summary>
            public SqlDateTime SelejtezesDat
            {
                get { return _SelejtezesDat; }
                set { _SelejtezesDat = value; }
            }


            private SqlGuid _FelhCsoport_Id_Selejtezo = SqlGuid.Null;

            /// <summary>
            /// FelhCsoport_Id_Selejtezo Base property </summary>
            public SqlGuid FelhCsoport_Id_Selejtezo
            {
                get { return _FelhCsoport_Id_Selejtezo; }
                set { _FelhCsoport_Id_Selejtezo = value; }
            }


            private SqlString _LeveltariAtvevoNeve = SqlString.Null;

            /// <summary>
            /// LeveltariAtvevoNeve Base property </summary>
            public SqlString LeveltariAtvevoNeve
            {
                get { return _LeveltariAtvevoNeve; }
                set { _LeveltariAtvevoNeve = value; }
            }

 			private SqlGuid _IrattarId = SqlGuid.Null;
           
        /// <summary>
        /// IrattarId Base property </summary>
            public SqlGuid IrattarId
            {
                get { return _IrattarId; }
                set { _IrattarId = value; }                                                        
            }  

            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            private SqlChars _IratPeldanyMegsemmisitve = SqlChars.Null;
            /// <summary>
            /// BLG_2948
            /// LZS
            /// IratPeldanyMegsemmisitve Base property </summary>
            public SqlChars IratPeldanyMegsemmisitve
            {
                get { return _IratPeldanyMegsemmisitve; }
                set { _IratPeldanyMegsemmisitve = value; }
            }


            private SqlDateTime _IratPeldanyMegsemmisitesDatuma = SqlDateTime.Null;
            /// <summary>
            /// BLG_2948
            /// LZS
            /// IratPeldanyMegsemmisitesDatuma Base property </summary>
            public SqlDateTime IratPeldanyMegsemmisitesDatuma
            {
                get { return _IratPeldanyMegsemmisitesDatuma; }
                set { _IratPeldanyMegsemmisitesDatuma = value; }
            }
            
        }
    }
}