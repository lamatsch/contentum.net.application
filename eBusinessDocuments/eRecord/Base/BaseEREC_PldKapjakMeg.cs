
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_PldKapjakMeg BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_PldKapjakMeg
    {
        [System.Xml.Serialization.XmlType("BaseEREC_PldKapjakMegBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _PldIratPeldany_Id = true;
               public bool PldIratPeldany_Id
               {
                   get { return _PldIratPeldany_Id; }
                   set { _PldIratPeldany_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id = true;
               public bool Csoport_Id
               {
                   get { return _Csoport_Id; }
                   set { _Csoport_Id = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   PldIratPeldany_Id = Value;               
                    
                   Csoport_Id = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_PldKapjakMegBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _PldIratPeldany_Id = SqlGuid.Null;
           
        /// <summary>
        /// PldIratPeldany_Id Base property </summary>
            public SqlGuid PldIratPeldany_Id
            {
                get { return _PldIratPeldany_Id; }
                set { _PldIratPeldany_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id Base property </summary>
            public SqlGuid Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}