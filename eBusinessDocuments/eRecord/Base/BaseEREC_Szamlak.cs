
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Szamlak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_Szamlak
    {
        [System.Xml.Serialization.XmlType("BaseEREC_SzamlakBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _Partner_Id_Szallito = true;
               public bool Partner_Id_Szallito
               {
                   get { return _Partner_Id_Szallito; }
                   set { _Partner_Id_Szallito = value; }
               }
                                                            
                                 
               private bool _Cim_Id_Szallito = true;
               public bool Cim_Id_Szallito
               {
                   get { return _Cim_Id_Szallito; }
                   set { _Cim_Id_Szallito = value; }
               }
                                                            
                                 
               private bool _NevSTR_Szallito = true;
               public bool NevSTR_Szallito
               {
                   get { return _NevSTR_Szallito; }
                   set { _NevSTR_Szallito = value; }
               }
                                                            
                                 
               private bool _CimSTR_Szallito = true;
               public bool CimSTR_Szallito
               {
                   get { return _CimSTR_Szallito; }
                   set { _CimSTR_Szallito = value; }
               }
                                                            
                                 
               private bool _Bankszamlaszam_Id = true;
               public bool Bankszamlaszam_Id
               {
                   get { return _Bankszamlaszam_Id; }
                   set { _Bankszamlaszam_Id = value; }
               }
                                                            
                                 
               private bool _SzamlaSorszam = true;
               public bool SzamlaSorszam
               {
                   get { return _SzamlaSorszam; }
                   set { _SzamlaSorszam = value; }
               }
                                                            
                                 
               private bool _FizetesiMod = true;
               public bool FizetesiMod
               {
                   get { return _FizetesiMod; }
                   set { _FizetesiMod = value; }
               }
                                                            
                                 
               private bool _TeljesitesDatuma = true;
               public bool TeljesitesDatuma
               {
                   get { return _TeljesitesDatuma; }
                   set { _TeljesitesDatuma = value; }
               }
                                                            
                                 
               private bool _BizonylatDatuma = true;
               public bool BizonylatDatuma
               {
                   get { return _BizonylatDatuma; }
                   set { _BizonylatDatuma = value; }
               }
                                                            
                                 
               private bool _FizetesiHatarido = true;
               public bool FizetesiHatarido
               {
                   get { return _FizetesiHatarido; }
                   set { _FizetesiHatarido = value; }
               }
                                                            
                                 
               private bool _DevizaKod = true;
               public bool DevizaKod
               {
                   get { return _DevizaKod; }
                   set { _DevizaKod = value; }
               }
                                                            
                                 
               private bool _VisszakuldesDatuma = true;
               public bool VisszakuldesDatuma
               {
                   get { return _VisszakuldesDatuma; }
                   set { _VisszakuldesDatuma = value; }
               }
                                                            
                                 
               private bool _EllenorzoOsszeg = true;
               public bool EllenorzoOsszeg
               {
                   get { return _EllenorzoOsszeg; }
                   set { _EllenorzoOsszeg = value; }
               }
                                                            
                                 
               private bool _PIRAllapot = true;
               public bool PIRAllapot
               {
                   get { return _PIRAllapot; }
                   set { _PIRAllapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }

            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
            private bool _VevoBesorolas = true;
            public bool VevoBesorolas
            {
                get { return _VevoBesorolas; }
                set { _VevoBesorolas = value; }
            }

            public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   Partner_Id_Szallito = Value;               
                    
                   Cim_Id_Szallito = Value;               
                    
                   NevSTR_Szallito = Value;               
                    
                   CimSTR_Szallito = Value;               
                    
                   Bankszamlaszam_Id = Value;               
                    
                   SzamlaSorszam = Value;               
                    
                   FizetesiMod = Value;               
                    
                   TeljesitesDatuma = Value;               
                    
                   BizonylatDatuma = Value;               
                    
                   FizetesiHatarido = Value;               
                    
                   DevizaKod = Value;               
                    
                   VisszakuldesDatuma = Value;               
                    
                   EllenorzoOsszeg = Value;               
                    
                   PIRAllapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;

                // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                VevoBesorolas = Value;
            }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_SzamlakBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id_Szallito = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id_Szallito Base property </summary>
            public SqlGuid Partner_Id_Szallito
            {
                get { return _Partner_Id_Szallito; }
                set { _Partner_Id_Szallito = value; }                                                        
            }        
                   
           
        private SqlGuid _Cim_Id_Szallito = SqlGuid.Null;
           
        /// <summary>
        /// Cim_Id_Szallito Base property </summary>
            public SqlGuid Cim_Id_Szallito
            {
                get { return _Cim_Id_Szallito; }
                set { _Cim_Id_Szallito = value; }                                                        
            }        
                   
           
        private SqlString _NevSTR_Szallito = SqlString.Null;
           
        /// <summary>
        /// NevSTR_Szallito Base property </summary>
            public SqlString NevSTR_Szallito
            {
                get { return _NevSTR_Szallito; }
                set { _NevSTR_Szallito = value; }                                                        
            }        
                   
           
        private SqlString _CimSTR_Szallito = SqlString.Null;
           
        /// <summary>
        /// CimSTR_Szallito Base property </summary>
            public SqlString CimSTR_Szallito
            {
                get { return _CimSTR_Szallito; }
                set { _CimSTR_Szallito = value; }                                                        
            }        
                   
           
        private SqlGuid _Bankszamlaszam_Id = SqlGuid.Null;
           
        /// <summary>
        /// Bankszamlaszam_Id Base property </summary>
            public SqlGuid Bankszamlaszam_Id
            {
                get { return _Bankszamlaszam_Id; }
                set { _Bankszamlaszam_Id = value; }                                                        
            }        
                   
           
        private SqlString _SzamlaSorszam = SqlString.Null;
           
        /// <summary>
        /// SzamlaSorszam Base property </summary>
            public SqlString SzamlaSorszam
            {
                get { return _SzamlaSorszam; }
                set { _SzamlaSorszam = value; }                                                        
            }        
                   
           
        private SqlString _FizetesiMod = SqlString.Null;
           
        /// <summary>
        /// FizetesiMod Base property </summary>
            public SqlString FizetesiMod
            {
                get { return _FizetesiMod; }
                set { _FizetesiMod = value; }                                                        
            }        
                   
           
        private SqlDateTime _TeljesitesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// TeljesitesDatuma Base property </summary>
            public SqlDateTime TeljesitesDatuma
            {
                get { return _TeljesitesDatuma; }
                set { _TeljesitesDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _BizonylatDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// BizonylatDatuma Base property </summary>
            public SqlDateTime BizonylatDatuma
            {
                get { return _BizonylatDatuma; }
                set { _BizonylatDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _FizetesiHatarido = SqlDateTime.Null;
           
        /// <summary>
        /// FizetesiHatarido Base property </summary>
            public SqlDateTime FizetesiHatarido
            {
                get { return _FizetesiHatarido; }
                set { _FizetesiHatarido = value; }                                                        
            }        
                   
           
        private SqlString _DevizaKod = SqlString.Null;
           
        /// <summary>
        /// DevizaKod Base property </summary>
            public SqlString DevizaKod
            {
                get { return _DevizaKod; }
                set { _DevizaKod = value; }                                                        
            }        
                   
           
        private SqlDateTime _VisszakuldesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VisszakuldesDatuma Base property </summary>
            public SqlDateTime VisszakuldesDatuma
            {
                get { return _VisszakuldesDatuma; }
                set { _VisszakuldesDatuma = value; }                                                        
            }        
                   
           
        private SqlDouble _EllenorzoOsszeg = SqlDouble.Null;
           
        /// <summary>
        /// EllenorzoOsszeg Base property </summary>
            public SqlDouble EllenorzoOsszeg
            {
                get { return _EllenorzoOsszeg; }
                set { _EllenorzoOsszeg = value; }                                                        
            }        
                   
           
        private SqlChars _PIRAllapot = SqlChars.Null;
           
        /// <summary>
        /// PIRAllapot Base property </summary>
            public SqlChars PIRAllapot
            {
                get { return _PIRAllapot; }
                set { _PIRAllapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }


        // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
        private SqlString _VevoBesorolas = SqlString.Null;

        /// <summary>
        /// VevoBesorolas Base property </summary>
        public SqlString VevoBesorolas
        {
            get { return _VevoBesorolas; }
            set { _VevoBesorolas = value; }
        }

        }
    }    
}