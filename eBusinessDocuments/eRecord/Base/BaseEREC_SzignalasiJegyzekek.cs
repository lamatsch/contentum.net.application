
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_SzignalasiJegyzekek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_SzignalasiJegyzekek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_SzignalasiJegyzekekBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _UgykorTargykor_Id = true;
            public bool UgykorTargykor_Id
            {
                get { return _UgykorTargykor_Id; }
                set { _UgykorTargykor_Id = value; }
            }


            private bool _Csoport_Id_Felelos = true;
            public bool Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private bool _SzignalasTipusa = true;
            public bool SzignalasTipusa
            {
                get { return _SzignalasTipusa; }
                set { _SzignalasTipusa = value; }
            }


            private bool _SzignalasiKotelezettseg = true;
            public bool SzignalasiKotelezettseg
            {
                get { return _SzignalasiKotelezettseg; }
                set { _SzignalasiKotelezettseg = value; }
            }

            private bool _HataridosFeladatId = true;
            public bool HataridosFeladatId
            {
                get { return _HataridosFeladatId; }
                set { _HataridosFeladatId = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                UgykorTargykor_Id = Value;

                Csoport_Id_Felelos = Value;

                SzignalasTipusa = Value;

                SzignalasiKotelezettseg = Value;

                HataridosFeladatId = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseEREC_SzignalasiJegyzekekBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlGuid _UgykorTargykor_Id = SqlGuid.Null;

            /// <summary>
            /// UgykorTargykor_Id Base property </summary>
            public SqlGuid UgykorTargykor_Id
            {
                get { return _UgykorTargykor_Id; }
                set { _UgykorTargykor_Id = value; }
            }


            private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private SqlString _SzignalasTipusa = SqlString.Null;

            /// <summary>
            /// SzignalasTipusa Base property </summary>
            public SqlString SzignalasTipusa
            {
                get { return _SzignalasTipusa; }
                set { _SzignalasTipusa = value; }
            }


            private SqlString _SzignalasiKotelezettseg = SqlString.Null;

            /// <summary>
            /// SzignalasiKotelezettseg Base property </summary>
            public SqlString SzignalasiKotelezettseg
            {
                get { return _SzignalasiKotelezettseg; }
                set { _SzignalasiKotelezettseg = value; }
            }

            private SqlGuid _HataridosFeladatId = SqlGuid.Null;

            /// <summary>
            /// HataridosFeladatId Base property </summary>
            public SqlGuid HataridosFeladatId
            {
                get { return _HataridosFeladatId; }
                set { _HataridosFeladatId = value; }
            }
        }
    }
}