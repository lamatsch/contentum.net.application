
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TargySzavak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_TargySzavak
    {
        [System.Xml.Serialization.XmlType("BaseEREC_TargySzavakBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _TargySzavak = true;
               public bool TargySzavak
               {
                   get { return _TargySzavak; }
                   set { _TargySzavak = value; }
               }
                                                            
                                 
               private bool _AlapertelmezettErtek = true;
               public bool AlapertelmezettErtek
               {
                   get { return _AlapertelmezettErtek; }
                   set { _AlapertelmezettErtek = value; }
               }
                                                            
                                 
               private bool _BelsoAzonosito = true;
               public bool BelsoAzonosito
               {
                   get { return _BelsoAzonosito; }
                   set { _BelsoAzonosito = value; }
               }
                                                            
                                 
               private bool _SPSSzinkronizalt = true;
               public bool SPSSzinkronizalt
               {
                   get { return _SPSSzinkronizalt; }
                   set { _SPSSzinkronizalt = value; }
               }
                                                            
                                 
               private bool _SPS_Field_Id = true;
               public bool SPS_Field_Id
               {
                   get { return _SPS_Field_Id; }
                   set { _SPS_Field_Id = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Tulaj = true;
               public bool Csoport_Id_Tulaj
               {
                   get { return _Csoport_Id_Tulaj; }
                   set { _Csoport_Id_Tulaj = value; }
               }
                                                            
                                 
               private bool _RegExp = true;
               public bool RegExp
               {
                   get { return _RegExp; }
                   set { _RegExp = value; }
               }
                                                            
                                 
               private bool _ToolTip = true;
               public bool ToolTip
               {
                   get { return _ToolTip; }
                   set { _ToolTip = value; }
               }
                                                            
                                 
               private bool _ControlTypeSource = true;
               public bool ControlTypeSource
               {
                   get { return _ControlTypeSource; }
                   set { _ControlTypeSource = value; }
               }
                                                            
                                 
               private bool _ControlTypeDataSource = true;
               public bool ControlTypeDataSource
               {
                   get { return _ControlTypeDataSource; }
                   set { _ControlTypeDataSource = value; }
               }
                                                            
                                 
               private bool _Targyszo_Id_Parent = true;
               public bool Targyszo_Id_Parent
               {
                   get { return _Targyszo_Id_Parent; }
                   set { _Targyszo_Id_Parent = value; }
               }
                                                            
                                 
               private bool _XSD = true;
               public bool XSD
               {
                   get { return _XSD; }
                   set { _XSD = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Tipus = Value;               
                    
                   TargySzavak = Value;               
                    
                   AlapertelmezettErtek = Value;               
                    
                   BelsoAzonosito = Value;               
                    
                   SPSSzinkronizalt = Value;               
                    
                   SPS_Field_Id = Value;               
                    
                   Csoport_Id_Tulaj = Value;               
                    
                   RegExp = Value;               
                    
                   ToolTip = Value;               
                    
                   ControlTypeSource = Value;               
                    
                   ControlTypeDataSource = Value;               
                    
                   Targyszo_Id_Parent = Value;               
                    
                   XSD = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_TargySzavakBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _TargySzavak = SqlString.Null;
           
        /// <summary>
        /// TargySzavak Base property </summary>
            public SqlString TargySzavak
            {
                get { return _TargySzavak; }
                set { _TargySzavak = value; }                                                        
            }        
                   
           
        private SqlString _AlapertelmezettErtek = SqlString.Null;
           
        /// <summary>
        /// AlapertelmezettErtek Base property </summary>
            public SqlString AlapertelmezettErtek
            {
                get { return _AlapertelmezettErtek; }
                set { _AlapertelmezettErtek = value; }                                                        
            }        
                   
           
        private SqlString _BelsoAzonosito = SqlString.Null;
           
        /// <summary>
        /// BelsoAzonosito Base property </summary>
            public SqlString BelsoAzonosito
            {
                get { return _BelsoAzonosito; }
                set { _BelsoAzonosito = value; }                                                        
            }        
                   
           
        private SqlChars _SPSSzinkronizalt = SqlChars.Null;
           
        /// <summary>
        /// SPSSzinkronizalt Base property </summary>
            public SqlChars SPSSzinkronizalt
            {
                get { return _SPSSzinkronizalt; }
                set { _SPSSzinkronizalt = value; }                                                        
            }        
                   
           
        private SqlGuid _SPS_Field_Id = SqlGuid.Null;
           
        /// <summary>
        /// SPS_Field_Id Base property </summary>
            public SqlGuid SPS_Field_Id
            {
                get { return _SPS_Field_Id; }
                set { _SPS_Field_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Tulaj = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Tulaj Base property </summary>
            public SqlGuid Csoport_Id_Tulaj
            {
                get { return _Csoport_Id_Tulaj; }
                set { _Csoport_Id_Tulaj = value; }                                                        
            }        
                   
           
        private SqlString _RegExp = SqlString.Null;
           
        /// <summary>
        /// RegExp Base property </summary>
            public SqlString RegExp
            {
                get { return _RegExp; }
                set { _RegExp = value; }                                                        
            }        
                   
           
        private SqlString _ToolTip = SqlString.Null;
           
        /// <summary>
        /// ToolTip Base property </summary>
            public SqlString ToolTip
            {
                get { return _ToolTip; }
                set { _ToolTip = value; }                                                        
            }        
                   
           
        private SqlString _ControlTypeSource = SqlString.Null;
           
        /// <summary>
        /// ControlTypeSource Base property </summary>
            public SqlString ControlTypeSource
            {
                get { return _ControlTypeSource; }
                set { _ControlTypeSource = value; }                                                        
            }        
                   
           
        private SqlString _ControlTypeDataSource = SqlString.Null;
           
        /// <summary>
        /// ControlTypeDataSource Base property </summary>
            public SqlString ControlTypeDataSource
            {
                get { return _ControlTypeDataSource; }
                set { _ControlTypeDataSource = value; }                                                        
            }        
                   
           
        private SqlGuid _Targyszo_Id_Parent = SqlGuid.Null;
           
        /// <summary>
        /// Targyszo_Id_Parent Base property </summary>
            public SqlGuid Targyszo_Id_Parent
            {
                get { return _Targyszo_Id_Parent; }
                set { _Targyszo_Id_Parent = value; }                                                        
            }        
                   
           
        private SqlXml _XSD = SqlXml.Null;
           
        /// <summary>
        /// XSD Base property </summary>
            public SqlXml XSD
            {
                get { return _XSD; }
                set { _XSD = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}