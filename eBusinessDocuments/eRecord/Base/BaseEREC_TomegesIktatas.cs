
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
	/// <summary>
	/// EREC_TomegesIktatas BusinessDocument Class </summary>
	[Serializable ( )]
	public class BaseEREC_TomegesIktatas
	{
		[System.Xml.Serialization.XmlType ( "BaseEREC_TomegesIktatasBaseUpdated" )]
		public class BaseUpdated
		{

			private bool _Id = true;
			public bool Id
			{
				get { return _Id; }
				set { _Id = value; }
			}


			private bool _ForrasTipusNev = true;
			public bool ForrasTipusNev
			{
				get { return _ForrasTipusNev; }
				set { _ForrasTipusNev = value; }
			}


			private bool _FelelosCsoport_Id = true;
			public bool FelelosCsoport_Id
			{
				get { return _FelelosCsoport_Id; }
				set { _FelelosCsoport_Id = value; }
			}


			private bool _AgazatiJelek_Id = true;
			public bool AgazatiJelek_Id
			{
				get { return _AgazatiJelek_Id; }
				set { _AgazatiJelek_Id = value; }
			}


			private bool _IraIrattariTetelek_Id = true;
			public bool IraIrattariTetelek_Id
			{
				get { return _IraIrattariTetelek_Id; }
				set { _IraIrattariTetelek_Id = value; }
			}


			private bool _Ugytipus_Id = true;
			public bool Ugytipus_Id
			{
				get { return _Ugytipus_Id; }
				set { _Ugytipus_Id = value; }
			}


			private bool _Irattipus_Id = true;
			public bool Irattipus_Id
			{
				get { return _Irattipus_Id; }
				set { _Irattipus_Id = value; }
			}


			private bool _TargyPrefix = true;
			public bool TargyPrefix
			{
				get { return _TargyPrefix; }
				set { _TargyPrefix = value; }
			}


			private bool _AlairasKell = true;
			public bool AlairasKell
			{
				get { return _AlairasKell; }
				set { _AlairasKell = value; }
			}


			private bool _AlairasMod = true;
			public bool AlairasMod
			{
				get { return _AlairasMod; }
				set { _AlairasMod = value; }
			}


			private bool _FelhasznaloCsoport_Id_Alairo = true;
			public bool FelhasznaloCsoport_Id_Alairo
			{
				get { return _FelhasznaloCsoport_Id_Alairo; }
				set { _FelhasznaloCsoport_Id_Alairo = value; }
			}


			private bool _FelhaszCsoport_Id_Helyettesito = true;
			public bool FelhaszCsoport_Id_Helyettesito
			{
				get { return _FelhaszCsoport_Id_Helyettesito; }
				set { _FelhaszCsoport_Id_Helyettesito = value; }
			}


			private bool _AlairasSzabaly_Id = true;
			public bool AlairasSzabaly_Id
			{
				get { return _AlairasSzabaly_Id; }
				set { _AlairasSzabaly_Id = value; }
			}


			private bool _HatosagiAdatlapKell = true;
			public bool HatosagiAdatlapKell
			{
				get { return _HatosagiAdatlapKell; }
				set { _HatosagiAdatlapKell = value; }
			}


			private bool _UgyFajtaja = true;
			public bool UgyFajtaja
			{
				get { return _UgyFajtaja; }
				set { _UgyFajtaja = value; }
			}


			private bool _DontestHozta = true;
			public bool DontestHozta
			{
				get { return _DontestHozta; }
				set { _DontestHozta = value; }
			}


			private bool _DontesFormaja = true;
			public bool DontesFormaja
			{
				get { return _DontesFormaja; }
				set { _DontesFormaja = value; }
			}


			private bool _UgyintezesHataridore = true;
			public bool UgyintezesHataridore
			{
				get { return _UgyintezesHataridore; }
				set { _UgyintezesHataridore = value; }
			}


			private bool _HataridoTullepes = true;
			public bool HataridoTullepes
			{
				get { return _HataridoTullepes; }
				set { _HataridoTullepes = value; }
			}


			private bool _HatosagiEllenorzes = true;
			public bool HatosagiEllenorzes
			{
				get { return _HatosagiEllenorzes; }
				set { _HatosagiEllenorzes = value; }
			}


			private bool _MunkaorakSzama = true;
			public bool MunkaorakSzama
			{
				get { return _MunkaorakSzama; }
				set { _MunkaorakSzama = value; }
			}


			private bool _EljarasiKoltseg = true;
			public bool EljarasiKoltseg
			{
				get { return _EljarasiKoltseg; }
				set { _EljarasiKoltseg = value; }
			}


			private bool _KozigazgatasiBirsagMerteke = true;
			public bool KozigazgatasiBirsagMerteke
			{
				get { return _KozigazgatasiBirsagMerteke; }
				set { _KozigazgatasiBirsagMerteke = value; }
			}


			private bool _SommasEljDontes = true;
			public bool SommasEljDontes
			{
				get { return _SommasEljDontes; }
				set { _SommasEljDontes = value; }
			}


			private bool _NyolcNapBelulNemSommas = true;
			public bool NyolcNapBelulNemSommas
			{
				get { return _NyolcNapBelulNemSommas; }
				set { _NyolcNapBelulNemSommas = value; }
			}


			private bool _FuggoHatalyuHatarozat = true;
			public bool FuggoHatalyuHatarozat
			{
				get { return _FuggoHatalyuHatarozat; }
				set { _FuggoHatalyuHatarozat = value; }
			}


			private bool _FuggoHatalyuVegzes = true;
			public bool FuggoHatalyuVegzes
			{
				get { return _FuggoHatalyuVegzes; }
				set { _FuggoHatalyuVegzes = value; }
			}


			private bool _HatAltalVisszafizOsszeg = true;
			public bool HatAltalVisszafizOsszeg
			{
				get { return _HatAltalVisszafizOsszeg; }
				set { _HatAltalVisszafizOsszeg = value; }
			}


			private bool _HatTerheloEljKtsg = true;
			public bool HatTerheloEljKtsg
			{
				get { return _HatTerheloEljKtsg; }
				set { _HatTerheloEljKtsg = value; }
			}


			private bool _FelfuggHatarozat = true;
			public bool FelfuggHatarozat
			{
				get { return _FelfuggHatarozat; }
				set { _FelfuggHatarozat = value; }
			}


			private bool _ErvKezd = true;
			public bool ErvKezd
			{
				get { return _ErvKezd; }
				set { _ErvKezd = value; }
			}


			private bool _ErvVege = true;
			public bool ErvVege
			{
				get { return _ErvVege; }
				set { _ErvVege = value; }
			}

			private bool _AutoExpedialasKell = true;
			public bool AutoExpedialasKell
			{
				get { return _AutoExpedialasKell; }
				set { _AutoExpedialasKell = value; }
			}

			private bool _UgyintezesAlapja = true;
			public bool UgyintezesAlapja
			{
				get { return _UgyintezesAlapja; }
				set { _UgyintezesAlapja = value; }
			}

			private bool _AdathordozoTipusa = true;
			public bool AdathordozoTipusa
			{
				get { return _AdathordozoTipusa; }
				set { _AdathordozoTipusa = value; }
			}

			private bool _ExpedialasModja = true;
			public bool ExpedialasModja
			{
				get { return _ExpedialasModja; }
				set { _ExpedialasModja = value; }
			}

			public void SetValueAll ( bool Value )
			{

				Id = Value;

				ForrasTipusNev = Value;

				FelelosCsoport_Id = Value;

				AgazatiJelek_Id = Value;

				IraIrattariTetelek_Id = Value;

				Ugytipus_Id = Value;

				Irattipus_Id = Value;

				TargyPrefix = Value;

				AlairasKell = Value;

				AlairasMod = Value;

				FelhasznaloCsoport_Id_Alairo = Value;

				FelhaszCsoport_Id_Helyettesito = Value;

				AlairasSzabaly_Id = Value;

				HatosagiAdatlapKell = Value;

				UgyFajtaja = Value;

				DontestHozta = Value;

				DontesFormaja = Value;

				UgyintezesHataridore = Value;

				HataridoTullepes = Value;

				HatosagiEllenorzes = Value;

				MunkaorakSzama = Value;

				EljarasiKoltseg = Value;

				KozigazgatasiBirsagMerteke = Value;

				SommasEljDontes = Value;

				NyolcNapBelulNemSommas = Value;

				FuggoHatalyuHatarozat = Value;

				FuggoHatalyuVegzes = Value;

				HatAltalVisszafizOsszeg = Value;

				HatTerheloEljKtsg = Value;

				FelfuggHatarozat = Value;

				ErvKezd = Value;

				ErvVege = Value;

				AutoExpedialasKell = Value;

				UgyintezesAlapja = Value;

				AdathordozoTipusa = Value;

				ExpedialasModja = Value;
			}

		}

		[System.Xml.Serialization.XmlType ( "BaseEREC_TomegesIktatasBaseTyped" )]
		public class BaseTyped
		{

			private SqlGuid _Id = SqlGuid.Null;

			/// <summary>
			/// Id Base property </summary>
			public SqlGuid Id
			{
				get { return _Id; }
				set { _Id = value; }
			}


			private SqlString _ForrasTipusNev = SqlString.Null;

			/// <summary>
			/// ForrasTipusNev Base property </summary>
			public SqlString ForrasTipusNev
			{
				get { return _ForrasTipusNev; }
				set { _ForrasTipusNev = value; }
			}


			private SqlGuid _FelelosCsoport_Id = SqlGuid.Null;

			/// <summary>
			/// FelelosCsoport_Id Base property </summary>
			public SqlGuid FelelosCsoport_Id
			{
				get { return _FelelosCsoport_Id; }
				set { _FelelosCsoport_Id = value; }
			}


			private SqlGuid _AgazatiJelek_Id = SqlGuid.Null;

			/// <summary>
			/// AgazatiJelek_Id Base property </summary>
			public SqlGuid AgazatiJelek_Id
			{
				get { return _AgazatiJelek_Id; }
				set { _AgazatiJelek_Id = value; }
			}


			private SqlGuid _IraIrattariTetelek_Id = SqlGuid.Null;

			/// <summary>
			/// IraIrattariTetelek_Id Base property </summary>
			public SqlGuid IraIrattariTetelek_Id
			{
				get { return _IraIrattariTetelek_Id; }
				set { _IraIrattariTetelek_Id = value; }
			}


			private SqlGuid _Ugytipus_Id = SqlGuid.Null;

			/// <summary>
			/// Ugytipus_Id Base property </summary>
			public SqlGuid Ugytipus_Id
			{
				get { return _Ugytipus_Id; }
				set { _Ugytipus_Id = value; }
			}


			private SqlGuid _Irattipus_Id = SqlGuid.Null;

			/// <summary>
			/// Irattipus_Id Base property </summary>
			public SqlGuid Irattipus_Id
			{
				get { return _Irattipus_Id; }
				set { _Irattipus_Id = value; }
			}


			private SqlString _TargyPrefix = SqlString.Null;

			/// <summary>
			/// TargyPrefix Base property </summary>
			public SqlString TargyPrefix
			{
				get { return _TargyPrefix; }
				set { _TargyPrefix = value; }
			}


			private SqlChars _AlairasKell = SqlChars.Null;

			/// <summary>
			/// AlairasKell Base property </summary>
			public SqlChars AlairasKell
			{
				get { return _AlairasKell; }
				set { _AlairasKell = value; }
			}


			private SqlGuid _AlairasMod = SqlGuid.Null;

			/// <summary>
			/// AlairasMod Base property </summary>
			public SqlGuid AlairasMod
			{
				get { return _AlairasMod; }
				set { _AlairasMod = value; }
			}


			private SqlGuid _FelhasznaloCsoport_Id_Alairo = SqlGuid.Null;

			/// <summary>
			/// FelhasznaloCsoport_Id_Alairo Base property </summary>
			public SqlGuid FelhasznaloCsoport_Id_Alairo
			{
				get { return _FelhasznaloCsoport_Id_Alairo; }
				set { _FelhasznaloCsoport_Id_Alairo = value; }
			}


			private SqlGuid _FelhaszCsoport_Id_Helyettesito = SqlGuid.Null;

			/// <summary>
			/// FelhaszCsoport_Id_Helyettesito Base property </summary>
			public SqlGuid FelhaszCsoport_Id_Helyettesito
			{
				get { return _FelhaszCsoport_Id_Helyettesito; }
				set { _FelhaszCsoport_Id_Helyettesito = value; }
			}


			private SqlGuid _AlairasSzabaly_Id = SqlGuid.Null;

			/// <summary>
			/// AlairasSzabaly_Id Base property </summary>
			public SqlGuid AlairasSzabaly_Id
			{
				get { return _AlairasSzabaly_Id; }
				set { _AlairasSzabaly_Id = value; }
			}


			private SqlChars _HatosagiAdatlapKell = SqlChars.Null;

			/// <summary>
			/// HatosagiAdatlapKell Base property </summary>
			public SqlChars HatosagiAdatlapKell
			{
				get { return _HatosagiAdatlapKell; }
				set { _HatosagiAdatlapKell = value; }
			}


			private SqlString _UgyFajtaja = SqlString.Null;

			/// <summary>
			/// UgyFajtaja Base property </summary>
			public SqlString UgyFajtaja
			{
				get { return _UgyFajtaja; }
				set { _UgyFajtaja = value; }
			}


			private SqlString _DontestHozta = SqlString.Null;

			/// <summary>
			/// DontestHozta Base property </summary>
			public SqlString DontestHozta
			{
				get { return _DontestHozta; }
				set { _DontestHozta = value; }
			}


			private SqlString _DontesFormaja = SqlString.Null;

			/// <summary>
			/// DontesFormaja Base property </summary>
			public SqlString DontesFormaja
			{
				get { return _DontesFormaja; }
				set { _DontesFormaja = value; }
			}


			private SqlString _UgyintezesHataridore = SqlString.Null;

			/// <summary>
			/// UgyintezesHataridore Base property </summary>
			public SqlString UgyintezesHataridore
			{
				get { return _UgyintezesHataridore; }
				set { _UgyintezesHataridore = value; }
			}


			private SqlInt32 _HataridoTullepes = SqlInt32.Null;

			/// <summary>
			/// HataridoTullepes Base property </summary>
			public SqlInt32 HataridoTullepes
			{
				get { return _HataridoTullepes; }
				set { _HataridoTullepes = value; }
			}


			private SqlChars _HatosagiEllenorzes = SqlChars.Null;

			/// <summary>
			/// HatosagiEllenorzes Base property </summary>
			public SqlChars HatosagiEllenorzes
			{
				get { return _HatosagiEllenorzes; }
				set { _HatosagiEllenorzes = value; }
			}


			private SqlDouble _MunkaorakSzama = SqlDouble.Null;

			/// <summary>
			/// MunkaorakSzama Base property </summary>
			public SqlDouble MunkaorakSzama
			{
				get { return _MunkaorakSzama; }
				set { _MunkaorakSzama = value; }
			}


			private SqlInt32 _EljarasiKoltseg = SqlInt32.Null;

			/// <summary>
			/// EljarasiKoltseg Base property </summary>
			public SqlInt32 EljarasiKoltseg
			{
				get { return _EljarasiKoltseg; }
				set { _EljarasiKoltseg = value; }
			}


			private SqlInt32 _KozigazgatasiBirsagMerteke = SqlInt32.Null;

			/// <summary>
			/// KozigazgatasiBirsagMerteke Base property </summary>
			public SqlInt32 KozigazgatasiBirsagMerteke
			{
				get { return _KozigazgatasiBirsagMerteke; }
				set { _KozigazgatasiBirsagMerteke = value; }
			}


			private SqlString _SommasEljDontes = SqlString.Null;

			/// <summary>
			/// SommasEljDontes Base property </summary>
			public SqlString SommasEljDontes
			{
				get { return _SommasEljDontes; }
				set { _SommasEljDontes = value; }
			}


			private SqlString _NyolcNapBelulNemSommas = SqlString.Null;

			/// <summary>
			/// NyolcNapBelulNemSommas Base property </summary>
			public SqlString NyolcNapBelulNemSommas
			{
				get { return _NyolcNapBelulNemSommas; }
				set { _NyolcNapBelulNemSommas = value; }
			}


			private SqlString _FuggoHatalyuHatarozat = SqlString.Null;

			/// <summary>
			/// FuggoHatalyuHatarozat Base property </summary>
			public SqlString FuggoHatalyuHatarozat
			{
				get { return _FuggoHatalyuHatarozat; }
				set { _FuggoHatalyuHatarozat = value; }
			}


			private SqlString _FuggoHatalyuVegzes = SqlString.Null;

			/// <summary>
			/// FuggoHatalyuVegzes Base property </summary>
			public SqlString FuggoHatalyuVegzes
			{
				get { return _FuggoHatalyuVegzes; }
				set { _FuggoHatalyuVegzes = value; }
			}


			private SqlInt32 _HatAltalVisszafizOsszeg = SqlInt32.Null;

			/// <summary>
			/// HatAltalVisszafizOsszeg Base property </summary>
			public SqlInt32 HatAltalVisszafizOsszeg
			{
				get { return _HatAltalVisszafizOsszeg; }
				set { _HatAltalVisszafizOsszeg = value; }
			}


			private SqlInt32 _HatTerheloEljKtsg = SqlInt32.Null;

			/// <summary>
			/// HatTerheloEljKtsg Base property </summary>
			public SqlInt32 HatTerheloEljKtsg
			{
				get { return _HatTerheloEljKtsg; }
				set { _HatTerheloEljKtsg = value; }
			}


			private SqlString _FelfuggHatarozat = SqlString.Null;

			/// <summary>
			/// FelfuggHatarozat Base property </summary>
			public SqlString FelfuggHatarozat
			{
				get { return _FelfuggHatarozat; }
				set { _FelfuggHatarozat = value; }
			}


			private SqlDateTime _ErvKezd = SqlDateTime.Null;

			/// <summary>
			/// ErvKezd Base property </summary>
			public SqlDateTime ErvKezd
			{
				get { return _ErvKezd; }
				set { _ErvKezd = value; }
			}


			private SqlDateTime _ErvVege = SqlDateTime.Null;

			/// <summary>
			/// ErvVege Base property </summary>
			public SqlDateTime ErvVege
			{
				get { return _ErvVege; }
				set { _ErvVege = value; }
			}

			private SqlChars _AutoExpedialasKell = SqlChars.Null;

			/// <summary>
			/// AutoExpedialasKell Base property </summary>
			public SqlChars AutoExpedialasKell
			{
				get { return _AutoExpedialasKell; }
				set { _AutoExpedialasKell = value; }
			}

			private SqlString _ExpedialasModja = SqlString.Null;

			/// <summary>
			/// ExpedialasModja Base property </summary>
			public SqlString ExpedialasModja
			{
				get { return _ExpedialasModja; }
				set { _ExpedialasModja = value; }
			}

			private SqlString _UgyintezesAlapja = SqlString.Null;

			/// <summary>
			/// UgyintezesAlapja Base property </summary>
			public SqlString UgyintezesAlapja
			{
				get { return _UgyintezesAlapja; }
				set { _UgyintezesAlapja = value; }
			}


			private SqlString _AdathordozoTipusa = SqlString.Null;

			/// <summary>
			/// AdathordozoTipusa Base property </summary>
			public SqlString AdathordozoTipusa
			{
				get { return _AdathordozoTipusa; }
				set { _AdathordozoTipusa = value; }
			}

		}
	}
}