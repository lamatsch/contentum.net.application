
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TomegesIktatasFolyamat BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_TomegesIktatasFolyamat
    {
        [System.Xml.Serialization.XmlType("BaseEREC_TomegesIktatasFolyamatBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Forras = true;
               public bool Forras
               {
                   get { return _Forras; }
                   set { _Forras = value; }
               }
                                                            
                                 
               private bool _Forras_Azonosito = true;
               public bool Forras_Azonosito
               {
                   get { return _Forras_Azonosito; }
                   set { _Forras_Azonosito = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }
                                                            
                                 
               private bool _RQ_Dokumentum_Id = true;
               public bool RQ_Dokumentum_Id
               {
                   get { return _RQ_Dokumentum_Id; }
                   set { _RQ_Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _RS_Dokumentum_Id = true;
               public bool RS_Dokumentum_Id
               {
                   get { return _RS_Dokumentum_Id; }
                   set { _RS_Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Forras = Value;               
                    
                   Forras_Azonosito = Value;               
                    
                   Prioritas = Value;               
                    
                   RQ_Dokumentum_Id = Value;               
                    
                   RS_Dokumentum_Id = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_TomegesIktatasFolyamatBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Forras = SqlString.Null;
           
        /// <summary>
        /// Forras Base property </summary>
            public SqlString Forras
            {
                get { return _Forras; }
                set { _Forras = value; }                                                        
            }        
                   
           
        private SqlString _Forras_Azonosito = SqlString.Null;
           
        /// <summary>
        /// Forras_Azonosito Base property </summary>
            public SqlString Forras_Azonosito
            {
                get { return _Forras_Azonosito; }
                set { _Forras_Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _Prioritas = SqlString.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlString Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }        
                   
           
        private SqlGuid _RQ_Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// RQ_Dokumentum_Id Base property </summary>
            public SqlGuid RQ_Dokumentum_Id
            {
                get { return _RQ_Dokumentum_Id; }
                set { _RQ_Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _RS_Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// RS_Dokumentum_Id Base property </summary>
            public SqlGuid RS_Dokumentum_Id
            {
                get { return _RS_Dokumentum_Id; }
                set { _RS_Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}