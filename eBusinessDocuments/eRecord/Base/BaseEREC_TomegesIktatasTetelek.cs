
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TomegesIktatasTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_TomegesIktatasTetelek
    {
        [System.Xml.Serialization.XmlType("BaseEREC_TomegesIktatasTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Folyamat_Id = true;
               public bool Folyamat_Id
               {
                   get { return _Folyamat_Id; }
                   set { _Folyamat_Id = value; }
               }
                                                            
                                 
               private bool _Forras_Azonosito = true;
               public bool Forras_Azonosito
               {
                   get { return _Forras_Azonosito; }
                   set { _Forras_Azonosito = value; }
               }
                                                            
                                 
               private bool _IktatasTipus = true;
               public bool IktatasTipus
               {
                   get { return _IktatasTipus; }
                   set { _IktatasTipus = value; }
               }
                                                            
                                 
               private bool _Alszamra = true;
               public bool Alszamra
               {
                   get { return _Alszamra; }
                   set { _Alszamra = value; }
               }
                                                            
                                 
               private bool _Iktatokonyv_Id = true;
               public bool Iktatokonyv_Id
               {
                   get { return _Iktatokonyv_Id; }
                   set { _Iktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _Ugyirat_Id = true;
               public bool Ugyirat_Id
               {
                   get { return _Ugyirat_Id; }
                   set { _Ugyirat_Id = value; }
               }
                                                            
                                 
               private bool _Kuldemeny_Id = true;
               public bool Kuldemeny_Id
               {
                   get { return _Kuldemeny_Id; }
                   set { _Kuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _ExecParam = true;
               public bool ExecParam
               {
                   get { return _ExecParam; }
                   set { _ExecParam = value; }
               }
                                                            
                                 
               private bool _EREC_UgyUgyiratok = true;
               public bool EREC_UgyUgyiratok
               {
                   get { return _EREC_UgyUgyiratok; }
                   set { _EREC_UgyUgyiratok = value; }
               }
                                                            
                                 
               private bool _EREC_UgyUgyiratdarabok = true;
               public bool EREC_UgyUgyiratdarabok
               {
                   get { return _EREC_UgyUgyiratdarabok; }
                   set { _EREC_UgyUgyiratdarabok = value; }
               }
                                                            
                                 
               private bool _EREC_IraIratok = true;
               public bool EREC_IraIratok
               {
                   get { return _EREC_IraIratok; }
                   set { _EREC_IraIratok = value; }
               }
                                                            
                                 
               private bool _EREC_PldIratPeldanyok = true;
               public bool EREC_PldIratPeldanyok
               {
                   get { return _EREC_PldIratPeldanyok; }
                   set { _EREC_PldIratPeldanyok = value; }
               }
                                                            
                                 
               private bool _EREC_HataridosFeladatok = true;
               public bool EREC_HataridosFeladatok
               {
                   get { return _EREC_HataridosFeladatok; }
                   set { _EREC_HataridosFeladatok = value; }
               }
                                                            
                                 
               private bool _EREC_KuldKuldemenyek = true;
               public bool EREC_KuldKuldemenyek
               {
                   get { return _EREC_KuldKuldemenyek; }
                   set { _EREC_KuldKuldemenyek = value; }
               }
                                                            
                                 
               private bool _IktatasiParameterek = true;
               public bool IktatasiParameterek
               {
                   get { return _IktatasiParameterek; }
                   set { _IktatasiParameterek = value; }
               }
                                                            
                                 
               private bool _ErkeztetesParameterek = true;
               public bool ErkeztetesParameterek
               {
                   get { return _ErkeztetesParameterek; }
                   set { _ErkeztetesParameterek = value; }
               }
                                                            
                                 
               private bool _Dokumentumok = true;
               public bool Dokumentumok
               {
                   get { return _Dokumentumok; }
                   set { _Dokumentumok = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Irat_Id = true;
               public bool Irat_Id
               {
                   get { return _Irat_Id; }
                   set { _Irat_Id = value; }
               }
                                                            
                                 
               private bool _Hiba = true;
               public bool Hiba
               {
                   get { return _Hiba; }
                   set { _Hiba = value; }
               }
                                                            
                                 
               private bool _Expedialas = true;
               public bool Expedialas
               {
                   get { return _Expedialas; }
                   set { _Expedialas = value; }
               }
                                                            
                                 
               private bool _TobbIratEgyKuldemenybe = true;
               public bool TobbIratEgyKuldemenybe
               {
                   get { return _TobbIratEgyKuldemenybe; }
                   set { _TobbIratEgyKuldemenybe = value; }
               }
                                                            
                                 
               private bool _KuldemenyAtadas = true;
               public bool KuldemenyAtadas
               {
                   get { return _KuldemenyAtadas; }
                   set { _KuldemenyAtadas = value; }
               }
                                                            
                                 
               private bool _HatosagiStatAdat = true;
               public bool HatosagiStatAdat
               {
                   get { return _HatosagiStatAdat; }
                   set { _HatosagiStatAdat = value; }
               }
                                                            
                                 
               private bool _EREC_IratAlairok = true;
               public bool EREC_IratAlairok
               {
                   get { return _EREC_IratAlairok; }
                   set { _EREC_IratAlairok = value; }
               }
                                                            
                                 
               private bool _Lezarhato = true;
               public bool Lezarhato
               {
                   get { return _Lezarhato; }
                   set { _Lezarhato = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Folyamat_Id = Value;               
                    
                   Forras_Azonosito = Value;               
                    
                   IktatasTipus = Value;               
                    
                   Alszamra = Value;               
                    
                   Iktatokonyv_Id = Value;               
                    
                   Ugyirat_Id = Value;               
                    
                   Kuldemeny_Id = Value;               
                    
                   ExecParam = Value;               
                    
                   EREC_UgyUgyiratok = Value;               
                    
                   EREC_UgyUgyiratdarabok = Value;               
                    
                   EREC_IraIratok = Value;               
                    
                   EREC_PldIratPeldanyok = Value;               
                    
                   EREC_HataridosFeladatok = Value;               
                    
                   EREC_KuldKuldemenyek = Value;               
                    
                   IktatasiParameterek = Value;               
                    
                   ErkeztetesParameterek = Value;               
                    
                   Dokumentumok = Value;               
                    
                   Azonosito = Value;               
                    
                   Allapot = Value;               
                    
                   Irat_Id = Value;               
                    
                   Hiba = Value;               
                    
                   Expedialas = Value;               
                    
                   TobbIratEgyKuldemenybe = Value;               
                    
                   KuldemenyAtadas = Value;               
                    
                   HatosagiStatAdat = Value;               
                    
                   EREC_IratAlairok = Value;               
                    
                   Lezarhato = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_TomegesIktatasTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Folyamat_Id = SqlGuid.Null;
           
        /// <summary>
        /// Folyamat_Id Base property </summary>
            public SqlGuid Folyamat_Id
            {
                get { return _Folyamat_Id; }
                set { _Folyamat_Id = value; }                                                        
            }        
                   
           
        private SqlString _Forras_Azonosito = SqlString.Null;
           
        /// <summary>
        /// Forras_Azonosito Base property </summary>
            public SqlString Forras_Azonosito
            {
                get { return _Forras_Azonosito; }
                set { _Forras_Azonosito = value; }                                                        
            }        
                   
           
        private SqlInt32 _IktatasTipus = SqlInt32.Null;
           
        /// <summary>
        /// IktatasTipus Base property </summary>
            public SqlInt32 IktatasTipus
            {
                get { return _IktatasTipus; }
                set { _IktatasTipus = value; }                                                        
            }        
                   
           
        private SqlInt32 _Alszamra = SqlInt32.Null;
           
        /// <summary>
        /// Alszamra Base property </summary>
            public SqlInt32 Alszamra
            {
                get { return _Alszamra; }
                set { _Alszamra = value; }                                                        
            }        
                   
           
        private SqlGuid _Iktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// Iktatokonyv_Id Base property </summary>
            public SqlGuid Iktatokonyv_Id
            {
                get { return _Iktatokonyv_Id; }
                set { _Iktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Ugyirat_Id = SqlGuid.Null;
           
        /// <summary>
        /// Ugyirat_Id Base property </summary>
            public SqlGuid Ugyirat_Id
            {
                get { return _Ugyirat_Id; }
                set { _Ugyirat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Kuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// Kuldemeny_Id Base property </summary>
            public SqlGuid Kuldemeny_Id
            {
                get { return _Kuldemeny_Id; }
                set { _Kuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlString _ExecParam = SqlString.Null;
           
        /// <summary>
        /// ExecParam Base property </summary>
            public SqlString ExecParam
            {
                get { return _ExecParam; }
                set { _ExecParam = value; }                                                        
            }        
                   
           
        private SqlString _EREC_UgyUgyiratok = SqlString.Null;
           
        /// <summary>
        /// EREC_UgyUgyiratok Base property </summary>
            public SqlString EREC_UgyUgyiratok
            {
                get { return _EREC_UgyUgyiratok; }
                set { _EREC_UgyUgyiratok = value; }                                                        
            }        
                   
           
        private SqlString _EREC_UgyUgyiratdarabok = SqlString.Null;
           
        /// <summary>
        /// EREC_UgyUgyiratdarabok Base property </summary>
            public SqlString EREC_UgyUgyiratdarabok
            {
                get { return _EREC_UgyUgyiratdarabok; }
                set { _EREC_UgyUgyiratdarabok = value; }                                                        
            }        
                   
           
        private SqlString _EREC_IraIratok = SqlString.Null;
           
        /// <summary>
        /// EREC_IraIratok Base property </summary>
            public SqlString EREC_IraIratok
            {
                get { return _EREC_IraIratok; }
                set { _EREC_IraIratok = value; }                                                        
            }        
                   
           
        private SqlString _EREC_PldIratPeldanyok = SqlString.Null;
           
        /// <summary>
        /// EREC_PldIratPeldanyok Base property </summary>
            public SqlString EREC_PldIratPeldanyok
            {
                get { return _EREC_PldIratPeldanyok; }
                set { _EREC_PldIratPeldanyok = value; }                                                        
            }        
                   
           
        private SqlString _EREC_HataridosFeladatok = SqlString.Null;
           
        /// <summary>
        /// EREC_HataridosFeladatok Base property </summary>
            public SqlString EREC_HataridosFeladatok
            {
                get { return _EREC_HataridosFeladatok; }
                set { _EREC_HataridosFeladatok = value; }                                                        
            }        
                   
           
        private SqlString _EREC_KuldKuldemenyek = SqlString.Null;
           
        /// <summary>
        /// EREC_KuldKuldemenyek Base property </summary>
            public SqlString EREC_KuldKuldemenyek
            {
                get { return _EREC_KuldKuldemenyek; }
                set { _EREC_KuldKuldemenyek = value; }                                                        
            }        
                   
           
        private SqlString _IktatasiParameterek = SqlString.Null;
           
        /// <summary>
        /// IktatasiParameterek Base property </summary>
            public SqlString IktatasiParameterek
            {
                get { return _IktatasiParameterek; }
                set { _IktatasiParameterek = value; }                                                        
            }        
                   
           
        private SqlString _ErkeztetesParameterek = SqlString.Null;
           
        /// <summary>
        /// ErkeztetesParameterek Base property </summary>
            public SqlString ErkeztetesParameterek
            {
                get { return _ErkeztetesParameterek; }
                set { _ErkeztetesParameterek = value; }                                                        
            }        
                   
           
        private SqlString _Dokumentumok = SqlString.Null;
           
        /// <summary>
        /// Dokumentumok Base property </summary>
            public SqlString Dokumentumok
            {
                get { return _Dokumentumok; }
                set { _Dokumentumok = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlGuid _Irat_Id = SqlGuid.Null;
           
        /// <summary>
        /// Irat_Id Base property </summary>
            public SqlGuid Irat_Id
            {
                get { return _Irat_Id; }
                set { _Irat_Id = value; }                                                        
            }        
                   
           
        private SqlString _Hiba = SqlString.Null;
           
        /// <summary>
        /// Hiba Base property </summary>
            public SqlString Hiba
            {
                get { return _Hiba; }
                set { _Hiba = value; }                                                        
            }        
                   
           
        private SqlInt32 _Expedialas = SqlInt32.Null;
           
        /// <summary>
        /// Expedialas Base property </summary>
            public SqlInt32 Expedialas
            {
                get { return _Expedialas; }
                set { _Expedialas = value; }                                                        
            }        
                   
           
        private SqlInt32 _TobbIratEgyKuldemenybe = SqlInt32.Null;
           
        /// <summary>
        /// TobbIratEgyKuldemenybe Base property </summary>
            public SqlInt32 TobbIratEgyKuldemenybe
            {
                get { return _TobbIratEgyKuldemenybe; }
                set { _TobbIratEgyKuldemenybe = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldemenyAtadas = SqlGuid.Null;
           
        /// <summary>
        /// KuldemenyAtadas Base property </summary>
            public SqlGuid KuldemenyAtadas
            {
                get { return _KuldemenyAtadas; }
                set { _KuldemenyAtadas = value; }                                                        
            }        
                   
           
        private SqlString _HatosagiStatAdat = SqlString.Null;
           
        /// <summary>
        /// HatosagiStatAdat Base property </summary>
            public SqlString HatosagiStatAdat
            {
                get { return _HatosagiStatAdat; }
                set { _HatosagiStatAdat = value; }                                                        
            }        
                   
           
        private SqlString _EREC_IratAlairok = SqlString.Null;
           
        /// <summary>
        /// EREC_IratAlairok Base property </summary>
            public SqlString EREC_IratAlairok
            {
                get { return _EREC_IratAlairok; }
                set { _EREC_IratAlairok = value; }                                                        
            }        
                   
           
        private SqlInt32 _Lezarhato = SqlInt32.Null;
           
        /// <summary>
        /// Lezarhato Base property </summary>
            public SqlInt32 Lezarhato
            {
                get { return _Lezarhato; }
                set { _Lezarhato = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}