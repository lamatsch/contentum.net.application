
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_UgyUgyiratdarabok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_UgyUgyiratdarabok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_UgyUgyiratdarabokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _UgyUgyirat_Id = true;
               public bool UgyUgyirat_Id
               {
                   get { return _UgyUgyirat_Id; }
                   set { _UgyUgyirat_Id = value; }
               }
                                                            
                                 
               private bool _EljarasiSzakasz = true;
               public bool EljarasiSzakasz
               {
                   get { return _EljarasiSzakasz; }
                   set { _EljarasiSzakasz = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _Hatarido = true;
               public bool Hatarido
               {
                   get { return _Hatarido; }
                   set { _Hatarido = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Ugyintez = true;
               public bool FelhasznaloCsoport_Id_Ugyintez
               {
                   get { return _FelhasznaloCsoport_Id_Ugyintez; }
                   set { _FelhasznaloCsoport_Id_Ugyintez = value; }
               }
                                                            
                                 
               private bool _ElintezesDat = true;
               public bool ElintezesDat
               {
                   get { return _ElintezesDat; }
                   set { _ElintezesDat = value; }
               }
                                                            
                                 
               private bool _LezarasDat = true;
               public bool LezarasDat
               {
                   get { return _LezarasDat; }
                   set { _LezarasDat = value; }
               }
                                                            
                                 
               private bool _ElintezesMod = true;
               public bool ElintezesMod
               {
                   get { return _ElintezesMod; }
                   set { _ElintezesMod = value; }
               }
                                                            
                                 
               private bool _LezarasOka = true;
               public bool LezarasOka
               {
                   get { return _LezarasOka; }
                   set { _LezarasOka = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _UgyUgyirat_Id_Elozo = true;
               public bool UgyUgyirat_Id_Elozo
               {
                   get { return _UgyUgyirat_Id_Elozo; }
                   set { _UgyUgyirat_Id_Elozo = value; }
               }
                                                            
                                 
               private bool _IraIktatokonyv_Id = true;
               public bool IraIktatokonyv_Id
               {
                   get { return _IraIktatokonyv_Id; }
                   set { _IraIktatokonyv_Id = value; }
               }
                                                            
                                 
               private bool _Foszam = true;
               public bool Foszam
               {
                   get { return _Foszam; }
                   set { _Foszam = value; }
               }
                                                            
                                 
               private bool _UtolsoAlszam = true;
               public bool UtolsoAlszam
               {
                   get { return _UtolsoAlszam; }
                   set { _UtolsoAlszam = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos_Elozo = true;
               public bool Csoport_Id_Felelos_Elozo
               {
                   get { return _Csoport_Id_Felelos_Elozo; }
                   set { _Csoport_Id_Felelos_Elozo = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   UgyUgyirat_Id = Value;               
                    
                   EljarasiSzakasz = Value;               
                    
                   Sorszam = Value;               
                    
                   Azonosito = Value;               
                    
                   Hatarido = Value;               
                    
                   FelhasznaloCsoport_Id_Ugyintez = Value;               
                    
                   ElintezesDat = Value;               
                    
                   LezarasDat = Value;               
                    
                   ElintezesMod = Value;               
                    
                   LezarasOka = Value;               
                    
                   Leiras = Value;               
                    
                   UgyUgyirat_Id_Elozo = Value;               
                    
                   IraIktatokonyv_Id = Value;               
                    
                   Foszam = Value;               
                    
                   UtolsoAlszam = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Csoport_Id_Felelos_Elozo = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_UgyUgyiratdarabokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _UgyUgyirat_Id = SqlGuid.Null;
           
        /// <summary>
        /// UgyUgyirat_Id Base property </summary>
            public SqlGuid UgyUgyirat_Id
            {
                get { return _UgyUgyirat_Id; }
                set { _UgyUgyirat_Id = value; }                                                        
            }        
                   
           
        private SqlString _EljarasiSzakasz = SqlString.Null;
           
        /// <summary>
        /// EljarasiSzakasz Base property </summary>
            public SqlString EljarasiSzakasz
            {
                get { return _EljarasiSzakasz; }
                set { _EljarasiSzakasz = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _Hatarido = SqlDateTime.Null;
           
        /// <summary>
        /// Hatarido Base property </summary>
            public SqlDateTime Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Ugyintez = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Ugyintez
            {
                get { return _FelhasznaloCsoport_Id_Ugyintez; }
                set { _FelhasznaloCsoport_Id_Ugyintez = value; }                                                        
            }        
                   
           
        private SqlDateTime _ElintezesDat = SqlDateTime.Null;
           
        /// <summary>
        /// ElintezesDat Base property </summary>
            public SqlDateTime ElintezesDat
            {
                get { return _ElintezesDat; }
                set { _ElintezesDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _LezarasDat = SqlDateTime.Null;
           
        /// <summary>
        /// LezarasDat Base property </summary>
            public SqlDateTime LezarasDat
            {
                get { return _LezarasDat; }
                set { _LezarasDat = value; }                                                        
            }        
                   
           
        private SqlString _ElintezesMod = SqlString.Null;
           
        /// <summary>
        /// ElintezesMod Base property </summary>
            public SqlString ElintezesMod
            {
                get { return _ElintezesMod; }
                set { _ElintezesMod = value; }                                                        
            }        
                   
           
        private SqlString _LezarasOka = SqlString.Null;
           
        /// <summary>
        /// LezarasOka Base property </summary>
            public SqlString LezarasOka
            {
                get { return _LezarasOka; }
                set { _LezarasOka = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlGuid _UgyUgyirat_Id_Elozo = SqlGuid.Null;
           
        /// <summary>
        /// UgyUgyirat_Id_Elozo Base property </summary>
            public SqlGuid UgyUgyirat_Id_Elozo
            {
                get { return _UgyUgyirat_Id_Elozo; }
                set { _UgyUgyirat_Id_Elozo = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIktatokonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIktatokonyv_Id Base property </summary>
            public SqlGuid IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Foszam = SqlInt32.Null;
           
        /// <summary>
        /// Foszam Base property </summary>
            public SqlInt32 Foszam
            {
                get { return _Foszam; }
                set { _Foszam = value; }                                                        
            }        
                   
           
        private SqlInt32 _UtolsoAlszam = SqlInt32.Null;
           
        /// <summary>
        /// UtolsoAlszam Base property </summary>
            public SqlInt32 UtolsoAlszam
            {
                get { return _UtolsoAlszam; }
                set { _UtolsoAlszam = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos_Elozo = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo Base property </summary>
            public SqlGuid Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}