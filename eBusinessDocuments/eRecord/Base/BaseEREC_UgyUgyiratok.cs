
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_UgyUgyiratok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_UgyUgyiratok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_UgyUgyiratokBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _Foszam = true;
            public bool Foszam
            {
                get { return _Foszam; }
                set { _Foszam = value; }
            }


            private bool _Sorszam = true;
            public bool Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }
            }


            private bool _Ugyazonosito = true;
            public bool Ugyazonosito
            {
                get { return _Ugyazonosito; }
                set { _Ugyazonosito = value; }
            }


            private bool _Alkalmazas_Id = true;
            public bool Alkalmazas_Id
            {
                get { return _Alkalmazas_Id; }
                set { _Alkalmazas_Id = value; }
            }


            private bool _UgyintezesModja = true;
            public bool UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }
            }


            private bool _Hatarido = true;
            public bool Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }
            }


            private bool _SkontrobaDat = true;
            public bool SkontrobaDat
            {
                get { return _SkontrobaDat; }
                set { _SkontrobaDat = value; }
            }


            private bool _LezarasDat = true;
            public bool LezarasDat
            {
                get { return _LezarasDat; }
                set { _LezarasDat = value; }
            }


            private bool _IrattarbaKuldDatuma = true;
            public bool IrattarbaKuldDatuma
            {
                get { return _IrattarbaKuldDatuma; }
                set { _IrattarbaKuldDatuma = value; }
            }


            private bool _IrattarbaVetelDat = true;
            public bool IrattarbaVetelDat
            {
                get { return _IrattarbaVetelDat; }
                set { _IrattarbaVetelDat = value; }
            }


            private bool _FelhCsoport_Id_IrattariAtvevo = true;
            public bool FelhCsoport_Id_IrattariAtvevo
            {
                get { return _FelhCsoport_Id_IrattariAtvevo; }
                set { _FelhCsoport_Id_IrattariAtvevo = value; }
            }


            private bool _SelejtezesDat = true;
            public bool SelejtezesDat
            {
                get { return _SelejtezesDat; }
                set { _SelejtezesDat = value; }
            }


            private bool _FelhCsoport_Id_Selejtezo = true;
            public bool FelhCsoport_Id_Selejtezo
            {
                get { return _FelhCsoport_Id_Selejtezo; }
                set { _FelhCsoport_Id_Selejtezo = value; }
            }


            private bool _LeveltariAtvevoNeve = true;
            public bool LeveltariAtvevoNeve
            {
                get { return _LeveltariAtvevoNeve; }
                set { _LeveltariAtvevoNeve = value; }
            }


            private bool _FelhCsoport_Id_Felulvizsgalo = true;
            public bool FelhCsoport_Id_Felulvizsgalo
            {
                get { return _FelhCsoport_Id_Felulvizsgalo; }
                set { _FelhCsoport_Id_Felulvizsgalo = value; }
            }


            private bool _FelulvizsgalatDat = true;
            public bool FelulvizsgalatDat
            {
                get { return _FelulvizsgalatDat; }
                set { _FelulvizsgalatDat = value; }
            }


            private bool _IktatoszamKieg = true;
            public bool IktatoszamKieg
            {
                get { return _IktatoszamKieg; }
                set { _IktatoszamKieg = value; }
            }


            private bool _Targy = true;
            public bool Targy
            {
                get { return _Targy; }
                set { _Targy = value; }
            }


            private bool _UgyUgyirat_Id_Szulo = true;
            public bool UgyUgyirat_Id_Szulo
            {
                get { return _UgyUgyirat_Id_Szulo; }
                set { _UgyUgyirat_Id_Szulo = value; }
            }


            private bool _UgyUgyirat_Id_Kulso = true;
            public bool UgyUgyirat_Id_Kulso
            {
                get { return _UgyUgyirat_Id_Kulso; }
                set { _UgyUgyirat_Id_Kulso = value; }
            }


            private bool _UgyTipus = true;
            public bool UgyTipus
            {
                get { return _UgyTipus; }
                set { _UgyTipus = value; }
            }


            private bool _IrattariHely = true;
            public bool IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }
            }


            private bool _SztornirozasDat = true;
            public bool SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }
            }


            private bool _Csoport_Id_Felelos = true;
            public bool Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private bool _FelhasznaloCsoport_Id_Orzo = true;
            public bool FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }
            }


            private bool _Csoport_Id_Cimzett = true;
            public bool Csoport_Id_Cimzett
            {
                get { return _Csoport_Id_Cimzett; }
                set { _Csoport_Id_Cimzett = value; }
            }


            private bool _Jelleg = true;
            public bool Jelleg
            {
                get { return _Jelleg; }
                set { _Jelleg = value; }
            }


            private bool _IraIrattariTetel_Id = true;
            public bool IraIrattariTetel_Id
            {
                get { return _IraIrattariTetel_Id; }
                set { _IraIrattariTetel_Id = value; }
            }


            private bool _IraIktatokonyv_Id = true;
            public bool IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }
            }


            private bool _SkontroOka = true;
            public bool SkontroOka
            {
                get { return _SkontroOka; }
                set { _SkontroOka = value; }
            }


            private bool _SkontroVege = true;
            public bool SkontroVege
            {
                get { return _SkontroVege; }
                set { _SkontroVege = value; }
            }


            private bool _Surgosseg = true;
            public bool Surgosseg
            {
                get { return _Surgosseg; }
                set { _Surgosseg = value; }
            }


            private bool _SkontrobanOsszesen = true;
            public bool SkontrobanOsszesen
            {
                get { return _SkontrobanOsszesen; }
                set { _SkontrobanOsszesen = value; }
            }


            private bool _MegorzesiIdoVege = true;
            public bool MegorzesiIdoVege
            {
                get { return _MegorzesiIdoVege; }
                set { _MegorzesiIdoVege = value; }
            }


            private bool _Partner_Id_Ugyindito = true;
            public bool Partner_Id_Ugyindito
            {
                get { return _Partner_Id_Ugyindito; }
                set { _Partner_Id_Ugyindito = value; }
            }


            private bool _NevSTR_Ugyindito = true;
            public bool NevSTR_Ugyindito
            {
                get { return _NevSTR_Ugyindito; }
                set { _NevSTR_Ugyindito = value; }
            }


            private bool _ElintezesDat = true;
            public bool ElintezesDat
            {
                get { return _ElintezesDat; }
                set { _ElintezesDat = value; }
            }


            private bool _FelhasznaloCsoport_Id_Ugyintez = true;
            public bool FelhasznaloCsoport_Id_Ugyintez
            {
                get { return _FelhasznaloCsoport_Id_Ugyintez; }
                set { _FelhasznaloCsoport_Id_Ugyintez = value; }
            }


            private bool _Csoport_Id_Felelos_Elozo = true;
            public bool Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }
            }


            private bool _KolcsonKikerDat = true;
            public bool KolcsonKikerDat
            {
                get { return _KolcsonKikerDat; }
                set { _KolcsonKikerDat = value; }
            }


            private bool _KolcsonKiadDat = true;
            public bool KolcsonKiadDat
            {
                get { return _KolcsonKiadDat; }
                set { _KolcsonKiadDat = value; }
            }


            private bool _Kolcsonhatarido = true;
            public bool Kolcsonhatarido
            {
                get { return _Kolcsonhatarido; }
                set { _Kolcsonhatarido = value; }
            }


            private bool _BARCODE = true;
            public bool BARCODE
            {
                get { return _BARCODE; }
                set { _BARCODE = value; }
            }


            private bool _IratMetadefinicio_Id = true;
            public bool IratMetadefinicio_Id
            {
                get { return _IratMetadefinicio_Id; }
                set { _IratMetadefinicio_Id = value; }
            }


            private bool _Allapot = true;
            public bool Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private bool _TovabbitasAlattAllapot = true;
            public bool TovabbitasAlattAllapot
            {
                get { return _TovabbitasAlattAllapot; }
                set { _TovabbitasAlattAllapot = value; }
            }


            private bool _Megjegyzes = true;
            public bool Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }
            }


            private bool _Azonosito = true;
            public bool Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }
            }


            private bool _Fizikai_Kezbesitesi_Allapot = true;
            public bool Fizikai_Kezbesitesi_Allapot
            {
                get { return _Fizikai_Kezbesitesi_Allapot; }
                set { _Fizikai_Kezbesitesi_Allapot = value; }
            }


            private bool _Kovetkezo_Orzo_Id = true;
            public bool Kovetkezo_Orzo_Id
            {
                get { return _Kovetkezo_Orzo_Id; }
                set { _Kovetkezo_Orzo_Id = value; }
            }


            private bool _Csoport_Id_Ugyfelelos = true;
            public bool Csoport_Id_Ugyfelelos
            {
                get { return _Csoport_Id_Ugyfelelos; }
                set { _Csoport_Id_Ugyfelelos = value; }
            }


            private bool _Elektronikus_Kezbesitesi_Allap = true;
            public bool Elektronikus_Kezbesitesi_Allap
            {
                get { return _Elektronikus_Kezbesitesi_Allap; }
                set { _Elektronikus_Kezbesitesi_Allap = value; }
            }


            private bool _Kovetkezo_Felelos_Id = true;
            public bool Kovetkezo_Felelos_Id
            {
                get { return _Kovetkezo_Felelos_Id; }
                set { _Kovetkezo_Felelos_Id = value; }
            }


            private bool _UtolsoAlszam = true;
            public bool UtolsoAlszam
            {
                get { return _UtolsoAlszam; }
                set { _UtolsoAlszam = value; }
            }


            private bool _UtolsoSorszam = true;
            public bool UtolsoSorszam
            {
                get { return _UtolsoSorszam; }
                set { _UtolsoSorszam = value; }
            }


            private bool _IratSzam = true;
            public bool IratSzam
            {
                get { return _IratSzam; }
                set { _IratSzam = value; }
            }


            private bool _ElintezesMod = true;
            public bool ElintezesMod
            {
                get { return _ElintezesMod; }
                set { _ElintezesMod = value; }
            }


            private bool _RegirendszerIktatoszam = true;
            public bool RegirendszerIktatoszam
            {
                get { return _RegirendszerIktatoszam; }
                set { _RegirendszerIktatoszam = value; }
            }


            private bool _GeneraltTargy = true;
            public bool GeneraltTargy
            {
                get { return _GeneraltTargy; }
                set { _GeneraltTargy = value; }
            }


            private bool _Cim_Id_Ugyindito = true;
            public bool Cim_Id_Ugyindito
            {
                get { return _Cim_Id_Ugyindito; }
                set { _Cim_Id_Ugyindito = value; }
            }


            private bool _CimSTR_Ugyindito = true;
            public bool CimSTR_Ugyindito
            {
                get { return _CimSTR_Ugyindito; }
                set { _CimSTR_Ugyindito = value; }
            }


            private bool _LezarasOka = true;
            public bool LezarasOka
            {
                get { return _LezarasOka; }
                set { _LezarasOka = value; }
            }


            private bool _FelfuggesztesOka = true;
            public bool FelfuggesztesOka
            {
                get { return _FelfuggesztesOka; }
                set { _FelfuggesztesOka = value; }
            }


            private bool _IrattarId = true;
            public bool IrattarId
            {
                get { return _IrattarId; }
                set { _IrattarId = value; }
            }


            private bool _SkontroOka_Kod = true;
            public bool SkontroOka_Kod
            {
                get { return _SkontroOka_Kod; }
                set { _SkontroOka_Kod = value; }
            }


            private bool _UjOrzesiIdo = true;
            public bool UjOrzesiIdo
            {
                get { return _UjOrzesiIdo; }
                set { _UjOrzesiIdo = value; }
            }


            private bool _UjOrzesiIdoIdoegyseg = true;
            public bool UjOrzesiIdoIdoegyseg
            {
                get { return _UjOrzesiIdoIdoegyseg; }
                set { _UjOrzesiIdoIdoegyseg = value; }
            }


            private bool _UgyintezesKezdete = true;
            public bool UgyintezesKezdete
            {
                get { return _UgyintezesKezdete; }
                set { _UgyintezesKezdete = value; }
            }


            private bool _FelfuggesztettNapokSzama = true;
            public bool FelfuggesztettNapokSzama
            {
                get { return _FelfuggesztettNapokSzama; }
                set { _FelfuggesztettNapokSzama = value; }
            }


            private bool _IntezesiIdo = true;
            public bool IntezesiIdo
            {
                get { return _IntezesiIdo; }
                set { _IntezesiIdo = value; }
            }


            private bool _IntezesiIdoegyseg = true;
            public bool IntezesiIdoegyseg
            {
                get { return _IntezesiIdoegyseg; }
                set { _IntezesiIdoegyseg = value; }
            }


            private bool _Ugy_Fajtaja = true;
            public bool Ugy_Fajtaja
            {
                get { return _Ugy_Fajtaja; }
                set { _Ugy_Fajtaja = value; }
            }


            private bool _SzignaloId = true;
            public bool SzignaloId
            {
                get { return _SzignaloId; }
                set { _SzignaloId = value; }
            }


            private bool _SzignalasIdeje = true;
            public bool SzignalasIdeje
            {
                get { return _SzignalasIdeje; }
                set { _SzignalasIdeje = value; }
            }


            private bool _ElteltIdo = true;
            public bool ElteltIdo
            {
                get { return _ElteltIdo; }
                set { _ElteltIdo = value; }
            }


            private bool _ElteltIdoIdoEgyseg = true;
            public bool ElteltIdoIdoEgyseg
            {
                get { return _ElteltIdoIdoEgyseg; }
                set { _ElteltIdoIdoEgyseg = value; }
            }


            private bool _ElteltidoAllapot = true;
            public bool ElteltidoAllapot
            {
                get { return _ElteltidoAllapot; }
                set { _ElteltidoAllapot = value; }
            }


            private bool _ElteltIdoUtolsoModositas = true;
            public bool ElteltIdoUtolsoModositas
            {
                get { return _ElteltIdoUtolsoModositas; }
                set { _ElteltIdoUtolsoModositas = value; }
            }


            private bool _SakkoraAllapot = true;
            public bool SakkoraAllapot
            {
                get { return _SakkoraAllapot; }
                set { _SakkoraAllapot = value; }
            }


            private bool _HatralevoNapok = true;
            public bool HatralevoNapok
            {
                get { return _HatralevoNapok; }
                set { _HatralevoNapok = value; }
            }


            private bool _HatralevoMunkaNapok = true;
            public bool HatralevoMunkaNapok
            {
                get { return _HatralevoMunkaNapok; }
                set { _HatralevoMunkaNapok = value; }
            }


            private bool _ElozoAllapot = true;
            public bool ElozoAllapot
            {
                get { return _ElozoAllapot; }
                set { _ElozoAllapot = value; }
            }


            private bool _Aktiv = true;
            public bool Aktiv
            {
                get { return _Aktiv; }
                set { _Aktiv = value; }
            }

            private bool _IrattarHelyfoglalas = true;
            public bool IrattarHelyfoglalas
            {
                get { return _IrattarHelyfoglalas; }
                set { _IrattarHelyfoglalas = value; }
            }

            private bool _ErvKezd = true;
            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private bool _ErvVege = true;
            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            public void SetValueAll(bool Value)
            {

                Id = Value;

                Foszam = Value;

                Sorszam = Value;

                Ugyazonosito = Value;

                Alkalmazas_Id = Value;

                UgyintezesModja = Value;

                Hatarido = Value;

                SkontrobaDat = Value;

                LezarasDat = Value;

                IrattarbaKuldDatuma = Value;

                IrattarbaVetelDat = Value;

                FelhCsoport_Id_IrattariAtvevo = Value;

                SelejtezesDat = Value;

                FelhCsoport_Id_Selejtezo = Value;

                LeveltariAtvevoNeve = Value;

                FelhCsoport_Id_Felulvizsgalo = Value;

                FelulvizsgalatDat = Value;

                IktatoszamKieg = Value;

                Targy = Value;

                UgyUgyirat_Id_Szulo = Value;

                UgyUgyirat_Id_Kulso = Value;

                UgyTipus = Value;

                IrattariHely = Value;

                SztornirozasDat = Value;

                Csoport_Id_Felelos = Value;

                FelhasznaloCsoport_Id_Orzo = Value;

                Csoport_Id_Cimzett = Value;

                Jelleg = Value;

                IraIrattariTetel_Id = Value;

                IraIktatokonyv_Id = Value;

                SkontroOka = Value;

                SkontroVege = Value;

                Surgosseg = Value;

                SkontrobanOsszesen = Value;

                MegorzesiIdoVege = Value;

                Partner_Id_Ugyindito = Value;

                NevSTR_Ugyindito = Value;

                ElintezesDat = Value;

                FelhasznaloCsoport_Id_Ugyintez = Value;

                Csoport_Id_Felelos_Elozo = Value;

                KolcsonKikerDat = Value;

                KolcsonKiadDat = Value;

                Kolcsonhatarido = Value;

                BARCODE = Value;

                IratMetadefinicio_Id = Value;

                Allapot = Value;

                TovabbitasAlattAllapot = Value;

                Megjegyzes = Value;

                Azonosito = Value;

                Fizikai_Kezbesitesi_Allapot = Value;

                Kovetkezo_Orzo_Id = Value;

                Csoport_Id_Ugyfelelos = Value;

                Elektronikus_Kezbesitesi_Allap = Value;

                Kovetkezo_Felelos_Id = Value;

                UtolsoAlszam = Value;

                UtolsoSorszam = Value;

                IratSzam = Value;

                ElintezesMod = Value;

                RegirendszerIktatoszam = Value;

                GeneraltTargy = Value;

                Cim_Id_Ugyindito = Value;

                CimSTR_Ugyindito = Value;

                LezarasOka = Value;

                FelfuggesztesOka = Value;

                IrattarId = Value;

                SkontroOka_Kod = Value;

                UjOrzesiIdo = Value;

                UjOrzesiIdoIdoegyseg = Value;

                UgyintezesKezdete = Value;

                FelfuggesztettNapokSzama = Value;

                IntezesiIdo = Value;

                IntezesiIdoegyseg = Value;

                Ugy_Fajtaja = Value;

                SzignaloId = Value;

                SzignalasIdeje = Value;

                ElteltIdo = Value;

                ElteltIdoIdoEgyseg = Value;

                ElteltidoAllapot = Value;

                ElteltIdoUtolsoModositas = Value;

                SakkoraAllapot = Value;

                HatralevoNapok = Value;

                HatralevoMunkaNapok = Value;

                ElozoAllapot = Value;

                Aktiv = Value;

                IrattarHelyfoglalas = Value;

                ErvKezd = Value;

                ErvVege = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseEREC_UgyUgyiratokBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlInt32 _Foszam = SqlInt32.Null;

            /// <summary>
            /// Foszam Base property </summary>
            public SqlInt32 Foszam
            {
                get { return _Foszam; }
                set { _Foszam = value; }
            }


            private SqlInt32 _Sorszam = SqlInt32.Null;

            /// <summary>
            /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }
            }


            private SqlString _Ugyazonosito = SqlString.Null;

            /// <summary>
            /// Ugyazonosito  Base property </summary>
            public SqlString Ugyazonosito
            {
                get { return _Ugyazonosito; }
                set { _Ugyazonosito = value; }
            }


            private SqlGuid _Alkalmazas_Id = SqlGuid.Null;

            /// <summary>
            /// Alkalmazas_Id Base property </summary>
            public SqlGuid Alkalmazas_Id
            {
                get { return _Alkalmazas_Id; }
                set { _Alkalmazas_Id = value; }
            }


            private SqlString _UgyintezesModja = SqlString.Null;

            /// <summary>
            /// UgyintezesModja Base property </summary>
            public SqlString UgyintezesModja
            {
                get { return _UgyintezesModja; }
                set { _UgyintezesModja = value; }
            }


            private SqlDateTime _Hatarido = SqlDateTime.Null;

            /// <summary>
            /// Hatarido Base property </summary>
            public SqlDateTime Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }
            }


            private SqlDateTime _SkontrobaDat = SqlDateTime.Null;

            /// <summary>
            /// SkontrobaDat Base property </summary>
            public SqlDateTime SkontrobaDat
            {
                get { return _SkontrobaDat; }
                set { _SkontrobaDat = value; }
            }


            private SqlDateTime _LezarasDat = SqlDateTime.Null;

            /// <summary>
            /// LezarasDat Base property </summary>
            public SqlDateTime LezarasDat
            {
                get { return _LezarasDat; }
                set { _LezarasDat = value; }
            }


            private SqlDateTime _IrattarbaKuldDatuma = SqlDateTime.Null;

            /// <summary>
            /// IrattarbaKuldDatuma  Base property </summary>
            public SqlDateTime IrattarbaKuldDatuma
            {
                get { return _IrattarbaKuldDatuma; }
                set { _IrattarbaKuldDatuma = value; }
            }


            private SqlDateTime _IrattarbaVetelDat = SqlDateTime.Null;

            /// <summary>
            /// IrattarbaVetelDat Base property </summary>
            public SqlDateTime IrattarbaVetelDat
            {
                get { return _IrattarbaVetelDat; }
                set { _IrattarbaVetelDat = value; }
            }


            private SqlGuid _FelhCsoport_Id_IrattariAtvevo = SqlGuid.Null;

            /// <summary>
            /// FelhCsoport_Id_IrattariAtvevo Base property </summary>
            public SqlGuid FelhCsoport_Id_IrattariAtvevo
            {
                get { return _FelhCsoport_Id_IrattariAtvevo; }
                set { _FelhCsoport_Id_IrattariAtvevo = value; }
            }


            private SqlDateTime _SelejtezesDat = SqlDateTime.Null;

            /// <summary>
            /// SelejtezesDat Base property </summary>
            public SqlDateTime SelejtezesDat
            {
                get { return _SelejtezesDat; }
                set { _SelejtezesDat = value; }
            }


            private SqlGuid _FelhCsoport_Id_Selejtezo = SqlGuid.Null;

            /// <summary>
            /// FelhCsoport_Id_Selejtezo Base property </summary>
            public SqlGuid FelhCsoport_Id_Selejtezo
            {
                get { return _FelhCsoport_Id_Selejtezo; }
                set { _FelhCsoport_Id_Selejtezo = value; }
            }


            private SqlString _LeveltariAtvevoNeve = SqlString.Null;

            /// <summary>
            /// LeveltariAtvevoNeve Base property </summary>
            public SqlString LeveltariAtvevoNeve
            {
                get { return _LeveltariAtvevoNeve; }
                set { _LeveltariAtvevoNeve = value; }
            }


            private SqlGuid _FelhCsoport_Id_Felulvizsgalo = SqlGuid.Null;

            /// <summary>
            /// FelhCsoport_Id_Felulvizsgalo Base property </summary>
            public SqlGuid FelhCsoport_Id_Felulvizsgalo
            {
                get { return _FelhCsoport_Id_Felulvizsgalo; }
                set { _FelhCsoport_Id_Felulvizsgalo = value; }
            }


            private SqlDateTime _FelulvizsgalatDat = SqlDateTime.Null;

            /// <summary>
            /// FelulvizsgalatDat Base property </summary>
            public SqlDateTime FelulvizsgalatDat
            {
                get { return _FelulvizsgalatDat; }
                set { _FelulvizsgalatDat = value; }
            }


            private SqlString _IktatoszamKieg = SqlString.Null;

            /// <summary>
            /// IktatoszamKieg Base property </summary>
            public SqlString IktatoszamKieg
            {
                get { return _IktatoszamKieg; }
                set { _IktatoszamKieg = value; }
            }


            private SqlString _Targy = SqlString.Null;

            /// <summary>
            /// Targy Base property </summary>
            public SqlString Targy
            {
                get { return _Targy; }
                set { _Targy = value; }
            }


            private SqlGuid _UgyUgyirat_Id_Szulo = SqlGuid.Null;

            /// <summary>
            /// UgyUgyirat_Id_Szulo Base property </summary>
            public SqlGuid UgyUgyirat_Id_Szulo
            {
                get { return _UgyUgyirat_Id_Szulo; }
                set { _UgyUgyirat_Id_Szulo = value; }
            }


            private SqlGuid _UgyUgyirat_Id_Kulso = SqlGuid.Null;

            /// <summary>
            /// UgyUgyirat_Id_Kulso Base property </summary>
            public SqlGuid UgyUgyirat_Id_Kulso
            {
                get { return _UgyUgyirat_Id_Kulso; }
                set { _UgyUgyirat_Id_Kulso = value; }
            }


            private SqlString _UgyTipus = SqlString.Null;

            /// <summary>
            /// UgyTipus Base property </summary>
            public SqlString UgyTipus
            {
                get { return _UgyTipus; }
                set { _UgyTipus = value; }
            }


            private SqlString _IrattariHely = SqlString.Null;

            /// <summary>
            /// IrattariHely Base property </summary>
            public SqlString IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }
            }


            private SqlDateTime _SztornirozasDat = SqlDateTime.Null;

            /// <summary>
            /// SztornirozasDat Base property </summary>
            public SqlDateTime SztornirozasDat
            {
                get { return _SztornirozasDat; }
                set { _SztornirozasDat = value; }
            }


            private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }
            }


            private SqlGuid _FelhasznaloCsoport_Id_Orzo = SqlGuid.Null;

            /// <summary>
            /// FelhasznaloCsoport_Id_Orzo Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Orzo
            {
                get { return _FelhasznaloCsoport_Id_Orzo; }
                set { _FelhasznaloCsoport_Id_Orzo = value; }
            }


            private SqlGuid _Csoport_Id_Cimzett = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Cimzett Base property </summary>
            public SqlGuid Csoport_Id_Cimzett
            {
                get { return _Csoport_Id_Cimzett; }
                set { _Csoport_Id_Cimzett = value; }
            }


            private SqlString _Jelleg = SqlString.Null;

            /// <summary>
            /// Jelleg Base property </summary>
            public SqlString Jelleg
            {
                get { return _Jelleg; }
                set { _Jelleg = value; }
            }


            private SqlGuid _IraIrattariTetel_Id = SqlGuid.Null;

            /// <summary>
            /// IraIrattariTetel_Id Base property </summary>
            public SqlGuid IraIrattariTetel_Id
            {
                get { return _IraIrattariTetel_Id; }
                set { _IraIrattariTetel_Id = value; }
            }


            private SqlGuid _IraIktatokonyv_Id = SqlGuid.Null;

            /// <summary>
            /// IraIktatokonyv_Id Base property </summary>
            public SqlGuid IraIktatokonyv_Id
            {
                get { return _IraIktatokonyv_Id; }
                set { _IraIktatokonyv_Id = value; }
            }


            private SqlString _SkontroOka = SqlString.Null;

            /// <summary>
            /// SkontroOka Base property </summary>
            public SqlString SkontroOka
            {
                get { return _SkontroOka; }
                set { _SkontroOka = value; }
            }


            private SqlDateTime _SkontroVege = SqlDateTime.Null;

            /// <summary>
            /// SkontroVege Base property </summary>
            public SqlDateTime SkontroVege
            {
                get { return _SkontroVege; }
                set { _SkontroVege = value; }
            }


            private SqlString _Surgosseg = SqlString.Null;

            /// <summary>
            /// Surgosseg Base property </summary>
            public SqlString Surgosseg
            {
                get { return _Surgosseg; }
                set { _Surgosseg = value; }
            }


            private SqlInt32 _SkontrobanOsszesen = SqlInt32.Null;

            /// <summary>
            /// SkontrobanOsszesen Base property </summary>
            public SqlInt32 SkontrobanOsszesen
            {
                get { return _SkontrobanOsszesen; }
                set { _SkontrobanOsszesen = value; }
            }


            private SqlDateTime _MegorzesiIdoVege = SqlDateTime.Null;

            /// <summary>
            /// MegorzesiIdoVege Base property </summary>
            public SqlDateTime MegorzesiIdoVege
            {
                get { return _MegorzesiIdoVege; }
                set { _MegorzesiIdoVege = value; }
            }


            private SqlGuid _Partner_Id_Ugyindito = SqlGuid.Null;

            /// <summary>
            /// Partner_Id_Ugyindito Base property </summary>
            public SqlGuid Partner_Id_Ugyindito
            {
                get { return _Partner_Id_Ugyindito; }
                set { _Partner_Id_Ugyindito = value; }
            }


            private SqlString _NevSTR_Ugyindito = SqlString.Null;

            /// <summary>
            /// NevSTR_Ugyindito Base property </summary>
            public SqlString NevSTR_Ugyindito
            {
                get { return _NevSTR_Ugyindito; }
                set { _NevSTR_Ugyindito = value; }
            }


            private SqlDateTime _ElintezesDat = SqlDateTime.Null;

            /// <summary>
            /// ElintezesDat Base property </summary>
            public SqlDateTime ElintezesDat
            {
                get { return _ElintezesDat; }
                set { _ElintezesDat = value; }
            }


            private SqlGuid _FelhasznaloCsoport_Id_Ugyintez = SqlGuid.Null;

            /// <summary>
            /// FelhasznaloCsoport_Id_Ugyintez Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Ugyintez
            {
                get { return _FelhasznaloCsoport_Id_Ugyintez; }
                set { _FelhasznaloCsoport_Id_Ugyintez = value; }
            }


            private SqlGuid _Csoport_Id_Felelos_Elozo = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Felelos_Elozo Base property </summary>
            public SqlGuid Csoport_Id_Felelos_Elozo
            {
                get { return _Csoport_Id_Felelos_Elozo; }
                set { _Csoport_Id_Felelos_Elozo = value; }
            }


            private SqlDateTime _KolcsonKikerDat = SqlDateTime.Null;

            /// <summary>
            /// KolcsonKikerDat Base property </summary>
            public SqlDateTime KolcsonKikerDat
            {
                get { return _KolcsonKikerDat; }
                set { _KolcsonKikerDat = value; }
            }


            private SqlDateTime _KolcsonKiadDat = SqlDateTime.Null;

            /// <summary>
            /// KolcsonKiadDat Base property </summary>
            public SqlDateTime KolcsonKiadDat
            {
                get { return _KolcsonKiadDat; }
                set { _KolcsonKiadDat = value; }
            }


            private SqlDateTime _Kolcsonhatarido = SqlDateTime.Null;

            /// <summary>
            /// Kolcsonhatarido Base property </summary>
            public SqlDateTime Kolcsonhatarido
            {
                get { return _Kolcsonhatarido; }
                set { _Kolcsonhatarido = value; }
            }


            private SqlString _BARCODE = SqlString.Null;

            /// <summary>
            /// BARCODE Base property </summary>
            public SqlString BARCODE
            {
                get { return _BARCODE; }
                set { _BARCODE = value; }
            }


            private SqlGuid _IratMetadefinicio_Id = SqlGuid.Null;

            /// <summary>
            /// IratMetadefinicio_Id Base property </summary>
            public SqlGuid IratMetadefinicio_Id
            {
                get { return _IratMetadefinicio_Id; }
                set { _IratMetadefinicio_Id = value; }
            }


            private SqlString _Allapot = SqlString.Null;

            /// <summary>
            /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }
            }


            private SqlString _TovabbitasAlattAllapot = SqlString.Null;

            /// <summary>
            /// TovabbitasAlattAllapot Base property </summary>
            public SqlString TovabbitasAlattAllapot
            {
                get { return _TovabbitasAlattAllapot; }
                set { _TovabbitasAlattAllapot = value; }
            }


            private SqlString _Megjegyzes = SqlString.Null;

            /// <summary>
            /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }
            }


            private SqlString _Azonosito = SqlString.Null;

            /// <summary>
            /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }
            }


            private SqlString _Fizikai_Kezbesitesi_Allapot = SqlString.Null;

            /// <summary>
            /// Fizikai_Kezbesitesi_Allapot Base property </summary>
            public SqlString Fizikai_Kezbesitesi_Allapot
            {
                get { return _Fizikai_Kezbesitesi_Allapot; }
                set { _Fizikai_Kezbesitesi_Allapot = value; }
            }


            private SqlGuid _Kovetkezo_Orzo_Id = SqlGuid.Null;

            /// <summary>
            /// Kovetkezo_Orzo_Id Base property </summary>
            public SqlGuid Kovetkezo_Orzo_Id
            {
                get { return _Kovetkezo_Orzo_Id; }
                set { _Kovetkezo_Orzo_Id = value; }
            }


            private SqlGuid _Csoport_Id_Ugyfelelos = SqlGuid.Null;

            /// <summary>
            /// Csoport_Id_Ugyfelelos Base property </summary>
            public SqlGuid Csoport_Id_Ugyfelelos
            {
                get { return _Csoport_Id_Ugyfelelos; }
                set { _Csoport_Id_Ugyfelelos = value; }
            }


            private SqlString _Elektronikus_Kezbesitesi_Allap = SqlString.Null;

            /// <summary>
            /// Elektronikus_Kezbesitesi_Allap Base property </summary>
            public SqlString Elektronikus_Kezbesitesi_Allap
            {
                get { return _Elektronikus_Kezbesitesi_Allap; }
                set { _Elektronikus_Kezbesitesi_Allap = value; }
            }


            private SqlGuid _Kovetkezo_Felelos_Id = SqlGuid.Null;

            /// <summary>
            /// Kovetkezo_Felelos_Id Base property </summary>
            public SqlGuid Kovetkezo_Felelos_Id
            {
                get { return _Kovetkezo_Felelos_Id; }
                set { _Kovetkezo_Felelos_Id = value; }
            }


            private SqlInt32 _UtolsoAlszam = SqlInt32.Null;

            /// <summary>
            /// UtolsoAlszam Base property </summary>
            public SqlInt32 UtolsoAlszam
            {
                get { return _UtolsoAlszam; }
                set { _UtolsoAlszam = value; }
            }


            private SqlInt32 _UtolsoSorszam = SqlInt32.Null;

            /// <summary>
            /// UtolsoSorszam Base property </summary>
            public SqlInt32 UtolsoSorszam
            {
                get { return _UtolsoSorszam; }
                set { _UtolsoSorszam = value; }
            }


            private SqlInt32 _IratSzam = SqlInt32.Null;

            /// <summary>
            /// IratSzam Base property </summary>
            public SqlInt32 IratSzam
            {
                get { return _IratSzam; }
                set { _IratSzam = value; }
            }


            private SqlString _ElintezesMod = SqlString.Null;

            /// <summary>
            /// ElintezesMod Base property </summary>
            public SqlString ElintezesMod
            {
                get { return _ElintezesMod; }
                set { _ElintezesMod = value; }
            }


            private SqlString _RegirendszerIktatoszam = SqlString.Null;

            /// <summary>
            /// RegirendszerIktatoszam Base property </summary>
            public SqlString RegirendszerIktatoszam
            {
                get { return _RegirendszerIktatoszam; }
                set { _RegirendszerIktatoszam = value; }
            }


            private SqlString _GeneraltTargy = SqlString.Null;

            /// <summary>
            /// GeneraltTargy Base property </summary>
            public SqlString GeneraltTargy
            {
                get { return _GeneraltTargy; }
                set { _GeneraltTargy = value; }
            }


            private SqlGuid _Cim_Id_Ugyindito = SqlGuid.Null;

            /// <summary>
            /// Cim_Id_Ugyindito  Base property </summary>
            public SqlGuid Cim_Id_Ugyindito
            {
                get { return _Cim_Id_Ugyindito; }
                set { _Cim_Id_Ugyindito = value; }
            }


            private SqlString _CimSTR_Ugyindito = SqlString.Null;

            /// <summary>
            /// CimSTR_Ugyindito  Base property </summary>
            public SqlString CimSTR_Ugyindito
            {
                get { return _CimSTR_Ugyindito; }
                set { _CimSTR_Ugyindito = value; }
            }


            private SqlString _LezarasOka = SqlString.Null;

            /// <summary>
            /// LezarasOka Base property </summary>
            public SqlString LezarasOka
            {
                get { return _LezarasOka; }
                set { _LezarasOka = value; }
            }


            private SqlString _FelfuggesztesOka = SqlString.Null;

            /// <summary>
            /// FelfuggesztesOka Base property </summary>
            public SqlString FelfuggesztesOka
            {
                get { return _FelfuggesztesOka; }
                set { _FelfuggesztesOka = value; }
            }


            private SqlGuid _IrattarId = SqlGuid.Null;

            /// <summary>
            /// IrattarId Base property </summary>
            public SqlGuid IrattarId
            {
                get { return _IrattarId; }
                set { _IrattarId = value; }
            }


            private SqlString _SkontroOka_Kod = SqlString.Null;

            /// <summary>
            /// SkontroOka_Kod Base property </summary>
            public SqlString SkontroOka_Kod
            {
                get { return _SkontroOka_Kod; }
                set { _SkontroOka_Kod = value; }
            }


            private SqlInt32 _UjOrzesiIdo = SqlInt32.Null;

            /// <summary>
            /// UjOrzesiIdo Base property </summary>
            public SqlInt32 UjOrzesiIdo
            {
                get { return _UjOrzesiIdo; }
                set { _UjOrzesiIdo = value; }
            }


            private SqlString _UjOrzesiIdoIdoegyseg = SqlString.Null;

            /// <summary>
            /// UjOrzesiIdoIdoegyseg Base property </summary>
            public SqlString UjOrzesiIdoIdoegyseg
            {
                get { return _UjOrzesiIdoIdoegyseg; }
                set { _UjOrzesiIdoIdoegyseg = value; }
            }


            private SqlDateTime _UgyintezesKezdete = SqlDateTime.Null;

            /// <summary>
            /// UgyintezesKezdete Base property </summary>
            public SqlDateTime UgyintezesKezdete
            {
                get { return _UgyintezesKezdete; }
                set { _UgyintezesKezdete = value; }
            }


            private SqlInt32 _FelfuggesztettNapokSzama = SqlInt32.Null;

            /// <summary>
            /// FelfuggesztettNapokSzama Base property </summary>
            public SqlInt32 FelfuggesztettNapokSzama
            {
                get { return _FelfuggesztettNapokSzama; }
                set { _FelfuggesztettNapokSzama = value; }
            }


            private SqlString _IntezesiIdo = SqlString.Null;

            /// <summary>
            /// IntezesiIdo Base property </summary>
            public SqlString IntezesiIdo
            {
                get { return _IntezesiIdo; }
                set { _IntezesiIdo = value; }
            }


            private SqlString _IntezesiIdoegyseg = SqlString.Null;

            /// <summary>
            /// IntezesiIdoegyseg Base property </summary>
            public SqlString IntezesiIdoegyseg
            {
                get { return _IntezesiIdoegyseg; }
                set { _IntezesiIdoegyseg = value; }
            }


            private SqlString _Ugy_Fajtaja = SqlString.Null;

            /// <summary>
            /// Ugy_Fajtaja Base property </summary>
            public SqlString Ugy_Fajtaja
            {
                get { return _Ugy_Fajtaja; }
                set { _Ugy_Fajtaja = value; }
            }


            private SqlGuid _SzignaloId = SqlGuid.Null;

            /// <summary>
            /// SzignaloId Base property </summary>
            public SqlGuid SzignaloId
            {
                get { return _SzignaloId; }
                set { _SzignaloId = value; }
            }


            private SqlDateTime _SzignalasIdeje = SqlDateTime.Null;

            /// <summary>
            /// SzignalasIdeje Base property </summary>
            public SqlDateTime SzignalasIdeje
            {
                get { return _SzignalasIdeje; }
                set { _SzignalasIdeje = value; }
            }


            private SqlString _ElteltIdo = SqlString.Null;

            /// <summary>
            /// ElteltIdo Base property </summary>
            public SqlString ElteltIdo
            {
                get { return _ElteltIdo; }
                set { _ElteltIdo = value; }
            }


            private SqlString _ElteltIdoIdoEgyseg = SqlString.Null;

            /// <summary>
            /// ElteltIdoIdoEgyseg Base property </summary>
            public SqlString ElteltIdoIdoEgyseg
            {
                get { return _ElteltIdoIdoEgyseg; }
                set { _ElteltIdoIdoEgyseg = value; }
            }


            private SqlInt32 _ElteltidoAllapot = SqlInt32.Null;

            /// <summary>
            /// ElteltidoAllapot Base property </summary>
            public SqlInt32 ElteltidoAllapot
            {
                get { return _ElteltidoAllapot; }
                set { _ElteltidoAllapot = value; }
            }


            private SqlDateTime _ElteltIdoUtolsoModositas = SqlDateTime.Null;

            /// <summary>
            /// ElteltIdoUtolsoModositas Base property </summary>
            public SqlDateTime ElteltIdoUtolsoModositas
            {
                get { return _ElteltIdoUtolsoModositas; }
                set { _ElteltIdoUtolsoModositas = value; }
            }


            private SqlString _SakkoraAllapot = SqlString.Null;

            /// <summary>
            /// SakkoraAllapot Base property </summary>
            public SqlString SakkoraAllapot
            {
                get { return _SakkoraAllapot; }
                set { _SakkoraAllapot = value; }
            }


            private SqlInt32 _HatralevoNapok = SqlInt32.Null;

            /// <summary>
            /// HatralevoNapok Base property </summary>
            public SqlInt32 HatralevoNapok
            {
                get { return _HatralevoNapok; }
                set { _HatralevoNapok = value; }
            }


            private SqlInt32 _HatralevoMunkaNapok = SqlInt32.Null;

            /// <summary>
            /// HatralevoMunkaNapok Base property </summary>
            public SqlInt32 HatralevoMunkaNapok
            {
                get { return _HatralevoMunkaNapok; }
                set { _HatralevoMunkaNapok = value; }
            }


            private SqlString _ElozoAllapot = SqlString.Null;

            /// <summary>
            /// ElozoAllapot Base property </summary>
            public SqlString ElozoAllapot
            {
                get { return _ElozoAllapot; }
                set { _ElozoAllapot = value; }
            }


            private SqlInt32 _Aktiv = SqlInt32.Null;

            /// <summary>
            /// Aktiv Base property </summary>
            public SqlInt32 Aktiv
            {
                get { return _Aktiv; }
                set { _Aktiv = value; }
            }

            private SqlDouble _IrattarHelyfoglalas = SqlDouble.Null;

            /// <summary>
            /// IrattarHelyfoglalas Base property </summary>
            public SqlDouble IrattarHelyfoglalas
            {
                get { return _IrattarHelyfoglalas; }
                set { _IrattarHelyfoglalas = value; }
            }

            private SqlDateTime _ErvKezd = SqlDateTime.Null;

            /// <summary>
            /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }


            private SqlDateTime _ErvVege = SqlDateTime.Null;

            /// <summary>
            /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }
        }
    }
}