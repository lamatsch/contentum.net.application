
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_UgyiratObjKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_UgyiratObjKapcsolatok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_UgyiratObjKapcsolatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _KapcsolatTipus = true;
               public bool KapcsolatTipus
               {
                   get { return _KapcsolatTipus; }
                   set { _KapcsolatTipus = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _Kezi = true;
               public bool Kezi
               {
                   get { return _Kezi; }
                   set { _Kezi = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Elozmeny = true;
               public bool Obj_Id_Elozmeny
               {
                   get { return _Obj_Id_Elozmeny; }
                   set { _Obj_Id_Elozmeny = value; }
               }
                                                            
                                 
               private bool _Obj_Tip_Id_Elozmeny = true;
               public bool Obj_Tip_Id_Elozmeny
               {
                   get { return _Obj_Tip_Id_Elozmeny; }
                   set { _Obj_Tip_Id_Elozmeny = value; }
               }
                                                            
                                 
               private bool _Obj_Type_Elozmeny = true;
               public bool Obj_Type_Elozmeny
               {
                   get { return _Obj_Type_Elozmeny; }
                   set { _Obj_Type_Elozmeny = value; }
               }
                                                            
                                 
               private bool _Obj_Id_Kapcsolt = true;
               public bool Obj_Id_Kapcsolt
               {
                   get { return _Obj_Id_Kapcsolt; }
                   set { _Obj_Id_Kapcsolt = value; }
               }
                                                            
                                 
               private bool _Obj_Tip_Id_Kapcsolt = true;
               public bool Obj_Tip_Id_Kapcsolt
               {
                   get { return _Obj_Tip_Id_Kapcsolt; }
                   set { _Obj_Tip_Id_Kapcsolt = value; }
               }
                                                            
                                 
               private bool _Obj_Type_Kapcsolt = true;
               public bool Obj_Type_Kapcsolt
               {
                   get { return _Obj_Type_Kapcsolt; }
                   set { _Obj_Type_Kapcsolt = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   KapcsolatTipus = Value;               
                    
                   Leiras = Value;               
                    
                   Kezi = Value;               
                    
                   Obj_Id_Elozmeny = Value;               
                    
                   Obj_Tip_Id_Elozmeny = Value;               
                    
                   Obj_Type_Elozmeny = Value;               
                    
                   Obj_Id_Kapcsolt = Value;               
                    
                   Obj_Tip_Id_Kapcsolt = Value;               
                    
                   Obj_Type_Kapcsolt = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_UgyiratObjKapcsolatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _KapcsolatTipus = SqlString.Null;
           
        /// <summary>
        /// KapcsolatTipus Base property </summary>
            public SqlString KapcsolatTipus
            {
                get { return _KapcsolatTipus; }
                set { _KapcsolatTipus = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlChars _Kezi = SqlChars.Null;
           
        /// <summary>
        /// Kezi Base property </summary>
            public SqlChars Kezi
            {
                get { return _Kezi; }
                set { _Kezi = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Elozmeny = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Elozmeny Base property </summary>
            public SqlGuid Obj_Id_Elozmeny
            {
                get { return _Obj_Id_Elozmeny; }
                set { _Obj_Id_Elozmeny = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Tip_Id_Elozmeny = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Tip_Id_Elozmeny Base property </summary>
            public SqlGuid Obj_Tip_Id_Elozmeny
            {
                get { return _Obj_Tip_Id_Elozmeny; }
                set { _Obj_Tip_Id_Elozmeny = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Type_Elozmeny = SqlString.Null;
           
        /// <summary>
        /// Obj_Type_Elozmeny Base property </summary>
            public SqlString Obj_Type_Elozmeny
            {
                get { return _Obj_Type_Elozmeny; }
                set { _Obj_Type_Elozmeny = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Id_Kapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Id_Kapcsolt Base property </summary>
            public SqlGuid Obj_Id_Kapcsolt
            {
                get { return _Obj_Id_Kapcsolt; }
                set { _Obj_Id_Kapcsolt = value; }                                                        
            }        
                   
           
        private SqlGuid _Obj_Tip_Id_Kapcsolt = SqlGuid.Null;
           
        /// <summary>
        /// Obj_Tip_Id_Kapcsolt Base property </summary>
            public SqlGuid Obj_Tip_Id_Kapcsolt
            {
                get { return _Obj_Tip_Id_Kapcsolt; }
                set { _Obj_Tip_Id_Kapcsolt = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Type_Kapcsolt = SqlString.Null;
           
        /// <summary>
        /// Obj_Type_Kapcsolt Base property </summary>
            public SqlString Obj_Type_Kapcsolt
            {
                get { return _Obj_Type_Kapcsolt; }
                set { _Obj_Type_Kapcsolt = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}