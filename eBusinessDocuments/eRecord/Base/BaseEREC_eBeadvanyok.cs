
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eBeadvanyok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_eBeadvanyok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_eBeadvanyokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Irany = true;
               public bool Irany
               {
                   get { return _Irany; }
                   set { _Irany = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _KuldoRendszer = true;
               public bool KuldoRendszer
               {
                   get { return _KuldoRendszer; }
                   set { _KuldoRendszer = value; }
               }
                                                            
                                 
               private bool _UzenetTipusa = true;
               public bool UzenetTipusa
               {
                   get { return _UzenetTipusa; }
                   set { _UzenetTipusa = value; }
               }
                                                            
                                 
               private bool _FeladoTipusa = true;
               public bool FeladoTipusa
               {
                   get { return _FeladoTipusa; }
                   set { _FeladoTipusa = value; }
               }
                                                            
                                 
               private bool _PartnerKapcsolatiKod = true;
               public bool PartnerKapcsolatiKod
               {
                   get { return _PartnerKapcsolatiKod; }
                   set { _PartnerKapcsolatiKod = value; }
               }
                                                            
                                 
               private bool _PartnerNev = true;
               public bool PartnerNev
               {
                   get { return _PartnerNev; }
                   set { _PartnerNev = value; }
               }
                                                            
                                 
               private bool _PartnerEmail = true;
               public bool PartnerEmail
               {
                   get { return _PartnerEmail; }
                   set { _PartnerEmail = value; }
               }
                                                            
                                 
               private bool _PartnerRovidNev = true;
               public bool PartnerRovidNev
               {
                   get { return _PartnerRovidNev; }
                   set { _PartnerRovidNev = value; }
               }
                                                            
                                 
               private bool _PartnerMAKKod = true;
               public bool PartnerMAKKod
               {
                   get { return _PartnerMAKKod; }
                   set { _PartnerMAKKod = value; }
               }
                                                            
                                 
               private bool _PartnerKRID = true;
               public bool PartnerKRID
               {
                   get { return _PartnerKRID; }
                   set { _PartnerKRID = value; }
               }
                                                            
                                 
               private bool _Partner_Id = true;
               public bool Partner_Id
               {
                   get { return _Partner_Id; }
                   set { _Partner_Id = value; }
               }
                                                            
                                 
               private bool _Cim_Id = true;
               public bool Cim_Id
               {
                   get { return _Cim_Id; }
                   set { _Cim_Id = value; }
               }
                                                            
                                 
               private bool _KR_HivatkozasiSzam = true;
               public bool KR_HivatkozasiSzam
               {
                   get { return _KR_HivatkozasiSzam; }
                   set { _KR_HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _KR_ErkeztetesiSzam = true;
               public bool KR_ErkeztetesiSzam
               {
                   get { return _KR_ErkeztetesiSzam; }
                   set { _KR_ErkeztetesiSzam = value; }
               }
                                                            
                                 
               private bool _Contentum_HivatkozasiSzam = true;
               public bool Contentum_HivatkozasiSzam
               {
                   get { return _Contentum_HivatkozasiSzam; }
                   set { _Contentum_HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _PR_HivatkozasiSzam = true;
               public bool PR_HivatkozasiSzam
               {
                   get { return _PR_HivatkozasiSzam; }
                   set { _PR_HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _PR_ErkeztetesiSzam = true;
               public bool PR_ErkeztetesiSzam
               {
                   get { return _PR_ErkeztetesiSzam; }
                   set { _PR_ErkeztetesiSzam = value; }
               }
                                                            
                                 
               private bool _KR_DokTipusHivatal = true;
               public bool KR_DokTipusHivatal
               {
                   get { return _KR_DokTipusHivatal; }
                   set { _KR_DokTipusHivatal = value; }
               }
                                                            
                                 
               private bool _KR_DokTipusAzonosito = true;
               public bool KR_DokTipusAzonosito
               {
                   get { return _KR_DokTipusAzonosito; }
                   set { _KR_DokTipusAzonosito = value; }
               }
                                                            
                                 
               private bool _KR_DokTipusLeiras = true;
               public bool KR_DokTipusLeiras
               {
                   get { return _KR_DokTipusLeiras; }
                   set { _KR_DokTipusLeiras = value; }
               }
                                                            
                                 
               private bool _KR_Megjegyzes = true;
               public bool KR_Megjegyzes
               {
                   get { return _KR_Megjegyzes; }
                   set { _KR_Megjegyzes = value; }
               }
                                                            
                                 
               private bool _KR_ErvenyessegiDatum = true;
               public bool KR_ErvenyessegiDatum
               {
                   get { return _KR_ErvenyessegiDatum; }
                   set { _KR_ErvenyessegiDatum = value; }
               }
                                                            
                                 
               private bool _KR_ErkeztetesiDatum = true;
               public bool KR_ErkeztetesiDatum
               {
                   get { return _KR_ErkeztetesiDatum; }
                   set { _KR_ErkeztetesiDatum = value; }
               }
                                                            
                                 
               private bool _KR_FileNev = true;
               public bool KR_FileNev
               {
                   get { return _KR_FileNev; }
                   set { _KR_FileNev = value; }
               }
                                                            
                                 
               private bool _KR_Kezbesitettseg = true;
               public bool KR_Kezbesitettseg
               {
                   get { return _KR_Kezbesitettseg; }
                   set { _KR_Kezbesitettseg = value; }
               }
                                                            
                                 
               private bool _KR_Idopecset = true;
               public bool KR_Idopecset
               {
                   get { return _KR_Idopecset; }
                   set { _KR_Idopecset = value; }
               }
                                                            
                                 
               private bool _KR_Valasztitkositas = true;
               public bool KR_Valasztitkositas
               {
                   get { return _KR_Valasztitkositas; }
                   set { _KR_Valasztitkositas = value; }
               }
                                                            
                                 
               private bool _KR_Valaszutvonal = true;
               public bool KR_Valaszutvonal
               {
                   get { return _KR_Valaszutvonal; }
                   set { _KR_Valaszutvonal = value; }
               }
                                                            
                                 
               private bool _KR_Rendszeruzenet = true;
               public bool KR_Rendszeruzenet
               {
                   get { return _KR_Rendszeruzenet; }
                   set { _KR_Rendszeruzenet = value; }
               }
                                                            
                                 
               private bool _KR_Tarterulet = true;
               public bool KR_Tarterulet
               {
                   get { return _KR_Tarterulet; }
                   set { _KR_Tarterulet = value; }
               }
                                                            
                                 
               private bool _KR_ETertiveveny = true;
               public bool KR_ETertiveveny
               {
                   get { return _KR_ETertiveveny; }
                   set { _KR_ETertiveveny = value; }
               }
                                                            
                                 
               private bool _KR_Lenyomat = true;
               public bool KR_Lenyomat
               {
                   get { return _KR_Lenyomat; }
                   set { _KR_Lenyomat = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _IratPeldany_Id = true;
               public bool IratPeldany_Id
               {
                   get { return _IratPeldany_Id; }
                   set { _IratPeldany_Id = value; }
               }
                                                            
                                 
               private bool _Cel = true;
               public bool Cel
               {
                   get { return _Cel; }
                   set { _Cel = value; }
               }
                                                            
                                 
               private bool _PR_Parameterek = true;
               public bool PR_Parameterek
               {
                   get { return _PR_Parameterek; }
                   set { _PR_Parameterek = value; }
               }
                                                            
                                 
               private bool _KR_Fiok = true;
               public bool KR_Fiok
               {
                   get { return _KR_Fiok; }
                   set { _KR_Fiok = value; }
               }
                                                            
                                 
               private bool _FeldolgozasStatusz = true;
               public bool FeldolgozasStatusz
               {
                   get { return _FeldolgozasStatusz; }
                   set { _FeldolgozasStatusz = value; }
               }
                                                            
                                 
               private bool _FeldolgozasiHiba = true;
               public bool FeldolgozasiHiba
               {
                   get { return _FeldolgozasiHiba; }
                   set { _FeldolgozasiHiba = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Irany = Value;               
                    
                   Allapot = Value;               
                    
                   KuldoRendszer = Value;               
                    
                   UzenetTipusa = Value;               
                    
                   FeladoTipusa = Value;               
                    
                   PartnerKapcsolatiKod = Value;               
                    
                   PartnerNev = Value;               
                    
                   PartnerEmail = Value;               
                    
                   PartnerRovidNev = Value;               
                    
                   PartnerMAKKod = Value;               
                    
                   PartnerKRID = Value;               
                    
                   Partner_Id = Value;               
                    
                   Cim_Id = Value;               
                    
                   KR_HivatkozasiSzam = Value;               
                    
                   KR_ErkeztetesiSzam = Value;               
                    
                   Contentum_HivatkozasiSzam = Value;               
                    
                   PR_HivatkozasiSzam = Value;               
                    
                   PR_ErkeztetesiSzam = Value;               
                    
                   KR_DokTipusHivatal = Value;               
                    
                   KR_DokTipusAzonosito = Value;               
                    
                   KR_DokTipusLeiras = Value;               
                    
                   KR_Megjegyzes = Value;               
                    
                   KR_ErvenyessegiDatum = Value;               
                    
                   KR_ErkeztetesiDatum = Value;               
                    
                   KR_FileNev = Value;               
                    
                   KR_Kezbesitettseg = Value;               
                    
                   KR_Idopecset = Value;               
                    
                   KR_Valasztitkositas = Value;               
                    
                   KR_Valaszutvonal = Value;               
                    
                   KR_Rendszeruzenet = Value;               
                    
                   KR_Tarterulet = Value;               
                    
                   KR_ETertiveveny = Value;               
                    
                   KR_Lenyomat = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   IratPeldany_Id = Value;               
                    
                   Cel = Value;               
                    
                   PR_Parameterek = Value;               
                    
                   KR_Fiok = Value;               
                    
                   FeldolgozasStatusz = Value;               
                    
                   FeldolgozasiHiba = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_eBeadvanyokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlChars _Irany = SqlChars.Null;
           
        /// <summary>
        /// Irany Base property </summary>
            public SqlChars Irany
            {
                get { return _Irany; }
                set { _Irany = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _KuldoRendszer = SqlString.Null;
           
        /// <summary>
        /// KuldoRendszer Base property </summary>
            public SqlString KuldoRendszer
            {
                get { return _KuldoRendszer; }
                set { _KuldoRendszer = value; }                                                        
            }        
                   
           
        private SqlString _UzenetTipusa = SqlString.Null;
           
        /// <summary>
        /// UzenetTipusa Base property </summary>
            public SqlString UzenetTipusa
            {
                get { return _UzenetTipusa; }
                set { _UzenetTipusa = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeladoTipusa = SqlInt32.Null;
           
        /// <summary>
        /// FeladoTipusa Base property </summary>
            public SqlInt32 FeladoTipusa
            {
                get { return _FeladoTipusa; }
                set { _FeladoTipusa = value; }                                                        
            }        
                   
           
        private SqlString _PartnerKapcsolatiKod = SqlString.Null;
           
        /// <summary>
        /// PartnerKapcsolatiKod Base property </summary>
            public SqlString PartnerKapcsolatiKod
            {
                get { return _PartnerKapcsolatiKod; }
                set { _PartnerKapcsolatiKod = value; }                                                        
            }        
                   
           
        private SqlString _PartnerNev = SqlString.Null;
           
        /// <summary>
        /// PartnerNev Base property </summary>
            public SqlString PartnerNev
            {
                get { return _PartnerNev; }
                set { _PartnerNev = value; }                                                        
            }        
                   
           
        private SqlString _PartnerEmail = SqlString.Null;
           
        /// <summary>
        /// PartnerEmail Base property </summary>
            public SqlString PartnerEmail
            {
                get { return _PartnerEmail; }
                set { _PartnerEmail = value; }                                                        
            }        
                   
           
        private SqlString _PartnerRovidNev = SqlString.Null;
           
        /// <summary>
        /// PartnerRovidNev Base property </summary>
            public SqlString PartnerRovidNev
            {
                get { return _PartnerRovidNev; }
                set { _PartnerRovidNev = value; }                                                        
            }        
                   
           
        private SqlString _PartnerMAKKod = SqlString.Null;
           
        /// <summary>
        /// PartnerMAKKod Base property </summary>
            public SqlString PartnerMAKKod
            {
                get { return _PartnerMAKKod; }
                set { _PartnerMAKKod = value; }                                                        
            }        
                   
           
        private SqlString _PartnerKRID = SqlString.Null;
           
        /// <summary>
        /// PartnerKRID Base property </summary>
            public SqlString PartnerKRID
            {
                get { return _PartnerKRID; }
                set { _PartnerKRID = value; }                                                        
            }        
                   
           
        private SqlGuid _Partner_Id = SqlGuid.Null;
           
        /// <summary>
        /// Partner_Id Base property </summary>
            public SqlGuid Partner_Id
            {
                get { return _Partner_Id; }
                set { _Partner_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Cim_Id = SqlGuid.Null;
           
        /// <summary>
        /// Cim_Id Base property </summary>
            public SqlGuid Cim_Id
            {
                get { return _Cim_Id; }
                set { _Cim_Id = value; }                                                        
            }        
                   
           
        private SqlString _KR_HivatkozasiSzam = SqlString.Null;
           
        /// <summary>
        /// KR_HivatkozasiSzam Base property </summary>
            public SqlString KR_HivatkozasiSzam
            {
                get { return _KR_HivatkozasiSzam; }
                set { _KR_HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlString _KR_ErkeztetesiSzam = SqlString.Null;
           
        /// <summary>
        /// KR_ErkeztetesiSzam Base property </summary>
            public SqlString KR_ErkeztetesiSzam
            {
                get { return _KR_ErkeztetesiSzam; }
                set { _KR_ErkeztetesiSzam = value; }                                                        
            }        
                   
           
        private SqlGuid _Contentum_HivatkozasiSzam = SqlGuid.Null;
           
        /// <summary>
        /// Contentum_HivatkozasiSzam Base property </summary>
            public SqlGuid Contentum_HivatkozasiSzam
            {
                get { return _Contentum_HivatkozasiSzam; }
                set { _Contentum_HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlString _PR_HivatkozasiSzam = SqlString.Null;
           
        /// <summary>
        /// PR_HivatkozasiSzam Base property </summary>
            public SqlString PR_HivatkozasiSzam
            {
                get { return _PR_HivatkozasiSzam; }
                set { _PR_HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlString _PR_ErkeztetesiSzam = SqlString.Null;
           
        /// <summary>
        /// PR_ErkeztetesiSzam Base property </summary>
            public SqlString PR_ErkeztetesiSzam
            {
                get { return _PR_ErkeztetesiSzam; }
                set { _PR_ErkeztetesiSzam = value; }                                                        
            }        
                   
           
        private SqlString _KR_DokTipusHivatal = SqlString.Null;
           
        /// <summary>
        /// KR_DokTipusHivatal Base property </summary>
            public SqlString KR_DokTipusHivatal
            {
                get { return _KR_DokTipusHivatal; }
                set { _KR_DokTipusHivatal = value; }                                                        
            }        
                   
           
        private SqlString _KR_DokTipusAzonosito = SqlString.Null;
           
        /// <summary>
        /// KR_DokTipusAzonosito Base property </summary>
            public SqlString KR_DokTipusAzonosito
            {
                get { return _KR_DokTipusAzonosito; }
                set { _KR_DokTipusAzonosito = value; }                                                        
            }        
                   
           
        private SqlString _KR_DokTipusLeiras = SqlString.Null;
           
        /// <summary>
        /// KR_DokTipusLeiras Base property </summary>
            public SqlString KR_DokTipusLeiras
            {
                get { return _KR_DokTipusLeiras; }
                set { _KR_DokTipusLeiras = value; }                                                        
            }        
                   
           
        private SqlString _KR_Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// KR_Megjegyzes Base property </summary>
            public SqlString KR_Megjegyzes
            {
                get { return _KR_Megjegyzes; }
                set { _KR_Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlDateTime _KR_ErvenyessegiDatum = SqlDateTime.Null;
           
        /// <summary>
        /// KR_ErvenyessegiDatum Base property </summary>
            public SqlDateTime KR_ErvenyessegiDatum
            {
                get { return _KR_ErvenyessegiDatum; }
                set { _KR_ErvenyessegiDatum = value; }                                                        
            }        
                   
           
        private SqlDateTime _KR_ErkeztetesiDatum = SqlDateTime.Null;
           
        /// <summary>
        /// KR_ErkeztetesiDatum Base property </summary>
            public SqlDateTime KR_ErkeztetesiDatum
            {
                get { return _KR_ErkeztetesiDatum; }
                set { _KR_ErkeztetesiDatum = value; }                                                        
            }        
                   
           
        private SqlString _KR_FileNev = SqlString.Null;
           
        /// <summary>
        /// KR_FileNev Base property </summary>
            public SqlString KR_FileNev
            {
                get { return _KR_FileNev; }
                set { _KR_FileNev = value; }                                                        
            }        
                   
           
        private SqlInt32 _KR_Kezbesitettseg = SqlInt32.Null;
           
        /// <summary>
        /// KR_Kezbesitettseg Base property </summary>
            public SqlInt32 KR_Kezbesitettseg
            {
                get { return _KR_Kezbesitettseg; }
                set { _KR_Kezbesitettseg = value; }                                                        
            }        
                   
           
        private SqlString _KR_Idopecset = SqlString.Null;
           
        /// <summary>
        /// KR_Idopecset Base property </summary>
            public SqlString KR_Idopecset
            {
                get { return _KR_Idopecset; }
                set { _KR_Idopecset = value; }                                                        
            }        
                   
           
        private SqlChars _KR_Valasztitkositas = SqlChars.Null;
           
        /// <summary>
        /// KR_Valasztitkositas Base property </summary>
            public SqlChars KR_Valasztitkositas
            {
                get { return _KR_Valasztitkositas; }
                set { _KR_Valasztitkositas = value; }                                                        
            }        
                   
           
        private SqlInt32 _KR_Valaszutvonal = SqlInt32.Null;
           
        /// <summary>
        /// KR_Valaszutvonal Base property </summary>
            public SqlInt32 KR_Valaszutvonal
            {
                get { return _KR_Valaszutvonal; }
                set { _KR_Valaszutvonal = value; }                                                        
            }        
                   
           
        private SqlChars _KR_Rendszeruzenet = SqlChars.Null;
           
        /// <summary>
        /// KR_Rendszeruzenet Base property </summary>
            public SqlChars KR_Rendszeruzenet
            {
                get { return _KR_Rendszeruzenet; }
                set { _KR_Rendszeruzenet = value; }                                                        
            }        
                   
           
        private SqlInt32 _KR_Tarterulet = SqlInt32.Null;
           
        /// <summary>
        /// KR_Tarterulet Base property </summary>
            public SqlInt32 KR_Tarterulet
            {
                get { return _KR_Tarterulet; }
                set { _KR_Tarterulet = value; }                                                        
            }        
                   
           
        private SqlChars _KR_ETertiveveny = SqlChars.Null;
           
        /// <summary>
        /// KR_ETertiveveny Base property </summary>
            public SqlChars KR_ETertiveveny
            {
                get { return _KR_ETertiveveny; }
                set { _KR_ETertiveveny = value; }                                                        
            }        
                   
           
        private SqlString _KR_Lenyomat = SqlString.Null;
           
        /// <summary>
        /// KR_Lenyomat Base property </summary>
            public SqlString KR_Lenyomat
            {
                get { return _KR_Lenyomat; }
                set { _KR_Lenyomat = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IratPeldany_Id = SqlGuid.Null;
           
        /// <summary>
        /// IratPeldany_Id Base property </summary>
            public SqlGuid IratPeldany_Id
            {
                get { return _IratPeldany_Id; }
                set { _IratPeldany_Id = value; }                                                        
            }        
                   
           
        private SqlString _Cel = SqlString.Null;
           
        /// <summary>
        /// Cel Base property </summary>
            public SqlString Cel
            {
                get { return _Cel; }
                set { _Cel = value; }                                                        
            }        
                   
           
        private SqlString _PR_Parameterek = SqlString.Null;
           
        /// <summary>
        /// PR_Parameterek Base property </summary>
            public SqlString PR_Parameterek
            {
                get { return _PR_Parameterek; }
                set { _PR_Parameterek = value; }                                                        
            }        
                   
           
        private SqlString _KR_Fiok = SqlString.Null;
           
        /// <summary>
        /// KR_Fiok Base property </summary>
            public SqlString KR_Fiok
            {
                get { return _KR_Fiok; }
                set { _KR_Fiok = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeldolgozasStatusz = SqlInt32.Null;
           
        /// <summary>
        /// FeldolgozasStatusz Base property </summary>
            public SqlInt32 FeldolgozasStatusz
            {
                get { return _FeldolgozasStatusz; }
                set { _FeldolgozasStatusz = value; }                                                        
            }        
                   
           
        private SqlString _FeldolgozasiHiba = SqlString.Null;
           
        /// <summary>
        /// FeldolgozasiHiba Base property </summary>
            public SqlString FeldolgozasiHiba
            {
                get { return _FeldolgozasiHiba; }
                set { _FeldolgozasiHiba = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}