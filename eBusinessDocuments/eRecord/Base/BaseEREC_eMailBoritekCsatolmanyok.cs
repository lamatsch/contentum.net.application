
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailBoritekCsatolmanyok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_eMailBoritekCsatolmanyok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_eMailBoritekCsatolmanyokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _eMailBoritek_Id = true;
               public bool eMailBoritek_Id
               {
                   get { return _eMailBoritek_Id; }
                   set { _eMailBoritek_Id = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Tomoritve = true;
               public bool Tomoritve
               {
                   get { return _Tomoritve; }
                   set { _Tomoritve = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   eMailBoritek_Id = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   Nev = Value;               
                    
                   Tomoritve = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_eMailBoritekCsatolmanyokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _eMailBoritek_Id = SqlGuid.Null;
           
        /// <summary>
        /// eMailBoritek_Id Base property </summary>
            public SqlGuid eMailBoritek_Id
            {
                get { return _eMailBoritek_Id; }
                set { _eMailBoritek_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlChars _Tomoritve = SqlChars.Null;
           
        /// <summary>
        /// Tomoritve Base property </summary>
            public SqlChars Tomoritve
            {
                get { return _Tomoritve; }
                set { _Tomoritve = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}