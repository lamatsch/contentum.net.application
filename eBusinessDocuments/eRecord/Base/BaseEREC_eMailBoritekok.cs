
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailBoritekok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_eMailBoritekok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_eMailBoritekokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Felado = true;
               public bool Felado
               {
                   get { return _Felado; }
                   set { _Felado = value; }
               }
                                                            
                                 
               private bool _Cimzett = true;
               public bool Cimzett
               {
                   get { return _Cimzett; }
                   set { _Cimzett = value; }
               }
                                                            
                                 
               private bool _CC = true;
               public bool CC
               {
                   get { return _CC; }
                   set { _CC = value; }
               }
                                                            
                                 
               private bool _Targy = true;
               public bool Targy
               {
                   get { return _Targy; }
                   set { _Targy = value; }
               }
                                                            
                                 
               private bool _FeladasDatuma = true;
               public bool FeladasDatuma
               {
                   get { return _FeladasDatuma; }
                   set { _FeladasDatuma = value; }
               }
                                                            
                                 
               private bool _ErkezesDatuma = true;
               public bool ErkezesDatuma
               {
                   get { return _ErkezesDatuma; }
                   set { _ErkezesDatuma = value; }
               }
                                                            
                                 
               private bool _Fontossag = true;
               public bool Fontossag
               {
                   get { return _Fontossag; }
                   set { _Fontossag = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _DigitalisAlairas = true;
               public bool DigitalisAlairas
               {
                   get { return _DigitalisAlairas; }
                   set { _DigitalisAlairas = value; }
               }
                                                            
                                 
               private bool _Uzenet = true;
               public bool Uzenet
               {
                   get { return _Uzenet; }
                   set { _Uzenet = value; }
               }
                                                            
                                 
               private bool _EmailForras = true;
               public bool EmailForras
               {
                   get { return _EmailForras; }
                   set { _EmailForras = value; }
               }
                                                            
                                 
               private bool _EmailGuid = true;
               public bool EmailGuid
               {
                   get { return _EmailGuid; }
                   set { _EmailGuid = value; }
               }
                                                            
                                 
               private bool _FeldolgozasIdo = true;
               public bool FeldolgozasIdo
               {
                   get { return _FeldolgozasIdo; }
                   set { _FeldolgozasIdo = value; }
               }
                                                            
                                 
               private bool _ForrasTipus = true;
               public bool ForrasTipus
               {
                   get { return _ForrasTipus; }
                   set { _ForrasTipus = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Felado = Value;               
                    
                   Cimzett = Value;               
                    
                   CC = Value;               
                    
                   Targy = Value;               
                    
                   FeladasDatuma = Value;               
                    
                   ErkezesDatuma = Value;               
                    
                   Fontossag = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   DigitalisAlairas = Value;               
                    
                   Uzenet = Value;               
                    
                   EmailForras = Value;               
                    
                   EmailGuid = Value;               
                    
                   FeldolgozasIdo = Value;               
                    
                   ForrasTipus = Value;               
                    
                   Allapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_eMailBoritekokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Felado = SqlString.Null;
           
        /// <summary>
        /// Felado Base property </summary>
            public SqlString Felado
            {
                get { return _Felado; }
                set { _Felado = value; }                                                        
            }        
                   
           
        private SqlString _Cimzett = SqlString.Null;
           
        /// <summary>
        /// Cimzett Base property </summary>
            public SqlString Cimzett
            {
                get { return _Cimzett; }
                set { _Cimzett = value; }                                                        
            }        
                   
           
        private SqlString _CC = SqlString.Null;
           
        /// <summary>
        /// CC Base property </summary>
            public SqlString CC
            {
                get { return _CC; }
                set { _CC = value; }                                                        
            }        
                   
           
        private SqlString _Targy = SqlString.Null;
           
        /// <summary>
        /// Targy Base property </summary>
            public SqlString Targy
            {
                get { return _Targy; }
                set { _Targy = value; }                                                        
            }        
                   
           
        private SqlDateTime _FeladasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// FeladasDatuma Base property </summary>
            public SqlDateTime FeladasDatuma
            {
                get { return _FeladasDatuma; }
                set { _FeladasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErkezesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// ErkezesDatuma Base property </summary>
            public SqlDateTime ErkezesDatuma
            {
                get { return _ErkezesDatuma; }
                set { _ErkezesDatuma = value; }                                                        
            }        
                   
           
        private SqlString _Fontossag = SqlString.Null;
           
        /// <summary>
        /// Fontossag Base property </summary>
            public SqlString Fontossag
            {
                get { return _Fontossag; }
                set { _Fontossag = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlString _DigitalisAlairas = SqlString.Null;
           
        /// <summary>
        /// DigitalisAlairas Base property </summary>
            public SqlString DigitalisAlairas
            {
                get { return _DigitalisAlairas; }
                set { _DigitalisAlairas = value; }                                                        
            }        
                   
           
        private SqlString _Uzenet = SqlString.Null;
           
        /// <summary>
        /// Uzenet Base property </summary>
            public SqlString Uzenet
            {
                get { return _Uzenet; }
                set { _Uzenet = value; }                                                        
            }        
                   
           
        private SqlString _EmailForras = SqlString.Null;
           
        /// <summary>
        /// EmailForras Base property </summary>
            public SqlString EmailForras
            {
                get { return _EmailForras; }
                set { _EmailForras = value; }                                                        
            }        
                   
           
        private SqlString _EmailGuid = SqlString.Null;
           
        /// <summary>
        /// EmailGuid Base property </summary>
            public SqlString EmailGuid
            {
                get { return _EmailGuid; }
                set { _EmailGuid = value; }                                                        
            }        
                   
           
        private SqlDateTime _FeldolgozasIdo = SqlDateTime.Null;
           
        /// <summary>
        /// FeldolgozasIdo Base property </summary>
            public SqlDateTime FeldolgozasIdo
            {
                get { return _FeldolgozasIdo; }
                set { _FeldolgozasIdo = value; }                                                        
            }        
                   
           
        private SqlString _ForrasTipus = SqlString.Null;
           
        /// <summary>
        /// ForrasTipus Base property </summary>
            public SqlString ForrasTipus
            {
                get { return _ForrasTipus; }
                set { _ForrasTipus = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}