
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailFiokok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseEREC_eMailFiokok
    {
        [System.Xml.Serialization.XmlType("BaseEREC_eMailFiokokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _UserNev = true;
               public bool UserNev
               {
                   get { return _UserNev; }
                   set { _UserNev = value; }
               }
                                                            
                                 
               private bool _Csoportok_Id = true;
               public bool Csoportok_Id
               {
                   get { return _Csoportok_Id; }
                   set { _Csoportok_Id = value; }
               }
                                                            
                                 
               private bool _Jelszo = true;
               public bool Jelszo
               {
                   get { return _Jelszo; }
                   set { _Jelszo = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _NapiMax = true;
               public bool NapiMax
               {
                   get { return _NapiMax; }
                   set { _NapiMax = value; }
               }
                                                            
                                 
               private bool _NapiTeny = true;
               public bool NapiTeny
               {
                   get { return _NapiTeny; }
                   set { _NapiTeny = value; }
               }
                                                            
                                 
               private bool _EmailCim = true;
               public bool EmailCim
               {
                   get { return _EmailCim; }
                   set { _EmailCim = value; }
               }
                                                            
                                 
               private bool _Modul_Id = true;
               public bool Modul_Id
               {
                   get { return _Modul_Id; }
                   set { _Modul_Id = value; }
               }
                                                            
                                 
               private bool _MappaNev = true;
               public bool MappaNev
               {
                   get { return _MappaNev; }
                   set { _MappaNev = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Nev = Value;               
                    
                   UserNev = Value;               
                    
                   Csoportok_Id = Value;               
                    
                   Jelszo = Value;               
                    
                   Tipus = Value;               
                    
                   NapiMax = Value;               
                    
                   NapiTeny = Value;               
                    
                   EmailCim = Value;               
                    
                   Modul_Id = Value;               
                    
                   MappaNev = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseEREC_eMailFiokokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _UserNev = SqlString.Null;
           
        /// <summary>
        /// UserNev Base property </summary>
            public SqlString UserNev
            {
                get { return _UserNev; }
                set { _UserNev = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoportok_Id = SqlGuid.Null;
           
        /// <summary>
        /// Csoportok_Id Base property </summary>
            public SqlGuid Csoportok_Id
            {
                get { return _Csoportok_Id; }
                set { _Csoportok_Id = value; }                                                        
            }        
                   
           
        private SqlString _Jelszo = SqlString.Null;
           
        /// <summary>
        /// Jelszo Base property </summary>
            public SqlString Jelszo
            {
                get { return _Jelszo; }
                set { _Jelszo = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlInt32 _NapiMax = SqlInt32.Null;
           
        /// <summary>
        /// NapiMax Base property </summary>
            public SqlInt32 NapiMax
            {
                get { return _NapiMax; }
                set { _NapiMax = value; }                                                        
            }        
                   
           
        private SqlInt32 _NapiTeny = SqlInt32.Null;
           
        /// <summary>
        /// NapiTeny Base property </summary>
            public SqlInt32 NapiTeny
            {
                get { return _NapiTeny; }
                set { _NapiTeny = value; }                                                        
            }        
                   
           
        private SqlString _EmailCim = SqlString.Null;
           
        /// <summary>
        /// EmailCim Base property </summary>
            public SqlString EmailCim
            {
                get { return _EmailCim; }
                set { _EmailCim = value; }                                                        
            }        
                   
           
        private SqlGuid _Modul_Id = SqlGuid.Null;
           
        /// <summary>
        /// Modul_Id Base property </summary>
            public SqlGuid Modul_Id
            {
                get { return _Modul_Id; }
                set { _Modul_Id = value; }                                                        
            }        
                   
           
        private SqlString _MappaNev = SqlString.Null;
           
        /// <summary>
        /// MappaNev Base property </summary>
            public SqlString MappaNev
            {
                get { return _MappaNev; }
                set { _MappaNev = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}