
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// HKP_DokumentumAdatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseHKP_DokumentumAdatok
    {
        [System.Xml.Serialization.XmlType("BaseHKP_DokumentumAdatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Irany = true;
               public bool Irany
               {
                   get { return _Irany; }
                   set { _Irany = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _FeladoTipusa = true;
               public bool FeladoTipusa
               {
                   get { return _FeladoTipusa; }
                   set { _FeladoTipusa = value; }
               }
                                                            
                                 
               private bool _KapcsolatiKod = true;
               public bool KapcsolatiKod
               {
                   get { return _KapcsolatiKod; }
                   set { _KapcsolatiKod = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _Email = true;
               public bool Email
               {
                   get { return _Email; }
                   set { _Email = value; }
               }
                                                            
                                 
               private bool _RovidNev = true;
               public bool RovidNev
               {
                   get { return _RovidNev; }
                   set { _RovidNev = value; }
               }
                                                            
                                 
               private bool _MAKKod = true;
               public bool MAKKod
               {
                   get { return _MAKKod; }
                   set { _MAKKod = value; }
               }
                                                            
                                 
               private bool _KRID = true;
               public bool KRID
               {
                   get { return _KRID; }
                   set { _KRID = value; }
               }
                                                            
                                 
               private bool _ErkeztetesiSzam = true;
               public bool ErkeztetesiSzam
               {
                   get { return _ErkeztetesiSzam; }
                   set { _ErkeztetesiSzam = value; }
               }
                                                            
                                 
               private bool _HivatkozasiSzam = true;
               public bool HivatkozasiSzam
               {
                   get { return _HivatkozasiSzam; }
                   set { _HivatkozasiSzam = value; }
               }
                                                            
                                 
               private bool _DokTipusHivatal = true;
               public bool DokTipusHivatal
               {
                   get { return _DokTipusHivatal; }
                   set { _DokTipusHivatal = value; }
               }
                                                            
                                 
               private bool _DokTipusAzonosito = true;
               public bool DokTipusAzonosito
               {
                   get { return _DokTipusAzonosito; }
                   set { _DokTipusAzonosito = value; }
               }
                                                            
                                 
               private bool _DokTipusLeiras = true;
               public bool DokTipusLeiras
               {
                   get { return _DokTipusLeiras; }
                   set { _DokTipusLeiras = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _FileNev = true;
               public bool FileNev
               {
                   get { return _FileNev; }
                   set { _FileNev = value; }
               }
                                                            
                                 
               private bool _ErvenyessegiDatum = true;
               public bool ErvenyessegiDatum
               {
                   get { return _ErvenyessegiDatum; }
                   set { _ErvenyessegiDatum = value; }
               }
                                                            
                                 
               private bool _ErkeztetesiDatum = true;
               public bool ErkeztetesiDatum
               {
                   get { return _ErkeztetesiDatum; }
                   set { _ErkeztetesiDatum = value; }
               }
                                                            
                                 
               private bool _Kezbesitettseg = true;
               public bool Kezbesitettseg
               {
                   get { return _Kezbesitettseg; }
                   set { _Kezbesitettseg = value; }
               }
                                                            
                                 
               private bool _Idopecset = true;
               public bool Idopecset
               {
                   get { return _Idopecset; }
                   set { _Idopecset = value; }
               }
                                                            
                                 
               private bool _ValaszTitkositas = true;
               public bool ValaszTitkositas
               {
                   get { return _ValaszTitkositas; }
                   set { _ValaszTitkositas = value; }
               }
                                                            
                                 
               private bool _ValaszUtvonal = true;
               public bool ValaszUtvonal
               {
                   get { return _ValaszUtvonal; }
                   set { _ValaszUtvonal = value; }
               }
                                                            
                                 
               private bool _Rendszeruzenet = true;
               public bool Rendszeruzenet
               {
                   get { return _Rendszeruzenet; }
                   set { _Rendszeruzenet = value; }
               }
                                                            
                                 
               private bool _Tarterulet = true;
               public bool Tarterulet
               {
                   get { return _Tarterulet; }
                   set { _Tarterulet = value; }
               }
                                                            
                                 
               private bool _ETertiveveny = true;
               public bool ETertiveveny
               {
                   get { return _ETertiveveny; }
                   set { _ETertiveveny = value; }
               }
                                                            
                                 
               private bool _Lenyomat = true;
               public bool Lenyomat
               {
                   get { return _Lenyomat; }
                   set { _Lenyomat = value; }
               }
                                                            
                                 
               private bool _Dokumentum_Id = true;
               public bool Dokumentum_Id
               {
                   get { return _Dokumentum_Id; }
                   set { _Dokumentum_Id = value; }
               }
                                                            
                                 
               private bool _KuldKuldemeny_Id = true;
               public bool KuldKuldemeny_Id
               {
                   get { return _KuldKuldemeny_Id; }
                   set { _KuldKuldemeny_Id = value; }
               }
                                                            
                                 
               private bool _IraIrat_Id = true;
               public bool IraIrat_Id
               {
                   get { return _IraIrat_Id; }
                   set { _IraIrat_Id = value; }
               }
                                                            
                                 
               private bool _IratPeldany_Id = true;
               public bool IratPeldany_Id
               {
                   get { return _IratPeldany_Id; }
                   set { _IratPeldany_Id = value; }
               }
                                                            
                                 
               private bool _Rendszer = true;
               public bool Rendszer
               {
                   get { return _Rendszer; }
                   set { _Rendszer = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Irany = Value;               
                    
                   Allapot = Value;               
                    
                   FeladoTipusa = Value;               
                    
                   KapcsolatiKod = Value;               
                    
                   Nev = Value;               
                    
                   Email = Value;               
                    
                   RovidNev = Value;               
                    
                   MAKKod = Value;               
                    
                   KRID = Value;               
                    
                   ErkeztetesiSzam = Value;               
                    
                   HivatkozasiSzam = Value;               
                    
                   DokTipusHivatal = Value;               
                    
                   DokTipusAzonosito = Value;               
                    
                   DokTipusLeiras = Value;               
                    
                   Megjegyzes = Value;               
                    
                   FileNev = Value;               
                    
                   ErvenyessegiDatum = Value;               
                    
                   ErkeztetesiDatum = Value;               
                    
                   Kezbesitettseg = Value;               
                    
                   Idopecset = Value;               
                    
                   ValaszTitkositas = Value;               
                    
                   ValaszUtvonal = Value;               
                    
                   Rendszeruzenet = Value;               
                    
                   Tarterulet = Value;               
                    
                   ETertiveveny = Value;               
                    
                   Lenyomat = Value;               
                    
                   Dokumentum_Id = Value;               
                    
                   KuldKuldemeny_Id = Value;               
                    
                   IraIrat_Id = Value;               
                    
                   IratPeldany_Id = Value;               
                    
                   Rendszer = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseHKP_DokumentumAdatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Irany = SqlInt32.Null;
           
        /// <summary>
        /// Irany Base property </summary>
            public SqlInt32 Irany
            {
                get { return _Irany; }
                set { _Irany = value; }                                                        
            }        
                   
           
        private SqlInt32 _Allapot = SqlInt32.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlInt32 Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeladoTipusa = SqlInt32.Null;
           
        /// <summary>
        /// FeladoTipusa Base property </summary>
            public SqlInt32 FeladoTipusa
            {
                get { return _FeladoTipusa; }
                set { _FeladoTipusa = value; }                                                        
            }        
                   
           
        private SqlString _KapcsolatiKod = SqlString.Null;
           
        /// <summary>
        /// KapcsolatiKod Base property </summary>
            public SqlString KapcsolatiKod
            {
                get { return _KapcsolatiKod; }
                set { _KapcsolatiKod = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlString _Email = SqlString.Null;
           
        /// <summary>
        /// Email Base property </summary>
            public SqlString Email
            {
                get { return _Email; }
                set { _Email = value; }                                                        
            }        
                   
           
        private SqlString _RovidNev = SqlString.Null;
           
        /// <summary>
        /// RovidNev Base property </summary>
            public SqlString RovidNev
            {
                get { return _RovidNev; }
                set { _RovidNev = value; }                                                        
            }        
                   
           
        private SqlString _MAKKod = SqlString.Null;
           
        /// <summary>
        /// MAKKod Base property </summary>
            public SqlString MAKKod
            {
                get { return _MAKKod; }
                set { _MAKKod = value; }                                                        
            }        
                   
           
        private SqlInt32 _KRID = SqlInt32.Null;
           
        /// <summary>
        /// KRID Base property </summary>
            public SqlInt32 KRID
            {
                get { return _KRID; }
                set { _KRID = value; }                                                        
            }        
                   
           
        private SqlString _ErkeztetesiSzam = SqlString.Null;
           
        /// <summary>
        /// ErkeztetesiSzam Base property </summary>
            public SqlString ErkeztetesiSzam
            {
                get { return _ErkeztetesiSzam; }
                set { _ErkeztetesiSzam = value; }                                                        
            }        
                   
           
        private SqlString _HivatkozasiSzam = SqlString.Null;
           
        /// <summary>
        /// HivatkozasiSzam Base property </summary>
            public SqlString HivatkozasiSzam
            {
                get { return _HivatkozasiSzam; }
                set { _HivatkozasiSzam = value; }                                                        
            }        
                   
           
        private SqlString _DokTipusHivatal = SqlString.Null;
           
        /// <summary>
        /// DokTipusHivatal Base property </summary>
            public SqlString DokTipusHivatal
            {
                get { return _DokTipusHivatal; }
                set { _DokTipusHivatal = value; }                                                        
            }        
                   
           
        private SqlString _DokTipusAzonosito = SqlString.Null;
           
        /// <summary>
        /// DokTipusAzonosito Base property </summary>
            public SqlString DokTipusAzonosito
            {
                get { return _DokTipusAzonosito; }
                set { _DokTipusAzonosito = value; }                                                        
            }        
                   
           
        private SqlString _DokTipusLeiras = SqlString.Null;
           
        /// <summary>
        /// DokTipusLeiras Base property </summary>
            public SqlString DokTipusLeiras
            {
                get { return _DokTipusLeiras; }
                set { _DokTipusLeiras = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlString _FileNev = SqlString.Null;
           
        /// <summary>
        /// FileNev Base property </summary>
            public SqlString FileNev
            {
                get { return _FileNev; }
                set { _FileNev = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvenyessegiDatum = SqlDateTime.Null;
           
        /// <summary>
        /// ErvenyessegiDatum Base property </summary>
            public SqlDateTime ErvenyessegiDatum
            {
                get { return _ErvenyessegiDatum; }
                set { _ErvenyessegiDatum = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErkeztetesiDatum = SqlDateTime.Null;
           
        /// <summary>
        /// ErkeztetesiDatum Base property </summary>
            public SqlDateTime ErkeztetesiDatum
            {
                get { return _ErkeztetesiDatum; }
                set { _ErkeztetesiDatum = value; }                                                        
            }        
                   
           
        private SqlInt32 _Kezbesitettseg = SqlInt32.Null;
           
        /// <summary>
        /// Kezbesitettseg Base property </summary>
            public SqlInt32 Kezbesitettseg
            {
                get { return _Kezbesitettseg; }
                set { _Kezbesitettseg = value; }                                                        
            }        
                   
           
        private SqlString _Idopecset = SqlString.Null;
           
        /// <summary>
        /// Idopecset Base property </summary>
            public SqlString Idopecset
            {
                get { return _Idopecset; }
                set { _Idopecset = value; }                                                        
            }        
                   
           
        private SqlChars _ValaszTitkositas = SqlChars.Null;
           
        /// <summary>
        /// ValaszTitkositas Base property </summary>
            public SqlChars ValaszTitkositas
            {
                get { return _ValaszTitkositas; }
                set { _ValaszTitkositas = value; }                                                        
            }        
                   
           
        private SqlInt32 _ValaszUtvonal = SqlInt32.Null;
           
        /// <summary>
        /// ValaszUtvonal Base property </summary>
            public SqlInt32 ValaszUtvonal
            {
                get { return _ValaszUtvonal; }
                set { _ValaszUtvonal = value; }                                                        
            }        
                   
           
        private SqlChars _Rendszeruzenet = SqlChars.Null;
           
        /// <summary>
        /// Rendszeruzenet Base property </summary>
            public SqlChars Rendszeruzenet
            {
                get { return _Rendszeruzenet; }
                set { _Rendszeruzenet = value; }                                                        
            }        
                   
           
        private SqlInt32 _Tarterulet = SqlInt32.Null;
           
        /// <summary>
        /// Tarterulet Base property </summary>
            public SqlInt32 Tarterulet
            {
                get { return _Tarterulet; }
                set { _Tarterulet = value; }                                                        
            }        
                   
           
        private SqlInt32 _ETertiveveny = SqlInt32.Null;
           
        /// <summary>
        /// ETertiveveny Base property </summary>
            public SqlInt32 ETertiveveny
            {
                get { return _ETertiveveny; }
                set { _ETertiveveny = value; }                                                        
            }        
                   
           
        private SqlString _Lenyomat = SqlString.Null;
           
        /// <summary>
        /// Lenyomat Base property </summary>
            public SqlString Lenyomat
            {
                get { return _Lenyomat; }
                set { _Lenyomat = value; }                                                        
            }        
                   
           
        private SqlGuid _Dokumentum_Id = SqlGuid.Null;
           
        /// <summary>
        /// Dokumentum_Id Base property </summary>
            public SqlGuid Dokumentum_Id
            {
                get { return _Dokumentum_Id; }
                set { _Dokumentum_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _KuldKuldemeny_Id = SqlGuid.Null;
           
        /// <summary>
        /// KuldKuldemeny_Id Base property </summary>
            public SqlGuid KuldKuldemeny_Id
            {
                get { return _KuldKuldemeny_Id; }
                set { _KuldKuldemeny_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IraIrat_Id = SqlGuid.Null;
           
        /// <summary>
        /// IraIrat_Id Base property </summary>
            public SqlGuid IraIrat_Id
            {
                get { return _IraIrat_Id; }
                set { _IraIrat_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _IratPeldany_Id = SqlGuid.Null;
           
        /// <summary>
        /// IratPeldany_Id Base property </summary>
            public SqlGuid IratPeldany_Id
            {
                get { return _IratPeldany_Id; }
                set { _IratPeldany_Id = value; }                                                        
            }        
                   
           
        private SqlString _Rendszer = SqlString.Null;
           
        /// <summary>
        /// Rendszer Base property </summary>
            public SqlString Rendszer
            {
                get { return _Rendszer; }
                set { _Rendszer = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}