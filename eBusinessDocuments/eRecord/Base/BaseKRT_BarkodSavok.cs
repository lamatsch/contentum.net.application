
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_BarkodSavok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_BarkodSavok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_BarkodSavokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _SavKezd = true;
               public bool SavKezd
               {
                   get { return _SavKezd; }
                   set { _SavKezd = value; }
               }
                                                            
                                 
               private bool _SavVege = true;
               public bool SavVege
               {
                   get { return _SavVege; }
                   set { _SavVege = value; }
               }
                                                            
                                 
               private bool _SavType = true;
               public bool SavType
               {
                   get { return _SavType; }
                   set { _SavType = value; }
               }
                                                            
                                 
               private bool _SavAllapot = true;
               public bool SavAllapot
               {
                   get { return _SavAllapot; }
                   set { _SavAllapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   SavKezd = Value;               
                    
                   SavVege = Value;               
                    
                   SavType = Value;               
                    
                   SavAllapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_BarkodSavokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlString _SavKezd = SqlString.Null;
           
        /// <summary>
        /// SavKezd Base property </summary>
            public SqlString SavKezd
            {
                get { return _SavKezd; }
                set { _SavKezd = value; }                                                        
            }        
                   
           
        private SqlString _SavVege = SqlString.Null;
           
        /// <summary>
        /// SavVege Base property </summary>
            public SqlString SavVege
            {
                get { return _SavVege; }
                set { _SavVege = value; }                                                        
            }        
                   
           
        private SqlChars _SavType = SqlChars.Null;
           
        /// <summary>
        /// SavType Base property </summary>
            public SqlChars SavType
            {
                get { return _SavType; }
                set { _SavType = value; }                                                        
            }        
                   
           
        private SqlChars _SavAllapot = SqlChars.Null;
           
        /// <summary>
        /// SavAllapot Base property </summary>
            public SqlChars SavAllapot
            {
                get { return _SavAllapot; }
                set { _SavAllapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}