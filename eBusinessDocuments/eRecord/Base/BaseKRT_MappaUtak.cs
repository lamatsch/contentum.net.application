
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_MappaUtak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_MappaUtak
    {
        [System.Xml.Serialization.XmlType("BaseKRT_MappaUtakBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Mappa_Id = true;
               public bool Mappa_Id
               {
                   get { return _Mappa_Id; }
                   set { _Mappa_Id = value; }
               }
                                                            
                                 
               private bool _PathFromORG = true;
               public bool PathFromORG
               {
                   get { return _PathFromORG; }
                   set { _PathFromORG = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Mappa_Id = Value;               
                    
                   PathFromORG = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_MappaUtakBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Mappa_Id = SqlGuid.Null;
           
        /// <summary>
        /// Mappa_Id Base property </summary>
            public SqlGuid Mappa_Id
            {
                get { return _Mappa_Id; }
                set { _Mappa_Id = value; }                                                        
            }        
                   
           
        private SqlString _PathFromORG = SqlString.Null;
           
        /// <summary>
        /// PathFromORG Base property </summary>
            public SqlString PathFromORG
            {
                get { return _PathFromORG; }
                set { _PathFromORG = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}