﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// KRT_Mentett_Talalati_Listak BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_Mentett_Talalati_Listak
    {
      [System.Xml.Serialization.XmlType("BaseKRT_Mentett_Talalati_ListakBaseUpdated")]
        public class BaseUpdated
        {
            private bool _Id = true;

            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private bool _ListName = true;

            public bool ListName
            {
                get { return _ListName; }
                set { _ListName = value; }
            }

            private bool _SaveDate = true;

            public bool SaveDate
            {
                get { return _SaveDate; }
                set { _SaveDate = value; }
            }

            private bool _Searched_Table = true;

            public bool Searched_Table
            {
                get { return _Searched_Table; }
                set { _Searched_Table = value; }
            }

            private bool _Requestor = true;

            public bool Requestor
            {
                get { return _Requestor; }
                set { _Requestor = value; }
            }

            private bool _HeaderXML = true;

            public bool HeaderXML
            {
                get { return _HeaderXML; }
                set { _HeaderXML = value; }
            }

            private bool _BodyXML = true;

            public bool BodyXML
            {
                get { return _BodyXML; }
                set { _BodyXML = value; }
            }

            private bool _WorkStation = true;

            public bool WorkStation
            {
                get { return _WorkStation; }
                set { _WorkStation = value; }
            }

            private bool _ErvKezd = true;

            public bool ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }

            private bool _ErvVege = true;

            public bool ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            public void SetValueAll(bool Value)
            {
                Id = Value;

                ListName = Value;

                SaveDate = Value;

                Searched_Table = Value;

                Requestor = Value;

                HeaderXML = Value;

                BodyXML = Value;

                WorkStation = Value;

                ErvKezd = Value;

                ErvVege = Value;
            }
        }

      [System.Xml.Serialization.XmlType("BaseKRT_Mentett_Talalati_ListakBaseTyped")]
      public class BaseTyped
      {
          private SqlGuid _Id = SqlGuid.Null;

          /// <summary>
          /// Id Base property </summary>
          public SqlGuid Id
          {
              get { return _Id; }
              set { _Id = value; }
          }

          private SqlString _ListName = SqlString.Null;

          /// <summary>
          /// ListName Base property </summary>
          public SqlString ListName
          {
              get { return _ListName; }
              set { _ListName = value; }
          }

          private SqlDateTime _SaveDate = SqlDateTime.Null;

          /// <summary>
          /// SaveDate Base property </summary>
          public SqlDateTime SaveDate
          {
              get { return _SaveDate; }
              set { _SaveDate = value; }
          }

          private SqlString _Searched_Table = SqlString.Null;

          /// <summary>
          /// Searched_Table Base property </summary>
          public SqlString Searched_Table
          {
              get { return _Searched_Table; }
              set { _Searched_Table = value; }
          }

          private SqlString _Requestor = SqlString.Null;

          /// <summary>
          /// Requestor Base property </summary>
          public SqlString Requestor
          {
              get { return _Requestor; }
              set { _Requestor = value; }
          }

          private SqlString _HeaderXML = SqlString.Null;
          
          /// <summary>
          /// HeaderXML Base property </summary>
          public SqlString HeaderXML
          {
              get { return _HeaderXML; }
              set { _HeaderXML = value; }
          }

          private SqlString _BodyXML = SqlString.Null;

          /// <summary>
          /// BodyXML Base property </summary>
          public SqlString BodyXML
          {
              get { return _BodyXML; }
              set { _BodyXML = value; }
          }

          private SqlString _WorkStation = SqlString.Null;

          /// <summary>
          /// WorkStation Base property </summary>
          public SqlString WorkStation
          {
              get { return _WorkStation; }
              set { _WorkStation = value; }
          }

          private SqlDateTime _ErvKezd = SqlDateTime.Null;

          /// <summary>
          /// ErvKezd Base property </summary>
          public SqlDateTime ErvKezd
          {
              get { return _ErvKezd; }
              set { _ErvKezd = value; }
          }

          private SqlDateTime _ErvVege = SqlDateTime.Null;

          /// <summary>
          /// ErvVege Base property </summary>
          public SqlDateTime ErvVege
          {
              get { return _ErvVege; }
              set { _ErvVege = value; }
          }

      }

    }
}
