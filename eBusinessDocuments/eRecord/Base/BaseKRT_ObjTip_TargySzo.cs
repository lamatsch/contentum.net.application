
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ObjTip_TargySzo BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_ObjTip_TargySzo
    {
        [System.Xml.Serialization.XmlType("BaseKRT_ObjTip_TargySzoBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _TargySzo_Id = true;
               public bool TargySzo_Id
               {
                   get { return _TargySzo_Id; }
                   set { _TargySzo_Id = value; }
               }
                                                            
                                 
               private bool _ObjTip_Id_AdatElem = true;
               public bool ObjTip_Id_AdatElem
               {
                   get { return _ObjTip_Id_AdatElem; }
                   set { _ObjTip_Id_AdatElem = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Sorszam = true;
               public bool Sorszam
               {
                   get { return _Sorszam; }
                   set { _Sorszam = value; }
               }
                                                            
                                 
               private bool _Opcionalis = true;
               public bool Opcionalis
               {
                   get { return _Opcionalis; }
                   set { _Opcionalis = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   TargySzo_Id = Value;               
                    
                   ObjTip_Id_AdatElem = Value;               
                    
                   Tipus = Value;               
                    
                   Sorszam = Value;               
                    
                   Opcionalis = Value;               
                    
                   Nev = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_ObjTip_TargySzoBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _TargySzo_Id = SqlGuid.Null;
           
        /// <summary>
        /// TargySzo_Id Base property </summary>
            public SqlGuid TargySzo_Id
            {
                get { return _TargySzo_Id; }
                set { _TargySzo_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _ObjTip_Id_AdatElem = SqlGuid.Null;
           
        /// <summary>
        /// ObjTip_Id_AdatElem Base property </summary>
            public SqlGuid ObjTip_Id_AdatElem
            {
                get { return _ObjTip_Id_AdatElem; }
                set { _ObjTip_Id_AdatElem = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorszam = SqlInt32.Null;
           
        /// <summary>
        /// Sorszam Base property </summary>
            public SqlInt32 Sorszam
            {
                get { return _Sorszam; }
                set { _Sorszam = value; }                                                        
            }        
                   
           
        private SqlChars _Opcionalis = SqlChars.Null;
           
        /// <summary>
        /// Opcionalis Base property </summary>
            public SqlChars Opcionalis
            {
                get { return _Opcionalis; }
                set { _Opcionalis = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}