
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_RagszamSavok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseKRT_RagszamSavok
    {
        [System.Xml.Serialization.XmlType("BaseKRT_RagszamSavokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Org = true;
               public bool Org
               {
                   get { return _Org; }
                   set { _Org = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Postakonyv_Id = true;
               public bool Postakonyv_Id
               {
                   get { return _Postakonyv_Id; }
                   set { _Postakonyv_Id = value; }
               }
                                                            
                                 
               private bool _SavKezd = true;
               public bool SavKezd
               {
                   get { return _SavKezd; }
                   set { _SavKezd = value; }
               }
                                                            
                                 
               private bool _SavKezdNum = true;
               public bool SavKezdNum
               {
                   get { return _SavKezdNum; }
                   set { _SavKezdNum = value; }
               }
                                                            
                                 
               private bool _SavVege = true;
               public bool SavVege
               {
                   get { return _SavVege; }
                   set { _SavVege = value; }
               }
                                                            
                                 
               private bool _SavVegeNum = true;
               public bool SavVegeNum
               {
                   get { return _SavVegeNum; }
                   set { _SavVegeNum = value; }
               }
                                                            
                                 
               private bool _SavType = true;
               public bool SavType
               {
                   get { return _SavType; }
                   set { _SavType = value; }
               }
                                                            
                                 
               private bool _SavAllapot = true;
               public bool SavAllapot
               {
                   get { return _SavAllapot; }
                   set { _SavAllapot = value; }
               }
                                                            
                                 
               private bool _ErvKezd = true;
               public bool ErvKezd
               {
                   get { return _ErvKezd; }
                   set { _ErvKezd = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }

                private bool _IgenyeltDarabszam = true;
                public bool IgenyeltDarabszam
            {
                get { return _IgenyeltDarabszam; }
                set { _IgenyeltDarabszam = value; }
            }


               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Org = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Postakonyv_Id = Value;               
                    
                   SavKezd = Value;               
                    
                   SavKezdNum = Value;               
                    
                   SavVege = Value;               
                    
                   SavVegeNum = Value;               
                    
                   SavType = Value;               
                    
                   SavAllapot = Value;               
                    
                   ErvKezd = Value;               
                    
                   ErvVege = Value;

                   IgenyeltDarabszam = Value;
            }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseKRT_RagszamSavokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Org = SqlGuid.Null;
           
        /// <summary>
        /// Org Base property </summary>
            public SqlGuid Org
            {
                get { return _Org; }
                set { _Org = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlGuid _Postakonyv_Id = SqlGuid.Null;
           
        /// <summary>
        /// Postakonyv_Id Base property </summary>
            public SqlGuid Postakonyv_Id
            {
                get { return _Postakonyv_Id; }
                set { _Postakonyv_Id = value; }                                                        
            }        
                   
           
        private SqlString _SavKezd = SqlString.Null;
           
        /// <summary>
        /// SavKezd Base property </summary>
            public SqlString SavKezd
            {
                get { return _SavKezd; }
                set { _SavKezd = value; }                                                        
            }        
                   
           
        private SqlDouble _SavKezdNum = SqlDouble.Null;
           
        /// <summary>
        /// SavKezdNum Base property </summary>
            public SqlDouble SavKezdNum
            {
                get { return _SavKezdNum; }
                set { _SavKezdNum = value; }                                                        
            }        
                   
           
        private SqlString _SavVege = SqlString.Null;
           
        /// <summary>
        /// SavVege Base property </summary>
            public SqlString SavVege
            {
                get { return _SavVege; }
                set { _SavVege = value; }                                                        
            }        
                   
           
        private SqlDouble _SavVegeNum = SqlDouble.Null;
           
        /// <summary>
        /// SavVegeNum Base property </summary>
            public SqlDouble SavVegeNum
            {
                get { return _SavVegeNum; }
                set { _SavVegeNum = value; }                                                        
            }        
                   
           
        private SqlString _SavType = SqlString.Null;
           
        /// <summary>
        /// SavType Base property </summary>
            public SqlString SavType
            {
                get { return _SavType; }
                set { _SavType = value; }                                                        
            }        
                   
           
        private SqlChars _SavAllapot = SqlChars.Null;
           
        /// <summary>
        /// SavAllapot Base property </summary>
            public SqlChars SavAllapot
            {
                get { return _SavAllapot; }
                set { _SavAllapot = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvKezd = SqlDateTime.Null;
           
        /// <summary>
        /// ErvKezd Base property </summary>
            public SqlDateTime ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }

            /// <summary>
            /// IgenyeltDarabszam Base property
            /// </summary>
            private SqlDouble _IgenyeltDarabszam = SqlDouble.Null;

            public SqlDouble IgenyeltDarabszam
            {
                get { return _IgenyeltDarabszam; }
                set { _IgenyeltDarabszam = value; }
            }
                           }
    }    
}