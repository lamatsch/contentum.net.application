using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class BaseKozterInterface
    {
        [System.Xml.Serialization.XmlType("BaseKozterInterfaceBaseUpdated")]
        public class BaseUpdated
        {

            private bool _ktdok_id = true;
            public bool ktdok_id
            {
                get { return _ktdok_id; }
                set { _ktdok_id = value; }
            }

            private bool _ktdok_kod = true;
            public bool ktdok_kod
            {
                get { return _ktdok_kod; }
                set { _ktdok_kod = value; }
            }

            private bool _ktdok_iktszam = true;
            public bool ktdok_iktszam
            {
                get { return _ktdok_iktszam; }
                set { _ktdok_iktszam = value; }
            }

            private bool _ktdok_vonalkod = true;
            public bool ktdok_vonalkod
            {
                get { return _ktdok_vonalkod; }
                set { _ktdok_vonalkod = value; }
            }

            private bool _ktdok_dokid = true;
            public bool ktdok_dokid
            {
                get { return _ktdok_dokid; }
                set { _ktdok_dokid = value; }
            }

            private bool _ktdok_url = true;
            public bool ktdok_url
            {
                get { return _ktdok_url; }
                set { _ktdok_url = value; }
            }

            private bool _ktdok_date = true;
            public bool ktdok_date
            {
                get { return _ktdok_date; }
                set { _ktdok_date = value; }
            }

            private bool _ktdok_siker = true;
            public bool ktdok_siker
            {
                get { return _ktdok_siker; }
                set { _ktdok_siker = value; }
            }

            private bool _ktdok_ok = true;
            public bool ktdok_ok
            {
                get { return _ktdok_ok; }
                set { _ktdok_ok = value; }
            }

            private bool _ktdok_atvet = true;
            public bool ktdok_atvet
            {
                get { return _ktdok_atvet; }
                set { _ktdok_atvet = value; }
            }

            public void SetValueAll(bool Value)
            {
                ktdok_id = Value;
                ktdok_kod = Value;
                ktdok_iktszam = Value;
                ktdok_vonalkod = Value;
                ktdok_dokid = Value;
                ktdok_url = Value;
                ktdok_date = Value;
                ktdok_siker = Value;
                ktdok_ok = Value;
                ktdok_atvet = Value;
            }
        }

        [System.Xml.Serialization.XmlType("BaseKozterInterfaceBaseTyped")]
        public class BaseTyped
        {

            private SqlInt32 _ktdok_id = SqlInt32.Null;
            public SqlInt32 ktdok_id
            {
                get { return _ktdok_id; }
                set { _ktdok_id = value; }
            }

            private SqlString _ktdok_kod = SqlString.Null;
            public SqlString ktdok_kod
            {
                get { return _ktdok_kod; }
                set { _ktdok_kod = value; }
            }

            private SqlString _ktdok_iktszam = SqlString.Null;
            public SqlString ktdok_iktszam
            {
                get { return _ktdok_iktszam; }
                set { _ktdok_iktszam = value; }
            }

            private SqlString _ktdok_vonalkod = SqlString.Null;
            public SqlString ktdok_vonalkod
            {
                get { return _ktdok_vonalkod; }
                set { _ktdok_vonalkod = value; }
            }

            private SqlGuid _ktdok_dokid = SqlGuid.Null;
            public SqlGuid ktdok_dokid
            {
                get { return _ktdok_dokid; }
                set { _ktdok_dokid = value; }
            }

            private SqlString _ktdok_url = SqlString.Null;
            public SqlString ktdok_url
            {
                get { return _ktdok_url; }
                set { _ktdok_url = value; }
            }

            private SqlDateTime _ktdok_date = SqlDateTime.Null;
            public SqlDateTime ktdok_date
            {
                get { return _ktdok_date; }
                set { _ktdok_date = value; }
            }

            private SqlString _ktdok_siker = SqlString.Null;
            public SqlString ktdok_siker
            {
                get { return _ktdok_siker; }
                set { _ktdok_siker = value; }
            }

            private SqlString _ktdok_ok = SqlString.Null;
            public SqlString ktdok_ok
            {
                get { return _ktdok_ok; }
                set { _ktdok_ok = value; }
            }

            private SqlDateTime _ktdok_atvet = SqlDateTime.Null;
            public SqlDateTime ktdok_atvet
            {
                get { return _ktdok_atvet; }
                set { _ktdok_atvet = value; }
            }

        }
    }
}
