
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// PirEdokSzamla BusinessDocument Class </summary>
    [Serializable()]
    public class BasePirEdokSzamla
    {
        [System.Xml.Serialization.XmlType("BasePirEdokSzamlaBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Vonalkod = true;
            public bool Vonalkod
            {
                get { return _Vonalkod; }
                set { _Vonalkod = value; }
            }


            private bool _Szamlaszam = true;
            public bool Szamlaszam
            {
                get { return _Szamlaszam; }
                set { _Szamlaszam = value; }
            }


            private bool _ErkeztetesDatuma = true;
            public bool ErkeztetesDatuma
            {
                get { return _ErkeztetesDatuma; }
                set { _ErkeztetesDatuma = value; }
            }


            private bool _SzervezetiEgysegKod = true;
            public bool SzervezetiEgysegKod
            {
                get { return _SzervezetiEgysegKod; }
                set { _SzervezetiEgysegKod = value; }
            }


            private bool _SzervezetiEgysegNev = true;
            public bool SzervezetiEgysegNev
            {
                get { return _SzervezetiEgysegNev; }
                set { _SzervezetiEgysegNev = value; }
            }


            private bool _FizetesiMod = true;
            public bool FizetesiMod
            {
                get { return _FizetesiMod; }
                set { _FizetesiMod = value; }
            }


            private bool _TeljesitesDatuma = true;
            public bool TeljesitesDatuma
            {
                get { return _TeljesitesDatuma; }
                set { _TeljesitesDatuma = value; }
            }


            private bool _BizonylatDatuma = true;
            public bool BizonylatDatuma
            {
                get { return _BizonylatDatuma; }
                set { _BizonylatDatuma = value; }
            }


            private bool _FizetesiHatarido = true;
            public bool FizetesiHatarido
            {
                get { return _FizetesiHatarido; }
                set { _FizetesiHatarido = value; }
            }


            private bool _DevizaKod = true;
            public bool DevizaKod
            {
                get { return _DevizaKod; }
                set { _DevizaKod = value; }
            }


            private bool _VisszakuldesDatuma = true;
            public bool VisszakuldesDatuma
            {
                get { return _VisszakuldesDatuma; }
                set { _VisszakuldesDatuma = value; }
            }


            private bool _EllenorzoOsszeg = true;
            public bool EllenorzoOsszeg
            {
                get { return _EllenorzoOsszeg; }
                set { _EllenorzoOsszeg = value; }
            }


            private bool _EDOKPartnerKod = true;
            public bool EDOKPartnerKod
            {
                get { return _EDOKPartnerKod; }
                set { _EDOKPartnerKod = value; }
            }


            private bool _EDOKPartnerNev = true;
            public bool EDOKPartnerNev
            {
                get { return _EDOKPartnerNev; }
                set { _EDOKPartnerNev = value; }
            }


            private bool _EDOKPartnerIranyitoszam = true;
            public bool EDOKPartnerIranyitoszam
            {
                get { return _EDOKPartnerIranyitoszam; }
                set { _EDOKPartnerIranyitoszam = value; }
            }


            private bool _EDOKPartnerHelyseg = true;
            public bool EDOKPartnerHelyseg
            {
                get { return _EDOKPartnerHelyseg; }
                set { _EDOKPartnerHelyseg = value; }
            }


            private bool _EDOKPartnerCim = true;
            public bool EDOKPartnerCim
            {
                get { return _EDOKPartnerCim; }
                set { _EDOKPartnerCim = value; }
            }


            private bool _BelfoldiAdoszam = true;
            public bool BelfoldiAdoszam
            {
                get { return _BelfoldiAdoszam; }
                set { _BelfoldiAdoszam = value; }
            }


            private bool _KozossegiAdoszam = true;
            public bool KozossegiAdoszam
            {
                get { return _KozossegiAdoszam; }
                set { _KozossegiAdoszam = value; }
            }


            private bool _MaganszemelyAdoazonositoJel = true;
            public bool MaganszemelyAdoazonositoJel
            {
                get { return _MaganszemelyAdoazonositoJel; }
                set { _MaganszemelyAdoazonositoJel = value; }
            }


            private bool _HelyrajziSzam = true;
            public bool HelyrajziSzam
            {
                get { return _HelyrajziSzam; }
                set { _HelyrajziSzam = value; }
            }


            private bool _Bankszamlaszam1 = true;
            public bool Bankszamlaszam1
            {
                get { return _Bankszamlaszam1; }
                set { _Bankszamlaszam1 = value; }
            }


            private bool _Bankszamlaszam2 = true;
            public bool Bankszamlaszam2
            {
                get { return _Bankszamlaszam2; }
                set { _Bankszamlaszam2 = value; }
            }


            private bool _Bankszamlaszam3 = true;
            public bool Bankszamlaszam3
            {
                get { return _Bankszamlaszam3; }
                set { _Bankszamlaszam3 = value; }
            }


            private bool _Telefonszam = true;
            public bool Telefonszam
            {
                get { return _Telefonszam; }
                set { _Telefonszam = value; }
            }


            private bool _Fax = true;
            public bool Fax
            {
                get { return _Fax; }
                set { _Fax = value; }
            }


            private bool _EmailCim = true;
            public bool EmailCim
            {
                get { return _EmailCim; }
                set { _EmailCim = value; }
            }


            private bool _PIRAllapot = true;
            public bool PIRAllapot
            {
                get { return _PIRAllapot; }
                set { _PIRAllapot = value; }
            }


            private bool _Megjegyzes = true;
            public bool Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }
            }

            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
            private bool _VevoBesorolas = true;
            public bool VevoBesorolas
            {
                get { return _VevoBesorolas; }
                set { _VevoBesorolas = value; }
            }

            public void SetValueAll(bool Value)
            {

                Vonalkod = Value;

                Szamlaszam = Value;

                ErkeztetesDatuma = Value;

                SzervezetiEgysegKod = Value;

                SzervezetiEgysegNev = Value;

                FizetesiMod = Value;

                TeljesitesDatuma = Value;

                BizonylatDatuma = Value;

                FizetesiHatarido = Value;

                DevizaKod = Value;

                VisszakuldesDatuma = Value;

                EllenorzoOsszeg = Value;

                EDOKPartnerKod = Value;

                EDOKPartnerNev = Value;

                EDOKPartnerIranyitoszam = Value;

                EDOKPartnerHelyseg = Value;

                EDOKPartnerCim = Value;

                BelfoldiAdoszam = Value;

                KozossegiAdoszam = Value;

                MaganszemelyAdoazonositoJel = Value;

                HelyrajziSzam = Value;

                Bankszamlaszam1 = Value;

                Bankszamlaszam2 = Value;

                Bankszamlaszam3 = Value;

                Telefonszam = Value;

                Fax = Value;

                EmailCim = Value;

                PIRAllapot = Value;

                Megjegyzes = Value;

                // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                VevoBesorolas = Value;
            }
        }
    
        [System.Xml.Serialization.XmlType("BasePirEdokSzamlaBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlString _Vonalkod = SqlString.Null;
           
        /// <summary>
        /// Vonalkod Base property </summary>
            public SqlString Vonalkod
            {
                get { return _Vonalkod; }
                set { _Vonalkod = value; }                                                        
            }        
                   
           
        private SqlString _Szamlaszam = SqlString.Null;
           
        /// <summary>
        /// Szamlaszam Base property </summary>
            public SqlString Szamlaszam
            {
                get { return _Szamlaszam; }
                set { _Szamlaszam = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErkeztetesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// ErkeztetesDatuma Base property </summary>
            public SqlDateTime ErkeztetesDatuma
            {
                get { return _ErkeztetesDatuma; }
                set { _ErkeztetesDatuma = value; }                                                        
            }        
                   
           
        private SqlString _SzervezetiEgysegKod = SqlString.Null;
           
        /// <summary>
        /// SzervezetiEgysegKod Base property </summary>
            public SqlString SzervezetiEgysegKod
            {
                get { return _SzervezetiEgysegKod; }
                set { _SzervezetiEgysegKod = value; }                                                        
            }        
                   
           
        private SqlString _SzervezetiEgysegNev = SqlString.Null;
           
        /// <summary>
        /// SzervezetiEgysegNev Base property </summary>
            public SqlString SzervezetiEgysegNev
            {
                get { return _SzervezetiEgysegNev; }
                set { _SzervezetiEgysegNev = value; }                                                        
            }        
                   
           
        private SqlString _FizetesiMod = SqlString.Null;
           
        /// <summary>
        /// FizetesiMod Base property </summary>
            public SqlString FizetesiMod
            {
                get { return _FizetesiMod; }
                set { _FizetesiMod = value; }                                                        
            }        
                   
           
        private SqlDateTime _TeljesitesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// TeljesitesDatuma Base property </summary>
            public SqlDateTime TeljesitesDatuma
            {
                get { return _TeljesitesDatuma; }
                set { _TeljesitesDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _BizonylatDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// BizonylatDatuma Base property </summary>
            public SqlDateTime BizonylatDatuma
            {
                get { return _BizonylatDatuma; }
                set { _BizonylatDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _FizetesiHatarido = SqlDateTime.Null;
           
        /// <summary>
        /// FizetesiHatarido Base property </summary>
            public SqlDateTime FizetesiHatarido
            {
                get { return _FizetesiHatarido; }
                set { _FizetesiHatarido = value; }                                                        
            }        
                   
           
        private SqlString _DevizaKod = SqlString.Null;
           
        /// <summary>
        /// DevizaKod Base property </summary>
            public SqlString DevizaKod
            {
                get { return _DevizaKod; }
                set { _DevizaKod = value; }                                                        
            }        
                   
           
        private SqlDateTime _VisszakuldesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VisszakuldesDatuma Base property </summary>
            public SqlDateTime VisszakuldesDatuma
            {
                get { return _VisszakuldesDatuma; }
                set { _VisszakuldesDatuma = value; }                                                        
            }        
                   
           
        private SqlDouble _EllenorzoOsszeg = SqlDouble.Null;
           
        /// <summary>
        /// EllenorzoOsszeg Base property </summary>
            public SqlDouble EllenorzoOsszeg
            {
                get { return _EllenorzoOsszeg; }
                set { _EllenorzoOsszeg = value; }                                                        
            }        
                   
           
        private SqlString _EDOKPartnerKod = SqlString.Null;
           
        /// <summary>
        /// EDOKPartnerKod Base property </summary>
            public SqlString EDOKPartnerKod
            {
                get { return _EDOKPartnerKod; }
                set { _EDOKPartnerKod = value; }                                                        
            }        
                   
           
        private SqlString _EDOKPartnerNev = SqlString.Null;
           
        /// <summary>
        /// EDOKPartnerNev Base property </summary>
            public SqlString EDOKPartnerNev
            {
                get { return _EDOKPartnerNev; }
                set { _EDOKPartnerNev = value; }                                                        
            }        
                   
           
        private SqlString _EDOKPartnerIranyitoszam = SqlString.Null;
           
        /// <summary>
        /// EDOKPartnerIranyitoszam Base property </summary>
            public SqlString EDOKPartnerIranyitoszam
            {
                get { return _EDOKPartnerIranyitoszam; }
                set { _EDOKPartnerIranyitoszam = value; }                                                        
            }        
                   
           
        private SqlString _EDOKPartnerHelyseg = SqlString.Null;
           
        /// <summary>
        /// EDOKPartnerHelyseg Base property </summary>
            public SqlString EDOKPartnerHelyseg
            {
                get { return _EDOKPartnerHelyseg; }
                set { _EDOKPartnerHelyseg = value; }                                                        
            }        
                   
           
        private SqlString _EDOKPartnerCim = SqlString.Null;
           
        /// <summary>
        /// EDOKPartnerCim Base property </summary>
            public SqlString EDOKPartnerCim
            {
                get { return _EDOKPartnerCim; }
                set { _EDOKPartnerCim = value; }                                                        
            }        
                   
           
        private SqlString _BelfoldiAdoszam = SqlString.Null;
           
        /// <summary>
        /// BelfoldiAdoszam Base property </summary>
            public SqlString BelfoldiAdoszam
            {
                get { return _BelfoldiAdoszam; }
                set { _BelfoldiAdoszam = value; }                                                        
            }        
                   
           
        private SqlString _KozossegiAdoszam = SqlString.Null;
           
        /// <summary>
        /// KozossegiAdoszam Base property </summary>
            public SqlString KozossegiAdoszam
            {
                get { return _KozossegiAdoszam; }
                set { _KozossegiAdoszam = value; }                                                        
            }        
                   
           
        private SqlString _MaganszemelyAdoazonositoJel = SqlString.Null;
           
        /// <summary>
        /// MaganszemelyAdoazonositoJel Base property </summary>
            public SqlString MaganszemelyAdoazonositoJel
            {
                get { return _MaganszemelyAdoazonositoJel; }
                set { _MaganszemelyAdoazonositoJel = value; }                                                        
            }        
                   
           
        private SqlString _HelyrajziSzam = SqlString.Null;
           
        /// <summary>
        /// HelyrajziSzam Base property </summary>
            public SqlString HelyrajziSzam
            {
                get { return _HelyrajziSzam; }
                set { _HelyrajziSzam = value; }                                                        
            }        
                   
           
        private SqlString _Bankszamlaszam1 = SqlString.Null;
           
        /// <summary>
        /// Bankszamlaszam1 Base property </summary>
            public SqlString Bankszamlaszam1
            {
                get { return _Bankszamlaszam1; }
                set { _Bankszamlaszam1 = value; }                                                        
            }        
                   
           
        private SqlString _Bankszamlaszam2 = SqlString.Null;
           
        /// <summary>
        /// Bankszamlaszam2 Base property </summary>
            public SqlString Bankszamlaszam2
            {
                get { return _Bankszamlaszam2; }
                set { _Bankszamlaszam2 = value; }                                                        
            }        
                   
           
        private SqlString _Bankszamlaszam3 = SqlString.Null;
           
        /// <summary>
        /// Bankszamlaszam3 Base property </summary>
            public SqlString Bankszamlaszam3
            {
                get { return _Bankszamlaszam3; }
                set { _Bankszamlaszam3 = value; }                                                        
            }        
                   
           
        private SqlString _Telefonszam = SqlString.Null;
           
        /// <summary>
        /// Telefonszam Base property </summary>
            public SqlString Telefonszam
            {
                get { return _Telefonszam; }
                set { _Telefonszam = value; }                                                        
            }        
                   
           
        private SqlString _Fax = SqlString.Null;
           
        /// <summary>
        /// Fax Base property </summary>
            public SqlString Fax
            {
                get { return _Fax; }
                set { _Fax = value; }                                                        
            }        
                   
           
        private SqlString _EmailCim = SqlString.Null;
           
        /// <summary>
        /// EmailCim Base property </summary>
            public SqlString EmailCim
            {
                get { return _EmailCim; }
                set { _EmailCim = value; }                                                        
            }        
                   
           
        private SqlChars _PIRAllapot = SqlChars.Null;
           
        /// <summary>
        /// PIRAllapot Base property </summary>
            public SqlChars PIRAllapot
            {
                get { return _PIRAllapot; }
                set { _PIRAllapot = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }

            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
            private SqlString _VevoBesorolas = SqlString.Null;

            /// <summary>
            /// VevoBesorolas Base property </summary>
            public SqlString VevoBesorolas
            {
                get { return _VevoBesorolas; }
                set { _VevoBesorolas = value; }
            }
        }
    }    
}