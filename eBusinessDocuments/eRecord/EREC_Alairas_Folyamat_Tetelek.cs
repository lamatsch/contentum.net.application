
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Alairas_Folyamat_Tetelek BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_Alairas_Folyamat_Tetelek : BaseEREC_Alairas_Folyamat_Tetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasFolyamat_Id property
        /// 
        /// </summary>
        public String AlairasFolyamat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AlairasFolyamat_Id); }
            set { Typed.AlairasFolyamat_Id = Utility.SetSqlGuidFromString(value, Typed.AlairasFolyamat_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csatolmany_Id property
        /// 
        /// </summary>
        public String Csatolmany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csatolmany_Id); }
            set { Typed.Csatolmany_Id = Utility.SetSqlGuidFromString(value, Typed.Csatolmany_Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasStatus property
        /// 
        /// </summary>
        public String AlairasStatus
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasStatus); }
            set { Typed.AlairasStatus = Utility.SetSqlStringFromString(value, Typed.AlairasStatus); }                                            
        }
                   
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        public String Hiba
        {
            get { return Utility.GetStringFromSqlString(Typed.Hiba); }
            set { Typed.Hiba = Utility.SetSqlStringFromString(value, Typed.Hiba); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}