
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Alairas_Folyamatok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_Alairas_Folyamatok : BaseEREC_Alairas_Folyamatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasStarted property
        /// 
        /// </summary>
        public String AlairasStarted
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AlairasStarted); }
            set { Typed.AlairasStarted = Utility.SetSqlDateTimeFromString(value, Typed.AlairasStarted); }                                            
        }
                   
           
        /// <summary>
        /// AlairasStopped property
        /// 
        /// </summary>
        public String AlairasStopped
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AlairasStopped); }
            set { Typed.AlairasStopped = Utility.SetSqlDateTimeFromString(value, Typed.AlairasStopped); }                                            
        }
                   
           
        /// <summary>
        /// AlairasStatus property
        /// 
        /// </summary>
        public String AlairasStatus
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasStatus); }
            set { Typed.AlairasStatus = Utility.SetSqlStringFromString(value, Typed.AlairasStatus); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}