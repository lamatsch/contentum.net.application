
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Csatolmanyok BusinessDocument Class
    /// Az iratok ill. a k�ldem�nyek dokumentumainak kapcsol�t�bl�ja.
    /// </summary>
    [Serializable()]
    public class EREC_Csatolmanyok : BaseEREC_Csatolmanyok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�nyazonos�t�, k�ldem�nyhez kapcsol�d� csatolm�ny eset�n.
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// Irat Id
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// A dokumentum kapcsolasahoz tartozo szabad szoveg.
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Lapszam property
        /// NEM HASZN�LT!
        /// Lapsz�m
        /// </summary>
        public String Lapszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Lapszam); }
            set { Typed.Lapszam = Utility.SetSqlInt32FromString(value, Typed.Lapszam); }                                            
        }
                   
           
        /// <summary>
        /// KapcsolatJelleg property
        /// Az irat �s a dokumentum k�z�tti kapcsolat jellege. KCS: DOKUMENTUM_KAPCSOLAT
        /// 1 - Alapkapcsolat (az irathoz alapb�l kapcsol�d� dokumentum)
        /// 2 - Duplik�lt kapcsolat (alapkapcsolattal rendelkez� dokumentum kapcsol�d�sa m�sik irathoz, fizikai szinten is - pl. m�solat)
        /// 3 - Hivatkoz�s (alapkapcsolattal rendekez� dokumentum kapcsol�d�sa m�sik irathoz, fizikai kapcsol�d�s n�lk�l)
        /// </summary>
        public String KapcsolatJelleg
        {
            get { return Utility.GetStringFromSqlString(Typed.KapcsolatJelleg); }
            set { Typed.KapcsolatJelleg = Utility.SetSqlStringFromString(value, Typed.KapcsolatJelleg); }                                            
        }
                   
           
        /// <summary>
        /// DokumentumSzerep property
        /// A dokumentum szerepe. KCS:DOKUMENTUM_SZEREP (F�dokumentum, Dokumentum mell�klet, Seg�ddokumentum)
        /// </summary>
        public String DokumentumSzerep
        {
            get { return Utility.GetStringFromSqlString(Typed.DokumentumSzerep); }
            set { Typed.DokumentumSzerep = Utility.SetSqlStringFromString(value, Typed.DokumentumSzerep); }                                            
        }
                   
           
        /// <summary>
        /// IratAlairoJel property
        /// A dokumentum al��rhat�s�ga az irat al��r�k �ltal, az adott kapcsolatban.
        /// 0 - nem al��rhat�
        /// 1 - al��rhat�
        /// </summary>
        public String IratAlairoJel
        {
            get { return Utility.GetStringFromSqlChars(Typed.IratAlairoJel); }
            set { Typed.IratAlairoJel = Utility.SetSqlCharsFromString(value, Typed.IratAlairoJel); }                                            
        }
                   
           
        /// <summary>
        /// CustomId property
        /// Sz�khely azonos�t�
        /// </summary>
        public String CustomId
        {
            get { return Utility.GetStringFromSqlString(Typed.CustomId); }
            set { Typed.CustomId = Utility.SetSqlStringFromString(value, Typed.CustomId); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}