
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_FeladatDefinicio BusinessDocument Class
    /// Le�rja, hogy az adott feladat v�grejajt�s�val bek�vetkezett esem�ny az adott t�pus� objektum adott �llapot�ban milyen �j feladatot gener�ljon, kijel�lt csoport sz�m�ra.
    /// </summary>
    [Serializable()]
    public class EREC_FeladatDefinicio : BaseEREC_FeladatDefinicio
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id_Kivalto property
        /// Az �j funkci� id�t�s�t kiv�lt� el�zm�ny funkci�.
        /// </summary>
        public String Funkcio_Id_Kivalto
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id_Kivalto); }
            set { Typed.Funkcio_Id_Kivalto = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id_Kivalto); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Metadefinicio_Id property
        /// Objektum metadefin�ci� azonos�t�
        /// </summary>
        public String Obj_Metadefinicio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Metadefinicio_Id); }
            set { Typed.Obj_Metadefinicio_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Metadefinicio_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjStateValue_Id property
        /// Az objektum �llapota a az adott feladat v�grehajt�sa el�tt.
        /// </summary>
        public String ObjStateValue_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjStateValue_Id); }
            set { Typed.ObjStateValue_Id = Utility.SetSqlGuidFromString(value, Typed.ObjStateValue_Id); }                                            
        }
                   
           
        /// <summary>
        /// FeladatDefinicioTipus property
        /// A feladat vagy �zenet gener�l�s felt�teleinek kateg�ri�i.
        /// KCS: FELADAT_DEFINICIO_TIPUS
        /// 1 - Esem�nyf�gg�
        /// 2 - Esem�ny- �s kateg�riaf�gg� (Obj_Metadefininicio_Id-vel defini�lt)
        /// 3 - Esem�ny-, kateg�ria- �s �llapotf�gg� (Obj_Metadefinicio_Id-vel �s ObjStateValue-val defini�lt)
        /// 4 - Esem�ny-, kateg�ria- �s folyamatf�gg� (mint 3, workflow �llapottal)
        /// 5 - �llapotf�gg� (�temezett �llapotvizsg�lat)
        /// </summary>
        public String FeladatDefinicioTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.FeladatDefinicioTipus); }
            set { Typed.FeladatDefinicioTipus = Utility.SetSqlStringFromString(value, Typed.FeladatDefinicioTipus); }                                            
        }
                   
           
        /// <summary>
        /// FeladatSorszam property
        /// Adott kiv�lt� esem�nyen bel�l futtatand� feladat sorrendi sz�ma.
        /// </summary>
        public String FeladatSorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladatSorszam); }
            set { Typed.FeladatSorszam = Utility.SetSqlInt32FromString(value, Typed.FeladatSorszam); }                                            
        }
                   
           
        /// <summary>
        /// MuveletDefinicio property
        /// A v�grehajtand� feldolgoz�si m�velet. KCS: MUVELET_DEFINICIO
        /// pl. sp_WF_FeladatKezelo - hat�rid�s feladatok/�rtes�t�sek kezel�se.
        /// </summary>
        public String MuveletDefinicio
        {
            get { return Utility.GetStringFromSqlString(Typed.MuveletDefinicio); }
            set { Typed.MuveletDefinicio = Utility.SetSqlStringFromString(value, Typed.MuveletDefinicio); }                                            
        }
                   
           
        /// <summary>
        /// SpecMuveletDefinicio property
        /// A v�grehajtand� speci�lis m�velet defin�ci�ja. KCS: SPECMUVELET_DEFINICIO
        /// pl. sp_WF_UgyitatSzignalasFeladat - feladat gener�l�s �gyirat szign�l�shoz
        /// </summary>
        public String SpecMuveletDefinicio
        {
            get { return Utility.GetStringFromSqlString(Typed.SpecMuveletDefinicio); }
            set { Typed.SpecMuveletDefinicio = Utility.SetSqlStringFromString(value, Typed.SpecMuveletDefinicio); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id_Inditando property
        /// KRT_Funkciok FK, a hat�rid�s feladatok eset�ben ez lesz ind�tand� funkci�k�nt be�ll�tva.
        /// </summary>
        public String Funkcio_Id_Inditando
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id_Inditando); }
            set { Typed.Funkcio_Id_Inditando = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id_Inditando); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id_Futtatando property
        /// 
        /// </summary>
        public String Funkcio_Id_Futtatando
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id_Futtatando); }
            set { Typed.Funkcio_Id_Futtatando = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id_Futtatando); }                                            
        }
                   
           
        /// <summary>
        /// BazisObjLeiro property
        /// A b�zisobjektum le�r�ja.
        /// </summary>
        public String BazisObjLeiro
        {
            get { return Utility.GetStringFromSqlString(Typed.BazisObjLeiro); }
            set { Typed.BazisObjLeiro = Utility.SetSqlStringFromString(value, Typed.BazisObjLeiro); }                                            
        }
                   
           
        /// <summary>
        /// FelelosTipus property
        /// Az Obj_Id_Felelos attributum szerinti felel�s t�pusa.
        /// KCS: FELADAT_FELELOS_TIPUS
        /// 1 - Csoport
        /// 2 - T�blaoszlop
        /// </summary>
        public String FelelosTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.FelelosTipus); }
            set { Typed.FelelosTipus = Utility.SetSqlStringFromString(value, Typed.FelelosTipus); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Felelos property
        /// A felel�s csoportot vagy csoporttagot defini�l� attributum azonos�t�ja.
        /// </summary>
        public String Obj_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Felelos); }
            set { Typed.Obj_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// FelelosLeiro property
        /// Az automatikusan el��rhat� feledat felel�s�nek meghat�roz�s�ra vonatkoz� defin�ci�.
        /// </summary>
        public String FelelosLeiro
        {
            get { return Utility.GetStringFromSqlString(Typed.FelelosLeiro); }
            set { Typed.FelelosLeiro = Utility.SetSqlStringFromString(value, Typed.FelelosLeiro); }                                            
        }
                   
           
        /// <summary>
        /// Idobazis property
        /// A hat�rid� sz�m�t�s b�zisideje. NULL �rt�k eset�n egyedileg meghat�rozand� adat. D�tumoszlop szerinti �rt�k meghat�roz�sa az esem�nyben �rintett objektum ObjTipus_Id_DateCol adata alapj�nt�rt�nik. KCS:FELADAT_IDOBAZIS
        /// 1 - Aktu�lis id�pont
        /// 2 - Aktu�lis d�tum
        /// 3 - D�tumoszlop
        /// 4 - D�tum t�rgysz�
        /// 5 - Helyettes�t� id�pont (ha az azonos�t� szerinti d�tum nem ismert vagy �res)
        /// 6 - Helyettes�t� d�tum (ha az azonos�t� szerinti d�tum nem ismert vagy �res)
        /// </summary>
        public String Idobazis
        {
            get { return Utility.GetStringFromSqlString(Typed.Idobazis); }
            set { Typed.Idobazis = Utility.SetSqlStringFromString(value, Typed.Idobazis); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_DateCol property
        /// Szabv�nyos d�tumoszlop vagy d�tumaadatot tartalmaz� t�rgysz� azonos�t�ja.
        /// </summary>
        public String ObjTip_Id_DateCol
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_DateCol); }
            set { Typed.ObjTip_Id_DateCol = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_DateCol); }                                            
        }
                   
           
        /// <summary>
        /// AtfutasiIdo property
        /// A feladat �tut�si ideje (a megadott Idoegyseg-ben). NULL �rt�k eset�n egyedileg meghat�rozand� adat.
        /// </summary>
        public String AtfutasiIdo
        {
            get { return Utility.GetStringFromSqlInt32(Typed.AtfutasiIdo); }
            set { Typed.AtfutasiIdo = Utility.SetSqlInt32FromString(value, Typed.AtfutasiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Idoegyseg property
        /// Az �tfut�si id� id�egys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az id�egys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        public String Idoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.Idoegyseg); }
            set { Typed.Idoegyseg = Utility.SetSqlStringFromString(value, Typed.Idoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// FeladatLeiras property
        /// Az automatikusan el��rhat� feladat sz�veges le�r�sa
        /// </summary>
        public String FeladatLeiras
        {
            get { return Utility.GetStringFromSqlString(Typed.FeladatLeiras); }
            set { Typed.FeladatLeiras = Utility.SetSqlStringFromString(value, Typed.FeladatLeiras); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// A feladat t�pusa. KCS: FELADAT_TIPUS
        /// F - Feladat (szabv�nyos vagy egyedileg defini�lt)
        /// M - Megjegyz�s (feladat n�lk�li megjegyz�s)
        /// 
        /// 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property
        /// Feladat priorit�s kateg�ri�k: KCS: FELADAT_PRIORITAS
        /// 11, 12, ... - Alacsony (�rtes�t�s n�lk�li)
        /// 21, 22, ... - K�zepes (T�R �rtes�t�s)
        /// 31, 32, ... - Magas  (E-mail �rtes�t�s)
        /// </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlString(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlStringFromString(value, Typed.Prioritas); }                                            
        }
                   
           
        /// <summary>
        /// LezarasPrioritas property
        /// Feladat lez�r�s�nak priorit�sa, ha az �llapot 3 vagy 4 lesz: KCS: FELADAT_PRIORITAS
        /// 11 - Alacsony (�rtes�t�s n�lk�li)
        /// 21 - K�zepes (T�R �rtes�t�s)
        /// 31 - Magas  (E-mail �rtes�t�s)
        /// </summary>
        public String LezarasPrioritas
        {
            get { return Utility.GetStringFromSqlString(Typed.LezarasPrioritas); }
            set { Typed.LezarasPrioritas = Utility.SetSqlStringFromString(value, Typed.LezarasPrioritas); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// Az esem�ny hat�s�ra be�ll�tand� �llapot a hat�rid�s feladatn�l. NULL �rt�k eset�n egyedileg meghat�rozand� adat.
        /// KCS: FELADAT_ALLAPOT �rt�kei:
        /// 0 - �j
        /// 1 - Nyitott
        /// 2 - Folyamatban
        /// 3 - Lez�rt
        /// 4 - Sztorn�zott
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property
        /// Objektum tipus Id
        /// Kapcsol�t�bl�ba: Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        public String Obj_type
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_type); }
            set { Typed.Obj_type = Utility.SetSqlStringFromString(value, Typed.Obj_type); }                                            
        }
                   
           
        /// <summary>
        /// Obj_SzukitoFeltetel property
        /// A konkr�t feladatra vagy �zenetre vonatkoz� lek�rdez�s sz�k�t� felt�tele.
        /// </summary>
        public String Obj_SzukitoFeltetel
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_SzukitoFeltetel); }
            set { Typed.Obj_SzukitoFeltetel = Utility.SetSqlStringFromString(value, Typed.Obj_SzukitoFeltetel); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}