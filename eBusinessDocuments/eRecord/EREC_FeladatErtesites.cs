
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_FeladatErtesites BusinessDocument Class
    /// Felhaszn�l� �ltal be�ll�that� riaszt�sok
    /// </summary>
    [Serializable()]
    public class EREC_FeladatErtesites : BaseEREC_FeladatErtesites
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// FeladatDefinicio_id property
        /// 
        /// </summary>
        public String FeladatDefinicio_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FeladatDefinicio_id); }
            set { Typed.FeladatDefinicio_id = Utility.SetSqlGuidFromString(value, Typed.FeladatDefinicio_id); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property
        /// 
        /// </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Ertesites property
        /// 
        /// </summary>
        public String Ertesites
        {
            get { return Utility.GetStringFromSqlChars(Typed.Ertesites); }
            set { Typed.Ertesites = Utility.SetSqlCharsFromString(value, Typed.Ertesites); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}