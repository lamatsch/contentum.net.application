
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_HataridosFeladatok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_HataridosFeladatok : BaseEREC_HataridosFeladatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Esemeny_Id_Kivalto property </summary>
        public String Esemeny_Id_Kivalto
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Esemeny_Id_Kivalto); }
            set { Typed.Esemeny_Id_Kivalto = Utility.SetSqlGuidFromString(value, Typed.Esemeny_Id_Kivalto); }                                            
        }
                   
           
        /// <summary>
        /// Esemeny_Id_Lezaro property </summary>
        public String Esemeny_Id_Lezaro
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Esemeny_Id_Lezaro); }
            set { Typed.Esemeny_Id_Lezaro = Utility.SetSqlGuidFromString(value, Typed.Esemeny_Id_Lezaro); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio_Id_Inditando property </summary>
        public String Funkcio_Id_Inditando
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Funkcio_Id_Inditando); }
            set { Typed.Funkcio_Id_Inditando = Utility.SetSqlGuidFromString(value, Typed.Funkcio_Id_Inditando); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_Felelos property </summary>
        public String Felhasznalo_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_Felelos); }
            set { Typed.Felhasznalo_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Kiado property </summary>
        public String Csoport_Id_Kiado
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Kiado); }
            set { Typed.Csoport_Id_Kiado = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Kiado); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_Kiado property </summary>
        public String Felhasznalo_Id_Kiado
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_Kiado); }
            set { Typed.Felhasznalo_Id_Kiado = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_Kiado); }                                            
        }
                   
           
        /// <summary>
        /// HataridosFeladat_Id property </summary>
        public String HataridosFeladat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.HataridosFeladat_Id); }
            set { Typed.HataridosFeladat_Id = Utility.SetSqlGuidFromString(value, Typed.HataridosFeladat_Id); }                                            
        }
                   
           
        /// <summary>
        /// ReszFeladatSorszam property </summary>
        public String ReszFeladatSorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ReszFeladatSorszam); }
            set { Typed.ReszFeladatSorszam = Utility.SetSqlInt32FromString(value, Typed.ReszFeladatSorszam); }                                            
        }
                   
           
        /// <summary>
        /// FeladatDefinicio_Id property </summary>
        public String FeladatDefinicio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FeladatDefinicio_Id); }
            set { Typed.FeladatDefinicio_Id = Utility.SetSqlGuidFromString(value, Typed.FeladatDefinicio_Id); }                                            
        }
                   
           
        /// <summary>
        /// FeladatDefinicio_Id_Lezaro property </summary>
        public String FeladatDefinicio_Id_Lezaro
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FeladatDefinicio_Id_Lezaro); }
            set { Typed.FeladatDefinicio_Id_Lezaro = Utility.SetSqlGuidFromString(value, Typed.FeladatDefinicio_Id_Lezaro); }                                            
        }
                   
           
        /// <summary>
        /// Forras property </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlChars(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlCharsFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Altipus property </summary>
        public String Altipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Altipus); }
            set { Typed.Altipus = Utility.SetSqlStringFromString(value, Typed.Altipus); }                                            
        }
                   
           
        /// <summary>
        /// Memo property </summary>
        public String Memo
        {
            get { return Utility.GetStringFromSqlChars(Typed.Memo); }
            set { Typed.Memo = Utility.SetSqlCharsFromString(value, Typed.Memo); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlString(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlStringFromString(value, Typed.Prioritas); }                                            
        }
                   
           
        /// <summary>
        /// LezarasPrioritas property </summary>
        public String LezarasPrioritas
        {
            get { return Utility.GetStringFromSqlString(Typed.LezarasPrioritas); }
            set { Typed.LezarasPrioritas = Utility.SetSqlStringFromString(value, Typed.LezarasPrioritas); }                                            
        }
                   
           
        /// <summary>
        /// KezdesiIdo property </summary>
        public String KezdesiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KezdesiIdo); }
            set { Typed.KezdesiIdo = Utility.SetSqlDateTimeFromString(value, Typed.KezdesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// IntezkHatarido property </summary>
        public String IntezkHatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IntezkHatarido); }
            set { Typed.IntezkHatarido = Utility.SetSqlDateTimeFromString(value, Typed.IntezkHatarido); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDatuma property </summary>
        public String LezarasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDatuma); }
            set { Typed.LezarasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_type property </summary>
        public String Obj_type
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_type); }
            set { Typed.Obj_type = Utility.SetSqlStringFromString(value, Typed.Obj_type); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Megoldas property </summary>
        public String Megoldas
        {
            get { return Utility.GetStringFromSqlString(Typed.Megoldas); }
            set { Typed.Megoldas = Utility.SetSqlStringFromString(value, Typed.Megoldas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}