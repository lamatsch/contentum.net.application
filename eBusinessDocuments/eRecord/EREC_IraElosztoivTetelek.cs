
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraElosztoivTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IraElosztoivTetelek : BaseEREC_IraElosztoivTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ElosztoIv_Id property </summary>
        public String ElosztoIv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ElosztoIv_Id); }
            set { Typed.ElosztoIv_Id = Utility.SetSqlGuidFromString(value, Typed.ElosztoIv_Id); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id property </summary>
        public String Cim_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id); }
            set { Typed.Cim_Id = Utility.SetSqlGuidFromString(value, Typed.Cim_Id); }                                            
        }
                   
           
        /// <summary>
        /// CimSTR property </summary>
        public String CimSTR
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR); }
            set { Typed.CimSTR = Utility.SetSqlStringFromString(value, Typed.CimSTR); }                                            
        }
                   
           
        /// <summary>
        /// NevSTR property </summary>
        public String NevSTR
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR); }
            set { Typed.NevSTR = Utility.SetSqlStringFromString(value, Typed.NevSTR); }                                            
        }
                   
           
        /// <summary>
        /// Kuldesmod property </summary>
        public String Kuldesmod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kuldesmod); }
            set { Typed.Kuldesmod = Utility.SetSqlStringFromString(value, Typed.Kuldesmod); }                                            
        }
                   
           
        /// <summary>
        /// Visszavarolag property </summary>
        public String Visszavarolag
        {
            get { return Utility.GetStringFromSqlChars(Typed.Visszavarolag); }
            set { Typed.Visszavarolag = Utility.SetSqlCharsFromString(value, Typed.Visszavarolag); }                                            
        }
                   
           
        /// <summary>
        /// VisszavarasiIdo property </summary>
        public String VisszavarasiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszavarasiIdo); }
            set { Typed.VisszavarasiIdo = Utility.SetSqlDateTimeFromString(value, Typed.VisszavarasiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Vissza_Nap_Mulva property </summary>
        public String Vissza_Nap_Mulva
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Vissza_Nap_Mulva); }
            set { Typed.Vissza_Nap_Mulva = Utility.SetSqlInt32FromString(value, Typed.Vissza_Nap_Mulva); }                                            
        }
                   
           
        /// <summary>
        /// AlairoSzerep property </summary>
        public String AlairoSzerep
        {
            get { return Utility.GetStringFromSqlChars(Typed.AlairoSzerep); }
            set { Typed.AlairoSzerep = Utility.SetSqlCharsFromString(value, Typed.AlairoSzerep); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}