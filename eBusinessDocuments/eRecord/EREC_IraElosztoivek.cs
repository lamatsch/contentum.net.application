
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraElosztoivek BusinessDocument Class
    /// Partnereket tartalmaz� template ... (Pl.: vezet�s�gnek k�ldend�)
    /// </summary>
    [Serializable()]
    public class EREC_IraElosztoivek : BaseEREC_IraElosztoivek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Ev property
        /// decimal(4)
        /// </summary>
        public String Ev
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ev); }
            set { Typed.Ev = Utility.SetSqlInt32FromString(value, Typed.Ev); }                                            
        }
                   
           
        /// <summary>
        /// Fajta property
        /// Az eloszt��v t�pusa, k�dt�ras. KCS: ELOSZTOIV_FAJTA 
        /// 1 - Irat eloszt��v
        /// 3 - Al��r�-lista
        /// 4 - Titkos�t� lista
        /// </summary>
        public String Fajta
        {
            get { return Utility.GetStringFromSqlString(Typed.Fajta); }
            set { Typed.Fajta = Utility.SetSqlStringFromString(value, Typed.Fajta); }                                            
        }
                   
           
        /// <summary>
        /// Kod property
        /// R�gi neve: ROVIDNEV
        /// </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// NEV property
        /// R�gi neve: MEGNEVEZES
        /// </summary>
        public String NEV
        {
            get { return Utility.GetStringFromSqlString(Typed.NEV); }
            set { Typed.NEV = Utility.SetSqlStringFromString(value, Typed.NEV); }                                            
        }
                   
           
        /// <summary>
        /// Hasznalat property
        /// Az eloszt��v haszn�lati k�r�t jel�li. KCS:ELOSZTOIV_HASZNALAT
        /// 0 - Glob�lis (mindenki �ltal el�rhet�)
        /// 1 - Szervezeti (a tulajdonos szervezeten bel�l el�rhet�)
        /// 2 - Egy�ni (a tulajdonos felhaszn�l� �ltal el�rhet�)
        /// </summary>
        public String Hasznalat
        {
            get { return Utility.GetStringFromSqlString(Typed.Hasznalat); }
            set { Typed.Hasznalat = Utility.SetSqlStringFromString(value, Typed.Hasznalat); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// 
        /// </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}