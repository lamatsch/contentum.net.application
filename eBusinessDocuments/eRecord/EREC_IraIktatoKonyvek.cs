
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIktatoKonyvek BusinessDocument Class
    /// �gyiratkezel�si k�nyvek. T�pusdefin�ci�: IktatoErkezteto.
    /// I = Iktat�k�nyv
    /// E = �rkeztetok�nyv 
    /// P = Postak�nyv
    /// -- �j (2017.07.13)
    /// S = Iratkezel�si seg�dlet
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    Statusz
    /// -- �j oszlopok (2017.07.13)
    /// Terjedelem
    /// SelejtezesDatuma
    /// KezelesTipusa
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_IraIktatoKonyvek : BaseEREC_IraIktatoKonyvek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Ev property
        /// Iktat�k�nyv �ve.
        /// </summary>
        public String Ev
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ev); }
            set { Typed.Ev = Utility.SetSqlInt32FromString(value, Typed.Ev); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Iktat�k�nyv azonos�t�,egyedi
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// DefaultIrattariTetelszam property
        /// R�gebben: FK volt, k�sobb nem kell
        /// </summary>
        public String DefaultIrattariTetelszam
        {
            get { return Utility.GetStringFromSqlGuid(Typed.DefaultIrattariTetelszam); }
            set { Typed.DefaultIrattariTetelszam = Utility.SetSqlGuidFromString(value, Typed.DefaultIrattariTetelszam); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Iktat�k�nyv megnevez�se
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// MegkulJelzes property
        /// Iktat�sz�m megk�l�nb�zteto jelz�se (az iktat�sz�m elso eleme)
        /// </summary>
        public String MegkulJelzes
        {
            get { return Utility.GetStringFromSqlString(Typed.MegkulJelzes); }
            set { Typed.MegkulJelzes = Utility.SetSqlStringFromString(value, Typed.MegkulJelzes); }                                            
        }
                   
           
        /// <summary>
        /// Iktatohely property
        /// Sz�veges meghat�roz�s
        /// </summary>
        public String Iktatohely
        {
            get { return Utility.GetStringFromSqlString(Typed.Iktatohely); }
            set { Typed.Iktatohely = Utility.SetSqlStringFromString(value, Typed.Iktatohely); }                                            
        }
                   
           
        /// <summary>
        /// KozpontiIktatasJelzo property
        /// 1-k�zponti iktat�s, 0-egy�bk�nt.
        /// </summary>
        public String KozpontiIktatasJelzo
        {
            get { return Utility.GetStringFromSqlChars(Typed.KozpontiIktatasJelzo); }
            set { Typed.KozpontiIktatasJelzo = Utility.SetSqlCharsFromString(value, Typed.KozpontiIktatasJelzo); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoFoszam property
        /// Utols� �rkeztetosz�m, vagy iktat�si sz�m (fosz�m).
        /// </summary>
        public String UtolsoFoszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoFoszam); }
            set { Typed.UtolsoFoszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoFoszam); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoSorszam property
        /// Adott iktat�k�nyvbe val� munka�gy iktat�s eset�n az utols� sorsz�m.
        /// </summary>
        public String UtolsoSorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoSorszam); }
            set { Typed.UtolsoSorszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoSorszam); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Olvaso property
        /// �ltal�ban a mindenki csoport �ll itt. Ha nem mindenki l�thatja, akkor egy konkr�t. Ez t�bbs�g�ben mesters�ges csoport (azaz nem felhaszn�l�, persze ez is lehet,
        /// de ekkor neki kell adminisztr�lnia.)
        /// </summary>
        public String Csoport_Id_Olvaso
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Olvaso); }
            set { Typed.Csoport_Id_Olvaso = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Olvaso); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Ebben kiz�r�lag a Rendszergazd�k csoportja lehet. (Egyedi rendszergazda sem lehet!)
        /// </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// IktSzamOsztas property
        /// KCS: IKT_SZAM_OSZTAS
        /// </summary>
        public String IktSzamOsztas
        {
            get { return Utility.GetStringFromSqlString(Typed.IktSzamOsztas); }
            set { Typed.IktSzamOsztas = Utility.SetSqlStringFromString(value, Typed.IktSzamOsztas); }                                            
        }
                   
           
        /// <summary>
        /// FormatumKod property
        /// Iktat�sz�m form�tum t�pusa
        /// 0-  megk�l�nb�zteto jelz�s/fosz�m-alsz�m/�v/
        /// 1-  fosz�m-alsz�m/�v/megk�l�nb�zteto jelz�s
        /// 
        /// </summary>
        public String FormatumKod
        {
            get { return Utility.GetStringFromSqlChars(Typed.FormatumKod); }
            set { Typed.FormatumKod = Utility.SetSqlCharsFromString(value, Typed.FormatumKod); }                                            
        }
                   
           
        /// <summary>
        /// Titkos property
        /// 1-Igen/0-nem.
        /// </summary>
        public String Titkos
        {
            get { return Utility.GetStringFromSqlChars(Typed.Titkos); }
            set { Typed.Titkos = Utility.SetSqlCharsFromString(value, Typed.Titkos); }                                            
        }
                   
           
        /// <summary>
        /// IktatoErkezteto property
        /// A k�nyv t�pusa:
        /// I = Iktat�k�nyv
        /// E = �rkeztetok�nyv,
        /// P = Postak�nyv
        /// </summary>
        public String IktatoErkezteto
        {
            get { return Utility.GetStringFromSqlChars(Typed.IktatoErkezteto); }
            set { Typed.IktatoErkezteto = Utility.SetSqlCharsFromString(value, Typed.IktatoErkezteto); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDatuma property
        /// Az iktat�k�ny �ves lez�r�s�nak idopontja.
        /// </summary>
        public String LezarasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDatuma); }
            set { Typed.LezarasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// PostakonyvVevokod property
        /// A Magyar Posta Zrt-n�l a Hivatalt azonos�t� k�d.
        /// </summary>
        public String PostakonyvVevokod
        {
            get { return Utility.GetStringFromSqlString(Typed.PostakonyvVevokod); }
            set { Typed.PostakonyvVevokod = Utility.SetSqlStringFromString(value, Typed.PostakonyvVevokod); }                                            
        }
                   
           
        /// <summary>
        /// PostakonyvMegallapodasAzon property
        /// A postak�nyv meg�llapod�st a Magyar Posta ZRT-n�l azonos�t� k�d.
        /// </summary>
        public String PostakonyvMegallapodasAzon
        {
            get { return Utility.GetStringFromSqlString(Typed.PostakonyvMegallapodasAzon); }
            set { Typed.PostakonyvMegallapodasAzon = Utility.SetSqlStringFromString(value, Typed.PostakonyvMegallapodasAzon); }                                            
        }
                   
           
        /// <summary>
        /// EFeladoJegyzekUgyfelAdatok property
        /// Az elekronikus felad�jegyz�k elk�sz�t�s�hez sz�ks�ges UGYFEL.XML (ahol az �gyf�l a felad� szervezet, a posta szempontj�b�l �rtendo) - postak�nyvh�z kapcsol�dik. Nem XML-k�nt, hanem nvarchar-k�nt t�roljuk.
        /// </summary>
        public String EFeladoJegyzekUgyfelAdatok
        {
            get { return Utility.GetStringFromSqlString(Typed.EFeladoJegyzekUgyfelAdatok); }
            set { Typed.EFeladoJegyzekUgyfelAdatok = Utility.SetSqlStringFromString(value, Typed.EFeladoJegyzekUgyfelAdatok); }                                            
        }
                   
           
        /// <summary>
        /// HitelesExport_Dok_ID property
        /// �ltal�ban a mindenki csoport �ll itt. Ha nem mindenki l�thatja, akkor egy konkr�t. Ez t�bbs�g�ben mesters�ges csoport (azaz nem felhaszn�l�, persze ez is lehet,
        /// de ekkor neki kell adminisztr�lnia.)
        /// </summary>
        public String HitelesExport_Dok_ID
        {
            get { return Utility.GetStringFromSqlGuid(Typed.HitelesExport_Dok_ID); }
            set { Typed.HitelesExport_Dok_ID = Utility.SetSqlGuidFromString(value, Typed.HitelesExport_Dok_ID); }                                            
        }
                   
           
        /// <summary>
        /// Ujranyitando property
        /// 1-Igen/0-nem.
        /// </summary>
        public String Ujranyitando
        {
            get { return Utility.GetStringFromSqlChars(Typed.Ujranyitando); }
            set { Typed.Ujranyitando = Utility.SetSqlCharsFromString(value, Typed.Ujranyitando); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// Statusz property
        /// 
        /// </summary>
        public String Statusz
        {
            get { return Utility.GetStringFromSqlChars(Typed.Statusz); }
            set { Typed.Statusz = Utility.SetSqlCharsFromString(value, Typed.Statusz); }                                            
        }
                   
           
        /// <summary>
        /// Terjedelem property
        /// Iktat�sz�m megk�l�nb�zteto jelz�se (az iktat�sz�m elso eleme)
        /// </summary>
        public String Terjedelem
        {
            get { return Utility.GetStringFromSqlString(Typed.Terjedelem); }
            set { Typed.Terjedelem = Utility.SetSqlStringFromString(value, Typed.Terjedelem); }                                            
        }
                   
           
        /// <summary>
        /// SelejtezesDatuma property
        /// Az iktat�k�ny �ves lez�r�s�nak idopontja.
        /// </summary>
        public String SelejtezesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SelejtezesDatuma); }
            set { Typed.SelejtezesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.SelejtezesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// KezelesTipusa property
        /// 1-k�zponti iktat�s, 0-egy�bk�nt.
        /// </summary>
        public String KezelesTipusa
        {
            get { return Utility.GetStringFromSqlChars(Typed.KezelesTipusa); }
            set { Typed.KezelesTipusa = Utility.SetSqlCharsFromString(value, Typed.KezelesTipusa); }                                            
        }
                           }
   
}