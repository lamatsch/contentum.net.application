
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIratok BusinessDocument Class
    /// Irat entit�s
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    Munkaallomas,
    ///    UgyintezesModja
    /// 
    /// �j oszlopok
    /// 
    /// ModositasErvenyessegKezdete
    /// MegszuntetoHatarozat
    /// FelulvizsgalatDatuma
    /// Felulvizsgalo_id
    /// </summary>
    [Serializable()]
    public class EREC_IraIratok : BaseEREC_IraIratok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi GUID azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// PostazasIranya property
        /// KCS: POSTAZAS_IRANYA
        /// 0 - Bels�
        /// 1 - Bej�v�
        /// </summary>
        public String PostazasIranya
        {
            get { return Utility.GetStringFromSqlChars(Typed.PostazasIranya); }
            set { Typed.PostazasIranya = Utility.SetSqlCharsFromString(value, Typed.PostazasIranya); }                                            
        }
                   
           
        /// <summary>
        /// Alszam property
        /// Iktat�si alsz�m
        /// </summary>
        public String Alszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Alszam); }
            set { Typed.Alszam = Utility.SetSqlInt32FromString(value, Typed.Alszam); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property
        /// Munkairat sorsz�m.
        /// </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoSorszam property
        /// Az irathoz (vagy munkairathoz) tartoz� iratp�ld�nyok utols� sorsz�ma.
        /// </summary>
        public String UtolsoSorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoSorszam); }
            set { Typed.UtolsoSorszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoSorszam); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyIratDarab_Id property
        /// �gyirat darab Id
        /// </summary>
        public String UgyUgyIratDarab_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyIratDarab_Id); }
            set { Typed.UgyUgyIratDarab_Id = Utility.SetSqlGuidFromString(value, Typed.UgyUgyIratDarab_Id); }                                            
        }
                   
           
        /// <summary>
        /// Kategoria property
        /// KCS: IRATKATEGORIA
        /// </summary>
        public String Kategoria
        {
            get { return Utility.GetStringFromSqlString(Typed.Kategoria); }
            set { Typed.Kategoria = Utility.SetSqlStringFromString(value, Typed.Kategoria); }                                            
        }
                   
           
        /// <summary>
        /// HivatkozasiSzam property
        /// �gyf�l, bek�ld� dokumenum�nak (fizikailag olvashat�) hivatkoz�si sz�ma (pl. egy sz�mlasz�m).
        /// </summary>
        public String HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.HivatkozasiSzam); }
            set { Typed.HivatkozasiSzam = Utility.SetSqlStringFromString(value, Typed.HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// IktatasDatuma property
        /// Iktat�s d�tuma
        /// </summary>
        public String IktatasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IktatasDatuma); }
            set { Typed.IktatasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.IktatasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// ExpedialasDatuma property
        /// Expedi�l�s d�tuma, ez sem kell
        /// </summary>
        public String ExpedialasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ExpedialasDatuma); }
            set { Typed.ExpedialasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.ExpedialasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// ExpedialasModja property
        /// KCS: EXPEDIALAS_MODJA, nem kell
        /// </summary>
        public String ExpedialasModja
        {
            get { return Utility.GetStringFromSqlString(Typed.ExpedialasModja); }
            set { Typed.ExpedialasModja = Utility.SetSqlStringFromString(value, Typed.ExpedialasModja); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial property
        /// Expedi�l�s m�dja, ez sem kell
        /// </summary>
        public String FelhasznaloCsoport_Id_Expedial
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Expedial); }
            set { Typed.FelhasznaloCsoport_Id_Expedial = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Expedial); }                                            
        }
                   
           
        /// <summary>
        /// Targy property
        /// Irat t�rgya, megnevez�se
        /// </summary>
        public String Targy
        {
            get { return Utility.GetStringFromSqlString(Typed.Targy); }
            set { Typed.Targy = Utility.SetSqlStringFromString(value, Typed.Targy); }                                            
        }
                   
           
        /// <summary>
        /// Jelleg property
        /// KCS.??
        /// </summary>
        public String Jelleg
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelleg); }
            set { Typed.Jelleg = Utility.SetSqlStringFromString(value, Typed.Jelleg); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Iktato property
        /// Iktat� felhaszn�l� Id
        /// </summary>
        public String FelhasznaloCsoport_Id_Iktato
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Iktato); }
            set { Typed.FelhasznaloCsoport_Id_Iktato = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Iktato); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kiadmany property
        /// 
        /// </summary>
        public String FelhasznaloCsoport_Id_Kiadmany
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Kiadmany); }
            set { Typed.FelhasznaloCsoport_Id_Kiadmany = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Kiadmany); }                                            
        }


        /// <summary>
        /// UgyintezesAlapja property
        /// KCS: ELSODLEGES_ADATHORDOZO:
        /// PapirAlapu_Irat = "01"
        /// Elektronikus_irat = "09"
        /// </summary>
        public String UgyintezesAlapja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesAlapja); }
            set { Typed.UgyintezesAlapja = Utility.SetSqlStringFromString(value, Typed.UgyintezesAlapja); }                                            
        }
                   
           
        /// <summary>
        /// AdathordozoTipusa property
        /// KCS:UGYINTEZES_ALAPJA
        /// 
        /// (Kor�bban: ADATHORDOZO_TIPUSA)
        /// 
        /// </summary>
        public String AdathordozoTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.AdathordozoTipusa); }
            set { Typed.AdathordozoTipusa = Utility.SetSqlStringFromString(value, Typed.AdathordozoTipusa); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Az irat iktat�j�nak az iktat�s id�pontj�ban �rv�nyes szervezeti azonos�t�ja.
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Ugyfelelos property
        /// �gyfelel�s
        /// </summary>
        public String Csoport_Id_Ugyfelelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Ugyfelelos); }
            set { Typed.Csoport_Id_Ugyfelelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Ugyfelelos); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Az irat iktat�j�nak azonos�t�ja.
        /// </summary>
        public String FelhasznaloCsoport_Id_Orzo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Orzo); }
            set { Typed.FelhasznaloCsoport_Id_Orzo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Orzo); }                                            
        }
                   
           
        /// <summary>
        /// KiadmanyozniKell property
        /// Az adott iratot kell-e postazas elott kiadmanyozni? 
        /// Alapertelmezes szerint nem (bejovoknel eleve nem, kimenoknel altalaban nem). 1-igen, 0-nem
        /// </summary>
        public String KiadmanyozniKell
        {
            get { return Utility.GetStringFromSqlChars(Typed.KiadmanyozniKell); }
            set { Typed.KiadmanyozniKell = Utility.SetSqlCharsFromString(value, Typed.KiadmanyozniKell); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez property
        /// �gyint�z�, �ltal�ban el�ad�
        /// </summary>
        public String FelhasznaloCsoport_Id_Ugyintez
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Ugyintez); }
            set { Typed.FelhasznaloCsoport_Id_Ugyintez = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Ugyintez); }                                            
        }
                   
           
        /// <summary>
        /// Hatarido property
        /// Elint�z�si hat�rid�
        /// </summary>
        public String Hatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Hatarido); }
            set { Typed.Hatarido = Utility.SetSqlDateTimeFromString(value, Typed.Hatarido); }                                            
        }
                   
           
        /// <summary>
        /// MegorzesiIdo property
        /// Irat meg�rz�s eddig a d�tumig.
        /// </summary>
        public String MegorzesiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegorzesiIdo); }
            set { Typed.MegorzesiIdo = Utility.SetSqlDateTimeFromString(value, Typed.MegorzesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// IratFajta property
        /// KCS: IRAT_FAJTA
        /// </summary>
        public String IratFajta
        {
            get { return Utility.GetStringFromSqlString(Typed.IratFajta); }
            set { Typed.IratFajta = Utility.SetSqlStringFromString(value, Typed.IratFajta); }                                            
        }
                   
           
        /// <summary>
        /// Irattipus property
        /// KCS:??
        /// </summary>
        public String Irattipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Irattipus); }
            set { Typed.Irattipus = Utility.SetSqlStringFromString(value, Typed.Irattipus); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemenyek_Id property
        /// K�ldem�ny Id
        /// </summary>
        public String KuldKuldemenyek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemenyek_Id); }
            set { Typed.KuldKuldemenyek_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemenyek_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: IRAT_ALLAPOT
        /// 0 - Megnyitott (munkairat)
        /// 1 - Iktatand� (munkairat)
        /// 2 - Befagyasztott (munkairat)
        /// 04 - Iktatott
        /// 06 - �tiktatott
        /// 30 - Kiadm�nyozott
        /// 90 - Sztorn�zott
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Iktat�sz�m sz�vegesen 
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// GeneraltTargy property
        /// el�re r�gz�tett sz�veg, t�rgy az iratmetadefici�k t�bl�b�l 
        /// </summary>
        public String GeneraltTargy
        {
            get { return Utility.GetStringFromSqlString(Typed.GeneraltTargy); }
            set { Typed.GeneraltTargy = Utility.SetSqlStringFromString(value, Typed.GeneraltTargy); }                                            
        }
                   
           
        /// <summary>
        /// IratMetaDef_Id property
        /// Az iratmetadefinici� t�bl�ra mutat
        /// </summary>
        public String IratMetaDef_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IratMetaDef_Id); }
            set { Typed.IratMetaDef_Id = Utility.SetSqlGuidFromString(value, Typed.IratMetaDef_Id); }                                            
        }
                   
           
        /// <summary>
        /// IrattarbaKuldDatuma  property
        /// Iratt�rba k�ld�s d�tuma 
        /// </summary>
        public String IrattarbaKuldDatuma 
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IrattarbaKuldDatuma ); }
            set { Typed.IrattarbaKuldDatuma  = Utility.SetSqlDateTimeFromString(value, Typed.IrattarbaKuldDatuma ); }                                            
        }
                   
           
        /// <summary>
        /// IrattarbaVetelDat property
        /// Iratt�rba v�tel d�tuma
        /// </summary>
        public String IrattarbaVetelDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IrattarbaVetelDat); }
            set { Typed.IrattarbaVetelDat = Utility.SetSqlDateTimeFromString(value, Typed.IrattarbaVetelDat); }                                            
        }
                   
           
        /// <summary>
        /// Ugyirat_Id property
        /// Az atu�lis �gyirat Id-je (szerel�s eset�n a legf�ls� szint), egyel�re nem haszn�lt
        /// </summary>
        public String Ugyirat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Ugyirat_Id); }
            set { Typed.Ugyirat_Id = Utility.SetSqlGuidFromString(value, Typed.Ugyirat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Minosites property
        /// A nyilv�nos �s nem nyilv�nos iratok megk�l�nb�ztet�se. KCS: IRAT_MINOSITES
        ///  �res- nyilv�nos
        ///  10 - bels� haszn�lat�, nem nyilv�nos
        ///  20 - d�nt�s el�k�sz�t�, nem nyilv�nos
        /// </summary>
        public String Minosites
        {
            get { return Utility.GetStringFromSqlString(Typed.Minosites); }
            set { Typed.Minosites = Utility.SetSqlStringFromString(value, Typed.Minosites); }                                            
        }
                   
           
        /// <summary>
        /// Elintezett property
        /// 
        /// </summary>
        public String Elintezett
        {
            get { return Utility.GetStringFromSqlChars(Typed.Elintezett); }
            set { Typed.Elintezett = Utility.SetSqlCharsFromString(value, Typed.Elintezett); }                                            
        }
                   
           
        /// <summary>
        /// IratHatasaUgyintezesre property
        /// KCS: IRAT_HATASA_UGYINTEZESRE
        /// </summary>
        public String IratHatasaUgyintezesre
        {
            get { return Utility.GetStringFromSqlString(Typed.IratHatasaUgyintezesre); }
            set { Typed.IratHatasaUgyintezesre = Utility.SetSqlStringFromString(value, Typed.IratHatasaUgyintezesre); }                                            
        }
                   
           
        /// <summary>
        /// FelfuggesztesOka property
        /// 
        /// </summary>
        public String FelfuggesztesOka
        {
            get { return Utility.GetStringFromSqlString(Typed.FelfuggesztesOka); }
            set { Typed.FelfuggesztesOka = Utility.SetSqlStringFromString(value, Typed.FelfuggesztesOka); }                                            
        }
                   
           
        /// <summary>
        /// LezarasOka property
        /// 
        /// </summary>
        public String LezarasOka
        {
            get { return Utility.GetStringFromSqlString(Typed.LezarasOka); }
            set { Typed.LezarasOka = Utility.SetSqlStringFromString(value, Typed.LezarasOka); }                                            
        }
                   
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        public String Munkaallomas
        {
            get { return Utility.GetStringFromSqlString(Typed.Munkaallomas); }
            set { Typed.Munkaallomas = Utility.SetSqlStringFromString(value, Typed.Munkaallomas); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesModja property
        /// 
        /// </summary>
        public String UgyintezesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesModja); }
            set { Typed.UgyintezesModja = Utility.SetSqlStringFromString(value, Typed.UgyintezesModja); }                                            
        }
                   
           
        /// <summary>
        /// IntezesIdopontja property
        /// 
        /// </summary>
        public String IntezesIdopontja
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IntezesIdopontja); }
            set { Typed.IntezesIdopontja = Utility.SetSqlDateTimeFromString(value, Typed.IntezesIdopontja); }                                            
        }
                   
           
        /// <summary>
        /// IntezesiIdo property
        /// �tfut�si id� a megadott id�egys�gben (�gyirat, elj�r�si szakasz, irat)
        /// </summary>
        public String IntezesiIdo
        {
            get { return Utility.GetStringFromSqlString(Typed.IntezesiIdo); }
            set { Typed.IntezesiIdo = Utility.SetSqlStringFromString(value, Typed.IntezesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// IntezesiIdoegyseg property
        /// Az �tfut�si id� id�egys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az id�egys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        public String IntezesiIdoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.IntezesiIdoegyseg); }
            set { Typed.IntezesiIdoegyseg = Utility.SetSqlStringFromString(value, Typed.IntezesiIdoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// UzemzavarKezdete property
        /// 
        /// </summary>
        public String UzemzavarKezdete
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UzemzavarKezdete); }
            set { Typed.UzemzavarKezdete = Utility.SetSqlDateTimeFromString(value, Typed.UzemzavarKezdete); }                                            
        }
                   
           
        /// <summary>
        /// UzemzavarVege property
        /// 
        /// </summary>
        public String UzemzavarVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UzemzavarVege); }
            set { Typed.UzemzavarVege = Utility.SetSqlDateTimeFromString(value, Typed.UzemzavarVege); }                                            
        }
                   
           
        /// <summary>
        /// UzemzavarIgazoloIratSzama property
        /// 
        /// </summary>
        public String UzemzavarIgazoloIratSzama
        {
            get { return Utility.GetStringFromSqlString(Typed.UzemzavarIgazoloIratSzama); }
            set { Typed.UzemzavarIgazoloIratSzama = Utility.SetSqlStringFromString(value, Typed.UzemzavarIgazoloIratSzama); }                                            
        }
                   
           
        /// <summary>
        /// FelfuggesztettNapokSzama property
        /// 
        /// </summary>
        public String FelfuggesztettNapokSzama
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FelfuggesztettNapokSzama); }
            set { Typed.FelfuggesztettNapokSzama = Utility.SetSqlInt32FromString(value, Typed.FelfuggesztettNapokSzama); }                                            
        }
                   
           
        /// <summary>
        /// VisszafizetesJogcime property
        /// KCS: VISSZAFIZETES_JOGCIME
        /// - 01- d�j
        /// - 02 - illet�k
        /// </summary>
        public String VisszafizetesJogcime
        {
            get { return Utility.GetStringFromSqlString(Typed.VisszafizetesJogcime); }
            set { Typed.VisszafizetesJogcime = Utility.SetSqlStringFromString(value, Typed.VisszafizetesJogcime); }                                            
        }
                   
           
        /// <summary>
        /// VisszafizetesOsszege property
        /// 
        /// </summary>
        public String VisszafizetesOsszege
        {
            get { return Utility.GetStringFromSqlInt32(Typed.VisszafizetesOsszege); }
            set { Typed.VisszafizetesOsszege = Utility.SetSqlInt32FromString(value, Typed.VisszafizetesOsszege); }                                            
        }
                   
           
        /// <summary>
        /// VisszafizetesHatarozatSzama property
        /// 
        /// </summary>
        public String VisszafizetesHatarozatSzama
        {
            get { return Utility.GetStringFromSqlString(Typed.VisszafizetesHatarozatSzama); }
            set { Typed.VisszafizetesHatarozatSzama = Utility.SetSqlStringFromString(value, Typed.VisszafizetesHatarozatSzama); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_VisszafizetesCimzet property
        /// 
        /// </summary>
        public String Partner_Id_VisszafizetesCimzet
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_VisszafizetesCimzet); }
            set { Typed.Partner_Id_VisszafizetesCimzet = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_VisszafizetesCimzet); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Nev_VisszafizetCimzett property
        /// 
        /// </summary>
        public String Partner_Nev_VisszafizetCimzett
        {
            get { return Utility.GetStringFromSqlString(Typed.Partner_Nev_VisszafizetCimzett); }
            set { Typed.Partner_Nev_VisszafizetCimzett = Utility.SetSqlStringFromString(value, Typed.Partner_Nev_VisszafizetCimzett); }                                            
        }
                   
           
        /// <summary>
        /// VisszafizetesHatarido property
        /// 
        /// </summary>
        public String VisszafizetesHatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszafizetesHatarido); }
            set { Typed.VisszafizetesHatarido = Utility.SetSqlDateTimeFromString(value, Typed.VisszafizetesHatarido); }                                            
        }
                   
           
        /// <summary>
        /// Minosito property
        /// 
        /// </summary>
        public String Minosito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Minosito); }
            set { Typed.Minosito = Utility.SetSqlGuidFromString(value, Typed.Minosito); }                                            
        }
                   
           
        /// <summary>
        /// MinositesErvenyessegiIdeje property
        /// 
        /// </summary>
        public String MinositesErvenyessegiIdeje
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesErvenyessegiIdeje); }
            set { Typed.MinositesErvenyessegiIdeje = Utility.SetSqlDateTimeFromString(value, Typed.MinositesErvenyessegiIdeje); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMennyiseg property
        /// 
        /// </summary>
        public String TerjedelemMennyiseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.TerjedelemMennyiseg); }
            set { Typed.TerjedelemMennyiseg = Utility.SetSqlInt32FromString(value, Typed.TerjedelemMennyiseg); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg property
        /// 
        /// </summary>
        public String TerjedelemMennyisegiEgyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.TerjedelemMennyisegiEgyseg); }
            set { Typed.TerjedelemMennyisegiEgyseg = Utility.SetSqlStringFromString(value, Typed.TerjedelemMennyisegiEgyseg); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMegjegyzes property
        /// 
        /// </summary>
        public String TerjedelemMegjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.TerjedelemMegjegyzes); }
            set { Typed.TerjedelemMegjegyzes = Utility.SetSqlStringFromString(value, Typed.TerjedelemMegjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        public String SelejtezesDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SelejtezesDat); }
            set { Typed.SelejtezesDat = Utility.SetSqlDateTimeFromString(value, Typed.SelejtezesDat); }                                            
        }
                   
           
        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtez� v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        public String FelhCsoport_Id_Selejtezo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhCsoport_Id_Selejtezo); }
            set { Typed.FelhCsoport_Id_Selejtezo = Utility.SetSqlGuidFromString(value, Typed.FelhCsoport_Id_Selejtezo); }                                            
        }
                   
           
        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvev� neve
        /// </summary>
        public String LeveltariAtvevoNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.LeveltariAtvevoNeve); }
            set { Typed.LeveltariAtvevoNeve = Utility.SetSqlStringFromString(value, Typed.LeveltariAtvevoNeve); }                                            
        }
                   
           
        /// <summary>
        /// ModositasErvenyessegKezdete property
        /// 
        /// </summary>
        public String ModositasErvenyessegKezdete
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ModositasErvenyessegKezdete); }
            set { Typed.ModositasErvenyessegKezdete = Utility.SetSqlDateTimeFromString(value, Typed.ModositasErvenyessegKezdete); }                                            
        }
                   
           
        /// <summary>
        /// MegszuntetoHatarozat property
        /// 
        /// </summary>
        public String MegszuntetoHatarozat
        {
            get { return Utility.GetStringFromSqlString(Typed.MegszuntetoHatarozat); }
            set { Typed.MegszuntetoHatarozat = Utility.SetSqlStringFromString(value, Typed.MegszuntetoHatarozat); }                                            
        }
                   
           
        /// <summary>
        /// FelulvizsgalatDatuma property
        /// 
        /// </summary>
        public String FelulvizsgalatDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FelulvizsgalatDatuma); }
            set { Typed.FelulvizsgalatDatuma = Utility.SetSqlDateTimeFromString(value, Typed.FelulvizsgalatDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Felulvizsgalo_id property
        /// 
        /// </summary>
        public String Felulvizsgalo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felulvizsgalo_id); }
            set { Typed.Felulvizsgalo_id = Utility.SetSqlGuidFromString(value, Typed.Felulvizsgalo_id); }                                            
        }
                   
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm property
        /// 
        /// </summary>
        public String MinositesFelulvizsgalatEredm
        {
            get { return Utility.GetStringFromSqlString(Typed.MinositesFelulvizsgalatEredm); }
            set { Typed.MinositesFelulvizsgalatEredm = Utility.SetSqlStringFromString(value, Typed.MinositesFelulvizsgalatEredm); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesKezdoDatuma property
        /// 
        /// </summary>
        public String UgyintezesKezdoDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UgyintezesKezdoDatuma); }
            set { Typed.UgyintezesKezdoDatuma = Utility.SetSqlDateTimeFromString(value, Typed.UgyintezesKezdoDatuma); }                                            
        }
                   
           
        /// <summary>
        /// EljarasiSzakasz property
        /// 
        /// </summary>
        public String EljarasiSzakasz
        {
            get { return Utility.GetStringFromSqlString(Typed.EljarasiSzakasz); }
            set { Typed.EljarasiSzakasz = Utility.SetSqlStringFromString(value, Typed.EljarasiSzakasz); }                                            
        }
                   
           
        /// <summary>
        /// HatralevoNapok property
        /// 
        /// </summary>
        public String HatralevoNapok
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatralevoNapok); }
            set { Typed.HatralevoNapok = Utility.SetSqlInt32FromString(value, Typed.HatralevoNapok); }                                            
        }
                   
           
        /// <summary>
        /// HatralevoMunkaNapok property
        /// 
        /// </summary>
        public String HatralevoMunkaNapok
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatralevoMunkaNapok); }
            set { Typed.HatralevoMunkaNapok = Utility.SetSqlInt32FromString(value, Typed.HatralevoMunkaNapok); }                                            
        }
                   
           
        /// <summary>
        /// Ugy_Fajtaja property
        /// 
        /// </summary>
        public String Ugy_Fajtaja
        {
            get { return Utility.GetStringFromSqlString(Typed.Ugy_Fajtaja); }
            set { Typed.Ugy_Fajtaja = Utility.SetSqlStringFromString(value, Typed.Ugy_Fajtaja); }                                            
        }
                   
           
        /// <summary>
        /// MinositoSzervezet property
        /// 
        /// </summary>
        public String MinositoSzervezet
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MinositoSzervezet); }
            set { Typed.MinositoSzervezet = Utility.SetSqlGuidFromString(value, Typed.MinositoSzervezet); }                                            
        }
                   
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag property
        /// 
        /// </summary>
        public String MinositesFelulvizsgalatBizTag
        {
            get { return Utility.GetStringFromSqlString(Typed.MinositesFelulvizsgalatBizTag); }
            set { Typed.MinositesFelulvizsgalatBizTag = Utility.SetSqlStringFromString(value, Typed.MinositesFelulvizsgalatBizTag); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}