
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIratokDokumentumok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IraIratokDokumentumok : BaseEREC_IraIratokDokumentumok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Lapszam property </summary>
        public String Lapszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Lapszam); }
            set { Typed.Lapszam = Utility.SetSqlInt32FromString(value, Typed.Lapszam); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// Forras property </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlString(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlStringFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// Formatum property </summary>
        public String Formatum
        {
            get { return Utility.GetStringFromSqlString(Typed.Formatum); }
            set { Typed.Formatum = Utility.SetSqlStringFromString(value, Typed.Formatum); }                                            
        }
                   
           
        /// <summary>
        /// Vonalkodozas property </summary>
        public String Vonalkodozas
        {
            get { return Utility.GetStringFromSqlChars(Typed.Vonalkodozas); }
            set { Typed.Vonalkodozas = Utility.SetSqlCharsFromString(value, Typed.Vonalkodozas); }                                            
        }
                   
           
        /// <summary>
        /// DokumentumSzerep property </summary>
        public String DokumentumSzerep
        {
            get { return Utility.GetStringFromSqlString(Typed.DokumentumSzerep); }
            set { Typed.DokumentumSzerep = Utility.SetSqlStringFromString(value, Typed.DokumentumSzerep); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}