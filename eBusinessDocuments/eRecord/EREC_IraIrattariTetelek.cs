
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraIrattariTetelek BusinessDocument Class
    /// CheckConstraint_fn
    /// 
    /// Review: csak a t�bla neve van �t�rva a funkci�k miatt!
    /// </summary>
    [Serializable()]
    public class EREC_IraIrattariTetelek : BaseEREC_IraIrattariTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// IrattariTetelszam property
        /// Iratt�ri t�telsz�m
        /// </summary>
        public String IrattariTetelszam
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariTetelszam); }
            set { Typed.IrattariTetelszam = Utility.SetSqlStringFromString(value, Typed.IrattariTetelszam); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Iratt�ri t�tel megnevez�se
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// IrattariJel property
        /// Iratt�ri jel
        /// </summary>
        public String IrattariJel
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariJel); }
            set { Typed.IrattariJel = Utility.SetSqlStringFromString(value, Typed.IrattariJel); }                                            
        }
                   
           
        /// <summary>
        /// MegorzesiIdo property
        /// KCS: SELEJTEZESI_IDO
        /// 1 = 2
        /// 2 = 5
        /// 3 = 10
        /// 4 = 15
        /// 5 = 30
        /// 6 = 75
        /// 0 = nem selejtezhet�
        /// 
        /// </summary>
        public String MegorzesiIdo
        {
            get { return Utility.GetStringFromSqlString(Typed.MegorzesiIdo); }
            set { Typed.MegorzesiIdo = Utility.SetSqlStringFromString(value, Typed.MegorzesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Idoegyseg property
        /// //A meg�rz�si id� egys�ge: nap, �v, vagy szab�ly alapj�n sz�molt.
        /// //N - nap, E-�v,  S-szab�ly alapj�n sz�molt.
        /// A meg�rz�si id� egys�ge:IDOEGYSEG k�dt�rb�l.
        /// </summary>
        public String Idoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.Idoegyseg); }
            set { Typed.Idoegyseg = Utility.SetSqlStringFromString(value, Typed.Idoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// Folyo_CM property
        /// ??
        /// </summary>
        public String Folyo_CM
        {
            get { return Utility.GetStringFromSqlString(Typed.Folyo_CM); }
            set { Typed.Folyo_CM = Utility.SetSqlStringFromString(value, Typed.Folyo_CM); }                                            
        }
                   
           
        /// <summary>
        /// AgazatiJel_Id property
        /// �gazati jel Id
        /// </summary>
        public String AgazatiJel_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AgazatiJel_Id); }
            set { Typed.AgazatiJel_Id = Utility.SetSqlGuidFromString(value, Typed.AgazatiJel_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// EvenTuliIktatas property
        /// 
        /// </summary>
        public String EvenTuliIktatas
        {
            get { return Utility.GetStringFromSqlChars(Typed.EvenTuliIktatas); }
            set { Typed.EvenTuliIktatas = Utility.SetSqlCharsFromString(value, Typed.EvenTuliIktatas); }                                            
        }
                           }
   
}