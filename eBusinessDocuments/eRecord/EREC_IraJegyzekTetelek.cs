
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraJegyzekTetelek BusinessDocument Class
    /// CheckConstraint_fn
    /// 
    /// Review: ObjitTip 
    /// �gy, p�ld�ny, k�ldem�ny, (�s pl. titkos �gykezel�sn�l) dokumentumokat tartalmazhat.
    /// Iratot nem tartalmazhat, mivel ez �n�ll�an nem jelenik meg fizikai megval�sul�sk�nt.
    /// </summary>
    [Serializable()]
    public class EREC_IraJegyzekTetelek : BaseEREC_IraJegyzekTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Jegyzek_Id property
        /// Iratt�ri jegyz�k t�tel Id
        /// </summary>
        public String Jegyzek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Jegyzek_Id); }
            set { Typed.Jegyzek_Id = Utility.SetSqlGuidFromString(value, Typed.Jegyzek_Id); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property
        /// Review: decimal(18)
        /// </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property
        /// Ez annak a valaminek a GUID -ja ("allObjGuids"-b�l)  amit csatoltunk
        /// </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        public String Obj_type
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_type); }
            set { Typed.Obj_type = Utility.SetSqlStringFromString(value, Typed.Obj_type); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: ??
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma
        /// </summary>
        public String SztornozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornozasDat); }
            set { Typed.SztornozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornozasDat); }                                            
        }
                   
           
        /// <summary>
        /// AtadasDatuma property
        /// Sztorn�z�s d�tuma
        /// </summary>
        public String AtadasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtadasDatuma); }
            set { Typed.AtadasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.AtadasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}