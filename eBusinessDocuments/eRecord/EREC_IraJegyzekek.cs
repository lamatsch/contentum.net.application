
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraJegyzekek BusinessDocument Class
    /// Ez egy (pl. selejtez�sre haszn�lt) halmaz... (pl. hogy selejtezhet� �iratokat �sszev�logatok)
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_IraJegyzekek : BaseEREC_IraJegyzekek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// ??
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Minosites property
        /// nem haszn�lt
        /// </summary>
        public String Minosites
        {
            get { return Utility.GetStringFromSqlString(Typed.Minosites); }
            set { Typed.Minosites = Utility.SetSqlStringFromString(value, Typed.Minosites); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        public String Csoport_Id_felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_felelos); }
            set { Typed.Csoport_Id_felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_felelos); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj property
        /// V�grehajt� felhaszn�l�
        /// </summary>
        public String FelhasznaloCsoport_Id_Vegrehaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Vegrehaj); }
            set { Typed.FelhasznaloCsoport_Id_Vegrehaj = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Vegrehaj); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_LeveltariAtvevo property
        /// Lev�lt�ri �tvev�
        /// </summary>
        public String Partner_Id_LeveltariAtvevo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_LeveltariAtvevo); }
            set { Typed.Partner_Id_LeveltariAtvevo = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_LeveltariAtvevo); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDatuma property
        /// Lez�r�s d�tuma
        /// </summary>
        public String LezarasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDatuma); }
            set { Typed.LezarasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Iratt�ri jegyz�k megnevez�se
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// VegrehajtasDatuma property
        /// Leveltari �tadas id�pontja
        /// </summary>
        public String VegrehajtasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VegrehajtasDatuma); }
            set { Typed.VegrehajtasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VegrehajtasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Foszam property
        /// Iktat�si f�sz�m
        /// </summary>
        public String Foszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Foszam); }
            set { Typed.Foszam = Utility.SetSqlInt32FromString(value, Typed.Foszam); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// �gyirat azonos�t�: f�sz�m, �v 
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// MegsemmisitesDatuma property
        /// Leveltari �tadas id�pontja
        /// </summary>
        public String MegsemmisitesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegsemmisitesDatuma); }
            set { Typed.MegsemmisitesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.MegsemmisitesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_FelelosSzervezet property
        /// 
        /// </summary>
        public String Csoport_Id_FelelosSzervezet
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_FelelosSzervezet); }
            set { Typed.Csoport_Id_FelelosSzervezet = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_FelelosSzervezet); }                                            
        }
                   
           
        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        public String IraIktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIktatokonyv_Id); }
            set { Typed.IraIktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.IraIktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: JEGYZEK_ALLAPOT
        /// 
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// VegrehajtasKezdoDatuma property
        /// Aszinkron v�grehajt�s eset�n a v�grehajt�s kezdet�nek d�tuma.
        /// </summary>
        public String VegrehajtasKezdoDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VegrehajtasKezdoDatuma); }
            set { Typed.VegrehajtasKezdoDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VegrehajtasKezdoDatuma); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}