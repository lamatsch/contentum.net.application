
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezFeljegyzesek BusinessDocument Class
    /// Irathoz tartoz� kezel�si feljegyz�sek 
    ///</summary>
    [Serializable()]
    public class EREC_IraKezFeljegyzesek : BaseEREC_IraKezFeljegyzesek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property 
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property 
        /// Irat Id
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// KezelesTipus property 
        /// KCS: KEZELESI_FELJEGYZESEK_TIPUSA
        /// 01 - Int�zked�sre
        /// 02 - V�lem�nyez�sre
        /// 03 - T�j�koztat�sra
        /// 04 - Megbesz�l�sre
        /// 05 - J�v�hagy�sra
        /// 06 - Al��r�sra
        /// 09 - Megjegyz�s
        /// 10 - T�rgyal�si helysz�n �s ido
        /// 11 - Iratt�roz�sra
        /// 12 - Iktat�sra
        /// 13 - Eload�i munkanapl� megjegyz�s
        /// T - T�rgyal�si helysz�n
        /// </summary>
        public String KezelesTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.KezelesTipus); }
            set { Typed.KezelesTipus = Utility.SetSqlStringFromString(value, Typed.KezelesTipus); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property 
        /// Le�r�s
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Note property 
        /// jegyzet (400 hossz�, h indexelni lehessen, ha valaki akarja.)
        /// </summary>
        public String Note
        {
            get { return Utility.GetStringFromSqlString(Typed.Note); }
            set { Typed.Note = Utility.SetSqlStringFromString(value, Typed.Note); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property 
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property 
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}