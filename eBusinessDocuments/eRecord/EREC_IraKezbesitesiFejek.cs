
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezbesitesiFejek BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IraKezbesitesiFejek : BaseEREC_IraKezbesitesiFejek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// CsomagAzonosito property </summary>
        public String CsomagAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.CsomagAzonosito); }
            set { Typed.CsomagAzonosito = Utility.SetSqlStringFromString(value, Typed.CsomagAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoNyomtatasIdo property </summary>
        public String UtolsoNyomtatasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoNyomtatasIdo); }
            set { Typed.UtolsoNyomtatasIdo = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoNyomtatasIdo); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoNyomtatas_Id property </summary>
        public String UtolsoNyomtatas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UtolsoNyomtatas_Id); }
            set { Typed.UtolsoNyomtatas_Id = Utility.SetSqlGuidFromString(value, Typed.UtolsoNyomtatas_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}