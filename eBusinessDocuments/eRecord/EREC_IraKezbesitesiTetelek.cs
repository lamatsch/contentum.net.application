
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraKezbesitesiTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IraKezbesitesiTetelek : BaseEREC_IraKezbesitesiTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KezbesitesFej_Id property </summary>
        public String KezbesitesFej_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KezbesitesFej_Id); }
            set { Typed.KezbesitesFej_Id = Utility.SetSqlGuidFromString(value, Typed.KezbesitesFej_Id); }                                            
        }
                   
           
        /// <summary>
        /// AtveteliFej_Id property </summary>
        public String AtveteliFej_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AtveteliFej_Id); }
            set { Typed.AtveteliFej_Id = Utility.SetSqlGuidFromString(value, Typed.AtveteliFej_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_type property </summary>
        public String Obj_type
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_type); }
            set { Typed.Obj_type = Utility.SetSqlStringFromString(value, Typed.Obj_type); }                                            
        }
                   
           
        /// <summary>
        /// AtadasDat property </summary>
        public String AtadasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtadasDat); }
            set { Typed.AtadasDat = Utility.SetSqlDateTimeFromString(value, Typed.AtadasDat); }                                            
        }
                   
           
        /// <summary>
        /// AtvetelDat property </summary>
        public String AtvetelDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtvetelDat); }
            set { Typed.AtvetelDat = Utility.SetSqlDateTimeFromString(value, Typed.AtvetelDat); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoNyomtatas_Id property </summary>
        public String UtolsoNyomtatas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UtolsoNyomtatas_Id); }
            set { Typed.UtolsoNyomtatas_Id = Utility.SetSqlGuidFromString(value, Typed.UtolsoNyomtatas_Id); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoNyomtatasIdo property </summary>
        public String UtolsoNyomtatasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UtolsoNyomtatasIdo); }
            set { Typed.UtolsoNyomtatasIdo = Utility.SetSqlDateTimeFromString(value, Typed.UtolsoNyomtatasIdo); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_Atado_USER property </summary>
        public String Felhasznalo_Id_Atado_USER
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_Atado_USER); }
            set { Typed.Felhasznalo_Id_Atado_USER = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_Atado_USER); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_Atado_LOGIN property </summary>
        public String Felhasznalo_Id_Atado_LOGIN
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_Atado_LOGIN); }
            set { Typed.Felhasznalo_Id_Atado_LOGIN = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_Atado_LOGIN); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Cel property </summary>
        public String Csoport_Id_Cel
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Cel); }
            set { Typed.Csoport_Id_Cel = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Cel); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_AtvevoUser property </summary>
        public String Felhasznalo_Id_AtvevoUser
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_AtvevoUser); }
            set { Typed.Felhasznalo_Id_AtvevoUser = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_AtvevoUser); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id_AtvevoLogin property </summary>
        public String Felhasznalo_Id_AtvevoLogin
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id_AtvevoLogin); }
            set { Typed.Felhasznalo_Id_AtvevoLogin = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id_AtvevoLogin); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlChars(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlCharsFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito_szoveges property </summary>
        public String Azonosito_szoveges
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito_szoveges); }
            set { Typed.Azonosito_szoveges = Utility.SetSqlStringFromString(value, Typed.Azonosito_szoveges); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesModja property </summary>
        public String UgyintezesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesModja); }
            set { Typed.UgyintezesModja = Utility.SetSqlStringFromString(value, Typed.UgyintezesModja); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}