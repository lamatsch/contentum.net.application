 
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IraOnkormAdatok BusinessDocument Class
    /// Az IRAT-tal, vagy a �gyirattal 1:1 fok� viszonyban �ll� egyed, amely az �nkorm�nyzati statisztik�k alapj�t k�pez� iratadatok nyilv�ntart�s�ra szolg�l. Racion�lis elemz�ssel arra a k�vetkeztet�sre kellene jutni, hogy ezek az adatok val�j�ban ink�bb az UGYIRAT (esetleg az UGYIRATDARAB) jellemz�i, semmint az IRAT-�. - Azonban a marcali-i gyakorklat szerint iratonk�nt r�gz�tenek ilyen jellmz�ket, m�ghozz� esetleg egy �gyiratban k�l�nb�z� iratoknak k�l�nb�z� �rt�kkel.
    /// </summary>
    [Serializable()]
    public class EREC_IraOnkormAdatok : BaseEREC_IraOnkormAdatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�, azonos az �gyirat Id-vel
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIratok_Id property
        /// 
        /// </summary>
        public String IraIratok_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIratok_Id); }
            set { Typed.IraIratok_Id = Utility.SetSqlGuidFromString(value, Typed.IraIratok_Id); }                                            
        }
                   
           
        /// <summary>
        /// UgyFajtaja property
        /// 2008.03.31 KCS: kcs.UGY_FAJTAJA
        /// 
        /// </summary>
        public String UgyFajtaja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyFajtaja); }
            set { Typed.UgyFajtaja = Utility.SetSqlStringFromString(value, Typed.UgyFajtaja); }                                            
        }
                   
           
        /// <summary>
        /// DontestHozta property
        /// 2. A d�nt�st hozta -> KCS: DONTEST_HOZTA
        /// k�zgy�l�s test�lete (1)
        /// k�zgy�l�s bizotts�ga (2)
        /// a r�sz�nkorm�nyzat test�lete (3)
        /// a (f�)polg�rmester (4)
        /// a (f�)jegyz� (5)
        /// </summary>
        public String DontestHozta
        {
            get { return Utility.GetStringFromSqlString(Typed.DontestHozta); }
            set { Typed.DontestHozta = Utility.SetSqlStringFromString(value, Typed.DontestHozta); }                                            
        }
                   
           
        /// <summary>
        /// DontesFormaja property
        /// 3. A d�nt�s form�ja -> KCS: DONTES_FORMAJA
        /// hat�rozat (1)
        /// egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat (2)
        /// hat�s�gi szerz�d�s (3)
        /// a Ket. 30. � alapj�n t�rt�n� elutas�t�s (4)
        /// a Ket. 31. � alapj�n t�rt�n� megsz�ntet�se (5)
        /// az els�fok� elj�r�sban hozott egy�b v�gz�s (6)
        /// 
        /// </summary>
        public String DontesFormaja
        {
            get { return Utility.GetStringFromSqlString(Typed.DontesFormaja); }
            set { Typed.DontesFormaja = Utility.SetSqlStringFromString(value, Typed.DontesFormaja); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesHataridore property
        /// 4. Az �gyint�z�s id�tartama -> KCS: UGYINTEZES_IDOTARTAMA
        /// hat�rid�n bel�l (1)
        /// hat�rid�n t�l (2)
        /// </summary>
        public String UgyintezesHataridore
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesHataridore); }
            set { Typed.UgyintezesHataridore = Utility.SetSqlStringFromString(value, Typed.UgyintezesHataridore); }                                            
        }
                   
           
        /// <summary>
        /// HataridoTullepes property
        /// Az �rdemi d�nt�ssel kapcsolatos esetleges hat�rid�-t�ll�p�s id�tartlma napokban (0-999)
        /// </summary>
        public String HataridoTullepes
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HataridoTullepes, String.Empty); }
            set { Typed.HataridoTullepes = Utility.SetSqlInt32FromString(value, Typed.HataridoTullepes); }                                            
        }
                   
           
        /// <summary>
        /// JogorvoslatiEljarasTipusa property
        /// 5. A jogorvoslati elj�r�s t�pusa -> KCS: JOGORVOSLATI_ELJARAS_TIPUSA
        /// k�relem alapj�n indult (1)
        /// hivatalb�l indult (2)
        /// </summary>
        public String JogorvoslatiEljarasTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.JogorvoslatiEljarasTipusa); }
            set { Typed.JogorvoslatiEljarasTipusa = Utility.SetSqlStringFromString(value, Typed.JogorvoslatiEljarasTipusa); }                                            
        }
                   
           
        /// <summary>
        /// JogorvoslatiDontesTipusa property
        /// 6. A jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa -> 
        /// KCS: JOGORVOSLATI_DONTES_TIPUSA
        /// v�gz�s (1)
        /// �rdemi d�nt�s (2)
        /// </summary>
        public String JogorvoslatiDontesTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.JogorvoslatiDontesTipusa); }
            set { Typed.JogorvoslatiDontesTipusa = Utility.SetSqlStringFromString(value, Typed.JogorvoslatiDontesTipusa); }                                            
        }
                   
           
        /// <summary>
        /// JogorvoslatiDontestHozta property
        /// 7. A jogorvoslati elj�r�sban sz�letett d�nt�st meghozta -> KCS: JOGORVOSLATI_DONTEST_HOZTA
        /// az els�fok� hat�s�g (1)
        /// az els�fok� hat�s�g �jrafelv�teli elj�r�sban (2)
        /// az els�fok� hat�s�g m�lt�nyoss�gi elj�r�sban (3)
        /// a k�pvisel�test�let m�sodfok� hat�sk�rben (4)
        /// a k�zigazgat�si hivatal (5)
        /// a dekoncentr�lt szerv (6)
        /// a b�r�s�g (7)
        /// a fel�gyeleti szerv/fel�gyeleti int�zked�s eset�n (8)
        /// </summary>
        public String JogorvoslatiDontestHozta
        {
            get { return Utility.GetStringFromSqlString(Typed.JogorvoslatiDontestHozta); }
            set { Typed.JogorvoslatiDontestHozta = Utility.SetSqlStringFromString(value, Typed.JogorvoslatiDontestHozta); }                                            
        }
                   
           
        /// <summary>
        /// JogorvoslatiDontesTartalma property
        /// 8. A jogorvoslati elj�r�sban sz�letett d�nt�st tartalma -> KCS: JOGORVOSLATI_DONTES_TARTALMA
        /// kicser�l�s, kieg�sz�t�s vagy kijav�t�s (1)
        /// m�dos�t�s (2)
        /// visszavon�s (3)
        /// �j d�nt�s (4)
        /// helybenhagy�s/kereset ill. k�relem, �jrafelv�teli vagy m�lt�nyoss�gi k�relem elutas�t�sa (5)
        /// megv�ltoztat�s (6)
        /// megsemmis�t�s vagy hat�lyon k�v�l helyez�s (7)
        /// megsemmis�t�s vagy hat�lyon k�v�l helyez�s �s �j elj�r�sra utas�t�s (8)
        /// </summary>
        public String JogorvoslatiDontesTartalma
        {
            get { return Utility.GetStringFromSqlString(Typed.JogorvoslatiDontesTartalma); }
            set { Typed.JogorvoslatiDontesTartalma = Utility.SetSqlStringFromString(value, Typed.JogorvoslatiDontesTartalma); }                                            
        }
                   
           
        /// <summary>
        /// JogorvoslatiDontes property
        /// A d�nt�s: 
        /// KCS: JOGORVOSLATI_DONTES
        /// 1, Kijav�t�sra ker�lt sor
        /// 2, Kieg�sz�t�sre ker�lt sor
        /// </summary>
        public String JogorvoslatiDontes
        {
            get { return Utility.GetStringFromSqlString(Typed.JogorvoslatiDontes); }
            set { Typed.JogorvoslatiDontes = Utility.SetSqlStringFromString(value, Typed.JogorvoslatiDontes); }                                            
        }
                   
           
        /// <summary>
        /// HatosagiEllenorzes property
        /// �rt�k: 0/1
        /// 
        /// K�rj�k felt�ntetni az adott elj�r�st�pus sor�n a t�rgyid�szakban lefolytatott hat�s�gi ellen�rz�sek sz�m�t.
        /// </summary>
        public String HatosagiEllenorzes
        {
            get { return Utility.GetStringFromSqlChars(Typed.HatosagiEllenorzes); }
            set { Typed.HatosagiEllenorzes = Utility.SetSqlCharsFromString(value, Typed.HatosagiEllenorzes); }                                            
        }
                   
           
        /// <summary>
        /// MunkaorakSzama property
        /// �rt�k: 3 eg�sz + 1 tizedes
        /// 
        /// Az adott �gazatban az egy �gy elint�z�s�re ford�tott munka�r�k �tlagos sz�m�t kell felt�ntetni (�sszes munka�ra / elj�r�sok sz�ma).
        /// </summary>
        public String MunkaorakSzama
        {
            get { return Utility.GetStringFromSqlDouble(Typed.MunkaorakSzama); }
            set { Typed.MunkaorakSzama = Utility.SetSqlDoubleFromString(value, Typed.MunkaorakSzama); }                                            
        }
                   
           
        /// <summary>
        /// EljarasiKoltseg property
        /// �rt�k: 6 jegy� eg�sz sz�m.
        /// 
        /// Az adott �gazatban az egy �gyre jut� �tlagos elj�r�si k�lts�get k�rj�k felt�ntetni (�sszes elj�r�si k�lts�g / elj�r�sok sz�ma).
        /// Elj�r�si k�lts�gen ebben a tekintetben a hat�s�g �ltal meg�llap�tott, a Ket. vagy m�s vonatkoz� jogszab�ly szerinti elj�r�si k�lts�get kell �rteni. A hat�s�g m�k�d�si k�lts�gei figyelmen k�v�l hagyand�ak.
        /// </summary>
        public String EljarasiKoltseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.EljarasiKoltseg, String.Empty); }
            set { Typed.EljarasiKoltseg = Utility.SetSqlInt32FromString(value, Typed.EljarasiKoltseg); }                                            
        }
                   
           
        /// <summary>
        /// KozigazgatasiBirsagMerteke property
        /// 
        /// </summary>
        public String KozigazgatasiBirsagMerteke
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KozigazgatasiBirsagMerteke, String.Empty); }
            set { Typed.KozigazgatasiBirsagMerteke = Utility.SetSqlInt32FromString(value, Typed.KozigazgatasiBirsagMerteke); }                                            
        }
                   
           
        /// <summary>
        /// SommasEljDontes property
        /// 
        /// </summary>
        public String SommasEljDontes
        {
            get { return Utility.GetStringFromSqlString(Typed.SommasEljDontes); }
            set { Typed.SommasEljDontes = Utility.SetSqlStringFromString(value, Typed.SommasEljDontes); }                                            
        }
                   
           
        /// <summary>
        /// NyolcNapBelulNemSommas property
        /// 
        /// </summary>
        public String NyolcNapBelulNemSommas
        {
            get { return Utility.GetStringFromSqlString(Typed.NyolcNapBelulNemSommas); }
            set { Typed.NyolcNapBelulNemSommas = Utility.SetSqlStringFromString(value, Typed.NyolcNapBelulNemSommas); }                                            
        }
                   
           
        /// <summary>
        /// FuggoHatalyuHatarozat property
        /// 
        /// </summary>
        public String FuggoHatalyuHatarozat
        {
            get { return Utility.GetStringFromSqlString(Typed.FuggoHatalyuHatarozat); }
            set { Typed.FuggoHatalyuHatarozat = Utility.SetSqlStringFromString(value, Typed.FuggoHatalyuHatarozat); }                                            
        }
                   
           
        /// <summary>
        /// FuggoHatHatalybaLepes property
        /// 
        /// </summary>
        public String FuggoHatHatalybaLepes
        {
            get { return Utility.GetStringFromSqlString(Typed.FuggoHatHatalybaLepes); }
            set { Typed.FuggoHatHatalybaLepes = Utility.SetSqlStringFromString(value, Typed.FuggoHatHatalybaLepes); }                                            
        }
                   
           
        /// <summary>
        /// FuggoHatalyuVegzes property
        /// 
        /// </summary>
        public String FuggoHatalyuVegzes
        {
            get { return Utility.GetStringFromSqlString(Typed.FuggoHatalyuVegzes); }
            set { Typed.FuggoHatalyuVegzes = Utility.SetSqlStringFromString(value, Typed.FuggoHatalyuVegzes); }                                            
        }
                   
           
        /// <summary>
        /// FuggoVegzesHatalyba property
        /// 
        /// </summary>
        public String FuggoVegzesHatalyba
        {
            get { return Utility.GetStringFromSqlString(Typed.FuggoVegzesHatalyba); }
            set { Typed.FuggoVegzesHatalyba = Utility.SetSqlStringFromString(value, Typed.FuggoVegzesHatalyba); }                                            
        }
                   
           
        /// <summary>
        /// HatAltalVisszafizOsszeg property
        /// 
        /// </summary>
        public String HatAltalVisszafizOsszeg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatAltalVisszafizOsszeg); }
            set { Typed.HatAltalVisszafizOsszeg = Utility.SetSqlInt32FromString(value, Typed.HatAltalVisszafizOsszeg); }                                            
        }
                   
           
        /// <summary>
        /// HatTerheloEljKtsg property
        /// 
        /// </summary>
        public String HatTerheloEljKtsg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatTerheloEljKtsg); }
            set { Typed.HatTerheloEljKtsg = Utility.SetSqlInt32FromString(value, Typed.HatTerheloEljKtsg); }                                            
        }
                   
           
        /// <summary>
        /// FelfuggHatarozat property
        /// 
        /// </summary>
        public String FelfuggHatarozat
        {
            get { return Utility.GetStringFromSqlString(Typed.FelfuggHatarozat); }
            set { Typed.FelfuggHatarozat = Utility.SetSqlStringFromString(value, Typed.FelfuggHatarozat); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}