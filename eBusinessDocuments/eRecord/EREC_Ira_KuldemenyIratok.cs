
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Ira_KuldemenyIratok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Ira_KuldemenyIratok : BaseEREC_Ira_KuldemenyIratok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Kuldemeny_Id property </summary>
        public String Kuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kuldemeny_Id); }
            set { Typed.Kuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.Kuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Irat_Id property </summary>
        public String Irat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Irat_Id); }
            set { Typed.Irat_Id = Utility.SetSqlGuidFromString(value, Typed.Irat_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}