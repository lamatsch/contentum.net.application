
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratAlairok BusinessDocument Class
    /// CheckConstraint_fn
    /// 
    /// /*KIR2.5*/ Kiadm�nyoz�sban j�v�hagy�k (al��r�k) nyilv�ntart�sa. v0.2 v�ltoz�s: KIRL_IRA_JOVAHAGYOK-r�l 
    /// Javasolt egy al��r� csoport l�trehoz�sa (pecs�ttel, v. digit�lis kulccsal), ebb�l kell szelekt�lni az al��r�kat/szign�l�kat. 
    /// </summary>
    [Serializable()]
    public class EREC_IratAlairok : BaseEREC_IratAlairok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// PldIratPeldany_Id property
        /// Al��r�i p�ld�ny azonos�t�ja (ha van).
        /// </summary>
        public String PldIratPeldany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.PldIratPeldany_Id); }
            set { Typed.PldIratPeldany_Id = Utility.SetSqlGuidFromString(value, Typed.PldIratPeldany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Objtip_Id property
        /// 
        /// </summary>
        public String Objtip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Objtip_Id); }
            set { Typed.Objtip_Id = Utility.SetSqlGuidFromString(value, Typed.Objtip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property
        /// 
        /// </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// AlairasDatuma property
        /// Al��r�s d�tuma
        /// </summary>
        public String AlairasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AlairasDatuma); }
            set { Typed.AlairasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.AlairasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// /*KIR2.5*/ v0.2 �j attrib�tum. Le�r�s, egy�b jellemz�.
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// AlairoSzerep property
        /// KCS: ALAIRO_SZEREP 
        /// 1- Kiadm�nyoz�s
        /// 2 - J�v�hagy�s
        /// 3 - L�ttamoz�s
        /// 
        /// </summary>
        public String AlairoSzerep
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairoSzerep); }
            set { Typed.AlairoSzerep = Utility.SetSqlStringFromString(value, Typed.AlairoSzerep); }                                            
        }
                   
           
        /// <summary>
        /// AlairasSorrend property
        /// Al��r�si sorrend szerinti csoportos�t�s (1, 2, ...n). Az 'n' az utols�.
        /// </summary>
        public String AlairasSorrend
        {
            get { return Utility.GetStringFromSqlInt32(Typed.AlairasSorrend); }
            set { Typed.AlairasSorrend = Utility.SetSqlInt32FromString(value, Typed.AlairasSorrend); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo property
        /// Az al��r� azonos�t�ja.
        /// </summary>
        public String FelhasznaloCsoport_Id_Alairo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Alairo); }
            set { Typed.FelhasznaloCsoport_Id_Alairo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Alairo); }                                            
        }
                   
           
        /// <summary>
        /// FelhaszCsoport_Id_Helyettesito property
        /// A helyettes al��r� azonos�t�ja.
        /// </summary>
        public String FelhaszCsoport_Id_Helyettesito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhaszCsoport_Id_Helyettesito); }
            set { Typed.FelhaszCsoport_Id_Helyettesito = Utility.SetSqlGuidFromString(value, Typed.FelhaszCsoport_Id_Helyettesito); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// AlairasMod property
        /// Az �l��r�s p�tl�lagos jelleg�t jelz� adat.
        /// 0 - Norm�l
        /// 1 - P�tl�s
        /// </summary>
        public String AlairasMod
        {
            get { return Utility.GetStringFromSqlString(Typed.AlairasMod); }
            set { Typed.AlairasMod = Utility.SetSqlStringFromString(value, Typed.AlairasMod); }                                            
        }
                   
           
        /// <summary>
        /// AlairasSzabaly_Id property
        /// 
        /// </summary>
        public String AlairasSzabaly_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.AlairasSzabaly_Id); }
            set { Typed.AlairasSzabaly_Id = Utility.SetSqlGuidFromString(value, Typed.AlairasSzabaly_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// Az adott felhaszn�l�i al��r�s �llapota. K�dcsoport: IRATALAIRAS_ALLAPOT
        /// 1 - Al��rand�
        /// 2 - Al��rt (minden al��rand� dokumentum al��r�sa megt�rt�nt)
        /// 3 - Visszautas�tott (az irat al��r�sa elutas�tva )
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// FelhaszCsop_Id_HelyettesAlairo property
        /// Helyettes al��r�.
        /// </summary>
        public String FelhaszCsop_Id_HelyettesAlairo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhaszCsop_Id_HelyettesAlairo); }
            set { Typed.FelhaszCsop_Id_HelyettesAlairo = Utility.SetSqlGuidFromString(value, Typed.FelhaszCsop_Id_HelyettesAlairo); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}