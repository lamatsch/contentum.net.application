
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratMellekletek BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IratMellekletek : BaseEREC_IratMellekletek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldMellekletek_Id property </summary>
        public String KuldMellekletek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldMellekletek_Id); }
            set { Typed.KuldMellekletek_Id = Utility.SetSqlGuidFromString(value, Typed.KuldMellekletek_Id); }                                            
        }
                   
           
        /// <summary>
        /// AdathordozoTipus property </summary>
        public String AdathordozoTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.AdathordozoTipus); }
            set { Typed.AdathordozoTipus = Utility.SetSqlStringFromString(value, Typed.AdathordozoTipus); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// MennyisegiEgyseg property </summary>
        public String MennyisegiEgyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.MennyisegiEgyseg); }
            set { Typed.MennyisegiEgyseg = Utility.SetSqlStringFromString(value, Typed.MennyisegiEgyseg); }                                            
        }
                   
           
        /// <summary>
        /// Mennyiseg property </summary>
        public String Mennyiseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Mennyiseg); }
            set { Typed.Mennyiseg = Utility.SetSqlInt32FromString(value, Typed.Mennyiseg); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}