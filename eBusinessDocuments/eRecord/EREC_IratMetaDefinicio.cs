
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratMetaDefinicio BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IratMetaDefinicio : BaseEREC_IratMetaDefinicio
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property.. </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Ugykor_Id property.. </summary>
        public String Ugykor_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Ugykor_Id); }
            set { Typed.Ugykor_Id = Utility.SetSqlGuidFromString(value, Typed.Ugykor_Id); }                                            
        }
                   
           
        /// <summary>
        /// UgykorKod property </summary>
        public String UgykorKod
        {
            get { return Utility.GetStringFromSqlString(Typed.UgykorKod); }
            set { Typed.UgykorKod = Utility.SetSqlStringFromString(value, Typed.UgykorKod); }                                            
        }
                   
           
        /// <summary>
        /// Ugytipus property </summary>
        public String Ugytipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Ugytipus); }
            set { Typed.Ugytipus = Utility.SetSqlStringFromString(value, Typed.Ugytipus); }                                            
        }
                   
           
        /// <summary>
        /// UgytipusNev property </summary>
        public String UgytipusNev
        {
            get { return Utility.GetStringFromSqlString(Typed.UgytipusNev); }
            set { Typed.UgytipusNev = Utility.SetSqlStringFromString(value, Typed.UgytipusNev); }                                            
        }
                   
           
        /// <summary>
        /// EljarasiSzakasz property </summary>
        public String EljarasiSzakasz
        {
            get { return Utility.GetStringFromSqlString(Typed.EljarasiSzakasz); }
            set { Typed.EljarasiSzakasz = Utility.SetSqlStringFromString(value, Typed.EljarasiSzakasz); }                                            
        }
                   
           
        /// <summary>
        /// Irattipus property </summary>
        public String Irattipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Irattipus); }
            set { Typed.Irattipus = Utility.SetSqlStringFromString(value, Typed.Irattipus); }                                            
        }
                   
           
        /// <summary>
        /// GeneraltTargy property </summary>
        public String GeneraltTargy
        {
            get { return Utility.GetStringFromSqlString(Typed.GeneraltTargy); }
            set { Typed.GeneraltTargy = Utility.SetSqlStringFromString(value, Typed.GeneraltTargy); }                                            
        }
                   
           
        /// <summary>
        /// Rovidnev property </summary>
        public String Rovidnev
        {
            get { return Utility.GetStringFromSqlString(Typed.Rovidnev); }
            set { Typed.Rovidnev = Utility.SetSqlStringFromString(value, Typed.Rovidnev); }                                            
        }
                   
           
        /// <summary>
        /// UgyFajta property </summary>
        public String UgyFajta
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyFajta); }
            set { Typed.UgyFajta = Utility.SetSqlStringFromString(value, Typed.UgyFajta); }                                            
        }
                   
           
        /// <summary>
        /// UgyiratIntezesiIdo property </summary>
        public String UgyiratIntezesiIdo
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UgyiratIntezesiIdo); }
            set { Typed.UgyiratIntezesiIdo = Utility.SetSqlInt32FromString(value, Typed.UgyiratIntezesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// Idoegyseg property </summary>
        public String Idoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.Idoegyseg); }
            set { Typed.Idoegyseg = Utility.SetSqlStringFromString(value, Typed.Idoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// UgyiratIntezesiIdoKotott property </summary>
        public String UgyiratIntezesiIdoKotott
        {
            get { return Utility.GetStringFromSqlChars(Typed.UgyiratIntezesiIdoKotott); }
            set { Typed.UgyiratIntezesiIdoKotott = Utility.SetSqlCharsFromString(value, Typed.UgyiratIntezesiIdoKotott); }                                            
        }
                   
           
        /// <summary>
        /// UgyiratHataridoKitolas property </summary>
        public String UgyiratHataridoKitolas
        {
            get { return Utility.GetStringFromSqlChars(Typed.UgyiratHataridoKitolas); }
            set { Typed.UgyiratHataridoKitolas = Utility.SetSqlCharsFromString(value, Typed.UgyiratHataridoKitolas); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}