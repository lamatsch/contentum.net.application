
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Irat_Iktatokonyvei BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Irat_Iktatokonyvei : BaseEREC_Irat_Iktatokonyvei
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIktatokonyv_Id property </summary>
        public String IraIktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIktatokonyv_Id); }
            set { Typed.IraIktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.IraIktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Iktathat property </summary>
        public String Csoport_Id_Iktathat
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Iktathat); }
            set { Typed.Csoport_Id_Iktathat = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Iktathat); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}