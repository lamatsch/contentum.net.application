
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IratelemKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IratelemKapcsolatok : BaseEREC_IratelemKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Melleklet_Id property </summary>
        public String Melleklet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Melleklet_Id); }
            set { Typed.Melleklet_Id = Utility.SetSqlGuidFromString(value, Typed.Melleklet_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csatolmany_Id property </summary>
        public String Csatolmany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csatolmany_Id); }
            set { Typed.Csatolmany_Id = Utility.SetSqlGuidFromString(value, Typed.Csatolmany_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}