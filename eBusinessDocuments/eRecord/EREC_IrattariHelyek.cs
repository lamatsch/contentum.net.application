
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IrattariHelyek BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_IrattariHelyek : BaseEREC_IrattariHelyek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Ertek property
        /// 
        /// </summary>
        public String Ertek
        {
            get { return Utility.GetStringFromSqlString(Typed.Ertek); }
            set { Typed.Ertek = Utility.SetSqlStringFromString(value, Typed.Ertek); }                                            
        }

        ////// CR3246 Irattári Helyek kezelésének módosítása
        ///// <summary>
        ///// IrattarTipus property
        ///// 
        ///// </summary>
        //public String IrattarTipus
        //{
        //    get { return Utility.GetStringFromSqlString(Typed.IrattarTipus); }
        //    set { Typed.IrattarTipus = Utility.SetSqlStringFromString(value, Typed.IrattarTipus);}
        //}

        // CR3246 Irattári Helyek kezelésének módosítása
        /// <summary>
        /// Felelos_Csoport_Id property
        /// 
        /// </summary>
        public String Felelos_Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felelos_Csoport_Id); }
            set { Typed.Felelos_Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Felelos_Csoport_Id); }
        }

        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Vonalkod property
        /// 
        /// </summary>
        public String Vonalkod
        {
            get { return Utility.GetStringFromSqlString(Typed.Vonalkod); }
            set { Typed.Vonalkod = Utility.SetSqlStringFromString(value, Typed.Vonalkod); }                                            
        }
                   
           
        /// <summary>
        /// SzuloId property
        /// 
        /// </summary>
        public String SzuloId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SzuloId); }
            set { Typed.SzuloId = Utility.SetSqlGuidFromString(value, Typed.SzuloId); }                                            
        }

        /// <summary>
        /// Kapacitas property
        /// 
        /// </summary>
        public String Kapacitas
        {
            get { return Utility.GetStringFromSqlDouble(Typed.Kapacitas); }
            set { Typed.Kapacitas = Utility.SetSqlDoubleFromString(value, Typed.Kapacitas); }
        }

        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}