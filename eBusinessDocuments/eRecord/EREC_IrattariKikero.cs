
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IrattariKikero BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IrattariKikero : BaseEREC_IrattariKikero
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Keres_note property </summary>
        public String Keres_note
        {
            get { return Utility.GetStringFromSqlString(Typed.Keres_note); }
            set { Typed.Keres_note = Utility.SetSqlStringFromString(value, Typed.Keres_note); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznalasiCel property </summary>
        public String FelhasznalasiCel
        {
            get { return Utility.GetStringFromSqlChars(Typed.FelhasznalasiCel); }
            set { Typed.FelhasznalasiCel = Utility.SetSqlCharsFromString(value, Typed.FelhasznalasiCel); }                                            
        }
                   
           
        /// <summary>
        /// DokumentumTipus property </summary>
        public String DokumentumTipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.DokumentumTipus); }
            set { Typed.DokumentumTipus = Utility.SetSqlCharsFromString(value, Typed.DokumentumTipus); }                                            
        }
                   
           
        /// <summary>
        /// Indoklas_note property </summary>
        public String Indoklas_note
        {
            get { return Utility.GetStringFromSqlString(Typed.Indoklas_note); }
            set { Typed.Indoklas_note = Utility.SetSqlStringFromString(value, Typed.Indoklas_note); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kikero property </summary>
        public String FelhasznaloCsoport_Id_Kikero
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Kikero); }
            set { Typed.FelhasznaloCsoport_Id_Kikero = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Kikero); }                                            
        }
                   
           
        /// <summary>
        /// KeresDatuma property </summary>
        public String KeresDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KeresDatuma); }
            set { Typed.KeresDatuma = Utility.SetSqlDateTimeFromString(value, Typed.KeresDatuma); }                                            
        }
                   
           
        /// <summary>
        /// KulsoAzonositok property </summary>
        public String KulsoAzonositok
        {
            get { return Utility.GetStringFromSqlString(Typed.KulsoAzonositok); }
            set { Typed.KulsoAzonositok = Utility.SetSqlStringFromString(value, Typed.KulsoAzonositok); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyirat_Id property </summary>
        public String UgyUgyirat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id); }
            set { Typed.UgyUgyirat_Id = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Tertiveveny_Id property </summary>
        public String Tertiveveny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tertiveveny_Id); }
            set { Typed.Tertiveveny_Id = Utility.SetSqlGuidFromString(value, Typed.Tertiveveny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// KikerKezd property </summary>
        public String KikerKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KikerKezd); }
            set { Typed.KikerKezd = Utility.SetSqlDateTimeFromString(value, Typed.KikerKezd); }                                            
        }
                   
           
        /// <summary>
        /// KikerVege property </summary>
        public String KikerVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KikerVege); }
            set { Typed.KikerVege = Utility.SetSqlDateTimeFromString(value, Typed.KikerVege); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Jovahagy property </summary>
        public String FelhasznaloCsoport_Id_Jovahagy
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Jovahagy); }
            set { Typed.FelhasznaloCsoport_Id_Jovahagy = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Jovahagy); }                                            
        }
                   
           
        /// <summary>
        /// JovagyasDatuma property </summary>
        public String JovagyasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.JovagyasDatuma); }
            set { Typed.JovagyasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.JovagyasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kiado property </summary>
        public String FelhasznaloCsoport_Id_Kiado
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Kiado); }
            set { Typed.FelhasznaloCsoport_Id_Kiado = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Kiado); }                                            
        }
                   
           
        /// <summary>
        /// KiadasDatuma property </summary>
        public String KiadasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KiadasDatuma); }
            set { Typed.KiadasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.KiadasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Visszaad property </summary>
        public String FelhasznaloCsoport_Id_Visszaad
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Visszaad); }
            set { Typed.FelhasznaloCsoport_Id_Visszaad = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Visszaad); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Visszave property </summary>
        public String FelhasznaloCsoport_Id_Visszave
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Visszave); }
            set { Typed.FelhasznaloCsoport_Id_Visszave = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Visszave); }                                            
        }
                   
           
        /// <summary>
        /// VisszaadasDatuma property </summary>
        public String VisszaadasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszaadasDatuma); }
            set { Typed.VisszaadasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VisszaadasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// Irattar_Id property </summary>
        public String Irattar_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Irattar_Id); }
            set { Typed.Irattar_Id = Utility.SetSqlGuidFromString(value, Typed.Irattar_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}