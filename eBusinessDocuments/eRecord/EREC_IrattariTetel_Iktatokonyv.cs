
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_IrattariTetel_Iktatokonyv BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IrattariTetel_Iktatokonyv : BaseEREC_IrattariTetel_Iktatokonyv
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// IrattariTetel_Id property </summary>
        public String IrattariTetel_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IrattariTetel_Id); }
            set { Typed.IrattariTetel_Id = Utility.SetSqlGuidFromString(value, Typed.IrattariTetel_Id); }                                            
        }
                   
           
        /// <summary>
        /// Iktatokonyv_Id property </summary>
        public String Iktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Iktatokonyv_Id); }
            set { Typed.Iktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.Iktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}