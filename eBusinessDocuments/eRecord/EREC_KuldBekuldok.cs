
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldBekuldok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_KuldBekuldok : BaseEREC_KuldBekuldok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_Bekuldo property </summary>
        public String Partner_Id_Bekuldo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Bekuldo); }
            set { Typed.Partner_Id_Bekuldo = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Bekuldo); }                                            
        }
                   
           
        /// <summary>
        /// PartnerCim_Id_Bekuldo property </summary>
        public String PartnerCim_Id_Bekuldo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.PartnerCim_Id_Bekuldo); }
            set { Typed.PartnerCim_Id_Bekuldo = Utility.SetSqlGuidFromString(value, Typed.PartnerCim_Id_Bekuldo); }                                            
        }
                   
           
        /// <summary>
        /// NevSTR property </summary>
        public String NevSTR
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR); }
            set { Typed.NevSTR = Utility.SetSqlStringFromString(value, Typed.NevSTR); }                                            
        }
                   
           
        /// <summary>
        /// CimSTR property </summary>
        public String CimSTR
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR); }
            set { Typed.CimSTR = Utility.SetSqlStringFromString(value, Typed.CimSTR); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}