
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_KuldKapcsolatok : BaseEREC_KuldKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KapcsolatTipus property </summary>
        public String KapcsolatTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.KapcsolatTipus); }
            set { Typed.KapcsolatTipus = Utility.SetSqlStringFromString(value, Typed.KapcsolatTipus); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Kezi property </summary>
        public String Kezi
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kezi); }
            set { Typed.Kezi = Utility.SetSqlCharsFromString(value, Typed.Kezi); }                                            
        }
                   
           
        /// <summary>
        /// Kuld_Kuld_Beepul property </summary>
        public String Kuld_Kuld_Beepul
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kuld_Kuld_Beepul); }
            set { Typed.Kuld_Kuld_Beepul = Utility.SetSqlGuidFromString(value, Typed.Kuld_Kuld_Beepul); }                                            
        }
                   
           
        /// <summary>
        /// Kuld_Kuld_Felepul property </summary>
        public String Kuld_Kuld_Felepul
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kuld_Kuld_Felepul); }
            set { Typed.Kuld_Kuld_Felepul = Utility.SetSqlGuidFromString(value, Typed.Kuld_Kuld_Felepul); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}