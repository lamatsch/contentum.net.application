
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldKuldemenyek BusinessDocument Class
    /// K�ldem�nyek: Bej�vo/kimeno/belso is
    /// -- m�d. index: TEST_KULD_PRT_ID_ORZO 
    /// -- m�d index: IX_NevSTR
    /// -- �j index: IX_Hivatkozsai_szam
    /// -- �j index: IX_Postazas_iranya
    /// -- �j index: IX_Ragszam
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    IktatastNemIgenyel,
    ///    KezbesitesModja,   
    ///    Munkaallomas,      
    ///   SerultKuldemeny,   
    ///   TevesCimzes,       
    ///   TevesErkeztetes 
    /// -- �j oszlop:  add AA 2015.12.10
    ///   CimzesTipusa 
    /// 
    /// �j oszlopok
    /// M�dos�t�s �rv�nyess�g�nek kezdete
    /// Megsz�nteto hat�rozat, javaslat
    /// A fel�lvizsg�lat d�tuma
    /// Fel�lvizsg�l�
    /// MinositesFelulvizsgalatEredm
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_KuldKuldemenyek : BaseEREC_KuldKuldemenyek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// A k�ldem�ny �llapota. KCS: KULDEMENY_ALLAPOT
        /// 00 - �tvett
        /// 01 - �rkeztetett
        /// 03 - Tov�bb�t�s alatt
        /// 04 - Iktatott
        /// 05 - Szign�lt
        /// 06 - Post�zott
        /// 10 - Iktat�s megtagadva
        /// 51 - Kimeno, �ssze�ll�t�s alatt
        /// 52 - Expedi�lt
        /// 90 - Sztorn�zott
        /// 99 - Lez�rt
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// BeerkezesIdeje property
        /// A k�ldem�ny be�rkez�si ideje 
        /// </summary>
        public String BeerkezesIdeje
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.BeerkezesIdeje); }
            set { Typed.BeerkezesIdeje = Utility.SetSqlDateTimeFromString(value, Typed.BeerkezesIdeje); }                                            
        }
                   
           
        /// <summary>
        /// FelbontasDatuma property
        /// 
        /// </summary>
        public String FelbontasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FelbontasDatuma); }
            set { Typed.FelbontasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.FelbontasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        public String IraIktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIktatokonyv_Id); }
            set { Typed.IraIktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.IraIktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldesMod property
        /// KCS: KULDEMENY_KULDES_MODJA
        /// 01 - Postai sima
        /// 02 - Postai t�rtivev�nyes
        /// 03 - Postai aj�nlott
        /// 04 - �gyf�l
        /// 05 - �llami fut�rszolg�lat
        /// 07 - Diplom�ciai fut�rszolg�lat
        /// 08 - L�giposta
        /// 09 - K�zbes�to �tj�n
        /// 10 - Fax
        /// 11 - E-mail
        /// 13 - Helyben
        /// 14 - Postai t�virat
        /// 15 - Port�lon kereszt�l
        /// 17 - Postai elsobbs�gi
        /// 18 - Fut�rszolg�lat
        /// </summary>
        public String KuldesMod
        {
            get { return Utility.GetStringFromSqlString(Typed.KuldesMod); }
            set { Typed.KuldesMod = Utility.SetSqlStringFromString(value, Typed.KuldesMod); }                                            
        }
                   
           
        /// <summary>
        /// Erkezteto_Szam property
        /// EDOK szerinti �rkeztetosz�m.
        /// 
        /// </summary>
        public String Erkezteto_Szam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Erkezteto_Szam); }
            set { Typed.Erkezteto_Szam = Utility.SetSqlInt32FromString(value, Typed.Erkezteto_Szam); }                                            
        }
                   
           
        /// <summary>
        /// HivatkozasiSzam property
        /// �gyf�l, bek�ldo hivatkoz�si sz�ma
        /// </summary>
        public String HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.HivatkozasiSzam); }
            set { Typed.HivatkozasiSzam = Utility.SetSqlStringFromString(value, Typed.HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// Targy property
        /// K�ldem�ny t�rgya, le�r�sa
        /// </summary>
        public String Targy
        {
            get { return Utility.GetStringFromSqlString(Typed.Targy); }
            set { Typed.Targy = Utility.SetSqlStringFromString(value, Typed.Targy); }                                            
        }
                   
           
        /// <summary>
        /// Tartalom property
        /// 
        /// </summary>
        public String Tartalom
        {
            get { return Utility.GetStringFromSqlString(Typed.Tartalom); }
            set { Typed.Tartalom = Utility.SetSqlStringFromString(value, Typed.Tartalom); }                                            
        }
                   
           
        /// <summary>
        /// RagSzam property
        /// Postai ragsz�m 
        /// </summary>
        public String RagSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.RagSzam); }
            set { Typed.RagSzam = Utility.SetSqlStringFromString(value, Typed.RagSzam); }                                            
        }
                   
           
        /// <summary>
        /// Surgosseg property
        /// KCS: SURGOSSEG
        /// 
        /// </summary>
        public String Surgosseg
        {
            get { return Utility.GetStringFromSqlString(Typed.Surgosseg); }
            set { Typed.Surgosseg = Utility.SetSqlStringFromString(value, Typed.Surgosseg); }                                            
        }
                   
           
        /// <summary>
        /// BelyegzoDatuma property
        /// B�lyegzo d�tuma, post�z�s d�tuma, kemenore, �s bej�vore
        /// </summary>
        public String BelyegzoDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.BelyegzoDatuma); }
            set { Typed.BelyegzoDatuma = Utility.SetSqlDateTimeFromString(value, Typed.BelyegzoDatuma); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesModja property
        /// KCS: 'UGYINTEZES_ALAPJA'
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// 
        /// </summary>
        public String UgyintezesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesModja); }
            set { Typed.UgyintezesModja = Utility.SetSqlStringFromString(value, Typed.UgyintezesModja); }                                            
        }
                   
           
        /// <summary>
        /// PostazasIranya property
        /// KCS: POSTAZAS_IRANYA
        /// 1 - Bej�vo
        /// 2 - Kimeno
        /// </summary>
        public String PostazasIranya
        {
            get { return Utility.GetStringFromSqlString(Typed.PostazasIranya); }
            set { Typed.PostazasIranya = Utility.SetSqlStringFromString(value, Typed.PostazasIranya); }                                            
        }
                   
           
        /// <summary>
        /// Tovabbito property
        /// KCS: TOVABBITO_SZERVEZET
        /// </summary>
        public String Tovabbito
        {
            get { return Utility.GetStringFromSqlString(Typed.Tovabbito); }
            set { Typed.Tovabbito = Utility.SetSqlStringFromString(value, Typed.Tovabbito); }                                            
        }
                   
           
        /// <summary>
        /// PeldanySzam property
        /// K�ldem�ny p�ld�nysz�ma
        /// </summary>
        public String PeldanySzam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.PeldanySzam); }
            set { Typed.PeldanySzam = Utility.SetSqlInt32FromString(value, Typed.PeldanySzam); }                                            
        }
                   
           
        /// <summary>
        /// IktatniKell property
        /// 0 - Nem, 1 - Igen
        /// 
        /// </summary>
        public String IktatniKell
        {
            get { return Utility.GetStringFromSqlString(Typed.IktatniKell); }
            set { Typed.IktatniKell = Utility.SetSqlStringFromString(value, Typed.IktatniKell); }                                            
        }
                   
           
        /// <summary>
        /// Iktathato property
        /// A t�tel szign�l�si �llapota. KCS: KuldIktSzignTipus
        ///  0 - szign�l�s elotti
        ///  S - szervezeti szign�l�s sz�ks�ges
        ///  U - szervezeti �s �gyint�zore val� szign�l�s sz�ks�ges
        ///  1- iktathat�
        /// 
        /// </summary>
        public String Iktathato
        {
            get { return Utility.GetStringFromSqlString(Typed.Iktathato); }
            set { Typed.Iktathato = Utility.SetSqlStringFromString(value, Typed.Iktathato); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id_Szulo property
        /// K�ldem�ny Id
        /// </summary>
        public String KuldKuldemeny_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id_Szulo); }
            set { Typed.KuldKuldemeny_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// Erkeztetes_Ev property
        /// �rkeztet�s �ve.
        /// </summary>
        public String Erkeztetes_Ev
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Erkeztetes_Ev); }
            set { Typed.Erkeztetes_Ev = Utility.SetSqlInt32FromString(value, Typed.Erkeztetes_Ev); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Cimzett property
        /// Minden jogad�s K�VETKEZETESEN CSOPORTnak t�rt�nik! 
        /// M�rpedig c�mzett csak olyan lehet, aki kezdeni is tud valamit a k�ldem�nynel (ok lesznek az elso felelos). 
        /// Ez�rt mind a c�mzett, mind a felelos csoport. A felelos MINDEN�TT csoport, a kimeno k�ldem�ny (iratp�ld�ny) c�mzettje - aki tipikusan k�lso - PARTNER!
        /// 
        /// </summary>
        public String Csoport_Id_Cimzett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Cimzett); }
            set { Typed.Csoport_Id_Cimzett = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Cimzett); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felelos felhaszn�l� Id
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial property
        /// 
        /// </summary>
        public String FelhasznaloCsoport_Id_Expedial
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Expedial); }
            set { Typed.FelhasznaloCsoport_Id_Expedial = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Expedial); }                                            
        }
                   
           
        /// <summary>
        /// ExpedialasIdeje property
        /// 
        /// </summary>
        public String ExpedialasIdeje
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ExpedialasIdeje); }
            set { Typed.ExpedialasIdeje = Utility.SetSqlDateTimeFromString(value, Typed.ExpedialasIdeje); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Minden olyan helyen, ahol adott SZEM�LY azonos�that� (valamit csin�lt, pl. �tvett, vagy v�grehajtott valamit), ott FELHASZN�L� van r�gz�tve, mert TUDJUK, hogy ki volt az elk�veto (a felelos eset�ben ezt SOHA nem tudjuk, mert helyettes�t�s, vagy m�s jog miatt m�g a szem�lyre szign�l�sn�l is LEHET elt�ro a t�nyleges v�grehajt�).
        /// (Kovax)
        /// </summary>
        public String FelhasznaloCsoport_Id_Orzo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Orzo); }
            set { Typed.FelhasznaloCsoport_Id_Orzo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Orzo); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Atvevo property
        /// Minden olyan helyen, ahol adott SZEM�LY azonos�that� (valamit csin�lt, pl. �tvett, vagy v�grehajtott valamit), ott FELHASZN�L� van r�gz�tve, mert TUDJUK, hogy ki volt az elk�veto (a felelos eset�ben ezt SOHA nem tudjuk, mert helyettes�t�s, vagy m�s jog miatt m�g a szem�lyre szign�l�sn�l is LEHET elt�ro a t�nyleges v�grehajt�). (Kovax)
        /// 
        /// </summary>
        public String FelhasznaloCsoport_Id_Atvevo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Atvevo); }
            set { Typed.FelhasznaloCsoport_Id_Atvevo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Atvevo); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_Bekuldo property
        /// Bek�ldo Id -ja...
        /// </summary>
        public String Partner_Id_Bekuldo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Bekuldo); }
            set { Typed.Partner_Id_Bekuldo = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Bekuldo); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id property
        /// C�m Id
        /// </summary>
        public String Cim_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id); }
            set { Typed.Cim_Id = Utility.SetSqlGuidFromString(value, Typed.Cim_Id); }                                            
        }
                   
           
        /// <summary>
        /// CimSTR_Bekuldo property
        /// Bek�ldo c�me sz�vegesen
        /// </summary>
        public String CimSTR_Bekuldo
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR_Bekuldo); }
            set { Typed.CimSTR_Bekuldo = Utility.SetSqlStringFromString(value, Typed.CimSTR_Bekuldo); }                                            
        }
                   
           
        /// <summary>
        /// NevSTR_Bekuldo property
        /// Bek�ldo neve sz�vegesen
        /// </summary>
        public String NevSTR_Bekuldo
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR_Bekuldo); }
            set { Typed.NevSTR_Bekuldo = Utility.SetSqlStringFromString(value, Typed.NevSTR_Bekuldo); }                                            
        }
                   
           
        /// <summary>
        /// AdathordozoTipusa property
        /// KCS:ADATHORDOZO_TIPUSA
        /// 
        /// </summary>
        public String AdathordozoTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.AdathordozoTipusa); }
            set { Typed.AdathordozoTipusa = Utility.SetSqlStringFromString(value, Typed.AdathordozoTipusa); }                                            
        }
                   
           
        /// <summary>
        /// ElsodlegesAdathordozoTipusa property
        /// KCS: ELSODLEGES_ADATHORDOZO pap�r, elektronikus
        /// </summary>
        public String ElsodlegesAdathordozoTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.ElsodlegesAdathordozoTipusa); }
            set { Typed.ElsodlegesAdathordozoTipusa = Utility.SetSqlStringFromString(value, Typed.ElsodlegesAdathordozoTipusa); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo property
        /// Al��r� felhaszn�l� Id
        /// </summary>
        public String FelhasznaloCsoport_Id_Alairo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Alairo); }
            set { Typed.FelhasznaloCsoport_Id_Alairo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Alairo); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Bonto property
        /// A kuldemenyt felbonto felhasznalo. A minosites miatt kotelezo a lehetoseg. Nem bonthato kuldemeny eseten (pl. nincs targy) maradhat uresen.
        /// </summary>
        public String FelhasznaloCsoport_Id_Bonto
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Bonto); }
            set { Typed.FelhasznaloCsoport_Id_Bonto = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Bonto); }                                            
        }
                   
           
        /// <summary>
        /// CsoportFelelosEloszto_Id property
        /// Felelos csoport/szem�ly Id-je
        /// </summary>
        public String CsoportFelelosEloszto_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.CsoportFelelosEloszto_Id); }
            set { Typed.CsoportFelelosEloszto_Id = Utility.SetSqlGuidFromString(value, Typed.CsoportFelelosEloszto_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id
        /// </summary>
        public String Csoport_Id_Felelos_Elozo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos_Elozo); }
            set { Typed.Csoport_Id_Felelos_Elozo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos_Elozo); }                                            
        }
                   
           
        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Felelos_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Felelos_Id); }
            set { Typed.Kovetkezo_Felelos_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Felelos_Id); }                                            
        }
                   
           
        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        public String Elektronikus_Kezbesitesi_Allap
        {
            get { return Utility.GetStringFromSqlString(Typed.Elektronikus_Kezbesitesi_Allap); }
            set { Typed.Elektronikus_Kezbesitesi_Allap = Utility.SetSqlStringFromString(value, Typed.Elektronikus_Kezbesitesi_Allap); }                                            
        }
                   
           
        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Orzo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Orzo_Id); }
            set { Typed.Kovetkezo_Orzo_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Orzo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        public String Fizikai_Kezbesitesi_Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Fizikai_Kezbesitesi_Allapot); }
            set { Typed.Fizikai_Kezbesitesi_Allapot = Utility.SetSqlStringFromString(value, Typed.Fizikai_Kezbesitesi_Allapot); }                                            
        }
                   
           
        /// <summary>
        /// IraIratok_Id property
        /// Visszamutat az iratra, 1..1-es kapcsolat.
        /// </summary>
        public String IraIratok_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIratok_Id); }
            set { Typed.IraIratok_Id = Utility.SetSqlGuidFromString(value, Typed.IraIratok_Id); }                                            
        }
                   
           
        /// <summary>
        /// BontasiMegjegyzes property
        /// A bot�ssal kapcsolatos megjegyz�s helye
        /// </summary>
        public String BontasiMegjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.BontasiMegjegyzes); }
            set { Typed.BontasiMegjegyzes = Utility.SetSqlStringFromString(value, Typed.BontasiMegjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// KCS: KULDEMENY_TIPUS
        /// 01 - Sz�mla
        /// 02 - Szerzod�s
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Minosites property
        /// A nyilv�nos �s nem nyilv�nos iratok (irat alapj�ul szolg�l� k�ldem�nyek) megk�l�nb�ztet�se. KCS: IRAT_MINOSITES
        ///  �res- nyilv�nos
        ///  10 - belso haszn�lat�, nem nyilv�nos
        ///  20 - d�nt�s elok�sz�to, nem nyilv�nos
        /// </summary>
        public String Minosites
        {
            get { return Utility.GetStringFromSqlString(Typed.Minosites); }
            set { Typed.Minosites = Utility.SetSqlStringFromString(value, Typed.Minosites); }                                            
        }
                   
           
        /// <summary>
        /// MegtagadasIndoka property
        /// Csak elektronikus k�ldem�ny iktat�s megtagad�sa eset�n
        /// </summary>
        public String MegtagadasIndoka
        {
            get { return Utility.GetStringFromSqlString(Typed.MegtagadasIndoka); }
            set { Typed.MegtagadasIndoka = Utility.SetSqlStringFromString(value, Typed.MegtagadasIndoka); }                                            
        }
                   
           
        /// <summary>
        /// Megtagado_Id property
        /// Felhaszn�l� id az elektronikus k�ldem�ny iktat�s megtagad�sa eset�n
        /// </summary>
        public String Megtagado_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Megtagado_Id); }
            set { Typed.Megtagado_Id = Utility.SetSqlGuidFromString(value, Typed.Megtagado_Id); }                                            
        }
                   
           
        /// <summary>
        /// MegtagadasDat property
        /// Az elektronikus k�ldem�ny iktat�s megtagad�si d�tuma
        /// </summary>
        public String MegtagadasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegtagadasDat); }
            set { Typed.MegtagadasDat = Utility.SetSqlDateTimeFromString(value, Typed.MegtagadasDat); }                                            
        }
                   
           
        /// <summary>
        /// KimenoKuldemenyFajta property
        /// Postazasi adat,  KCS: KIMENO_KULDEMENY_FAJTA
        /// </summary>
        public String KimenoKuldemenyFajta
        {
            get { return Utility.GetStringFromSqlString(Typed.KimenoKuldemenyFajta); }
            set { Typed.KimenoKuldemenyFajta = Utility.SetSqlStringFromString(value, Typed.KimenoKuldemenyFajta); }                                            
        }
                   
           
        /// <summary>
        /// Elsobbsegi property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String Elsobbsegi
        {
            get { return Utility.GetStringFromSqlChars(Typed.Elsobbsegi); }
            set { Typed.Elsobbsegi = Utility.SetSqlCharsFromString(value, Typed.Elsobbsegi); }                                            
        }
                   
           
        /// <summary>
        /// Ajanlott property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String Ajanlott
        {
            get { return Utility.GetStringFromSqlChars(Typed.Ajanlott); }
            set { Typed.Ajanlott = Utility.SetSqlCharsFromString(value, Typed.Ajanlott); }                                            
        }
                   
           
        /// <summary>
        /// Tertiveveny property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String Tertiveveny
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tertiveveny); }
            set { Typed.Tertiveveny = Utility.SetSqlCharsFromString(value, Typed.Tertiveveny); }                                            
        }
                   
           
        /// <summary>
        /// SajatKezbe property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String SajatKezbe
        {
            get { return Utility.GetStringFromSqlChars(Typed.SajatKezbe); }
            set { Typed.SajatKezbe = Utility.SetSqlCharsFromString(value, Typed.SajatKezbe); }                                            
        }
                   
           
        /// <summary>
        /// E_ertesites property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String E_ertesites
        {
            get { return Utility.GetStringFromSqlChars(Typed.E_ertesites); }
            set { Typed.E_ertesites = Utility.SetSqlCharsFromString(value, Typed.E_ertesites); }                                            
        }
                   
           
        /// <summary>
        /// E_elorejelzes property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String E_elorejelzes
        {
            get { return Utility.GetStringFromSqlChars(Typed.E_elorejelzes); }
            set { Typed.E_elorejelzes = Utility.SetSqlCharsFromString(value, Typed.E_elorejelzes); }                                            
        }
                   
           
        /// <summary>
        /// PostaiLezaroSzolgalat property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        public String PostaiLezaroSzolgalat
        {
            get { return Utility.GetStringFromSqlChars(Typed.PostaiLezaroSzolgalat); }
            set { Typed.PostaiLezaroSzolgalat = Utility.SetSqlCharsFromString(value, Typed.PostaiLezaroSzolgalat); }                                            
        }
                   
           
        /// <summary>
        /// Ar property
        /// Postazasi adat, a feladand� k�ldem�ny post�z�si d�ja
        /// </summary>
        public String Ar
        {
            get { return Utility.GetStringFromSqlString(Typed.Ar); }
            set { Typed.Ar = Utility.SetSqlStringFromString(value, Typed.Ar); }                                            
        }
                   
           
        /// <summary>
        /// KimenoKuld_Sorszam  property
        /// Postazasi adat, gener�lt sorsz�m
        /// </summary>
        public String KimenoKuld_Sorszam 
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KimenoKuld_Sorszam ); }
            set { Typed.KimenoKuld_Sorszam  = Utility.SetSqlInt32FromString(value, Typed.KimenoKuld_Sorszam ); }                                            
        }
                   
           
        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        public String IrattarId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IrattarId); }
            set { Typed.IrattarId = Utility.SetSqlGuidFromString(value, Typed.IrattarId); }                                            
        }
                   
           
        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        public String IrattariHely
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariHely); }
            set { Typed.IrattariHely = Utility.SetSqlStringFromString(value, Typed.IrattariHely); }                                            
        }
                   
           
        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: KULDEMENY_ALLAPOT
        /// </summary>
        public String TovabbitasAlattAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.TovabbitasAlattAllapot); }
            set { Typed.TovabbitasAlattAllapot = Utility.SetSqlStringFromString(value, Typed.TovabbitasAlattAllapot); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// A k�ldem�ny azonos�t�ja k�lso rendszerbeli (pl. eGovPortal) azonos�t�ja.
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// BoritoTipus property
        /// KCS:KULDEMENY_BORITO_TIPUS, 01-Bor�t�k, 02-Bor�t�, 03-Irat
        /// </summary>
        public String BoritoTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.BoritoTipus); }
            set { Typed.BoritoTipus = Utility.SetSqlStringFromString(value, Typed.BoritoTipus); }                                            
        }
                   
           
        /// <summary>
        /// MegorzesJelzo property
        /// 1:megorzendo, 0-egy�bk�nt
        /// </summary>
        public String MegorzesJelzo
        {
            get { return Utility.GetStringFromSqlChars(Typed.MegorzesJelzo); }
            set { Typed.MegorzesJelzo = Utility.SetSqlCharsFromString(value, Typed.MegorzesJelzo); }                                            
        }
                   
           
        /// <summary>
        /// KulsoAzonosito property
        /// 
        /// </summary>
        public String KulsoAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.KulsoAzonosito); }
            set { Typed.KulsoAzonosito = Utility.SetSqlStringFromString(value, Typed.KulsoAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// IktatastNemIgenyel property
        /// 
        /// </summary>
        public String IktatastNemIgenyel
        {
            get { return Utility.GetStringFromSqlChars(Typed.IktatastNemIgenyel); }
            set { Typed.IktatastNemIgenyel = Utility.SetSqlCharsFromString(value, Typed.IktatastNemIgenyel); }                                            
        }
                   
           
        /// <summary>
        /// KezbesitesModja property
        /// 
        /// </summary>
        public String KezbesitesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.KezbesitesModja); }
            set { Typed.KezbesitesModja = Utility.SetSqlStringFromString(value, Typed.KezbesitesModja); }                                            
        }
                   
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        public String Munkaallomas
        {
            get { return Utility.GetStringFromSqlString(Typed.Munkaallomas); }
            set { Typed.Munkaallomas = Utility.SetSqlStringFromString(value, Typed.Munkaallomas); }                                            
        }
                   
           
        /// <summary>
        /// SerultKuldemeny property
        /// 
        /// </summary>
        public String SerultKuldemeny
        {
            get { return Utility.GetStringFromSqlChars(Typed.SerultKuldemeny); }
            set { Typed.SerultKuldemeny = Utility.SetSqlCharsFromString(value, Typed.SerultKuldemeny); }                                            
        }
                   
           
        /// <summary>
        /// TevesCimzes property
        /// 
        /// </summary>
        public String TevesCimzes
        {
            get { return Utility.GetStringFromSqlChars(Typed.TevesCimzes); }
            set { Typed.TevesCimzes = Utility.SetSqlCharsFromString(value, Typed.TevesCimzes); }                                            
        }
                   
           
        /// <summary>
        /// TevesErkeztetes property
        /// 
        /// </summary>
        public String TevesErkeztetes
        {
            get { return Utility.GetStringFromSqlChars(Typed.TevesErkeztetes); }
            set { Typed.TevesErkeztetes = Utility.SetSqlCharsFromString(value, Typed.TevesErkeztetes); }                                            
        }
                   
           
        /// <summary>
        /// CimzesTipusa property
        /// 
        /// </summary>
        public String CimzesTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.CimzesTipusa); }
            set { Typed.CimzesTipusa = Utility.SetSqlStringFromString(value, Typed.CimzesTipusa); }                                            
        }
                   
           
        /// <summary>
        /// FutarJegyzekListaSzama property
        /// Fut�rszolg�lati k�zbes�t�s eset�n Fut�rjegyz�k-lista sz�ma, Szem�lyes k�zbes�t�s eset�n K�lso k�zbes�tok�nyv nyilv�ntart�si sz�ma. Csak T�K-ben haszn�ljuk
        /// </summary>
        public String FutarJegyzekListaSzama
        {
            get { return Utility.GetStringFromSqlString(Typed.FutarJegyzekListaSzama); }
            set { Typed.FutarJegyzekListaSzama = Utility.SetSqlStringFromString(value, Typed.FutarJegyzekListaSzama); }                                            
        }
                   
           
        /// <summary>
        /// KezbesitoAtvetelIdopontja property
        /// 
        /// </summary>
        public String KezbesitoAtvetelIdopontja
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KezbesitoAtvetelIdopontja); }
            set { Typed.KezbesitoAtvetelIdopontja = Utility.SetSqlDateTimeFromString(value, Typed.KezbesitoAtvetelIdopontja); }                                            
        }
                   
           
        /// <summary>
        /// KezbesitoAtvevoNeve property
        /// 
        /// </summary>
        public String KezbesitoAtvevoNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.KezbesitoAtvevoNeve); }
            set { Typed.KezbesitoAtvevoNeve = Utility.SetSqlStringFromString(value, Typed.KezbesitoAtvevoNeve); }                                            
        }
                   
           
        /// <summary>
        /// Minosito property
        /// 
        /// </summary>
        public String Minosito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Minosito); }
            set { Typed.Minosito = Utility.SetSqlGuidFromString(value, Typed.Minosito); }                                            
        }
                   
           
        /// <summary>
        /// MinositesErvenyessegiIdeje property
        /// 
        /// </summary>
        public String MinositesErvenyessegiIdeje
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MinositesErvenyessegiIdeje); }
            set { Typed.MinositesErvenyessegiIdeje = Utility.SetSqlDateTimeFromString(value, Typed.MinositesErvenyessegiIdeje); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMennyiseg property
        /// 
        /// </summary>
        public String TerjedelemMennyiseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.TerjedelemMennyiseg); }
            set { Typed.TerjedelemMennyiseg = Utility.SetSqlInt32FromString(value, Typed.TerjedelemMennyiseg); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg property
        /// 
        /// </summary>
        public String TerjedelemMennyisegiEgyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.TerjedelemMennyisegiEgyseg); }
            set { Typed.TerjedelemMennyisegiEgyseg = Utility.SetSqlStringFromString(value, Typed.TerjedelemMennyisegiEgyseg); }                                            
        }
                   
           
        /// <summary>
        /// TerjedelemMegjegyzes property
        /// 
        /// </summary>
        public String TerjedelemMegjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.TerjedelemMegjegyzes); }
            set { Typed.TerjedelemMegjegyzes = Utility.SetSqlStringFromString(value, Typed.TerjedelemMegjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// ModositasErvenyessegKezdete property
        /// 
        /// </summary>
        public String ModositasErvenyessegKezdete
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ModositasErvenyessegKezdete); }
            set { Typed.ModositasErvenyessegKezdete = Utility.SetSqlDateTimeFromString(value, Typed.ModositasErvenyessegKezdete); }                                            
        }
                   
           
        /// <summary>
        /// MegszuntetoHatarozat property
        /// 
        /// </summary>
        public String MegszuntetoHatarozat
        {
            get { return Utility.GetStringFromSqlString(Typed.MegszuntetoHatarozat); }
            set { Typed.MegszuntetoHatarozat = Utility.SetSqlStringFromString(value, Typed.MegszuntetoHatarozat); }                                            
        }
                   
           
        /// <summary>
        /// FelulvizsgalatDatuma property
        /// 
        /// </summary>
        public String FelulvizsgalatDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FelulvizsgalatDatuma); }
            set { Typed.FelulvizsgalatDatuma = Utility.SetSqlDateTimeFromString(value, Typed.FelulvizsgalatDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Felulvizsgalo_id property
        /// 
        /// </summary>
        public String Felulvizsgalo_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felulvizsgalo_id); }
            set { Typed.Felulvizsgalo_id = Utility.SetSqlGuidFromString(value, Typed.Felulvizsgalo_id); }                                            
        }
                   
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm property
        /// 
        /// </summary>
        public String MinositesFelulvizsgalatEredm
        {
            get { return Utility.GetStringFromSqlString(Typed.MinositesFelulvizsgalatEredm); }
            set { Typed.MinositesFelulvizsgalatEredm = Utility.SetSqlStringFromString(value, Typed.MinositesFelulvizsgalatEredm); }                                            
        }
                   
           
        /// <summary>
        /// MinositoSzervezet property
        /// 
        /// </summary>
        public String MinositoSzervezet
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MinositoSzervezet); }
            set { Typed.MinositoSzervezet = Utility.SetSqlGuidFromString(value, Typed.MinositoSzervezet); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_BekuldoKapcsolt property
        /// 
        /// </summary>
        public String Partner_Id_BekuldoKapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_BekuldoKapcsolt); }
            set { Typed.Partner_Id_BekuldoKapcsolt = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_BekuldoKapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag property
        /// 
        /// </summary>
        public String MinositesFelulvizsgalatBizTag
        {
            get { return Utility.GetStringFromSqlString(Typed.MinositesFelulvizsgalatBizTag); }
            set { Typed.MinositesFelulvizsgalatBizTag = Utility.SetSqlStringFromString(value, Typed.MinositesFelulvizsgalatBizTag); }                                            
        }
                   
           
        /// <summary>
        /// Erteknyilvanitas property
        /// 
        /// </summary>
        public String Erteknyilvanitas
        {
            get { return Utility.GetStringFromSqlChars(Typed.Erteknyilvanitas); }
            set { Typed.Erteknyilvanitas = Utility.SetSqlCharsFromString(value, Typed.Erteknyilvanitas); }                                            
        }
                   
           
        /// <summary>
        /// ErteknyilvanitasOsszege property
        /// 
        /// </summary>
        public String ErteknyilvanitasOsszege
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ErteknyilvanitasOsszege); }
            set { Typed.ErteknyilvanitasOsszege = Utility.SetSqlInt32FromString(value, Typed.ErteknyilvanitasOsszege); }                                            
        }
                           }
   
}