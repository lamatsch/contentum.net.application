
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_KuldTertivevenyek BusinessDocument Class
    /// KImen� k�ldem�nyekhez kapcsol�d� t�rtivev�nyek.
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    KezbVelelemBeallta,
    ///    KezbVelelemDatuma
    /// </summary>
    [Serializable()]
    public class EREC_KuldTertivevenyek : BaseEREC_KuldTertivevenyek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Kuldemeny_Id property
        /// FK
        /// </summary>
        public String Kuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kuldemeny_Id); }
            set { Typed.Kuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.Kuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Ragszam property
        /// Postai ragsz�m 
        /// </summary>
        public String Ragszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Ragszam); }
            set { Typed.Ragszam = Utility.SetSqlStringFromString(value, Typed.Ragszam); }                                            
        }
                   
           
        /// <summary>
        /// TertivisszaKod property
        /// KCS: TERTIVEVENY_VISSZA_KOD
        /// </summary>
        public String TertivisszaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.TertivisszaKod); }
            set { Typed.TertivisszaKod = Utility.SetSqlStringFromString(value, Typed.TertivisszaKod); }                                            
        }
                   
           
        /// <summary>
        /// TertivisszaDat property
        /// T�rtivev�ny vissza�rkez�si d�tuma
        /// </summary>
        public String TertivisszaDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.TertivisszaDat); }
            set { Typed.TertivisszaDat = Utility.SetSqlDateTimeFromString(value, Typed.TertivisszaDat); }                                            
        }
                   
           
        /// <summary>
        /// AtvevoSzemely property
        /// �tvev� szem�ly neve 
        /// </summary>
        public String AtvevoSzemely
        {
            get { return Utility.GetStringFromSqlString(Typed.AtvevoSzemely); }
            set { Typed.AtvevoSzemely = Utility.SetSqlStringFromString(value, Typed.AtvevoSzemely); }                                            
        }
                   
           
        /// <summary>
        /// AtvetelDat property
        /// T�rtivev�ny �tv�tel�nek d�tuma
        /// </summary>
        public String AtvetelDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtvetelDat); }
            set { Typed.AtvetelDat = Utility.SetSqlDateTimeFromString(value, Typed.AtvetelDat); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: TERTIVEV_ALLAPOT
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// KezbVelelemBeallta property
        /// 
        /// </summary>
        public String KezbVelelemBeallta
        {
            get { return Utility.GetStringFromSqlChars(Typed.KezbVelelemBeallta); }
            set { Typed.KezbVelelemBeallta = Utility.SetSqlCharsFromString(value, Typed.KezbVelelemBeallta); }                                            
        }
                   
           
        /// <summary>
        /// KezbVelelemDatuma property
        /// 
        /// </summary>
        public String KezbVelelemDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KezbVelelemDatuma); }
            set { Typed.KezbVelelemDatuma = Utility.SetSqlDateTimeFromString(value, Typed.KezbVelelemDatuma); }                                            
        }
                           }
   
}