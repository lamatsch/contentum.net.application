
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Kuldemeny_IratPeldanyai BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Kuldemeny_IratPeldanyai : BaseEREC_Kuldemeny_IratPeldanyai
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Peldany_Id property </summary>
        public String Peldany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Peldany_Id); }
            set { Typed.Peldany_Id = Utility.SetSqlGuidFromString(value, Typed.Peldany_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}