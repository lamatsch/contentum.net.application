
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Mellekletek BusinessDocument Class
    /// Az iratok ill. a k�ldem�nyek mell�kleteinek kapcsol�t�bl�ja.
    /// --�j index: IX_Kuldemeny_Id
    /// -- �j oszlop:  add AA 2015.12.10
    ///   MellekletAdathordozoTipus 
    /// </summary>
    [Serializable()]
    public class EREC_Mellekletek : BaseEREC_Mellekletek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�nyazonos�t�, k�ldem�nyhez kapcsol�d� mell�klet eset�n.
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// Irat Id
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// AdathordozoTipus property
        /// KCS:ADATHORDOZO_TIPUSA
        /// 01 - Pap�r  irat
        /// 02 - Fax
        /// 03 - E-mail
        /// 04 - Floppy lemez
        /// 05 - CD
        /// 06 - DVD
        /// 07 - Vide�
        /// 08 - Hang kazetta
        /// 09 - Egy�b
        /// 10 - Csomag
        /// 11 - Pendrive
        /// 12 - Bor�t�k
        /// 13 - Egy�b pap�r
        /// 
        /// </summary>
        public String AdathordozoTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.AdathordozoTipus); }
            set { Typed.AdathordozoTipus = Utility.SetSqlStringFromString(value, Typed.AdathordozoTipus); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// MennyisegiEgyseg property
        /// KCS: MENNYISEGI_EGYSEG
        /// 
        /// </summary>
        public String MennyisegiEgyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.MennyisegiEgyseg); }
            set { Typed.MennyisegiEgyseg = Utility.SetSqlStringFromString(value, Typed.MennyisegiEgyseg); }                                            
        }
                   
           
        /// <summary>
        /// Mennyiseg property
        /// Mennyis�gi egys�g szerinti mennyis�g
        /// </summary>
        public String Mennyiseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Mennyiseg); }
            set { Typed.Mennyiseg = Utility.SetSqlInt32FromString(value, Typed.Mennyiseg); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// MellekletAdathordozoTipus property
        /// 
        /// </summary>
        public String MellekletAdathordozoTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.MellekletAdathordozoTipus); }
            set { Typed.MellekletAdathordozoTipus = Utility.SetSqlStringFromString(value, Typed.MellekletAdathordozoTipus); }                                            
        }
                           }
   
}