
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_MetaDefinicio BusinessDocument Class
    /// Ezen l�gnak az �gyirat, �gyiratdarab, Irat, (Dokumentum), �s egy�b Objektumok meta adatai. 
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_Obj_MetaDefinicio : BaseEREC_Obj_MetaDefinicio
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Objtip_Id property
        /// Definici�Tipus vez�rel:
        /// 0., nincs objtip => KRT_DOKUMENTUMOK -ban t�roljuk a meta-t
        /// 1., nem az �gyiratnyilv�ntart�  alap t�bl�ihoz kapcsol�d�
        /// 2., az �gyiratnyilv�ntart�  alap t�bl�ihoz kapcsol�d�
        ///  Az �gyt�pus al�bont�si m�lys�g�nek megfelel� egyedt�pust azonos�tja. (�gyirat/�gyiratdarab/Irat eset�n az 5 attrib�tum �sszerendel�se,
        /// m�g m�s objtip -n�l az objtip_id_column, �s a Ertek).
        /// 
        /// </summary>
        public String Objtip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Objtip_Id); }
            set { Typed.Objtip_Id = Utility.SetSqlGuidFromString(value, Typed.Objtip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Objtip_Id_Column property
        /// NEM HASZN�LT!!! T�bla oszlop objektum Id. ( az el�fordul�s ft�pus besorol� oszlopa,  
        /// (melyhez �ltal�ban kodcsoport - kodt�r �rt�k tartozik, de lehet flag, vagy ak�rmilyen konkr�t �rt�k is)
        /// </summary>
        public String Objtip_Id_Column
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Objtip_Id_Column); }
            set { Typed.Objtip_Id_Column = Utility.SetSqlGuidFromString(value, Typed.Objtip_Id_Column); }                                            
        }
                   
           
        /// <summary>
        /// ColumnValue property
        /// Az Objtip_Id_Column t�blaoszlop �rt�ke.
        /// </summary>
        public String ColumnValue
        {
            get { return Utility.GetStringFromSqlString(Typed.ColumnValue); }
            set { Typed.ColumnValue = Utility.SetSqlStringFromString(value, Typed.ColumnValue); }                                            
        }
                   
           
        /// <summary>
        /// DefinicioTipus property
        /// Az irat metadefin�ci� t�pusa. KCS: OBJ_METADEF_TIPUS 
        /// A0 - nincs objtip => KRT_DOKUMENTUMOK -ban CTT-vel hivatkozunk (pl. sz�mla)
        /// B1 - �ltal�nos objektum kieg�sz�t�s (pl. partner)
        /// B2 - �ltal�nos t�pusos objektum kieg�sz�t�s (pl. adott t�pus� partner)
        /// C2 - az �gyirat nyilv�ntart�shoz kapcsol�d� t�pusos objektum kieg�sz�t�s (az IratMetaDefinicio t�telein kereszt�l)
        /// 
        /// </summary>
        public String DefinicioTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.DefinicioTipus); }
            set { Typed.DefinicioTipus = Utility.SetSqlStringFromString(value, Typed.DefinicioTipus); }                                            
        }
                   
           
        /// <summary>
        /// Felettes_Obj_Meta property
        /// Egyedi azonos�t�
        /// </summary>
        public String Felettes_Obj_Meta
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felettes_Obj_Meta); }
            set { Typed.Felettes_Obj_Meta = Utility.SetSqlGuidFromString(value, Typed.Felettes_Obj_Meta); }                                            
        }
                   
           
        /// <summary>
        /// MetaXSD property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!!
        /// Ez lenne a Meta tipusa, ha pl. InfoPath �rlappal dolgozn�nk.
        /// 
        /// </summary>
        public String MetaXSD
        {
            get { return Utility.GetStringFromSqlXml(Typed.MetaXSD); }
            set { Typed.MetaXSD = Utility.SetSqlXmlFromString(value, Typed.MetaXSD); }                                            
        }
                   
           
        /// <summary>
        /// ImportXML property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!!
        /// </summary>
        public String ImportXML
        {
            get { return Utility.GetStringFromSqlXml(Typed.ImportXML); }
            set { Typed.ImportXML = Utility.SetSqlXmlFromString(value, Typed.ImportXML); }                                            
        }
                   
           
        /// <summary>
        /// EgyebXML property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!! V�sztartal�knak: hat�rid�k+feladatok 
        /// </summary>
        public String EgyebXML
        {
            get { return Utility.GetStringFromSqlXml(Typed.EgyebXML); }
            set { Typed.EgyebXML = Utility.SetSqlXmlFromString(value, Typed.EgyebXML); }                                            
        }
                   
           
        /// <summary>
        /// ContentType property
        /// A sablonazonos�t� els� eleme (a SharePoint-ban).
        /// </summary>
        public String ContentType
        {
            get { return Utility.GetStringFromSqlString(Typed.ContentType); }
            set { Typed.ContentType = Utility.SetSqlStringFromString(value, Typed.ContentType); }                                            
        }
                   
           
        /// <summary>
        /// SPSSzinkronizalt property
        /// A t�rgysz� szinkroniz�l�sa az SPS-sel (0/1)
        /// </summary>
        public String SPSSzinkronizalt
        {
            get { return Utility.GetStringFromSqlChars(Typed.SPSSzinkronizalt); }
            set { Typed.SPSSzinkronizalt = Utility.SetSqlCharsFromString(value, Typed.SPSSzinkronizalt); }                                            
        }
                   
           
        /// <summary>
        /// SPS_CTT_Id property
        /// A ContentType bels� azonos�t�ja a dokumentumt�rban (SPS-ben).
        /// </summary>
        public String SPS_CTT_Id
        {
            get { return Utility.GetStringFromSqlString(Typed.SPS_CTT_Id); }
            set { Typed.SPS_CTT_Id = Utility.SetSqlStringFromString(value, Typed.SPS_CTT_Id); }                                            
        }
                   
           
        /// <summary>
        /// WorkFlowVezerles property
        /// Jelzi, hogy a  CTT szerinti dokumentum �llapotvez�relt-e, workflow defin�ci�n kereszt�l (0/1).
        /// </summary>
        public String WorkFlowVezerles
        {
            get { return Utility.GetStringFromSqlChars(Typed.WorkFlowVezerles); }
            set { Typed.WorkFlowVezerles = Utility.SetSqlCharsFromString(value, Typed.WorkFlowVezerles); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}