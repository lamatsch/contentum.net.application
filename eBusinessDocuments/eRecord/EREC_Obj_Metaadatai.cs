
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_MetaAdatai BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Obj_MetaAdatai : BaseEREC_Obj_MetaAdatai
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_MetaDefinicio_Id property </summary>
        public String Obj_MetaDefinicio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_MetaDefinicio_Id); }
            set { Typed.Obj_MetaDefinicio_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_MetaDefinicio_Id); }                                            
        }
                   
           
        /// <summary>
        /// Targyszavak_Id property </summary>
        public String Targyszavak_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Targyszavak_Id); }
            set { Typed.Targyszavak_Id = Utility.SetSqlGuidFromString(value, Typed.Targyszavak_Id); }                                            
        }
                   
           
        /// <summary>
        /// AlapertelmezettErtek property </summary>
        public String AlapertelmezettErtek
        {
            get { return Utility.GetStringFromSqlString(Typed.AlapertelmezettErtek); }
            set { Typed.AlapertelmezettErtek = Utility.SetSqlStringFromString(value, Typed.AlapertelmezettErtek); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Opcionalis property </summary>
        public String Opcionalis
        {
            get { return Utility.GetStringFromSqlChars(Typed.Opcionalis); }
            set { Typed.Opcionalis = Utility.SetSqlCharsFromString(value, Typed.Opcionalis); }                                            
        }
                   
           
        /// <summary>
        /// Ismetlodo property </summary>
        public String Ismetlodo
        {
            get { return Utility.GetStringFromSqlChars(Typed.Ismetlodo); }
            set { Typed.Ismetlodo = Utility.SetSqlCharsFromString(value, Typed.Ismetlodo); }                                            
        }
                   
           
        /// <summary>
        /// Funkcio property </summary>
        public String Funkcio
        {
            get { return Utility.GetStringFromSqlString(Typed.Funkcio); }
            set { Typed.Funkcio = Utility.SetSqlStringFromString(value, Typed.Funkcio); }                                            
        }
                   
           
        /// <summary>
        /// ControlTypeSource property </summary>
        public String ControlTypeSource
        {
            get { return Utility.GetStringFromSqlString(Typed.ControlTypeSource); }
            set { Typed.ControlTypeSource = Utility.SetSqlStringFromString(value, Typed.ControlTypeSource); }                                            
        }
                   
           
        /// <summary>
        /// ControlTypeDataSource property </summary>
        public String ControlTypeDataSource
        {
            get { return Utility.GetStringFromSqlString(Typed.ControlTypeDataSource); }
            set { Typed.ControlTypeDataSource = Utility.SetSqlStringFromString(value, Typed.ControlTypeDataSource); }                                            
        }
                   
           
        /// <summary>
        /// SPSSzinkronizalt property </summary>
        public String SPSSzinkronizalt
        {
            get { return Utility.GetStringFromSqlChars(Typed.SPSSzinkronizalt); }
            set { Typed.SPSSzinkronizalt = Utility.SetSqlCharsFromString(value, Typed.SPSSzinkronizalt); }                                            
        }
                   
           
        /// <summary>
        /// SPS_Field_Id property </summary>
        public String SPS_Field_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SPS_Field_Id); }
            set { Typed.SPS_Field_Id = Utility.SetSqlGuidFromString(value, Typed.SPS_Field_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}