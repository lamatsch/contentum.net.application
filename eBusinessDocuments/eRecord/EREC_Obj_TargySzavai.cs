
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Obj_TargySzavai BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Obj_TargySzavai : BaseEREC_Obj_TargySzavai
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// TargySzo_Id property </summary>
        public String TargySzo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.TargySzo_Id); }
            set { Typed.TargySzo_Id = Utility.SetSqlGuidFromString(value, Typed.TargySzo_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_AdatElem property </summary>
        public String ObjTip_Id_AdatElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_AdatElem); }
            set { Typed.ObjTip_Id_AdatElem = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_AdatElem); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// ErtekTipus_Id property </summary>
        public String ErtekTipus_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ErtekTipus_Id); }
            set { Typed.ErtekTipus_Id = Utility.SetSqlGuidFromString(value, Typed.ErtekTipus_Id); }                                            
        }
                   
           
        /// <summary>
        /// Ertek property </summary>
        public String Ertek
        {
            get { return Utility.GetStringFromSqlString(Typed.Ertek); }
            set { Typed.Ertek = Utility.SetSqlStringFromString(value, Typed.Ertek); }                                            
        }
                   
           
        /// <summary>
        /// xml property </summary>
        public String xml
        {
            get { return Utility.GetStringFromSqlXml(Typed.xml); }
            set { Typed.xml = Utility.SetSqlXmlFromString(value, Typed.xml); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}