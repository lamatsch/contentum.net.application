
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_ObjektumTargyszavai BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_ObjektumTargyszavai : BaseEREC_ObjektumTargyszavai
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Targyszo_Id property </summary>
        public String Targyszo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Targyszo_Id); }
            set { Typed.Targyszo_Id = Utility.SetSqlGuidFromString(value, Typed.Targyszo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Metaadatai_Id property </summary>
        public String Obj_Metaadatai_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Metaadatai_Id); }
            set { Typed.Obj_Metaadatai_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Metaadatai_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Targyszo property </summary>
        public String Targyszo
        {
            get { return Utility.GetStringFromSqlString(Typed.Targyszo); }
            set { Typed.Targyszo = Utility.SetSqlStringFromString(value, Typed.Targyszo); }                                            
        }
                   
           
        /// <summary>
        /// Forras property </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlString(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlStringFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// SPSSzinkronizalt property </summary>
        public String SPSSzinkronizalt
        {
            get { return Utility.GetStringFromSqlChars(Typed.SPSSzinkronizalt); }
            set { Typed.SPSSzinkronizalt = Utility.SetSqlCharsFromString(value, Typed.SPSSzinkronizalt); }                                            
        }
                   
           
        /// <summary>
        /// Ertek property </summary>
        public String Ertek
        {
            get { return Utility.GetStringFromSqlString(Typed.Ertek); }
            set { Typed.Ertek = Utility.SetSqlStringFromString(value, Typed.Ertek); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Targyszo_XML property </summary>
        public String Targyszo_XML
        {
            get { return Utility.GetStringFromSqlXml(Typed.Targyszo_XML); }
            set { Typed.Targyszo_XML = Utility.SetSqlXmlFromString(value, Typed.Targyszo_XML); }                                            
        }
                   
           
        /// <summary>
        /// Targyszo_XML_Ervenyes property </summary>
        public String Targyszo_XML_Ervenyes
        {
            get { return Utility.GetStringFromSqlChars(Typed.Targyszo_XML_Ervenyes); }
            set { Typed.Targyszo_XML_Ervenyes = Utility.SetSqlCharsFromString(value, Typed.Targyszo_XML_Ervenyes); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}