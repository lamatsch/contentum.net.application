
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_PldIratPeldanyok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_PldIratPeldanyok : BaseEREC_PldIratPeldanyok
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }
        }


        /// <summary>
        /// UgyUgyirat_Id_Kulso property
        /// Az iratp�ld�nyhoz tartoz� legf�ls�bb szint� �gyirat azonos�t�ja.
        /// </summary>
        public String UgyUgyirat_Id_Kulso
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id_Kulso); }
            set { Typed.UgyUgyirat_Id_Kulso = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id_Kulso); }
        }


        /// <summary>
        /// Sorszam property
        /// Iratp�ld�ny sorsz�ma
        /// </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }
        }


        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }
        }


        /// <summary>
        /// AtvetelDatuma property
        /// Iratp�ld�ny �tv�tel d�tuma
        /// </summary>
        public String AtvetelDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtvetelDatuma); }
            set { Typed.AtvetelDatuma = Utility.SetSqlDateTimeFromString(value, Typed.AtvetelDatuma); }
        }


        /// <summary>
        /// Eredet property
        /// KCS: IRAT_FAJTA 
        /// (eredeti, szign�lt iratt�ri p�ld�ny, m�solat, m�sodlat, ...)
        /// </summary>
        public String Eredet
        {
            get { return Utility.GetStringFromSqlString(Typed.Eredet); }
            set { Typed.Eredet = Utility.SetSqlStringFromString(value, Typed.Eredet); }
        }


        /// <summary>
        /// KuldesMod property
        /// M�OSOSITAND�, KCS:KULDEMENY_KULDES_MODJA, pap�ros v. elekronikus �ton k�ri a v�laszt.
        /// 
        /// </summary>
        public String KuldesMod
        {
            get { return Utility.GetStringFromSqlString(Typed.KuldesMod); }
            set { Typed.KuldesMod = Utility.SetSqlStringFromString(value, Typed.KuldesMod); }
        }


        /// <summary>
        /// Ragszam property
        /// Nem haszn�lt! (Postai ragsz�m.)
        /// </summary>
        public String Ragszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Ragszam); }
            set { Typed.Ragszam = Utility.SetSqlStringFromString(value, Typed.Ragszam); }
        }


        /// <summary>
        /// UgyintezesModja property
        /// KCS: 'UGYINTEZES_ALAPJA'
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// </summary>
        public String UgyintezesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesModja); }
            set { Typed.UgyintezesModja = Utility.SetSqlStringFromString(value, Typed.UgyintezesModja); }
        }


        /// <summary>
        /// VisszaerkezesiHatarido property
        /// V�lasz vissza�rkez�si hat�rideje
        /// </summary>
        public String VisszaerkezesiHatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszaerkezesiHatarido); }
            set { Typed.VisszaerkezesiHatarido = Utility.SetSqlDateTimeFromString(value, Typed.VisszaerkezesiHatarido); }
        }


        /// <summary>
        /// Visszavarolag property
        /// KCS: VISSZAVAROLAG 
        /// (visszav�r�lag t�j�koztat�sra, ...). K�perny�n: Elv�r�s n�v alatt.
        /// 
        /// </summary>
        public String Visszavarolag
        {
            get { return Utility.GetStringFromSqlString(Typed.Visszavarolag); }
            set { Typed.Visszavarolag = Utility.SetSqlStringFromString(value, Typed.Visszavarolag); }
        }


        /// <summary>
        /// VisszaerkezesDatuma property
        /// V�lasz vissz�rkez�si d�tuma
        /// </summary>
        public String VisszaerkezesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszaerkezesDatuma); }
            set { Typed.VisszaerkezesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VisszaerkezesDatuma); }
        }


        /// <summary>
        /// Cim_id_Cimzett property
        /// C�mzett Partner Cim_Id 
        /// </summary>
        public String Cim_id_Cimzett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_id_Cimzett); }
            set { Typed.Cim_id_Cimzett = Utility.SetSqlGuidFromString(value, Typed.Cim_id_Cimzett); }
        }


        /// <summary>
        /// Partner_Id_Cimzett property
        /// Bek�ld� Id -ja...
        /// </summary>
        public String Partner_Id_Cimzett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Cimzett); }
            set { Typed.Partner_Id_Cimzett = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Cimzett); }
        }

        /// <summary>
        /// Partner_Id_CimzettKapcsolt property
        /// 
        /// </summary>
        public String Partner_Id_CimzettKapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_CimzettKapcsolt); }
            set { Typed.Partner_Id_CimzettKapcsolt = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_CimzettKapcsolt); }                                            
        }
		
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }
        }


        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Iratpld �rz� Id
        /// </summary>
        public String FelhasznaloCsoport_Id_Orzo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Orzo); }
            set { Typed.FelhasznaloCsoport_Id_Orzo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Orzo); }
        }


        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id
        /// </summary>
        public String Csoport_Id_Felelos_Elozo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos_Elozo); }
            set { Typed.Csoport_Id_Felelos_Elozo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos_Elozo); }
        }


        /// <summary>
        /// CimSTR_Cimzett property
        /// C�mzett sz�veges c�me
        /// </summary>
        public String CimSTR_Cimzett
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR_Cimzett); }
            set { Typed.CimSTR_Cimzett = Utility.SetSqlStringFromString(value, Typed.CimSTR_Cimzett); }
        }


        /// <summary>
        /// NevSTR_Cimzett property
        /// C�mzett teljes neve
        /// </summary>
        public String NevSTR_Cimzett
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR_Cimzett); }
            set { Typed.NevSTR_Cimzett = Utility.SetSqlStringFromString(value, Typed.NevSTR_Cimzett); }
        }


        /// <summary>
        /// Tovabbito property
        /// K�s�bb nem kell!! KCS: TOV�BB�T� SZERVEZET (Posta, Fut�rszolg�lat...).
        /// </summary>
        public String Tovabbito
        {
            get { return Utility.GetStringFromSqlString(Typed.Tovabbito); }
            set { Typed.Tovabbito = Utility.SetSqlStringFromString(value, Typed.Tovabbito); }
        }


        /// <summary>
        /// IraIrat_Id_Kapcsolt property
        /// Irat Id
        /// </summary>
        public String IraIrat_Id_Kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id_Kapcsolt); }
            set { Typed.IraIrat_Id_Kapcsolt = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id_Kapcsolt); }
        }


        /// <summary>
        /// IrattariHely property
        /// Iratt�ri hely, amennyiben iratt�roz�sra ker�lt.
        /// </summary>
        public String IrattariHely
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariHely); }
            set { Typed.IrattariHely = Utility.SetSqlStringFromString(value, Typed.IrattariHely); }
        }


        /// <summary>
        /// Gener_Id property
        /// ??
        /// </summary>
        public String Gener_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Gener_Id); }
            set { Typed.Gener_Id = Utility.SetSqlGuidFromString(value, Typed.Gener_Id); }
        }


        /// <summary>
        /// PostazasDatuma property
        /// NEM kell!!! P�ld�ny post�z�si d�tuma (kimen� pld elest�n)
        /// </summary>
        public String PostazasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.PostazasDatuma); }
            set { Typed.PostazasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.PostazasDatuma); }
        }


        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }
        }


        /// <summary>
        /// Allapot property
        /// KCS: IRATPELDANY_ALLAPOT
        /// 0 - Megnyitott (munkap�ld�ny)
        /// 2 - Befagyasztott (munkap�ld�ny)
        /// 04 - Iktatott
        /// 30 - Kiadm�nyozott
        /// 31 - Lez�rt jegyz�ken
        /// 32 - Jegyz�kre helyezett
        /// 40 - C�mzett �tvette
        /// 45 - Post�zott
        /// 50 - Tov�bb�t�s alatt
        /// 60 - J�v�hagy�s alatt
        /// 61 - �jrak�ldend�
        /// 63 - Kimen� k�ldem�nyben
        /// 64 - Expedi�lt
        /// 90 - Sztorn�zott
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }
        }


        /// <summary>
        /// Azonosito property
        /// Iktat�sz�m sz�vegesen.
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }
        }


        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Felelos_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Felelos_Id); }
            set { Typed.Kovetkezo_Felelos_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Felelos_Id); }
        }


        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        public String Elektronikus_Kezbesitesi_Allap
        {
            get { return Utility.GetStringFromSqlString(Typed.Elektronikus_Kezbesitesi_Allap); }
            set { Typed.Elektronikus_Kezbesitesi_Allap = Utility.SetSqlStringFromString(value, Typed.Elektronikus_Kezbesitesi_Allap); }
        }


        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Orzo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Orzo_Id); }
            set { Typed.Kovetkezo_Orzo_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Orzo_Id); }
        }


        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        public String Fizikai_Kezbesitesi_Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Fizikai_Kezbesitesi_Allapot); }
            set { Typed.Fizikai_Kezbesitesi_Allapot = Utility.SetSqlStringFromString(value, Typed.Fizikai_Kezbesitesi_Allapot); }
        }


        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: IRATPELDANY_ALLAPOT
        /// </summary>
        public String TovabbitasAlattAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.TovabbitasAlattAllapot); }
            set { Typed.TovabbitasAlattAllapot = Utility.SetSqlStringFromString(value, Typed.TovabbitasAlattAllapot); }
        }


        /// <summary>
        /// PostazasAllapot property
        /// KCS: IRATPELDANY_POSTAZAS_ALLAPOT
        /// </summary>
        public String PostazasAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.PostazasAllapot); }
            set { Typed.PostazasAllapot = Utility.SetSqlStringFromString(value, Typed.PostazasAllapot); }
        }


        /// <summary>
        /// ValaszElektronikus property
        /// 0-nem, 1-igen. 2. mindegy. 
        ///  A partnernek joga van meg,hat�rozni, hogy a v�laszt elektronikusan k�ri e.
        /// </summary>
        public String ValaszElektronikus
        {
            get { return Utility.GetStringFromSqlChars(Typed.ValaszElektronikus); }
            set { Typed.ValaszElektronikus = Utility.SetSqlCharsFromString(value, Typed.ValaszElektronikus); }
        }


        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        public String SelejtezesDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SelejtezesDat); }
            set { Typed.SelejtezesDat = Utility.SetSqlDateTimeFromString(value, Typed.SelejtezesDat); }
        }


        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtez� v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        public String FelhCsoport_Id_Selejtezo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhCsoport_Id_Selejtezo); }
            set { Typed.FelhCsoport_Id_Selejtezo = Utility.SetSqlGuidFromString(value, Typed.FelhCsoport_Id_Selejtezo); }
        }


        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvev� neve
        /// </summary>
        public String LeveltariAtvevoNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.LeveltariAtvevoNeve); }
            set { Typed.LeveltariAtvevoNeve = Utility.SetSqlStringFromString(value, Typed.LeveltariAtvevoNeve); }
        }

		/// <summary>
        /// IrattarId property
        /// Selejtezo v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        public String IrattarId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IrattarId); }
            set { Typed.IrattarId = Utility.SetSqlGuidFromString(value, Typed.IrattarId); }                                            
        }
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }
        }


        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }
        }

        /// <summary>
        /// IratPeldanyMegsemmisitve property
        /// BLG_2948 - LZS - Iratp�ld�ny megsemmis�tve tulajdons�g
        /// </summary>
        public String IratPeldanyMegsemmisitve
        {
            get { return Utility.GetStringFromSqlChars(Typed.IratPeldanyMegsemmisitve); }
            set { Typed.IratPeldanyMegsemmisitve = Utility.SetSqlCharsFromString(value, Typed.IratPeldanyMegsemmisitve); }
        }

        /// <summary>
        /// IratPeldanyMegsemmisitve property
        /// BLG_2948 - LZS - Iratp�ld�ny megsemmis�t�s d�tuma tulajdons�g
        /// </summary>
        public String IratPeldanyMegsemmisitesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IratPeldanyMegsemmisitesDatuma); }
            set { Typed.IratPeldanyMegsemmisitesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.IratPeldanyMegsemmisitesDatuma); }
        }
    }

}