
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_PldKapjakMeg BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_PldKapjakMeg : BaseEREC_PldKapjakMeg
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// PldIratPeldany_Id property </summary>
        public String PldIratPeldany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.PldIratPeldany_Id); }
            set { Typed.PldIratPeldany_Id = Utility.SetSqlGuidFromString(value, Typed.PldIratPeldany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id property </summary>
        public String Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id); }
            set { Typed.Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}