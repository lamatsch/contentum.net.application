
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_Szamlak BusinessDocument Class
    /// Számlanyilvántartáshoz kapcsolódó, küldeményt vagy dokumentumot kiegészítő adatok
    /// </summary>
    [Serializable()]
    public class EREC_Szamlak : BaseEREC_Szamlak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// Küldemény Id
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_Szallito property
        /// 
        /// </summary>
        public String Partner_Id_Szallito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Szallito); }
            set { Typed.Partner_Id_Szallito = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Szallito); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id_Szallito property
        /// 
        /// </summary>
        public String Cim_Id_Szallito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id_Szallito); }
            set { Typed.Cim_Id_Szallito = Utility.SetSqlGuidFromString(value, Typed.Cim_Id_Szallito); }                                            
        }
                   
           
        /// <summary>
        /// NevSTR_Szallito property
        /// Beküldő neve szövegesen
        /// </summary>
        public String NevSTR_Szallito
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR_Szallito); }
            set { Typed.NevSTR_Szallito = Utility.SetSqlStringFromString(value, Typed.NevSTR_Szallito); }                                            
        }
                   
           
        /// <summary>
        /// CimSTR_Szallito property
        /// Beküldő címe szövegesen
        /// </summary>
        public String CimSTR_Szallito
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR_Szallito); }
            set { Typed.CimSTR_Szallito = Utility.SetSqlStringFromString(value, Typed.CimSTR_Szallito); }                                            
        }
                   
           
        /// <summary>
        /// Bankszamlaszam_Id property
        /// Az átutaláshoz megadott bankszámlaszámot meghatározó azonosító
        /// </summary>
        public String Bankszamlaszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Bankszamlaszam_Id); }
            set { Typed.Bankszamlaszam_Id = Utility.SetSqlGuidFromString(value, Typed.Bankszamlaszam_Id); }                                            
        }
                   
           
        /// <summary>
        /// SzamlaSorszam property
        /// 
        /// </summary>
        public String SzamlaSorszam
        {
            get { return Utility.GetStringFromSqlString(Typed.SzamlaSorszam); }
            set { Typed.SzamlaSorszam = Utility.SetSqlStringFromString(value, Typed.SzamlaSorszam); }                                            
        }
                   
           
        /// <summary>
        /// FizetesiMod property
        /// 
        /// </summary>
        public String FizetesiMod
        {
            get { return Utility.GetStringFromSqlString(Typed.FizetesiMod); }
            set { Typed.FizetesiMod = Utility.SetSqlStringFromString(value, Typed.FizetesiMod); }                                            
        }
                   
           
        /// <summary>
        /// TeljesitesDatuma property
        /// 
        /// </summary>
        public String TeljesitesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.TeljesitesDatuma); }
            set { Typed.TeljesitesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.TeljesitesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// BizonylatDatuma property
        /// 
        /// </summary>
        public String BizonylatDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.BizonylatDatuma); }
            set { Typed.BizonylatDatuma = Utility.SetSqlDateTimeFromString(value, Typed.BizonylatDatuma); }                                            
        }
                   
           
        /// <summary>
        /// FizetesiHatarido property
        /// 
        /// </summary>
        public String FizetesiHatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FizetesiHatarido); }
            set { Typed.FizetesiHatarido = Utility.SetSqlDateTimeFromString(value, Typed.FizetesiHatarido); }                                            
        }
                   
           
        /// <summary>
        /// DevizaKod property
        /// 
        /// </summary>
        public String DevizaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.DevizaKod); }
            set { Typed.DevizaKod = Utility.SetSqlStringFromString(value, Typed.DevizaKod); }                                            
        }
                   
           
        /// <summary>
        /// VisszakuldesDatuma property
        /// 
        /// </summary>
        public String VisszakuldesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszakuldesDatuma); }
            set { Typed.VisszakuldesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VisszakuldesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// EllenorzoOsszeg property
        /// 
        /// </summary>
        public String EllenorzoOsszeg
        {
            get { return Utility.GetStringFromSqlDouble(Typed.EllenorzoOsszeg); }
            set { Typed.EllenorzoOsszeg = Utility.SetSqlDoubleFromString(value, Typed.EllenorzoOsszeg); }                                            
        }
                   
           
        /// <summary>
        /// PIRAllapot property
        /// 
        /// </summary>
        public String PIRAllapot
        {
            get { return Utility.GetStringFromSqlChars(Typed.PIRAllapot); }
            set { Typed.PIRAllapot = Utility.SetSqlCharsFromString(value, Typed.PIRAllapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }


        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        /// <summary>
        /// VevoBesorolas property
        /// Számla átadásánál használható csoportosító mező
        /// </summary>
        public String VevoBesorolas
        {
            get { return Utility.GetStringFromSqlString(Typed.VevoBesorolas); }
            set { Typed.VevoBesorolas = Utility.SetSqlStringFromString(value, Typed.VevoBesorolas); }
        }
    }


   
}