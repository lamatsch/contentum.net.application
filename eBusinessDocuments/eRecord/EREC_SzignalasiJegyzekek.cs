
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
    /// <summary>
    /// EREC_SzignalasiJegyzekek BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_SzignalasiJegyzekek : BaseEREC_SzignalasiJegyzekek
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// UgykorTargykor_Id property </summary>
        public String UgykorTargykor_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgykorTargykor_Id); }
            set { Typed.UgykorTargykor_Id = Utility.SetSqlGuidFromString(value, Typed.UgykorTargykor_Id); }
        }


        /// <summary>
        /// Csoport_Id_Felelos property </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }
        }


        /// <summary>
        /// SzignalasTipusa property </summary>
        public String SzignalasTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.SzignalasTipusa); }
            set { Typed.SzignalasTipusa = Utility.SetSqlStringFromString(value, Typed.SzignalasTipusa); }
        }


        /// <summary>
        /// SzignalasiKotelezettseg property </summary>
        public String SzignalasiKotelezettseg
        {
            get { return Utility.GetStringFromSqlString(Typed.SzignalasiKotelezettseg); }
            set { Typed.SzignalasiKotelezettseg = Utility.SetSqlStringFromString(value, Typed.SzignalasiKotelezettseg); }
        }

        /// <summary>
        /// Hataridos feladat Id</summary>
        public String HataridosFeladatId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.HataridosFeladatId); }
            set { Typed.HataridosFeladatId = Utility.SetSqlGuidFromString(value, Typed.HataridosFeladatId); }
        }
    }

}