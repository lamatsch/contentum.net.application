
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TargySzavak BusinessDocument Class
    /// CheckConstraint_fn
    /// 
    /// 2007.05.23: KovAx&HK&FF&PP: t�rgyszavak -ra defini�ljuk a meta adatokat is!
    /// </summary>
    [Serializable()]
    public class EREC_TargySzavak : BaseEREC_TargySzavak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// Review: T�rgysz� �rt�kkel/�rt�k n�lk�l (1/0).
        /// (Pl. TAJ Sz�m + �rt�k)
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// TargySzavak property
        /// ez nem felt�tlen egyetlen sz�!!
        /// </summary>
        public String TargySzavak
        {
            get { return Utility.GetStringFromSqlString(Typed.TargySzavak); }
            set { Typed.TargySzavak = Utility.SetSqlStringFromString(value, Typed.TargySzavak); }                                            
        }
                   
           
        /// <summary>
        /// AlapertelmezettErtek property
        /// A t�rgysz� alap�rtelmezett �rt�ke.
        /// </summary>
        public String AlapertelmezettErtek
        {
            get { return Utility.GetStringFromSqlString(Typed.AlapertelmezettErtek); }
            set { Typed.AlapertelmezettErtek = Utility.SetSqlStringFromString(value, Typed.AlapertelmezettErtek); }                                            
        }
                   
           
        /// <summary>
        /// BelsoAzonosito property
        /// A TargySzavak �rt�kb�l a (CovetrToIdentifier) f�ggv�nnyel k�pzett (�kezetes karakterek, spec karakterek konverzi�ja) �rt�k.
        /// </summary>
        public String BelsoAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.BelsoAzonosito); }
            set { Typed.BelsoAzonosito = Utility.SetSqlStringFromString(value, Typed.BelsoAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// SPSSzinkronizalt property
        /// A t�rgysz� szinkroniz�l�sa az SPS-sel (0/1)
        /// </summary>
        public String SPSSzinkronizalt
        {
            get { return Utility.GetStringFromSqlChars(Typed.SPSSzinkronizalt); }
            set { Typed.SPSSzinkronizalt = Utility.SetSqlCharsFromString(value, Typed.SPSSzinkronizalt); }                                            
        }
                   
           
        /// <summary>
        /// SPS_Field_Id property
        /// A t�rgysz� azonos�t�ja a dokumentumt�rban (SPS-ben).
        /// </summary>
        public String SPS_Field_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SPS_Field_Id); }
            set { Typed.SPS_Field_Id = Utility.SetSqlGuidFromString(value, Typed.SPS_Field_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Tulajdonos Id-je
        /// </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// RegExp property
        /// Regul�ris kifejer�s a t�rgysz� �rt�k�re.
        /// </summary>
        public String RegExp
        {
            get { return Utility.GetStringFromSqlString(Typed.RegExp); }
            set { Typed.RegExp = Utility.SetSqlStringFromString(value, Typed.RegExp); }                                            
        }
                   
           
        /// <summary>
        /// ToolTip property
        /// A regul�ris kifejez�shez tartoz� magyar�z� sz�veg.
        /// </summary>
        public String ToolTip
        {
            get { return Utility.GetStringFromSqlString(Typed.ToolTip); }
            set { Typed.ToolTip = Utility.SetSqlStringFromString(value, Typed.ToolTip); }                                            
        }
                   
           
        /// <summary>
        /// ControlTypeSource property
        /// Vez�rl�elem t�pusok KCS: CONTROLTYPE_SOURCE
        /// (Vez�rl�elem forr�sa (n�vt�rrel min�s�tett oszt�lyn�v, pontosvessz�vel elv�lasztva k�vetheti az Assembly neve, vagy bet�ltend� ascx-f�jl virtu�lis �tvonallal))
        /// Contentum.eUIControls.CustomTextBox;Contentum.eUIControls - Sz�vegmez� (CustomTextBox)
        /// System.Web.UI.WebControls.CheckBox;System.Web - Jel�l�n�gyzet (CheckBox)
        /// ~/Component/CalendarControl.ascx - Napt�r (CalendarControl)
        /// ~/Component/KodTarakDropDownList.ascx - K�dv�laszt� leg�rd�l� lista (KodTarakDropDownList)
        /// ~/Component/EditablePartnerTextBox.ascx - Partnerv�laszt� mez�, szerkeszthet� (EditablePartnerTextBox)
        /// Contentum.eUIControls.eDropDownList,Contentum.eUIControls - Leg�rd�l� lista (eDropDownList)
        /// System.Web.UI.WebControls.Label;System.Web - C�mke (Label)
        /// </summary>
        public String ControlTypeSource
        {
            get { return Utility.GetStringFromSqlString(Typed.ControlTypeSource); }
            set { Typed.ControlTypeSource = Utility.SetSqlStringFromString(value, Typed.ControlTypeSource); }                                            
        }
                   
           
        /// <summary>
        /// ControlTypeDataSource property
        /// Vez�rl�elem felt�lt�s�hez haszn�lt forr�s (jelenleg csak a k�dcsoportot �rtelmezi az alkalmaz�s, ha ControlTypeSource �rt�ke "KodTarakDropDownList", de k�s�bb esetleg szerv�z + met�dus + param�terek k�dol�s is kidolgozhat�)
        /// </summary>
        public String ControlTypeDataSource
        {
            get { return Utility.GetStringFromSqlString(Typed.ControlTypeDataSource); }
            set { Typed.ControlTypeDataSource = Utility.SetSqlStringFromString(value, Typed.ControlTypeDataSource); }                                            
        }
                   
           
        /// <summary>
        /// Targyszo_Id_Parent property
        /// 
        /// </summary>
        public String Targyszo_Id_Parent
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Targyszo_Id_Parent); }
            set { Typed.Targyszo_Id_Parent = Utility.SetSqlGuidFromString(value, Typed.Targyszo_Id_Parent); }                                            
        }
                   
           
        /// <summary>
        /// XSD property
        /// Ha Metaaatot �r le, akkor az XSD kit�lt�tt...
        /// </summary>
        public String XSD
        {
            get { return Utility.GetStringFromSqlXml(Typed.XSD); }
            set { Typed.XSD = Utility.SetSqlXmlFromString(value, Typed.XSD); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}