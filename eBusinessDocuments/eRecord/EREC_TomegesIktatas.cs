
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{
	/// <summary>
	/// EREC_TomegesIktatas BusinessDocument Class
	/// 
	/// </summary>
	[Serializable ( )]
	public class EREC_TomegesIktatas : BaseEREC_TomegesIktatas
	{
		[NonSerializedAttribute]
		public BaseTyped Typed = new BaseTyped ( );
		[NonSerializedAttribute]
		public BaseDocument Base = new BaseDocument ( );
		[NonSerializedAttribute]
		public BaseUpdated Updated = new BaseUpdated ( );

		/// <summary>
		/// Id property
		/// Egyedi azonos�t�
		/// </summary>
		public String Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.Id ); }
			set { Typed.Id = Utility.SetSqlGuidFromString ( value, Typed.Id ); }
		}


		/// <summary>
		/// ForrasTipusNev property
		/// 
		/// </summary>
		public String ForrasTipusNev
		{
			get { return Utility.GetStringFromSqlString ( Typed.ForrasTipusNev ); }
			set { Typed.ForrasTipusNev = Utility.SetSqlStringFromString ( value, Typed.ForrasTipusNev ); }
		}


		/// <summary>
		/// FelelosCsoport_Id property
		/// 
		/// </summary>
		public String FelelosCsoport_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.FelelosCsoport_Id ); }
			set { Typed.FelelosCsoport_Id = Utility.SetSqlGuidFromString ( value, Typed.FelelosCsoport_Id ); }
		}


		/// <summary>
		/// AgazatiJelek_Id property
		/// 
		/// </summary>
		public String AgazatiJelek_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.AgazatiJelek_Id ); }
			set { Typed.AgazatiJelek_Id = Utility.SetSqlGuidFromString ( value, Typed.AgazatiJelek_Id ); }
		}


		/// <summary>
		/// IraIrattariTetelek_Id property
		/// 
		/// </summary>
		public String IraIrattariTetelek_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.IraIrattariTetelek_Id ); }
			set { Typed.IraIrattariTetelek_Id = Utility.SetSqlGuidFromString ( value, Typed.IraIrattariTetelek_Id ); }
		}


		/// <summary>
		/// Ugytipus_Id property
		/// 
		/// </summary>
		public String Ugytipus_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.Ugytipus_Id ); }
			set { Typed.Ugytipus_Id = Utility.SetSqlGuidFromString ( value, Typed.Ugytipus_Id ); }
		}


		/// <summary>
		/// Irattipus_Id property
		/// 
		/// </summary>
		public String Irattipus_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.Irattipus_Id ); }
			set { Typed.Irattipus_Id = Utility.SetSqlGuidFromString ( value, Typed.Irattipus_Id ); }
		}


		/// <summary>
		/// TargyPrefix property
		/// 
		/// </summary>
		public String TargyPrefix
		{
			get { return Utility.GetStringFromSqlString ( Typed.TargyPrefix ); }
			set { Typed.TargyPrefix = Utility.SetSqlStringFromString ( value, Typed.TargyPrefix ); }
		}


		/// <summary>
		/// AlairasKell property
		/// 
		/// </summary>
		public String AlairasKell
		{
			get { return Utility.GetStringFromSqlChars ( Typed.AlairasKell ); }
			set { Typed.AlairasKell = Utility.SetSqlCharsFromString ( value, Typed.AlairasKell ); }
		}


		/// <summary>
		/// AlairasMod property
		/// 
		/// </summary>
		public String AlairasMod
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.AlairasMod ); }
			set { Typed.AlairasMod = Utility.SetSqlGuidFromString ( value, Typed.AlairasMod ); }
		}


		/// <summary>
		/// FelhasznaloCsoport_Id_Alairo property
		/// 
		/// </summary>
		public String FelhasznaloCsoport_Id_Alairo
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.FelhasznaloCsoport_Id_Alairo ); }
			set { Typed.FelhasznaloCsoport_Id_Alairo = Utility.SetSqlGuidFromString ( value, Typed.FelhasznaloCsoport_Id_Alairo ); }
		}


		/// <summary>
		/// FelhaszCsoport_Id_Helyettesito property
		/// 
		/// </summary>
		public String FelhaszCsoport_Id_Helyettesito
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.FelhaszCsoport_Id_Helyettesito ); }
			set { Typed.FelhaszCsoport_Id_Helyettesito = Utility.SetSqlGuidFromString ( value, Typed.FelhaszCsoport_Id_Helyettesito ); }
		}


		/// <summary>
		/// AlairasSzabaly_Id property
		/// 
		/// </summary>
		public String AlairasSzabaly_Id
		{
			get { return Utility.GetStringFromSqlGuid ( Typed.AlairasSzabaly_Id ); }
			set { Typed.AlairasSzabaly_Id = Utility.SetSqlGuidFromString ( value, Typed.AlairasSzabaly_Id ); }
		}


		/// <summary>
		/// HatosagiAdatlapKell property
		/// 
		/// </summary>
		public String HatosagiAdatlapKell
		{
			get { return Utility.GetStringFromSqlChars ( Typed.HatosagiAdatlapKell ); }
			set { Typed.HatosagiAdatlapKell = Utility.SetSqlCharsFromString ( value, Typed.HatosagiAdatlapKell ); }
		}


		/// <summary>
		/// UgyFajtaja property
		/// 2008.03.31 KCS: kcs.UGY_FAJTAJA
		/// 
		/// </summary>
		public String UgyFajtaja
		{
			get { return Utility.GetStringFromSqlString ( Typed.UgyFajtaja ); }
			set { Typed.UgyFajtaja = Utility.SetSqlStringFromString ( value, Typed.UgyFajtaja ); }
		}


		/// <summary>
		/// DontestHozta property
		/// 2. A d�nt�st hozta -> KCS: DONTEST_HOZTA
		/// k�zgy�l�s test�lete (1)
		/// k�zgy�l�s bizotts�ga (2)
		/// a r�sz�nkorm�nyzat test�lete (3)
		/// a (f�)polg�rmester (4)
		/// a (f�)jegyz� (5)
		/// </summary>
		public String DontestHozta
		{
			get { return Utility.GetStringFromSqlString ( Typed.DontestHozta ); }
			set { Typed.DontestHozta = Utility.SetSqlStringFromString ( value, Typed.DontestHozta ); }
		}


		/// <summary>
		/// DontesFormaja property
		/// 3. A d�nt�s form�ja -> KCS: DONTES_FORMAJA
		/// hat�rozat (1)
		/// egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat (2)
		/// hat�s�gi szerz�d�s (3)
		/// a Ket. 30. � alapj�n t�rt�n� elutas�t�s (4)
		/// a Ket. 31. � alapj�n t�rt�n� megsz�ntet�se (5)
		/// az els�fok� elj�r�sban hozott egy�b v�gz�s (6)
		/// 
		/// </summary>
		public String DontesFormaja
		{
			get { return Utility.GetStringFromSqlString ( Typed.DontesFormaja ); }
			set { Typed.DontesFormaja = Utility.SetSqlStringFromString ( value, Typed.DontesFormaja ); }
		}


		/// <summary>
		/// UgyintezesHataridore property
		/// 4. Az �gyint�z�s id�tartama -> KCS: UGYINTEZES_IDOTARTAMA
		/// hat�rid�n bel�l (1)
		/// hat�rid�n t�l (2)
		/// </summary>
		public String UgyintezesHataridore
		{
			get { return Utility.GetStringFromSqlString ( Typed.UgyintezesHataridore ); }
			set { Typed.UgyintezesHataridore = Utility.SetSqlStringFromString ( value, Typed.UgyintezesHataridore ); }
		}


		/// <summary>
		/// HataridoTullepes property
		/// Az �rdemi d�nt�ssel kapcsolatos esetleges hat�rid�-t�ll�p�s id�tartlma napokban (0-999)
		/// </summary>
		public String HataridoTullepes
		{
			get { return Utility.GetStringFromSqlInt32 ( Typed.HataridoTullepes ); }
			set { Typed.HataridoTullepes = Utility.SetSqlInt32FromString ( value, Typed.HataridoTullepes ); }
		}


		/// <summary>
		/// HatosagiEllenorzes property
		/// �rt�k: 0/1
		/// 
		/// K�rj�k felt�ntetni az adott elj�r�st�pus sor�n a t�rgyid�szakban lefolytatott hat�s�gi ellen�rz�sek sz�m�t.
		/// </summary>
		public String HatosagiEllenorzes
		{
			get { return Utility.GetStringFromSqlChars ( Typed.HatosagiEllenorzes ); }
			set { Typed.HatosagiEllenorzes = Utility.SetSqlCharsFromString ( value, Typed.HatosagiEllenorzes ); }
		}


		/// <summary>
		/// MunkaorakSzama property
		/// �rt�k: 3 eg�sz + 1 tizedes
		/// 
		/// Az adott �gazatban az egy �gy elint�z�s�re ford�tott munka�r�k �tlagos sz�m�t kell felt�ntetni (�sszes munka�ra / elj�r�sok sz�ma).
		/// </summary>
		public String MunkaorakSzama
		{
			get { return Utility.GetStringFromSqlDouble ( Typed.MunkaorakSzama ); }
			set { Typed.MunkaorakSzama = Utility.SetSqlDoubleFromString ( value, Typed.MunkaorakSzama ); }
		}


		/// <summary>
		/// EljarasiKoltseg property
		/// �rt�k: 6 jegy� eg�sz sz�m.
		/// 
		/// Az adott �gazatban az egy �gyre jut� �tlagos elj�r�si k�lts�get k�rj�k felt�ntetni (�sszes elj�r�si k�lts�g / elj�r�sok sz�ma).
		/// Elj�r�si k�lts�gen ebben a tekintetben a hat�s�g �ltal meg�llap�tott, a Ket. vagy m�s vonatkoz� jogszab�ly szerinti elj�r�si k�lts�get kell �rteni. A hat�s�g m�k�d�si k�lts�gei figyelmen k�v�l hagyand�ak.
		/// </summary>
		public String EljarasiKoltseg
		{
			get { return Utility.GetStringFromSqlInt32 ( Typed.EljarasiKoltseg ); }
			set { Typed.EljarasiKoltseg = Utility.SetSqlInt32FromString ( value, Typed.EljarasiKoltseg ); }
		}


		/// <summary>
		/// KozigazgatasiBirsagMerteke property
		/// 
		/// </summary>
		public String KozigazgatasiBirsagMerteke
		{
			get { return Utility.GetStringFromSqlInt32 ( Typed.KozigazgatasiBirsagMerteke ); }
			set { Typed.KozigazgatasiBirsagMerteke = Utility.SetSqlInt32FromString ( value, Typed.KozigazgatasiBirsagMerteke ); }
		}


		/// <summary>
		/// SommasEljDontes property
		/// 
		/// </summary>
		public String SommasEljDontes
		{
			get { return Utility.GetStringFromSqlString ( Typed.SommasEljDontes ); }
			set { Typed.SommasEljDontes = Utility.SetSqlStringFromString ( value, Typed.SommasEljDontes ); }
		}


		/// <summary>
		/// NyolcNapBelulNemSommas property
		/// 
		/// </summary>
		public String NyolcNapBelulNemSommas
		{
			get { return Utility.GetStringFromSqlString ( Typed.NyolcNapBelulNemSommas ); }
			set { Typed.NyolcNapBelulNemSommas = Utility.SetSqlStringFromString ( value, Typed.NyolcNapBelulNemSommas ); }
		}


		/// <summary>
		/// FuggoHatalyuHatarozat property
		/// 
		/// </summary>
		public String FuggoHatalyuHatarozat
		{
			get { return Utility.GetStringFromSqlString ( Typed.FuggoHatalyuHatarozat ); }
			set { Typed.FuggoHatalyuHatarozat = Utility.SetSqlStringFromString ( value, Typed.FuggoHatalyuHatarozat ); }
		}


		/// <summary>
		/// FuggoHatalyuVegzes property
		/// 
		/// </summary>
		public String FuggoHatalyuVegzes
		{
			get { return Utility.GetStringFromSqlString ( Typed.FuggoHatalyuVegzes ); }
			set { Typed.FuggoHatalyuVegzes = Utility.SetSqlStringFromString ( value, Typed.FuggoHatalyuVegzes ); }
		}


		/// <summary>
		/// HatAltalVisszafizOsszeg property
		/// 
		/// </summary>
		public String HatAltalVisszafizOsszeg
		{
			get { return Utility.GetStringFromSqlInt32 ( Typed.HatAltalVisszafizOsszeg ); }
			set { Typed.HatAltalVisszafizOsszeg = Utility.SetSqlInt32FromString ( value, Typed.HatAltalVisszafizOsszeg ); }
		}


		/// <summary>
		/// HatTerheloEljKtsg property
		/// 
		/// </summary>
		public String HatTerheloEljKtsg
		{
			get { return Utility.GetStringFromSqlInt32 ( Typed.HatTerheloEljKtsg ); }
			set { Typed.HatTerheloEljKtsg = Utility.SetSqlInt32FromString ( value, Typed.HatTerheloEljKtsg ); }
		}


		/// <summary>
		/// FelfuggHatarozat property
		/// 
		/// </summary>
		public String FelfuggHatarozat
		{
			get { return Utility.GetStringFromSqlString ( Typed.FelfuggHatarozat ); }
			set { Typed.FelfuggHatarozat = Utility.SetSqlStringFromString ( value, Typed.FelfuggHatarozat ); }
		}


		/// <summary>
		/// ErvKezd property
		/// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
		/// </summary>
		public String ErvKezd
		{
			get { return Utility.GetStringFromSqlDateTime ( Typed.ErvKezd ); }
			set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString ( value, Typed.ErvKezd ); }
		}


		/// <summary>
		/// ErvVege property
		/// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
		/// </summary>
		public String ErvVege
		{
			get { return Utility.GetStringFromSqlDateTime ( Typed.ErvVege ); }
			set { Typed.ErvVege = Utility.SetSqlDateTimeFromString ( value, Typed.ErvVege ); }
		}

		/// <summary>
		/// AutoExpedialasKell property
		/// 
		/// </summary>
		public String AutoExpedialasKell
		{
			get { return Utility.GetStringFromSqlChars ( Typed.AutoExpedialasKell ); }
			set { Typed.AutoExpedialasKell = Utility.SetSqlCharsFromString ( value, Typed.AutoExpedialasKell ); }
		}

		/// <summary>
		/// UgyintezesAlapja property
		/// KCS: UGYINTEZES_ALAPJA:
		/// 0 - Hagyom�nyos, pap�r
		/// 1 - Elektronikus (nincs pap�r)
		/// </summary>
		public String UgyintezesAlapja
		{
			get { return Utility.GetStringFromSqlString ( Typed.UgyintezesAlapja ); }
			set { Typed.UgyintezesAlapja = Utility.SetSqlStringFromString ( value, Typed.UgyintezesAlapja ); }
		}


		/// <summary>
		/// AdathordozoTipusa property
		/// KCS:UGYINTEZES_ALAPJA
		/// 
		/// (Kor�bban: ADATHORDOZO_TIPUSA)
		/// 
		/// </summary>
		public String AdathordozoTipusa
		{
			get { return Utility.GetStringFromSqlString ( Typed.AdathordozoTipusa ); }
			set { Typed.AdathordozoTipusa = Utility.SetSqlStringFromString ( value, Typed.AdathordozoTipusa ); }
		}

		/// <summary>
		/// ExpedialasModja property
		/// KCS: EXPEDIALAS_MODJA, nem kell
		/// </summary>
		public String ExpedialasModja
		{
			get { return Utility.GetStringFromSqlString ( Typed.ExpedialasModja ); }
			set { Typed.ExpedialasModja = Utility.SetSqlStringFromString ( value, Typed.ExpedialasModja ); }
		}

	}

}