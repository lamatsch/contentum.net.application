
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TomegesIktatasFolyamat BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_TomegesIktatasFolyamat : BaseEREC_TomegesIktatasFolyamat
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Forras property
        /// Melyik rendszer hozta l�tre a folyamatot
        /// </summary>
        public String Forras
        {
            get { return Utility.GetStringFromSqlString(Typed.Forras); }
            set { Typed.Forras = Utility.SetSqlStringFromString(value, Typed.Forras); }                                            
        }
                   
           
        /// <summary>
        /// Forras_Azonosito property
        /// Az adott sor egyedi azonos�t�ja, amivel egy k�ls� rendszer lek�rdezheti a st�tusz�t
        /// </summary>
        public String Forras_Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Forras_Azonosito); }
            set { Typed.Forras_Azonosito = Utility.SetSqlStringFromString(value, Typed.Forras_Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property
        /// TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS k�dcsoport �rt�kk�szlet�nek egy eleme, default: 2
        /// </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlString(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlStringFromString(value, Typed.Prioritas); }                                            
        }
                   
           
        /// <summary>
        /// RQ_Dokumentum_Id property
        /// A t�meges iktat�st kezdem�nyez� nyers adat dokumentum_id-ja
        /// </summary>
        public String RQ_Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.RQ_Dokumentum_Id); }
            set { Typed.RQ_Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.RQ_Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// RS_Dokumentum_Id property
        /// A t�meges iktat�s kezdem�nyez�sre adott v�lasz dokumentum_id-ja
        /// </summary>
        public String RS_Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.RS_Dokumentum_Id); }
            set { Typed.RS_Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.RS_Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT: inicializ�l�s folyamatban, inicializ�lt, v�grehajt�s folyamatban, v�grehajtott.
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}