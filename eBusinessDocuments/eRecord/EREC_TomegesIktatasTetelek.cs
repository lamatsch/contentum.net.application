
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_TomegesIktatasTetelek BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_TomegesIktatasTetelek : BaseEREC_TomegesIktatasTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Folyamat_Id property
        /// 
        /// </summary>
        public String Folyamat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Folyamat_Id); }
            set { Typed.Folyamat_Id = Utility.SetSqlGuidFromString(value, Typed.Folyamat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Forras_Azonosito property
        /// Egy adott t�tel forr�s oldali azonos�t�ja, amivel lek�rdezhet� az adott t�tel st�tusza
        /// </summary>
        public String Forras_Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Forras_Azonosito); }
            set { Typed.Forras_Azonosito = Utility.SetSqlStringFromString(value, Typed.Forras_Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// IktatasTipus property
        /// bej�v� = 0 vagy bels� = 1
        /// </summary>
        public String IktatasTipus
        {
            get { return Utility.GetStringFromSqlInt32(Typed.IktatasTipus); }
            set { Typed.IktatasTipus = Utility.SetSqlInt32FromString(value, Typed.IktatasTipus); }                                            
        }
                   
           
        /// <summary>
        /// Alszamra property
        /// norm�l = 0 vagy alsz�mra = 1
        /// </summary>
        public String Alszamra
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Alszamra); }
            set { Typed.Alszamra = Utility.SetSqlInt32FromString(value, Typed.Alszamra); }                                            
        }
                   
           
        /// <summary>
        /// Iktatokonyv_Id property
        /// FK az EREC_IraIktatokonyvek.Id mez�re
        /// </summary>
        public String Iktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Iktatokonyv_Id); }
            set { Typed.Iktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.Iktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// Ugyirat_Id property
        /// FK az EREC_UgyUgyratok.Id mez�re (ez csak alsz�mos iktat�sn�l van kit�ltve)
        /// </summary>
        public String Ugyirat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Ugyirat_Id); }
            set { Typed.Ugyirat_Id = Utility.SetSqlGuidFromString(value, Typed.Ugyirat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Kuldemeny_Id property
        /// FK az EREC_KuldKuldemenyek.Id mez�re (ez csak bej�v� irat iktat�sn�l van kit�ltve, amikor a Contentum-ban azonos�tott k�ldem�nyt iktatunk)
        /// </summary>
        public String Kuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kuldemeny_Id); }
            set { Typed.Kuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.Kuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// ExecParam property
        /// A futtat�shoz sz�ks�ges ExecParam oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String ExecParam
        {
            get { return Utility.GetStringFromSqlString(Typed.ExecParam); }
            set { Typed.ExecParam = Utility.SetSqlStringFromString(value, Typed.ExecParam); }                                            
        }
                   
           
        /// <summary>
        /// EREC_UgyUgyiratok property
        /// A futtat�shoz sz�ks�ges EREC_UgyUgyiratok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_UgyUgyiratok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_UgyUgyiratok); }
            set { Typed.EREC_UgyUgyiratok = Utility.SetSqlStringFromString(value, Typed.EREC_UgyUgyiratok); }                                            
        }
                   
           
        /// <summary>
        /// EREC_UgyUgyiratdarabok property
        /// A futtat�shoz sz�ks�ges EREC_UgyUgyiratdarabok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_UgyUgyiratdarabok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_UgyUgyiratdarabok); }
            set { Typed.EREC_UgyUgyiratdarabok = Utility.SetSqlStringFromString(value, Typed.EREC_UgyUgyiratdarabok); }                                            
        }
                   
           
        /// <summary>
        /// EREC_IraIratok property
        /// A futtat�shoz sz�ks�ges EREC_IraIratok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_IraIratok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_IraIratok); }
            set { Typed.EREC_IraIratok = Utility.SetSqlStringFromString(value, Typed.EREC_IraIratok); }                                            
        }
                   
           
        /// <summary>
        /// EREC_PldIratPeldanyok property
        /// A futtat�shoz sz�ks�ges EREC_PldIratPeldanyok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_PldIratPeldanyok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_PldIratPeldanyok); }
            set { Typed.EREC_PldIratPeldanyok = Utility.SetSqlStringFromString(value, Typed.EREC_PldIratPeldanyok); }                                            
        }
                   
           
        /// <summary>
        /// EREC_HataridosFeladatok property
        /// A futtat�shoz sz�ks�ges EREC_HataridosFeladatok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_HataridosFeladatok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_HataridosFeladatok); }
            set { Typed.EREC_HataridosFeladatok = Utility.SetSqlStringFromString(value, Typed.EREC_HataridosFeladatok); }                                            
        }
                   
           
        /// <summary>
        /// EREC_KuldKuldemenyek property
        /// A futtat�shoz sz�ks�ges EREC_KuldKuldemenyek oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String EREC_KuldKuldemenyek
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_KuldKuldemenyek); }
            set { Typed.EREC_KuldKuldemenyek = Utility.SetSqlStringFromString(value, Typed.EREC_KuldKuldemenyek); }                                            
        }
                   
           
        /// <summary>
        /// IktatasiParameterek property
        /// A futtat�shoz sz�ks�ges IktatasiParameterek oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String IktatasiParameterek
        {
            get { return Utility.GetStringFromSqlString(Typed.IktatasiParameterek); }
            set { Typed.IktatasiParameterek = Utility.SetSqlStringFromString(value, Typed.IktatasiParameterek); }                                            
        }
                   
           
        /// <summary>
        /// ErkeztetesParameterek property
        /// A futtat�shoz sz�ks�ges ErkeztetesParametere oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        public String ErkeztetesParameterek
        {
            get { return Utility.GetStringFromSqlString(Typed.ErkeztetesParameterek); }
            set { Typed.ErkeztetesParameterek = Utility.SetSqlStringFromString(value, Typed.ErkeztetesParameterek); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentumok property
        /// A csatoland� dokumentumok neve �s el�rhet�s�ge (UNC filepath, vagy URL)
        /// </summary>
        public String Dokumentumok
        {
            get { return Utility.GetStringFromSqlString(Typed.Dokumentumok); }
            set { Typed.Dokumentumok = Utility.SetSqlStringFromString(value, Typed.Dokumentumok); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// TOMEGES_IKTATAS_TETEL_ALLAPOT
        /// - 1 - inicializ�lt (iktat�sz�m meghat�rozott, de m�g nincs iktatva)
        /// - 2 - iktatott (sikerese befejez�d�tt az iktat�s)
        /// - 9 - hib�s (az iktat�s hib�ra futott, jav�tani kell az adatokat)
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Irat_Id property
        /// FK az EREC_IraIratok.Id mez�re, az adott sorhoz tartoz� irat azonos�t�ja
        /// </summary>
        public String Irat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Irat_Id); }
            set { Typed.Irat_Id = Utility.SetSqlGuidFromString(value, Typed.Irat_Id); }                                            
        }
                   
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        public String Hiba
        {
            get { return Utility.GetStringFromSqlString(Typed.Hiba); }
            set { Typed.Hiba = Utility.SetSqlStringFromString(value, Typed.Hiba); }                                            
        }
                   
           
        /// <summary>
        /// Expedialas property
        ///   0|1 - iktat�s ut�na az iratp�ld�ny expedi�land�
        /// </summary>
        public String Expedialas
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Expedialas); }
            set { Typed.Expedialas = Utility.SetSqlInt32FromString(value, Typed.Expedialas); }                                            
        }
                   
           
        /// <summary>
        /// TobbIratEgyKuldemenybe property
        ///  0|1 - expedi�l�skor az azonos t�meges iktat�si folyamatban l�v�, azonos c�mzettnek, azonos c�mre k�ldend� iratp�ld�nnyal egy�tt tehet� egy k�ldem�nybe
        /// </summary>
        public String TobbIratEgyKuldemenybe
        {
            get { return Utility.GetStringFromSqlInt32(Typed.TobbIratEgyKuldemenybe); }
            set { Typed.TobbIratEgyKuldemenybe = Utility.SetSqlInt32FromString(value, Typed.TobbIratEgyKuldemenybe); }                                            
        }
                   
           
        /// <summary>
        /// KuldemenyAtadas property
        /// expedi�l�s ut�n a k�ldem�ny melyik "csoport"-nak ker�lj�n �tad�sra (�tad�sra kijel�l, �tad)
        /// </summary>
        public String KuldemenyAtadas
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldemenyAtadas); }
            set { Typed.KuldemenyAtadas = Utility.SetSqlGuidFromString(value, Typed.KuldemenyAtadas); }                                            
        }
                   
           
        /// <summary>
        /// HatosagiStatAdat property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        public String HatosagiStatAdat
        {
            get { return Utility.GetStringFromSqlString(Typed.HatosagiStatAdat); }
            set { Typed.HatosagiStatAdat = Utility.SetSqlStringFromString(value, Typed.HatosagiStatAdat); }                                            
        }
                   
           
        /// <summary>
        /// EREC_IratAlairok property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        public String EREC_IratAlairok
        {
            get { return Utility.GetStringFromSqlString(Typed.EREC_IratAlairok); }
            set { Typed.EREC_IratAlairok = Utility.SetSqlStringFromString(value, Typed.EREC_IratAlairok); }                                            
        }
                   
           
        /// <summary>
        /// Lezarhato property
        ///  0|1 - expedi�l�skor az azonos t�meges iktat�si folyamatban l�vo, azonos c�mzettnek, azonos c�mre k�ldendo iratp�ld�nnyal egy�tt teheto egy k�ldem�nybe
        /// </summary>
        public String Lezarhato
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Lezarhato); }
            set { Typed.Lezarhato = Utility.SetSqlInt32FromString(value, Typed.Lezarhato); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}