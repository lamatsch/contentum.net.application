
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_UgyUgyiratdarabok BusinessDocument Class
    /// �gyirat darabok 
    ///</summary>
    [Serializable()]
    public class EREC_UgyUgyiratdarabok : BaseEREC_UgyUgyiratdarabok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property 
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyirat_Id property 
        /// �gyirat Id
        /// </summary>
        public String UgyUgyirat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id); }
            set { Typed.UgyUgyirat_Id = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id); }                                            
        }
                   
           
        /// <summary>
        /// EljarasiSzakasz property 
        /// KCS: ELJARASI_SZAKASZ
            
        /// </summary>
        public String EljarasiSzakasz
        {
            get { return Utility.GetStringFromSqlString(Typed.EljarasiSzakasz); }
            set { Typed.EljarasiSzakasz = Utility.SetSqlStringFromString(value, Typed.EljarasiSzakasz); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property 
        /// �gyiraton bel�li sorsz�m a megjelen�t�shez
        /// </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property 
        /// �gyiratdarab azonos�t�: tetsz�leges sz�veg, itt mappa nev�re haszn�ljuk.
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Hatarido property 
        /// Int�z�si hat�rid�
        /// </summary>
        public String Hatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Hatarido); }
            set { Typed.Hatarido = Utility.SetSqlDateTimeFromString(value, Typed.Hatarido); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez property 
        /// �gyint�z� Id
        /// </summary>
        public String FelhasznaloCsoport_Id_Ugyintez
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Ugyintez); }
            set { Typed.FelhasznaloCsoport_Id_Ugyintez = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Ugyintez); }                                            
        }
                   
           
        /// <summary>
        /// ElintezesDat property 
        /// Elint�z�s d�tuma
        /// </summary>
        public String ElintezesDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ElintezesDat); }
            set { Typed.ElintezesDat = Utility.SetSqlDateTimeFromString(value, Typed.ElintezesDat); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDat property 
        /// Lez�r�s d�tuma
        /// </summary>
        public String LezarasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDat); }
            set { Typed.LezarasDat = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDat); }                                            
        }
                   
           
        /// <summary>
        /// ElintezesMod property 
        /// KCS:??
        /// </summary>
        public String ElintezesMod
        {
            get { return Utility.GetStringFromSqlString(Typed.ElintezesMod); }
            set { Typed.ElintezesMod = Utility.SetSqlStringFromString(value, Typed.ElintezesMod); }                                            
        }
                   
           
        /// <summary>
        /// LezarasOka property 
        /// KCS:??
        /// </summary>
        public String LezarasOka
        {
            get { return Utility.GetStringFromSqlString(Typed.LezarasOka); }
            set { Typed.LezarasOka = Utility.SetSqlStringFromString(value, Typed.LezarasOka); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property 
        /// Le�r�s, megnevez�s
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyirat_Id_Elozo property 
        /// Annak az ugyiratnak ID-je, amelybe az ugyiratdarab beleszuletetett. Nem valtozik.
        /// </summary>
        public String UgyUgyirat_Id_Elozo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id_Elozo); }
            set { Typed.UgyUgyirat_Id_Elozo = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id_Elozo); }                                            
        }
                   
           
        /// <summary>
        /// IraIktatokonyv_Id property 
        /// IraIktatokonyv Id
        /// </summary>
        public String IraIktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIktatokonyv_Id); }
            set { Typed.IraIktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.IraIktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// Foszam property 
        /// Iktat�si f�sz�m
        /// </summary>
        public String Foszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Foszam); }
            set { Typed.Foszam = Utility.SetSqlInt32FromString(value, Typed.Foszam); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoAlszam property 
        /// Utols� iktat�si alsz�m
        /// </summary>
        public String UtolsoAlszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoAlszam); }
            set { Typed.UtolsoAlszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoAlszam); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property 
        /// Felel�s felhaszn�l� Id
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo property 
        /// Elozo felelos CSOPORT Id
        /// </summary>
        public String Csoport_Id_Felelos_Elozo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos_Elozo); }
            set { Typed.Csoport_Id_Felelos_Elozo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos_Elozo); }                                            
        }
                         
           
        /// <summary>
        /// Allapot property 
        /// KCS: UGYIRATDARAB_ALLAPOT
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property 
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property 
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}