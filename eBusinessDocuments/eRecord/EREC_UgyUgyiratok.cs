
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_UgyUgyiratok BusinessDocument Class
    /// �gyirat entit�s
    /// 
    /// --�j index: IX_Ugyinteto_Id
    /// --�j index: IX_Foszam
    /// --M�d. index: TEIV_PRT_ID_ORZO_I
    /// --M�d. index: ALLAPOT_I
    /// -- �j oszlop(ok):  add AA 2015.11.03
    ///    UjOrzesiIdo
    /// -- UjOrzesiIdoIdoegyseg (2018.09.20)
    /// 
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_UgyUgyiratok : BaseEREC_UgyUgyiratok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Foszam property
        /// Iktat�si fosz�m
        /// </summary>
        public String Foszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Foszam); }
            set { Typed.Foszam = Utility.SetSqlInt32FromString(value, Typed.Foszam); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property
        /// Munka�gy iktat�si sorsz�ma.
        /// </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Ugyazonosito  property
        /// �gyazonos�t�, k�lso rendszerbol
        /// </summary>
        public String Ugyazonosito 
        {
            get { return Utility.GetStringFromSqlString(Typed.Ugyazonosito ); }
            set { Typed.Ugyazonosito  = Utility.SetSqlStringFromString(value, Typed.Ugyazonosito ); }                                            
        }
                   
           
        /// <summary>
        /// Alkalmazas_Id property
        /// Alkalmazas_Id, k�lso rendszer Id-je
        /// </summary>
        public String Alkalmazas_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Alkalmazas_Id); }
            set { Typed.Alkalmazas_Id = Utility.SetSqlGuidFromString(value, Typed.Alkalmazas_Id); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesModja property
        /// KCS: UGYINTEZES_ALAPJA
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// </summary>
        public String UgyintezesModja
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyintezesModja); }
            set { Typed.UgyintezesModja = Utility.SetSqlStringFromString(value, Typed.UgyintezesModja); }                                            
        }
                   
           
        /// <summary>
        /// Hatarido property
        /// Elint�z�si hat�rido
        /// </summary>
        public String Hatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Hatarido); }
            set { Typed.Hatarido = Utility.SetSqlDateTimeFromString(value, Typed.Hatarido); }                                            
        }
                   
           
        /// <summary>
        /// SkontrobaDat property
        /// Skont�ba t�tel d�tuma
        /// </summary>
        public String SkontrobaDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SkontrobaDat); }
            set { Typed.SkontrobaDat = Utility.SetSqlDateTimeFromString(value, Typed.SkontrobaDat); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDat property
        /// Lez�r�s d�tuma
        /// </summary>
        public String LezarasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDat); }
            set { Typed.LezarasDat = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDat); }                                            
        }
                   
           
        /// <summary>
        /// IrattarbaKuldDatuma  property
        /// Iratt�rba k�ld�s d�tuma 
        /// </summary>
        public String IrattarbaKuldDatuma 
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IrattarbaKuldDatuma ); }
            set { Typed.IrattarbaKuldDatuma  = Utility.SetSqlDateTimeFromString(value, Typed.IrattarbaKuldDatuma ); }                                            
        }
                   
           
        /// <summary>
        /// IrattarbaVetelDat property
        /// Iratt�rba v�tel d�tuma
        /// </summary>
        public String IrattarbaVetelDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IrattarbaVetelDat); }
            set { Typed.IrattarbaVetelDat = Utility.SetSqlDateTimeFromString(value, Typed.IrattarbaVetelDat); }                                            
        }
                   
           
        /// <summary>
        /// FelhCsoport_Id_IrattariAtvevo property
        /// Iratt�ri �tvevo Id-je
        /// </summary>
        public String FelhCsoport_Id_IrattariAtvevo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhCsoport_Id_IrattariAtvevo); }
            set { Typed.FelhCsoport_Id_IrattariAtvevo = Utility.SetSqlGuidFromString(value, Typed.FelhCsoport_Id_IrattariAtvevo); }                                            
        }
                   
           
        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        public String SelejtezesDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SelejtezesDat); }
            set { Typed.SelejtezesDat = Utility.SetSqlDateTimeFromString(value, Typed.SelejtezesDat); }                                            
        }
                   
           
        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtezo v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        public String FelhCsoport_Id_Selejtezo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhCsoport_Id_Selejtezo); }
            set { Typed.FelhCsoport_Id_Selejtezo = Utility.SetSqlGuidFromString(value, Typed.FelhCsoport_Id_Selejtezo); }                                            
        }
                   
           
        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvevo neve
        /// </summary>
        public String LeveltariAtvevoNeve
        {
            get { return Utility.GetStringFromSqlString(Typed.LeveltariAtvevoNeve); }
            set { Typed.LeveltariAtvevoNeve = Utility.SetSqlStringFromString(value, Typed.LeveltariAtvevoNeve); }                                            
        }
                   
           
        /// <summary>
        /// FelhCsoport_Id_Felulvizsgalo property
        /// Fel�lvizsg�l� Id-je
        /// </summary>
        public String FelhCsoport_Id_Felulvizsgalo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhCsoport_Id_Felulvizsgalo); }
            set { Typed.FelhCsoport_Id_Felulvizsgalo = Utility.SetSqlGuidFromString(value, Typed.FelhCsoport_Id_Felulvizsgalo); }                                            
        }
                   
           
        /// <summary>
        /// FelulvizsgalatDat property
        /// Fel�lvizsg�lat d�tuma
        /// </summary>
        public String FelulvizsgalatDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FelulvizsgalatDat); }
            set { Typed.FelulvizsgalatDat = Utility.SetSqlDateTimeFromString(value, Typed.FelulvizsgalatDat); }                                            
        }
                   
           
        /// <summary>
        /// IktatoszamKieg property
        /// ?? Az iktat�sz�m kieg�sz�to r�sze
        /// </summary>
        public String IktatoszamKieg
        {
            get { return Utility.GetStringFromSqlString(Typed.IktatoszamKieg); }
            set { Typed.IktatoszamKieg = Utility.SetSqlStringFromString(value, Typed.IktatoszamKieg); }                                            
        }
                   
           
        /// <summary>
        /// Targy property
        /// Ugyirat t�rgya (le�r�sa)
        /// </summary>
        public String Targy
        {
            get { return Utility.GetStringFromSqlString(Typed.Targy); }
            set { Typed.Targy = Utility.SetSqlStringFromString(value, Typed.Targy); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyirat_Id_Szulo property
        /// Szerelt �gyirat eset�n a befogad� (amibe a szerel�s t�rt�nt) �gyirat Id-je.
        /// </summary>
        public String UgyUgyirat_Id_Szulo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id_Szulo); }
            set { Typed.UgyUgyirat_Id_Szulo = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id_Szulo); }                                            
        }
                   
           
        /// <summary>
        /// UgyUgyirat_Id_Kulso property
        /// A szerelt �gyiratok eset�ben a legf�lsobb szintu �gyirat azonos�t�ja. A nem szereltekn�l NULL.
        /// </summary>
        public String UgyUgyirat_Id_Kulso
        {
            get { return Utility.GetStringFromSqlGuid(Typed.UgyUgyirat_Id_Kulso); }
            set { Typed.UgyUgyirat_Id_Kulso = Utility.SetSqlGuidFromString(value, Typed.UgyUgyirat_Id_Kulso); }                                            
        }
                   
           
        /// <summary>
        /// UgyTipus property
        /// Az IratMetaDefinici� szerinti besorol�s.
        /// </summary>
        public String UgyTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.UgyTipus); }
            set { Typed.UgyTipus = Utility.SetSqlStringFromString(value, Typed.UgyTipus); }                                            
        }
                   
           
        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        public String IrattariHely
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariHely); }
            set { Typed.IrattariHely = Utility.SetSqlStringFromString(value, Typed.IrattariHely); }                                            
        }
                   
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        public String SztornirozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornirozasDat); }
            set { Typed.SztornirozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornirozasDat); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Helyette: a folyamatban o a k�v.felhaszn�l�, akinek ezzel az �ggyel kolga van. A fel�leten: kezelo.
        /// -- Tulajdonos... (iratt�rba ker�l�sn�l az iratt�rosra ker�l) -- 
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// �gyiratot orzo felhaszn�l� (akin�l van), fel�leten: az �gyirat helye.
        /// </summary>
        public String FelhasznaloCsoport_Id_Orzo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Orzo); }
            set { Typed.FelhasznaloCsoport_Id_Orzo = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Orzo); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Cimzett property
        /// A j�v�hagy�st ig�nyl� folyamat c�mzettje
        /// </summary>
        public String Csoport_Id_Cimzett
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Cimzett); }
            set { Typed.Csoport_Id_Cimzett = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Cimzett); }                                            
        }
                   
           
        /// <summary>
        /// Jelleg property
        /// KCS: UGY_JELLEG
        /// </summary>
        public String Jelleg
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelleg); }
            set { Typed.Jelleg = Utility.SetSqlStringFromString(value, Typed.Jelleg); }                                            
        }
                   
           
        /// <summary>
        /// IraIrattariTetel_Id property
        /// Iratt�ri t�tel Id 
        /// </summary>
        public String IraIrattariTetel_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrattariTetel_Id); }
            set { Typed.IraIrattariTetel_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrattariTetel_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        public String IraIktatokonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIktatokonyv_Id); }
            set { Typed.IraIktatokonyv_Id = Utility.SetSqlGuidFromString(value, Typed.IraIktatokonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// SkontroOka property
        /// A skontr�ba helyez�s ok le�r�sa
        /// </summary>
        public String SkontroOka
        {
            get { return Utility.GetStringFromSqlString(Typed.SkontroOka); }
            set { Typed.SkontroOka = Utility.SetSqlStringFromString(value, Typed.SkontroOka); }                                            
        }
                   
           
        /// <summary>
        /// SkontroVege property
        /// Skontr� v�ge d�tuma
        /// </summary>
        public String SkontroVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SkontroVege); }
            set { Typed.SkontroVege = Utility.SetSqlDateTimeFromString(value, Typed.SkontroVege); }                                            
        }
                   
           
        /// <summary>
        /// Surgosseg property
        /// KCS: SURGOSSEG
        /// </summary>
        public String Surgosseg
        {
            get { return Utility.GetStringFromSqlString(Typed.Surgosseg); }
            set { Typed.Surgosseg = Utility.SetSqlStringFromString(value, Typed.Surgosseg); }                                            
        }
                   
           
        /// <summary>
        /// SkontrobanOsszesen property
        /// Skont�ban t�lt�tt napok sz�ma
        /// </summary>
        public String SkontrobanOsszesen
        {
            get { return Utility.GetStringFromSqlInt32(Typed.SkontrobanOsszesen); }
            set { Typed.SkontrobanOsszesen = Utility.SetSqlInt32FromString(value, Typed.SkontrobanOsszesen); }                                            
        }
                   
           
        /// <summary>
        /// MegorzesiIdoVege property
        /// �gyirat megorz�se eddig a d�tumig
        /// </summary>
        public String MegorzesiIdoVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegorzesiIdoVege); }
            set { Typed.MegorzesiIdoVege = Utility.SetSqlDateTimeFromString(value, Typed.MegorzesiIdoVege); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id_Ugyindito property
        /// �gyind�t� partner (�gyf�l) Id-je
        /// </summary>
        public String Partner_Id_Ugyindito
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id_Ugyindito); }
            set { Typed.Partner_Id_Ugyindito = Utility.SetSqlGuidFromString(value, Typed.Partner_Id_Ugyindito); }                                            
        }
                   
           
        /// <summary>
        /// NevSTR_Ugyindito property
        /// Az �gyind�t�/bek�ldo neve
        /// </summary>
        public String NevSTR_Ugyindito
        {
            get { return Utility.GetStringFromSqlString(Typed.NevSTR_Ugyindito); }
            set { Typed.NevSTR_Ugyindito = Utility.SetSqlStringFromString(value, Typed.NevSTR_Ugyindito); }                                            
        }
                   
           
        /// <summary>
        /// ElintezesDat property
        /// Ugyirat elintezesenek datuma.
        /// </summary>
        public String ElintezesDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ElintezesDat); }
            set { Typed.ElintezesDat = Utility.SetSqlDateTimeFromString(value, Typed.ElintezesDat); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez property
        /// Az �gyet fel�gyeli, az �rdemi munk�t �sszefogja. Fel�leten: �gyint�zo. T�vlatilag v�gig viszi az �gyet.
        /// Ha m�r iratt�rba ker�lt, itt l�that�, ki volt az �gyint�zo...
        /// </summary>
        public String FelhasznaloCsoport_Id_Ugyintez
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Ugyintez); }
            set { Typed.FelhasznaloCsoport_Id_Ugyintez = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Ugyintez); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id. Szignalas - tovabbitas visszavonasakor tudni kell, ki volt elozoleg a felelos.
        /// </summary>
        public String Csoport_Id_Felelos_Elozo
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos_Elozo); }
            set { Typed.Csoport_Id_Felelos_Elozo = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos_Elozo); }                                            
        }
                   
           
        /// <summary>
        /// KolcsonKikerDat property
        /// �gyirat kik�r�si d�tuma
        /// </summary>
        public String KolcsonKikerDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KolcsonKikerDat); }
            set { Typed.KolcsonKikerDat = Utility.SetSqlDateTimeFromString(value, Typed.KolcsonKikerDat); }                                            
        }
                   
           
        /// <summary>
        /// KolcsonKiadDat property
        /// �gyirat (k�lcs�nz�sre) kiad�si d�tuma 
        /// </summary>
        public String KolcsonKiadDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KolcsonKiadDat); }
            set { Typed.KolcsonKiadDat = Utility.SetSqlDateTimeFromString(value, Typed.KolcsonKiadDat); }                                            
        }
                   
           
        /// <summary>
        /// Kolcsonhatarido property
        /// K�lcs�nz�si hat�rido
        /// </summary>
        public String Kolcsonhatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Kolcsonhatarido); }
            set { Typed.Kolcsonhatarido = Utility.SetSqlDateTimeFromString(value, Typed.Kolcsonhatarido); }                                            
        }
                   
           
        /// <summary>
        /// BARCODE property
        /// Vonalk�d �rt�ke
        /// </summary>
        public String BARCODE
        {
            get { return Utility.GetStringFromSqlString(Typed.BARCODE); }
            set { Typed.BARCODE = Utility.SetSqlStringFromString(value, Typed.BARCODE); }                                            
        }
                   
           
        /// <summary>
        /// IratMetadefinicio_Id property
        /// Id
        /// </summary>
        public String IratMetadefinicio_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IratMetadefinicio_Id); }
            set { Typed.IratMetadefinicio_Id = Utility.SetSqlGuidFromString(value, Typed.IratMetadefinicio_Id); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// 0 - Megnyitott (munka�gy)
        /// 2 - Befagyasztott (munka�gy)
        /// 03	Szign�lt
        /// 04	Iktatott
        /// 06	�gyint�z�s alatt
        /// 07	Skontr�ban
        /// 09	Lez�rt
        /// 10	Iratt�rban orz�tt
        /// 11	Iratt�rba k�ld�tt (�ton l�vo)
        /// 13	K�lcs�nz�tt (k�zponti iratt�r)
        /// 30	Jegyz�kre helyezett
        /// 31	Lez�rt jegyz�kben l�vo
        /// 32	Selejtezett
        /// 33	Lev�lt�rba adott
        /// 50	Tov�bb�t�s alatt (�ton l�vo)
        /// 52	Iratt�roz�sra j�v�hagy�s alatt
        /// 55	Iratt�rb�l elk�rt (�ton l�vo)
        /// 56	Enged�lyezett kik�ron l�vo
        /// 57	Skontr�b�l elk�rt
        /// 60	Szerelt
        /// 70	Skontr�ba helyez�s j�v�hagy�sa
        /// 71	Skontr�b�l kik�rt
        /// 90	Sztorn�zott
        /// 99	Elint�zett
        /// 
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// </summary>
        public String TovabbitasAlattAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.TovabbitasAlattAllapot); }
            set { Typed.TovabbitasAlattAllapot = Utility.SetSqlStringFromString(value, Typed.TovabbitasAlattAllapot); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// �gyirat azonos�t�: fosz�m, �v 
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        public String Fizikai_Kezbesitesi_Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Fizikai_Kezbesitesi_Allapot); }
            set { Typed.Fizikai_Kezbesitesi_Allapot = Utility.SetSqlStringFromString(value, Typed.Fizikai_Kezbesitesi_Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Orzo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Orzo_Id); }
            set { Typed.Kovetkezo_Orzo_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Orzo_Id); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Ugyfelelos property
        /// Fel�leten: �gyfelelos. 
        /// Az �gy jogi felelose, rendszerint az �gyoszt�ly felelos vezetoje. Hosszabb t�von �lland� marad.
        /// </summary>
        public String Csoport_Id_Ugyfelelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Ugyfelelos); }
            set { Typed.Csoport_Id_Ugyfelelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Ugyfelelos); }                                            
        }
                   
           
        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        public String Elektronikus_Kezbesitesi_Allap
        {
            get { return Utility.GetStringFromSqlString(Typed.Elektronikus_Kezbesitesi_Allap); }
            set { Typed.Elektronikus_Kezbesitesi_Allap = Utility.SetSqlStringFromString(value, Typed.Elektronikus_Kezbesitesi_Allap); }                                            
        }
                   
           
        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        public String Kovetkezo_Felelos_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Kovetkezo_Felelos_Id); }
            set { Typed.Kovetkezo_Felelos_Id = Utility.SetSqlGuidFromString(value, Typed.Kovetkezo_Felelos_Id); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoAlszam property
        /// Utols� iktat�si alsz�m
        /// </summary>
        public String UtolsoAlszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoAlszam); }
            set { Typed.UtolsoAlszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoAlszam); }                                            
        }
                   
           
        /// <summary>
        /// UtolsoSorszam property
        /// A �gyiratba (vagy munk�gyiratba) val� munkairat iktat�s utols� sorsz�ma.
        /// </summary>
        public String UtolsoSorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UtolsoSorszam); }
            set { Typed.UtolsoSorszam = Utility.SetSqlInt32FromString(value, Typed.UtolsoSorszam); }                                            
        }
                   
           
        /// <summary>
        /// IratSzam property
        /// Az �gyirathoz tartot� iratok aktu�lis sz�ma (az �tiktattott �s sztorn�zott iratok sz�m�val kevesebb, mint az utols� alsz�m).
        /// </summary>
        public String IratSzam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.IratSzam); }
            set { Typed.IratSzam = Utility.SetSqlInt32FromString(value, Typed.IratSzam); }                                            
        }
                   
           
        /// <summary>
        /// ElintezesMod property
        /// KCS: ELINTEZESMOD
        /// </summary>
        public String ElintezesMod
        {
            get { return Utility.GetStringFromSqlString(Typed.ElintezesMod); }
            set { Typed.ElintezesMod = Utility.SetSqlStringFromString(value, Typed.ElintezesMod); }                                            
        }
                   
           
        /// <summary>
        /// RegirendszerIktatoszam property
        /// A r�gi iktat�rendszerbol sz�rmaz� �gyirat sz�veges azonos�t�ja
        /// </summary>
        public String RegirendszerIktatoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.RegirendszerIktatoszam); }
            set { Typed.RegirendszerIktatoszam = Utility.SetSqlStringFromString(value, Typed.RegirendszerIktatoszam); }                                            
        }
                   
           
        /// <summary>
        /// GeneraltTargy property
        /// elore r�gz�tett sz�veg, t�rgy az iratmetadefici�k t�bl�b�l 
        /// </summary>
        public String GeneraltTargy
        {
            get { return Utility.GetStringFromSqlString(Typed.GeneraltTargy); }
            set { Typed.GeneraltTargy = Utility.SetSqlStringFromString(value, Typed.GeneraltTargy); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id_Ugyindito  property
        /// 
        /// </summary>
        public String Cim_Id_Ugyindito 
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id_Ugyindito ); }
            set { Typed.Cim_Id_Ugyindito  = Utility.SetSqlGuidFromString(value, Typed.Cim_Id_Ugyindito ); }                                            
        }
                   
           
        /// <summary>
        /// CimSTR_Ugyindito  property
        /// 
        /// </summary>
        public String CimSTR_Ugyindito 
        {
            get { return Utility.GetStringFromSqlString(Typed.CimSTR_Ugyindito ); }
            set { Typed.CimSTR_Ugyindito  = Utility.SetSqlStringFromString(value, Typed.CimSTR_Ugyindito ); }                                            
        }
                   
           
        /// <summary>
        /// LezarasOka property
        /// KCS: LEZARAS_OKA
        /// </summary>
        public String LezarasOka
        {
            get { return Utility.GetStringFromSqlString(Typed.LezarasOka); }
            set { Typed.LezarasOka = Utility.SetSqlStringFromString(value, Typed.LezarasOka); }                                            
        }
                   
           
        /// <summary>
        /// FelfuggesztesOka property
        /// 
        /// </summary>
        public String FelfuggesztesOka
        {
            get { return Utility.GetStringFromSqlString(Typed.FelfuggesztesOka); }
            set { Typed.FelfuggesztesOka = Utility.SetSqlStringFromString(value, Typed.FelfuggesztesOka); }                                            
        }
                   
           
        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        public String IrattarId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IrattarId); }
            set { Typed.IrattarId = Utility.SetSqlGuidFromString(value, Typed.IrattarId); }                                            
        }
                   
           
        /// <summary>
        /// SkontroOka_Kod property
        /// KCS: SKONTRO_OKA
        /// </summary>
        public String SkontroOka_Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.SkontroOka_Kod); }
            set { Typed.SkontroOka_Kod = Utility.SetSqlStringFromString(value, Typed.SkontroOka_Kod); }                                            
        }
                   
           
        /// <summary>
        /// UjOrzesiIdo property
        /// 
        /// </summary>
        public String UjOrzesiIdo
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UjOrzesiIdo); }
            set { Typed.UjOrzesiIdo = Utility.SetSqlInt32FromString(value, Typed.UjOrzesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// UjOrzesiIdoIdoegyseg property
        /// KCS:IDOEGYSEG
        /// </summary>
        public String UjOrzesiIdoIdoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.UjOrzesiIdoIdoegyseg); }
            set { Typed.UjOrzesiIdoIdoegyseg = Utility.SetSqlStringFromString(value, Typed.UjOrzesiIdoIdoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// UgyintezesKezdete property
        /// 
        /// </summary>
        public String UgyintezesKezdete
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.UgyintezesKezdete); }
            set { Typed.UgyintezesKezdete = Utility.SetSqlDateTimeFromString(value, Typed.UgyintezesKezdete); }                                            
        }
                   
           
        /// <summary>
        /// FelfuggesztettNapokSzama property
        /// 
        /// </summary>
        public String FelfuggesztettNapokSzama
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FelfuggesztettNapokSzama); }
            set { Typed.FelfuggesztettNapokSzama = Utility.SetSqlInt32FromString(value, Typed.FelfuggesztettNapokSzama); }                                            
        }
                   
           
        /// <summary>
        /// IntezesiIdo property
        /// �tfut�si ido a megadott idoegys�gben (�gyirat, elj�r�si szakasz, irat)
        /// </summary>
        public String IntezesiIdo
        {
            get { return Utility.GetStringFromSqlString(Typed.IntezesiIdo); }
            set { Typed.IntezesiIdo = Utility.SetSqlStringFromString(value, Typed.IntezesiIdo); }                                            
        }
                   
           
        /// <summary>
        /// IntezesiIdoegyseg property
        /// Az �tfut�si ido idoegys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az idoegys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        public String IntezesiIdoegyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.IntezesiIdoegyseg); }
            set { Typed.IntezesiIdoegyseg = Utility.SetSqlStringFromString(value, Typed.IntezesiIdoegyseg); }                                            
        }
                   
           
        /// <summary>
        /// Ugy_Fajtaja property
        /// 
        /// </summary>
        public String Ugy_Fajtaja
        {
            get { return Utility.GetStringFromSqlString(Typed.Ugy_Fajtaja); }
            set { Typed.Ugy_Fajtaja = Utility.SetSqlStringFromString(value, Typed.Ugy_Fajtaja); }                                            
        }
                   
           
        /// <summary>
        /// SzignaloId property
        /// 
        /// </summary>
        public String SzignaloId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SzignaloId); }
            set { Typed.SzignaloId = Utility.SetSqlGuidFromString(value, Typed.SzignaloId); }                                            
        }
                   
           
        /// <summary>
        /// SzignalasIdeje property
        /// 
        /// </summary>
        public String SzignalasIdeje
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SzignalasIdeje); }
            set { Typed.SzignalasIdeje = Utility.SetSqlDateTimeFromString(value, Typed.SzignalasIdeje); }                                            
        }
                   
           
        /// <summary>
        /// ElteltIdo property
        /// 
        /// </summary>
        public String ElteltIdo
        {
            get { return Utility.GetStringFromSqlString(Typed.ElteltIdo); }
            set { Typed.ElteltIdo = Utility.SetSqlStringFromString(value, Typed.ElteltIdo); }                                            
        }
                   
           
        /// <summary>
        /// ElteltIdoIdoEgyseg property
        /// 
        /// </summary>
        public String ElteltIdoIdoEgyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.ElteltIdoIdoEgyseg); }
            set { Typed.ElteltIdoIdoEgyseg = Utility.SetSqlStringFromString(value, Typed.ElteltIdoIdoEgyseg); }                                            
        }
                   
           
        /// <summary>
        /// ElteltidoAllapot property
        /// 
        /// </summary>
        public String ElteltidoAllapot
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ElteltidoAllapot); }
            set { Typed.ElteltidoAllapot = Utility.SetSqlInt32FromString(value, Typed.ElteltidoAllapot); }                                            
        }
                   
           
        /// <summary>
        /// ElteltIdoUtolsoModositas property
        /// 
        /// </summary>
        public String ElteltIdoUtolsoModositas
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ElteltIdoUtolsoModositas); }
            set { Typed.ElteltIdoUtolsoModositas = Utility.SetSqlDateTimeFromString(value, Typed.ElteltIdoUtolsoModositas); }                                            
        }
                   
           
        /// <summary>
        /// SakkoraAllapot property
        /// 
        /// </summary>
        public String SakkoraAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.SakkoraAllapot); }
            set { Typed.SakkoraAllapot = Utility.SetSqlStringFromString(value, Typed.SakkoraAllapot); }                                            
        }
                   
           
        /// <summary>
        /// HatralevoNapok property
        /// 
        /// </summary>
        public String HatralevoNapok
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatralevoNapok); }
            set { Typed.HatralevoNapok = Utility.SetSqlInt32FromString(value, Typed.HatralevoNapok); }                                            
        }
                   
           
        /// <summary>
        /// HatralevoMunkaNapok property
        /// 
        /// </summary>
        public String HatralevoMunkaNapok
        {
            get { return Utility.GetStringFromSqlInt32(Typed.HatralevoMunkaNapok); }
            set { Typed.HatralevoMunkaNapok = Utility.SetSqlInt32FromString(value, Typed.HatralevoMunkaNapok); }                                            
        }
                   
           
        /// <summary>
        /// ElozoAllapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// 0 - Megnyitott (munka�gy)
        /// 2 - Befagyasztott (munka�gy)
        /// 03	Szign�lt
        /// 04	Iktatott
        /// 06	�gyint�z�s alatt
        /// 07	Skontr�ban
        /// 09	Lez�rt
        /// 10	Iratt�rban orz�tt
        /// 11	Iratt�rba k�ld�tt (�ton l�vo)
        /// 13	K�lcs�nz�tt (k�zponti iratt�r)
        /// 30	Jegyz�kre helyezett
        /// 31	Lez�rt jegyz�kben l�vo
        /// 32	Selejtezett
        /// 33	Lev�lt�rba adott
        /// 50	Tov�bb�t�s alatt (�ton l�vo)
        /// 52	Iratt�roz�sra j�v�hagy�s alatt
        /// 55	Iratt�rb�l elk�rt (�ton l�vo)
        /// 56	Enged�lyezett kik�ron l�vo
        /// 57	Skontr�b�l elk�rt
        /// 60	Szerelt
        /// 70	Skontr�ba helyez�s j�v�hagy�sa
        /// 71	Skontr�b�l kik�rt
        /// 90	Sztorn�zott
        /// 99	Elint�zett
        /// 
        /// </summary>
        public String ElozoAllapot
        {
            get { return Utility.GetStringFromSqlString(Typed.ElozoAllapot); }
            set { Typed.ElozoAllapot = Utility.SetSqlStringFromString(value, Typed.ElozoAllapot); }                                            
        }


        /// <summary>
        /// IrattarHelyfoglalas property
        /// 
        /// </summary>
        public String IrattarHelyfoglalas
        {
            get { return Utility.GetStringFromSqlDouble(Typed.IrattarHelyfoglalas); }
            set { Typed.IrattarHelyfoglalas = Utility.SetSqlDoubleFromString(value, Typed.IrattarHelyfoglalas); }
        }

        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}