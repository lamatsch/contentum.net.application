
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_UgyiratObjKapcsolatok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_UgyiratObjKapcsolatok : BaseEREC_UgyiratObjKapcsolatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// KapcsolatTipus property </summary>
        public String KapcsolatTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.KapcsolatTipus); }
            set { Typed.KapcsolatTipus = Utility.SetSqlStringFromString(value, Typed.KapcsolatTipus); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Kezi property </summary>
        public String Kezi
        {
            get { return Utility.GetStringFromSqlChars(Typed.Kezi); }
            set { Typed.Kezi = Utility.SetSqlCharsFromString(value, Typed.Kezi); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Elozmeny property </summary>
        public String Obj_Id_Elozmeny
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Elozmeny); }
            set { Typed.Obj_Id_Elozmeny = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Elozmeny); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Tip_Id_Elozmeny property </summary>
        public String Obj_Tip_Id_Elozmeny
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Tip_Id_Elozmeny); }
            set { Typed.Obj_Tip_Id_Elozmeny = Utility.SetSqlGuidFromString(value, Typed.Obj_Tip_Id_Elozmeny); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Type_Elozmeny property </summary>
        public String Obj_Type_Elozmeny
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Type_Elozmeny); }
            set { Typed.Obj_Type_Elozmeny = Utility.SetSqlStringFromString(value, Typed.Obj_Type_Elozmeny); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id_Kapcsolt property </summary>
        public String Obj_Id_Kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id_Kapcsolt); }
            set { Typed.Obj_Id_Kapcsolt = Utility.SetSqlGuidFromString(value, Typed.Obj_Id_Kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Tip_Id_Kapcsolt property </summary>
        public String Obj_Tip_Id_Kapcsolt
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Tip_Id_Kapcsolt); }
            set { Typed.Obj_Tip_Id_Kapcsolt = Utility.SetSqlGuidFromString(value, Typed.Obj_Tip_Id_Kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Type_Kapcsolt property </summary>
        public String Obj_Type_Kapcsolt
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Type_Kapcsolt); }
            set { Typed.Obj_Type_Kapcsolt = Utility.SetSqlStringFromString(value, Typed.Obj_Type_Kapcsolt); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}