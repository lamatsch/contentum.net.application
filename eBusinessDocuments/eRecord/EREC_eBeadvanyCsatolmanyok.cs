
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eBeadvanyCsatolmanyok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_eBeadvanyCsatolmanyok : BaseEREC_eBeadvanyCsatolmanyok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// eBeadvany_Id property
        /// FK EREC_ElektronikusBeadvanyok.Id Elektronikus beadv�ny azonos�t�
        /// </summary>
        public String eBeadvany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.eBeadvany_Id); }
            set { Typed.eBeadvany_Id = Utility.SetSqlGuidFromString(value, Typed.eBeadvany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// FK KRT_Dokumentumok.Id Dokumentum azonos�t�.
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// A f�jl neve.
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}