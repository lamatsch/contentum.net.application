
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eBeadvanyok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public partial class EREC_eBeadvanyok : BaseEREC_eBeadvanyok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Irany property
        /// bej�v�, vagy kimen� �zenet
        /// </summary>
        public String Irany
        {
            get { return Utility.GetStringFromSqlChars(Typed.Irany); }
            set { Typed.Irany = Utility.SetSqlCharsFromString(value, Typed.Irany); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// FK KRT_Kodtarak, ahol a k�dcsoport neve: �eBEADVANY_ALLAPOT� Az Elektronikus beadv�ny �llapota.
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// KuldoRendszer property
        /// Az elektronikus beadv�nyt k�ld� rendszer (pl. �NYK, HivataliKapu, stb.)
        /// </summary>
        public String KuldoRendszer
        {
            get { return Utility.GetStringFromSqlString(Typed.KuldoRendszer); }
            set { Typed.KuldoRendszer = Utility.SetSqlStringFromString(value, Typed.KuldoRendszer); }                                            
        }
                   
           
        /// <summary>
        /// UzenetTipusa property
        /// UZENET_TIPUS k�dcsoport egy �rt�ke
        /// </summary>
        public String UzenetTipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.UzenetTipusa); }
            set { Typed.UzenetTipusa = Utility.SetSqlStringFromString(value, Typed.UzenetTipusa); }                                            
        }
                   
           
        /// <summary>
        /// FeladoTipusa property
        /// Szervezet, szem�ly, stb.
        /// </summary>
        public String FeladoTipusa
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladoTipusa); }
            set { Typed.FeladoTipusa = Utility.SetSqlInt32FromString(value, Typed.FeladoTipusa); }                                            
        }
                   
           
        /// <summary>
        /// PartnerKapcsolatiKod property
        /// �llampolg�r felad� eset�n a kapcsolati k�d.
        /// </summary>
        public String PartnerKapcsolatiKod
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerKapcsolatiKod); }
            set { Typed.PartnerKapcsolatiKod = Utility.SetSqlStringFromString(value, Typed.PartnerKapcsolatiKod); }                                            
        }
                   
           
        /// <summary>
        /// PartnerNev property
        /// �llampolg�r teljes neve, vagy a Hivatal teljes neve.
        /// </summary>
        public String PartnerNev
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerNev); }
            set { Typed.PartnerNev = Utility.SetSqlStringFromString(value, Typed.PartnerNev); }                                            
        }
                   
           
        /// <summary>
        /// PartnerEmail property
        /// �llampolg�r email c�me-
        /// </summary>
        public String PartnerEmail
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerEmail); }
            set { Typed.PartnerEmail = Utility.SetSqlStringFromString(value, Typed.PartnerEmail); }                                            
        }
                   
           
        /// <summary>
        /// PartnerRovidNev property
        /// Hivatal r�vid neve.
        /// </summary>
        public String PartnerRovidNev
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerRovidNev); }
            set { Typed.PartnerRovidNev = Utility.SetSqlStringFromString(value, Typed.PartnerRovidNev); }                                            
        }
                   
           
        /// <summary>
        /// PartnerMAKKod property
        /// Hivatal M�K k�dja.
        /// </summary>
        public String PartnerMAKKod
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerMAKKod); }
            set { Typed.PartnerMAKKod = Utility.SetSqlStringFromString(value, Typed.PartnerMAKKod); }                                            
        }
                   
           
        /// <summary>
        /// PartnerKRID property
        /// Hivatal KapuRendszer azonos�t�ja.
        /// </summary>
        public String PartnerKRID
        {
            get { return Utility.GetStringFromSqlString(Typed.PartnerKRID); }
            set { Typed.PartnerKRID = Utility.SetSqlStringFromString(value, Typed.PartnerKRID); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property
        /// FK KRT_Partnerek
        /// </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// Cim_Id property
        /// FK KRT_Cimek
        /// </summary>
        public String Cim_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Cim_Id); }
            set { Typed.Cim_Id = Utility.SetSqlGuidFromString(value, Typed.Cim_Id); }                                            
        }
                   
           
        /// <summary>
        /// KR_HivatkozasiSzam property
        /// KapuRendszer el�zm�ny azonos�t�ja
        /// </summary>
        public String KR_HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_HivatkozasiSzam); }
            set { Typed.KR_HivatkozasiSzam = Utility.SetSqlStringFromString(value, Typed.KR_HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// KR_ErkeztetesiSzam property
        /// KapuRendszer iktat�sz�ma
        /// </summary>
        public String KR_ErkeztetesiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_ErkeztetesiSzam); }
            set { Typed.KR_ErkeztetesiSzam = Utility.SetSqlStringFromString(value, Typed.KR_ErkeztetesiSzam); }                                            
        }
                   
           
        /// <summary>
        /// Contentum_HivatkozasiSzam property
        /// Contentum-os el�zm�ny azonos�t�ja (�gyirat ID)
        /// </summary>
        public String Contentum_HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Contentum_HivatkozasiSzam); }
            set { Typed.Contentum_HivatkozasiSzam = Utility.SetSqlGuidFromString(value, Typed.Contentum_HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// PR_HivatkozasiSzam property
        /// El�t�t (proxy) rendszer el�zm�ny azonos�t�ja
        /// </summary>
        public String PR_HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.PR_HivatkozasiSzam); }
            set { Typed.PR_HivatkozasiSzam = Utility.SetSqlStringFromString(value, Typed.PR_HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// PR_ErkeztetesiSzam property
        /// El�t�t (proxy) rendszer �rkeztet�si azonos�t�ja
        /// </summary>
        public String PR_ErkeztetesiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.PR_ErkeztetesiSzam); }
            set { Typed.PR_ErkeztetesiSzam = Utility.SetSqlStringFromString(value, Typed.PR_ErkeztetesiSzam); }                                            
        }
                   
           
        /// <summary>
        /// KR_DokTipusHivatal property
        /// KapuRendszer META adat
        /// </summary>
        public String KR_DokTipusHivatal
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_DokTipusHivatal); }
            set { Typed.KR_DokTipusHivatal = Utility.SetSqlStringFromString(value, Typed.KR_DokTipusHivatal); }                                            
        }
                   
           
        /// <summary>
        /// KR_DokTipusAzonosito property
        /// KapuRendszer META adat
        /// </summary>
        public String KR_DokTipusAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_DokTipusAzonosito); }
            set { Typed.KR_DokTipusAzonosito = Utility.SetSqlStringFromString(value, Typed.KR_DokTipusAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// KR_DokTipusLeiras property
        /// KapuRendszer META adat
        /// </summary>
        public String KR_DokTipusLeiras
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_DokTipusLeiras); }
            set { Typed.KR_DokTipusLeiras = Utility.SetSqlStringFromString(value, Typed.KR_DokTipusLeiras); }                                            
        }
                   
           
        /// <summary>
        /// KR_Megjegyzes property
        /// KapuRendszer META adat
        /// </summary>
        public String KR_Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_Megjegyzes); }
            set { Typed.KR_Megjegyzes = Utility.SetSqlStringFromString(value, Typed.KR_Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// KR_ErvenyessegiDatum property
        /// KapuRendszer �ltal az �zenethez r�gz�tett �rv�nyess�gi d�tum.
        /// </summary>
        public String KR_ErvenyessegiDatum
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KR_ErvenyessegiDatum); }
            set { Typed.KR_ErvenyessegiDatum = Utility.SetSqlDateTimeFromString(value, Typed.KR_ErvenyessegiDatum); }                                            
        }
                   
           
        /// <summary>
        /// KR_ErkeztetesiDatum property
        /// Az �zenet KapuRendszer-ben t�rt�n� regisztr�l�s�nak id�pontja.
        /// </summary>
        public String KR_ErkeztetesiDatum
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.KR_ErkeztetesiDatum); }
            set { Typed.KR_ErkeztetesiDatum = Utility.SetSqlDateTimeFromString(value, Typed.KR_ErkeztetesiDatum); }                                            
        }
                   
           
        /// <summary>
        /// KR_FileNev property
        /// KapuRendszer META adat, a csatolm�nyk�nt k�ld�tt f�jl neve
        /// </summary>
        public String KR_FileNev
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_FileNev); }
            set { Typed.KR_FileNev = Utility.SetSqlStringFromString(value, Typed.KR_FileNev); }                                            
        }
                   
           
        /// <summary>
        /// KR_Kezbesitettseg property
        /// Az �zenet KapuRendszer beli k�zbes�tetts�g�nek st�tusza.
        /// </summary>
        public String KR_Kezbesitettseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KR_Kezbesitettseg); }
            set { Typed.KR_Kezbesitettseg = Utility.SetSqlInt32FromString(value, Typed.KR_Kezbesitettseg); }                                            
        }
                   
           
        /// <summary>
        /// KR_Idopecset property
        /// A KapuRendszerben az �zenethez rendelt id�pecs�t.
        /// </summary>
        public String KR_Idopecset
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_Idopecset); }
            set { Typed.KR_Idopecset = Utility.SetSqlStringFromString(value, Typed.KR_Idopecset); }                                            
        }
                   
           
        /// <summary>
        /// KR_Valasztitkositas property
        /// A felad� k�ri-e a v�lasz �zenet titkos�t�s�t. Amennyiben igen, akkor a v�laszt csak a KR rendszerb�l let�lthet� publikus kulccsal titkos�tva lehet elk�ldeni.
        /// </summary>
        public String KR_Valasztitkositas
        {
            get { return Utility.GetStringFromSqlChars(Typed.KR_Valasztitkositas); }
            set { Typed.KR_Valasztitkositas = Utility.SetSqlCharsFromString(value, Typed.KR_Valasztitkositas); }                                            
        }
                   
           
        /// <summary>
        /// KR_Valaszutvonal property
        /// Az �rkeztet�st visszaigazol� v�lasz �zenet �tvonala (0-�rtes�t�si t�rhely, 1-k�zvetlen e-mail). A szervezetnek nem kell a v�lasz�tvonallal foglalkoznia. Az csak az �rkeztet�si visszaigazol�sra vonatkozik, melyet a KR v�gez.
        /// </summary>
        public String KR_Valaszutvonal
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KR_Valaszutvonal, String.Empty); }
            set { Typed.KR_Valaszutvonal = Utility.SetSqlInt32FromString(value, Typed.KR_Valaszutvonal); }                                            
        }
                   
           
        /// <summary>
        /// KR_Rendszeruzenet property
        /// Felhaszn�l� �ltal k�ld�tt dokumentum eset�n �rt�ke hamis (false), olvas�si visszaigazol�s eset�n igaz (true)
        /// </summary>
        public String KR_Rendszeruzenet
        {
            get { return Utility.GetStringFromSqlChars(Typed.KR_Rendszeruzenet); }
            set { Typed.KR_Rendszeruzenet = Utility.SetSqlCharsFromString(value, Typed.KR_Rendszeruzenet); }                                            
        }
                   
           
        /// <summary>
        /// KR_Tarterulet property
        /// �tmeneti vagy tart�s t�rb�l sz�rmazik az �zenet.
        /// </summary>
        public String KR_Tarterulet
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KR_Tarterulet); }
            set { Typed.KR_Tarterulet = Utility.SetSqlInt32FromString(value, Typed.KR_Tarterulet); }                                            
        }
                   
           
        /// <summary>
        /// KR_ETertiveveny property
        /// Kimen� �zenethez k�r�nk-e eTertivevenyt (jelenleg nincs haszn�latban, de kompatibilit�si okokb�l megtartjuk).
        /// </summary>
        public String KR_ETertiveveny
        {
            get { return Utility.GetStringFromSqlChars(Typed.KR_ETertiveveny); }
            set { Typed.KR_ETertiveveny = Utility.SetSqlCharsFromString(value, Typed.KR_ETertiveveny); }                                            
        }
                   
           
        /// <summary>
        /// KR_Lenyomat property
        /// A dokumentum SHA-256 lenyomata.
        /// </summary>
        public String KR_Lenyomat
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_Lenyomat); }
            set { Typed.KR_Lenyomat = Utility.SetSqlStringFromString(value, Typed.KR_Lenyomat); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// 
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// IratPeldany_Id property
        /// 
        /// </summary>
        public String IratPeldany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IratPeldany_Id); }
            set { Typed.IratPeldany_Id = Utility.SetSqlGuidFromString(value, Typed.IratPeldany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Cel property
        /// Az automatikus feldolgoz�st seg�t� META adat, pl. NMHH szervezeti egys�g, vagy c�lrendszer (HAIR)
        /// </summary>
        public String Cel
        {
            get { return Utility.GetStringFromSqlString(Typed.Cel); }
            set { Typed.Cel = Utility.SetSqlStringFromString(value, Typed.Cel); }                                            
        }
                   
           
        /// <summary>
        /// PR_Parameterek property
        /// Elhisz param�terek
        /// </summary>
        public String PR_Parameterek
        {
            get { return Utility.GetStringFromSqlString(Typed.PR_Parameterek); }
            set { Typed.PR_Parameterek = Utility.SetSqlStringFromString(value, Typed.PR_Parameterek); }                                            
        }
                   
           
        /// <summary>
        /// KR_Fiok property
        /// Hivatali kapu fi�k: melyik fi�kkal lett let�ltve illetve elk�ldve az �zenet
        /// </summary>
        public String KR_Fiok
        {
            get { return Utility.GetStringFromSqlString(Typed.KR_Fiok); }
            set { Typed.KR_Fiok = Utility.SetSqlStringFromString(value, Typed.KR_Fiok); }                                            
        }
                   
           
        /// <summary>
        /// FeldolgozasStatusz property
        /// 
        /// </summary>
        public String FeldolgozasStatusz
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeldolgozasStatusz); }
            set { Typed.FeldolgozasStatusz = Utility.SetSqlInt32FromString(value, Typed.FeldolgozasStatusz); }                                            
        }
                   
           
        /// <summary>
        /// FeldolgozasiHiba property
        /// 
        /// </summary>
        public String FeldolgozasiHiba
        {
            get { return Utility.GetStringFromSqlString(Typed.FeldolgozasiHiba); }
            set { Typed.FeldolgozasiHiba = Utility.SetSqlStringFromString(value, Typed.FeldolgozasiHiba); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}