
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailBoritekCimei BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_eMailBoritekCimei : BaseEREC_eMailBoritekCimei
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// eMailBoritek_Id property </summary>
        public String eMailBoritek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.eMailBoritek_Id); }
            set { Typed.eMailBoritek_Id = Utility.SetSqlGuidFromString(value, Typed.eMailBoritek_Id); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// MailCim property </summary>
        public String MailCim
        {
            get { return Utility.GetStringFromSqlString(Typed.MailCim); }
            set { Typed.MailCim = Utility.SetSqlStringFromString(value, Typed.MailCim); }                                            
        }
                   
           
        /// <summary>
        /// Partner_Id property </summary>
        public String Partner_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_Id); }
            set { Typed.Partner_Id = Utility.SetSqlGuidFromString(value, Typed.Partner_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}