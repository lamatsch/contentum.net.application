
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailBoritekCsatolmanyok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_eMailBoritekCsatolmanyok : BaseEREC_eMailBoritekCsatolmanyok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// eMailBoritek_Id property </summary>
        public String eMailBoritek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.eMailBoritek_Id); }
            set { Typed.eMailBoritek_Id = Utility.SetSqlGuidFromString(value, Typed.eMailBoritek_Id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Tomoritve property </summary>
        public String Tomoritve
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tomoritve); }
            set { Typed.Tomoritve = Utility.SetSqlCharsFromString(value, Typed.Tomoritve); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}