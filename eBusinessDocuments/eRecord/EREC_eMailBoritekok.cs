
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailBoritekok BusinessDocument Class
    /// CheckConstraint_fn
    /// Review: Ne legyen itt is az Id a PK??! 
    /// Ez egy mail. Szerveren kezelt. A bej�v�n�l egy nyilv�nos (pl. Exchange) mapp�ba
    /// �th�zza a felhaszn�l�, iktatand� t�telekbe ker�lnek. (Hiszen iktat�k�nyvet, �gyk�dot,
    /// t�rgyk�dot stb...)
    /// 
    /// </summary>
    [Serializable()]
    public class EREC_eMailBoritekok : BaseEREC_eMailBoritekok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Felado property
        /// 
        /// </summary>
        public String Felado
        {
            get { return Utility.GetStringFromSqlString(Typed.Felado); }
            set { Typed.Felado = Utility.SetSqlStringFromString(value, Typed.Felado); }                                            
        }
                   
           
        /// <summary>
        /// Cimzett property
        /// 
        /// </summary>
        public String Cimzett
        {
            get { return Utility.GetStringFromSqlString(Typed.Cimzett); }
            set { Typed.Cimzett = Utility.SetSqlStringFromString(value, Typed.Cimzett); }                                            
        }
                   
           
        /// <summary>
        /// CC property
        /// EMail CarbonCopy c�mzettjei
        /// </summary>
        public String CC
        {
            get { return Utility.GetStringFromSqlString(Typed.CC); }
            set { Typed.CC = Utility.SetSqlStringFromString(value, Typed.CC); }                                            
        }
                   
           
        /// <summary>
        /// Targy property
        /// Mail t�rgya, megnevez�se
        /// </summary>
        public String Targy
        {
            get { return Utility.GetStringFromSqlString(Typed.Targy); }
            set { Typed.Targy = Utility.SetSqlStringFromString(value, Typed.Targy); }                                            
        }
                   
           
        /// <summary>
        /// FeladasDatuma property
        /// Felad�s d�tuma
        /// </summary>
        public String FeladasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FeladasDatuma); }
            set { Typed.FeladasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.FeladasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// ErkezesDatuma property
        /// Email �rkez�s d�tuma
        /// </summary>
        public String ErkezesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErkezesDatuma); }
            set { Typed.ErkezesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.ErkezesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Fontossag property
        /// KCS: ??
        /// </summary>
        public String Fontossag
        {
            get { return Utility.GetStringFromSqlString(Typed.Fontossag); }
            set { Typed.Fontossag = Utility.SetSqlStringFromString(value, Typed.Fontossag); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�ny Id-je
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// DigitalisAlairas property
        /// ??
        /// </summary>
        public String DigitalisAlairas
        {
            get { return Utility.GetStringFromSqlString(Typed.DigitalisAlairas); }
            set { Typed.DigitalisAlairas = Utility.SetSqlStringFromString(value, Typed.DigitalisAlairas); }                                            
        }
                   
           
        /// <summary>
        /// Uzenet property
        /// �zenet sz�vege
        /// </summary>
        public String Uzenet
        {
            get { return Utility.GetStringFromSqlString(Typed.Uzenet); }
            set { Typed.Uzenet = Utility.SetSqlStringFromString(value, Typed.Uzenet); }                                            
        }
                   
           
        /// <summary>
        /// EmailForras property
        /// 
        /// </summary>
        public String EmailForras
        {
            get { return Utility.GetStringFromSqlString(Typed.EmailForras); }
            set { Typed.EmailForras = Utility.SetSqlStringFromString(value, Typed.EmailForras); }                                            
        }
                   
           
        /// <summary>
        /// EmailGuid property
        /// 
        /// </summary>
        public String EmailGuid
        {
            get { return Utility.GetStringFromSqlString(Typed.EmailGuid); }
            set { Typed.EmailGuid = Utility.SetSqlStringFromString(value, Typed.EmailGuid); }                                            
        }
                   
           
        /// <summary>
        /// FeldolgozasIdo property
        /// Feldolgoz�s ideje
        /// </summary>
        public String FeldolgozasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FeldolgozasIdo); }
            set { Typed.FeldolgozasIdo = Utility.SetSqlDateTimeFromString(value, Typed.FeldolgozasIdo); }                                            
        }
                   
           
        /// <summary>
        /// ForrasTipus property
        /// KCS: EMAILFORRASTIPUS B-bels�, K: k�ls�
        /// </summary>
        public String ForrasTipus
        {
            get { return Utility.GetStringFromSqlString(Typed.ForrasTipus); }
            set { Typed.ForrasTipus = Utility.SetSqlStringFromString(value, Typed.ForrasTipus); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: EMAILBORITEK_ALLAPOT T:t�rolt, F: feldolgozot
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}