
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// EREC_eMailFiokok BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_eMailFiokok : BaseEREC_eMailFiokok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// UserNev property </summary>
        public String UserNev
        {
            get { return Utility.GetStringFromSqlString(Typed.UserNev); }
            set { Typed.UserNev = Utility.SetSqlStringFromString(value, Typed.UserNev); }                                            
        }
                   
           
        /// <summary>
        /// Csoportok_Id property </summary>
        public String Csoportok_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoportok_Id); }
            set { Typed.Csoportok_Id = Utility.SetSqlGuidFromString(value, Typed.Csoportok_Id); }                                            
        }
                   
           
        /// <summary>
        /// Jelszo property </summary>
        public String Jelszo
        {
            get { return Utility.GetStringFromSqlString(Typed.Jelszo); }
            set { Typed.Jelszo = Utility.SetSqlStringFromString(value, Typed.Jelszo); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// NapiMax property </summary>
        public String NapiMax
        {
            get { return Utility.GetStringFromSqlInt32(Typed.NapiMax); }
            set { Typed.NapiMax = Utility.SetSqlInt32FromString(value, Typed.NapiMax); }                                            
        }
                   
           
        /// <summary>
        /// NapiTeny property </summary>
        public String NapiTeny
        {
            get { return Utility.GetStringFromSqlInt32(Typed.NapiTeny); }
            set { Typed.NapiTeny = Utility.SetSqlInt32FromString(value, Typed.NapiTeny); }                                            
        }
                   
           
        /// <summary>
        /// EmailCim property </summary>
        public String EmailCim
        {
            get { return Utility.GetStringFromSqlString(Typed.EmailCim); }
            set { Typed.EmailCim = Utility.SetSqlStringFromString(value, Typed.EmailCim); }                                            
        }
                   
           
        /// <summary>
        /// Modul_Id property </summary>
        public String Modul_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Modul_Id); }
            set { Typed.Modul_Id = Utility.SetSqlGuidFromString(value, Typed.Modul_Id); }                                            
        }
                   
           
        /// <summary>
        /// MappaNev property </summary>
        public String MappaNev
        {
            get { return Utility.GetStringFromSqlString(Typed.MappaNev); }
            set { Typed.MappaNev = Utility.SetSqlStringFromString(value, Typed.MappaNev); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}