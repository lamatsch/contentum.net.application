﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    [System.Xml.Serialization.XmlInclude(typeof(ErkeztetesIktatasResult))]
    public class ErkeztetesIktatasResult
    {
        private string kuldemenyId = null;

        public string KuldemenyId
        {
            get { return kuldemenyId; }
            set { kuldemenyId = value; }
        }

        private string kuldemenyAzonosito = null;

        public string KuldemenyAzonosito
        {
            get { return kuldemenyAzonosito; }
            set { kuldemenyAzonosito = value; }
        }

        private string ugyiratId = null;

        public string UgyiratId
        {
            get { return ugyiratId; }
            set { ugyiratId = value; }
        }

        private string ugyiratAzonosito = null;

        public string UgyiratAzonosito
        {
            get { return ugyiratAzonosito; }
            set { ugyiratAzonosito = value; }
        }

        private string ugyiratCsoportIdFelelos = null;

        public string UgyiratCsoportIdFelelos
        {
            get { return ugyiratCsoportIdFelelos; }
            set { ugyiratCsoportIdFelelos = value; }
        }

        private string iratId = null;

        public string IratId
        {
            get { return iratId; }
            set { iratId = value; }
        }

        private string iratAzonosito = null;

        public string IratAzonosito
        {
            get { return iratAzonosito; }
            set { iratAzonosito = value; }
        }

        private bool munkaPeldany = false;

        // A keletkezett irat munkapéldány-e?
        public bool MunkaPeldany 
        {
            get { return munkaPeldany; }
            set { munkaPeldany = value; }
        }

        // alszámra iktatásnál ügyirat határidő módosítás
        private string ugyiratUjHatarido = null;

        public string UgyiratUjHatarido
        {
            get { return ugyiratUjHatarido; }
            set { ugyiratUjHatarido = value; }
        }

        public ErkeztetesIktatasResult()
        {
        }

        public ErkeztetesIktatasResult(ErkeztetesIktatasResult erkeztetesResult, ErkeztetesIktatasResult iktatasResult)
        {
            if (erkeztetesResult != null)
            {
                this.kuldemenyId = erkeztetesResult.kuldemenyId;
                this.kuldemenyAzonosito = erkeztetesResult.kuldemenyAzonosito;
            }

            if (iktatasResult != null)
            {
                this.ugyiratId = iktatasResult.ugyiratId;
                this.ugyiratAzonosito = iktatasResult.ugyiratAzonosito;
                this.ugyiratCsoportIdFelelos = iktatasResult.ugyiratCsoportIdFelelos;
                this.iratId = iktatasResult.iratId;
                this.iratAzonosito = iktatasResult.iratAzonosito;
                this.munkaPeldany = iktatasResult.munkaPeldany;
                this.ugyiratUjHatarido = iktatasResult.ugyiratUjHatarido;
            }
        }


    }
}
