
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// HKP_DokumentumAdatok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class HKP_DokumentumAdatok : BaseHKP_DokumentumAdatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Irany property
        /// 0 - Bej�v�, 1 - Kimen�
        /// </summary>
        public String Irany
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Irany); }
            set { Typed.Irany = Utility.SetSqlInt32FromString(value, Typed.Irany); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// 0 - Let�ltve
        /// 10 - Elk�ld�sre el�k�sz�tve
        /// 11 - �gyf�lkapunak elk�ldve
        /// 12 - C�mzett �tvette
        /// 13 - C�mzett nem vette �t
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlInt32FromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// FeladoTipusa property
        /// 0-�llampolg�r, 1-Hivatal, 2 -Rendszer
        /// </summary>
        public String FeladoTipusa
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladoTipusa); }
            set { Typed.FeladoTipusa = Utility.SetSqlInt32FromString(value, Typed.FeladoTipusa); }                                            
        }
                   
           
        /// <summary>
        /// KapcsolatiKod property
        /// A felad� �llampolg�r kapcsolati k�dja
        /// K�ld�s eset�n a felad� azonos�t�ja.
        /// </summary>
        public String KapcsolatiKod
        {
            get { return Utility.GetStringFromSqlString(Typed.KapcsolatiKod); }
            set { Typed.KapcsolatiKod = Utility.SetSqlStringFromString(value, Typed.KapcsolatiKod); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// A felad� �llampolg�r/hivatal teljes neve
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Email property
        /// A felad� �llampolg�r email c�me
        /// </summary>
        public String Email
        {
            get { return Utility.GetStringFromSqlString(Typed.Email); }
            set { Typed.Email = Utility.SetSqlStringFromString(value, Typed.Email); }                                            
        }
                   
           
        /// <summary>
        /// RovidNev property
        /// A felad� hivatal r�vidneve
        /// </summary>
        public String RovidNev
        {
            get { return Utility.GetStringFromSqlString(Typed.RovidNev); }
            set { Typed.RovidNev = Utility.SetSqlStringFromString(value, Typed.RovidNev); }                                            
        }
                   
           
        /// <summary>
        /// MAKKod property
        /// A felad� hivatal M�K k�dja 
        /// </summary>
        public String MAKKod
        {
            get { return Utility.GetStringFromSqlString(Typed.MAKKod); }
            set { Typed.MAKKod = Utility.SetSqlStringFromString(value, Typed.MAKKod); }                                            
        }
                   
           
        /// <summary>
        /// KRID property
        /// A felad� hivatal KRID-je.
        /// K�ld�s eset�n a felad� azonos�t�ja.
        /// </summary>
        public String KRID
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KRID); }
            set { Typed.KRID = Utility.SetSqlInt32FromString(value, Typed.KRID); }                                            
        }
                   
           
        /// <summary>
        /// ErkeztetesiSzam property
        /// A dokumentumhoz a rendszer �ltal gener�lt �rkeztet�si sz�m
        /// Az �rkeztet�si sz�m fel�p�t�se a k�vetkez�: 9 jegy� KR ID azonos�t� +����HHNN��PP+ 6 jegy� foly� sz�msorozat
        /// </summary>
        public String ErkeztetesiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.ErkeztetesiSzam); }
            set { Typed.ErkeztetesiSzam = Utility.SetSqlStringFromString(value, Typed.ErkeztetesiSzam); }                                            
        }
                   
           
        /// <summary>
        /// HivatkozasiSzam property
        /// A dokumentum hivatkoz�si sz�ma. Kit�lt�tt, amennyiben van hivatkozott dokumentum. �rt�ke a hivatkozott dokumentum �rkeztet�si sz�ma. 
        /// Jelenleg felt�lt�si nyugt�kban, �s olvasatlan �zenetek visszaigazol�saiban van szerepe, illetve dokumentum felt�lt�skor.
        /// </summary>
        public String HivatkozasiSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.HivatkozasiSzam); }
            set { Typed.HivatkozasiSzam = Utility.SetSqlStringFromString(value, Typed.HivatkozasiSzam); }                                            
        }
                   
           
        /// <summary>
        /// DokTipusHivatal property
        /// A dokumentumot kibocs�t� szervezet r�vid neve
        /// </summary>
        public String DokTipusHivatal
        {
            get { return Utility.GetStringFromSqlString(Typed.DokTipusHivatal); }
            set { Typed.DokTipusHivatal = Utility.SetSqlStringFromString(value, Typed.DokTipusHivatal); }                                            
        }
                   
           
        /// <summary>
        /// DokTipusAzonosito property
        /// A dokumentum t�pus�nak azonos�t�ja
        /// </summary>
        public String DokTipusAzonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.DokTipusAzonosito); }
            set { Typed.DokTipusAzonosito = Utility.SetSqlStringFromString(value, Typed.DokTipusAzonosito); }                                            
        }
                   
           
        /// <summary>
        /// DokTipusLeiras property
        /// A dokumentum t�pus�nak megnevez�se
        /// </summary>
        public String DokTipusLeiras
        {
            get { return Utility.GetStringFromSqlString(Typed.DokTipusLeiras); }
            set { Typed.DokTipusLeiras = Utility.SetSqlStringFromString(value, Typed.DokTipusLeiras); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// FileNev property
        /// A feladott f�jl (SOAP mell�kletben utaz�) neve
        /// </summary>
        public String FileNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FileNev); }
            set { Typed.FileNev = Utility.SetSqlStringFromString(value, Typed.FileNev); }                                            
        }
                   
           
        /// <summary>
        /// ErvenyessegiDatum property
        /// A dokumentum t�rol�si ideje (ezen id�pontig t�rol�dik a BEDSZ-ben)
        /// </summary>
        public String ErvenyessegiDatum
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvenyessegiDatum); }
            set { Typed.ErvenyessegiDatum = Utility.SetSqlDateTimeFromString(value, Typed.ErvenyessegiDatum); }                                            
        }
                   
           
        /// <summary>
        /// ErkeztetesiDatum property
        /// A dokumentum �rkeztet�si (befogad�si) d�tuma
        /// </summary>
        public String ErkeztetesiDatum
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErkeztetesiDatum); }
            set { Typed.ErkeztetesiDatum = Utility.SetSqlDateTimeFromString(value, Typed.ErkeztetesiDatum); }                                            
        }
                   
           
        /// <summary>
        /// Kezbesitettseg property
        /// A dokumentum k�zbes�tetts�g�nek jelz�je
        /// (0-K�zbes�t�sre v�r, 2-K�zbes�tetlen, 3-K�zbes�t�si nyugta).
        /// </summary>
        public String Kezbesitettseg
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Kezbesitettseg); }
            set { Typed.Kezbesitettseg = Utility.SetSqlInt32FromString(value, Typed.Kezbesitettseg); }                                            
        }
                   
           
        /// <summary>
        /// Idopecset property
        /// Az �rkeztet�st igazol� id�b�lyegz�s. Az id�b�lyeg a felk�ld�tt dokumentumb�l SHA1-es algoritmussal k�sz�lt lenyomatra ker�l. Az elem tartalmazza az id�b�lyeget base64 k�doltan, alkalmas szervezeti oldalon a dokumentum integrit�s�nak ellen�rz�s�re. Az id�b�lyegz�s az RFC 3161 szabv�nynak megfelel� form�tum�.
        /// </summary>
        public String Idopecset
        {
            get { return Utility.GetStringFromSqlString(Typed.Idopecset); }
            set { Typed.Idopecset = Utility.SetSqlStringFromString(value, Typed.Idopecset); }                                            
        }
                   
           
        /// <summary>
        /// ValaszTitkositas property
        /// A felad� k�rheti a szervezetet, hogy a v�laszt titkos�tva k�ri. 
        /// (false � nem kell titkos�tani, true � titkos�tani kell) 
        /// A titkos�t�s a felad� nyilv�nos kulcs�val t�rt�nik, amit az �gyf�l kapcsolati k�dj�val lehet lek�rdezni a kulcst�rb�l. 
        /// </summary>
        public String ValaszTitkositas
        {
            get { return Utility.GetStringFromSqlChars(Typed.ValaszTitkositas); }
            set { Typed.ValaszTitkositas = Utility.SetSqlCharsFromString(value, Typed.ValaszTitkositas); }                                            
        }
                   
           
        /// <summary>
        /// ValaszUtvonal property
        /// Az �rkeztet�st visszaigazol� v�lasz �zenet �tvonala (0-�rtes�t�si t�rhely, 1-k�zvetlen e-mail). 
        /// A szervezetnek nem kell a v�lasz�tvonallal foglalkoznia. Az csak az �rkeztet�si visszaigazol�sra vonatkozik, melyet a KR v�gez.
        /// </summary>
        public String ValaszUtvonal
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ValaszUtvonal); }
            set { Typed.ValaszUtvonal = Utility.SetSqlInt32FromString(value, Typed.ValaszUtvonal); }                                            
        }
                   
           
        /// <summary>
        /// Rendszeruzenet property
        /// Felhaszn�l� �ltal k�ld�tt dokumentum eset�n �rt�ke hamis (false), olvas�si visszaigazol�s eset�n igaz (true)
        /// </summary>
        public String Rendszeruzenet
        {
            get { return Utility.GetStringFromSqlChars(Typed.Rendszeruzenet); }
            set { Typed.Rendszeruzenet = Utility.SetSqlCharsFromString(value, Typed.Rendszeruzenet); }                                            
        }
                   
           
        /// <summary>
        /// Tarterulet property
        /// A T�R t�pusa (0-�tmeneti t�r)
        /// 
        /// Technikai param�ter, amely jelzi, hogy az adott �zenet ideiglenes, vagy tart�s t�rban tal�lhat�-e. Szervezetek eset�n �rt�ke mindig 0, azaz ideiglenes t�r.
        /// </summary>
        public String Tarterulet
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Tarterulet); }
            set { Typed.Tarterulet = Utility.SetSqlInt32FromString(value, Typed.Tarterulet); }                                            
        }
                   
           
        /// <summary>
        /// ETertiveveny property
        /// true/false, A visszaigazol�s, tipus�t tekintve ET�rtivev�ny legyen
        /// </summary>
        public String ETertiveveny
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ETertiveveny); }
            set { Typed.ETertiveveny = Utility.SetSqlInt32FromString(value, Typed.ETertiveveny); }                                            
        }
                   
           
        /// <summary>
        /// Lenyomat property
        /// A dokumentum SHA1 lenyomata. Nem k�telez�.
        /// </summary>
        public String Lenyomat
        {
            get { return Utility.GetStringFromSqlString(Typed.Lenyomat); }
            set { Typed.Lenyomat = Utility.SetSqlStringFromString(value, Typed.Lenyomat); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// A dokumentumhoz tartoz� f�jl.
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// �rkeztetett k�ldem�ny id-ja.
        /// </summary>
        public String KuldKuldemeny_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.KuldKuldemeny_Id); }
            set { Typed.KuldKuldemeny_Id = Utility.SetSqlGuidFromString(value, Typed.KuldKuldemeny_Id); }                                            
        }
                   
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        public String IraIrat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IraIrat_Id); }
            set { Typed.IraIrat_Id = Utility.SetSqlGuidFromString(value, Typed.IraIrat_Id); }                                            
        }
                   
           
        /// <summary>
        /// IratPeldany_Id property
        /// Expedi�lt iratp�ld�ny azonos�t�ja.
        /// </summary>
        public String IratPeldany_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IratPeldany_Id); }
            set { Typed.IratPeldany_Id = Utility.SetSqlGuidFromString(value, Typed.IratPeldany_Id); }                                            
        }
                   
           
        /// <summary>
        /// Rendszer property
        /// Melyik rendszer fel� tov�bb�tjuk az adott �zenetet:
        /// WEB - Contentum fel�leten jelen�tj�k meg
        /// HAIR - HAIR rendszernek �djuk �t lek�rdez�s hat�s�ra (csatolm�nnyal egy�tt)
        /// </summary>
        public String Rendszer
        {
            get { return Utility.GetStringFromSqlString(Typed.Rendszer); }
            set { Typed.Rendszer = Utility.SetSqlStringFromString(value, Typed.Rendszer); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}