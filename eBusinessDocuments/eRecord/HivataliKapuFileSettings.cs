﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class HivataliKapuFileSettings
    {
        private bool _PackFiles;

        public bool PackFiles
        {
            get { return _PackFiles; }
            set { _PackFiles = value; }
        }

        private string _PackedFileName;

        public string PackedFileName
        {
            get { return _PackedFileName; }
            set { _PackedFileName = value; }
        }

        private HivataliKapuFileFormat _FileFormat = HivataliKapuFileFormat.Zip;

        public HivataliKapuFileFormat FileFormat
        {
            get { return _FileFormat; }
            set { _FileFormat = value; }
        }

        public enum HivataliKapuFileFormat
        {
            Zip,
            Krx
        }

        private bool _EncryptFiles;

        public bool EncryptFiles
        {
            get { return _EncryptFiles; }
            set { _EncryptFiles = value; }
        }
    }
}
