using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class IratPeldanyHierarchia
    {
        private EREC_PldIratPeldanyok iratPeldanyObj;

        public EREC_PldIratPeldanyok IratPeldanyObj
        {
            get { return iratPeldanyObj; }
            set { iratPeldanyObj = value; }
        }
	
        
        private EREC_IraIratok iratObj;

        public EREC_IraIratok IratObj
        {
            get { return iratObj; }
            set { iratObj = value; }
        }


        private EREC_UgyUgyiratdarabok ugyiratDarabObj;

        public EREC_UgyUgyiratdarabok UgyiratDarabObj
        {
            get { return ugyiratDarabObj; }
            set { ugyiratDarabObj = value; }
        }


        private EREC_UgyUgyiratok ugyiratObj;

        public EREC_UgyUgyiratok UgyiratObj
        {
            get { return ugyiratObj; }
            set { ugyiratObj = value; }
        }
	
    }
}
