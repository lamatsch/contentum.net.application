
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_BarkodSavok BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_BarkodSavok : BaseKRT_BarkodSavok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        public String Org
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Org); }
            set { Typed.Org = Utility.SetSqlGuidFromString(value, Typed.Org); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Aki a vonalkód tartományt létrehozta, vagy akinél éppen van.
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// SavKezd property
        /// 
        /// </summary>
        public String SavKezd
        {
            get { return Utility.GetStringFromSqlString(Typed.SavKezd); }
            set { Typed.SavKezd = Utility.SetSqlStringFromString(value, Typed.SavKezd); }                                            
        }
                   
           
        /// <summary>
        /// SavVege property
        /// 
        /// </summary>
        public String SavVege
        {
            get { return Utility.GetStringFromSqlString(Typed.SavVege); }
            set { Typed.SavVege = Utility.SetSqlStringFromString(value, Typed.SavVege); }                                            
        }
                   
           
        /// <summary>
        /// SavType property
        /// N,P
        /// </summary>
        public String SavType
        {
            get { return Utility.GetStringFromSqlChars(Typed.SavType); }
            set { Typed.SavType = Utility.SetSqlCharsFromString(value, Typed.SavType); }                                            
        }
                   
           
        /// <summary>
        /// SavAllapot property
        /// nyomtatás alatt, felhasználható, törölt
        /// </summary>
        public String SavAllapot
        {
            get { return Utility.GetStringFromSqlChars(Typed.SavAllapot); }
            set { Typed.SavAllapot = Utility.SetSqlCharsFromString(value, Typed.SavAllapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}