
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_MappaUtak BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_MappaUtak : BaseKRT_MappaUtak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Mappa_Id property </summary>
        public String Mappa_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Mappa_Id); }
            set { Typed.Mappa_Id = Utility.SetSqlGuidFromString(value, Typed.Mappa_Id); }                                            
        }
                   
           
        /// <summary>
        /// PathFromORG property </summary>
        public String PathFromORG
        {
            get { return Utility.GetStringFromSqlString(Typed.PathFromORG); }
            set { Typed.PathFromORG = Utility.SetSqlStringFromString(value, Typed.PathFromORG); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}