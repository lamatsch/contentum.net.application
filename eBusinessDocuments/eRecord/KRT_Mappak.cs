
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Mappak BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_Mappak : BaseKRT_Mappak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// SzuloMappa_Id property </summary>
        public String SzuloMappa_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.SzuloMappa_Id); }
            set { Typed.SzuloMappa_Id = Utility.SetSqlGuidFromString(value, Typed.SzuloMappa_Id); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Tulaj property </summary>
        public String Csoport_Id_Tulaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Tulaj); }
            set { Typed.Csoport_Id_Tulaj = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Tulaj); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}