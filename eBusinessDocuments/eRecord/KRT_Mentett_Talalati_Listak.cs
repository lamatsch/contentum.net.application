
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Mentett_Talalati_Listak BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class KRT_Mentett_Talalati_Listak : BaseKRT_Mentett_Talalati_Listak
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// ListName property
        /// 
        /// </summary>
        public String ListName
        {
            get { return Utility.GetStringFromSqlString(Typed.ListName); }
            set { Typed.ListName = Utility.SetSqlStringFromString(value, Typed.ListName); }                                            
        }
                   
           
        /// <summary>
        /// SaveDate property
        /// 
        /// </summary>
        public String SaveDate
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SaveDate); }
            set { Typed.SaveDate = Utility.SetSqlDateTimeFromString(value, Typed.SaveDate); }                                            
        }
                   
           
        /// <summary>
        /// Searched_Table property
        /// 
        /// </summary>
        public String Searched_Table
        {
            get { return Utility.GetStringFromSqlString(Typed.Searched_Table); }
            set { Typed.Searched_Table = Utility.SetSqlStringFromString(value, Typed.Searched_Table); }                                            
        }
                   
           
        /// <summary>
        /// Requestor property
        /// 
        /// </summary>
        public String Requestor
        {
            get { return Utility.GetStringFromSqlString(Typed.Requestor); }
            set { Typed.Requestor = Utility.SetSqlStringFromString(value, Typed.Requestor); }                                            
        }
                   
           
        /// <summary>
        /// HeaderXML property
        /// 
        /// </summary>
        public String HeaderXML
        {
            get { return Utility.GetStringFromSqlString(Typed.HeaderXML); }
            set { Typed.HeaderXML = Utility.SetSqlStringFromString(value, Typed.HeaderXML); }                                            
        }
                   
           
        /// <summary>
        /// BodyXML property
        /// 
        /// </summary>
        public String BodyXML
        {
            get { return Utility.GetStringFromSqlString(Typed.BodyXML); }
            set { Typed.BodyXML = Utility.SetSqlStringFromString(value, Typed.BodyXML); }                                            
        }
                   
           
        /// <summary>
        /// WorkStation property
        /// 
        /// </summary>
        public String WorkStation
        {
            get { return Utility.GetStringFromSqlString(Typed.WorkStation); }
            set { Typed.WorkStation = Utility.SetSqlStringFromString(value, Typed.WorkStation); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}