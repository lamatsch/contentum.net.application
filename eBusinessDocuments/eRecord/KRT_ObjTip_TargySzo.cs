
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_ObjTip_TargySzo BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_ObjTip_TargySzo : BaseKRT_ObjTip_TargySzo
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// TargySzo_Id property </summary>
        public String TargySzo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.TargySzo_Id); }
            set { Typed.TargySzo_Id = Utility.SetSqlGuidFromString(value, Typed.TargySzo_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id_AdatElem property </summary>
        public String ObjTip_Id_AdatElem
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id_AdatElem); }
            set { Typed.ObjTip_Id_AdatElem = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id_AdatElem); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Sorszam property </summary>
        public String Sorszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorszam); }
            set { Typed.Sorszam = Utility.SetSqlInt32FromString(value, Typed.Sorszam); }                                            
        }
                   
           
        /// <summary>
        /// Opcionalis property </summary>
        public String Opcionalis
        {
            get { return Utility.GetStringFromSqlChars(Typed.Opcionalis); }
            set { Typed.Opcionalis = Utility.SetSqlCharsFromString(value, Typed.Opcionalis); }                                            
        }
                   
           
        /// <summary>
        /// Nev property </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}