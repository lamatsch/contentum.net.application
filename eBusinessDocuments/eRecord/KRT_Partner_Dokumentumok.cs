
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_Partner_Dokumentumok BusinessDocument Class
    /// Partnerekhez tartozó dokumentumok kapcsolótáblája
    /// </summary>
    [Serializable()]
    public class KRT_Partner_Dokumentumok : BaseKRT_Partner_Dokumentumok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Partner_id property
        /// Partner id
        /// </summary>
        public String Partner_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Partner_id); }
            set { Typed.Partner_id = Utility.SetSqlGuidFromString(value, Typed.Partner_id); }                                            
        }
                   
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        public String Dokumentum_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Dokumentum_Id); }
            set { Typed.Dokumentum_Id = Utility.SetSqlGuidFromString(value, Typed.Dokumentum_Id); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}