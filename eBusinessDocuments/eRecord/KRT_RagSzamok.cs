
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// KRT_RagSzamok BusinessDocument Class
    /// Ragsz�mok
    /// </summary>
    [Serializable()]
    public class KRT_RagSzamok : BaseKRT_RagSzamok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Kod property
        /// A b�rk�d azonos�t�ja.
        /// </summary>
        public String Kod
        {
            get { return Utility.GetStringFromSqlString(Typed.Kod); }
            set { Typed.Kod = Utility.SetSqlStringFromString(value, Typed.Kod); }                                            
        }
                   
           
        /// <summary>
        /// KodNum property
        /// 
        /// </summary>
        public String KodNum
        {
            get { return Utility.GetStringFromSqlDouble(Typed.KodNum); }
            set { Typed.KodNum = Utility.SetSqlDoubleFromString(value, Typed.KodNum); }                                            
        }
                   
           
        /// <summary>
        /// Postakonyv_Id property
        /// 
        /// </summary>
        public String Postakonyv_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Postakonyv_Id); }
            set { Typed.Postakonyv_Id = Utility.SetSqlGuidFromString(value, Typed.Postakonyv_Id); }                                            
        }
                   
           
        /// <summary>
        /// RagszamSav_Id property
        /// 
        /// </summary>
        public String RagszamSav_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.RagszamSav_Id); }
            set { Typed.RagszamSav_Id = Utility.SetSqlGuidFromString(value, Typed.RagszamSav_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property
        /// A b�rk�ddal ell�tott objektum ID -ja. 
        /// Bts: r�gt�n lehessen l�tni, hova ker�lt a b�rk�d felhaszn�l�sra.
        /// </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlGuidFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        public String ObjTip_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ObjTip_Id); }
            set { Typed.ObjTip_Id = Utility.SetSqlGuidFromString(value, Typed.ObjTip_Id); }                                            
        }
                   
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        public String Obj_type
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_type); }
            set { Typed.Obj_type = Utility.SetSqlStringFromString(value, Typed.Obj_type); }                                            
        }
                   
           
        /// <summary>
        /// KodType property
        /// G,N,P
        /// </summary>
        public String KodType
        {
            get { return Utility.GetStringFromSqlChars(Typed.KodType); }
            set { Typed.KodType = Utility.SetSqlCharsFromString(value, Typed.KodType); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// KCS: BARKOD_ALLAPOT
        /// S - Szabad
        /// F - Felhaszn�lt
        /// T - T�r�lt
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvKezd
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvKezd); }
            set { Typed.ErvKezd = Utility.SetSqlDateTimeFromString(value, Typed.ErvKezd); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                   
           
        /// <summary>
        /// Tranz_id property
        /// Tranzakci� azonos�t�: egy program �ltal egy logikai egys�gk�nt t�rolt inform�ci�kat 
        /// egyetlen Guid -al azonos�tunk. (pl. egy ASP.Net lap �ltal egy ment�ssel, ha t�bb t�bl�ba t�rol
        /// inform�ci�kat, ezen azonos�t� alapj�n lehet tudni, egy program volt az inform�ci� forr�s.Vagy: 
        /// Az XMLImport seg�dprogram fut�s�t c�lszer� az xml file-ban egyetlen tranz_id -val azonos�tani)
        /// </summary>
        public String Tranz_id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Tranz_id); }
            set { Typed.Tranz_id = Utility.SetSqlGuidFromString(value, Typed.Tranz_id); }                                            
        }
                           }
   
}