using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eBusinessDocuments
{
    [Serializable()]
    public class KozterInterface : BaseKozterInterface
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        public String ktdok_id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ktdok_id); }
            set { Typed.ktdok_id = Utility.SetSqlInt32FromString(value, Typed.ktdok_id); }
        }

        public String ktdok_kod
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_kod); }
            set { Typed.ktdok_kod = Utility.SetSqlStringFromString(value, Typed.ktdok_kod); }
        }

        public String ktdok_iktszam
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_iktszam); }
            set { Typed.ktdok_iktszam = Utility.SetSqlStringFromString(value, Typed.ktdok_iktszam); }
        }

        public String ktdok_vonalkod
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_vonalkod); }
            set { Typed.ktdok_vonalkod = Utility.SetSqlStringFromString(value, Typed.ktdok_vonalkod); }
        }

        public String ktdok_dokid
        {
            get { return Utility.GetStringFromSqlGuid(Typed.ktdok_dokid); }
            set { Typed.ktdok_dokid = Utility.SetSqlGuidFromString(value, Typed.ktdok_dokid); }
        }

        public String ktdok_url
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_url); }
            set { Typed.ktdok_url = Utility.SetSqlStringFromString(value, Typed.ktdok_url); }
        }

        public String ktdok_date
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ktdok_date); }
            set { Typed.ktdok_date = Utility.SetSqlDateTimeFromString(value, Typed.ktdok_date); }
        }

        public String ktdok_siker
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_siker); }
            set { Typed.ktdok_siker = Utility.SetSqlStringFromString(value, Typed.ktdok_siker); }
        }

        public String ktdok_ok
        {
            get { return Utility.GetStringFromSqlString(Typed.ktdok_ok); }
            set { Typed.ktdok_ok = Utility.SetSqlStringFromString(value, Typed.ktdok_ok); }
        }

        public String ktdok_atvet
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ktdok_atvet); }
            set { Typed.ktdok_atvet = Utility.SetSqlDateTimeFromString(value, Typed.ktdok_atvet); }
        }

    }
}
