
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// PirEdokSzamla BusinessDocument Class
    /// EDOK-PIR interf�sz adat�tvitelre szolg�l� mez�i
    /// 
    /// f_vonalkod - char(13) - Vonalk�d - Kit�lt�se k�telez�
    /// f_kbsz - char(16) - Sz�mlasz�m - Kit�lt�se k�telez�
    /// f_erkdat - smalldatetime - �rkeztet�s d�tuma - Kit�lt�se k�telez�
    /// f_fldhely - char(36) - Szerv. egys�g EDOK k�d - Kit�lt�se k�telez�
    /// f_fldhelynev - char(100) - Szerv. egys�g EDOK neve - Kit�lt�se k�telez�
    /// f_fizmod - char(4) - Fizet�si m�d - Kit�lt�se k�telez� (k�dt�r)
    /// f_tdat - smalldatetime - Teljes�t�s d�tuma - Kit�lt�se opcion�lis
    /// f_bizdat - smalldatetime - Bizonylat d�tuma - Kit�lt�se opcion�lis
    /// f_fhi - smalldatetime - Fizet�si hat�rid� - Kit�lt�se opcion�lis
    /// f_dkod - char(3) - Deviza k�d - Kit. opc., alap�rt.: �HUF�
    /// f_vidat - smalldatetime - Visszak�ld�s d�tuma - Kit�lt�se opcion�lis
    /// f_csumma - float - Ellen�rz� �sszeg - Kit�lt�se k�telez�
    /// f_szrpartn - char(36) - EDOK partnerk�d - Kit�lt�se k�telez�
    /// f_nev1 - varchar(400) - EDOK partner neve - Kit�lt�se k�telez�
    /// f_irsz - char(10) - EDOK part.ir�ny�t�sz�ma - Kit�lt�se k�telez�
    /// f_helyseg - char(20) - EDOK part. helys�ge - Kit�lt�se k�telez�
    /// f_hcim - char(100) - EDOK partner c�me - Kit�lt�se opcion�lis
    /// f_aiasz - char(15) - Belf�ldi ad�sz�m - Kit�lt�se opcion�lis
    /// f_kaiasz - char(15) - K�z�ss�gi (EU) ad�sz�m - Kit�lt�se opcion�lis
    /// f_adoazon - char(10) - Mag.szem. ad�azon. jele - Kit�lt�se opcion�lis
    /// f_telex - char(15) - Helyrajzi sz�m - Kit�lt�se opcion�lis
    /// f_pbsz1 - char(8) - Banksz�mlasz�m1 - Kit�lt�se opcion�lis
    /// f_pbsz2 - char(8) - Banksz�mlasz�m2 - Kit�lt�se opcion�lis
    /// f_pbsz3 - char(8) - Banksz�mlasz�m3 - Kit�lt�se opcion�lis
    /// f_tel1 - char(15) - Telefonsz�m1 - Kit�lt�se opcion�lis
    /// f_fax - char(15) - Fax -  - Kit�lt�se opcion�lis
    /// f_email - varchar(60) - E-mail c�m - Kit�lt�se opcion�lis
    /// f_fstatus - char(1) - PIR szerinti �llapot (F fogadott, U elutas�tott,R rontott) - Kit�lt�se opcion�lis
    /// f_mgj - varchar(40) - Megjegyz�s - Kit�lt�se opcion�lis
    /// 
    /// 
    /// </summary>
    [Serializable()]
    public class PirEdokSzamla : BasePirEdokSzamla
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Vonalkod property
        /// f_vonalkod - Vonalk�d
        /// </summary>
        public String Vonalkod
        {
            get { return Utility.GetStringFromSqlString(Typed.Vonalkod); }
            set { Typed.Vonalkod = Utility.SetSqlStringFromString(value, Typed.Vonalkod); }                                            
        }
                   
           
        /// <summary>
        /// Szamlaszam property
        /// f_kbsz - Sz�mlasz�m
        /// </summary>
        public String Szamlaszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Szamlaszam); }
            set { Typed.Szamlaszam = Utility.SetSqlStringFromString(value, Typed.Szamlaszam); }                                            
        }
                   
           
        /// <summary>
        /// ErkeztetesDatuma property
        /// f_erkdat - �rkeztet�s d�tuma
        /// </summary>
        public String ErkeztetesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErkeztetesDatuma); }
            set { Typed.ErkeztetesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.ErkeztetesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// SzervezetiEgysegKod property
        /// f_fldhely - Szerv. egys�g EDOK k�d
        /// </summary>
        public String SzervezetiEgysegKod
        {
            get { return Utility.GetStringFromSqlString(Typed.SzervezetiEgysegKod); }
            set { Typed.SzervezetiEgysegKod = Utility.SetSqlStringFromString(value, Typed.SzervezetiEgysegKod); }                                            
        }
                   
           
        /// <summary>
        /// SzervezetiEgysegNev property
        /// f_fldhelynev - Szerv. egys�g EDOK neve
        /// </summary>
        public String SzervezetiEgysegNev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzervezetiEgysegNev); }
            set { Typed.SzervezetiEgysegNev = Utility.SetSqlStringFromString(value, Typed.SzervezetiEgysegNev); }                                            
        }
                   
           
        /// <summary>
        /// FizetesiMod property
        /// f_fizmod - Fizet�si m�d
        /// 
        /// FIZA - Azonnali inkassz�
        /// FIZB - Bankgarancia
        /// FIZC - Csekk
        /// FIZD - �d�l�si csekk
        /// FIZE - Banki �tutal�s engedm�nyez�ssel
        /// FIZF - Munkab�rb�l levon�s
        /// FIZH - Hat�rid�s inkassz�
        /// FIZI - Hitelk�rtya
        /// FIZK - K�szp�nz
        /// FIZL - Akkredit�v
        /// FIZM - Kompenz�l�s
        /// FIZP - Postai ut�nv�t/kifizet�s
        /// FIZR - Barter
        /// FIZS - Csoportos beszed�s
        /// FIZT - Csoportos utal�s
        /// FIZU - Banki �tutal�s
        /// FIZV - V�lt�
        /// FIZX - P�nz�gyileg nem rendezend�
        /// FIZY - Bankk�rtya
        /// 
        /// </summary>
        public String FizetesiMod
        {
            get { return Utility.GetStringFromSqlString(Typed.FizetesiMod); }
            set { Typed.FizetesiMod = Utility.SetSqlStringFromString(value, Typed.FizetesiMod); }                                            
        }
                   
           
        /// <summary>
        /// TeljesitesDatuma property
        /// f_tdat - Teljes�t�s d�tuma
        /// </summary>
        public String TeljesitesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.TeljesitesDatuma); }
            set { Typed.TeljesitesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.TeljesitesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// BizonylatDatuma property
        /// f_bizdat - Bizonylat d�tuma
        /// </summary>
        public String BizonylatDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.BizonylatDatuma); }
            set { Typed.BizonylatDatuma = Utility.SetSqlDateTimeFromString(value, Typed.BizonylatDatuma); }                                            
        }
                   
           
        /// <summary>
        /// FizetesiHatarido property
        /// f_fhi - Fizet�si hat�rid�
        /// </summary>
        public String FizetesiHatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FizetesiHatarido); }
            set { Typed.FizetesiHatarido = Utility.SetSqlDateTimeFromString(value, Typed.FizetesiHatarido); }                                            
        }
                   
           
        /// <summary>
        /// DevizaKod property
        /// f_dkod - Deviza k�d (alap�rtelmez�s: "HUF")
        /// </summary>
        public String DevizaKod
        {
            get { return Utility.GetStringFromSqlString(Typed.DevizaKod); }
            set { Typed.DevizaKod = Utility.SetSqlStringFromString(value, Typed.DevizaKod); }                                            
        }
                   
           
        /// <summary>
        /// VisszakuldesDatuma property
        /// f_vidat - Visszak�ld�s d�tuma
        /// </summary>
        public String VisszakuldesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VisszakuldesDatuma); }
            set { Typed.VisszakuldesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VisszakuldesDatuma); }                                            
        }
                   
           
        /// <summary>
        /// EllenorzoOsszeg property
        /// f_csumma - Ellen�rz� �sszeg
        /// </summary>
        public String EllenorzoOsszeg
        {
            get { return Utility.GetStringFromSqlDouble(Typed.EllenorzoOsszeg); }
            set { Typed.EllenorzoOsszeg = Utility.SetSqlDoubleFromString(value, Typed.EllenorzoOsszeg); }                                            
        }
                   
           
        /// <summary>
        /// EDOKPartnerKod property
        /// f_szrpartn - EDOK partnerk�d
        /// </summary>
        public String EDOKPartnerKod
        {
            get { return Utility.GetStringFromSqlString(Typed.EDOKPartnerKod); }
            set { Typed.EDOKPartnerKod = Utility.SetSqlStringFromString(value, Typed.EDOKPartnerKod); }                                            
        }
                   
           
        /// <summary>
        /// EDOKPartnerNev property
        /// f_nev1 - EDOK partner neve
        /// </summary>
        public String EDOKPartnerNev
        {
            get { return Utility.GetStringFromSqlString(Typed.EDOKPartnerNev); }
            set { Typed.EDOKPartnerNev = Utility.SetSqlStringFromString(value, Typed.EDOKPartnerNev); }                                            
        }
                   
           
        /// <summary>
        /// EDOKPartnerIranyitoszam property
        /// f_irsz - EDOK part.ir�ny�t�sz�ma
        /// </summary>
        public String EDOKPartnerIranyitoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.EDOKPartnerIranyitoszam); }
            set { Typed.EDOKPartnerIranyitoszam = Utility.SetSqlStringFromString(value, Typed.EDOKPartnerIranyitoszam); }                                            
        }
                   
           
        /// <summary>
        /// EDOKPartnerHelyseg property
        /// f_helyseg - EDOK part. helys�ge
        /// </summary>
        public String EDOKPartnerHelyseg
        {
            get { return Utility.GetStringFromSqlString(Typed.EDOKPartnerHelyseg); }
            set { Typed.EDOKPartnerHelyseg = Utility.SetSqlStringFromString(value, Typed.EDOKPartnerHelyseg); }                                            
        }
                   
           
        /// <summary>
        /// EDOKPartnerCim property
        /// f_hcim - EDOK partner c�me
        /// </summary>
        public String EDOKPartnerCim
        {
            get { return Utility.GetStringFromSqlString(Typed.EDOKPartnerCim); }
            set { Typed.EDOKPartnerCim = Utility.SetSqlStringFromString(value, Typed.EDOKPartnerCim); }                                            
        }
                   
           
        /// <summary>
        /// BelfoldiAdoszam property
        /// f_aiasz - Belf�ldi ad�sz�m
        /// </summary>
        public String BelfoldiAdoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.BelfoldiAdoszam); }
            set { Typed.BelfoldiAdoszam = Utility.SetSqlStringFromString(value, Typed.BelfoldiAdoszam); }                                            
        }
                   
           
        /// <summary>
        /// KozossegiAdoszam property
        /// f_kaiasz - K�z�ss�gi (EU) ad�sz�m
        /// </summary>
        public String KozossegiAdoszam
        {
            get { return Utility.GetStringFromSqlString(Typed.KozossegiAdoszam); }
            set { Typed.KozossegiAdoszam = Utility.SetSqlStringFromString(value, Typed.KozossegiAdoszam); }                                            
        }
                   
           
        /// <summary>
        /// MaganszemelyAdoazonositoJel property
        /// f_adoazon - Mag�nszem�ly ad�azon. jele
        /// </summary>
        public String MaganszemelyAdoazonositoJel
        {
            get { return Utility.GetStringFromSqlString(Typed.MaganszemelyAdoazonositoJel); }
            set { Typed.MaganszemelyAdoazonositoJel = Utility.SetSqlStringFromString(value, Typed.MaganszemelyAdoazonositoJel); }                                            
        }
                   
           
        /// <summary>
        /// HelyrajziSzam property
        /// f_telex - Helyrajzi sz�m
        /// </summary>
        public String HelyrajziSzam
        {
            get { return Utility.GetStringFromSqlString(Typed.HelyrajziSzam); }
            set { Typed.HelyrajziSzam = Utility.SetSqlStringFromString(value, Typed.HelyrajziSzam); }                                            
        }
                   
           
        /// <summary>
        /// Bankszamlaszam1 property
        /// f_pbsz1 - Banksz�mlasz�m1
        /// </summary>
        public String Bankszamlaszam1
        {
            get { return Utility.GetStringFromSqlString(Typed.Bankszamlaszam1); }
            set { Typed.Bankszamlaszam1 = Utility.SetSqlStringFromString(value, Typed.Bankszamlaszam1); }                                            
        }
                   
           
        /// <summary>
        /// Bankszamlaszam2 property
        /// f_pbsz2 - Banksz�mlasz�m2
        /// </summary>
        public String Bankszamlaszam2
        {
            get { return Utility.GetStringFromSqlString(Typed.Bankszamlaszam2); }
            set { Typed.Bankszamlaszam2 = Utility.SetSqlStringFromString(value, Typed.Bankszamlaszam2); }                                            
        }
                   
           
        /// <summary>
        /// Bankszamlaszam3 property
        /// f_pbsz3 - Banksz�mlasz�m3
        /// </summary>
        public String Bankszamlaszam3
        {
            get { return Utility.GetStringFromSqlString(Typed.Bankszamlaszam3); }
            set { Typed.Bankszamlaszam3 = Utility.SetSqlStringFromString(value, Typed.Bankszamlaszam3); }                                            
        }
                   
           
        /// <summary>
        /// Telefonszam property
        /// f_tel1 - Telefonsz�m1
        /// </summary>
        public String Telefonszam
        {
            get { return Utility.GetStringFromSqlString(Typed.Telefonszam); }
            set { Typed.Telefonszam = Utility.SetSqlStringFromString(value, Typed.Telefonszam); }                                            
        }
                   
           
        /// <summary>
        /// Fax property
        /// f_fax - Fax
        /// </summary>
        public String Fax
        {
            get { return Utility.GetStringFromSqlString(Typed.Fax); }
            set { Typed.Fax = Utility.SetSqlStringFromString(value, Typed.Fax); }                                            
        }
                   
           
        /// <summary>
        /// EmailCim property
        /// f_email - E-mail c�m
        /// </summary>
        public String EmailCim
        {
            get { return Utility.GetStringFromSqlString(Typed.EmailCim); }
            set { Typed.EmailCim = Utility.SetSqlStringFromString(value, Typed.EmailCim); }                                            
        }
                   
           
        /// <summary>
        /// PIRAllapot property
        /// f_fstatus - PIR szerinti �llapot (F fogadott, U elutas�tott,R rontott)
        /// </summary>
        public String PIRAllapot
        {
            get { return Utility.GetStringFromSqlChars(Typed.PIRAllapot); }
            set { Typed.PIRAllapot = Utility.SetSqlCharsFromString(value, Typed.PIRAllapot); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// f_mgj - Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }

        /// <summary>
        /// VevoBesorolas property
        /// Nincs PIR megfelel�je
        /// </summary>
        public String VevoBesorolas
        {
            get { return Utility.GetStringFromSqlString(Typed.VevoBesorolas); }
            set { Typed.VevoBesorolas = Utility.SetSqlStringFromString(value, Typed.VevoBesorolas); }
        }
    }
   
}