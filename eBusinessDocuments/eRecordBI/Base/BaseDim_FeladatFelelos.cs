
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_FeladatFelelos BusinessDocument Class </summary>
    [Serializable()]
    public class BaseDim_FeladatFelelos
    {
        [System.Xml.Serialization.XmlType("BaseDim_FeladatFelelosBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _FelelosTipusKod = true;
               public bool FelelosTipusKod
               {
                   get { return _FelelosTipusKod; }
                   set { _FelelosTipusKod = value; }
               }
                                                            
                                 
               private bool _FelelosTipusNev = true;
               public bool FelelosTipusNev
               {
                   get { return _FelelosTipusNev; }
                   set { _FelelosTipusNev = value; }
               }
                                                            
                                 
               private bool _Szervezet_Id = true;
               public bool Szervezet_Id
               {
                   get { return _Szervezet_Id; }
                   set { _Szervezet_Id = value; }
               }
                                                            
                                 
               private bool _SzervezetNev = true;
               public bool SzervezetNev
               {
                   get { return _SzervezetNev; }
                   set { _SzervezetNev = value; }
               }
                                                            
                                 
               private bool _Felhasznalo_Id = true;
               public bool Felhasznalo_Id
               {
                   get { return _Felhasznalo_Id; }
                   set { _Felhasznalo_Id = value; }
               }
                                                            
                                 
               private bool _FelhasznaloNev = true;
               public bool FelhasznaloNev
               {
                   get { return _FelhasznaloNev; }
                   set { _FelhasznaloNev = value; }
               }
                                                            
                                 
               private bool _FrissitesIdo = true;
               public bool FrissitesIdo
               {
                   get { return _FrissitesIdo; }
                   set { _FrissitesIdo = value; }
               }
                                                            
                                 
               private bool _Modositas = true;
               public bool Modositas
               {
                   get { return _Modositas; }
                   set { _Modositas = value; }
               }
                                                            
                                 
               private bool _ETL_Load_Id = true;
               public bool ETL_Load_Id
               {
                   get { return _ETL_Load_Id; }
                   set { _ETL_Load_Id = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   FelelosTipusKod = Value;               
                    
                   FelelosTipusNev = Value;               
                    
                   Szervezet_Id = Value;               
                    
                   SzervezetNev = Value;               
                    
                   Felhasznalo_Id = Value;               
                    
                   FelhasznaloNev = Value;               
                    
                   FrissitesIdo = Value;               
                    
                   Modositas = Value;               
                    
                   ETL_Load_Id = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseDim_FeladatFelelosBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlInt32 _Id = SqlInt32.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlInt32 Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlChars _FelelosTipusKod = SqlChars.Null;
           
        /// <summary>
        /// FelelosTipusKod Base property </summary>
            public SqlChars FelelosTipusKod
            {
                get { return _FelelosTipusKod; }
                set { _FelelosTipusKod = value; }                                                        
            }        
                   
           
        private SqlString _FelelosTipusNev = SqlString.Null;
           
        /// <summary>
        /// FelelosTipusNev Base property </summary>
            public SqlString FelelosTipusNev
            {
                get { return _FelelosTipusNev; }
                set { _FelelosTipusNev = value; }                                                        
            }        
                   
           
        private SqlGuid _Szervezet_Id = SqlGuid.Null;
           
        /// <summary>
        /// Szervezet_Id Base property </summary>
            public SqlGuid Szervezet_Id
            {
                get { return _Szervezet_Id; }
                set { _Szervezet_Id = value; }                                                        
            }        
                   
           
        private SqlString _SzervezetNev = SqlString.Null;
           
        /// <summary>
        /// SzervezetNev Base property </summary>
            public SqlString SzervezetNev
            {
                get { return _SzervezetNev; }
                set { _SzervezetNev = value; }                                                        
            }        
                   
           
        private SqlGuid _Felhasznalo_Id = SqlGuid.Null;
           
        /// <summary>
        /// Felhasznalo_Id Base property </summary>
            public SqlGuid Felhasznalo_Id
            {
                get { return _Felhasznalo_Id; }
                set { _Felhasznalo_Id = value; }                                                        
            }        
                   
           
        private SqlString _FelhasznaloNev = SqlString.Null;
           
        /// <summary>
        /// FelhasznaloNev Base property </summary>
            public SqlString FelhasznaloNev
            {
                get { return _FelhasznaloNev; }
                set { _FelhasznaloNev = value; }                                                        
            }        
                   
           
        private SqlDateTime _FrissitesIdo = SqlDateTime.Null;
           
        /// <summary>
        /// FrissitesIdo Base property </summary>
            public SqlDateTime FrissitesIdo
            {
                get { return _FrissitesIdo; }
                set { _FrissitesIdo = value; }                                                        
            }        
                   
           
        private SqlDateTime _Modositas = SqlDateTime.Null;
           
        /// <summary>
        /// Modositas Base property </summary>
            public SqlDateTime Modositas
            {
                get { return _Modositas; }
                set { _Modositas = value; }                                                        
            }        
                   
           
        private SqlInt32 _ETL_Load_Id = SqlInt32.Null;
           
        /// <summary>
        /// ETL_Load_Id Base property </summary>
            public SqlInt32 ETL_Load_Id
            {
                get { return _ETL_Load_Id; }
                set { _ETL_Load_Id = value; }                                                        
            }        
                           }
    }    
}