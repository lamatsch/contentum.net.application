
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_LejaratKategoria BusinessDocument Class </summary>
    [Serializable()]
    public class BaseDim_LejaratKategoria
    {
        [System.Xml.Serialization.XmlType("BaseDim_LejaratKategoriaBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Sorrend = true;
               public bool Sorrend
               {
                   get { return _Sorrend; }
                   set { _Sorrend = value; }
               }
                                                            
                                 
               private bool _TetelJel = true;
               public bool TetelJel
               {
                   get { return _TetelJel; }
                   set { _TetelJel = value; }
               }
                                                            
                                 
               private bool _LejaratKod = true;
               public bool LejaratKod
               {
                   get { return _LejaratKod; }
                   set { _LejaratKod = value; }
               }
                                                            
                                 
               private bool _LejaratNev = true;
               public bool LejaratNev
               {
                   get { return _LejaratNev; }
                   set { _LejaratNev = value; }
               }
                                                            
                                 
               private bool _ElteresErtek = true;
               public bool ElteresErtek
               {
                   get { return _ElteresErtek; }
                   set { _ElteresErtek = value; }
               }
                                                            
                                 
               private bool _ElteresNev = true;
               public bool ElteresNev
               {
                   get { return _ElteresNev; }
                   set { _ElteresNev = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Sorrend = Value;               
                    
                   TetelJel = Value;               
                    
                   LejaratKod = Value;               
                    
                   LejaratNev = Value;               
                    
                   ElteresErtek = Value;               
                    
                   ElteresNev = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseDim_LejaratKategoriaBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlInt32 _Id = SqlInt32.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlInt32 Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Sorrend = SqlInt32.Null;
           
        /// <summary>
        /// Sorrend Base property </summary>
            public SqlInt32 Sorrend
            {
                get { return _Sorrend; }
                set { _Sorrend = value; }                                                        
            }        
                   
           
        private SqlInt32 _TetelJel = SqlInt32.Null;
           
        /// <summary>
        /// TetelJel Base property </summary>
            public SqlInt32 TetelJel
            {
                get { return _TetelJel; }
                set { _TetelJel = value; }                                                        
            }        
                   
           
        private SqlChars _LejaratKod = SqlChars.Null;
           
        /// <summary>
        /// LejaratKod Base property </summary>
            public SqlChars LejaratKod
            {
                get { return _LejaratKod; }
                set { _LejaratKod = value; }                                                        
            }        
                   
           
        private SqlString _LejaratNev = SqlString.Null;
           
        /// <summary>
        /// LejaratNev Base property </summary>
            public SqlString LejaratNev
            {
                get { return _LejaratNev; }
                set { _LejaratNev = value; }                                                        
            }        
                   
           
        private SqlInt32 _ElteresErtek = SqlInt32.Null;
           
        /// <summary>
        /// ElteresErtek Base property </summary>
            public SqlInt32 ElteresErtek
            {
                get { return _ElteresErtek; }
                set { _ElteresErtek = value; }                                                        
            }        
                   
           
        private SqlString _ElteresNev = SqlString.Null;
           
        /// <summary>
        /// ElteresNev Base property </summary>
            public SqlString ElteresNev
            {
                get { return _ElteresNev; }
                set { _ElteresNev = value; }                                                        
            }        
                           }
    }    
}