
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_Prioritas BusinessDocument Class </summary>
    [Serializable()]
    public class BaseDim_Prioritas
    {
        [System.Xml.Serialization.XmlType("BaseDim_PrioritasBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _PrioritasSzint = true;
               public bool PrioritasSzint
               {
                   get { return _PrioritasSzint; }
                   set { _PrioritasSzint = value; }
               }
                                                            
                                 
               private bool _PrioritasSzintNev = true;
               public bool PrioritasSzintNev
               {
                   get { return _PrioritasSzintNev; }
                   set { _PrioritasSzintNev = value; }
               }
                                                            
                                 
               private bool _Prioritas = true;
               public bool Prioritas
               {
                   get { return _Prioritas; }
                   set { _Prioritas = value; }
               }


               private bool _PrioritasNev = true;
               public bool PrioritasNev
               {
                   get { return _PrioritasNev; }
                   set { _PrioritasNev = value; }
               }


               private bool _ETL_Load_Id = true;
               public bool ETL_Load_Id
               {
                   get { return _ETL_Load_Id; }
                   set { _ETL_Load_Id = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   PrioritasSzint = Value;               
                    
                   PrioritasSzintNev = Value;               
                    
                   Prioritas = Value;

                   PrioritasNev = Value; 
                    
                   ETL_Load_Id = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseDim_PrioritasBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlInt32 _Id = SqlInt32.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlInt32 Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _PrioritasSzint = SqlInt32.Null;
           
        /// <summary>
        /// PrioritasSzint Base property </summary>
            public SqlInt32 PrioritasSzint
            {
                get { return _PrioritasSzint; }
                set { _PrioritasSzint = value; }                                                        
            }        
                   
           
        private SqlString _PrioritasSzintNev = SqlString.Null;
           
        /// <summary>
        /// PrioritasSzintNev Base property </summary>
            public SqlString PrioritasSzintNev
            {
                get { return _PrioritasSzintNev; }
                set { _PrioritasSzintNev = value; }                                                        
            }        
                   
           
        private SqlInt32 _Prioritas = SqlInt32.Null;
           
        /// <summary>
        /// Prioritas Base property </summary>
            public SqlInt32 Prioritas
            {
                get { return _Prioritas; }
                set { _Prioritas = value; }                                                        
            }


       private SqlString _PrioritasNev = SqlString.Null;

        /// <summary>
        /// PrioritasNev Base property </summary>
            public SqlString PrioritasNev
            {
                get { return _PrioritasNev; }
                set { _PrioritasNev = value; }
            }    
           
        private SqlInt32 _ETL_Load_Id = SqlInt32.Null;
           
        /// <summary>
        /// ETL_Load_Id Base property </summary>
            public SqlInt32 ETL_Load_Id
            {
                get { return _ETL_Load_Id; }
                set { _ETL_Load_Id = value; }                                                        
            }        
                           }
    }    
}