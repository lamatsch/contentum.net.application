
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Fact_Feladatok BusinessDocument Class </summary>
    [Serializable()]
    public class BaseFact_Feladatok
    {
        [System.Xml.Serialization.XmlType("BaseFact_FeladatokBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _FeladatFelelos_Id = true;
               public bool FeladatFelelos_Id
               {
                   get { return _FeladatFelelos_Id; }
                   set { _FeladatFelelos_Id = value; }
               }
                                                            
                                 
               private bool _FrissitesDatum_Id = true;
               public bool FrissitesDatum_Id
               {
                   get { return _FrissitesDatum_Id; }
                   set { _FrissitesDatum_Id = value; }
               }
                                                            
                                 
               private bool _FeladatTipus_Id = true;
               public bool FeladatTipus_Id
               {
                   get { return _FeladatTipus_Id; }
                   set { _FeladatTipus_Id = value; }
               }
                                                            
                                 
               private bool _FeladatStatusz_Id = true;
               public bool FeladatStatusz_Id
               {
                   get { return _FeladatStatusz_Id; }
                   set { _FeladatStatusz_Id = value; }
               }
                                                            
                                 
               private bool _LejaratKategoria_Id = true;
               public bool LejaratKategoria_Id
               {
                   get { return _LejaratKategoria_Id; }
                   set { _LejaratKategoria_Id = value; }
               }
                                                            
                                 
               private bool _Prioritas_Id = true;
               public bool Prioritas_Id
               {
                   get { return _Prioritas_Id; }
                   set { _Prioritas_Id = value; }
               }
                                                            
                                 
               private bool _Telelszam = true;
               public bool Telelszam
               {
                   get { return _Telelszam; }
                   set { _Telelszam = value; }
               }
                                                            
                                 
               private bool _Obj_Id = true;
               public bool Obj_Id
               {
                   get { return _Obj_Id; }
                   set { _Obj_Id = value; }
               }
                                                            
                                 
               private bool _ETL_Load_Id = true;
               public bool ETL_Load_Id
               {
                   get { return _ETL_Load_Id; }
                   set { _ETL_Load_Id = value; }
               }
                                                            
                                 
               private bool _LetrehozasIdo = true;
               public bool LetrehozasIdo
               {
                   get { return _LetrehozasIdo; }
                   set { _LetrehozasIdo = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   FeladatFelelos_Id = Value;               
                    
                   FrissitesDatum_Id = Value;               
                    
                   FeladatTipus_Id = Value;               
                    
                   FeladatStatusz_Id = Value;               
                    
                   LejaratKategoria_Id = Value;               
                    
                   Prioritas_Id = Value;               
                    
                   Telelszam = Value;               
                    
                   Obj_Id = Value;               
                    
                   ETL_Load_Id = Value;               
                    
                   LetrehozasIdo = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseFact_FeladatokBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlInt32 _FeladatFelelos_Id = SqlInt32.Null;
           
        /// <summary>
        /// FeladatFelelos_Id Base property </summary>
            public SqlInt32 FeladatFelelos_Id
            {
                get { return _FeladatFelelos_Id; }
                set { _FeladatFelelos_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _FrissitesDatum_Id = SqlInt32.Null;
           
        /// <summary>
        /// FrissitesDatum_Id Base property </summary>
            public SqlInt32 FrissitesDatum_Id
            {
                get { return _FrissitesDatum_Id; }
                set { _FrissitesDatum_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeladatTipus_Id = SqlInt32.Null;
           
        /// <summary>
        /// FeladatTipus_Id Base property </summary>
            public SqlInt32 FeladatTipus_Id
            {
                get { return _FeladatTipus_Id; }
                set { _FeladatTipus_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _FeladatStatusz_Id = SqlInt32.Null;
           
        /// <summary>
        /// FeladatStatusz_Id Base property </summary>
            public SqlInt32 FeladatStatusz_Id
            {
                get { return _FeladatStatusz_Id; }
                set { _FeladatStatusz_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _LejaratKategoria_Id = SqlInt32.Null;
           
        /// <summary>
        /// LejaratKategoria_Id Base property </summary>
            public SqlInt32 LejaratKategoria_Id
            {
                get { return _LejaratKategoria_Id; }
                set { _LejaratKategoria_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Prioritas_Id = SqlInt32.Null;
           
        /// <summary>
        /// Prioritas_Id Base property </summary>
            public SqlInt32 Prioritas_Id
            {
                get { return _Prioritas_Id; }
                set { _Prioritas_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Telelszam = SqlInt32.Null;
           
        /// <summary>
        /// Telelszam Base property </summary>
            public SqlInt32 Telelszam
            {
                get { return _Telelszam; }
                set { _Telelszam = value; }                                                        
            }        
                   
           
        private SqlString _Obj_Id = SqlString.Null;
           
        /// <summary>
        /// Obj_Id Base property </summary>
            public SqlString Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _ETL_Load_Id = SqlInt32.Null;
           
        /// <summary>
        /// ETL_Load_Id Base property </summary>
            public SqlInt32 ETL_Load_Id
            {
                get { return _ETL_Load_Id; }
                set { _ETL_Load_Id = value; }                                                        
            }        
                   
           
        private SqlDateTime _LetrehozasIdo = SqlDateTime.Null;
           
        /// <summary>
        /// LetrehozasIdo Base property </summary>
            public SqlDateTime LetrehozasIdo
            {
                get { return _LetrehozasIdo; }
                set { _LetrehozasIdo = value; }                                                        
            }        
                           }
    }    
}