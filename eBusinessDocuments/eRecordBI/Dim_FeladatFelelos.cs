
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_FeladatFelelos BusinessDocument Class </summary>
    [Serializable()]
    public class Dim_FeladatFelelos : BaseDim_FeladatFelelos
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Id); }
            set { Typed.Id = Utility.SetSqlInt32FromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// FelelosTipusKod property </summary>
        public String FelelosTipusKod
        {
            get { return Utility.GetStringFromSqlChars(Typed.FelelosTipusKod); }
            set { Typed.FelelosTipusKod = Utility.SetSqlCharsFromString(value, Typed.FelelosTipusKod); }                                            
        }
                   
           
        /// <summary>
        /// FelelosTipusNev property </summary>
        public String FelelosTipusNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FelelosTipusNev); }
            set { Typed.FelelosTipusNev = Utility.SetSqlStringFromString(value, Typed.FelelosTipusNev); }                                            
        }
                   
           
        /// <summary>
        /// Szervezet_Id property </summary>
        public String Szervezet_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Szervezet_Id); }
            set { Typed.Szervezet_Id = Utility.SetSqlGuidFromString(value, Typed.Szervezet_Id); }                                            
        }
                   
           
        /// <summary>
        /// SzervezetNev property </summary>
        public String SzervezetNev
        {
            get { return Utility.GetStringFromSqlString(Typed.SzervezetNev); }
            set { Typed.SzervezetNev = Utility.SetSqlStringFromString(value, Typed.SzervezetNev); }                                            
        }
                   
           
        /// <summary>
        /// Felhasznalo_Id property </summary>
        public String Felhasznalo_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Felhasznalo_Id); }
            set { Typed.Felhasznalo_Id = Utility.SetSqlGuidFromString(value, Typed.Felhasznalo_Id); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloNev property </summary>
        public String FelhasznaloNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FelhasznaloNev); }
            set { Typed.FelhasznaloNev = Utility.SetSqlStringFromString(value, Typed.FelhasznaloNev); }                                            
        }
                   
           
        /// <summary>
        /// FrissitesIdo property </summary>
        public String FrissitesIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.FrissitesIdo); }
            set { Typed.FrissitesIdo = Utility.SetSqlDateTimeFromString(value, Typed.FrissitesIdo); }                                            
        }
                   
           
        /// <summary>
        /// Modositas property </summary>
        public String Modositas
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Modositas); }
            set { Typed.Modositas = Utility.SetSqlDateTimeFromString(value, Typed.Modositas); }                                            
        }
                   
           
        /// <summary>
        /// ETL_Load_Id property </summary>
        public String ETL_Load_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ETL_Load_Id); }
            set { Typed.ETL_Load_Id = Utility.SetSqlInt32FromString(value, Typed.ETL_Load_Id); }                                            
        }
                           }
   
}