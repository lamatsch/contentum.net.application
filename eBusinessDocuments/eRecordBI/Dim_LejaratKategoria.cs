
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_LejaratKategoria BusinessDocument Class </summary>
    [Serializable()]
    public class Dim_LejaratKategoria : BaseDim_LejaratKategoria
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Id); }
            set { Typed.Id = Utility.SetSqlInt32FromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Sorrend property </summary>
        public String Sorrend
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Sorrend); }
            set { Typed.Sorrend = Utility.SetSqlInt32FromString(value, Typed.Sorrend); }                                            
        }
                   
           
        /// <summary>
        /// TetelJel property </summary>
        public String TetelJel
        {
            get { return Utility.GetStringFromSqlInt32(Typed.TetelJel); }
            set { Typed.TetelJel = Utility.SetSqlInt32FromString(value, Typed.TetelJel); }                                            
        }
                   
           
        /// <summary>
        /// LejaratKod property </summary>
        public String LejaratKod
        {
            get { return Utility.GetStringFromSqlChars(Typed.LejaratKod); }
            set { Typed.LejaratKod = Utility.SetSqlCharsFromString(value, Typed.LejaratKod); }                                            
        }
                   
           
        /// <summary>
        /// LejaratNev property </summary>
        public String LejaratNev
        {
            get { return Utility.GetStringFromSqlString(Typed.LejaratNev); }
            set { Typed.LejaratNev = Utility.SetSqlStringFromString(value, Typed.LejaratNev); }                                            
        }
                   
           
        /// <summary>
        /// ElteresErtek property </summary>
        public String ElteresErtek
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ElteresErtek); }
            set { Typed.ElteresErtek = Utility.SetSqlInt32FromString(value, Typed.ElteresErtek); }                                            
        }
                   
           
        /// <summary>
        /// ElteresNev property </summary>
        public String ElteresNev
        {
            get { return Utility.GetStringFromSqlString(Typed.ElteresNev); }
            set { Typed.ElteresNev = Utility.SetSqlStringFromString(value, Typed.ElteresNev); }                                            
        }
                           }
   
}