
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Dim_Prioritas BusinessDocument Class </summary>
    [Serializable()]
    public class Dim_Prioritas : BaseDim_Prioritas
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Id); }
            set { Typed.Id = Utility.SetSqlInt32FromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// PrioritasSzint property </summary>
        public String PrioritasSzint
        {
            get { return Utility.GetStringFromSqlInt32(Typed.PrioritasSzint); }
            set { Typed.PrioritasSzint = Utility.SetSqlInt32FromString(value, Typed.PrioritasSzint); }                                            
        }
                   
           
        /// <summary>
        /// PrioritasSzintNev property </summary>
        public String PrioritasSzintNev
        {
            get { return Utility.GetStringFromSqlString(Typed.PrioritasSzintNev); }
            set { Typed.PrioritasSzintNev = Utility.SetSqlStringFromString(value, Typed.PrioritasSzintNev); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas property </summary>
        public String Prioritas
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Prioritas); }
            set { Typed.Prioritas = Utility.SetSqlInt32FromString(value, Typed.Prioritas); }                                            
        }

        /// <summary>
        /// PrioritasNev property </summary>
        public String PrioritasNev
        {
            get { return Utility.GetStringFromSqlString(Typed.PrioritasNev); }
            set { Typed.PrioritasNev = Utility.SetSqlStringFromString(value, Typed.PrioritasNev); }
        }

        /// <summary>
        /// ETL_Load_Id property </summary>
        public String ETL_Load_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ETL_Load_Id); }
            set { Typed.ETL_Load_Id = Utility.SetSqlInt32FromString(value, Typed.ETL_Load_Id); }                                            
        }
                           }
   
}