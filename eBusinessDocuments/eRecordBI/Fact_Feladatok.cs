
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// Fact_Feladatok BusinessDocument Class </summary>
    [Serializable()]
    public class Fact_Feladatok : BaseFact_Feladatok
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// FeladatFelelos_Id property </summary>
        public String FeladatFelelos_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladatFelelos_Id); }
            set { Typed.FeladatFelelos_Id = Utility.SetSqlInt32FromString(value, Typed.FeladatFelelos_Id); }                                            
        }
                   
           
        /// <summary>
        /// FrissitesDatum_Id property </summary>
        public String FrissitesDatum_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FrissitesDatum_Id); }
            set { Typed.FrissitesDatum_Id = Utility.SetSqlInt32FromString(value, Typed.FrissitesDatum_Id); }                                            
        }
                   
           
        /// <summary>
        /// FeladatTipus_Id property </summary>
        public String FeladatTipus_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladatTipus_Id); }
            set { Typed.FeladatTipus_Id = Utility.SetSqlInt32FromString(value, Typed.FeladatTipus_Id); }                                            
        }
                   
           
        /// <summary>
        /// FeladatStatusz_Id property </summary>
        public String FeladatStatusz_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.FeladatStatusz_Id); }
            set { Typed.FeladatStatusz_Id = Utility.SetSqlInt32FromString(value, Typed.FeladatStatusz_Id); }                                            
        }
                   
           
        /// <summary>
        /// LejaratKategoria_Id property </summary>
        public String LejaratKategoria_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.LejaratKategoria_Id); }
            set { Typed.LejaratKategoria_Id = Utility.SetSqlInt32FromString(value, Typed.LejaratKategoria_Id); }                                            
        }
                   
           
        /// <summary>
        /// Prioritas_Id property </summary>
        public String Prioritas_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Prioritas_Id); }
            set { Typed.Prioritas_Id = Utility.SetSqlInt32FromString(value, Typed.Prioritas_Id); }                                            
        }
                   
           
        /// <summary>
        /// Telelszam property </summary>
        public String Telelszam
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Telelszam); }
            set { Typed.Telelszam = Utility.SetSqlInt32FromString(value, Typed.Telelszam); }                                            
        }
                   
           
        /// <summary>
        /// Obj_Id property </summary>
        public String Obj_Id
        {
            get { return Utility.GetStringFromSqlString(Typed.Obj_Id); }
            set { Typed.Obj_Id = Utility.SetSqlStringFromString(value, Typed.Obj_Id); }                                            
        }
                   
           
        /// <summary>
        /// ETL_Load_Id property </summary>
        public String ETL_Load_Id
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ETL_Load_Id); }
            set { Typed.ETL_Load_Id = Utility.SetSqlInt32FromString(value, Typed.ETL_Load_Id); }                                            
        }
                   
           
        /// <summary>
        /// LetrehozasIdo property </summary>
        public String LetrehozasIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LetrehozasIdo); }
            set { Typed.LetrehozasIdo = Utility.SetSqlDateTimeFromString(value, Typed.LetrehozasIdo); }                                            
        }
                           }
   
}