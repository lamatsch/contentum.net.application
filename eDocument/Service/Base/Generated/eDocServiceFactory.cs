
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eDocument.Service
{

    public partial class ServiceFactory : IDisposable
    {
        void IDisposable.Dispose() { }
        public void Dispose() { }

        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        public KRT_DokumentumokService GetKRT_DokumentumokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumokService _Service = new KRT_DokumentumokService(_BusinessServiceUrl + "KRT_DokumentumokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_AlairtDokumentumokService GetKRT_AlairtDokumentumokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_AlairtDokumentumokService _Service = new KRT_AlairtDokumentumokService(_BusinessServiceUrl + "KRT_AlairtDokumentumokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public KRT_DokuAlairasKapcsolatokService GetKRT_DokuAlairasKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokuAlairasKapcsolatokService _Service = new KRT_DokuAlairasKapcsolatokService(_BusinessServiceUrl + "KRT_DokuAlairasKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public DokumentumAlairasService GetDokumentumAlairasService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                DokumentumAlairasService _Service = new DokumentumAlairasService(_BusinessServiceUrl + "DokumentumAlairasService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public KRT_DokumentumKapcsolatokService GetKRT_DokumentumKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumKapcsolatokService _Service = new KRT_DokumentumKapcsolatokService(_BusinessServiceUrl + "KRT_DokumentumKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public UCMDocumentService GetUCMDocumentService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                UCMDocumentService _Service = new UCMDocumentService(_BusinessServiceUrl + "UCMDocumentService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_DokumentumAlairasokService GetKRT_DokumentumAlairasService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_DokumentumAlairasokService _Service = new KRT_DokumentumAlairasokService(_BusinessServiceUrl + "DokumentumAlairasService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
    }
}