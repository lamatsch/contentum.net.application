﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eDocument.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_Dokumentumok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_DokumentumokService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        #region GetAllWithExtensionAndJogosultak

        private System.Threading.SendOrPostCallback GetAllWithExtensionAndJogosultakOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionAndJogosultakCompletedEventHandler GetAllWithExtensionAndJogosultakCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eDocument.WebService/GetAllWithExtensionAndJogosultak", RequestNamespace = "Contentum.eDocument.WebService", ResponseNamespace = "Contentum.eDocument.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtensionAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak)
        {
            object[] results = this.Invoke("GetAllWithExtensionAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        jogosultak});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtensionAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtensionAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        jogosultak}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtensionAndJogosultak(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAndJogosultakAsync(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak)
        {
            this.GetAllWithExtensionAndJogosultakAsync(execParam, krt_DokumentumokSearch, jogosultak, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAndJogosultakAsync(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak, object userState)
        {
            if ((this.GetAllWithExtensionAndJogosultakOperationCompleted == null))
            {
                this.GetAllWithExtensionAndJogosultakOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionAndJogosultakOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtensionAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        jogosultak}, this.GetAllWithExtensionAndJogosultakOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionAndJogosultakOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionAndJogosultakCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionAndJogosultakCompleted(this, new GetAllWithExtensionAndJogosultakCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionAndJogosultakCompletedEventHandler(object sender, GetAllWithExtensionAndJogosultakCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionAndJogosultakCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionAndJogosultakCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetWithRightCheck

        private System.Threading.SendOrPostCallback GetWithRightCheckOperationCompleted;

        /// <remarks/>
        public event GetWithRightCheckCompletedEventHandler GetWithRightCheckCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eDocument.WebService/GetWithRightCheck", RequestNamespace = "Contentum.eDocument.WebService", ResponseNamespace = "Contentum.eDocument.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
        {
            object[] results = this.Invoke("GetWithRightCheck", new object[] {
                        execParam,
                        Jogszint});
            return ((Result)(results[0]));
        }


        /// <remarks/>
        public System.IAsyncResult BeginGetWithRightCheck(ExecParam execParam, char Jogszint, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetWithRightCheck", new object[] {
                        execParam,
                        Jogszint}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetWithRightCheck(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetWithRightCheckAsync(ExecParam execParam, char Jogszint)
        {
            this.GetWithRightCheckAsync(execParam, Jogszint, null);
        }

        /// <remarks/>
        public void GetWithRightCheckAsync(ExecParam execParam, char Jogszint, object userState)
        {
            if ((this.GetWithRightCheckOperationCompleted == null))
            {
                this.GetWithRightCheckOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetWithRightCheckOperationCompleted);
            }
            this.InvokeAsync("GetWithRightCheck", new object[] {
                        execParam,
                        Jogszint}, this.GetWithRightCheckOperationCompleted, userState);
        }

        private void OnGetWithRightCheckOperationCompleted(object arg)
        {
            if ((this.GetWithRightCheckCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetWithRightCheckCompleted(this, new GetWithRightCheckCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetWithRightCheckCompletedEventHandler(object sender, GetWithRightCheckCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetWithRightCheckCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetWithRightCheckCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region DokumentumHierarchiaGetAll

        private System.Threading.SendOrPostCallback DokumentumHierarchiaGetAllOperationCompleted;

        /// <remarks/>
        public event DokumentumHierarchiaGetAllCompletedEventHandler DokumentumHierarchiaGetAllCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eDocument.WebService/DokumentumHierarchiaGetAll", RequestNamespace = "Contentum.eDocument.WebService", ResponseNamespace = "Contentum.eDocument.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result DokumentumHierarchiaGetAll(ExecParam execParam, string dokumentumId, char standalone)
        {
            object[] results = this.Invoke("DokumentumHierarchiaGetAll", new object[] {
                        execParam,
                        dokumentumId,
                        standalone});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDokumentumHierarchiaGetAll(ExecParam execParam, string dokumentumId, char standalone, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DokumentumHierarchiaGetAll", new object[] {
                        execParam,
                        dokumentumId,
                        standalone}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndDokumentumHierarchiaGetAll(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void DokumentumHierarchiaGetAllAsync(ExecParam execParam, string dokumentumId, char standalone)
        {
            this.DokumentumHierarchiaGetAllAsync(execParam, dokumentumId, standalone, null);
        }

        /// <remarks/>
        public void DokumentumHierarchiaGetAllAsync(ExecParam execParam, string dokumentumId, char standalone, object userState)
        {
            if ((this.DokumentumHierarchiaGetAllOperationCompleted == null))
            {
                this.DokumentumHierarchiaGetAllOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDokumentumHierarchiaGetAllOperationCompleted);
            }
            this.InvokeAsync("DokumentumHierarchiaGetAll", new object[] {
                        execParam,
                        dokumentumId,
                        standalone}, this.DokumentumHierarchiaGetAllOperationCompleted, userState);
        }

        private void OnDokumentumHierarchiaGetAllOperationCompleted(object arg)
        {
            if ((this.DokumentumHierarchiaGetAllCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DokumentumHierarchiaGetAllCompleted(this, new DokumentumHierarchiaGetAllCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void DokumentumHierarchiaGetAllCompletedEventHandler(object sender, DokumentumHierarchiaGetAllCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class DokumentumHierarchiaGetAllCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal DokumentumHierarchiaGetAllCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAllWithExtensionAndMetaAndJogosultak
        private System.Threading.SendOrPostCallback GetAllWithExtensionAndMetaAndJogosultakOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionAndMetaAndJogosultakCompletedEventHandler GetAllWithExtensionAndMetaAndJogosultakCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eDocument.WebService/GetAllWithExtensionAndMetaAndJogosultak", RequestNamespace = "Contentum.eDocument.WebService", ResponseNamespace = "Contentum.eDocument.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtensionAndMetaAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, string Obj_Id, string ObjMetaDefinicio_Id, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, bool bStandaloneMode, bool jogosultak)
        {
            object[] results = this.Invoke("GetAllWithExtensionAndMetaAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        Obj_Id,
                        ObjMetaDefinicio_Id,
                        ColumnName,
                        ColumnValues,
                        bColumnValuesExclude,
                        DefinicioTipus,
                        bCsakSajatSzint,
                        bCsakAutomatikus,
                        bStandaloneMode,
                        jogosultak});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtensionAndMetaAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, string Obj_Id, string ObjMetaDefinicio_Id, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, bool bStandaloneMode, bool jogosultak, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtensionAndMetaAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        Obj_Id,
                        ObjMetaDefinicio_Id,
                        ColumnName,
                        ColumnValues,
                        bColumnValuesExclude,
                        DefinicioTipus,
                        bCsakSajatSzint,
                        bCsakAutomatikus,
                        bStandaloneMode,
                        jogosultak}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtensionAndMetaAndJogosultak(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAndMetaAndJogosultakAsync(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, string Obj_Id, string ObjMetaDefinicio_Id, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, bool bStandaloneMode, bool jogosultak)
        {
            this.GetAllWithExtensionAndMetaAndJogosultakAsync(execParam, krt_DokumentumokSearch, Obj_Id, ObjMetaDefinicio_Id, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus, bStandaloneMode, jogosultak, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAndMetaAndJogosultakAsync(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, string Obj_Id, string ObjMetaDefinicio_Id, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, bool bStandaloneMode, bool jogosultak, object userState)
        {
            if ((this.GetAllWithExtensionAndMetaAndJogosultakOperationCompleted == null))
            {
                this.GetAllWithExtensionAndMetaAndJogosultakOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionAndMetaAndJogosultakOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtensionAndMetaAndJogosultak", new object[] {
                        execParam,
                        krt_DokumentumokSearch,
                        Obj_Id,
                        ObjMetaDefinicio_Id,
                        ColumnName,
                        ColumnValues,
                        bColumnValuesExclude,
                        DefinicioTipus,
                        bCsakSajatSzint,
                        bCsakAutomatikus,
                        bStandaloneMode,
                        jogosultak}, this.GetAllWithExtensionAndMetaAndJogosultakOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionAndMetaAndJogosultakOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionAndMetaAndJogosultakCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionAndMetaAndJogosultakCompleted(this, new GetAllWithExtensionAndMetaAndJogosultakCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionAndMetaAndJogosultakCompletedEventHandler(object sender, GetAllWithExtensionAndMetaAndJogosultakCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionAndMetaAndJogosultakCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionAndMetaAndJogosultakCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion GetAllWithExtensionAndMetaAndJogosultak
    }
}