public class Constants : Contentum.eUtility.Constants
{
    /// FsWs-ben kellenek a fileInfohoz:
    public const string VERZIO = "VERZIO";
    public const string CHECKOUT = "CHECKOUT";
    public const string FSBASELOCATION = "FileSystemBaseLocation";

    /// DocumentService-ben:
    public const string MERET_MB = "MB"; //?
    public const string NORMAL = "NORMAL";
}