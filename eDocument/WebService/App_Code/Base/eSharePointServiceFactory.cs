using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eUtility;

/// <summary>
/// Summary description for eSharePointServiceFactory
/// </summary>
public class eSharePointService
{
	public eSharePointService()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public class BusinessService
    {
        private string _Type = "";

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        private string _Url = "";

        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        private string _Authentication = "";

        public string Authentication
        {
            get { return _Authentication; }
            set { _Authentication = value; }
        }
        private string _UserDomain = "";

        public string UserDomain
        {
            get { return _UserDomain; }
            set { _UserDomain = value; }
        }
        private string _UserName = "";

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Password = "";

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

    }

    public static Contentum.eSharePoint.Service.ServiceFactory GetServiceFactory(BusinessService BS)
    {
        return GeteSharePointServiceFactory(BS);
    }

    public static Contentum.eSharePoint.Service.ServiceFactory ServiceFactory
    {
        get { return GeteSharePointServiceFactory(); }
    }

    private static Contentum.eSharePoint.Service.ServiceFactory GeteSharePointServiceFactory()
    {
        BusinessService BS = new BusinessService();
        BS.Type = UI.GetAppSetting("eSharePointBusinessServiceType");
        BS.Url = UI.GetAppSetting("eSharePointBusinessServiceUrl");
        BS.Authentication = UI.GetAppSetting("eSharePointBusinessServiceAuthentication");
        BS.UserDomain = UI.GetAppSetting("eSharePointBusinessServiceUserDomain");
        BS.UserName = UI.GetAppSetting("eSharePointBusinessServiceUserName");
        BS.Password = UI.GetAppSetting("eSharePointBusinessServicePassword");
        return GeteSharePointServiceFactory(BS);
    }

    private static Contentum.eSharePoint.Service.ServiceFactory GeteSharePointServiceFactory(BusinessService BS)
    {
        Contentum.eSharePoint.Service.ServiceFactory sc = new Contentum.eSharePoint.Service.ServiceFactory();
        sc.BusinessServiceType = BS.Type;
        sc.BusinessServiceUrl = BS.Url;
        sc.BusinessServiceAuthentication = BS.Authentication;
        sc.BusinessServiceUserDomain = BS.UserDomain;
        sc.BusinessServiceUserName = BS.UserName;
        sc.BusinessServicePassword = BS.Password;
        return sc;
    }
}
