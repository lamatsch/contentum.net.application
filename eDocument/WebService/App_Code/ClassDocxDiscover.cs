﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO.Packaging;
using System.Xml;
using System.IO;
using Contentum.eBusinessDocuments;
//using Microsoft.Office.DocumentFormat.OpenXml.Packaging;   //OpenXML

/// <summary>
/// ClassDocxDiscover
/// A docx (xmlben leirt word dokumentum :)) fileok kezelese. Kiszedi a tartalomtipus id-t.
/// </summary>
//namespace Contentum.eDocument.WebService
//{
    public class ClassDocxDiscover: IDisposable
    {
        public const string Header = "$Header: ClassDocxDiscover.cs, 9, 2010.02.17 13:52:26, Varsanyi P?ter$";
        public const string Version = "$Revision: 9$";

        void IDisposable.Dispose() { }
        public void Dispose() { }

        const string customPropertiesRelationshipType = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties";
        const string customPropertiesSchema = "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";
        const string customVTypesSchema = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";

        const string metadataContentType = "http://schemas.microsoft.com/office/2006/metadata/contentType";
        const string customXmlProperties = "http://schemas.openxmlformats.org/officedocument/2006/relationships/customXmlProps";

        public ClassDocxDiscover()
        {
            Logger.Info(String.Format("DocumentService.ClassDocxDiscover start.- {0} {1}", Header, Version));
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Megadott byte-tombbe alakitott docx-bol kiveszi a content type-id-t.
        /// Ha nincs, vagy nem talalja, akkor null-t ad vissza.
        /// </summary>
        /// <param name="docxS">Byte tomben levo docx dokumentum.</param>
        /// <returns>String, ami az id vagy ures.</returns>
        /// 
        public string GetContentTypeId(byte[] docxS)
        {
            Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId start."));

            //const string customPropertiesRelationshipType = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties";
            //const string customPropertiesSchema = "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";
            //const string customVTypesSchema = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";

            //const string metadataContentType = "http://schemas.microsoft.com/office/2006/metadata/contentType";
            //const string customXmlProperties = "http://schemas.openxmlformats.org/officedocument/2006/relationships/customXmlProps";

            string propertyValue = string.Empty;
            PackagePart customPropertiesPart = null;

            //  memoria streambe tesszuk a byte tombot
            Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId: memoryStream-me konvertalas elott."));
            MemoryStream ms = new MemoryStream();
            ms.Write(docxS, 0, docxS.Length);
            ms.Position = 0;

            //  az memoria stream feldogozasa, a megfelelo resz kivetele

            Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId: memstream megnyitasa."));
            using (Package wdPackage = Package.Open(ms))
            {

                Uri targetUri = null; //new Uri("/customXml/item1.xml");

                Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId: start while."));

                //  keresett resz:.....
                bool megMehet = true;
                int fileItemNameProp = 1;

                while (megMehet)
                {
                    targetUri = null;
                    customPropertiesPart = null;

                    string itemName = String.Format("/customXml/item{0}.xml", fileItemNameProp);
                    Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId: itemName: {0}.",itemName));

                    Logger.Info(String.Format("ClassDocxDiscover.GetContentTypeId: wdPackage.GetParts()."));

                    foreach (PackagePart pp in wdPackage.GetParts())
                    {
                        //Console.WriteLine(String.Format("//  Ez itt az: {0}", pp.Uri));
                        if (String.Format("{0}", pp.Uri).Equals(itemName))
                        {
                            targetUri = pp.Uri;
                        }
                    } // foreach

                    //  megvan a target
                    if (targetUri != null)
                    {
                        Logger.Info(String.Format("megvan a keresett resz. A megtalalt resz kivetele."));

                        Uri documentUri = PackUriHelper.ResolvePartUri(new Uri("/", UriKind.Relative), targetUri);
                        customPropertiesPart = wdPackage.GetPart(documentUri);

                        //Console.WriteLine(String.Format("Eremeny: {0}", customPropertiesPart.ContentType));
                    }
                    else
                    {
                        megMehet = false;
                    }


                    //  ha megvan a keresett resz, akkor kivesszuk belole, ami kell
                    if (customPropertiesPart != null)
                    {
                        Logger.Info(String.Format("A megtalalt reszben a CTT id keresese."));

                        //  Manage namespaces to perform Xml XPath queries.
                        NameTable nt = new NameTable();
                        XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
                        nsManager.AddNamespace("d", customPropertiesSchema);
                        nsManager.AddNamespace("ct", metadataContentType);

                        //  Get the properties from the package.
                        XmlDocument xdoc = new XmlDocument(nt);

                        //  Load the XML in the part into an XmlDocument instance.
                        xdoc.Load(customPropertiesPart.GetStream());

                        string searchString = string.Format("ct:contentTypeSchema");
                        //Console.WriteLine(String.Format("Count: {0}", xdoc.SelectNodes(searchString, nsManager).Count));
                        if (xdoc.SelectNodes(searchString, nsManager).Count == 1)
                        {
                            XmlNode xNode = xdoc.SelectSingleNode(searchString, nsManager);
                            
                            //Console.WriteLine(String.Format("CTT id: {0}", xNode.Attributes["ma:contentTypeID"].Value));
                            propertyValue = String.Format("{0}", xNode.Attributes["ma:contentTypeID"].Value);
                            Logger.Info(String.Format("Megvan a CTT id: {0}", propertyValue));
                            megMehet = false;
                        }
                        else
                        {
                            Logger.Info(String.Format("Nincs meg, mert a keresett string ({0}) talalat nem 1, hanem: {1}", searchString, xdoc.SelectNodes(searchString, nsManager).Count));
                        }
                    }

                    fileItemNameProp++;
                } //while



            }
            return propertyValue;
        }


        /// <summary>
        /// Sablonazonosito megkeresese a word docbol.
        /// </summary>
        /// <param name="docxS"></param>
        /// <returns></returns>
        public string GetSablonazonosito(byte[] docxS)
        {
            Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito start."));

            string propertyValue = string.Empty;
            PackagePart customPropertiesPart = null;

            //  memoria streambe tesszuk a byte tombot
            Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: memoryStream-me konvertalas elott."));
            MemoryStream ms = new MemoryStream();
            ms.Write(docxS, 0, docxS.Length);
            ms.Position = 0;

            //  az memoria stream feldogozasa, a megfelelo resz kivetele

            Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: memstream megnyitasa."));
            using (Package wdPackage = Package.Open(ms))
            {

                Uri targetUri = null; //new Uri("/customXml/item1.xml");

                Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: start while."));

                //  keresett resz:.....
                bool megMehet = true;
                int fileItemNameProp = 1;

                while (megMehet)
                {
                    targetUri = null;
                    customPropertiesPart = null;

                    string itemName = String.Format("/customXml/item{0}.xml", fileItemNameProp);
                    Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: itemName: {0}.", itemName));

                    Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: wdPackage.GetParts()."));

                    foreach (PackagePart pp in wdPackage.GetParts())
                    {
                        //Console.WriteLine(String.Format("//  Ez itt az: {0}", pp.Uri));
                        if (String.Format("{0}", pp.Uri).Equals(itemName))
                        {
                            targetUri = pp.Uri;
                        }
                    } // foreach

                    //  megvan a target
                    if (targetUri != null && String.IsNullOrEmpty(propertyValue))
                    {
                        Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: megvan a keresett resz. A megtalalt resz kivetele."));

                        Uri documentUri = PackUriHelper.ResolvePartUri(new Uri("/", UriKind.Relative), targetUri);
                        customPropertiesPart = wdPackage.GetPart(documentUri);

                        //Console.WriteLine(String.Format("Eremeny: {0}", customPropertiesPart.ContentType));
                    }
                    else
                    {
                        megMehet = false;
                    }


                    //  ha megvan a keresett resz, akkor kivesszuk belole, ami kell
                    if (customPropertiesPart != null)
                    {
                        Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: A megtalalt reszben a ssablonazonsotio keresese."));

                        //  Manage namespaces to perform Xml XPath queries.
                        NameTable nt = new NameTable();
                        XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
                        nsManager.AddNamespace("p", "http://schemas.microsoft.com/office/2006/metadata/properties");
                        nsManager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                        nsManager.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/v3");

                        //  Get the properties from the package.
                        XmlDocument xdoc = new XmlDocument(nt);

                        //  Load the XML in the part into an XmlDocument instance.
                        xdoc.Load(customPropertiesPart.GetStream());

                        if (xdoc.SelectNodes("/p:properties/documentManagement", nsManager).Count == 1)
                        {
                            Logger.Info("ClassDocxDiscover.GetSablonazonosito: xmlD.SelectSingleNode(\"/p:properties/documentManagement\" node kivetele elott.");
                            XmlNode dm = xdoc.SelectSingleNode("/p:properties/documentManagement", nsManager);

                            Logger.Info("ClassDocxDiscover.GetSablonazonosito: *[local-name()='edok_w_sablonazonosito'] count elott.");
                            if (dm.SelectNodes("*[local-name()='edok_w_sablonazonosito']", nsManager).Count == 1)
                            {
                                Logger.Info("ClassDocxDiscover.GetSablonazonosito: van edok_w_sablonazonosito node: ertek kivetele elott.");
                                propertyValue = String.Format("{0}", dm.SelectNodes("*[local-name()='edok_w_sablonazonosito']", nsManager)[0].InnerText);
                                Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: kapott ertek: {0}", propertyValue));
                                megMehet = false;
                            }
                            else
                            {
                                Logger.Info("ClassDocxDiscover.GetSablonazonosito: nincs edok_w_sablonazonosito node.");
                            }

                            //string retVal = String.Empty;
                            //int y = 0;

                            //while (String.IsNullOrEmpty(retVal) && y < dm.ChildNodes.Count)
                            //{

                            //    Console.WriteLine(dm.ChildNodes[y].LocalName);
                            //    if (dm.ChildNodes[y].LocalName.Equals("edok_w_sablonazonosito"))
                            //    {
                            //        retVal = Convert.ToString(dm.ChildNodes[y].InnerText);
                            //        y = dm.ChildNodes.Count;
                            //    }

                            //    y++;
                            //} //while
                        }
                        else
                        {
                            Logger.Info(String.Format("ClassDocxDiscover.GetSablonazonosito: nincs meg a /p:properties/documentManagement resz."));
                        }

                    }

                    fileItemNameProp++;
                } //while



            }
            return propertyValue;
        }

        /// <summary>
        /// Egy megadott property erteket beallitja a docxben.
        /// </summary>
        /// <param name="docxS"></param>
        /// <param name="propInternalName"></param>
        /// <param name="propValue"></param>
        /// <returns></returns>
        public Result SetMetaProperty(byte[] docxS, String propInternalName, String propValue)
        {
            Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty start."));

            Result result = new Result();
            PackagePart customPropertiesPart = null;

            //  memoria streambe tesszuk a byte tombot
            Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty: memoryStream-me konvertalas elott."));
            MemoryStream ms = new MemoryStream();
            ms.Write(docxS, 0, docxS.Length);
            ms.Position = 0;

            //  az memoria stream feldogozasa, a megfelelo resz kivetele

            Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty: memstream megnyitasa."));
            using (Package wdPackage = Package.Open(ms))
            {

                Uri targetUri = null; //new Uri("/customXml/item1.xml");

                Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty: start while."));

                //  keresett resz:.....
                bool megMehet = true;
                int fileItemNameProp = 1;

                while (megMehet)
                {
                    targetUri = null;
                    customPropertiesPart = null;

                    string itemName = String.Format("/customXml/item{0}.xml", fileItemNameProp);
                    Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty: itemName: {0}.", itemName));

                    Logger.Info(String.Format("ClassDocxDiscover.SetMetaProperty: wdPackage.GetParts()."));

                    foreach (PackagePart pp in wdPackage.GetParts())
                    {
                        //Console.WriteLine(String.Format("//  Ez itt az: {0}", pp.Uri));
                        if (String.Format("{0}", pp.Uri).Equals(itemName))
                        {
                            targetUri = pp.Uri;
                        }
                    } // foreach

                    //  megvan a target
                    if (targetUri != null)
                    {
                        Logger.Info(String.Format("megvan a keresett resz. A megtalalt resz kivetele."));

                        Uri documentUri = PackUriHelper.ResolvePartUri(new Uri("/", UriKind.Relative), targetUri);
                        customPropertiesPart = wdPackage.GetPart(documentUri);

                        //Console.WriteLine(String.Format("Eremeny: {0}", customPropertiesPart.ContentType));
                    }
                    else
                    {
                        megMehet = false;
                    }


                    //  ha megvan a keresett resz, akkor kivesszuk belole, ami kell
                    if (customPropertiesPart != null)
                    {
                        Logger.Info(String.Format("A megtalalt reszben a CTT id keresese."));

                        //  Manage namespaces to perform Xml XPath queries.
                        NameTable nt = new NameTable();
                        XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
                        nsManager.AddNamespace("d", customPropertiesSchema);
                        nsManager.AddNamespace("ct", metadataContentType);

                        //  Get the properties from the package.
                        XmlDocument xdoc = new XmlDocument(nt);

                        //  Load the XML in the part into an XmlDocument instance.
                        xdoc.Load(customPropertiesPart.GetStream());

                        string searchString = string.Format("ct:contentTypeSchema");
                        //Console.WriteLine(String.Format("Count: {0}", xdoc.SelectNodes(searchString, nsManager).Count));
                        if (xdoc.SelectNodes(searchString, nsManager).Count == 1)
                        {
                            XmlNode xNode = xdoc.SelectSingleNode(searchString, nsManager);

                            //Console.WriteLine(String.Format("CTT id: {0}", xNode.Attributes["ma:contentTypeID"].Value));
                            String propertyValue = String.Format("{0}", xNode.Attributes["ma:contentTypeID"].Value);
                            Logger.Info(String.Format("Megvan a CTT id: {0}", propertyValue));
                            megMehet = false;
                        }
                        else
                        {
                            Logger.Info(String.Format("Nincs meg, mert a keresett string ({0}) talalat nem 1, hanem: {1}", searchString, xdoc.SelectNodes(searchString, nsManager).Count));
                        }
                    }

                    fileItemNameProp++;
                } //while



            }
            return result;
        }

        //public Result GetMSFileContentSHA1()
        //{
        //    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
        //    {
        //        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
        //        CommentsPart commentsPart = mainPart.CommentsPart;

        //        using (StreamReader streamReader = new StreamReader(commentsPart.GetStream()))
        //        {
        //            comments = streamReader.ReadToEnd();
        //        }
        //    }

        //}

    } //class
//}