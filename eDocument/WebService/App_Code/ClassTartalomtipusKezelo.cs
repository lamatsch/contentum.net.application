﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using Contentum.eBusinessDocuments;

/// <summary>
/// ClassTartalomtipusKezelo
/// A tartalomtipusokat csoportosito XML leiro kezeleset tartalmazo eljarasok.
/// </summary>
//namespace Contentum.eDocument.WebService
//{
public class ClassTartalomtipusKezelo : IDisposable
    {
        public const string Header = "$Header: ClassTartalomtipusKezelo.cs, 6, 2010.02.17 13:52:27, Varsanyi P?ter$";
        public const string Version = "$Revision: 6$";

        //  a leirast tartalmazo xml dokumentum
        private XmlDocument ttCsoportokatLeiroXml;
        private bool ttCsoportokatLeiroXmlHiba;

        void IDisposable.Dispose() { }
        public void Dispose() { }

        public ClassTartalomtipusKezelo()
        {
            Logger.Info(String.Format("DocumentService.ClassTartalomtipusKezelo start.- {0} {1}", Header, Version));

            string sablonTarhely = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SablontarTarHely");
            Logger.Info(String.Format("A webconfigbol kapott xml leiro helye: {0}", sablonTarhely));

            //  ha eseteleg nincs, akkor alapbol a filesystemben keresunk
            sablonTarhely = (String.IsNullOrEmpty(sablonTarhely)) ? "FILESYSTEM" : sablonTarhely;

            Logger.Info(String.Format("xml leiro helye: {0}", sablonTarhely));

            this.ttCsoportokatLeiroXmlHiba = false;
            string xmlLeiroFileNeve = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("TTcsoportokXmlMapFile");

            Logger.Info(String.Format("Leiro XML file: {0}", xmlLeiroFileNeve));

            //  SPS
            if (sablonTarhely.Equals("SPS"))
            {
                string xmlFileTartalom = String.Empty;

                try
                {
                    System.Net.WebClient client = new System.Net.WebClient();
                    client.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                    xmlFileTartalom = client.DownloadString(xmlLeiroFileNeve);

                    Logger.Info(String.Format("Leiro XML file megszerzese SPSbol OK."));
                }
                catch (Exception ex)
                {
                    Logger.Warn(String.Format("Hiba a tartalomtipusokat csoportositoleiro allomany spsbol valo megszerzesekor! Ha nem letezik nem hiba, mert akkor nincs csoportositas (nem kotelezo)."));
                    Logger.Warn(String.Format("Ahiba oka: message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                    this.ttCsoportokatLeiroXmlHiba = true;
                }

                if (this.ttCsoportokatLeiroXmlHiba == false)
                {
                    try
                    {
                        Logger.Info(String.Format("Leiro XML file konvertalasa xmldocca elott."));

                        this.ttCsoportokatLeiroXml = new XmlDocument();

                        //  spsben tarolt eseten:
                        this.ttCsoportokatLeiroXml.LoadXml(xmlFileTartalom);
                        Logger.Info(String.Format("Leiro XML file konvertalasa xmldocca OK."));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba a tartalomtipusokat csoportositoleiro allomany megnyitasakor (es xml-e konvertalasakor)!"));
                        Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                        this.ttCsoportokatLeiroXmlHiba = true;
                    }
                }

            } //if SPS


            //  FILESYSTEM
            if (sablonTarhely.Equals("FILESYSTEM"))
            {

                try
                {
                    Logger.Info(String.Format("Leiro XML file megnyitasa elott."));

                    this.ttCsoportokatLeiroXml = new XmlDocument();
                    //  filerendszeren tarolt xml leiro esete:
                    this.ttCsoportokatLeiroXml.Load(xmlLeiroFileNeve);

                    Logger.Info(String.Format("Leiro XML file megnyitasa OK."));
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Hiba a leiro megnyitasakor!"));
                    Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                    this.ttCsoportokatLeiroXmlHiba = true;
                }
            } //if FILESYSTEM

        }

        /// <summary>
        /// SPS ctt id alapjan megnezi a file-ban hol van definialva.
        /// Ha nem talalja meg, lekeri az SPSbol es az alapjan ad majd DocLib nevet!
        /// Tehat mindenkeppen visszaad valamilyen string tartalmat!
        /// </summary>
        /// <param name="cttId"></param>
        /// <returns></returns>
        public string GetStoreDocLibNameByCTTId(string cttId)
        {
            Logger.Info(String.Format("ClassTartalomtipusKezelo.GetStoreDocLibNameByCTTId indul."));
            Logger.Info(String.Format("keresett id: {0}", cttId));

            if (this.ttCsoportokatLeiroXmlHiba) 
            {
                Logger.Info(String.Format("ttCsoportokatLeiroXmlHiba=true -> hiba volt a leiro olvasasanal!!!"));
                return "";
            }
            
            //  csoport ?

            int talaltCsoportDb = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//ttcsoport[@id='{0}']", cttId)).Count;
            Logger.Info(String.Format("Csoportra kereses db: {0}", talaltCsoportDb));
            
            if (talaltCsoportDb == 1)
            {
                XmlNode node = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//ttcsoport[@id='{0}']", cttId))[0];

                string thnev = Convert.ToString(node.Attributes["tarhelynev"].Value);

                Logger.Info(String.Format("Talalt csoport alapjan a tarhelynev: {0}", thnev));
                return thnev;
            }


            //  tag ?
            
            int talaltTagDb = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//tag[@id='{0}']", cttId)).Count;
            Logger.Info(String.Format("Tagra kereses db: {0}", talaltTagDb));

            if (talaltTagDb == 1)
            {
                XmlNode node = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//tag[@id='{0}']", cttId))[0];

                string thnev = Convert.ToString(node.ParentNode.Attributes["tarhelynev"].Value);

                Logger.Info(String.Format("Talalt tag alapjan a tarhelynev: {0}", thnev));
                return thnev;
            }

            Logger.Info(String.Format("Nincs a csoportosito XML-ben a keresett CTT!!!"));

            return "";
        }

        /// <summary>
        /// A megadott név alapján keres az XML mapben.
        /// Ugyanaz, mint az Id alajan kereso.
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        public string GetStoreDocLibNameByName(string sName)
        {
            Logger.Info(String.Format("ClassTartalomtipusKezelo.GetStoreDocLibNameByName indul."));
            Logger.Info(String.Format("keresett nev: {0}", sName));

            if (this.ttCsoportokatLeiroXmlHiba)
            {
                Logger.Info(String.Format("ttCsoportokatLeiroXmlHiba=true -> hiba volt a leiro olvasasanal!!!"));
                return "";
            }

            //  csoport ?

            int talaltCsoportDb = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//ttcsoport[@nev='{0}']", sName)).Count;
            Logger.Info(String.Format("Csoportra kereses db: {0}", talaltCsoportDb));

            if (talaltCsoportDb == 1)
            {
                XmlNode node = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//ttcsoport[@nev='{0}']", sName))[0];

                string thnev = Convert.ToString(node.Attributes["tarhelynev"].Value);

                Logger.Info(String.Format("Talalt csoport alapjan a tarhelynev: {0}", thnev));
                return thnev;
            }


            //  tag ?

            int talaltTagDb = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//tag[@nev='{0}']", sName)).Count;
            Logger.Info(String.Format("Tagra kereses db: {0}", talaltTagDb));

            if (talaltTagDb == 1)
            {
                XmlNode node = this.ttCsoportokatLeiroXml.SelectNodes(String.Format("//tag[@nev='{0}']", sName))[0];

                string thnev = Convert.ToString(node.ParentNode.Attributes["tarhelynev"].Value);

                Logger.Info(String.Format("Talalt tag alapjan a tarhelynev: {0}", thnev));
                return thnev;
            }

            Logger.Info(String.Format("Nincs a csoportosito XML-ben a keresett CTT!!!"));


            return "";
        }

    } //class
//}