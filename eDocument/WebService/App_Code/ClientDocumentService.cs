using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

using Contentum.eBusinessDocuments;
using Contentum.eUtility;

using log4net;
using log4net.Config;
using System.Xml;
using System.Net;
using System.Data.SqlClient;


/// <summary>
/// Summary description for ClientDocumentService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ClientDocumentService : System.Web.Services.WebService
{

    public const string Header = "$Header: ClientDocumentService.cs, 10, 2010.12.21 8:20:38, Varsanyi P?ter$";
    public const string Version = "$Revision: 10$";

    public ClientDocumentService()
    {
        Logger.Info(String.Format("DocumentService.ClientDocumentService start.- {0} {1}", Header, Version));
    }

    [WebMethod]
    public string GetUjVonalkod() {
        return "uj_vonalkod_from_server";
    }

    [WebMethod]
    public string RegistringUploadedFile(string path, string fileName)
    {
        return "OK";
    }

    /// <summary>
    /// A kliensen futo VBA scriptnek ez adja meg pontosvesszovel elvalasztott stringkent
    /// az adatokat. Fontos a visszaadott adatok sorrendje!
    /// </summary>
    /// <returns>String - prontosvesszovel visszaadott adatok.</returns>
    [WebMethod]
    public string GetStoreParameters()
    {
        string ret = "";
        ret += System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eDocumentStoreType");
        ret += ";" + System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUrl");
        ret += ";";
        ret += ";" + "Documents";

        return ret;
        
    }

    /// <summary>
    /// Ugyiratkereses.
    /// Azert string tomb a retval, mert nem akartam spec objektumot, mert ha netalantan
    /// VBA-s (fujj!) tud csak majd mukodni (VSTO helyett), akkor ott nem lehet hivatkozni
    /// dll-re. E miatt kicsit meg is kellett eroszakolni a string tomb-ot (lasd a visszateresei
    /// ertek leirasanal).
    /// </summary>
    /// <param name="foszamtol"></param>
    /// <param name="foszamig"></param>
    /// <param name="targy"></param>
    /// <returns>String tomb - ketdimenzios.
    /// Az elso sor (0-dik index 0-dik eleme) mindig informaciot tartalmaz.
    /// Ha minden rendben, akkor egy ;-vel szeparalt "valtozo=ertek" parokat tartalmazo string.
    /// Lehetseges valotozok: OSSZESDARABSZAM
    /// Ha hiba volt, akkor ez a hibat tartalmazza. ;-vel szeparalt.
    /// Van benne egy ERROR tag.
    /// Egy hiba string igy nez ki: ERROR;ERRORCODE=ahibakodja;ERRORMSG=a_hibauzenet_szovege
    /// 
    /// </returns>
    //[WebMethod]
    //public string UgyiratKereses(string foszamtol, string foszamig, string targy)
    //{
    //    DataContext dataContext = new DataContext(this.Application);
    //    FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

    //    Result rete = sp.GetAll("select * from krt_felhasznalok");
        
    //    if (!String.IsNullOrEmpty(rete.ErrorCode)) 
    //    {
    //        return String.Format("Hiba: {0}", rete.ErrorMessage);
    //    }
    //    else 
    //    {
    //        return String.Format("OK: {0}", rete.Ds.Tables.Count);
    //    }
        


    //    XmlDocument xmlDoc = new System.Xml.XmlDocument();
    //    XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
    //    XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
    //    XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
    //    infoXml.InnerXml=String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />",3);
        

    //    //rowsetXml.InnerXml = "<row><val>1</val><val>1234555</val><val>targy mezo 1</val></row>" +
    //    //    "<row><val>2</val><val>123123</val><val>targy mezo 2</val></row>" +
    //    //    "<row><val>3</val><val>1223423434555</val><val>targy mezo 3</val></row>";

 
    //    //Contentum.eRecord.Service.EREC_IraIratokService rs = new Contentum.eRecord.Service.EREC_IraIratokService("asfd");
    //    MySqlConnect mySql = new MySqlConnect();
    //    string selectStr = "select * from krt_felhasznalok";
    //    SqlDataReader rd = null;

    //    try
    //    {
    //        mySql.Open();

    //        try
    //        {
    //            rd = mySql.Query(selectStr); 
    //        }
    //        catch (Exception ex2)
    //        {
    //            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>", 0, "ExecuteSql1", ex2.ToString());
    //            mySql.Close();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>",0,"OpenSql1",ex.ToString());
    //    }
        


    //    if (rd != null)
    //    {
    //        rowsetXml.InnerXml = "";

    //        int darab = 0;
    //        while (rd.Read())
    //        {
    //            string rowStr = "<row>";

    //            for (int i = 0; i < rd.FieldCount; i++)
    //            {
    //                rowStr += String.Format("<val>{0}</val>",rd[i]);
    //            }
    //            rowStr += "</row>";

    //            rowsetXml.InnerXml += rowStr;
    //            darab++;
    //        }

    //        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>", darab, "", "");

    //    }

    //    // eredmeny tomb string tombbe konvertalasa


    //    docc.AppendChild(infoXml);
    //    docc.AppendChild(rowsetXml);
    //    xmlDoc.AppendChild(docc);
    //    return xmlDoc.OuterXml.ToString();
    //}

    [WebMethod]
    public string CimzettKereses(string nev, string cim)
    {
        Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
        FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />", 0);

        //SELECT     part.Id, part.Org, part.Orszag_Id, part.Tipus, part.Nev, part.Hierarchia, part.KulsoAzonositok, part.PublikusKulcs, part.Kiszolgalo, 
        //              part.LetrehozoSzervezet, part.MaxMinosites, part.MinositesKezdDat, part.MinositesVegDat, part.MaxMinositesSzervezet, part.Belso, part.Forras, 
        //              part.Ver, part.Note, part.Stat_id, part.ErvKezd, part.ErvVege, part.Letrehozo_id, part.LetrehozasIdo, part.Modosito_id, part.ModositasIdo, part.Zarolo_id, 
        //              part.ZarolasIdo, part.Tranz_id, part.UIAccessLog_id
        //FROM          KRT_Partnerek AS part LEFT OUTER JOIN
        //              KRT_PartnerCimek AS pc ON part.Id = pc.Partner_id LEFT OUTER JOIN
        //              KRT_Cimek AS cimek ON pc.Cim_Id = cimek.Id

        /// ---------------------------------------------------------------------------------------------
        /// partnerek select

        string seli = "select * from KRT_Partnerek where (tipus='10') ";
        string where = "";

        if (!String.IsNullOrEmpty(nev))
        {
            where += String.Format(" and (nev like '%{0}%')", nev.Trim());
        }

        if (where != "") seli += where;

        Result pResult = sp.GetAll(seli);

        if (!String.IsNullOrEmpty(pResult.ErrorCode))
        {
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}(Partnerek lekerdezesenel.)</errormessage>", 0, pResult.ErrorCode, pResult.ErrorMessage);
        }
        else
        {
            /// ---------------------------------------------------------------------------------------------
            /// ha sok jott -> szukitse tovabb uzenet
            /// 
            int osszdarabszam = (int)pResult.Ds.Tables[0].Rows.Count;
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />", osszdarabszam);

            if (osszdarabszam > 100)
            {
                infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>", osszdarabszam, "1111133", "T�l sok eredm�nysor! Adjon meg t�bb/b�vebb felt�teleket!");
            }
            else
            {

                for (int i = 0; i < osszdarabszam; i++)
                {

                    /// ---------------------------------------------------------------------------------------------
                    /// partner rowset
                    /// 

                    XmlNode partnerXml = xmlDoc.CreateNode(XmlNodeType.Element, "row", "");

                    string partner_id = Convert.ToString(pResult.Ds.Tables[0].Rows[i]["id"]);
                    partnerXml.InnerXml = String.Format("<val>{0}</val><val>{1}</val>"
                        , partner_id
                        , pResult.Ds.Tables[0].Rows[i]["nev"]);


                    /// ---------------------------------------------------------------------------------------------
                    /// cimek select
                    /// 

                    //XmlNode partnerCimekXml = xmlDoc.CreateNode(XmlNodeType.Element, "addresses", "");

                    seli = "select cimek.* FROM KRT_Cimek AS cimek INNER JOIN KRT_PartnerCimek AS pc ON cimek.Id = pc.Cim_Id ";
                    where = "";

                    if (!String.IsNullOrEmpty(cim))
                    {
                        where += String.Format(" where cimek.cim like '%{0}%' and pc.partner_id='{1}'", cim.Trim(), partner_id);
                    }
                    else
                    {
                        where += String.Format(" where pc.partner_id='{0}'", partner_id);
                    }

                    Result cResult = sp.GetAll(seli);

                    if (!String.IsNullOrEmpty(cResult.ErrorCode))
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2} (cimlekerdezesnel!)</errormessage><sql>{3}</sql>", 0, cResult.ErrorCode, cResult.ErrorMessage, seli);
                        docc.AppendChild(infoXml);
                        xmlDoc.AppendChild(docc);
                        return xmlDoc.OuterXml.ToString();
                    }
                    else
                    {
                        XmlNode partnerCimekXml = xmlDoc.CreateNode(XmlNodeType.Element, "addresses", "");

                        if (cResult.Ds.Tables.Count != 0)
                        {

                            for (int j = 0; j < cResult.Ds.Tables[0].Rows.Count; j++)
                            {
                                XmlNode partnerCimXml = xmlDoc.CreateNode(XmlNodeType.Element, "address", "");
                                partnerCimXml.InnerXml = String.Format("<val>{0}</val><val>{1}</val>"
                                    , cResult.Ds.Tables[0].Rows[j]["id"]
                                    , cResult.Ds.Tables[0].Rows[j]["nev"]);

                                partnerCimekXml.AppendChild(partnerCimXml);
                                //InnerXml.Insert(partnerCimekXml.InnerText.Length,String.Format("{0}", partnerCimXml.OuterXml.ToString()));
                                //partnerCimekXml.InnerXml += String.Format("{0}", partnerCimXml.OuterXml.ToString());

                            } //for j



                        } //if

                        //partnerXml.InnerXml += String.Format("{0}", partnerCimekXml.OuterXml.ToString());
                        partnerXml.AppendChild(partnerCimekXml);
                    }

                    //rowsetXml.InnerXml += String.Format("{0}", partnerXml.OuterXml.ToString());
                    rowsetXml.AppendChild(partnerXml);

                } //for i 

                docc.AppendChild(rowsetXml);
            }

        }

        docc.AppendChild(infoXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    [WebMethod]
    public string AlairoKereses(string nev)
    {
        Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
        //KRT_CsoportokSearch

        FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />", 3);

        string seli = "select * from KRT_Csoportok where (tipus='1') ";
        string where = "";

        if (!String.IsNullOrEmpty(nev))
        {
            where += String.Format(" and (nev like '%{0}%')", nev.Trim());
        }

        if (where != "") seli += where;

        Result pResult = sp.GetAll(seli);

        if (!String.IsNullOrEmpty(pResult.ErrorCode))
        {
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}(Partnerek lekerdezesenel.)</errormessage>", 0, pResult.ErrorCode, pResult.ErrorMessage);
        }
        else
        {
            /// ---------------------------------------------------------------------------------------------
            /// ha sok jott -> szukitse tovabb uzenet
            /// 
            int osszdarabszam = (int)pResult.Ds.Tables[0].Rows.Count;
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />", osszdarabszam);

            if (osszdarabszam > 100)
            {
                infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>", osszdarabszam, "1111133", "T�l sok eredm�nysor! Adjon meg t�bb/b�vebb felt�teleket!");
            }
            else
            {

                for (int i = 0; i < osszdarabszam; i++)
                {

                    /// ---------------------------------------------------------------------------------------------
                    /// rowset
                    /// 

                    XmlNode partnerXml = xmlDoc.CreateNode(XmlNodeType.Element, "row", "");

                    string partner_id = Convert.ToString(pResult.Ds.Tables[0].Rows[i]["id"]);
                    partnerXml.InnerXml = String.Format("<val>{0}</val><val>{1}</val>"
                        , partner_id
                        , pResult.Ds.Tables[0].Rows[i]["nev"]);

                    rowsetXml.AppendChild(partnerXml);
                } //for i 

                docc.AppendChild(rowsetXml);
            }
        }

        docc.AppendChild(infoXml);
        docc.AppendChild(rowsetXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    [WebMethod]
    public string UgyiratKereses(string foszamtol, string foszamig, string targy)
    {
        Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
        FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

        //Result rete = sp.GetAll("select * from krt_felhasznalok");

        //if (!String.IsNullOrEmpty(rete.ErrorCode))
        //{
        //    return String.Format("Hiba: {0}", rete.ErrorMessage);
        //}
        //else
        //{
        //    return String.Format("OK: {0}", rete.Ds.Tables[0].Rows.Count);
        //}
        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");

        Result rete = sp.GetAll("select * from krt_felhasznalok"); //erec_ugyugyiratok");
        if (!String.IsNullOrEmpty(rete.ErrorCode))
        {
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>{1}</errorcode><errormessage>{2}</errormessage>", 0, rete.ErrorCode, rete.ErrorMessage);
        }
        else
        {
            infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode /><errormessage />", rete.Ds.Tables[0].Rows.Count);
            rowsetXml.InnerXml = "";
            for (int i = 0; i < rete.Ds.Tables[0].Rows.Count; i++)
            {
                rowsetXml.InnerXml += String.Format("<row><val>{0}</val><val>{1}</val><val>{2}</val></row>"
                    , rete.Ds.Tables[0].Rows[i]["id"]
                    , rete.Ds.Tables[0].Rows[i]["nev"]
                    , rete.Ds.Tables[0].Rows[i]["nev"]);
            }
        }


        docc.AppendChild(infoXml);
        docc.AppendChild(rowsetXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    [WebMethod]
    public string GetVarhatoIktatasAdatok()
    {
        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode vonalkodXml = xmlDoc.CreateNode(XmlNodeType.Element, "vonalkod", "");
        XmlNode iktatoszamXml = xmlDoc.CreateNode(XmlNodeType.Element, "iktatoszam", "");
        XmlNode iktatasDatumaXml = xmlDoc.CreateNode(XmlNodeType.Element, "iktatasdatuma", "");
        infoXml.InnerXml = String.Format("<errorcode /><errormessage />");
        //infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Hu itt a hiba</errormessage>");
        vonalkodXml.InnerText = String.Format("*123456*");
        iktatoszamXml.InnerText = String.Format("IKT1/123-1/2007");
        iktatasDatumaXml.InnerText = String.Format("{0}", DateTime.Today.ToString("yyyy.MM.dd."));
        docc.AppendChild(infoXml);
        docc.AppendChild(vonalkodXml);
        docc.AppendChild(iktatoszamXml);
        docc.AppendChild(iktatasDatumaXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    //[WebMethod]
    //public string Iktatas(string xmlParameterekStr, byte[] fileTartalom)
    //{
    //    XmlDocument xmlParams = new XmlDocument();
    //    xmlParams.LoadXml(xmlParameterekStr);

    //    XmlDocument xmlDoc = new System.Xml.XmlDocument();
    //    XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
    //    XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
    //    infoXml.InnerXml = String.Format("<errorcode /><errormessage />");
    //    //infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Hu itt a hiba</errormessage>");

    //    XmlNode nodeParam = xmlParams.FirstChild;
    //    bool hibavolt = false;
    //    string filename = "";
    //    try
    //    {
    //        filename = nodeParam.SelectSingleNode("filename").InnerText;
    //    }
    //    catch (Exception ex)
    //    {
    //        hibavolt = true;
    //        infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Nincs megadva a filenev!</errormessage>");
    //    }

    //    if (!hibavolt && fileTartalom.Length == 0)
    //    {
    //        hibavolt = true;
    //        infoXml.InnerXml = String.Format("<errorcode>11112</errorcode><errormessage>Ures a file tartalma!</errormessage>");
    //    }

    //    if (!hibavolt)
    //    {
    //        DocumentService ds = new DocumentService();
    //        //ds.Credentials = System.Net.CredentialCache.DefaultCredentials; // NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; 

    //        ExecParam execP = new ExecParam();
    //        execP.Felhasznalo_Id = "f088e89e-7917-4d0b-b94f-f4dac84f0f95";
    //        execP.Alkalmazas_Id = "272277bb-27b7-4689-be45-db5994a0fba9";

    //        Result ret = ds.UploadFromeRecord(execP, Contentum.eUtility.Constants.DocumentStoreType.SharePoint
    //            , "eRecord", "Documents1", "WordTeszt", filename, fileTartalom, null, true);

    //        if (!String.IsNullOrEmpty(ret.ErrorCode))
    //        {
    //            hibavolt = true;
    //            infoXml.InnerXml = String.Format("<errorcode>{0}</errorcode><errormessage>{1}</errormessage>", ret.ErrorCode, ret.ErrorMessage);
    //        }
    //        else
    //        {
    //            XmlNode fileurlXml = xmlDoc.CreateNode(XmlNodeType.Element, "fileurl", "");
    //            fileurlXml.InnerText = String.Format("http://srvcontentumweb.axis.hu/eRecord/Documents1/WordTeszt/{0}", filename);
    //            docc.AppendChild(fileurlXml);
    //        }
    //    }

    //    docc.AppendChild(infoXml);
    //    xmlDoc.AppendChild(docc);
    //    return xmlDoc.OuterXml.ToString();
    //}

    [WebMethod]
    public string CimzettKeresesXmlFeldolgozas()
    {
        string inputStr = "<result><rowset><row><val>8f80602e-31f1-49f5-b91d-6800f3c8c315</val><val>Ingatlangazd�lkod�si Aloszt�ly</val><addresses><address><val>745e2d53-cf33-4523-986f-c9bd586236fd</val><val>1052. Budapest, V�rosh�z utca 9-11</val></address></addresses></row><row><val>d093b485-d55f-4a18-9239-018871887059</val><val>Ingatlanhasznos�t�si Aloszt�ly</val><addresses><address><val>745e2d53-cf33-4523-986f-c9bd586236fd</val><val>1052. Budapest, V�rosh�z utca 9-11</val></address></addresses></row></rowset><info><osszesdarabszam>2</osszesdarabszam><errorcode /><errormessage /></info></result>";

        XmlDocument xmlEredmeny = new XmlDocument();
        try
        {
            xmlEredmeny.LoadXml(inputStr);
        }
        catch (XmlException xe)
        {
            return String.Format("Create XML eredmeny doc exception: {0}", xe.ToString());
        }

        XmlNode nodeResult = xmlEredmeny.FirstChild;
        XmlNode nodeInfo = nodeResult.SelectSingleNode("info");
        XmlNode nodeRowset = nodeResult.SelectSingleNode("rowset");

        string vissza = "";

        for (int i = 0; i < nodeRowset.ChildNodes.Count; i++)
        {
            XmlNode partner = nodeRowset.ChildNodes[i];
            string partner_id = partner.ChildNodes[0].InnerText.ToString();
            string partner_nev = partner.ChildNodes[1].InnerText.ToString();
            XmlNode partner_cimek = partner.ChildNodes[2];
            vissza += String.Format("-{0} partner: {1}", i, partner_nev);

            for (int j = 0; j < partner_cimek.ChildNodes.Count; j++)
            {
                XmlNode partnerCim = partner_cimek.ChildNodes[j];
                string cim_id = partnerCim.ChildNodes[0].InnerText.ToString();
                string cim_nev = partnerCim.ChildNodes[1].InnerText.ToString();
                vissza += String.Format("-{0} cim: {1}", j, cim_nev);

            }

        } //for i

        return vissza;
    }

    [WebMethod]
    public string GetAlairoId(string xmlParameterekStr)
    {
        XmlDocument xmlParams = new XmlDocument();
        xmlParams.LoadXml(xmlParameterekStr);

        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", 0);
        //infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Hu itt a hiba</errormessage>");

        XmlNode nodeParam = xmlParams.FirstChild;
        bool hibavolt = false;
        string name = "";

        try
        {
            name = nodeParam.SelectSingleNode("name").InnerText;
        }
        catch (Exception ex)
        {
            hibavolt = true;
            infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nincs megadva a nev parameter!</errormessage>");
        }

        if (!hibavolt)
        {
            Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
            FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

            string seli = String.Format("SELECT cs.* FROM KRT_Csoportok AS cs where (tipus='1') and (cs.nev like '%{0}%')", name);

            Result pResult = sp.GetAll(seli);

            if (!String.IsNullOrEmpty(pResult.ErrorCode))
            {
                infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>{0}</errorcode><errormessage>{1}</errormessage>", pResult.ErrorCode, pResult.ErrorMessage);
            }
            else
            {
                if (pResult.Ds.Tables.Count != 0)
                {
                    if (pResult.Ds.Tables[0].Rows.Count == 1)
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", pResult.Ds.Tables[0].Rows.Count);
                        XmlNode rowXml = xmlDoc.CreateNode(XmlNodeType.Element, "row", "");
                        XmlNode val1Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");
                        XmlNode val2Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");

                        val1Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["id"]);
                        val2Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["nev"]);

                        rowXml.AppendChild(val1Xml);
                        rowXml.AppendChild(val2Xml);
                        rowsetXml.AppendChild(rowXml);
                    }
                    else
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a sz�veg alapj�n az al��r�. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                    }
                }
                else
                {
                    infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a sz�veg alapj�n az al��r�. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                }
            }
        }

        docc.AppendChild(infoXml);
        docc.AppendChild(rowsetXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    [WebMethod]
    public string GetCimzettId(string xmlParameterekStr)
    {
        XmlDocument xmlParams = new XmlDocument();
        xmlParams.LoadXml(xmlParameterekStr);

        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", 0);
        //infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Hu itt a hiba</errormessage>");

        XmlNode nodeParam = xmlParams.FirstChild;
        bool hibavolt = false;
        string name = "";

        try
        {
            name = nodeParam.SelectSingleNode("name").InnerText;
        }
        catch (Exception ex)
        {
            hibavolt = true;
            infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nincs megadva a nev parameter!</errormessage>");
        }

        if (!hibavolt)
        {
            Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
            FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

            string seli = String.Format("SELECT p.* FROM KRT_Partnerek AS p where (p.tipus='10') and p.nev like '%{0}%'", name);

            Result pResult = sp.GetAll(seli);

            if (!String.IsNullOrEmpty(pResult.ErrorCode))
            {
                infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>{0}</errorcode><errormessage>{1}</errormessage>", pResult.ErrorCode, pResult.ErrorMessage);
            }
            else
            {
                if (pResult.Ds.Tables.Count != 0)
                {
                    if (pResult.Ds.Tables[0].Rows.Count == 1)
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", pResult.Ds.Tables[0].Rows.Count);
                        XmlNode rowXml = xmlDoc.CreateNode(XmlNodeType.Element, "row", "");
                        XmlNode val1Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");
                        XmlNode val2Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");

                        val1Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["id"]);
                        val2Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["nev"]);

                        rowXml.AppendChild(val1Xml);
                        rowXml.AppendChild(val2Xml);
                        rowsetXml.AppendChild(rowXml);
                    }
                    else
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a sz�veg alapj�n a c�mezett. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                    }
                }
                else
                {
                    infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a sz�veg alapj�n a c�mezett. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                }
            }
        }

        docc.AppendChild(infoXml);
        docc.AppendChild(rowsetXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

    [WebMethod]
    public string GetCimzettCimeId(string xmlParameterekStr)
    {
        XmlDocument xmlParams = new XmlDocument();
        xmlParams.LoadXml(xmlParameterekStr);

        XmlDocument xmlDoc = new System.Xml.XmlDocument();
        XmlNode docc = xmlDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode infoXml = xmlDoc.CreateNode(XmlNodeType.Element, "info", "");
        XmlNode rowsetXml = xmlDoc.CreateNode(XmlNodeType.Element, "rowset", "");
        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", 0);
        //infoXml.InnerXml = String.Format("<errorcode>11111</errorcode><errormessage>Hu itt a hiba</errormessage>");

        XmlNode nodeParam = xmlParams.FirstChild;
        bool hibavolt = false;
        string pid = "";
        string name = "";
        try
        {
            pid = nodeParam.SelectSingleNode("id").InnerText;
        }
        catch (Exception ex)
        {
            hibavolt = true;
            infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nincs megadva az id parameter!</errormessage>");
        }

        try
        {
            name = nodeParam.SelectSingleNode("name").InnerText;
        }
        catch (Exception ex)
        {
            hibavolt = true;
            infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nincs megadva a nev parameter!</errormessage>");
        }

        if (!hibavolt)
        {
            Contentum.eUtility.DataContext dataContext = new Contentum.eUtility.DataContext(this.Application);
            FreeQueryStoredProcedure sp = new FreeQueryStoredProcedure(dataContext);

            string seli = String.Format("SELECT cimek.* FROM KRT_Cimek AS cimek INNER JOIN KRT_PartnerCimek AS pc ON cimek.Id = pc.Cim_Id and pc.partner_id='{0}' and cimek.nev like '%{1}%' ", pid, name);

            Result pResult = sp.GetAll(seli);

            if (!String.IsNullOrEmpty(pResult.ErrorCode))
            {
                infoXml.InnerXml = String.Format("<osszesdarabszam>0</osszesdarabszam><errorcode>{0}</errorcode><errormessage>{1}</errormessage>", pResult.ErrorCode, pResult.ErrorMessage);
            }
            else
            {
                if (pResult.Ds.Tables.Count != 0)
                {
                    if (pResult.Ds.Tables[0].Rows.Count == 1)
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode/><errormessage/>", pResult.Ds.Tables[0].Rows.Count);
                        XmlNode rowXml = xmlDoc.CreateNode(XmlNodeType.Element, "row", "");
                        XmlNode val1Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");
                        XmlNode val2Xml = xmlDoc.CreateNode(XmlNodeType.Element, "val", "");

                        val1Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["id"]);
                        val2Xml.InnerText = Convert.ToString(pResult.Ds.Tables[0].Rows[0]["nev"]);

                        rowXml.AppendChild(val1Xml);
                        rowXml.AppendChild(val2Xml);
                        rowsetXml.AppendChild(rowXml);
                    }
                    else
                    {
                        infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a c�m sz�vege alapj�n a c�m. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                    }
                }
                else
                {
                    infoXml.InnerXml = String.Format("<osszesdarabszam>{0}</osszesdarabszam><errorcode>11111</errorcode><errormessage>Nem azonos�that� be a c�m sz�vege alapj�n a c�m. K�rem v�lassza ki!</errormessage>", pResult.Ds.Tables[0].Rows.Count);
                }
            }
        }

        docc.AppendChild(infoXml);
        docc.AppendChild(rowsetXml);
        xmlDoc.AppendChild(docc);
        return xmlDoc.OuterXml.ToString();
    }

}