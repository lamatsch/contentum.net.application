﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;


/// <summary>
/// Summary description for ContentSearchService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ContentSearchService : System.Web.Services.WebService {

    public ContentSearchService () 
    {

    }

    [WebMethod]
    public Result SharepointFileContentSearch(ExecParam execParam, string keywords)
    {
        Logger.Info("SharepointFileContentSearch Start");


        Logger.Info("MossKeresEx indul.");
        Result res = new Result();

        #region MOSS root site-on kereses ws meghivasa

        string searchGuid = String.Empty;

        // hogy az összes találatot visszaadja:
        int start = 1;
        int count = 0;

        String qXMLString = "<QueryPacket xmlns='urn:Microsoft.Search.Query'>" +
        "<Query><QueryId>" + searchGuid + "</QueryId><SupportedFormats><Format revision='1'>" +
        "urn:Microsoft.Search.Response.Document:Document</Format>" +
        "</SupportedFormats><Context><QueryText language='hu-HU' type='STRING'>" +
        keywords + "</QueryText></Context><Range><StartAt>" + Convert.ToString(start) + "</StartAt><Count>" + Convert.ToString(count) + "</Count></Range>" +
        "<TrimDuplicates>false</TrimDuplicates><IncludeSpecialTermResults>true</IncludeSpecialTermResults>" +
        "</Query></QueryPacket>";


        SPSearches.QueryService queryService = new SPSearches.QueryService();
        queryService.Url = String.Format("{0}{1}_vti_bin/Search.asmx", Contentum.eDocument.SharePoint.Utility.GetSharePointUrl(), Contentum.eDocument.SharePoint.Utility.SharePointUrlPostfix());

        Logger.Info(String.Format("queryService.Url: {0}", queryService.Url));

        queryService.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();  // System.Net.CredentialCache.DefaultCredentials;

        System.Data.DataSet queryResults = null;

        try
        {
            queryResults = queryService.QueryEx(qXMLString);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba queryService.QueryEx WS (MOSS) vegrehajtaskor!\n{0}\n{1}", ex.Message, ex.StackTrace));
            Logger.Error(String.Format("Ezzel a query stringgel hivtuk: {0}", qXMLString));
            res.ErrorCode = "QERR00001";
            res.ErrorMessage = String.Format("Hiba queryService.QueryEx webservice (MOSS) végrehajtásakor!\n{0}\n{1}", ex.Message, ex.StackTrace);
            return res;
        }

        res.Ds = queryResults;

        #endregion

        Logger.Info("SharepointFileContentSearch End");

        return res;
    }
    
}

