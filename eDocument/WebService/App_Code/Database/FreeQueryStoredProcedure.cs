using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for FreeQueryStoredProcedure
/// </summary>

public partial class FreeQueryStoredProcedure
{
    private DataContext dataContext;

    public FreeQueryStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    //public Result GetAll(ExecParam ExecParam, string sqlSelectText)
    public Result GetAll(string sqlSelectText)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_FreeQuery");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();

                SqlCommand SqlComm = new SqlCommand("[sp_FreeQuery]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
var conn = new ConnectionStringSettings();
                conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];
                SqlComm.Connection = new SqlConnection(conn.ConnectionString);//"Data Source=.;Initial Catalog=ContentumDev;Integrated Security=True;Min Pool Size=10;Max Pool Size=50;");                //SqlComm.Connection = dataContext.Connection;
                //SqlComm.Transaction = dataContext.Transaction;

                //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, "", 0);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@sqlcmd", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@sqlcmd"].Value = sqlSelectText; 

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        //log.SpEnd(ExecParam,_ret);
        return _ret;

    }
}