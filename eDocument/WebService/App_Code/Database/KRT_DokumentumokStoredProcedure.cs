﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_DokumentumokStoredProcedure
/// </summary>
public partial class KRT_DokumentumokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_DokumentumokGetAllWithExtension");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(krt_DokumentumokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += krt_DokumentumokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, krt_DokumentumokSearch.OrderBy, krt_DokumentumokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char, 1));
            SqlComm.Parameters["@Jogosultak"].Value = jogosultak ? '1' : '0';

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = execParam.Typed.FelhasznaloSzervezet_Id;

            if (krt_DokumentumokSearch.Manual_FajlTartalom != null
                && !string.IsNullOrEmpty(krt_DokumentumokSearch.Manual_FajlTartalom.Filter))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Filter_FileContent", SqlDbType.NVarChar, 400));
                SqlComm.Parameters["@Filter_FileContent"].Value = krt_DokumentumokSearch.Manual_FajlTartalom.Filter;
            }

            if (krt_DokumentumokSearch.Manual_BarCodeFilter != null)
            {
                SqlComm.Parameters.Add(new SqlParameter("@Filter_BarCode", SqlDbType.NVarChar, 20));
                SqlComm.Parameters["@Filter_BarCode"].Value = krt_DokumentumokSearch.Manual_BarCodeFilter.Value;
            }

            if (krt_DokumentumokSearch.Fts_altalanos != null
                && !String.IsNullOrEmpty(krt_DokumentumokSearch.Fts_altalanos.Filter))
            {

                string altalanos_Fts_value = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(
                    krt_DokumentumokSearch.Fts_altalanos.Filter).Normalized;

                SqlComm.Parameters.Add(new SqlParameter("@Altalanos_FTS", SqlDbType.NVarChar));
                SqlComm.Parameters["@Altalanos_FTS"].Value = altalanos_Fts_value;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;

    }

    public Result GetAllWithExtensionAndMeta(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, String ColumnNames, bool bStandaloneMode, bool jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_DokumentumokGetAllWithExtensionAndMeta");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(krt_DokumentumokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += krt_DokumentumokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokGetAllWithExtensionAndMeta]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, krt_DokumentumokSearch.OrderBy, krt_DokumentumokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char, 1));
            SqlComm.Parameters["@Jogosultak"].Value = jogosultak ? '1' : '0';

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = execParam.Typed.FelhasznaloSzervezet_Id;

            if (krt_DokumentumokSearch.Manual_FajlTartalom != null
                && !string.IsNullOrEmpty(krt_DokumentumokSearch.Manual_FajlTartalom.Filter))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Filter_FileContent", SqlDbType.NVarChar, 400));
                SqlComm.Parameters["@Filter_FileContent"].Value = krt_DokumentumokSearch.Manual_FajlTartalom.Filter;
            }

            if (krt_DokumentumokSearch.Fts_altalanos != null
                && !String.IsNullOrEmpty(krt_DokumentumokSearch.Fts_altalanos.Filter))
            {

                string altalanos_Fts_value = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(
                    krt_DokumentumokSearch.Fts_altalanos.Filter).Normalized;

                SqlComm.Parameters.Add(new SqlParameter("@Altalanos_FTS", SqlDbType.NVarChar));
                SqlComm.Parameters["@Altalanos_FTS"].Value = altalanos_Fts_value;
            }

            // WorkAround:
            // mivel az FullTextSearchTree belső fája jelenleg nem sorosítható,
            // már készen, stringként vesszük át a "SELECT" feltételt
            if (!String.IsNullOrEmpty(krt_DokumentumokSearch.ObjektumTargyszavai_ObjIdFilter))
            {
                SqlComm.Parameters.Add(new SqlParameter("@ObjektumTargyszavai_ObjIdFilter", SqlDbType.NVarChar));
                SqlComm.Parameters["@ObjektumTargyszavai_ObjIdFilter"].Value = krt_DokumentumokSearch.ObjektumTargyszavai_ObjIdFilter;
            }

            if (!String.IsNullOrEmpty(ColumnNames))
            {
                SqlComm.Parameters.Add(new SqlParameter("@ColumnNames", SqlDbType.NVarChar));
                SqlComm.Parameters["@ColumnNames"].Value = ColumnNames;
            }

                SqlComm.Parameters.Add(new SqlParameter("@StandaloneMode", SqlDbType.Char, 1));
                SqlComm.Parameters["@StandaloneMode"].Value = bStandaloneMode ? '1' : '0';

            if (!String.IsNullOrEmpty(krt_DokumentumokSearch.Partner_id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Partner_id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Partner_id"].Value = new Guid(krt_DokumentumokSearch.Partner_id);
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;

    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_DokumentumokGetWithRightCheck");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_DokumentumokGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            KRT_Dokumentumok _KRT_Dokumentumok = new KRT_Dokumentumok();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Dokumentumok, SqlComm);
            _ret.Record = _KRT_Dokumentumok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }



    // BUG_10252
    public Result DokumentumHierarchiaGetAll(ExecParam execParam, string dokumentumId, char standalone)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_DokumentumHierarchiaGetAll");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_DokumentumHierarchiaGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;


            SqlComm.Parameters.Add(new SqlParameter("@DokumentumId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@DokumentumId"].Value = System.Data.SqlTypes.SqlGuid.Parse(dokumentumId);

            // BUG_10252
            SqlComm.Parameters.Add(new SqlParameter("@Standalone", SqlDbType.Char, 1));
            SqlComm.Parameters["@Standalone"].Value = standalone;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }



}
