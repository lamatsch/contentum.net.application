using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data.SqlTypes;


/// <summary>
/// Summary description for KRT_EsemenyekStoredProcedure
/// </summary>
public partial class KRT_EsemenyekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_EsemenyekSearch _KRT_EsemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_EsemenyekGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_EsemenyekSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_EsemenyekSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekGetAllWithExtension]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;             
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_EsemenyekSearch.OrderBy, _KRT_EsemenyekSearch.TopRow);

                if (_KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch != null)
                {
                    Query query_ObjTip = new Query();
                    query_ObjTip.BuildFromBusinessDocument(_KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch);
                    query_ObjTip.Where += _KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch.WhereByManual;

                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_ObjTip", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_ObjTip"].Size = 4000;
                    SqlComm.Parameters["@Where_ObjTip"].Value = query_ObjTip.Where;
                }

                if (_KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch != null)
                {
                    Query query_Funkciok = new Query();
                    query_Funkciok.BuildFromBusinessDocument(_KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch);
                    query_Funkciok.Where += _KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch.WhereByManual;

                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Funkcio", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_Funkcio"].Size = 4000;
                    SqlComm.Parameters["@Where_Funkcio"].Value = query_Funkciok.Where;
                }

                if (_KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch != null)
                {
                    Query query_Helyettesitesek = new Query();
                    query_Helyettesitesek.BuildFromBusinessDocument(_KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch);
                    query_Helyettesitesek.Where += _KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch.WhereByManual;

                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Helyettesitesek", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_Helyettesitesek"].Size = 4000;
                    SqlComm.Parameters["@Where_Helyettesitesek"].Value = query_Helyettesitesek.Where;
                }

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetParamsByFunkcioKod(ExecParam ExecParam, string ObjId, string TablaNev, string FunkcioKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_EsemenyekGetParamsByFunkcioKod");

        Result _ret = new Result();
        KRT_Esemenyek _krt_esemenyek = new KRT_Esemenyek();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekGetParamsByFunkcioKod]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ObjId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ObjId"].Value = string.IsNullOrEmpty(ObjId)?SqlGuid.Null:SqlGuid.Parse(ObjId);

        SqlComm.Parameters.Add(new SqlParameter("@TablaNev", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@TablaNev"].Size = 400;
        SqlComm.Parameters["@TablaNev"].Value = TablaNev;

        SqlComm.Parameters.Add(new SqlParameter("@FunkcioKod", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@FunkcioKod"].Size = 400;
        SqlComm.Parameters["@FunkcioKod"].Value = FunkcioKod;

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = SqlComm;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0) return null;

            _krt_esemenyek.Obj_Id = string.IsNullOrEmpty(ObjId)?"":ObjId;
            _krt_esemenyek.ObjTip_Id = ds.Tables[0].Rows[0]["ObjTipId"].ToString();
            _krt_esemenyek.Funkcio_Id = ds.Tables[0].Rows[0]["FunkcioId"].ToString();
            _krt_esemenyek.Azonositoja = ds.Tables[0].Rows[0]["Azonosito"].ToString();
            
            _ret.Record = _krt_esemenyek;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result GetParamsByMuveletKod(ExecParam ExecParam, string ObjId, string TablaNev, string MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_EsemenyekGetParamsByMuveletKod");

        Result _ret = new Result();
        KRT_Esemenyek _krt_esemenyek = new KRT_Esemenyek();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekGetParamsByMuveletKod]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ObjId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ObjId"].Value = string.IsNullOrEmpty(ObjId) ? SqlGuid.Null : SqlGuid.Parse(ObjId);

        SqlComm.Parameters.Add(new SqlParameter("@TablaNev", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@TablaNev"].Size = 400;
        SqlComm.Parameters["@TablaNev"].Value = TablaNev;

        SqlComm.Parameters.Add(new SqlParameter("@MuveletKod", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@MuveletKod"].Size = 400;
        SqlComm.Parameters["@MuveletKod"].Value = MuveletKod;

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = SqlComm;
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0) return null;

            _krt_esemenyek.Obj_Id = string.IsNullOrEmpty(ObjId) ? "" : ObjId;
            _krt_esemenyek.ObjTip_Id = ds.Tables[0].Rows[0]["ObjTipId"].ToString();
            _krt_esemenyek.Funkcio_Id = ds.Tables[0].Rows[0]["FunkcioId"].ToString();
            _krt_esemenyek.Azonositoja = ds.Tables[0].Rows[0]["Azonosito"].ToString();

            _ret.Record = _krt_esemenyek;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result InsertTomeges(ExecParam ExecParam, string ObjIds, string TablaNev, string MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_EsemenyekInsertTomeges");

        Result _ret = new Result();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekInsertTomeges]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ObjIds", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@ObjIds"].Value = ObjIds;

        SqlComm.Parameters.Add(new SqlParameter("@TablaNev", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@TablaNev"].Size = 400;
        SqlComm.Parameters["@TablaNev"].Value = TablaNev;

        SqlComm.Parameters.Add(new SqlParameter("@MuveletKod", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@MuveletKod"].Size = 400;
        SqlComm.Parameters["@MuveletKod"].Value = MuveletKod;

        SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelhasznaloId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

        SqlComm.Parameters.Add(new SqlParameter("@HelyettesitettId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@HelyettesitettId"].Value = SqlGuid.Parse(ExecParam.LoginUser_Id);

        SqlComm.Parameters.Add(new SqlParameter("@FelettesSzervezetId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelettesSzervezetId"].Value = SqlGuid.Parse(ExecParam.FelhasznaloSzervezet_Id);

        if (!String.IsNullOrEmpty(ExecParam.Helyettesites_Id))
        {
            SqlComm.Parameters.Add(new SqlParameter("@HelyettesitesId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@HelyettesitesId"].Value = SqlGuid.Parse(ExecParam.Helyettesites_Id);
        }

        // Tranzakci� Id a Page_Id az ExecParamb�l:
        SqlComm.Parameters.Add(new SqlParameter("@tranzId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@tranzId"].Value = SqlGuid.Parse(ExecParam.Page_Id);

        //M�s�tva: 2016.03.25 csatolm�ny t�rl�s
        // Eddig nem adta �t param�ternek a munka�llom�st �s a m�velet kimenetel�t
        SqlComm.Parameters.Add(new SqlParameter("@MunkaAllomas", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@MunkaAllomas"].Value = ExecParam.UserHostAddress;

        SqlComm.Parameters.Add(new SqlParameter("@MuveletKimenete", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@MuveletKimenete"].Value = "SIKERES";

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result InsertTomegesByFunkcioKod(ExecParam ExecParam, string ObjIds, string FunkcioKod, string Note)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_EsemenyekInsertTomegesByFunkcioKod");

        Result _ret = new Result();

        SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekInsertTomegesByFunkcioKod]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ObjIds", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@ObjIds"].Value = ObjIds;

        SqlComm.Parameters.Add(new SqlParameter("@FunkcioKod", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@FunkcioKod"].Size = 400;
        SqlComm.Parameters["@FunkcioKod"].Value = FunkcioKod;

        SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelhasznaloId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

        SqlComm.Parameters.Add(new SqlParameter("@HelyettesitettId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@HelyettesitettId"].Value = SqlGuid.Parse(ExecParam.LoginUser_Id);

        if (!String.IsNullOrEmpty(ExecParam.Helyettesites_Id))
        {
            SqlComm.Parameters.Add(new SqlParameter("@HelyettesitesId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@HelyettesitesId"].Value = SqlGuid.Parse(ExecParam.Helyettesites_Id);
        }

        SqlComm.Parameters.Add(new SqlParameter("@FelettesSzervezetId", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelettesSzervezetId"].Value = SqlGuid.Parse(ExecParam.FelhasznaloSzervezet_Id);

        // Tranzakci� Id a Page_Id az ExecParamb�l:
        if (!String.IsNullOrEmpty(ExecParam.Page_Id))
        {
            SqlComm.Parameters.Add(new SqlParameter("@tranzId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@tranzId"].Value = SqlGuid.Parse(ExecParam.Page_Id);
        }

        if (!String.IsNullOrEmpty(Note))
        {
            SqlComm.Parameters.Add(new SqlParameter("@Note", System.Data.SqlDbType.NVarChar)).Value = Note;
        }

        SqlComm.Parameters.Add(new SqlParameter("@MunkaAllomas", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@MunkaAllomas"].Value = ExecParam.UserHostAddress;
        SqlComm.Parameters.Add(new SqlParameter("@MuveletKimenete", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@MuveletKimenete"].Value = "SIKERES";


        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result Insert(String Method, ExecParam ExecParam, KRT_Esemenyek Record)
    {
        try
        {
            return Insert(Method, ExecParam, Record, DateTime.Now);
        }
        catch (Exception e)
        {
            Result _result = Contentum.eUtility.ResultException.GetResultFromException(e);
            Logger.Info("KRT_Esemenyek::Insert: ", e);
            return _result;
        }
    }

    public Result GetAllWithExtensionForTortenet(ExecParam execParam, string objId, KRT_EsemenyekSearch _KRT_EsemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_EsemenyekGetAllWithExtensionForTortenet");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_EsemenyekGetAllWithExtensionForTortenet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_EsemenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_EsemenyekSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, _KRT_EsemenyekSearch.OrderBy, _KRT_EsemenyekSearch.TopRow);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ObjId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ObjId"].Value = SqlGuid.Parse(objId);

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            //SqlComm.Parameters["@OrderBy"].Size = 200;
            //SqlComm.Parameters["@OrderBy"].Value = " order by " + _KRT_EsemenyekSearch.OrderBy;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            //SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageNumber", System.Data.SqlDbType.Int));
            //SqlComm.Parameters["@pageNumber"].Value = execParam.Paging.PageNumber;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageSize", System.Data.SqlDbType.Int));
            //SqlComm.Parameters["@pageSize"].Value = execParam.Paging.PageSize;

            //if (!string.IsNullOrEmpty(execParam.Paging.SelectedRowId))
            //{
            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SelectedRowId", System.Data.SqlDbType.UniqueIdentifier));
            //    SqlComm.Parameters["@SelectedRowId"].Value = SqlGuid.Parse(execParam.Paging.SelectedRowId);
            //}

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}
