﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KRT_FelhasznalokStoredProcedure
/// </summary>
public class KRT_FelhasznalokStoredProcedure
{
    private DataContext dataContext;

    public KRT_FelhasznalokStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result GetAll(ExecParam ExecParam, KRT_FelhasznalokSearch _KRT_FelhasznalokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_FelhasznalokGetAll");

        Result _ret = new Result();


        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_FelhasznalokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_FelhasznalokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_FelhasznalokGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_FelhasznalokSearch.OrderBy, _KRT_FelhasznalokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}