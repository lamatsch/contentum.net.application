﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RightsServiceStoredProcedure
/// </summary>
public class RightsServiceStoredProcedure
{
    private DataContext dataContext;

    public RightsServiceStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

    }

    public Result AddCsoportToJogtargy(ExecParam execParam, string JogtargyId, string CsoportId, char Kezi, char Jogszint, char Tipus)
    {
        Result _ret = new Result();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_AddCsoportToJogtargy");

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_AddCsoportToJogtargy]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(JogtargyId);
        SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);
        SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(execParam.Felhasznalo_Id);

        SqlComm.Parameters.Add(new SqlParameter("@Kezi", SqlDbType.Char));
        SqlComm.Parameters["@Kezi"].Size = 1;
        SqlComm.Parameters["@Kezi"].Value = Kezi;

        SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
        SqlComm.Parameters["@Jogszint"].Size = 1;
        SqlComm.Parameters["@Jogszint"].Value = Jogszint;

        SqlComm.Parameters.Add(new SqlParameter("@Tipus", SqlDbType.Char));
        SqlComm.Parameters["@Tipus"].Size = 1;
        SqlComm.Parameters["@Tipus"].Value = Tipus;


        try
        {
            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }      

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}