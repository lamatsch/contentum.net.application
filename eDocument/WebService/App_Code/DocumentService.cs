using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

using Contentum.eBusinessDocuments;
using Contentum.eDocument.SharePoint;
using Contentum.eUtility;

using log4net;
using log4net.Config;
using System.Xml;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Data;
using Contentum.eRecord.Service;
using System.Globalization;
using Contentum.eAdmin.Service;


/// <summary>
/// eDoc-ot kezel� service oszt�ly.
/// Alapvet�en ebben vannak az eRecord oldal�r�l h�vhat� methodok.
/// Itt kell felhaszn�lni a kapcsol�d� t�rak webservice h�v�saihoz implement�lt caller oszt�lyokat
/// (pl Sharepointhoz a SPCaller vagy filesystemhez FileSystemCaller)
/// Ez a calleres megold�s lehet morbid. :(
/// 
/// DCs aranykopesek:
/// - Azt hiszik, h az erosebb kutya baszik. Pedig nem, a gyengebb is, csak azt basszak! 
/// - A biblia mondja szeresd felebaratodat. A kamasutra megmondja hogyan, a koran pedig, hogy hogyan csinalj belole business-t.
/// - Akit le akarnak szopni, azt a trabantban is leszopj�k. Az audi A8-ban meg azt is leszopj�k, aki nem akarja.
/// - Igyon szk�jdrollt! :) (A ricinusolaj szemleges�t�.)
/// 
/// 
/// Csabi fogorvostol jott meg, Peti mellett ult.
/// Peti: - Gyere ide, gyokerkezelt Csabi!
/// aztan:
/// Peti: - Hu, de fogorvos szagod van!
/// 
/// 
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class DocumentService : System.Web.Services.WebService {

    private DataContext dataContext;

    public const String Header = "$Header: DocumentService.cs, 184, 2016.01.06. 14:57:11, Bern�th L�szl�$";
    public const String Version = "$Revision: 184$";

    public char[] kiterjesztesVegeirol = { '.', ' ' };

    private static readonly object _sync = new object();

    /// <summary>
    /// DocumentService
    /// </summary>
    public DocumentService() {
        this.dataContext = new DataContext(this.Application);
        Logger.Info(String.Format("DocumentService.eDocument start.- {0} {1}", Header, Version));
    }

    /// <summary>
    /// Teszt method. Tipikusan bels� tesztel�shez.
    /// Alapesetben "OK"-ot ad vissza a Record mez�ben.
    /// </summary>
    /// <returns>Teszt eset�n Result-ot ad vissza. Egy�bk�nt Result.Record="OK".</returns>
    [WebMethod(Description = "<i>eRecord</i> site-ra <i>Teszt</i> doclib ala egy <i>TesztFolder</i> folder al� felnyom " +
        "egy <i>file1.txt</i> file-t.")]
    public Result Test()
    {
        //ExecParam execP2 = new ExecParam();
        //execP2.Felhasznalo_Id = "96382D51-1D8A-DD11-BBCD-005056C00008";
        //execP2.LoginUser_Id = execP2.Felhasznalo_Id;
        //execP2.Alkalmazas_Id = "272277bb-27b7-4689-be45-db5994a0fba9";

        //String xmlParamString = "<params>"
        //+"<krtdocument_id></krtdocument_id>"
        //+"<metaadatok>"
        //+"<metaadat internalName=\"edok_w_vonalkod\" value=\"1234567891233\" />"
        //+"<metaadat internalName=\"edok_w_iktatoszam\" value=\"S/00/12345\" />"
        //+"</metaadatok>"
        //+"</params>";


        //return SetDocumentPropertiesAndRevalidate(execP2, xmlParamString);

        return new Result();

        //select * from krt_csoporttagok where csoport_id_jogalany='B33D5432-97E8-DD11-A3D9-005056C00008'

        //Result alapesetResult = new Result();
        //alapesetResult.Record = "OK";
        ////return alapesetResult;

        //Result retek = new Result();

        //ExecParam execP2 = new ExecParam();
        //execP2.Felhasznalo_Id = "96382D51-1D8A-DD11-BBCD-005056C00008";
        //execP2.Alkalmazas_Id = "272277bb-27b7-4689-be45-db5994a0fba9";

        //String feltoltendoFileok = "<params><krtdocument_id>9E2184C2-25C8-DD11-BC82-005056C00008</krtdocument_id><metaadatok><metaadat internalName=\"edok_w_vonalkod\" value=\"1234\" /><metaadat internalName=\"edok_w_iktatoszam\" value=\"FPH009 /10 - XX /2008\" /></metaadatok></params>";

        //return this.SetDocumentPropertiesAndRevalidate(execP2, feltoltendoFileok);

        //Contentum.eDocument.SharePoint.ContentTypeService cttServ = new ContentTypeService();
        //return cttServ.AddContentType2DocumentLibrary("http://vtesztsps2/sites/proba/alproba", "teszt1", "");

        //String sazn = "U31-075-01-AuditDokumentum_alap";

        //Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
        //EREC_ObjektumTargyszavaiService ots = sf.GetEREC_ObjektumTargyszavaiService();

        //ots.Timeout = 3 * 60 * 100;

        //XmlDocument xmld = new XmlDocument();
        //XmlNode ndMetaadatok = xmld.CreateNode(XmlNodeType.Element, "metaadatok", null);
        //ndMetaadatok.InnerText = "<metaadat azonosito=\"ctt_w_alairosz_auditor1\" nev=\"Auditor 1 szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_minosegugyiVezeto\" nev=\"Min�s�g�gyi Vezet� szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_MunkLeirModDb\" nev=\"Munkak�ri le�r�s m�dos�tand� hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_UtoellenorzesIdopontja\" nev=\"Ut�ellen�rz�s id�pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_auditor2\" nev=\"Auditor 2 szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alosztaly\" nev=\"�gyoszt�ly aloszt�lya, ahol a vizsg�lat lesz\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditAllapot\" nev=\"Audit �llapot\" ertek=\"Elrendelt\" /><metaadat azonosito=\"ctt_w_alairo_ugyosztalyVezeto\" nev=\"�gyoszt�ly vezet�je\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditaltSzervezetVezeto\" nev=\"Audit�lt szervezet vezet�je\" ertek=\"\" /><metaadat azonosito=\"ctt_w_EgyebHibaDb\" nev=\"Egy�b hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_EljarasSzam\" nev=\"Elj�r�s sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_NyilvVezHibaDb\" nev=\"Nyilv�ntart�s vezet�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_BizonylatModHibaDb\" nev=\"Bizonylati album m�dos�tand� hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_AuditItezkedesKell\" nev=\"Audit int�zked�s sz�ks�ges\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_minosegugyiVezeto\" nev=\"Min�s�g�gyi Vezet�\" ertek=\"G�bor Mikl�s\" /><metaadat azonosito=\"ctt_w_alairosz_ugyosztalyVezeto\" nev=\"�gyoszt�ly vezet�je szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_ugyosztaly\" nev=\"�gyoszt�ly, ahol a vizsg�lat lesz\" ertek=\"\" /><metaadat azonosito=\"ctt_w_FolyamatszabalyozasHibaDb\" nev=\"Folyamatszab�lyoz�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_LezarasIdopontja\" nev=\"Lez�r�s id�pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditTervIdopont\" nev=\"Audit tervezett id�pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditor2\" nev=\"Auditor 2\" ertek=\"\" /><metaadat azonosito=\"ctt_w_IratkezelesHibaDb\" nev=\"Iratkezel�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditTipus\" nev=\"Vizsg�lat t�pusa\" ertek=\"Folyamatszab�lyoz�s �s a kapcsol�d� elj�r�sok\" /><metaadat azonosito=\"ctt_w_auditorBetanulo\" nev=\"Betanul� Auditor\" ertek=\"\" /><metaadat azonosito=\"ctt_w_MinositettBeszKesveDb\" nev=\"Min�s�tett besz�ll�t�k list�j�ra k�s�bb ker�lt fel hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_auditaltSzervezetVezeto\" nev=\"Audit�lt szervezet vezet�je szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditIdopont\" nev=\"Audit id�pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditor1\" nev=\"Auditor 1\" ertek=\"\" /><metaadat azonosito=\"ctt_w_megjegyzes\" nev=\"Megjegyz�s\" ertek=\"\" />";

        //ExecParam execP1 = new ExecParam();
        //execP1.Felhasznalo_Id = "96382D51-1D8A-DD11-BBCD-005056C00008";
        //execP1.Alkalmazas_Id = "272277bb-27b7-4689-be45-db5994a0fba9";

        ////Result resultMeta = ots.MergeWordMetaByContentType(execP1
        ////    , ndMetaadatok
        ////    , "e2e0a1c8-f48e-dd11-bbd8-005056c00008"
        ////    , "EREC_IraIratok"
        ////    , "U31-075-01-AuditDokumentum"
        ////    , false
        ////    );

        ////return resultMeta;

        Result res = new Result();

        String filename = "alap_ikt2333.docx";
        String filepath = @"C:\Documents and Settings\varsanyi.peter\Dokumentumok\";
        byte[] cont;

        try
        {
            FileInfo fInfo = new FileInfo(String.Format("{0}{1}", filepath, filename));
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(String.Format("{0}{1}", filepath, filename), FileMode.Open, FileAccess.Read);

            BinaryReader br = new BinaryReader(fStream);

            cont = br.ReadBytes((int)numBytes);

            br.Close();

            fStream.Close();
        }
        catch (Exception exc)
        {
            res.ErrorCode = "READFILE0001";
            res.ErrorMessage = String.Format("Read file exception!\nMessage: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
            return res;
        }

        ExecParam execP = new ExecParam();
        execP.Felhasznalo_Id = "7342B050-2789-449C-8635-FE87919D6231";
        execP.Alkalmazas_Id = "272277bb-27b7-4689-be45-db5994a0fba9";

        // ikt parameterek
        String iktParams = "<iktatasiadatok><kivalasztottUgyiratId>1acdec9c-3787-4fd1-8cba-b88230edfec4</kivalasztottUgyiratId><alszam>7</alszam><vonalkod>1000000000126</vonalkod><targy>marhjo</targy><cimzett_nev>1146 Ajt�si D�rer Sor 5 T�rsash. (108959693)</cimzett_nev><cimzett_id>51b997bf-7813-46d5-bc52-0ea0eaacd22f</cimzett_id><cimzett_cim>1119 Budapest Etele utca 29 </cimzett_cim><cimzett_cim_id>657d7a5b-aeef-4299-aaaf-11e6bff109c1</cimzett_cim_id><megjegyzes>wsewe</megjegyzes><munkaanyag>NEM</munkaanyag><ujirat>IGEN</ujirat><kivalasztottIratId></kivalasztottIratId><docmetaIratId></docmetaIratId><docmetaDokumentumId></docmetaDokumentumId><ujIratCttjeLegyen>NEM</ujIratCttjeLegyen><tartalomSHA1>A8563F587E09C5B748463126E0DE4C2E3F6A8C84</tartalomSHA1><alairok><alairo><id>7342b050-2789-449c-8635-fe87919d6231</id><nev>Fab�nyi Luca Anna</nev><szerepkor></szerepkor><szervezet>�t �s M?t�rgy Aloszt�ly</szervezet><szervezetid>3cbf4b5c-8718-4195-912d-fbda354a2364</szervezetid></alairo></alairok><metaadatok><metaadat azonosito=\"ctt_w_alairosz_auditor1\" nev=\"Auditor 1 szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_minosegugyiVezeto\" nev=\"Min?s�g�gyi Vezet? szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_MunkLeirModDb\" nev=\"Munkak�ri le�r�s m�dos�tand� hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_UtoellenorzesIdopontja\" nev=\"Ut�ellen?rz�s id?pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_auditor2\" nev=\"Auditor 2 szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alosztaly\" nev=\"�gyoszt�ly aloszt�lya, ahol a vizsg�lat lesz\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditAllapot\" nev=\"Audit �llapot\" ertek=\"Elrendelt\" /><metaadat azonosito=\"ctt_w_alairo_ugyosztalyVezeto\" nev=\"�gyoszt�ly vezet?je\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditaltSzervezetVezeto\" nev=\"Audit�lt szervezet vezet?je\" ertek=\"\" /><metaadat azonosito=\"ctt_w_EgyebHibaDb\" nev=\"Egy�b hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_EljarasSzam\" nev=\"Elj�r�s sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_NyilvVezHibaDb\" nev=\"Nyilv�ntart�s vezet�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_BizonylatModHibaDb\" nev=\"Bizonylati album m�dos�tand� hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_AuditItezkedesKell\" nev=\"Audit int�zked�s sz�ks�ges\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_minosegugyiVezeto\" nev=\"Min?s�g�gyi Vezet?\" ertek=\"Fab�nyi Luca Anna\" /><metaadat azonosito=\"ctt_w_alairosz_ugyosztalyVezeto\" nev=\"�gyoszt�ly vezet?je szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_ugyosztaly\" nev=\"�gyoszt�ly, ahol a vizsg�lat lesz\" ertek=\"\" /><metaadat azonosito=\"ctt_w_FolyamatszabalyozasHibaDb\" nev=\"Folyamatszab�lyoz�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_LezarasIdopontja\" nev=\"Lez�r�s id?pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditTervIdopont\" nev=\"Audit tervezett id?pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditor2\" nev=\"Auditor 2\" ertek=\"\" /><metaadat azonosito=\"ctt_w_IratkezelesHibaDb\" nev=\"Iratkezel�si hib�k sz�ma\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditTipus\" nev=\"Vizsg�lat t�pusa\" ertek=\"Folyamatszab�lyoz�s �s a kapcsol�d� elj�r�sok\" /><metaadat azonosito=\"ctt_w_auditorBetanulo\" nev=\"Betanul� Auditor\" ertek=\"\" /><metaadat azonosito=\"ctt_w_MinositettBeszKesveDb\" nev=\"Min?s�tett besz�ll�t�k list�j�ra k�s?bb ker�lt fel hibasz�m\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairosz_auditaltSzervezetVezeto\" nev=\"Audit�lt szervezet vezet?je szerepk�r\" ertek=\"\" /><metaadat azonosito=\"ctt_w_auditIdopont\" nev=\"Audit id?pontja\" ertek=\"\" /><metaadat azonosito=\"ctt_w_alairo_auditor1\" nev=\"Auditor 1\" ertek=\"\" /><metaadat azonosito=\"ctt_w_megjegyzes\" nev=\"Megjegyz�s\" ertek=\"\" /></metaadatok><ujiratkiadmanyozhato>NEM</ujiratkiadmanyozhato></iktatasiadatok>";

        string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{1}</iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<kivalasztottIratId>{2}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId>{3}</docmetaDokumentumId>" +
                                                        "<megjegyzes>{4}</megjegyzes>" +
                                                        "<munkaanyag>{5}</munkaanyag>" +
                                                        "<ujirat>{6}</ujirat>" +
                                                        "<vonalkod>{7}</vonalkod>" +
                                                        "<docmetaIratId>{8}</docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{9}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{10}</tartalomSHA1>" +
                                                        "<alairaskell>NEM</alairaskell>" +
                                                    "</uploadparameterek>"
                                                    , "1"
                                                    , "FPH009"
                                                    , "125DFBDB-B744-405C-9BEB-000577307F60"
                                                    , String.Empty
                                                    , String.Empty
                                                    , "NEM"
                                                    , "NEM"
                                                    , "12345"
                                                    , String.Empty
                                                    , "IGEN"
                                                    , String.Empty
                                                    );


        return this.UploadFromeRecordWithCTTCheckin(execP
            , Contentum.eUtility.Constants.DocumentStoreType.SharePoint
            , filename
            , cont
            , null
            , uploadXmlStrParams);

        Logger.Info("Test vege ------------------------------------------------------------------------------------");

    }

    /// <summary>
    /// T�rhelyen bel�li m�sol�s.
    /// Megadott krt_Dok GUID alapj�n kiv�lasztja a dokumentumot �s a param�terek �ltal megadott t�rhleyre egy m�solatot k�sz�t.
    /// A krt_Dok GUID-ot az ExecParam UID-j�be kell rakni.
    /// 
    /// Visszaselect krt_Dokb�l, ez alapj�n az adattartalom visszaszed�se a t�rhelyr�l,
    /// majd megh�vja a UploadFromeRecordWithCTTCheckin-t.
    /// </summary>
    /// <param name="execparam">Uidbe a krt_Dok bejegyz�s GUIDja.</param>
    /// <param name="documentStoreType">Sharepoint</param>
    /// <param name="documentSite">C�l site path.</param>
    /// <param name="documentStore">C�l docLib neve.</param>
    /// <param name="documentFolder">C�l folder path.</param>
    /// <returns>Result - uid krt_Dok guid.</returns>
    [WebMethod]
    public Result CopyFromeRecord(ExecParam execparam
        , String documentStoreType
        , String documentSite
        , String documentStore
        , String documentFolder)
    {
        Logger.Info(String.Format("CopyFromeRecord start - documentSite: {0} - documentStore: {1} - documentFolder: {2}", documentSite, documentStore, documentFolder));
        Result ret = new Result();

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(documentStore) ||
            String.IsNullOrEmpty(documentFolder))
        {
            // TODO: alap parameter hibak:
            Logger.Error("Ures parameter!");
            return ResultError.CreateNewResultWithErrorCode(200000);
        }



        /// dokumentum info megszerz�se
        /// 
        Logger.Info("Dokumentum keresese id alapjan.");
        Result dokumentumResult = GetDoc(execparam);
        if (!String.IsNullOrEmpty(dokumentumResult.ErrorCode))
        {
            Logger.Error("Nincs meg a dokumentum!");
            return dokumentumResult;
        }

        KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)dokumentumResult.Record;

        //string fileFormatum = dokumentum.Formatum.ToString();
        //string fileTipus = dokumentum.Tipus.ToString();

        ///  dokumentum tartalom 
        ///  
        byte[] dokumentumTartalom = null;

        if (dokumentum.External_Source == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Logger.Info("SharePoint dokservice hivas.");
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();

            if (!documentSite.EndsWith("/")) { documentSite += "/"; }
            if (!documentStore.EndsWith("/")) { documentStore += "/"; }

            string filename = dokumentum.FajlNev.Trim();
            int pathLength = dokumentum.External_Link.Length;
            string ppath = dokumentum.External_Info.ToString().Substring(0, pathLength - filename.Length);

            Logger.Info(String.Format("ppath: {0}", ppath));
            Logger.Info(String.Format("dokumentum.External_Info: {0}", dokumentum.External_Info));

            string[] pparts = dokumentum.External_Info.ToString().Split(';');

            Logger.Info(String.Format("pparts.Length: {0}", pparts.Length));

            //string _pFromSite = (pparts.Length >= 0) ? pparts[0] : ""; 
            //string _pFromDoclib = (pparts.Length >= 1) ? pparts[1] : "";
            //string _pFromFolders = (pparts.Length >= 2) ? pparts[2] : "";

            String _pFromSite = String.Format("{0}{1}", pparts[1], pparts[2]);
            String _pFromDoclib = String.Format("{0}", pparts[3]);
            String _pFromFolders = String.Format("{0}", pparts[4]);

            Logger.Info(String.Format("_pFromSite: {0} - _pFromDoclib: {1} - _pFromFolders: {2}", _pFromSite, _pFromDoclib, _pFromFolders));

            Result res = ds.Get(_pFromSite, _pFromDoclib, _pFromFolders, filename);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Error("Sharepointbol dokumentum tartalom keres hiba! " + res.ErrorCode.ToString() + "; " + res.ErrorMessage.ToString());
                return res;
            }

            dokumentumTartalom = (byte[])res.Record;

        }
        else if (dokumentum.External_Source == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {
            /// TODO:
            Logger.Info("Filesystem-bol file tartalom kinyerese. LESZ MAJD!");
        }
        else if (dokumentum.External_Source == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {
            /// TODO:
            Logger.Info("Database-bol file tartalom kinyerese. LESZ MAJD!");
        }

        //
        //  xmlParam string feltoltese
        string xmlParams = String.Format("<uploadparameterek>" +
                                            "<iktatokonyv></iktatokonyv>" +
                                            "<sourceSharePath></sourceSharePath>" +
                                            "<foszam>{0}</foszam>" +
                                            "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                            "<kivalasztottKuldemenyId>{6}</kivalasztottKuldemenyId>" +
                                            "<docmetaDokumentumId></docmetaDokumentumId>" +
                                            "<megjegyzes></megjegyzes>" +
                                            "<munkaanyag>{2}</munkaanyag>" +
                                            "<ujirat>{3}</ujirat>" +
                                            "<vonalkod></vonalkod>" +
                                            "<docmetaIratId></docmetaIratId>" +
                                            "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                            "<tartalomSHA1>{5}</tartalomSHA1>" +
                                        "</uploadparameterek>"
                                        , documentFolder   //ExecParam.Record_Id // foszam
                                        , String.Empty
                                        , "NEM"
                                        , "IGEN"
                                        , "IGEN"
                                        , dokumentum.TartalomHash
                                        , documentFolder  //kivalasztottKuldemenyId
                                        );

        //String xmlParams = String.Format("<uploadparameterek>" +
        //                                "<iktatokonyv>{5}</iktatokonyv>" +
        //                                "<sourceSharePath></sourceSharePath>" +
        //                                "<foszam>{0}</foszam>" +
        //                                "<kivalasztottIratId>{1}</kivalasztottIratId>" +
        //                                "<docmetaDokumentumId></docmetaDokumentumId>" +
        //                                "<megjegyzes></megjegyzes>" +
        //                                "<munkaanyag>{2}</munkaanyag>" +
        //                                "<ujirat>{3}</ujirat>" +
        //                                "<vonalkod></vonalkod>" +
        //                                "<docmetaIratId></docmetaIratId>" +
        //                                "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
        //                                "<tartalomSHA1>{6}</tartalomSHA1>" +
        //                            "</uploadparameterek>"
        //                            , "??"    //ugyiratFoszam
        //                            , "??"    //kivalasztottIratId
        //                            , "NEM"
        //                            , "NEM"
        //                            , "NEM"
        //                            , "??"    //iktatokonyvIktatohely
        //                            , dokumentum.TartalomHash
        //                            );

        /// kapott tartalom feltoltese
        /// 
        ExecParam ex = execparam.Clone();
        ex.Record_Id = "";
        //Logger.Info("UploadFromeRecord hivas");
        //ret = UploadFromeRecord(ex, documentStoreType, RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), dokumentum.FajlNev.ToString(), dokumentumTartalom, null, false);
        //Logger.Info("UploadFromeRecord hivas vege");
        Logger.Info("UploadFromeRecordWithCTTCheckin hivas.");
        ret = UploadFromeRecordWithCTTCheckin(ex
                , documentStoreType
                , dokumentum.FajlNev.Trim()
                , dokumentumTartalom
                , null
                , xmlParams);
        Logger.Info("UploadFromeRecordWithCTTCheckin hivas vege.");

        Logger.Info("CopyFromeRecord vege");
        return ret;
    }


    /// <summary>
    /// File felt�lt�se.
    /// DEPRECATED!
    /// <br>L�tez�t fel�l�r.
    /// 
    /// formatum  mezobe: dokumentum_formatum kodcsoportbol a formatum
    /// parameterkent be, ha nincs a kiterjesztesebol megallapitani
    ///  (KodCsoport_Id = '644d3323-050c-4fbe-a380-25fedc063b8e')
    /// 
    /// tipus: parameterkent fogadni a tipust
    /// 
    /// 
    /// 
    /// </summary>
    /// <param name="execparam">Execparam</param>
    /// <returns>Result - eredm�ny Uid = dokumentum eDoc db-ben l�v� objektum Id-ja</returns>
    /// 
    [WebMethod(Description = "<br><b>ExecParam</b> - az alkalmazas es felhasznalo id-knek ki kell lenniuk toltve. " +
          "<br><b>documentStoreType</b> - ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint " +
          "<br><b>documentSite</b> - site neve => \"eRecord\" legyen!" +
          "<br><b>documentStore</b> - doclib neve" +
          "<br><b>documentFolder</b> - konyvtar(ak) nev(ei); ha tobb, / jellel elvalasztva " +
          "<br><b>filename</b> - file neve" +
          "<br><b>cont</b> - byte tomb, a file tartalma" +
          "<br><b>kapottKrtDokObj</b> - KRT_Dokumentumok, egy nagyon szep KRT_Dokumentumok objektum" +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.")]
    public Result UploadFromeRecord(ExecParam execparam
        , String documentStoreType
        , String documentSite
        , String documentStore
        , String documentFolder
        , String filename
        , byte[] cont
        , KRT_Dokumentumok kapottKrtDokObj
        , bool iktatas)
    {
        Logger.Info("UploadFromeRecord start - DEPRECATED");
        Logger.Info("Kapott parameterek: documentStoreType=" + documentStoreType + "; documentSite=" + documentSite + "; documentFolder=" + documentFolder + "; filename=" + filename);
        Result result = new Result();

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(documentStore) ||
            String.IsNullOrEmpty(documentFolder) ||
            String.IsNullOrEmpty(filename) ||
            cont == null
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStore) ? " documentStore" : "";
            strErrorInfo += String.IsNullOrEmpty(documentFolder) ? " documentFolder" : "";
            strErrorInfo += String.IsNullOrEmpty(filename) ? " filename" : "";
            strErrorInfo += (cont == null) ? " cont" : "";

            // TODO: alap parameter hibak:
            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            /// -----------------------------------------------------------------------------------------------
            result = _UploadFromeRecord(execparam
            , documentStoreType
            , documentSite
            , documentStore
            , documentFolder
            , filename.Trim()
            , cont
            , kapottKrtDokObj
            , false);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    /// <summary>
    /// Csak felt�lt�s! Ut�na kell a CheckInUploadedFileWithCTT-t futtatni!
    /// A k�t l�p�ses felt�lt�s els� l�p�se. A m�sik l�p�s a checkin-es �s egy�b dolgok be�ll�t�sa kell legyen.
    /// (A neve alapj�n tartalomt�pusos (CTT) felt�lt�s, de tartalomt�pustalan is lehet!)
    /// F�leg word iktat�sb�l haszn�latos.
    /// 
    /// A kiindul�, alap felt�lt�si strukt�ra meghat�roz�sa (TarolasiHelyStrukturaNevEsTartalomAlapjan).
    /// Dbben ellen�rz�s: az �t megl�te, illetve, ha nincs, l�trehozza (TarhelyekEllenorzeseMappakDb).
    /// T�rhely ellen�rz�s: a t�rol�si helyek megl�te, l�trehoz�sa (TarhelyekEllenorzeseMappakTarhely).
    /// Checkout: ha van a c�l t�rhelyen m�r olyan nev� t�rolt �llom�ny, �s a krt_dok bejegyzes megkeres�se.
    /// Felt�lt�s.
    /// Krt_Dok bejegyz�s l�trehoz�sa.
    /// A CheckInUploadedFileWithCTT r�sz�re �tadand� XML param�terek �ssze�ll�t�sa. Ezt a Record mez�ben adja vissza String-k�nt.
    /// 
    /// A (Sharepoint) t�rhely funkci�k el�r�s�re: 
    ///     - m�dos�t�s, l�trehoz�s eset�n a seg�d WS-t haszn�lja,
    ///     - lek�rdez�sre bels�leg l�trehozott methodokb�l h�vott WSS/MOSS ws h�v�sokat.
    ///     
    /// Param�terk�nt �tadott XML string:
    /// 
    /// <uploadparameterek>
    ///     <iktatokonyv>String</iktatokonyv>           <!-- iktat�k�ny jel (pl: FPH001) MOSS t�rol�si strukt�r�hoz, site urlbe -->
    ///     <foszam>String</foszam>                     <!-- �gyirat f�sz�ma: MOSS t�rol�si strukt�r�hoz, folder -->
    ///     <kivalasztottIratId>GUID</kivalasztottIratId>    <!-- ahova csatoljuk majd, az irat id-je vagy �res -->
    ///     <docmetaDokumentumId>GUID</docmetaDokumentumId>  <!-- word iktatsbol jon, dokumentumban tarolt krt_Dok GUID, vagy �res -->
    ///     <megjegyzes>String</megjegyzes>                 <!-- megjegyz�s a MOSS verzi�hoz -->
    ///     <munkaanyag>IGEN/NEM</munkaanyag>               <!-- IGEN/NEM - a l�trehozand� irat munkaanyag lesz; nem irat eset�n NEM -->
    ///     <ujirat>IGEN/NEM</ujirat>                       <!-- IGEN/NEM - a l�trehozand� irat �j-e; nem irat eset�n NEM -->
    ///     <vonalkod>String</vonalkod>                     <!-- a vonalk�d �rt�ke, ami a krt_Dok bejegyz�sebe ker�l -->
    ///     <docmetaIratId>GUID</docmetaIratId>             <!-- word iktatsbol jon, dokumentumban tarolt irat id GUID, vagy �res -->
    ///     <ujKrtDokBejegyzesKell>IGEN/NEM</ujKrtDokBejegyzesKell> <!-- IGEN/NEM - mindenk�ppen kell-e �j krt_Dok bejegyz�s -->
    ///     <tartalomSHA1>String</tartalomSHA1>             <!-- SHA1 chksum tartalomra; krt_dokba ker�l.  -->
    ///     <alairaskell>IGEN/NEM</alairaskell>             <!-- IGEN/NEM - kell-e elektronikus al��r�s... -->
    /// </uploadparameterek>
    /// 
    /// Visszadott XML string:
    /// 
    /// <uploadparameterek> 
    ///     <documentStoreType>String</documentStoreType>           <!-- t�rol�s t�pusa - pl SharePoint-->
    ///     <rootSiteCollectionUrl>String</rootSiteCollectionUrl>   <!-- MOSS: root site Collection url, tipikusan: sites/edok-->
    ///     <doktarSitePath>String</doktarSitePath>                 <!-- MOSS: a site Collecton bel�li site(ok) path: pl dokumentumtar/2008/CSATOLMANYOK-->
    ///     <doktarDocLibPath>String</doktarDocLibPath>             <!-- MOSS: a DocumentLibrary neve -->
    ///     <doktarFolderPath>String</doktarFolderPath>             <!-- MOSS: a docliben bel�li folder path, �res is lehet-->
    ///     <dokumentum_id>GUID</dokumentum_id>                     <!-- krt_dok bejegyz�s id - r�gebbr�l maradt, de haszn�latos -->
    ///     <spsCttId>String</spsCttId>                             <!-- MOSS ctt id -->
    ///     <eDocMappaGuid>GUID</eDocMappaGuid>                     <!-- eDocument Mappa bejegyz�s GUID-->
    ///     <filename>String</filename>                             <!-- File neve -->
    ///     <iktatokonyv>String</iktatokonyv>                       <!-- iktat�k�ny jel (pl: FPH001) MOSS t�rol�si strukt�r�hoz, site urlbe -->
    ///     <foszam>String</foszam>                                 <!-- �gyirat f�sz�ma: MOSS t�rol�si strukt�r�hoz, folder -->
    ///     <iktatoszam>String</iktatoszam>                         <!-- iktat�sz�m - ELAVULT -->
    ///     <ujKrtDokGuid>String</ujKrtDokGuid>                     <!-- Ha �j krt_Dok begyez�s j�tt l�tre ez az a GUID.-->
    ///     <megjegyzes>String</megjegyzes>                         <!-- MOSS verzi�hoz megyjegyz�s.-->
    ///     <munkaanyag>IGEN/NEM</munkaanyag>                       <!-- IGEN/NEM: word itkat�shoz: a l�trej�v� irat munkaanyag lesz -->
    ///     <docmetaDokumentumId>GUID</docmetaDokumentumId>         <!-- word itkat�shoz: a dokumentumban t�rolt krt_Dok id -->
    ///     <dokumentumFullUrl>String</dokumentumFullUrl>           <!-- A file teljes el�r�si �tja. -->
    ///     <ujirat>IGEN/NEM</ujirat>                               <!-- IGEN/NEM: word itkat�shoz: �j irat l�tre kell hogy l�trej�jj�n. -->
    ///     <cttNeve>String</cttNeve>                               <!-- MOSS contentType neve -->
    ///     <docmetaIratId>GUID</docmetaIratId>                     <!-- word itkat�shoz: Ha nem �j irat, az irt_idje, ami eredetileg a docban volt. -->
    ///     <kivalasztottIratId>GUID</kivalasztottIratId>           <!-- word itkat�shoz: Nem biztos, hogy egyezik az el�z�vel; amihez csatolunk iratID. -->
    ///     <ujKrtDokBejegyzesKell>IGEN/NEM</ujKrtDokBejegyzesKell> <!-- IGEN/NEM: mindek�ppen kell-e �j krt_dok bejegyz�s -->
    ///     <tartalomSHA1>String</tartalomSHA1>                     <!-- SHA1 chksum tartalomra; krt_dokba ker�l.  -->
    ///     <iratMetaDefId>GUID</iratMetaDefId>                     <!-- NEM HASZN�LT -->
    ///     <iratMetaDefFoDokumentum>IGEN/NEM</iratMetaDefFoDokumentum>     <!-- NEM HASZN�LT -->
    ///     <sablonAzonosito>String</sablonAzonosito>                       <!-- Sablonazonos�t�. -->
    ///     <kivalasztottKuldemenyId>GUID</kivalasztottKuldemenyId>         <!-- Kiv�lasztott k�ldem�ny GUID. -->
    ///     <tortentUjKrtDokBejegyzes>IGEN/NEM</tortentUjKrtDokBejegyzes>   <!-- IGEN/NEM: t�rt�nt krt_dok bejegyz�s (?? ez a k�ldem�nyekn�l k�l�n van)-->
    ///     <csatolmanyId>GUID</csatolmanyId>                               <!-- Csatolm�ny GUID, ha van. -->
    ///     <csatolmanyVerzio>String</csatolmanyVerzio>             <!-- Csatolm�ny bejegyz�s verzi�sz�ma, a update-hez kell majd. -->
    /// </uploadparameterek>
    ///  
    /// </summary>
    /// <param name="execparam">a felhasznalo id kell.</param>
    /// <param name="documentStoreType">ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint</param>
    /// <param name="filename">A file neve.</param>
    /// <param name="cont">[opcion�lis] byte tomb, a file tartalma (share eset�n nem kell)</param>
    /// <param name="kapottKrtDokObj">KRT_Dokumentumok, egy nagyon szep KRT_Dokumentumok objektum vagy null ha nincs.</param>
    /// <param name="xmlStrParams">Tov�bbi param�terek megad�sa xml stringk�nt.</param>
    /// <returns>Result - az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba. Record mez� - xml string.</returns>
    [WebMethod(Description = "Csak feltoltes! Utana kell a CheckInUploadedFileWithCTT!<br><b>ExecParam</b> - a felhasznalo id kell. " +
          "<br><b>documentStoreType</b> - ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint " +
          "<br><b>filename</b> - file neve" +
          "<br><b>cont</b> - [opcion�lis] byte tomb, a file tartalma (share eset�n nem kell)" +
          "<br><b>kapottKrtDokObj</b> - KRT_Dokumentumok, egy nagyon szep KRT_Dokumentumok objektum" +
          "<br><b>xmlStrParams</b> - Tov�bbi param�terek megad�sa xml stringk�nt." +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.")]
    public Result UploadFromeRecordWithCTT(ExecParam execparam
        , String documentStoreType
        , String filename
        , byte[] cont
        , KRT_Dokumentumok kapottKrtDokObj
        , String xmlStrParams)
    {
        Logger.Info("------------- UploadFromeRecordWithCTT -------------");
        Result result = new Result();

        #region Parameter XML string feldolgozasa.

        XmlDocument xmlDoc = new XmlDocument();

        if (xmlStrParams != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlDoc.LoadXml(xmlStrParams);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, xmlStrParams));
                result.ErrorCode = "XMLPAR0001";
                result.ErrorMessage = String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return result;
            }
        }

        #endregion

        //  Parameterezes ellenorzese

        string sourceSharePath = GetParam("sourceSharePath", xmlDoc);
        string ujirat = GetParam("ujirat", xmlDoc);
        string kivalasztottIratId = GetParam("kivalasztottIratId", xmlDoc);
        string kivalasztottKuldemenyId = GetParam("kivalasztottKuldemenyId", xmlDoc);

        //  felhasznalo id, store type es file name megadasa kotelezo
        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(filename)
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id -" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType -" : "";
            strErrorInfo += String.IsNullOrEmpty(filename) ? " filename -" : "";

            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(201001);
        }

        //  nem lehet ures a tartalom es a sourcePath egyszerre
        if (cont == null &&
            String.IsNullOrEmpty(sourceSharePath)
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (cont == null) ? " execparam.Felhasznalo_Id -" : "";
            strErrorInfo += String.IsNullOrEmpty(sourceSharePath) ? " documentStoreType -" : "";

            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(201002);
        }

        if (ujirat.Equals("NEM") && String.IsNullOrEmpty(kivalasztottIratId) && String.IsNullOrEmpty(kivalasztottKuldemenyId))
        {
            Logger.Error(String.Format("Ha nem uj irat van (ujirat==NEM), meg kell adni mindenkeppen a kivalasztott irat idjet vagy a kuldemeny idjet!"));
            Logger.Error(String.Format("Hianyzo parameter! -> kivalasztottIratId es kivalasztottKuldemenyId is ures"));
            return ResultError.CreateNewResultWithErrorCode(201003);
        }

        //  ---------------------------------------------------------------------

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentumManager _UMCDocumentumManager = new UCMDocumentumManager();
                /// -----------------------------------------------------------------------------------------------
                result = _UMCDocumentumManager.UploadFromeRecordWithCTT(execparam
                , documentStoreType
                , filename.Trim()
                , cont
                , kapottKrtDokObj
                , xmlDoc
                );
            }
            else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
            {
                FileSystem.FileSystemManager _FileSystemManager = new FileSystem.FileSystemManager();
                /// -----------------------------------------------------------------------------------------------
                result = _FileSystemManager.UploadFromeRecordWithCTT(execparam
                , documentStoreType
                , filename.Trim()
                , cont
                , kapottKrtDokObj
                , xmlDoc
                );
            }
            else
            {
                result = _UploadFromeRecordWithCTT(execparam
                    , documentStoreType
                    , filename.Trim()
                    , cont
                    , kapottKrtDokObj
                    , xmlDoc
                    );
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    /// <summary>
    /// Kett� az egyben felt�lt�s! :)
    /// T�pusos vagy t�pustalan felt�lt�s.
    /// Egym�s ut�n h�vja a megfelel� elj�r�sokat: UploadFromeRecordWithCTT, CheckInUploadedFileWithCTT.
    /// 
    /// R�szletes le�r�st l�sd a fenti methodokn�l.
    /// 
    /// Visszaadott Result.Record mez� XML string: 
    /// <result>
    ///    <dokumentumFullUrl>dok full url-je</dokumentumFullUrl>
    ///    <tortentUjKrtDokBejegyzes>tortent-e krt_dok bejegyzes IGEN/NEM</tortentUjKrtDokBejegyzes>
    ///    <csatolmanyId>vajon ez miiii?</csatolmanyId>"
    ///    <csatolmanyVerzio>verizo, az sql update-hez kell</csatolmanyVerzio>
    /// </result>
    /// 
    /// </summary>
    /// <param name="execparam"> a felhasznalo id kell.</param>
    /// <param name="documentStoreType">ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint</param>
    /// <param name="filename">file neve</param>
    /// <param name="cont">[opcion�lis] byte tomb, a file tartalma (share eset�n nem kell)</param>
    /// <param name="kapottKrtDokObj">KRT_Dokumentumok, egy nagyon szep KRT_Dokumentumok objektum</param>
    /// <param name="xmlStrParams">Tov�bbi param�terek megad�sa xml stringk�nt.</param>
    /// <returns>Result - az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.</returns>
    [WebMethod(Description = "Ketto az egyben feltolt es csekinel!<br><b>ExecParam</b> - a felhasznalo id kell. " +
          "<br><b>documentStoreType</b> - ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint " +
          "<br><b>filename</b> - file neve" +
          "<br><b>cont</b> - [opcion�lis] byte tomb, a file tartalma (share eset�n nem kell)" +
          "<br><b>kapottKrtDokObj</b> - KRT_Dokumentumok, egy nagyon szep KRT_Dokumentumok objektum" +
          "<br><b>xmlStrParams</b> - Tov�bbi param�terek megad�sa xml stringk�nt." +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.")]
    public Result UploadFromeRecordWithCTTCheckin(ExecParam execparam
        , String documentStoreType
        , String filename
        , byte[] cont
        , KRT_Dokumentumok kapottKrtDokObj
        , String xmlStrParams)
    {
        Logger.Info("------------- UploadFromeRecordWithCTTCheckin -------------");
        Result result = new Result();

        #region Parameter XML string feldolgozasa.

        XmlDocument xmlDoc = new XmlDocument();

        if (xmlStrParams != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlDoc.LoadXml(xmlStrParams);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, xmlStrParams));
                result.ErrorCode = "XMLPAR0001";
                result.ErrorMessage = String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return result;
            }
        }

        #endregion

        filename = eDocumentUtility.ConvertIllegalCharactersInFilename(filename);

        //  Parameterezes ellenorzese

        string sourceSharePath = GetParam("sourceSharePath", xmlDoc);
        string ujirat = GetParam("ujirat", xmlDoc);
        string kivalasztottIratId = GetParam("kivalasztottIratId", xmlDoc);
        string kivalasztottKuldemenyId = GetParam("kivalasztottKuldemenyId", xmlDoc);

        //  felhasznalo id, store type es file name megadasa kotelezo
        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(filename)
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id -" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType -" : "";
            strErrorInfo += String.IsNullOrEmpty(filename) ? " filename -" : "";

            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(201001);
        }

        //  nem lehet ures a tartalom es a sourcePath egyszerre
        if (cont == null &&
            String.IsNullOrEmpty(sourceSharePath)
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (cont == null) ? " execparam.Felhasznalo_Id -" : "";
            strErrorInfo += String.IsNullOrEmpty(sourceSharePath) ? " documentStoreType -" : "";

            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(201002);
        }

        if (ujirat.Equals("NEM") && String.IsNullOrEmpty(kivalasztottIratId) && String.IsNullOrEmpty(kivalasztottKuldemenyId))
        {
            Logger.Error(String.Format("Ha nem uj irat van (ujirat==NEM), meg kell adni mindenkeppen a kivalasztott irat idjet vagy a kuldemeny idjet!"));
            Logger.Error(String.Format("Hianyzo parameter! -> kivalasztottIratId es kivalasztottKuldemenyId is ures"));
            return ResultError.CreateNewResultWithErrorCode(201003);
        }

        //  ---------------------------------------------------------------------

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentumManager _UMCDocumentumManager = new UCMDocumentumManager();
                /// -----------------------------------------------------------------------------------------------
                result = _UMCDocumentumManager.UploadFromeRecordWithCTT(execparam
                , documentStoreType
                , filename.Trim()
                , cont
                , kapottKrtDokObj
                , xmlDoc
                );
            }
            else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
            {
                FileSystem.FileSystemManager _FileSystemManager = new FileSystem.FileSystemManager();
                /// -----------------------------------------------------------------------------------------------
                result = _FileSystemManager.UploadFromeRecordWithCTT(execparam
                , documentStoreType
                , filename.Trim()
                , cont
                , kapottKrtDokObj
                , xmlDoc
                );
            }
            else
            {
                result = _UploadFromeRecordWithCTT(execparam
                    , documentStoreType
                    , filename.Trim()
                    , cont
                    , kapottKrtDokObj
                    , xmlDoc
                    );
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            /// -----------------------------------------------------------------------------------------------
            XmlDocument feltoltesbolKapottXmlDoc = new XmlDocument();
            try
            {
                feltoltesbolKapottXmlDoc.LoadXml((string)result.Record);
            }
            catch (Exception exc1)
            {
                Logger.Error(String.Format("Feltoltesbol kapott xml string konvertalasa xml docca hiba.\nMessage: {0}\nStackTrace: {1}\nXmlString: {2}", exc1.Message, exc1.StackTrace, (string)result.Record));
                result = new Result();
                result.ErrorCode = "UPLDXML0001";
                result.ErrorMessage = String.Format("Feltoltesbol kapott xml string konvertalasa xml docca hiba.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
                throw new ResultException(result);
            }

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentumManager _UMCDocumentumManager = new UCMDocumentumManager();
                /// -----------------------------------------------------------------------------------------------
                result = _UMCDocumentumManager.CheckInUploadedFileWithCTT(execparam
                    , feltoltesbolKapottXmlDoc);
            }
            else
            {
                /// -----------------------------------------------------------------------------------------------
                result = _CheckInUploadedFileWithCTT(execparam
                    , feltoltesbolKapottXmlDoc, cont);
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    /// <summary>
    /// Felt�lt�tt file checkin.
    /// A k�t l�p�ses felt�lt�s m�sodik l�p�se. Az els�: UploadFromeRecordWithCTT.
    /// A xmlStringk�nt �tadand� param�terek le�r�s�t l�sd itt: UploadFromeRecordWithCTT.
    /// 
    /// Felt�lt�tt �llom�ny veriz�j�nak lek�rdez�se.
    /// BDC k�t�s - egy�b (meta)mez�k �rt�keinek be�ll�t�sa.
    /// Felt�lt�tt file checkin.
    /// K�ls� SHA1 chksum sz�mol�sa, bejegyz�se.
    /// Szerver oldalo al��r�s.
    /// Mappatartalom bejegyz�se.
    /// 
    /// Record mezoben visszaadott xml string:
    /// <result>
    ///    <dokumentumFullUrl>dok full url-je</dokumentumFullUrl>
    ///    <tortentUjKrtDokBejegyzes>tortent-e krt_dok bejegyzes IGEN/NEM</tortentUjKrtDokBejegyzes>
    ///    <csatolmanyId>vajon ez miiii?</csatolmanyId>"
    ///    <csatolmanyVerzio>verizo, az sql update-hez kell</csatolmanyVerzio>
    /// </result>
    /// 
    /// </summary>
    /// <param name="execparam">a felhasznalo id kell, meg krt_dok GUID kell!</param>
    /// <param name="xmlStrParams">Tov�bbi param�terek megad�sa xml stringk�nt.</param>
    /// <returns>Result - az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza, Record mezoben xml adatok. Vagy hiba.</returns>
    [WebMethod(Description = "<br><b>ExecParam</b> - a felhasznalo id kell, meg krt_dok GUID kell! " +
          "<br><b>xmlStrParams</b> - Tov�bbi param�terek megad�sa xml stringk�nt." +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza,Record mezoben xml adatok. Vagy hiba.")]
    public Result CheckInUploadedFileWithCTT(ExecParam execparam
        , String xmlStrParams)
    {
        Logger.Info("------------- CheckInUploadedFileWithCTT -------------");
        Result result = new Result();

        #region Parameter XML string feldolgozasa.

        XmlDocument xmlDoc = new XmlDocument();

        if (xmlStrParams != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlDoc.LoadXml(xmlStrParams);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, xmlStrParams));
                result.ErrorCode = "XMLPAR0001";
                result.ErrorMessage = String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return result;
            }
        }

        #endregion

        //  Parameterezes ellenorzese

        string sourceSharePath = GetParam("sourceSharePath", xmlDoc);

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(GetParam("documentStoreType", xmlDoc)) ||
            String.IsNullOrEmpty(GetParam("filename", xmlDoc))
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id -" : "";
            strErrorInfo += String.IsNullOrEmpty(GetParam("documentStoreType", xmlDoc)) ? " documentStoreType -" : "";
            strErrorInfo += String.IsNullOrEmpty(GetParam("filename", xmlDoc)) ? " filename -" : "";

            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(201001);
        }

        //  ---------------------------------------------------------------------

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            String documentStoreType = GetParam("documentStoreType", xmlDoc);
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentumManager _UMCDocumentumManager = new UCMDocumentumManager();
                /// -----------------------------------------------------------------------------------------------
                result = _UMCDocumentumManager.CheckInUploadedFileWithCTT(execparam
                    , xmlDoc);
            }
            else
            {
                /// -----------------------------------------------------------------------------------------------
                result = _CheckInUploadedFileWithCTT(execparam
                    , xmlDoc, null);
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    /// <summary>
    /// File felt�lt�se.
    /// <br>L�tez�t fel�l�r.
    /// <br>BELSO
    /// </summary>
    /// <returns>Result - eredm�ny Uid = dokumentum eDoc db-ben l�v� objektum Id-ja</returns>
    /// 
    private Result _UploadFromeRecord(ExecParam execparam
        , String documentStoreType
        , String documentSite
        , String documentStore
        , String documentFolder
        , String filename
        , byte[] cont
        , KRT_Dokumentumok kapottKrtDokObj
        , bool iktatas)
    {
        Result _ret = new Result();

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        /// -----------------------------------------------------------------------------------------------

        string siteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");
        string rootDoktarSite = UI.GetAppSetting("DokumentumtarSite");

        //documentSite = String.Format("{0}/{1}/",DateTime.Today.ToString("yyyy"), DateTime.Today.ToString("MM"));
        //documentSite = String.Format("{0}/{1}/", DateTime.Today.ToString("yyyy"), DateTime.Today.ToString("MM"));

        /// 1. tarhelyek ellenorzese
        /// ha nincs letrehozzuk oket

        //Result eDocMappaGuid = TarhelyekEllenorzeseMappakDb(execparam, documentSite, documentStore, documentFolder);

        //if (!String.IsNullOrEmpty(eDocMappaGuid.ErrorCode)) return eDocMappaGuid;

        Result storeMappaGuid = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, documentSite, documentStore, documentFolder, String.Empty);

        if (!String.IsNullOrEmpty(storeMappaGuid.ErrorCode)) return storeMappaGuid;

        /// -----------------------------------------------------------------------------------------------
        /// 2. file feltoltese ahova kell
        /// 
        Result felotoltottFileRet = null;

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();

            if (!documentSite.EndsWith("/")) { documentSite += "/"; }
            if (!documentStore.EndsWith("/")) { documentStore += "/"; }

            Logger.Info("Csekaut, ha letezik mar...");
            ListUtilService lisu = new Contentum.eDocument.SharePoint.ListUtilService();
            Result listResult = lisu.GetListItemIdByName(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), filename);

            if (String.IsNullOrEmpty(listResult.ErrorCode))
            {
                Result csekoutEllResult = ds.CheckOut(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), filename);
                if (!String.IsNullOrEmpty(csekoutEllResult.ErrorCode))
                {
                    Logger.Error("Csekaut hiba: " + csekoutEllResult.ErrorCode + "; " + csekoutEllResult.ErrorMessage);
                    return csekoutEllResult;
                }
            }
            else
            {
                if (!listResult.ErrorCode.Equals("190000002"))
                {
                    Logger.Error(String.Format("GetListItemIdByName hiba: {0}\n{1}", listResult.ErrorCode, listResult.ErrorMessage));
                    return listResult;
                }
            }

            Logger.Info("Sharepoint-ba file feltoltese.");
            felotoltottFileRet = ds.Upload(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), filename, cont);
            if (!String.IsNullOrEmpty(felotoltottFileRet.ErrorCode))
            {
                Logger.Error("Sharepoint-ba file feltoltese hiba: " + felotoltottFileRet.ErrorCode + "; " + felotoltottFileRet.ErrorMessage);
                return felotoltottFileRet;
            }


            /// ------------------------------------------------------------
            /// BDC kotes beallitasa

            //if (iktatas) {
            //    string ufilePath = ((!documentSite.EndsWith("/") && !String.IsNullOrEmpty(documentSite)) ? documentSite + "/" : documentSite)
            //        + ((!documentStore.EndsWith("/") && !String.IsNullOrEmpty(documentStore)) ? documentStore + "/" : documentStore)
            //        + ((!documentFolder.EndsWith("/") && !String.IsNullOrEmpty(documentFolder)) ? documentFolder + "/" : documentFolder);

            //    if (felotoltottFileRet.Uid != null)
            //    {
            //        AxisUploadToSps.Upload us = new AxisUploadToSps.Upload();
            //        try
            //        {
            //            string useredm = us.SetItemBDCConnection(ufilePath, filename, "91b526d8-2d51-455f-b926-4f464ec6b777"); //felotoltottFileRet.Uid.ToString());
            //            Logger.Info(String.Format("BDC beallitasa ennek :\nFile: {1}{2}\n ID: {0}", felotoltottFileRet.Uid.ToString(), ufilePath, filename));
            //            Logger.Info(String.Format("Eredm�ny: {0}", useredm));
            //        }
            //        catch (Exception ex)
            //        {
            //            Logger.Info(String.Format("BDC beallitasa ennek :\nFile: {1}{2}\n ID: {0}", felotoltottFileRet.Uid.ToString(), ufilePath, filename));
            //            Logger.Info(String.Format("EXCEPTION VOLT!\n{0}\n{1}", ex.ToString(), ex.InnerException));
            //        }
            //    }
            //    else
            //    {
            //        Logger.Info(String.Format("BDC beallitasa ennek :\nFile: {1}{2}\n ID: {0}", felotoltottFileRet.Uid.ToString(), ufilePath, filename));
            //        Logger.Info("Kapott ID ures!!!!");
            //    }
            //} //if iktatas

            ///
            /// ------------------------------------------------------------

            Contentum.eDocument.SharePoint.FolderService fos = new Contentum.eDocument.SharePoint.FolderService();


            //Logger.Info("-Verziozott-e a doclib?");
            //Result ret = fos.IsVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore));
            //bool verziozott = (bool)ret.Record;

            //if (!verziozott)
            //{
            //    Logger.Info("-Nem. Verzi�z�s bekapcsol�sa �s checkin.");
            //    Result retSetVer = fos.SetVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore), true);
            //    if (!String.IsNullOrEmpty(retSetVer.ErrorCode))
            //    {
            //        Logger.Info(String.Format("Verizozas bekapcsolasa sikertelen: site: {0} - store: {1}", documentSite, documentStore));
            //        Logger.Info(String.Format("Hiba oka: {0} - {1} ", retSetVer.ErrorCode, retSetVer.ErrorMessage));
            //        return retSetVer;
            //    }
            //}
            //else
            //{
            //    Logger.Info("-Igen. Sharepoint-ba feltoltott file checkin.");
            //    Result checkinRet = ds.CheckIn(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), filename, ".", "0"); //Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn);
            //    if (!String.IsNullOrEmpty(checkinRet.ErrorCode))
            //    {
            //        /// -2130245361 = nincs be�ll�tva a folderen a verzio kezeles beallitva
            //        if (!checkinRet.ErrorCode.Equals("-2130245361"))
            //        {
            //            Logger.Error("Sharepoint-ba feltoltott file checkin hiba: " + checkinRet.ErrorCode + "; " + checkinRet.ErrorMessage);
            //            return checkinRet;
            //        }
            //        else
            //        {
            //            Logger.Info("Sharepoint-ba feltoltott file checkin hiba: Nincs beallitva a verziokezeles a folderen!");
            //        }
            //    }
            //}
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

            Logger.Info("Filesystem-be file feltoltese.");

            string path = RemoveSlash(documentSite) + "/" + RemoveSlash(documentSite) + "/" + RemoveSlash(documentFolder);
            path = path.Replace('/', '\\');

            FsWs fsWs = new FsWs();

            felotoltottFileRet = fsWs.Upload(path, filename, cont);
            if (!String.IsNullOrEmpty(felotoltottFileRet.ErrorCode))
            {
                Logger.Error("Filesystem file feltoltese hiba: " + felotoltottFileRet.ErrorMessage.ToString());
                return felotoltottFileRet;
            }
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {
            /// TODO:
            Logger.Info("Database-be file feltoltese. LESZ MAJD!");

        }

        /// -----------------------------------------------------------------------------------------------
        /// 3. file eDocba
        /// 


        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
        /*KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

        krt_Dokumentumok.Updated.SetValueAll(false);
        krt_Dokumentumok.Base.Updated.SetValueAll(false);
        //krt_Dokumentumok.Base.Ver = 8; // csak updatenel ugyanazt a vert kell beadni, amivel kiszelektaltuk

        /// ------- kotelezok:

        krt_Dokumentumok.Csoport_Id_Tulaj = execparam.Felhasznalo_Id; 
        krt_Dokumentumok.Updated.Csoport_Id_Tulaj = true;

        krt_Dokumentumok.FajlNev = filename;
        krt_Dokumentumok.Updated.FajlNev = true;

        krt_Dokumentumok.Meret = cont.Length.ToString();
        krt_Dokumentumok.Updated.Meret = true;

        krt_Dokumentumok.Allapot = "1";
        krt_Dokumentumok.Updated.Allapot = true;

        krt_Dokumentumok.Tipus = (filename.LastIndexOf('.') == -1) ? "" : filename.Trim().Substring(filename.LastIndexOf('.'));
        krt_Dokumentumok.Updated.Tipus = true;

        /// -------

        krt_Dokumentumok.Alkalmazas_Id = execparam.Alkalmazas_Id;
        krt_Dokumentumok.Updated.Alkalmazas_Id = true;

        krt_Dokumentumok.Dokumentum_Id = krt_Dokumentumok.Id;
        krt_Dokumentumok.Updated.Dokumentum_Id = true;
        
        krt_Dokumentumok.VerzioJel = "001"; //? mi a fasz ez, meg minek?
        krt_Dokumentumok.Updated.VerzioJel = true;

        krt_Dokumentumok.External_Id = (felotoltottFileRet.Uid == null) ? "" : felotoltottFileRet.Uid.ToString(); 
        krt_Dokumentumok.Updated.External_Id = true;

        krt_Dokumentumok.External_Link = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + siteCollectionUrl + rootDoktarSite + documentSite + documentStore + ((documentFolder.EndsWith("/")) ? documentFolder : documentFolder + "/") + filename;
        krt_Dokumentumok.Updated.External_Link = true;

        // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
        // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
        krt_Dokumentumok.External_Info = siteCollectionUrl + rootDoktarSite + RemoveSlash(documentSite) + ";" + RemoveSlash(documentStore) + ";" + RemoveSlash(documentFolder);
        krt_Dokumentumok.Updated.External_Info = true;

        krt_Dokumentumok.External_Source = documentStoreType;
        krt_Dokumentumok.Updated.External_Source = true;

        //krt_Dokumentumok.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
        krt_Dokumentumok.Leiras = filename;
        krt_Dokumentumok.Updated.Leiras = true;

        krt_Dokumentumok.Tartalom = "<null>";
        krt_Dokumentumok.Updated.Tartalom = true;

        krt_Dokumentumok.Kivonat = "<null>";
        krt_Dokumentumok.Updated.Kivonat = true;

        Logger.Info("File regisztralasa eDocba.");
        _ret = dokumentumService.Insert(execparam, krt_Dokumentumok);
        
        */
        if (kapottKrtDokObj == null)
        {
            kapottKrtDokObj = new KRT_Dokumentumok();
            kapottKrtDokObj.Updated.SetValueAll(false);
            kapottKrtDokObj.Base.Updated.SetValueAll(false);
        }

        kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
        kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

        kapottKrtDokObj.FajlNev = filename.Trim();
        kapottKrtDokObj.Updated.FajlNev = true;

        kapottKrtDokObj.Meret = cont.Length.ToString();
        kapottKrtDokObj.Updated.Meret = true;

        kapottKrtDokObj.Allapot = "1";
        kapottKrtDokObj.Updated.Allapot = true;

        //
        //  CR#2191 innenn

        /*
        if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
        {
            kapottKrtDokObj.Tipus = "Egy�b";
            kapottKrtDokObj.Updated.Tipus = true;
        }

        //(filename.LastIndexOf('.') == -1) ? "" : filename.Trim().Substring(filename.LastIndexOf('.'));
        if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
        {
            kapottKrtDokObj.Formatum = GetFormatum(execparam, filename); // (fileTipus == null) ? "Egy�b" : fileTipus;
            kapottKrtDokObj.Updated.Formatum = true;
        }
        */

        if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
        {
            kapottKrtDokObj.Tipus = GetFormatum(execparam, filename);
            kapottKrtDokObj.Updated.Tipus = true;
        }

        if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
        {
            kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
            kapottKrtDokObj.Updated.Formatum = true;
        }

        //
        //  CR#2191 idaig


        /// -------

        kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
        kapottKrtDokObj.Updated.Alkalmazas_Id = true;

        kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
        kapottKrtDokObj.Updated.Dokumentum_Id = true;

        kapottKrtDokObj.VerzioJel = "001"; //? mi a fasz ez, meg minek?
        kapottKrtDokObj.Updated.VerzioJel = true;

        kapottKrtDokObj.External_Id = (felotoltottFileRet.Uid == null) ? "" : felotoltottFileRet.Uid.ToString();
        kapottKrtDokObj.Updated.External_Id = true;

        kapottKrtDokObj.External_Link = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + siteCollectionUrl + rootDoktarSite + documentSite + documentStore + ((documentFolder.EndsWith("/")) ? documentFolder : documentFolder + "/") + filename;
        kapottKrtDokObj.Updated.External_Link = true;

        // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
        // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
        kapottKrtDokObj.External_Info = siteCollectionUrl + rootDoktarSite + RemoveSlash(documentSite) + ";" + RemoveSlash(documentStore) + ";" + RemoveSlash(documentFolder);
        kapottKrtDokObj.Updated.External_Info = true;

        kapottKrtDokObj.External_Source = documentStoreType;
        kapottKrtDokObj.Updated.External_Source = true;

        //kapottKrtDokObj.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
        kapottKrtDokObj.Leiras = originalFileName;
        kapottKrtDokObj.Updated.Leiras = true;

        kapottKrtDokObj.TartalomHash = "<null>";
        kapottKrtDokObj.Updated.TartalomHash = true;

        kapottKrtDokObj.KivonatHash = "<null>";
        kapottKrtDokObj.Updated.KivonatHash = true;

        // a fajl feltoltesekor az alairas felulvizsgalat datumot
        // automatikusan generaljuk a feltoltessel egyidejuleg
        // de csak ha van ElektronikusAlairas informacionk
        //if (!String.IsNullOrEmpty(kapottKrtDokObj.ElektronikusAlairas)
        //    && kapottKrtDokObj.ElektronikusAlairas != "<null>")
        //{
        //    kapottKrtDokObj.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
        //    kapottKrtDokObj.Updated.AlairasFelulvizsgalat = true;
        //}

        Logger.Info("File regisztralasa eDocba.");
        _ret = dokumentumService.Insert(execparam, kapottKrtDokObj);

        if (!String.IsNullOrEmpty(_ret.ErrorCode))
        {
            Result tmpRes = DeleteExternalFile(documentStoreType, "", documentSite, documentStore, documentFolder, filename);
            Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);

            /// rollback
            //ContextUtil.SetAbort();

            return _ret;
        }

        _ret.Record = Convert.ToString(Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + siteCollectionUrl + rootDoktarSite + documentSite + documentStore + ((documentFolder.EndsWith("/")) ? documentFolder : documentFolder + "/") + filename);

        /// -----------------------------------------------------------------------------------------------
        /// 4. mappatartalom insert
        /// 
        //Logger.Info("Mappatartalom eDocba.");
        //Result eDocMappatartalomGuid = SeteDocMappatartalom(execparam, eDocMappaGuid.Uid.ToString(), _ret.Uid.ToString());
        //if (!String.IsNullOrEmpty(eDocMappatartalomGuid.ErrorCode))
        //{
        //    Result tmpRes = DeleteExternalFile(documentStoreType, documentSite, documentStore, documentFolder, filename);

        //    Logger.Error("Mappatartalom eDocba hiba: " + eDocMappatartalomGuid.ErrorCode + "; " + eDocMappatartalomGuid.ErrorMessage);

        //    /// rollback
        //    //ContextUtil.SetAbort();

        //    return eDocMappatartalomGuid;
        //}

        /// -----------------------------------------------------------------------------------------------

        Logger.Info("UploadFromeRecord vege");
        return _ret;
    }

    /// <summary>
    /// Tipus kod visszaadasa a megadott kod vagy kiterjesztes alapjan
    /// </summary>
    /// <param name="filename">string - a file neve, kiterjesztessel</param>
    /// <returns>string - a kapott adatokhoz tartozo kod</returns>
    private string GetFormatum(ExecParam ex1, string filename)
    {
        Logger.Info(String.Format("DocumentService.GetFormatum indul. Filename: {0}", filename));

        Result kodok = GetKodTar(ex1, "DOKUMENTUM_FORMATUM");

        if (!String.IsNullOrEmpty(kodok.ErrorCode))
        {
            Logger.Info(String.Format("DOKUMENTUM_FORMATUM kodlekerdezesek hiba: {0} {1}", kodok.ErrorCode, kodok.ErrorMessage));
            return "Egyeb";
        }

        if (kodok.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Info(String.Format("DOKUMENTUM_FORMATUM kodlekerdezesek ures, nincs eredmenysor."));
            return "Egyeb";
        }

        Logger.Info(String.Format("kapott kodok darabszama: {0}", kodok.Ds.Tables[0].Rows.Count));

        string kiterjesztes = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
        Logger.Info(String.Format("Megallapitott kiterjesztes: {0}", kiterjesztes));

        int idx = 0;
        string retVal = "";
        while ((idx < kodok.Ds.Tables[0].Rows.Count) && (String.IsNullOrEmpty(retVal)))
        {
            //string egyeb = (String.IsNullOrEmpty(String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Kod"]))) ? "." : kodok.Ds.Tables[0].Rows[idx]["Kod"].ToString();
            string egyeb = Convert.ToString(kodok.Ds.Tables[0].Rows[idx]["Kod"]);
            if (egyeb.Trim().ToLower().Equals(kiterjesztes.Trim().ToLower()))
            {
                //retVal = String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Kod"]);
                retVal = String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Nev"]);
            }
            idx++;
        }
        Logger.Info(String.Format("Megallapitott tipus: {0}", retVal));

        return String.IsNullOrEmpty(retVal) ? KodTarak.DOKUMENTUMTIPUS.Egyeb : retVal; //"Egyeb" : retVal;
    }

    /// <summary>
    /// Felt�lt�s shareb�l.
    /// DEPRECATED!
    /// Nem haszn�lt.
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="documentSite"></param>
    /// <param name="documentStore"></param>
    /// <param name="documentFolder"></param>
    /// <param name="sourceSharePath"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    [WebMethod(Description = "<br><b>ExecParam</b> - az alkalmazas es felhasznalo id-knek ki kell lenniuk toltve. " +
          "<br><b>documentStoreType</b> - ez legyen: Contentum.eUtility.Constants.DocumentStoreType.SharePoint " +
          "<br><b>documentSite</b> - site neve => \"eRecord\" legyen!" +
          "<br><b>documentStore</b> - doclib neve" +
          "<br><b>documentFolder</b> - konyvtar(ak) nev(ei); ha tobb, / jellel elvalasztva " +
          "<br><b>sourceSharePath</b> - forras share eleresi ut" +
          "<br><b>filename</b> - file neve" +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.")]
    public Result UploadFromShare(ExecParam execparam
        , String documentStoreType
        , String documentSite
        , String documentStore
        , String documentFolder
        , String sourceSharePath
        , String filename)
    {
        Logger.Info("UploadFromShare start - DEPRECATED");
        Logger.Info("Kapott parameterek: documentStoreType=" + documentStoreType + "; documentSite=" + documentSite + "; documentFolder=" + documentFolder + "; sourceSharePath= " + sourceSharePath + "; filename=" + filename);

        Result result = new Result();
        return result;

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(documentStore) ||
            String.IsNullOrEmpty(documentFolder) ||
            String.IsNullOrEmpty(sourceSharePath) ||
            String.IsNullOrEmpty(filename)
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStore) ? " documentStore" : "";
            strErrorInfo += String.IsNullOrEmpty(documentFolder) ? " documentFolder" : "";
            strErrorInfo += String.IsNullOrEmpty(sourceSharePath) ? " sourceSharePath" : "";
            strErrorInfo += String.IsNullOrEmpty(filename) ? " filename" : "";

            // TODO: alap parameter hibak:
            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));
            return ResultError.CreateNewResultWithErrorCode(200000);
        }
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            /// -----------------------------------------------------------------------------------------------
            result = _UploadFromShare(execparam
            , documentStoreType
            , documentSite
            , documentStore
            , documentFolder
            , sourceSharePath
            , filename);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    /// <summary>
    /// BELSO UploadFromShare
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="documentSite"></param>
    /// <param name="documentStore"></param>
    /// <param name="documentFolder"></param>
    /// <param name="sourceSharePath"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    private Result _UploadFromShare(ExecParam execparam
        , String documentStoreType
        , String documentSite
        , String documentStore
        , String documentFolder
        , String sourceSharePath
        , String filename)
    {
        Result _ret = new Result();

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        // ---------------------------------------------------

        String source = ((!String.IsNullOrEmpty(sourceSharePath) && !sourceSharePath.EndsWith("/")) ? sourceSharePath + "/" : sourceSharePath) +
            filename;

        Logger.Info(String.Format("source file: {0}", source));

        byte[] fileContents = null;

        Logger.Info("Olvasas indul...");
        // get data
        try
        {
            System.IO.FileStream stream = new System.IO.FileStream(source, System.IO.FileMode.Open, System.IO.FileAccess.Read);

            System.IO.BinaryReader reader = new System.IO.BinaryReader(stream);

            fileContents = reader.ReadBytes((int)stream.Length);

            reader.Close();
            stream.Close();
        }
        catch (Exception ex)
        {
            Logger.Info(String.Format("File adatok olvasasa hiba ({0}): {1}", source, ex.ToString()));
            _ret.ErrorCode = "11111113454";
            _ret.ErrorMessage = String.Format("File adatok olvasasa hiba ({0}): {1}", source, ex.ToString());
            return _ret;
        }

        _ret = this._UploadFromeRecordWithCTT(execparam, documentStoreType, filename, fileContents, null, new XmlDocument());
        return _ret;

        /// ---------------------------------------------------


        string siteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");
        string rootDoktarSite = UI.GetAppSetting("DokumentumtarSite");

        //documentSite = String.Format("{0}/{1}/", DateTime.Today.ToString("yyyy"), DateTime.Today.ToString("MM"));
        documentSite = String.Format("{0}/{1}/", DateTime.Today.ToString("yyyy"), DateTime.Today.ToString("MM"));

        /// -----------------------------------------------------------------------------------------------
        /// 1. cel mappa megvan-e eDocba, id kerese, majd dokstoreban l�trehozni
        /// 

        //Result eDocMappaGuid = TarhelyekEllenorzeseMappakDb(execparam, documentSite, documentStore, documentFolder);

        //if (!String.IsNullOrEmpty(eDocMappaGuid.ErrorCode)) return eDocMappaGuid;

        Result storeMappaGuid = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, documentSite, documentStore, documentFolder, String.Empty);

        if (!String.IsNullOrEmpty(storeMappaGuid.ErrorCode)) return storeMappaGuid;


        /// -----------------------------------------------------------------------------------------------
        /// 2. file feltoltese ahova kell
        /// 
        Result felotoltottFileRet = new Result();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();

            if (!documentSite.EndsWith("/")) { documentSite += "/"; }
            if (!documentStore.EndsWith("/")) { documentStore += "/"; }

            Logger.Info("Sharepoint-ba file feltoltese elokeszites.");

            string spsBaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUrl");

            string vartEredmenyStr = spsBaseUrl + siteCollectionUrl + rootDoktarSite +
            ((!String.IsNullOrEmpty(documentSite) && !documentSite.EndsWith("/")) ? documentSite + "/" : documentSite) +
            ((!String.IsNullOrEmpty(documentStore) && !documentStore.EndsWith("/")) ? documentStore + "/" : documentStore) +
            ((!String.IsNullOrEmpty(documentFolder) && !documentFolder.EndsWith("/")) ? documentFolder + "/" : documentFolder) +
            filename;

            Logger.Info(String.Format("Vart eredemny: {0}", vartEredmenyStr));

            AxisUploadToSps.Upload upld = this.GetAxisUploadToSps();

            Logger.Info("Sharepoint-ba file feltoltese futtatas...");

            string strUploadEredmeny = upld.UploadFromUrl(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), RemoveSlash(sourceSharePath), filename);

            Logger.Info("Sharepoint-ba file feltoltese futtatas... vege.");

            if (!strUploadEredmeny.Equals(vartEredmenyStr))
            {
                Logger.Error("Sharepoint-ba file feltoltese hiba! Vart eredmeny: " + vartEredmenyStr + "\nKapott eredmeny: " + strUploadEredmeny);
                Result retuuu = new Result();
                retuuu.ErrorCode = "11111111111";
                retuuu.ErrorMessage = strUploadEredmeny;
                return retuuu;
            }

            Contentum.eDocument.SharePoint.FolderService fos = new Contentum.eDocument.SharePoint.FolderService();

            Logger.Info("-Verziozott-e a doclib?");
            //Result ret = fos.IsVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore));
            //bool verziozott = (bool)ret.Record;

            //if (!verziozott)
            //{
            //    Logger.Info("-Nem. Verzi�z�s bekapcsol�sa �s checkin.");
            //    Result retSetVer = fos.SetVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore), true);
            //    if (!String.IsNullOrEmpty(retSetVer.ErrorCode))
            //    {
            //        Logger.Info(String.Format("Verizozas bekapcsolasa sikertelen: site: {0} - store: {1}", documentSite, documentStore));
            //        Logger.Info(String.Format("Hiba oka: {0} - {1} ", retSetVer.ErrorCode, retSetVer.ErrorMessage));
            //        return retSetVer;
            //    }
            //}
            //else
            //{
            //    Logger.Info("-Igen. Sharepoint-ba feltoltott file checkin.");
            //    Result checkinRet = ds.CheckIn(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(documentFolder), filename, ".", "0"); //Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn);
            //    if (!String.IsNullOrEmpty(checkinRet.ErrorCode))
            //    {
            //        /// -2130245361 = nincs be�ll�tva a folderen a verzio kezeles beallitva
            //        if (!checkinRet.ErrorCode.Equals("-2130245361"))
            //        {
            //            Logger.Error("Sharepoint-ba feltoltott file checkin hiba: " + checkinRet.ErrorCode + "; " + checkinRet.ErrorMessage);
            //            return checkinRet;
            //        }
            //        else
            //        {
            //            Logger.Info("Sharepoint-ba feltoltott file checkin hiba: Nincs beallitva a verziokezeles a folderen!");
            //        }
            //    }
            //}
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

            Logger.Info("Filesystem-be file feltoltese.");

            //string path = RemoveSlash(documentSite) + "/" + RemoveSlash(documentSite) + "/" + RemoveSlash(documentFolder);
            //path = path.Replace('/', '\\');

            //FsWs fsWs = new FsWs();

            //felotoltottFileRet = fsWs.Upload(path, filename, cont);
            //if (!String.IsNullOrEmpty(felotoltottFileRet.ErrorCode))
            //{
            //    Logger.Error("Filesystem file feltoltese hiba: " + felotoltottFileRet.ErrorMessage.ToString());
            //    return felotoltottFileRet;
            //}
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {
            /// TODO:
            Logger.Info("Database-be file feltoltese. LESZ MAJD!");

        }

        /// -----------------------------------------------------------------------------------------------
        /// 3. file eDocba
        /// 

        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

        krt_Dokumentumok.Updated.SetValueAll(false);
        krt_Dokumentumok.Base.Updated.SetValueAll(false);
        //krt_Dokumentumok.Base.Ver = 8; // csak updatenel ugyanazt a vert kell beadni, amivel kiszelektaltuk

        /// ------- kotelezok:

        krt_Dokumentumok.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
        krt_Dokumentumok.Updated.Csoport_Id_Tulaj = true;

        krt_Dokumentumok.FajlNev = filename.Trim();
        krt_Dokumentumok.Updated.FajlNev = true;

        krt_Dokumentumok.Meret = "0"; // ezt majd vissza kellene adnia a feltoltonek!
        krt_Dokumentumok.Updated.Meret = true;

        krt_Dokumentumok.Allapot = "1";
        krt_Dokumentumok.Updated.Allapot = true;

        //
        // CR#2191 innen

        /*
        krt_Dokumentumok.Tipus = "Egy�b";
        krt_Dokumentumok.Updated.Tipus = true;

        krt_Dokumentumok.Formatum = GetFormatum(execparam, filename);
        krt_Dokumentumok.Updated.Formatum = true;
        */

        krt_Dokumentumok.Tipus = GetFormatum(execparam, filename);
        krt_Dokumentumok.Updated.Tipus = true;

        krt_Dokumentumok.Formatum = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
        krt_Dokumentumok.Updated.Formatum = true;

        //
        // CR#2191 idaig

        /// -------

        krt_Dokumentumok.Alkalmazas_Id = execparam.Alkalmazas_Id;
        krt_Dokumentumok.Updated.Alkalmazas_Id = true;

        krt_Dokumentumok.Dokumentum_Id = krt_Dokumentumok.Id;
        krt_Dokumentumok.Updated.Dokumentum_Id = true;

        krt_Dokumentumok.VerzioJel = "001"; //? mi a fasz ez, meg minek?
        krt_Dokumentumok.Updated.VerzioJel = true;

        felotoltottFileRet.Uid = Guid.NewGuid().ToString();
        krt_Dokumentumok.External_Id = (felotoltottFileRet.Uid == null) ? "x" : felotoltottFileRet.Uid.ToString();
        krt_Dokumentumok.Updated.External_Id = true;

        krt_Dokumentumok.External_Link = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + siteCollectionUrl + rootDoktarSite + documentSite + documentStore + ((documentFolder.EndsWith("/")) ? documentFolder : documentFolder + "/") + filename;
        krt_Dokumentumok.Updated.External_Link = true;

        // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
        // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
        krt_Dokumentumok.External_Info = siteCollectionUrl + rootDoktarSite + RemoveSlash(documentSite) + ";" + RemoveSlash(documentStore) + ";" + RemoveSlash(documentFolder);
        krt_Dokumentumok.Updated.External_Info = true;

        krt_Dokumentumok.External_Source = documentStoreType;
        krt_Dokumentumok.Updated.External_Source = true;

        //krt_Dokumentumok.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
        krt_Dokumentumok.Leiras = originalFileName;
        krt_Dokumentumok.Updated.Leiras = true;

        krt_Dokumentumok.TartalomHash = "<null>";
        krt_Dokumentumok.Updated.TartalomHash = true;

        krt_Dokumentumok.KivonatHash = "<null>";
        krt_Dokumentumok.Updated.KivonatHash = true;

        Logger.Info("File regisztralasa eDocba.");
        _ret = dokumentumService.Insert(execparam, krt_Dokumentumok);

        if (!String.IsNullOrEmpty(_ret.ErrorCode))
        {
            Result tmpRes = DeleteExternalFile(documentStoreType, "", documentSite, documentStore, documentFolder, filename);
            Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);

            /// rollback
            //ContextUtil.SetAbort();

            return _ret;
        }


        /// -----------------------------------------------------------------------------------------------
        /// 4. mappatartalom insert
        /// 
        //Logger.Info("Mappatartalom eDocba.");
        //Result eDocMappatartalomGuid = SeteDocMappatartalom(execparam, eDocMappaGuid.Uid.ToString(), _ret.Uid.ToString());
        //if (!String.IsNullOrEmpty(eDocMappatartalomGuid.ErrorCode))
        //{
        //    Result tmpRes = DeleteExternalFile(documentStoreType, documentSite, documentStore, documentFolder, filename);

        //    Logger.Error("Mappatartalom eDocba hiba: " + eDocMappatartalomGuid.ErrorCode + "; " + eDocMappatartalomGuid.ErrorMessage);

        //    /// rollback
        //    //ContextUtil.SetAbort();

        //    return eDocMappatartalomGuid;
        //}

        /// -----------------------------------------------------------------------------------------------

        Logger.Info("UploadFromShare vege");
        return _ret;
    }

    /// <summary>
    /// Elejerol es vegerol leveszi a / vagy \ jelet.
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    private string RemoveSlash(String s)
    {
        return DocumentService.Utility.RemoveSlash(s);
    }


    /// <summary>
    /// Execparamban (Record_id) megadott id-ju, MOSSban t�rolt dokument-ra, undo checkout.
    /// Kit�rli a checkedouted mez�ket.
    /// 
    /// KRT Dokumentum lek�rdez�se.
    /// Undo chkout.
    /// 
    /// </summary>
    /// <param name="execparam">ExecParam -  record_id-ben a dokumentum guid-ja</param>
    /// <returns>Result - idvel jon vissza vagy err-el</returns>
    /// 
    [WebMethod]
    public Result UndoCheckOut(ExecParam execparam)
    {

        if (execparam.Felhasznalo_Id == null ||
            execparam.Record_Id == null)
        {
            // TODO: alap parameter hibak:
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
        Result dok = dokumentumService.Get(execparam);

        if (!String.IsNullOrEmpty(dok.ErrorCode))
            return dok;

        string filenev = dok.Ds.Tables[0].Rows[0]["FajlNev"].ToString().Trim();
        int utvonalHossza = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Length;
        string utvonal = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Substring(0, utvonalHossza - filenev.Length);
        string documentStoreType = dok.Ds.Tables[0].Rows[0]["External_Source"].ToString();
        string documentExternalInfo = dok.Ds.Tables[0].Rows[0]["External_Info"].ToString();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();
            Result doktarRet = ds.UndoCheckOut(GetPart(documentExternalInfo, 0), GetPart(documentExternalInfo, 1), GetPart(documentExternalInfo, 2), filenev);

            if (!String.IsNullOrEmpty(doktarRet.ErrorCode))
            {
                return doktarRet;
            }
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {

        }

        KRT_Dokumentumok updateDok = (KRT_Dokumentumok)dok.Record;

        updateDok.Updated.SetValueAll(false);
        updateDok.Base.Updated.SetValueAll(false);
        //updateDok.Base.Ver = dok.Ds.Tables[0].Rows[0]["Ver"].ToString();

        updateDok.CheckedOut = "";
        updateDok.Updated.CheckedOut = true;
        updateDok.CheckedOutTime = "";
        updateDok.Updated.CheckedOutTime = true;

        Result retDokUpdate = dokumentumService.Update(execparam, updateDok);

        if (!String.IsNullOrEmpty(retDokUpdate.ErrorCode))
            return retDokUpdate;

        return dok;
    }

    /// <summary>
    /// MOSSban t�rolt �llom�ny, legutols� verzi�j�nak kicsekkol�sa.
    /// �ll�tja a dokumentumok checkedouted mez�ket.
    /// 
    /// KRT Dokumentum bejegyz�s keres�se.
    /// MOSS file chk out.
    /// 
    /// TODO: file link es/vagy tartalom visszaadasa.
    /// 
    /// </summary>
    /// <param name="execparam">ExecParam - record_id-ben a dokumentum guidja</param>
    /// <returns>Result - a DataSet-ben (majd :( ) a rekorddal  vagy error</returns>
    /// 
    [WebMethod]
    public Result CheckOut(ExecParam execparam)
    {
        if (execparam.Felhasznalo_Id == null ||
            execparam.Record_Id == null)
        {
            // TODO: alap parameter hibak:
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();

        Result dok = dokumentumService.Get(execparam);

        if (!String.IsNullOrEmpty(dok.ErrorCode))
        {
            return dok;
        }

        string filenev = dok.Ds.Tables[0].Rows[0]["FajlNev"].ToString().Trim();
        int utvonalHossza = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Length;
        string utvonal = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Substring(0, utvonalHossza - filenev.Length);
        string documentStoreType = dok.Ds.Tables[0].Rows[0]["External_Source"].ToString();
        string documentExternalInfo = dok.Ds.Tables[0].Rows[0]["External_Info"].ToString();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();
            Result doktarRet = ds.CheckOut(GetPart(documentExternalInfo, 0), GetPart(documentExternalInfo, 1), GetPart(documentExternalInfo, 2), filenev);

            if (!String.IsNullOrEmpty(doktarRet.ErrorCode))
            {
                return doktarRet;
            }
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {

        }


        KRT_Dokumentumok updateDok = (KRT_Dokumentumok)dok.Record;
        ExecParam execparamDok = execparam.Clone();
        execparamDok.Record_Id = "";

        updateDok.Updated.SetValueAll(false);
        updateDok.Base.Updated.SetValueAll(false);
        //updateDok.Base.Ver = dok.Ds.Tables[0].Rows[0]["Ver"].ToString();

        updateDok.CheckedOut = execparam.Felhasznalo_Id.ToString();
        updateDok.Updated.CheckedOut = true;
        updateDok.CheckedOutTime = DateTime.Now.ToString();
        updateDok.Updated.CheckedOutTime = true;

        Result retDokUpdate = dokumentumService.Update(execparamDok, updateDok);

        if (!String.IsNullOrEmpty(retDokUpdate.ErrorCode))
            return retDokUpdate;

        return dok;
    }

    /// <summary>
    /// MOSSban t�rolt �llom�ny, legutols� verzi�j�nak csekinel�se.
    /// MinorCheckin-t (Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn) csin�l.
    /// 
    /// KRT Dokumentum bejegyz�s keres�se.
    /// MOSS file chk out.
    /// 
    /// </summary>
    /// <param name="execparam">Execparam, record_id=eDocGuiddal megfejelve.</param>
    /// <returns>Result - eDoc guid vagy Error.</returns>
    /// 
    [WebMethod]
    public Result CheckIn(ExecParam execparam, String komment)
    {

        if (execparam.Felhasznalo_Id == null ||
            execparam.Record_Id == null)
        {
            // TODO: alap parameter hibak:
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
        Result dok = dokumentumService.Get(execparam);

        if (!String.IsNullOrEmpty(dok.ErrorCode))
            return dok;

        string filenev = dok.Ds.Tables[0].Rows[0]["FajlNev"].ToString().Trim();
        int utvonalHossza = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Length;
        string utvonal = dok.Ds.Tables[0].Rows[0]["External_Link"].ToString().Substring(0, utvonalHossza - filenev.Length);
        string documentStoreType = dok.Ds.Tables[0].Rows[0]["External_Source"].ToString();
        string documentExternalInfo = dok.Ds.Tables[0].Rows[0]["External_Info"].ToString();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();
            Result doktarRet = ds.CheckIn(GetPart(documentExternalInfo, 0), GetPart(documentExternalInfo, 1), GetPart(documentExternalInfo, 2), filenev, komment, "0"); //Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn);

            if (!String.IsNullOrEmpty(doktarRet.ErrorCode))
            {
                return doktarRet;
            }
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {

        }

        KRT_Dokumentumok updateDok = (KRT_Dokumentumok)dok.Record;

        updateDok.Updated.SetValueAll(false);
        updateDok.Base.Updated.SetValueAll(false);
        //updateDok.Base.Ver = dok.Ds.Tables[0].Rows[0]["Ver"].ToString();

        updateDok.CheckedOut = "";
        updateDok.Updated.CheckedOut = true;
        updateDok.CheckedOutTime = "";
        updateDok.Updated.CheckedOutTime = true;

        Result retDokUpdate = dokumentumService.Update(execparam, updateDok);

        if (!String.IsNullOrEmpty(retDokUpdate.ErrorCode))
            return retDokUpdate;

        return dok;
    }

    /// <summary>
    /// Kulso file torlese hiba eseten.
    /// </summary>
    /// <param name="documentStoreType"></param>
    /// <param name="documentSite"></param>
    /// <param name="documentStore"></param>
    /// <param name="documentFolder"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    /// 
    private Result DeleteExternalFile(string documentStoreType, String rootsite, String documentSite, String documentStore, String documentFolder, String filename)
    {
        Result ret = new Result();

        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        if (String.IsNullOrEmpty(documentSite) ||
            String.IsNullOrEmpty(documentStore) ||
            String.IsNullOrEmpty(documentFolder) ||
            String.IsNullOrEmpty(filename))
        {
            // TODO:
            ret.ErrorCode = "1000002";
            return ret;
        }

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            //Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();
            ListUtilService lisu = new Contentum.eDocument.SharePoint.ListUtilService();

            //if (!documentSite.EndsWith("/")) { documentSite += "/"; }
            //if (!documentStore.EndsWith("/")) { documentStore += "/"; }

            ret = lisu.Delete(rootsite, documentSite, documentStore, documentFolder, filename);
            if (!String.IsNullOrEmpty(ret.ErrorCode))
            {
                return ret;
            }
        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {

        }
        return ret;
    }

    /// ---------------------------------------------------------------------------------------------

    /// <summary>
    /// L�trehozza a t�rhelyen a megadott strukt�r�t.
    /// 
    /// V�gigmegy a (documentSite) site pathon, ellen�rzi a megl�t�ket �s l�trehozza, ha nincs.
    /// A library-t (documentStore) l�trehozza. Ez nem path, mivel nem lehet al�bont�s!
    /// A foldereket (documentFolder) ellen�rzi �s l�tre is hozza �ket.
    /// 
    /// TODO: Csak MOSSra van k�sz.
    /// 
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="documentStoreType">tipus</param>
    /// <param name="documentSite">site path</param>
    /// <param name="documentStore">doclib neve</param>
    /// <param name="documentFolder">folder path (lehet ures is)</param>
    /// <param name="cttId">Ez nem kotelezo. A tobbi de.</param>
    /// <returns>Result - ha ok, innrelevans mit ad vissza, a lenyeg, h ne legyen ErrorCode! (Mint az MS fele WebClient-es upload! :))</returns>
    /// 
    [WebMethod] //bernat.laszlo WATCH
    public Result TarhelyekEllenorzeseMappakTarhely(ExecParam ex
        , string documentStoreType
        , string documentSite
        , string documentStore
        , string documentFolder
        , string cttId)
    {
        Result ret = new Result();
        Logger.Info("TarhelyekEllenorzeseMappakTarhely start.");

        if (ex.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(documentSite) ||
            String.IsNullOrEmpty(documentStore)
            )
        {
            // TODO: alap parameter hibak:
            Logger.Error("TarhelyekEllenorzeseMappakTarhely parameter hianyzik hiba.");
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        Logger.Info(String.Format("Kapott cttid: {0}", cttId));

        /// tarolo tipusa szerint
        /// 

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            // site letrehozasa, ha nincs
            Logger.Info("Sharepoint create site.");

            AxisUploadToSps.Upload upld = this.GetAxisUploadToSps();
            //upld.Credentials = new System.Net.NetworkCredential("contentumspuser", "123456", "axis");

            // 
            documentSite = RemoveSlash(documentSite);
            char[] elvalaszto = { (char)'/' };
            string[] sites = documentSite.Split(elvalaszto);
            int u = 0;
            string tempSpath = String.Empty;

            Logger.Info("Sharepoint create site ciklus elott.");

            try
            {
                while (u < sites.Length)
                {
                    string createdUrl = upld.CreateSite(tempSpath, sites[u], sites[u], "STS#2");

                    tempSpath += (String.IsNullOrEmpty(tempSpath)) ? sites[u] : String.Format("/{0}", sites[u]);
                    Logger.Info(String.Format("{1} tempSpath: {0}, siteName: {2}", tempSpath, u, sites[u]));

                    if (String.IsNullOrEmpty(createdUrl))
                    {
                        Logger.Error("Sharepoint create site error!!!");
                        ret.ErrorCode = "SITE01";
                        ret.ErrorMessage = "Hiba a site l�trehoz�skor! N�zze meg a AxisFileUpload WS logj�t!";
                        return ret;
                    }
                    Logger.Info(String.Format("{1} createdUrl: {0}", createdUrl, u));

                    u++;
                }
            }
            catch (Exception exc1)
            {
                Logger.Error(String.Format("Site l�trehoz�s hiba a whileban!\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
                ret.ErrorCode = "SITE00";
                ret.ErrorMessage = String.Format("Site l�trehoz�s hiba!\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
                return ret;
            }

            /// doktar letrehozasa, ha nincs
            Logger.Info("Sharepoint create doclib.");
            FolderService folderservice = new Contentum.eDocument.SharePoint.FolderService();
            //folderservice.Credentials = new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials;
            ret = folderservice.CreateDocumentLibrary(RemoveSlash(documentSite), RemoveSlash(documentStore), ".");
            if (!String.IsNullOrEmpty(ret.ErrorCode))
            {
                Logger.Error("Sharepoint create doclib error: " + ret.ErrorCode + "; " + ret.ErrorMessage);
                return ret;
            }

            //  doktar ellenorzese, h a verziozas be van-e allaitva

            Logger.Info("-Verziozott-e a doclib?");

            ret = folderservice.IsVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore));

            bool verziozott = (bool)ret.Record;

            lock (_sync)
            {
                ret = folderservice.IsVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore));
                verziozott = (bool) ret.Record;

                Logger.Info("Verzi�z�s bekapcsol�sa.");
                Result retSetVer = folderservice.SetVersioning(RemoveSlash(documentSite), RemoveSlash(documentStore), true);
                if (!String.IsNullOrEmpty(retSetVer.ErrorCode))
                {
                    Logger.Error(String.Format("Verizozas bekapcsolasa sikertelen: site: {0} - store: {1}",
                        documentSite, documentStore));
                    Logger.Error(String.Format("Hiba oka: {0} - {1} ", retSetVer.ErrorCode, retSetVer.ErrorMessage));
                    return retSetVer;
                }
            }

            //  doktar ellenorzese, a content type engedelyezes be van-e allitva (ha kell)
            //  doktarhoz CTT kotese (ha kell)
            //  TODO: ellenorizni kell, h hozza van-e rendelve mar ahhoz a doclibhez!!!!

            if (!String.IsNullOrEmpty(cttId))
            {
                //  doktar ellenorzese, a content type engedelyezes be van-e allitva 

                Logger.Info("Content type-ok hasznalatanak engedelyezese...");

                using (AxisUploadToSps.Upload upld2 = GetAxisUploadToSps())
                {

                    try
                    {
                        Logger.Info("url osszeallitasa elott.");

                        string allowDocLibPath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + UI.GetAppSetting("SiteCollectionUrl") + documentSite;

                        Logger.Info("upld2.AllowContentTypeInDocLib elott.");

                        if (!upld2.AllowContentTypeInDocLib(allowDocLibPath, RemoveSlash(documentStore), true))
                        {
                            Logger.Error(String.Format("Content type hasznalatanak engedelyezese hiba: site: {0} - store: {1}", documentSite, documentStore));
                            Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                            ret = new Result();
                            ret.ErrorCode = "ALLOWCTT1";
                            ret.ErrorMessage = String.Format("Content type hasznalatanak engedelyezese hiba: site: {0} - store: {1}", documentSite, documentStore);
                            return ret;
                        }
                    }
                    catch (Exception exx)
                    {
                        Logger.Error(String.Format("Exception!!!!\nMessage: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace));
                        ret = new Result();
                        ret.ErrorCode = "ALLOWCTT2";
                        ret.ErrorMessage = String.Format("Hiba a docLib tartalomt�pus haszn�lat�nak enged�lyez�sekor!!!!\nMessage: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace);
                        return ret;
                    }
                } // using

                //  doktarhoz CTT kotese 

                Logger.Info("Content type doktarhoz kotese...");

                ContentTypeService cttService = new ContentTypeService();

                string docLibFullUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUrl");
                docLibFullUrl += (!UI.GetAppSetting("SiteCollectionUrl").EndsWith("/") && !String.IsNullOrEmpty(UI.GetAppSetting("SiteCollectionUrl"))) ? String.Format("{0}/", UI.GetAppSetting("SiteCollectionUrl")) : String.Format("{0}", UI.GetAppSetting("SiteCollectionUrl"));
                //docLibFullUrl += (!UI.GetAppSetting("DokumentumtarSite").EndsWith("/") && !String.IsNullOrEmpty(UI.GetAppSetting("DokumentumtarSite"))) ? String.Format("{0}/", UI.GetAppSetting("DokumentumtarSite")) : String.Format("{0}", UI.GetAppSetting("DokumentumtarSite"));
                docLibFullUrl += String.Format("{0}/", RemoveSlash(documentSite));
                //docLibFullUrl += String.Format("{0}/", RemoveSlash(documentStore));

                Logger.Info("Content type id doktaron vizsgalata elott.");

                if (cttService.IsExistsContentTypeOnDocLibById(docLibFullUrl, RemoveSlash(documentStore), cttId) == false)
                {
                    Logger.Info("lisu.AddContentType2DocumentLibrary hivasa elott.");

                    Result addCttResult = cttService.AddContentType2DocumentLibrary(docLibFullUrl, RemoveSlash(documentStore), cttId);

                    if (!String.IsNullOrEmpty(addCttResult.ErrorCode))
                    {
                        Logger.Error(String.Format("Content type hozzakotese doclibhez hiba: url: {0} cttid: {1}", docLibFullUrl, cttId));
                        return addCttResult;
                    }
                } //if IsExists...

                Logger.Info("BDC koto mezo letrehozasa...");

                try
                {
                    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                    {
                        string retBDC = upld3.AddBDCColumnToDocLib(docLibFullUrl, RemoveSlash(documentStore));

                        if (!retBDC.Equals("eDok"))
                        {
                            Logger.Error(String.Format("{2}\n site: {0} - store: {1}", documentSite, documentStore, retBDC));
                            Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                            ret = new Result();
                            ret.ErrorCode = "ADDBDC1";
                            ret.ErrorMessage = String.Format("{2}\n site: {0} - store: {1}", documentSite, documentStore, retBDC);
                            return ret;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("Hiba a BDC koto mezo letrehozasakor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                    ret = new Result();
                    ret.ErrorCode = "ADDBDC2";
                    ret.ErrorMessage = String.Format("Hiba a BDC koto mezo letrehozasakor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                    return ret;
                }
            }

            if (!String.IsNullOrEmpty(documentFolder))
            {
                string[] documentFolderParts = GetPathParts(documentFolder);

                /// folder letrehozasa, ha nincs
                /// 
                Logger.Info("Sharepoint create folder.");
                string folderNames = "";
                for (int i = 0; i < documentFolderParts.Length; i++)
                {
                    folderNames += (!String.IsNullOrEmpty(folderNames)) ? "/" + documentFolderParts[i].ToString() : documentFolderParts[i].ToString();

                    ret = folderservice.CreateFolder(RemoveSlash(documentSite), RemoveSlash(documentStore), RemoveSlash(folderNames));
                    if (!String.IsNullOrEmpty(ret.ErrorCode))
                    {
                        Logger.Error("Sharepoint create folder error: " + ret.ErrorCode + "; " + ret.ErrorMessage);
                        return ret;
                    }

                }
            }

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        {
            Logger.Info("Filesystem create path.");

            string path = RemoveSlash(documentSite) + "/" + RemoveSlash(documentSite) + "/" + RemoveSlash(documentFolder);
            path = path.Replace('/', '\\');

            FsWs fsWs = new FsWs();

            ret = fsWs.CreateFolder(path);
            if (!String.IsNullOrEmpty(ret.ErrorCode))
            {
                Logger.Error("Filesystem create path hiba: " + ret.ErrorMessage.ToString());
                return ret;
            }

        }
        else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        {
            /// itt nem kell csinalni semmit, mert mar elozoleg ok,
            /// legfeljebb visszaadhatjuk a cel mapp id-jet (mint kulso id :) ), ha majd kell
            Logger.Info("Database create dir.");
        }

        Logger.Info("TarhelyekEllenorzeseMappakTarhely vege.");
        return ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private AxisUploadToSps.Upload GetAxisUploadToSps()
    {
        return DocumentService.Utility.GetAxisUploadToSps();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ppath"></param>
    /// <returns></returns>
    private string[] GetPathParts(String ppath)
    {
        string p = ppath.Replace('\\', '/').Trim();
        p = (p.Substring(0, 1).Equals("/")) ? p.Substring(1) : p;
        p = (p.Substring(p.Length - 1).Equals("/")) ? p.Substring(0, p.Length - 1) : p;
        return p.Split('/');
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ppath"></param>
    /// <returns></returns>
    private string GetMappaRovidNev(String ppath)
    {
        string p = (ppath.Substring(ppath.Length - 1).Equals("/")) ? ppath.Substring(0, ppath.Length - 1) : ppath;
        string[] parts = p.Split('/');
        return parts[parts.Length - 1].Trim();
    }

    /// <summary>
    /// Az execparam Record_Id mezojebe kell tenni a krt_dokumentumok rekord id-jet.
    /// A Record mezobe jon a KRT_Dok objektum.
    /// </summary>
    /// <param name="execparam"></param>
    /// <returns></returns>
    private Result GetDoc(ExecParam execparam)
    {
        Logger.Info("GetDoc indul.");

        if (String.IsNullOrEmpty(execparam.Record_Id))
        {
            Logger.Error("Ures az execparam Record_Id mezoje!");
            Result reske = new Result();
            reske.ErrorCode = "PARAM0001";
            reske.ErrorMessage = "eDocumentWebService.DocumentService.GetDoc (private) method param�terez�si hiba! �res az execparam Record_Id mezoje!";
            return reske;
        }

        KRT_DokumentumokService dokService = new KRT_DokumentumokService();
        Result dokumentumResult = dokService.Get(execparam);

        if (!String.IsNullOrEmpty(dokumentumResult.ErrorCode))
            Logger.Error(String.Format("Hibaval tert vissza a dokService.Get: {0}\nErrMsg: {1}"
                , dokumentumResult.ErrorCode
                , dokumentumResult.ErrorMessage));

        //KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)dokumentumResult.Record;

        return dokumentumResult;
    }

    /// ---------------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <returns></returns>
    private XmlNode ConvertString2XmlNode(String xmlString)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xmlString);
        XmlNode root = xmlDoc.DocumentElement;
        return root;
    }

    /// ---------------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <returns></returns>
    private string GetPart(String sourceStr, int p, char elvalaszto)
    {
        if (String.IsNullOrEmpty(sourceStr)) return "";
        if (p < 0) return "";
        string[] parts = sourceStr.Split(elvalaszto);
        if (0 > p || parts.Length - 1 < p) return "";
        return parts[p];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <returns></returns>
    private string GetPart(String sourceStr, int p)
    {
        return GetPart(sourceStr, p, ';');
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <returns></returns>
    private Result GetKodTar(ExecParam ex1, string p_kodcsoport)
    {

        Logger.Info("GetKodTar indul");
        Result mresult = new Result();

        KRT_KodCsoportokService kcsServ = new KRT_KodCsoportokService();
        KRT_KodCsoportokSearch kcss = new KRT_KodCsoportokSearch();
        kcss.Kod.Value = p_kodcsoport; // "DOKUMENTUMTIPUS";
        kcss.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

        Result kodcsoportResult = kcsServ.GetAll(ex1, kcss);

        // akarmi hiba
        if (!String.IsNullOrEmpty(kodcsoportResult.ErrorCode))
        {
            return kodcsoportResult;
        }

        Logger.Info(String.Format("GetKodTar: kodtarcsoportok talalt rekordok szama: {0}", kodcsoportResult.Ds.Tables[0].Rows.Count));

        // nincs meg a keresett kodcsoport hiba
        if (kodcsoportResult.Ds.Tables[0].Rows.Count == 0)
        {
            mresult.ErrorCode = "ROWC1";
            mresult.ErrorMessage = "Nincs meg a keresett kodcsoport.";
            return mresult;
        }

        // tul sok kodcsoport vissza hiba
        if (kodcsoportResult.Ds.Tables[0].Rows.Count > 1)
        {
            mresult.ErrorCode = "ROWC2";
            mresult.ErrorMessage = "A kodcsoport kereses soran kapott sorok szama nem 1.";
            return mresult;
        }

        KRT_KodTarakService ktSerc = new KRT_KodTarakService();
        KRT_KodTarakSearch kts = new KRT_KodTarakSearch();

        kts.KodCsoport_Id.Value = kodcsoportResult.Ds.Tables[0].Rows[0]["Id"].ToString();
        kts.KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result kodtarResult = ktSerc.GetAll(ex1, kts);

        Logger.Info(String.Format("GetKodTar: kodtar ertekek talalt rekordok szama: {0}", kodtarResult.Ds.Tables[0].Rows.Count));

        return kodtarResult;
    }

    ///
    /// -------------------------------------------------------------------------------------------------------------------------------
    /// 

    ///
    /// <summary>
    /// Egy file feltoltese a megadott tipusu tarhelyre. A pontos tarolasi helyet ez a progi hatarozza meg.
    /// Share-en megosztott file feltoltesere is hasznalhato.
    /// 
    /// Ha az iktatokonyv ki van toltve, akkor iktatas, egyebkent a masik tarhely a cel.
    /// 
    /// Ha a kapottKrtDokObj GUID-ja ki van toltve VAGY a cel tarhelyen van mar olyan nevu file, akkor uj alverzio jon letre!
    /// Ha megvan adva az iktatokonyv, akkor foverzio.
    /// 
    /// Akkor csinal uj krt_dok bejegyzest, ha
    /// az ujirat parameter IGEN
    /// es a ujKrtDokBejegyzesKell is IGEN.
    /// 
    /// xmlStrParams struktura:
    /// <uploadparameterek>
    ///     <iktatokonyv>Iktatokonyv. Ha ez ki van toltve, akkor iktatasrol van szo.</iktatokonyv>
    ///     <sourceSharePath>tavoli megosztas cime, ahol a file van; nem kotelezo; szkennelesnel kell</sourceSharePath>
    ///     <foszam>Ugyirat foszama. Nem kotelezo.</foszam>
    ///     <megjegyzes>A keletkezo verziohoz tartozo megjegyzes.</megjegyzes>
    ///     <munkaanyag>IGEN/NEM</munkaanyag>
    ///     <irat_id>irat_id</irat_id>
    /// </uploadparameterek>
    /// 
    ///             string uploadXmlStrParams = String.Format("<uploadparameterek>" +
    ///                                                "<iktatokonyv></iktatokonyv>" +
    ///                                                "<sourceSharePath></sourceSharePath>" +
    ///                                                "<foszam></foszam>" +
    ///                                                "<irat_id></irat_id>" +
    ///                                                "<dokumentum_id></dokumentum_id>" +
    ///                                                "<megjegyzes></megjegyzes>" +
    ///                                                "<munkaanyag></munkaanyag>" +
    ///                                            "</uploadparameterek>");
    /// 
    /// </summary>
    /// <param name="execparam">Execparam. Kotelezo mindenkeppen!</param>
    /// <param name="documentStoreType">Tarolas tipusa: Contentum.eUtility.Constants.DocumentStoreType. Kotelezo mindenkeppen!</param>
    /// <param name="filename">A file neve kiterjesztessel. Kotelezo mindenkeppen!</param>
    /// <param name="cont">A file tartalma, byte tombkent. Lehet ures, de akkor kell lennie sourceSharePath-nak!</param>
    /// <param name="kapottKrtDokObj">Kapott KRT_Dokumentum objektum. Nem kotelezo.</param>
    /// <param name="xmlStrParams">Stringkent atadott xml struktura XmlDocument formaban, mely plussz parametereket tartalmaz. (Lasd leiras.)</param>
    /// <returns>Result - mely error vagy tartalmazza a beszurt KRT_Dokumentum id-jet</returns>
    /// 
    private Result _UploadFromeRecordWithCTT(
      ExecParam execparam
    , string documentStoreType
    , string filename
    , byte[] cont
    , KRT_Dokumentumok kapottKrtDokObj
    , XmlDocument xmlParams
    )
    {

        #region Doc

        /* **********************************************************************************
         * Lepesek, nagyvonalakban:
         * 
         * feltoltesi hely-utvonal meghatarozasa
	     *    ha iktat�s van, akkor iktatas ala, ha nem csatolas
         * adatbazisban es spsben letrehozas, ha nincs meg az mappa/utvonal
	     *     (ha nem uj irat az valasztottIratId alapjan vissza kell szedni a dokumentum_id-jet!)
         * csekout
	     *     ha mar van ott olyan nevu file SPSben
         * feltoltes
         * 	
         * regisztralas
	     *    ha �j irat
	     *     ha m�g nem volt csatolva hozz�
         * 
         * 
         * verziok lekerdezese
         * sps metaadatok beallitasa
         * chekin
	     *     irat munkaanyag ellenorzes
	     *    foverzio, ha iktatas/vagy iktatott anyag ujra csatolasa
	     *     alverzio, ha munakanyag/munkaanyag ujra csatolasa
         * egyeb adatok (external_link) modositasa a bejegyzeseben
	     *     ha van elozo verzio
         * mappatartalom regisztralasa
         * 
         *************************************************************************** */

        #endregion

        Logger.Info(String.Format("DocumentService._UploadFromeRecordWithCTT indul."));

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result _ret = new Result();

        #region A kiindulo, alap feltoltesi struktura meghatarozasa.

        Result resultTHStrukt = TarolasiHelyStrukturaNevEsTartalomAlapjan(execparam, documentStoreType, filename, cont, xmlParams.OuterXml.ToString());

        if (!String.IsNullOrEmpty(resultTHStrukt.ErrorCode))
        {
            return resultTHStrukt;
        }

        try
        {
            xmlParams.LoadXml((string)resultTHStrukt.Record);
        }
        catch (Exception exc1)
        {
            Logger.Info(String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
            _ret.ErrorCode = "XMLC0001";
            _ret.ErrorMessage = String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
            return _ret;
        }


        Utility.TarolasiHelyStruktura tarolasiHely = new Utility.TarolasiHelyStruktura();
        //  A site collection - a kiindul�si url.
        string rootSiteCollectionUrl = GetParam("rootSiteCollectionUrl", xmlParams);
        tarolasiHely.RootSiteCollectionUrl = rootSiteCollectionUrl;
        //  Ez a dokumentumt�r url neve �s az �v.
        string doktarSitePath = RemoveSlash(GetParam("doktarSitePath", xmlParams));
        tarolasiHely.DoktarSitePath = doktarSitePath;
        string doktarDocLibPath = RemoveSlash(GetParam("doktarDocLibPath", xmlParams));
        tarolasiHely.DoktarDocLibPath = doktarDocLibPath;
        string doktarFolderPath = GetParam("doktarFolderPath", xmlParams);
        tarolasiHely.DoktarFolderPath = doktarFolderPath;
        tarolasiHely.FileName = filename;

        //  Ezzel vizsgalhatjuk a kesobbiek folyaman, hogy tipusos dokumentumrol van-e szo. (Ha nem nyilvan empty :))
        string spsCttId = GetParam("spsCttId", xmlParams);

        //  Ez lesz az uj Krt_dok bejegyzes GUIDja.
        //  Vagy ha letezo, akkor ebbe allitjuk be az erteket.
        Guid ujKrtDokGuid = new Guid(GetParam("ujKrtDokGuid", xmlParams));
        bool letezoKrtDok = false;

        //  ha kaptunk dokumentum guid-ot, akkor az lesz az ujKrtDokGuid
        //  de ellenorizzuk, hogy letezik-e
        if (!String.IsNullOrEmpty(GetParam("docmetaDokumentumId", xmlParams)))
        {
            Logger.Info(String.Format("Van dokumentum_id az xml parameterek kozott. Ellenorzunk. ({0})", GetParam("docmetaDokumentumId", xmlParams)));

            ExecParam gede = new ExecParam();
            gede.Alkalmazas_Id = execparam.Alkalmazas_Id;
            gede.Felhasznalo_Id = execparam.Felhasznalo_Id;
            gede.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
            gede.Record_Id = GetParam("docmetaDokumentumId", xmlParams);

            Result resultIsDokExists = GetDoc(gede);

            if (!String.IsNullOrEmpty(resultIsDokExists.ErrorCode))
            {
                if (resultIsDokExists.ErrorCode.Equals("-2146232060") && resultIsDokExists.ErrorMessage.Equals("[50101]"))
                {
                    resultIsDokExists = new Result();
                    resultIsDokExists.ErrorCode = "NDIDA00001";
                    resultIsDokExists.ErrorMessage = "A dokumentum tartalmaz egy dokumentum id-t. Ez nem tal�lhat� a rendszerben!";
                }
                Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor!ErrCode: {0}\nErrMsg: {1}", resultIsDokExists.ErrorCode, resultIsDokExists.ErrorMessage));
                return resultIsDokExists;
            }

            if (resultIsDokExists.Record == null)
            {
                Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes."));
                _ret.ErrorCode = "NEX0001";
                _ret.ErrorMessage = "Upload: Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes.";
                return _ret;
            }

            ujKrtDokGuid = new Guid(GetParam("docmetaDokumentumId", xmlParams));
            letezoKrtDok = true;
        }

        //  2. Ha docx, akkor megnezzuk van-e tartalomtipusa
        string kiterjesztes = (filename.LastIndexOf('.') == -1) ? " " : filename.Trim().Substring(filename.LastIndexOf('.') + 1);
        string sablonAzonosito = GetParam("sablonAzonosito", xmlParams);

        Logger.Info(String.Format("filename: {0} - kiterjesztes: {1}", filename, kiterjesztes));
        Logger.Info(String.Format("Kapott cuccok: doktarSitePath: {0}\ndoktarDocLibPath: {1}\ndoktarFolderPath: {2}\nsablonAzonosito: {3}", doktarSitePath, doktarDocLibPath, doktarFolderPath, sablonAzonosito));

        #endregion

        #region ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet

        string kivalasztottIratIdAlapjanDokumentumId = String.Empty;
        string ujKrtDokBejegyzesKell = this.GetParam("ujKrtDokBejegyzesKell", xmlParams);
        String csatolmanyId = String.Empty;
        String csatolmanyVerzio = String.Empty;

        if (GetParam("ujirat", xmlParams).Equals("NEM"))
        {
            bool vanIlyenNevuNeki = true;
            Logger.Info(String.Format("ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet - indul. kivalasztottIratId: {0} - kivalasztottKuldemenyId: {1}", GetParam("kivalasztottIratId", xmlParams), GetParam("kivalasztottKuldemenyId", xmlParams)));

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
            Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

            if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
            {
                csatolmanyokSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                csatolmanyokSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                csatolmanyokSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            csatolmanyokSearch.Manual_Dokumentum_Nev.Value = filename;
            csatolmanyokSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratDokRes = csatolmanyokService.GetAllWithExtension(execparam, csatolmanyokSearch);

            if (!String.IsNullOrEmpty(iratDokRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az irat dokumentum kapcsol�t�bla lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", iratDokRes.ErrorCode, iratDokRes.ErrorMessage));
                return iratDokRes;
            }

            if (iratDokRes.Ds.Tables[0].Rows.Count == 0)
            {
                //Logger.Error(String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!"));
                //_ret = new Result();
                //_ret.ErrorCode = "IRATK00001";
                //_ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!");
                //return _ret;
                ujKrtDokBejegyzesKell = "IGEN";
                vanIlyenNevuNeki = false;
                Logger.Info(String.Format("Nincs ilyen nevu neki: {0}", filename));
            }

            if (iratDokRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATK00002";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!");
                return _ret;
            }

            Logger.Info(String.Format("iratDokRes.Ds.Tables[0].Rows.Count: {0}", iratDokRes.Ds.Tables[0].Rows.Count));
            if (iratDokRes.Ds.Tables[0].Rows.Count == 1)
            {
                csatolmanyId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Id"]);
                csatolmanyVerzio = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["erecCsat_Ver"]);
                Logger.Info(String.Format("kapott csatolmanyId: {0} - csatolmanyVerzio: {1}", csatolmanyId, csatolmanyVerzio));

                #region zarolas figyelese

                Logger.Info("Dokumentum zarolas figyelese.");

                if (!String.IsNullOrEmpty(Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"])))
                {
                    //  TODO: itt az lenne szep, h a zarolo id mellett a nevet is visszadnank!

                    Logger.Error(String.Format("A dokumentum zarolva van! Zarolo_Id: {0} ZarolasIdo: {1}", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]));

                    Result iratDokRes2 = new Result();
                    iratDokRes2.ErrorCode = "ZAROLT001";
                    iratDokRes2.ErrorMessage = String.Format("A felt�ltend� dokumentum z�rolva van (ki: {0}, mikor: {1})!", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                    iratDokRes2.Record = String.Format("<result><zarolo_id>{0}</zarolo_id><zarolasido>{1}</zarolasido></result>", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                    return iratDokRes2;
                }

                #endregion
            }

            if (vanIlyenNevuNeki)
            {
                Logger.Info(String.Format("Eredmeny ok"));

                kivalasztottIratIdAlapjanDokumentumId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Dokumentum_Id"]);

                Logger.Info(String.Format("Kapott dokumentum id: {0}", kivalasztottIratIdAlapjanDokumentumId));
            }

            #region dokumentum �thelyezes�se (ha m�r l�tezett de rossz helyen)

            if (vanIlyenNevuNeki)
            {
                string oldExternalInfo = iratDokRes.Ds.Tables[0].Rows[0]["External_Info"].ToString();
                string verzioJel = iratDokRes.Ds.Tables[0].Rows[0]["VerzioJel"].ToString();
                Utility.TarolasiHelyStruktura oldTarolasiHely = new Utility.TarolasiHelyStruktura(oldExternalInfo, filename);
                tarolasiHely.SpsMachineName = oldTarolasiHely.SpsMachineName;
                ExecParam xpmMoveDokument = execparam.Clone();
                Result resMoveDokument = this.MoveDokument(xpmMoveDokument, oldTarolasiHely, tarolasiHely, kivalasztottIratIdAlapjanDokumentumId, null, verzioJel);

                if (resMoveDokument.IsError)
                {
                    Logger.Error("MoveDokument hiba", xpmMoveDokument, resMoveDokument);
                    //r�gi t�rol�si hely be�ll�t�sa
                    doktarFolderPath = tarolasiHely.DoktarFolderPath;
                }
            }


            #endregion
        }

        #endregion

        #region A felepitett struktura ellenorzese es letrehozasa a DB-ben

        //Result eDocMappaGuid = null;
        Result storeMappaGuid = null;

        //if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        //{
        //    Logger.Info("SharePoint: tarhely felepitese indul.");

        //    eDocMappaGuid = TarhelyekEllenorzeseMappakDb(execparam, doktarSitePath, doktarDocLibPath, doktarFolderPath);

        //    if (!String.IsNullOrEmpty(eDocMappaGuid.ErrorCode))
        //    {
        //        return eDocMappaGuid;
        //    }
        //}
        //else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
        //{
        //    Logger.Info("!!! TODO !!! FileSystem: tarhely felepitese.");
        //}
        //else if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.Database)
        //{
        //    Logger.Info("!!! TODO !!! Database: tarhely felepitese.");
        //}

        #endregion

        #region A felepitett struktura ellenorzese es letrehozasa a TARHELY-en

        Logger.Info("SharePoint: tarhely felepitese indul.");

        storeMappaGuid = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, spsCttId);

        if (!String.IsNullOrEmpty(storeMappaGuid.ErrorCode))
        {
            return storeMappaGuid;
        }

        #endregion

        #region  Megnezzuk van-e ugyanolyan nevu file a cel helyen.

        bool vanUgyanolyanFileACelHelyen = false;
        Result resultKrtDokbolKivett = null;

        //if (!String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))
        //{

        Contentum.eDocument.SharePoint.ListUtilService ds = new Contentum.eDocument.SharePoint.ListUtilService();
        Result retSpsbenVaneIlyenFile = ds.GetListItemsByItemNameAppendWithPath(doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);

        if (!String.IsNullOrEmpty(retSpsbenVaneIlyenFile.ErrorCode))
        {
            Logger.Error(String.Format("Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
            return retSpsbenVaneIlyenFile;
        }

        XmlDocument xmlDoc = new XmlDocument();

        try
        {
            xmlDoc.LoadXml(Convert.ToString(retSpsbenVaneIlyenFile.Record));
        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
            Result retXmlError = new Result();
            retXmlError.ErrorCode = "XMLP001";
            retXmlError.ErrorMessage = String.Format("{0}\n {1}", exc.Message, exc.StackTrace);
            return retXmlError;
        }

        Logger.Info(String.Format("GetListItemsByItemName adott xml eredmeny: {0}", xmlDoc.OuterXml.ToString()));

        int dbTalalt = ds.GetItemCount(xmlDoc.FirstChild);

        Logger.Info(String.Format("spsben nev alapjan talalt db: {0}", dbTalalt));

        if (dbTalalt > 0)
        {
            vanUgyanolyanFileACelHelyen = true;
        }

        #region kommentbe teve 2008.05.10

        /* ************************************************************************
             * kommentbe teve 2008.05.10  

            //  ha nev alapjan talaltunk, akkor vissza kell szedni a db-bol az id-je miatt

            if (dbTalalt == 1)
            {
                string keresettExternalLink = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") 
                    + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                    + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath)
                    + ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath)
                    + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath) + filename;
                
                Logger.Info(String.Format("SPS-ben talalt file-t! Db-ben kereses indul... Keresett external_link: {0}", keresettExternalLink));

                ExecParam dokSearchExecparam = new ExecParam();
                dokSearchExecparam.Alkalmazas_Id = execparam.Alkalmazas_Id;
                dokSearchExecparam.Felhasznalo_Id = execparam.Felhasznalo_Id;
                dokSearchExecparam.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;

                KRT_DokumentumokService dokService = new KRT_DokumentumokService();
                KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();

                dokSearch.External_Link.Value = keresettExternalLink;
                dokSearch.External_Link.Operator = Contentum.eQuery.Query.Operators.equals;

                Result extLinkKeresettKrtDokResult = dokService.GetAll(dokSearchExecparam, dokSearch);

                if (!String.IsNullOrEmpty(extLinkKeresettKrtDokResult.ErrorCode))
                {
                    Logger.Error(String.Format("External link alapjan kereses hiba!\nErrCode: {0}\nErrMessage: {1}", extLinkKeresettKrtDokResult.ErrorCode, extLinkKeresettKrtDokResult.ErrorMessage));
                    return extLinkKeresettKrtDokResult;
                }

                if (extLinkKeresettKrtDokResult.Ds.Tables[0].Rows.Count == 0)
                {
                    Logger.Error("Nincs meg a keresett external_link!");
                    _ret = new Result();
                    _ret.ErrorCode = "ELSEARCH001";
                    _ret.ErrorMessage = "Nem tal�lhat� bejegyz�s a filer�l az external_link alapj�n!";
                    return _ret;
                }

                if (extLinkKeresettKrtDokResult.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error("T�l sok eredm�nysor a keresett external_link alapj�n (1)!");
                    _ret = new Result();
                    _ret.ErrorCode = "ELSEARCH0011";
                    _ret.ErrorMessage = "T�l sok eredm�nysor a keresett external_link alapj�n (1)!";
                    return _ret;
                }

                resultKrtDokbolKivett = extLinkKeresettKrtDokResult;
                vanUgyanolyanFileACelHelyen = true;
            }

             * *************************************************** */

        #endregion

        //} //if (!String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))

        #endregion

        #region Feltoltes elott: ha letezett es iktatas, akkor check-out kell

        string felotoltottItemSpsId = String.Empty;

        Result felotoltottFileRet = null;

        Contentum.eDocument.SharePoint.DocumentService spsDs = new Contentum.eDocument.SharePoint.DocumentService();

        //if (!String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)) && vanUgyanolyanFileACelHelyen)
        if (vanUgyanolyanFileACelHelyen)
        {
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
            {

                Logger.Info("Csekaut, ha letezik mar...");
                //ListUtilService lisu = new Contentum.eDocument.SharePoint.ListUtilService();
                //Result listResult = lisu.GetListItemIdByName(RemoveSlash(doktarSitePath), RemoveSlash(doktarDocLibPath), RemoveSlash(doktarFolderPath), filename);

                //if (String.IsNullOrEmpty(listResult.ErrorCode))
                //{
                //    Result csekoutEllResult = spsDs.CheckOut(RemoveSlash(doktarSitePath), RemoveSlash(doktarDocLibPath), RemoveSlash(doktarFolderPath), filename);
                //    if (!String.IsNullOrEmpty(csekoutEllResult.ErrorCode))
                //    {
                //        Logger.Error("Csekaut hiba: " + csekoutEllResult.ErrorCode + "; " + csekoutEllResult.ErrorMessage);
                //        return csekoutEllResult;
                //    }
                //}
                //else
                //{
                //    if (!listResult.ErrorCode.Equals("190000002"))
                //    {
                //        Logger.Error(String.Format("GetListItemIdByName hiba: {0}\n{1}", listResult.ErrorCode, listResult.ErrorMessage));
                //        return listResult;
                //    }
                //}

                try
                {
                    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                    {
                        string itemSitePath = tarolasiHely.GetItemSitePath();

                        String res = upld3.ItemCheckout(itemSitePath
                            , RemoveSlash(tarolasiHely.DoktarDocLibPath)
                            , doktarFolderPath
                            , filename);

                        XmlDocument resultDoc = new XmlDocument();
                        try
                        {
                            resultDoc.LoadXml(res);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(String.Format("Hiba az ItemCheckout altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                            _ret.ErrorCode = "ItemCheckout";
                            _ret.ErrorMessage = "Hiba az ItemCheckout f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                        }

                        if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                        {
                            _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                            _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                            Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                            return _ret;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("Hiba a file checkoutkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                    Result ret = new Result();
                    ret.ErrorCode = "ItemCheckout";
                    ret.ErrorMessage = String.Format("Hiba a file checkout-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                    return ret;
                }

            } // if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)

        } // if (String.IsNullOrEmpty(iktatokonyv) && vanUgyanolyanFileACelHelyen)

        #endregion

        #region Feltoltes: a file feltoltese

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {

            if (cont == null && !String.IsNullOrEmpty(GetParam("sourceSharePath", xmlParams)))
            {
                //  TODO: from share upload ide be kell tenni
                Logger.Error("!!!!TODO: IDE MEG BE KELL TENNI A SHARE-ES UPLOADOT!!!!!");
            }
            else
            {

                //  TODO:
                //NetworkCredential uploadCred = new NetworkCredential(GetSharePointUserName(), GetSharePointPassword(), GetSharePointUserDomain());
                //NetworkCredential uploadCred = new NetworkCredential(

                felotoltottFileRet = spsDs.Upload(RemoveSlash(tarolasiHely.DoktarSitePath)
                    , RemoveSlash(tarolasiHely.DoktarDocLibPath)
                    , RemoveSlash(doktarFolderPath)
                    , filename
                    , cont);

                if (!String.IsNullOrEmpty(felotoltottFileRet.ErrorCode))
                {
                    Logger.Error("Sharepoint-ba file feltoltese hiba: " + felotoltottFileRet.ErrorCode + "; " + felotoltottFileRet.ErrorMessage);
                    return felotoltottFileRet;
                }

                // feloltott item id
                felotoltottItemSpsId = felotoltottFileRet.Uid;
                Logger.Info(String.Format("A feltoltott item id: {0}", felotoltottItemSpsId));
            }
        }

        #endregion

        #region kell-e uj krtDok bejegyzes (vizsgalat, h nem uj vagy ujKrtDokBejegyzesKell nem letezik)

        if (GetParam("ujirat", xmlParams).Equals("NEM") && !this.IsParamExists("ujKrtDokBejegyzesKell", xmlParams) && !"NEM".Equals(ujKrtDokBejegyzesKell))
        {
            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();

            ExecParam execp2 = execparam.Clone();

            EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

            if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
            {
                csatSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                csatSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                csatSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            csatSearch.Manual_Dokumentum_Nev.Value = filename;
            csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result resultCsatt = csatolmanyokService.GetAllWithExtension(execp2, csatSearch);

            if (!String.IsNullOrEmpty(resultCsatt.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza a kiv�lasztott irat id csatolm�ny neve alapj�n lek�rdez�s! ErrCode: {0}\nErrM: {1}", resultCsatt.ErrorCode, resultCsatt.ErrorMessage));
                return resultCsatt;
            }

            if (resultCsatt.Ds.Tables[0].Rows.Count == 0)
            {
                ujKrtDokBejegyzesKell = "IGEN";
                Logger.Info(String.Format("Nulla a tal�lt csatolm�nyok sz�ma a megadott irat idhez ({0}), ez�rt lesz insert.", GetParam("kivalasztottIratId", xmlParams)));
            }

        }

        #endregion

        #region Krt_dokumentum bejegyzes megejtese.

        string externalLink = tarolasiHely.ToExternalLink();

        String tortentUjKrtDokBejegyzes = String.Empty;

        if (GetParam("ujirat", xmlParams).Equals("IGEN") || ujKrtDokBejegyzesKell.Equals("IGEN"))
        {

            if (kapottKrtDokObj == null)
            {
                kapottKrtDokObj = new KRT_Dokumentumok();
                kapottKrtDokObj.Updated.SetValueAll(false);
                kapottKrtDokObj.Base.Updated.SetValueAll(false);
            }

            kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
            kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

            kapottKrtDokObj.FajlNev = filename.Trim();
            kapottKrtDokObj.Updated.FajlNev = true;

            kapottKrtDokObj.Meret = cont.Length.ToString();
            kapottKrtDokObj.Updated.Meret = true;

            kapottKrtDokObj.Allapot = "1";
            kapottKrtDokObj.Updated.Allapot = true;

            //
            //  CR#2191 innen

            /*
             
            if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
            {
                kapottKrtDokObj.Tipus = "Egy�b";
                kapottKrtDokObj.Updated.Tipus = true;
            }

            //(filename.LastIndexOf('.') == -1) ? "" : filename.Trim().Substring(filename.LastIndexOf('.'));
            if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
            {
                kapottKrtDokObj.Formatum = GetFormatum(execparam, filename); // (fileTipus == null) ? "Egy�b" : fileTipus;
                kapottKrtDokObj.Updated.Formatum = true;
            }
             
            */

            if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
            {
                kapottKrtDokObj.Tipus = GetFormatum(execparam, filename);
                kapottKrtDokObj.Updated.Tipus = true;
            }

            if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
            {
                kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
                kapottKrtDokObj.Updated.Formatum = true;
            }

            //
            //  CR#2191 idaig

            /// -------

            kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
            kapottKrtDokObj.Updated.Alkalmazas_Id = true;

            kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
            kapottKrtDokObj.Updated.Dokumentum_Id = true;

            //kapottKrtDokObj.VerzioJel = aktVersion;
            //kapottKrtDokObj.Updated.VerzioJel = true;

            //kapottKrtDokObj.External_Id = felotoltottFileSpsId;
            //kapottKrtDokObj.Updated.External_Id = true;

            kapottKrtDokObj.External_Link = externalLink;
            kapottKrtDokObj.Updated.External_Link = true;

            // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
            // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
            kapottKrtDokObj.External_Info = tarolasiHely.ToExternalInfo();
            kapottKrtDokObj.Updated.External_Info = true;

            kapottKrtDokObj.External_Source = documentStoreType;
            kapottKrtDokObj.Updated.External_Source = true;

            //kapottKrtDokObj.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
            kapottKrtDokObj.Leiras = originalFileName;
            kapottKrtDokObj.Updated.Leiras = true;

            kapottKrtDokObj.TartalomHash = "<null>";
            kapottKrtDokObj.Updated.TartalomHash = true;

            kapottKrtDokObj.KivonatHash = "<null>";
            kapottKrtDokObj.Updated.KivonatHash = true;

            if (!String.IsNullOrEmpty(sablonAzonosito))
            {
                kapottKrtDokObj.SablonAzonosito = sablonAzonosito;
                kapottKrtDokObj.Updated.SablonAzonosito = true;
            }

            kapottKrtDokObj.BarCode = GetParam("vonalkod", xmlParams);
            kapottKrtDokObj.Updated.BarCode = true;

            // a fajl feltoltesekor az alairas felulvizsgalat datumot
            // automatikusan generaljuk a feltoltessel egyidejuleg
            // de csak ha van ElektronikusAlairas informacionk
            if (!String.IsNullOrEmpty(kapottKrtDokObj.ElektronikusAlairas)
                && kapottKrtDokObj.ElektronikusAlairas != "<null>")
            {
                kapottKrtDokObj.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                kapottKrtDokObj.Updated.AlairasFelulvizsgalat = true;
            }

            Logger.Info("File regisztralasa eDocba.");
            KRT_DokumentumokService dokumentumService2 = new KRT_DokumentumokService();
            _ret = dokumentumService2.Insert(execparam, kapottKrtDokObj);

            if (!String.IsNullOrEmpty(_ret.ErrorCode))
            {
                Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);
                return _ret;
            }

            tortentUjKrtDokBejegyzes = _ret.Uid;
        }
        else
        {
            //_ret.Uid = GetParam("docmetaDokumentumId", xmlParams);
            _ret.Uid = kivalasztottIratIdAlapjanDokumentumId;
        }

        // BUG_6073
        //_ret.Record = externalLink;

        var context = System.Web.HttpContext.Current;
        string root = context.Request.Url.OriginalString;
        root = root.Remove(root.LastIndexOf("/"));

        // a filename (b�rmilyen docx kiterjeszt�s� lehet) kell a sharepointnak, k�l�nben a tulajdons�gok n�lk�l adja vissza a filet :(
        // string url = root + "/GetDocumentContent.aspx/x.docx?id=" + _ret.Uid; 
        string url = root + "/GetDocumentContent.aspx/"+ filename + "?id=" + _ret.Uid;
        _ret.Record = url;
        #endregion

        #region A rekord mezobe visszaadando, a kovetkezo lepeshez hasznalatos parameterek

        string uploadXmlStrParams_alairasResz = "";
        string alairaskell = GetParam("alairaskell", xmlParams);
        if (alairaskell == "IGEN")
        {
            uploadXmlStrParams_alairasResz = String.Format("<alairaskell>{0}</alairaskell>"
                                                         + "<alairasSzabalyId>{1}</alairasSzabalyId>"
                                                         + "<alairasSzint>{2}</alairasSzint>"
                                                         + "<easzKod>{3}</easzKod>"
                                                         + "<tanusitvanyId>{4}</tanusitvanyId>"
                                                         + "<kivarasiIdo>{5}</kivarasiIdo>"
                                                         + "<alairasMod>{6}</alairasMod>"
                                                         + "<alairasTulajdonos>{7}</alairasTulajdonos>"
                                                         , GetParam("alairaskell", xmlParams)
                                                         , GetParam("alairasSzabalyId", xmlParams)
                                                         , GetParam("alairasSzint", xmlParams)
                                                         , GetParam("easzKod", xmlParams)
                                                         , GetParam("tanusitvanyId", xmlParams)
                                                         , GetParam("kivarasiIdo", xmlParams)
                                                         , GetParam("alairasMod", xmlParams)
                                                         , GetParam("alairasTulajdonos", xmlParams));
        }

        string visszaadandoXmlString = String.Format("<uploadparameterek>" +
            "<documentStoreType>{0}</documentStoreType>" +
            "<rootSiteCollectionUrl>{1}</rootSiteCollectionUrl>" +
            "<doktarSitePath>{2}</doktarSitePath>" +
            "<doktarDocLibPath>{3}</doktarDocLibPath>" +
            "<doktarFolderPath>{4}</doktarFolderPath>" +
            "<dokumentum_id>{5}</dokumentum_id>" +
            "<spsCttId>{6}</spsCttId>" +
            "<eDocMappaGuid>{7}</eDocMappaGuid>" +
            "<filename>{8}</filename>" +
            "<iktatokonyv>{9}</iktatokonyv>" +
            "<sourceSharePath>{10}</sourceSharePath>" +
            "<foszam>{11}</foszam>" +
            "<iktatoszam>{12}</iktatoszam>" +
            "<ujKrtDokGuid>{13}</ujKrtDokGuid>" +
            "<megjegyzes>{14}</megjegyzes>" +
            "<munkaanyag>{15}</munkaanyag>" +
            "<docmetaDokumentumId>{16}</docmetaDokumentumId>" +
            "<dokumentumFullUrl>{17}</dokumentumFullUrl>" +
            "<ujirat>{18}</ujirat>" +
            "<cttNeve>{19}</cttNeve>" +
            "<docmetaIratId>{20}</docmetaIratId>" +
            "<kivalasztottIratId>{21}</kivalasztottIratId>" +
            "<ujKrtDokBejegyzesKell>{22}</ujKrtDokBejegyzesKell>" +
            "<tartalomSHA1>{23}</tartalomSHA1>" +
            "<iratMetaDefId>{24}</iratMetaDefId>" +
            "<iratMetaDefFoDokumentum>{25}</iratMetaDefFoDokumentum>" +
            "<sablonAzonosito>{26}</sablonAzonosito>" +
            uploadXmlStrParams_alairasResz +
            "<kivalasztottKuldemenyId>{27}</kivalasztottKuldemenyId>" +
            "<tortentUjKrtDokBejegyzes>{28}</tortentUjKrtDokBejegyzes>" +
            "<csatolmanyId>{29}</csatolmanyId>" +
            "<csatolmanyVerzio>{30}</csatolmanyVerzio>" +
            "<originalFileName>{31}</originalFileName>" +
            "</uploadparameterek>"
            , documentStoreType
            , tarolasiHely.RootSiteCollectionUrl
            , tarolasiHely.DoktarSitePath
            , tarolasiHely.DoktarDocLibPath
            , doktarFolderPath
            , _ret.Uid
            , spsCttId
            , String.Empty         //eDocMappaGuid.Uid.ToString()
            , Contentum.eUtility.XmlFunction.ConvertToCData(filename)
            , GetParam("iktatokonyv", xmlParams)
            , GetParam("sourceSharePath", xmlParams)
            , GetParam("foszam", xmlParams)
            , GetParam("iktatoszam", xmlParams)
            , GetParam("docmetaDokumentumId", xmlParams)
            , GetParam("megjegyzes", xmlParams)
            , GetParam("munkaanyag", xmlParams)
            , _ret.Uid.ToString()
            , _ret.Record.ToString()
            , GetParam("ujirat", xmlParams)
            , GetParam("cttNeve", xmlParams)
            , GetParam("docmetaIratId", xmlParams)
            , GetParam("kivalasztottIratId", xmlParams)
            , ujKrtDokBejegyzesKell
            , GetParam("tartalomSHA1", xmlParams)
            , GetParam("iratMetaDefId", xmlParams)
            , GetParam("iratMetaDefFoDokumentum", xmlParams)
            , GetParam("sablonAzonosito", xmlParams)
            , GetParam("kivalasztottKuldemenyId", xmlParams)
            , tortentUjKrtDokBejegyzes
            , csatolmanyId
            , csatolmanyVerzio
            , Contentum.eUtility.XmlFunction.ConvertToCData(originalFileName)
            );

        Logger.Info(String.Format("Tovabbadott xml paramterek: {0}", visszaadandoXmlString));

        _ret.Record = visszaadandoXmlString;

        #endregion

        Logger.Info("DocumentService._UploadFromeRecordWithCTT vege.");

        return _ret;
    } // _UploadFromeRecordWithCTT

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="xmlParams"></param>
    /// <returns></returns>
    private Result _CheckInUploadedFileWithCTT(ExecParam execparam
        , XmlDocument xmlParams, byte[] cont)
    {

        Logger.Info(String.Format("_CheckInUploadedFileWithCTT indul."));

        string filename = GetParam("filename", xmlParams);

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result _ret = new Result();

        #region Xmlbol parameterek valtozokba "konvertalasa"

        Logger.Info(String.Format("Kapott XML parameter: {0}", xmlParams.OuterXml.ToString()));

        String documentStoreType = GetParam("documentStoreType", xmlParams);

        Utility.TarolasiHelyStruktura tarolasiHely = new Utility.TarolasiHelyStruktura();

        tarolasiHely.RootSiteCollectionUrl = GetParam("rootSiteCollectionUrl", xmlParams);
        tarolasiHely.DoktarSitePath = GetParam("doktarSitePath", xmlParams);
        tarolasiHely.DoktarDocLibPath = GetParam("doktarDocLibPath", xmlParams);
        tarolasiHely.DoktarFolderPath = GetParam("doktarFolderPath", xmlParams);
        tarolasiHely.FileName = filename;
        String ujKrtDokBejegyzesKell = GetParam("ujKrtDokBejegyzesKell", xmlParams);

        String eDocMappaGuid = GetParam("eDocMappaGuid", xmlParams);
        String dokumentumId = GetParam("dokumentum_id", xmlParams);  // ez a dok id, amit beszurtunk vagy updateteltunk

        string munkaanyag = GetParam("munkaanyag", xmlParams);


        #endregion

        #region munkaanyag ellenorzes: ha van kivalsztott iratId, akkor ellenorizzuk, hogy munkaanyag-e, mert ha igen, akkor munkaanyag csekin kell

        Logger.Info("--- munkaanyag ellenorzes: ha van kivalsztott iratId, akkor ellenorizzuk, hogy munkaanyag-e, mert ha igen, akkor munkaanyag csekin kell");

        if (munkaanyag.Equals("NEM") && !String.IsNullOrEmpty(GetParam("kivalasztottIratId", xmlParams)))
        {
            Logger.Info("munkaanyag ellenorzes: ha van kivalsztott iratId, akkor ellenorizzuk, hogy munkaanyag-e, mert ha igen, akkor munkaanyag csekin kell - indul.");

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_IraIratokService iratService = sf.GetEREC_IraIratokService();
            Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

            iratSearch.Id.Value = GetParam("kivalasztottIratId", xmlParams);
            iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratRes = iratService.GetAllWithExtension(execparam, iratSearch);

            if (!String.IsNullOrEmpty(iratRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az irat (munkaanyagellenorzes) lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", iratRes.ErrorCode, iratRes.ErrorMessage));
                return iratRes;
            }

            if (iratRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Nem adott vissza eredmenyt az irat select (munkaanyagellenorzes)!"));
                _ret = new Result();
                _ret.ErrorCode = "MNKE00001";
                _ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat select (munkaanyagellenorzes)!");
                return _ret;
            }

            if (iratRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat select (munkaanyagellenorzes)!"));
                _ret = new Result();
                _ret.ErrorCode = "MNKE00002";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat select (munkaanyagellenorzes)!");
                return _ret;
            }

            if (Convert.ToString(iratRes.Ds.Tables[0].Rows[0]["allapot"]).Equals("0"))
                munkaanyag = "IGEN";

        }

        #endregion

        #region Feltoltes: verzio lekerdezese. - Krt_Dok bejegyzes update

        Logger.Info("--- Feltoltes: verzio lekerdezese. - Krt_Dok bejegyzes update");

        string aktVersion = String.Empty;
        string elozoVerzio = String.Empty;
        string elozoVerzioUrl = String.Empty;
        string aktualisKrtDokGuid = String.Empty;  //elozo updatehez hasznaljuk
        byte[] feltoltottFileByteArray = null;
        bool getContent = true;

        //0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn
        string checkInMode = Contentum.eUtility.Constants.SharePointCheckIn.MajorCheckIn;

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            string itemSitePath = tarolasiHely.GetItemSitePath();
            string itemFullPath = ((!tarolasiHely.DoktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarDocLibPath)) ? tarolasiHely.DoktarDocLibPath + "/" : tarolasiHely.DoktarDocLibPath) + ((!tarolasiHely.DoktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarFolderPath)) ? tarolasiHely.DoktarFolderPath + "/" : tarolasiHely.DoktarFolderPath) + filename;

            Logger.Info(String.Format("Csekkolt verzio lekerdezese ({0}).", itemFullPath));

            Contentum.eDocument.SharePoint.VersionService vser = new VersionService();
            //Result versionRet = vser.GetCurrentVersion(itemSitePath, itemFullPath);
            Result versionRet = vser.GetCurrentVersion(itemSitePath
                , RemoveSlash(tarolasiHely.DoktarDocLibPath)
                , tarolasiHely.DoktarFolderPath
                , filename);

            if (!String.IsNullOrEmpty(versionRet.ErrorCode))
            {
                Logger.Error("Elozo verzio lekerdezes hiba!");
                return versionRet;
            }

            aktVersion = Convert.ToString(versionRet.Uid);
            if (vser.LatestGetCVResultRowCount != 1)
            {
                elozoVerzio = vser.LatestGetCVResultPrevVersion;
                elozoVerzioUrl = vser.LatestGetCVResultPrevVersionUrl;
            }
            Logger.Info(String.Format("aktVersion: {2} - elozoVerzio: {0} - elozoVerzioUrl: {1}", elozoVerzio, elozoVerzioUrl, aktVersion));
            Logger.Info(String.Format("A verziojel joslas. ujKrtDokBejegyzesKell: {0} - iktatokonyv: {1} - munkaanyag: {2} - ujirat: {3}", ujKrtDokBejegyzesKell, (String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)) ? "ures" : GetParam("iktatokonyv", xmlParams)), munkaanyag, GetParam("ujirat", xmlParams)));

            try
            {
                NumberFormatInfo numInfo = System.Globalization.NumberFormatInfo.CurrentInfo;

                try
                {
                    decimal tmpD = 0;
                    if (!Decimal.TryParse(aktVersion, out tmpD))
                    {
                        Logger.Info(String.Format("Kiserlet 1: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                        aktVersion = aktVersion.Replace(numInfo.CurrencyGroupSeparator, numInfo.CurrencyDecimalSeparator);
                        Logger.Info(String.Format("Kiserlet 2 elott: Atalakitottuk a verzioszamot ilyenne: {0}. Parse kiserlet jon. CurrencyGroupSeparator: {1} - CurrencyDecimalSeparator: {2}", aktVersion, numInfo.CurrencyGroupSeparator, numInfo.CurrencyDecimalSeparator));
                        if (!Decimal.TryParse(aktVersion, out tmpD))
                        {
                            aktVersion = aktVersion.Replace(numInfo.CurrencyDecimalSeparator, numInfo.CurrencyGroupSeparator);
                            Logger.Info(String.Format("Kiserlet 3 elott: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                            if (!Decimal.TryParse(aktVersion, out tmpD))
                            {
                                aktVersion = aktVersion.Replace(".", numInfo.CurrencyDecimalSeparator); //  ez ha SPS angol - edok webserver magyar
                                Logger.Info(String.Format("Kiserlet 4 elott: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                                if (!Decimal.TryParse(aktVersion, out tmpD))
                                {
                                    Logger.Error(String.Format("Aktualis verzioszam decimal parszolas kiserletei sikertelenek!"));
                                    _ret = new Result();
                                    _ret.ErrorCode = "KONVERT0011";
                                    _ret.ErrorMessage = String.Format("Aktu�lis verzi�sz�m decimal konvert�l�s k�s�rletei sikertelenek!");
                                    return _ret;
                                }
                            }
                        }
                        else
                        {
                            Logger.Info("Parszolas sikeres.");
                        }
                    }
                }
                catch (Exception exBelso1)
                {
                    Logger.Error(String.Format("Aktualis verzioszam decimal konvertalas hiba! Message: {0}\nStackTrace: {1}", exBelso1.Message, exBelso1.StackTrace));
                    _ret = new Result();
                    _ret.ErrorCode = "KONVERT0001";
                    _ret.ErrorMessage = String.Format("Aktu�lis verzi�sz�m decimal konvert�l�s hiba! Message: {0}\nStackTrace: {1}", exBelso1.Message, exBelso1.StackTrace);
                    return _ret;
                }

                // "megjosuljuk" mi lesz a kovetkezo verzio, mert be kell jegyeznunk a krt_dokumentumokba
                //if ((String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)) || munkaanyag.Equals("IGEN")) && (!String.IsNullOrEmpty(elozoVerzio)))
                if (((String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)) || munkaanyag.Equals("IGEN")) || ujKrtDokBejegyzesKell.Equals("NEM")) && ("NEM".Equals(GetParam("ujirat", xmlParams))))
                {
                    Logger.Info(String.Format("Minor verzio joslat."));
                    //decimal d = Convert.ToDecimal(aktVersion.Replace(".", ","));
                    decimal d = Convert.ToDecimal(aktVersion);
                    //string nextMinorVersion = Convert.ToString(d + 0.1m).Replace(",", ".");
                    string nextMinorVersion = Convert.ToString(d).Replace(",", ".");
                    Logger.Info(String.Format("aktversion: {0} -> ez lesz: {1}", aktVersion, nextMinorVersion));
                    aktVersion = nextMinorVersion;
                    checkInMode = Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn;
                }
                else
                {
                    //checkInMode = Contentum.eUtility.Constants.SharePointCheckIn.MajorCheckIn;
                    Logger.Info(String.Format("Major verzio joslat."));
                    //decimal d = Convert.ToDecimal(aktVersion.Replace(".", ","));
                    decimal d = Convert.ToDecimal(aktVersion);
                    string nextMajorVersion = String.Format("{0}.0", System.Math.Floor(d + 1.0m));
                    Logger.Info(String.Format("aktversion: {0} -> ez lesz: {1}", aktVersion, nextMajorVersion));
                    aktVersion = nextMajorVersion;
                }
            }
            catch (Exception excc)
            {
                Logger.Error(String.Format("HIBA a verizo konvertalasa sor�n!\nMessage: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace));
                _ret = new Result();
                _ret.ErrorCode = "VERPARS0001";
                _ret.ErrorMessage = String.Format("HIBA a verizo konvertalasa sor�n!\nMessage: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace);
                return _ret;
            }

            Logger.Info("A verziojel bejegyzese.");

            ExecParam execp = new ExecParam();
            execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
            execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
            execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
            execp.Record_Id = dokumentumId;

            Result resultDokSel = GetDoc(execp);

            if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor.\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                return resultDokSel;
            }

            if (resultDokSel.Record == null)
            {
                Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor. Null a result record mezoje!"));
                resultDokSel = new Result();
                resultDokSel.ErrorCode = "VESD00001";
                resultDokSel.ErrorMessage = "Hiba a krt_dokumentumok bejegyz�s lek�rdez�sekor! Null a visszaadott objektum.";
                return resultDokSel;
            }

            Logger.Info("Lekerdezes ok. Update elokeszitese.");

            KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

            aktualisKrtDokGuid = updateDok.Id;

            updateDok.Updated.SetValueAll(false);
            updateDok.Base.Updated.SetValueAll(false);

            updateDok.VerzioJel = aktVersion;
            updateDok.Updated.VerzioJel = true;

            //
            // az url-t is update-elj�k a legfrisebb verziora
            updateDok.External_Link = tarolasiHely.ToExternalLink();
            updateDok.Updated.External_Link = true;

            #region sps id bejegyzese, ha nincs meg

            if (String.IsNullOrEmpty(updateDok.External_Id))
            {
                ListUtilService lisu = new ListUtilService();

                Logger.Info("Ures a rekord external_id mezoje. Lekerdezzuk az spsbol, h be tudjuk allitani.");

                //Result resultSpsId = lisu.GetListItemIdByName(doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);
                //if (!String.IsNullOrEmpty(resultSpsId.ErrorCode))
                //{
                //    return resultSpsId;
                //}
                //updateDok.External_Id = resultSpsId.Uid.ToString();
                //updateDok.Updated.External_Id = true;

                try
                {
                    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                    {
                        String res = upld3.GetItemGuid(itemSitePath
                            , RemoveSlash(tarolasiHely.DoktarDocLibPath)
                            , tarolasiHely.DoktarFolderPath
                            , filename);

                        XmlDocument resultDoc = new XmlDocument();
                        try
                        {
                            resultDoc.LoadXml(res);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(String.Format("Hiba az GetItemGuid altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                            _ret.ErrorCode = "GetItemGuid";
                            _ret.ErrorMessage = "Hiba az GetItemGuid f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                        }

                        if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                        {
                            _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                            _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                            Logger.Error(String.Format("Hiba az spsen levo UploadFile.GetItemGuid fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                            return _ret;
                        }

                        updateDok.External_Id = resultDoc.SelectSingleNode("//uniqueid").InnerText;
                        updateDok.Updated.External_Id = true;
                        Logger.Info(String.Format("Kapot ABRAKADABRA guid: {0}", updateDok.External_Id));
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("Hiba az item Guidjanak megallpitasakor, a checkinUploadban! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                    Result ret = new Result();
                    ret.ErrorCode = "GetItemGuid";
                    ret.ErrorMessage = String.Format("Hiba az item Guid p�tl�sakor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                    return ret;
                }

            }  //if String.IsNullOrEmpty(updateDok.External_Id

            #endregion

            #region SHA1 checksumma update a filera (jelenleg, ha SPS van csak akkor)

            Logger.Info("--- SHA1 checksumma update a filera (jelenleg, ha SPS van csak akkor)");

            Logger.Info("Lekerdezes ok. (SHA1 chk)");

            string kitejresztes = Path.GetExtension(filename);
            bool isDocx = ".docx".Equals(kitejresztes, StringComparison.InvariantCultureIgnoreCase);
            bool isContent = (cont != null);
            getContent = !isContent || isDocx;

            if (!getContent)
            {
                Logger.Info("SHA1 checksumma update a filera resz: tartalom parameterbol");

                feltoltottFileByteArray = cont;

                string sha1chk = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(feltoltottFileByteArray);

                Logger.Info(String.Format("Szamolt SHA1: {0}", sha1chk));

                Logger.Info(String.Format("_CheckInUploadedFileWithCTT kapott tartalomSHA1:", GetParam("tartalomSHA1", xmlParams)));

                //  tartalom: a belso tartalomra hash (word addin szamolja es adja at parameterkent)
                updateDok.TartalomHash = GetParam("tartalomSHA1", xmlParams);
                updateDok.Updated.TartalomHash = true;

                //  kivonat: a sps-be feltoltott allapot, teljes file hash-e.
                updateDok.KivonatHash = sha1chk;
                updateDok.Updated.KivonatHash = true;
            }

            #endregion

            Logger.Info(String.Format("VerzioJel: {0}, ver: {1}", aktVersion, updateDok.Base.Ver));

            updateDok.Base.Updated.Ver = true;

            Logger.Info(String.Format("VerzioJel: {0}, ver: {1}", aktVersion, updateDok.Base.Ver));

            //execp = new ExecParam();
            //execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
            //execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
            //execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
            //execp.Record_Id = dokumentumId;

            Logger.Info("Update elott.");

            KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
            resultDokSel = dokumentumService.Update(execp, updateDok);

            if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
            {
                Logger.Error(String.Format("Aktualis verzio update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                return resultDokSel;
            }
        }

        #endregion

        #region Feltoltes: a BDC kotes. Plussz egyeb mezok beallitasa.

        Logger.Info("--- Feltoltes: a BDC kotes. Plussz egyeb mezok beallitasa.");

        if (documentStoreType.Equals(Contentum.eUtility.Constants.DocumentStoreType.SharePoint))
        {
            try
            {

                Logger.Info(String.Format("BDC kotes elott (upld.SetItemBDCConnection)."));

                using (AxisUploadToSps.Upload upld = GetAxisUploadToSps())
                {
                    String baseFullUrl = tarolasiHely.GetItemSitePath();
                    String docLibName = (!tarolasiHely.DoktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarDocLibPath)) ? tarolasiHely.DoktarDocLibPath + "/" : tarolasiHely.DoktarDocLibPath;
                    String folderPath = (!tarolasiHely.DoktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarFolderPath)) ? tarolasiHely.DoktarFolderPath + "/" : tarolasiHely.DoktarFolderPath;


                    Logger.Info(String.Format("BDC kotes. krt_dok id: {0}", dokumentumId));

                    try
                    {

                        string bdcSetResultStr = upld.SetItemBDCConnection(baseFullUrl, docLibName, folderPath, filename, dokumentumId);

                        Logger.Info(String.Format("upld.SetItemBDCConnection visszaadott cucc: {0}", bdcSetResultStr));

                        //if (String.IsNullOrEmpty(bdcSetResultStr))
                        //{
                        //    Logger.Error("Ures eredmennyel tert vissza a BDC mezo beallitasa eljaras. A hiba leirasa az AxisFileUpload logjaban van!");
                        //    _ret = new Result();
                        //    _ret.ErrorCode = "SETBDC0001";
                        //    _ret.ErrorMessage = "A BDC mez� be�ll�t�sa nem t�rt�nt meg. A hiba r�szletes le�r�sa az sps kiszolg�l�n (AxisFileUpload) l�v� logban van.";
                        //    return _ret;
                        //}

                    }
                    catch (Exception exc1)
                    {
                        Logger.Error(String.Format("BDC mezo kotes beallitasa hiba!"));
                        Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
                        _ret = new Result();
                        _ret.ErrorCode = "BDCKBE0001";
                        _ret.ErrorMessage = String.Format("BDC kotes beallitasa!\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
                        return _ret;
                    }


                }
            }
            catch (Exception exbdc)
            {
                Logger.Error(String.Format("BDC kotes exception!\nMessage: {0}\nStackTrace: {1}", exbdc.Message, exbdc.StackTrace));
                _ret = new Result();
                _ret.ErrorCode = "BDC002";
                _ret.ErrorMessage = String.Format("BDC kotes exception!\nMessage: {0}\nStackTrace: {1}", exbdc.Message, exbdc.StackTrace);
                return _ret;
            }

            //  egyebek:
            //  TODO: fodokumentum - MAJD KESOBB
            if (!String.IsNullOrEmpty(GetParam("spsCttId", xmlParams)) && !GetParam("spsCttId", xmlParams).Equals("0x0101") && !GetParam("spsCttId", xmlParams).Equals("ALAPFELT"))
            {
                try
                {
                    using (AxisUploadToSps.Upload upld = GetAxisUploadToSps())
                    {
                        string sPath = tarolasiHely.GetItemSitePath();

                        String docLibName = (!tarolasiHely.DoktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarDocLibPath)) ? tarolasiHely.DoktarDocLibPath + "/" : tarolasiHely.DoktarDocLibPath;
                        String folderPath = (!tarolasiHely.DoktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarFolderPath)) ? tarolasiHely.DoktarFolderPath + "/" : tarolasiHely.DoktarFolderPath;

                        //  verzio

                        Logger.Info(String.Format("set doc meta prop spsben: edok_w_verzio = {0}", aktVersion));

                        string resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_verzio", aktVersion);
                        if (String.IsNullOrEmpty(resStr))
                        {
                            Logger.Error(String.Format("sps verzio tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                            _ret = new Result();
                            _ret.ErrorCode = "TUL0000";
                            _ret.ErrorMessage = String.Format("sps verzio tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                            return _ret;
                        }


                        //  dokumentum_id
                        Logger.Info(String.Format("set doc meta prop spsben: edok_w_dokumentum_id = {0}", dokumentumId));

                        resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_dokumentum_id", dokumentumId);
                        if (String.IsNullOrEmpty(resStr))
                        {
                            Logger.Error(String.Format("Dokumentum id tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                            _ret = new Result();
                            _ret.ErrorCode = "TUL0002";
                            _ret.ErrorMessage = String.Format("Dokumentum id tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                            return _ret;
                        }

                        //  edok_w_vegrehajto_uid
                        //Logger.Info(String.Format("set doc meta prop spsben: edok_w_vegrehajto_uid = {0}", GetParam("?????", xmlParams)));

                        //resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_vegrehajto_uid", GetParam("???????", xmlParams));
                        //if (String.IsNullOrEmpty(resStr))
                        //{
                        //    Logger.Error(String.Format("edok_w_vegrehajto_uid tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                        //    _ret = new Result();
                        //    _ret.ErrorCode = "TUL00021";
                        //    _ret.ErrorMessage = String.Format("edok_w_vegrehajto_uid tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                        //    return _ret;
                        //}

                        //  edok_w_vegrehajto_nev
                        //Logger.Info(String.Format("set doc meta prop spsben: edok_w_vegrehajto_nev = {0}", GetParam("?????", xmlParams)));

                        //resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_vegrehajto_nev", GetParam("???????", xmlParams));
                        //if (String.IsNullOrEmpty(resStr))
                        //{
                        //    Logger.Error(String.Format("edok_w_vegrehajto_nev tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                        //    _ret = new Result();
                        //    _ret.ErrorCode = "TUL00021";
                        //    _ret.ErrorMessage = String.Format("edok_w_vegrehajto_nev tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                        //    return _ret;
                        //}

                        //  iktatas eseten az alabbiakat is kell

                        if (!String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))
                        {

                            //  ujirat flag

                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_ujirat = {0}", "0"));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_ujirat", "0");
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("Ujirat flag tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL0003";
                                _ret.ErrorMessage = String.Format("�j irat flag tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                            //  edok_w_url_gep
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_url_gep = {0}", String.Format("{0}", Contentum.eUtility.UI.GetAppSetting("SharePointUrl"))));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_url_gep", String.Format("{0}", Contentum.eUtility.UI.GetAppSetting("SharePointUrl")));
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("edok_w_url_gep tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL0004";
                                _ret.ErrorMessage = String.Format("edok_w_url_gep tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                            //  edok_w_url_rootdoktar
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_url_rootdoktar = {0}", String.Format("{0}", GetParam("rootSiteCollectionUrl", xmlParams))));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_url_rootdoktar", String.Format("{0}", GetParam("rootSiteCollectionUrl", xmlParams)));
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("edok_w_url_rootdoktar tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL00041";
                                _ret.ErrorMessage = String.Format("edok_w_url_rootdoktar tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                            //  edok_w_url_site
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_url_site = {0}", String.Format("{0}", sPath)));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_url_site", String.Format("{0}", sPath));
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("edok_w_url_site tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL00042";
                                _ret.ErrorMessage = String.Format("edok_w_url_site tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                            //  edok_w_url_doknev
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_url_doknev = {0}", String.Format("{0}", filename)));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_url_doknev", String.Format("{0}", filename));
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("edok_w_url_doknev tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL00043";
                                _ret.ErrorMessage = String.Format("edok_w_url_doknev tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                            //  irat_id = irat_id
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_irat_id = {0}", GetParam("irat_id", xmlParams)));

                            resStr = upld.SetItemProperty(sPath, docLibName, folderPath, filename, "edok_w_irat_id", GetParam("irat_id", xmlParams));
                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("Irat id tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                _ret = new Result();
                                _ret.ErrorCode = "TUL0001";
                                _ret.ErrorMessage = String.Format("Irat id tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                        } // if iktatokonyv
                    }

                }
                catch (Exception exbdc)
                {
                    Logger.Error(String.Format("Egy�b SPS tulajdons�gok be�ll�t�sa k�zben t�rt�nt exception!\nMessage: {0}\nStackTrace: {1}", exbdc.Message, exbdc.StackTrace));
                    _ret = new Result();
                    _ret.ErrorCode = "BDC003";
                    _ret.ErrorMessage = String.Format("Egy�b SPS tulajdons�gok be�ll�t�sa k�zben t�rt�nt exception!\nMessage: {0}\nStackTrace: {1}", exbdc.Message, exbdc.StackTrace);
                    return _ret;
                }

            } // if (!String.IsNullOrEmpty(GetParam("spsCttId", xmlParams)))

        } //if SharePoint

        #endregion

        #region Feltoltes: a feltoltott file check in

        Logger.Info("--- Feltoltes: a feltoltott file check in");

        Contentum.eDocument.SharePoint.DocumentService spsDs = new Contentum.eDocument.SharePoint.DocumentService();

        if (documentStoreType.Equals(Contentum.eUtility.Constants.DocumentStoreType.SharePoint))
        {

            Logger.Info(String.Format("ujKrtDokBejegyzesKell: {0} || munkaanyag: {1} -> checkInMode: {2} (0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)", ujKrtDokBejegyzesKell, munkaanyag, checkInMode));

            Contentum.eDocument.SharePoint.FolderService fos = new Contentum.eDocument.SharePoint.FolderService();

            string megjegyzes = String.IsNullOrEmpty(GetParam("megjegyzes", xmlParams)) ? "." : GetParam("megjegyzes", xmlParams);

            //Logger.Info(String.Format("Sharepoint-ba feltoltott file checkin. (Mode: {0})", checkInMode));
            //Result checkinRet = spsDs.CheckIn(RemoveSlash(doktarSitePath), RemoveSlash(doktarDocLibPath), RemoveSlash(doktarFolderPath), filename, megjegyzes, checkInMode); //Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn);
            //if (!String.IsNullOrEmpty(checkinRet.ErrorCode))
            //{
            //    /// -2130245361 = nincs be�ll�tva a folderen a verzio kezeles beallitva
            //    if (!checkinRet.ErrorCode.Equals("-2130245361"))
            //    {
            //        Logger.Error("Sharepoint-ba feltoltott file checkin hiba: " + checkinRet.ErrorCode + "; " + checkinRet.ErrorMessage);
            //        return checkinRet;
            //    }
            //    else
            //    {
            //        Logger.Info("Sharepoint-ba feltoltott file checkin hiba: Nincs beallitva a verziokezeles a folderen!");
            //    }
            //}

            try
            {
                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = tarolasiHely.GetItemSitePath();

                    String res = upld3.ItemCheckin(itemSitePath
                        , RemoveSlash(tarolasiHely.DoktarDocLibPath)
                        , tarolasiHely.DoktarFolderPath
                        , filename
                        , checkInMode
                        , megjegyzes);

                    XmlDocument resultDoc = new XmlDocument();
                    try
                    {
                        resultDoc.LoadXml(res);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba az ItemCheckin altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                        _ret.ErrorCode = "ItemCheckin";
                        _ret.ErrorMessage = "Hiba az ItemCheckin f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                    }

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                        return _ret;
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Hiba a file checkinkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                Result ret = new Result();
                ret.ErrorCode = "ItemCheckin";
                ret.ErrorMessage = String.Format("Hiba a file checkin-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                return ret;
            }


        }

        #endregion

        #region SHA1 checksumma update a filera (jelenleg, ha SPS van csak akkor)

        Logger.Info("--- SHA1 checksumma update a filera (jelenleg, ha SPS van csak akkor)");

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            if (getContent)
            {
                Logger.Info("SHA1 checksumma update a filera resz: indul.");

                string sha1chk = String.Empty;

                ExecParam execp = new ExecParam();
                execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
                execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
                execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
                execp.Record_Id = dokumentumId;

                Logger.Info("SHA1 checksumma update a filera resz: GetDoc elott.");
                Result resultDokSel = GetDoc(execp);

                if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor (SHA1 chk).\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                    return resultDokSel;
                }

                if (resultDokSel.Record == null)
                {
                    Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor. Null a result record mezoje! (SHA1 chk)"));
                    resultDokSel = new Result();
                    resultDokSel.ErrorCode = "VESD00001";
                    resultDokSel.ErrorMessage = "Hiba a krt_dokumentumok bejegyz�s lek�rdez�sekor! Null a visszaadott objektum. (SHA1 chk)";
                    return resultDokSel;
                }

                Logger.Info("Lekerdezes ok. (SHA1 chk)");

                Logger.Info("SHA1 checksumma update a filera resz: tartalom leszedese sharepointbol.");

                using (SPCopys.Copy cpy = new SPCopys.Copy())
                {

                    cpy.Url = String.Format("{0}{1}{2}_vti_bin/copy.asmx", Contentum.eUtility.UI.GetAppSetting("SharePointUrl"), ((!tarolasiHely.RootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.RootSiteCollectionUrl)) ? tarolasiHely.RootSiteCollectionUrl + "/" : tarolasiHely.RootSiteCollectionUrl), ((!tarolasiHely.DoktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(tarolasiHely.DoktarSitePath)) ? tarolasiHely.DoktarSitePath + "/" : tarolasiHely.DoktarSitePath));
                    cpy.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                    Logger.Info(String.Format("Copy WS url: {0}", cpy.Url));

                    SPCopys.FieldInformation myFieldInfo = new SPCopys.FieldInformation();
                    SPCopys.FieldInformation[] myFieldInfoArray = { myFieldInfo };

                    try
                    {
                        string copySource = tarolasiHely.ToExternalLink();

                        Logger.Info(String.Format("item copySource: {0}", copySource));

                        uint myGetUint = cpy.GetItem(copySource, out myFieldInfoArray, out feltoltottFileByteArray);

                        Logger.Info(String.Format("SPS Copy WS GetItem visszateresi erteke (myGetUint): {0}", myGetUint));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba a SPS Copy WS GetItem hivasakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                        _ret = new Result();
                        _ret.ErrorCode = "CPYSSHA10001";
                        _ret.ErrorMessage = String.Format("Hiba a SPS Copy WS GetItem hivasakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                        return _ret;
                    }

                    sha1chk = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(feltoltottFileByteArray);

                    Logger.Info(String.Format("Szamolt SHA1: {0}", sha1chk));
                }

                Logger.Info("Update elokeszitese. (SHA1 chk)");

                KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

                updateDok.Updated.SetValueAll(false);
                updateDok.Base.Updated.SetValueAll(false);

                Logger.Info(String.Format("_CheckInUploadedFileWithCTT kapott tartalomSHA1:", GetParam("tartalomSHA1", xmlParams)));

                //  tartalom: a belso tartalomra hash (word addin szamolja es adja at parameterkent)
                updateDok.TartalomHash = GetParam("tartalomSHA1", xmlParams);
                updateDok.Updated.TartalomHash = true;

                //  kivonat: a sps-be feltoltott allapot, teljes file hash-e.
                updateDok.KivonatHash = sha1chk;
                updateDok.Updated.KivonatHash = true;

                updateDok.Base.Updated.Ver = true;

                Logger.Info("Update elott. (SHA1 chk)");

                KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
                resultDokSel = dokumentumService.Update(execp, updateDok);

                if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                {
                    Logger.Error(String.Format("SHA1 chk update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                    return resultDokSel;
                }
            }

        }

        #endregion


        #region Ha szerepelt mar a file, akkor az elozo krt_dok bejegyzes urljenek atirasa.

        Logger.Info("--- Ha szerepelt mar a file, akkor az elozo krt_dok bejegyzes urljenek atirasa.");

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {

            Logger.Info(String.Format("Nem csak egy verzio van. elozoVerzio: {0} - ujirat: {1}", elozoVerzio, GetParam("ujirat", xmlParams)));

            //if (!String.IsNullOrEmpty(elozoVerzio) && GetParam("ujirat", xmlParams).Equals("IGEN"))
            if (!String.IsNullOrEmpty(elozoVerzio))
            {

                Logger.Info("Nem csak egy verzio van. Az elozot urljet at kell allitani.");

                string keresettExternalLink = tarolasiHely.ToExternalLink();

                Logger.Info(String.Format("Db-ben kereses indul... Keresett external_link: {0} es verziojel: {1}", keresettExternalLink, elozoVerzio));

                ExecParam execp = new ExecParam();
                execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
                execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
                execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;

                KRT_DokumentumokSearch krtDokSearch = new KRT_DokumentumokSearch();

                krtDokSearch.External_Link.Value = keresettExternalLink;
                krtDokSearch.External_Link.Operator = Query.Operators.equals;

                krtDokSearch.VerzioJel.Value = elozoVerzio;
                krtDokSearch.VerzioJel.Operator = Query.Operators.equals;

                KRT_DokumentumokService dokService = new KRT_DokumentumokService();
                Result resultElozoVerDok = dokService.GetAll(execp, krtDokSearch);

                if (!String.IsNullOrEmpty(resultElozoVerDok.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba az elozo verzio lekerdezeskor (dokService.GetAll)!\nKod: {0}\nMessage: {1}", resultElozoVerDok.ErrorCode, resultElozoVerDok.ErrorMessage));
                    return resultElozoVerDok;
                }

                Logger.Info(String.Format("Elozo verzio lekerdezes ok. Talalt darabszamok ellenorzese."));

                //  ez itt azert nem kell, mert ha elso verzio, akkor biztos nem talalunk bejegyzest
                //  helyesebb lenne azt figyelni, h ez az elso verzio-e, de idohiany miatt majd kesobb

                //if (resultElozoVerDok.Ds.Tables[0].Rows.Count == 0)
                //{
                //    Logger.Error("Nem talaltunk elozo verzio bejegyzest az adatbazisban!");
                //    _ret.ErrorCode = "PREVV0001";
                //    _ret.ErrorMessage = "Nem tal�ltuk meg az el�z� verzi�hoz tartoz� bejegyz�st az adatb�zisban!";
                //    return _ret;
                //}

                if (resultElozoVerDok.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error(String.Format("Tobb bejegyzest talaltunk az elozo verziora az adatbazisban ({0})!", resultElozoVerDok.Ds.Tables[0].Rows.Count));
                    _ret.ErrorCode = "PREVV0002";
                    _ret.ErrorMessage = "T�bb bejegyz�st tal�ltuk az el�z� verzi�hoz az adatb�zisban!";
                    return _ret;
                }

                if (resultElozoVerDok.Ds.Tables[0].Rows.Count == 1)
                {
                    Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas elokeszites kezdodik."));

                    execp.Record_Id = Convert.ToString(resultElozoVerDok.Ds.Tables[0].Rows[0]["Id"]);
                    Result resultDokSel = GetDoc(execp);

                    if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                    {
                        Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor.\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                        return resultDokSel;
                    }

                    Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas amit kell."));

                    KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

                    updateDok.Updated.SetValueAll(false);
                    updateDok.Base.Updated.SetValueAll(false);

                    updateDok.External_Link = elozoVerzioUrl;
                    updateDok.Updated.External_Link = true;

                    updateDok.Dokumentum_Id_Kovetkezo = aktualisKrtDokGuid;
                    updateDok.Updated.Dokumentum_Id_Kovetkezo = true;

                    updateDok.Base.Updated.Ver = true;

                    execp = new ExecParam();
                    execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
                    execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
                    execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
                    execp.Record_Id = updateDok.Id;

                    KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
                    Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas update elott."));
                    resultDokSel = dokumentumService.Update(execp, updateDok);

                    if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                    {
                        Logger.Error(String.Format("Elozo verzio update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                        return resultDokSel;
                    }

                }

            } //if (nem ures az elozoVerzio)


        }

        #endregion

        #region Szerver oldali alairas

        Logger.Info("--- Szerver oldali alairas");

        string param_alairasKell = GetParam("alairaskell", xmlParams);
        if (param_alairasKell == "IGEN")
        {
            // param�terek kinyer�se:
            string alairasSzabalyId = GetParam("alairasSzabalyId", xmlParams);
            string alairasSzint = GetParam("alairasSzint", xmlParams);
            string easzKod = GetParam("easzKod", xmlParams);
            string tanusitvanyId = GetParam("tanusitvanyId", xmlParams);
            string kivarasiIdo = GetParam("kivarasiIdo", xmlParams);
            string alairasMod = GetParam("alairasMod", xmlParams);
            string alairasTulajdonos = GetParam("alairasTulajdonos", xmlParams);

            Logger.Info(String.Format("Szerver oldali al��r�s - Param�terek: al��r�s szab�ly id: '{0}'; al��r�sszint: '{1}'; easzKod: '{2}'; tan�s�tv�ny id: '{3}'"
                , alairasSzabalyId, alairasSzint, easzKod, tanusitvanyId));

            //
            // feltoltott file vissza SPSbol:
            //
            // ebben van: feltoltottFileByteArray

            DokumentumAlairasService dokumentumAlairasService = new DokumentumAlairasService(this.dataContext);

            #region Szerver oldali al��r�s:

            byte[] alairtTartalom;
            string alairtFileNev;

            dokumentumAlairasService.SzerverOldaliAlairas(feltoltottFileByteArray, filename, easzKod, out alairtTartalom, out alairtFileNev);

            #endregion

            #region Ellen�rz�s, �s al��rt file felt�lt�s

            string external_Id = "";
            string external_Link = "";
            string external_Source = "";
            string external_Info = "";

            Result result_alairtFileFeltoltes = dokumentumAlairasService.AlairtFileFeltoltes(spsDs, alairtTartalom, Contentum.eUtility.FileFunctions.GetValidSharePointFileName(alairtFileNev)
                , documentStoreType, tarolasiHely.RootSiteCollectionUrl, tarolasiHely.DoktarSitePath, tarolasiHely.DoktarDocLibPath, tarolasiHely.DoktarFolderPath
                , out external_Id, out external_Link, out external_Source, out external_Info);

            if (result_alairtFileFeltoltes.IsError)
            {
                // hiba:
                return result_alairtFileFeltoltes;
            }

            #endregion

            #region Al��rt dokumentum beregisztr�l�sa (KRT_Dokumentumok, KRT_DokumentumAlairasok, KRT_DokumentumKapcsolatok)

            dokumentumAlairasService.AlairtDokumentumRegisztralasa(execparam, dokumentumId, alairtTartalom, Contentum.eUtility.FileFunctions.GetValidSharePointFileName(alairtFileNev)
                , alairasSzabalyId, alairasMod, alairasSzint, alairasTulajdonos, tanusitvanyId, kivarasiIdo
                , external_Link, external_Id, external_Source, external_Info);

            #endregion

        }
        #endregion

        #region Mappatartalom regisztralasa.

        //Logger.Info("--- Mappatartalom regisztralasa.");

        //if (GetParam("ujirat", xmlParams).Equals("IGEN"))
        //{
        //    Logger.Info("Mappatartalom bejegyzes elott.");

        //    Result eDocMappatartalomGuid = SeteDocMappatartalom(execparam, eDocMappaGuid, dokumentumId);

        //    if (!String.IsNullOrEmpty(eDocMappatartalomGuid.ErrorCode))
        //    {
        //        if (GetParam("ujirat", xmlParams).Equals("IGEN") && aktVersion.Equals("0.1"))
        //        {
        //            Result tmpRes = DeleteExternalFile(documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);
        //        }
        //        Logger.Error("Mappatartalom eDocba hiba: " + eDocMappatartalomGuid.ErrorCode + "; " + eDocMappatartalomGuid.ErrorMessage);

        //        return eDocMappatartalomGuid;
        //    }
        //}

        #endregion

        #region esemenykezeles

        //  Ennek az esem�nybejegyz�senek nem itt a helye, mert most itt az id-k (kuldemeny vagy irat id) megletetol fugg!

        String esemenyhezTartozoObjektumId = String.Empty;

        try
        {

            Logger.Info("DokumentumokNew esemeny bejegyzes.");

            if (!String.IsNullOrEmpty(GetParam("kivalasztottIratId", xmlParams)))
            {
                esemenyhezTartozoObjektumId = GetParam("kivalasztottIratId", xmlParams);
            }

            if (String.IsNullOrEmpty(esemenyhezTartozoObjektumId))
            {
                if (!String.IsNullOrEmpty(GetParam("csatolmanyId", xmlParams)))
                {
                    Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
                    Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
                    Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

                    //if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
                    //{
                    //    csatolmanyokSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                    //    csatolmanyokSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                    //}
                    //else
                    //{
                    //    csatolmanyokSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                    //    csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                    //}

                    csatolmanyokSearch.Id.Value = GetParam("csatolmanyId", xmlParams);
                    csatolmanyokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result iratDokRes = csatolmanyokService.GetAll(execparam.Clone(), csatolmanyokSearch);

                    if (!String.IsNullOrEmpty(iratDokRes.ErrorCode))
                    {
                        Logger.Warn(String.Format("Hib�val t�rt vissza az irat dokumentum kapcsol�t�bla lek�rdez�s! De nem �llunk le, mert csak az esem�ny r�gz�t�s�hez kell!\nErrorCode: {0}\nErrorMessage: {1}", iratDokRes.ErrorCode, iratDokRes.ErrorMessage));
                        //return iratDokRes;
                    }

                    if (iratDokRes.Ds.Tables[0].Rows.Count == 0)
                    {
                        Logger.Error(String.Format("Nem adott vissza eredmenyt a kapcsol�t�bla kapcsolotabla select!"));
                    }

                    if (iratDokRes.Ds.Tables[0].Rows.Count > 1)
                    {
                        Logger.Error(String.Format("Tul sok eredmenyt adott a kapcsol�t�bla select!"));
                    }

                    if (iratDokRes.Ds.Tables[0].Rows.Count == 1)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"])))
                        {
                            esemenyhezTartozoObjektumId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"]);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["IraIrat_Id"])))
                        {
                            esemenyhezTartozoObjektumId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["IraIrat_Id"]);
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(esemenyhezTartozoObjektumId))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                ExecParam exxx1 = execparam.Clone();


                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(exxx1
                    , dataContext.Tranz_Id
                    , esemenyhezTartozoObjektumId
                    , "KRT_Dokumentumok"
                    , "DokumentumokNew").Record;

                //
                //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....
                //  az emailnal pl ha ezt hozzaadtuk, akkor mukodott rendesen
                //  itt is hozzaadjuk a biztosag kedveert

                if (String.IsNullOrEmpty(execparam.Felhasznalo_Id))
                {
                    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DokumentumokNew esemeny!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = (exxx1.Helyettesites_Id != null) ? exxx1.LoginUser_Id : exxx1.Felhasznalo_Id;
                }

                Result eventLogResult = eventLogService.Insert(exxx1, eventLogRecord);
            }

        }
        catch (Exception exev)
        {
            Logger.Error(String.Format("Hiba az esemenybejegyzes reszben: {0}\nStackTrace: {1}\nSource: {2}", exev.Message, exev.StackTrace, exev.Source));
        }

        #endregion

        Logger.Info("dokumentumId a resultba");
        _ret.Uid = dokumentumId;
        //Logger.Info("dokumentumFullUrl a resultba");
        //_ret.Record = GetParam("dokumentumFullUrl", xmlParams);

        Logger.Info("result Xml string gyartasa.");

        String visszaXml = "<result>"
            + "<dokumentumFullUrl>" + Contentum.eUtility.XmlFunction.ConvertToCData(GetParam("dokumentumFullUrl", xmlParams)) + "</dokumentumFullUrl>"
            + "<tortentUjKrtDokBejegyzes>" + GetParam("tortentUjKrtDokBejegyzes", xmlParams) + "</tortentUjKrtDokBejegyzes>"
            + "<csatolmanyId>" + GetParam("csatolmanyId", xmlParams) + "</csatolmanyId>"
            + "<csatolmanyVerzio>" + GetParam("csatolmanyVerzio", xmlParams) + "</csatolmanyVerzio>"
            + "</result>";

        _ret.Record = visszaXml;

        Logger.Info(String.Format("result Xml string: {0}", visszaXml));

        Logger.Info("DocumentService._CheckInUploadedFileWithCTT vege.");

        return _ret;
    } // _UploadFromeRecordWithCTT


    /// <summary>
    /// Az uploadban hasznalt xmlStrParambol visszaadja a megadott nevu parameter erteket.
    /// </summary>
    /// <param name="pname">A parameter neve.</param>
    /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
    /// <returns></returns>
    private string GetParam(String pname, XmlDocument xdoc)
    {
        return DocumentService.Utility.GetParam(pname, xdoc);
    }

    private string GetParam(String pname, XmlDocument xdoc, string defaultValue)
    {
        string pvalue = GetParam(pname, xdoc);

        if (String.IsNullOrEmpty(pvalue))
            return defaultValue;

        return pvalue;
    }

    /// <summary>
    /// Az uploadban hasznalt xmlStrParambol visszaadja hogy a megadott nevu parameter letezik-e.
    /// </summary>
    /// <param name="pname">A parameter neve.</param>
    /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
    /// <returns>TRUE/FALSE</returns>
    private bool IsParamExists(String pname, XmlDocument xdoc)
    {
        if (xdoc == null) return false;
        return (xdoc.SelectNodes(String.Format("//{0}", pname)).Count == 1) ? true : false;
    }

    /// <summary>
    /// A MOSS t�rol�si strukt�r�j�nak a k�vetelm�nyei alapj�n meghat�rozza a file tartalom �s a beadott param�terek alapj�n.
    /// A tartalom alapj�n: ez csak DOCX-ekre vonatkozik, belen�z.
    /// 
    /// A kiindul�, alap felt�lt�si strukt�ra meghat�roz�sa.
    /// Ha DOCX-r�l van sz�, CTT vizsg�lat, illetve a CTT MOSSbeli megl�t�nek vizsg�lata.
    /// 
    /// Kapott param�ter XML string (az UploadFromeRecordWithCTT amit kap azt adja tov�bb ennek):
    /// <uploadparameterek>
    ///     <iktatokonyv>String</iktatokonyv>           <!-- iktat�k�ny jel (pl: FPH001) MOSS t�rol�si strukt�r�hoz, site urlbe -->
    ///     <foszam>String</foszam>                     <!-- �gyirat f�sz�ma: MOSS t�rol�si strukt�r�hoz, folder -->
    ///     <kivalasztottIratId>GUID</kivalasztottIratId>    <!-- ahova csatoljuk majd, az irat id-je vagy �res -->
    ///     <docmetaDokumentumId>GUID</docmetaDokumentumId>  <!-- word iktatsbol jon, dokumentumban tarolt krt_Dok GUID, vagy �res -->
    ///     <megjegyzes>String</megjegyzes>                 <!-- megjegyz�s a MOSS verzi�hoz -->
    ///     <munkaanyag>IGEN/NEM</munkaanyag>               <!-- IGEN/NEM - a l�trehozand� irat munkaanyag lesz; nem irat eset�n NEM -->
    ///     <ujirat>IGEN/NEM</ujirat>                       <!-- IGEN/NEM - a l�trehozand� irat �j-e; nem irat eset�n NEM -->
    ///     <vonalkod>String</vonalkod>                     <!-- a vonalk�d �rt�ke, ami a krt_Dok bejegyz�sebe ker�l -->
    ///     <docmetaIratId>GUID</docmetaIratId>             <!-- word iktatsbol jon, dokumentumban tarolt irat id GUID, vagy �res -->
    ///     <ujKrtDokBejegyzesKell>IGEN/NEM</ujKrtDokBejegyzesKell> <!-- IGEN/NEM - mindenk�ppen kell-e �j krt_Dok bejegyz�s -->
    ///     <tartalomSHA1>String</tartalomSHA1>             <!-- SHA1 chksum tartalomra; krt_dokba ker�l.  -->
    ///     <alairaskell>IGEN/NEM</alairaskell>             <!-- IGEN/NEM - kell-e elektronikus al��r�s... -->
    /// </uploadparameterek>
    /// 
    /// 
    /// Visszaadott XML string (a kapottat eg�sz�t� ki ezekkel):
    ///   <rootSiteCollectionUrl>String</rootSiteCollectionUrl>         <!-- root site collection url, tipikusan: sites/edok/-->
    ///   <doktarSitePath>String</doktarSitePath>                       <!-- dokumentumtar/2008/CSATOLMANYOK/ -->
    ///   <doktarDocLibPath>String</doktarDocLibPath>                   <!-- Doclib neve -->
    ///   <doktarFolderPath>String</doktarFolderPath>                   <!-- Folder path vagy n�v vagy �res-->
    ///   <spsCttId/>                                                   <!-- ha l�tezik MOSSban a CTT idje, vagy �res -->
    ///   <sablonAzonosito/>                                            <!-- sablonazonos�t�, vagy �res -->
    ///   <ujKrtDokGuid>GUID</ujKrtDokGuid>                             <!-- A l�trej�v� krt_dok bejegyz�s azonos�t�ja (NEM HASZN�LT!) -->
    ///   <cttNeve/>                                                    <!-- MOSS ctt neve -->
    ///   <iratMetaDefId/>                                              <!-- NEM HASZN�LT -->
    ///   <iratMetaDefFoDokumentum/>                                    <!-- NEM HASZN�LT -->
    /// 
    /// TODO: Itt �ll�t�dnak be az iratMetaDef-es dolgok.
    /// 
    /// </summary>
    /// <param name="execparam">Ezt most nem hasznaljuk semmire, de lehet majd kesobb kell?!?!</param>
    /// <param name="documentStoreType">Sharepoint.</param>
    /// <param name="filename">�llom�ny neve kiterjeszt�ssel.</param>
    /// <param name="cont">�llom�ny tartalma.</param>
    /// <param name="paramsXmlStr">Egy�b param�terek XML stringk�nt.</param>
    /// <returns>A record-ban xml stringben a cuccok.</returns>
    /// 
    [WebMethod]
    public Result TarolasiHelyStrukturaNevEsTartalomAlapjan(
          ExecParam execparam
        , string documentStoreType
        , string filename
        , byte[] cont
        , string paramsXmlStr
     )
    {
        Result _ret = new Result();

        Logger.Info("--- DocumentService.TarolasiHelyStrukturaNevEsTartalomAlapjan indul ---");

        #region Xml parameterek xmldocca es parameterek ellenorzese

        XmlDocument xmlParams = new XmlDocument();

        if (paramsXmlStr != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlParams.LoadXml(paramsXmlStr);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, paramsXmlStr));
                _ret.ErrorCode = "XMLPAR0001";
                _ret.ErrorMessage = String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return _ret;
            }
        }
        else
        {
            Logger.Error(String.Format("Hiba az XML param�ter stringgel. Ures string!"));
            _ret.ErrorCode = "XMLPAR0002";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Hiba az XML param�ter stringgel! �res string!");
            return _ret;
        }

        //if (cont == null)
        //{
        //    Logger.Error(String.Format("Ures file tartalom parameter!"));
        //    _ret.ErrorCode = "XMLPAR0003";
        //    _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: null a file tartalma param�ter!");
        //    return _ret;
        //}

        if (String.IsNullOrEmpty(filename))
        {
            Logger.Error(String.Format("Ures file neve parameter!"));
            _ret.ErrorCode = "XMLPAR0004";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: nincs megadva a file neve param�ter!");
            return _ret;
        }

        if (xmlParams.SelectNodes("//iktatokonyv").Count == 0)
        {
            Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!"));
            _ret.ErrorCode = "XMLPAR0005";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!");
            return _ret;
        }

        if (xmlParams.SelectNodes("//foszam").Count == 0)
        {
            Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott a foszam!"));
            _ret.ErrorCode = "XMLPAR0006";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott a foszam!");
            return _ret;
        }

        //  Ezzel vizsgalhatjuk a kesobbiek folyaman, hogy tipusos dokumentumrol van-e szo. (Ha nem nyilvan empty :))
        string spsCttId = String.Empty;
        string sablonAzonosito = String.Empty;

        //  ha a file tartalom ures, akkor lennie kell sablon azonositonak es content type id nek
        int i_sablonAzonosito = xmlParams.SelectNodes("//sablonAzonosito").Count;
        int i_spsCttId = xmlParams.SelectNodes("//spsCttId").Count;

        if ((i_sablonAzonosito != 1 || i_spsCttId != 1) && cont == null)
        {
            Logger.Error(String.Format("Parameterezesi hiba! Ha a tartalom nincs megadva meg kell adni a CTTid-t es a sablonazonositot az xmlbe!"));
            Logger.Error(String.Format("sablonAzonosito node db: {0} ; spsCttId node db: {1}", i_sablonAzonosito, i_spsCttId));
            _ret.ErrorCode = "XMLPAR0007";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Parameterezesi hiba! Ha a tartalom nincs megadva meg kell adni a CTTid-t es a sablonazonositot az xmlbe!");
            return _ret;
        }
        else
        {
            spsCttId = GetParam("spsCttId", xmlParams);
            sablonAzonosito = GetParam("sablonAzonosito", xmlParams);
            Logger.Info(String.Format("XML parameterben kapott sablonAzonosito: {0} es spsCttId: {1}", sablonAzonosito, spsCttId));
        }

        #endregion

        #region A kiindulo, alap feltoltesi struktura meghatarozasa.

        //  A site collection - a kiindul�si url.
        string rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");

        //  Ez a dokumentumt�r url neve �s az �v.
        string doktarSitePath = String.Format("{0}/{1}/", RemoveSlash(UI.GetAppSetting("DokumentumtarSite")), DateTime.Today.ToString("yyyy"));

        string doktarDocLibPath = GetParam("customDoktarDocLibPath", xmlParams, "Egyeb");

        string doktarFolderPath = String.Empty;

        string foszam = GetParam("foszam", xmlParams);

        string alszam = GetParam("alszam", xmlParams);

        Guid ujKrtDokGuid = Guid.NewGuid();

        //  Sitepath valtozik annak fuggvenyeben, h iktatas vagy nem.
        if (String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))
        {
            //doktarSitePath += "CSATOLMANYOK/";
            doktarSitePath += String.Format("CSATOLMANYOK/{0}/", DateTime.Today.ToString("yyyyMM"));

            if (doktarDocLibPath != "KapuRendszerVeveny")
            {
                doktarFolderPath = ujKrtDokGuid.ToString();
            }
        }
        else
        {
            doktarSitePath += String.Format("IKTATOTTANYAGOK/{0}/", RemoveSlash(GetParam("iktatokonyv", xmlParams)));

            if (!String.IsNullOrEmpty(foszam))
            {
                Logger.Info("a k�nyvt�rba beker�l a f�sz�m mellett az alsz�m is");

                if (String.IsNullOrEmpty(alszam))
                {
                    string kivalasztottIratId = GetParam("kivalasztottIratId", xmlParams);
                    if (!String.IsNullOrEmpty(kivalasztottIratId))
                    {
                        Logger.Info(String.Format("A kiv�lasztott irat {0} lek�r�se.", kivalasztottIratId));
                        ExecParam xpm = execparam.Clone();
                        xpm.Record_Id = kivalasztottIratId;
                        EREC_IraIratokService svc = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        Result res = svc.Get(xpm);
                        if (!res.IsError)
                        {
                            EREC_IraIratok kivalasztottIrat = res.Record as EREC_IraIratok;
                            alszam = Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(kivalasztottIrat);

                            Logger.Info(String.Format("A kiv�lasztott irat alsz�ma: {0}", alszam));
                        }
                        else
                        {
                            Logger.Error(String.Format("Hiba a kiv�lasztott irat lek�r�se {0} k�zben.", kivalasztottIratId), xpm, res);
                        }
                    }
                }
                else
                {
                    Logger.Info("A param�terben kapott alsz�m: " + alszam);
                }

            }
            else
            {
                Logger.Info("nincs f�sz�m �tadva");
            }

            doktarFolderPath = Utility.GetIktatottAnyagFolderPath(foszam, alszam);
            Logger.Info(String.Format("doktarFolderPath={0}", doktarFolderPath));

            //doktarFolderPath = foszam;


        }



        #endregion

        #region Ha DOCX kiterjesztesu, akkor CTT vizsgalat s ami meg ezzel jar.

        //  2. Ha docx, akkor megnezzuk van-e tartalomtipusa
        string kiterjesztes = (filename.LastIndexOf('.') == -1) ? " " : filename.Trim().Substring(filename.LastIndexOf('.') + 1);
        string cttNeve = String.Empty;

        Logger.Info(String.Format("filename: {0} - kiterjesztes: {1}", filename, kiterjesztes));

        if (kiterjesztes.Trim().ToLower().Equals("docx"))
        {
            //  Kivessz�k a docxbol a CTT id-t.
            //  Ha nincs, akkor marad Egyeb a DocLib
            string docmetaCttId = String.Empty;

            if (String.IsNullOrEmpty(spsCttId))
            {
                ClassDocxDiscover docxDis = new ClassDocxDiscover();
                docmetaCttId = docxDis.GetContentTypeId(cont);
            }
            else
            {
                docmetaCttId = spsCttId;
            }

            //  Ha �res, akkor marad egyeb.
            if (!String.IsNullOrEmpty(docmetaCttId))
            {
                Logger.Info(String.Format("Docx Wordben talalt CTTid: {0}", docmetaCttId));

                Contentum.eDocument.SharePoint.ContentTypeService cttService = new Contentum.eDocument.SharePoint.ContentTypeService();

                //  megnezzuk megvan-e a sharepoint oldalon, van-e definialva
                //  es nem Dokumentum tipusu!!!!
                //if (!String.IsNullOrEmpty(spsCttId))  // a neve miatt le kell kerni a CTT-t
                spsCttId = cttService.GetContentTypeByWordCTT(docmetaCttId);

                if (!String.IsNullOrEmpty(spsCttId) && !spsCttId.Equals("0x0101"))
                {

                    //  Ha megvan SPS-ben is, akkor meg kell nezni az XML leiroban hova van bekotve.

                    //  visszaveszuk a nevet
                    doktarDocLibPath = cttService.LatestContentTypeName;
                    cttNeve = cttService.LatestContentTypeName;

                    //  Sablon azonosito kivetele
                    Logger.Info("Sablonazonosito kivetele elott.");
                    if (String.IsNullOrEmpty(sablonAzonosito))
                    {
                        using (ClassDocxDiscover docxDis = new ClassDocxDiscover())
                        {
                            sablonAzonosito = docxDis.GetSablonazonosito(cont);
                        }
                    }

                    //  Ha van sablon azonosito, akkor jo, megyunk tovabb
                    if (!String.IsNullOrEmpty(sablonAzonosito))
                    {

                        Logger.Info(String.Format("A kapott sablon azonosito ({0}) bontasa.", sablonAzonosito));
                        char[] sepa = { '_' };
                        string[] saParts = sablonAzonosito.Trim().Split(sepa);

                        // ha nem 3 a szeparalt darabok szama, akkor marad a CTT neve a DocLib neve, s ezzel nezzuk meg az XML mapet
                        // egyebkent meg kivesszuk az elso reszt es az lesz DocLib neve

                        if (saParts.Length == 3)
                        {
                            doktarDocLibPath = saParts[0].Trim();
                            Logger.Info(String.Format("Ok. 3 darab van, ezert ez lett a doktarDocLibPath: {0}", doktarDocLibPath));
                        }
                        else
                        {
                            Logger.Info(String.Format("Nem 3  (hanem {1}) darab van, ezert ez maradt a doktarDocLibPath: {0}", doktarDocLibPath, saParts.Length));
                        }

                    } //if (!String.IsNullOrEmpty(sablonAzonosito))


                    // XML map feldolgoz�s
                    Logger.Info("XML MAP feldolgozas indul.");
                    ClassTartalomtipusKezelo xmlLeiroKezelo = new ClassTartalomtipusKezelo();

                    string tmpDocLibName = xmlLeiroKezelo.GetStoreDocLibNameByName(doktarDocLibPath);

                    if (!String.IsNullOrEmpty(tmpDocLibName))
                    {
                        doktarDocLibPath = tmpDocLibName.Trim();
                    }
                    else
                    {
                        Logger.Info("Nincs a csoport xml leiroban, vagy nincs csoport xml leiro.");
                    }

                }
                else
                {
                    //  nincs meg site szinten a wordben talalt CTT - kiveve a Dokumentum
                    //  ezert hibat dobunk (Palival egyeztetve)
                    if (!spsCttId.Equals("0x0101"))
                    {
                        Logger.Error("HIBA: SPS-ben nincs meg a wordben talalt CTT id!");
                        _ret = new Result();
                        _ret.ErrorCode = "CTTNOINSPS00001";
                        _ret.ErrorMessage = "A sharepointban nem tal�lhat� az a tartalomt�pus azonos�t�, ami a wordben van! Felt�lt�s megszak�tva.";
                        return _ret;
                    }
                }

            }
            else
            {
                Logger.Info("�res a docx CTT!");
            }

        } //  if kiterjesztes docx


        if ((!String.IsNullOrEmpty(doktarDocLibPath)) && (!doktarDocLibPath.EndsWith("/"))) doktarDocLibPath += "/";

        Logger.Info(String.Format("doktarSitePath: {0}\ndoktarDocLibPath: {1}\ndoktarFolderPath: {2}", doktarSitePath, doktarDocLibPath, doktarFolderPath));

        #endregion

        #region Visszaadott parameterek xmlbe torteno betetele majd a resultba illesztese

        Logger.Info("vissza param xmlhez nodok legyartasa");

        XmlNode nd1 = xmlParams.CreateNode(XmlNodeType.Element, "rootSiteCollectionUrl", "");
        nd1.InnerText = rootSiteCollectionUrl;

        XmlNode nd2 = xmlParams.CreateNode(XmlNodeType.Element, "doktarSitePath", "");
        nd2.InnerText = doktarSitePath;

        XmlNode nd3 = xmlParams.CreateNode(XmlNodeType.Element, "doktarDocLibPath", "");
        nd3.InnerText = Contentum.eDocument.SharePoint.Utility.ConvertToUrl(doktarDocLibPath);

        XmlNode nd4 = xmlParams.CreateNode(XmlNodeType.Element, "doktarFolderPath", "");
        nd4.InnerText = doktarFolderPath;

        XmlNode nd5 = xmlParams.CreateNode(XmlNodeType.Element, "spsCttId", "");
        nd5.InnerText = spsCttId;

        XmlNode nd6 = xmlParams.CreateNode(XmlNodeType.Element, "sablonAzonosito", "");
        nd6.InnerText = sablonAzonosito;

        XmlNode nd7 = xmlParams.CreateNode(XmlNodeType.Element, "ujKrtDokGuid", "");
        nd7.InnerText = ujKrtDokGuid.ToString();

        XmlNode nd8 = xmlParams.CreateNode(XmlNodeType.Element, "cttNeve", "");
        nd8.InnerText = cttNeve;

        XmlNode nd9 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefId", "");
        nd9.InnerText = String.Empty;

        XmlNode nd10 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefFoDokumentum", "");
        nd10.InnerText = String.Empty;

        Logger.Info("vissza param xmlhez nodok hozzafuzese");

        xmlParams.ChildNodes[0].AppendChild(nd1);
        xmlParams.ChildNodes[0].AppendChild(nd2);
        xmlParams.ChildNodes[0].AppendChild(nd3);
        xmlParams.ChildNodes[0].AppendChild(nd4);
        xmlParams.ChildNodes[0].AppendChild(nd5);
        xmlParams.ChildNodes[0].AppendChild(nd6);
        xmlParams.ChildNodes[0].AppendChild(nd7);
        xmlParams.ChildNodes[0].AppendChild(nd8);
        xmlParams.ChildNodes[0].AppendChild(nd9);
        xmlParams.ChildNodes[0].AppendChild(nd10);

        Logger.Info("vissza param xml bele a record mezobe");

        _ret.Record = xmlParams.OuterXml.ToString();

        Logger.Info(String.Format("Visszaadott xml: {0}", xmlParams.OuterXml.ToString()));

        #endregion

        Logger.Info("--- DocumentService.TarolasiHelyStrukturaNevEsTartalomAlapjan rendben vege. ---");

        return _ret;
    }


    /// <summary>
    /// A t�rol�si helyen megn�zni, van-e m�r ott ilyen �llom�ny. 
    /// Ez k�s�bb k�sz�lt a word iktat�s sz�m�ra.
    /// NEM HASZN�LT.
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="filename"></param>
    /// <param name="cont"></param>
    /// <param name="paramsXmlStr"></param>
    /// <returns>Result - hiba vagy </returns>
    [WebMethod]
    public Result LetezikMarIlyenNevuWithCTT(
          ExecParam execparam
        , string documentStoreType
        , string filename
        , byte[] cont
        , string paramsXmlStr)
    {
        Logger.Info("DocumentService.LetezikMarIlyenNevuSPSbenWithCTT indul.");

        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result _ret = new Result();

        #region Megnezzuk hova kerul majd a file

        Result resultTHStrukt = this.TarolasiHelyStrukturaNevEsTartalomAlapjan(execparam, documentStoreType, filename, cont, paramsXmlStr);

        if (!String.IsNullOrEmpty(resultTHStrukt.ErrorCode))
        {
            return resultTHStrukt;
        }

        XmlDocument xmlParams = new XmlDocument();

        try
        {
            xmlParams.LoadXml((string)resultTHStrukt.Record);
        }
        catch (Exception exc1)
        {
            Logger.Info(String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
            _ret.ErrorCode = "XMLC0001";
            _ret.ErrorMessage = String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
            return _ret;
        }

        //  A site collection - a kiindul�si url.
        string rootSiteCollectionUrl = GetParam("rootSiteCollectionUrl", xmlParams);

        //  Ez a dokumentumt�r url neve �s az �v.
        string doktarSitePath = RemoveSlash(GetParam("doktarSitePath", xmlParams));

        string doktarDocLibPath = RemoveSlash(GetParam("doktarDocLibPath", xmlParams));

        string doktarFolderPath = GetParam("doktarFolderPath", xmlParams);

        //  Ezzel vizsgalhatjuk a kesobbiek folyaman, hogy tipusos dokumentumrol van-e szo. (Ha nem nyilvan empty :))
        string spsCttId = GetParam("spsCttId", xmlParams);

        #endregion

        #region Megnezzuk letezik-e ilyen nevu file ott

        ListUtilService lisu = new ListUtilService();
        Result resultLisu = lisu.GetListItemIdByName(doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);

        string hibakod = resultLisu.ErrorCode;

        if (!String.IsNullOrEmpty(resultLisu.ErrorCode))
        {
            if (!hibakod.Equals("190000002"))
            {
                Logger.Error(String.Format("Hibaval tert vissza a ListUtilService.GetListItemIdByName.\nErrcode: {0}\nErrm: {1}", resultLisu.ErrorCode, resultLisu.ErrorMessage));
                return resultLisu;
            }
            hibakod = String.Empty;
        }

        #endregion

        #region Bovitett info osszeaalitasa a filerol

        //if (String.IsNullOrEmpty(hibakod))
        //{
        //    Logger.Info("Bovitett info osszeaalitasa a filerol resz indul.");

        //    string path = Convert.ToString(Contentum.eUtility.UI.GetAppSetting("SharePointUrl") 
        //    + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
        //    + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath)
        //    + ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath)
        //    + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath));

        //    string vonalkod = this.GetUploadedFileProperty(path, filename, "edok_w_vonalkod");
        //    Logger.Info(String.Format("Feltoltott doksitol kapott vonalkod: {0}", vonalkod));

        //}

        #endregion

        Logger.Info("DocumentService.LetezikMarIlyenNevuSPSbenWithCTT rendben vege.");
        return _ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="filename"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    private string GetUploadedFileProperty(String path, String filename, String key)
    {
        string _ret = String.Empty;

        Logger.Info(String.Format("GetUploadedFileProperty indul. Kapott parameterek: path: {0} ; filename: {1} ; key: {2}", path, filename, key));

        using (AxisUploadToSps.Upload upld = this.GetAxisUploadToSps())
        {
            try
            {
                //_ret = upld.GetItemProperty(path, filename, key);

                // TODO: ???!!!!!!!!!
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("DocumentService.GetUploadedFileProperty hiba! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            }
        }

        return _ret;
    }

    /// <summary>
    /// Egy megadott file �tnevez�se. Word addin funkci�.
    /// 
    /// N�v form�tum ellen�rz�s.
    /// KRT Dok bejegyz�st megkeresi.
    /// Az �j �s a r�gi n�v kiterjeszt�se meg kell egyezzen!
    /// �tnevez�s: csak MOSS �g van k�szen.
    ///     MOSSban overwrite checkint haszn�lunk!!!!
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="krtDokId">Krt_dok id</param>
    /// <param name="ujNev">�j filen�v kiterjesztessel.</param>
    /// <returns>Result - a Record mez�ben az �j n�vvel, ha sikeres volt.</returns>
    [WebMethod]
    public Result RenameUploadedDocument(ExecParam execparam, String krtDokId, String ujNev)
    {
        Logger.Info(String.Format("DocumentService.RenameUploadedDocument indul. krtDokId param: {0} - ujNev: {1}", krtDokId, ujNev));

        string originalUjNev = ujNev;
        ujNev = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(ujNev);
        Logger.Info(String.Format("DocumentService.RenameUploadedDocument indul. krtDokId param: {0} - ujNev: {1}", krtDokId, ujNev));
        Result _ret = new Result();

        if (String.IsNullOrEmpty(krtDokId))
        {
            Logger.Error(String.Format("�res a kapott dokumentum id param�ter!"));
            _ret.ErrorCode = "PARAM0001";
            _ret.ErrorMessage = "�res a kapott dokumentum id param�ter!";
            return _ret;
        }

        #region jo formaju-e az uj nev

        //TODO: EZT VISSZA KELL RAKNI, CSAK kommentbe raktam, hogy a CR#1483-as rendben felmehessen (2008.07.16)

        ujNev = ujNev.Trim();
        if (!ujNev.Equals(Contentum.eDocument.SharePoint.Utility.validIktatandoDokumentumNeve(ujNev)))
        {
            Logger.Error(String.Format("A megadott �j n�v nem megengedett karaktereket tartalmaz!"));
            _ret.ErrorCode = "PARAM0002";
            _ret.ErrorMessage = "A megadott �j n�v nem megengedett karaktereket tartalmaz!";
            return _ret;
        }

        #endregion

        #region Bejegyzes megkeresese krt_Dokban.

        ExecParam execp2 = execparam.Clone();
        execp2.Record_Id = krtDokId;

        Result resultDok = this.GetDoc(execp2);

        if (!String.IsNullOrEmpty(resultDok.ErrorCode))
        {
            Logger.Error(String.Format("Dokumentum rekord select (id alapjan) hiba!\nErrCode: {0}\nErrMessage: {1}", resultDok.ErrorCode, resultDok.ErrorMessage));
            return resultDok;
        }

        if (resultDok.Record == null)
        {
            Logger.Error("Null a GetDoc altal visszaadott Record mezo!");
            _ret.ErrorCode = "NR0001";
            _ret.ErrorMessage = "RenameUploadedDocument hiba! Null a param�terk�nt kapott krt_dok id �ltal visszaadott eredm�ny.";
            return _ret;
        }

        KRT_Dokumentumok dokuRekord = (KRT_Dokumentumok)resultDok.Record;

        string regiNev = dokuRekord.FajlNev.Trim();

        #endregion

        #region eredeti nevenek a kiterjesztesevel osszevetes

        Logger.Info("eredeti nevenek a kiterjesztesevel osszevetes");

        int liE = regiNev.Trim().ToLower().LastIndexOf(".");
        int liU = ujNev.Trim().ToLower().LastIndexOf(".");
        String kiterjesztesEredeti = (liE == -1) ? "" : regiNev.Trim().ToLower().Substring(liE);
        String kiterjesztesUjNev = (liU == -1) ? "" : ujNev.Trim().ToLower().Substring(liU);

        Logger.Info(String.Format("kiterjesztesEredeti: {0} - kiterjesztesUjNev: {1}", kiterjesztesEredeti, kiterjesztesUjNev));

        if (!String.IsNullOrEmpty(kiterjesztesEredeti))
        {
            if (!kiterjesztesEredeti.Equals(kiterjesztesUjNev))
            {
                Logger.Error("Nem egyezik meg a ket kiterjesztes!");
                _ret.ErrorCode = "NR00034";
                _ret.ErrorMessage = "RenameUploadedDocument hiba! A kapott �j n�v kiterjeszt�se nem egyezik meg a r�gi n�v kiterjeszt�s�vel!";
                return _ret;
            }
        }

        #endregion

        #region Az external id (ha ures, external link) alapjan leszedjuk az osszeset

        Logger.Info("Kereses external_id alapjan.");

        KRT_DokumentumokService dokService = new KRT_DokumentumokService();
        KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();

        //dokSearch.External_Link.Value = dokuRekord.External_Link;
        //dokSearch.External_Link.Operator = Contentum.eQuery.Query.Operators.equals;

        dokSearch.External_Source.Value = dokuRekord.External_Source;
        dokSearch.External_Source.Operator = Contentum.eQuery.Query.Operators.equals;

        if (String.IsNullOrEmpty(dokuRekord.External_Id))
        {
            Logger.Error(String.Format("Ures az external id! External link ({0}) - external_source ({1}).", dokuRekord.External_Link, dokSearch.External_Source));
            _ret = new Result();
            _ret.ErrorCode = "NULLEXID00001";
            _ret.ErrorMessage = "�res a rekord external_id bejegyz�se, ez�rt nem tudjuk �tnevezni, mert nem tudjuk �sszefogni az �sszetartoz� verzi�kat!";
            return _ret;
        }
        else
        {
            Logger.Info(String.Format("Van external id ({0}). /external link: ({1}) external_source: ({2})/ ", dokuRekord.External_Id, dokuRekord.External_Link, dokSearch.External_Source));

            dokSearch.External_Id.Value = dokuRekord.External_Id;
            dokSearch.External_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        // a legelso (0-dik) legyen az aktulais verzio
        dokSearch.OrderBy = "VerzioJel desc";

        Result resultDokok = dokService.GetAll(execparam, dokSearch);

        if (!String.IsNullOrEmpty(resultDokok.ErrorCode))
        {
            Logger.Error(String.Format("Kereses hiba (external_link alapjan)!\nErrCode: {0}\nErrMessage: {1}", resultDokok.ErrorCode, resultDokok.ErrorMessage));
            return resultDokok;
        }

        if (resultDokok.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error(String.Format("Keresesi hiba (external_link alapjan)! Nem adott vissza sorokat, pedig id alapjan kaptunk egyet!"));
            _ret.ErrorCode = "EXLK0001";
            _ret.ErrorMessage = String.Format("Keresesi hiba (external_link alapjan)! Nem adott vissza sorokat, pedig id alapjan kaptunk egyet!");
            return _ret;
        }

        #endregion

        #region Ha sps: checkout, atnevezes, csekin, verziok lekerdezese

        if (dokuRekord.External_Source == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Logger.Info("sps: checkout, atnevezes, csekin, verziok lekerdezese");

            //  aktualis verzio a legelso elem a resultban
            string[] parts = Convert.ToString(resultDokok.Ds.Tables[0].Rows[0]["External_Info"]).Split(';');

            String gepNev = parts[0];
            String rootSitePath = parts[1];
            String doktarSitePath = parts[2];
            String doktarDocLibPath = parts[3];
            String doktarFolderPath = parts[4];
            String filename = dokuRekord.FajlNev.ToString().Trim();
            String rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");

            //  csekout
            try
            {
                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.ItemCheckout(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , RemoveSlash(doktarFolderPath)
                        , filename);

                    XmlDocument resultDoc = new XmlDocument();
                    try
                    {
                        resultDoc.LoadXml(res);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Szerver oldali alairas: Hiba az ItemCheckout altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                        _ret.ErrorCode = "ItemCheckout";
                        _ret.ErrorMessage = "Szerver oldali alairas: Hiba az ItemCheckout f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                    }

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Szerver oldali alairas: Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                        return _ret;
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Szerver oldali alairas: Hiba a file checkoutkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("Szerver oldali alairas: A hiba oka az AxisFileUpload WSben van az SPSen!"));
                Result ret = new Result();
                ret.ErrorCode = "ALAItemCheckout";
                ret.ErrorMessage = String.Format("Szerver oldali alairas: Hiba a file checkout-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                return ret;
            }

            //  atnevezes

            Logger.Info("SPS atnevezes elott.");
            Logger.Info(String.Format("regi nev: {1} - kapott uj nev: {0}", ujNev, filename));
            String kiterjesztes = ujNev.Substring(ujNev.LastIndexOf("."));
            Logger.Info(String.Format("uj nev kiterjesztes: {0}", kiterjesztes));
            String updateNev = ujNev.Substring(0, ujNev.LastIndexOf("."));
            Logger.Info(String.Format("uj nev kiterjesztes: {0} - updateNev: {1}", kiterjesztes, updateNev));

            Contentum.eDocument.SharePoint.DocumentService ds = new Contentum.eDocument.SharePoint.DocumentService();

            Result renameResult = ds.Rename(gepNev
                , String.Format("{0}/{1}", RemoveSlash(rootSitePath), RemoveSlash(doktarSitePath))
                , RemoveSlash(doktarDocLibPath)
                , RemoveSlash(doktarFolderPath)
                , filename
                , updateNev);

            if (!String.IsNullOrEmpty(renameResult.ErrorCode))
            {
                Logger.Error(String.Format("Hiba az atnevezes soran! ErrCode: {0}\nMessage: {1}", renameResult.ErrorCode, renameResult.ErrorMessage));
                return renameResult;
            }

            //  csekin: overwrite!

            Logger.Info("SPS check in elott.");

            try
            {
                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.UndoCheckout(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , doktarFolderPath
                        , ujNev);

                    XmlDocument resultDoc = new XmlDocument();
                    try
                    {
                        resultDoc.LoadXml(res);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba az ItemCheckin altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                        _ret.ErrorCode = "ItemCheckin";
                        _ret.ErrorMessage = "Hiba az ItemCheckin f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                    }

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                        return _ret;
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Hiba a file checkinkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                Result ret = new Result();
                ret.ErrorCode = "ItemCheckin";
                ret.ErrorMessage = String.Format("Hiba a file checkin-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                return ret;
            }

            //  sps verziok lekerdezese

            //Logger.Info("SPS verziok lekerdezese.");

            //string itemSitePath2 = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
            //    + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
            //    + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);
            //string itemFullPath = ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath) + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath) + filename;

            //Logger.Info(String.Format("Csekkolt verzio lekerdezese ({0}).", itemFullPath));

            //Contentum.eDocument.SharePoint.VersionService vser = new VersionService();
            ////Result versionRet = vser.GetCurrentVersion(itemSitePath, itemFullPath);
            //Result versionRet = vser.GetCurrentVersion(itemSitePath2
            //    , RemoveSlash(doktarDocLibPath)
            //    , doktarFolderPath
            //    , filename);

            //if (!String.IsNullOrEmpty(versionRet.ErrorCode))
            //{
            //    Logger.Error("Elozo verzio lekerdezes hiba!");
            //    return versionRet;
            //}

            //String aktVersion = Convert.ToString(versionRet.Uid);
            String aktVersion = String.Empty;

            //Logger.Info(String.Format("aktVersion: {0}", aktVersion));

            //  verziokon vegigmegyunk, atnevezes az adatbazisban!

            Logger.Info("Adatbazisban atnevezesek elott.");

            KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();

            String ujFormatum = this.GetFormatum(execparam.Clone(), ujNev);

            int x = 0;
            while (x < resultDokok.Ds.Tables[0].Rows.Count)
            {
                Logger.Info(String.Format("dok leszedese. id: {0}", resultDokok.Ds.Tables[0].Rows[x]["Id"]));

                //  dok leszedese
                execp2 = execparam.Clone();
                execp2.Record_Id = Convert.ToString(resultDokok.Ds.Tables[0].Rows[x]["Id"]);

                resultDok = this.GetDoc(execp2);

                if (!String.IsNullOrEmpty(resultDok.ErrorCode))
                {
                    Logger.Error(String.Format("NAGY HIBA! EMBERI KORREKCIO SZUKSEGES!!!! (krtDokId: {0} - aktVersion: {1})", resultDokok.Ds.Tables[0].Rows[x]["Id"], aktVersion));
                    Logger.Error(String.Format("Dokumentum rekord SELECT atnevezeskor(id alapjan) hiba!\nErrCode: {0}\nErrMessage: {1}", resultDok.ErrorCode, resultDok.ErrorMessage));
                    _ret = new Result();
                    _ret.ErrorCode = "GIGAERR000002";
                    _ret.ErrorMessage = String.Format("Hiba az �tnevez�s sor�n (db atnevezes while - SELECT dok id alapjan)! Halad�ktalanul t�j�koztassa az �zemeltet�si oszt�lyt!\nKapott hibak�d: {0}\n{1}", resultDok.ErrorCode, resultDok.ErrorMessage);
                    return _ret;
                }

                //  update mezok

                dokuRekord = (KRT_Dokumentumok)resultDok.Record;

                dokuRekord.Updated.SetValueAll(false);
                dokuRekord.Base.Updated.SetValueAll(false);

                dokuRekord.Base.Updated.Ver = true;

                dokuRekord.External_Link = dokuRekord.External_Link.Replace(regiNev, ujNev);
                dokuRekord.Updated.External_Link = true;

                dokuRekord.External_Info = dokuRekord.External_Info.Replace(regiNev, ujNev);
                dokuRekord.Updated.External_Info = true;

                dokuRekord.FajlNev = ujNev.Trim();
                dokuRekord.Updated.FajlNev = true;

                dokuRekord.Leiras = originalUjNev;
                dokuRekord.Updated.Leiras = true;

                //dokuRekord.VerzioJel = aktVersion;
                //dokuRekord.Updated.VerzioJel = true;

                //
                // CR#2191 innen

                //if (!ujFormatum.Equals(dokuRekord.Formatum)) 
                //{
                //    dokuRekord.Formatum = ujFormatum;
                //    dokuRekord.Updated.Formatum = true;
                //}

                dokuRekord.Tipus = GetFormatum(execparam, ujNev);
                dokuRekord.Updated.Tipus = true;

                dokuRekord.Formatum = Path.GetExtension(ujNev).Trim(kiterjesztesVegeirol);
                dokuRekord.Updated.Formatum = true;

                //
                // CR#2191 idaig

                //  update vegrehajtasa

                Logger.Info(String.Format("dok update. id: {0} - dokuRekord.Base.Ver: {1}", resultDokok.Ds.Tables[0].Rows[x]["Id"], dokuRekord.Base.Ver));

                execp2 = execparam.Clone();
                execp2.Record_Id = Convert.ToString(resultDokok.Ds.Tables[0].Rows[x]["Id"]);

                Result updateDokResult = dokumentumService.Update(execp2, dokuRekord);

                if (!String.IsNullOrEmpty(updateDokResult.ErrorCode))
                {
                    Logger.Error(String.Format("NAGY HIBA! EMBERI KORREKCIO SZUKSEGES!!!! (krtDokId: {0} - aktVersion: {1})", resultDokok.Ds.Tables[0].Rows[x]["Id"], aktVersion));
                    Logger.Error(String.Format("Dokumentum rekord db UPDATE atnevezesnel(id alapjan) hiba!\nErrCode: {0}\nErrMessage: {1}", updateDokResult.ErrorCode, updateDokResult.ErrorMessage));
                    _ret = new Result();
                    _ret.ErrorCode = "GIGAERR000002";
                    _ret.ErrorMessage = String.Format("Hiba az �tnevez�s sor�n (db atnevezes while - UPDATE dok id alapjan)! Halad�ktalanul t�j�koztassa az �zemeltet�si oszt�lyt!\nKapott hibak�d: {0}\n{1}", updateDokResult.ErrorCode, updateDokResult.ErrorMessage);
                    return _ret;
                }


                x++;
            }
        }

        #endregion

        _ret = new Result();
        _ret.Record = ujNev;

        return _ret;
    }

    /// <summary>
    /// MOSS tartalomt�pus id alapj�n visszaadja egy xmlben a nem hidden filed-eket!
    /// Az eredm�ny xml string a Record mez�ben lesz.
    /// </summary>
    /// <param name="siteUrl">Site Url path.</param>
    /// <param name="cttId">ctt id</param>
    /// <returns>Result - Xml stringet ad vissza a Record mezoben.</returns>
    [WebMethod]
    public Result GetCTTFieldsById(String siteUrl, String cttId)
    {
        ContentTypeService cttS = new ContentTypeService();
        return cttS.GetCTTFieldsById(siteUrl, cttId);
    }

    /// <summary>
    /// A MOSS alapiktat�s ctt mez�in k�v�li mez�ket visszaad� f�ggv�ny. 
    /// Egy xml stringben adja vissza az eredm�nyt, amit a Record mezobe tesz.
    /// 
    /// Leszedi a CTThezt tartoz� �sszes mez�t, az alapikthez is, �s a k�t halmazb�l k�pez egy k�l�nbs�get.
    /// </summary>
    /// <param name="siteUrl">site Url</param>
    /// <param name="cttId">ctt Id</param>
    /// <returns>Result.Record mezoben xml string.</returns>
    [WebMethod]
    public Result GetCTTFieldsWithoutIktatasFieldsById(String siteUrl, string cttId)
    {
        Logger.Info(String.Format("GetCTTFieldsWithoutIktatasFieldsById indul. Parameterek: siteUrl: {0} ; cttId: {1}", siteUrl, cttId));

        Result _ret = new Result();

        #region Paramok ellenorzese

        if (String.IsNullOrEmpty(siteUrl))
        {
            Logger.Error("Parameterezesi hiba! Ures a siteUrl parameter!");
            _ret.ErrorCode = "PARAM0001";
            _ret.ErrorMessage = "Parameterezesi hiba! Ures a siteUrl parameter!";
            return _ret;
        }

        if (String.IsNullOrEmpty(cttId))
        {
            Logger.Error("Parameterezesi hiba! Ures a cttId parameter!");
            _ret.ErrorCode = "PARAM0002";
            _ret.ErrorMessage = "Parameterezesi hiba! Ures a cttId parameter!";
            return _ret;
        }

        #endregion

        #region Osszes mezo leszedese

        Logger.Info("Osszes mezo leszedese.");

        ContentTypeService cttS = new ContentTypeService();
        _ret = cttS.GetCTTFieldsById(siteUrl, cttId);

        if (!String.IsNullOrEmpty(_ret.ErrorCode))
        {
            Logger.Error(String.Format("Hibaval tert vissza a cttS.GetCTTFieldsById! Errc: {0}\nErrM: {1}", _ret.ErrorCode, _ret.ErrorMessage));
            return _ret;
        }

        XmlDocument xmlDocCttMezoi = new XmlDocument();

        try
        {
            xmlDocCttMezoi.LoadXml((string)_ret.Record);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a kapott xml eredmeny konvertalasakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            _ret = new Result();
            _ret.ErrorCode = "XMLC0001";
            _ret.ErrorMessage = String.Format("Hiba a kapott xml eredmeny konvertalasakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        #region Az ALAPIKT ctt idjenek leszedese.

        Logger.Info("Az ALAPIKT ctt idjenek leszedese.");

        String alapiktCttId = String.Empty;

        try
        {
            SPWebs.Webs webs = new SPWebs.Webs();
            webs.Url = String.Format("{0}_vti_bin/webs.asmx", (siteUrl.EndsWith("/")) ? siteUrl : siteUrl + "/");
            webs.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            Logger.Info(String.Format("web.url: {0}", webs.Url));

            XmlNode siteCtts = webs.GetContentTypes();

            XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
            xmlNsMan.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

            XmlNodeList xmlNlis = siteCtts.SelectNodes(String.Format("/a:ContentType[@Name='ALAPIKT']"), xmlNsMan);

            if (xmlNlis.Count == 0)
            {
                Logger.Error(String.Format("HIBA: Nem tal�lhato az ALAPIKT nevu CTT!!!"));
                _ret = new Result();
                _ret.ErrorCode = "CTTDB0001";
                _ret.ErrorMessage = "HIBA: Nem tal�lhato az ALAPIKT nevu CTT!!!";
                return _ret;
            }

            if (xmlNlis.Count > 1)
            {
                Logger.Error(String.Format("HIBA: T�l sok sort tal�lt az ALAPIKT nev alapj�n!!!"));
                _ret = new Result();
                _ret.ErrorCode = "CTTDB0002";
                _ret.ErrorMessage = "HIBA: T�l sok sort tal�lt az ALAPIKT nev alapj�n!!!";
                return _ret;
            }

            Logger.Info("Az ALAPIKT ctt id-jenek kivetele:");
            alapiktCttId = Convert.ToString(xmlNlis[0].Attributes["ID"].Value);
            Logger.Info(String.Format("id = {0}", alapiktCttId));

        }
        catch (SoapException spe)
        {
            Logger.Error("SoapException: " +
            String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
            Logger.Error("Kapott parameterek: "
               + " siteUrl = " + siteUrl + "\n"
               + " cttId = " + cttId);
            _ret = new Result();
            _ret.ErrorCode = "ALAPIKTCTTID001";
            _ret.ErrorMessage = String.Format("Remote exception az ALAPIKT ctt meg�llap�t�s�n�l!\nMessage: {0}\nStackTrace: {1}", spe.Message, spe.StackTrace);
            return _ret;
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1} ", ex.Message, ex.StackTrace));
            Logger.Error("Kapott parameterek: "
               + " siteUrl = " + siteUrl + "\n"
               + " cttId = " + cttId);
            _ret = new Result();
            _ret.ErrorCode = "ALAPIKTCTTID002";
            _ret.ErrorMessage = String.Format("Exception az ALAPIKT ctt meg�llap�t�s�n�l!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }


        #endregion

        #region Az ALAPIKT mezoinek leszedese

        Logger.Info("Az ALAPIKT mezoinek leszedese.");

        _ret = cttS.GetCTTFieldsById(siteUrl, alapiktCttId);

        if (!String.IsNullOrEmpty(_ret.ErrorCode))
        {
            Logger.Error(String.Format("Hibaval tert vissza a cttS.GetCTTFieldsById! (ALAPIKT) Errc: {0}\nErrM: {1}", _ret.ErrorCode, _ret.ErrorMessage));
            return _ret;
        }

        XmlDocument xmlDocAlapiktMezoi = new XmlDocument();

        try
        {
            xmlDocAlapiktMezoi.LoadXml((string)_ret.Record);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a kapott xml eredmeny (ALAPIKT) konvertalasakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            _ret = new Result();
            _ret.ErrorCode = "XMLC0001";
            _ret.ErrorMessage = String.Format("Hiba a kapott xml eredmeny (ALAPIKT) konvertalasakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        #region A ket mezohalmaz kulonbsegenek eloallitasa

        Logger.Info("A ket mezohalmaz kulonbsegenek eloallitasa");

        Logger.Info(String.Format("Ctt mezok: {0}", xmlDocCttMezoi.OuterXml.ToString()));
        Logger.Info(String.Format("ALAPIKT Ctt mezok: {0}", xmlDocAlapiktMezoi.OuterXml.ToString()));

        XmlDocument xmlEredmeny = new XmlDocument();

        XmlNode ndRoot = xmlEredmeny.CreateNode(XmlNodeType.Element, "Fields", "");

        try {
            foreach (XmlNode nd in xmlDocCttMezoi.SelectNodes("//Field"))
            {
                string fieldName = Convert.ToString(nd.Attributes["StaticName"].Value);
                bool alapbaIsMegvan = false;
                int i = 0;
                while (i < xmlDocAlapiktMezoi.SelectNodes("//Field").Count && !alapbaIsMegvan)
                {
                    XmlNode ndA = xmlDocAlapiktMezoi.SelectNodes("//Field")[i];

                    string fieldNameA = Convert.ToString(ndA.Attributes["StaticName"].Value);

                    if (fieldNameA.Equals(fieldName)) alapbaIsMegvan = true;

                    i++;
                }

                if (!alapbaIsMegvan)
                {
                    XmlNode ndUj = xmlEredmeny.CreateNode(XmlNodeType.Element, "Field", "");

                    for (int t = 0; t < nd.Attributes.Count; t++)
                    {
                        String attrName = nd.Attributes[t].Name;
                        XmlAttribute a = xmlEredmeny.CreateAttribute(attrName);
                        a.Value = Convert.ToString(nd.Attributes[attrName].Value);
                        ndUj.Attributes.Append(a);
                    }

                    ndRoot.AppendChild(ndUj);
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            _ret = new Result();
            _ret.ErrorCode = "MEZOHALM0001";
            _ret.ErrorMessage = String.Format("Exception a k�t mez�halmaz k�l�nbs�g�nek el��ll�t�sa k�zben! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        xmlEredmeny.AppendChild(ndRoot);

        _ret = new Result();
        _ret.Record = xmlEredmeny.OuterXml.ToString();

        #endregion

        return _ret;
    }

    /// <summary>
    /// A krt dok id alapj�n visszadja a MOSS verzi�kat megjelen�t� lap urljet (a Record mezoben).
    /// Ez a word addin egyik funkci�j�n�l haszn�latos: Ugr�s a verzi�kra.
    /// Az execparam.Record_id-je kell tartalmazza krtDokId-t!
    /// </summary>
    /// <param name="execparam">execparam</param>
    /// <returns>Result - kitoltve</returns>
    [WebMethod]
    public Result GetSpsItemVersionsUrl(ExecParam execparam)
    {
        Logger.Info(String.Format("GetSpsItemVersionsUrl indul."));

        Result _ret = new Result();

        #region krt_dok bejegyzes megszerzese

        Logger.Info("krt_dok bejegyzes megszerzese");

        if (String.IsNullOrEmpty(execparam.Record_Id))
        {
            Logger.Error("Parameterezesi hiba: Ures az execparam Record_id mezoje!");
            _ret.ErrorCode = "PARHI0001";
            _ret.ErrorMessage = "Param�ter hiba: �res a megadott ExecParam Record_Id mez�je!";
            return _ret;
        }

        _ret = this.GetDoc(execparam);

        if (!String.IsNullOrEmpty(_ret.ErrorCode))
        {
            Logger.Error(String.Format("Hib�val t�rt vissza a GedDoc!\nErrCode: {0}\nErrMsg: {1}", _ret.ErrorCode, _ret.ErrorMessage));
            return _ret;
        }

        KRT_Dokumentumok dok = (KRT_Dokumentumok)_ret.Record;

        if (String.IsNullOrEmpty(dok.External_Link))
        {
            Logger.Error("Ures a kapott krt_dok bejegyzes external link mezoje!");
            _ret = new Result();
            _ret.ErrorCode = "UEL00001";
            _ret.ErrorMessage = "Hiba! �res a kapott krt_dok bejegyz�s external_link mez�je, ez�rt nem hat�rozhat� meg a verzi�kat megjelen�t� url!";
            return _ret;
        }

        if (String.IsNullOrEmpty(dok.External_Id))
        {
            Logger.Error("Ures a kapott krt_dok bejegyzes external id mezoje!");
            _ret = new Result();
            _ret.ErrorCode = "UEL00002";
            _ret.ErrorMessage = "Hiba! �res a kapott krt_dok bejegyz�s external_id mez�je, ez�rt nem hat�rozhat� meg a verzi�kat megjelen�t� url!";
            return _ret;
        }

        #endregion

        #region a list id es a dokumentum (item) id megszerzese

        Logger.Info("a list id es a dokumentum (item) id megszerzese");

        string listId = String.Empty;
        string itemId = String.Empty;
        string[] externalInfoParts = dok.External_Info.Split(';');

        try
        {
            SPLists.Lists list = new SPLists.Lists();
            list.Url = String.Format("{0}{1}{2}/_vti_bin/lists.asmx", externalInfoParts[0], externalInfoParts[1], externalInfoParts[2]);
            list.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            Logger.Info(String.Format("A list sps ws url: {0}", list.Url));
            Logger.Info(String.Format("DocLib neve: {0}", externalInfoParts[3]));

            Logger.Info("list megszerzese: ws hivas elott.");

            XmlNode ndList = list.GetList(externalInfoParts[3]);

            Logger.Info("list megszerzese: ws hivas ok.");

            listId = Convert.ToString(ndList.Attributes["ID"].Value);

            Logger.Info(String.Format("list megszerzese: id: {0}", listId));

            Logger.Info("list items megszerzese: kezdodik - beallitasok");

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
            XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", ""); ;
            XmlNode ndQuery = null;

            ndQueryOptions.InnerXml = "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                "<DateInUtc>FALSE</DateInUtc>";

            ndViewFields.InnerXml = "<FieldRef Name='Id' /><FieldRef Name='Title' /><FieldRef Name='Name' /><FieldRef Name='Owner' /><FieldRef Name='BaseName' />";
            ndQueryOptions.InnerXml += "<Folder></Folder>";

            ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");
            ndQuery.InnerXml = "";
            ndQuery.InnerXml = String.Format("<Where><Eq><FieldRef Name=\"UniqueId\" /><Value Type=\"Text\">{0}</Value></Eq></Where>", "{" + dok.External_Id.ToUpper() + "}");

            Logger.Info(String.Format("list items megszerzese: ws hivas elott. query: {0}", ndQuery.OuterXml.ToString()));

            XmlNode ndListItems = list.GetListItems(externalInfoParts[3], null, ndQuery, ndViewFields, null, ndQueryOptions, null);

            Logger.Info(String.Format("list items megszerzese: ws hivas ok. eredmeny: {0}", ndListItems.OuterXml.ToString()));

            ListUtilService listUS = new ListUtilService();
            int liCount = listUS.GetItemCount(ndListItems);

            Logger.Info(String.Format("kapott eredmenysorok darabszama: {0}", liCount));

            if (liCount == 1)
            {
                itemId = listUS.GetItemOwsId(ndListItems);
                Logger.Info(String.Format("kapott ows_id: {0}", itemId));
            }
            else
            {
                Logger.Error("A kapott list item eredmenysorok szama nem 1!");
                _ret = new Result();
                _ret.ErrorCode = "XI00001";
                _ret.ErrorMessage = "Hiba! A kapott a sharepointos item-et query eredm�nysorainak sz�ma nem egy! Ez�rt a verzi�kra mutat� URL-t nem lehet meghat�rozni.";
                return _ret;
            }

            ////   .... item keres�se a xml rengetegben, majd ows_id kiv�tele
            //NameTable nt = new NameTable();
            //XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
            //nsManager.AddNamespace("s","uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
            //nsManager.AddNamespace("dt","uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
            //nsManager.AddNamespace("rs","urn:schemas-microsoft-com:rowset");
            //nsManager.AddNamespace("z","#RowsetSchema");
            //nsManager.AddNamespace("alap", "http://schemas.microsoft.com/sharepoint/soap/");

            //string keresettItem = String.Format("//z:row[contains(@ows_UniqueId,'{0}')]", dok.External_Id.ToUpper());

            //Logger.Info(String.Format("keresett item xpath: {0}", keresettItem));

            //XmlNodeList items = ndListItems.SelectNodes(keresettItem, nsManager);

            //Logger.Info(String.Format("keresett item xpath: kereses ok."));

            //if (items.Count == 0)
            //{
            //    Logger.Error("Nem sikerult meghatarozni az xml-ben a unique_id alapjan az item-et! Nem kaptunk sorokat.");
            //    _ret = new Result();
            //    _ret.ErrorCode = "XI00001";
            //    _ret.ErrorMessage = "Hiba! A kapott XMLben nem siker�lt megtal�lni a sharepointos item-et (id alapj�n nincs eredmenysor)! Ez�rt a verzi�kra mutat� URL-t nem lehet meghat�rozni.";
            //    return _ret;
            //}

            //if (items.Count > 1)
            //{
            //    Logger.Error("Nem sikerult meghatarozni az xml-ben a unique_id alapjan az item-et! Tul sok eredmenysor.");
            //    _ret = new Result();
            //    _ret.ErrorCode = "XI00002";
            //    _ret.ErrorMessage = "Hiba! A kapott XMLben nem siker�lt megtal�lni a sharepointos item-et (id alapj�n t�l sok eredm�nysor)! Ez�rt a verzi�kra mutat� URL-t nem lehet meghat�rozni.";
            //    return _ret;
            //}

            //Logger.Info("ows_id meghatarozasa elott.");

            //itemId = Convert.ToString(items[0].Attributes["ows_ID"].Value);

            //Logger.Info(String.Format("kapott ows_id: {0}", itemId));
        }
        catch (SoapException soae)
        {
            Logger.Error(String.Format("Soap exception!\n{0}\n{1}", soae.Message, soae.StackTrace));
            _ret = new Result();
            _ret.ErrorCode = "SOAPEXC0001";
            _ret.ErrorMessage = String.Format("Soap exception!\n{0}\n{1}", soae.Message, soae.StackTrace);
            return _ret;
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a futas soran!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            _ret = new Result();
            _ret.ErrorCode = "EXC0001";
            _ret.ErrorMessage = String.Format("Hiba a fut�s sor�n!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        #region az external_link alapjan az url osszeallitasa

        // http://fphtestnote-w/sites/edok/dokumentumtar/2008/IKTATOTTANYAGOK/FPH001/_layouts/Versions.aspx?list={AE6C2E9E-D6A9-4A08-8ABE-DFE68826C51B}&ID=15&FileName=%2Fsites%2Fedok%2Fdokumentumtar%2F2008%2FIKTATOTTANYAGOK%2FFPH001%2FOSSZEG%2F3%2Fosszeggel%5Fsablon%5F222%2Edocx

        string versionUrl = String.Format("{0}{1}{2}/_layouts/Versions.aspx?list={3}&ID={4}", externalInfoParts[0], externalInfoParts[1], externalInfoParts[2], listId, itemId);

        Logger.Info(String.Format("visszaadando url: {0}", versionUrl));

        _ret = new Result();
        _ret.Record = versionUrl;

        #endregion

        return _ret;
    }

    /// <summary>
    /// Tartalomt�pus l�t�nek ellen�rz�se MOSSban.
    /// A sharepointban meglelt CTT id-t adja vissza (a Record mezoben).
    /// Ha nem talalt, akkor �res.
    /// </summary>
    /// <param name="wordCttId">wordbol kiszedett CTT id</param>
    /// <returns>Result</returns>
    [WebMethod]
    public Result GetSPSSiteCttIdFromWordCttId(String wordCttId)
    {
        Logger.Info(String.Format("DocumentService.GetSPSSiteCttIdFromWordCttId indul."));

        Result _ret = new Result();
        try
        {
            ContentTypeService cttService = new ContentTypeService();
            String cttId = cttService.GetContentTypeByWordCTT(wordCttId);
            _ret.Record = String.Format("<result><cttid>{0}</cttid><cttname>{1}</cttname></result>", cttId, cttService.LatestContentTypeName);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            _ret.ErrorCode = "H00001";
            _ret.ErrorMessage = String.Format("Hiba a GetSPSSiteCttIdFromWordCttId elj�r�s futtat�sa sor�n!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }
        return _ret;
    }

    /// <summary>
    /// SHA1 csekszumma byte t�mbre.
    /// </summary>
    /// <param name="tartalom"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetSHA1CheckSum(byte[] tartalom)
    {
        Logger.Info(String.Format("DocumentService.GetSHA1CheckSum indul."));
        return Contentum.eDocument.SharePoint.Utility.CalculateSHA1(tartalom);
    }

    /// <summary>
    /// A felt�ltend� dokumentumr�l sz�l� megadott adatok alapj�n hiba�zeneteket (k�dokat) ad vissza, ellen�rz�seket hajt v�gre.
    /// A bementei param�ter: stringk�nt megadott XML adatok.
    /// A kimenet: ugyancsak xml, stringk�nt visszaadva a Record mezoben.
    /// A word addin haszn�lja.
    /// 
    /// param IN:
    /// kivalasztottUgyiratId, kivalasztottIratId, csatolmanyNeve, spsCttId, 
    /// docmetaCttId, docmetaVerzio, docmetaDokumentumId, docmetaIratId, ujirat
    /// kivalasztottUgyUgydarabId
    /// 
    /// param OUT:
    /// <result>
    ///   <hibak>
    ///     <hiba kod="xxx" szoveg="cccc" />
    ///   </hibak>
    ///   
    /// </result>
    /// 
    /// A k�d a word addinban haszn�latos k�dok.
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="strParamsInXml">XML string</param>
    /// <returns>Result. A record mezoben az eredmeny XML stringkent.</returns>
    [WebMethod]
    public Result CanUploadDocument(ExecParam execparam, String strParamsInXml)
    {
        Logger.Info(String.Format("DocumentService.CanUploadDocument indul. Kapott strParamsInXml: {0}", strParamsInXml));

        Result _ret = new Result();

        #region parameterek beolvasasa egy xmlDocba

        Logger.Info("--- parameterek beolvasasa egy xmlDocba");

        XmlDocument xmlParams = new XmlDocument();

        try
        {
            xmlParams.LoadXml(strParamsInXml);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a kapott string xml adatok parameter xmlDocca valo konvertalasakor!\nMessage: {0}\nStackTrace: {1}\nstrXmlParam: {2}", ex.Message, ex.StackTrace, strParamsInXml));
            _ret.ErrorCode = "PAR0000123";
            _ret.ErrorMessage = String.Format("Hiba a kapott xml string param�ter adatok XML dokumentumm� val� konvert�l�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        #region ugyiratok leszedese

        Logger.Info("--- ugyiratok leszedese");

        XmlDocument retDoc = new XmlDocument();

        XmlNode ndResult = retDoc.CreateNode(XmlNodeType.Element, "result", "");

        XmlNode ndHibak = retDoc.CreateNode(XmlNodeType.Element, "hibak", "");

        string ugyanolyanNevutTartalmazoIratId = String.Empty;

        Result kivalasztottUgyRes = null;
        Result docmetaUgyRes = null;


        #region kivalasztott ugyirat leszedese

        Logger.Info("--- kivalasztott ugyirat leszedese");

        if (!String.IsNullOrEmpty(GetParam("kivalasztottUgyiratId", xmlParams)))
        {

            Logger.Info("kivalasztott ugyirat leszedese");

            Contentum.eRecord.Service.ServiceFactory erecsf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());

            Contentum.eRecord.Service.EREC_UgyUgyiratokService ugyService = erecsf.GetEREC_UgyUgyiratokService();
            Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch ugySearch = new Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch();

            ugySearch.Id.Value = GetParam("kivalasztottUgyiratId", xmlParams);
            ugySearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            kivalasztottUgyRes = ugyService.GetAllWithExtension(execparam, ugySearch);

            if (!String.IsNullOrEmpty(kivalasztottUgyRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az ugyirat lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", kivalasztottUgyRes.ErrorCode, kivalasztottUgyRes.ErrorMessage));
                return kivalasztottUgyRes;
            }

            if (kivalasztottUgyRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Ures eredmenyhalmazzal t�rt vissza az (kivalasztott) ugyirat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0003";
                _ret.ErrorMessage = "�res eredm�nnyel t�rt vissza az (kivalasztott) �gyirat lek�rdez�s!";
                return _ret;
            }

            if (kivalasztottUgyRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok sorral t�rt vissza az (kivalasztott) ugyirat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0004";
                _ret.ErrorMessage = "T�l sok eredm�nnyel t�rt vissza az (kivalasztott) �gyirat lek�rdez�s!";
                return _ret;
            }
        }
        #endregion

        #region docmetabol jovo irat id alapjan megkeresett ugyirat leszedese

        if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
        {

            Logger.Info("docmetabol jovo irat id alapjan megkeresett ugyirat leszedese");

            Contentum.eRecord.Service.ServiceFactory erecsf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());

            Contentum.eRecord.Service.EREC_IraIratokService iratService = erecsf.GetEREC_IraIratokService();
            Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

            //  irat --------------------------------------------------------------------

            iratSearch.Id.Value = GetParam("docmetaIratId", xmlParams);
            iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratRes = iratService.GetAll(execparam, iratSearch);

            if (!String.IsNullOrEmpty(iratRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza a irat lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", iratRes.ErrorCode, iratRes.ErrorMessage));
                return iratRes;
            }

            if (iratRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Ures eredmenyhalmazzal t�rt vissza az irat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "IRAT0001";
                _ret.ErrorMessage = "�res eredm�nnyel t�rt vissza az irat lek�rdez�s!";
                return _ret;
            }

            if (iratRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok sorral t�rt vissza az irat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "IRAT0002";
                _ret.ErrorMessage = "T�l sok eredm�nnyel t�rt vissza az irat lek�rdez�s!";
                return _ret;
            }

            Logger.Info(String.Format("Kapott ugyirat id: {0}", Convert.ToString(iratRes.Ds.Tables[0].Rows[0]["UgyUgyIratDarab_Id"])));

            //  ugyirat darab --------------------------------------------------------------------

            Logger.Info("ugyirat darab lekerdezes indul.");

            Contentum.eRecord.Service.EREC_UgyUgyiratdarabokService ugyDarabService = erecsf.GetEREC_UgyUgyiratdarabokService();
            Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratdarabokSearch ugyDarabSearch = new Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratdarabokSearch();

            ugyDarabSearch.Id.Value = Convert.ToString(iratRes.Ds.Tables[0].Rows[0]["UgyUgyIratDarab_Id"]);
            ugyDarabSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result ugyDarabRes = ugyDarabService.GetAll(execparam, ugyDarabSearch);

            if (!String.IsNullOrEmpty(ugyDarabRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az ugyirat darab lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", ugyDarabRes.ErrorCode, ugyDarabRes.ErrorMessage));
                return ugyDarabRes;
            }

            if (ugyDarabRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Ures eredmenyhalmazzal t�rt vissza az (docmeta) ugyirat darab lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0001";
                _ret.ErrorMessage = "�res eredm�nnyel t�rt vissza az (docmeta) �gyirat darab lek�rdez�s!";
                return _ret;
            }

            if (ugyDarabRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok sorral t�rt vissza az (docmeta) ugyirat darab lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0002";
                _ret.ErrorMessage = "T�l sok eredm�nnyel t�rt vissza az (docmeta) �gyirat darab lek�rdez�s!";
                return _ret;

            }

            //  ugyirat --------------------------------------------------------------------

            Logger.Info("ugyirat lekerdezes indul.");

            Contentum.eRecord.Service.EREC_UgyUgyiratokService ugyService = erecsf.GetEREC_UgyUgyiratokService();
            Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch ugySearch = new Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch();

            ugySearch.Id.Value = Convert.ToString(ugyDarabRes.Ds.Tables[0].Rows[0]["UgyUgyirat_Id"]);
            ugySearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            docmetaUgyRes = ugyService.GetAllWithExtension(execparam, ugySearch);

            if (!String.IsNullOrEmpty(docmetaUgyRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az ugyirat lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", docmetaUgyRes.ErrorCode, docmetaUgyRes.ErrorMessage));
                return docmetaUgyRes;
            }

            if (docmetaUgyRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Ures eredmenyhalmazzal t�rt vissza az (docmeta) ugyirat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0003";
                _ret.ErrorMessage = "�res eredm�nnyel t�rt vissza az (docmeta) �gyirat lek�rdez�s!";
                return _ret;
            }

            if (kivalasztottUgyRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok sorral t�rt vissza az (docmeta) ugyirat lek�rdez�s!"));
                _ret = new Result();
                _ret.ErrorCode = "UGY0004";
                _ret.ErrorMessage = "T�l sok eredm�nnyel t�rt vissza az (docmeta) �gyirat lek�rdez�s!";
                return _ret;

            }
        }

        #endregion

        #endregion

        #region iratok leszedese

        Result kivalasztottIratRes = null;
        Result docmetaIratRes = null;

        #region kivalasztott irat leszedese

        if (!String.IsNullOrEmpty(GetParam("kivalasztottIratId", xmlParams)))
        {
            Logger.Info(String.Format("kivalasztottIratId alapjan irat select indul. {0}", GetParam("kivalasztottIratId", xmlParams)));

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_IraIratokService iratService = sf.GetEREC_IraIratokService();
            Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

            iratSearch.Id.Value = GetParam("kivalasztottIratId", xmlParams);
            iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            kivalasztottIratRes = iratService.GetAllWithExtension(execparam, iratSearch);

            if (!String.IsNullOrEmpty(kivalasztottIratRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az irat (kivalasztottIratId) lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", kivalasztottIratRes.ErrorCode, kivalasztottIratRes.ErrorMessage));
                return kivalasztottIratRes;
            }

            if (kivalasztottIratRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Nem adott vissza eredmenyt az irat select (kivalasztottIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00011";
                _ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat select (kivalasztottIratId)!");
                return _ret;
            }

            if (kivalasztottIratRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat select (kivalasztottIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00012";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat select (kivalasztottIratId)!");
                return _ret;
            }
        }

        #endregion

        #region docmeta irat leszedese

        if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
        {
            Logger.Info(String.Format("docmetaIratId alapjan irat select indul: ", String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)) ? "�res!" : GetParam("docmetaIratId", xmlParams)));

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_IraIratokService iratService = sf.GetEREC_IraIratokService();
            Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

            iratSearch.Id.Value = GetParam("docmetaIratId", xmlParams);
            iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            docmetaIratRes = iratService.GetAllWithExtension(execparam, iratSearch);

            if (!String.IsNullOrEmpty(docmetaIratRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az irat (docmetaIratId) lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", docmetaIratRes.ErrorCode, docmetaIratRes.ErrorMessage));
                return docmetaIratRes;
            }

            if (docmetaIratRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Nem adott vissza eredmenyt az irat select (docmetaIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00021";
                _ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat select (docmetaIratId)!");
                return _ret;
            }

            if (docmetaIratRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat select (docmetaIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00022";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat select (docmetaIratId)!");
                return _ret;
            }
        }

        #endregion

        #endregion

        #region ha munkapeldanyBeiktatasa igen, akkor ellenorizzuk, hogy kivalasztott irat munkaanyag-e

        if (GetParam("munkapeldanyBeiktatasa", xmlParams).Equals("IGEN"))
        {
            Logger.Info("munkapeldanyBeiktatasa: IGEN");

            if (kivalasztottIratRes == null)
            {
                Logger.Info("Hiba! A kivalasztott irat result null!");
                _ret.ErrorCode = "MPIKT0001";
                _ret.ErrorMessage = "Nincs kiv�lasztott irat!";
                return _ret;
            }

            if (!Convert.ToString(kivalasztottIratRes.Ds.Tables[0].Rows[0]["Allapot"]).Equals("0"))
            {
                Logger.Info("A kivalasztott irat nem munkaanyag es igy nem lehet iktatni. Ki kell venni az elesites pipat.");
                _ret.ErrorCode = "MPIKT0002";
                _ret.ErrorMessage = "A kiv�lasztott irat nem munkaanyag �s �gy nem lehet iktatni! Vegye ki a \"munkaanyag �les�t�se\" pip�t!";
                return _ret;
            }
        }

        #endregion

        #region ha nem egyezik a kivalasztott irat id es a dokumentumba levo, akkor megnezzuk van-e mar ilyen CTT a kivalasztott ugyben

        Logger.Info("ha nem egyezik a kivalasztott irat id es a dokumentumba levo, akkor megnezzuk van-e mar ilyen CTT a kivalasztott ugyben - resz kezdodik");

        //  csak uj eseten nezunk ennek utana
        if (GetParam("ujirat", xmlParams).Equals("IGEN"))
        {
            //  uressel nem hasonlitunk
            if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
            {

                if (!GetParam("docmetaIratId", xmlParams).Equals(GetParam("kivalasztottIratId", xmlParams)))
                {

                    if (!GetParam("spsCttId", xmlParams).Equals("ISMERETLEN") && !GetParam("spsCttId", xmlParams).Equals("0x0101") && !String.IsNullOrEmpty(GetParam("docmetaSablonAzn", xmlParams)))
                    {

                        Logger.Info("beestunk az ifbe, de nem csinalunk semmit");

                        //  TODO: Erre kell majd irni egy eljarast.

                        //Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
                        //Contentum.eRecord.Service.EREC_IraIratokDokumentumokService csatService = sf.GetEREC_IraIratokDokumentumokService();
                        //Contentum.eQuery.BusinessDocuments.EREC_IraIratokDokumentumokSearch csatSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokDokumentumokSearch();

                        //csatSearch.Manual_Ugyiratok_Id.Value = GetParam("kivalasztottUgyiratId", xmlParams);
                        //csatSearch.Manual_Ugyiratok_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                        //csatSearch.Manual_Dokumentum_Nev.Value = GetParam("csatolmanyNeve", xmlParams);
                        //csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                        ////  ha csatolva volt mar, akkor kiszurjuk azt az iratot, amihez eleve csatolva lett
                        //if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
                        //{
                        //    csatSearch.IraIrat_Id.Value = GetParam("docmetaIratId", xmlParams);
                        //    csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.notequals;
                        //}

                        //Result ugyanolyanNevucsatRes = csatService.GetAllWithExtension(execparam, csatSearch);

                        //if (!String.IsNullOrEmpty(ugyanolyanNevucsatRes.ErrorCode))
                        //{
                        //    Logger.Error(String.Format("Hib�val t�rt vissza a csatolm�nyok lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", ugyanolyanNevucsatRes.ErrorCode, ugyanolyanNevucsatRes.ErrorMessage));
                        //    return ugyanolyanNevucsatRes;
                        //}

                        //if (ugyanolyanNevucsatRes.Ds.Tables[0].Rows.Count == 0)
                        //{
                        //    //MessageBox.Show("nincs ilyen");
                        //    //  Akkor jo, nem csinalunk semmit 
                        //}
                        //else
                        //{
                        //    XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                        //    hiba.InnerXml = String.Format("<hiba kod=\"ujdok_azonosnevu\" szoveg=\"Van azonos nev� a kijel�lt �gyiratban!s\" />");
                        //    hibak.AppendChild(hiba);

                        //    ugyanolyanNevutTartalmazoIratId = Convert.ToString(ugyanolyanNevucsatRes.Ds.Tables[0].Rows[0]["IraIrat_Id"]);
                        //}

                    }
                }

            }

        } //if (GetParam("ujirat", xmlParams).Equals("IGEN"))

        #endregion

        #region kivalasztott ugyhoz tartozik-e egy ilyen nevu dokumentum

        Logger.Info("kivalasztott ugyhoz tartozik-e egy ilyen nevu dokumentum resz kezdodik");

        //  csak uj eseten nezunk ennek utana
        //if (GetParam("ujirat", xmlParams).Equals("IGEN"))
        //{

        bool nevVizsgalatKell = true;

        //  uressel nem hasonlitunk
        if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
        {

            if (!GetParam("docmetaIratId", xmlParams).Equals(GetParam("kivalasztottIratId", xmlParams)))
            {
                nevVizsgalatKell = false;
            }

        }

        if (nevVizsgalatKell)
        {
            Logger.Info("nevVizsgalatKell ifben vagyunk");

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatService = sf.GetEREC_CsatolmanyokService();
            Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

            csatSearch.Manual_Ugyiratok_Id.Value = GetParam("kivalasztottUgyiratId", xmlParams);
            csatSearch.Manual_Ugyiratok_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            csatSearch.Manual_Dokumentum_Nev.Value = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(GetParam("csatolmanyNeve", xmlParams));
            csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            //  ha csatolva volt mar, akkor kiszurjuk azt az iratot, amihez eleve csatolva lett
            //if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)))
            //{
            //    csatSearch.IraIrat_Id.Value = GetParam("docmetaIratId", xmlParams);
            //    csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.notequals;
            //}

            Result ugyanolyanNevucsatRes = csatService.GetAllWithExtension(execparam, csatSearch);

            if (!String.IsNullOrEmpty(ugyanolyanNevucsatRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza a csatolm�nyok lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", ugyanolyanNevucsatRes.ErrorCode, ugyanolyanNevucsatRes.ErrorMessage));
                return ugyanolyanNevucsatRes;
            }

            if (ugyanolyanNevucsatRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Info("nevVizsgalatKell: ugyanolyanNevucsatRes.Ds.Tables[0].Rows.Count == 0");

                //MessageBox.Show("nincs ilyen");
                //  Akkor jo, nem csinalunk semmit 
            }
            else
            {
                Logger.Info("nevVizsgalatKell: ugyanolyanNevucsatRes.Ds.Tables[0].Rows.Count == 0 ELSE ag!");

                XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                XmlAttribute a = retDoc.CreateAttribute("kod");
                a.Value = "ujdok_azonosnevu";
                hiba.Attributes.Append(a);
                a = retDoc.CreateAttribute("szoveg");
                a.Value = "Van azonos nev� a kijel�lt �gyiratban!";
                hiba.Attributes.Append(a);
                ndHibak.AppendChild(hiba);

                ugyanolyanNevutTartalmazoIratId = Convert.ToString(ugyanolyanNevucsatRes.Ds.Tables[0].Rows[0]["IraIrat_Id"]);
            }
        }


        //} //if (GetParam("ujirat", xmlParams).Equals("IGEN"))

        #endregion

        #region docmeta szerint ugyanahhoz az ugyhoz es ugyanahhoz az irathoz akarjak csatolni, amihez mar egyszer volt - sima csatolas ... stb.

        Logger.Info("docmeta szerint ugyanahhoz az ugyhoz es ugyanahhoz az irathoz akarjak csatolni, amihez mar egyszer volt - sima csatolas ... stb. resz kezdodik");

        bool kijeloltIratLekerdezese = false;

        //  csak akkor nezzuk ezeket, ha nem ujiratrol van szo!!!
        if (GetParam("ujirat", xmlParams).Equals("NEM"))
        {
            // ha nem ures a docmetaUgyRes, akkor tudunk hasonlitani ugyiratok es iratok kozott
            // ellenben csak az irat idket (ha vannak) tudjuk hasonlitani
            if (docmetaUgyRes != null)
            {
                string kivalasztottUgyiratId = Convert.ToString(kivalasztottUgyRes.Ds.Tables[0].Rows[0]["Id"]);
                string docmetaUgyiratId = Convert.ToString(docmetaUgyRes.Ds.Tables[0].Rows[0]["Id"]);

                //  mas ugyirat - mas irat
                if (!kivalasztottUgyiratId.Equals(docmetaUgyiratId))
                {
                    Logger.Info("if (!kivalasztottUgyiratId.Equals(docmetaUgyiratId))-ben vagyunk");

                    XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                    XmlAttribute a = retDoc.CreateAttribute("kod");
                    a.Value = "nemujdok_masugymasirat";
                    hiba.Attributes.Append(a);
                    a = retDoc.CreateAttribute("szoveg");
                    a.Value = "A dokumentum alabvet�en egy m�sik �gyirathoz volt csatolva!";
                    hiba.Attributes.Append(a);
                    //hiba.InnerXml = String.Format("<hiba kod=\"nemujdok_masugymasirat\" szoveg=\"A dokumentum alabvet�en egy m�sik �gyirathoz volt csatolva!\" />");
                    ndHibak.AppendChild(hiba);
                }
                else
                {
                    Logger.Info("if (!kivalasztottUgyiratId.Equals(docmetaUgyiratId)) ELSE agban vagyunk");

                    if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)) && !String.IsNullOrEmpty(GetParam("kivalasztottIratId", xmlParams)))
                    {
                        if (!GetParam("docmetaIratId", xmlParams).Equals(GetParam("kivalasztottIratId", xmlParams)))
                        {
                            Logger.Info("dupla if agban vagyunk");

                            XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                            XmlAttribute a = retDoc.CreateAttribute("kod");
                            a.Value = "nemujdok_uaugymasirat";
                            hiba.Attributes.Append(a);
                            a = retDoc.CreateAttribute("szoveg");
                            a.Value = "Ezen �gyirat egy m�sik irat�hoz m�r csatolva van ez a dokumentum!";
                            hiba.Attributes.Append(a);
                            //hiba.InnerXml = String.Format("<hiba kod=\"nemujdok_uaugymasirat\" szoveg=\"Ezen �gyirat egy m�sik irat�hoz m�r csatolva van ez a dokumentum!\" />");
                            ndHibak.AppendChild(hiba);
                        }
                        else
                        {
                            Logger.Info("dupla if ELSE agban vagyunk");

                            XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                            XmlAttribute a = retDoc.CreateAttribute("kod");
                            a.Value = "nemujdok_uaugyuairat";
                            hiba.Attributes.Append(a);
                            a = retDoc.CreateAttribute("szoveg");
                            a.Value = "A dokumentum egy �jabb verzi�ja fog l�trej�nni!";
                            hiba.Attributes.Append(a);
                            //hiba.InnerXml = String.Format("<hiba kod=\"nemujdok_uaugyuairat\" szoveg=\"A dokumentum egy �jabb verzi�ja fog l�trej�nni!\" />");
                            ndHibak.AppendChild(hiba);
                        }
                    }
                }
            }
            else
            {
                Logger.Info("kakadu zorba 1");

                if (!String.IsNullOrEmpty(GetParam("docmetaIratId", xmlParams)) && !String.IsNullOrEmpty(GetParam("kivalasztottIratId", xmlParams)))
                {
                    if (!GetParam("docmetaIratId", xmlParams).Equals(GetParam("kivalasztottIratId", xmlParams)))
                    {
                        Logger.Info("kakadu zorba 3");

                        XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                        XmlAttribute a = retDoc.CreateAttribute("kod");
                        a.Value = "nemujdok_uaugymasirat";
                        hiba.Attributes.Append(a);
                        a = retDoc.CreateAttribute("szoveg");
                        a.Value = "Ezen �gyirat egy m�sik irat�hoz m�r csatolva van ez a dokumentum!";
                        hiba.Attributes.Append(a);
                        //hiba.InnerXml = String.Format("<hiba kod=\"nemujdok_uaugymasirat\" szoveg=\"Ezen �gyirat egy m�sik irat�hoz m�r csatolva van ez a dokumentum!\" />");
                        ndHibak.AppendChild(hiba);
                    }
                    else
                    {
                        Logger.Info("kakadu zorba 223133");

                        XmlNode hiba = retDoc.CreateNode(XmlNodeType.Element, "hiba", "");
                        XmlAttribute a = retDoc.CreateAttribute("kod");
                        a.Value = "nemujdok_uaugyuairat";
                        hiba.Attributes.Append(a);
                        a = retDoc.CreateAttribute("szoveg");
                        a.Value = "A dokumentum egy �jabb verzi�ja fog l�trej�nni!";
                        hiba.Attributes.Append(a);
                        //hiba.InnerXml = String.Format("<hiba kod=\"nemujdok_uaugyuairat\" szoveg=\"A dokumentum egy �jabb verzi�ja fog l�trej�nni!\" />");
                        ndHibak.AppendChild(hiba);
                    }
                }
            }
        }

        #endregion

        #region visszaadando eredmeny xml osszeallitasa

        Logger.Info("visszaadando eredmeny xml osszeallitasa resz kezdodik");

        //  hibak xmlbe

        ndResult.AppendChild(ndHibak);

        //  ha volt ugyanolyan neu irat, akkor az adatai xmlbe

        if (!String.IsNullOrEmpty(ugyanolyanNevutTartalmazoIratId))
        {
            Logger.Info(String.Format("ugyanolyanNevutTartalmazoIratId alapjan irat leszedese indul. id: {0}", ugyanolyanNevutTartalmazoIratId));

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_IraIratokService iratService = sf.GetEREC_IraIratokService();
            Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

            iratSearch.Id.Value = ugyanolyanNevutTartalmazoIratId;
            iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratRes = iratService.GetAllWithExtension(execparam, iratSearch);

            if (!String.IsNullOrEmpty(iratRes.ErrorCode))
            {
                Logger.Error(String.Format("Hib�val t�rt vissza az irat (ugyanolyanNevutTartalmazoIratId) lek�rdez�s!\nErrorCode: {0}\nErrorMessage: {1}", iratRes.ErrorCode, iratRes.ErrorMessage));
                return iratRes;
            }

            if (iratRes.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("Nem adott vissza eredmenyt az irat select (ugyanolyanNevutTartalmazoIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00001";
                _ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat select (ugyanolyanNevutTartalmazoIratId)!");
                return _ret;
            }

            if (iratRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat select (ugyanolyanNevutTartalmazoIratId)!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATSEL00002";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat select (ugyanolyanNevutTartalmazoIratId)!");
                return _ret;
            }

            XmlNode ndKivalasztottUgy = retDoc.CreateNode(XmlNodeType.Element, "ugyanolyanNevutTartalmazoIrat", "");

            foreach (DataColumn dc in iratRes.Ds.Tables[0].Columns)
            {
                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                a.Value = Convert.ToString(iratRes.Ds.Tables[0].Rows[0][dc.ColumnName]);
                ndKivalasztottUgy.Attributes.Append(a);
            }

            ndResult.AppendChild(ndKivalasztottUgy);

        }

        //  kivalasztott irat adatok xmlbe

        if (kivalasztottIratRes != null)
        {
            Logger.Info("kivalasztottIratRes alapjan irat adatok xmlbe.");

            XmlNode ndKivalasztottUgy = retDoc.CreateNode(XmlNodeType.Element, "kivalasztottIrat", "");

            foreach (DataColumn dc in kivalasztottIratRes.Ds.Tables[0].Columns)
            {
                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                a.Value = Convert.ToString(kivalasztottIratRes.Ds.Tables[0].Rows[0][dc.ColumnName]);
                ndKivalasztottUgy.Attributes.Append(a);
            }

            ndResult.AppendChild(ndKivalasztottUgy);

        }

        //  docmeta irat adatok xmlbe

        if (docmetaIratRes != null)
        {
            Logger.Info("docmetaIratRes alapjan irat adatok xmlbe.");

            XmlNode ndKivalasztottUgy = retDoc.CreateNode(XmlNodeType.Element, "docmetaIrat", "");

            foreach (DataColumn dc in docmetaIratRes.Ds.Tables[0].Columns)
            {
                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                a.Value = Convert.ToString(docmetaIratRes.Ds.Tables[0].Rows[0][dc.ColumnName]);
                ndKivalasztottUgy.Attributes.Append(a);
            }

            ndResult.AppendChild(ndKivalasztottUgy);

        }

        //  kivalasztott ugyirat xmlbe

        if (kivalasztottUgyRes != null)
        {
            XmlNode ndKivalasztottUgy = retDoc.CreateNode(XmlNodeType.Element, "kivalasztottUgy", "");

            foreach (DataColumn dc in kivalasztottUgyRes.Ds.Tables[0].Columns)
            {
                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                a.Value = Convert.ToString(kivalasztottUgyRes.Ds.Tables[0].Rows[0][dc.ColumnName]);
                ndKivalasztottUgy.Attributes.Append(a);
            }

            ndResult.AppendChild(ndKivalasztottUgy);
        }

        //  docmeta ugy adatai xmlbe

        if (docmetaUgyRes != null)
        {
            XmlNode ndDocmetaUgy = retDoc.CreateNode(XmlNodeType.Element, "docmetaUgy", "");

            foreach (DataColumn dc in docmetaUgyRes.Ds.Tables[0].Columns)
            {
                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                a.Value = Convert.ToString(docmetaUgyRes.Ds.Tables[0].Rows[0][dc.ColumnName]);
                ndDocmetaUgy.Attributes.Append(a);
            }

            ndResult.AppendChild(ndDocmetaUgy);
        }

        retDoc.AppendChild(ndResult);

        Logger.Info(String.Format("Visszaadando xml str: {0}", retDoc.OuterXml.ToString()));

        _ret.Record = retDoc.OuterXml.ToString();

        #endregion

        return _ret;
    }

    /// <summary>
    /// Ez k�l�n keres�s, csak hashre. Az�rt, mert wordben azut�n csin�lunk hashsz�mol�st tartalomra, miut�n
    /// minden adat (vonalkod, iktszam) be lett �rva. Ment�s-lez�r�s el�tt.
    /// Amikor a sima CanUploadDocument-et hivjuk, akkor meg nincs meg ez a hash sz�mol�s.
    /// 
    /// A visszaadott XML <ugyiratok></ugyiratok> form�ban megy.
    /// 
    /// Xml param�ter string:
    /// <params><hashkod>hask�d</hashkod><userid>felhaszn�l� GUID</userid><alkalmazasid>alkalmaz�s GUID</alkalmazasid></params>
    /// 
    /// </summary>
    /// <param name="execparam">V�grehajt�</param>
    /// <param name="strParamsInXml">Param�terek XMLben.</param>
    /// <returns>Result - az eredm�ny XML a Record mez�ben stringk�nt.</returns>
    /// 
    [WebMethod]
    public Result CanUploadDocumentHash(ExecParam execparam, String strParamsInXml)
    {
        Logger.Info(String.Format("DocumentService.CanUploadDocumentHash indul. Kapott strParamsInXml: {0}", strParamsInXml));

        Result _ret = new Result();

        XmlDocument retDoc = new XmlDocument();

        //XmlNode ndResult = retDoc.CreateNode(XmlNodeType.Element, "result", "");
        XmlNode ndUgyiratok = retDoc.CreateNode(XmlNodeType.Element, "ugyiratok", "");


        #region parameterek beolvasasa egy xmlDocba

        Logger.Info("--- parameterek beolvasasa egy xmlDocba");

        XmlDocument xmlParams = new XmlDocument();

        try
        {
            xmlParams.LoadXml(strParamsInXml);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a kapott string xml adatok parameter xmlDocca valo konvertalasakor!\nstrXmlParam: {0}", strParamsInXml), ex);
            _ret.ErrorCode = "PAR0000123";
            _ret.ErrorMessage = String.Format("Hiba a kapott xml string param�ter adatok XML dokumentumm� val� konvert�l�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        //  valamiert itt soha nem kaptuk meg az execparam-ot, mindig null jott at :(
        //  ezert az xmlparamba kajuk meg a juzer es alkalmazas id-t

        #region execparam

        //Logger.Info("--- execparam");

        //if (execparam == null)
        //{
        //    Logger.Info("Null execparam!");

        //    if (String.IsNullOrEmpty(GetParam("userid", xmlParams)))
        //    {
        //        Logger.Error("Hianyzo parameter! Nincs meg a userid az xmlben!");
        //        _ret.ErrorCode = "CUDHP00001";
        //        _ret.ErrorMessage = "Hianyzo parameter! Nincs meg a userid az xmlben!";
        //        return _ret;
        //    }

        //    if (String.IsNullOrEmpty(GetParam("alkalmazasid", xmlParams)))
        //    {
        //        Logger.Error("Hianyzo parameter! Nincs meg az alkalmazasid az xmlben!");
        //        _ret.ErrorCode = "CUDHP00002";
        //        _ret.ErrorMessage = "Hianyzo parameter! Nincs meg az alkalmazasid az xmlben!";
        //        return _ret;
        //    }

        //    if (String.IsNullOrEmpty(GetParam("szervezetid", xmlParams)))
        //    {
        //        Logger.Error("Hianyzo parameter! Nincs meg az szervezetid az xmlben!");
        //        _ret.ErrorCode = "CUDHP00002";
        //        _ret.ErrorMessage = "Hianyzo parameter! Nincs meg az szervezetid az xmlben!";
        //        return _ret;
        //    }

        //    Logger.Info("execparam letrehozasa");

        //    execparam = new ExecParam();
        //    execparam.Felhasznalo_Id = GetParam("userid", xmlParams); //"0F38FB99-4CC9-4C31-9A03-7AE0E8FD3CA5"; // "EE3F3ECB-8BAA-446D-9F1D-00A580B4F719";
        //    execparam.Alkalmazas_Id = GetParam("alkalmazasid", xmlParams); //"272277BB-27B7-4689-BE45-DB5994A0FBA9";
        //    execparam.FelhasznaloSzervezet_Id = GetParam("szervezetid", xmlParams);
        //}

        #endregion

        bool isKuldemenyRelevant = (GetParam("getkuldemeny", xmlParams) == Boolean.TrueString); // �rkeztetett k�ldem�nyek
        bool isDokumentumRelevant = (GetParam("getdokumentum", xmlParams) == Boolean.TrueString); // csak t�rolt dokumentumok

        string hashKod = GetParam("hashkod", xmlParams);

        if (String.IsNullOrEmpty(hashKod))
        {
            _ret.ErrorCode = "CUDH0001";
            _ret.ErrorMessage = "Nincs megadva hash!";
            return _ret;
        }

        #region lekerdezes hashre

        Logger.Info("--- lekerdezes hashre");

        //KRT_DokumentumokService dokService = new KRT_DokumentumokService();
        //KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();

        Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
        Contentum.eRecord.Service.EREC_CsatolmanyokService csatService = sf.GetEREC_CsatolmanyokService();

        Result resultCsatolmanyok = null;
        Result resultDokumentumok = null;
        List<string> arr_DokumentumIdk = null;
        if (isDokumentumRelevant)
        {
            KRT_DokumentumokService dokService = new KRT_DokumentumokService();
            KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();

            dokSearch.TartalomHash.Value = hashKod;
            dokSearch.TartalomHash.Operator = Contentum.eQuery.Query.Operators.equals;
            dokSearch.TartalomHash.Group = "100";
            dokSearch.TartalomHash.GroupOperator = Contentum.eQuery.Query.Operators.or;

            dokSearch.KivonatHash.Value = hashKod;
            dokSearch.KivonatHash.Operator = Contentum.eQuery.Query.Operators.equals;
            dokSearch.KivonatHash.Group = "100";
            dokSearch.KivonatHash.GroupOperator = Contentum.eQuery.Query.Operators.or;

            resultDokumentumok = dokService.GetAll(execparam, dokSearch);

            if (resultDokumentumok.IsError)
            {
                Logger.Error("Hibaval tert vissza a dokumentumok lekerdezes!", execparam, resultDokumentumok);
                return resultDokumentumok;
            }

            Logger.Info(String.Format("azonoshashdb: {0}", resultDokumentumok.Ds.Tables[0].Rows.Count));

            if (resultDokumentumok.Ds.Tables[0].Rows.Count > 0)
            {
                arr_DokumentumIdk = new List<string>(resultDokumentumok.Ds.Tables[0].Rows.Count);

                foreach (DataRow row in resultDokumentumok.Ds.Tables[0].Rows)
                {
                    string dokumentumId = row["Id"].ToString();
                    if (!String.IsNullOrEmpty(dokumentumId) && !arr_DokumentumIdk.Contains(dokumentumId))
                    {
                        arr_DokumentumIdk.Add(dokumentumId);
                    }
                }

                EREC_CsatolmanyokSearch csatolmanySearch = new EREC_CsatolmanyokSearch();
                csatolmanySearch.Dokumentum_Id.Value = Search.GetSqlInnerString(arr_DokumentumIdk.ToArray());
                csatolmanySearch.Dokumentum_Id.Operator = Contentum.eQuery.Query.Operators.inner;

                resultCsatolmanyok = csatService.GetAll(execparam, csatolmanySearch);
            }
        }
        else
        {
            EREC_CsatolmanyokSearch csatolmanySearch = new EREC_CsatolmanyokSearch();

            csatolmanySearch.Manual_Dokumentum_TartalomHash.Value = hashKod;
            csatolmanySearch.Manual_Dokumentum_TartalomHash.Operator = Contentum.eQuery.Query.Operators.equals;
            csatolmanySearch.Manual_Dokumentum_TartalomHash.Group = "100";
            csatolmanySearch.Manual_Dokumentum_TartalomHash.GroupOperator = Contentum.eQuery.Query.Operators.or;

            csatolmanySearch.Manual_Dokumentum_KivonatHash.Value = hashKod;
            csatolmanySearch.Manual_Dokumentum_KivonatHash.Operator = Contentum.eQuery.Query.Operators.equals;
            csatolmanySearch.Manual_Dokumentum_KivonatHash.Group = "100";
            csatolmanySearch.Manual_Dokumentum_KivonatHash.GroupOperator = Contentum.eQuery.Query.Operators.or;

            resultCsatolmanyok = csatService.GetAllWithExtension(execparam, csatolmanySearch);
        }

        //Result resultCsatolmanyok = csatService.GetAllWithExtension(execparam, csatolmanySearch);

        //XmlNode ndHashDb = retDoc.CreateNode(XmlNodeType.Element, "azonoshashdb", "");
        //ndHashDb.InnerText = Convert.ToString(_ret.Ds.Tables[0].Rows.Count);

        //ndResult.AppendChild(ndHashDb);

        Logger.Info("lekerdezes hashre rendben lement");

        #endregion

        #region a kapcsolodo ugyiratok - iratok lekerdezese es xmlbeeeee

        Logger.Info("--- a kapcsolodo ugyiratok - iratok lekerdezese es xmlbeeeee;");
        try
        {
            if (resultCsatolmanyok != null)
            {
                if (resultCsatolmanyok.IsError)
                {
                    Logger.Error("Hibaval tert vissza a csatolmanyok lekerdezes!", execparam, resultCsatolmanyok);
                    return resultCsatolmanyok;
                }
                Logger.Info(String.Format("azonoshashdb csatolm�nyok: {0}", resultCsatolmanyok.Ds.Tables[0].Rows.Count));

                if (resultCsatolmanyok.Ds.Tables[0].Rows.Count > 0)
                {

                    List<string> arr_IratIdk = new List<string>(resultCsatolmanyok.Ds.Tables[0].Rows.Count);
                    List<string> arr_KuldemenyIdk = new List<string>(resultCsatolmanyok.Ds.Tables[0].Rows.Count);
                    List<string> arr_UgyiratIdk = new List<string>(resultCsatolmanyok.Ds.Tables[0].Rows.Count);

                    #region csatolmanyokbol iratId-k osszeszedese

                    Logger.Info("--- csatolmanyokbol iratId-k osszeszedese");

                    foreach (DataRow row in resultCsatolmanyok.Ds.Tables[0].Rows)
                    {
                        string iratId = row["IraIrat_Id"].ToString();

                        if (!String.IsNullOrEmpty(iratId) && !arr_IratIdk.Contains(iratId))
                        {
                            arr_IratIdk.Add(iratId);
                        }

                        if (isKuldemenyRelevant || isDokumentumRelevant)
                        {
                            string kuldemenyId = row["KuldKuldemeny_Id"].ToString();

                            if (isKuldemenyRelevant)
                            {
                                if (!String.IsNullOrEmpty(kuldemenyId) && !arr_KuldemenyIdk.Contains(kuldemenyId))
                                {
                                    arr_KuldemenyIdk.Add(kuldemenyId);
                                }
                            }

                            if (isDokumentumRelevant)
                            {
                                string dokumentumId = row["Dokumentum_Id"].ToString();
                                if (arr_DokumentumIdk.Contains(dokumentumId))
                                {
                                    arr_DokumentumIdk.Remove(dokumentumId);
                                }
                            }
                        }
                    }

                    #endregion

                    #region k�ldem�nyek, ha kell

                    Result resultKuldemenyek = null;
                    if (isKuldemenyRelevant)
                    {
                        Logger.Info("--- k�ldem�nyek  ---");
                        Logger.Info(String.Format("arr_KuldemenyIdk.Count: {0}", arr_KuldemenyIdk.Count));
                        if (arr_KuldemenyIdk.Count > 0)
                        {
                            Contentum.eRecord.Service.EREC_KuldKuldemenyekService kuldemenyService = sf.GetEREC_KuldKuldemenyekService();
                            EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();

                            kuldemenySearch.Id.Value = Search.GetSqlInnerString(arr_KuldemenyIdk.ToArray());
                            kuldemenySearch.Id.Operator = Contentum.eQuery.Query.Operators.inner;

                            Logger.Info("GetAll inditasa.");

                            resultKuldemenyek = kuldemenyService.GetAll(execparam, kuldemenySearch);

                            if (resultKuldemenyek.IsError)
                            {
                                Logger.Error("Hib�val t�rt vissza a k�ldem�ny lek�rdez�s!", execparam, resultKuldemenyek);
                                return resultKuldemenyek;
                            }
                        }
                    }
                    #endregion

                    #region iratok extension - csatext resultbol - distinct ugyirat_id-re

                    Logger.Info("--- iratok extension - csatext resultbol - distinct ugyirat_id-re");

                    Contentum.eRecord.Service.EREC_IraIratokService iratService = sf.GetEREC_IraIratokService();
                    //Contentum.eRecord.Service.EREC_UgyUgyiratdarabokService ugyDarabokService = sf.GetEREC_UgyUgyiratdarabokService();
                    Contentum.eRecord.Service.EREC_UgyUgyiratokService ugyiratokService = sf.GetEREC_UgyUgyiratokService();

                    Logger.Info(String.Format("arr_IratIdk.Count: {0}", arr_IratIdk.Count)); //---- uj 2008.09.20

                    Result resultIratok = null;

                    if (arr_IratIdk.Count > 0)
                    {
                        Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch iratSearch = new Contentum.eQuery.BusinessDocuments.EREC_IraIratokSearch();

                        iratSearch.Id.Value = Search.GetSqlInnerString(arr_IratIdk.ToArray());
                        iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.inner;

                        Logger.Info("GetAllWithExtension inditasa."); //---- uj 2008.09.20 // index torolve: 2010.07.23

                        resultIratok = iratService.GetAllWithExtension(execparam, iratSearch);

                        if (resultIratok.IsError)
                        {
                            Logger.Error("Hib�val t�rt vissza az irat lek�rdez�s!", execparam, resultIratok);
                            return resultIratok;
                        }

                        Logger.Info("iratService.GetAllWithExtension lefutott.");
                        Logger.Info(String.Format("resultIratok tables count: {0}", resultIratok.Ds.Tables.Count));
                        Logger.Info(String.Format("resultIratok tables[0].rows count: {0}", resultIratok.Ds.Tables[0].Rows.Count));

                        // TODO: ellen�rz�s, minden irat visszaj�tt-e

                        foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
                        {
                            string ugyiratId = row["UgyiratId"].ToString();
                            if (!String.IsNullOrEmpty(ugyiratId) && !arr_UgyiratIdk.Contains(ugyiratId))
                            {
                                arr_UgyiratIdk.Add(ugyiratId);
                            }
                        }
                    } // if (arr_iratIdk.Count > 0)

                    #endregion

                    #region ugyirat lekerdezes

                    Logger.Info(String.Format("--- ugyirat lekerdezes, arr_UgyiratIdk.Count: {0}", arr_UgyiratIdk.Count));

                    if (arr_UgyiratIdk.Count > 0)
                    {
                        Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch ugySearch = new Contentum.eQuery.BusinessDocuments.EREC_UgyUgyiratokSearch();

                        ugySearch.Id.Value = Search.GetSqlInnerString(arr_UgyiratIdk.ToArray());
                        ugySearch.Id.Operator = Contentum.eQuery.Query.Operators.inner;

                        Result resultUgyiratok = ugyiratokService.GetAllWithExtension(execparam, ugySearch);

                        if (resultUgyiratok.IsError)
                        {
                            Logger.Error("Hib�val t�rt vissza az �gyirat lek�rdez�s!", execparam, resultUgyiratok);
                            return resultUgyiratok;
                        }

                        Logger.Info(String.Format("kapott eredmeny: resultUgyiratok.Ds.Tables: {0} - resultUgyiratok.Ds.Tables.Rows: {1}", resultUgyiratok.Ds.Tables.Count, resultUgyiratok.Ds.Tables[0].Rows.Count));

                        foreach (DataRow row in resultUgyiratok.Ds.Tables[0].Rows)
                        {
                            XmlNode ugyiratXmlNode = retDoc.CreateNode(XmlNodeType.Element, "ugyirat", null);

                            foreach (DataColumn dc in resultUgyiratok.Ds.Tables[0].Columns)
                            {
                                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                a.Value = Convert.ToString(row[dc.ColumnName]);
                                ugyiratXmlNode.Attributes.Append(a);
                            }

                            DataRow[] iratRows = resultIratok.Ds.Tables[0].Select(String.Format("UgyiratId='{0}'", row["Id"].ToString()));
                            foreach (DataRow iratRow in iratRows)
                            {
                                XmlNode iratXmlNode = retDoc.CreateNode(XmlNodeType.Element, "irat", null);
                                foreach (DataColumn dc in resultIratok.Ds.Tables[0].Columns)
                                {
                                    XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                    a.Value = Convert.ToString(iratRow[dc.ColumnName]);
                                    iratXmlNode.Attributes.Append(a);
                                }

                                DataRow[] csatolmanyRows = resultCsatolmanyok.Ds.Tables[0].Select(String.Format("IraIrat_Id='{0}'", iratRow["Id"].ToString()));
                                foreach (DataRow csatolmanyRow in csatolmanyRows)
                                {
                                    if (isKuldemenyRelevant)
                                    {
                                        string kuldemenyId = csatolmanyRow["KuldKuldemeny_Id"].ToString();
                                        if (!String.IsNullOrEmpty(kuldemenyId) && arr_KuldemenyIdk.Contains(kuldemenyId))
                                        {
                                            DataRow[] kuldemenyRows = resultKuldemenyek.Ds.Tables[0].Select(String.Format("Id='{0}'", kuldemenyId));
                                            if (kuldemenyRows.Length > 0)
                                            {
                                                // csak egyszer adjuk hozz�
                                                arr_KuldemenyIdk.Remove(kuldemenyId);

                                                foreach (DataRow kuldemenyRow in kuldemenyRows)
                                                {
                                                    XmlNode kuldemenyXmlNode = retDoc.CreateNode(XmlNodeType.Element, "kuldemeny", null);
                                                    foreach (DataColumn dc in resultKuldemenyek.Ds.Tables[0].Columns)
                                                    {
                                                        XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                                        a.Value = Convert.ToString(kuldemenyRow[dc.ColumnName]);
                                                        kuldemenyXmlNode.Attributes.Append(a);
                                                    }

                                                    iratXmlNode.AppendChild(kuldemenyXmlNode);
                                                }
                                            }
                                        }
                                    }


                                    XmlNode csatolmanyXmlNode = retDoc.CreateNode(XmlNodeType.Element, "csatolmany", null);
                                    foreach (DataColumn dc in resultCsatolmanyok.Ds.Tables[0].Columns)
                                    {
                                        XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                        a.Value = Convert.ToString(csatolmanyRow[dc.ColumnName]);
                                        csatolmanyXmlNode.Attributes.Append(a);
                                    }

                                    iratXmlNode.AppendChild(csatolmanyXmlNode);
                                }

                                ugyiratXmlNode.AppendChild(iratXmlNode);
                            }
                            ndUgyiratok.AppendChild(ugyiratXmlNode);
                        }
                    }
                    #endregion ugyirat lekerdezes

                    #region k�ldem�nyek feldolgoz�sa, ha kell

                    if (isKuldemenyRelevant)
                    {
                        Logger.Info("--- kuldemenyek  ---");
                        Logger.Info(String.Format("arr_KuldemenyIdk.Count: {0}", arr_KuldemenyIdk.Count));
                        if (arr_KuldemenyIdk.Count > 0)
                        {
                            XmlNode ndKuldemenyek = retDoc.CreateNode(XmlNodeType.Element, "kuldemenyek", "");

                            DataRow[] kuldemenyRows = resultKuldemenyek.Ds.Tables[0].Select(String.Format("Convert(Id, 'System.String') IN ('{0}')", String.Join("','", arr_KuldemenyIdk.ToArray())));
                            Logger.Info(String.Format("kuldemenyRows.Length: {0}", kuldemenyRows.Length));
                            foreach (DataRow kuldemenyRow in kuldemenyRows)
                            {
                                XmlNode kuldemenyXmlNode = retDoc.CreateNode(XmlNodeType.Element, "kuldemeny", null);
                                foreach (DataColumn dc in resultKuldemenyek.Ds.Tables[0].Columns)
                                {
                                    XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                    a.Value = Convert.ToString(kuldemenyRow[dc.ColumnName]);
                                    kuldemenyXmlNode.Attributes.Append(a);
                                }

                                ndKuldemenyek.AppendChild(kuldemenyXmlNode);
                            }

                            ndUgyiratok.AppendChild(ndKuldemenyek);
                        }
                    }
                    #endregion k�ldem�nyek feldolgoz�sa, ha kell
                }
            }

            #region dokumentumok feldolgoz�sa, ha kell

            if (isDokumentumRelevant && arr_DokumentumIdk != null) // ha nem tal�ltunk dokumentumok, akkor null
            {
                Logger.Info("--- dokumentumok  ---");
                Logger.Info(String.Format("arr_DokumentumIdk.Count: {0}", arr_DokumentumIdk.Count));
                if (arr_DokumentumIdk.Count > 0)
                {
                    XmlNode ndDokumentumok = retDoc.CreateNode(XmlNodeType.Element, "dokumentumok", "");

                    foreach (DataRow dokumentumRow in resultDokumentumok.Ds.Tables[0].Rows)
                    {
                        string dokumentumId = dokumentumRow["Id"].ToString();

                        if (arr_DokumentumIdk.Contains(dokumentumId))
                        {
                            XmlNode dokumentumXmlNode = retDoc.CreateNode(XmlNodeType.Element, "dokumentum", null);
                            foreach (DataColumn dc in resultDokumentumok.Ds.Tables[0].Columns)
                            {
                                XmlAttribute a = retDoc.CreateAttribute(dc.ColumnName.ToLower());
                                a.Value = Convert.ToString(dokumentumRow[dc.ColumnName]);
                                dokumentumXmlNode.Attributes.Append(a);
                            }

                            ndDokumentumok.AppendChild(dokumentumXmlNode);
                        }
                    }

                    ndUgyiratok.AppendChild(ndDokumentumok);
                }
            }
            #endregion dokumentumok feldolgoz�sa, ha kell

            #region eredmeny osszeallitasa

            Logger.Info("eredmeny osszeallitasa");

            //retDoc.AppendChild(ndResult);
            retDoc.AppendChild(ndUgyiratok);

            _ret.Record = retDoc.OuterXml;
            Logger.Info(String.Format("ezt adjuk vissza: {0}", retDoc.OuterXml));
            //_ret.Record = String.Format("<result><azonoshashdb>{0}</azonoshashdb></result>", _ret.Ds.Tables[0].Rows.Count);

            #endregion
        }
        catch (Exception ex)
        {
            Logger.Error("Exception! a kapcsolodo ugyiratok - iratok lekerdezese es xmlbeeeee reszben!", ex);
            _ret.ErrorCode = "MAGICHIBA1";
            _ret.ErrorMessage = String.Format("Hiba a tartalom hash alap� ellen�rz�sekor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            return _ret;
        }

        #endregion

        return _ret;
    }


    const string defaultContentTypes = @"<ContentTypes xmlns=""http://schemas.microsoft.com/sharepoint/soap/"">
	<ContentType Name=""ALAPFELT"" ID=""0x0101003685BE14D0DA486792C1E617240A9722"" Description=""�j dokumentum l�trehoz�sa."" Group=""Contentum"" NewDocumentControl="""" Scope=""http://contentumsptest/sites/2017"" Version=""1"" RequireClientRenderingOnNew=""TRUE""/>
	<ContentType Name=""ALAPIKT"" ID=""0x0101003685BE14D0DA486792C1E617240A972201"" Description=""�j dokumentum l�trehoz�sa."" Group=""Contentum"" NewDocumentControl="""" Scope=""http://contentumsptest/sites/2017"" Version=""1"" RequireClientRenderingOnNew=""TRUE""/>
</ContentTypes>";

    /// <summary>
    /// A MOSSb�l kiveszi a content type-okat es a Record mez�ben visszaadja.
    /// </summary>
    /// <returns>Result: a Record mezoben XML stringkent .</returns>
    /// 
    [WebMethod]
    public Result GetContentTypesFromSps()
    {
        Logger.Info(String.Format("DocumentService.GetContentTypesFromSps indul."));
        Result result = new Result();

        Logger.Info(String.Format("eDocumentStoreType=", UI.GetAppSetting("eDocumentStoreType")));

        if (Contentum.eUtility.Constants.DocumentStoreType.SharePoint.Equals(UI.GetAppSetting("eDocumentStoreType"), StringComparison.InvariantCultureIgnoreCase))
        {
            try
            {
                SPWebs.Webs web = new SPWebs.Webs();
                web.Url = String.Format("{0}{1}{2}_vti_bin/webs.asmx", Contentum.eDocument.SharePoint.Utility.GetSharePointUrl(), Contentum.eDocument.SharePoint.Utility.SharePointUrlPostfixRootSiteUrl(), DateTime.Today.ToString("yyyy") + "/");
                Logger.Info(String.Format("web url: {0}", web.Url));
                web.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                XmlNode ndResult = web.GetContentTypes();

                result.Record = ndResult.OuterXml.ToString();
                Logger.Info(String.Format("Kapott eredmeny: {0}", ndResult.OuterXml));
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba a GetContentTypesFromSps futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                result = new Result();
                result.ErrorCode = "GCTSERR0001";
                result.ErrorMessage = String.Format("Hiba a GetContentTypesFromSps futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            }
        }
        else
        {
            Logger.Info("Default Content Type-ok visszaadasa.");
            result.Record = defaultContentTypes;
            Logger.Info(String.Format("Kapott eredmeny: {0}", result.Record));
        }

        return result;
    }

    /// <summary>
    /// Visszaadja MOSSb�ol a megadott content type id-ju CTT-t.
    /// </summary>
    /// <param name="contentTypeId">Content Type Id.</param>
    /// <returns>Result. A Record mez�ben az XML stringk�nt.</returns>
    /// 
    [WebMethod]
    public Result GetContentTypeFromSps(String contentTypeId)
    {
        Logger.Info(String.Format("DocumentService.GetContentTypeFromSps indul. - contentTypeId: {0}", contentTypeId));
        Result result = new Result();

        try
        {
            SPWebs.Webs web = new SPWebs.Webs();
            web.Url = String.Format("{0}{1}{2}_vti_bin/webs.asmx", Contentum.eDocument.SharePoint.Utility.GetSharePointUrl(), Contentum.eDocument.SharePoint.Utility.SharePointUrlPostfixRootSiteUrl(), DateTime.Today.ToString("yyyy") + "/");
            Logger.Info(String.Format("web url: {0}", web.Url));
            web.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            XmlNode ndResult = web.GetContentType(contentTypeId);

            result.Record = ndResult.OuterXml.ToString();
            Logger.Info(String.Format("Kapott eredmeny: {0}", ndResult.OuterXml));
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a GetContentTypeFromSps futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            result = new Result();
            result.ErrorCode = "GCTERR0001";
            result.ErrorMessage = String.Format("Hiba a GetContentTypeFromSps futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
        }

        return result;
    }

    /// <summary>
    /// Csatolm�ny linekel�se  - tranzakcioval.
    /// Egy megadott csatolm�ny k�t�se (dupla felt�lt�s n�lk�l) egy irathoz.
    /// Word addinb�l h�vott elj�r�s.
    /// 
    /// Kapott param�terek:
    /// <params> 
    ///    <kivalasztottUgyiratId>GUID</kivalasztottUgyiratId>      <!-- a kiv�lasztott �gy idje -->
    ///    <kivalasztottIratId>GUID</kivalasztottIratId>            <!-- a kiv�lasztott irat idje -->
    ///    <documentumId>GUID</documentumId>                        <!-- a linkelni k�v�nt krt_dok idje -->
    ///    <documentumVerzio>String</documentumVerzio>              <!-- a linkelni k�v�nt dokumentum verzi�ja -->
    /// </params>
    /// 
    /// Objektumok megl�t�nek ellen�rz�se.
    /// Az irathoz hozz� van-e rendelve a linkelni k�v�nt dokumentum veriz� vagy m�sik verzi�ja... mert az hiba.
    /// A linkel�s.
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="xmlStrParams">Param�terek stringk�nt xmlben.</param>
    /// <returns>Result - �res vagy hiba van benne.</returns>
    /// 
    [WebMethod]
    public Result CsatolmanyLinkelese(ExecParam execparam, String xmlStrParams)
    {
        Logger.Info(String.Format("------------- CsatolmanyLinkelese -------------"));
        Result result = new Result();

        #region Parameter XML string feldolgozasa.

        XmlDocument xmlDoc = new XmlDocument();

        if (xmlStrParams != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlDoc.LoadXml(xmlStrParams);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, xmlStrParams));
                result.ErrorCode = "XMLPAR0001";
                result.ErrorMessage = String.Format("Hiba az XML param�ter string �talak�t�sakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return result;
            }
        }

        #endregion

        #region Parameterezes ellenorzese

        //string sourceSharePath = GetParam("sourceSharePath", xmlDoc);
        //string ujirat = GetParam("ujirat", xmlDoc);
        //string kivalasztottIratId = GetParam("kivalasztottIratId", xmlDoc);

        ////  felhasznalo id, store type es file name megadasa kotelezo
        //if (execparam.Felhasznalo_Id == null ||
        //    String.IsNullOrEmpty(documentStoreType) ||
        //    String.IsNullOrEmpty(filename)
        //    )
        //{
        //    string strErrorInfo = "";
        //    strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id -" : "";
        //    strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType -" : "";
        //    strErrorInfo += String.IsNullOrEmpty(filename) ? " filename -" : "";

        //    Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

        //    return ResultError.CreateNewResultWithErrorCode(201001);
        //}

        ////  nem lehet ures a tartalom es a sourcePath egyszerre
        //if (cont == null &&
        //    String.IsNullOrEmpty(sourceSharePath)
        //    )
        //{
        //    string strErrorInfo = "";
        //    strErrorInfo += (cont == null) ? " execparam.Felhasznalo_Id -" : "";
        //    strErrorInfo += String.IsNullOrEmpty(sourceSharePath) ? " documentStoreType -" : "";

        //    Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

        //    return ResultError.CreateNewResultWithErrorCode(201002);
        //}

        //if (ujirat.Equals("NEM") && String.IsNullOrEmpty(kivalasztottIratId))
        //{
        //    Logger.Error(String.Format("Ha nem uj irat van (ujirat==NEM), meg kell adni mindenkeppen a kivalasztott irat idjet!"));
        //    Logger.Error(String.Format("Hianyzo parameter! -> kivalasztottIratId"));
        //    return ResultError.CreateNewResultWithErrorCode(201003);
        //}

        #endregion

        //  ---------------------------------------------------------------------

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            /// -----------------------------------------------------------------------------------------------
            result = _CsatolmanyLinkelese(execparam
                , xmlDoc
                );

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    /// <summary>
    /// Csatolmany linekelese. 
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="xmlParams">Parameterek. :)</param>
    /// <returns>Result - ures vagy hiba van benne.</returns>
    private Result _CsatolmanyLinkelese(ExecParam execparam, XmlDocument xmlParams)
    {
        Logger.Info(String.Format("_CsatolmanyLinkelese indul. xmlParams: {0}", xmlParams.OuterXml.ToString()));

        Result result = new Result();

        #region kapott objektum id-k alapjan az objektumok letenek ellenorzese

        Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());

        //
        //  kivalasztott ugyirat

        Logger.Info(String.Format("kivalasztott ugyirat leszedese indul."));

        Contentum.eRecord.Service.EREC_UgyUgyiratokService serviceUgy = sf.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch searchUgy = new EREC_UgyUgyiratokSearch();

        searchUgy.Id.Value = GetParam("kivalasztottUgyiratId", xmlParams);
        searchUgy.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result resultUgy = serviceUgy.GetAllWithExtensionAndJogosultak(execparam, searchUgy, true);

        if (!String.IsNullOrEmpty(resultUgy.ErrorCode))
        {
            Logger.Error(String.Format("Hiba az ugy lekeredezese soran! ErrCode: {0}\nErrMsg: {1}", resultUgy.ErrorCode, resultUgy.ErrorMessage));
            return resultUgy;
        }

        Logger.Info(String.Format("UGYIRAT - kapott eredmenysorok szama: {0}", resultUgy.Ds.Tables[0].Rows.Count));

        if (resultUgy.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error("Kivalasztott ugyirat keresese: id alapjan nem talahato!");
            result.ErrorCode = "UGY00001";
            result.ErrorMessage = "Nincs meg a kiv�lasztott �gyirat az adatb�zisban!";
            return result;
        }

        if (resultUgy.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Error("Kivalasztott ugyirat keresese: id alapjan tul sok sort adott vissza a select!");
            result.ErrorCode = "UGY00002";
            result.ErrorMessage = "T�l sok adatsort adott vissza az �gyirat lek�rdez�s!";
            return result;
        }

        //
        //  kivalasztott irat

        Logger.Info(String.Format("kivalasztott irat leszedese indul."));

        Contentum.eRecord.Service.EREC_IraIratokService serviceIrat = sf.GetEREC_IraIratokService();
        EREC_IraIratokSearch searchIrat = new EREC_IraIratokSearch();

        searchIrat.Id.Value = GetParam("kivalasztottIratId", xmlParams);
        searchIrat.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result resultIrat = serviceIrat.GetAllWithExtension(execparam, searchIrat);

        if (!String.IsNullOrEmpty(resultIrat.ErrorCode))
        {
            Logger.Error(String.Format("Hiba az irat lekeredezese soran! ErrCode: {0}\nErrMsg: {1}", resultIrat.ErrorCode, resultIrat.ErrorMessage));
            return resultIrat;
        }

        Logger.Info(String.Format("IRAT - kapott eredmenysorok szama: {0}", resultIrat.Ds.Tables[0].Rows.Count));

        if (resultIrat.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error("Kivalasztott irat keresese: id alapjan nem talahato!");
            result.ErrorCode = "IRA00001";
            result.ErrorMessage = "Nincs meg a kiv�lasztott irat az adatb�zisban!";
            return result;
        }

        if (resultIrat.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Error("Kivalasztott irat keresese: id alapjan tul sok sort adott vissza a select!");
            result.ErrorCode = "IRA00002";
            result.ErrorMessage = "T�l sok adatsort adott vissza az irat lek�rdez�s!";
            return result;
        }

        //
        //  dokumentum (id-re kerdezunk es verziora is ellenorzunk)

        Logger.Info(String.Format("dokumentum leszedese indul."));

        KRT_DokumentumokService serviceDok = new KRT_DokumentumokService();
        KRT_DokumentumokSearch searchDok = new KRT_DokumentumokSearch();

        searchDok.Id.Value = GetParam("documentumId", xmlParams);
        searchDok.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        searchDok.VerzioJel.Value = GetParam("documentumVerzio", xmlParams);
        searchDok.VerzioJel.Operator = Contentum.eQuery.Query.Operators.equals;

        Result resultDok = serviceDok.GetAll(execparam, searchDok);

        if (!String.IsNullOrEmpty(resultDok.ErrorCode))
        {
            Logger.Error(String.Format("Hiba az irat lekeredezese soran! ErrCode: {0}\nErrMsg: {1}", resultDok.ErrorCode, resultDok.ErrorMessage));
            return resultDok;
        }

        Logger.Info(String.Format("DOK - kapott eredmenysorok szama: {0}", resultDok.Ds.Tables[0].Rows.Count));

        if (resultDok.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error("Kivalasztott dokumentum keresese: id alapjan nem talahato!");
            result.ErrorCode = "DOK00001";
            result.ErrorMessage = "Nincs meg a kiv�lasztott dokumentum az adatb�zisban!";
            return result;
        }

        if (resultDok.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Error("Kivalasztott dokumentum keresese: id alapjan tul sok sort adott vissza a select!");
            result.ErrorCode = "DOK00002";
            result.ErrorMessage = "T�l sok adatsort adott vissza a dokumentum lek�rdez�s!";
            return result;
        }


        #endregion

        #region irathoz hozza van-e mar esetleg ez a verzio rendelve

        Logger.Info("irathoz hozza van-e mar esetleg ez a verzio rendelve?");

        Contentum.eRecord.Service.EREC_CsatolmanyokService serviceCsat = sf.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch searchCsat = new EREC_CsatolmanyokSearch();

        searchCsat.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
        searchCsat.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        searchCsat.Dokumentum_Id.Value = GetParam("documentumId", xmlParams);
        searchCsat.Dokumentum_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result resultCsat = serviceCsat.GetAllWithExtension(execparam, searchCsat);

        if (!String.IsNullOrEmpty(resultCsat.ErrorCode))
        {
            Logger.Error(String.Format("Hiba az csatolmany lekeredezese soran! ErrCode: {0}\nErrMsg: {1}", resultCsat.ErrorCode, resultCsat.ErrorMessage));
            return resultCsat;
        }

        if (resultCsat.Ds.Tables[0].Rows.Count != 0)
        {
            Logger.Error("Kivalasztott irathoz hozza van rendelve mar a csatolmany!");
            result.ErrorCode = "CSATLINK0001";
            result.ErrorMessage = "A kiv�lasztott irathoz hozz� van m�r csatolva ez a dokumentum!";
            return result;
        }

        #endregion

        #region irathoz hozza van-e mar esetleg ugyanennek a dokumentumnak egy masik verzioja rendelve

        Logger.Info("irat csatolmanyai lekerdezes; utana external_id-re vizsgalat");

        searchCsat = new EREC_CsatolmanyokSearch();

        searchCsat.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
        searchCsat.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        //  vegigmegyunk az irat csatolmanyain s ha megtalaljuk a dokumentum external id-jet, akkor
        //  egy masik verziot mar linkeltek hozza

        resultCsat = serviceCsat.GetAllWithExtension(execparam, searchCsat);

        if (!String.IsNullOrEmpty(resultCsat.ErrorCode))
        {
            Logger.Error(String.Format("(ITT1) Hiba az irat csatolmany lekeredezese soran! ErrCode: {0}\nErrMsg: {1}", resultCsat.ErrorCode, resultCsat.ErrorMessage));
            return resultCsat;
        }

        int x = 0;
        while (x < resultCsat.Ds.Tables[0].Rows.Count)
        {
            if (Convert.ToString(resultCsat.Ds.Tables[0].Rows[x]["External_Id"]).Equals(Convert.ToString(resultDok.Ds.Tables[0].Rows[0]["External_id"])))
            {
                Logger.Error("Kivalasztott irathoz hozza van rendelve mar a csatolmany egy masik verzioja!");
                result.ErrorCode = "CSATLINK0002";
                result.ErrorMessage = "A kiv�lasztott irathoz hozz� van m�r csatolva ennek a dokumentumnak egy m�sik verzi�ja!";
                return result;
            }
        }


        #endregion

        #region a link kotes megvalosulasa

        Logger.Info("Csatolm�ny hozz�k�t�se az irathoz");

        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

        erec_Csatolmanyok.IraIrat_Id = GetParam("kivalasztottIratId", xmlParams);
        erec_Csatolmanyok.Updated.IraIrat_Id = true;

        erec_Csatolmanyok.Dokumentum_Id = GetParam("documentumId", xmlParams);
        erec_Csatolmanyok.Updated.Dokumentum_Id = true;

        ExecParam execParam_iratDok = execparam.Clone();
        Result result_iratDokInsert = serviceCsat.Insert(execParam_iratDok, erec_Csatolmanyok);
        if (!String.IsNullOrEmpty(result_iratDokInsert.ErrorCode))
        {
            Logger.Error(String.Format("Csatolmany kotese irathoz hiba! ErrCode: {0}\nErrMessage: {1}", result_iratDokInsert.ErrorCode, result_iratDokInsert.ErrorMessage));
            return result_iratDokInsert;
        }

        #endregion

        return result;
    }

    /// <summary>
    /// Egy string (xml) file visszanyerese spsbol url alapj�n.
    /// </summary>
    /// <param name="xmlFileUrl">Full url.</param>
    /// <returns>Result - ha hiba volt, akkor err mezok; egyebkent az eredmeny string a Record mezoben</returns>
    /// 
    [WebMethod]
    public Result GetXmlFileFromSPS(String xmlFileUrl)
    {
        Logger.Info(String.Format("DocumentService.GetXmlFileFromSPS indul. - xmlFileUrl: {0}", xmlFileUrl));

        Result result = new Result();

        String xmlFileTartalom = String.Empty;

        try
        {
            System.Net.WebClient client = new System.Net.WebClient();
            client.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            xmlFileTartalom = client.DownloadString(xmlFileUrl);

            Logger.Info(String.Format("Leiro XML file megszerzese SPSbol OK."));

            result.Record = xmlFileTartalom;
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("GetXmlFileFromSPS: Hiba a file ({0}) spsbol valo megszerzesekor!", xmlFileUrl));
            Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            result.ErrorCode = "HIBA00001";
            result.ErrorMessage = String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
        }

        return result;
    }

    /// <summary>
    /// Direkt feloltes. Meg csak sharepointba kesz.
    /// </summary>
    /// <param name="execparam">Execparam.</param>
    /// <param name="documentStoreType">tarolas tipus: Contentum.eUtility.Rendszerparameterek.Get(execParam_upload, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)</param>
    /// <param name="doktarSitePath">site eleresi ut</param>
    /// <param name="doktarDocLibPath">docLib neve</param>
    /// <param name="doktarFolderPath">folder path vagy ures</param>
    /// <param name="filename">file neve</param>
    /// <param name="cont">tartalom; byte tomb</param>
    /// <param name="checkInMode">(0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)</param>
    /// <returns>Result - az Uid-ban a feltoltott MOSS itemId-vel j�n vissza. Vagy hiba.</returns>
    public Result _DirectUploadWithCheckin(ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode
        )
    {
        Logger.Info("DocumentService.DirectUploadWithCheckin ");

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result result = new Result();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            string rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");


            // TODO: ide kell ellenorzes, h honnan hivjuk, ne lehessen barhonnan! SECHOLE!

            #region A felepitett struktura ellenorzese es letrehozasa a TARHELY-en

            Logger.Info("SharePoint: tarhely felepitese indul.");

            Result storeMappaGuid = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, String.Empty);

            if (!String.IsNullOrEmpty(storeMappaGuid.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a tarhely letenek ellenorzesekor! storeMappaGuid.Error: {0} - {1}", storeMappaGuid.ErrorCode, storeMappaGuid.ErrorMessage));
                return storeMappaGuid;
            }

            #endregion

            #region checkout ha kell

            Contentum.eDocument.SharePoint.DocumentService spsDs = new Contentum.eDocument.SharePoint.DocumentService();

            #region van-e ilyen a cel helye spsben ellenorzes

            Logger.Info("ellenorzes, h van-e mar olyan file a cel helyen -> ha igen checkout kell... resz kezdodik");

            bool vanUgyanolyanFileACelHelyen = false;

            Contentum.eDocument.SharePoint.ListUtilService ds = new Contentum.eDocument.SharePoint.ListUtilService();
            Result retSpsbenVaneIlyenFile = ds.GetListItemsByItemNameAppendWithPath(RemoveSlash(doktarSitePath)
                , RemoveSlash(doktarDocLibPath)
                , RemoveSlash(doktarFolderPath)
                , filename);

            if (!String.IsNullOrEmpty(retSpsbenVaneIlyenFile.ErrorCode))
            {
                Logger.Error(String.Format("DirectUploadWithCheckin: Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                return retSpsbenVaneIlyenFile;
            }

            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.LoadXml(Convert.ToString(retSpsbenVaneIlyenFile.Record));
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("DirectUploadWithCheckin: Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                Result retXmlError = new Result();
                retXmlError.ErrorCode = "DIRECTUPLXMLP001";
                retXmlError.ErrorMessage = String.Format("{0}\n {1}", exc.Message, exc.StackTrace);
                return retXmlError;
            }

            Logger.Info(String.Format("DirectUploadWithCheckin: GetListItemsByItemName adott xml eredmeny: {0}", xmlDoc.OuterXml.ToString()));

            int dbTalalt = ds.GetItemCount(xmlDoc.FirstChild);

            Logger.Info(String.Format("spsben nev alapjan talalt db: {0}", dbTalalt));

            if (dbTalalt > 0)
            {
                vanUgyanolyanFileACelHelyen = true;
            }

            #endregion

            Logger.Info("Csekaut, ha letezik mar...");
            if (vanUgyanolyanFileACelHelyen)
            {

                try
                {
                    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                    {
                        string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                            + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                            + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                        String res = upld3.ItemCheckout(itemSitePath
                            , RemoveSlash(doktarDocLibPath)
                            , doktarFolderPath
                            , filename);

                        XmlDocument resultDoc = new XmlDocument();
                        try
                        {
                            resultDoc.LoadXml(res);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(String.Format("Hiba az ItemCheckout altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                            result.ErrorCode = "ItemCheckout";
                            result.ErrorMessage = "Hiba az ItemCheckout f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                        }

                        if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                        {
                            result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                            result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                            Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                            return result;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("Hiba a file checkoutkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                    Result ret = new Result();
                    ret.ErrorCode = "ItemCheckout";
                    ret.ErrorMessage = String.Format("Hiba a file checkout-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                    return ret;
                }

            }

            #endregion

            #region feloltes
            //  TODO:
            //NetworkCredential uploadCred = new NetworkCredential(GetSharePointUserName(), GetSharePointPassword(), GetSharePointUserDomain());
            //NetworkCredential uploadCred = new NetworkCredential(

            try
            {
                result = spsDs.Upload(RemoveSlash(doktarSitePath)
                    , RemoveSlash(doktarDocLibPath)
                    , RemoveSlash(doktarFolderPath)
                    , filename
                    , cont);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Feloltes hiba!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                result = new Result();
                result.ErrorCode = "DUPLD00001";
                result.ErrorMessage = String.Format("Feloltes hiba!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return result;
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("Sharepoint-ba file feltoltese hiba: " + result.ErrorCode + "; " + result.ErrorMessage);
                return result;
            }

            // feloltott item id
            Logger.Info(String.Format("A feltoltott item id: {0}", result.Uid));

            String fullItemUrl = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath)
                + ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath)
                + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath)
                + filename;

            Logger.Info(String.Format("fullItemUrl: {0}", fullItemUrl));

            result.Record = fullItemUrl;

            #endregion

            #region checkin

            //Contentum.eDocument.SharePoint.FolderService fos = new Contentum.eDocument.SharePoint.FolderService();

            //Logger.Info(String.Format("Sharepoint-ba feltoltott file checkin. (Mode: {0})", checkInMode));
            //Result checkinRet = spsDs.CheckIn(RemoveSlash(doktarSitePath), RemoveSlash(doktarDocLibPath), RemoveSlash(doktarFolderPath), filename, String.Empty, checkInMode); //Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn);
            //if (!String.IsNullOrEmpty(checkinRet.ErrorCode))
            //{
            //    /// -2130245361 = nincs be�ll�tva a folderen a verzio kezeles beallitva
            //    if (!checkinRet.ErrorCode.Equals("-2130245361"))
            //    {
            //        Logger.Error("Sharepoint-ba feltoltott file checkin hiba: " + checkinRet.ErrorCode + "; " + checkinRet.ErrorMessage);
            //        return checkinRet;
            //    }
            //    else
            //    {
            //        Logger.Info("Sharepoint-ba feltoltott file checkin hiba: Nincs beallitva a verziokezeles a folderen!");
            //    }
            //}

            try
            {
                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.ItemCheckin(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , RemoveSlash(doktarFolderPath)
                        , filename
                        , checkInMode
                        , String.Empty);

                    XmlDocument resultDoc = new XmlDocument();
                    try
                    {
                        resultDoc.LoadXml(res);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba az ItemCheckin altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                        result.ErrorCode = "ItemCheckin";
                        result.ErrorMessage = "Hiba az ItemCheckin f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                        return result;
                    }

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                        return result;
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Hiba a file checkinkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                Result ret = new Result();
                ret.ErrorCode = "ItemCheckin";
                ret.ErrorMessage = String.Format("Hiba a file checkin-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                return ret;
            }

            #endregion
        }
        else
        {
            result = new Result();
            result.ErrorCode = "DSTYP00001";
            result.ErrorMessage = "Nem �rt�kelhet� a documentStoreType param�ter!";
        }
        return result;
    }

    /// <summary>
    /// A verziok leszedese utan feltesszuk helyes eredmeny ad vissza az sps webmethod!
    /// 
    /// A visszatorlo reszek (SPS ws hivasok - verzio torlese, item torlese) reszek onmagukban tesztelve lettek
    /// az srvsharpoint gepen. Ott mukodnak. Sajnos a vtesztsps2-n folyton soap exception-t dobnak, s a logban sem
    /// talatam semmit. Gondolom a vtesztsps2 folytonos leallasa miatt az a gep nem mukodik megfeleloen,
    /// amugyis wsdl-t behuzni projektbe sem lehet rola.... :(
    /// Szoval varhatoan meni fog rendesen. 
    /// (Most nincs idom attenni az egeszet srvsharepoint-ra... seged ws is kellene oda, stb....)
    /// 
    /// SegedWS-el kapcsolatban +info: A forrasban mar javitottam, meg itt nalunk is, de FPH-ba korulmenyes betenni.
    /// Ezert KOTELEZO az FPH eseten megadni a doktarFolderPath-t is!
    /// Az itthoni javitottban nem kotelezo doktarFolderPath megadni!
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="doktarSitePath"></param>
    /// <param name="doktarDocLibPath"></param>
    /// <param name="doktarFolderPath">Itthoniban lehet ures, FPH-ban viszont NEM!</param>
    /// <param name="filename"></param>
    /// <param name="cont"></param>
    /// <param name="checkInMode">(0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)</param>
    /// <param name="krt_Dokumentumok"></param>
    /// <returns></returns>
    public Result _DirectUploadAndDocumentInsert(ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode
        , KRT_Dokumentumok krt_Dokumentumok
        )
    {
        Logger.Info("DocumentService.DirectUploadAndDocumentInsert ");

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result result = new Result();

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Logger.Info("_DirectUploadWithCheckin hivas");

            Result resultUpload = this._DirectUploadWithCheckin(execparam
                , documentStoreType
                , doktarSitePath
                , doktarDocLibPath
                , doktarFolderPath
                , filename
                , cont
                , checkInMode);

            if (!String.IsNullOrEmpty(resultUpload.ErrorCode))
            {
                Logger.Error(String.Format("Hibat adott vissza a _DirectUploadWithCheckin method."));
                return resultUpload;
            }
            else
            {
                #region verzio lekerdezese

                Logger.Info("verzio lekerdezese");

                string itemFullUrl = String.Format("{0}/{1}/{2}/{3}{4}{5}"
                    , RemoveSlash(Contentum.eUtility.UI.GetAppSetting("SharePointUrl"))
                    , RemoveSlash(UI.GetAppSetting("SiteCollectionUrl"))
                    , RemoveSlash(doktarSitePath)
                    , RemoveSlash(doktarDocLibPath)
                    , ((String.IsNullOrEmpty(doktarFolderPath)) ? "/" : ("/" + RemoveSlash(doktarFolderPath) + "/"))
                    , filename
                    );

                SPVersions.Versions versionWS = new SPVersions.Versions();
                versionWS.Url = String.Format("{0}/{1}/{2}/_vti_bin/Versions.asmx"
                    , RemoveSlash(Contentum.eUtility.UI.GetAppSetting("SharePointUrl"))
                    , RemoveSlash(UI.GetAppSetting("SiteCollectionUrl"))
                    , RemoveSlash(doktarSitePath));

                versionWS.Credentials = Utility.GetAxisSpsCredential();

                XmlNode ndItemVersionsResult = versionWS.GetVersions(itemFullUrl);

                XmlNamespaceManager xmlNsm = new XmlNamespaceManager(new NameTable());
                xmlNsm.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                XmlNodeList ndList = ndItemVersionsResult.SelectNodes("//a:result", xmlNsm);
                String aktVersion = String.Empty;

                if (ndList.Count != 0)
                {
                    char[] levagando = { '@' };
                    aktVersion = ndList[0].Attributes["version"].Value.Trim(levagando);
                }
                else
                {
                    Logger.Error(String.Format("Nincsenek meg a verziok a GetVersions altal visszaadott eredmenyhalmazban! Ezt adta vissza: {0}"
                        , ndItemVersionsResult.OuterXml));
                }

                #endregion

                #region krt_dok bejegyzes

                Logger.Info("krt_dok bejegyzes");

                KRT_Dokumentumok kapottKrtDokObj = krt_Dokumentumok;
                if (kapottKrtDokObj == null)
                {
                    kapottKrtDokObj = new KRT_Dokumentumok();

                    kapottKrtDokObj.Updated.SetValueAll(false);
                    kapottKrtDokObj.Base.Updated.SetValueAll(false);
                }

                kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
                kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

                kapottKrtDokObj.FajlNev = filename.Trim();
                kapottKrtDokObj.Updated.FajlNev = true;

                kapottKrtDokObj.Meret = cont.Length.ToString();
                kapottKrtDokObj.Updated.Meret = true;

                kapottKrtDokObj.Allapot = "1";
                kapottKrtDokObj.Updated.Allapot = true;

                if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus) || kapottKrtDokObj.Updated.Tipus == false)
                {
                    kapottKrtDokObj.Tipus = GetFormatum(execparam, filename);
                    kapottKrtDokObj.Updated.Tipus = true;
                }

                if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum) || kapottKrtDokObj.Updated.Formatum == false)
                {
                    kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
                    kapottKrtDokObj.Updated.Formatum = true;
                }

                kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
                kapottKrtDokObj.Updated.Alkalmazas_Id = true;

                kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
                kapottKrtDokObj.Updated.Dokumentum_Id = true;

                kapottKrtDokObj.VerzioJel = aktVersion;
                kapottKrtDokObj.Updated.VerzioJel = true;

                kapottKrtDokObj.External_Id = resultUpload.Uid;
                kapottKrtDokObj.Updated.External_Id = true;

                kapottKrtDokObj.External_Link = itemFullUrl;
                kapottKrtDokObj.Updated.External_Link = true;

                // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
                // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
                kapottKrtDokObj.External_Info = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + ";"
                    + RemoveSlash(UI.GetAppSetting("SiteCollectionUrl")) + ";"
                    + RemoveSlash(doktarSitePath.Trim()) + ";"
                    + RemoveSlash(doktarDocLibPath.Trim()) + ";"
                    + RemoveSlash(doktarFolderPath.Trim());
                kapottKrtDokObj.Updated.External_Info = true;

                kapottKrtDokObj.External_Source = documentStoreType;
                kapottKrtDokObj.Updated.External_Source = true;

                //kapottKrtDokObj.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
                kapottKrtDokObj.Leiras = originalFileName;
                kapottKrtDokObj.Updated.Leiras = true;

                kapottKrtDokObj.TartalomHash = GetSHA1CheckSum(cont);
                kapottKrtDokObj.Updated.TartalomHash = true;

                kapottKrtDokObj.KivonatHash = GetSHA1CheckSum(cont);
                kapottKrtDokObj.Updated.KivonatHash = true;

                //kapottKrtDokObj.BarCode = GetParam("vonalkod", xmlParams);
                //kapottKrtDokObj.Updated.BarCode = true;

                Logger.Info("File regisztralasa eDocba.");

                KRT_DokumentumokService dokumentumService2 = new KRT_DokumentumokService();
                Result _ret = dokumentumService2.Insert(execparam, kapottKrtDokObj);
                //Result _ret = new Result();
                //_ret.ErrorCode = "TESZTHIBA1";
                //_ret.ErrorMessage = "HIBA csak teszt miatt kezzel generalva! Mindenkeppen hiba, nincs vegrehajtas!";

                if (!String.IsNullOrEmpty(_ret.ErrorCode))
                {
                    #region sps item torlese vagy verzio torlese, ha nem sikerult az insert

                    Logger.Info("sps item torlese vagy verzio torlese, ha nem sikerult az insert");

                    try
                    {
                        ndItemVersionsResult = versionWS.GetVersions(itemFullUrl);

                        ndList = ndItemVersionsResult.SelectNodes("//a:result", xmlNsm);
                        aktVersion = String.Empty;

                        if (ndList.Count != 0)
                        {
                            char[] levagando = { '@' };
                            aktVersion = ndList[0].Attributes["version"].Value.Trim(levagando);
                        }
                        else
                        {
                            Logger.Error(String.Format("Nincsenek meg a verziok a GetVersions altal visszaadott eredmenyhalmazban! Ezt adta vissza: {0}"
                                , ndItemVersionsResult.OuterXml));
                        }

                        //
                        //  aktVersion-nel nezzuk nem volt-e hiba a lekerdezes soran

                        if (!String.IsNullOrEmpty(aktVersion) && ndList.Count != 0)
                        {
                            // 
                            //  ha tobb verzio, van, akkor csak a legfelsot toroljuk, kulonben az egesz item-et
                            //  egy verzio van -> egesz item torlese:
                            if (ndList.Count == 1)
                            {
                                try
                                {
                                    #region lista verzio lekerese

                                    Logger.Info("list verziojanak megallapitasa...");

                                    char[] vegerol = { '/', ' ' };
                                    String listVersion = String.Empty;

                                    SPLists.Lists list = new SPLists.Lists();
                                    list.Url = String.Format("{0}/{1}/{2}/{3}/_vti_bin/lists.asmx"
                                        , Contentum.eDocument.SharePoint.Utility.GetSharePointUrl().Trim(vegerol)
                                        , UI.GetAppSetting("SiteCollectionUrl").Trim(vegerol)
                                        , doktarSitePath.Trim(vegerol)
                                        , DateTime.Today.ToString("yyyy")
                                        );
                                    list.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                                    XmlNode ndResult = list.GetListCollection();

                                    //Logger.Info(ndResult.OuterXml);

                                    //
                                    // eleg gaz: a DefaultViewUrl elejebol allapitjuk meg, h letezik-e

                                    String keresettUrl = String.Format("/{0}/{1}/{2}"
                                        , UI.GetAppSetting("SiteCollectionUrl").Trim(vegerol)
                                        , doktarSitePath.Trim(vegerol)
                                        , doktarDocLibPath.Trim(vegerol)
                                        );

                                    XmlNamespaceManager xmlNSM = new XmlNamespaceManager(new NameTable());
                                    xmlNSM.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                                    ndList = ndResult.SelectNodes("//a:List", xmlNSM);

                                    int megvan = -1;
                                    int idx_i = 0;
                                    while (idx_i < ndList.Count && megvan == -1)
                                    {
                                        if (Convert.ToString(ndList[idx_i].Attributes["DefaultViewUrl"].Value).Trim().ToLower().StartsWith(keresettUrl.Trim().ToLower()))
                                        {
                                            megvan = idx_i;
                                        }

                                        idx_i++;
                                    }

                                    if (megvan == -1)
                                    {
                                        Logger.Info("Nincs meg a doclib --> return false.");
                                    }
                                    else
                                    {
                                        listVersion = Convert.ToString(ndList[idx_i].Attributes["Version"].Value);
                                    }

                                    #endregion

                                    #region item torlese

                                    if (!String.IsNullOrEmpty(listVersion))
                                    {

                                        Logger.Info("item torlese kovetkezik...");

                                        String strBatch = String.Format("<Method ID=\"1\" Cmd=\"Delete\">"
                                                + "<Field Name=\"ID\">1</Field>"
                                                + "<Field Name=\"FileRef\">{0}</Field>"
                                                + "</Method>"
                                                , itemFullUrl);

                                        XmlDocument xmlDoc = new System.Xml.XmlDocument();

                                        System.Xml.XmlElement elBatch = xmlDoc.CreateElement("Batch");

                                        elBatch.SetAttribute("OnError", "Continue");
                                        elBatch.SetAttribute("ListVersion", listVersion);
                                        //elBatch.SetAttribute("ViewName","0d7fcacd-1d7c-45bc-bcfc-6d7f7d2eeb40");

                                        elBatch.InnerXml = strBatch;

                                        XmlNode ndReturn = list.UpdateListItems("List_Name", elBatch);

                                        Logger.Info("Elem torolve.");
                                    }

                                    #endregion

                                }
                                catch (System.Web.Services.Protocols.SoapException sex1)
                                {
                                    Logger.Error(String.Format("SOAP exception az item torlesekor! {0}\nSource: {1}\nStack: {2}\nActor: {3}\nCode: {4}\nDetail: {5}"
                                        , sex1.Message
                                        , sex1.Source
                                        , sex1.StackTrace
                                        , sex1.Actor
                                        , sex1.Code
                                        , sex1.Detail
                                    ));
                                }
                                catch (Exception ex5)
                                {
                                    Logger.Error(String.Format("Hiba az item torlesekor! {0}\nSource: {1}\nStack: {2}\nInnerMessage: {3}\nInnerSource: {4}\nInnerStack: {5}"
                                        , ex5.Message
                                        , ex5.Source
                                        , ex5.StackTrace
                                        , ((ex5.InnerException != null) ? ex5.InnerException.Message : "null")
                                        , ((ex5.InnerException != null) ? ex5.InnerException.Source : "null")
                                        , ((ex5.InnerException != null) ? ex5.InnerException.StackTrace : "null")
                                    ));
                                }
                            }
                            else
                            {
                                // 
                                //  ha tobb verzio, van, akkor csak a legfelsot toroljuk, kulonben az egesz item-et
                                //  tobb verzio van -> csak egy verzio torlese:

                                Logger.Info(String.Format("torles elott, az aktualis verziot - a legutolsot toroljuk: {0}", aktVersion));

                                try
                                {

                                    //#region checkout - NEM IS KELL EZ A RESZ!

                                    //Contentum.eDocument.SharePoint.DocumentService spsDs = new Contentum.eDocument.SharePoint.DocumentService();

                                    //Logger.Info("Csekaut...");

                                    //using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                                    //{
                                    //    string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                                    //        + RemoveSlash(UI.GetAppSetting("SiteCollectionUrl")) + "/"
                                    //        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                                    //    String res = upld3.ItemCheckout(itemSitePath
                                    //        , RemoveSlash(doktarDocLibPath)
                                    //        , doktarFolderPath
                                    //        , filename);

                                    //    XmlDocument resultDoc = new XmlDocument();
                                    //    try
                                    //    {
                                    //        resultDoc.LoadXml(res);
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        Logger.Error(String.Format("Hiba az ItemCheckout altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                                    //        result.ErrorCode = "ItemCheckout";
                                    //        result.ErrorMessage = "Hiba az ItemCheckout f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                                    //    }

                                    //    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                                    //    {
                                    //        result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                                    //        result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                                    //        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                                    //        return result;
                                    //    }
                                    //}


                                    //#endregion


                                    String s = String.Format("{0}{1}{2}"
                                        , RemoveSlash(doktarDocLibPath)
                                        , ((String.IsNullOrEmpty(doktarFolderPath)) ? "/" : ("/" + RemoveSlash(doktarFolderPath) + "/"))
                                        , filename
                                        );

                                    Logger.Info(String.Format("version ws url: {0}\nversion delete method fileName parameter: {1} "
                                        , versionWS.Url
                                        , s));

                                    ndItemVersionsResult = versionWS.DeleteVersion(s, aktVersion);

                                    Logger.Info("Version deleted.");

                                    //#region checkin - NEM IS KELL EZ A RESZ!

                                    //try
                                    //{
                                    //    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                                    //    {
                                    //        string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                                    //            + RemoveSlash(UI.GetAppSetting("SiteCollectionUrl")) + "/"
                                    //            + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                                    //        String res = upld3.ItemCheckin(itemSitePath
                                    //            , RemoveSlash(doktarDocLibPath)
                                    //            , RemoveSlash(doktarFolderPath)
                                    //            , filename
                                    //            , checkInMode
                                    //            , String.Empty);

                                    //        XmlDocument resultDoc = new XmlDocument();
                                    //        try
                                    //        {
                                    //            resultDoc.LoadXml(res);
                                    //        }
                                    //        catch (Exception ex)
                                    //        {
                                    //            Logger.Error(String.Format("Hiba az ItemCheckin altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                                    //            result.ErrorCode = "ItemCheckin";
                                    //            result.ErrorMessage = "Hiba az ItemCheckin f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                                    //            return result;
                                    //        }

                                    //        if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                                    //        {
                                    //            result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                                    //            result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                                    //            Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                                    //            return result;
                                    //        }
                                    //    }
                                    //}
                                    //catch (Exception exc)
                                    //{
                                    //    Logger.Error(String.Format("Hiba a file checkinkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                                    //    Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                                    //    Result ret = new Result();
                                    //    ret.ErrorCode = "ItemCheckin";
                                    //    ret.ErrorMessage = String.Format("Hiba a file checkin-el�sekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                                    //    return ret;
                                    //}

                                    //#endregion

                                }
                                catch (System.Web.Services.Protocols.SoapException sex1)
                                {
                                    Logger.Error(String.Format("SOAP exception a verzio torlesekor! {0}\nSource: {1}\nStack: {2}\nActor: {3}\nCode: {4}\nDetail: {5}"
                                        , sex1.Message
                                        , sex1.Source
                                        , sex1.StackTrace
                                        , sex1.Actor
                                        , sex1.Code
                                        , sex1.Detail
                                    ));
                                }
                                catch (Exception ex4)
                                {
                                    Logger.Error(String.Format("Hiba a verizio torlesekor! {0}\nSource: {1}\nStack: {2}\nInnerMessage: {3}\nInnerSource: {4}\nInnerStack: {5}"
                                        , ex4.Message
                                        , ex4.Source
                                        , ex4.StackTrace
                                        , ((ex4.InnerException != null) ? ex4.InnerException.Message : "null")
                                        , ((ex4.InnerException != null) ? ex4.InnerException.Source : "null")
                                        , ((ex4.InnerException != null) ? ex4.InnerException.StackTrace : "null")
                                    ));
                                }
                            }

                        }

                    }
                    catch (Exception ex1)
                    {
                        Logger.Error(String.Format("HIBA a sps item torlese vagy verzio torlese, ha nem sikerult az insert utan!!!!!!!\nMessage: {0}\nSource:{1}\nStackTrace: {2}"
                            , ex1.Message
                            , ex1.Source
                            , ex1.StackTrace));
                    }


                    #endregion

                    Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);
                    return _ret;
                }

                kapottKrtDokObj.Id = _ret.Uid;

                result = _ret;
                result.Record = kapottKrtDokObj;

                #endregion
            }
        }
        else
        {
            result = new Result();
            result.ErrorCode = "DSTYP00001";
            result.ErrorMessage = "Nem �rt�kelhet� a documentStoreType param�ter!";
        }
        return result;
    }

    /// <summary>
    /// K�zvetlen �llom�ny felt�lt�s.
    /// Csak a MOSS �g van k�sz.
    /// Felt�lt, semmi m�st nem csin�l. A krt_dok bejegyz�st sem teszi meg.
    /// Checkinel is.
    /// Haszn�lja a t�rhelystrukt�ra meghat�roz� elj�r�st (TarhelyekEllenorzeseMappakTarhely).
    /// Ha van a c�l helyen egy ugyanolyan nev�, akkor �j verzi� fog keletkezni.
    /// 
    /// A visszatorlo reszek (SPS ws hivasok - verzio torlese, item torlese) reszek onmagukban tesztelve lettek
    /// az srvsharpoint gepen. Ott mukodnak. Sajnos a vtesztsps2-n folyton soap exception-t dobnak, s a logban sem
    /// talatam semmit. Gondolom a vtesztsps2 folytonos leallasa miatt az a gep nem mukodik megfeleloen,
    /// amugyis wsdl-t behuzni projektbe sem lehet rola.... :(
    /// Szoval varhatoan meni fog rendesen. 
    /// (Most nincs idom attenni az egeszet srvsharepoint-ra... seged ws is kellene oda, stb....)
    /// 
    /// SegedWS-el kapcsolatban +info: A forrasban mar javitottam, meg itt nalunk is, de FPH-ba korulmenyes betenni.
    /// Ezert KOTELEZO az FPH eseten megadni a doktarFolderPath-t is!
    /// Az itthoni javitottban nem kotelezo doktarFolderPath megadni!
    ///
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="doktarSitePath"></param>
    /// <param name="doktarDocLibPath"></param>
    /// <param name="doktarFolderPath">Itthon nem kotelezo, FPHban viszont igen!</param>
    /// <param name="filename"></param>
    /// <param name="cont"></param>
    /// <param name="checkInMode">CheckIn mode. (0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)</param>
    /// <returns>Result - az Uid-ban a feltoltott MOSS itemId-vel j�n vissza. Vagy hiba.</returns>
    [WebMethod(Description = "<br><b>ExecParam</b> - az alkalmazas es felhasznalo id-knek ki kell lenniuk toltve. " +
          "<br><b>documentStoreType</b> - ez legyen: Contentum.eUtility.Rendszerparameterek.Get(execParam_upload, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS) " +
          "<br><b>doktarSitePath</b> - site path" +
          "<br><b>doktarDocLibPath</b> - doclib neve" +
          "<br><b>doktarFolderPath</b> - konyvtar(ak) nev(ei); ha tobb, / jellel elvalasztva ; lehet ures" +
          "<br><b>filename</b> - file neve" +
          "<br><b>cont</b> - byte tomb, a file tartalma" +
          "<br><b>checkInMode</b> - CheckIn mode. (0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)" +
          "<br><br><b>Result:</b> az Uid-ban a feltoltott (a Krt_dokumentumok tablaban letrejott rekord) id-je jon vissza. Vagy hiba.")]
    public Result DirectUploadWithCheckin(ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode)
    {
        Logger.Info("DirectUploadWithCheckin start");
        Logger.Info("Kapott parameterek: documentStoreType=" + documentStoreType + "; doktarSitePath=" + doktarSitePath + "; doktarDocLibPath=" + doktarDocLibPath + ";doktarFolderPath=" + doktarFolderPath + "; filename=" + filename);
        Result result = new Result();

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(doktarSitePath) ||
            String.IsNullOrEmpty(doktarDocLibPath) ||
            String.IsNullOrEmpty(filename) ||
            cont == null
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType" : "";
            strErrorInfo += String.IsNullOrEmpty(doktarSitePath) ? " doktarSitePath" : "";
            strErrorInfo += String.IsNullOrEmpty(doktarDocLibPath) ? " doktarDocLibPath" : "";
            strErrorInfo += String.IsNullOrEmpty(filename) ? " filename" : "";
            strErrorInfo += (cont == null) ? " cont" : "";

            // TODO: alap parameter hibak:
            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        if (!"0".Equals(checkInMode) && !"1".Equals(checkInMode) && !"2".Equals(checkInMode))
        {
            Logger.Error(String.Format("Rossz checkInMode parameter ertek {0}!", checkInMode));
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            /// -----------------------------------------------------------------------------------------------
            result = _DirectUploadWithCheckin(execparam
            , documentStoreType
            , doktarSitePath
            , doktarDocLibPath
            , doktarFolderPath
            , filename.Trim()
            , cont
            , checkInMode
            );

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execparam"></param>
    /// <param name="documentStoreType"></param>
    /// <param name="doktarSitePath"></param>
    /// <param name="doktarDocLibPath"></param>
    /// <param name="doktarFolderPath"></param>
    /// <param name="filename"></param>
    /// <param name="cont"></param>
    /// <param name="checkInMode">(0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)</param>
    /// <param name="krt_Dokumentumok"></param>
    /// <returns></returns>
    [WebMethod]
    public Result DirectUploadAndDocumentInsert
        (ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode
        , KRT_Dokumentumok krt_Dokumentumok)
    {
        Logger.Info("DirectUploadAndDocumentInsert start");
        Logger.Info("Kapott parameterek: documentStoreType=" + documentStoreType + "; doktarSitePath=" + doktarSitePath + "; doktarDocLibPath=" + doktarDocLibPath + ";doktarFolderPath=" + doktarFolderPath + "; filename=" + filename);
        Result result = new Result();

        if (execparam.Felhasznalo_Id == null ||
            String.IsNullOrEmpty(documentStoreType) ||
            String.IsNullOrEmpty(doktarSitePath) ||
            String.IsNullOrEmpty(doktarDocLibPath) ||
            cont == null
            )
        {
            string strErrorInfo = "";
            strErrorInfo += (execparam.Felhasznalo_Id == null) ? " execparam.Felhasznalo_Id" : "";
            strErrorInfo += String.IsNullOrEmpty(documentStoreType) ? " documentStoreType" : "";
            strErrorInfo += String.IsNullOrEmpty(doktarSitePath) ? " doktarSitePath" : "";
            strErrorInfo += String.IsNullOrEmpty(doktarDocLibPath) ? " doktarDocLibPath" : "";
            strErrorInfo += (cont == null) ? " cont" : "";

            // TODO: alap parameter hibak:
            Logger.Error(String.Format("Hianyzo parameter! ->", strErrorInfo));

            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        if (!"0".Equals(checkInMode) && !"1".Equals(checkInMode) && !"2".Equals(checkInMode))
        {
            Logger.Error(String.Format("Rossz checkInMode parameter ertek {0}!", checkInMode));
            return ResultError.CreateNewResultWithErrorCode(200000);
        }

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = this.dataContext.BeginTransactionIfRequired();

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
            {
                FileSystem.FileSystemManager _FileSystemManager = new FileSystem.FileSystemManager();
                result = _FileSystemManager.DirectUploadAndDocumentInsert(execparam
                , documentStoreType
                , doktarSitePath
                , doktarDocLibPath
                , doktarFolderPath
                , filename.Trim()
                , cont
                , checkInMode
                , krt_Dokumentumok
                );
            }
            else
            {
                /// -----------------------------------------------------------------------------------------------
                result = _DirectUploadAndDocumentInsert(execparam
                , documentStoreType
                , doktarSitePath
                , doktarDocLibPath
                , doktarFolderPath
                , filename.Trim()
                , cont
                , checkInMode
                , krt_Dokumentumok
                );
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            this.dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    /// <summary>
    /// Keres�s a MOSS-ban.
    /// Az els� 10 tal�latot adja vissza.
    /// A result setbe visszaadott oszlopok:
    /// 
    ///    WorkId
    ///    Rank 
    ///    Title - file neve
    ///    Author
    ///    Size - file merete
    ///    Path - file neve, eleresi uttal; URL; = Krt_Dokumentumok.External_Link
    ///    Description
    ///    Write
    ///    SiteName
    ///    CollapsingStatus
    ///    HitHighlightedSummary
    ///    HitHighlightedProperties
    ///    ContentClass
    ///    IsDocument - =1 ha file, =0 ha nem (hanem aspx/folder/site...)
    ///    PictureThumbnailURL
    ///    UniqueId - unique id (csak file eseten van kitoltve); = Krt_Dokumentumok.External_Id
    ///
    ///    A MossKeresEx-et h�vja, 1-10-iggel.
    ///    
    /// </summary>
    /// <param name="execParam">Execparam.</param>
    /// <param name="keywords">Keresett szavak.</param>
    /// <returns>Result - Ds.Table[0] result set. - Record mezo: <searchresultinfo guid=".." totalrows="xx" istotalrowsexact="True|False" start="xx" count="xx" /> </returns>
    [WebMethod(Description = "<br><b>execParam</b> - az alkalmazas es felhasznalo id-knek ki kell lenniuk toltve. " +
          "<br><b>keywords</b> - Keresett kulcssz� (vagy kulcsszavak).")]
    public Result MossKeres(ExecParam execParam, String keywords)
    {
        Logger.Info("MossKeres indul.");
        return MossKeresEx(execParam, keywords, Guid.NewGuid().ToString(), 1, 10);
    }

    /// <summary>
    /// Keres�s MOSSban, nagyobb eredmenyhalmaz eset�n lapoz�ssal is haszn�lhat�.
    /// A result setbe visszaadott oszlopok:
    /// 
    ///    WorkId
    ///    Rank 
    ///    Title - file neve
    ///    Author
    ///    Size - file merete
    ///    Path - file neve, eleresi uttal; URL; = Krt_Dokumentumok.External_Link
    ///    Description
    ///    Write
    ///    SiteName
    ///    CollapsingStatus
    ///    HitHighlightedSummary
    ///    HitHighlightedProperties
    ///    ContentClass
    ///    IsDocument - =1 ha file, =0 ha nem (hanem aspx/folder/site...)
    ///    PictureThumbnailURL
    ///    UniqueId - unique id (csak file eseten van kitoltve); = Krt_Dokumentumok.External_Id
    ///    
    /// Tudnival�:
    /// K�l�n szedi le a dokumentumhoz tartoz� imtemGuid-ot, ez�rt lass�.
    /// MOSS ws nem hajland� visszaadni.
    /// 
    /// </summary>
    /// <param name="execParam">Execparam.</param>
    /// <param name="keywords">Keresett kulcsszavak.</param>
    /// <param name="searchGuid">A keresest azonosito GUID. (MOSS azonositja, amugy jo valamire, gyorsit????)</param>
    /// <param name="start">Kereses eredmenyhalmaz: ettol a sorszamutol adja vissza (1-tol kezdodik).</param>
    /// <param name="count">Kereses eredmenyhalmaz: a <i>start</i> utan ennyit ad vissza (ha 0-at adunk meg, az osszeset hozza).</param>
    /// <returns>Result - Ds.Table[0] result set. - Record mezo: <searchresultinfo guid=".." totalrows="xx" istotalrowsexact="True|False" start="xx" count="xx" /></returns>
    [WebMethod(Description = "<br><b>execParam</b> - az alkalmazas es felhasznalo id-knek ki kell lenniuk toltve. " +
          "<br><b>keywords</b> - Keresett kulcssz� (vagy kulcsszavak)." +
          "<br><b>searchGuid</b> - Kereses azonosito GUID. (MOSS azonositja, amugy jo valamire, gyorsit????)" +
          "<br><b>start</b> - Kereses eredmenyhalmaz: ettol a sorszamutol adja vissza (1-tol kezdodik)." +
          "<br><b>count</b> - Kereses eredmenyhalmaz: a <i>start</i> utan ennyit ad vissza (ha 0-at adunk meg, az osszeset hozza)."
          )]
    public Result MossKeresEx(ExecParam execParam
        , String keywords
        , String searchGuid
        , int start
        , int count
        )
    {
        #region info

        //
        // apro komment az osszes darabszam meghatarozasara:
        // http://www.sharepointblogs.com/koning53/archive/2008/01/21/moss-search-totalavailable-amp-totalrows-not-accurate.aspx
        //
        // "The total number available. This number may not be 100% accurate and all these results may not be accessible."
        //
        // PropertyCollection p = queryResults.ExtendedProperties;
        // key: ElapsedTime
        // key: IgnoredNoiseWords
        // key: SpellingSuggestion
        // key: Keyword
        // key: QueryTerms
        // key: Definition
        //
        // PropertyCollection p = queryResults.Tables[0].ExtendedProperties;
        // key: IsTotalRowsExact
        // key: TotalRows
        //

        #endregion

        Logger.Info("MossKeresEx indul.");
        Result res = new Result();

        #region MOSS root site-on kereses ws meghivasa

        String qXMLString = "<QueryPacket xmlns='urn:Microsoft.Search.Query'>" +
        "<Query><QueryId>" + searchGuid + "</QueryId><SupportedFormats><Format revision='1'>" +
        "urn:Microsoft.Search.Response.Document:Document</Format>" +
        "</SupportedFormats><Context><QueryText language='hu-HU' type='STRING'>" +
        keywords + "</QueryText></Context><Range><StartAt>" + Convert.ToString(start) + "</StartAt><Count>" + Convert.ToString(count) + "</Count></Range>" +
        "<TrimDuplicates>false</TrimDuplicates><IncludeSpecialTermResults>true</IncludeSpecialTermResults>" +
        "</Query></QueryPacket>";


        SPSearches.QueryService queryService = new SPSearches.QueryService();
        queryService.Url = String.Format("{0}{1}_vti_bin/Search.asmx", Contentum.eDocument.SharePoint.Utility.GetSharePointUrl(), Contentum.eDocument.SharePoint.Utility.SharePointUrlPostfix());

        Logger.Info(String.Format("queryService.Url: {0}", queryService.Url));

        queryService.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();  // System.Net.CredentialCache.DefaultCredentials;

        System.Data.DataSet queryResults = null;

        try
        {
            queryResults = queryService.QueryEx(qXMLString);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba queryService.QueryEx WS (MOSS) vegrehajtaskor!\n{0}\n{1}", ex.Message, ex.StackTrace));
            Logger.Error(String.Format("Ezzel a query stringgel hivtuk: {0}", qXMLString));
            res.ErrorCode = "QERR00001";
            res.ErrorMessage = String.Format("Hiba queryService.QueryEx webservice (MOSS) v�grehajt�sakor!\n{0}\n{1}", ex.Message, ex.StackTrace);
            return res;
        }

        #endregion

        #region a dokumentum id-k lekerdezese

        //
        //  egy uj oszlop hozzaadasa a result sethez
        //....
        DataColumn ujDc = new DataColumn("UniqueId");
        queryResults.Tables[0].Columns.Add(ujDc);

        /// TODO: ez most soronkent megy, ami kurva szar!!!!!!!!!!

        Logger.Info("a dokumentum id-k lekerdezese");

        foreach (DataRow row in queryResults.Tables[0].Rows)
        {

            String external_id = String.Empty;

            if ("1".Equals(row["IsDocument"].ToString()))
            {

                #region a dokumentum idket visszaado CAML lekerdezes osszeallitasa

                Logger.Info("a dokumentum idket visszaado CAML lekerdezes osszeallitasa");

                Logger.Info("===================");
                String siteName = row["SiteName"].ToString();
                Logger.Info(String.Format("siteName: {0}", siteName));
                String title = row["Title"].ToString();
                Logger.Info(String.Format("title: {0}", title));
                String path = row["Path"].ToString();
                Logger.Info(String.Format("path: {0}", path));

                #region ha a pathban a gep utan van domain, akkor kivesszuk azt

                String gepSiteName = siteName.Substring(7, siteName.IndexOf("/", 7));
                Logger.Info(String.Format("gepSiteName: {0}", gepSiteName));
                String gepPath = path.Substring(7, path.IndexOf("/", 7));
                Logger.Info(String.Format("gepPath: {0}", gepPath));
                if (!gepPath.Equals(gepSiteName))
                {
                    if (gepPath.Length > gepSiteName.Length)
                    {
                        path = path.Replace(gepPath, gepSiteName);
                    }
                    else
                    {
                        siteName = siteName.Replace(gepSiteName, gepPath);
                    }
                }

                Logger.Info(String.Format("(uj)siteName: {0}", siteName));
                Logger.Info(String.Format("(uj)path: {0}", path));


                #endregion

                String listName = path.Substring(siteName.Length + 1);
                Logger.Info(String.Format("listName: {0}", listName));
                listName = listName.Substring(0, listName.IndexOf("/"));
                Logger.Info(String.Format("listName (2): {0}", listName));
                String size = row["Size"].ToString();
                Logger.Info(String.Format("size: {0}", size));
                Logger.Info("===================");

                XmlDocument xmlDoc = new System.Xml.XmlDocument();

                XmlNode ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");
                XmlNode ndViewFields =
                    xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
                XmlNode ndQueryOptions =
                    xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

                ndQueryOptions.InnerXml =
                    "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                    "<Folder />" +
                    "<DateInUtc>TRUE</DateInUtc>";

                //"<Eq><FieldRef Name='ows_EncodedAbsUrl'/><Value Type='Text'>" + path + "</Value></Eq>" +
                ndQuery.InnerXml = "<Where><And>" +
                        "<Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + title + "</Value></Eq>" +
                        "<Eq><FieldRef Name='File_x0020_Size'/><Value Type='Number'>" + size + "</Value></Eq>" +
                        "</And></Where>";

                #endregion

                #region a CAML query futtatasa

                Logger.Info("a CAML query futtatasa");

                SPLists.Lists list = new SPLists.Lists();
                list.Url = siteName + "/_vti_bin/lists.asmx";
                list.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                XmlNode ndListItems = null;

                Logger.Info(String.Format("\nlist.Url: {0}", list.Url));
                try
                {
                    ndListItems = list.GetListItems(listName, null, ndQuery, ndViewFields, null, ndQueryOptions, null);
                }
                catch (Exception ex1)
                {
                    Logger.Error(String.Format("Hiba (EXCEPTION) a list.GetListItems hivasakor!\n{0}\n{1}", ex1.Message, ex1.StackTrace));
                    Logger.Error(String.Format("listname: {0}\nndQuery.OuterXml: {1}", listName, ndQuery.OuterXml.ToString()));
                }

                #endregion

                #region a CAML eredmeny beleolvasztasa a result setbe

                Logger.Info("a CAML eredmeny beleolvasztasa a result setbe");

                if (ndListItems != null)
                {
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(new NameTable());
                    nsManager.AddNamespace("s", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
                    nsManager.AddNamespace("dt", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
                    nsManager.AddNamespace("rs", "urn:schemas-microsoft-com:rowset");
                    nsManager.AddNamespace("z", "#RowsetSchema");
                    nsManager.AddNamespace("alap", "http://schemas.microsoft.com/sharepoint/soap/");

                    //  item count kivetele
                    try
                    {
                        XmlNode nd1 = ndListItems.SelectSingleNode("//rs:data", nsManager);
                        if (Convert.ToInt16(nd1.Attributes["ItemCount"].InnerXml.ToString()) == 1)
                        {

                            // ok 1 van, megkeressuk a guidot

                            try
                            {
                                nd1 = ndListItems.SelectSingleNode("//z:row", nsManager);
                                external_id = nd1.Attributes["ows_UniqueId"].InnerXml.ToString();

                                char[] elejerol = { '{' };
                                char[] vegerol = { '}' };
                                external_id = external_id.Substring(external_id.IndexOf("{"));
                                external_id = external_id.TrimEnd(vegerol);
                                external_id = external_id.TrimStart(elejerol);
                            }
                            catch (Exception exxml2)
                            {
                                Logger.Error(String.Format("guid xml bugazas hiba!\n{0}\n{1}", exxml2.Message, exxml2.StackTrace));
                            }

                        }
                        else
                        {
                            Logger.Info(String.Format("A nev es meret alapjan nem egy sort kaptunk vissza! Kapott db: {0}\nKapott xml: {1}", nd1.Attributes["ItemCount"].InnerXml, ndListItems.OuterXml));

                            String ows_EncodedAbsUrl = String.Format("//z:row[@ows_EncodedAbsUrl='{0}']", path);

                            if (ndListItems.SelectNodes(ows_EncodedAbsUrl, nsManager).Count == 1)
                            {
                                nd1 = ndListItems.SelectSingleNode(ows_EncodedAbsUrl, nsManager);
                                external_id = nd1.Attributes["ows_UniqueId"].InnerXml.ToString();

                                char[] elejerol = { '{' };
                                char[] vegerol = { '}' };
                                external_id = external_id.Substring(external_id.IndexOf("{"));
                                external_id = external_id.TrimEnd(vegerol);
                                external_id = external_id.TrimStart(elejerol);

                            }
                            else
                            {
                                Logger.Error(String.Format("Az xmlben nem talaltuk meg egyertelmuen a keresett file-t ows_EncodedAbsUrl alapjan! Ezt kerestuk: {0}\nTalalat szama: {1}", path, ndListItems.SelectNodes(ows_EncodedAbsUrl, nsManager).Count));
                            }

                        }
                    }
                    catch (Exception exxml1)
                    {
                        Logger.Error(String.Format("item count xml bugazas hiba!\n{0}\n{1}", exxml1.Message, exxml1.StackTrace));
                    }
                }

                #endregion
            }

            #region ds manipulalasa

            row["UniqueId"] = external_id;

            #endregion

        } //foreach

        #endregion

        #region a result preparalasa 

        Logger.Info("a result preparalasa");

        res.Ds = queryResults;
        res.Record = String.Format("<searchresultinfo guid=\"{0}\" totalrows=\"{1}\" istotalrowsexact=\"{2}\" start=\"{3}\" count=\"{4}\" />", searchGuid, queryResults.Tables[0].ExtendedProperties["TotalRows"], queryResults.Tables[0].ExtendedProperties["IsTotalRowsExact"], start, queryResults.Tables[0].Rows.Count);

        #endregion

        return res;
    }

    /// <summary>
    /// TemplateManager �ltal t�rol�sra haszn�lt site path adja vissza.
    /// </summary>
    /// <returns>String - sitePath</returns>
    [WebMethod]
    public String GetTemplateManagerSitePath()
    {
        Logger.Info("GetTemplateManagerSitePath indul.");

        String templatManagerSiteName = String.Empty;

        try
        {
            templatManagerSiteName = UI.GetAppSetting("TemplateManagerSiteName");
        }
        catch (Exception ex)
        {
            templatManagerSiteName = String.Empty;
        }

        if (String.IsNullOrEmpty(templatManagerSiteName))
            templatManagerSiteName = "TemplateManager";

        //String rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");
        //String doktarSitePath = String.Format("{0}/{1}/{2}/", RemoveSlash(UI.GetAppSetting("DokumentumtarSite")), DateTime.Today.ToString("yyyy"), templatManagerSiteName);
        //String retSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
        //    + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
        //    + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);
        //return retSitePath;

        return templatManagerSiteName;
    }

    /// <summary>
    /// TempalteManager �ltal t�rol�sra haszn�lt document library nev�t adja vissza.
    /// </summary>
    /// <returns>String - docLib neve</returns>
    [WebMethod]
    public String GetTemplateManagerDocLib()
    {
        Logger.Info("GetTemplateManagerSitePath indul.");

        String templateManagerDoclibName = String.Empty;

        try
        {
            templateManagerDoclibName = UI.GetAppSetting("TemplateManagerDoclibName");
        }
        catch (Exception ex)
        {
            templateManagerDoclibName = String.Empty;
        }

        if (String.IsNullOrEmpty(templateManagerDoclibName))
            templateManagerDoclibName = "Templates";

        return templateManagerDoclibName;
    }

    /// <summary>
    /// Visszaadja felt�lthet�k-e a megadott t�pus� �llom�nyok a sharepointba.
    /// Lek�rdezi az blokkolt t�pus� (kiterjeszt�seket) �llom�nyokat �s azzal hasonl�tja �ssze.
    /// Arra is vizsg�l, hogy tartalmaz-e az �llom�nyn�v tiltott karaktereket.
    /// Az eredm�nyt xmlStringk�nt adja vissza a Result Record mezej�ben.
    /// 
    /// Input xml string param�ter:
    ///   <params>
    ///     <filename value="file1 neve kiterjeszt�ssel" size="file1 merete"/>
    ///     <filename value="file2 neve kiterjeszt�ssel" size="file1 merete"/>
    ///     <filename value="file3 neve kiterjeszt�ssel" size="file1 merete"/>
    ///     stb...
    ///   </params>
    ///   
    /// A record mez�ben visszadott xml eredm�ny string:
    ///   <params>
    ///     <filename value="file1 neve kiterjeszt�ssel" size="file1 merete" uploadable="YES|NO" reason="ha uploadble==YES, akkor ures, kulonben itt az ok"/>
    ///     <filename value="file2 neve kiterjeszt�ssel" size="file1 merete" uploadable="YES|NO" reason=""/>
    ///     <filename value="file3 neve kiterjeszt�ssel" size="file1 merete" uploadable="YES|NO" reason=""/>
    ///     stb...
    ///   </params>
    ///   
    /// Ha nem lehet felt�lteni, annak k�t oka lehet, tiltva van, vagy van benne olyan karakter, ami nem mehet spsbe!
    /// 
    /// </summary>
    /// <param name="ex">ExecParam</param>
    /// <param name="xmlStrFilenames">xml string</param>
    /// <returns>Result - Recordban az eredm�ny xml string vagy hiba</returns>
    [WebMethod]
    public Result IsFilesUploadableToMOSS(ExecParam ex, String xmlStrFilenames)
    {
        string eDocumentStoreType = UI.GetAppSetting("eDocumentStoreType");
        Logger.Info(String.Format("eDocumentStoreType=", eDocumentStoreType));
        if ("UCM".Equals(eDocumentStoreType, StringComparison.InvariantCultureIgnoreCase))
        {
            UCMDocumentumManager _UCMDocumentumManager = new UCMDocumentumManager();
            return _UCMDocumentumManager.IsFilesUploadableToMOSS(ex, xmlStrFilenames);
        }
        else if (Contentum.eUtility.Constants.DocumentStoreType.FileSystem.Equals(eDocumentStoreType, StringComparison.InvariantCultureIgnoreCase))
        {
            FileSystem.FileSystemManager _FileSystemManager = new FileSystem.FileSystemManager();
            return _FileSystemManager.IsFilesUploadableToMOSS(ex, xmlStrFilenames);
        }
        else
        {
            return _IsFilesUploadableToMOSS(ex, xmlStrFilenames);
        }
    }

    public Result _IsFilesUploadableToMOSS(ExecParam ex, String xmlStrFilenames)
    {
        Logger.Info(String.Format("IsFilesUploadableToMOSS indul - xmlStrFilenames: {0}", xmlStrFilenames));

        Result result = new Result();

        try
        {

            #region bejovo parameterek ellenorzese

            Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek ellenorzese");

            XmlDocument xmlParams = new XmlDocument();
            xmlParams.LoadXml(xmlStrFilenames);

            if (xmlParams.SelectNodes("//filename").Count == 0)
            {
                Logger.Error("Nincs megadva az xml stringben egy filename node sem!");
                result.ErrorCode = "PARAM0001";
                result.ErrorMessage = "Nincs megadva az xml param�ter stringben egy filename node sem!";
                return result;
            }


            #endregion

            #region bejovo parameterek attributumokkal valo kiegeszitese

            Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek attributumokkal valo kiegeszitese");

            foreach (XmlNode nd in xmlParams.SelectNodes("//filename"))
            {
                bool vanAttrUp = false;
                bool vanAttrRe = false;

                for (int i = 0; i < nd.Attributes.Count; i++)
                {
                    if (nd.Attributes[i].Name.Equals("uploadable"))
                        vanAttrUp = true;
                    if (nd.Attributes[i].Name.Equals("reason"))
                        vanAttrRe = true;
                }

                if (!vanAttrUp)
                {
                    XmlAttribute a = xmlParams.CreateAttribute("uploadable");
                    a.Value = "YES";
                    nd.Attributes.Append(a);
                }
                else
                {
                    nd.Attributes["uploadable"].Value = "YES";
                }

                if (!vanAttrRe)
                {
                    XmlAttribute a = xmlParams.CreateAttribute("reason");
                    a.Value = "";
                    nd.Attributes.Append(a);
                }
                else
                {
                    nd.Attributes["reason"].Value = "";
                }

            }

            #endregion

            #region blokkolt kiterjesztesek lekerdezese


            String rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");
            String sitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                            + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl);

            Logger.Info(String.Format("IsFilesUploadableToMOSS: blokkolt kiterjesztesek lekerdezese - sitePath: {0}", sitePath));

            AxisUploadToSps.Upload upld = this.GetAxisUploadToSps();
            String blockedTypesXmlStr = upld.GetBlockedFileTypes(sitePath);

            Logger.Info(String.Format("IsFilesUploadableToMOSS: kapott eredmeny: {0}", blockedTypesXmlStr));

            XmlDocument xmlBlockedTypes = new XmlDocument();
            xmlBlockedTypes.LoadXml(blockedTypesXmlStr);

            if (!String.IsNullOrEmpty(xmlBlockedTypes.SelectSingleNode("//errorcode").InnerText))
            {
                Logger.Error("IsFilesUploadableToMOSS: hibat adott vissza a blokkolt tipusokat lekerdezo eljaras!");
                result.ErrorCode = xmlBlockedTypes.SelectSingleNode("//errorcode").InnerText;
                result.ErrorMessage = xmlBlockedTypes.SelectSingleNode("//errormessage").InnerText;
                return result;
            }

            #endregion

            #region vizsgalat - eredmeny osszeallitasa

            Logger.Info("IsFilesUploadableToMOSS: vizsgalat - eredmeny osszeallitasa");

            foreach (XmlNode ndKit in xmlBlockedTypes.SelectNodes("//bext"))
            {
                String vizsgaltKiterjesztes = ndKit.InnerText.ToLower();

                foreach (XmlNode ndFile in xmlParams.SelectNodes("//filename"))
                {
                    String fileName = Convert.ToString(ndFile.Attributes["value"].Value).ToLower().Trim();
                    fileName = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(fileName);
                    String kiterjesztes = fileName.Substring(fileName.LastIndexOf(".") + 1);

                    //Logger.Info(String.Format("fileName: {0} - kiterjesztes: {1}", fileName, kiterjesztes));

                    //
                    //  vizsgalat kiterjesztes alapjan 

                    if (kiterjesztes.Equals(vizsgaltKiterjesztes))
                    {
                        ndFile.Attributes["uploadable"].Value = "NO";
                        ndFile.Attributes["reason"].Value = "Kiszolg�l� �ltal blokkolt �llom�nyt�pus!";
                    }
                    else
                    {
                        //
                        //  vizsgalat tiltott karakterekre

                        //Contentum.eDocument.SharePoint.Utility util = new Contentum.eDocument.SharePoint.Utility();

                        //int v = 0;
                        //while (v < Contentum.eDocument.SharePoint.Utility.GetamiTiltott().Length) 
                        //{
                        //    if (fileName.IndexOf(Contentum.eDocument.SharePoint.Utility.GetamiTiltott()[v]) != -1)
                        //    {
                        //        ndFile.Attributes["uploadable"].Value = "NO";
                        //        ndFile.Attributes["reason"].Value = "Az �llom�ny neve tiltott karaktereket tartalmaz!";
                        //    }

                        //    v++;
                        //}

                    }
                }

            }

            #endregion

            #region meretre valo ellenorzes - jelenleg kommentben van! majd tovabbfejleszteskor lesz megirva!!!

            // maxRequestLength

            //foreach (XmlNode ndFile in xmlParams.SelectNodes("//filename"))
            //{
            //    //
            //    //  megvan-e az size attributum
            //    bool megvanaSizeAttr = false;

            //    int i = 0;
            //    while (i < ndFile.Attributes.Count || megvanaSizeAttr == false)
            //    {
            //        if (ndFile.Attributes[i].Name.ToLower().Equals("size"))
            //            megvanaSizeAttr = true;
            //        i++;
            //    } // while

            //    if (megvanaSizeAttr)
            //    {

            //    } //if

            //} // foreach

            #endregion

            result.Record = xmlParams.OuterXml.ToString();

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("fut�si hiba a IsFilesUploadableToMOSS eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            result.ErrorCode = "FUTHIBA00001";
            result.ErrorMessage = String.Format("Hiba az eDocumentService.IsFilesUploadableToMOSS elj�r�s fut�sa k�zben! Hiba�zenet: {0}", exc.Message);
        }

        return result;
    }

    /// <summary>
    /// Egy megadott item-rol megmondja letezik-e. Az item full url alapjan.
    /// Result.Record-ban bool-t ad vissza.
    /// Ellenorzi a megadott utvonalat is, s ha valamelyik nem letezik, akkor is false-t ad vissza.
    /// </summary>
    /// <param name="doktarSitePath"></param>
    /// <param name="doktarDocLibPath"></param>
    /// <param name="doktarFolderPath"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    [WebMethod]
    public Result FileExists(String doktarSitePath, String doktarDocLibPath, String doktarFolderPath, String filename)
    {
        string eDocumentStoreType = UI.GetAppSetting("eDocumentStoreType");
        Logger.Info(String.Format("eDocumentStoreType=", eDocumentStoreType));
        if (Contentum.eUtility.Constants.DocumentStoreType.FileSystem.Equals(eDocumentStoreType, StringComparison.InvariantCultureIgnoreCase))
        {
            FileSystem.FileSystemManager _FileSystemManager = new FileSystem.FileSystemManager();
            return _FileSystemManager.FileExists(doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);
        }
        else
        {
            return _FileExists(doktarSitePath, doktarDocLibPath, doktarFolderPath, filename);
        }
        
    }

    public Result _FileExists(String doktarSitePath, String doktarDocLibPath, String doktarFolderPath, String filename)
    {
        Logger.Info(String.Format("FileExists indul - doktarSitePath: {0} - doktarDocLibPath: {1} - doktarFolderPath: {2}  - filename: {3}", doktarSitePath, doktarDocLibPath, doktarFolderPath, filename));

        Result result = new Result();

        char[] vegerol = { '/', ' ' };

        try
        {
            filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);


            #region site hierarchi ellenorzes: megvan-e a site struktura

            Logger.Info("site hierarchi ellenorzes: megvan-e a site struktura");

            SPWebs.Webs web = new SPWebs.Webs();
            web.Url = String.Format("{0}{1}{2}_vti_bin/webs.asmx", Contentum.eDocument.SharePoint.Utility.GetSharePointUrl(), Contentum.eDocument.SharePoint.Utility.SharePointUrlPostfixRootSiteUrl(), DateTime.Today.ToString("yyyy") + "/");
            Logger.Info(String.Format("web url: {0}", web.Url));
            web.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            XmlNode ndResult = web.GetAllSubWebCollection();

            //Logger.Info(ndResult.OuterXml);

            XmlNamespaceManager xmlNSM = new XmlNamespaceManager(new NameTable());
            xmlNSM.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

            String keresettUrl = String.Format("{0}/{1}/{2}"
                , Contentum.eDocument.SharePoint.Utility.GetSharePointUrl().Trim(vegerol)
                , UI.GetAppSetting("SiteCollectionUrl").Trim(vegerol)
                , doktarSitePath.Trim(vegerol));

            Uri uriKeresettUrl = new Uri(keresettUrl.Trim().ToLower());


            XmlNodeList ndList = ndResult.SelectNodes("//a:Web", xmlNSM);

            Logger.Info(String.Format("ndList.count: {0} - keresett site url: {1}", ndList.Count, keresettUrl));

            int megvan = -1;
            int idx_i = 0;
            while (idx_i < ndList.Count && megvan == -1)
            {
                Uri uri = new Uri(Convert.ToString(ndList[idx_i].Attributes["Url"].Value).Trim().ToLower());
                if (uri.AbsolutePath == uri.AbsolutePath)
                {
                    megvan = idx_i;
                }

                idx_i++;
            }

            if (megvan == -1)
            {
                Logger.Info("Nincs meg a site URL --> return false.");
                result.Record = false;
                return result;
            }

            #endregion

            #region doclib megletenek ellenorzese

            Logger.Info("doclib megletenek ellenorzese");

            SPLists.Lists list = new SPLists.Lists();
            list.Url = String.Format("{0}/{1}/{2}/_vti_bin/lists.asmx"
                , Contentum.eDocument.SharePoint.Utility.GetSharePointUrl().Trim(vegerol)
                , UI.GetAppSetting("SiteCollectionUrl").Trim(vegerol)
                , doktarSitePath.Trim(vegerol)
                );
            list.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

            ndResult = list.GetListCollection();

            //Logger.Info(ndResult.OuterXml);

            //
            // eleg gaz: a DefaultViewUrl elejebol allapitjuk meg, h letezik-e

            keresettUrl = String.Format("/{0}/{1}/{2}"
                , UI.GetAppSetting("SiteCollectionUrl").Trim(vegerol)
                , doktarSitePath.Trim(vegerol)
                , doktarDocLibPath.Trim(vegerol)
                );

            ndList = ndResult.SelectNodes("//a:List", xmlNSM);

            megvan = -1;
            idx_i = 0;
            while (idx_i < ndList.Count && megvan == -1)
            {
                if (Convert.ToString(ndList[idx_i].Attributes["DefaultViewUrl"].Value).Trim().ToLower().StartsWith(keresettUrl.Trim().ToLower()))
                {
                    megvan = idx_i;
                }

                idx_i++;
            }

            if (megvan == -1)
            {
                Logger.Info("Nincs meg a doclib --> return false.");
                result.Record = false;
                return result;
            }

            #endregion

            //
            // folder-t nem kell ellenorizni, mert azt az sps-es method amugy is kiszuri, ha nincs

            #region file megletenek ellenorzese

            Logger.Info("file megletenek ellenorzese");

            Contentum.eDocument.SharePoint.ListUtilService ds = new Contentum.eDocument.SharePoint.ListUtilService();
            Result retSpsbenVaneIlyenFile = ds.GetListItemsByItemNameAppendWithPath(doktarSitePath
                , doktarDocLibPath
                , doktarFolderPath
                , filename);

            if (!String.IsNullOrEmpty(retSpsbenVaneIlyenFile.ErrorCode))
            {
                Logger.Error(String.Format("Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                return retSpsbenVaneIlyenFile;
            }

            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.LoadXml(Convert.ToString(retSpsbenVaneIlyenFile.Record));
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                Result retXmlError = new Result();
                retXmlError.ErrorCode = "XMLP001";
                retXmlError.ErrorMessage = String.Format("{0}\n {1}", exc.Message, exc.StackTrace);
                return retXmlError;
            }

            Logger.Info(String.Format("GetListItemsByItemName adott xml eredmeny: {0}", xmlDoc.OuterXml.ToString()));

            int dbTalalt = ds.GetItemCount(xmlDoc.FirstChild);

            Logger.Info(String.Format("spsben nev alapjan talalt db: {0}", dbTalalt));

            bool vanUgyanolyanFileACelHelyen = false;

            if (dbTalalt > 0)
            {
                vanUgyanolyanFileACelHelyen = true;
            }

            result.Record = vanUgyanolyanFileACelHelyen;

            #endregion 
        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("fut�si hiba a FileExists eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            result.ErrorCode = "FUTHIBA00001";
            result.ErrorMessage = String.Format("Hiba az eDocumentService.FileExists elj�r�s fut�sa k�zben! Hiba�zenet: {0}", exc.Message);
        }

        return result;
    }

    /// <summary>
    /// Megadott helyen l�v�, m�r felt�lt�tt �llom�ny metaadatainak be�ll�t�sa.
    /// A method komplett: csekoutol, v�ltoztat �s csekinel.
    /// 
    /// Megn�zi a KRT_Dokumentumok.Formatum mez�j�t, ha nem DOCX, akkor kil�p!
    /// 
    /// A krt_dokumentumok sort is m�dos�tja, updateli, �jrasz�molja a hash-t.
    /// 
    /// A bemen� xml string param�ter a k�vetkez�:
    ///   <params>
    ///     <krtdocument_id>a KRT_dokumentumok id-je</krtdocument_id>
    ///     <metaadatok>
    ///       <metaadat internalName="internal Nev" value="beallitando ertek" />
    ///     </metaadatok>
    ///   </params>
    /// 
    /// A visszaadott xml string j�n a recordba.
    /// Ugyanaz, mint a bej�v�.
    /// Kieg�sz�tve a k�vetkez�kkel: 
    ///   <params>
    ///     [...]
    ///     <krtdocument_ver>A krt_dokumentumok rekord verzi�sz�ma.</krtdocument_ver>
    ///     <item_ver>A felt�lt�tt item aktu�lis veriz�sz�ma.</item_ver>
    ///     <item_fullurl>Az elem teljes el�r�si �tja.</item_fullurl>
    ///   </params>
    /// 
    /// </summary>
    /// <param name="ex">Execparam</param>
    /// <param name="xmlString">Xml string form�tumban.</param>
    /// <returns>Result - hiba vagy xml string a Record mez�ben.</returns>
    [WebMethod]
    public Result SetDocumentPropertiesAndRevalidate(ExecParam ex, String xmlParamString)
    {
        Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate indul - xmlParamString: {0}", xmlParamString));

        Result result = new Result();

        try
        {
            #region xml adatok objektumba - adatok megletenek ellenorzese

            Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: xml adatok objektumba - adatok megletenek ellenorzese"));

            XmlDocument xmlParams = new XmlDocument();
            xmlParams.LoadXml(xmlParamString);

            if (xmlParams.SelectNodes("//krtdocument_id").Count == 0)
            {
                result.ErrorCode = "PARAM0001";
                result.ErrorMessage = "Hi�nyzik a krtdocument_id param�ter!";
                Logger.Error(result.ErrorMessage);
                return result;
            }

            if (xmlParams.SelectNodes("//metaadat").Count == 0)
            {
                result.ErrorCode = "PARAM0002";
                result.ErrorMessage = "Nincsenek megadott metadat v�ltoz�sok az xml stringben!";
                Logger.Error(result.ErrorMessage);
                return result;
            }

            foreach (XmlNode nd in xmlParams.SelectNodes("//metaadat"))
            {
                Logger.Info(String.Format("metaadat xml adatok ellenorzes: {0}", nd.OuterXml.ToString()));

                bool vanInternalNameAttr = false;
                bool vanValueAttr = false;
                bool vanInternalNameAttrValue = false;

                int x = 0;
                while (x < nd.Attributes.Count)
                {
                    if (nd.Attributes[x].Name.Equals("internalName"))
                    {
                        vanInternalNameAttr = true;

                        String inv = Convert.ToString(nd.Attributes["internalName"].Value);
                        if (!String.IsNullOrEmpty(inv))
                            vanInternalNameAttrValue = true;
                    }

                    if (nd.Attributes[x].Name.Equals("value"))
                        vanValueAttr = true;

                    x++;
                }


                if (vanInternalNameAttr == false)
                {
                    result.ErrorCode = "PARAM0003";
                    result.ErrorMessage = String.Format("Hiba a metadatok param�terek megad�s�ban! Hi�nyzik a metaadat param�ter bels� megnevez�s attrib�tuma!");
                    Logger.Error(result.ErrorMessage);
                    return result;
                }

                if (vanInternalNameAttrValue == false)
                {
                    result.ErrorCode = "PARAM0004";
                    result.ErrorMessage = String.Format("Hiba a metadatok param�terek megad�s�ban! �res a metaadat param�ter bels� megnevez�s attrib�tum �rt�ke!");
                    Logger.Error(result.ErrorMessage);
                    return result;
                }

                if (vanValueAttr == false)
                {
                    result.ErrorCode = "PARAM0005";
                    result.ErrorMessage = String.Format("Hiba a metadatok param�terek megad�s�ban! Hi�nyzik a metaadat param�ter �rt�k attrib�tuma!");
                    Logger.Error(result.ErrorMessage);
                    return result;
                }


            }

            #endregion

            #region krt_dokumentum rekord megszerz�se

            Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: krt_dokumentum rekord megszerz�se - id: {0}", xmlParams.SelectSingleNode("//krtdocument_id").InnerText));

            ExecParam ex1 = ex.Clone();
            ex1.Record_Id = xmlParams.SelectSingleNode("//krtdocument_id").InnerText;

            Result tmpDokRes = GetDoc(ex1);

            if (!String.IsNullOrEmpty(tmpDokRes.ErrorCode))
            {
                Logger.Error(String.Format("SetDocumentPropertiesAndRevalidate: Hiba krt_doc rekord szelektalasakor! {0} - {1}", tmpDokRes.ErrorCode, tmpDokRes.ErrorMessage));
                return tmpDokRes;
            }

            if (tmpDokRes.Record == null)
            {
                Logger.Error(String.Format("SetDocumentPropertiesAndRevalidate: Hiba krt_doc rekord mezoje null!"));
                result.ErrorCode = "NRM0001";
                result.ErrorMessage = "A kapott krt_dokumentumok rekord NULL objektum eredm�nyt adott!";
                return result;
            }

            KRT_Dokumentumok dok = (KRT_Dokumentumok)tmpDokRes.Record;

            //UCM eset�n
            if(dok.External_Source == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                UCMDocumentumManager ucmManager = new UCMDocumentumManager();
                return ucmManager.SetDocumentPropertiesAndRevalidate(ex, xmlParams, dok);
            }

            char[] vago = { ';' };
            String[] parts = dok.External_Info.Split(vago);

            if (parts.Length < 5)
            {
                Logger.Error(String.Format("A kapott External_info mezo tartalma nem megfelelo! A reszeinek a darabszama kisebb, mint 5! -> {0}", dok.External_Info));

                result.ErrorCode = "EXTINFO00001";
                result.ErrorMessage = "A kapott KRT_Dokumentumok objektum External_Info mez�j�nek nem megfelel� a tartalma!";
                return result;
            }

            String spsMachineUrl = parts[0];
            String rootSiteCollectionUrl = parts[1];
            String doktarSitePath = parts[2];
            String doktarDocLibPath = parts[3];
            String doktarFolderPath = parts[4];

            #endregion

            if ("docx".Equals(Convert.ToString(dok.Formatum).ToLower().Trim()))
            {

                #region csekout

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: csekout"));

                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = spsMachineUrl
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.ItemCheckout(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , doktarFolderPath
                        , dok.FajlNev.Trim());

                    Logger.Info(String.Format("upld3.ItemCheckout result: {0}", res));

                    XmlDocument resultDoc = new XmlDocument();
                    resultDoc.LoadXml(res);

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                        return result;
                    }
                }

                #endregion

                #region verzi� sz�m�t�sa - major leptetes

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: verzi� sz�m�t�sa - major leptetes"));

                string aktVersion = String.Empty;
                string elozoVerzio = String.Empty;
                string elozoVerzioUrl = String.Empty;
                string aktualisKrtDokGuid = String.Empty;  //elozo updatehez hasznaljuk

                //0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn
                string checkInMode = Contentum.eUtility.Constants.SharePointCheckIn.MajorCheckIn;

                String itemSitePath2 = spsMachineUrl
                    + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                    + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);
                string itemFullPath = ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath) + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath) + dok.FajlNev.Trim();

                Logger.Info(String.Format("Csekkolt verzio lekerdezese ({0}).", itemFullPath));

                Contentum.eDocument.SharePoint.VersionService vser = new VersionService();
                //Result versionRet = vser.GetCurrentVersion(itemSitePath, itemFullPath);
                Result versionRet = vser.GetCurrentVersion(itemSitePath2
                    , RemoveSlash(doktarDocLibPath)
                    , doktarFolderPath
                    , dok.FajlNev.Trim());

                if (!String.IsNullOrEmpty(versionRet.ErrorCode))
                {
                    Logger.Error("Elozo verzio lekerdezes hiba!");
                    return versionRet;
                }

                aktVersion = Convert.ToString(versionRet.Uid);
                if (vser.LatestGetCVResultRowCount != 1)
                {
                    elozoVerzio = vser.LatestGetCVResultPrevVersion;
                    elozoVerzioUrl = vser.LatestGetCVResultPrevVersionUrl;
                }

                Logger.Info(String.Format("aktVersion: {0}", aktVersion));
                Logger.Info(String.Format("A verziojel joslas. "));

                try
                {
                    NumberFormatInfo numInfo = System.Globalization.NumberFormatInfo.CurrentInfo;

                    try
                    {
                        decimal tmpD = 0;
                        if (!Decimal.TryParse(aktVersion, out tmpD))
                        {
                            Logger.Info(String.Format("Kiserlet 1: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                            aktVersion = aktVersion.Replace(numInfo.CurrencyGroupSeparator, numInfo.CurrencyDecimalSeparator);
                            Logger.Info(String.Format("Kiserlet 2 elott: Atalakitottuk a verzioszamot ilyenne: {0}. Parse kiserlet jon. CurrencyGroupSeparator: {1} - CurrencyDecimalSeparator: {2}", aktVersion, numInfo.CurrencyGroupSeparator, numInfo.CurrencyDecimalSeparator));
                            if (!Decimal.TryParse(aktVersion, out tmpD))
                            {
                                aktVersion = aktVersion.Replace(numInfo.CurrencyDecimalSeparator, numInfo.CurrencyGroupSeparator);
                                Logger.Info(String.Format("Kiserlet 3 elott: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                                if (!Decimal.TryParse(aktVersion, out tmpD))
                                {
                                    aktVersion = aktVersion.Replace(".", numInfo.CurrencyDecimalSeparator); //  ez ha SPS angol - edok webserver magyar
                                    Logger.Info(String.Format("Kiserlet 4 elott: Parszolni probaltuk decimalla a verzioszamot: {0} nem sikerult!", aktVersion));
                                    if (!Decimal.TryParse(aktVersion, out tmpD))
                                    {
                                        Logger.Error(String.Format("Aktualis verzioszam decimal parszolas kiserletei sikertelenek!"));
                                        Result _ret = new Result();
                                        _ret.ErrorCode = "KONVERT0011";
                                        _ret.ErrorMessage = String.Format("Aktu�lis verzi�sz�m decimal konvert�l�s k�s�rletei sikertelenek!");
                                        return _ret;
                                    }
                                }
                            }
                            else
                            {
                                Logger.Info("Parszolas sikeres.");
                            }
                        }
                    }
                    catch (Exception exBelso1)
                    {
                        Logger.Error(String.Format("Aktualis verzioszam decimal konvertalas hiba! Message: {0}\nStackTrace: {1}", exBelso1.Message, exBelso1.StackTrace));
                        Result _ret = new Result();
                        _ret.ErrorCode = "KONVERT0001";
                        _ret.ErrorMessage = String.Format("Aktu�lis verzi�sz�m decimal konvert�l�s hiba! Message: {0}\nStackTrace: {1}", exBelso1.Message, exBelso1.StackTrace);
                        return _ret;
                    }

                    //checkInMode = Contentum.eUtility.Constants.SharePointCheckIn.MajorCheckIn;
                    Logger.Info(String.Format("Major verzio joslat."));
                    //decimal d = Convert.ToDecimal(aktVersion.Replace(".", ","));
                    decimal d = Convert.ToDecimal(aktVersion);
                    string nextMajorVersion = String.Format("{0}.0", System.Math.Floor(d + 1.0m));
                    Logger.Info(String.Format("aktversion: {0} -> ez lesz: {1}", aktVersion, nextMajorVersion));
                    aktVersion = nextMajorVersion;
                }
                catch (Exception excc)
                {
                    Logger.Error(String.Format("HIBA a verizo konvertalasa sor�n!\nMessage: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace));
                    Result _ret = new Result();
                    _ret.ErrorCode = "VERPARS0001";
                    _ret.ErrorMessage = String.Format("HIBA a verizo konvertalasa sor�n!\nMessage: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace);
                    return _ret;
                }

                Logger.Info(String.Format("VerzioJel: {0}, dok.Base.Ver: {1}", aktVersion, dok.Base.Ver));



                #endregion

                #region adatok be�ll�t�sa

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: adatok be�ll�t�sa"));

                try
                {

                    using (AxisUploadToSps.Upload upld4 = GetAxisUploadToSps())
                    {
                        String sPath = spsMachineUrl;
                        sPath += (!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl;
                        sPath += (!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath;

                        String docLibName = (!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath;
                        String folderPath = (!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath;

                        //
                        //  property-k lekerdezese

                        String mossItemPropertiesXmlStr = upld4.GetItemProperties(sPath, docLibName, folderPath, dok.FajlNev.Trim());

                        Logger.Info(String.Format("kapott item properties: {0}", mossItemPropertiesXmlStr));

                        if (String.IsNullOrEmpty(mossItemPropertiesXmlStr))
                        {
                            Logger.Error(String.Format("Ures eredmenyt adott vissza a getItemProperties! A hibarol bovebben a seged webserce logjaban!"));
                            Result _ret = new Result();
                            _ret.ErrorCode = "GIP0001";
                            _ret.ErrorMessage = String.Format("�res eredm�nyt adott vissza a property-ket lek�rdez� f�ggv�ny!");
                            return _ret;
                        }

                        XmlDocument mossItemProperties = new XmlDocument();
                        mossItemProperties.LoadXml(mossItemPropertiesXmlStr);

                        Logger.Info(String.Format("xmlParams.SelectNodes(\"metaadat\").Count: {0}", xmlParams.SelectNodes("//metaadat").Count));

                        //
                        //  property-k beallitasa

                        foreach (XmlNode ndProp in xmlParams.SelectNodes("//metaadat"))
                        {
                            Logger.Info(String.Format("set doc meta prop spsben: {0} = {1}", ndProp.Attributes["internalName"].Value, ndProp.Attributes["value"].Value));

                            string resStr = upld4.SetItemProperty(sPath, docLibName, folderPath, dok.FajlNev.Trim(), Convert.ToString(ndProp.Attributes["internalName"].Value), Convert.ToString(ndProp.Attributes["value"].Value));

                            if (String.IsNullOrEmpty(resStr))
                            {
                                Logger.Error(String.Format("sps tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                Result _ret = new Result();
                                _ret.ErrorCode = "TUL000X";
                                _ret.ErrorMessage = String.Format("sps tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }
                        }

                        //
                        //  verzio property allitasa, ha van olyan neki

                        Logger.Info(String.Format("set doc meta prop spsben: edok_w_verzio elott"));

                        if (mossItemProperties.SelectNodes("//property[@name='edok_w_verzio']").Count == 1)
                        {
                            Logger.Info(String.Format("set doc meta prop spsben: edok_w_verzio = {0}", aktVersion));

                            string resStr1 = upld4.SetItemProperty(sPath, docLibName, folderPath, dok.FajlNev.Trim(), "edok_w_verzio", aktVersion);

                            if (String.IsNullOrEmpty(resStr1))
                            {
                                Logger.Error(String.Format("upld4.SetItemProperty ezt adta vissza: {0}", resStr1));
                                Logger.Error(String.Format("sps verzio tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban."));
                                Result _ret = new Result();
                                _ret.ErrorCode = "TUL000X2";
                                _ret.ErrorMessage = String.Format("sps verzio tulajdons�g be�ll�t�s hiba! A hiba oka a sharepointon levo AxisUpload logban.");
                                return _ret;
                            }

                        }

                    }

                }
                catch (Exception exP)
                {
                    Logger.Error(String.Format("!!!!Nem allitjuk meg a folyamatot e hiba miatt!!!!\nFutasi hiba a property-k beallitasa kozben: {0}\nSource: {1}\nStackTrace: {2}"
                        , exP.Message
                        , exP.Source
                        , exP.StackTrace));
                }

                #endregion

                #region csekin

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: csekin"));

                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = spsMachineUrl
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.ItemCheckin(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , doktarFolderPath
                        , dok.FajlNev.Trim()
                        , checkInMode
                        , String.Empty);

                    Logger.Info(String.Format("upld3.ItemCheckin result: {0}", res));

                    XmlDocument resultDoc = new XmlDocument();
                    resultDoc.LoadXml(res);

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        result.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        result.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", result.ErrorCode, result.ErrorMessage));
                        return result;
                    }
                }

                #endregion

                #region �llom�ny megszerz�se a hash szamolasahoz es hash szamolasa

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: �llom�ny megszerz�se a hash szamolasahoz es hash szamolasa"));

                byte[] feltoltottFileByteArray = null;

                string sha1chk = String.Empty;

                ExecParam execp = ex.Clone();
                execp.Record_Id = xmlParams.SelectSingleNode("//krtdocument_id").InnerText;

                Logger.Info("SHA1 checksumma update a filera resz: GetDoc elott.");
                Result resultDokSel = GetDoc(execp);

                if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor (SHA1 chk).\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                    return resultDokSel;
                }

                if (resultDokSel.Record == null)
                {
                    Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor. Null a result record mezoje! (SHA1 chk)"));
                    resultDokSel = new Result();
                    resultDokSel.ErrorCode = "VESD00001";
                    resultDokSel.ErrorMessage = "Hiba a krt_dokumentumok bejegyz�s lek�rdez�sekor! Null a visszaadott objektum. (SHA1 chk)";
                    return resultDokSel;
                }

                Logger.Info("Lekerdezes ok. (SHA1 chk)");

                Logger.Info("SHA1 checksumma update a filera resz: tartalom leszedese sharepointbol.");

                String copySource = String.Empty;

                using (SPCopys.Copy cpy = new SPCopys.Copy())
                {

                    cpy.Url = String.Format("{0}{1}{2}_vti_bin/copy.asmx", parts[0], parts[1], parts[2]);
                    cpy.Credentials = Contentum.eDocument.SharePoint.Utility.GetCredential();

                    Logger.Info(String.Format("Copy WS url: {0}", cpy.Url));

                    SPCopys.FieldInformation myFieldInfo = new SPCopys.FieldInformation();
                    SPCopys.FieldInformation[] myFieldInfoArray = { myFieldInfo };

                    try
                    {
                        copySource = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                            + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                            + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath)
                            + ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath)
                            + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath)
                            + dok.FajlNev.Trim();

                        Logger.Info(String.Format("item copySource: {0}", copySource));

                        uint myGetUint = cpy.GetItem(copySource, out myFieldInfoArray, out feltoltottFileByteArray);

                        Logger.Info(String.Format("SPS Copy WS GetItem visszateresi erteke (myGetUint): {0}", myGetUint));
                    }
                    catch (Exception exx)
                    {
                        Logger.Error(String.Format("Hiba a SPS Copy WS GetItem hivasakor!\nMessage: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace));
                        Result _ret = new Result();
                        _ret.ErrorCode = "CPYSSHA10001";
                        _ret.ErrorMessage = String.Format("Hiba a SPS Copy WS GetItem hivasakor!\nMessage: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace);
                        return _ret;
                    }

                    sha1chk = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(feltoltottFileByteArray);

                    Logger.Info(String.Format("Szamolt SHA1: {0}", sha1chk));
                }

                //    Logger.Info("Update elokeszitese. (SHA1 chk)");

                //    KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

                //    updateDok.Updated.SetValueAll(false);
                //    updateDok.Base.Updated.SetValueAll(false);

                //    Logger.Info(String.Format("_CheckInUploadedFileWithCTT kapott tartalomSHA1:", GetParam("tartalomSHA1", xmlParams)));

                //    //  tartalom: a belso tartalomra hash (word addin szamolja es adja at parameterkent)
                //    updateDok.TartalomHash = GetParam("tartalomSHA1", xmlParams);
                //    updateDok.Updated.TartalomHash = true;

                //    //  kivonat: a sps-be feltoltott allapot, teljes file hash-e.
                //    updateDok.KivonatHash = sha1chk;
                //    updateDok.Updated.KivonatHash = true;

                //    updateDok.Base.Updated.Ver = true;

                //    Logger.Info("Update elott. (SHA1 chk)");

                //    KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
                //    resultDokSel = dokumentumService.Update(execp, updateDok);

                //    if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                //    {
                //        Logger.Error(String.Format("SHA1 chk update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                //        return resultDokSel;
                //    }

                //}


                #endregion

                #region a kapott adatok update a krt_dokumentumokba

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: a kapott adatok update a krt_dokumentumokba"));

                KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();

                ExecParam exUpdDok = ex.Clone();
                exUpdDok.Record_Id = xmlParams.SelectSingleNode("//krtdocument_id").InnerText;

                dok.Updated.SetValueAll(false);
                dok.Base.Updated.SetValueAll(false);

                dok.Base.Updated.Ver = true;

                dok.VerzioJel = aktVersion;
                dok.Updated.VerzioJel = true;

                //  kivonat: a sps-be feltoltott allapot, teljes file hash-e.
                dok.KivonatHash = sha1chk;
                dok.Updated.KivonatHash = true;

                dok.External_Link = copySource;
                dok.Updated.External_Link = true;

                result = dokumentumService.Update(exUpdDok, dok);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    Logger.Error(String.Format("Krt_dok update hiba!\nErrocode: {0}\nErrorMessage: {1}", result.ErrorCode, result.ErrorMessage));
                    return result;
                }

                #endregion

                #region Ha szerepelt mar a file, akkor az elozo krt_dok bejegyzes urljenek atirasa.

                Logger.Info("--- Ha szerepelt mar a file, akkor az elozo krt_dok bejegyzes urljenek atirasa.");
                Logger.Info(String.Format("Nem csak egy verzio van. elozoVerzio: {0} - ujirat: {1}", elozoVerzio, GetParam("ujirat", xmlParams)));

                //if (!String.IsNullOrEmpty(elozoVerzio) && GetParam("ujirat", xmlParams).Equals("IGEN"))
                if (!String.IsNullOrEmpty(elozoVerzio))
                {

                    Logger.Info("Nem csak egy verzio van. Az elozot urljet at kell allitani.");

                    Logger.Info(String.Format("Db-ben kereses indul... Keresett external_link: {0} es verziojel: {1}", itemFullPath, elozoVerzio));

                    execp = new ExecParam();
                    execp.Alkalmazas_Id = ex.Alkalmazas_Id;
                    execp.Felhasznalo_Id = ex.Felhasznalo_Id;
                    execp.FelhasznaloSzervezet_Id = ex.FelhasznaloSzervezet_Id;

                    KRT_DokumentumokSearch krtDokSearch = new KRT_DokumentumokSearch();

                    krtDokSearch.External_Link.Value = itemFullPath;
                    krtDokSearch.External_Link.Operator = Query.Operators.equals;

                    krtDokSearch.VerzioJel.Value = elozoVerzio;
                    krtDokSearch.VerzioJel.Operator = Query.Operators.equals;

                    KRT_DokumentumokService dokService = new KRT_DokumentumokService();
                    Result resultElozoVerDok = dokService.GetAll(execp, krtDokSearch);

                    if (!String.IsNullOrEmpty(resultElozoVerDok.ErrorCode))
                    {
                        Logger.Error(String.Format("Hiba az elozo verzio lekerdezeskor (dokService.GetAll)!\nKod: {0}\nMessage: {1}", resultElozoVerDok.ErrorCode, resultElozoVerDok.ErrorMessage));
                        return resultElozoVerDok;
                    }

                    Logger.Info(String.Format("Elozo verzio lekerdezes ok. Talalt darabszamok ellenorzese."));

                    //  ez itt azert nem kell, mert ha elso verzio, akkor biztos nem talalunk bejegyzest
                    //  helyesebb lenne azt figyelni, h ez az elso verzio-e, de idohiany miatt majd kesobb

                    //if (resultElozoVerDok.Ds.Tables[0].Rows.Count == 0)
                    //{
                    //    Logger.Error("Nem talaltunk elozo verzio bejegyzest az adatbazisban!");
                    //    _ret.ErrorCode = "PREVV0001";
                    //    _ret.ErrorMessage = "Nem tal�ltuk meg az el�z� verzi�hoz tartoz� bejegyz�st az adatb�zisban!";
                    //    return _ret;
                    //}

                    if (resultElozoVerDok.Ds.Tables[0].Rows.Count > 1)
                    {
                        Logger.Error(String.Format("Tobb bejegyzest talaltunk az elozo verziora az adatbazisban ({0})!", resultElozoVerDok.Ds.Tables[0].Rows.Count));
                        result = new Result();
                        result.ErrorCode = "PREVV0002";
                        result.ErrorMessage = "T�bb bejegyz�st tal�ltuk az el�z� verzi�hoz az adatb�zisban!";
                        return result;
                    }

                    if (resultElozoVerDok.Ds.Tables[0].Rows.Count == 1)
                    {
                        Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas elokeszites kezdodik."));

                        execp.Record_Id = Convert.ToString(resultElozoVerDok.Ds.Tables[0].Rows[0]["Id"]);
                        resultDokSel = GetDoc(execp);

                        if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                        {
                            Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor.\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                            return resultDokSel;
                        }

                        Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas amit kell."));

                        KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

                        updateDok.Updated.SetValueAll(false);
                        updateDok.Base.Updated.SetValueAll(false);

                        updateDok.External_Link = elozoVerzioUrl;
                        updateDok.Updated.External_Link = true;

                        updateDok.Dokumentum_Id_Kovetkezo = aktualisKrtDokGuid;
                        updateDok.Updated.Dokumentum_Id_Kovetkezo = true;

                        updateDok.Base.Updated.Ver = true;

                        execp = new ExecParam();
                        execp.Alkalmazas_Id = ex.Alkalmazas_Id;
                        execp.Felhasznalo_Id = ex.Felhasznalo_Id;
                        execp.FelhasznaloSzervezet_Id = ex.FelhasznaloSzervezet_Id;
                        execp.Record_Id = updateDok.Id;

                        Logger.Info(String.Format("Elozo verzio adatbazis bejegyzes modositas update elott."));
                        resultDokSel = dokumentumService.Update(execp, updateDok);

                        if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
                        {
                            Logger.Error(String.Format("Elozo verzio update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
                            return resultDokSel;
                        }

                    }

                } //if (nem ures az elozoVerzio)


                #endregion


            }
            else
            {
                Logger.Info("Nem docx a formatum kiterjesztese, ezert nem csinalunk semmit.");
            }

            #region a visszatero xml visszallitasa

            Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: a visszatero xml osszeallitasa"));

            ///     <krtdocument_ver>A krt_dokumentumok rekord verzi�sz�ma.</krtdocument_ver>
            ///     <item_ver>A felt�lt�tt item aktu�lis veriz�sz�ma.</item_ver>
            ///     <item_fullurl>Az elem teljes el�r�si �tja.</item_fullurl>

            XmlNode ndUj1 = xmlParams.CreateNode(XmlNodeType.Element, "krtdocument_ver", "");
            ndUj1.InnerText = dok.Base.Ver;

            XmlNode ndUj2 = xmlParams.CreateNode(XmlNodeType.Element, "item_ver", "");
            ndUj2.InnerText = dok.VerzioJel;

            XmlNode ndUj3 = xmlParams.CreateNode(XmlNodeType.Element, "item_fullurl", "");
            ndUj3.InnerText = dok.External_Link;

            xmlParams.FirstChild.AppendChild(ndUj1);
            xmlParams.FirstChild.AppendChild(ndUj2);
            xmlParams.FirstChild.AppendChild(ndUj3);

            result.Record = xmlParams.OuterXml.ToString();

            #endregion

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("fut�si hiba a SetDocumentPropertiesAndRevalidate eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            result.ErrorCode = "FUTHIBA00001";
            result.ErrorMessage = String.Format("Hiba az eDocumentService.SetDocumentPropertiesAndRevalidate elj�r�s fut�sa k�zben! Hiba�zenet: {0}", exc.Message);
        }

        return result;
    }


    #region fodokumentum vizsgalo eljarasok

    /// <summary>
    /// A megadott dokumentum f�dokumentum-e.
    /// K�t param�terez�s:
    /// - az execParam.Record_Id-ben megadjuk a KRT_Dokumentumok ID-t, ilyenkor a doc param�ter lehet null.
    /// - a doc param�terben adjuk az eg�sz KRT_Dokumentumok objektumot.
    /// 
    /// Az iratId-t csak akkor kell megadni, ha csatom�ny �rv�nyess�g ellen�rz�st is akarunk!
    /// 
    /// </summary>
    /// <param name="execParam">ExecParam - Record_Id-ben a KRT_Dokumentumok sor ID-je, vagy nem.</param>
    /// <param name="doc">KRT_Dokumnetumok objektum vagy null, ha az execParam.Record_Id-je kit�lt�tt.</param>
    /// <param name="iratId">Az irat id-je. Nem k�telez�! Csak ha csatErvEllenorzes=True.</param>
    /// <param name="csatErvEllenorzes">True/false - ellen�rizze-e a csatolm�ny (EREC_Csatolmanyok) �rv�nyess�gi idej�t.</param>
    /// <returns>Result - hiba vagy a Record mezej�ben true/false</returns>
    private Result IsDokumentumFodokumentum(ExecParam execParam, KRT_Dokumentumok doc, String iratId, Boolean csatErvEllenorzes)
    {
        Result result = new Result();
        result.Record = false;

        Logger.Info(String.Format("eDocument.IsDokumentumFodokumentum indul. - iratId: {0} - csatErvEllenorzes: {1}", iratId, csatErvEllenorzes));

        try
        {

            #region parameterek ellenorzese

            if (String.IsNullOrEmpty(execParam.Record_Id) && doc == null)
            {
                Logger.Error("parameterezesi hiba! Ures a execparam.record_id es a doc is null.");
                result.ErrorCode = "PARAM0001";
                result.ErrorMessage = "Param�terez�si hiba! �res az execparam.Record_id mez�je �s doc param�ter is!";
                return result;
            }

            if (String.IsNullOrEmpty(iratId) && csatErvEllenorzes == true)
            {
                Logger.Error("parameterezesi hiba! Nincs megadva az irat id!");
                result.ErrorCode = "PARAM0002";
                result.ErrorMessage = "Param�terez�si hiba! Nincs megadva az irat Id vagy ne legyen a csatolm�ny ellen�rz�s (true-ra) be�ll�tva!";
                return result;
            }

            if (!String.IsNullOrEmpty(execParam.Record_Id))
            {
                try
                {
                    Guid value = new Guid(execParam.Record_Id);
                }
                catch (FormatException)
                {
                    Logger.Error("parameterezesi hiba!A execparam.record_id nem GUID ertek talalhato.");
                    result.ErrorCode = "PARAM0003";
                    result.ErrorMessage = "Param�terez�si hiba! Az execparam.Record_id mez�j�ben nem val�s GUID �rt�k tal�lhat�!";
                    return result;
                }
            }

            if (!String.IsNullOrEmpty(iratId))
            {
                try
                {
                    Guid value = new Guid(iratId);
                }
                catch (FormatException)
                {
                    Logger.Error("parameterezesi hiba! Az irat id nem GUID.");
                    result.ErrorCode = "PARAM0004";
                    result.ErrorMessage = "Param�terez�si hiba! Az irat id nem GUID!";
                    return result;
                }
            }

            #endregion

            #region krt_dok id van megadva - select krt_dok

            if (!String.IsNullOrEmpty(execParam.Record_Id))
            {

                Result resultDoc = GetDoc(execParam.Clone());

                if (!String.IsNullOrEmpty(resultDoc.ErrorCode))
                {
                    Logger.Error(String.Format("Hibaval tert vissza a GetDoc fuggveny: {0} - {1}", resultDoc.ErrorCode, resultDoc.ErrorMessage));
                    return resultDoc;
                }

                if (resultDoc.Record == null)
                {
                    Logger.Error(String.Format("Ures (null) ertekkel tert vissza vissza a GetDoc fuggveny! Nem talalhato a megadott id-ju krt_dok bejegyzes: {0}", execParam.Record_Id));
                    result.ErrorCode = "NULLR00001";
                    result.ErrorMessage = "�res (null) �rt�kkel t�rt vissza a GetDoc f�ggv�ny! Nem tal�lhat� a megadott id-j� KRT_Dokumentumok bejegyz�s.";
                    return result;
                }

                doc = (KRT_Dokumentumok)resultDoc.Record;

            }

            #endregion

            #region csatolmany ervenyessegenek ellenorzese

            if (csatErvEllenorzes)
            {

                Logger.Info("csatolmany ervenyessegenek ellenorzese");

                Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
                Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
                Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

                csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.notnull;

                csatolmanyokSearch.Dokumentum_Id.Value = doc.Id;
                csatolmanyokSearch.Dokumentum_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result iratDokCsatRes = csatolmanyokService.GetAllWithExtension(execParam.Clone(), csatolmanyokSearch);

                if (!String.IsNullOrEmpty(iratDokCsatRes.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba a csatolmanyok vizsgalatakor! {0} - {1}", iratDokCsatRes.ErrorCode, iratDokCsatRes.ErrorMessage));
                    return iratDokCsatRes;
                }

                if (iratDokCsatRes.Ds == null)
                {
                    Logger.Error("Ures (null) csatolmany result objektum Ds mezoje!");
                    result.ErrorCode = "CSAT0001";
                    result.ErrorMessage = "Csatolm�nyok t�bla vizsg�lata: �res (null) a kapott result objektum Ds mez�je!";
                    return result;
                }

                if (iratDokCsatRes.Ds.Tables.Count == 0)
                {
                    Logger.Error("Ures (0) csatolmany result objektum Ds.Tables objektum gyujtemenye!");
                    result.ErrorCode = "CSAT0001";
                    result.ErrorMessage = "Csatolm�nyok t�bla vizsg�lata: nincs az eredm�nyben Tables objektumgy�jtem�ny!";
                    return result;
                }

                if (iratDokCsatRes.Ds.Tables[0].Rows.Count == 1)
                {
                    DateTime d;
                    if (DateTime.TryParse(Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[0]["ErvVege"]), out d))
                    {
                        d = DateTime.Parse(Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[0]["ErvVege"]));

                        //
                        //  ha lejart a csatolmany ervenyessegi edeje, akkor nem lehet fodoku

                        if (DateTime.Today.CompareTo(d) > 0)
                        {
                            Logger.Info("A csatolmanynak lejart az ervenyessege, ezert nem lehet fodoku.");
                            result.Record = false;
                            return result;

                        }
                    }
                    else
                    {
                        Logger.Info(String.Format("Csatolmanyok ellenorzese hiba!: nem valod datum az ErvVege: {0}", iratDokCsatRes.Ds.Tables[0].Rows[0]["ErvVege"]));
                    }
                }
                else
                {
                    Logger.Info(String.Format("Csatolmanyok ellenorzese hiba!: nem 1 a kapott sorok szama! Hanem: {0}", iratDokCsatRes.Ds.Tables[0].Rows.Count));
                }

            }

            #endregion

            #region sps meta adatmezo nevek lekerdezese

            Logger.Info("sps meta adatmezo nevek lekerdezese");

            XmlDocument mossItemProperties = new XmlDocument();

            using (AxisUploadToSps.Upload upld4 = GetAxisUploadToSps())
            {
                char[] elvalaszto = { ';' };
                string[] parts = doc.External_Info.Split(elvalaszto);

                String sPath = parts[0] + parts[1] + parts[2];
                String docLibName = parts[3];
                String folderPath = parts[4];

                //
                //  property-k lekerdezese

                String mossItemPropertiesXmlStr = upld4.GetItemProperties(sPath, docLibName, folderPath, doc.FajlNev.Trim());

                Logger.Info(String.Format("kapott item properties: {0}", mossItemPropertiesXmlStr));

                if (String.IsNullOrEmpty(mossItemPropertiesXmlStr))
                {
                    Logger.Error(String.Format("Ures eredmenyt adott vissza a getItemProperties! A hibarol bovebben a seged webserce logjaban!"));
                    Result _ret = new Result();
                    _ret.ErrorCode = "GIP0001";
                    _ret.ErrorMessage = String.Format("�res eredm�nyt adott vissza a property-ket lek�rdez� f�ggv�ny!");
                    return _ret;
                }

                mossItemProperties.LoadXml(mossItemPropertiesXmlStr);
            }

            #endregion

            #region ha van megfelelo metaadat mezo, akkor az adat lekerese

            Logger.Info("ha van megfelelo metaadat mezo, akkor az adat lekerese");

            String keresettNode = "//property[@name='edok_w_fodokumentum']";

            if (mossItemProperties.SelectNodes(keresettNode).Count == 1)
            {
                XmlNode nd = mossItemProperties.SelectSingleNode(keresettNode);
                if (Convert.ToString(nd.Attributes["value"].Value).Equals("1"))
                {
                    result.Record = true;
                }
                else
                {
                    result.Record = false;
                }
            }
            else
            {
                Logger.Info(String.Format("Nincs meg, vagy tul sok van (?!) az `edok_w_fodokumentum` property! Ennyit talaltunk: {0}", mossItemProperties.SelectNodes("//property[@name='edok_w_fodokumentum']").Count));
                result.Record = false;
            }

            #endregion

        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("EXCEPTION: Hiba a IsDokumentumFodokumentum vegrehajtas kozben! {0} - {1}", ex.Message, ex.StackTrace));
            result.ErrorCode = "EXCK0000001";
            result.ErrorMessage = String.Format("EXCEPTION: Hiba a IsDokumentumFodokumentum vegrehajtas kozben! {0} - {1}", ex.Message, ex.StackTrace);
        }

        return result;
    }

    /// <summary>
    /// Az irat alapj�n van-e, �s ha igen, melyik a f�dokumentum.
    /// Az irat id alapj�n keres, ha megadjuk az id-j�t az execParam.Record_Id-be.
    /// KRT_Dokumentumok objektumot ad vissza a Result.Record mezej�ben.
    /// </summary>
    /// <param name="execParam">Execparam.</param>
    /// <returns>Result - hiba, vagy �res Record mez�, akkor nincs, vagy ha van Record mez�ben KRT_Dokumentumok objektum.</returns>
    private Result GetIratFokdokumentum(ExecParam execParam)
    {
        Result result = new Result();

        Logger.Info(String.Format("eDocument.GetIratFokdokumentum indul."));

        try
        {
            #region parameterek ellenorzese

            if (String.IsNullOrEmpty(execParam.Record_Id))
            {
                Logger.Error("parameterezesi hiba! Ures a execparam.record_id (null).");
                result.ErrorCode = "PARAM0001";
                result.ErrorMessage = "Param�terez�si hiba! �res az execparam.Record_id mez�je param�ter!";
                return result;
            }

            if (!String.IsNullOrEmpty(execParam.Record_Id))
            {
                try
                {
                    Guid value = new Guid(execParam.Record_Id);
                }
                catch (FormatException)
                {
                    Logger.Error("parameterezesi hiba!A execparam.record_id nem GUID ertek talalhato.");
                    result.ErrorCode = "PARAM0003";
                    result.ErrorMessage = "Param�terez�si hiba! Az execparam.Record_id mez�j�ben nem val�s GUID �rt�k tal�lhat�!";
                    return result;
                }
            }

            #endregion

            #region irat id van megadva - select irat = KOMMENTBEN! (mert felesleges)

            //if (!String.IsNullOrEmpty(execParam.Record_Id))
            //{

            //    Contentum.eRecord.Service.ServiceFactory sfx = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            //    Contentum.eRecord.Service.EREC_IraIratokService iratService = sfx.GetEREC_IraIratokService();

            //    ExecParam iratExecParam = execParam.Clone();

            //    Result resultIrat = iratService.Get(iratExecParam);

            //    if (!String.IsNullOrEmpty(resultIrat.ErrorCode))
            //    {
            //        Logger.Error(String.Format("Hibaval tert vissza az irat select: {0} - {1}", resultIrat.ErrorCode, resultIrat.ErrorMessage));
            //        return resultIrat;
            //    }

            //    if (resultIrat.Record == null)
            //    {
            //        Logger.Error(String.Format("Ures (null) ertekkel tert vissza vissza az irat select! Nem talalhato a megadott id-ju irat bejegyzes: {0}", execParam.Record_Id));
            //        result.ErrorCode = "NULLR00001";
            //        result.ErrorMessage = "�res (null) �rt�kkel t�rt vissza az irat select! Nem tal�lhat� a megadott id-j� Irat objektum bejegyz�s.";
            //        return result;
            //    }

            //    irat = (EREC_IraIratok)resultIrat.Record;

            //}

            #endregion

            #region csatolmanyok vizsgalata

            Logger.Info("csatolmanyok vizsgalata");

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
            Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

            csatolmanyokSearch.IraIrat_Id.Value = execParam.Record_Id;
            csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratDokCsatRes = csatolmanyokService.GetAllWithExtension(execParam.Clone(), csatolmanyokSearch);

            if (!String.IsNullOrEmpty(iratDokCsatRes.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a csatolmanyok vizsgalatakor! {0} - {1}", iratDokCsatRes.ErrorCode, iratDokCsatRes.ErrorMessage));
                return iratDokCsatRes;
            }

            if (iratDokCsatRes.Ds == null)
            {
                Logger.Error("Ures (null) csatolmany result objektum Ds mezoje!");
                result.ErrorCode = "CSAT0001";
                result.ErrorMessage = "Csatolm�nyok t�bla vizsg�lata: �res (null) a kapott result objektum Ds mez�je!";
                return result;
            }

            if (iratDokCsatRes.Ds.Tables.Count == 0)
            {
                Logger.Error("Ures (0) csatolmany result objektum Ds.Tables objektum gyujtemenye!");
                result.ErrorCode = "CSAT0001";
                result.ErrorMessage = "Csatolm�nyok t�bla vizsg�lata: nincs az eredm�nyben Tables objektumgy�jtem�ny!";
                return result;
            }

            Logger.Info(String.Format("Talalt csatolmanyok szama: {0}", iratDokCsatRes.Ds.Tables[0].Rows.Count));

            int i = 0;
            result.Record = null;

            while (i < iratDokCsatRes.Ds.Tables[0].Rows.Count && result.Record == null)
            {

                ExecParam execP1 = execParam.Clone();
                execP1.Record_Id = Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[i]["Dokumentum_Id"]);

                Logger.Info(String.Format("Vizsgalt dokumentum: {0}", Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[i]["Dokumentum_Id"])));

                DateTime d;
                if (DateTime.TryParse(Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[i]["ErvVege"]), out d))
                {
                    d = DateTime.Parse(Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[i]["ErvVege"]));

                    //
                    //  ha lejart a csatolmany ervenyessegi edeje, akkor nem lehet fodoku

                    if (DateTime.Today.CompareTo(d) > 0)
                    {

                        Result resultGetFoDok = IsDokumentumFodokumentum(execP1, null, execParam.Record_Id, false);

                        if (String.IsNullOrEmpty(resultGetFoDok.ErrorCode))
                        {

                            if ((Boolean)resultGetFoDok.Record == true)
                            {

                                ExecParam execP2 = execParam.Clone();
                                execP2.Record_Id = Convert.ToString(iratDokCsatRes.Ds.Tables[0].Rows[i]["Dokumentum_Id"]);

                                Result resultGetDoc = GetDoc(execP2);

                                if (!String.IsNullOrEmpty(resultGetDoc.ErrorCode))
                                {
                                    Logger.Error(String.Format("Hiba a megallapitott fodokumentum db seletjekor! {0} - {1}", resultGetDoc.ErrorCode, resultGetDoc.ErrorMessage));
                                    return resultGetDoc;
                                }

                                if (resultGetDoc.Record == null)
                                {
                                    Logger.Error("Hiba a fokdokumentum selet altal visszaadott resultban! Ures (null) a Record mezo!");
                                    result.ErrorCode = "URESR0001";
                                    result.ErrorMessage = "Hiba a f�dokumentum Krt_Dokumentumok objektum adatb�zisb�l val� selectjekor! �res (null) record mez�!";
                                    return result;
                                }

                                result.Record = (KRT_Dokumentumok)resultGetDoc.Record;
                            }
                        }
                    }
                }

                i++;
            }

            #endregion

        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("EXCEPTION: Hiba a GetIratFokdokumentum vegrehajtas kozben! {0} - {1}", ex.Message, ex.StackTrace));
            result.ErrorCode = "EXCK0000001";
            result.ErrorMessage = String.Format("EXCEPTION: Hiba a GetIratFokdokumentum vegrehajtas kozben! {0} - {1}", ex.Message, ex.StackTrace);
        }

        return result;
    }


    #endregion

    /// <summary>
    /// Vissaadja a sharepointbol egy megadott krt_dok bejegyzeshez tartzo verziokat a result table-ben.
    /// </summary>
    /// <param name="ex">Execparam. A Record_Id-ben az krt_dok idvel.</param>
    /// <returns>Result - dataTable 0 kiroltve - vagy Error
    /// DataTable columns:
    ///     name        -   file neve
    ///     version     -   verzioszam
    ///     url         -   url
    ///     created     -   mikor jott letre a verzio
    ///     comments    -   a verziohoz tarozo megjegyzes
    /// </returns>
    [WebMethod(Description = "Vissaadja a sharepointbol egy megadott krt_dok bejegyzeshez tartzo verziokat a result table-ben." +
              "<br><b>Execparam</b> - Execparam. A Record_Id-ben az krt_dok idvel. " +
              "<br><br><b>Result:</b> Result - dataTable 0 kiroltve - vagy Error.")]
    public Result GetDocumentVersionsFromMOSS(ExecParam ex)
    {
        Logger.Info("GetDocumentVersionsFromMOSS indul.");

        Result retResult = new Result();

        string eDocumentStoreType = UI.GetAppSetting("eDocumentStoreType");
        Logger.Info(String.Format("eDocumentStoreType=", eDocumentStoreType));

        if ("UCM".Equals(eDocumentStoreType, StringComparison.InvariantCultureIgnoreCase))
        {
            UCMDocumentumManager manager = new UCMDocumentumManager();
            retResult = manager.GetDocumentVersionsFromMOSS(ex);
        }
        else
        {
            if (String.IsNullOrEmpty(ex.Record_Id))
            {
                retResult.ErrorCode = "PARAM0001";
                retResult.ErrorMessage = "Hi�nyzik az execparam Record_Id-j�b�l a krt_dokumentum id!";
                return retResult;
            }

            try
            {
                #region krt_dok bejegyzes lekerdezese

                Result resDoc = GetDoc(ex.Clone());

                if (!String.IsNullOrEmpty(resDoc.ErrorCode))
                {
                    Logger.Error(String.Format("GetDoc hiba: {0}", resDoc.ErrorMessage));
                    return resDoc;
                }

                KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)resDoc.Record;

                #endregion

                #region verziok lekerdezese 

                char[] VagoIstvan = { '/', ' ' };

                string[] pparts = dokumentum.External_Info.ToString().Split(';');

                SPVersions.Versions versionWS = new SPVersions.Versions();
                versionWS.Url = String.Format("{0}/{1}/{2}/_vti_bin/Versions.asmx"
                    , pparts[0].TrimEnd(VagoIstvan)
                    , pparts[1].TrimEnd(VagoIstvan)
                    , pparts[2].TrimEnd(VagoIstvan));
                versionWS.Credentials = Utility.GetAxisSpsCredential();

                XmlNode ndItemVersionsResult = versionWS.GetVersions(dokumentum.External_Link);

                #endregion

                #region result dataset osszeallaitasa

                retResult.Ds = new DataSet();
                DataTable dt = retResult.Ds.Tables.Add();

                dt.Columns.Add("Id", typeof(Guid));
                dt.Columns.Add("name", typeof(String));
                dt.Columns.Add("Formatum", typeof(String));
                dt.Columns.Add("version", typeof(String));
                dt.Columns.Add("url", typeof(String));
                dt.Columns.Add("External_Link", typeof(String));
                dt.Columns.Add("size", typeof(Int32));
                dt.Columns.Add("created", typeof(DateTime));
                dt.Columns.Add("comments", typeof(String));

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
                nsmgr.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                XmlNodeList ndListVersions = ndItemVersionsResult.SelectNodes("//a:result", nsmgr);

                int idx_Version = 0;
                while (idx_Version < ndListVersions.Count)
                {
                    XmlNode nd = ndListVersions[idx_Version];

                    dt.Rows.Add(
                        dokumentum.Id
                        , dokumentum.FajlNev.Trim()
                        , dokumentum.Formatum
                        , Convert.ToString(nd.Attributes["version"].Value).Replace("@", "")
                        , Convert.ToString(nd.Attributes["url"].Value)
                        , Convert.ToString(nd.Attributes["url"].Value)
                        , nd.Attributes["size"].Value
                        , Convert.ToString(nd.Attributes["created"].Value)
                        , Convert.ToString(nd.Attributes["comments"].Value)
                        );

                    idx_Version++;
                }

                #endregion

            }
            catch (Exception ex1)
            {
                Logger.Error(String.Format("GetDocumentVersionsFromMOSS futasi hiba: {0}\nSource: {1}\nStackTrace: {2}", ex1.Message, ex1.Source, ex1.StackTrace));
                retResult.ErrorCode = "RTM0001";
                retResult.ErrorMessage = String.Format("GetDocumentVersionsFromMOSS fut�si hiba: {0}", ex1.Message);
            }
        }

        return retResult;
    }

    [WebMethod()]
    public Result UpdateIktatottIratDocumentsLocation(ExecParam execParam, EREC_UgyUgyiratok ugyirat, EREC_IraIratok irat)
    {
        Logger.Info("UpdateIratDocumentsLocation kezdete");

        Result res = new Result();

        try
        {
            Logger.Info(String.Format("irat id: {0}", execParam.Record_Id));
            EREC_CsatolmanyokService svcCsatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch schCsatolmanyok = new EREC_CsatolmanyokSearch();
            schCsatolmanyok.IraIrat_Id.Value = irat.Id;
            schCsatolmanyok.IraIrat_Id.Operator = Query.Operators.equals;
            Result resCsatolmanyok = svcCsatolmanyok.GetAll(execParam.Clone(), schCsatolmanyok);

            if (resCsatolmanyok.IsError)
            {
                Logger.Error("Csatolmanyok.GetAll hiba", execParam, resCsatolmanyok);
                throw new ResultException(resCsatolmanyok);
            }

            List<string> dokumentumokIdList = new List<string>();

            foreach (DataRow row in resCsatolmanyok.Ds.Tables[0].Rows)
            {
                dokumentumokIdList.Add(row["Dokumentum_Id"].ToString());
            }

            Logger.Debug(String.Format("Dokumentumok sz�ma: {0}",dokumentumokIdList.Count));
            if (dokumentumokIdList.Count > 0)
            {
                KRT_DokumentumokService svcDokumentumok = new KRT_DokumentumokService(this.dataContext);
                KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
                schDokumentumok.Id.Value = Search.GetSqlInnerString(dokumentumokIdList.ToArray());
                schDokumentumok.Id.Operator = Query.Operators.inner;

                Result resDokumentumok = svcDokumentumok.GetAll(execParam.Clone(), schDokumentumok);

                if (resDokumentumok.IsError)
                {
                    Logger.Error("Dokumentumok.GetAll hiba", execParam, resDokumentumok);
                    throw new ResultException(resDokumentumok);
                }


                foreach (DataRow row in resDokumentumok.Ds.Tables[0].Rows)
                {
                    string external_Info = row["External_Info"].ToString();
                    string external_Source = row["External_Source"].ToString();
                    Logger.Debug(String.Format("External_Info: {0}", external_Info));
                    Logger.Debug(String.Format("External_Source: {0}", external_Source));

                    if (!String.IsNullOrEmpty(external_Info))
                    {
                        Result resMoveDokument = new Result();

                        if (external_Source == Contentum.eUtility.Constants.DocumentStoreType.UCM)
                        {
                            UCMDocumentumManager ucmManager = new UCMDocumentumManager();
                            resMoveDokument = ucmManager.MoveDokument(execParam, ugyirat, irat, row);
                        }
                        else
                        {
                            string fileName = row["FajlNev"].ToString();
                            Utility.TarolasiHelyStruktura oldTarolasiHely = new Utility.TarolasiHelyStruktura(external_Info, fileName);
                            string newDoktarFolderPath = Utility.GetIktatottAnyagFolderPath(Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat), Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(irat));
                            Logger.Debug(String.Format("NewDoktarFolderPath: {0}", newDoktarFolderPath));
                            Utility.TarolasiHelyStruktura newTarolasiHely = new Utility.TarolasiHelyStruktura(external_Info, fileName);
                            newTarolasiHely.DoktarFolderPath = newDoktarFolderPath;


                            string id = row["Id"].ToString();
                            string ver = row["Ver"].ToString();
                            string VerzioJel = row["VerzioJel"].ToString();

                            resMoveDokument = this.MoveDokument(execParam, oldTarolasiHely, newTarolasiHely, id, ver, VerzioJel);
                        }

                        if (resMoveDokument.IsError)
                        {
                            Logger.Error("MoveDokument hiba", execParam, resMoveDokument);
                            res = resMoveDokument;
                            continue;
                        }
                    }
                }

            }
            
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("UpdateIratDocumentsLocation hiba", execParam, res);
        }

        Logger.Info("UpdateIratDocumentsLocation vege");
        return res;
    }

    #region Selejtez�si funkci� 
    //bernat.laszlo added
    [WebMethod()]
    public Result SelejtezesIratDocumentsLocation(ExecParam execParam, EREC_IraIratok irat)
    {
        Logger.Info("SelejtezesIratDocumentsLocation kezdete");

        Result res = new Result();

        try
        {
            Logger.Info(String.Format("irat id: {0}", irat.Id));
            Logger.Info(String.Format("Jegyzek id: {0}", execParam.Record_Id));
            EREC_CsatolmanyokService svcCsatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch schCsatolmanyok = new EREC_CsatolmanyokSearch();
            schCsatolmanyok.IraIrat_Id.Value = irat.Id;
            schCsatolmanyok.IraIrat_Id.Operator = Query.Operators.equals;
            Result resCsatolmanyok = svcCsatolmanyok.GetAll(execParam.Clone(), schCsatolmanyok);

            if (resCsatolmanyok.IsError)
            {
                Logger.Error("Csatolmanyok.GetAll hiba", execParam, resCsatolmanyok);
                throw new ResultException(resCsatolmanyok);
            }

            List<string> dokumentumokIdList = new List<string>();

            foreach (DataRow row in resCsatolmanyok.Ds.Tables[0].Rows)
            {
                dokumentumokIdList.Add(row["Dokumentum_Id"].ToString());
            }

            Logger.Debug(String.Format("Dokumentumok sz�ma: {0}", dokumentumokIdList.Count));
            if (dokumentumokIdList.Count > 0)
            {
                KRT_DokumentumokService svcDokumentumok = new KRT_DokumentumokService(this.dataContext);
                KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
                schDokumentumok.Id.Value = Search.GetSqlInnerString(dokumentumokIdList.ToArray());
                schDokumentumok.Id.Operator = Query.Operators.inner;

                Result resDokumentumok = svcDokumentumok.GetAll(execParam.Clone(), schDokumentumok);

                if (resDokumentumok.IsError)
                {
                    Logger.Error("Dokumentumok.GetAll hiba", execParam, resDokumentumok);
                    throw new ResultException(resDokumentumok);
                }


                foreach (DataRow row in resDokumentumok.Ds.Tables[0].Rows)
                {
                    string external_Info = row["External_Info"].ToString();
                    Logger.Debug(String.Format("External_Info: {0}", external_Info));

                    if (!String.IsNullOrEmpty(external_Info))
                    {
                        string fileName = row["FajlNev"].ToString();
                        Utility.TarolasiHelyStruktura tarolasiHely = new Utility.TarolasiHelyStruktura(external_Info, fileName);

                        string id = row["Id"].ToString();
                        string ver = row["Ver"].ToString();
                        string VerzioJel = row["VerzioJel"].ToString();

                        Result resMoveDokument = this.Selejtezes_DeleteDokument(execParam, tarolasiHely, id, ver, VerzioJel);

                        if (resMoveDokument.IsError)
                        {
                            Logger.Error("Selejtezes_DeleteDokument hiba", execParam, resMoveDokument);
                            res = resMoveDokument;
                            continue;
                        }
                    }
                }

            }

        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("SelejtezesIratDocumentsLocation hiba", execParam, res);
        }

        Logger.Info("SelejtezesIratDocumentsLocation vege");
        return res;
    }

    private Result Selejtezes_MoveDokument(ExecParam execParam, Utility.TarolasiHelyStruktura srcTarolasihely, Utility.TarolasiHelyStruktura destTarolasiHely, string dokumentumId, string dokumentumVer, string dokumentumVerzioJel)
    {
        Logger.Info("Selejtezes_MoveDocument kezdete");
        Result res = new Result();
        bool sharepointFileMoved = false;
        try
        {
            Logger.Debug(String.Format("srcTarolasiHely:{0}", srcTarolasihely.ToExternalLink()));
            Logger.Debug(String.Format("destTarolasiHely:{0}", destTarolasiHely.ToExternalLink()));
            Logger.Debug(String.Format("dokumentumId:{0}", dokumentumId ?? "NULL"));
            Logger.Debug(String.Format("dokumentumVer:{0}", dokumentumVer ?? "NULL"));

            if (String.IsNullOrEmpty(dokumentumId))
            {
                throw new ResultException("DokumentumId param�ter �res!");
            }

            if (srcTarolasihely.DoktarSitePath.Contains("IKTATOTTANYAGOK") || srcTarolasihely.DoktarSitePath.Contains("CSATOLMANYOK"))
            {
                Logger.Debug("IKTATOTTANYAGOK vagy CSATOLMANYOK");

                if (srcTarolasihely.DoktarSitePath != destTarolasiHely.DoktarSitePath)
                {
                    Logger.Debug("srcTarolasihely.DoktarSitePath != destTarolasiHely.DoktarSitePath");

                    //Megvizsg�ljuk nem tartozik-e t�bb irathoz a sharepoint dokumentum
                    KRT_DokumentumokService svcDokumentumok = new KRT_DokumentumokService();
                    ExecParam xpmDokumentumGetAll = execParam.Clone();
                    KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
                    schDokumentumok.FajlNev.Value = srcTarolasihely.FileName;
                    schDokumentumok.FajlNev.Operator = Query.Operators.equals;
                    schDokumentumok.External_Info.Value = srcTarolasihely.ToExternalInfo();
                    schDokumentumok.External_Info.Operator = Query.Operators.equals;
                    Result resDokumentumokGetAll = svcDokumentumok.GetAll(xpmDokumentumGetAll, schDokumentumok);

                    if (resDokumentumokGetAll.IsError)
                    {
                        Logger.Error("Dokumentumok.GetAll hiba", xpmDokumentumGetAll, resDokumentumokGetAll);
                        throw new ResultException(resDokumentumokGetAll);
                    }

                    int num = resDokumentumokGetAll.Ds.Tables[0].Rows.Count;

                    Logger.Debug(String.Format("Dokumentumok szama: {0}", num));

                    #region SPS selejtez�s MoveDokument Core
                    if (num > 0)
                    {
                        Logger.Debug(String.Format("CopyFiles: form: {0},  to: {1}", srcTarolasihely.ToExternalLink(), destTarolasiHely.ToExternalLink()));
                        
                        #region File lek�r�se

                        System.Net.WebRequest request = System.Net.WebRequest.Create(srcTarolasihely.ToExternalLink());
                        request.Credentials = new System.Net.NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                        System.Net.WebResponse response = request.GetResponse();

                        System.IO.Stream s = response.GetResponseStream();
                        byte[] buf = new byte[20480];

                        while (true)
                        {
                            int readBytes = s.Read(buf, 0, buf.Length);

                            if (readBytes == 0)
                                break;
                        }
                        s.Close();
                        bool skipFile = false;
                        if (buf.Length == 0)
                        {
                            Logger.Warn("Selejtez�s error: A f�jl nem tal�lhat�. Folyamat folytat�sa!");
                            skipFile = true;
                        }

                        #endregion

                        if (!skipFile)
                        {
                            #region File felt�lt�se a SELEJTEZ�S sitera
                            string documentStoreType = "SharePoint";
                            string documentSite = destTarolasiHely.DoktarSitePath;
                            string documentStore = destTarolasiHely.DoktarDocLibPath;
                            string documentFolderPath = destTarolasiHely.DoktarFolderPath;
                            ExecParam tarhely_ExecParam = execParam.Clone();

                            Result upload_result = _DirectUploadWithCheckin(tarhely_ExecParam.Clone()
                            , "SharePoint"
                            , destTarolasiHely.DoktarSitePath
                            , destTarolasiHely.DoktarDocLibPath
                            , destTarolasiHely.DoktarFolderPath
                            , destTarolasiHely.FileName
                            , buf
                            , "2");

                            if (!upload_result.IsError)
                            {
                                Logger.Info(String.Format("File moved! New place: {0}", destTarolasiHely.ToExternalLink()));
                                sharepointFileMoved = true;
                                #region SPS Delete file
                                Result delete_result = DeleteExternalFile("SharePoint"
                                    , srcTarolasihely.RootSiteCollectionUrl
                                    , srcTarolasihely.DoktarSitePath
                                    , srcTarolasihely.DoktarDocLibPath
                                    , srcTarolasihely.DoktarFolderPath
                                    , srcTarolasihely.FileName);
                                if (delete_result.IsError)
                                {
                                    Logger.Warn(String.Format("Delete error occured. Problem Code: {0}; Error Message: {1}"
                                        , delete_result.ErrorCode
                                        , delete_result.ErrorMessage));
                                }
                                #endregion
                            }
                            else
                            {
                                Logger.Error("SPSUpload failed.", tarhely_ExecParam.Clone(), upload_result);
                                Logger.Info("Skip document");
                                sharepointFileMoved = false;
                            }

                            #endregion
                        }
                    }
                    if (sharepointFileMoved)
                    {
                        ExecParam xpmDokumentumUpdate = execParam.Clone();
                        xpmDokumentumUpdate.Record_Id = dokumentumId;

                        if (String.IsNullOrEmpty(dokumentumVer))
                        {
                            Logger.Debug(String.Format("Dokumentumok.Get: {0}", dokumentumId));
                            Result resDokumentumGet = svcDokumentumok.Get(xpmDokumentumUpdate);

                            if (resDokumentumGet.IsError)
                            {
                                Logger.Error("Dokumentumok.Get hiba", xpmDokumentumUpdate, resDokumentumGet);
                                throw new ResultException(resDokumentumGet);
                            }

                            dokumentumVer = (resDokumentumGet.Record as KRT_Dokumentumok).Base.Ver;

                        }

                        KRT_Dokumentumok dokumentum = new KRT_Dokumentumok();
                        dokumentum.Updated.SetValueAll(false);
                        dokumentum.Base.Updated.SetValueAll(false);

                        dokumentum.Base.Ver = dokumentumVer;
                        dokumentum.Base.Updated.Ver = true;

                        dokumentum.External_Info = destTarolasiHely.ToExternalInfo();
                        dokumentum.Updated.External_Info = true;

                        dokumentum.External_Link = destTarolasiHely.ToExternalLink();
                        dokumentum.Updated.External_Link = true;

                        Logger.Debug(String.Format("Dokumentumok.Update: {0}", dokumentumId));
                        Result resDokumentumUpdate = svcDokumentumok.Update(xpmDokumentumUpdate, dokumentum);

                        if (resDokumentumUpdate.IsError)
                        {
                            Logger.Error("Dokumentumok.Update hiba", xpmDokumentumUpdate, resDokumentumUpdate);
                            throw new ResultException(resDokumentumUpdate);
                        }
                    }
                    #endregion
                }
                
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("Selejtezes_MoveDocument hiba", execParam, res);

            //Ha a sharepoint f�jl �t lett helyezve, de a dokumentumok t�bla update-je sikertelen, 
            //visszahelyezz�k a sharepointban az eredeti helyre
            if (sharepointFileMoved)
            {
                AxisUploadToSps.Upload upld = GetAxisUploadToSps();
                string resUploadString = upld.MoveFiles(new string[1] { destTarolasiHely.ToExternalLink() }, new string[1] { srcTarolasihely.ToExternalLink() }, new string[1] { dokumentumVerzioJel });
            }
        }
        Logger.Info("Selejtezes_MoveDocument vege");
        return res;
    }

    private Result Selejtezes_DeleteDokument(ExecParam execParam, Utility.TarolasiHelyStruktura tarolasiHely, string dokumentumId, string dokumentumVer, string dokumentumVerzioJel)
    {
        Logger.Info("Selejtezes_DeleteDokument kezdete");
        Result res = new Result();

        try
        {
            Logger.Debug(String.Format("tarolasiHely:{0}", tarolasiHely.ToExternalLink()));
            Logger.Debug(String.Format("dokumentumId:{0}", dokumentumId ?? "NULL"));
            Logger.Debug(String.Format("dokumentumVer:{0}", dokumentumVer ?? "NULL"));

            if (String.IsNullOrEmpty(dokumentumId))
            {
                throw new ResultException("DokumentumId param�ter �res!");
            }

            #region SPS Delete file
            Result delete_result = DeleteExternalFile("SharePoint"
                , tarolasiHely.RootSiteCollectionUrl
                , tarolasiHely.DoktarSitePath
                , tarolasiHely.DoktarDocLibPath
                , tarolasiHely.DoktarFolderPath
                , tarolasiHely.FileName);
            if (delete_result.IsError)
            {
                Logger.Error(String.Format("DeleteExternalFile hiba. ErrorCode: {0}; ErrorMessage: {1}", delete_result.ErrorCode, delete_result.ErrorMessage));
                throw new ResultException(delete_result);
            }
            #endregion

            KRT_DokumentumokService svcDokumentumok = new KRT_DokumentumokService();
            ExecParam xpmDokumentumInvalidate = execParam.Clone();
            xpmDokumentumInvalidate.Record_Id = dokumentumId;
            Logger.Debug(String.Format("Dokumentumok.Invalidate: {0}", dokumentumId));
            Result resDokumentumInvalidate = svcDokumentumok.Invalidate(xpmDokumentumInvalidate);

            if (resDokumentumInvalidate.IsError)
            {
                Logger.Error("Dokumentumok.Update hiba", xpmDokumentumInvalidate, resDokumentumInvalidate);
                throw new ResultException(resDokumentumInvalidate);
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("Selejtezes_DeleteDokument hiba", execParam, res);
        }
        Logger.Info("Selejtezes_DeleteDokument vege");
        return res;
    }
    #endregion

    private Result MoveDokument(ExecParam execParam, Utility.TarolasiHelyStruktura srcTarolasihely, Utility.TarolasiHelyStruktura destTarolasiHely, string dokumentumId, string dokumentumVer, string dokumentumVerzioJel)
    {
        Logger.Info("MoveDocument kezdete");
        Result res = new Result();
        bool sharepointFileMoved = false;
        try
        {
            Logger.Debug(String.Format("srcTarolasiHely:{0}",srcTarolasihely.ToExternalLink()));
            Logger.Debug(String.Format("destTarolasiHely:{0}",destTarolasiHely.ToExternalLink()));
            Logger.Debug(String.Format("dokumentumId:{0}",dokumentumId ?? "NULL"));
            Logger.Debug(String.Format("dokumentumVer:{0}",dokumentumVer ?? "NULL"));

            if (String.IsNullOrEmpty(dokumentumId))
            {
                throw new ResultException("DokumentumId param�ter �res!");
            }

            if (srcTarolasihely.DoktarSitePath.Contains("IKTATOTTANYAGOK"))
            {
                Logger.Debug("IKTATOTTANYAGOK");

                if (srcTarolasihely.DoktarFolderPath != destTarolasiHely.DoktarFolderPath)
                {
                    Logger.Debug("srcTarolasihely.DoktarFolderPath != destTarolasiHely.DoktarFolderPath");

                    //Megvizsg�ljuk nem tartozik-e t�bb irathoz a sharepoint dokumentum
                    KRT_DokumentumokService svcDokumentumok = new KRT_DokumentumokService();
                    ExecParam xpmDokumentumGetAll = execParam.Clone();
                    KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
                    schDokumentumok.FajlNev.Value = srcTarolasihely.FileName;
                    schDokumentumok.FajlNev.Operator = Query.Operators.equals;
                    schDokumentumok.External_Info.Value = srcTarolasihely.ToExternalInfo();
                    schDokumentumok.External_Info.Operator = Query.Operators.equals;
                    Result resDokumentumokGetAll = svcDokumentumok.GetAll(xpmDokumentumGetAll, schDokumentumok);

                    if (resDokumentumokGetAll.IsError)
                    {
                        Logger.Error("Dokumentumok.GetAll hiba", xpmDokumentumGetAll, resDokumentumokGetAll);
                        throw new ResultException(resDokumentumokGetAll);
                    }

                    int num = resDokumentumokGetAll.Ds.Tables[0].Rows.Count;

                    Logger.Debug(String.Format("Dokumentumok szama: {0}",num));
                    AxisUploadToSps.Upload upld = GetAxisUploadToSps();

                    //Ha t�bb irathoz is tartozik a dokumentum, akkor csak m�soljuk, nem mozgatjuk
                    if (num > 1)
                    {
                        Logger.Debug(String.Format("CopyFiles: form: {0},  to: {0}", srcTarolasihely.ToExternalLink(), destTarolasiHely.ToExternalLink()));
                        string resUploadString = upld.CopyFiles(new string[1] { srcTarolasihely.ToExternalLink() }, new string[1] { destTarolasiHely.ToExternalLink() }, new string[1]{dokumentumVerzioJel});

                        if (!String.IsNullOrEmpty(resUploadString))
                        {
                            Result resUpload = Utility.GetResultFromXml(resUploadString);
                            Logger.Error(" AxisUploadToSps.Upload.CopyFiles hiba", execParam, resUpload);
                            throw new ResultException(resUpload);
                        }
                    }
                    else
                    {
                        Logger.Debug(String.Format("MoveFiles: form: {0},  to: {0}", srcTarolasihely.ToExternalLink(), destTarolasiHely.ToExternalLink()));
                        string resUploadString = upld.MoveFiles(new string[1] { srcTarolasihely.ToExternalLink() }, new string[1] { destTarolasiHely.ToExternalLink() }, new string[1] {dokumentumVerzioJel});

                        if (!String.IsNullOrEmpty(resUploadString))
                        {
                            Result resUpload = Utility.GetResultFromXml(resUploadString);
                            Logger.Error(" AxisUploadToSps.Upload.MovieFiles hiba", execParam, resUpload);
                            throw new ResultException(resUpload);
                        }

                        sharepointFileMoved = true;
                    }

                    ExecParam xpmDokumentumUpdate = execParam.Clone();
                    xpmDokumentumUpdate.Record_Id = dokumentumId;

                    if (String.IsNullOrEmpty(dokumentumVer))
                    {
                        Logger.Debug(String.Format("Dokumentumok.Get: {0}", dokumentumId));
                        Result resDokumentumGet = svcDokumentumok.Get(xpmDokumentumUpdate);

                        if (resDokumentumGet.IsError)
                        {
                            Logger.Error("Dokumentumok.Get hiba", xpmDokumentumUpdate, resDokumentumGet);
                            throw new ResultException(resDokumentumGet);
                        }

                        dokumentumVer = (resDokumentumGet.Record as KRT_Dokumentumok).Base.Ver;

                    }

                    KRT_Dokumentumok dokumentum = new KRT_Dokumentumok();
                    dokumentum.Updated.SetValueAll(false);
                    dokumentum.Base.Updated.SetValueAll(false);

                    dokumentum.Base.Ver = dokumentumVer;
                    dokumentum.Base.Updated.Ver = true;

                    dokumentum.External_Info = destTarolasiHely.ToExternalInfo();
                    dokumentum.Updated.External_Info = true;

                    dokumentum.External_Link = destTarolasiHely.ToExternalLink();
                    dokumentum.Updated.External_Link = true;

                    Logger.Debug(String.Format("Dokumentumok.Update: {0}", dokumentumId));
                    Result resDokumentumUpdate = svcDokumentumok.Update(xpmDokumentumUpdate, dokumentum);

                    if (resDokumentumUpdate.IsError)
                    {
                        Logger.Error("Dokumentumok.Update hiba", xpmDokumentumUpdate, resDokumentumUpdate);
                        throw new ResultException(resDokumentumUpdate);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("MoveDocument hiba", execParam, res);

            //Ha a sharepoint f�jl �t lett helyezve, de a dokumentumok t�bla update-je sikertelen, 
            //visszahelyezz�k a sharepointban az eredeti helyre
            if (sharepointFileMoved)
            {
                AxisUploadToSps.Upload upld = GetAxisUploadToSps();
                string resUploadString = upld.MoveFiles(new string[1] { destTarolasiHely.ToExternalLink() }, new string[1] { srcTarolasihely.ToExternalLink() }, new string[1] {dokumentumVerzioJel});
            }
        }
        Logger.Info("MoveDocument vege");
        return res;
    }

    // ------------------------------------------------------------------------------------------------------
    [WebMethod]
    public Result UploadToUCM(ExecParam execParam, byte[] content, string fileName, string ugyiratszam, string alszam)
    {
        UCMDocumentumManager manager = new UCMDocumentumManager();
        Result uploadResult = manager.Upload(content, fileName, ugyiratszam, null, alszam, null, null, null, null, false);
        if (!uploadResult.IsError)
        {
            string path = uploadResult.Uid;
            string externalLink = NMHH.UCM.Servlet.ServletHelper.GetExternalLink(path);
            uploadResult.Uid = externalLink;
        }
        return uploadResult;
    }


    public static class Utility
    {
        /// <summary>
        /// Elejerol es vegerol leveszi a / vagy \ jelet.
        /// Elejerol es vegerol leveszi a "." jelet.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveSlash(String s)
        {
            if (String.IsNullOrEmpty(s)) return String.Empty;
            s = s.Replace('\\', '/');
            string x = s.Trim().StartsWith("/") ? s.Trim().Substring(1) : s.Trim();
            x = x.EndsWith("/") ? x.Substring(0, x.Length - 1) : x;

            //LZS - BUG_5520 - Remove "."
            x = x.Trim().StartsWith(".") ? x.Trim().Substring(1) : x.Trim();
            x = x.EndsWith(".") ? x.Substring(0, x.Length - 1) : x;

            return x;
        }


        public static AxisUploadToSps.Upload GetAxisUploadToSps()
        {
            Logger.Info("GetAxisUploadToSps indul.");

            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

            Logger.Info(String.Format("Upload Credentials: {0}\\{1}", __doamin, __usernev));

            AxisUploadToSps.Upload upld = new AxisUploadToSps.Upload();
            upld.Url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AxisUploadToSps.upload");

            Logger.Info(String.Format("Upload url: {0}", upld.Url));

            upld.Credentials = new System.Net.NetworkCredential(__usernev, __password, __doamin);

            return upld;
        }

        public static ICredentials GetAxisSpsCredential()
        {
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __domain = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

            return new System.Net.NetworkCredential(__usernev, __password, __domain);
        }

   
        /// <summary>
        /// Az uploadban hasznalt xmlStrParambol visszaadja a megadott nevu parameter erteket.
        /// </summary>
        /// <param name="pname">A parameter neve.</param>
        /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
        /// <returns></returns>
        public static string GetParam(string pname, XmlDocument xdoc)
        {
            //Logger.Info(String.Format("GetParam - Keresett par: {0} - ebben:  {1}", pname, xdoc.OuterXml.ToString()));

            string _retval = String.Empty;

            if (xdoc == null) return _retval;

            string xpath = String.Format("//{0}", pname);

            if (xdoc.SelectNodes(xpath).Count == 1)
            {
                try
                {
                    XmlNode xn = xdoc.SelectSingleNode(xpath);
                    if (xn != null)
                    {
                        _retval = Convert.ToString(xn.InnerText);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("DocumentService.GetParam hiba!");
                    Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                    Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
                }
            }

            return _retval;
        }

        /// <summary>
        /// Az uploadban hasznalt xmlStrParambol visszaadja a megadott nevu parameter erteket, mint XML-t.
        /// </summary>
        /// <param name="pname">A parameter neve.</param>
        /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
        /// <returns></returns>
        public static string GetParamXml(string pname, XmlDocument xdoc)
        {
            //Logger.Info(String.Format("GetParam - Keresett par: {0} - ebben:  {1}", pname, xdoc.OuterXml.ToString()));

            string _retval = String.Empty;

            if (xdoc == null) return _retval;

            string xpath = String.Format("//{0}", pname);

            if (xdoc.SelectNodes(xpath).Count == 1)
            {
                try
                {
                    XmlNode xn = xdoc.SelectSingleNode(xpath);
                    if (xn != null)
                    {
                        _retval = Convert.ToString(xn.InnerXml);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("DocumentService.GetParam hiba!");
                    Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                    Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
                }
            }

            return _retval;
        }


        /// <summary>
        /// A KRT_Dokumentumok External_Info mez�b�l kiszedi a sharepointos t�rol�si strukt�r�t
        /// </summary>
        /// <param name="external_info"></param>
        /// <param name="rootSiteCollectionUrl"></param>
        /// <param name="doktarSitePath"></param>
        /// <param name="doktarDocLibPath"></param>
        /// <param name="doktarFolderPath"></param>
        public static void GetSpsTarolasiHelyStrukturaFromExternalInfo(string external_info, out string rootSiteCollectionUrl
            , out string doktarSitePath, out string doktarDocLibPath, out string doktarFolderPath)
        {
            Logger.Info("External_Info mez� feldolgoz�sa... (External_Info: " + external_info + ")");

            string[] parts = external_info.Split(';');

            if (parts.Length != 5)
            {
                throw new Exception("Hib�s form�tum� External_Info mez�");
            }

            rootSiteCollectionUrl = parts[1];
            doktarSitePath = parts[2];
            doktarDocLibPath = parts[3];
            doktarFolderPath = parts[4];
        }

        public class TarolasiHelyStruktura
        {
            private string spsMachineName = String.Empty;

            public string SpsMachineName
            {
                get { return spsMachineName; }
                set { spsMachineName = value; }
            }

            private string rootSiteCollectionUrl = String.Empty;

            public string RootSiteCollectionUrl
            {
                get { return rootSiteCollectionUrl; }
                set { rootSiteCollectionUrl = value; }
            }

            private string doktarSitePath = String.Empty;

            public string DoktarSitePath
            {
                get { return doktarSitePath; }
                set { doktarSitePath = value; }
            }

            private string doktarDocLibPath = String.Empty;

            public string DoktarDocLibPath
            {
                get { return doktarDocLibPath; }
                set { doktarDocLibPath = value; }
            }

            private string doktarFolderPath = String.Empty;

            public string DoktarFolderPath
            {
                get { return doktarFolderPath; }
                set { doktarFolderPath = value; }
            }

            private string fileName = String.Empty;

            public string FileName
            {
                get { return fileName; }
                set { fileName = value; }
            }

            public string SharePointFileName
            {
                get
                {
                    return Contentum.eUtility.FileFunctions.GetValidSharePointFileName(fileName);
                }
            }

            private const string infoSeperator = ";";
            private const string linkSeperator = "/";

            public TarolasiHelyStruktura()
            {
                this.spsMachineName = Contentum.eUtility.UI.GetAppSetting("SharePointUrl");
            }

            public TarolasiHelyStruktura(string External_Info, string fileName)
            {
                string[] parts = External_Info.Split(new string[1]{infoSeperator}, StringSplitOptions.None);

                if (parts.Length != 5)
                {
                    throw new Exception("Hib�s form�tum� External_Info mez�");
                }

                this.spsMachineName = parts[0];
                this.rootSiteCollectionUrl = parts[1];
                this.doktarSitePath = parts[2];
                this.doktarDocLibPath = parts[3];
                this.doktarFolderPath = parts[4];
                this.fileName = fileName;
            }

            public string ToExternalInfo()
            {
                string info = this.spsMachineName;
                info += infoSeperator;
                info += this.rootSiteCollectionUrl;
                info += infoSeperator;
                info += this.doktarSitePath;
                info += infoSeperator;
                info += this.doktarDocLibPath;
                info += infoSeperator;
                info += this.doktarFolderPath;
                return info;
            }


            private string RemoveSlash(string text)
            {
                if (String.IsNullOrEmpty(text))
                    return String.Empty;

                return text.TrimEnd(linkSeperator.ToCharArray()).TrimStart(linkSeperator.ToCharArray());
            }
            public string ToExternalLink()
            {
                string link = RemoveSlash(this.spsMachineName);
                link += linkSeperator;
                link += RemoveSlash(this.rootSiteCollectionUrl);
                link += linkSeperator;
                link += RemoveSlash(this.doktarSitePath);
                link += linkSeperator;
                link += RemoveSlash(this.doktarDocLibPath);
                if (!String.IsNullOrEmpty(doktarFolderPath))
                {
                    link += linkSeperator;
                    link += RemoveSlash(this.doktarFolderPath);
                }
                link += linkSeperator;
                link += RemoveSlash(this.fileName);
                return link;
            }

            public string GetItemSitePath()
            {
                string itemSitePath = RemoveSlash(this.spsMachineName);
                itemSitePath += linkSeperator;
                itemSitePath += RemoveSlash(this.rootSiteCollectionUrl);
                itemSitePath += linkSeperator;
                itemSitePath += RemoveSlash(this.doktarSitePath);
                itemSitePath += linkSeperator;

                return itemSitePath;
            }
        }

        public static Result GetResultFromXml(string resultXml)
        {
            Result res = new Result();
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(resultXml);

                res.ErrorCode = docResult.SelectSingleNode("result/errorcode").InnerText;
                res.ErrorMessage = docResult.SelectSingleNode("result/errormessage").InnerText;
            }
            catch (Exception ex)
            {
                res = ResultException.GetResultFromException(ex);
            }

            return res;
        }

        public static string GetIktatottAnyagFolderPath(string foszam, string alszam)
        {
            string ret = String.Empty;
            if (!String.IsNullOrEmpty(foszam))
            {
                ret = foszam;

                if (!String.IsNullOrEmpty(alszam))
                {
                    ret += "/" + alszam;
                }
                else
                {
                    Logger.Error("GetIktatottAnyagFolderPath hiba: alszam null or empty");
                }
            }
            else
            {
                Logger.Error("GetIktatottAnyagFolderPath hiba: foszam null or empty");
            }

            return ret;
        }


    }


} //class